---
title: 2021-02-06-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MountSefton_EN-CN4284831269_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-02-06 22:29:42
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MountSefton_EN-CN4284831269_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [C++11 Tuple Implementation Details (2012)](http://mitchnull.blogspot.com/2012/06/c11-tuple-implementation-details-part-1.html)
> 概要: C++11 Tuple Implementation Details (2012)
### [组图：张纪中吕良伟聚餐 仅差5岁“一黑一白”像隔代](http://slide.ent.sina.com.cn/star/slide_4_704_352252.html)
> 概要: 组图：张纪中吕良伟聚餐 仅差5岁“一黑一白”像隔代
### [生活类家庭喜剧，过气编剧逆袭变身价](https://news.dmzj1.com/article/70079.html)
> 概要: 2021年1月上新剧《写不出来！？~编剧吉丸圭佑的无剧本生活~》现已播出，预计全篇共8集，单集50分钟左右，情节紧凑生活感极强，由生田斗真、吉濑美智子主演，讲述男主这位偶尔写写剧本的家庭主夫被选为黄金档连续剧的编剧，为了事业和家庭全力以赴的轻喜剧...
### [解约失败！黄婷婷被判继续和丝芭履行合约](https://ent.sina.com.cn/s/m/2021-02-06/doc-ikftssap4412094.shtml)
> 概要: 新浪娱乐讯 近日，一份SNH48黄婷婷与丝芭传媒解约的判决文书流出，文书显示黄婷婷被判继续和丝芭履行合约，解约官司完败且需要承担所有诉讼费用。此前，黄婷婷曾向丝芭单方面提出解约......
### [「Newtype」3月号杂志封面公开](http://acg.178.com//202102/406578327170.html)
> 概要: 近日，杂志「Newtype」公开了3月号的封面图，本次封面图绘制的是「进击的巨人」中的三笠和利威尔。「进击的巨人」是日本漫画家谏山创创作的少年漫画作品，于2009年开始连载。该作品曾获2011年“这本......
### [「泽塔奥特曼英雄传 英雄历程」第六话预告PV公开](http://acg.178.com//202102/406578359322.html)
> 概要: 今日（2月6日），奥特空想特摄系列剧集「泽塔奥特曼英雄传 英雄历程」公开了第六话的预告PV。本次剧集系列的播出时间为每周六的早9:30分。「泽塔奥特曼英雄传 英雄历程」第六话预告PV该作讲述的是奥特英......
### [天问一号传回首幅火星图像 地貌特征清晰可见](https://www.3dmgame.com/bagua/4291.html)
> 概要: 媒体从中国国家航天局获悉，2021年2月5日20时，首次火星探测任务天问一号探测器发动机点火工作，顺利完成地火转移段第四次轨道中途修正，以确保按计划实施火星捕获。截至目前，天问一号已在轨飞行约197天......
### [《音乐之声》上校去世 曾出演《利刃出鞘》 享年91岁](https://www.3dmgame.com/bagua/4292.html)
> 概要: 加拿大演员克里斯托弗·普卢默去世，享年91岁。他是《音乐之声》的“上校”，也出演了《美丽心灵》《最后一站》《初学者》《飞屋环游记》《龙纹身的女孩》《十二猴子》等。近年仍很活跃，在《金钱世界》《利刃出鞘......
### [组图：沈月中国风大片超惊艳 长发拉弓飒爽十足](http://slide.ent.sina.com.cn/star/slide_4_704_352258.html)
> 概要: 组图：沈月中国风大片超惊艳 长发拉弓飒爽十足
### [“民工歌神”吴恩师的互联网奇幻漂流](https://www.tuicool.com/articles/quIfQjq)
> 概要: 吴恩师走红于2020年的夏天。因为一条对着镜头唱情歌的视频，这位相貌平平无奇的农民大叔成了网友心中的“民工歌神”，被认为拥有“被上帝吻过的嗓子”。导演贾樟柯也在微博转发了这条视频，写道：“这才是唱歌”......
### [愿你不孤单，一份春节美滋滋书影音清单](https://www.huxiu.com/article/408027.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 水原瓜子题图 | 虎扯自制本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。记得一定读到文末，有......
### [组图：“甜茶”笑容灿烂现身片场 与莱昂纳多等全A咖阵容合作新片](http://slide.ent.sina.com.cn/star/slide_4_704_352263.html)
> 概要: 组图：“甜茶”笑容灿烂现身片场 与莱昂纳多等全A咖阵容合作新片
### [《工作细胞》中文配音版正式预告：关于身体的故事](https://acg.gamersky.com/news/202102/1361240.shtml)
> 概要: 《工作细胞》中配版正式预告发布了~
### [「Soleil opened!」巧克力 赛车女郎手办开订](http://acg.178.com//202102/406591024148.html)
> 概要: 「Soleil opened!」巧克力 赛车女郎手办开订，作品采用ABS、PVC材质，全高约235mm，日版售价14500日元（含税），约合人民币935元，预计将于2021年5月发售......
### [AI maths whiz creates tough new problems for humans to solve](https://www.nature.com/articles/d41586-021-00304-8)
> 概要: AI maths whiz creates tough new problems for humans to solve
### [第99号元素，发现近70年后终于开始露出真面目](https://www.huxiu.com/article/408846.html)
> 概要: 美国伯克利实验室的科学家们对这种高放射性元素进行实验，揭示出锿元素意料之外的性质。本文来自微信公众号：科研圈（ID：keyanquan），作者：谢一璇，原文标题：《这个以爱因斯坦命名的元素，发现100......
### [动画《环太平洋：黑色禁区》公布正式预告](https://news.dmzj1.com/article/70080.html)
> 概要: Netflix公布了《环太平洋》系列原创动画《环太平洋：黑色禁区》的第1弹预告及海报。
### [动画《美妙世界》4月9日开播，新PV公布](https://news.dmzj1.com/article/70082.html)
> 概要: TV动画《美妙世界 The Animation》确定将于4月9日开始播出，新PV《OP rap ver.》也一并得到了公布。
### [上海新房摇号首次采用“计分排序” 满足这些条件家庭能优先](https://finance.sina.com.cn/china/2021-02-06/doc-ikftpnny5465910.shtml)
> 概要: 2月6日，上海新一轮楼市调控“沪十条”中有关新房摇号的新规正式落地实施。闵行区“永康城浦上雅苑”、宝山区“经纬城市绿洲家园三期”...
### [有私募基金人均赚近一亿！难怪这么多公募大佬想奔私](https://finance.sina.com.cn/wm/2021-02-06/doc-ikftssap4482885.shtml)
> 概要: 还在为某公募基金经理7000万元年终奖的传闻感到震惊？有私募基金人均赚近一亿元！ 私募基金被称为资产管理业“皇冠上的明珠”。一方面，金融圈大佬大多都有一个私募梦...
### [组图：戚薇银发梦境大片摩登复古 金色吊带裙勾勒曼妙身材](http://slide.ent.sina.com.cn/slide_4_704_352276.html)
> 概要: 组图：戚薇银发梦境大片摩登复古 金色吊带裙勾勒曼妙身材
### [公募调研忙“春播” 这一冷门行业突然火了！](https://finance.sina.com.cn/china/2021-02-06/doc-ikftpnny5472603.shtml)
> 概要: 公募调研忙“春播”，这一冷门行业突然火了！明星基金经理扎堆关注 原创曹雯璟中国基金报 开年迄今，公募基金正紧锣密鼓对上市公司进行调研，为年后“春播”布局积极做准备。
### [发行方评级公信力迷失 交易商协会强化市场选择评级报告](https://finance.sina.com.cn/roll/2021-02-06/doc-ikftpnny5477114.shtml)
> 概要: 原标题：发行方评级公信力迷失 交易商协会强化市场选择评级报告 来源：中国经营网 本报记者 张晓迪 北京报道 继债券发行注册制后，债券市场又迎来了一件大事。
### [尴尬了，《斗罗大陆》获官媒发文表扬，却被曝片头涉嫌抄袭游戏CG](https://new.qq.com/omn/20210206/20210206A0BIH300.html)
> 概要: 近日，电视剧《斗罗大陆》正式在央8开播，虽然是中午档，但剧集目前的收视和口碑都在水准线之上，并且，主演肖战与吴宣仪两人在新人时期的演技也可圈可点，让人倍感惊讶。            2月6日上午，在......
### [北京将发放5万个200元数字人民币红包，如何领取？攻略来了](https://finance.sina.com.cn/china/dfjj/2021-02-06/doc-ikftssap4496614.shtml)
> 概要: “数字王府井 冰雪购物节”活动来了！本次活动由北京市东城区人民政府主办，面向在京个人消费者发放5万个数字人民币红包。红包采取抽签形式发放...
### [《乘风破浪的姐姐2》加长版，那英透露很焦虑，跨界表演不容易](https://new.qq.com/omn/20210206/20210206A0BR4A00.html)
> 概要: 《乘风破浪的姐姐2》第一次公演舞台，那英与王鸥、左小青合作，带来了唱跳表演《在这里请你随意》，那英在现场的表现很好，在舞台上也很自信，但是在节目放出的Plus加长版中，那英却透露自己很焦虑，在练习舞蹈......
### [3DM轻松一刻第459期 我为啥第一眼不是在看妹子](https://www.3dmgame.com/bagua/4293.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！金角大王原来是中空？这是神的力量吗？不，这是科学给大......
### [俄美女COS《LOL》KDA阿狸 黑丝兔耳超性感](https://www.3dmgame.com/bagua/4294.html)
> 概要: 今天为大家带来的是俄罗斯美女Coser shirogane_sama的Cos作品，近日她放出了《英雄联盟》KDA兔女郎版的阿狸Cos，颇为性感，吸引了众多粉丝点赞......
### [视觉Transformer最新综述](https://www.tuicool.com/articles/iYbE3uM)
> 概要: ©PaperWeekly 原创 · 作者｜张一帆学校｜华南理工大学本科生研究方向｜CV，Causality本文是对论文 A Survey on Visual Transformer 的阅读，以及自己对......
### [滴滴回应 “司机被醉酒乘客辱骂殴打”：已对叫车人进行封禁处理，永久不允许其使用服务](https://www.ithome.com/0/534/308.htm)
> 概要: 近日，针对 1 月 27 日晚发生的滴滴司机被醉酒乘客辱骂殴打事件，滴滴在官微回应称，平台决定对叫车人即视频中的女乘客进行封禁处理，永久不允许其使用滴滴服务。根据滴滴通报，1 月 28 日早上 8 点......
### [陈羽凡与友人参加聚会唱歌，身材发福啤酒肚明显有点认不出](https://new.qq.com/omn/20210206/20210206A0C8YZ00.html)
> 概要: 陈羽凡与友人参加聚会唱歌，身材发福啤酒肚明显有点认不出
### [重磅！科兴新冠疫苗获批附条件上市 注射器需求暴增](https://finance.sina.com.cn/wm/2021-02-06/doc-ikftpnny5486346.shtml)
> 概要: 好消息，科兴中维新冠病毒疫苗获批附条件上市！ 随着国内新冠疫苗相继获批附条件上市，叠加全球疫苗接种计划迅速推进，一时之间，令注射器成为刚需。
### [传《战地6》将有免费元素 支持跨平台联机](https://www.3dmgame.com/news/202102/3808070.html)
> 概要: 推主Battlefield 6 News曝光了《战地6》的更多细节：包括一个战斗通行证更大/更好的Levolution系统免费游玩元素（此前的传闻中曾指出《战地6》有大逃杀模式）新的单位（3-5人小队......
### [富士康年会现场！郭台铭豪送十台车、半亿现金，小公主惊喜助阵](https://new.qq.com/omn/20210206/20210206A0CKI300.html)
> 概要: 作为全球最大数码制造商之一、代工产业翘楚的鸿海集团（富士康），其商业帝国遍布世界各地、横跨欧、美、亚三大洲，员工人数超过120万，市值近800亿美元。此番体量的上市集团，每年的年终“尾牙”（年会）备受......
### [王腾 Redmi K40 截图泄露屏幕比例  20:9 且孔径很小，强烈暗示小米 MIX 新品今年的确有](https://www.ithome.com/0/534/313.htm)
> 概要: IT之家2月6日消息 根据最新的微博小尾巴，小米卢伟冰、王腾等人已经用上了 Redmi K40，Redmi 产品总监王腾更是激动宣布微博粉丝突破 50 万，并且还晒出了 Redmi K40 手机的上微......
### [太拼了！49岁陈志朋继减肥24斤后，又在舞台上大秀一字马](https://new.qq.com/omn/20210206/20210206A0CJQI00.html)
> 概要: 近几年来，陈志朋的复出之路略为坎坷，特别是在走红毯以奇装异服来博眼球受到不少网友的吐槽，对比曾经的队友苏有朋、吴奇隆，陈志朋的处境令人心酸不已。不过功夫不负有心人，在参加《追光吧哥哥》后，竟然真的有翻......
# 小说
### [我给万物加个点](https://m.qidian.com/book/1015338557/catalog)
> 作者：常世

> 标签：青春日常

> 简介：【高订2.6w+的精品小说，起点编辑联合推荐】给物品加点，可以让物品拥有奇怪的特殊能力，给动植物加点，可以改变它们的形态，可以点化它们成妖怪，给自己加点，可以获得种种技能，天赋和超能力，这是一个有趣的故事，生活向轻松小说。已给系统，主角自己，手机，游泳池，马戏团，书本，沙漏，豹子，竹林等等加点...尽情期待后续内容。===常世读者1群：664616129（2000人已满，所以加2群吧）=======常世读者2群：233122761（很多空位哟）=========全订读者群：777015057（粉丝值8000以上，加群前请找管理验证截图）=====已有高订8000+精品完本小说《神级采集术》，两本书连载期间，从未断更。请放心开宰！

> 章节总数：共952章

> 状态：完本
# 论文
### [Type4Py: Deep Similarity Learning-Based Type Inference for Python](https://paperswithcode.com/paper/type4py-deep-similarity-learning-based-type)
> 日期：12 Jan 2021

> 标签：TYPE PREDICTION

> 代码：https://github.com/saltudelft/type4py

> 描述：Dynamic languages, such as Python and Javascript, trade static typing for developer flexibility. While this allegedly enables greater productivity, lack of static typing can cause runtime exceptions, type inconsistencies, and is a major factor for weak IDE support.
### [A Framework for Authorial Clustering of Shorter Texts in Latent Semantic Spaces](https://paperswithcode.com/paper/a-framework-for-authorial-clustering-of)
> 日期：30 Nov 2020

> 标签：FEATURE SELECTION

> 代码：https://github.com/rtrad89/authorship_clustering_code_repo

> 描述：Authorial clustering involves the grouping of documents written by the same author or team of authors without any prior positive examples of an author's writing style or thematic preferences. For authorial clustering on shorter texts (paragraph-length texts that are typically shorter than conventional documents), the document representation is particularly important: very high-dimensional feature spaces lead to data sparsity and suffer from serious consequences like the curse of dimensionality, while feature selection may lead to information loss.
