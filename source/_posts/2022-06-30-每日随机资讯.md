
---
title: 2022-06-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CoteSauvage_ZH-CN9967984163_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-06-30 23:11:18
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CoteSauvage_ZH-CN9967984163_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [抖音达人困境：平台变现更难了，留下还是离开？](http://www.woshipm.com/it/5508696.html)
> 概要: 编辑导语：今年618，抖音直播电商销售额同比增长124%，与此同时，行业头部达人的直播带货份额，却急剧下滑。这极大的反差让抖音达人开始思考，平台变现更难了，留下还是离开？一起来看看吧！“之前一年能赚五......
### [揭秘算法推荐：统计、分类与分发织成信息牢笼](http://www.woshipm.com/it/5508595.html)
> 概要: 编辑导语：在信息传播过程中，算法推荐具有垄断性与不稳定性，极易对普通用户造成信息牢笼。对待千人千面的算法推荐，不同的人有不同的态度，有人欢喜，有人愁。算法推荐未来会走向何方呢？一起来看看吧！“让人类永......
### [MBTI 职场图鉴：ESTJ 想当霸总，INFP 擅长摆烂](http://www.woshipm.com/zhichang/5503479.html)
> 概要: 编辑导语：前段时间，MBTI人格测试在年轻人的社交场景中频频出现。如果我们将MBTI与职场环境相结合，你知道哪种人格更适合选择哪种岗位吗？年轻人如果想更快速地获得测试结果，可以选择哪些轻量测试来达成目......
### [日画1978~2007 问题在风中飘扬](https://www.zcool.com.cn/work/ZNjA2NjIwMjg=.html)
> 概要: 2022年6月的每日一画合集。在隔离与解禁之间来回拉扯。虽然意外很多，但心情良好，因为充满希望的七月要来了......
### [蔚来的“糊涂账”，该解释清楚](https://www.huxiu.com/article/595140.html)
> 概要: 出品丨虎嗅汽车组作者丨周到图片丨视觉中国一旦一家公司的业务趋于稳定，那么它的其他问题就会成为外界关注的焦点。蔚来便是如此。美国东部时间6月28日，做空机构灰熊（Grizzly Research）发布报......
### [索尼新PC外设国行售价公开：电竞显示器售6999元](https://www.3dmgame.com/news/202206/3845854.html)
> 概要: 昨晚(6月29日)索尼举办新品发布会，正式推出电竞品牌INZONE，并发布首批两款电竞显示器、三款电竞耳机，均能完美搭档PS5主机。INZONE M9是一款27英寸的4K旗舰显示器，采用全阵列式背光技......
### [P站美图推荐——画师カリマリカCARIMARICA特辑](https://news.dmzj.com/article/74805.html)
> 概要: カリマリカCARIMARICA老师的人体十分美丽，搭配宛若真实的布纹褶皱，太好康了吧！.jpg
### [任天堂称中国主机市场不大 将携手腾讯继续推广NS](https://www.3dmgame.com/news/202206/3845857.html)
> 概要: 早先我们报道过2020年任天堂大量经典游戏源代码被泄露，包括《耀西岛》《超级马里奥赛车》《塞尔达传说：时之笛》等N64游戏。泄露的文件还包含N64、GameCube、NDS、Wii和小神游相关内部文件......
### [动画「青之芦苇」后半季PV公开](http://acg.178.com/202206/450554508580.html)
> 概要: 足球题材漫改TV动画「青之芦苇」发布了后半季PV，本作将于7月2日开始播出后半季。动画「青之芦苇」后半季PVCAST青井葦人：大鈴功起大友栄作：橘龍丸橘総一朗：山下誠一郎冨樫慶司：八代拓黒田勘平：堀江......
### [任天堂在欧洲申请“NSW”商标 或为Switch的新简称](https://www.3dmgame.com/news/202206/3845873.html)
> 概要: 根据Resetera论坛用户的发现，任天堂已经在欧洲提交了“NSW”一词的商标申请，注册日期为2022年6月29日，涵盖了“电子游戏”和“游戏”等描述。目前尚不清楚提交申请的原因，在网络上，“NSW”......
### [硬核科普书《跟动物交换身体》动画电影化 10月上映](https://acg.gamersky.com/news/202206/1495450.shtml)
> 概要: 硬核科普书籍《跟动物交换身体》宣布将推出动画电影，本作将动物的身体结构与人类的身体结构一一结合，构成了极富冲击力的画面。
### [社交媒体让我们更加愤怒了吗？](https://www.huxiu.com/article/595066.html)
> 概要: 本文来自微信公众号：腾讯研究院 （ID：cyberlawrc），作者：梁晓健（腾讯研究院助理研究员），头图来自：unsplash我们似乎生活在一个易怒的时代。知名咨询公司盖洛普从 2005 年开始追踪......
### [轻小说「精灵幻想记」第22卷封面公开](http://acg.178.com/202206/450556324522.html)
> 概要: 轻小说「精灵幻想记」公开了第22卷「精灵幻想记22·纯白方程式」的封面图，该卷将于8月1日正式发售。「精灵幻想记」是北山结莉创作、Riv负责插画的轻小说作品，于2015年10月1日由Hobby JAP......
### [声优偶像宅进入节目制作公司 在推特晒相关消息被开除](https://news.dmzj.com/article/74807.html)
> 概要: 近日，一名喜欢22/7偶像宫濑玲奈的宅，入职了制作由宫濑冠名的娱乐节目的公司YOUDEAL。不过在近日公司后，这位没有老老实实的干活，而是私自回复宫濑发布的推特，另外还在推特上炫耀自己利用工作之便和宫濑有了接触。
### [这次把最近大热的数据可视化行业讲讲透](https://www.zcool.com.cn/article/ZMTQxOTAzNg==.html)
> 概要: 这次把最近大热的数据可视化行业讲讲透
### [动画「继母的拖油瓶是我的前女友」播放前贺图第五弹公开](http://acg.178.com/202206/450557383451.html)
> 概要: 近日，动画「继母的拖油瓶是我的前女友」官方公开了播放前的第五弹贺图，由画师はシソ绘制。该作将于2022年7月6日正式播出。动画「继母的拖油瓶是我的前女友」（継母の連れ子が元カノだった）改编自由纸城境介......
### [「盾之勇者成名录」第2季拉芙塔莉雅手办开订](http://acg.178.com/202206/450557457460.html)
> 概要: 出自动画「盾之勇者成名录」第2季的拉芙塔莉雅幼少化ver.1/7手办现已开启预订。该手办由B´full制作，原型师为矶+今朝丸Moineau，全高约111mm（含底座），材质为PVC/ABS，定价18......
### [万事屋全员大冒险 《梦幻模拟战》x《银魂》联动](https://shouyou.3dmgame.com/news/62806.html)
> 概要: 日本Extreme原厂授权、紫龙游戏研发运营的高策略王道幻想神作《梦幻模拟战》手游今日正式宣布，将在6月30日与经典动画作品《银魂》开展联动合作。本次联动不仅有《银魂》人气角色坂田银时、志村新八及神乐......
### [用人体来表现的动物图鉴！《龟壳就是肋骨》剧场版动画10月公开](https://news.dmzj.com/article/74808.html)
> 概要: 由川崎悟司创作的动物图鉴《龟壳就是肋骨》宣布了将于10月上映动画电影的消息。本作的特报视频也一并公开。
### [知名声优竹达彩奈宣布怀孕 梶裕贵将升级做爸爸](https://acg.gamersky.com/news/202206/1495525.shtml)
> 概要: 日本知名声优竹达彩奈在今天（6月30日）宣布怀孕消息，表示目前处于安定期。同为声优的丈夫梶裕贵也公开了这一喜讯。
### [声优竹达彩奈宣布怀孕！梶裕贵成为父亲](https://news.dmzj.com/article/74811.html)
> 概要: 声优竹达彩奈（33岁）和声优梶裕贵（36岁）在本日（30日）更新的推特，称竹达彩奈已经怀孕。
### [-2022半年总结-](https://www.zcool.com.cn/work/ZNjA2NjkyNzI=.html)
> 概要: 我是景怡，概念设计师、插画师。沉迷人文民俗，市井老街，本土怪谈。喜欢描绘1980-1990年代不知名南方小城的风景，热爱东方美学。这是我2022年上半年创作总结......
### [反垄断压力之下，谷歌与奈飞进行广告合作会有什么利好？](https://finance.sina.com.cn/tech/2022-06-30/doc-imizmscu9479013.shtml)
> 概要: 记者 | 李京亚......
### [百度全资控股集度汽车](https://finance.sina.com.cn/tech/2022-06-30/doc-imizmscu9479673.shtml)
> 概要: 新浪科技讯 6月30日下午消息，天眼查App显示，6月28日，集度汽车有限公司发生工商变更，吉利旗下上海华普汽车有限公司退出股东行列，百度关联公司达孜县百瑞翔创业投资管理有限责任公司持股比例升至100......
### [裕太微拟科创板IPO：募资13亿元投建车载以太网芯片等项目](http://www.investorscn.com/2022/06/30/101576/)
> 概要: 据集微网报道 6月29日,上交所正式受理了裕太微电子股份有限公司(简称:裕太微)科创板上市申请......
### [工信部：我国算力规模全球第二 产业链市场超两万亿](https://finance.sina.com.cn/tech/2022-06-30/doc-imizirav1310953.shtml)
> 概要: 工信部负责人今天（6月30日）在北京举行的中国算力大会新闻发布会上透露，我国近五年算力年均增速超过30%，算力规模排名全球第二......
### [百度回应](https://finance.sina.com.cn/tech/2022-06-30/doc-imizmscu9484569.shtml)
> 概要: 相关新闻：......
### [小学馆创立100周年加入元宇宙 柯南等资源众多](https://www.3dmgame.com/news/202206/3845901.html)
> 概要: 6月30日今天，日本出版巨头小学馆宣布迎来创立100周年，作为纪念策划之一，正式加入元宇宙事业，旗下包括 柯南在内IP资源众多，今后将有利于元宇宙活动。•是日本的综合出版社，名称的由来是创办时以出版适......
### [组图：沈月戴贝雷帽现身机场 穿粉色T恤搭背带裤青春靓丽](http://slide.ent.sina.com.cn/star/slide_4_86448_371985.html)
> 概要: 组图：沈月戴贝雷帽现身机场 穿粉色T恤搭背带裤青春靓丽
### [工信部：1-5 月手机产量 6.09 亿台同比下降 1.7%，其中智能手机 4.7 亿台](https://www.ithome.com/0/627/201.htm)
> 概要: IT之家6 月 30 日消息，据工业和信息化部网站，工信部运行监测协调局 6 月 29 日发布了 2022 年 1—5 月份电子信息制造业运行情况。工信部数据显示，1—5 月份，主要产品中，手机产量 ......
### [「36亿美元市值」基因编辑公司深陷专利纠纷，联合创始人为诺奖得主](https://www.tuicool.com/articles/jMVRvuj)
> 概要: 「36亿美元市值」基因编辑公司深陷专利纠纷，联合创始人为诺奖得主
### [微星主板新 BIOS 优化 R7 5800X3D，支持三级性能提升](https://www.ithome.com/0/627/221.htm)
> 概要: IT之家6 月 30 日消息，据 Tom's Hardware 消息，微星为部分 X570S、X570 和 B550 主板推出了新的 Beta BIOS，为 R7 5800X3D 处理器带来了 Kom......
### [融资丨「Magical Mushroom Company」种子轮融资300万英镑，Eco](https://www.tuicool.com/articles/MFzie2I)
> 概要: 融资丨「Magical Mushroom Company」种子轮融资300万英镑，Eco
### [元宇宙引擎的双雄战事（上）](https://www.tuicool.com/articles/Ibi2YrA)
> 概要: 元宇宙引擎的双雄战事（上）
### [组图：孔刘为汤唯新电影打call 亲切称呼她为“汤汤”](http://slide.ent.sina.com.cn/film/k/slide_4_704_371996.html)
> 概要: 组图：孔刘为汤唯新电影打call 亲切称呼她为“汤汤”
### [除了千手和宇智波，木叶村还有哪些家族？](https://new.qq.com/omn/20220630/20220630A08P0O00.html)
> 概要: 动漫火影忍者中，木叶村作为整个忍界之中最强的村子，自然也有很多的家族都加入到了木叶村的阵营之中，也正是因为这些家族之间的团结，才让木叶村能够走向繁荣，那么，木叶村中都有哪些家族呢？          ......
### [从火影忍者到博人传，第七班的画风变了多少？](https://new.qq.com/omn/20220630/20220630A08OXV00.html)
> 概要: 动漫火影忍者如今已经完结，在火影忍者这么多年的发展过程中，忍者们的画风也有了非常大的变化，从最开始的火影忍者到后来的博人传，作为主角的第七班成员在画风上的改变可以说是非常巨大的，这么多年过去了，第七班......
### [开启万花筒写轮眼有多痛苦？](https://new.qq.com/omn/20220630/20220630A08OWW00.html)
> 概要: 动漫火影忍者中，宇智波一族的写轮眼也可以说是最强的一种血继限界，而且这种能力还可以进化，但是普通写轮眼要进化成为万花筒写轮眼的话，也要承受非常强烈的痛苦，那么动漫中开启的万花筒写轮眼中，它们的主人都承......
### [这一次，苹果又被高通拿捏了？](https://www.huxiu.com/article/596018.html)
> 概要: 出品 | 虎嗅科技组作者 | 丸都山头图 | IC Photo“我还是喜欢你桀骜不驯的样子，你稍微恢复一下。”这句《西虹市首富》中的台词，可能无比适用于当下的高通与苹果。过去五年，苹果与高通关于基带芯......
### [常年坚持有趣的高质量内容，洪恩长期霸榜Apple Store](http://www.investorscn.com/2022/06/30/101584/)
> 概要: 目前，育儿领域成为了家长们越来越关注的方向之一。疫情期间，家长们对孩子的陪伴时间也同步增加，如何让居家的孩子能得到更为高效的时间利用与快乐成长，这也是每个家长一直在探索的话题。家长愿意借助更便捷的科技......
### [深度分析丨“奶茶币”与“虚拟股票”玩法背后的合规玄机](https://www.tuicool.com/articles/eqEriuE)
> 概要: 深度分析丨“奶茶币”与“虚拟股票”玩法背后的合规玄机
### [龙珠超85话：孙悟空觉醒，营救贝吉塔，升级版自在极意功亮相](https://new.qq.com/omn/20220630/20220630A0AI2H00.html)
> 概要: 本期聊聊日本动漫龙珠超漫画第85话的剧情，“幸存者格兰诺拉篇”的大决战正式开始，面对强大的反派佳斯，贝吉塔和孙悟空放手一搏，贝吉塔使用“破坏神形态”，孙悟空使用“自在极意功”，具体的战况如何呢？   ......
### [债券通这五年：从千亿外资向北流，到南北双向开放](https://www.yicai.com/news/101461146.html)
> 概要: 债券通已经成为境外投资者参与中国债券市场的主流渠道之一。
### [阿尔法犬X象棋神话](https://www.zcool.com.cn/work/ZNjA2NzkxMjA=.html)
> 概要: 阿尔法犬X象棋神话主题设定：xingceng0073D：xingceng0072D:  我不是寸头插画：我不是寸头排版：xingceng007文案：xingceng007设计初衷：希望以自己方式来传达......
### [《幸福到万家》幕后：婚闹打戏，竟给赵丽颖准备了7张板凳](https://new.qq.com/rain/a/20220630V05OGJ00)
> 概要: 《幸福到万家》幕后：婚闹打戏，竟给赵丽颖准备了7张板凳
### [花式促销、24城发布限跌令背后：城市人均住房面积已超36平](https://www.yicai.com/news/101461198.html)
> 概要: 当前中国城市人均住房面积已经超过了36平方米，中国楼市发展已经走过高峰期。
### [乘联会秘书长崔东树：2022 年 1-5 月中国占世界新能源车 59% 份额](https://www.ithome.com/0/627/266.htm)
> 概要: IT之家6 月 30 日消息，今日，乘联会秘书长崔东树发文表示，2022 年 1-5 月的广义新能源汽车（含普通混动）销量 461 万台，其中混合动力达到 139 万台，占比 30%。2022 年世界......
### [数字人民币也有存取款机？这一专利申请透露新动向](https://www.yicai.com/news/101461223.html)
> 概要: “一种数字货币存取款机”专利申请日前被公开。
### [下半年财政政策重点！落实落细税费政策，增加举债补收支缺口](https://www.yicai.com/news/101461231.html)
> 概要: 罗志恒认为，下一步增量财政政策的可能有三个选项，一是增发特别国债；二是上调赤字率；三是提前下发2023年专项债额度，在四季度使用。
### [深夜剧上新丨这才是有钱人玩的游戏，不到最后猜不到结局《心理游戏》](https://new.qq.com/rain/a/20220630V093U100)
> 概要: 深夜剧上新丨这才是有钱人玩的游戏，不到最后猜不到结局《心理游戏》
### [这是房地产迄今最大的信心](https://www.huxiu.com/article/595130.html)
> 概要: 作者｜周超臣头图｜虎嗅，拍摄于2022年6月30日傍晚这是一个剩者为王的时代，“活下去”是一种奢侈。今年上半年的房地产和去年下半年的房地产有个共同的特点：魔幻。去年的魔幻是著名和非著名房企的连环爆雷主......
### [《人生大事》票房破5亿 位列上半年票房第九位](https://ent.sina.com.cn/m/c/2022-06-30/doc-imizmscu9543473.shtml)
> 概要: 新浪娱乐讯 6月30日，由韩延监制、刘江江执导，朱一龙领衔主演的电影《人生大事》票房突破5亿大关，位列2022上半年票房TOP9。　　今年上半年票房前十影片分别为：　　1《长津湖之水门桥》40.65亿......
### [AYANEO AIR 掌机 Retro Power 配色开启预售：R5 5560U 版 4499 元](https://www.ithome.com/0/627/270.htm)
> 概要: IT之家6 月 30 日消息，日前，AYANEO AIR 系列掌机公布了 Retro Power 配色，今日官网开启预售，售价如下：AYANEO AIR ：R5 5560U + 16GB + 512G......
### [科创板股权投资全景图：头部梯队引领投资格局，产业资本深耕单一赛道](https://www.yicai.com/news/101461338.html)
> 概要: 科创板三年，股权投资生态改变。
### [盛景研究院院长彭志强：美国技术性衰退，中国经济会U型反弹](https://finance.sina.com.cn/china/gncj/2022-06-30/doc-imizirav1373288.shtml)
> 概要: 21专访丨盛景研究院院长彭志强：美国技术性衰退，中国经济会U型反弹 21世纪经济报道记者 张望 报道 对美国加息，市场一直在保持高度关注。
### [从蒙古国赴华所有人员不再隔离？中使馆紧急提醒：网传消息不实](https://finance.sina.com.cn/jjxw/2022-06-30/doc-imizmscu9549851.shtml)
> 概要: 中国驻蒙古使馆领事侨务处6月30日发布紧急提醒称，今日，在蒙脸书等媒体上转发的“2022年7月1日起，从蒙古赴华的所有人员不再隔离”的信息为假消息。
### [发力“稳外贸” 外贸企业获广东工行融资1070亿元](https://finance.sina.com.cn/jjxw/2022-06-30/doc-imizmscu9550186.shtml)
> 概要: 中新网广州6月30日电 （记者 唐贵江）东莞SQ公司是一家生产销售人工散热膜、人工散热片等电子产品配件的制造业企业，每月PI膜等原材料采购量达2500万元。
### [英国杂志评孙宇晨出席WTO MC12：区块链引领全球数字化转型](http://www.investorscn.com/2022/06/30/101587/)
> 概要: 近日，英国商业战略杂志Welp Magazine就格林纳达常驻世界贸易组织代表、特命全权大使孙宇晨出席WTO第12届部长级会议一事，发表了《孙宇晨代表格林纳达首登 WTO 舞台》的评论文章。文章指出，......
### [《中国统一战线》陈利浩：推动中国低碳发展的软件先锋](http://www.investorscn.com/2022/06/30/101588/)
> 概要: 陈利浩：推动中国低碳发展的软件先锋......
### [威胜信息上半年中标超千万项目同比翻番，6月中标3.9亿占2021年营收21.43%](http://www.investorscn.com/2022/06/30/101590/)
> 概要: 6月30日，威胜信息发布6月中标合同情况的自愿性披露公告。公告显示，公司中标国家电网、南方电网、各省网公司五个超千万元的项目，累计中标金额超3.9亿人民币，占公司2021年度经审计的营业总收入的21......
### [稳投资再加力，国常会部署发行金融债券等筹资3000亿](https://finance.sina.com.cn/roll/2022-06-30/doc-imizmscu9551221.shtml)
> 概要: 当前我国经济下行压力加大，投资对于经济发展的支撑作用更加凸显。 据央视新闻，国务院总理李克强29日主持召开国务院常务会议，确定政策性...
### [流言板马赛官方：2米中卫图雷500万转会费签约5年，此前差点去曼城](https://bbs.hupu.com/54517949.html)
> 概要: 虎扑06月30日讯 法甲球会马赛官方宣布，从勒阿弗尔竞技俱乐部签下法国U19国脚伊萨克-图雷。在体检成功后，这名19岁的后卫与俱乐部签订了一份截止到2027年6月30日的合同。继塞缪尔-吉戈特之后，图
### [流言板穆迪：我了解球队体系，夏季联赛我会努力为队友提供帮助](https://bbs.hupu.com/54518084.html)
> 概要: 虎扑06月30日讯 勇士球员摩西-穆迪在训练结束后接受了采访。谈到夏季联赛，穆迪说：“我们会在场上努力赢球。当我们得到机会时，我会告诉我的队友他们去哪里，我了解这个体系，我会努力帮助我的队友。”对于自
### [流言板连媒：谢晖的轮换计划完美实施，大连人已经掌握抢分主动权](https://bbs.hupu.com/54518141.html)
> 概要: 虎扑06月30日讯 中超联赛第7轮，大连人3-0击败广州城，前七轮战罢大连人2胜3平2负积9分，暂时排名积分榜第12位。大连媒体《半岛晨报》发文表示谢晖的轮换计划完美实施，大连人已经掌握了抢分主动权，
### [贝瑞基因三代SMA技术检出率超98%，此前相关药物为“天价救命药”](https://finance.sina.com.cn/chanjing/gsnews/2022-06-30/doc-imizirav1375869.shtml)
> 概要: 贝瑞基因宣布其三代SMA（脊髓性肌萎缩症）研究成果发表于分子诊断领域权威国际学术期刊 Journal of Molecular Diagnostics。
### [流言板鲁尼发文感谢德比郡：谢谢三年来的支持，最艰难的决定之一](https://bbs.hupu.com/54518305.html)
> 概要: 虎扑06月30日讯 鲁尼正式离开德比郡，并在个人社交媒体上发文感谢。鲁尼写道：“非常感谢德比郡的球迷、球员以及工作人员这三年来给予我的的支持，这是我职业生涯中做出的最艰难决定之一。谢谢你们把我当做团队
# 小说
### [我在废土打怪升级](http://book.zongheng.com/book/1104957.html)
> 作者：石竹卧青山

> 标签：科幻游戏

> 简介：废墟中崛起的少年，异兽横行的地球，尔虞我诈的文明交锋，谁能笑到最后？PS：本书又名《我的山海物语》！

> 章节末：完结感言

> 状态：完本
# 论文
### [An adaptive graph learning method for automated molecular interactions and properties predictions | Papers With Code](https://paperswithcode.com/paper/an-adaptive-graph-learning-method-for)
> 日期：Nature Machine Intelligence 2022

> 标签：None

> 代码：https://github.com/yvquanli/GLAM

> 描述：Improving drug discovery efficiency is a core and long-standing challenge in drug discovery. For this purpose, many graph learning methods have been developed to search potential drug candidates with fast speed and low cost. In fact, the pursuit of high prediction performance on a limited number of datasets has crystallized their architectures and hyperparameters, making them lose advantage in repurposing to new data generated in drug discovery. Here we propose a flexible method that can adapt to any dataset and make accurate predictions. The proposed method employs an adaptive pipeline to learn from a dataset and output a predictor. Without any manual intervention, the method achieves far better prediction performance on all tested datasets than traditional methods, which are based on hand-designed neural architectures and other fixed items. In addition, we found that the proposed method is more robust than traditional methods and can provide meaningful interpretability. Given the above, the proposed method can serve as a reliable method to predict molecular interactions and properties with high adaptability, performance, robustness and interpretability. This work takes a solid step forward to the purpose of aiding researchers to design better drugs with high efficiency.
### [Data-Efficient Structured Pruning via Submodular Optimization | Papers With Code](https://paperswithcode.com/paper/data-efficient-structured-pruning-via)
> 概要: Structured pruning is an effective approach for compressing large pre-trained neural networks without significantly affecting their performance, which involves removing redundant regular regions of weights. However, current structured pruning methods are highly empirical in nature, do not provide any theoretical guarantees, and often require fine-tuning, which makes them inapplicable in the limited-data regime. We propose a principled data-efficient structured pruning method based on submodular optimization. In particular, for a given layer, we select neurons/channels to prune and corresponding new weights for the next layer, that minimize the change in the next layer's input induced by pruning. We show that this selection problem is a weakly submodular maximization problem, thus it can be provably approximated using an efficient greedy algorithm. Our method is one of the few in the literature that uses only a limited-number of training data and no labels. Our experimental results demonstrate that our method outperforms popular baseline methods in various one-shot pruning settings.
