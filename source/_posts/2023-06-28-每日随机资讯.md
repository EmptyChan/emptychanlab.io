---
title: 2023-06-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ItalyCinqueTerre_ZH-CN6495965228_1920x1080.webp&qlt=50
date: 2023-06-28 22:50:06
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ItalyCinqueTerre_ZH-CN6495965228_1920x1080.webp&qlt=50)
# 新闻
### [Herniated Disk: What It Is, Symptoms & Treatment](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2177934578503&cate_id=-1)
> 概要: 椎间盘突出的疼痛和不适会严重影响您的生活质量......
### [Fasting Diets vs. Cutting Calories: Which Works Best?](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx9722514395976&cate_id=-1)
> 概要: 一项新的临床试验表明，一种时髦的间歇性禁食方式似乎确实能帮助人们减轻一些体重ーー尽管它可能不比老式的卡路里计数方式好多少......
### [Deaf mice have nearly normal inner ear function until ear canal opens](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2922859746130&cate_id=-1)
> 概要: 总结：根据一项新的研究，患有遗传性耳聋的小鼠在出生后的前两周内，听觉系统的神经活动几乎正常。先前的研究表明，这种早期的听觉活动--在听力开始之前--提供了一种训练，使大脑在听力开始时准备好处理声音......
### [Human embryo-like models created from stem cells to understand earliest stages of human development](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx8303161922385&cate_id=-1)
> 概要: 摘要: 科学家通过对人类干细胞进行重编程，在实验室建立了一个人类胚胎干细胞衍生模型。这一突破性进展可能有助于研究遗传性疾病以及了解怀孕失败的原因和方式......
### [【STTT】北大季加孚教授团队发现调节PD-L1新机制，开发胃癌治疗新方法](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx5369749590857&cate_id=-1)
> 概要: <strong>导读	</strong>免疫检查点阻断（ICB）为治疗胃癌（G.C.）提供了新的机会。了解免疫检查点的上游调控对于进一步提高ICB治疗的疗效至关重要......
### [3980的旅游卡成本不到1块钱，它是怎么割韭菜的？](https://www.woshipm.com/marketing/5856098.html)
> 概要: 在几年前，我们见过旅游卡这类项目，但如今它又重新在线上线下火了起来。本文从旅游卡这种项目的起源出发，详细分析了旅游卡的发展历史、运营模式等方面内容，希望对你了解旅游卡有所帮助。有个四五年前的项目，最近......
### [抖音本地生活难打“地面战”](https://www.woshipm.com/it/5855932.html)
> 概要: 前段时间有消息称，抖音外卖放弃今年达成 1000 亿元 GMV（交易总额）的目标，而在这则消息背后，我们看到，抖音的外卖业务仍有待进一步跑通，想做好外卖业务的抖音，还有许多问题和挑战仍待解决。一起来看......
### [网约车市场，饱和了？](https://www.woshipm.com/it/5855851.html)
> 概要: 最近一段时间，随着市场的变化，我们常能听到关于网约车市场“饱和”的相关讨论。那么在挑战之下，网约车平台要如何应对，或者如何调优自身的经营策略？本篇文章里，作者就针对网约车市场、网约车平台的发展做了解读......
### [融云出海：TikTok 百亿美元投向东南亚，巨头将如何影响市场格局](https://segmentfault.com/a/1190000043944219)
> 概要: 👆三折抢购《出海作战地图》，7 月 15 日恢复原价比白皮书更精炼省流，比图谱更实用有效。关注【融云全球互联网通信云】了解更多融云《社交泛娱乐出海作战地图》，在海量报告研读和案头工作基础上，融合了 2......
### [即时通讯技术文集（第18期）：IM架构设计基础知识合集 共16篇](https://segmentfault.com/a/1190000043944543)
> 概要: 为了更好地分类阅读 52im.net 总计1000多篇精编文章，我将在每周三推送新的一期技术文集，本次是第18 期。- 1 - IM系统的MQ消息中间件选型：Kafka还是RabbitMQ？链接htt......
### [已开源！网易云信的热点探测平台实践](https://segmentfault.com/a/1190000043944752)
> 概要: 背景对于一个互联网平台，特别是 toB 的 PaaS/SaaS 平台，热点 key 是一个绕不过去的问题。作为一个开放的系统，平台每天要承载来自大量的外部系统或者海量终端的请求，虽然所有的请求都需要满......
### [终于肝完了！全网最全、最详细、最全面的 Hadoop大数据学习教程（ 2023最新版 ）](https://segmentfault.com/a/1190000043945295)
> 概要: 大家好，我是民工哥！前面给大家介绍了：关系型数据库MySQL、 NoSQL 数据库Redis、MongoDB、搜索引擎ElasticSearch等知识体系学习的文章。在当今这样的就业大背景下，卷是肯定......
### [DC新片《蓝甲虫》发布新海报 拉丁青少年变超英](https://ent.sina.com.cn/m/f/2023-06-28/doc-imyyuvxy9753704.shtml)
> 概要: 新浪娱乐讯 北京时间6月28日消息，据外国媒体报道，DC新片《蓝甲虫》发布新海报，全员亮相。　　索洛·马里多纳（《眼镜蛇》）饰“蓝甲虫”海梅·雷耶斯，布鲁娜·马尔科辛（《海伦娜的影子》）、苏珊·萨兰登......
### [邂逅＃粉嫩多巴胺少女＃少女大秘境](https://www.zcool.com.cn/work/ZNjU3MDQ0MzY=.html)
> 概要: 这幅画有点珠光宝气,其实是少女坐在梳妆台前的幻想.这是她内心的秘境,属于自己的秘密花园.这次与小王子联动,让我发现与粉嫩多巴胺少女有很多相似之处.小王子的旅行,遇到了各种各样的人,自我觉醒学会了爱,少......
### [组图：马丽晒照庆41岁生日 穿蓝色露肩礼服风韵十足尽显成熟魅力](http://slide.ent.sina.com.cn/star/slide_4_704_386747.html)
> 概要: 组图：马丽晒照庆41岁生日 穿蓝色露肩礼服风韵十足尽显成熟魅力
### [漫改电影《OUT》公开第1弹PV！定档11月17日](https://news.dmzj.com/article/78490.html)
> 概要: 电影《OUT》公开最新海报和第1弹PV，本作将于11月17日上映。
### [TV动画《祭品公主与兽之王》最新后半季PV第4弹公开](https://news.dmzj.com/article/78491.html)
> 概要: TV动画《祭品公主与兽之王》最新后半季PV第4弹公开。
### [海外New Things |「Agile Space Industries」完成1300万美元种子轮融资，研发新型航天推进器](https://36kr.com/p/2319725774373251)
> 概要: 海外New Things |「Agile Space Industries」完成1300万美元种子轮融资，研发新型航天推进器-36氪
### [早期项目 | 「晶栅科技」完成Pre-A轮融资，支持半导体行业数智化转型](https://36kr.com/p/2319757003997831)
> 概要: 早期项目 | 「晶栅科技」完成Pre-A轮融资，支持半导体行业数智化转型-36氪
### [海外new things | 法国风投公司「Frst」旗下基金募资有望达1亿欧元，届时资产总额将突破2亿欧元](https://36kr.com/p/2319683313172865)
> 概要: 海外new things | 法国风投公司「Frst」旗下基金募资有望达1亿欧元，届时资产总额将突破2亿欧元-36氪
### [《飞翔吧！琦玉》续篇制作决定！定档11月23日](https://news.dmzj.com/article/78492.html)
> 概要: 《飞翔吧！琦玉》续篇制作决定！定档11月23日。
### [科幻喜剧动画《飞出个未来》PV公开！定档7月24日](https://news.dmzj.com/article/78494.html)
> 概要: 科幻喜剧动画《飞出个未来》PV公开！定档7月24日。
### [《无畏契约》第七幕第一章开幕 新模式新英雄上线](https://www.3dmgame.com/news/202306/3872234.html)
> 概要: 《无畏契约》国际服发布了第七幕第一章的开幕预告，新章节目前已正式上线，引入了一位新英雄“蒂罗”（Deadlock），定位为守卫；新5v5模式“团队死斗”，三张全新地图，玩家每隔1.5秒即可重生；进度系......
### [反光品牌 | 壹杯随时椰 · 满满南洋味](https://www.zcool.com.cn/work/ZNjU3MDc0NDA=.html)
> 概要: 项目名称 | 春光随时椰·南洋椰茶项目地点 | 海南·文昌设计时间 | 2022年10月开业时间 | 2023年03月设计服务 | 策划 / 品牌 / 空间设计机构 | 成都肯默文化创意服务有限公司 ......
### [陈建州曾节目亲吻Selina 向SHE及公司致歉才平息](https://ent.sina.com.cn/s/h/2023-06-28/doc-imyyvhpt2844728.shtml)
> 概要: 新浪娱乐讯 近日，有网友翻出22年前陈建州在综艺节目中亲吻Selina，让Selina当捂脸惨叫，崩溃表示：“为什么我觉得有舌头的感觉”。据悉，节目播出后，该片段遭到S.H.E所属唱片公司的严正抗议，......
### [战绩彪炳，紫光展锐新帅领军芯片设计双龙头](http://www.investorscn.com/2023/06/28/108518/)
> 概要: 7日早间，紫光展锐官方公众号发布公告称，为进一步助力紫光展锐的高质量发展，加强公司战略规划与管理，紫光集团委派集团执行副总裁马道杰任紫光展锐董事并选派其为紫光展锐董事长......
### [“科技+服务”双向赋能，大家保险全面提升客户服务价值](http://www.investorscn.com/2023/06/28/108519/)
> 概要: 近日，大家保险集团发布了2022企业社会责任报告（简称《报告》）。《报告》全面展示了2022年大家保险在金融服务、民生福祉、绿色发展等领域的实践，以及在数字化转型、深化客户服务等方向的创新与成果......
### [安徽省歙县市场监管局多维优化服务助企纾困解难](https://finance.sina.com.cn/jjxw/2023-06-28/doc-imyyvtct0844698.shtml)
> 概要: 中国质量新闻网讯 为助推经济社会高质量发展，推深做实“一改两为”，解民忧纾企困，安徽省歙县市场监督管理局积极探索“1+2+3”的工作模式，以点带线，以线扩面，多维发力...
### [广东特检院东莞检测院探索“三个机制”构建特种设备检验质量监督新模式](https://finance.sina.com.cn/jjxw/2023-06-28/doc-imyyvtcq9442774.shtml)
> 概要: 中国质量新闻网讯 为进一步提高特种设备质量安全水平，防范特种设备质量安全风险，广东省特种设备检测研究院东莞检测院坚持完善检验质量监督机制，通过“质把关、复验证...
### [微软用AI缩短癌症放疗时间 扫描速度提高2.5倍](https://www.3dmgame.com/news/202306/3872252.html)
> 概要: IT之家今日（6月28日）消息，据BBC报道，英国正计划将一种新型人工智能技术以成本价提供给所有NHS（英国国家医疗服务体系）信托机构，该技术可帮助医生更快计算放疗辐射束的投放位置，从而有效减少患者接......
### [【洛城里】保留核心阵容，培养成熟新秀，掘金独特的可持续冠军战略](https://bbs.hupu.com/60955241.html)
> 概要: 约基奇今年28岁，贾马尔·穆雷今年26岁，小波特即将年满25岁。戈登在下赛季训练营开始时将年满28岁。由于年轻球员组成了丹佛的核心阵容，丹佛掘金有机会打造一个长期的冠军窗口。艰难的部分已经完成，他们在
### [下班加油！国内油价再上调，加满一箱多花2.5元](https://finance.sina.com.cn/china/2023-06-28/doc-imyyvtcp2723885.shtml)
> 概要: 中新经纬6月28日电 油价年内第五涨！据国家发改委网站消息，6月28日24时起国内成品油价格按机制上调。 国家发改委表示，根据近期国际市场油价变化情况...
### [恰到好处的凉：安睡宝 3D cool 凉席套装 79 元发车](https://lapin.ithome.com/html/digi/702498.htm)
> 概要: 天猫【安睡宝旗舰店】安睡宝 3D cool 凉席套装（凉席 + 枕垫 2 只）日常售价 149 元起，今日可领 222 元大额券，实付 79 元起近期新低。0.9m 单人床款券后此价，1.5m 款券后......
### [中国移动元宇宙产业联盟成立，首批成员含华为、科大讯飞、小米等](https://www.ithome.com/0/702/504.htm)
> 概要: IT之家6 月 28 日消息，今日，中国移动元宇宙产业联盟正式成立，首批共 24 家成员，包括中国移动咪咕、科大讯飞、华为、小米、芒果 TV 等，涵盖内容制作、XR 终端、 关键技术、算力网络等领域......
### [这次是正版了 育碧为《极限国度》添加新曲For Ya](https://www.3dmgame.com/news/202306/3872264.html)
> 概要: 育碧宣布《极限国度》第七赛季更新将添加新曲《For Ya》，目前已正式上线，玩家更新游戏后，在山脊电台就可以找到这首歌。《For Ya》与一个玩家整活的梗“正版极限国度”有关，一般玩家会将其他游戏的滑......
### [中国台湾玩家来贴吧求助：请支援《星空》繁中请愿](https://www.3dmgame.com/news/202306/3872266.html)
> 概要: 之前一位名叫“Hango Shu”的玩家在change.org平台发起请愿，请求B社为《星空》添加繁体中文支持。让中国台湾和其他使用繁中地区的玩家能更好体验游戏。因为请愿签字人数不太多，一位中国台湾玩......
### [端午档9亿票房列影史第二，光峰科技影院业务持续向好](http://www.investorscn.com/2023/06/28/108528/)
> 概要: 据灯塔专业版数据显示,刚刚结束的2023年端午档(6月22日至24日)电影票房为9.09亿元,位列影史端午档票房第二的成绩(2018年端午档9.12亿),观影总人次2225万,总场次128.2万。其中......
### [险企布局养老赛道，爱心人寿实现跨越里程碑的一步](http://www.investorscn.com/2023/06/28/108531/)
> 概要: 人口老龄化已经成为我国重要的社会问题。据国家卫健委测算，目前我国已经进入中度老龄化阶段，预计2035年左右，60岁及以上老年人口将突破4亿，进入重度老龄化阶段，养老问题成为全社会关注的焦点。针对这一问......
### [叮当健康HealthGPT首推叮当药师、营养师AI助手](http://www.investorscn.com/2023/06/28/108532/)
> 概要: 6月28日,叮当健康(09886·HK)正式启动发布叮当HealthGPT,并首推基于此研发的应用型医药AI产品——叮当药师、营养师AI助手。叮当快药科技集团总经理杨益斌表示,“国内外大模型百花齐放,......
### [涉仁东控股、金力泰等股闪崩、操纵，配资庄家李跃宗明日受审](https://www.yicai.com/news/101793519.html)
> 概要: 李跃宗及其合伙人邱黎斌涉操纵证券、期货市场案即将开庭。李跃宗在2020年末仁东控股等庄股暴跌后被上海警方控制。
### [住建部部长发表署名文章，指出住房城乡建设形势“发生深刻变化”](https://finance.sina.com.cn/china/2023-06-28/doc-imyywctk9308511.shtml)
> 概要: 由中央党校主管主办的《学习时报》开辟专栏，刊发数位二十届中央委员的署名文章，结合各自领域的工作，谈深入开展学习贯彻习近平新时代中国特色社会主义思想主题教育的体会...
### [含铂量不低于99.9%，铂金条投资迎来首个国标](https://www.yicai.com/news/101793650.html)
> 概要: 统一规范的标准将助于铂金投资市场发展
### [ESG强制披露对企业影响多大？ 亚洲领军企业分享经验](https://www.yicai.com/news/101793693.html)
> 概要: “企业执行高要求的可持续发展标准，不只是花些成本完成报告这么简单。”
### [36氪首发 | 消费科技品牌Nothing获9600万美元新一轮融资，全球销量突破150万台](https://36kr.com/p/2319726497153673)
> 概要: 36氪首发 | 消费科技品牌Nothing获9600万美元新一轮融资，全球销量突破150万台-36氪
### [全球正在进入可持续投资新时代，这三大事件值得关注｜直击夏季达沃斯](https://finance.sina.com.cn/china/2023-06-28/doc-imyywcti2541269.shtml)
> 概要: 原标题：全球正在进入可持续投资新时代，这三大事件值得关注|直击夏季达沃斯 “这三大重大发展同时发生，使2023年成为全球投资政策和实践的新纪元。
### [财政部会计财务评价中心声明：未与任何培训机构以任何形式组织相关考试培训辅导活动](https://www.yicai.com/news/101793713.html)
> 概要: 财政部会计财务评价中心未与任何培训机构以任何形式组织相关考试培训辅导活动。
### [长安加速新能源车攻势，Alpine计划2030年营收80亿欧元](https://www.yicai.com/news/101793764.html)
> 概要: 深蓝、启源、阿维塔多线并进。
### [Xbox未达季度收入预期 相差8亿美元](https://www.3dmgame.com/news/202306/3872273.html)
> 概要: 尽管微软已经从上一代游戏主机中恢复过来，但最近的财务信息并没有达到公司的预期。据TweakTown报道，Xbox未能达到季度收入预期，相差将近8亿美元。在Xbox Series S|X发售后，微软的表......
### [【直播】dys：乌兹神！要五杀了！喜欢亮G2！谁抢的五杀](https://bbs.hupu.com/60960032.html)
> 概要: 【直播】dys：乌兹神！要五杀了！喜欢亮G2！谁抢的五杀
### [微星发布两款 RTX 4060 显卡：GAMING 与 VENTUS](https://www.ithome.com/0/702/555.htm)
> 概要: IT之家6 月 28 日消息，微星今日发布 RTX 4060 GAMING 和 VENTUS 2X BLACK 显卡，两款新品将在 2023 年 6 月 29 日正式开售。微星表示，微星 RTX 40......
### [赛后采访北京WB.梓墨：我只能说炽阳神光是最猛的](https://bbs.hupu.com/60960301.html)
> 概要: 来源：  虎扑
### [限时补券：特仑苏纯牛奶 2.7 元/盒，蒙牛纯牛奶 2 元/盒](https://lapin.ithome.com/html/digi/702558.htm)
> 概要: 天猫【特仑苏旗舰店】特仑苏 纯牛奶 250ml*16 包日常售价为 89 元，今日下单 2 件可用 89 元优惠券，2 件实付 89 元。到手 2 箱 32 盒，折合 2.78 元 / 盒、11.1 ......
### [流言板安东尼：我不是施暴者，调查结束后我的清白将得到证明](https://bbs.hupu.com/60960353.html)
> 概要: 虎扑06月28日讯 曼联边锋安东尼在Ins发声表示自己不是施暴者，在此前一段时间，安东尼前女友曾控诉他家暴，而这也是他在家暴风波后首次发声回应。安东尼写到：“致我的朋友、球迷和关注者，在我在警察局提交
### [U19世界杯速递追到三分！王俊杰外线续上火力](https://bbs.hupu.com/60960456.html)
> 概要: U19世界杯速递追到三分！王俊杰外线续上火力
# 小说
### [我们的花季时代](https://book.zongheng.com/book/1071289.html)
> 作者：不花辣子

> 标签：都市娱乐

> 简介：学霸不算什么，玩出精彩才叫人生！回想年少的轻狂，颇幼稚、颇搞笑、颇怀念！谨以此文，献给各奔东西的小伙伴们！

> 章节末：第一百一十四章  还有完吗？

> 状态：完本
# 论文
### [CrossRE: A Cross-Domain Dataset for Relation Extraction | Papers With Code](https://paperswithcode.com/paper/crossre-a-cross-domain-dataset-for-relation)
> 日期：17 Oct 2022

> 标签：None

> 代码：https://github.com/mainlp/crossre

> 描述：Relation Extraction (RE) has attracted increasing attention, but current RE evaluation is limited to in-domain evaluation setups. Little is known on how well a RE system fares in challenging, but realistic out-of-distribution evaluation setups. To address this gap, we propose CrossRE, a new, freely-available cross-domain benchmark for RE, which comprises six distinct text domains and includes multi-label annotations. An additional innovation is that we release meta-data collected during annotation, to include explanations and flags of difficult instances. We provide an empirical evaluation with a state-of-the-art model for relation classification. As the meta-data enables us to shed new light on the state-of-the-art model, we provide a comprehensive analysis on the impact of difficult cases and find correlations between model and human annotations. Overall, our empirical investigation highlights the difficulty of cross-domain RE. We release our dataset, to spur more research in this direction.
### [A parametric approach to the estimation of convex risk functionals based on Wasserstein distance | Papers With Code](https://paperswithcode.com/paper/a-parametric-approach-to-the-estimation-of)
> 日期：25 Oct 2022

> 标签：None

> 代码：https://github.com/sgarale/wasserstein_parametrization

> 描述：In this paper, we explore a static setting for the assessment of risk in the context of mathematical finance and actuarial science that takes into account model uncertainty in the distribution of a possibly infinite-dimensional risk factor. We allow for perturbations around a baseline model, measured via Wasserstein distance, and we investigate to which extent this form of probabilistic imprecision can be parametrized. The aim is to come up with a convex risk functional that incorporates a sefety margin with respect to nonparametric uncertainty and still can be approximated through parametrized models. The particular form of the parametrization allows us to develop a numerical method, based on neural networks, which gives both the value of the risk functional and the optimal perturbation of the reference measure. Moreover, we study the problem under additional constraints on the perturbations, namely, a mean and a martingale constraint. We show that, in both cases, under suitable conditions on the loss function, it is still possible to estimate the risk functional by passing to a parametric family of perturbed models, which again allows for a numerical approximation via neural networks.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
