---
title: 2022-10-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FlamingoTeacher_ZH-CN5688509752_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-05 22:42:34
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FlamingoTeacher_ZH-CN5688509752_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [打工人创业不是从员工到老板的转变，而是从职场到商场的转变](https://www.woshipm.com/chuangye/5632795.html)
> 概要: 当一个打工人开始创业的时候，很容易犯的一个错就是避免自己作为员工时上一任老板对自己做的事情，这样的行为其实是很不好的。打工人创业，并不只是身份的转变，而是从职场到商场的转变，如何把握好这个转变呢？一起......
### [虚拟数字人如何赋能品牌营销？](https://www.woshipm.com/ai/5632866.html)
> 概要: 目前虚拟数字人在元宇宙虚拟世界的应用场景中，主要包括电商直播、虚拟偶像以及品牌营销。那么，如何用虚拟数字人来赋能品牌营销，抓住消费主流Z时代呢？一起来看一下吧。2021年10月31日，一个名为“柳夜熙......
### [双十一，正成就一个「新电商」](https://www.woshipm.com/it/5632776.html)
> 概要: 直播带货这样一种新奇的方式，让电商重新焕发了生机与活力，然而经过几年的发展，直播带货在获取流量，实现转化的问题上似乎也遇到了问题。在即将到来的双十一，直播带货又会给电商带来什么转变呢？当李佳琦回归，很......
### [从70万元砍价到3.3万！凭一句话令无数人动容的她，首次吐露心路历程](https://finance.sina.com.cn/china/gncj/2022-10-05/doc-imqmmtha9813309.shtml)
> 概要: “你们是不是已经尽到最大的努力了？” 去年年底，医保目录药品谈判现场 国家医保局谈判代表张劲妮 “灵魂砍价”，连砍八轮 给人留下深刻印象 最终...
### [推特大涨](https://finance.sina.com.cn/tech/internet/2022-10-05/doc-imqmmtha9813404.shtml)
> 概要: 据美国多家媒体报道，当地时间10月4日，在特斯拉首席执行官马斯克计划以此前协议原价，即每股54.20美元的价格收购推特公司后，推特当天股价飙升15%，并在飙升后停牌。因马斯克此前试图终止收购协议，双方......
### [欧盟将统一便携智能设备充电接口](https://finance.sina.com.cn/tech/tele/2022-10-05/doc-imqmmtha9812984.shtml)
> 概要: 欧洲议会10月4日通过一项新规，要求从2024年底开始，所有手机、平板电脑等便携智能设备新机都使用USB Type-C的充电接口......
### [总结本周遇到的问题](https://segmentfault.com/a/1190000042573964)
> 概要: 问题一报错如下：搜索后发现此类错误和数据库的字符集有关系之所以之前没发现此类问题是因为之前测试时只测试了保存英文字符，然而当我们存储中文字符时就会出现此类错误。很显然我们建库时并没有注意到这些，并且在......
### [前端实现切换主题方案](https://segmentfault.com/a/1190000042573976)
> 概要: 前端开发人员面临着制作增强用户体验和适应用户偏好的用户界面的任务。带有css的react主要可用于创建多色可切换主题。为用户提供了在给定时间点在主题颜色之间切换以适合他们的偏好的特权。介绍在本文中，我......
### [自言字语 —— 一组手写毛笔字](https://www.zcool.com.cn/work/ZNjIyMDY5NDg=.html)
> 概要: 自言字语 —— 揮墨陶情......
### [经济周期和你有什么关系？](https://www.huxiu.com/article/678342.html)
> 概要: 来源｜秦朔朋友圈（ID：qspyq2015）作者｜李纲头图｜电影《华尔街之狼》笔者从事个人投资（“投机”）近20年，投资品种涉及债券、基金、股票、期货、外汇、加密货币等。工作上，在一级市场担任私募股权......
### [上海昨日新增本土无症状感染者6例，一区域划为疫情中风险区](https://www.yicai.com/news/101553960.html)
> 概要: 上海昨日无新增本土新冠肺炎确诊病例，新增本土无症状感染者6例。
### [中创新航：预计将于10月6日登陆港交所主板IPO上市](https://finance.sina.com.cn/tech/it/2022-10-05/doc-imqqsmrp1529693.shtml)
> 概要: 中创新航10月4日公布配发结果，公司全球发售约2.65亿股H股，其中香港发售占1%，国际发售占99%，目前超额配股权尚未获行使。每股发售价为38港元，每手100股，预期H股将于10月6日(星期四)上午......
### [书法字体设计｜第212回](https://www.zcool.com.cn/work/ZNjIyMDcyODA=.html)
> 概要: Collect......
### [动辄近万的护眼屏，能让天天上网课的孩子保持好视力吗？](https://finance.sina.com.cn/tech/it/2022-10-05/doc-imqqsmrp1536623.shtml)
> 概要: 整个暑假，肖云（化名）都在犹豫要不要购入一台护眼屏供孩子上网课。“社交平台铺天盖地都是种草文：孩子上网课更护眼，连接平板手机很方便，高清不伤眼……真把我们宝妈的心理拿捏地死死的。”最后，肖云花了上千元......
### [欧盟批准强制苹果使用USB-C接口 2024年底开始](https://www.3dmgame.com/news/202210/3853075.html)
> 概要: 欧洲议会于4日通过新规，要求从2024年底开始，所有手机和平板电脑等便携智能设备新机都要使用统一的USB Type-C充电接口。欧洲议会当天以602票赞成、13票反对的投票结果通过有关统一便携智能设备......
### [国家卫健委：昨日新增本土223+747](https://www.yicai.com/news/101553997.html)
> 概要: 昨日新增本土确诊病例223例，其中内蒙古64例、广东34例、四川32例。
### [外汇局：我国对外金融资产继续处于较高水平](https://finance.sina.com.cn/china/gncj/2022-10-05/doc-imqmmtha9825010.shtml)
> 概要: 新华社北京10月5日电（记者刘开雄）国家外汇管理局日前发布的《2022年上半年中国国际收支报告》显示，2022年6月末，我国对外金融资产91563亿美元，对外负债70746亿美元...
### [燃油附加费本月不调整，航企遭遇油汇“双击”](https://www.yicai.com/news/101554037.html)
> 概要: 新一周期内的国内航空煤油综合采购成本有望大幅下降，11月5日开始的国内机票燃油附加费有望下调。
### [手写字体设计（第78回）](https://www.zcool.com.cn/work/ZNjIyMDc5MjQ=.html)
> 概要: 一组手写字形表现......
### [sofa模块化隔离](https://segmentfault.com/a/1190000042574759)
> 概要: 传统项目传统项目中有时会Klass service层直接调用Student service层的Bean去实现某些逻辑，当项目变得越来越大时，这种层与层之间的依赖关系变得十分复杂，不利于开发。多个团队同......
### [贰婶手写--奇妙的中国汉字【拙字集拾`伍】](https://www.zcool.com.cn/work/ZNjIyMDgzNTI=.html)
> 概要: 贰婶手写--奇妙的中国汉字【拙字集拾`伍】......
### [Supercell宣布结束社交建造手游《山谷物语》开发](https://www.3dmgame.com/news/202210/3853086.html)
> 概要: 芬兰工作室Supercell 在其旗下手游《山谷物语（Everdale）》推出仅一年多后就决定了结束其后续开发工作。开发商在公告中写道：“我们怀着非常沉重的心情通知你们，团队已经决定结束《山谷物语》的......
### [国庆假期过半 北京消费市场稳中有升](https://finance.sina.com.cn/china/gncj/2022-10-05/doc-imqqsmrp1546009.shtml)
> 概要: 国庆假期前四日，北京市重点监测的百货、超市、专业专卖店、餐饮和电商等业态百家企业实现销售额49.2亿元，同比增长2.2%；其中，10月4日当天实现销售额10.7亿元...
### [美国劳工委员会称动视对QA工会进行了报复](https://www.3dmgame.com/news/202210/3853089.html)
> 概要: 据《华盛顿邮报》报道，美国国家劳资关系委员会认为，代表Raven Software QA 测试人员的工会对动视暴雪提起的投诉至少有一部分是有道理的。董事会已经确定，动视暴雪 4 月份决定提高除Rave......
### [进博会倒计时30天，“首发”“首展”通关高峰即将来临](https://www.yicai.com/news/101554053.html)
> 概要: 全球展品正在陆续进境
### [美股迎强劲四季度开局，真要熊转牛了？](https://www.yicai.com/news/101554087.html)
> 概要: 投资者对美联储政策拐点的预期能否成为现实？
### [《守望先锋：归来》IGN 8分：随时准备重新大放异彩](https://www.3dmgame.com/news/202210/3853101.html)
> 概要: 《守望先锋：归来》今日正式上线，IGN给出8.0分评价。在评测中，IGN编辑表示：《守望先锋》曾经是多人射击游戏领域的一把尖刀，而暴雪注意力的分散眼看就要让它变成一把钝刀，在这时《守望先锋：归来》为其......
### [联电设备学院开幕：计划一年内培训 600 位设备工程师](https://www.ithome.com/0/645/046.htm)
> 概要: IT之家10 月 5 日消息，据台湾地区联合新闻网报道，联华电子近期宣布，设置在南科 12A 厂 P5 厂区的半导体设备学院开幕，预计在一年内完成 600 位设备工程师培训，并自 2023 年起推广至......
### [脸僵腿瘸？经纪人回应萧亚轩时装周状态](https://ent.sina.com.cn/y/ygangtai/2022-10-05/doc-imqmmtha9850254.shtml)
> 概要: 新浪娱乐讯 近日萧亚轩拄拐现身巴黎时装周，面部状态也引争议。经纪人对此回应：“她一个女明星从罗浮宫走到会场，穿着薄的上衣，下半身又露大腿，在十几度的低温之下走那几百公尺的距离，当然会冷，脸会僵啊！”而......
### [苹果iOS 16.1 Beta 4发布 iPhone 14pro灵动岛变色](https://www.3dmgame.com/news/202210/3853107.html)
> 概要: 今天天凌晨，苹果向iPhone用户推送了iOS 16.1开发者预览版Beta 4更新（内部版本号：20B5064c），距离上次发布间隔7天。这次新版本肉眼可见的最大变化，就是灵动岛“变色”了。在最新系......
### [讯景 AMD RX 6700 XL “矿卡”曝光：2304 流处理器，10GB 显存](https://www.ithome.com/0/645/058.htm)
> 概要: 感谢IT之家网友OC_Formula、华南吴彦祖的线索投递！IT之家10 月 5 日消息，据 VideoCardz 消息，讯景的 RX 6700 XL 显卡曝光了，2304 流处理器，10GB 显存......
### [别尬黑，张译不是“文版战狼”](https://www.huxiu.com/article/678268.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜《万里归途》海报《万里归途》一骑绝尘，正以“逆跌”之势替国庆档挽尊。自 9 月 29 日点映上线至今，《万里归途》已蝉联五天票房日冠、累计票房突破 8 亿，再加上......
### [安森美推出 3 款碳化硅车载充电器的汽车功率模块](https://www.ithome.com/0/645/064.htm)
> 概要: 10 月 5 日消息，安森美 (onsemi) 近日宣布推出三款基于碳化硅 (以下简称”SiC”) 的功率模块，采用压铸模技术，用于所有类型电动汽车 (以下简称“xEV”) 的车载充电和高压 (以下简......
### [美锦能源旗下飞驰科技借助氢风，氢能重卡订单数不断创新高](http://www.investorscn.com/2022/10/05/103346/)
> 概要: 在山西能源的鼎盛发展阶段，以美锦能源为代表的传统能源企业遇到了不可多得的“黄金十年”，但在几年前，意识到煤炭黄金时代即将结束的美锦能源，早就先其他同业一步开始寻求煤炭能源之外的发展可能性......
### [韩国艺术家、漫画家金政基因病去世 年仅47岁](https://acg.gamersky.com/news/202210/1524486.shtml)
> 概要: 官方宣布韩国艺术家、漫画家金政基因突发疾病于10月3日去世，享年47岁。
### [《海贼王》1062话情报：科学之国大冒险 新兵器登场](https://acg.gamersky.com/news/202210/1524492.shtml)
> 概要: 《海贼王》1062话情报公开，路飞他们全员登上新岛，开始冒险。波妮与熊的关系终于揭晓，波妮说贝加庞克把她的父亲(熊)做成了兵器。
### [组图：马思纯分享外出游玩照 伸手比耶嘟嘴瞪眼搞怪自拍](http://slide.ent.sina.com.cn/star/slide_4_86512_376075.html)
> 概要: 组图：马思纯分享外出游玩照 伸手比耶嘟嘴瞪眼搞怪自拍
### [点击化学，凭什么拿下诺奖？](https://www.huxiu.com/article/678638.html)
> 概要: 本文来自微信公众号：返朴 （ID：fanpu2019），作者：返朴，原文标题：《生物正交化学和点击化学摘得2022化学诺奖，Sharpless二度获奖！Bertozzi如何创立生物正交化学？》，题图来......
### [影像修复师自述：中国家庭的秘密，都藏在老照片里](https://www.huxiu.com/article/678214.html)
> 概要: 本文来自微信公众号：十点人物志 （ID：sdrenwu），作者：刘小云，编辑：灯灯，头图来自：视觉中国一年前，26岁的小胡误打误撞成为了一名影像修复师，专门替人修复老照片和录像带。这一年里，他修过的照......
### [【IT之家开箱】七彩虹 iGame RTX 4090 Vulcan OC 显卡图赏，金属外骨骼 + 后赛博风格](https://www.ithome.com/0/645/083.htm)
> 概要: 9 月 20 日，英伟达正式发布了新一代 RTX 4090 显卡，IT之家拿到了多张首批非公型号的 RTX 4090 显卡，其中就包括七彩虹 iGame GeForce RTX 4090 Vulcan......
### [组图：王心凌分享与好友合照 穿无袖连体衣搭短靴活力满满](http://slide.ent.sina.com.cn/star/slide_4_86512_376076.html)
> 概要: 组图：王心凌分享与好友合照 穿无袖连体衣搭短靴活力满满
### [流言板梅州主帅：其实球队有机会，但运气站在了泰山队那边](https://bbs.hupu.com/55784901.html)
> 概要: 虎扑10月05日讯 中超第21轮比赛，梅州客家1-2不敌山东泰山。赛后，梅州客家主帅米兰-里斯蒂奇认为运气站在了对手那一边。对比赛进行点评怎么说呢，首先非常遗憾，我们比赛输了，我们虽然输球了，但是输给
### [流言板高天意：很开心打入在国安的首球；赛前把防守细节练得很好](https://bbs.hupu.com/55784910.html)
> 概要: 虎扑10月05日讯 中超联赛第21轮，上海海港0-1北京国安，赛后北京国安球员高天意接受媒体采访。打入国安生涯处子球的心情很开心能打入在国安的首球，希望球队取得更多胜利，个人拿到更多进球和助攻。赛前，
### [流言板王燊超：打出了教练要求的东西，但是把握机会没把握好](https://bbs.hupu.com/55784938.html)
> 概要: 虎扑10月05日讯 中超联赛第21轮，上海海港0-1北京国安，赛后上海海港球员王燊超接受媒体采访。武磊缺席是否给球队带来影响不仅是武磊吧，还有徐新也伤了。队内伤病很多，训练时候人员也不齐整。我们比赛里
### [流言板理查德：不在意每一场要进多少个球，取得胜利是最重要的](https://bbs.hupu.com/55784983.html)
> 概要: 虎扑10月05日讯 中超联赛第21轮结束的一场比赛，成都蓉城1-0战胜武汉长江。赛后，成都蓉城队球员理查德接受了媒体采访。点评一下自己的这个进球首先非常感谢大家的祝福，之前也是进过球，但是被吹了，比较
# 小说
### [渐进的改革：中国政治体制改革的经验与反思](http://book.zongheng.com/book/881046.html)
> 作者：刘智峰

> 标签：评论文集

> 简介：客观理性看待中国政治体制，构建中国特色的改革政治学。把脉中国局势，聚焦改革热点。深入破解中国政治体制改革的走向，解读党和国家及普通民众当下最关注的政治改革热点话题。《渐进的改革：中国政治体制改革的经验与反思》从分析中国改革的初始条件和所需要解决的迫切问题入手，论述了中 国的改革开放战略为什么没有选择前苏联激进式的改革，而是采取了从经济改革入手，然后逐步深入到社会和政治领域的渐进式改革模式的原因；从多个侧面和角度论证渐进式改革的合理性 及在政治体制改革领域取得的重要进展，并指出中国的改革开放和繁荣发展以及经济发展方式的转变，和道德伦理方面的改善等一系列经济社会领域问题的解决，其关键都在于政治体制的改革。但政治体制改革不能只有激情和理想，必须理性认识，清醒抉择，脚踏实地，立足于中国的历史和国情，坚持有领导、可控制、积极而稳妥的渐进式原则。古今中外，由于政治发展道路选择错误而导致社会动荡

> 章节末：后记

> 状态：完本
# 论文
### [Disentangled Modeling of Domain and Relevance for Adaptable Dense Retrieval | Papers With Code](https://paperswithcode.com/paper/disentangled-modeling-of-domain-and-relevance)
> 日期：11 Aug 2022

> 标签：None

> 代码：https://github.com/jingtaozhan/disentangled-retriever

> 描述：Recent advance in Dense Retrieval (DR) techniques has significantly improved the effectiveness of first-stage retrieval. Trained with large-scale supervised data, DR models can encode queries and documents into a low-dimensional dense space and conduct effective semantic matching. However, previous studies have shown that the effectiveness of DR models would drop by a large margin when the trained DR models are adopted in a target domain that is different from the domain of the labeled data. One of the possible reasons is that the DR model has never seen the target corpus and thus might be incapable of mitigating the difference between the training and target domains. In practice, unfortunately, training a DR model for each target domain to avoid domain shift is often a difficult task as it requires additional time, storage, and domain-specific data labeling, which are not always available. To address this problem, in this paper, we propose a novel DR framework named Disentangled Dense Retrieval (DDR) to support effective and flexible domain adaptation for DR models. DDR consists of a Relevance Estimation Module (REM) for modeling domain-invariant matching patterns and several Domain Adaption Modules (DAMs) for modeling domain-specific features of multiple target corpora. By making the REM and DAMs disentangled, DDR enables a flexible training paradigm in which REM is trained with supervision once and DAMs are trained with unsupervised data. Comprehensive experiments in different domains and languages show that DDR significantly improves ranking performance compared to strong DR baselines and substantially outperforms traditional retrieval methods in most scenarios.
### [SpeechNAS: Towards Better Trade-off between Latency and Accuracy for Large-Scale Speaker Verification | Papers With Code](https://paperswithcode.com/paper/speechnas-towards-better-trade-off-between)
> 概要: Recently, x-vector has been a successful and popular approach for speaker verification, which employs a time delay neural network (TDNN) and statistics pooling to extract speaker characterizing embedding from variable-length utterances. Improvement upon the x-vector has been an active research area, and enormous neural networks have been elaborately designed based on the x-vector, eg, extended TDNN (E-TDNN), factorized TDNN (F-TDNN), and densely connected TDNN (D-TDNN).