---
title: 2022-12-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Borovets_ZH-CN5914681811_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-15 22:57:04
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Borovets_ZH-CN5914681811_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [2022年十大副业，要多卷有多卷](https://www.woshipm.com/zhichang/5706029.html)
> 概要: 越来越多年轻人开始整起了副业，以尝试在本职工作之外找到更多赚钱机会，那么回顾这个2022，都有哪些副业给人们留下了深刻印象？本篇文章里，作者便总结了2022相对较火的十大副业，一起来看一下吧。2022......
### [我在小红书约拍，月入1.5W](https://www.woshipm.com/operate/5707401.html)
> 概要: 随着互联网的发展，大家开始逐渐在网络上找“副业”，“搞钱”的方式也是五花八门。最让人印象深刻的应该是约拍了，那么以这类技能为主的账号发展空间有多大呢？这篇文章可能有你想要的答案。去年12月，豆瓣用户@......
### [写在2022年末 | 对助贷行业的思考与展望](https://www.woshipm.com/it/5705172.html)
> 概要: 助贷即协助机构进行放贷的意思，并从中赚取费用。这一行业，发展的方式是怎样的，未来的趋势又是如何，互金助贷平台破局的方向有哪些？作者在文章中进行了分析，一起来看看吧。一、助贷业务模式介绍1、定义：顾名思......
### [视频：黄圣依近90岁婆婆感染新冠 杨子发文透露已转阴大赞老中药](https://video.sina.com.cn/p/ent/2022-12-15/detail-imxwsmqi3846884.d.html)
> 概要: 视频：黄圣依近90岁婆婆感染新冠 杨子发文透露已转阴大赞老中药
### [中国品牌，终于干翻日系车了](https://www.huxiu.com/article/736054.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博编辑｜周到头图｜视频截图4.5 + 7.8 + 14 = 26.326.3 ÷ 23 = 1.14在文章开头陈列两行算式，不是为了让你重回小学数学课堂，而是希望用最直观的方......
### [36氪首发 | 「燃麦科技」获BAI资本千万美元A轮投资，将为全球用户建造数字人美好生活作为未来的战略方向](https://36kr.com/p/2043721939586050)
> 概要: 36氪首发 | 「燃麦科技」获BAI资本千万美元A轮投资，将为全球用户建造数字人美好生活作为未来的战略方向-36氪
### [《卧龙：苍天陨落》Steam版预售开启 售价298元](https://www.3dmgame.com/news/202212/3858438.html)
> 概要: 近日《卧龙：苍天陨落》在Steam开启预售，国区标准版售价298元，数字豪华版售价447元，有兴趣的玩家可以预购了。Steam商店地址：点击进入数字豪华版包含游戏本体、游戏季票(追加DLC“逐鹿中原”......
### [《JOJO的奇妙冒险》第9部漫画 23年2月17日开始连载](https://acg.gamersky.com/news/202212/1547451.shtml)
> 概要: 荒木飞吕彦原作的《JOJO的奇妙冒险》第9部漫画《JOJOLANDS》，宣布将于2023年2月17日开始在《Ultra Jump》3月号上连载，​​​封面彩图公开。
### [TV动画《间谍教室》公开主宣传图与PV](https://news.dmzj.com/article/76542.html)
> 概要: TV动画《间谍教室》宣布了将于2023年1月5日开始播出的消息。本作的主宣传图与PV也一并公开。在这次的PV中，伴随着nonoc演唱的OP主题曲《灯火》，可以看到各个主角的样子。
### [重塑中的一级市场双边秩序｜36氪年度机构名册发布](https://36kr.com/p/2043983183858690)
> 概要: 重塑中的一级市场双边秩序｜36氪年度机构名册发布-36氪
### [升级“五大举措”保障消费者寄递体验 德邦快递连续三年春节不休](http://www.investorscn.com/2022/12/15/104840/)
> 概要: 春节临近，又到了回家看望父母、走亲访友的日子，以前许多人返乡途中总是手提着大包小包的礼物，现在有了快递公司的春节不打烊服务，越来越多的人选择将礼物和行李寄递回家乡，自己便能轻轻松松返乡过大年。作为大件......
### [日本拍摄《城市猎人》真人电影！铃木亮平主演](https://news.dmzj.com/article/76544.html)
> 概要: 北条司创作的《城市猎人》宣布了真人电影化决定的消息。这次的电影将由日本拍摄，在Netflix上播出。
### [苹果供应链东南亚大转移，可它离不开中国工厂 | 焦点分析](https://36kr.com/p/2043678118022145)
> 概要: 苹果供应链东南亚大转移，可它离不开中国工厂 | 焦点分析-36氪
### [宇宙偶像的故事 短篇动画《SPACE IDOL》1月播出](https://news.dmzj.com/article/76546.html)
> 概要: 原创短篇动画《SPACE IDOL》宣布了将于2023年1月在DMM TV上播出的消息。
### [BEARKING 汉堡品牌设计](https://www.zcool.com.cn/work/ZNjMzNDI3ODQ=.html)
> 概要: 设计：胡晓波、人可IP形象：决绝困C4D：马点本作品由胡晓波工作室原创出品......
### [日本卡片游戏市场两年增长4成 人气激增带来高价倒卖](https://www.3dmgame.com/news/202212/3858474.html)
> 概要: 日前据日媒报道，目前日本卡片游戏市场越来越火爆，市场规模两年时间就增长4成，而且各种恶意高价倒卖新闻也越来越频发。·根据日本玩具协会的统计，日本卡片游戏·集换式卡片市场规模2021年度约为1782亿日......
### [《一拳超人》重制版221话:龙卷登场 对战人造能力者](https://acg.gamersky.com/news/202212/1547528.shtml)
> 概要: 《一拳超人》重制版221话公开，为了想要变得更强，赛克斯不断地研究新能力——能看到未来的“第三只眼”，然而结果却是让她走向了崩溃。
### [200块一张票的《阿凡达2》能支棱起来吗？](https://www.huxiu.com/article/742878.html)
> 概要: 本文来自微信公众号：毒眸（ID：DomoreDumou），作者：刘南豆，编辑：张颖，头图来源：《阿凡达：水之道》剧照你买了《阿凡达：水之道》的票吗？由于影片在全国开启了3000场超前点映，最早的一批的......
### [B站公布2022年度弹幕：“优雅”当选！](https://www.3dmgame.com/news/202212/3858478.html)
> 概要: 随着2022年仅剩不到最后半个月，B站也如往年一般，公布了今年的年度弹幕：“优雅”。2022年的年度弹幕虽然也来自“梗”，但却不再是网络生造词，而是更多呈现出了原有词汇在互联网传播中的解构与再造。视频......
### [纪念动画播出30周年 《幽游白书》珍藏唱片集推出](https://www.3dmgame.com/news/202212/3858479.html)
> 概要: 喜欢复古唱片的小伙伴注意了，收集困难的《幽游白书》珍藏唱片集即将推出，预定2023年2月18日发售，作为纪念《幽游白书》电视动画播出30周年特别产品。·《幽游白书》珍藏唱片集曾经在2017年时，作为电......
### [Falcom发表游戏《伊苏》新作《伊苏10-NORDICS-》](https://news.dmzj.com/article/76551.html)
> 概要: 日本Falcom宣布了《伊苏》系列新作《伊苏10-NORDICS-》将于2023年发售的消息。本作游戏对应PS5、PS4、NS平台。
### [《Abyss World : Apocalypse》Steam页面 支持简中](https://www.3dmgame.com/news/202212/3858490.html)
> 概要: 今日（12月15日），第三人称ARPG游戏《深渊国度：天启（Abyss World : Apocalypse）》Steam页面上线，游戏支持简体中文，感兴趣的玩家可以点击此处进入商店页面。游戏介绍：深......
### [精确集团CEO修仕辉：同频时代脉动、聚焦永续发展 争创全球投资一流企业](http://www.investorscn.com/2022/12/15/104845/)
> 概要: “精益求精 确然不群”精确集团的名字由此而来。前者代表用行动诠释的工匠精神，后者是用精神传达的气节风骨......
### [精确集团资本中心总裁齐子恺：扎实深入为产业发展注入资本动能](http://www.investorscn.com/2022/12/15/104846/)
> 概要: 精确资本中心作为精确集团落实现代化产业体系建设的产业投融资中心，承载着精确集团推动绿色发展及新能源产业、科技创新产业、矿产及资源再生产业、生物科技产业四大领域高质量发展的愿景......
### [昆仑万维重磅发布AIGC全系列算法与模型，领跑未来](http://www.investorscn.com/2022/12/15/104851/)
> 概要: 2022年12月15日，昆仑万维在北京举行AIGC技术发布会，会上昆仑万维CEO方汉正式发布了「昆仑天工」AIGC全系列算法与模型，并宣布模型开源。「昆仑天工」旗下模型包括天工巧绘SkyPaint、天......
### [国6B汽油要全面上线了，听说这玩意又贵又不耐烧？](https://www.huxiu.com/article/742675.html)
> 概要: 本文来自微信公众号：差评 （ID：chaping321），撰文：八戒，编辑：面线，题图来自：视觉中国前几天我去加油，结果遇到了加油站卸油，等了好久。而且这次卸油和平时还不一样，据说是换了新油。回来看了......
### [海外new things | 网页设计工具「Komi」种子轮融资500万美元，为中小内容创作者定制网站首页](https://36kr.com/p/2039798863326466)
> 概要: 海外new things | 网页设计工具「Komi」种子轮融资500万美元，为中小内容创作者定制网站首页-36氪
### [日媒曝佐藤健将和绫濑遥闪婚 双方事务所暂未回应](https://ent.sina.com.cn/s/j/2022-12-15/doc-imxwucxt6560833.shtml)
> 概要: 新浪娱乐讯 据媒体15日报道，日本演员佐藤健将和绫濑遥闪婚，目前双方事务所没有回应。　　据悉，佐藤健和绫濑遥在2018年时因合作电视剧《继母与女儿的蓝调》曾传出绯闻，今年再度合作《继母与女儿的蓝调》的......
### [视频：田曦薇GQ红毯造型曝光 公主切白纱裙氛围感十足](https://video.sina.com.cn/p/ent/2022-12-15/detail-imxwucxp2935467.d.html)
> 概要: 视频：田曦薇GQ红毯造型曝光 公主切白纱裙氛围感十足
### [欺负以色列记者，成了世界杯的流行文化](https://www.huxiu.com/article/743203.html)
> 概要: 出品 | 那個NG作者 | 渣渣郡本文首发于虎嗅青年文化组公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。莫德里奇和C罗的最后一届世界杯之旅，结束了......
### [《2022中国中青年养老成熟度调查报告》出炉，揭开中青年的养老自画像](http://www.investorscn.com/2022/12/15/104853/)
> 概要: 2022年,个人养老金制度落地,关于养老话题的讨论热度持续攀升。近日,在中国证券投资基金业协会的指导下,中信证券联合中国人民大学发布《2022中国中青年养老成熟度调查报告》,通过对3000余名中青年的......
### [宁德大涨超5%，创业板指能否转弱为强？](https://www.yicai.com/video/101625289.html)
> 概要: 宁德大涨超5%，创业板指能否转弱为强？
### [9 种糕点组合，真巧饼干零食 1.5 斤礼盒 29.9 元（减 50 元）](https://lapin.ithome.com/html/digi/661426.htm)
> 概要: 【真巧旗舰店】9 种糕点组合，真巧饼干零食 749g 礼盒报价 79.9 元，限时限量 50 元券，实付 29.9 元包邮，领券并购买。使用最会买 App下单，预计还能再返 4.93 元，返后 24......
### [消息称用于生产计算机芯片的石英将再次涨价](https://www.ithome.com/0/661/428.htm)
> 概要: IT之家12 月 15 日消息，据 The Elec 报道，用于生产计算机芯片的石英价格预计将再次上涨。它们的价格在今年早些时候已经上涨，但预计将再增加 10%。这是由于原材料价格的上涨以及不利的汇率......
### [辉瑞新冠特效药秒下架，印度“平替”火了](https://finance.sina.com.cn/china/gncj/2022-12-15/doc-imxwukfr6655883.shtml)
> 概要: 来源：北京商报 继开放“网售”又火速下架后，新冠特效药再次搅动市场，只不过这次的主角变成了印度版的“平替”。12月15日，“新冠印度仿制药被卖到一盒上千元”登上热搜...
### [研究:新冠患者存明显的维生素C缺乏症,有必要补充VC](https://www.yicai.com/news/101625319.html)
> 概要: 研究结果显示：未接受VC治疗的COVID-19患者的平均血浆VC浓度为2.00 mg/L，几乎是健康志愿者的五分之一，后者为9.23 mg/L。
### [指数走势分化 结构性行情如何把握？](https://www.yicai.com/video/101625326.html)
> 概要: 指数走势分化 结构性行情如何把握？
### [花旗宣布将逐步关闭中国大陆个人银行业务 继续发展企业与机构客户业务](https://finance.sina.com.cn/china/gncj/2022-12-15/doc-imxwukfp9877997.shtml)
> 概要: 来源：北京商报 北京商报讯（记者 李海颜）继此前宣布计划退出包括中国大陆市场在内的14个市场个人银行业务后，12月15日，花旗正式宣布将逐步关闭中国大陆的个人银行业务...
### [630亿元！地方政府“出手”发行专项债 中小银行“补血”进行时](https://finance.sina.com.cn/china/gncj/2022-12-15/doc-imxwukfp9877332.shtml)
> 概要: 来源：北京商报 为增强地方银行资本实力和风险抵御能力，多地政府相继“出手”发行中小银行专项债。12月15日，北京商报记者梳理发现，今年以来已有辽宁省、甘肃省、河南省...
### [钟南山：相比香港早期，内地疫苗接种率较高、不会出现高病死率](https://finance.sina.com.cn/china/gncj/2022-12-15/doc-imxwukfr6663001.shtml)
> 概要: 新冠病毒“北强南弱”？越早“阳”越好？高烧咽痛这么难受也算轻症？感染奥密克戎有后遗症吗？会出现反复感染吗？学校怎么做好上课和防控？
### [【IT之家评测室】引领行业快速迈步折叠屏轻量化时代：OPPO Find N2 深度评测](https://www.ithome.com/0/661/447.htm)
> 概要: 在 OPPO 未来科技大会 2022（OPPO INNO DAY 2022）的第二天，Find N2 系列折叠旗舰新品正式发布，此次 Find N2 系列折叠屏手机包括两款，一款是 OPPO Find......
### [华纳兄弟 100 周年将至，旗下十几部经典电影将 4K 重制](https://www.ithome.com/0/661/448.htm)
> 概要: IT之家12 月 15 日消息，据华纳兄弟官方消息，为庆祝 2023 年 4 月 4 日华纳兄弟影业成立 100 周年，华纳兄弟探索集团（今天正式启动以 “让每一个故事绽放光芒” 为主题的全球庆祝活动......
### [欧洲央行决定加息50个基点](https://www.yicai.com/news/101625424.html)
> 概要: 这是欧洲央行自今年7月以来的第四次加息。欧洲央行持续加息的主要目的是遏制欧元区的通胀。与加息相配合，欧洲央行还采取了收回长期再贷款和缩减购债规模等措施。
### [社论：扩内需的重要基础是持续扩大中等收入群体规模](https://www.yicai.com/news/101625428.html)
> 概要: 扩大中等收入群体，没有其自身努力是不行的，使更多劳动者通过自身努力进入中等收入群体，这是将来的方向。
### [刘鹤：对于明年中国经济实现整体性好转 我们极有信心](https://finance.sina.com.cn/china/gncj/2022-12-15/doc-imxwuqpm9788099.shtml)
> 概要: 原标题 刘鹤在第五轮中国-欧盟工商领袖和前高官对话上发表致辞 新华社北京12月15日电12月15日，国务院副总理刘鹤在第五轮中国-欧盟工商领袖和前高官对话上发表书面致辞。
### [大马丁中场讲话：他们要是想进球，必须先把我杀了](https://bbs.hupu.com/56972104.html)
> 概要: 阿根廷媒体曝光了中场门将埃米利亚诺-马丁内斯的讲话。大马丁：“我们必须完成零封，伙计们。他们要是想进球，必须先把我杀了。我们一定要寸步不让！” 来源：  新浪微博    标签：埃米里奥-马丁内斯阿根廷
### [流言板汉普顿主动要求球队将自己下放，目的是为了获得出场时间](https://bbs.hupu.com/56972231.html)
> 概要: 虎扑12月15日讯 今日魔术官方宣布，将RJ-汉普顿下放至发展联盟。根据魔术记者Khobi Price的报道，汉普顿主动要求球队将自己下放，目的则是为了获得出场时间。本赛季至今，汉普顿场均出战15.5
### [西热力江晒对阵天津队集锦：第二阶段首胜，兄弟们再接再厉](https://bbs.hupu.com/56972238.html)
> 概要: 2022-23赛季CBA常规赛第13轮，同曦队111-103战胜天津队。赛后同曦队主教练兼队员西热力江更新抖音，晒比赛集锦。配文：“第二阶段首胜，兄弟们再接再厉！”12月17日，同曦队将对阵福建队。
### [流言板佩里西奇：无缘世界杯决赛让我们很伤心，现在要为奖牌而战](https://bbs.hupu.com/56972268.html)
> 概要: 虎扑12月15日讯 此前结束的卡塔尔世界杯半决赛的比赛中，克罗地亚0-3不敌阿根廷，无缘连续两届世界杯进入决赛。而在无缘世界杯决赛后，克罗地亚前锋佩里西奇也在社交媒体上发文谈到了自己的感受。佩里西奇在
# 小说
### [江故](https://book.zongheng.com/book/867179.html)
> 作者：鸭头

> 标签：武侠仙侠

> 简介：妖魔鬼怪，四大异族妖怪妖怪，皆由牲畜修炼而来，修出内丹的是妖，未修出内丹的是怪魔鬼魔鬼，为之必先为人，堕仙之人即入魔，生前有怨可化鬼千万年来，与之水火不容的神宗乃是天下第一名门正派当这名出家和尚走出神宗时，才发现江湖之大，岂止只有两方争斗；群雄争霸，诸方势力比肩而起；腥风血雨，小僧只好四海为家。

> 章节末：第九百四十章 最长情的告白

> 状态：完本
# 论文
### [ESKNet-An enhanced adaptive selection kernel convolution for breast tumors segmentation | Papers With Code](https://paperswithcode.com/paper/esknet-an-enhanced-adaptive-selection-kernel)
> 日期：5 Nov 2022

> 标签：None

> 代码：https://github.com/cgpxy/esknet

> 描述：Breast cancer is one of the common cancers that endanger the health of women globally. Accurate target lesion segmentation is essential for early clinical intervention and postoperative follow-up. Recently, many convolutional neural networks (CNNs) have been proposed to segment breast tumors from ultrasound images. However, the complex ultrasound pattern and the variable tumor shape and size bring challenges to the accurate segmentation of the breast lesion. Motivated by the selective kernel convolution, we introduce an enhanced selective kernel convolution for breast tumor segmentation, which integrates multiple feature map region representations and adaptively recalibrates the weights of these feature map regions from the channel and spatial dimensions. This region recalibration strategy enables the network to focus more on high-contributing region features and mitigate the perturbation of less useful regions. Finally, the enhanced selective kernel convolution is integrated into U-net with deep supervision constraints to adaptively capture the robust representation of breast tumors. Extensive experiments with twelve state-of-the-art deep learning segmentation methods on three public breast ultrasound datasets demonstrate that our method has a more competitive segmentation performance in breast ultrasound images.
### [CEMENT: Incomplete Multi-View Weak-Label Learning with Long-Tailed Labels | Papers With Code](https://paperswithcode.com/paper/cement-incomplete-multi-view-weak-label)
> 概要: A variety of modern applications exhibit multi-view multi-label learning, where each sample has multi-view features, and multiple labels are correlated via common views. In recent years, several methods have been proposed to cope with it and achieve much success, but still suffer from two key problems: 1) lack the ability to deal with the incomplete multi-view weak-label data, in which only a subset of features and labels are provided for each sample; 2) ignore the presence of noisy views and tail labels usually occurring in real-world problems.