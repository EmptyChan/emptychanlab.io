---
title: 2022-10-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PrinceChristianSound_ZH-CN0274463143_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-16 21:56:58
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PrinceChristianSound_ZH-CN0274463143_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [SaaS系统框架搭建详解](https://www.woshipm.com/pd/5644497.html)
> 概要: SaaS系统能提供一个或者多个行业常见场景的功能支持，只要在有网络的情况下，便“随处可用、拿来即用、不用下载”，所以现在也是一个流行的趋势。本文介绍了SaaS系统的框架搭建，一起来学习一下吧。根据百度......
### [年轻人“羊毛图鉴”，从外卖到直播间](https://www.woshipm.com/it/5644411.html)
> 概要: 如今年轻人的消费观，似乎有些“奇怪”，1000块可以花，1块必须省。他们热衷于在各个平台上寻找优惠，从薅“外卖羊毛”到蹲“直播间便宜货”，享受快乐的同时，也有相应的“不愉悦”。商家与用户，是如何“薅”......
### [广告策略产品（1）：广告业务目标构建与思考](https://www.woshipm.com/operate/5607581.html)
> 概要: 广告策略整体来看，会复杂一些，尤其在定向、排序、广告展示、归因策略等方面有明显的差异性。本文作者从广告问题的整体业务目标与函数构建、预估问题、广告定价问题这三个方面，分享了对广告业务目标构建的思考，一......
### [《王冠》第5季发布新剧照 德比齐饰演戴安娜](https://ent.sina.com.cn/v/u/2022-10-16/doc-imqmmthc1027096.shtml)
> 概要: 新浪娱乐讯 北京时间10月16日消息，据外国媒体报道，网飞热剧《王冠》第5季发布新剧照，11月9日上线。新一批演员出演英国王室成员，造型和氛围不错。　　这季来到1990年代，聚焦备受关注的查尔斯戴安娜......
### [《继承之战》第4季在挪威拍摄 斯卡斯加德回老家](https://ent.sina.com.cn/v/u/2022-10-16/doc-imqmmthc1027901.shtml)
> 概要: 新浪娱乐讯 北京时间10月16日消息，据外国媒体报道，北欧人本色，《继承之战》第4季将在挪威拍摄，亚历山大·斯卡斯加德于第3季加盟饰演科技大亨卢卡斯·马特森，其公司GoJo正在收购主角家的Waysta......
### [研究人员开发出新技术 十分钟完成电动汽车充电](https://finance.sina.com.cn/tech/roll/2022-10-16/doc-imqqsmrp2713064.shtml)
> 概要: 科技日报北京10月15日电 （记者张梦然）据发表在最新一期《自然》上的一项研究，美国宾夕法尼亚州立大学研究人员开发出一种突破性技术，将电动汽车电池的充电时间缩短为仅10分钟，这是更短充电时间和更长驾驶......
### [办了会员也得看广告？网飞含广告套餐6.99美元/月](https://finance.sina.com.cn/tech/internet/2022-10-16/doc-imqmmthc1034170.shtml)
> 概要: 中关村在线消息：近日，Netflix推出了订阅价格更低的含广告会员套餐，根据Netflix官方公布的消息，含广告套餐的订阅价格为6.99美元/月，下个月将在12个国家/地区正式推出，包括澳大利亚、巴西......
### [iPhone折叠屏专利曝光 屏幕折痕或可自行修复](https://finance.sina.com.cn/tech/it/2022-10-16/doc-imqmmthc1034168.shtml)
> 概要: 中关村在线消息：近日，据国内媒体报道，苹果一项名为“具有柔性显示屏覆盖层的电子设备”新专利曝光。根据专利所展示的内容来看，该设备设计有一个铰链，设备可围绕一个弯曲轴进行弯曲，一个显示屏可以横跨弯曲轴......
### [《漫威暗夜之子》妮可·米诺鲁预告片展示技能特性](https://www.3dmgame.com/news/202210/3853861.html)
> 概要: 2K Games和FiraxisGames日前分享了回合制战术游戏《漫威暗夜之子》新一期角色实机预告片。与《漫威暗夜之子》之前的角色介绍方式一样，官方之前已经分享了简短的角色登场预告。这次的视频讲解了......
### [李一男的白日梦，要被华为拍醒了](https://www.huxiu.com/article/683982.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 自游家汽车人称“华为太子”的李一男，很喜欢自己那辆JEEP大切诺基。当然，这件事是在他造车以后，才被大家知道的。在第一款产品自游家NV上，李......
### [不到一个月降价580元！华为Mate 50系列5G通信壳便宜了：219元起](https://finance.sina.com.cn/tech/it/2022-10-16/doc-imqqsmrp2721992.shtml)
> 概要: 9月23日，数源科技SoyeAlink打造的华为Mate 50系列5G通信壳上市发售，其中Mate 50、Mate 50E、Mate 50 Pro三款通信壳售价799元，RS保时捷版售价899元......
### [组图：霍启刚郭晶晶看芭蕾舞剧 两人戴黑框眼镜相依偎](http://slide.ent.sina.com.cn/star/slide_4_704_376527.html)
> 概要: 组图：霍启刚郭晶晶看芭蕾舞剧 两人戴黑框眼镜相依偎
### [十大商战法则：成为第一胜过做得更好](https://www.huxiu.com/article/686678.html)
> 概要: 本文来自微信公众号：长江商学院（ID：Weixin_CKGSB），作者：艾·里斯，原文标题：《“第一”胜过“更好”：「定位之父」里斯留下的十大商战法则》，头图来自：《功夫》剧照定位理论创始人、《定位》......
### [《荣耀战魂》新万圣节活动公布 4人大战蜘蛛怪](https://www.3dmgame.com/news/202210/3853864.html)
> 概要: 育碧为《荣耀战魂》公布了一个新的万圣节主题活动“Web of the Jorogumo”，该活动将于10月20日开始，11月10日结束。视频：“Web of the Jorogumo”活动将带来一个4......
### [《蔑视》Steam褒贬不一 在线峰值不到8000](https://www.3dmgame.com/news/202210/3853867.html)
> 概要: 恐怖冒险游戏《蔑视》已于10月14日推出，截止到目前Steam上本作评价为“褒贬不一”，好评率为69%。据玩家评价，《蔑视》的强项在于其美术风格，但缺点也很明显，包括流程太短，5小时便可通过，速度快的......
### [现代轻奢风](https://www.zcool.com.cn/work/ZNjIzODM3NTY=.html)
> 概要: Collect......
### [谢谢你，守住我整个童年](https://www.huxiu.com/article/687069.html)
> 概要: 本文来自微信公众号：Sir电影 （ID：dushetv），作者：毒Sir，编辑助理：小田不让切、吉尔莫的陀螺，头图来自：《哈利·波特》剧照相信昨天大家都看到了这条消息：他是我们曾经最忠诚的猎场看护员，......
### [《我的世界：传奇》明年春季发售 全新CG演示赏](https://www.3dmgame.com/news/202210/3853870.html)
> 概要: 在动作/策略游戏《我的世界：传奇》中，玩家将要对抗猪灵，拯救主世界。开发团队在Minecraft Live 2022活动期间宣布本作将于明年春季发售。除了大致发售时间之外，开发团队还公布了一段游戏CG......
### [古一、奥丁、灭霸到底谁更强](https://new.qq.com/rain/a/20221016V03XVZ00)
> 概要: 古一、奥丁、灭霸到底谁更强
### [《王者天下》TV动画官方放出先导图 将于24年1月开播](https://acg.gamersky.com/news/202210/1527434.shtml)
> 概要: 《王者天下》官方宣布TV动画第五季将于2024年1月开播。
### [中国区域经济进阶之路：五大核心增长极崛起，迈向世界级城市群](https://finance.sina.com.cn/china/gncj/2022-10-16/doc-imqmmthc1069170.shtml)
> 概要: 21世纪经济报道记者刘美琳、吴文汐 广州报道 中国区域经济根植于中国经济的整体发展，也与区域发展战略的变化密切相关。 10月13日，上海举行市政府新闻发布会...
### [香港财政司司长陈茂波：今年香港金融科技周将发布虚拟资产在港发展政策宣言](https://finance.sina.com.cn/china/gncj/2022-10-16/doc-imqqsmrp2752945.shtml)
> 概要: 香港财政司司长陈茂波10月16日发表题目为《香港的创科发展》的文章，表示特区政府将于今年10月31日至11月4日举行一年一度的香港金融科技周...
### [信创概念走强，多路资金同时加仓，北上资金“戒酒”，两大白酒股净流出超50亿元](https://finance.sina.com.cn/stock/y/2022-10-16/doc-imqqsmrp2753187.shtml)
> 概要: 本周共有38股登上北上资金成交活跃榜。 节后一周A股市场强势反弹，市场情绪活跃，两市交易额相对回升。市场一周表现来看，沪指一周涨1.57%，深证成指涨3.18%，创业板指涨6...
### [在希望的田野上丨河南卫辉冬小麦播种进行时](https://finance.sina.com.cn/china/gncj/2022-10-16/doc-imqmmthc1071352.shtml)
> 概要: 眼下，河南卫辉50万亩冬小麦进入适播期，当地抢抓当前土壤墒情和天气晴好有利时机，组织农业机械及农技人员深入田间地头，帮助农户抓紧进行小麦播种。
### [张益唐被曝已证明黎曼猜想相关问题，震动数学界](https://www.huxiu.com/article/687240.html)
> 概要: 本文来自微信公众号：量子位 （ID：QbitAI），作者：金磊、Alex，头图来自：视觉中国网传数学家张益唐，已经攻克了朗道-西格尔零点猜想（Landau-Siegel Zeros Conjectur......
### [氢科学领域初创企业氢美科技宣布完成2,000万元Pre-A轮融资](http://www.investorscn.com/2022/10/16/103509/)
> 概要: 近日，氢科学领域初创企业氢美健康科技宣布完成了一轮2,000万元的Pre-A融资，本轮融资由A股上市公司世纪华通旗下盛趣资本、以及唯快资本、能图资本等数家基金共同联合投资......
### [“V”形两端，硅料龙头“饮冰十年”](http://www.investorscn.com/2022/10/16/103510/)
> 概要: 每经研究员 朱成祥......
### [《微软飞行模拟》升级档将支持FSR 2.0和DLSS 3](https://www.3dmgame.com/news/202210/3853884.html)
> 概要: 微软近日宣布，《微软飞行模拟》新版beta升级档即将发布，将为AMD FSR 2.0和NVIDIA DLSS 3提供支持。Beta升级档1.29.22.0将为DX12提供全新内存碎片管理系统，防止显存......
### [杨洋《我的人间烟火》杀青！消防员造型曝光，实景拍摄打磨用心](https://new.qq.com/rain/a/20221016V036QX00)
> 概要: 杨洋《我的人间烟火》杀青！消防员造型曝光，实景拍摄打磨用心
### [我国天地联合观测到迄今最亮伽马射线暴：超以往最亮 10 倍以上，在国际引发巨大反响](https://www.ithome.com/0/646/905.htm)
> 概要: IT之家10 月 16 日消息，据中国科学院高能物理研究所消息，2022 年 10 月 9 日 21 点 17 分（北京时间），高海拔宇宙线观测站（LHAASO，拉索）、高能爆发探索者（HEBS）和慧......
### [中国共产党第二十次全国代表大会在北京隆重开幕](https://www.yicai.com/image/101563037.html)
> 概要: 中国共产党第二十次全国代表大会在北京隆重开幕
### [一个空荡荡的元宇宙世界！Meta元宇宙平台用户不足20万](https://finance.sina.com.cn/chanjing/cyxw/2022-10-16/doc-imqqsmrp2772446.shtml)
> 概要: Meta元宇宙虚拟社交平台Horizon Worlds被曝用户数量不及预期。 当地时间10月15日，《华尔街日报》报道称，脸书（Facebook）母公司Meta旗下的虚拟社交平台Horizon...
### [《爱在黎明破晓前》这是最美的爱情，既浪漫又真实](https://new.qq.com/rain/a/20221016V05ALA00)
> 概要: 《爱在黎明破晓前》这是最美的爱情，既浪漫又真实
### [美股超级行情一日游，反转仍需等待](https://www.yicai.com/news/101563232.html)
> 概要: 资金持续外流，新风暴或在酝酿中。
### [安卓代码曝光谷歌 Pixel 平板电脑和折叠屏手机：配备侧边指纹传感器，分辨率出炉](https://www.ithome.com/0/646/943.htm)
> 概要: IT之家10 月 16 日消息，根据外媒 9to5google 发现的代码信息，谷歌 Pixel 平板电脑和折叠屏手机将配备侧边指纹传感器。9to5google 发现了专门用于测试侧边指纹传感器 UI......
### [她和“纸片人”恋爱，与你何关？](https://new.qq.com/rain/a/20221016A05MP700)
> 概要: 喜剧是冒犯的艺术，但充斥刻板印象的冒犯，算不上艺术。最近，《一年一度喜剧大赛2》正在热播，第3期节目的纯享版上线后，喜剧作品《男友来了》在网络上引起不小的争议。剧情内容大致是：            ......
### [主板IPO周报：多利汽车过会，与特斯拉销售关系引关注](https://www.yicai.com/news/101563186.html)
> 概要: 彩蝶实业上会在即。
### [消息称《巫师 3》次世代版有望在 12 月 9 日发售，登陆主机、PC 平台](https://www.ithome.com/0/646/944.htm)
> 概要: IT之家10 月 16 日消息，《巫师》官方此前表示，《巫师 3：狂猎》次世代版本将在今年第四季度发布。现在，Reddit 论坛有一位自称是欧洲头部游戏零售商 Game UK 内部员工的用户爆料称，他......
### [三季报逾九成公司预喜，11家增速超10倍，锂矿股表现最亮眼](https://www.yicai.com/news/101563278.html)
> 概要: 锂矿价格四季度或难向下
### [国产道格 Doogee S96 GT 三防手机发布：土豪金配色，搭载 15 米夜视镜头，首发约 1440 元起](https://www.ithome.com/0/646/945.htm)
> 概要: IT之家10 月 16 日消息，国产手机厂商 Doogee（道格）推出了 S96 Pro 的继任者S96 GT 手机，在处理器、内部存储、无线充电和视频录制功能方面进行了改进，首发价 200 美元起（......
### [悲观情绪弥漫IMF年会，熊市何时是尽头？](https://www.yicai.com/news/101563293.html)
> 概要: 美联储究竟要加息到什么程度才会收手，成了最受关注的问题。
### [流言板劳塔罗：很高兴追平米利托进球纪录，过几天我们就一起吃饭](https://bbs.hupu.com/55940637.html)
> 概要: 虎扑10月16日讯 在刚刚结束的意甲第10轮比赛中，国米主场2-0战胜萨勒尼塔纳，赛后进球功臣劳塔罗也接受了DAZN、意大利天空体育以及国米官方TV的采访。国米重新回到了胜利轨道中…“这对我们来说是重
### [流言板芒特英超进球达25粒，成为蓝军队史第二年轻达次成就球员](https://bbs.hupu.com/55940735.html)
> 概要: 虎扑10月16日讯 在切尔西客场对阵维拉比赛第5分钟，芒特帮助球队首开纪录。此球过后，芒特个人英超进球数已达到25粒，如今23岁279天的他也成为了切尔西队史第二年轻打进25球的球员，仅次于此前的阿扎
### [流言板官方：利兹联主场停电导致裁判通讯系统故障，比赛暂时中断](https://bbs.hupu.com/55940759.html)
> 概要: 虎扑10月16日讯 利兹联和阿森纳官方共同宣布，因利兹联的主场埃兰路球场停电，导致裁判的通讯系统出现问题，本场阿森纳对阵利兹联的比赛暂时中断。   来源： 虎扑    标签：利兹联阿森纳
### [凯指导我的超人！维拉禁区内三连击，凯帕神勇化解](https://bbs.hupu.com/55940771.html)
> 概要: 凯指导我的超人！维拉禁区内三连击，凯帕神勇化解
# 小说
### [美漫诸天](https://m.qidian.com/book/1023802291/catalog)
> 作者：西湖龙腾

> 标签：衍生同人

> 简介：漫威宇宙。林克：“科尔森是吧？我给你介绍个女朋友吧……她叫贞子，别看她披头散发的，其实超美的！”科尔森：“我能……拒绝么？她老想掐死我……”“得给卤蛋找点事做……不然整天就会暗地里搞监视！”感受着周围的特工，林克穿上黑袍，戴上头罩、眼罩，在黑暗中大声的喊了句：“九头蛇万岁！”看着四周原本自己神盾局的下属，现在的九头蛇，尼克弗瑞内心一阵mmp，但面容却异常庄重的喊道：九头蛇万岁！”钢铁侠：“你怎么提了只鸡？”林克：“不！它表面上是一只鸡，其实是哥斯拉！”钢铁侠：“谢特……你个乌鸦嘴离我远点……法克我刚装修好的大楼……”林克看着远处的灭霸，伸出了手掌：“别看我手上什么也没有，但只要我打起响指……啪！”

> 章节总数：共467章

> 状态：完本
# 论文
### [Towards optimized actions in critical situations of soccer games with deep reinforcement learning | Papers With Code](https://paperswithcode.com/paper/towards-optimized-actions-in-critical)
> 概要: Soccer is a sparse rewarding game: any smart or careless action in critical situations can change the result of the match. Therefore players, coaches, and scouts are all curious about the best action to be performed in critical situations, such as the times with a high probability of losing ball possession or scoring a goal.
### [Neural optimal feedback control with local learning rules | Papers With Code](https://paperswithcode.com/paper/neural-optimal-feedback-control-with-local)
> 概要: A major problem in motor control is understanding how the brain plans and executes proper movements in the face of delayed and noisy stimuli. A prominent framework for addressing such control problems is Optimal Feedback Control (OFC).