---
title: 2023-06-03-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SouthKaibabTrail_ZH-CN1186135534_1920x1080.webp&qlt=50
date: 2023-06-03 22:01:21
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SouthKaibabTrail_ZH-CN1186135534_1920x1080.webp&qlt=50)
# 新闻
### [解构业财融合：财务系统数字化架构](https://www.woshipm.com/pd/5839253.html)
> 概要: 在企业运营过程中，业务动作和财务密不可分。业务动作需要大量的资金支持，而财务则需要根据业务情况对资金以及后续的记账、报税等进行有效的管理。本文作者对财务系统数字化架构进行了分析，希望对你有帮助。一、业......
### [苹果直播不带货，百万人看了个寂寞](https://www.woshipm.com/it/5839904.html)
> 概要: 在小米、华为、VIVO等手机同行纷纷投入直播的时候，苹果的直播才姗姗来迟。并且在618的促销时期，苹果首次直播不卖货，只上课，但效果却不尽如人意。本文作者对此发表了自己的看法，与你分享。苹果开启全球首......
### [产品原型设计的思考过程](https://www.woshipm.com/pd/5839889.html)
> 概要: 本篇文章，作者将从五个步骤讲述如何做产品原型设计——以微信小程序为例。而做一个产品，不单单仅考虑产品的本身，还需考虑生活日常、市场环境等因素。接下来，让我们跟着作者，一起了解产品设计的流程吧~很多新手......
### [B站危险了？](https://www.huxiu.com/article/1629292.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国当爱奇艺、快手纷纷爬出亏损的泥潭，市场越发期待哔哩哔哩（下简称 B 站）盈亏平衡的捷报——然而，B 站刚递出的“成绩单”仍未达标，只靠大幅减亏勉强撑住面子......
### [美债务上限法案将于6月3日签署 道指狂飙700点](https://www.yicai.com/news/101773755.html)
> 概要: 受强劲非农就业数据及美国债务上限进展推动，美股集体飙高。
### [谢楠分享吴所谓得知吴京去开家长会的反应](https://ent.sina.com.cn/s/m/2023-06-03/doc-imyvykhm0953305.shtml)
> 概要: 新浪娱乐讯 6月2日，谢楠发文分享吴所谓日常，称“知道爸爸要参加家长会的吴所谓求生欲拉满，甚至还准备了零食贿赂老爹”。图中，吴所谓在课桌上画了一个牌子，写道“放过我吧爸爸！”。(责编：小万)......
### [早期项目丨服务80%头部数字人直播，「风平智能」基于AIGC智能化IP打造平台](https://36kr.com/p/2285342452439049)
> 概要: 早期项目丨服务80%头部数字人直播，「风平智能」基于AIGC智能化IP打造平台-36氪
### [银行最“流氓”的7个规定，每一个都不能忍](https://www.huxiu.com/article/1632801.html)
> 概要: 本文来自微信公众号：8号窗口（ID：gyjjinrong），作者：桂圆君，头图来自：《半泽直树2》剧照截图每个行业都会有一些规定，银行也不例外，规定是为了帮助企业提高生产效率，让员工和客户的权益得到保......
### [本周双碳大事：生态环境部称年内争取重启CCER；马斯克来华行程引关注；5月全国碳市场量价齐升](https://36kr.com/p/2284617412318977)
> 概要: 本周双碳大事：生态环境部称年内争取重启CCER；马斯克来华行程引关注；5月全国碳市场量价齐升-36氪
### [30年研究路 190篇论文 14项专利——走近“卷积杨立昆之父”杨立昆](https://www.yicai.com/video/101773704.html)
> 概要: 杨立昆为卷积神经网络和图像识别领域做出了重要贡献，以手写字体识别、图像压缩和人工智能硬件等主题发表过190多份论文，研发了很多关于深度学习的项目，并且拥有14项相关的美国专利。
### [球票刺客？堪比世界杯半决赛票价的友谊赛值吗](https://finance.sina.com.cn/jjxw/2023-06-03/doc-imyvyuwh7518247.shtml)
> 概要: 中新网北京6月3日电(记者 卞立群)世界杯冠军阿根廷2023年中国行票价于1日晚终于出炉，共分为580元、1380元、2800元、3200元、3800元、4800元六档。
### [eXtremeRate推出Steam Deck第三方全透明外壳](https://www.3dmgame.com/news/202306/3870563.html)
> 概要: 许多数字产品爱好者中，有许多都对透明外壳情有独钟，因为这样可以一览设备内部的各种零件，非常的有科技感。目前，游戏主机外设厂商 eXtremeRate 推出了Valve 热门掌上 PC Steam De......
### [苹果专利可设计屏下Face ID 无开孔真全面屏iPhone要来](https://www.3dmgame.com/news/202306/3870565.html)
> 概要: 从去年iPhone 14 Pro开始，苹果引入灵动岛挖孔，终于抛弃刘海屏。其实一直以来，苹果的终极目标就是打造一款完全无开孔的手机，包括按键、接口、听筒等等都完全隐藏，当然也包括刘海区域。为此，苹果已......
### [穿越评级风云，复星何以成为标普们眼中的“2%”？](https://36kr.com/p/2284579908425475)
> 概要: 穿越评级风云，复星何以成为标普们眼中的“2%”？-36氪
### [4个月卖1751.5亿元背后，彩票盯上年轻人？](https://finance.sina.com.cn/china/2023-06-03/doc-imyvyuwh7530487.shtml)
> 概要: 中新经纬6月3日电 (李自曼)“最近买的彩票都没中。”李明明(化名)已经记不清最近两年她买过多少彩票了。虽然很少中奖，但她仍持续买。
### [在产险业占比仅1.1%的家财险，如何重现“辉煌”？](https://www.yicai.com/news/101773826.html)
> 概要: 家财险未来发展方向将主要集中在产品升级、增值服务和渠道整合三大方面。
### [网传上海一新盘只需一成首付？记者去现场看了看](https://finance.sina.com.cn/jjxw/2023-06-03/doc-imyvyzec0632797.shtml)
> 概要: 转自：上海网络辟谣 近日，上海青浦一名为“安联·虹悦”的新盘打出了“今年买房 明年付款”的广告，有中介喊出“10%首付即可置业大虹桥”的宣传语。
### [比亚迪能否赚大钱，就看腾势了](https://www.huxiu.com/article/1631557.html)
> 概要: 出品丨虎嗅汽车组作者丨周到编辑丨张博文头图丨视觉中国刚刚过去的5月，比亚迪迎来了该公司自打成立以来的高光时刻：全月，比亚迪共卖出了24.02万辆新能源汽车，创造了单月销量的新高。与此同时，很多人也注意......
### [每年发生上万起事故，造成超1万人死亡，印度火车为何成“全世界最不安全”？](https://finance.sina.com.cn/china/2023-06-03/doc-imyvzmtx0439589.shtml)
> 概要: 印度铁路又出事了。 据印度媒体6月3日报道，当地时间6月2日晚，印度东部奥迪沙邦的列车发生严重的脱轨相撞。 截至发稿，这起印度本世纪最严重的铁路事故...
### [京东超市首次公布选品方法论 34款新品入选618最值得购买榜单](http://www.investorscn.com/2023/06/03/107914/)
> 概要: 京东618正如火如荼的进行中，6月2日，在京东超市618家用快消品趋势及新品发布会上，京东超市正式发布家用快消品行业首份618“趋势新品榜”和“热销爆品榜”，并首次公布京东超市选品方法论......
### [黄渤王一博《热烈》担任上海国际电影节闭幕电影](https://ent.sina.com.cn/m/c/2023-06-03/doc-imyvyzee7424719.shtml)
> 概要: 新浪娱乐讯 由大鹏执导，黄渤、王一博领衔主演电影《热烈》官宣将作为第二十五届上海国际电影节闭幕影片放映，并发布“追梦版”预告与“炙热版”海报。　　影片讲述了街舞老炮儿丁雷（黄渤 饰），遇到了心怀热爱、......
### [《叠叠高城》Steam EA发售 综合评价“好评”](https://www.3dmgame.com/news/202306/3870577.html)
> 概要: 今日（6月3日），由土耳其独立研发团队Remoob开发的策略建造游戏《叠叠高城（Pile Up!）》Steam EA发售，支持简体中文，Steam综合评价“好评”，国区原价42元，感兴趣的玩家可以点击......
### [《网络奇兵：重制版》获IGN 9分好评：经典再现](https://www.3dmgame.com/news/202306/3870578.html)
> 概要: IGN近日针对最近发售的《网络奇兵：重制版》发表评测文章，并给出了9分的高评价。在评测中，IGN编辑表示：很多现代游戏里，玩家都是可以放松下来尽情享受的。但《网络奇兵：重制版》则希望玩家绷紧神经，亲身......
### [折叠手机618销量翻倍增长 今年国内预计卖五六百万台](https://www.yicai.com/news/101773538.html)
> 概要: 折叠手机大规模下降到三四千元，可能要到2025年，今年和明年都会有难度。
### [A股：否极泰来？丨第六交易日](https://www.yicai.com/video/101773921.html)
> 概要: A股：否极泰来？丨第六交易日
### [9家消费公司拿到新钱，交个朋友京东直播首秀战绩1.5亿元，618预售黑马品牌揭晓｜创投大视野](https://36kr.com/p/2285727400843271)
> 概要: 9家消费公司拿到新钱，交个朋友京东直播首秀战绩1.5亿元，618预售黑马品牌揭晓｜创投大视野-36氪
### [找工作，直接和ChatGPT谈](https://www.huxiu.com/article/1634308.html)
> 概要: 本文来自微信公众号：爱范儿（ID：ifanr），作者：张成晨，题图来自：视觉中国找工作，是人与社会的一次交锋。你需要清楚自己的优势，从字里行间解读公司的诉求，掂量能力和经验与岗位匹不匹配，还有无数的经......
### [黄渤回应节目中资助贫困女孩：给孩子安静的空间](https://ent.sina.com.cn/s/m/2023-06-03/doc-imyvzmty7205561.shtml)
> 概要: 新浪娱乐讯 6月3日，黄渤回应节目中资助贫困女孩，“对于孩子家庭存在的一些误解，相信每个人都是因为抱着善意而关注，但也希望大家在她紧张的学习阶段给孩子一个安静的空间。对于小媛丽的明天充满期待！”　　据......
### [《暗黑4》总监：这是我们讲过的最黑暗的故事](https://www.3dmgame.com/news/202306/3870583.html)
> 概要: 近日《暗黑4》游戏总监Joe Shely和资深游戏制作人Paul Lee接受了媒体的采访，Shely确认《暗黑4》是暴雪暗黑团队所讲过的最黑暗的故事，他们想在《暗黑破坏神4》呈现圣休亚瑞的人类希望自己......
### [黄渤回应贫困女孩争议！知情人透露孩子生活很好，田径成绩第一名](https://new.qq.com/rain/a/20230603A082L500)
> 概要: 黄渤回应贫困女孩争议！知情人透露孩子生活很好，田径成绩第一名
### [美债务上限协议通过！全球资产影响几何？](https://finance.sina.com.cn/china/2023-06-03/doc-imyvzmtx0438875.shtml)
> 概要: 中国基金报 记者赵心怡叶诗婕 美国债务上限危机暂时“告一段落”——美东时间5月31日，美国国会众议院通过了一项关于联邦政府债务上限和预算的法案，隔日参议院通过...
### [6月3日，黄渤回应贫困女孩争议：对孩子的家庭存在一些误解。女孩父母：一家人很疼爱女儿，节目和资助无关。](https://new.qq.com/rain/a/20230603V0835D00)
> 概要: 6月3日，黄渤回应贫困女孩争议：对孩子的家庭存在一些误解。女孩父母：一家人很疼爱女儿，节目和资助无关。
### [熟年：老大一家太压抑，倪俊颓废，吴二琥闹笑话，倪伟民太扎心](https://new.qq.com/rain/a/20230603A085FO00)
> 概要: 熟年：老大一家太压抑，倪俊颓废，吴二琥闹笑话，倪伟民太扎心
### [后浪：师父前去抗疫 赵露思倍感担忧](https://new.qq.com/rain/a/20230603V086WM00)
> 概要: 后浪：师父前去抗疫 赵露思倍感担忧
### [《护心》甜回来了，糖分升级，剧情加持下重回热度？](https://new.qq.com/rain/a/20230603A087JF00)
> 概要: 《护心》甜回来了，糖分升级，剧情加持下重回热度？
# 小说
### [双生旧忆花开半夏](http://book.zongheng.com/book/1171741.html)
> 作者：源梦雪

> 标签：奇幻玄幻

> 简介：盛夏流年的年华，是我对你的甜忆，在热闹的大街，街上漫布的人烟，你的身影闯入到我的视线。时间定格，行人停住，我走到你的眼前，凝视，说声好久不见，右眼的泪珠从脸上滑落。你笑了，说好久不见！

> 章节末：终：不过是大梦一场空

> 状态：完本
# 论文
### [Feature Distillation Interaction Weighting Network for Lightweight Image Super-Resolution | Papers With Code](https://paperswithcode.com/paper/feature-distillation-interaction-weighting)
> 概要: Convolutional neural networks based single-image super-resolution (SISR) has made great progress in recent years. However, it is difficult to apply these methods to real-world scenarios due to the computational and memory cost.
### [PLSSVM: A (multi-)GPGPU-accelerated Least Squares Support Vector Machine | Papers With Code](https://paperswithcode.com/paper/plssvm-a-multi-gpgpu-accelerated-least)
> 概要: Machine learning algorithms must be able to efficiently cope with massive data sets. Therefore, they have to scale well on any modern system and be able to exploit the computing power of accelerators independent of their vendor. In the field of supervised learning, Support Vector Machines (SVMs) are widely used. However, even modern and optimized implementations such as LIBSVM or ThunderSVM do not scale well for large non-trivial dense data sets on cutting-edge hardware: Most SVM implementations are based on Sequential Minimal Optimization, an optimized though inherent sequential algorithm. Hence, they are not well-suited for highly parallel GPUs. Furthermore, we are not aware of a performance portable implementation that supports CPUs and GPUs from different vendors. We have developed the PLSSVM library to solve both issues. First, we resort to the formulation of the SVM as a least squares problem. Training an SVM then boils down to solving a system of linear equations for which highly parallel algorithms are known. Second, we provide a hardware independent yet efficient implementation: PLSSVM uses different interchangeable backends--OpenMP, CUDA, OpenCL, SYCL--supporting modern hardware from various vendors like NVIDIA, AMD, or Intel on multiple GPUs. PLSSVM can be used as a drop-in replacement for LIBSVM. We observe a speedup on CPUs of up to 10 compared to LIBSVM and on GPUs of up to 14 compared to ThunderSVM. Our implementation scales on many-core CPUs with a parallel speedup of 74.7 on up to 256 CPU threads and on multiple GPUs with a parallel speedup of 3.71 on four GPUs. The code, utility scripts, and the documentation are available on GitHub: https://github.com/SC-SGS/PLSSVM.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
