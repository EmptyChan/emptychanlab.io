---
title: 2023-02-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HippoDayChobe_ZH-CN2883647954_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-15 22:51:12
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HippoDayChobe_ZH-CN2883647954_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [2亿手机熬夜党，隐秘在夜色中的B面人生](https://www.woshipm.com/user-research/5754259.html)
> 概要: 现代人晚睡都出于哪些原因？可能大多数人都是因为手机，比如看短视频、玩游戏、刷购物软件……一不小心，用户的时间就被手机刷走了。那么，这些晚睡、放不下手机的人，背后都有哪些故事呢？一起来看看本文的故事自述......
### [你还没脱单，这些软件都得背锅](https://www.woshipm.com/it/5754779.html)
> 概要: 各位单身的朋友们，为了脱单，你做出过哪些努力？相亲，又或者交友婚恋App？如今的社交App已经变得相当多元，可以满足各类人群的各项需求，但投诉和营销推广也到处可见，用户对此是怎么看的，还会继续使用这类......
### [当新能源车也搞“付费订阅”：方向盘加热包、后轮转向、辅助驾驶包……](https://www.woshipm.com/evaluating/5754248.html)
> 概要: 现在大多数产品都有和“付费订阅”相关的功能服务，比如长视频平台，比如近段时间引发大量讨论的智能电视，再到现在越来越多人使用的新能源汽车。企业可能想通过“付费订阅”获得更多收入，但是，什么样的“付费订阅......
### [想在印度复刻“中国制造”，苹果公司困难重重](https://finance.sina.com.cn/jjxw/2023-02-15/doc-imyftpra9101490.shtml)
> 概要: 来源：环球时报......
### [美国毒气泄露，严重到什么程度？](https://www.huxiu.com/article/794608.html)
> 概要: 本文来自微信公众号：地球知识局 （ID：diqiuzhishiju），作者：无梦，题图来自：视觉中国伴随着滚滚的黑烟和翻腾的火光，大量致命的有害物质正从事故发生地不断地泄漏到周围的环境中，并乘着大气环......
### [36氪首发｜功能性咖啡品牌「奢啡CEPHEI」完成数千万融资，由金沙江创投投资](https://36kr.com/p/2115986061969798)
> 概要: 36氪首发｜功能性咖啡品牌「奢啡CEPHEI」完成数千万融资，由金沙江创投投资-36氪
### [36氪首发 | 搭建肿瘤介入治疗平台，海杰亚医疗获数亿元D轮融资](https://36kr.com/p/2132332896414976)
> 概要: 36氪首发 | 搭建肿瘤介入治疗平台，海杰亚医疗获数亿元D轮融资-36氪
### [海信换帅：五大挑战，摆在51岁贾少谦面前](https://finance.sina.com.cn/tech/it/2023-02-15/doc-imyfttwv2553895.shtml)
> 概要: 文 | 新浪科技 周文猛......
### [卫健委正式受理NMN作为食品添加剂  政策利好金达威等相关企业](http://www.investorscn.com/2023/02/15/105745/)
> 概要: 近日，1月28日，国家卫生健康委员会政务服务平台官网发布卫食添新申字(2023)第0022号公告，正式受理NMN作为食品添加剂新品种。这表明NMN的价值及作用逐渐被认可，NMN市场未来的发展将会迎来更......
### [赛力斯发布公告：华为研发人员撤离公司不属实 将深化合作推新产品](http://www.investorscn.com/2023/02/15/105750/)
> 概要: 2月14日晚,赛力斯发布公告回应关于华为研发人员撤离及公司与华为关系改变的不实传言......
### [浦泽直树x手塚治虫《PLUTO》动画PV公开 画面超爽](https://acg.gamersky.com/news/202302/1566566.shtml)
> 概要: 手冢治虫、浦泽直树原作漫画《PLUTO～冥王～》同名动画公开先导PV及视觉图公开，该作将于2023年开始在Netflix全球独家播出。
### [浦泽直树×手冢治虫《PLUTO》动画化决定](https://news.dmzj.com/article/77149.html)
> 概要: 由浦泽直树创作的《PLUTO》宣布了动画化决定的消息。本作将于2023年年内在Netflix平台播出。
### [植发也是“一口价” 雍禾植发情人节调价打出透明诚信牌](http://www.investorscn.com/2023/02/15/105752/)
> 概要: 2023年2月14日，毛发医疗行业龙头雍禾植发在全国所有雍禾植发及发之初院部进行调价。此次主动调价，雍禾植发在医生分级诊疗体系的基础上以一口价的方式向消费者提供更透明、简单的选择......
### [助力乡村建设，辛巴辛有志以直播带货赢得更多可能](http://www.investorscn.com/2023/02/15/105754/)
> 概要: 助农作为一项惠民利民工程，一直受到社会各界的关注，科技助农、电商助农等活动在中国各地纷纷开展。各行业头部力量联合政府部门通过组织农业领域的专家、技术人员等，为农户、乡村中小微企业提供技术帮扶指导，部分......
### [P站美图推荐——过膝靴特辑（二）](https://news.dmzj.com/article/77150.html)
> 概要: 同样凸显绝对领域，靴子的质感又赋予过膝靴另一种与过膝袜不尽相同的美感，好哇。
### [中国式相亲2蹭名气对外宣传 中国式家长发声明澄清](https://www.3dmgame.com/news/202302/3862733.html)
> 概要: 《中国式家长》开发商墨鱼玩游戏工作室昨天晚上发布声明，澄清《中国式相亲2》并非该团队制作人作品，前主美杨葛一郎很早就离开了团队，但其多次未经同意以《中国式家长》制作人名义对外宣传，为自己的波浪科技谋取......
### [第一批00后已经离婚](https://www.huxiu.com/article/795013.html)
> 概要: 本文来自微信公众号：真实故事计划 （ID：zhenshigushi1），作者：陆成玉，编辑：温丽虹，头图来自：《不期而至》千禧一代已经逐步进入法律规定的适婚年龄，走入古老的婚姻制度。与此同时，第一批0......
### [TV动画《伊甸星原》第二季公开PV](https://news.dmzj.com/article/77153.html)
> 概要: 根据真岛浩原作制作的TV动画《伊甸星原》宣布了第二季将于4月1日开始播出的消息。本作的PV也一并公开。在这次的PV中，可以看到主人公们与强敌苦战的场景。
### [求购Space X、Neuralink老股；转让持有Shein、某头部自动驾驶公司的基金份额｜资情留言板第81期](https://36kr.com/p/2132613012925446)
> 概要: 求购Space X、Neuralink老股；转让持有Shein、某头部自动驾驶公司的基金份额｜资情留言板第81期-36氪
### [【士气集团】TUKKI 潮玩设计](https://www.zcool.com.cn/work/ZNjQwMDYwNjQ=.html)
> 概要: 有幸加入了士气集团，创作了这组以扑克牌花色为世界观的角色设定非常感谢@小田仙人@林超黑Tt@非常严肃的Joker@拾伍小弟四位老师的悉心指导新的一年保持热爱！希望大家喜欢......
### [传奇歌手刘文正病逝 曾培养伊能静巫启贤等艺人](https://ent.sina.com.cn/s/h/2023-02-15/doc-imyfukut6254548.shtml)
> 概要: 新浪娱乐讯 据报道，70年代巨星刘文正惊传病逝美国，他的经纪人夏玉顺刚才证实消息，据知他是在去年生日前后11月12日因心肌梗塞病逝于美国赌城拉斯维加斯，享年70岁。　　刘文正是台湾男歌手、演员、主持人......
### [微众银行携手创业黑马举办“2023氢能项目专场投融资对接会”助力氢能企业发展](http://www.investorscn.com/2023/02/15/105759/)
> 概要: 氢能是未来国家能源体系的重要组成部分，是国家“新基建”的重点产业之一。然而，当前其推广及应用仍存一定痛点，为攻坚运输、制氢方式等行业难题，需要氢能企业持续以创新为核、驱动技术革新，以企业高质量发展带动......
### [《最后的生还者》片场趣闻：剧组成员不许提“僵尸”](https://www.3dmgame.com/news/202302/3862763.html)
> 概要: 对于那些不熟悉游戏的美剧观众，《最后的生还者》往往会被拿来和《行尸走肉》作比较，因为它看起来就是一部“僵尸末世”题材电视剧。然而事实上，剧组摄影师 Eben Bolter 最近分享称，“僵尸”一词已经......
### [GSC《关于我在无意间被隔壁的天使变成废柴这件事》粘土人](https://news.dmzj.com/article/77157.html)
> 概要: GSC根据《关于我在无意间被隔壁的天使变成废柴这件事》中的椎名真昼制作的粘土人正在预订中。本作有“笑颜”、“冷淡颜”和“害羞颜”三种表情，另外还有“蛋包饭”、“雨伞”和“玩偶”配件。
### [《海贼王》1075话情报：死亡游戏！山治又玩疯了](https://acg.gamersky.com/news/202302/1566636.shtml)
> 概要: 《海贼王》1075话情报公开，本集中路飞他们和贝加庞克的克隆体们分成了四组，开始了搜索。另一边，面对炽天使的突然攻击，路奇和路飞索隆说他们应该暂时一起战斗。
### [视频：黄多多首次担任导演策划舞台剧 穿白西装谢幕大气有范](https://video.sina.com.cn/p/ent/2023-02-15/detail-imyfukut6266566.d.html)
> 概要: 视频：黄多多首次担任导演策划舞台剧 穿白西装谢幕大气有范
### [丰年资本投资高速连接器国产替代领军企业「西点精工」](https://36kr.com/p/2132891280845832)
> 概要: 丰年资本投资高速连接器国产替代领军企业「西点精工」-36氪
### [苹果在印度生产iPhone14结果坑惨 和中国制造差距大](https://www.3dmgame.com/news/202302/3862782.html)
> 概要: 随着苹果公司近年来不断加速推进产业链的调整，印度在许多人眼中无疑正成为一个有着巨大吸引力的潜在选择，但想要摆脱中国制造，属实还有点困难。去年，在中国生产的旗舰版iPhone 14上市几周后，印度供应商......
### [老后破产：当长寿变成穷人的诅咒](https://www.huxiu.com/article/795464.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 、题图 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。“延迟退休”四......
### [琼瑶称两天前得知刘文正过世 深表遗憾与不舍](https://ent.sina.com.cn/s/h/2023-02-15/doc-imyfuram2110745.shtml)
> 概要: 新浪娱乐讯 据媒体报道，70年代巨星刘文正惊传病逝美国，他的经纪人夏玉顺15日证实消息，指他是在去年生日前后11月12日因心肌梗塞病逝于美国赌城拉斯维加斯，享寿70岁。但消息传出之后有媒体称他新加坡的......
### [1月外汇市场喜提“双顺差”，外资净买入境内股票创历史新高](https://www.yicai.com/news/101675883.html)
> 概要: 反映我国资本市场对外资吸引力增强。
### [我，未婚单身，情人节花钱看别人化约会妆](https://www.huxiu.com/article/795603.html)
> 概要: 出品｜虎嗅商业消费组作者｜赵烨楠编辑｜苗正卿头图｜视觉中国2月14号，趁着情人节的档口，我来到北京某美妆工作室，体验了一套完整的约会妆发。虽然我并没有约会。最初对化妆室感兴趣，是看到K-pop产业里，......
### [中办、国办发文多举措整治财会违法，将严打这些企业财务造假](https://www.yicai.com/news/101675922.html)
> 概要: 加强对中介机构执业质量监督。
### [DITA 新款旗舰 Perpetua C 耳机及 Celeste Kondo 纯银耳机线上市：售价 4 万元、2.1 万元](https://www.ithome.com/0/673/624.htm)
> 概要: 感谢IT之家网友机智的BLACK的线索投递！IT之家2 月 15 日消息，DITA 宣布新款旗舰特别版 Perpetua C 耳机及 Celeste Kondo 纯银耳机线上市。Perpetua C ......
### [央行：精准加强重点领域和薄弱环节金融支持](https://finance.sina.com.cn/china/2023-02-15/doc-imyfuzsi2269930.shtml)
> 概要: 记者从中国人民银行获悉，人民银行将做好政策性开发性金融工具、设备更新改造专项再贷款等稳经济大盘政策工具存续期管理，完善支持普惠小微、绿色发展...
### [证监会重申券商跨境展业整改原则，明确“不会无故限制存量客户交易”](https://www.yicai.com/news/101675962.html)
> 概要: “不会无故限制存量客户交易”
### [欧洲停售燃油车，将如何惠及正在“出海”的A股产业链？](https://www.yicai.com/news/101675907.html)
> 概要: 2021年以来，欧洲占据我国新能源汽车出口规模最高份额。
### [全国春运结束 客流量预计近16亿人次](https://finance.sina.com.cn/china/2023-02-15/doc-imyfuvkn9115101.shtml)
> 概要: 央视网消息（新闻联播）：2023年春运今天（2月15日）结束，作为疫情防控进入新阶段的第一个春运，跨区域人员流动明显增加。国务院联防联控机制春运工作专班数字显示...
### [《大富翁 11》现已支持 Steam Deck 游戏掌机](https://www.ithome.com/0/673/628.htm)
> 概要: IT之家2 月 15 日消息，《大富翁 11》去年 10 月上线 Steam 平台与 Switch 平台，Steam 版售价 66 元人民币，Switch 版售价 129 港币（约 119 元人民币）......
### [央行：精准加强重点领域和薄弱环节金融支持](https://finance.sina.com.cn/jjxw/2023-02-15/doc-imyfuvkn9132525.shtml)
> 概要: 新华社北京2月15日电（记者吴雨）记者15日从中国人民银行获悉，人民银行将做好政策性开发性金融工具、设备更新改造专项再贷款等稳经济大盘政策工具存续期管理...
### [女子试驾蔚来致一死一伤被刑拘，蔚来：等警方调查；顺丰回应爱马仕包运输中被烧毁](https://www.yicai.com/news/101676029.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [沙特继续增持任天堂 目前已拥有7%的股份](https://www.3dmgame.com/news/202302/3862786.html)
> 概要: 据外媒消息，沙特阿拉伯增加了对任天堂的持股，这已经是一个月内第二次增持。据Trading View报道，沙特王储Mohammed bin Salman领导的主权基金——公共投资基金(PIF)已将其在任......
### [国家医保局通知：进一步做好定点零售药店纳入门诊统筹管理](https://finance.sina.com.cn/jjxw/2023-02-15/doc-imyfuzsk9049295.shtml)
> 概要: 国家医疗保障局办公室关于进一步做好定点零售药店纳入门诊统筹管理的通知
### [联想小新 2023 锐龙版笔记本将于明晚发布，最高搭载 R7 7735HS 处理器](https://www.ithome.com/0/673/640.htm)
> 概要: IT之家2 月 15 日消息，联想于今日晚间举行了联想小新生态新品春季发布会，推出了 Pro / Air / 数字系列小新笔记本酷睿版新品，以及多款一体机、配件产品。根据联想官方最新消息，小新 202......
### [卧谈会什么时候你会突然觉得“自己已经赶不上这个时代了”](https://bbs.hupu.com/58009593.html)
> 概要: 感觉自己out了！什么时候你会突然觉得“自己已经赶不上这个时代了”
### [OpenAI安抚外界担忧：ChatGPT不是人类的就业杀手](https://www.3dmgame.com/news/202302/3862787.html)
> 概要: 据报道，OpenAI CEO萨姆·奥特曼今日安抚外界的担忧，称人工智能聊天机器人ChatGPT不会成为人类的“就业杀手”。ChatGPT是由OpenAI研发的一款人工智能聊天机器人，一经推出就迅速成为......
### [美特斯邦威清仓：卫衣 / T 恤 / Polo 衫 49 元起（27 款任选）](https://lapin.ithome.com/html/digi/673645.htm)
> 概要: 【美特斯邦威官方 outlets 店】断码清仓：美特斯邦威男女卫衣 / T 恤 / Polo 衫报价 179-199 元，限时限量 130 元券，实付 49-69 元包邮天猫美特斯邦威男女卫衣 / T......
### [重要消息！中办 国办最新印发！信息量巨大→](https://finance.sina.com.cn/china/2023-02-15/doc-imyfuzsk9080389.shtml)
> 概要: 来源：央视财经 近日，中共中央办公厅、国务院办公厅印发了《关于进一步加强财会监督工作的意见》，并发出通知，要求各地区各部门结合实际认真贯彻落实。
### [流言板76人等一些球队截止日前有意德拉蒙德，但公牛决定让他留队](https://bbs.hupu.com/58010944.html)
> 概要: 虎扑02月15日讯 根据记者Marc Stein的报道，在交易截止日前，76人和其他一些球队有意公牛球员安德烈-德拉蒙德，但公牛最终决定让德拉蒙德留队。本赛季，德拉蒙德打了44场比赛，场均可以得到6.
### [流言板米尔斯：我们有非常可靠的球员，这毫无疑问是支季后赛球队](https://bbs.hupu.com/58011012.html)
> 概要: 虎扑02月15日讯 近日，篮网球衣帕蒂-米尔斯接受了记者采访，谈到球队新阵容时，米尔斯表示：“这是一个明显的变化，乐观地看着我们现在的阵容，我们有一个很棒的更衣室。一群特别好的球员，他们都在之前的球队
### [流言板罗马诺：药厂中卫因卡皮耶新约无解约金，热刺等队夏窗有意](https://bbs.hupu.com/58011022.html)
> 概要: 虎扑02月15日讯 在2月12日，勒沃库森官方宣布与队内21岁中卫因卡皮耶续约至2027年。而根据罗马诺的报道，因卡皮耶签下的新合同中没有解约金条款，包括热刺在内的许多俱乐部都有意在夏窗签下他，不过届
# 小说
### [万世为王](https://book.zongheng.com/book/796075.html)
> 作者：贪睡的龙

> 标签：奇幻玄幻

> 简介：再生少年时，重走修行路，既如此，此生当无敌，诸天千域，千秋万世，我为至尊神王！微信公众号：贪睡的龙

> 章节末：第2000章 有趣的事（大结局）

> 状态：完本
# 论文
### [Monocular Depth Estimation Using Cues Inspired by Biological Vision Systems | Papers With Code](https://paperswithcode.com/paper/monocular-depth-estimation-using-cues)
> 概要: Monocular depth estimation (MDE) aims to transform an RGB image of a scene into a pixelwise depth map from the same camera view. It is fundamentally ill-posed due to missing information: any single image can have been taken from many possible 3D scenes. Part of the MDE task is, therefore, to learn which visual cues in the image can be used for depth estimation, and how. With training data limited by cost of annotation or network capacity limited by computational power, this is challenging. In this work we demonstrate that explicitly injecting visual cue information into the model is beneficial for depth estimation. Following research into biological vision systems, we focus on semantic information and prior knowledge of object sizes and their relations, to emulate the biological cues of relative size, familiar size, and absolute size. We use state-of-the-art semantic and instance segmentation models to provide external information, and exploit language embeddings to encode relational information between classes. We also provide a prior on the average real-world size of objects. This external information overcomes the limitation in data availability, and ensures that the limited capacity of a given network is focused on known-helpful cues, therefore improving performance. We experimentally validate our hypothesis and evaluate the proposed model on the widely used NYUD2 indoor depth estimation benchmark. The results show improvements in depth prediction when the semantic information, size prior and instance size are explicitly provided along with the RGB images, and our method can be easily adapted to any depth estimation system.
### [Model Criticism for Long-Form Text Generation | Papers With Code](https://paperswithcode.com/paper/model-criticism-for-long-form-text-generation)
> 日期：16 Oct 2022

> 标签：None

> 代码：https://github.com/da03/criticize_text_generation

> 描述：Language models have demonstrated the ability to generate highly fluent text; however, it remains unclear whether their output retains coherent high-level structure (e.g., story progression). Here, we propose to apply a statistical tool, model criticism in latent space, to evaluate the high-level structure of the generated text. Model criticism compares the distributions between real and generated data in a latent space obtained according to an assumptive generative process. Different generative processes identify specific failure modes of the underlying model. We perform experiments on three representative aspects of high-level discourse -- coherence, coreference, and topicality -- and find that transformer-based language models are able to capture topical structures but have a harder time maintaining structural coherence or modeling coreference.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
