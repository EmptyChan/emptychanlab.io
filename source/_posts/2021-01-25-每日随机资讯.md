---
title: 2021-01-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MangroveForest_EN-CN1485397789_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-25 21:48:44
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MangroveForest_EN-CN1485397789_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [高管套现200亿、高瓴赚了100亿，这家公司还有“油水”吗？](https://www.huxiu.com/article/405920.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年1月4日，隆基股份（601012.SH）公告披露，李春安（实控人的一致行动人）在2020年12月7日～31日期间，通过集中竞价减持3771.7万股，套现3......
### [村上春树原作电影《驾驶我的车》新角色 今夏上映](https://www.3dmgame.com/news/202101/3807029.html)
> 概要: 根据著名作家村上春树短篇小说集《没有女人的男人们》中的作品《驾驶我的车》改编的同名电影官方日前公开了新角色，本片预定今夏上映，敬请期待。·《驾驶我的车》故事讲述舞台剧演员家福（60岁左右）明知同为演员......
### [索尼可能正为PS5开发《对马岛之鬼》续作](https://www.3dmgame.com/news/202101/3807030.html)
> 概要: 虽然索尼仅仅半年前发售了《对马岛之鬼》，但开发商Sucker Punch工作室一名开发者的领英简历可能暗示Sucker Punch可能已经在开发一个续作了。Dave Molloy是Sucker Pun......
### [组图：刘亦菲清纯旧照曝光 阴雨天撑伞捧书文艺范儿十足](http://slide.ent.sina.com.cn/star/slide_4_704_351687.html)
> 概要: 组图：刘亦菲清纯旧照曝光 阴雨天撑伞捧书文艺范儿十足
### [久保由利香5周年纪念单曲「君なら君しか」试听片段公开](http://acg.178.com//202101/405542724752.html)
> 概要: 近日，声优歌手久保由利香公开了5周年纪念单曲「君なら君しか」的试听片段。「君なら君しか」试听片段本专辑包含的单曲分别为「君なら君しか」和「Lovely Lovely Strawberry」。专辑售价为......
### [视频：拒绝拍吻戏？昆凌还表示希望再生一个孩子](https://video.sina.com.cn/p/ent/2021-01-25/detail-ikftpnny1520430.d.html)
> 概要: 视频：拒绝拍吻戏？昆凌还表示希望再生一个孩子
### [《星期一的丰满》：塑身不能等 马上开始锻炼](https://acg.gamersky.com/news/202101/1357643.shtml)
> 概要: 现在《星期一的丰满》的故事行越来越连贯了，本周上班族迷糊妹子开始了她的锻炼之旅。为了能够快速让身体恢复到之前的状况，她已经开始锻炼了。
### [动画电影「新神榜：哪吒重生」发布粤语版预告](http://acg.178.com//202101/405552668926.html)
> 概要: 动画电影「新神榜：哪吒重生」今日（1月25日）发布了粤语版预告，并公布电影粤语版与国语版将于大年初一（2月12日）同步上映。据悉，粤语版开场段落还有对电影「功夫」的致敬。「新神榜：哪吒重生」粤语版预告......
### [「熊出没·狂野大陆」公布方言版预告](http://acg.178.com//202101/405552801921.html)
> 概要: 动画电影「熊出没·狂野大陆」今日（1月25日）公布了一支方言版预告，其中包括河南话、四川话、粤语三种方言版本，方言版电影将与普通话版本在大年初一（2月12日）一起上映。「熊出没·狂野大陆」方言版预告......
### [吃出来的盛世元年——2020消费投资复盘](https://www.huxiu.com/article/402666.html)
> 概要: 出品｜虎嗅大商业组作者｜房煜  虎嗅主笔吃吃喝喝，本来就是人生常态，不过在消费品投资领域，过去这一直是冷门的存在。不过，在2020年，形势发生了变化。越来越多的消费品牌成为投资人眼中的香馍馍，融资金额......
### [想成功改变，你需要知道这件事](https://www.tuicool.com/articles/AJFVner)
> 概要: 编辑导语：flag不停地在立，然而也不停地在倒，为什么我们很难实现自己信誓旦旦立下的flag呢？本篇文章中，作者通过列举了一些案例，为我们分享了一个转换行动模式的神奇句式“当何时何地就会做何事”，以及......
### [野蛮生长的闲鱼，成了谁的财富密码？](https://www.tuicool.com/articles/EJFjmmm)
> 概要: ​本文来自微信公众号：新熵（ID:baoliaohui），作者：王鑫鑫编辑：潮声“也就是不让买卖人口，不然郑爽能把孩子挂闲鱼上卖。”风口浪尖上的郑爽凭一己之力完成了微博、知乎、豆瓣等大瓜搬运地的年底K......
### [Philosophy of Mathematics (SEP)](https://plato.stanford.edu/entries/philosophy-mathematics/)
> 概要: Philosophy of Mathematics (SEP)
### [芯片竞争加剧：三星砸千亿在美建厂，死磕台积电](https://www.ithome.com/0/532/042.htm)
> 概要: 1 月 25 日消息，据华尔街日报报道，三星将投资 170 亿美元（折合约 1101 亿人民币）在美建芯片代工厂，其地址可能位于美国亚利桑那州、德克萨斯州或者纽约。这一举措可能与台积电之前在美国亚利桑......
### [她是《浪姐2》里的隐形富婆，满身高奢镶钻珠宝，身价比整个公司还高？](https://new.qq.com/omn/20210125/20210125A0BFB500.html)
> 概要: 最近《乘风破浪的姐姐》第二季开始了，作为曾经在第一部的开播和收官都掀起过不少风浪的综艺，第二部自然也不甘落后。节目开始姐姐们的见面社交会就足够出一篇论文，到了各自舞台表演的时候除了专业歌手，基本都是十......
### [组图：沈梦辰一家穿白衣拍全家福 沈妈妈戴礼帽墨镜年轻状态好](http://slide.ent.sina.com.cn/star/w/slide_4_86512_351702.html)
> 概要: 组图：沈梦辰一家穿白衣拍全家福 沈妈妈戴礼帽墨镜年轻状态好
### [SpaceX 一箭 143 星「太空拼车」破人类航天纪录，一箭多星技术大起底](https://www.ithome.com/0/532/065.htm)
> 概要: 就在昨夜，SpaceX 又一次创造了历史！北京时间 2021 年 1 月 24 日 23 时，美国佛罗里达卡纳维拉尔角发射基地，SpaceX 的猎鹰 9 号火箭携 143 颗卫星顺利发射升空。约 9 ......
### [组图：2020最美表演对话佘诗曼 闭眼洗澡体验角色](http://slide.ent.sina.com.cn/star/slide_4_86512_351703.html)
> 概要: 组图：2020最美表演对话佘诗曼 闭眼洗澡体验角色
### [3DM速报：《赛博朋克2077》新补丁带来新BUG](https://www.3dmgame.com/news/202101/3807070.html)
> 概要: 欢迎来到今日的三大妈速报三分钟带你了解游戏业最新资讯大家好，我是米瑟《赛博朋克2077》的新补丁出了，新Bug们也是；Steam周销榜：“给她爱”，你可终于没了！《塞尔达无双：灾厄启示录》出货量破35......
### [三星将在德州投资100亿美元建3nm晶圆厂 追赶台积电](https://www.3dmgame.com/news/202101/3807071.html)
> 概要: 目前三星正在使用5nm EUV工艺进行生产，希望在先进工艺上追上台积电。据了解，三星将完全跳过4nm工艺节点，直接到3nm。要实现这样的计划，需要更大规模的投资。据报道，三星计划在德克萨斯州奥斯汀建设......
### [当下“热度最高”的剧：《山海情》霸榜多日，《有翡》让人意外](https://new.qq.com/omn/20210125/20210125A0DK8W00.html)
> 概要: 当下“热度最高”的剧：《山海情》霸榜多日，《有翡》让人意外最近一段时间新剧开播的数量非常之多，像开播已久的《暗恋橘生淮南》、《我就是这般女子》等剧集，吸引了不少网友的眼球，同时也有刚开播的新剧，像《正......
### [李香琴设灵，刘德华周星驰周润发送花牌，干儿女苗侨伟曾华倩黄日华到场送别](https://new.qq.com/omn/20210125/20210125A05SG600.html)
> 概要: 香港资深艺人李香琴于今年1月4日因病去世，享年88岁。            1月25日，李香琴家人在红磡世界殡仪馆为琴姐设灵，琴姐女儿萧芷筠到场为亡母打点一切。            萧芷筠与媒体打......
### [It’s always windy somewhere: Balancing renewable energy in Europe (2017)](https://arstechnica.com/science/2017/07/its-always-windy-somewhere-balancing-renewable-energy-in-europe/)
> 概要: It’s always windy somewhere: Balancing renewable energy in Europe (2017)
### [习近平：你输我赢、赢者通吃不是中国人的处世哲学](https://finance.sina.com.cn/china/gncj/2021-01-25/doc-ikftssap0700080.shtml)
> 概要: #领航中国##达沃斯议程对话会#【习近平：你输我赢、赢者通吃不是中国人的处世哲学】国家主席习近平25日晚在北京以视频方式出席世界经济论坛“达沃斯议程”对话会并发表特别致...
### [华晨宇一波三折的当爹路，背后尽显人性，真相细思恐极……](https://new.qq.com/omn/20210125/20210125A0EF3T00.html)
> 概要: 姐妹们大家好，今天的所长肚子里没有一粒米，全是瓜！谁叫春天还没到，瓜田就已经热闹起来了咧～            你以为的小说桥段其实是一段绝妙的公关案例，深情奶爸很可能脚踏两条船，就问你绝不绝？那是......
### [习近平：你输我赢、赢者通吃不是中国人的处世哲学](https://finance.sina.com.cn/china/gncj/2021-01-25/doc-ikftpnny1685148.shtml)
> 概要: 习近平：你输我赢、赢者通吃不是中国人的处世哲学 国家主席习近平25日晚在北京以视频方式出席世界经济论坛“达沃斯议程”对话会并发表特别致辞。
### [习近平：对抗将把人类引入死胡同](https://finance.sina.com.cn/china/2021-01-25/doc-ikftssap0700531.shtml)
> 概要: “多边主义的要义是国际上的事由大家共同商量着办，世界前途命运由各国共同掌握。一个分裂的世界无法应对人类面临的共同挑战，对抗将把人类引入死胡同。
### [美国新冠确诊超2500万 中美表现迥异的原因2500年前就定了？](https://finance.sina.com.cn/wm/2021-01-25/doc-ikftpnny1688383.shtml)
> 概要: 原标题：美国新冠确诊超2500万，中美表现迥异的原因2500年前就定了？ 来源：国是直通车 作者：王凯歌 中央社会主义学院中华文化教研部文化比较教研室教师 反差...
### [辽宁政协原副主席李文喜接受中央纪委国家监委纪律审查和监察调查](https://finance.sina.com.cn/jjxw/2021-01-25/doc-ikftpnny1685953.shtml)
> 概要: 中央纪委国家监委网站客户端1月25日消息，辽宁省政协原副主席李文喜涉嫌严重违纪违法，目前正接受中央纪委国家监委纪律审查和监察调查。
### [习近平在世界经济论坛“达沃斯议程”对话会上的特别致辞(全文)](https://finance.sina.com.cn/china/gncj/2021-01-25/doc-ikftpnny1687057.shtml)
> 概要: 【全文来了 | 习近平在世界经济论坛“达沃斯议程”对话会上的特别致辞】 新华社北京1月25日电 让多边主义的火炬照亮人类前行之路 ——在世界经济论坛“达沃斯议程”对话会上的特...
### [乌克兰长腿美女Karolina福利图 日本最美身材冠军](https://www.3dmgame.com/bagua/4229.html)
> 概要: 近日日本举办了大型健康选美比赛，决赛在神奈川举行。共有582位男女参与比赛。女性部门(Girls Class)冠军由乌克兰美女Karolina(カロリーナ)获得，这也起了网友热议。Karolina出生......
### [被指出轨 朋友曝张恒哑火原因：美国不许父母网络相互攻击 曾希望尽早回国](https://new.qq.com/omn/20210125/20210125A0EFR300.html)
> 概要: 郑爽的瓜，吃累了吗？或许你吃累了，但瓜的后续，还是再一次来了。1月25日，一位张恒朋友名为大茹爱study的网友曝光多张张恒美国照顾子女照片，辟谣所谓代孕机构张恒只来看过两次等消息。         ......
### [组图：终于见面了！杨丞琳李荣浩异地300天后晒同框视频](http://slide.ent.sina.com.cn/star/slide_4_704_351707.html)
> 概要: 组图：终于见面了！杨丞琳李荣浩异地300天后晒同框视频
# 小说
### [秦吏](https://m.qidian.com/book/1011097497/catalog)
> 作者：七月新番

> 标签：上古先秦

> 简介：战国之末，华夏千年未有之大变局。有人天生世卿。有人贵为公子。他却重生成秦国小卒黑夫，云梦秦简中的小人物。为免死于沟壑，为掌握自己命运，他奋力向上攀爬。好在，他赶上了一个大时代。六王毕，四海一！千年血统，敌不过军功授爵。六国豪贵，皆被秦吏踩在脚下。黑夫只想笑问一句:王侯将相，宁有种乎?南取百越，北却匈奴，氐羌西遁，楼船东渡。六合之内，皇帝之土。在他参与下，历史有何改变？秦始皇固有一死，天下将分。身为秦吏，又当如何抉择，是推波助澜，还是力挽狂澜?

> 章节总数：共1056章

> 状态：完本
# 论文
### [Mass classification of dark matter perturbers of stellar tidal streams](https://paperswithcode.com/paper/mass-classification-of-dark-matter-perturbers)
> 日期：21 Dec 2020

> 标签：MODEL SELECTION

> 代码：https://gitlab.com/montanari/stream-dm

> 描述：Stellar streams formed by tidal stripping of progenitors orbiting around the Milky Way are expected to be perturbed by encounters with dark matter subhalos. Recent studies have shown that they are an excellent proxy to infer properties of the perturbers, such as their mass.
### [Tonic: A Deep Reinforcement Learning Library for Fast Prototyping and Benchmarking](https://paperswithcode.com/paper/tonic-a-deep-reinforcement-learning-library)
> 日期：15 Nov 2020

> 标签：CONTINUOUS CONTROL

> 代码：https://github.com/fabiopardo/tonic

> 描述：Deep reinforcement learning has been one of the fastest growing fields of machine learning over the past years and numerous libraries have been open sourced to support research. However, most codebases have a steep learning curve or limited flexibility that do not satisfy a need for fast prototyping in fundamental research.
