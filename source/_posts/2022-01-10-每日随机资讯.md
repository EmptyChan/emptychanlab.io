---
title: 2022-01-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SkiTouring_ZH-CN0237169285_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-01-10 20:20:01
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SkiTouring_ZH-CN0237169285_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [2022，七“贱”下天山](https://www.huxiu.com/article/489024.html)
> 概要: 本文来自微信公众号：思想钢印（ID：sxgy9999），作者：人神共奋，头图来自：电视剧《七剑下天山》一、烂行业的逆袭有一个过去常被问起的问题：问：“刚大，为啥片仔癀100多倍的PE，云南白药才20倍......
### [UJCMS 1.0.0 发布，国内开源 Java cms](https://www.oschina.net/news/177777/yjcms-1-0-0-relwased)
> 概要: UJCMS是在Jspxcms多年的开发经验上，重新设计开发的Java开源内容管理系统(java cms)。使用SpringBoot、MyBatis、Shiro、Lucene、FreeMarker、Ty......
### [Firefox 95 和 Chrome 97 在 Linux 的性能对比](https://www.oschina.net/news/177764/firefox-95-chrome-97-performance-on-linux)
> 概要: 看看两款主流浏览器 —— Firefox 和 Chrome 最新版本在 Linux 桌面的性能对比。本次运行的 Benchmark 基准测试在 AMD Ryzen 9 5950X 台式机上进行，该机器......
### [龙芯发布 LoongArch64-.NET-SDK-6.0.100 开发者试用版](https://www.oschina.net/news/177758)
> 概要: 龙芯开源社区发布了支持 LoongArch64 架构的 .NET-SDK-6.0.100 开发者试用版。据介绍，龙芯 .NET 基于上游社区版本适配支持龙芯平台架构，目前支持 LoongArch64 ......
### [qBittorrent 4.4 发布，支持 Qt 6 和 Bittorrent v2](https://www.oschina.net/news/177754/qbittorrent-4-4-0-released)
> 概要: qBittorrent 一个跨平台的 BitTorrent 客户端，具有美观的 Qt 用户界面以及用于远程控制的 Web UI 和集成搜索引擎。qBittorrent 旨在满足大多数用户的需求，同时使......
### [P站美图推荐——振袖特辑](https://news.dmzj.com/article/73291.html)
> 概要: 麻将游戏《雀魂》确定将会制作TV动画，2022年4月开始放送。
### [新加坡18岁女儿用父亲的信用卡氪金 父亲要求退款](https://news.dmzj.com/article/73293.html)
> 概要: 根据雅虎新闻报道，一名居住在新加坡的父亲的信用卡，被18岁的女儿偷偷在游戏《原神》中氪金2万美元。父亲在与信用卡公司交涉后，获得了1万美元的退款。
### [冬奥进入倒计时，音视频从业者该如何进行最后升级](https://segmentfault.com/a/1190000041257528?utm_source=sf-homepage)
> 概要: 2022 年到来，新的一年最令我们激动的事情之一，莫过于冬奥会的举办。近期，北京市广播电视局发布《关于推动广播电视和网络视听高质量发展的意见》，意见提出推动 5G+8K+ 互动视频，开办 8K 实验频......
### [GSC《初音未来》雪初音Grand Voyage Ver.粘土人](https://news.dmzj.com/article/73296.html)
> 概要: GSC根据《初音未来》中的雪初音制作的Grand Voyage Ver.粘土人开订了。本作采用了“piapro”上征集的以“海”为主题的雪初音服装，包括“通常颜”、“笑颜”和“兴奋颜”三种表情，另外配有“帽子”、“望远镜”、“墨镜”和“脱下外衣的...
### [被告白就要死！](https://news.dmzj.com/article/73297.html)
> 概要: 今天为大家介绍来自六升六郎太的新连载《外冷内热的青梅对我的暗恋暴露无遗》。
### [贵圈｜干一年倒贴150万，没戏拍没收入：中国新导演还有出路吗](https://new.qq.com/rain/a/20220109A0405M00)
> 概要: * 版权声明：腾讯新闻出品内容，未经授权，不得复制和转载，否则将追究法律责任1月7日上线的《导演请指教》总决赛，曾赠获得“年度价值导演”的荣誉。两个月前，第一期节目播出。曾赠的父母开着弹幕看完，立刻打......
### [《明日方舟》主线动画先导图公开 黎明前奏即将唱响](https://acg.gamersky.com/news/202201/1451416.shtml)
> 概要: 《明日方舟》官方公布了主线动画《明日方舟：黎明前奏》的先导图，动画正在制作中。
### [仁井学公开「吹响！上低音号」久石奏新绘](http://acg.178.com/202201/435780188493.html)
> 概要: 今日（1月10日），日本知名动画原画师、作画监督仁井学老师公开了其最新绘制的「吹响！上低音号」久石奏插画。「吹响！上低音号～欢迎来到北宇治高中吹奏乐部～」是日本小说家武田绫乃创作的小说作品，由宝岛社出......
### [脑洞大开的联动！日本开卖吃豆人肉夹馍](https://www.3dmgame.com/news/202201/3832872.html)
> 概要: 诞生于上个世纪的Namco公司吉祥物——吃豆人，作为游戏行业的一个符号被全世界内的人们所熟知。最近，日本老店“岩崎本铺”宣布与吃豆人进行跨界联动，推出看起来非常像吃豆人的美味肉夹馍。其实，说这个是肉夹......
### [漫画「六道斗争纪」第三卷封面插图公开](http://acg.178.com/202201/435781283853.html)
> 概要: 漫画「六道斗争纪」公开了最新卷第三卷的封面插图，本次封面绘制的是ヴァルナ。漫画第三卷售价为748日元，将于2022年2月7日正式发售。「六道斗争纪」（六道闘争紀）是小田世里奈创作、讲谈社出版发行的漫画......
### [《幻想三国志1-4&外传》现已在Steam发售 5代永降](https://www.3dmgame.com/news/202201/3832874.html)
> 概要: 《幻想三国志1-4》以及《幻想三国志4外传》现已在Steam上发售，《幻想三国志5》国区从90元永降至65元。同时，《幻想三国志》全系列正在Steam降价促销，全系列合辑72% off，原价181元，......
### [石原里美宣布怀孕 孩子性别尚未公开 预产期约在春季](https://new.qq.com/rain/a/20220110A02ZKN00)
> 概要: 石原里美宣布怀孕 孩子性别尚未公开 预产期约在春季
### [《巨人》最终季Part2 ED及OP公开 地鸣声中战斗不息](https://acg.gamersky.com/news/202201/1451446.shtml)
> 概要: TV动画《进击的巨人》官方于今日公布了巨人动画最终季Part2主题曲，OP是由SiM演唱的《The Rumbling（地鸣）》；ED是由ヒグチアイ演唱的《悪魔の子》。
### [《单身即地狱》宋智雅广告报价60万，上恋综后疯狂营销引争议](https://new.qq.com/rain/a/20220106A057HD00)
> 概要: 因为恋综《单身即地狱》的播出，其中一位名为宋智雅的女嘉宾也引发大量关注和讨论，最初大家只是觉得她撞脸JENNIE和张元英，后续她本人凭借在节目中展现出的自信态度吸粉无数。            在已经......
### [虞书欣父母公司被恢复执行222万 曾称是间接受害](https://ent.sina.com.cn/s/m/2022-01-10/doc-ikyamrmz4226731.shtml)
> 概要: 新浪娱乐讯 据天眼查显示，近日，虞书欣父母关联公司新余豪誉实业有限公司新增恢复被执行人信息，执行标的222万余元，执行法院为南昌市东湖区人民法院，关联案件为相关买卖合同纠纷。该公司成立于2009年，由......
### [视频：刘德华拍视频为香港市民打气 号召大家同心抗疫](https://video.sina.com.cn/p/ent/2022-01-10/detail-ikyakumx9401247.d.html)
> 概要: 视频：刘德华拍视频为香港市民打气 号召大家同心抗疫
### [视频：杨颖首登北京台春晚跟家人炫耀 周深找不到拍照位置左右摇摆](https://video.sina.com.cn/p/ent/2022-01-10/detail-ikyakumx9402286.d.html)
> 概要: 视频：杨颖首登北京台春晚跟家人炫耀 周深找不到拍照位置左右摇摆
### [noDRM's GitHub repo DeDRM_tools is disabled due to Readium's DMCA notice](https://github.com/github/dmca/blob/master/2022/01/2022-01-04-readium.md)
> 概要: noDRM's GitHub repo DeDRM_tools is disabled due to Readium's DMCA notice
### [漫画「恶狼游戏」第4卷封面公开](http://acg.178.com/202201/435784513255.html)
> 概要: 漫画「恶狼游戏」公开了第4卷的封面图，该卷将于1月27日发售。漫画「恶狼游戏」改编自Studio Wasabi开发的一款移动端平台逃脱游戏，由紺野ぱる担任作画，连载于漫画杂志「月刊Comic Gene......
### [「时光代理人」日配版ED主题曲MV片段公开](http://acg.178.com/202201/435785605217.html)
> 概要: 由bilibili与哔梦联合出品，澜映画负责制作的原创网络动画作品「时光代理人」近期公开了日配版ED主题曲「OverThink」的MV片段。该作日语版已于1月9日开始播出。「OverThink」MV片......
### [State of the Web: Deno](https://byteofdev.com/posts/deno/)
> 概要: State of the Web: Deno
### [AMD故意限制RX6500 XT挖矿性能 只给4GB显存](https://www.3dmgame.com/news/202201/3832884.html)
> 概要: 在2022年CES展会上，AMD推出了RX6500 XT，目标是让玩家们能买到廉价游戏显卡。RX6500 XT基于Navi24核心，拥有1024个流处理器和光追单元，16MB无限缓存，游戏加速频率可达......
### [视频：2021最美表演 杨洋《开始》正片](https://video.sina.com.cn/p/ent/2022-01-10/detail-ikyamrmz4241134.d.html)
> 概要: 视频：2021最美表演 杨洋《开始》正片
### [《街头霸王》系列35周年 卡普空公布新纪念LOGO](https://www.3dmgame.com/news/202201/3832896.html)
> 概要: 卡普空公布了《街头霸王》系列35周年的纪念Logo，《街头霸王》系列的首部作品于1987年8月12日在日本街机平台上发布，所以这款战斗游戏将在2022年8月12日庆祝它35周年的纪念日。卡普空此前曾透......
### [互联网广告的葬礼](https://www.huxiu.com/article/489107.html)
> 概要: 本文来自微信公众号：品玩（ID：pinwancool），作者：沈丹阳，题图来自：视觉中国作为一个在行业里摸爬滚打了十多年的资深“广告狗”，冯铭明显地感觉到互联网生意越来越不好做了。一方面新客户越来越少......
### [碳交易市场：谁自愿花钱买“空气”？](https://www.huxiu.com/article/489157.html)
> 概要: 出品｜虎嗅驻西南编辑作者｜雨林下题图｜东方IC“现在的碳交易市场就像一局新桌游，大家都是不懂规则的小白，先听裁判把玩法讲清楚，再上手参与几次，之后才可能游刃有余。”一位电力公司负责人如此形象比喻。20......
### [谷歌副总裁怒喷苹果 iMessage 封闭，使 iPhone / 安卓用户无法良好交流](https://www.ithome.com/0/597/675.htm)
> 概要: IT之家1 月 10 日消息，Hiroshi Lockheimer 作为平台和生态系统的高级副总裁，负责监督谷歌的所有操作系统。近年来，他一直批评苹果不支持 RCS（Rich Communicatio......
### [OPPO 首款平板电脑通过 3C 认证，支持 33W 快充](https://www.ithome.com/0/597/681.htm)
> 概要: IT之家1 月 10 日消息，OPPO 副总裁、中国区总裁刘波此前透露，OPPO 正在拓展新的平板电脑，还有一些多形态的智能手机产品，预计在 2022 年上半年推出第一款平板电脑。现有网友发现，OPP......
### [LG 宣布加入 IBM 量子网络计划，以探索新技术在工业中的应用](https://www.ithome.com/0/597/682.htm)
> 概要: IT之家1 月 10 日消息，根据 IBM 官方消息，LG Electronics 正式加入 IBM 量子网络计划，将会获得 IBM 量子计算系统以及 IBM开源量子信息软件开发工具包 Qiskit ......
### [Art of War《剑风传奇》限定T恤预定中 售价546元](https://acg.gamersky.com/news/202201/1451586.shtml)
> 概要: 剑风传奇展（大阪会场）× ART OF WAR 合作T恤开启预定。
### [小米推出游戏鼠标 Lite，众筹价 99 元](https://www.ithome.com/0/597/706.htm)
> 概要: 感谢IT之家网友HNwwa的线索投递！IT之家1 月 10 日消息，小米商城公布了新款小米游戏鼠标 Lite，将于 1 月 12 日开启众筹，零售价 129 元，众筹价 99 元。IT之家了解到，这款......
### [视频：越剧金派创始人金采风逝世 享年92岁](https://video.sina.com.cn/p/ent/2022-01-10/detail-ikyamrmz4312712.d.html)
> 概要: 视频：越剧金派创始人金采风逝世 享年92岁
### [offer3：王蕊被劝退，发消息质问导师：我第一周工作不认真吗？](https://new.qq.com/rain/a/20220107V0DI9G00)
> 概要: offer3：王蕊被劝退，发消息质问导师：我第一周工作不认真吗？
### [数字藏品江湖风起](https://www.tuicool.com/articles/YJNJneJ)
> 概要: 文丨来咖智库，作者丨柴犬，编辑丨G3007数字藏品离规模化发展仍有很长的路要走，但现在对这一问题的讨论，却是较为有趣的。刚过去的一年，数字藏品市场变化极快。从国际上看，NFT的交易额呈现出爆发增长的局......
### [NFT只是小图片？他们已经开始雇无聊猿上班了](https://www.tuicool.com/articles/aUB3ayI)
> 概要: 起床，洗漱，穿搭整齐，带上耳机出门，买咖啡，打开电脑，一边听歌一边写乐评，晚上还要去客串《黑泡泡电台》。几乎和我们所有人一样，无聊猿猴（BAYC）#4593 的一天开始了。2021 年 12 月 17......
### [买完狂跌20倍，这冤枉钱我不花了](https://www.huxiu.com/article/489237.html)
> 概要: 本文来自微信公众号：Vista看天下（ID：vistaweek），作者：叶橙子，头图来自：《破坏之王》肯德基与泡泡玛特在本周推出的联名盲盒，已经在二手市场上炒出了天价。联名活动本身，是购买肯德基99元......
### [3DM速报： 严阵以待周销三连冠 老头环捏脸自由](https://www.3dmgame.com/news/202201/3832919.html)
> 概要: 欢迎来到今日的三大妈速报三分钟带你了解游戏业最新资讯大家好，我是米瑟“老头环”角色自定义系统曝光超自由；《消逝的光芒2》百分百玩穿需要500小时1、Steam周销《严阵以待》三连冠，“老头环”捏脸超自......
### [敬“醉美中国年”，汾酒春节营销有点暖心！](https://www.tuicool.com/articles/UVb6Vvu)
> 概要: 作者：电商君来源：电商报Pro（ID：kandianshang）绿蚁新醅酒，红泥小火炉。春节是一年中最不可替代的家人团聚时刻，到了春节，很多人蛰伏了近一年的消费需求也会得到集中释放。所以，春节也是各大......
### [NetShears: iOS Network monitor/interceptor framework written in Swift](https://github.com/divar-ir/NetShears)
> 概要: NetShears: iOS Network monitor/interceptor framework written in Swift
### [鬼灭之刃：作为天花板存在的缘一出场，无惨恐惧的细胞都在颤抖](https://new.qq.com/omn/20220110/20220110A09TS600.html)
> 概要: 《鬼灭之刃》第二季游郭篇已经更新到第六集，在这一集中的打戏可以说是非常流畅和精彩了。为了和上玄六的堕姬交手，炭治郎的身体多次超越了极限，使用了日之呼吸的炭治郎让堕姬似乎看到了作为天花板存在的缘一，这让......
### [安徽网友呼吁组建相亲公园，官方：将提升相亲交友活动覆盖面](https://finance.sina.com.cn/china/gncj/2022-01-10/doc-ikyakumx9522495.shtml)
> 概要: 原标题：安徽网友呼吁组建相亲公园，官方：将提升相亲交友活动覆盖面 大龄青年相亲婚恋问题是社会关注的焦点。 被单身困住的不仅是一线城市的青年。
### [河南安阳基本锁定本轮疫情感染源](https://finance.sina.com.cn/china/gncj/2022-01-10/doc-ikyakumx9526527.shtml)
> 概要: 【#河南安阳基本锁定本轮疫情感染源#】总台记者从河南安阳市疫情防控工作发布会了解到，#河南安阳26例确诊病例均属轻症# ，已转至定点医院进行隔离治疗。
### [鬼灭之刃第6话，蕨姬记忆之中的男人是谁，让无惨都这么害怕？](https://new.qq.com/omn/20220110/20220110A09WHW00.html)
> 概要: 我们知道这周更新了《鬼灭之刃·游郭篇》最新一期的动画第6话，这一话开始，靠着提高体温，炭治郎终于获得了与蕨姬一战的实力，只是这段时间并不能太长。而就在两人战斗正激烈的时候，            蕨姬......
### [城市引才政策生变：北上松动、深杭收紧，2021年谁将成为“抢人”赢家？](https://finance.sina.com.cn/china/gncj/2022-01-10/doc-ikyakumx9527534.shtml)
> 概要: 原标题：城市引才政策生变：北上松动、深杭收紧，2021年谁将成为“抢人”赢家？ 21世纪经济报道记者王帆深圳报道 过去一年里，深圳、杭州、上海...
### [李克强主持召开国务院常务会议](https://finance.sina.com.cn/china/2022-01-10/doc-ikyakumx9526975.shtml)
> 概要: 李克强主持召开国务院常务会议，部署加快推进“十四五”规划《纲要》和专项规划确定的重大项目，扩大有效投资。
### [鬼灭之刃第6话，愤怒的祢豆子有多可怕，直接压制上弦之六的蕨姬](https://new.qq.com/omn/20220110/20220110A0ADMG00.html)
> 概要: 我们知道这周更新了《鬼灭之刃·游郭篇》最新一期的动画第6话，在这一话之中，炭治郎这边在蕨姬破坏了一条花街之后，还想用言语说服蕨姬，看来蕨姬对自己鬼的身份还是很满意的。            炭治郎轻松......
### [Node.js 动态表格大文件下载实践](https://www.tuicool.com/articles/2Y7FRvY)
> 概要: 前言最近优化了几个新人写出的动态表格文件下载接口的性能瓶颈，感觉非常有必要总结一篇文章作为文档来抛砖引玉，以促进大家学习一起写出更专业的代码。HTTP 文件下载讲具体问题之前需要先了解一些 HTTP ......
### [首批55个国家级旅游休闲街区出炉，你准备去逛哪儿？](https://finance.sina.com.cn/jjxw/2022-01-10/doc-ikyakumx9531357.shtml)
> 概要: 原标题：首批55个国家级旅游休闲街区出炉，你准备去逛哪儿？ 来源：视觉中国 据国家旅游局公布的信息，首批国家级旅游休闲街区名单正式公示。
### [Covid-19 may have killed nearly 3M in India, far more than official counts show](https://www.science.org/content/article/covid-19-may-have-killed-nearly-3-million-india-far-more-official-counts-show)
> 概要: Covid-19 may have killed nearly 3M in India, far more than official counts show
### [保供电、减税负应对“三重压力”，广东计划五年新增2.5万亿战略性产业集群融资](https://finance.sina.com.cn/china/gncj/2022-01-10/doc-ikyakumx9536218.shtml)
> 概要: 原标题：保供电、减税负应对“三重压力”，广东计划五年新增2.5万亿战略性产业集群融资 南方财经全媒体见习记者刘黎霞 广州报道 工业是经济发展的“压舱石”，企业是动力源。
### [伊藤润二漫画再出手办，上色款不够味？黑白款才是精髓](https://new.qq.com/omn/20220110/20220110A0AWSU00.html)
> 概要: 伊藤润二的漫画作品有着极强的吸引力，高超独特的绘画风格和暗藏深意的故事构架，让不少漫画爱好者深陷其中，也出现了很多伊藤润二漫画作品的资深读者。有了如此庞大的读者群体，伊藤润二漫画作品自然也少不了推出周......
# 小说
### [末日尸国](http://book.zongheng.com/book/852740.html)
> 作者：辉月姓陈

> 标签：科幻游戏

> 简介：2018年5月的一天，49颗陨石降临地球，带来外星尸化病毒，导致，整个齐鲁境内沦陷，变成丧尸的乐园，同年，一个名为猎尸系统的东西，连接到刘琪的手机上，从那天开始，主人公刘琪，带着自己的系统，一枚戒指，与成千上万的丧尸，展开了生死博弈。不料，半年后的某天，震惊世界的一幕降临华夏，真正的敌人，远不止如此，为了活下去，唯有团结一致。

> 章节末：第二百五十五章 五大护卫，死！

> 状态：完本
# 论文
### [Team Enigma at ArgMining-EMNLP 2021: Leveraging Pre-trained Language Models for Key Point Matching | Papers With Code](https://paperswithcode.com/paper/team-enigma-at-argmining-emnlp-2021)
> 日期：24 Oct 2021

> 标签：None

> 代码：None

> 描述：We present the system description for our submission towards the Key Point Analysis Shared Task at ArgMining 2021. Track 1 of the shared task requires participants to develop methods to predict the match score between each pair of arguments and keypoints, provided they belong to the same topic under the same stance.
### [Boosted CVaR Classification | Papers With Code](https://paperswithcode.com/paper/boosted-cvar-classification)
> 日期：26 Oct 2021

> 标签：None

> 代码：None

> 描述：Many modern machine learning tasks require models with high tail performance, i.e. high performance over the worst-off samples in the dataset. This problem has been widely studied in fields such as algorithmic fairness, class imbalance, and risk-sensitive decision making.
