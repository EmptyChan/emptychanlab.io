---
title: 2021-08-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LammasDay_ZH-CN4229387191_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-08-01 16:01:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LammasDay_ZH-CN4229387191_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [CNCF 宣布 Grafana 实验室升级为白金会员](https://www.oschina.net/news/153174)
> 概要: 可观察性方面的领导者表明了在云原生工作负载的前沿解决方案上的合作承诺CNCF（云原生计算基金会）宣布，Grafana 实验室的会员资格从银牌会员升级到白金会员。随着承诺的扩大，该公司还将加入 CNCF......
### [GOTC 2021 深圳站首日圆满落幕](https://www.oschina.net/news/153172)
> 概要: 7 月 31 日，由开放原子开源基金会与 Linux 基金会亚太区联合开源中国共同举办的“全球开源技术峰会 GOTC 2021 深圳站”在深圳会展中心拉开帷幕。本次峰会汇聚国内顶级开源厂商和开源社区成......
### [「中国开源原生商业社区」启动](https://www.oschina.net/news/153170)
> 概要: 7 月 31 日，在 GOTC 全球开源技术峰会深圳站「开源原生商业」专题论坛上，中国开源原生商业社区正式宣告成立。国家信息安全发展中心副主任董大健，开源中国 CEO 马越，多家社区创始成员单位代表出......
### [数据科学领域的顶级语言：Python 居首，SQL 次之](https://www.oschina.net/news/153173/top-programming-language-for-data-science)
> 概要: Anaconda 最新发布的一份《State of Data Science 2021》报告指出，数据科学和机器学习专业人士推动了 Python 编程语言的采用，但数据科学和机器学习在商业中仍然缺乏关......
### [过气网红与妈宝男的高颜值NTR爱情喜剧](https://news.dmzj.com/article/71719.html)
> 概要: 泰国7月新剧《46天婚礼大作战》已上线，剧情狗血又好看，属于轻喜剧爱情线，男女主颜值般配，演技过关，由平采娜和查侬主演，平采娜简直泰国热播剧女神，基本部部剧爆火，当初一部《初恋这件小事儿》使她名声大噪，查侬则是靠着《天才枪手》里的出色演技令观众记...
### [视频：平安北京发布通报 吴亦凡因涉嫌强奸罪被警方刑拘](https://video.sina.com.cn/p/ent/2021-08-01/detail-ikqcfncc0181440.d.html)
> 概要: 视频：平安北京发布通报 吴亦凡因涉嫌强奸罪被警方刑拘
### [Download a CSV of your Amazon purchases](https://www.amazon.com/gp/b2b/reports)
> 概要: Download a CSV of your Amazon purchases
### [Txiki.js: Tiny JavaScript runtime built with QuickJS and libuv](https://github.com/saghul/txiki.js)
> 概要: Txiki.js: Tiny JavaScript runtime built with QuickJS and libuv
### [明年4月番！《测不准的阿波连同学》TV动画化决定](https://news.dmzj.com/article/71720.html)
> 概要: 水あさと原作漫画《测不准的阿波连同学》确定将会改编为TV动画，2022年4月开播。
### [Blizzard's reputation collapsed in just three years](https://www.pcgamer.com/how-blizzards-reputation-collapsed-in-just-3-years/)
> 概要: Blizzard's reputation collapsed in just three years
### [资深工程师之路（一）](https://www.tuicool.com/articles/EFrqiii)
> 概要: 之前的专栏文章都是偏技术介绍方向的，比如之前的《算法工程师技术路线图》，受到了不少同学的关注。不过作为工程师的职业发展，显然不仅仅是只有技术的部分，尤其是发展到工作五年后逐渐成为资深工程师后，会碰到进......
### [组图：为河南暴雨事件筹款！36位港星开线上音乐会](http://slide.ent.sina.com.cn/star/slide_4_704_359865.html)
> 概要: 组图：为河南暴雨事件筹款！36位港星开线上音乐会
### [大战略游戏《地球不屈》上架Steam 支持中文](https://www.3dmgame.com/news/202108/3820231.html)
> 概要: 4X大战略游戏《地球不屈（Terra Invicta）》现已上架Steam，支持简体中文，将于年内发售。神秘的外星力量逼近太阳系，不知是敌是友。关于外星人的动机和应对方法，众说纷纭。你将参与各方力量的......
### [我们绝不能再落后下去](https://www.huxiu.com/article/445059.html)
> 概要: 本文来自微信公众号：宁南山（ID：ningnanshan2017），作者：宁南山，题图来自视觉中国隔三差五，就会有西方人收养的中国孩子新闻出来，比如前几天，7月26日，加拿大华裔Maggie MacN......
### [微服务架构设计模式 - 进程间通信](https://www.tuicool.com/articles/fYZjAzv)
> 概要: 微服务架构进程间通信概述进程间的通信本质是交换消息交互方式第一个维度：一对一和一对多一对一：一个请求一个服务实例处理一对多：一个请求多个服务实例处理第二个维度：同步和异步同步模式：客户端请求服务端实时......
### [I was a teacher for 17 years, but couldn't read or write](https://www.bbc.co.uk/news/stories-43700153)
> 概要: I was a teacher for 17 years, but couldn't read or write
### [组图：太养眼！超模安布罗休拍写真清新甜美](http://slide.ent.sina.com.cn/star/slide_4_704_359869.html)
> 概要: 组图：太养眼！超模安布罗休拍写真清新甜美
### [《上行战场》简中翻译已重做 官方特别感谢中国玩家](https://www.3dmgame.com/news/202108/3820233.html)
> 概要: 俯视角赛博朋克射击游戏《上行战场》现已在Steam上推出，获得玩家“多半好评”。很多玩家反映中文翻译问题比较恶劣，地图没有汉化、人物对白需要改进、物品和任务描述没有做完。目前糟糕的翻译水平让部分不懂英......
### [汤姆克鲁斯《壮志凌云2》定档 11月19日日美同时首映](https://www.3dmgame.com/news/202108/3820235.html)
> 概要: 备受影迷期待的汤姆·克鲁斯经典电影《壮志凌云》续集、《壮志凌云2：独行侠》日前官方宣布定档，11月19日日美同时首映，最新预告（日版字幕）同时公开，一起来先睹为快。·电影《壮志凌云2》是34年前大热军......
### [两张都好看的动漫闺蜜头像](https://new.qq.com/omn/20210722/20210722A0DPUU00.html)
> 概要: 所有图片均来自于网络各处......
### [今日份动漫男生头像](https://new.qq.com/omn/20210725/20210725A05T5700.html)
> 概要: 所有图片均来自于网络各处......
### [动画「测不准的阿波连同学」先导PV及主视觉图公开](http://acg.178.com/202108/421784191891.html)
> 概要: 电视动画「测不准的阿波连同学」公开了先导PV和主视觉图，该作品将于2022年4月开播。「测不准的阿波连同学」先导PVSTAFF原作：水あさと（集英社「少年ジャンプ＋」連載）総監督：山本靖貴監督：牧野友......
### [组图：2021港姐入围第二次比基尼造型亮相 佳丽大秀好身材](http://slide.ent.sina.com.cn/star/slide_4_704_359871.html)
> 概要: 组图：2021港姐入围第二次比基尼造型亮相 佳丽大秀好身材
### [「青春×机关枪」作者新绘「咒术回战」乙骨忧太公开](http://acg.178.com/202108/421785215969.html)
> 概要: 近日，「青春×机关枪」作者NAOE公开了其绘制的「咒术回战」乙骨忧太。乙骨忧太是少年漫画作品「咒术回战」中的角色，东京都立咒术高等专门学校二年级学生，现存四大特级咒术师之一......
### [滴滴，全球资本的超级工程如何成为资本的吞噬者](https://www.huxiu.com/article/445076.html)
> 概要: 本文来自微信公众号：晚点LatePost（ID：postlate），作者：万珮、黄俊杰，编辑：黄俊杰，制图：时娴、龚方毅，题图来自电影《银翼杀手2049》投入 200 多亿美元，让数万人前仆后继、加班......
### [《王冠》第五季发布首张剧照 伊丽莎白女王亮相](https://ent.sina.com.cn/m/f/2021-08-01/doc-ikqcfncc0237490.shtml)
> 概要: 新浪娱乐讯 Netflix热剧《王冠》第5季发布首张剧照，艾美达·斯丹顿（《哈利·波特》《莎翁情史》）饰演的伊丽莎白女王亮相。　　本季由新一批演员出演，伊丽莎白·德比齐（《信条》《夜班经理》）饰演戴安......
### [「Vivy -Fluorite Eye's Song-お疲れさま本」封面图公开](http://acg.178.com/202108/421787895737.html)
> 概要: TV动画「Vivy -Fluorite Eye's Song-」官方插画集「Vivy -Fluorite Eye's Song-お疲れさま本」封面图公开。该商品将于9月24日发售，封面图由动画总作画监......
### [「超时空要塞Δ 绝对LIVE!!!!!!」联动Chugai Grace Café宣传绘图公开](http://acg.178.com/202108/421789742449.html)
> 概要: 完全新作剧场版动画「超时空要塞Δ 绝对LIVE!!!!!!」公开了与Chugai Grace Café联动的宣传绘图。联动活动将于8月17日开始，持续至9月1日。「超时空要塞Δ 绝对LIVE......
### [要劫狱？吴亦凡粉丝，你清醒一点](https://www.huxiu.com/article/445093.html)
> 概要: 题图来自：视觉中国昨晚（7月31日）十点半左右，北京朝阳警方一纸公告让顶流吴亦凡成为内娱涉嫌强奸刑拘第一人。消息一出，互联网金句虽迟但到，“铐，吴语，亦后要吃牢凡了。”而我在第一时间去搜索了微博吴亦凡......
### [美团图数据库平台建设及业务实践](https://www.tuicool.com/articles/m2ium2Y)
> 概要: 前言图数据结构，能够很自然地表征现实世界。比如用户、门店、骑手这些实体可以用图中的点来表示，用户到门店的消费行为、骑手给用户的送餐行为可以用图中的边来表示。使用图的方式对场景建模，便于描述复杂关系。在......
### [中广核：因少量燃料破损，对台山核电厂 1 号机组停机检修](https://www.ithome.com/0/566/500.htm)
> 概要: IT之家8 月 1 日消息 中广核集团于 7 月 30 日发布公告，表示目前位于广东省的台山核电厂 1 号机组运行过程中出现少量燃料破损，但仍在技术规范允许范围内，机组可以继续稳定运行。考虑到 1 号......
### [1 个人 70 万行代码，20 年持续更新：这款游戏号称开发到死，永不停更](https://www.ithome.com/0/566/503.htm)
> 概要: 8 月 1 日消息 这是一款「开发到死」，「永不停更」的游戏。兄弟两人，一人开发，一人剧情，共同维持了这款游戏近 20 年。现在的玩家刚刚打开它，往往会发出“这啥玩意儿？”的疑问：没错，这款《矮人要塞......
### [PancakeSwap发起调整农场奖励提案相关投票](https://www.btc126.com//view/179516.html)
> 概要: 官方消息，PancakeSwap发推称，关于农场奖励调整提案的投票正在进行中，将于8月3日结束。该提案涉及停止36个农场奖励、降低20个农场奖励权重、提高2个农场奖励权重......
### [湖南常德桃花源景区：9场演出777名观众属于高风险人群](https://finance.sina.com.cn/jjxw/2021-08-01/doc-ikqcfncc0264839.shtml)
> 概要: 原标题：湖南常德桃花源景区：9场演出777名观众属于高风险人群 8月1日，湖南常德市桃花源旅游管理区新型冠状病毒肺炎疫情防控指挥部发布消息称，在7月25日、26日...
### [乌兰察布市委原书记杜学军被“双开”：贪财无度、肆意用权](https://finance.sina.com.cn/china/dfjj/2021-08-01/doc-ikqciyzk8869121.shtml)
> 概要: 中央纪委国家监委网站7月31日消息，据内蒙古自治区纪委监委消息：日前，经内蒙古自治区党委批准，内蒙古自治区纪委监委对乌兰察布市委原书记杜学军严重违纪违法问题进行了...
### [大小周的终结与「失速」的奋斗](https://www.tuicool.com/articles/VRziAbJ)
> 概要: 采写 | 沈知涵编辑 | 卫诗婕导语：今天，字节跳动实行了八年的「大小周」制度正式取消。2013 年，大小周起源于一个字节内部一个名叫「周日大讲堂」的项目。起初，周末加班没有双倍薪资，实行积分换 iP......
### [周笔畅罕见扮嫩，穿花瓣衬衣迎36岁生日，终于不走酷拽风了](https://new.qq.com/omn/20210730/20210730A03LA400.html)
> 概要: 有人说2018年中国选秀元年，毕竟在这一年里，很多选秀节目逐渐兴起，像《创造营》和《青春有你》等，都受到不少网友追捧。不过其实早在这之前，国内的选秀节目早已出现，同样备受关注。像2015年盛行的选秀娱......
### [央视网评：吴亦凡一案，该让某些走火入魔的粉丝清醒了！](https://finance.sina.com.cn/china/gncj/2021-08-01/doc-ikqciyzk8870569.shtml)
> 概要: 7月31日，因涉嫌强奸罪，曾经人气火爆的吴亦凡被北京市朝阳公安分局依法刑事拘留。案件侦办工作正在进一步开展。 围绕着此案相关的传言和情节...
### [“相声演员”大兵的毁灭史，他的故事远比你想得更恶劣](https://new.qq.com/omn/20210801/20210801A05XX800.html)
> 概要: 文/文刀贰2009年，在《天下校友会》综艺“谁是下一个小沈阳”环节中。一个名叫“棒棒糖”的选手声称要演唱香港BRYNOD乐队“黄家驹”的《真的爱你》。但却将“驹”读成了“狗”。            ......
### [北京丽泽金融商务区打造数字货币应用生态圈助推城南发展](https://www.btc126.com//view/179519.html)
> 概要: 7月29日，北京市正式发布《推动城市南部地区高质量发展行动计划(2021—2025年)》，提出构建以新兴金融为主、科技和专业服务为附的产业体系，建设金融科技创新示范区。未来五年，丽泽金融商务区将加快打......
### [湖南张家界：加强小区管理原则上不准外出 所有公职人员非必要一律在家待命](https://finance.sina.com.cn/china/dfjj/2021-08-01/doc-ikqcfncc0273160.shtml)
> 概要: 原标题：湖南张家界：加强小区管理原则上不准外出 所有公职人员非必要一律在家待命 记者今天（8月1日）从湖南张家界市新型冠状病毒感染肺炎疫情防控工作指挥部获悉...
### [严控出入！湖南张家界发布社区（村）疫情防控十项工作制度](https://finance.sina.com.cn/china/dfjj/2021-08-01/doc-ikqcfncc0274480.shtml)
> 概要: 原标题：严控出入！湖南张家界发布社区（村）疫情防控十项工作制度 来源：央视新闻客户端 记者今天（8月1日）从张家界市新型冠状病毒感染肺炎疫情防控工作指挥部获悉...
### [经典巫毒显卡制造商3dfx时隔20年或卷土重来](https://www.3dmgame.com/news/202108/3820252.html)
> 概要: 如今的显卡市场仅有英伟达、AMD两家大厂，从2020年开始，英特尔也加入了竞争，推出了自家的独立显卡产品。但是早在20多年之前，PC显卡市场的品牌更加丰富，上世纪90年代3dfx推出的“Voodoo”......
### [张家界对全市辖区所有道路实行交通管制](https://finance.sina.com.cn/jjxw/2021-08-01/doc-ikqcfncc0277017.shtml)
> 概要: 原标题：张家界对全市辖区所有道路实行交通管制 据@张家界日报-掌上张家界 消息，张家界市公安局交通警察支队8月1日发布《关于在全市范围内实行交通管制的通告》：...
### [1025万枚EOS从未知钱包转至火币交易所](https://www.btc126.com//view/179521.html)
> 概要: Whale Alert监测数据显示，北京时间8月1日14:23:15，1025万枚EOS（价值超过4268.7万美元）从未知钱包（5mgr1pmtxa4y）转至火币交易所钱包（vuniyuoxoeub......
### [《最终幻想7》原有望登陆世嘉土星 但谈判未获成功](https://www.3dmgame.com/news/202108/3820253.html)
> 概要: 当年的世嘉土星主机在北美销量不佳，但是在日本卖得不错，超越任天堂N64主机，仅次于索尼PlayStation拿下第二位。而当时按照计划，《最终幻想7》原有可能把土星主机的销量再往上推一把。土星主机的游......
### [《精灵与萤火意志》两部合集 10 月登陆 Switch 平台，预售价 49.99 美元](https://www.ithome.com/0/566/521.htm)
> 概要: IT之家8 月 1 日消息 微软 Moon Studios 工作室的知名游戏《精灵与黑暗森林》、《精灵与萤火意志》两部曲的合集将在10 月份登陆任天堂 Switch 平台，预售价49.99 美元。IT......
### [阿里自营：虾选医用口罩 50 片 6.8 元（灭菌级）](https://lapin.ithome.com/html/digi/566525.htm)
> 概要: 【阿里自营 活动随时结束】虾选 掌护医用口罩（灭菌级）50 片售价 19.8 元，今日可领 13 元大额券，实付 6.8 元包邮：天猫掌护 一次性医用口罩 50 片灭菌版券后 6.8 元领 13 元券......
### [奥运新闻阿根廷主帅：我爱斯科拉，他做出了太多贡献](https://bbs.hupu.com/44517688.html)
> 概要: 虎扑08月01日讯 早前结束的一场奥运男篮比赛，阿根廷男篮97-77击败日本男篮。赛后，阿根廷男篮主帅塞尔吉奥-赫尔南德斯接受了采访。谈到路易斯-斯科拉，赫尔南德斯说道：“他的表现太棒了。我感觉，过去
### [数据：比特币用户数量从4月底的1.43亿增加到6月份的2.21亿](https://www.btc126.com//view/179524.html)
> 概要: Micro Strategy首席执行官Michael Saylor指出：“比特币目前被1.14亿人持有，成为世界历史上增长最快、持有最广泛的金融资产。”而比特币用户数量从4月底的1.43亿增加到6月份......
### [流言板郝伟：本场比赛会考虑轮换，但还是要走得稳一点](https://bbs.hupu.com/44517764.html)
> 概要: 虎扑08月01日讯 北京时间8月2日20点,中超联赛第11轮山东泰山队和青岛队的比赛将在广州花都体育场打响，今日泰山队主教练郝伟携球员孙准浩出席了赛前新闻发布会。谈备战情况郝伟：“队伍也是和以前一样，
### [赛后MTG 3-0 深圳DYG，一鼓作气速战速决，MTG零封拿下比赛](https://bbs.hupu.com/44517872.html)
> 概要: Game3：MTG蓝色方，深圳DYG红色方【Ban/Pick】【高光时刻】星宇墨子走位失误，华郴蒙犽将其击杀拿到一血华郴蒙犽伤害拉满越塔强杀萧玦百里守约一夫当关万夫莫开，久龙关羽越塔分割战场后丝血逃生
### [还记得《仙剑奇侠传三》重楼吗？他去开店当老板了？](https://new.qq.com/omn/20210801/20210801A0709700.html)
> 概要: 魔尊重楼武功高强，天下间惟有飞蓬将军可与之对战，敌逢对手惺惺相惜，重楼入人界寻飞蓬，他的故事就此展开。            重楼虽是魔尊，可魔亦有道从不侵扰人间，与飞蓬决战也追求公平，趁人之危赢了也......
### [《牛气满满的哥哥》第三期：乔振宇放弃游泳、李维嘉故意自爆](https://new.qq.com/omn/20210801/20210801A070J700.html)
> 概要: 《牛气满满的哥哥》第二季，从一开始，大家就看出来了，大哥哥队和小哥哥队实力不均衡。先不说小哥哥队全员都是新人了，就说大哥哥队的两个“核心人物”，一个舅舅王耀庆，一个拼命三郎吴奇隆。          ......
### [流言板记者：张呈栋背后对巴顿犯规未判罚，津门虎将上诉](https://bbs.hupu.com/44517930.html)
> 概要: 虎扑08月01日讯 据天津记者徐国玺微博报道，津门虎将就昨天中超第10轮比赛中的争议进行上诉。昨天进行的中超第10轮比赛，津门虎1-2不敌河北队。徐国玺写道：昨天比赛，张呈栋背后对巴顿犯规但主裁判并未
# 小说
### [龙楼宝殿](http://book.zongheng.com/book/380539.html)
> 作者：山野树

> 标签：悬疑灵异

> 简介：身怀观天遁地绝学，特种侦察兵出生，被战友连累提前退伍，因为好奇踏入人生中第一座地下王陵，从此一发不可收，永无回头日，入大川，斩血尸，闯异界，战奇兽，战友情，离别殇，尽在《龙楼宝殿》，原名《升棺发财》

> 章节末：一梦十年（大结局）

> 状态：完本
# 论文
### [Using Voice and Biofeedback to Predict User Engagement during Requirements Interviews](https://paperswithcode.com/paper/using-voice-and-biofeedback-to-predict-user)
> 日期：6 Apr 2021

> 标签：None

> 代码：https://github.com/alessioferrari/VoiceBiofeedEmo

> 描述：Capturing users engagement is crucial for gathering feedback about the features of a software product. In a market-driven context, current approaches to collect and analyze users feedback are based on techniques leveraging information extracted from product reviews and social media.
### [MeDAL: Medical Abbreviation Disambiguation Dataset for Natural Language Understanding Pretraining](https://paperswithcode.com/paper/medal-medical-abbreviation-disambiguation)
> 日期：27 Dec 2020

> 标签：NATURAL LANGUAGE UNDERSTANDING

> 代码：https://github.com/BruceWen120/medal

> 描述：One of the biggest challenges that prohibit the use of many current NLP methods in clinical settings is the availability of public datasets. In this work, we present MeDAL, a large medical text dataset curated for abbreviation disambiguation, designed for natural language understanding pre-training in the medical domain.
