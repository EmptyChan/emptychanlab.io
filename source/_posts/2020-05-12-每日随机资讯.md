---
title: 2020-05-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FlorenceNightingale_EN-CN1255280162_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-12 21:47:31
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FlorenceNightingale_EN-CN1255280162_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：刘嘉玲晒照为妈妈庆祝母亲节 豪宅室内曝光充满老上海风味](http://slide.ent.sina.com.cn/star/slide_4_86512_338562.html)
> 概要: 组图：刘嘉玲晒照为妈妈庆祝母亲节 豪宅室内曝光充满老上海风味
### [深夜开车？萧敬腾自拍用美食滤镜称给粉丝们吃](https://ent.sina.com.cn/y/ygangtai/2020-05-12/doc-iirczymk1103457.shtml)
> 概要: 新浪娱乐讯 12日零点刚过不久，萧敬腾在微博晒出一张自拍，并配文写道“抱歉来晚了！！只好送上自拍一张了，滤镜选了美食，给你们吃。。。”　　围观粉丝纷纷留言，“你就是那道美食”“吃……吃你咩”“哥哥我来......
### [超暖心！尼格买提为白衣天使献花感谢医护工作者](https://ent.sina.com.cn/s/m/2020-05-12/doc-iirczymk1188979.shtml)
> 概要: 新浪娱乐讯 5月12日是国际护士节，尼格买提在微博分享为医护人员送花的视频并暖心发文：“今天是国际护士节，正好陪妈妈在医院做手术，让我有机会说一声节日快乐。如果你身边也有医护工作者，送TA一朵祝福吧”......
### [组图：汶川地震12周年 舒淇Baby张杰黄晓明张艺兴等发文缅怀](http://slide.ent.sina.com.cn/star/w/slide_4_704_338542.html)
> 概要: 组图：汶川地震12周年 舒淇Baby张杰黄晓明张艺兴等发文缅怀
### [韩国法院批准警方拘留"N号房"创建人godgod](https://ent.sina.com.cn/s/j/2020-05-12/doc-iircuyvi2692839.shtml)
> 概要: 新浪娱乐讯 5月12日，据韩媒，韩国大邱安东地方法院今天下午批准了警方对“N号房”事件主犯“GODGOD”的拘留申请。　　网名“GODGOD”的A某是“N号房”非法聊天群的创始人，曾大量制作性剥削视频......
### [蝴蝶姐姐丁恺乐仍恋猪，一片痴心不改，父母寺庙祈盼女斩断烂桃花](https://new.qq.com/omn/20200512/20200512A0M34O00.html)
> 概要: 自从周扬青在微博上宣布与罗志祥分手，许多明星艺人就被牵扯其中，目前可以说是一波未平一波又起。周扬青称罗志祥跟旗下的女艺人有不正当的男女关系，而不少人扒出来其实这位女艺人就是恺乐。恺乐是小猪公司里唯一一......
### [又出事？课前放《光点》被学生反对，肖战老师粉当众痛骂学生](https://new.qq.com/omn/20200512/20200512A0I9JX00.html)
> 概要: “227”事件所带来的影响还没有结束，圈内顶流肖战又出事了。有网友上传一段视频，视频中一名老师让孩子们齐声喊着：“肖战哥哥你很好，我们很喜欢，冲啊！”作为一位教书育人的教师，在课堂上 组织学生给自己喜......
### [辞去铁饭碗成育儿专家的“央视名嘴”，如何做到事业家庭双平衡](https://new.qq.com/omn/20200512/20200512A0DK9700.html)
> 概要: 【本文主笔：幸子】提到央视主持界的“名嘴”，那可真是数不胜数，从现任的董卿、朱迅、康辉，李思思，再到退休的老员工李瑞英、邢质斌、张宏民、董浩，中央电视台可谓是主持界精英的聚集地。           ......
### [黄磊失误掉“鸡脚筋”，张婧仪连连道歉，何炅成“暖男”！](https://new.qq.com/omn/20200512/20200512A0NK0C00.html)
> 概要: 黄磊娱乐圈内有名的好男人，好爸爸，认识黄磊还是从《爸爸去哪儿》这个节目，那时候的黄磊带着多多参加这个节目，不知道吸引了多少的粉丝。            离开了《爸爸去哪儿》之后黄磊日复一日的在社交平......
### [惊艳众人！娄艺潇古装造型十分惊艳，粉丝们接连二三得都看呆了](https://new.qq.com/omn/20200512/20200512V0NBGQ00.html)
> 概要: 惊艳众人！娄艺潇古装造型十分惊艳，粉丝们接连二三得都看呆了
# 动漫
### [TV动画《食戟之灵 豪之皿》宣布7月开始继续播出](https://news.dmzj.com/article/67359.html)
> 概要: 根据附田祐斗、佐伯俊原作制作的TV动画《食戟之灵 豪之皿》宣布了将于7月开始播出第3话以后的内容。有关具体的播出的时间，还将于确定后另行通知。
### [LL水团联动日本厚生劳动省推出防疫情海报](https://news.dmzj.com/article/67356.html)
> 概要: 日升动画和日本厚生劳动省合作，推出了使用《LoveLive!Sunshine!!》中的组合Aqours的形象制作的预防新冠病毒疫情的海报。
### [动画《Lapis Re:LiGHTs》宣布7月开播！公开新PV](https://news.dmzj.com/article/67352.html)
> 概要: TV动画《Lapis Re:LiGHTs》宣布了从7月开始播出的消息，新的主宣图、第二弹PV等也一并公开。
### [寿屋HORROR美少女《小丑回魂》Pennywise手办开订](https://news.dmzj.com/article/67358.html)
> 概要: 寿屋的HORROR美少女系列根据《小丑回魂》中的Pennywise制作的手办目前已经开订了。本作在保留原作的氛围下进行了大胆的创新，制作成了面带诡异笑容的举着气球邀请你来玩耍的美少女。
### [男帅女美，盘点动漫中的绿发角色，你最喜欢谁？](https://new.qq.com/omn/20200512/20200512A0NA6H00.html)
> 概要: 《海贼王》索隆说到绿发男神，我的脑海里首先冒出来的就是索隆，草帽一伙的剑士，相当于副船长的存在，为人高冷帅气，一头绿发更是衬得他霸气十足。当然她不止头发是绿色的，平时的穿着也是以绿色为主，完全就是从……
### [《斗罗大陆》被举报，只因唐三小舞的关系“惹”到部分家长](https://new.qq.com/omn/20200512/20200512A0O85Q00.html)
> 概要: 大家好，我是你们的速递君。导语：现在优秀的国漫可以说是如雨后春笋般纷纷出现，可以说我们是见证着国漫逐渐崛起的一代，然而此时国漫崛起之路依然充满“曲折”，因为其中剧情一旦涉及到一些家长的“底线”，就会……
### [小花仙：女王范十足的五位花仙精灵王，相比如意，我更喜欢九千岁](https://new.qq.com/omn/20200512/20200512A0ITW100.html)
> 概要: 《小花仙》是一部很精彩的国产动漫，经过数年的连载，如今第五季已经更新完毕，前四季的主角都是夏安安，因此在很多粉丝心目中，夏安安就是《小花仙》里的灵魂人物。这部动漫主要讲述的是地球少女夏安安因为母亲莉……
### [当虹猫蓝兔七侠传用国漫3D拟人化，蓝兔背影绝美，马三娘我爱了！](https://new.qq.com/omn/20200512/20200512A0FJW400.html)
> 概要: 大家好，北冥特摄漫评，带你看不一样的特摄资讯。（转自子非鱼非鱼）虹猫蓝兔作为很多人小时候不可磨灭的记忆，也是一部非常热血的国漫。主要讲述的是七侠守护家园的故事，而他们也是由七种不同的动物组成，分别就……
### [《公主连结》阿库娅表情接连出现，确定不是照着《素晴》画的？](https://new.qq.com/omn/20200512/20200512A0J51000.html)
> 概要: 《公主连结》动画已经播出将近一半，不知道你对它的看法如何。在这部动画里有很多地方和《为美好的世界献上祝福》（素晴）相似，主要是因为导演就是原《素晴》导演，并且这次还负责了动画脚本，所以免不了会很像《……
### [假面骑士OOO迎来十周年纪念，携手假面骑士W一同登陆一番赏系列！](https://new.qq.com/omn/20200512/20200512A0MLFG00.html)
> 概要: 关注公众号：阿伦模玩，即刻获取最新模玩资讯一番赏新弹假面骑士W于2009年正式开播作品在前不久也迎来了10周年纪念随着新一年的到来近期也是迎来了假面骑士OOO的10周年纪念一番赏系列近期就以这两部作……
# 财经
### [今日财经TOP10|刘鹤：支持各类中小微企业顺利渡过难关](https://finance.sina.com.cn/china/gncj/2020-05-12/doc-iirczymk1276540.shtml)
> 概要: 【宏观要闻】 NO.1 中国公布第二批对美加征关税商品第二次排除清单 据财政部网站消息，国务院关税税则委员会12日公布第二批对美加征关税商品第二次排除清单。
### [习近平勉励山西：早日蹚出一条转型发展的新路子](https://finance.sina.com.cn/china/gncj/2020-05-12/doc-iirczymk1271082.shtml)
> 概要: 原标题：习近平勉励山西：早日蹚出一条转型发展的新路子 12日上午，正在山西考察的习近平总书记来到山西转型综合改革示范区政务服务中心，了解示范区改革创新发展情况。
### [北京城管执法部门集中开展生活垃圾分类执法处罚](https://finance.sina.com.cn/china/dfjj/2020-05-12/doc-iirczymk1269950.shtml)
> 概要: 原标题：北京城管执法部门集中开展生活垃圾分类执法处罚 首日立案查处违法行为51起 新修订的《北京市生活垃圾管理条例》实施以来...
### [央行：受疫情冲击影响 一季度我国宏观杠杆率有显著上升](https://finance.sina.com.cn/china/gncj/2020-05-12/doc-iirczymk1274658.shtml)
> 概要: 原标题：人民银行调查统计司有关负责人就4月份金融统计数据情况答《金融时报》记者问 问：4月份M2增长较快，请问您怎样看待当前货币供应量增长的？
### [央行：一季度宏观杠杆率显著回升只是阶段性的](https://finance.sina.com.cn/roll/2020-05-12/doc-iircuyvi2746484.shtml)
> 概要: 原标题：央行：一季度宏观杠杆率显著回升只是阶段性的 5月12日，中国人民银行调查统计司有关负责人就4月份金融统计数据情况答《金融时报》记者问。
### [没有好大学的中西部省份 究竟有多努力？](https://finance.sina.com.cn/china/gncj/2020-05-12/doc-iircuyvi2747485.shtml)
> 概要: 原标题：没有好大学的中西部省份，究竟有多努力？ 来源：每日经济新闻 每经记者 黄名扬  随着地方财政纷纷向高等教育倾斜，再加上国家政策加持，河南...
# 科技
### [⚛️ useWorker() - 一个无阻塞后台任务的React Hook](https://www.ctolib.com/alewin-useWorker.html)
> 概要: ⚛️ useWorker() - 一个无阻塞后台任务的React Hook
### [immudb是用于系统和应用程序的轻量级高速不可变数据库](https://www.ctolib.com/codenotary-immudb.html)
> 概要: immudb是用于系统和应用程序的轻量级高速不可变数据库
### [Java后端面试题精选](https://www.ctolib.com/NotFound9-interviewGuide.html)
> 概要: Java后端面试题精选
### [GitHub的VS Code主题](https://www.ctolib.com/primer-github-vscode-theme.html)
> 概要: GitHub的VS Code主题
### [人类终极问题：我是谁？](https://www.tuicool.com/articles/Qjq6vue)
> 概要: 一千个人眼里，有一千个哈姆雷特。对于心理学，不同的人持有不同的看法。或许每个人都有属于自己的对于心理学的不同的理解。那么，什么是心理学？这实在是一个比较宏大的问题。在回答它之前，我们不妨先来确定“我是......
### [数据需求文档案例：豆瓣“书影音档案”功能埋点设计](https://www.tuicool.com/articles/jaaINfY)
> 概要: 了解豆瓣【书影音功能】的数据表现，以便分析功能是否受用户欢迎，对刺激用户产生更多数据的贡献程度。问题背景豆瓣【书影音档案】功能，通过展现个人的观影，看书，听音乐的标注记录，让用户在豆瓣留下的痕迹拥有资......
### [G1GC慢的排查过程分享](https://www.tuicool.com/articles/iIRNVr3)
> 概要: 本文主要分析严选库存中心压测期间G1GC收集比较慢的问题。背景11月6日严选全链路压测期间，发现有部分机器RT有毛刺。耗时相对其余时间较为明显。现象如下：问题定位利用严选caesar分析很快清楚大部分......
### [178页，四年图神经网络研究精华，图卷积网络作者Thomas Kipf博士论文公布](https://www.tuicool.com/articles/FjQnQnV)
> 概要: 在深度学习领域，图神经网络早已成为热门话题。去年年底，有人统计了 2019 年到 2020 年各大顶会提交论文关键词的数据。结果显示，「graph neural network”」的增长速度位列榜首，......
### [丰巢风波背后：王卫疑似为实控人，个人财富超千亿元](https://www.ithome.com/0/486/785.htm)
> 概要: 5月12日消息，3月以来海底捞陆续恢复堂食，但消费者却发现海底捞菜品涨价了；而同样涨价的还有明星企业西贝以及喜茶。不过，很快海底捞、西贝恢复原价，喜茶道歉。继海底捞等涨价风波后，近期快递柜丰巢科技引发......
### [顺丰偷袭外卖“珍珠港”](https://www.ithome.com/0/486/827.htm)
> 概要: 一个微妙的时间点，顺丰出手了。周末，5 月 10 日，顺丰推出了「丰食」平台，主打企业员工团餐。必胜客、德克士、和合谷以及吉野家等数十家知名餐饮企业目前已经入驻平台。同时，引人注意的是，对于入驻的品牌......
### [菜鸟裹裹宣布上线国际寄件：全国2小时上门](https://www.ithome.com/0/486/779.htm)
> 概要: 菜鸟裹裹今天对外宣布，联手递四方，已推出国际寄件服务，在全国2800多个区县，使用手机下单，2小时上门，口罩等物资就能寄达全球。据悉，目前，通过菜鸟裹裹APP，选择“寄国际&港澳台”下单，口罩（非医用......
### [刘涛入职阿里巴巴，年薪150万+超过欧阳娜娜](https://www.ithome.com/0/486/816.htm)
> 概要: IT之家5月12日消息 5月9日，刘涛发微博宣布，自己正式加入阿里巴巴大家庭，成为了聚划算官方优选官，花名刘一刀，并且晒出了工牌。近年来已有不少明星加盟阿里巴巴，例如花名矮大紧的高晓松，担任阿里娱乐战......
# 小说
### [洪荒剑祖](http://book.zongheng.com/book/349048.html)
> 作者：熊掌天下

> 标签：奇幻玄幻

> 简介：一剑风云万骨枯，铁血丹心仙魔舞。纵剑逍遥尽杀戮，须知剑道无坦途。手握一柄巨剑，带领一帮兄弟，杀得天界仙神心惊胆寒，杀得洪荒万魔闻风丧胆。终成一代不败剑祖，写下一段传奇，为后人传颂！（本小说为纵横中文网首发，请可爱的朋友们支持正版！拒绝盗版！）

> 章节末：第一百七十二章：魔祖降临万魔随，剑神回归天仙飞

> 状态：完本
### [黄庭道主](https://m.qidian.com/book/1012426341/catalog)
> 作者：妖僧花无缺

> 标签：游戏异界

> 简介：地球青年陆青峰因车祸意外死亡，穿越到仙侠世界，并获得两千年后地球联邦开发出的大型真实网游《洪荒》登陆戒指……PS：建了两个书友群VIP群（粉丝值3000+）：486411676普通群：676084011（已满）黄庭道主②群：964551655

> 章节总数：共781章

> 状态：完本
# 游戏
### [《终末的后宫》2021年TV动画化 99.9%女性的未来](https://www.3dmgame.com/news/202005/3788318.html)
> 概要: 著名后宫系悬疑漫画名作《终末的后宫》将制作TV动画预定2021年推出，5月12日今天日媒曝料了这个消息引发绅士们群情激昂，一起来期待下。·《终末的后宫》是成人漫画家宵野小太郎首部连载在《JUMP》系杂......
### [玩家评选《FF7重制版》女主排行 女装克劳德排了第4](https://www.3dmgame.com/news/202005/3788313.html)
> 概要: 史艾旗下大作《最终幻想7：重制版》刚刚发售了一段时间，相信不少玩家都在重温经典记忆中，而关于谁是第一女主角之争似乎从未平息，近日日媒就发动2千玩家评选《FF7重制版》女主排行，究竟谁人气登顶下文可知，......
### [简单粗暴！前泽有作再次豪掷10亿给1万单亲家庭发钱](https://www.3dmgame.com/bagua/3081.html)
> 概要: 日本巨佬富豪前泽有作之前已经多次无条件发钱引发世界性话题，近日宣布成立《前泽单亲支援基金》，首弹行动将向1万个单亲家庭每户发放10万日元补助，再次引发社会热议。·有着“日本思聪”外号的日本富豪前泽目前......
### [影之国紫发女王登场！邻家大姐姐斯卡哈手办公开](https://www.3dmgame.com/news/202005/3788347.html)
> 概要: 近日，由Plum推出的一款斯卡哈手办上架AmiAmi商城，这位角色来自于《FGO》，作为一名强力枪阶从者登场。该手办预计将于2020年6月晚些时候上市，目前，官方已经公开了该手办的一些介绍内容，以下为......
### [《LOL》外服测试服显示无限火力5月14日回归](https://ol.3dmgame.com/news/202005/26176.html)
> 概要: 《LOL》外服测试服显示无限火力5月14日回归
# 论文
### [Latent Replay for Real-Time Continual Learning](https://paperswithcode.com/paper/latent-replay-for-real-time-continual)
> 日期：2 Dec 2019

> 标签：

> 代码：https://github.com/lrzpellegrini/Latent-Replay

> 描述：Training deep networks on light computational devices is nowadays very challenging. Continual learning techniques, where complex models are incrementally trained on small batches of new data, can make the learning problem tractable even for CPU-only edge devices.
### [A Hierarchical Model for Data-to-Text Generation](https://paperswithcode.com/paper/a-hierarchical-model-for-data-to-text)
> 日期：20 Dec 2019

> 标签：DATA-TO-TEXT GENERATION

> 代码：https://github.com/KaijuML/data-to-text-hierarchical

> 描述：Transcribing structured data into natural language descriptions has emerged as a challenging task, referred to as "data-to-text". These structures generally regroup multiple elements, as well as their attributes.
### [PointASNL: Robust Point Clouds Processing using Nonlocal Neural Networks with Adaptive Sampling](https://paperswithcode.com/paper/pointasnl-robust-point-clouds-processing)
> 日期：1 Mar 2020

> 标签：None

> 代码：https://github.com/yanx27/PointASNL

> 描述：Raw point clouds data inevitably contains outliers or noise through acquisition from 3D sensors or reconstruction algorithms. In this paper, we present a novel end-to-end network for robust point clouds processing, named PointASNL, which can deal with point clouds with noise effectively.
