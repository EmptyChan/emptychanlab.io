---
title: 2021-01-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SpanishSeaSlug_EN-CN3198406214_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-09 20:29:52
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SpanishSeaSlug_EN-CN3198406214_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：向佐郭碧婷女儿出生一百天 开心晒出一家三口牵手照](http://slide.ent.sina.com.cn/star/slide_4_86512_350975.html)
> 概要: 组图：向佐郭碧婷女儿出生一百天 开心晒出一家三口牵手照
### [组图：没什么用的冷知识又增加！今天是斯内普教授的生日](http://slide.ent.sina.com.cn/film/slide_4_704_350944.html)
> 概要: 组图：没什么用的冷知识又增加！今天是斯内普教授的生日
### [组图：俞灏明穿羽绒服暖意现身长腿吸睛 剃寸头戴鸭舌帽清爽利落](http://slide.ent.sina.com.cn/star/slide_4_704_350963.html)
> 概要: 组图：俞灏明穿羽绒服暖意现身长腿吸睛 剃寸头戴鸭舌帽清爽利落
### [组图：华晨宇带助理深夜泡吧 酒吧公区悠闲吸烟动作娴熟](http://slide.ent.sina.com.cn/y/slide_4_704_350959.html)
> 概要: 组图：华晨宇带助理深夜泡吧 酒吧公区悠闲吸烟动作娴熟
### [为李菲儿发声？苏醒：不参加节目你养我啊](https://ent.sina.com.cn/s/m/2021-01-09/doc-iiznctkf0989645.shtml)
> 概要: 新浪娱乐讯 1月8日，苏醒在微博小号发文：“看到网上最好笑的一种评论就是，谁谁谁就不该去哪个节目，现在找个工作挣个钱还要在乎这些网友感受了么？不去，你养我么？”最后，苏醒补充：“说的我自己，给我自己加......
### [全员美人的聊斋剧，杨幂的小倩、蒋欣的蛇妖，都不敌最美的她](https://new.qq.com/omn/20210109/20210109A09FVU00.html)
> 概要: 聊斋的故事到今天已经拍了很多个版本，但有一版却一直让小扒念念不忘，那就是2005年版的《新聊斋志异》。小扒去复盘了一下，发现这里面居然全都是帅哥美女，很多演员现在都还活跃在娱乐圈甚至成为了顶流，不得不......
### [周末补剧｜来点硬核的！《黑白禁区》《风声》今晚大结局](https://new.qq.com/omn/20210109/20210109A09M5U00.html)
> 概要: 低温警告持续敲响但火热的追剧魂不可就此熄灭窝在暖暖的被子里必须得来点硬货才度过寒冷周末《黑白禁区》20点会员结局            身在黑暗，心向光明砥砺前行，守护安宁缉毒警的工作不到最后一刻仍会......
### [辛巴徒弟蛋蛋回归直播，首场带货被王海盯上，产品或为虚假宣传？](https://new.qq.com/omn/20210109/20210109A08RZ300.html)
> 概要: 近期有关网红辛巴的话题在网上持续引发关注，因为燕窝事件辛巴已经被国内各大媒体所关注，到如今辛巴依然处在封禁期，加上之前停播的一个月前后时间共有一百多天，对辛巴整个团队的影响非常大。虽然辛巴和时大漂亮目......
### [赵薇当年才是真顶流，出席活动交通瘫痪，农民工爬到树上为她应援](https://new.qq.com/omn/20210108/20210108A0C9IK00.html)
> 概要: 提起现在人气和热度居高不下的流量明星，网友们的态度褒贬不一，但是没有能否认他们的受欢迎，很多流量都曾经遭遇过因为活动现场人太多而不得不临时取消的经历，比如杨紫。            看着粉丝们疯狂的......
### [“蛇精男”刘梓晨在美国感染新冠，出现整容后遗症全脸毁容还患了抑郁症](https://new.qq.com/omn/20210109/20210109A055QW00.html)
> 概要: “蛇精男”刘梓晨在美国感染新冠，出现整容后遗症全脸毁容还患了抑郁症
# 动漫
### [骨头社原创动画《SK∞》公开先行预告！](https://news.dmzj.com/article/69842.html)
> 概要: 内海纮子×大河内一楼×骨头社，滑板题材原创TV动画预告公开。
### [《春心萌动的老屋缘廊》真人电影化决定！](https://news.dmzj.com/article/69843.html)
> 概要: 鹤谷香央理的漫画《春心萌动的老屋缘廊》真人电影化决定。
### [青梅竹马伪骨科，傲娇哥哥爱上叛逆妹妹](https://news.dmzj.com/article/69841.html)
> 概要: 首次Mark和Kao共同合作演绎《直到天空迎来太阳》，二人都是很亚洲人的长相，除了说话感到很泰之外，并不会很出戏。毕竟演技流畅，CP感十足，帅哥+甜妹就是最棒哒！值得夸赞的是，虽然又是泰式无脑小甜剧，可导演拍摄也没有松懈，画面清新不油腻，怼脸拍也...
# 财经
### [前董事长神秘“失联”后收关注函！公司不吭声股价大跌26%](https://finance.sina.com.cn/wm/2021-01-09/doc-iiznctkf1094854.shtml)
> 概要: 顺利办的董事长被“办”了。中国证券报微信公号1月7日刊发报道《诡异！董事长被警方带走11天，公司秘而不宣，股价大跌24.5%，有人先知先觉夺路而逃？》。
### [拼多多通报：一员工在家中跳楼自杀 将全力配合善后](https://finance.sina.com.cn/china/2021-01-09/doc-iiznezxt1523167.shtml)
> 概要: 痛心！又一个．．．．拼多多刚刚通报：一员工在家中跳楼自杀，入职仅半年，将全力配合善后！ 中国基金报 泰勒 拼多多最近真是“多事之秋”...
### [市场监管总局局长：促进市场公平竞争 推动反垄断法加快修订](https://finance.sina.com.cn/china/2021-01-09/doc-iiznezxt1519722.shtml)
> 概要: 原标题：市场监管总局局长：促进市场公平竞争，推动反垄断法加快修订 新华社北京1月9日消息，我国有亿户市场主体，“十四五”开局之年...
### [国家能源局：加强应急准备 确保电力安全稳定供应](https://finance.sina.com.cn/roll/2021-01-09/doc-iiznezxt1517875.shtml)
> 概要: 原标题：国家能源局：加强应急准备 确保电力安全稳定供应 国家能源局电力安全监管司司长童光毅表示，受多轮寒潮天气和经济持续稳定恢复等因素叠加影响...
### [饿了么回应骑手猝死事件：正在优化外卖骑手的保险结构](https://finance.sina.com.cn/china/2021-01-09/doc-iiznctkf1099406.shtml)
> 概要: 原标题：饿了么独家回应骑手猝死事件：正在优化外卖骑手的保险结构 近日，饿了么外卖员送餐时猝死一事备受关注。43岁的韩某伟倒在了送外卖的路上，再也没有醒来。
### [特斯拉引爆 新能车彻底火了！还能上车么？](https://finance.sina.com.cn/china/2021-01-09/doc-iiznezxt1519913.shtml)
> 概要: 特斯拉引爆，新能车彻底火了！还能上车么？五大绩优基金经理最新解盘来了 中国基金报记者方丽 新能车相关产业链成为近期A股市场“最亮的星星”。
# 科技
### [蔚来推出蓝点计划，推出首款轿车 NIO ET7，发布第二代换电站以及固态电池包等](https://www.ithome.com/0/529/143.htm)
> 概要: IT之家 1 月 9 日消息 在今日的 NIO Day 2020 活动上，蔚来推出了蓝点计划，李斌宣布蔚来将成为全球第一家帮助用户完成碳减排认证交易的汽车公司。蔚来今日正式推出了首款旗舰轿车 NIO ......
### [根本停不下来：卜珂肉松海苔卷 5 元 / 罐速囤](https://lapin.ithome.com/html/digi/529129.htm)
> 概要: 卜珂零点 肉松海苔卷 100g 报价9.9元，下单4件立减9.7元，叠加限量10元券，实付19.9元包邮，领券并购买。已经下单的小伙伴反馈味道不错，小编与同事们也觉得OK，值得试一试。19.9元4盒的......
### [Sriwijaya Air flight SJ182, a Boeing 737, has disappeared from radar](https://twitter.com/spectatorindex/status/1347856317839417345)
> 概要: Sriwijaya Air flight SJ182, a Boeing 737, has disappeared from radar
### [They spent 12 years solving a puzzle. It yielded the first Covid-19 vaccines](https://www.nationalgeographic.com/science/2020/12/these-scientists-spent-twelve-years-solving-puzzle-yielded-coronavirus-vaccines/)
> 概要: They spent 12 years solving a puzzle. It yielded the first Covid-19 vaccines
### [常被混淆的账号体系与账户体系](https://www.tuicool.com/articles/eIree2y)
> 概要: ​导语：账户体系是任意一款互联网产品都必有的基础体系，而账户体系的产品设计文章却寥寥无几。本文小编将从互联保险产品的账户体系入手，来聊一聊如何基于复杂业务场景构建一套完整账户体系。账户体系、账号体系、......
### [快手发力小游戏平台，挑战字节？](https://www.tuicool.com/articles/77n6Zb3)
> 概要: 经过两年多发展，随着游戏丰富度、列表效率不断提升，快手小游戏规模很难再往上走一个台阶。基于上述考量，快手开始考量主场景是不是会有更大的机会？因此，官方把快手小游戏升级成小程序游戏，为内容提供更多的功能......
# 小说
### [不死至尊](http://book.zongheng.com/book/395200.html)
> 作者：东坡居士

> 标签：奇幻玄幻

> 简介：九次冲击凝真境都失败的废物凌羽，一朝觉醒，体内宝血九炼，融合至宝——无字天书，涅槃九转而生，铸就无上武道根基，修炼绝世武学，练就通天神通，收罗天下珍宝，绝色红颜相伴，开始了他征战天下的传奇一生！

> 章节末：第二百三十五章 归一

> 状态：完本
# 游戏
### [Netflix《索尼克》3D动画将不会基于索尼克漫画](https://www.3dmgame.com/news/202101/3805931.html)
> 概要: 世嘉旗下的刺猬索尼克是游戏历史上最受欢迎的角色之一，除了电子游戏，它还衍生出了电影，漫画等作品。上个月，Netflix宣布将为索尼克打造3D动画。据悉，这部Netflix出品的3D《索尼克动画》将由S......
### [因太火热《天穗之咲稻姬》限界年数扩张至9999年](https://www.3dmgame.com/news/202101/3805935.html)
> 概要: 和风动作RPG《天穗之咲稻姬》在发售之后便迎来了玩家媒体的一致好评，人气急速上升。甚至连发行商Marvelous都表示“每天都在想是不是哪里弄错了”。此前还发生过农林水产省官方采访游戏团队的情况，更有......
### [主播引领游戏风潮？开放式沙盒游戏《腐蚀》焕发新生](https://www.3dmgame.com/news/202101/3805954.html)
> 概要: 《腐蚀（Rust）》的开发者Garry Newman近日在推特上透露，在过去的一周中，这款老游戏有两天的时候，在Steam平台上的收入超过了100万美元。作为一款2018年初发售的游戏，这个成绩非常不......
### [庄羽谈于正郭敬明道歉：系从业者联名抵制后被迫](https://www.3dmgame.com/news/202101/3805955.html)
> 概要: 2020年的最后一天，郭敬明在微博上发文为自己当年的小说《梦里花落知多少》抄袭庄羽的作品《圈里圈外》道歉，并称要把《梦里花落知多少》的版权收入全部交给庄羽或捐赠给公益机构。郭敬明2020年12月31日......
### [《新世纪福音战士 新剧场版：终》世界最速上映中止](https://www.3dmgame.com/news/202101/3805956.html)
> 概要: 继昨日《新世纪福音战士》官方宣布《新世纪福音战士 新剧场版：Q3.333》暂停上映后，今日（1.9）官方发布推文，宣布《新世纪福音战士 新剧场版：终》原定于1月23日0:00场的“世界最速上映”中止，......
# 论文
### [Cross-context News Corpus for Protest Events related Knowledge Base Construction](https://paperswithcode.com/paper/cross-context-news-corpus-for-protest-events)
> 日期：1 Aug 2020

> 标签：ACTIVE LEARNING

> 代码：https://github.com/emerging-welfare/glocongold

> 描述：We describe a gold standard corpus of protest events that comprise of various local and international sources from various countries in English. The corpus contains document, sentence, and token level annotations.
### [Network interventions for managing the COVID-19 pandemic and sustaining economy](https://pattern.swarma.org/paper?id=58cab3d0-24bf-11eb-83fd-0242ac1a000c)
> 概要: It has been challenging to identify nonpharmaceutical intervention strategies that reconcile two conflicting aims: Reducing the spread of infections while maintaining economic activities amid the coronavirus disease 2019 (COVID-19) pandemic. Commonly implemented strategies such as lockdowns or
