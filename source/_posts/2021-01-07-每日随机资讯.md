---
title: 2021-01-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WhiteCliffs_EN-CN0425774257_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-07 20:39:02
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WhiteCliffs_EN-CN0425774257_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [屈楚萧律师回应：女方索要钱财未果 恶意剪辑录音](https://ent.sina.com.cn/s/m/2021-01-07/doc-iiznctkf0529241.shtml)
> 概要: 新浪娱乐讯 1月6日，自称屈楚萧女友的网友曝光疑似屈楚萧及其经纪人让她发澄清微博的录音，录音中经纪人表示可以在网上毁了女方，以后工作恋爱都会受到很大影响等，引发网友热议。刚刚，屈楚萧委托律师在微博发文......
### [杨天真谈不当经纪人：承受不了委屈所以放弃](https://ent.sina.com.cn/s/m/2021-01-07/doc-iiznezxt1055873.shtml)
> 概要: 新浪娱乐讯 近日，杨天真在一次活动中谈到自己不当经纪人原因，表示当经纪人需要承受很多委屈，承受不了所以放弃了，现在发现是个正确的选择。她还表示，很不开心就会买包，再不开心的事都能过了......
### [组图：汪峰晒全家陪章子怡拍戏旧照 母女扮医生病人氛围欢乐](http://slide.ent.sina.com.cn/star/slide_4_704_350865.html)
> 概要: 组图：汪峰晒全家陪章子怡拍戏旧照 母女扮医生病人氛围欢乐
### [2020影视公司成绩两极分化 2021重磅剧集都在这儿](https://ent.sina.com.cn/v/m/2021-01-07/doc-iiznezxt1063537.shtml)
> 概要: 刚刚过去的2020年，影视行业在震荡中寻求破局。突如其来的新冠肺炎疫情让整个行业停摆数月之久，在拍影视项目被迫停工和延期，影视公司资金链紧张，全年有上千家影视类公司注销。但困境也蕴含了机遇，疫情使得线......
### [组图：南柱赫旧照撞脸杨迪 身穿校服面庞青涩](http://slide.ent.sina.com.cn/star/k/slide_4_704_350884.html)
> 概要: 组图：南柱赫旧照撞脸杨迪 身穿校服面庞青涩
### [吴宗宪台上对女儿又亲又摸，吴姗儒表情抗拒，连番阻挡无效超尴尬](https://new.qq.com/omn/20210107/20210107A09CQ300.html)
> 概要: 1月7日，有媒体在社交平台晒出了一段吴宗宪和女儿吴姗儒同台主持的视频，在台上，吴宗宪或许是为了活跃气氛的缘故，主动调侃女儿，称她穿得要去旅游一样，还直言你是我在台上唯一可以乱摸的女艺人，之后竟然还真的......
### [张柏芝带三儿子去游乐场，小Q逗弟弟超暖心，Lucas正脸太像谢霆锋](https://new.qq.com/omn/20210107/20210107A0CWLA00.html)
> 概要: 1月7日张柏芝罕晒出日常，分享和三个儿子在游乐场所尽情嗨玩的画面，顺带宣传自己的品牌产品，张柏芝录制《乘风破浪的姐姐2》，闲暇之余还有空闲陪着孩子们玩耍，幸福不过如此。            次子qu......
### [郑爽不惧严寒粉色纱裙走红毯，生图曝光状态能打，被赞仙女下凡](https://new.qq.com/omn/20210107/20210107A0E1OH00.html)
> 概要: 郑爽不惧严寒粉色纱裙走红毯，生图曝光状态能打，被赞仙女下凡
### [李小璐与朋友聚餐，下巴突兀被疑又整容，戴定制项链镶满耀眼钻石](https://new.qq.com/omn/20210107/20210107A08IWT00.html)
> 概要: 1月6日，李小璐在社交平台上更新了一组照片，和大家分享自己的近况。            当天，李小璐身穿粉红色卫衣，脸上化着粉嘟嘟的妆容，高挺的鼻梁尖尖的下巴，相当精致，她头上还有一个毛茸茸的头饰，......
### [好友泄露马国明汤洛雯婚期：得奖与否都会结婚，曝两人很想生孩子](https://new.qq.com/omn/20210107/20210107A0CDSN00.html)
> 概要: 1月7日，TVB艺人黎诺懿出席活动，被问到好兄弟马国明是否拿到本届视帝之后，就会迎娶女友汤洛雯，黎诺懿毫不留情揭穿：无论马国明有没有得奖，他都会跟汤洛雯结婚，两人都想生孩子了。            ......
# 动漫
### [P站美图推荐——雪景特辑](https://news.dmzj.com/article/69816.html)
> 概要: 皑皑白雪和冬装的美少女，简直OK顶呱呱
### [动画《银魂THE SEMI-FINAL》万事屋篇预告片与先行图](https://news.dmzj.com/article/69826.html)
> 概要: 根据空知英秋原作制作的动画特别篇《银魂 THE SEMI-FINAL》公开了即将于15日播出的第一话万事屋篇的预告和先行图。
### [FuRyu《摇曳露营△》各务原抚子和服版1/7比例手办](https://news.dmzj.com/article/69820.html)
> 概要: FuRyu根据大人气动画《摇曳露营△》中的各务原抚子制作的和服版1/7比例手办目前已经开订了。本作采用了抚子身穿振袖和服，拿着手提包和苹果糖欢度新年的样子。另外本作还可以和另行发售的志摩凛和服版放在一起装饰。
### [男子因伪造中奖冰棍儿棍换宝可梦卡被逮捕](https://news.dmzj.com/article/69822.html)
> 概要: 赤城乳业在1月6日发布了一名涉嫌伪造冰棍儿“GariGari君”中奖棍的男子被警方逮捕的消息。
### [“兵马俑侠”是如何炼成的？《秦侠》纪录片制作特辑上线](https://new.qq.com/omn/ACF20210/ACF2021010701027000.html)
> 概要: 好动画的灵魂是什么？是故事啊！那么一个跌宕起伏的好故事、一群鲜活有趣的角色是如何诞生的？今天就带你走进《秦侠》炼成记！由《秦侠》动画监制熊猫盖饭和动画导演陈烨，为你揭秘“兵马俑侠”是如何诞生的！Ps......
### [仙界打工人！《我是大神仙》泰岳门篇开启！](https://new.qq.com/omn/ACF20210/ACF2021010700929800.html)
> 概要: 月棱镜威力·巴啦啦能量·炫光舞法·哈啊啊啊啊啊啊啊·封印解除·变身！在无数漫迷的翘首以盼中，时·魔法少女·游戏高玩·七岁神童·仙界大佬预备役·江，终于念完了这长达N集的变身咒，一举从寂寂无名的废印村考......
### [《美少女战士 Eternal》后篇预告公开 2月11日上映](https://acg.gamersky.com/news/202101/1352698.shtml)
> 概要: 《美少女战士 Eternal》今日公开后篇预告，将于2月11日在日本上映。
### [《魔法少女小圆》十周年纪念企划 官网上线特别活动](https://acg.gamersky.com/news/202101/1352734.shtml)
> 概要: 今日是《魔法少女小圆》播出10周年，官方开启了十周年纪念界面。
# 财经
### [大连:进口冷链食品从业人员入职需进行14天健康观察和2次核酸检测](https://finance.sina.com.cn/china/gncj/2021-01-07/doc-iiznctkf0695448.shtml)
> 概要: 原标题：大连：进口冷链食品从业人员入职前需进行14天健康观察和2次核酸检测 1月7日下午，大连市召开疫情防控工作新闻发布会，大连市卫生健康委副主任赵连对严格执行冷库冷...
### [最新！石家庄人员车辆，均不得出市](https://finance.sina.com.cn/wm/2021-01-07/doc-iiznezxt1110155.shtml)
> 概要: 今天（7日）下午，石家庄召开第三场新闻发布会称，在全员核酸检测中，再次发现11例阳性样本。 而且，鉴于目前石家庄正在进行全员核酸检测，全市所有车辆及人员均不出市。
### [外汇局公布数据显示：11月末外储升幅1.2% 总体将稳定](https://finance.sina.com.cn/china/bwdt/2021-01-07/doc-iiznctkf0697634.shtml)
> 概要: 原标题：11月末外储升幅1.2%，总体将稳定 1月7日，外汇局公布数据显示，截至2020年12月末，我国外汇储备规模为32165亿美元，较11月末上升380亿美元，升幅为1.2%。
### [中国社科院发布绿皮书：大学毛入学率接近50%](https://finance.sina.com.cn/china/gncj/2021-01-07/doc-iiznezxt1112419.shtml)
> 概要: 原标题：中国社科院发布绿皮书：大学毛入学率接近50%，我国即将进入高等教育普及化阶段 每经记者 李彪 北京摄影报道 “未来中国高质量发展最重要的就是要解决人力资本的问题...
### [央行、外汇局：企业跨境融资宏观审慎调节参数由1.25下调至1](https://finance.sina.com.cn/china/gncj/2021-01-07/doc-iiznctkf0696179.shtml)
> 概要: 原标题：央行、外汇局：企业跨境融资宏观审慎调节参数由1.25下调至1 证券时报e公司讯，央行1月7日消息，中国人民银行、国家外汇管理局决定将企业的跨境融资宏观审慎调节参...
### [广州逾6个月拖欠燃气费、生活垃圾费将被信用扣分](https://finance.sina.com.cn/china/dfjj/2021-01-07/doc-iiznezxt1109359.shtml)
> 概要: 原标题：广州逾6个月拖欠燃气费、生活垃圾费将被信用扣分 3月1日起广州正式实施《广州市城市管理领域公共信用信息分级使用管理办法》，其中办法规定...
# 科技
### [飞腾详解首款 8 核桌面处理器 D2000：性能翻倍，相关笔记本 Q1 上市](https://www.ithome.com/0/528/810.htm)
> 概要: IT之家1月7日消息 上月底，在 2020 飞腾生态伙伴大会上，飞腾桌面处理器芯片腾锐 D2000 发布。今天，飞腾官方对这款处理器进行了详细介绍，并公布了应用进展。首先，在参数方面，腾锐 D2000......
### [SpaceX 星际飞船原型 SN9 完成首次静态点火测试，发动机点亮大约一秒钟](https://www.ithome.com/0/528/757.htm)
> 概要: 1 月 7 日消息，美国东部时间周三下午 5 时 7 分，在太空探索公司 SpaceX 位于德克萨斯州南部的博卡奇卡设施，星际飞船原型 SN9 顺利完成首次静态点火测试，三部猛禽发动机同时点亮了大约一......
### [A new SaaS metric for demonstrating the ROI of community](https://orbit.love/blog/whats-your-communitys-nrg)
> 概要: A new SaaS metric for demonstrating the ROI of community
### [A developer's perspective: the problem with screen reader testing](https://jaketracey.com/a-developers-perspective-the-problem-with-screen-reader-testing/)
> 概要: A developer's perspective: the problem with screen reader testing
### [站在风口上，冰火两重天，企业服务2021年如何破局？](https://www.tuicool.com/articles/fUreA3Z)
> 概要: 编辑导语：随着时代的发展和变化，企业服务市场一直也是各大企业热衷的一个领域，从如今的形势来看，国内的企业服务市场还有很大的发展空间；本文作者分享了关于新的一年企业服务行业的思考，我们一起来看一下。在过......
### [从Aha时刻来看新用户激活](https://www.tuicool.com/articles/6jyINjr)
> 概要: 编辑导读：Aha时刻是新用户第一次认识到产品的价值，脱口而出“啊哈，原来这个产品可以帮我做这个”。当花了大力气吸引来了一波新用户时，如何帮助留存转化，就要依靠Aha时刻。本文作者将围绕Aha时刻进行三......
# 小说
### [突然无敌了](https://m.qidian.com/book/1013032437/catalog)
> 作者：踏仙路的冰尘

> 标签：衍生同人

> 简介：说出来你可能不信，我突然就无敌了……PS：读者大大群1：572671454读者大大群2：777626707

> 章节总数：共592章

> 状态：完本
# 游戏
### [斗鱼2020年度十大弹幕：就这 针不戳 芜湖起飞上榜](https://www.3dmgame.com/news/202101/3805822.html)
> 概要: 弹幕是直播的看点之一，近日游戏直播平台斗鱼发布了2020年度十大弹幕。“就这、针不戳、芜湖起飞、上流、痛苦面具、我接受不了、‘歪比歪比、歪比巴卜’、找个班上吧、究极长痛、肉蛋葱鸡”当选年度弹幕TOP1......
### [MSI微星电竞产品震撼CES 2021 荣获多项创新大奖](https://3c.3dmgame.com/show-14-13545-1.html)
> 概要: 全球电竞及创新商务的知名品牌MSI微星科技在CES2021表现亮眼，旗下全新系列的笔记本及电脑产品分别在三个类别拿下CES创新大奖，一举收获多个奖项。MSI微星科技行销副总经理程惠正表示：“MSI微星......
### [最壕“年终奖”出炉！雷军这次下了血本：史无前例](https://www.3dmgame.com/news/202101/3805838.html)
> 概要: 1月7日，2020年小米百万美金技术大奖揭晓，经过小米集团技术委员会多轮评选，两支年轻的工程师团队脱颖而出：手机部小米秒充团队、软件与体验部的MIUI隐私保护团队，他们凭借极致的创新、领先的硬核技术，......
### [为电竞干杯 微星海皇戟闪耀BW2020广州站](https://3c.3dmgame.com/show-42-13544-1.html)
> 概要: 前不久，由bilibili官方举办的线下盛会Bilibili World 2020广州站在广州广交会展馆盛大召开，微星作为本次BW的参展商不仅展出了旗下的诸多精品，还对现场赛事提供了硬件支持。氪金枪2......
### [剧集《上阳赋》预告片发布 章子怡主演首部电视剧](https://www.3dmgame.com/news/202101/3805797.html)
> 概要: 由章子怡领衔主演的古装历史剧《上阳赋》，今日（1月7日）发布正式预告片，这是章子怡主演的首部电视剧，该剧的具体开播时间尚未确认。预告片：《上阳赋》由侯咏、程源海执导，章子怡领衔主演，周一围、于和伟、左......
# 论文
### [BOP Challenge 2020 on 6D Object Localization](https://paperswithcode.com/paper/bop-challenge-2020-on-6d-object-localization)
> 日期：15 Sep 2020

> 标签：6D POSE ESTIMATION USING RGB

> 代码：https://github.com/ylabbe/cosypose

> 描述：This paper presents the evaluation methodology, datasets, and results of the BOP Challenge 2020, the third in a series of public competitions organized with the goal to capture the status quo in the field of 6D object pose estimation from an RGB-D image. In 2020, to reduce the domain gap between synthetic training and real test RGB images, the participants were provided 350K photorealistic trainining images generated by BlenderProc4BOP, a~new open-source and light-weight physically-based renderer (PBR) and procedural data generator.
### [UNION: An Unreferenced Metric for Evaluating Open-ended Story Generation](https://paperswithcode.com/paper/union-an-unreferenced-metric-for-evaluating)
> 日期：16 Sep 2020

> 标签：TEXT GENERATION

> 代码：https://github.com/thu-coai/UNION

> 描述：Despite the success of existing referenced metrics (e.g., BLEU and MoverScore), they correlate poorly with human judgments for open-ended text generation including story or dialog generation because of the notorious one-to-many issue: there are many plausible outputs for the same input, which may differ substantially in literal or semantics from the limited number of given references. To alleviate this issue, we propose UNION, a learnable unreferenced metric for evaluating open-ended story generation, which measures the quality of a generated story without any reference.
