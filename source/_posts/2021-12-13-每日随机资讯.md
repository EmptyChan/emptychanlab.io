---
title: 2021-12-13-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ElPanecilloHill_ZH-CN0527709139_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-12-13 21:18:09
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ElPanecilloHill_ZH-CN0527709139_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [RuoYi-Cloud 3.3.0 发布，支持 Vue3](https://www.oschina.net/news/173405/ruoyi-cloud-3-3-0-released)
> 概要: 2021云上架构与运维峰会将于12月10日13：00线上直播，欢迎大家踊跃报名参会 >>>>>若依 Cloud 微服务版本v3.3.0 已发布，更新日志：新增配套并同步的Vue3前端版本新增认证对象简......
### [集群列表集成 KubePi，KubeOperator 开源容器平台 v3.12.0 发布](https://www.oschina.net/news/173529/kubeoperator-3-12-0-released)
> 概要: 2021云上架构与运维峰会将于12月10日13：00线上直播，欢迎大家踊跃报名参会 >>>>>12月13日，开源容器平台KubeOperator正式发布v3.12.0版本。在这一版本中，KubeOpe......
### [Linux Mint 20.3 Beta 发布，新外观、新应用](https://www.oschina.net/news/173392/linux-mint-20-3-beta-released)
> 概要: 2021云上架构与运维峰会将于12月10日13：00线上直播，欢迎大家踊跃报名参会 >>>>>Linux Mint 20.3 "Una" 的测试版现已提供下载，并计划于圣诞节假期附近正式发布。该系列基......
### [交付，行云流水：Zadig V1.7 系列版本发布，搞定权限管理、支持 SSO](https://www.oschina.net/news/173501/zadig-1-7-0-released)
> 概要: 2021云上架构与运维峰会将于12月10日13：00线上直播，欢迎大家踊跃报名参会 >>>>>“交付，行云流水”经过二个月的研发迭代Zadig V1.7 版本终于发布了V1.7.0 支持权限管理、统一......
### [This website has 81% battery power remaining](https://solar.lowtechmagazine.com/power.html)
> 概要: This website has 81% battery power remaining
### [深入剖析分布式事务一致性](https://segmentfault.com/a/1190000041106277?utm_source=sf-homepage)
> 概要: 概述分布式事务是用来解决跨数据库、跨服务更新数据一致性问题的。那么这里的一致性指的是什么，什么是强一致性，什么是弱一致性，与CAP理论中的一致性概念是一样的吗？本文将为您深入解答相关的问题。一致性指什......
### [这本漫画真厉害2022获奖结果 12月第2周新闻汇总](https://news.dmzj.com/article/73030.html)
> 概要: 这次准备的是这本漫画真厉害2022公开获奖结果，过去的VTuber之王“绊爱”2022年2月停止活动，“Yahoo!检索大奖”公开年度人物榜，英国推出高还原度人形机器人，最后还是惯例的新作相关消息。
### [Everyone on Earth is your cousin (2015)](https://qz.com/557639/everyone-on-earth-is-actually-your-cousin/)
> 概要: Everyone on Earth is your cousin (2015)
### [动画《打工吧！魔王大人》第二季7月开播](https://news.dmzj.com/article/73035.html)
> 概要: TV动画《打工吧！魔王大人》宣布了第二季将于2022年7月开始播出的消息。本作的新宣传图和PV也一并公开。
### [动画《魔王学院的不适合者》宣布变更主角声优](https://news.dmzj.com/article/73036.html)
> 概要: TV动画《魔王学院的不适合者 ～史上最强的魔王始祖、转生之后入学到子孙们的学校～》宣布了变更第二部中阿诺斯·波鲁迪戈乌多的声优。梅原裕一郎将代替在第一季中，为阿诺斯配音的铃木达央。
### [Chemical emitted by babies could make men more docile, women more aggressive](https://www.science.org/content/article/chemical-emitted-babies-could-make-men-more-docile-women-more-aggressive)
> 概要: Chemical emitted by babies could make men more docile, women more aggressive
### [“买买买”的水电之王，稳稳的10%幸福](https://www.huxiu.com/article/480800.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年11月29日盘前，长江电力（600900.SH）发布《筹划重大资产重组的停牌公告》。重组事项为乌东德、白鹤滩电站资产的注入。根据12月11日年的《预案》，......
### [演员北村谅与声优田野麻美结婚](https://news.dmzj.com/article/73038.html)
> 概要: 出演过舞台剧《刀剑乱舞》系列的演员北村谅，在本日（13日）宣布了和声优田野麻美结婚的消息。
### [半导体人没有中年焦虑](https://www.huxiu.com/article/481011.html)
> 概要: 本文作者：吴先之，编辑：王潘，头图来自：视觉中国姚禅之在35岁时入职一家互联网公司，2016年的互联网人还不知道“中年焦虑”是何物。当时王兴嘴里蹦出“下半场”的概念，外界大多以为是危言耸听之论。那时的......
### [元宇宙还没捂热，Web3又是什么鬼](https://www.huxiu.com/article/481032.html)
> 概要: 本文来自微信公众号：阑夕（ID：techread），作者：阑夕，题图来自：视觉中国互联网前沿概念快要不够用了。曾经短暂担任过美国货币监理署代理署长的Brian Brook，在最近一场国会听证会上向议员......
### [漫改日剧「青春方程式」宣传图公布](http://acg.178.com/202112/433360758325.html)
> 概要: 近日，安达充原作短篇集漫画「青春方程式」宣布将制作真人日剧，并公开了最新宣传图，本作预计将于2022年3月1日正式播出。CAST&STAFF出演：大平祥生、川尻蓮、川西拓実、木全翔也、金城碧海、河野純......
### [网剧《巫师2》分享希里训练片段 正片本周五开播](https://www.3dmgame.com/news/202112/3830676.html)
> 概要: Netflix网剧《巫师》第二季的官方推特分享了电视剧的一个小片段，官方表示这只是给粉丝们开开胃。电视剧将于12月17日正式开播，第三季将于2022年第一季度进入制作阶段。网剧片段官方分享的片段里，希......
### [《地下城与勇士：决斗》暗刃演示 12月18-20日公测](https://www.3dmgame.com/news/202112/3830680.html)
> 概要: 今日（12月13日），格斗游戏《地下城与勇士：决斗》油管官方频道公布了《地下城与勇士：决斗》“暗刃”演示此外官方宣布将于12月18日~20日期间在PS4及PS5平台上实施公测。《地下城与勇士：决斗》“......
### [视频：昆凌晒女儿的信求助如何准备圣诞礼物 周杰伦转发动态出主意](https://video.sina.com.cn/p/ent/2021-12-13/detail-ikyamrmy8659921.d.html)
> 概要: 视频：昆凌晒女儿的信求助如何准备圣诞礼物 周杰伦转发动态出主意
### [奥特曼“兼职”：希卡利是快递员，捷德是招待，你还知道哪些？](https://new.qq.com/omn/20211212/20211212A04BJO00.html)
> 概要: 奥特曼主要的职业是打怪兽，但是，奥特曼也有兼职的。下面给大家介绍。            赛罗奥特曼是宇宙警察。赛罗自从多次打败贝利亚后，获得光之国的支持，加入了宇宙警备队。赛罗经常在其他奥特曼客串，......
### [漫画「我的首推是恶役大小姐」第3卷封面公开](http://acg.178.com/202112/433367992514.html)
> 概要: 漫画「我的首推是恶役大小姐」公开了第3卷的封面图，该卷将于12月18日发售。漫画「我的首推是恶役大小姐」改编自いのり原作，花ヶ田插画的轻小说作品，由青乃下负责作画，连载于一迅社旗下漫画杂志「Comic......
### [我的年终奖，等不到了](https://www.huxiu.com/article/481067.html)
> 概要: 作者：宛其、唐亚华、李秋涵、王敏、邹帅，编辑：宛其，题图来自：视觉中国年终临近，又到了职场人掰着手等待发放年终奖的日子。的确，BOSS直聘此前发布的《2020年职场人年终奖调查报告》显示，在表明将下发......
### [动画「最游记 RELOAD -ZEROIN-」BD早期预约特典图公开](http://acg.178.com/202112/433372072098.html)
> 概要: 电视动画「最游记 RELOAD -ZEROIN-」公开了由峰仓和也绘制插画的BD早期预约特典图。上卷特典为玄奘三蔵＆孙悟空，下卷特典为沙悟净＆猪八戒。该动画将于2022年1月6日开始播出。STAFF监......
### [「咒术回战0」乙骨忧太/特级咒灵祈本里香开订](http://acg.178.com/202112/433372079390.html)
> 概要: F：NEX作品「咒术回战0」乙骨忧太/特级咒灵祈本里香开订，全高约315mm，主体采用ABS和PVC材料制造。该手办定价为59950日元（含税），约合人民币3359元，预计于2022年12月发售......
### [韩剧收视：《太宗李芳远》火爆 《那年我们》上榜](https://ent.sina.com.cn/v/j/2021-12-13/doc-ikyakumx3865098.shtml)
> 概要: （韩国全国收视统计；数据来源：agb尼尔森）　　1、周末剧 KBS2 31.5%　　2、日日剧 KBS2 18.9%　　3、日日剧《国家代表老婆》 KBS1 14.4%　　4、金土剧 MBC 10.5......
### [奥斯卡得主哈莉贝瑞谈父亲 称已原谅其虐待恶行](https://ent.sina.com.cn/s/u/2021-12-13/doc-ikyamrmy8692125.shtml)
> 概要: 新浪娱乐讯 据外媒报道，55岁的奥斯卡奖得主哈莉贝瑞（Halle Berry）在做客《新鲜空气》（Fresh Air）播客节目时谈到了她导演的处女作——《挫伤》（她在其中担任主演）中描述的虐待关系，虐......
### [这十条金钱法则，帮助你避免成为钱的奴隶](https://www.tuicool.com/articles/qy6zMvN)
> 概要: 神译局是36氪旗下编译团队，关注科技、商业、职场、生活等领域，重点介绍国外的新技术、新观点、新风向。编者按：在货币不断贬值的今天，想要靠攒钱实现财务自由显然是不切实际的。制定正确的法则，及时调整自己和......
### [组图：刘雯羊羔毛大衣配皮裤个性时髦 搭经典球鞋走路带风](http://slide.ent.sina.com.cn/star/slide_4_704_364543.html)
> 概要: 组图：刘雯羊羔毛大衣配皮裤个性时髦 搭经典球鞋走路带风
### [《双人成行》只能2人玩？樱井政博一人或许也可以](https://www.3dmgame.com/news/202112/3830708.html)
> 概要: 几天前黑马游戏《双人成行》斩获TGA年度游戏大奖，立刻引发全球玩家关注，原本就是一个约定双人同乐乐的游戏，不过总有人表示独狼惯了一人偏要搞定，《大乱斗》之父樱井政博就晒出了自己的独门绝技，看看一个人怎......
### [高标准住宅，房地产的“救星”？](https://www.tuicool.com/articles/a2YV7vf)
> 概要: 本文来自微信公众号：经济观察网（ID：eeojjgcw），作者：符小茵，原文标题：《房企投身高标准住宅，到底划不划的来？》，头图来自：视觉中国苏志对来售楼处看样板间的购房者讲起了建筑技法，这在他6年的......
### [MCN不是网红们的救世主](https://www.tuicool.com/articles/QreeInz)
> 概要: 撰文 | 狸 七编辑 | 白 望“签约之后，轻松月入过万，能火还有钱赚。”在up主胡基森的视频里，MCN拿着“有流量扶持、孵化百万级账号”的噱头，忽悠博主签约，盘问后才知，对方只是想空手套白狼。图：来......
### [库克透露苹果投资三大方向：暗示 AR 头显和自动驾驶汽车](https://www.ithome.com/0/592/135.htm)
> 概要: IT之家12 月 13 日消息，根据苹果首席执行官蒂姆-库克最近的一次采访，苹果公司正重点专注三大领域。库克在采访中说：“我们非常关注增强现实，我们非常关注人工智能，我们非常关注自动驾驶。”库克将这些......
### [京阿尼《Free!》新剧场版后篇预告 4月22日上映](https://www.3dmgame.com/news/202112/3830721.html)
> 概要: 京都动画（京阿尼）旗下游泳番经典名作《Free！》全新动画电影《Free!–the Final Stroke–》分为前后2篇上映，前篇已经于9月17日上映，后篇将于2022年4月22日上映，日前官方公......
### [去河南寻宝的陨石猎人，连灭霸都能扒出来](https://www.tuicool.com/articles/AfaQfu7)
> 概要: 本文来自微信公众号：不相及研究所（ID：buuuxiangji），作者：发财金刚，题图来自：视觉中国和古老而富有神秘色彩的“赏金猎人”相比，陨石猎人只能算新兴职业，这一工种出现的年份和方便面的发明时间......
### [视频：林依晨自曝日常帮女儿洗澡喂奶 连化妆的时间都没有](https://video.sina.com.cn/p/ent/2021-12-13/detail-ikyakumx3902974.d.html)
> 概要: 视频：林依晨自曝日常帮女儿洗澡喂奶 连化妆的时间都没有
### [日本近海出现密密麻麻鱼类尸体：原因尚不明确](https://www.3dmgame.com/news/202112/3830729.html)
> 概要: 综合国内媒体报道，据日本放送协会(NHK)消息，当地时间12日上午，日本青森县的近海处，发现大量类似于沙丁鱼的死鱼尸体，目前死亡原因不明。报道称，当地时间12日上午，日本海上自卫队发现大量死鱼漂浮在海......
### [曝《霍格沃兹：遗产》将参加 PS 展会，原定在 TGA 公布后被《神奇女侠》CG 替换](https://www.ithome.com/0/592/148.htm)
> 概要: IT之家12 月 13 日消息，距离华纳公布《霍格沃兹：遗产》游戏已经过去了一年多，在这段时间内，该作并没有太多实质性的新内容公布。今日，推特用户 Latest PS5 爆料称，《霍格沃兹：遗产》可能......
### [中国电信子公司“天翼数字生活”加入 openEuler 欧拉开源社区](https://www.ithome.com/0/592/149.htm)
> 概要: IT之家12 月 13 日消息，近日，天翼数字生活科技有限公司签署 CLA（Contributor License Agreement 贡献者许可协议），正式加入欧拉开源社区。据介绍，天翼数字生活科技......
### [重庆江北区市场监管局约谈美团、饿了么：只重网络外卖业务发展，自身检查机制不完善](https://www.ithome.com/0/592/152.htm)
> 概要: IT之家12 月 13 日消息，重庆江北区市场监管局近日召集美团、饿了么两家网络外卖平台片区负责人，开展落实网络外卖平台责任行政指导会。江北区市场监管局相关负责人表示，今年“网监行动”以来，市场监管部......
### [Deno Joins TC39](https://deno.com/blog/deno-joins-tc39)
> 概要: Deno Joins TC39
### [最懂女人的导演关锦鹏：16部导演作品，捧出5位影帝影后，拿了11个奖](https://new.qq.com/rain/a/20211213A0B6GQ00)
> 概要: 三八一一，我在老地方等你，如花。《胭脂扣》2021年10月22日开始在内地上映的文艺剧情片《第一炉香》，虽然云集了马思纯、俞飞鸿、彭于晏、张钧甯、张佳宁和范伟、秦沛等优秀演员，仍挡不住它口碑和票房的双......
### [赵丽颖王一博新剧遭恶意控评，央视点名曝水军乱象：接1单最高赚1元](https://new.qq.com/rain/a/20211213A0BCWV00)
> 概要: 今日，央视以新闻报道的方式披露了影视评论圈的“水军乱象”。            据悉，近日里有两部剧接连出现了莫名其妙地“超前点评”的现象，在某个针对影视剧点评打分的社区平台上，有大量ID账号在两部......
### [市场监管总局：8批次食品抽检不合格 涉农药残留超标等](https://finance.sina.com.cn/china/gncj/2021-12-13/doc-ikyamrmy8752244.shtml)
> 概要: 原标题：市场监管总局：8批次食品抽检不合格 涉农药残留超标等 据国家市场监督管理总局网站13日消息，近期，市场监管总局组织食品安全监督抽检，抽取餐饮食品...
### [屠光绍详解上海国际金融中心建设3.0版，进一步扩大金融制度型开放是关键](https://finance.sina.com.cn/china/gncj/2021-12-13/doc-ikyamrmy8752821.shtml)
> 概要: 作者：林洁琛 ▪ 段思宇 要进一步扩大金融制度型开放，这是金融开放中更基础性、更具长远意义的、更可持续的开放。 继2020年上海基本建成与中国经济实力以及人民币国际地位...
### [央视曝光水军控评，王一博赵丽颖新剧被殃及，《小敏家》疑买水军](https://new.qq.com/rain/a/20211213A0BJNA00)
> 概要: 接近年底，各大平台都开始播出重磅剧集，《风起洛阳》、《谁是凶手》、《小敏家》等剧的热播背后引出的刷好评、控评等相关事件，也吸引了媒体的关注。近日，央视新闻频道对这些争议进行了相关报道。        ......
### [国家卫生健康委：落实好三孩生育政策 积极应对人口老龄化](https://finance.sina.com.cn/china/gncj/2021-12-13/doc-ikyakumx3927211.shtml)
> 概要: 原标题：国家卫生健康委：落实好三孩生育政策 积极应对人口老龄化 上证报中国证券网讯 据国家卫生健康委12月13日消息，今天上午，国家卫生健康委党组召开会议...
### [财经TOP10|紫光集团重整细节曝光，清偿方案三选一](https://finance.sina.com.cn/china/caijingtop10/2021-12-13/doc-ikyakumx3930419.shtml)
> 概要: 【政经要闻】 NO.1李克强：加大对中小微企业和个体工商户财政金融支持力度 国务院总理李克强12月13日上午在中南海紫光阁视频会见世界银行行长马尔帕斯。
### [天津从入境人员中检出新冠病毒奥密克戎变异株，为中国内地首次检出](https://finance.sina.com.cn/china/gncj/2021-12-13/doc-ikyakumx3929650.shtml)
> 概要: 记者12月13日从天津市疫情防控指挥部获悉，市疾病预防控制中心对12月9日我市境外输入新冠病毒无症状感染者呼吸道标本进行新冠病毒全基因组测序和序列分析...
### [天津从入境人员中检出奥密克戎变异株！为中国内地首次检出](https://finance.sina.com.cn/china/gncj/2021-12-13/doc-ikyakumx3929313.shtml)
> 概要: 来源：天津日报 记者12月13日从天津市疫情防控指挥部获悉，市疾病预防控制中心对12月9日我市境外输入新冠病毒无症状感染者呼吸道标本进行新冠病毒全基因组测序和序列分析...
### [年底大剧扎堆上线，题材各异，哪部更有“爆款相”？](https://new.qq.com/rain/a/20211213A0BY1N00)
> 概要: 临近年底，国产剧突然开启了扎堆上线的模式。不管是主打古装传奇路线的《风起洛阳》、《雪中悍刀行》，还是现实题材上轻车熟路的《小敏家》、另辟蹊径的《女心理师》，哪怕是类型剧也先后上马了谍战剧《也平凡》和《......
# 小说
### [仙凡神宇](http://book.zongheng.com/book/994177.html)
> 作者：芷上草

> 标签：武侠仙侠

> 简介：一株与天地宇宙同生的青莲，一个万世孤清的凡人，当两道目光上下相对时，世间也就没有孤独了…青莲证道，凡人开花！！不一样的仙侠，不一样的人生！！

> 章节末：完本感言

> 状态：完本
# 论文
### [AVA-AVD: Audio-visual Speaker Diarization in the Wild | Papers With Code](https://paperswithcode.com/paper/ava-avd-audio-visual-speaker-diarization-in)
> 日期：29 Nov 2021

> 标签：None

> 代码：None

> 描述：Audio-visual speaker diarization aims at detecting ``who spoken when`` using both auditory and visual signals. Existing audio-visual diarization datasets are mainly focused on indoor environments like meeting rooms or news studios, which are quite different from in-the-wild videos in many scenarios such as movies, documentaries, and audience sitcoms.
### [OmiTrans: generative adversarial networks based omics-to-omics translation framework | Papers With Code](https://paperswithcode.com/paper/omitrans-generative-adversarial-networks)
> 日期：27 Nov 2021

> 标签：None

> 代码：None

> 描述：With the rapid development of high-throughput experimental technologies, different types of omics (e.g., genomics, epigenomics, transcriptomics, proteomics, and metabolomics) data can be produced from clinical samples. The correlations between different omics types attracts a lot of research interest, whereas the stduy on genome-wide omcis data translation (i.e, generation and prediction of one type of omics data from another type of omics data) is almost blank.
