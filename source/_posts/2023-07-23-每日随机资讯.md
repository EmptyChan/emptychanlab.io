---
title: 2023-07-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TeaEstate_ZH-CN9645412630_1920x1080.webp&qlt=50
date: 2023-07-23 21:48:13
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TeaEstate_ZH-CN9645412630_1920x1080.webp&qlt=50)
# 新闻
### [小红书 7000字讲透 | 茶饮赛道内卷，新品牌如何“跳出围墙”？](https://www.woshipm.com/marketing/5871567.html)
> 概要: 随着消费升级和健康意识的增强，消费者对新鲜口味、个性化产品的不断追求，使得奶茶行业的竞争日益激烈。在这样的市场大环境中，品牌要想在激烈的竞争中脱颖而出，可以说是难上加难，从卷规模到卷营销、卷产品，在这......
### [一文讲透！小红书营销组件及评论区组件闭环收割玩法！](https://www.woshipm.com/marketing/5871804.html)
> 概要: 除了评论区组件，小红书在近一年上线了组合式笔记营销组件。这篇文章从评论区组件出发，讲解小红书营销组件和评论区组件的闭环收割的玩法以及该功能的作用，适合什么品类和发展阶段的商家使用，希望对你有所帮助。其......
### [聊聊播客这片流量洼地](https://www.woshipm.com/it/5871668.html)
> 概要: 随着播客的听众群体扩大，越来越多人开始在播客场景里“掘金”。那么综合来看，音频消费市场的表现如何？播客的商业化道路，又存在着哪些难题待解决？本篇文章里，作者就对当前的播客生态、即播客生态背后隐藏的商业......
### [尾田荣一郎短篇漫画《MONSTERS》动画化决定!](https://news.dmzj.com/article/78722.html)
> 概要: 尾田荣一郎短篇漫画《MONSTERS》动画化决定。
### [TV动画《偶像大师 闪耀色彩》公开最新PV！](https://news.dmzj.com/article/78724.html)
> 概要: TV动画《偶像大师 闪耀色彩》公开最新PV，2024年春季开播。
### [ABEMA电视剧《我放纵情欲的夜晚》漫画化启动](https://news.dmzj.com/article/78725.html)
> 概要: ABEMA电视剧《我放纵情欲的夜晚》将改编三部漫画。
### [融资13亿的明星企业宣布破产](https://36kr.com/p/2355139345676805)
> 概要: 融资13亿的明星企业宣布破产-36氪
### [《网络创世纪》缔造者谈当今游戏界 流行未必能尝新](https://www.3dmgame.com/news/202307/3873949.html)
> 概要: 提起最古老一代的网络游戏，《网络创世纪》绝对是其中的佼佼者，近日外媒采访了《网络创世纪》缔造者、传奇游戏开发者Raph Koster，一起来看看Koster对当今的游戏界是如何看待的。·《网络创世纪》......
### [致钓鱼台的插画](https://www.zcool.com.cn/work/ZNjU5NzQ4NDg=.html)
> 概要: 很开心和钓鱼台的一次合作~......
### [林志玲回应引退传闻：姐姐我还是会一直陪伴大家](https://ent.sina.com.cn/s/h/2023-07-23/doc-imzcruey3435344.shtml)
> 概要: 新浪娱乐讯 据媒体报道，昨晚林志玲现身支持老公AKIRA演出有媒体报道她证实了引退传言，引发热议。对此，林志玲亲自回应：“姐姐我还是会一直陪伴大家”，经纪人也表示：“不是引退啦！劳逸结合，热情工作，热......
### [俞敏洪杀入旅游业](https://36kr.com/p/2355002180909573)
> 概要: 俞敏洪杀入旅游业-36氪
### [《失忆症：地堡》开发商将减少恐怖游戏制作](https://www.3dmgame.com/news/202307/3873954.html)
> 概要: 专精恐怖游戏开发的Frictional Games准备“稍作调整”，减少恐怖游戏的开发，以便于关注其它方面的情感。在接受媒体采访时，创意总监Thomas Grip介绍了工作室在发布《失忆症：地堡》之后......
### [导演拒绝删除芭比偶遇老奶奶片段:这是电影的核心](https://ent.sina.com.cn/m/f/2023-07-23/doc-imzcryny0103691.shtml)
> 概要: 新浪娱乐讯 据媒体，真人《芭比》电影的导演Greta Gerwig透露，她坚持反对华纳兄弟高层要求剪掉芭比遇见一位老奶奶的场景，Greta说：“对我来说，这就是这部电影的核心。如果我剪掉了那个场景，我......
### [科技周报｜促进民营经济发展壮大的意见来了](https://www.yicai.com/news/101816099.html)
> 概要: 31条《意见》从八个方面提出了31条针对性强的举措，包括持续优化民营经济发展环境、加大对民营经济政策支持力度、强化民营经济发展法治保障等。
### [谁能成为“两轮蔚来”？](https://36kr.com/p/2354924395837440)
> 概要: 谁能成为“两轮蔚来”？-36氪
### [索尼新机通过工信部无线电核准入网](https://www.ithome.com/0/707/613.htm)
> 概要: IT之家7 月 23 日消息，现有一款型号为 XQ-DE72 的索尼新机通过了工信部无线电入网核准，疑似索尼 Xperia PRO-i Ⅱ。不过很可惜，目前还没有关于这款新机的进一步消息，IT之家后续......
### [特斯拉Model Y获欧洲销量第一 电车销量首次超油车](https://www.3dmgame.com/news/202307/3873960.html)
> 概要: 欧洲汽车制造商协会近日公布数据称，今年前四个月，欧洲30个国家共售出559733辆电动汽车，同比增长37%。而同期欧洲燃油车销量仅为550391辆，同比下降0.5%。这也意味着，电动车在欧洲市场对燃油......
### [《量子误差》开发接近完成 但Xbox版优化尚需时日](https://www.3dmgame.com/news/202307/3873961.html)
> 概要: 《量子误差》作为最早公布的PS5独占游戏之一名噪一时，而后来又转向虚幻引擎5，并宣布推出Xbox Series X/S版本，引发诸多关注和争议。如今，TeamKill Media可能对后面的决定感到后......
### [《英雄联盟》未来新模式将不再专注MOBA类型](https://www.3dmgame.com/news/202307/3873964.html)
> 概要: 《英雄联盟》最新游戏模式“斗魂竞技场”现已上线，玩家可以参与2v2v2v2的对战模式中。而在“斗魂竞技场”上线之前，《英雄联盟》模式产品负责人“Cadmus”Eduardo Cortejoso接受采访......
### [被亿万富翁贝索斯收购十年后，《华盛顿邮报》今年面临 1 亿美元亏损](https://www.ithome.com/0/707/628.htm)
> 概要: IT之家7 月 23 日消息，据《纽约时报》报道，亚马逊创始人、亿万富翁杰夫・贝索斯（Jeff Bezos）在 2013 年以 2.5 亿美元（IT之家备注：当前约 17.95 亿元人民币）收购的《华......
### [华为推出暑促超级秒杀日活动：至高优惠 2180 元，涉及手机笔记本手表等品类](https://www.ithome.com/0/707/632.htm)
> 概要: IT之家7 月 23 日消息，华为商城今日发布微博，华为推出暑促超级秒杀日活动，华为手机秒杀至高优惠 2180 元，华为电脑至高省 1000 元，预约还可抽 199 元蓝牙音箱。从官方发布的海报来看，......
### [【洛城里】你选哪个重启：科炮合作、KD不去勇士、老詹留热火](https://bbs.hupu.com/61346194.html)
> 概要: 今天凌晨，美媒NBA Retweet晒出合照并发问：罗斯健康、科比保罗合作、杜兰特不去勇士、詹姆斯永远留在热火，你会选择哪个？引来热议，一起来看看球迷如何评论！Lebron, obviously显然选
### [【篮凤凰】皮尔斯：我技术不比韦德差，给我老詹奥尼尔我不会只夺一冠](https://bbs.hupu.com/61346208.html)
> 概要: 【篮凤凰】皮尔斯：我技术不比韦德差，给我老詹奥尼尔我不会只夺一冠
### [从“小透明”到“全网销售额领先” 网友：白象支棱起来了](http://www.investorscn.com/2023/07/23/109074/)
> 概要: 89个国家和地区的代表团、近一万名代表团运动员，即将在7月28日齐聚成都，参加第31届世界大学生运动会......
### [场内流动性和情绪的拐点将至；国内人形机器人零部件产业链有较大机会](https://www.yicai.com/news/101816149.html)
> 概要: A股后市怎么走？看看机构怎么说：
### [TV动画《佐佐木与文鸟小哔》公开最新情报](https://news.dmzj.com/article/78726.html)
> 概要: TV动画《佐佐木与文鸟小哔》公开视觉图、出演者和制作阵容，将于2024年开播。
### [内外资一致、明确豁免情形，特定短线交易新规影响几何？](https://www.yicai.com/news/101816150.html)
> 概要: 明确境内外特定投资者适用短线交易制度的标准，明晰豁免适用短线交易的情形。
### [本周双碳大事：全国碳市场第二个履约期差异化分配配额；“远程汽车”融资6亿美元；国内光伏新增装机预期上调至120-140GW](https://36kr.com/p/2356351562955780)
> 概要: 本周双碳大事：全国碳市场第二个履约期差异化分配配额；“远程汽车”融资6亿美元；国内光伏新增装机预期上调至120-140GW-36氪
### [微博会员将新增 VVIP：商业变现权益计划回收到该体系之中](https://www.ithome.com/0/707/640.htm)
> 概要: 感谢IT之家网友Monsterwolf的线索投递！IT之家7 月 23 日消息，微博近日发文介绍了有关 VVIP 会员体系想法的介绍。官方表示，之前例如微博小店、广告共享计划、V + 会员等功能的准入......
### [中炬高新：公司总经理邓祖明辞职](https://www.yicai.com/news/101816164.html)
> 概要: 因个人原因，邓祖明先生辞去公司总经理职务。辞职后，邓祖明先生将不在公司及下属子公司担任任何职务。
### [组图：关晓彤活动生图曝光 穿白色一字肩吊带裙身材比例惊人](http://slide.ent.sina.com.cn/star/slide_4_704_387348.html)
> 概要: 组图：关晓彤活动生图曝光 穿白色一字肩吊带裙身材比例惊人
### [全球大宗商品波动加剧，4只商品期货ETF上半年净值均下跌](https://www.yicai.com/news/101816185.html)
> 概要: 上半年全球大宗商品价格持续回调
### [Xiaohu力挽狂澜，Light输出拉满，谁是今日最佳？](https://bbs.hupu.com/61349391.html)
> 概要: 2023LPL夏季赛季后赛第二轮比赛结束。WBG3-1轻取NIP成功挺进下一轮。选手们都有不错的发挥，TheShy奎桑提三次单杀，Weiwei翠神节奏起飞，Xiaohu沙皇神奇绕后，Light尼菈摧枯
### [大暑时节，脾胃要这样养护](https://finance.sina.com.cn/jjxw/2023-07-23/doc-imzcsvsr6360264.shtml)
> 概要: 转自：上观新闻今天（7月23日）是二十四节气中的第12个节气，也是夏季最后一个节气——大暑。大暑是夏季最炎热的时候，北新泾街道社区卫生服务中心中医全科副主任医师王荔源...
### [新疆农业电商总部基地落户乌鲁木齐经开区（头屯河区）](https://finance.sina.com.cn/jjxw/2023-07-23/doc-imzcsvsi4707915.shtml)
> 概要: 乌鲁木齐晚报全媒体讯（记者唐红梅）近日，新疆农业电商总部基地落户经开区（头屯河区）翼展天地大厦，首期计划吸引70家农业电商相关企业入驻。
### [合肥最新公开招聘](https://finance.sina.com.cn/jjxw/2023-07-23/doc-imzcsvsi4708858.shtml)
> 概要: 转自：合肥发布1.合肥惠科金扬科技有限公司公司于2014年成立，为惠科集团全资公司，注册资本3亿元。公司在合肥投资50亿元，建设年产3000万台液晶电视机及配套生产项目...
### [国图、人教社等15家单位协同打造“紫竹有我”城市小公民成长营](https://finance.sina.com.cn/jjxw/2023-07-23/doc-imzcsvsr6361559.shtml)
> 概要: 新京报讯（记者吴为）7月23日，由国家图书馆、北京外国语大学、人民教育出版社等多家单位协同打造的“紫竹有我”城市小公民成长营在中关村科学城城市大脑开营。
### [长江上游新一轮强降雨来袭，长江防总、长江委发布提醒](https://finance.sina.com.cn/jjxw/2023-07-23/doc-imzcsvsn2902640.shtml)
> 概要: 转自：北京日报客户端据水文气象预报，7月23日，嘉岷流域北部有中雨、局地大雨；24～27日，长江上游干流附近有中到大雨、局地暴雨的降水过程...
### [流言板肯巴-沃克：我想去信任我、想要我的球队，摩纳哥符合要求](https://bbs.hupu.com/61349610.html)
> 概要: 虎扑07月23日讯 近日，欧洲俱乐部摩纳哥队新援肯巴-沃克接受了记者的采访。谈到签约摩纳哥，沃克说：“这支球队对我有意，我就是想去一直想要我的、信任我的队伍。我感觉我可以帮助摩纳哥再上一层楼，所以我做
### [流言板塞尔维亚媒体：约基奇将缺席今夏的篮球世界杯](https://bbs.hupu.com/61349718.html)
> 概要: 虎扑07月23日讯 据塞尔维亚媒体Mozza Spo报道，约基奇将缺席今夏的篮球世界杯。塞尔维亚主帅佩西奇和约基奇进行了最新的交流，队长博格丹-博格达诺维奇也试图说服他，不过最终，约基奇仍然决定今夏休
# 小说
### [汉末将星传](https://book.zongheng.com/book/845413.html)
> 作者：苍暮老山

> 标签：历史军事

> 简介：孙子云:凡用兵之法，全国为上，破国次之；全军为上，破军次之；全旅为上，破旅次之；全卒为上，破卒次之；全伍为上，破伍次之。是故百战百胜，非善之善者也；不战而屈人之兵，善之善者也。兵法的最高境界，是战无不胜，而谋略的最高境界，是不战而屈人之兵。看汉末乱世，一个柔弱的文士，如何凭借胸中韬略，将一个风雨飘摇的曹姓诸侯，扶持为君临天下的堂堂曹魏！

> 章节末：第七百三十六章:大结局

> 状态：完本
# 论文
### [A Baselined Gated Attention Recurrent Network for Request Prediction in Ridesharing | Papers With Code](https://paperswithcode.com/paper/a-baselined-gated-attention-recurrent-network)
> 日期：11 Jul 2022

> 标签：None

> 代码：https://github.com/wingsupete/rsodp

> 描述：Ridesharing has received global popularity due to its convenience and cost efficiency for both drivers and passengers and its strong potential to contribute to the implementation of the UN Sustainable Development Goals. As a result recent years have witnessed an explosion of research interest in the RSODP (Origin-Destination Prediction for Ridesharing) problem with the goal of predicting the future ridesharing requests and providing schedules for vehicles ahead of time. Most of existing prediction models utilise Deep Learning, however they fail to effectively consider both spatial and temporal dynamics. In this paper the Baselined Gated Attention Recurrent Network (BGARN), is proposed, which uses graph convolution with multi-head gated attention to extract spatial features, a recurrent module to extract temporal features, and a baselined transferring layer to calculate the final results. The model is implemented with PyTorch and DGL (Deep Graph Library) and is experimentally evaluated using the New York Taxi Demand Dataset. The results show that BGARN outperforms all the other existing models in terms of prediction accuracy.
### [Audio-Visual Face Reenactment | Papers With Code](https://paperswithcode.com/paper/audio-visual-face-reenactment)
> 日期：6 Oct 2022

> 标签：None

> 代码：https://github.com/mdv3101/AVFR-Gan

> 描述：This work proposes a novel method to generate realistic talking head videos using audio and visual streams. We animate a source image by transferring head motion from a driving video using a dense motion field generated using learnable keypoints. We improve the quality of lip sync using audio as an additional input, helping the network to attend to the mouth region. We use additional priors using face segmentation and face mesh to improve the structure of the reconstructed faces. Finally, we improve the visual quality of the generations by incorporating a carefully designed identity-aware generator module. The identity-aware generator takes the source image and the warped motion features as input to generate a high-quality output with fine-grained details. Our method produces state-of-the-art results and generalizes well to unseen faces, languages, and voices. We comprehensively evaluate our approach using multiple metrics and outperforming the current techniques both qualitative and quantitatively. Our work opens up several applications, including enabling low bandwidth video calls. We release a demo video and additional information at http://cvit.iiit.ac.in/research/projects/cvit-projects/avfr.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
