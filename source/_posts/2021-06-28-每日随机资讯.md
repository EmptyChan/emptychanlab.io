---
title: 2021-06-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Cittadella_ZH-CN0039969121_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-06-28 20:46:33
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Cittadella_ZH-CN0039969121_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [全票通过，网易开源项目 Kyuubi 进入 Apache 孵化器](https://www.oschina.net/news/148016)
> 概要: 机器学习 推开AI应用的门！>>>北京时间2021年6月21日，全球顶级开源组织 Apache 软件基金会宣布，网易数帆开源的大数据项目 Kyuubi 以全票通过的表现，正式进入 Apache 软件基......
### [Pulsar 社区周报：Pulsar client、broker、transactions](https://www.oschina.net/news/148020)
> 概要: 机器学习 推开AI应用的门！>>>关于 Apache PulsarApache Pulsar 是 Apache 软件基金会顶级项目，是下一代云原生分布式消息流平台，集消息、存储、轻量化函数式计算为一体......
### [LF AI & Data 基金会宣布 Milvus 项目毕业](https://www.oschina.net/news/147960)
> 概要: 机器学习 推开AI应用的门！>>>LF AI & Data 基金会是一家致力于构建生态系统，以支持人工智能（AI）和数据开源项目的开源创新的组织，今天宣布，托管项目Milvus1将从一个孵化级项目推进......
### [NVIDIA 提议引入 Linux 硬件时间戳引擎](https://www.oschina.net/news/147949/nvidia-proposes-the-linux-hardware-timestamping-engine)
> 概要: 机器学习 推开AI应用的门！>>>根据邮件列表显示，NVIDIA 工程师提出了一项在 Linux 内核中引入硬件时间戳引擎（HTE）子系统的提案。某些设备具有内置的硬件时间戳引擎，可以实时监测系统信号......
### [Let's code a TCP/IP stack (2016)](https://www.saminiir.com/lets-code-tcp-ip-stack-1-ethernet-arp/)
> 概要: Let's code a TCP/IP stack (2016)
### [纯电动车的悲哀](https://www.huxiu.com/article/436580.html)
> 概要: 出品｜虎嗅汽车组作者｜梓楠法师头图｜视觉中国“比亚迪刀片电池以一己之力把磷酸铁锂从边缘化拉回来”，在6月13日的2021年中国汽车重庆论坛上，比亚迪总裁王传福如是说。磷酸铁锂电池在产销数据上的逆袭，给......
### [如何买到好吃的水果？](https://www.huxiu.com/article/437462.html)
> 概要: 本文来自微信公众号：行业研习（ID：hangyeyanxi），作者：走南闯北的社长，题图来自：视觉中国一、甜甜的瓜香夏天的午后，在骄阳烘烤下，花村的知了也显得无精打采，有一声没一声地叫着。大飞哥慵懒地......
### [动画「Love Live! Superstar!!」正式PV公开](http://acg.178.com/202106/418843486378.html)
> 概要: 电视动画「Love Live! Superstar!!」公开了正式PV，该动画将于2021年7月11日开始播出。「Love Live! Superstar!!」正式PVSTAFF原作：矢立 肇原案：公......
### [动画「奇蛋物语」BD第3卷封面公开](http://acg.178.com/202106/418846470356.html)
> 概要: 电视动画「奇蛋物语」公开了BD第3卷的封面图，该商品将于2021年8月25日发售。该商品Blu-ray版售价为14,300日元（含税），DVD版售价为12,100日元（含税），收录了动画第10-12话......
### [Steam官方动手 将大批阿根廷跨区用户强制转回国区](https://www.3dmgame.com/news/202106/3817720.html)
> 概要: 近几日有玩家发现，Steam官方开始将大批阿根廷跨区用户强制转回国区，不管是老号或是新号都有被遣返的可能，账户中余下的阿根廷比索也被自动转换为人民币。不过也有一些漏网之鱼。据玩家推测，遣返原因可能有以......
### [动画电影「爱丽丝与泰瑞斯的梦幻工厂」先导视觉图与特报PV公开](http://acg.178.com/202106/418847000899.html)
> 概要: 新作动画电影「爱丽丝与泰瑞斯的梦幻工厂」于近日公开了先导视觉图与特报PV！据悉，该动画电影讲述少年少女们以“恋心”为武器，与命运开展战斗的故事。「爱丽丝与泰瑞斯的梦幻工厂」特报PVSTAFF监督：岡田......
### [招聘信息透露 开发商IO Interactive新项目或为MMO](https://www.3dmgame.com/news/202106/3817722.html)
> 概要: 制作出热门杀手系列的工作室IO Interactive近日在其官方网站发布招聘声明，其主要是在为新项目特工007以及一款未宣发游戏招募人手。其实今年早些时候就有媒体曝料称微软在和IO Interact......
### [MAPPA公开10周年纪念绘图](http://acg.178.com/202106/418847825749.html)
> 概要: 今年（2021年）是MAPPA成立的第10年，官方公开了10周年的纪念绘图，图中为MAPPA制作的作品中的众多主要人物。MAPPA是一个以动画企划与制作为主的日本企业，于2011年成立，主要作品有「B......
### [视频：关之琳在家吃一桌油炸食品！毫不顾忌女神形象](https://video.sina.com.cn/p/ent/2021-06-28/detail-ikqciyzk2291278.d.html)
> 概要: 视频：关之琳在家吃一桌油炸食品！毫不顾忌女神形象
### [陈学冬发小狐狸照片庆生 许愿31岁要支棱起来](https://ent.sina.com.cn/s/m/2021-06-28/doc-ikqcfnca3695656.shtml)
> 概要: 新浪娱乐讯 6月28日，陈学冬发布照片庆祝31岁的生日，许愿自己要“支棱”起来。照片中一只小狐狸玩偶双手合拢作出许愿的姿势，看起来虔诚又可爱。粉丝纷纷在评论中送出另类祝福：“”，互动好笑又有爱。(责编......
### [《绝密者》投资方起诉郑爽 案件将直播公开庭审](https://ent.sina.com.cn/v/m/2021-06-28/doc-ikqcfnca3713112.shtml)
> 概要: 新浪娱乐讯 6月28日，有网友发现某平台上有关郑爽及其公司的“司法状态”更新，更新显示，由郑爽担任法人的“九江羊群效应影视工作室”新添一则司法信息，具体为“霍尔果斯尚晖影视”将“郑爽”以及“九江羊群效......
### [在线教育辅导老师：为了卖课，我们照着剧本骗家长](https://www.huxiu.com/article/437466.html)
> 概要: 深燃（shenrancaijing）原创，作者：王敏，编辑：向小园，头图来自：视觉中国即将进入2021年下半年，教培行业从业者的集体恐慌和焦虑还在继续。6月18日，一则关于“新东方内部座谈会”的消息流......
### [《怦然心动20岁》男嘉宾许杰为回应争议言论](https://ent.sina.com.cn/tv/zy/2021-06-28/doc-ikqcfnca3716666.shtml)
> 概要: 新浪娱乐讯 6月28日，恋爱综艺《怦然心动20岁》男嘉宾许杰为凌晨发长文回应此前在节目中对女嘉宾朱非儿的饱受争议的言论。他表示节目没有剧本，为自己说过的话道歉，并解释道之所以会和阿卓说自己的感情想法，......
### [矿视界译文：采用比特币作法币的萨尔瓦多现在怎样了？](https://www.tuicool.com/articles/uU3y2m3)
> 概要: 萨尔瓦多是一个人口650万的独立主权国家，它没有自己的货币，使用美元作为本国货币。由于萨尔瓦多政府无法自主印钞，他们就必须通过从海外赚取和借贷的方式来获得足够的美元供国内使用。萨尔瓦多从海外赚取美元的......
### [腾讯获推荐婚恋对象专利授权 用信用分找心上人](https://www.3dmgame.com/news/202106/3817747.html)
> 概要: 据天眼查App显示，6月25日，腾讯科技（深圳）有限公司获得“一种婚恋对象推荐方法及装置”发明专利授权，公告号CN108073659B，申请日期为2016年11月。专利摘要显示，该专利将提供一种婚恋对......
### [Why Computing Students Should Contribute to Open Source Software Projects](https://cacm.acm.org/magazines/2021/7/253459-why-computing-students-should-contribute-to-open-source-software-projects/fulltext)
> 概要: Why Computing Students Should Contribute to Open Source Software Projects
### [日本环球影城将增设《鬼灭》主题景区 9月起限时开放](https://acg.gamersky.com/news/202106/1401256.shtml)
> 概要: 日本环球影城将增设《鬼灭之刃》主题景区，9月17日到次年2月13日限时开放。
### [云麦科技完成C2轮融资，打造全民化健身新常态](https://www.tuicool.com/articles/rqIV3yb)
> 概要: 【猎云网（微信：）北京】6月28日报道近日，智能运动健康企业云麦科技宣布完成2亿元C2轮融资，本轮融资由乐耕资本领投，长润资本、璀璨资本以及明势资本跟投。本轮融资将主要用于提升产品技术研发能力，品牌建......
### [组图：龚俊周也中餐厅录制再同框！打扮休闲清新兄妹两人状态好](http://slide.ent.sina.com.cn/z/v/slide_4_704_358606.html)
> 概要: 组图：龚俊周也中餐厅录制再同框！打扮休闲清新兄妹两人状态好
### [中芯国际 CEO 赵海军：有信心在同类产品上与世界上任何公司比较](https://www.ithome.com/0/559/803.htm)
> 概要: IT之家6 月 28 日消息 6 月 25 日中芯国际（688981）召开了 2021 年第一次临时股东大会，表决通过了 2020 年年度报告。此外，股东大会批准了《2021 年科创板限制性股票激励计......
### [企业培训的十条军规](https://www.huxiu.com/article/437559.html)
> 概要: IBM一项面向全球CEO的调查显示：80%的CEO认为能力问题是制约企业发展的瓶颈。但在同一份问卷中，65%的CEO认为企业当前的培训是无效的。如何刷新老板眼中，企业培训是“吃不死人，但也治不好病”的......
### [瑞士经济学家：区块链并不适合央行数字货币](https://www.ithome.com/0/559/819.htm)
> 概要: 6 月 28 日消息，据国外媒体报道，瑞士央行首席经济学家 Carlos Lenz 认为，基于区块链的去中心化特征对于像数字法郎这样受国家控制的数字货币来说并不会十分有效，区块链不是央行数字货币（CB......
### [Dear Google: Public domain compositions exist](https://blog.dbmiller.org/2021-06-28-dear-google-public-domain-compositions-exist)
> 概要: Dear Google: Public domain compositions exist
### [腾讯视频 VIP 大促：季卡 + 月卡 39.9 元、年卡 113 元](https://www.ithome.com/0/559/825.htm)
> 概要: 腾讯视频 VIP 年中大促，年卡原价 253 元、4.5 折 113 元，季卡原价 68 元、5.8 折 39.9 元再加送月卡。活动时间：6 月 27 日 - 30 日拼多多腾讯视频 VIP 会员 ......
### [王思聪和孙一宁的“战争”，靠王思聪洗白，准备进军娱乐圈？](https://new.qq.com/omn/20210628/20210628A08XBH00.html)
> 概要: 前段时间，王思聪与孙一宁展开大战，两人在网络上互撕，最终显然是孙一宁获得了胜利，不仅让王思聪获得了“舔狗”新标签，自己的粉丝还增加了百万。王思聪观众都非常熟悉，作为王健林的儿子，万达集团的公子。   ......
### [理性讨论，维拉30M报价史密斯罗是否意味着格拉利什铁定要走了？](https://bbs.hupu.com/43921306.html)
> 概要: 感觉这个买人法，格拉利什大概率要走人了
### [英伟达：NVIDIA Aerial 5G 平台扩大对 Arm 架构 CPU 的支持](https://www.ithome.com/0/559/841.htm)
> 概要: IT之家6 月 28 日消息 今日，英伟达宣布将在 NVIDIA Aerial A100 AI-on-5G 平台中扩展对基于 Arm 架构的 CPU 的支持，为 5G 生态系统带来更多选择。英伟达表示......
### [传《使命召唤》新作伦敦码头等经典地图将回归](https://www.3dmgame.com/news/202106/3817774.html)
> 概要: 近日《使命召唤》2021年新作泄露消息暗示《使命召唤：二战》的伦敦码头地图将回归，并且其他经典的多人游戏地图也在回归计划之中。据之前的泄露消息称新作的标题为《使命召唤：先锋》。众多传闻消息宣称，新作将......
### [BabyHusky公布产品赋能及运营推广计划方案](https://www.btc126.com//view/174129.html)
> 概要: 近日，BabyHusky项目方正式公布BabyHusky产品赋能计划，初期将从机枪池及NFT两方面对BabyHusky进行赋能：项目方将于7月10日和13日上线BSC机枪池功能，分别支持单币及LP存款......
### [KuCoin将于6月29日18点上线全球支付平台NGM，现已开放充值](https://www.btc126.com//view/174131.html)
> 概要: 据KuCoin官方公告，KuCoin宣布上线e-Money (NGM)项目并支持NGM/USDT交易对，NGM的充值服务现已开放，将于6月29日18点正式开放交易。e-Money 是基于 Cosmos......
### [Subdomains for IPFS](https://ipds.io/)
> 概要: Subdomains for IPFS
### [放心了，张颂文接住了何书衡这个角色，刘昊然有两场戏，非常惊艳](https://new.qq.com/omn/20210628/20210628A08NO100.html)
> 概要: 6月，电影市场异常的冷清，新上映的十几部电影里竟然没有一部票房超过1.5亿的。而随着暑期的到来，大家又急需一部重量级大片来解渴。            然后，《1921》来了。103位知名演员出演，大......
### [诺基亚推出支持支付宝的功能机 为老年用户提供便利](https://www.3dmgame.com/news/202106/3817776.html)
> 概要: 今日（6月28日）诺基亚官方微博发布“暗示性”海报及信息，微博内容为“轻便小巧，支持 __ __ __ 。”，通过海报我们可以看出这是一款支持支付宝的功能机，新手机将于明天十点公布。目前市面上专供老年......
### [狗狗币核心开发者Patrick Lodder提出降低狗狗币链上交易费用提案](https://www.btc126.com//view/174132.html)
> 概要: 狗狗币核心开发者 Patrick Lodder 今日在 Github 上提出降低狗狗币链上交易手续费提案，该提案指出，目前狗狗币每日链上交易手续费高达 34.5 万美元，接近LTC交易费的 5 倍，后......
### [FTX创始人：机构资金仍在缓慢流入加密市场](https://www.btc126.com//view/174133.html)
> 概要: 雅虎财经消息，加密交易平台FTX创始人Sam Bankman-Fried表示，自己对于市场的未来发展还是持有较为乐观的态度，他认为机构仍在进入加密领域，只不过这个过程将会十分漫长和谨慎......
### [杨伟民：出口钢可能赚了外汇 但是赚的钱可能还不够去治理环境的](https://finance.sina.com.cn/china/2021-06-28/doc-ikqciyzk2386384.shtml)
> 概要: 中国经济50人论坛学术委员会成员、十三届全国政协常委、经济委员会副主任杨伟民在“长安讲坛”上表示，出口钢，可能赚了外汇，但是赚的钱可能还不够去治理环境的。
### [侠客岛：一枚意义重大的勋章背后的故事](https://finance.sina.com.cn/china/2021-06-28/doc-ikqciyzk2387468.shtml)
> 概要: 明天有一件大事：6月29日上午10时，人民大会堂将隆重举行“七一勋章”颁授仪式。这是中国共产党党内最高荣誉。 荣誉是一个共同体团结的精神维系，褒扬榜样，成风化人。
### [【直播】云缨很弱？小胖1700分用云缨打出15.3分，什么水平？](https://bbs.hupu.com/43922640.html)
> 概要: 【直播】云缨很弱？小胖1700分用云缨打出15.3分，什么水平？
### [慕了！扬州大学食堂推出轻食减脂餐4元起 成校园饮食“新宠”](https://finance.sina.com.cn/china/2021-06-28/doc-ikqcfnca3786990.shtml)
> 概要: 来源：荔枝新闻 【慕了！#大学食堂推出轻食减脂餐4元起#】近日，@扬州大学 推出10多款轻食减脂套餐，好吃不贵还可自选搭配。套餐奉行少糖少油少盐和高蛋白高纤维...
### [欧洲杯传声筒MD15 失意出局，C罗赛后用队长袖标苦练任意球！](https://bbs.hupu.com/43922785.html)
> 概要: #以下评论皆来自redditON：比利时 1-0 葡萄牙                                       –IBuildZombies 645 指標 5小時前*Not
### [转发提醒！手里有这类卡的 月底前全部注销→](https://finance.sina.com.cn/wm/2021-06-28/doc-ikqcfnca3788602.shtml)
> 概要: 近日，工业和信息化部、公安部联合发布关于依法清理整治涉诈电话卡、物联网卡以及关联互联网账号的通告，指出电信主管部门、公安机关将持续深入推进“断卡行动”...
### [安哥拉驻香港前任总领事科佩蒂诺：中国的成功给发展中国家开辟了一条新路](https://finance.sina.com.cn/china/2021-06-28/doc-ikqciyzk2388887.shtml)
> 概要: 原标题：安哥拉驻香港前任总领事科佩蒂诺：中国的成功给发展中国家开辟了一条新路 为庆祝中国共产党成立100周年，近日，安哥拉驻香港前任总领事科佩蒂诺在安哥拉《国家报》...
### [Js是怎样运行起来的？](https://www.tuicool.com/articles/bMvYruR)
> 概要: 前言不知道大家有没有想过这样一个问题，我们所写的 JavaScript 代码是怎样被计算机认识并且执行的呢？这中间的过程具体是怎样的呢？有的同学可能已经知道，Js 是通过 Js 引擎运行起来的，那么什......
### [特别纪念|高尚全：改革开放要坚持四个不动摇](https://finance.sina.com.cn/china/gncj/2021-06-28/doc-ikqciyzk2389307.shtml)
> 概要: 来源：北大国发院 题记：2021年6月27日下午，原国家体改委副主任、中国经济体制改革研究会原会长高尚全先生在北京逝世，享年92岁。
### [央视主持马智宇与老婆直播对骂，约定七月离婚，曾与李小璐传绯闻](https://new.qq.com/omn/20210628/20210628A0B3FQ00.html)
> 概要: 6月28日，央视主持人、号称是当下最贵的婚礼司仪马智宇与妻子在直播间互相指责，大吵了一架。其实夫妻两人原本就有一些矛盾，在粉丝的建议下决定直播间连线沟通。可惜，从视频中可以看到两个人的情绪都十分的激动......
### [一个搞艺术的女性决定做独立游戏](https://www.tuicool.com/articles/bM7nYnv)
> 概要: 本文来自微信公众号：北方公园NorthPark（ID：northpark2018），作者：王小笨，头图来自：《地平线：零之曙光》记忆对我们到底意味着什么？这是我在玩《遗忘工程师》这款游戏的时候一直在思......
### [00后还看湖南卫视吗？](https://new.qq.com/omn/20210628/20210628A0B62I00.html)
> 概要: 文/王心怡前不久，河南卫视端午晚会再度引起热议，地方卫视再一次成为了讨论的热点之一。在网台融合的大趋势下，伴随着视频网站自制内容质量、数量不断攀升，以及用户观看习惯进一步改变，地方卫视被冲击已久。在今......
### [OMG vs TT 精彩回顾：瑞尔无解开团，伊泽瑞尔无情输出收获3杀](https://bbs.hupu.com/43923312.html)
> 概要: 来源：  虎扑    标签：TTOMG
# 小说
### [那里有微风吹过](http://book.zongheng.com/book/960343.html)
> 作者：彩虹屁

> 标签：都市娱乐

> 简介：：唐晓，做个交易如何？反正你只喜欢钱。而我也早就想摧毁这已经烂到骨子里的公司。：哦？我可不单单是喜欢钱而已。：看在我们的感情上就当帮我一个忙。：你可别跟我提感情，怪伤钱的。忙倒是可以帮，不过这钱不钱的不重要，我要股份。：你倒是看的长远。

> 章节末：88

> 状态：完本
# 论文
### [TopoTxR: A Topological Biomarker for Predicting Treatment Response in Breast Cancer](https://paperswithcode.com/paper/topotxr-a-topological-biomarker-for)
> 日期：13 May 2021

> 标签：None

> 代码：https://github.com/TopoXLab/TopoTxR

> 描述：Characterization of breast parenchyma on dynamic contrast-enhanced magnetic resonance imaging (DCE-MRI) is a challenging task owing to the complexity of underlying tissue structures. Current quantitative approaches, including radiomics and deep learning models, do not explicitly capture the complex and subtle parenchymal structures, such as fibroglandular tissue.
### [Visual Compositional Learning for Human-Object Interaction Detection](https://paperswithcode.com/paper/visual-compositional-learning-for-human)
> 日期：24 Jul 2020

> 标签：HUMAN-OBJECT INTERACTION DETECTION

> 代码：https://github.com/zhihou7/VCL

> 描述：Human-Object interaction (HOI) detection aims to localize and infer relationships between human and objects in an image. It is challenging because an enormous number of possible combinations of objects and verbs types forms a long-tail distribution.
