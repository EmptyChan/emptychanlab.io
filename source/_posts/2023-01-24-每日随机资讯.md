---
title: 2023-01-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ColleSantaLucia_ZH-CN7638164714_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-01-24 21:56:30
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ColleSantaLucia_ZH-CN7638164714_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [加码短视频，小红书“抖音化”？](https://www.woshipm.com/operate/5735312.html)
> 概要: 近日，小红书开始对自己的内容社区进行完善优化，入口的“购物”被“视频”取代。小红书这一改动意味着什么？本文对其变化进行分析，一起来看看。近日，部分用户发现，在小红书最新的版本升级中，原先底部导航栏的“......
### [春晚红包背上了互联网大厂新的KPI](https://www.woshipm.com/it/5735308.html)
> 概要: 相比于以往，今年的春晚红包显得过于低调，曾经野蛮生长的互联网行业逐渐走入精细化运营阶段。用户的拉新和转化，成为了互联网的财富密码。本文对春晚红包的发展进行分析，一起来看看吧。相比往年提前半月就开始宣传......
### [年货的40年变迁，一部消费升级史](https://www.woshipm.com/marketing/5735328.html)
> 概要: 每逢春节，置办年货就成为了每家的仪式感，如今，置办年货的清单一直在变化。中国家庭的年货清单俨然成为一部反映国民消费升级的编年史。本文对年货的发展史进行了总结，一起来看看。每逢春节前夕，置办年货就成为每......
### [《死亡空间：重制版》全球上线时间公布](https://www.3dmgame.com/news/202301/3861154.html)
> 概要: 经过十多年的蛰伏，《死亡空间》系列终于准备再度成为人们关注的焦点。MotiveStudio和EA合作的这部备受期待的重制系列第一部现在离发行只有几天了，随着人们对其发行的兴奋度持续增加，EA提供了其发......
### [视频：新浪娱乐独家对话邓超 直言批评是非常好的良药](https://video.sina.com.cn/p/ent/2023-01-24/detail-imycftav1330266.d.html)
> 概要: 视频：新浪娱乐独家对话邓超 直言批评是非常好的良药
### [中国驻洛杉矶总领馆：确认有中国公民在枪击事件中不幸遇难](https://www.yicai.com/news/101657697.html)
> 概要: 总领馆发言人表示，目前确认有中国公民在事件中不幸遇难，我们正会同国内相关部门及美方全力协助处理有关善后事宜。
### [纳指涨超2%，美股全线收高！市场押注美联储下周仅加息25个基点](https://www.yicai.com/news/101657690.html)
> 概要: 投资者正在权衡美联储放缓加息步伐的概率
### [Spotify裁员6% 数字广告“盛世”不再](https://www.yicai.com/news/101657687.html)
> 概要: 所有的科技头部企业都希望能够维持大流行带来的强劲发展势头，我也如此，以为广告收入放缓的风险对我们的影响较小。但事后看来，在我们的收入增长之前，投资仍然过于迅猛了。
### [玩《巫师3》触发独狼结局？这款Mod让你还能选](https://www.3dmgame.com/news/202301/3861171.html)
> 概要: 玩过《巫师3》的玩家都知道，白狼可以与特莉丝和叶奈法同时谈恋爱，但最终要做出选择特莉丝或叶奈法的艰难决定。如果玩家想全都要，那就会变成独狼结局！近日Mod作者Freak发布《巫师3》浪漫模式Mod，提......
### [【动漫之家】漫谈米哈游《崩坏：星穹铁道》 有机会领取三测资格！](https://news.dmzj.com/article/76926.html)
> 概要: 漫谈米哈游《崩坏：星穹铁道》 有机会领取三测资格！
### [越南网红美女Nguyễn福利图赏 性感非凡身材魅惑](https://www.3dmgame.com/bagua/5777.html)
> 概要: 今天为大家带来的是越南网红美女Nguyễn Thị Kim Anh的福利图。妹子刚从大学毕业，拥有宛如邻家妹妹的美貌，在越南的人气很高。Kim Anh于2000年出生，小时候又黑又瘦，被同学嘲笑后想要......
### [《龙与地下城：侠盗荣耀》新预告公布 2023年上线](https://www.3dmgame.com/news/202301/3861175.html)
> 概要: 今日（1月24日），派拉蒙影业公布《龙与地下城：侠盗荣耀》终极预告，影片由美国派拉蒙影片公司出品，克里斯·派恩、米歇尔·罗德里格兹、雷吉-让·佩吉、贾斯蒂斯·史密斯、休·格兰特等全明星阵容出演，约翰·......
### [《满江红》大战《流浪地球2》，电影院挤爆了](https://36kr.com/p/2101259798724992)
> 概要: 《满江红》大战《流浪地球2》，电影院挤爆了-36氪
### [南美小国，为何爱买中国车？](https://36kr.com/p/2101369533923715)
> 概要: 南美小国，为何爱买中国车？-36氪
### [Galaxy Book 3 Ultra 率先装备，三星正为笔记本开发全新 120Hz OLED 触控屏幕](https://www.ithome.com/0/669/372.htm)
> 概要: IT之家1 月 24 日消息，根据国外科技媒体 SamMobile 报道，三星即将推出的 Galaxy Book 3 Ultra 将会采用全新的 OLED 触控面板屏幕。该机屏幕尺寸为 16 英寸，分......
### [暴雪与中国玩家的二十年](https://www.huxiu.com/article/776798.html)
> 概要: 本文来自微信公众号：慢放（ID：manfangsd），作者：文八娘，编辑：刘涵，策划：杨倩，题图来自：视觉中国菲茨杰拉德在《了不起的盖茨比》里写，我们拼命划桨，奋力与波浪抗争，最后却被冲回到我们的往昔......
### [《勿言推理》广岛篇电影化海报公开](https://news.dmzj.com/article/76927.html)
> 概要: 《勿言推理》广岛篇电影化海报公开
### [《比宇宙更远的地方》舞台化！](https://news.dmzj.com/article/76928.html)
> 概要: TV动画《比宇宙更遥远的地方》舞台剧化决定。
### [《美少女战士Cosmos》公开新声优](https://news.dmzj.com/article/76929.html)
> 概要: 剧场版动画《美少女战士Cosmos》公布了第2弹声优名单
### [金士顿 DDR5 4800 RDIMM 内存已在第四代英特尔至强可扩展处理器中完成验证](https://www.ithome.com/0/669/394.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家1 月 24 日消息，金士顿今日宣布，其64GB、32GB 和 16GB服务器 Premier DDR5 4800MT / s Registered DI......
### [谁说中国人造不出保时捷？](https://www.huxiu.com/article/777018.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博头图｜视频截图在中国农历新年整活儿，还得是中国本土汽车品牌。1 月 22 日，吉利汽车正式对外公布全新中高端新能源系列，该系列产品型谱将由多款全新纯电、插电混动、增程式混动......
### [太假了！网友分析《三体》动画B站真实评分为2.1分](https://acg.gamersky.com/news/202301/1559876.shtml)
> 概要: 网友分析：三体动画B站真实评分为2.1左右。
### [微软 Edge 110 浏览器测试新功能：在一个窗口中并排显示两个标签页](https://www.ithome.com/0/669/404.htm)
> 概要: IT之家1 月 24 日消息，微软 Edge 浏览器正在测试一个新的生产力功能，目前在 Beta、Dev 和 Canary 频道的测试人员可以启用 “微软 Edge 分屏”flag，在一个浏览器窗口中......
### [2023年不要买的五件东西](https://36kr.com/p/2100368875553152)
> 概要: 2023年不要买的五件东西-36氪
### [下一代3D封装竞赛开打：为什么是混合键合？](https://36kr.com/p/2101282905653638)
> 概要: 下一代3D封装竞赛开打：为什么是混合键合？-36氪
### [毁誉参半，《深海》到底行不行？](https://www.huxiu.com/article/777002.html)
> 概要: 本文来自微信公众号：幕味儿 （ID：movie1958），作者：刘艺源，题图来自：《深海》八年前，田晓鹏导演的《大圣归来》可谓是开创了国漫创作的新局面，成为一部现象级的划时代作品。而作为田晓鹏导演打磨......
### [从摇滚鼓手到债券之王——走近草根出身的“新债王”杰弗里·冈拉克](https://www.yicai.com/news/101657806.html)
> 概要: 自从2010年4月成立以来，冈拉克旗下双线资本公司成为历史上成长最快的基金公司，管理的资产已经达到640亿美元。
### [小米汽车被曝前脸与车尾设计图 官方回应非最终文件](https://www.yicai.com/news/101657811.html)
> 概要: 小米方面表示，泄密图片涉及的供应商仅是为模具打样的供应商，泄密的文件是非常早期的招标过程的设计稿，并非最终文件。
### [农历新年前后加州连发枪击案，3天已有19人死亡](https://www.huxiu.com/article/777042.html)
> 概要: 本文来自微信公众号：每经头条 （ID：nbdtoutiao），作者：李孟林，编辑：程鹏、兰素英、盖源源，校对：刘小英，原文标题：《农历新年前后，加州接连发生大规模枪击，3天已有19人死亡！美国开年平均......
### [热榜第一！江西一地明后两天全员核酸，当地回应：为统计阳性感染率，不做强制要求](https://finance.sina.com.cn/jjxw/2023-01-24/doc-imychywn1396775.shtml)
> 概要: 每经编辑 张锦河 1月24日，“江西一地明后两天全员核酸”引发广泛关注，该话题登上每经热榜第一。截图来源：每日经济新闻APP 1月24日...
### [摩托罗拉 Moto G53 / G73 5G 发布，搭载 120Hz 屏、安卓 13](https://www.ithome.com/0/669/415.htm)
> 概要: IT之家1 月 24 日消息，摩托罗拉今天发布了 Moto G53 5G 和 Moto G73 5G，这两款手机定位入门机型，一起了解一下。IT之家了解到，这两款手机都采用 6.5 英寸显示屏，刷新率......
### [《满江红》票房领跑春节档 原著兼编剧陈宇：和张艺谋的合作打9分，共同创造一个很绝的故事](https://finance.sina.com.cn/jjxw/2023-01-24/doc-imychywn1403634.shtml)
> 概要: 每经记者 毕媛媛每经实习记者 宋美璐每经编辑 梁枭 电影春节档越战越酣畅。 据灯塔专业版实时数据，截至1月24日18时42分，2023春节档（1月21日～1月27日）总票房（含预售）...
### [春节档票房突破37亿！300亿先生来了！](https://finance.sina.com.cn/china/gncj/2023-01-24/doc-imychywk4639251.shtml)
> 概要: 影院人头攒动，爆米花香味扑鼻而来，中国电影市场迎来开门红。 据灯塔专业版数据，截至1月24日19时，2023年春节档总票房（含预售）已突破37亿元。
### [300亿元，吴京再创新记录](https://finance.sina.com.cn/jjxw/2023-01-24/doc-imychywk4657780.shtml)
> 概要: 每经编辑 张锦河 猫眼专业版数据显示，2023年1月24日18时27分，2023年春节档（1月21日-1月27日）总票房（含预售）破37亿。《满江红》《流浪地球2》《熊出没·伴我“熊芯”》分...
### [《狂飙》爱奇艺热度值破10600 成站内总榜TOP2](https://ent.sina.com.cn/v/m/2023-01-24/doc-imychywn1449238.shtml)
> 概要: 新浪娱乐讯 24日，电视剧爱奇艺站内热度值突破10600，登站内总榜TOP 2，已超越《风吹半夏》等热剧，仅次于热度值10745的。(责编：珞小嬜)......
### [流言板活塞对博扬坚持要价无保护首轮，湖人只愿送出乐透保护首轮](https://bbs.hupu.com/57557371.html)
> 概要: 虎扑01月24日讯 根据记者Jovan Buha的报道，联盟圈子中的消息显示，目前活塞球员博扬-波格丹诺维奇的名字仍然经常和湖人联系在一起。根据之前的报道，活塞仍然坚持对博扬的最低要价是一个不受保护的
### [古天乐方回应编剧被拖欠薪金:按合约支付相关酬金](https://ent.sina.com.cn/m/c/2023-01-24/doc-imychywn1462205.shtml)
> 概要: 新浪娱乐讯 据媒体报道 有关“香港编剧权益联盟”于昨日发表公开信，要求就《纸皮婆婆》不公平真相，与电影制作公司“天下一”老板古天乐（古仔）直接对话一事，古仔方已委托律师行作出回应。　　律师指称“天下一......
### [流言板跟队：切尔西想报价又不想付恩佐解约金，凯塞多交易也难成](https://bbs.hupu.com/57557472.html)
> 概要: 虎扑01月24日讯 Football.London切尔西方面记者Adam Newson直播回答了切尔西球迷的一些问题。球迷提出的问题是：Is it possible that Benfica will
### [反超，票房破13亿！《满江红》为何能靠口碑逆袭？编剧陈宇：和张艺谋共同创造一个很绝的故事](https://finance.sina.com.cn/china/gncj/2023-01-24/doc-imychywn1469145.shtml)
> 概要: 电影春节档越战越酣畅。 据猫眼专业版实时数据，截至1月24日19时38分，2023春节档（1月21日～1月27日）总票房（含预售）突破38亿元...
### [流言板申京：以前总是三节好球末节乱打，今天我们整场都打得无私](https://bbs.hupu.com/57557605.html)
> 概要: 虎扑01月24日讯 火箭119-114战胜森林狼，火箭球员阿尔佩伦-申京接受采访。申京表示：以前的比赛我们总是能打三节好球，然后第四节各自为战，但今天的比赛我们每一节的表现都一样，都在分享球，没有人打
### [《最后的生还者》粉丝相信Abby将出现在第二季](https://www.3dmgame.com/news/202301/3861200.html)
> 概要: 一些《最后的生还者》粉丝相信HBO已经确定了Abby Anderson这个角色会出现在第二季。在HBO《最后的生还者》真人剧上周首播后，粉丝账号The Last of Us News发现导演，联合编剧......
### [流言板邮报独家：贝尔萨告知埃弗顿他们阵容速度太慢，很难踢得好](https://bbs.hupu.com/57557733.html)
> 概要: 虎扑01月24日讯 根据每日邮报东北部著名记者Craig Hope的独家报道，前利兹联主教练贝尔萨明确告知埃弗顿，他们的阵容“太慢了”，不能按照他想要的风格完成比赛任务。如果想要他取代弗兰克-兰帕德成
# 小说
### [极品大道祖](http://book.zongheng.com/book/1171179.html)
> 作者：悟则演八方

> 标签：都市娱乐

> 简介：万千道法中择一脉便可修至长生！当文明被定义后是走向归宿？还是会极限再创辉煌？当道法与文明相遇，又会擦出怎样的火花？神奇的大千世界，浩瀚的宇宙星空，梦字脉修道者一直都在！

> 章节末：168  开启新征程（大结局）

> 状态：完本
# 论文
### [Hindi/Bengali Sentiment Analysis Using Transfer Learning and Joint Dual Input Learning with Self Attention | Papers With Code](https://paperswithcode.com/paper/hindi-bengali-sentiment-analysis-using)
> 概要: Sentiment Analysis typically refers to using natural language processing, text analysis and computational linguistics to extract affect and emotion based information from text data. Our work explores how we can effectively use deep neural networks in transfer learning and joint dual input learning settings to effectively classify sentiments and detect hate speech in Hindi and Bengali data. We start by training Word2Vec word embeddings for Hindi \textbf{HASOC dataset} and Bengali hate speech and then train LSTM and subsequently, employ parameter sharing based transfer learning to Bengali sentiment classifiers by reusing and fine-tuning the trained weights of Hindi classifiers with both classifier being used as baseline in our study. Finally, we use BiLSTM with self attention in joint dual input learning setting where we train a single neural network on Hindi and Bengali dataset simultaneously using their respective embeddings.
### [Match Cutting: Finding Cuts with Smooth Visual Transitions | Papers With Code](https://paperswithcode.com/paper/match-cutting-finding-cuts-with-smooth-visual)
> 日期：11 Oct 2022

> 标签：None

> 代码：https://github.com/netflix/matchcut

> 描述：A match cut is a transition between a pair of shots that uses similar framing, composition, or action to fluidly bring the viewer from one scene to the next. Match cuts are frequently used in film, television, and advertising. However, finding shots that work together is a highly manual and time-consuming process that can take days. We propose a modular and flexible system to efficiently find high-quality match cut candidates starting from millions of shot pairs. We annotate and release a dataset of approximately 20k labeled pairs that we use to evaluate our system, using both classification and metric learning approaches that leverage a variety of image, video, audio, and audio-visual feature extractors. In addition, we release code and embeddings for reproducing our experiments at github.com/netflix/matchcut.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
