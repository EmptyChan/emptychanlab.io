---
title: 2022-12-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WinterberryBush_ZH-CN1414026440_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-19 22:22:47
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WinterberryBush_ZH-CN1414026440_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [大厂APP正默默撤下banner](https://www.woshipm.com/pd/5709609.html)
> 概要: 现在，越来越多大厂的APP都开始撤下、或者缩小banner区的存在，这一趋向一方面隐藏了产品交互上的原因，另一方面也可能透露出了大厂们在提升广告投放效率等方面的思考。具体如何解读APP们的banner......
### [爽文被搬上小程序短剧：3天拍100集，45天赚1300万](https://www.woshipm.com/it/5709349.html)
> 概要: 现在，越来越多人涌入了小程序短剧的风口，而这类微信小程序短剧的商业模式和原先短视频平台短剧的商业模式却有些不同，许多涌入其中的人更多赚的还是“快钱”。具体如何解读微信小程序短剧的出现和发展？不如来看看......
### [涌入AIGC的创业者，该靠什么活下去](https://www.woshipm.com/it/5709726.html)
> 概要: 谈及VR，我们能够想到与它齐肩的就只剩下AI了，从前的AI大都源于巨头，而这一波AI热潮则是来源于普通用户，皆是终端消费者能够看得见用得着的产品。而如何搭建起与终端消费者的良性互动生态，将是这一次AI......
### [lululemon模仿者游戏](https://www.huxiu.com/article/745040.html)
> 概要: 出品｜虎嗅商业消费组作者｜昭晰编辑｜苗正卿题图｜视觉中国北京朝阳大悦城一楼，lululemon标志性的红圈logo高悬。陈列区，五层高的格柜里，整齐叠放着瑜伽服，格柜上方，一整排半身模特道具，身穿不同......
### [心理生存恐怖游戏《Holstin》上架Steam](https://www.3dmgame.com/news/202212/3858706.html)
> 概要: 心理生存恐怖游戏《Holstin》上架Steam平台，自带简中。预告片：这是一款心理生存恐怖游戏，故事发生在一个诡异的、与世隔绝的90年代波兰小镇，小镇笼罩在一股不祥的气息之下。你是来寻找答案的，但每......
### [《巫师3》超《战神5》成今年MTC评分第二高PS5游戏](https://www.3dmgame.com/news/202212/3858708.html)
> 概要: 《巫师3》PS5版现已超过了《战神5》成为Metacritic上媒体评分第二高的PS5游戏，仅次于《艾尔登法环》。Metacritic上《巫师3》PS5版收录了24家媒体评分，综合平均分96分。相比来......
### [尾田荣一郎：海贼王不会那么快完结 明年“大混战”](https://acg.gamersky.com/news/202212/1548522.shtml)
> 概要: 在本周末举办的jump festa 2023上，海贼王舞台部分也是相当吸引人，漫画作者尾田荣一郎的寄语公开了漫画接下来的剧情走向。
### [专家谈RPG正确玩法 该低等级挑战利于个人思维成长](https://www.3dmgame.com/news/202212/3858720.html)
> 概要: 近日一位思维科学专家关于游戏玩法的建议被日本玩家发掘了出来，源自其很久以前的言论，称玩RPG时就该低等级差装备挑战，利于个人PDCA思维成长，而升够级攒够资源去碾压敌人并无益处。·言论出自石松拓人的意......
### [搭建一站式医学增材智造平台，「瓴域影诺」要推动医疗3D打印规模化发展](https://36kr.com/p/2043447441804545)
> 概要: 搭建一站式医学增材智造平台，「瓴域影诺」要推动医疗3D打印规模化发展-36氪
### [动画电影《哆啦A梦：大雄与天空的理想乡》公开预告片](https://news.dmzj.com/article/76570.html)
> 概要: 《哆啦A梦》系列最新动画电影《哆啦A梦：大雄与天空的理想乡》公开了预告片。在这次的预告片中，使用了NiziU演唱的新主题曲《Paradise》的片段。
### [《后浪》获发行许可证 吴刚赵露思罗一舟主演](https://ent.sina.com.cn/v/m/2022-12-19/doc-imxxepna7136943.shtml)
> 概要: 新浪娱乐讯 近日，吴刚、赵露思、罗一舟主演的电视剧《后浪》过审，已经取得发行许可证。据悉，《后浪》由四川耀客文化传媒有限公司制作，编剧为六六，共40集......
### [汤臣杰逊 X 海澜之家内衣｜品牌新视觉](https://www.zcool.com.cn/work/ZNjMzODMzMjA=.html)
> 概要: Play VideoPlayreplay 10 secondsforward 10 secondsCurrent Time0:00/Duration Time0:00Progress: NaN%Pla......
### [布兰妮父亲和前夫将合作写书 内容关于如何当父亲](https://ent.sina.com.cn/y/youmei/2022-12-19/doc-imxxettt8565165.shtml)
> 概要: 新浪娱乐讯 12月19日，据报道，布兰妮的父亲和前夫即将合作写一本关于“当父亲”的书。靠布兰妮的离婚赡养费十几年来每天不工作在家数钱的前夫Kevin Federine 正在计划出一本关于他“如何当父亲......
### [大叔食堂小二烧肉六周年店庆，门店再掀火爆热潮！](http://www.investorscn.com/2022/12/19/104899/)
> 概要: 6年的时光，对于一家餐饮品牌而言，交出一份怎样的成绩单才算合格......
### [舍得酒业兔年文创生肖酒上新，抢跑春节限量发售](http://www.investorscn.com/2022/12/19/104900/)
> 概要: 随着春节临近，白酒企业纷纷开启了抢“春”大战，当然这个春节“攻坚战”也显得格外关键：一方面是惯例性的白酒销售旺季，许多酒企出货的重要时间节点；另一方面，随着国家相关政策的进一步放开，尤其是取消限制跨区......
### [“纯财务背景的投资人不要”](https://www.huxiu.com/article/746220.html)
> 概要: 本文来自微信公众号：投中网 （ID：China-Venture），作者：张雪，编辑：张丽娟，头图来自：视觉中国我最近听一个猎头朋友Jerry（化名）提到：“今年除了一些地方区域负责人的招聘比较着急，其......
### [ODD web Design](https://www.zcool.com.cn/work/ZNjMzODQ0MzI=.html)
> 概要: Collect......
### [TV动画《肌肉魔法使-MASHLE-》4月开始播出](https://news.dmzj.com/article/76576.html)
> 概要: 根据甲本一原作制作的TV动画《肌肉魔法使-MASHLE-》宣布了将于2023年4月开始播出的消息。本作的主宣传图与前导PV也一并公开。
### [海外new things | 印度尼西亚农业技术初创「Eratani」种子轮融资380万美元，为中小农户提供营运资金支持](https://36kr.com/p/2043266779630592)
> 概要: 海外new things | 印度尼西亚农业技术初创「Eratani」种子轮融资380万美元，为中小农户提供营运资金支持-36氪
### [海外New Things |「SafeAI」获3800万美元B轮融资，加速重型设备自动化](https://36kr.com/p/2050624535549189)
> 概要: 海外New Things |「SafeAI」获3800万美元B轮融资，加速重型设备自动化-36氪
### [DPI 3.2，润土投资的专注与稳健](http://www.investorscn.com/2022/12/19/104907/)
> 概要: 11月6日，在润土投资2022年内部交流会上，一份成绩单映入LP眼帘。据润土披露，其综合基金以DPI 3.2的数字，为LP交上了一份在资本寒冬里显得尤为亮眼的成绩单。此外，还有7家润土投资企业正在申请......
### [柴霖霖插画-剪映LOGO风格化演绎](https://www.zcool.com.cn/work/ZNjMzODUxNzY=.html)
> 概要: 柴霖霖插画-剪映LOGO风格化演绎尝试用了不用风格去演绎绘制剪映LOGO......
### [海外new things | 澳大利亚无人机开发商「Swoop Aero」获USAID注资150万美元，已完成2万次飞行](https://36kr.com/p/2044373437860871)
> 概要: 海外new things | 澳大利亚无人机开发商「Swoop Aero」获USAID注资150万美元，已完成2万次飞行-36氪
### [2022游戏安全行业峰会成功举办！](https://ol.3dmgame.com/news/202212/35994.html)
> 概要: 2022游戏安全行业峰会成功举办！
### [ALTER《十三机兵防卫圈》冬坂五百里1/7比例手办](https://news.dmzj.com/article/76577.html)
> 概要: ALTER根据《十三机兵防卫圈》中的冬坂五百里制作的1/7比例手办正在预订中。本作再现了冬坂五百里启动机兵时的样子。
### [雷克萨斯怎么突然在中国卖不动了？](https://www.huxiu.com/article/746302.html)
> 概要: 本文来自微信公众号：经济观察网 （ID：eeojjgcw），作者：周菊，头图来自：视觉中国在华销量连续17年增长的豪华品牌“黑马”雷克萨斯，却在今年一改向上态势，出现了罕见的销量下滑。上险量数据显示，......
### [梅西圆梦了，餐饮圈尚未](https://www.huxiu.com/article/745171.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国虎嗅注：12月19日凌晨，阿根廷足球队在世界杯时隔36年夺冠，球王梅西圆梦卡塔尔。在过去一个多月，餐饮圈进入了“世界杯消费季”。但曾经在2014、2018......
### [圣诞假期前，苹果美国 iPhone 14 Pro / Max 发货延迟缩短到 1-2 周](https://www.ithome.com/0/662/119.htm)
> 概要: IT之家12 月 19 日消息，随着圣诞假期的临近，苹果在线商店的 iPhone 14 Pro、iPhone 14 Pro Max 型号的供货情况开始改善，这表明该公司在生产面临中断后可能已基本赶上需......
### [桥本环奈《黑色夜晚大游行》电影剧照 12.23日上映](https://www.3dmgame.com/news/202212/3858749.html)
> 概要: 根据同名人气漫画改编，桥本环奈主演的搞笑电影《黑色夜晚大游行》电影预定12月23日上映，日前官方了多张新拍摄花絮剧照，一起来欣赏下。《黑色夜晚大游行》的男主人公名叫日野三春，现在一名自由职业者，没有通......
### [《肌肉魔法使-MASHLE-》舞台剧化 2023年7月上演](https://news.dmzj.com/article/76581.html)
> 概要: 甲本一创作的《肌肉魔法使-MASHLE-》宣布了舞台剧化的消息。本作将于2023年7月在日本上演。这次的舞台剧将由松崎史也担任综合演出，龟田真二郎负责剧本。
### [小米消金：全力支撑复工复产，助力消费者共度难关](http://www.investorscn.com/2022/12/19/104916/)
> 概要: 近日，随着疫情防控形势逐步趋稳向好，重庆小米消费金融有限公司认真落实重庆银保监局《关于做好银行业保险业支持复工复产工作的通知》要求，重点围绕疫情防控与业务开展，积极响应，全面推进复工复产......
### [远端安全气囊，出海新标配？](https://www.ithome.com/0/662/145.htm)
> 概要: 原文标题：《国产车忙出海，会让新车更安全？》如果在今年 8 月之后喜提一台 Model Y 新车，会额外获得一个国内尚属罕见的新配置，远端安全气囊。同样捏着这张 SSR 卡牌的，还有蔚来 ET7、极星......
### [比 IF 函数更合适，Excel 计算阶梯提成，用 VLOOKUP 就够了](https://www.ithome.com/0/662/150.htm)
> 概要: 最近我在苦恼一个问题。前阵子老板让我用 Excel 给公司同事们算提成，层层嵌套的 IF 函数把给我写伤了。可那天瞥到新来的同事只用了很简短的一串函数，就算好了提成。为什么？这到底是为什么？在我苦思冥......
### [阿根廷夺冠引爆舆论 ，世界杯成平台流量密码？](https://www.yicai.com/news/101628000.html)
> 概要: 短期而言，转播世界杯可以让平台依靠IP吸引大量用户和广告主，围绕转播进行大量、大额的商业化运营。
### [BNB股价暴跌！币圈或将再起波澜？](https://www.yicai.com/video/101628028.html)
> 概要: BNB股价暴跌！币圈或将再起波澜？
### [沪指大跌近2% 3100点附近是否有支撑？](https://www.yicai.com/video/101628039.html)
> 概要: 沪指大跌近2% 3100点附近是否有支撑？
### [《英雄联盟》2022 净化成果：故意送人头玩家减少 50%，辱骂 / 挂机玩家减少 1/3](https://www.ithome.com/0/662/164.htm)
> 概要: IT之家12 月 19 日消息，腾讯游戏安全中心今日公布《英雄联盟》2022 净化成果。官方表示，2022 年游戏中故意送人头的玩家减少约 50%，在游戏中发送辱骂言论、挂机的玩家减少了约 1/3。官......
### [中概股摘牌风险解除，众多国际机构增加互联网敞口](https://www.yicai.com/news/101628091.html)
> 概要: 常态化监管、平台公司降本成效凸显等有助于提振投资者的信心。
### [演员金宝罗和经纪公司合约到期不续签](https://ent.sina.com.cn/s/j/2022-12-19/doc-imxxfkrm8222602.shtml)
> 概要: 新浪娱乐讯 据媒体报道，演员金宝罗和现经纪公司合约到期不续签。据悉，金宝罗曾出演《天空之城》《她的私生活》等热门剧集。(责编：小5)......
### [顺丰控股：业务量连续6个月增长，电商退货件份额大幅提高](http://www.investorscn.com/2022/12/19/104918/)
> 概要: 12月19日晚间,顺丰控股(002352.SZ)披露了《2022年11月快递物流业务经营简报》。公告显示,公司2022年11月营业收入223.18亿元。其中,速运物流业务营业收入为157.19亿元,同......
### [稳健医疗：近期抗原试剂收入超过今年前面几个月的总和](https://www.yicai.com/news/101628121.html)
> 概要: 稳健医疗提示，抗原检测试剂产品目前占公司收入的比重不到1%，请投资者注意投资风险。
### [11部门印发意见 多措并举推动家政进社区](https://finance.sina.com.cn/china/2022-12-19/doc-imxxfqxi8096499.shtml)
> 概要: 新华社北京12月19日电（记者潘洁）国家发展改革委等11部门印发的《关于推动家政进社区的指导意见》19日对外公布。意见明确五方面推动家政进社区的重点任务，提出到2025年...
### [老年人疫苗接种有何禁忌？如何让老年人接种更加便利？——权威专家解答防疫热点问题](https://finance.sina.com.cn/china/2022-12-19/doc-imxxfqxm4874554.shtml)
> 概要: 老年人疫苗接种有何禁忌？如何让老年人接种更加便利？——权威专家解答防疫热点问题
### [流言板梅西庆祝Ins点赞数达43m，超“梅罗对弈”成体育类历史第一](https://bbs.hupu.com/57039788.html)
> 概要: 虎扑12月19日讯 梅西在率领阿根廷多的世界杯冠军后，在Instagram上发布了一则庆祝动态。16小时内，这条动态的点赞数达到了4289万。这也超越了此前C罗发布的“梅罗对弈”LV广告，成为体育类I
### [中共中央、国务院：统筹构建规范高效的数据交易场所](https://finance.sina.com.cn/china/2022-12-19/doc-imxxfqxi8097862.shtml)
> 概要: 财联社12月19日电，中共中央、国务院发布关于构建数据基础制度更好发挥数据要素作用的意见，其中提到，统筹构建规范高效的数据交易场所。
### [中共中央、国务院：构建数据安全合规有序跨境流通机制](https://finance.sina.com.cn/china/2022-12-19/doc-imxxfqxm4875189.shtml)
> 概要: 财联社12月19日电，中共中央、国务院发布关于构建数据基础制度更好发挥数据要素作用的意见，其中提到，构建数据安全合规有序跨境流通机制。
### [中共中央、国务院：完善数据全流程合规与监管规则体系](https://finance.sina.com.cn/china/2022-12-19/doc-imxxfqxs3930280.shtml)
> 概要: 财联社12月19日电，中共中央、国务院发布关于构建数据基础制度更好发挥数据要素作用的意见，其中提到，完善数据全流程合规与监管规则体系。
### [赛场速递不行还有我！皮特森三分没扔进，林葳跳高补篮](https://bbs.hupu.com/57039841.html)
> 概要: 赛场速递不行还有我！皮特森三分没扔进，林葳跳高补篮
### [赛场速递葳少发威！同曦再抢断，林葳一条龙上篮](https://bbs.hupu.com/57039849.html)
> 概要: 赛场速递葳少发威！同曦再抢断，林葳一条龙上篮
### [流言板Stein：关于国王GM蒙特-麦克奈尔的续约谈判已经取得了进展](https://bbs.hupu.com/57039940.html)
> 概要: 虎扑12月19日讯 根据记者Marc Stein的报道，联盟消息人士透露，关于国王总经理蒙特-麦克奈尔的续约谈判已经取得了进展，预计会在明年年初正式达成续约。本赛季是麦克奈尔当前三年合同中的最后一年。
# 小说
### [末日霸权](https://book.zongheng.com/book/792020.html)
> 作者：梦里银河

> 标签：科幻游戏

> 简介：因守护而兴杀伐，因杀伐而衍霸权！林辰身负逆天宝物和奇异能力，回到末日前夕。展望未来，林辰雄姿英发。要让这天，不能逆他意；让这地，不能遮他眼！（林辰：其实霸权什么的我原本都没太在意。我只是想给我所爱者提供至高守护。然而踩的人多了，没人敢惹了，也就成了霸权。）

> 章节末：0971 踏上新征途（完本）

> 状态：完本
# 论文
### [UKP-SQuARE v2 Explainability and Adversarial Attacks for Trustworthy QA | Papers With Code](https://paperswithcode.com/paper/ukp-square-v2-explainability-and-adversarial)
> 日期：19 Aug 2022

> 标签：None

> 代码：https://github.com/ukp-square/square-core

> 描述：Question Answering (QA) systems are increasingly deployed in applications where they support real-world decisions. However, state-of-the-art models rely on deep neural networks, which are difficult to interpret by humans. Inherently interpretable models or post hoc explainability methods can help users to comprehend how a model arrives at its prediction and, if successful, increase their trust in the system. Furthermore, researchers can leverage these insights to develop new methods that are more accurate and less biased. In this paper, we introduce SQuARE v2, the new version of SQuARE, to provide an explainability infrastructure for comparing models based on methods such as saliency maps and graph-based explanations. While saliency maps are useful to inspect the importance of each input token for the model's prediction, graph-based explanations from external Knowledge Graphs enable the users to verify the reasoning behind the model prediction. In addition, we provide multiple adversarial attacks to compare the robustness of QA models. With these explainability methods and adversarial attacks, we aim to ease the research on trustworthy QA models. SQuARE is available on https://square.ukp-lab.de.
### [Cross-Domain Aspect Extraction using Transformers Augmented with Knowledge Graphs | Papers With Code](https://paperswithcode.com/paper/cross-domain-aspect-extraction-using)
> 日期：18 Oct 2022

> 标签：None

> 代码：https://github.com/intellabs/nlp-architect

> 描述：The extraction of aspect terms is a critical step in fine-grained sentiment analysis of text. Existing approaches for this task have yielded impressive results when the training and testing data are from the same domain. However, these methods show a drastic decrease in performance when applied to cross-domain settings where the domain of the testing data differs from that of the training data. To address this lack of extensibility and robustness, we propose a novel approach for automatically constructing domain-specific knowledge graphs that contain information relevant to the identification of aspect terms. We introduce a methodology for injecting information from these knowledge graphs into Transformer models, including two alternative mechanisms for knowledge insertion: via query enrichment and via manipulation of attention patterns. We demonstrate state-of-the-art performance on benchmark datasets for cross-domain aspect term extraction using our approach and investigate how the amount of external knowledge available to the Transformer impacts model performance.
