---
title: 2023-01-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HighArchChina_ZH-CN8170154553_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-01-26 20:03:01
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HighArchChina_ZH-CN8170154553_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [新品上市想卖爆？这5点做对才有戏](https://www.woshipm.com/marketing/5735605.html)
> 概要: 在新品上市营销阶段，通常会涉及较多的营销内容，且很多内容都是环环相扣，如果不提前做好规划，可能会让一组漂亮的营销组合拳变成无效的吸金黑洞。那么，新品营销时要注意什么呢？本文作者总结了5个误区，一起来看......
### [20 年前的「电子垃圾」，成了小红书、TikTok 上的顶流](https://www.woshipm.com/it/5735643.html)
> 概要: 在过去不久的2022年，怀旧是一个关键词，复古潮不止席卷着文娱内容，也轮到了20年前的电子产品。本文作者对近年来老数码相机翻红热潮进行了总结分析，一起来看一下吧。在过去不久的 2022 年，怀旧是一个......
### [功能下线的艺术](https://www.woshipm.com/operate/5735471.html)
> 概要: 一个产品在其生命周期中总会有错误的决策，需要在过程中随时调整，所以学会删除下线功能是让产品蜕变的必经之路。如何让产品正确下线呢？一起来看一下吧。功能下线向来像是一件难以启齿的事，看似会成为产品生涯的“......
### [交投清淡！美股平收，特斯拉全年交付量创纪录](https://www.yicai.com/news/101658019.html)
> 概要: 标普500成分股公司中逾19%的公司已发布业绩。
### [吴京回应点赞票房成绩“只服吴京”：手滑了](https://ent.sina.com.cn/m/c/2023-01-26/doc-imycnmqu4136942.shtml)
> 概要: 新浪娱乐讯 1月26日消息，昨日（1.25）下午，演员吴京被发现点赞了一条列举他的票房成绩和表示“只服吴京”的微博。随后，他取消了这个点赞，并发文称：“手滑了。”加上大笑和抱拳表情。网友在评论区表示：......
### [《我的世界：传奇》上市日期公布 全新预告片欣赏](https://www.3dmgame.com/news/202301/3861247.html)
> 概要: 在今天的Xbox开发者直面会上，《我的世界：传奇》正式宣布上市日期。本作将于2023年4月18日发售，登陆PC、Xbox One和Xbox Series X|S平台。本作将于上市首日同时登陆Xbox ......
### [《海贼王》1073话情报：斯图西瞬秒路奇 五老星来了](https://acg.gamersky.com/news/202301/1560112.shtml)
> 概要: 《海贼王》1073话情报公开了，路奇试图攻击斯图西，但他打的只是幻影。斯图西用内部含有海楼石的口红去碰他，路奇没来得及反抗就失去了意识。
### [影院回应《流浪地球2》改播《满江红》：设备出故障](https://www.3dmgame.com/news/202301/3861259.html)
> 概要: 1月24日，有微博网友反映称自己买的《流浪地球2》电影票被影院以“设备故障”为理由退款，随后又发现影院将当天同一时段的同一放映厅让给了同期上映的《满江红》。消息曝光后引发争议。对此，涉事电影院工作人员......
### [日薪千元留人，春节快递还是没送到](https://www.huxiu.com/article/777665.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：唐亚华，题图来自：视觉中国“过年也能收到快递，春节不放假的商家和快递我真的爱了”“我买的花初一就到站点了，但是不派送，要到初六之后......
### [出海周刊31期 | 出海，互联网大厂必修课](https://36kr.com/p/2096227911155844)
> 概要: 出海周刊31期 | 出海，互联网大厂必修课-36氪
### [中国制造汽车出口大涨！中国车企：欧洲消费者正慢慢习惯中国产品的高质量](https://www.ithome.com/0/669/549.htm)
> 概要: 1 月 26 日消息，在过去几年里，中国汽车产业已经取得了长足的进步，国产汽车在国际市场上正变得越来越有竞争力。分析师预计，中国已经超过德国，成为仅次于日本的世界第二大乘用车出口国，并有望成为全球最大......
### [【字幕】外媒：八村垒的无球防守一直是弱项，但AD可以掩盖这个问题](https://bbs.hupu.com/57577965.html)
> 概要: ﻿【三叶屋出品】JR们如果本作品满意，可以点右下方推荐👍，能让更多人看到自己喜欢的球星！
### [1500亿，史上最大S基金来了](https://36kr.com/p/2103128255168899)
> 概要: 1500亿，史上最大S基金来了-36氪
### [LP提着猪头找不着庙门](https://36kr.com/p/2103083549966471)
> 概要: LP提着猪头找不着庙门-36氪
### [漫画《DOG SIGNAL》TV动画化决定](https://news.dmzj.com/article/76933.html)
> 概要: 漫画《DOG SIGNAL》宣布了TV动画化决定的消息，本作将于2023年秋播出。
### [GSC《月姬 -A piece of blue glass moon-》爱尔奎特手办](https://news.dmzj.com/article/76934.html)
> 概要: GSC根据《月姬 -A piece of blue glass moon-》中的爱尔奎特制作的POP UP PARADE系列手办正在预订中。
### [杨紫琼入围奥斯卡影后，这次成龙亏大了](https://www.huxiu.com/article/777651.html)
> 概要: 本文来自微信公众号：外滩TheBund（ID：the-Bund），作者：Cardi C，题图来自：《瞬息全宇宙》2023年第95届奥斯卡公布提名名单，毫无悬念的，杨紫琼凭借《瞬息全宇宙》入围最佳女主角......
### [视频：独家！新浪娱乐新春群星园游会 送年货扔飞镖看点满满](https://video.sina.com.cn/p/ent/2023-01-26/detail-imycnwen9188051.d.html)
> 概要: 视频：独家！新浪娱乐新春群星园游会 送年货扔飞镖看点满满
### [SE宣布手游《勇者斗恶龙：达伊的大冒险》4月停服](https://www.3dmgame.com/news/202301/3861268.html)
> 概要: 史克威尔艾尼克斯宣布，手游团队ARPG《勇者斗恶龙：达伊的大冒险》将于北京时间4月27日中午12时停止服务。史克威尔艾尼克斯在声明中表示：“本作自2021年9月27日上市以来，目标一直是为玩家提供最令......
### [一家成立13年的民营环保企业挂牌转让：整合兼并成市场主流](https://www.yicai.com/news/101658041.html)
> 概要: 近年来，环保产业市场竞争愈加激烈，行业“洗牌”的趋势更加明显。同质化竞争严重、缺乏核心技术和创新能力的企业逐步淘汰退出。整合、兼并、上市融资成为环保行业市场主流。
### [俄罗斯供应冲击将至？ 欧佩克正陷入两难境地](https://www.yicai.com/news/101658083.html)
> 概要: 下半年供需形势或迎转折点，油价上限取决于亚洲。
### [集英社编辑讨论修改漫画引热议 作者要有自己的思想](https://acg.gamersky.com/news/202301/1560218.shtml)
> 概要: 在日本漫画界中，编辑对于一部作品的影响力一直是漫画迷们关注的焦点。那么站在漫画家的角度来说，究竟该听编辑的建议听到什么程度才好呢？
### [英特尔新款 56 核工作站处理器 W9-3495X 跑分曝光，略弱于 AMD 线程撕裂者 PRO 5995WX](https://www.ithome.com/0/669/581.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家1 月 26 日消息，英特尔首款用于工作站的 Sapphire Rapids 处理器在发布前曝光了跑分信息，型号为至强 W9-3495X，是该系列的最高端......
### [TV动画《虽然等级只有1级但固有技能是最强的》PV](https://news.dmzj.com/article/76935.html)
> 概要: TV动画《虽然等级只有1级但固有技能是最强的》宣布了将于2023年7月开始播出的消息。本作的PV也一并公开。在这次公开的PV中，可以看到主人公佐藤亮太和女主角艾米莉相遇等的场景。
### [《霍格沃茨之遗》将会拥有100多个支线任务](https://www.3dmgame.com/news/202301/3861275.html)
> 概要: RPG新作《霍格沃茨之遗》将于二月正式发售。本作设定在《哈利波特》系列小说第一部的100年之前，讲述一位拥有奇异能力的五年级学生加入魔法学校的故事。基于游戏官方设定集泄露的情报显示，本作将拥有35小时......
### [艾莉扮演者认为《最后的生还者》可能拍摄第二季](https://www.3dmgame.com/news/202301/3861276.html)
> 概要: HBO《最后的生还者》剧集开局不错，口碑和收视率实现双丰收。该剧第一季有9集内容，包含游戏初代和DLC内容。但《最后的生还者》后续剧集是否在制作中呢?在《最后的生还者》中扮演艾莉的贝拉·拉姆齐认为，拍......
### [兔年春节假期前四天，长线游同比增长超500% 境内酒店消费超2019年同期](https://finance.sina.com.cn/jjxw/2023-01-26/doc-imycpann4123591.shtml)
> 概要: 每经记者 杨卉每经编辑 董兴生 春节假期已经过了大半，消费者们正抓紧假期尾巴出行。1月26日下午，《每日经济新闻》记者从飞猪平台获悉，兔年春节前4天...
### [外企衰落15周年祭](https://www.huxiu.com/article/777864.html)
> 概要: 本文来自微信公众号：瞎说职场（ID：HRInsight），作者：Sean Ye，原文标题：《外企衰落11周年祭》，文章首发时间：2019年9月17日，头图来自：视觉中国2018年，如果你问一个应届毕业......
### [男子点海鲜，葱姜费竟相当于菜价60%！餐厅回应：“葱姜”指做法，收的是加工费](https://finance.sina.com.cn/jjxw/2023-01-26/doc-imycphun0776882.shtml)
> 概要: 每经编辑 孙志成 1月26日，#男子称餐厅点海鲜葱姜费占菜价60%#的话题登上热搜，啥情况？ 餐厅回应：收取的是加工费 近日，宁夏银川的李先生反映，大年初三晚上和朋友聚餐...
### [B站与晋江原创订立综合合作框架协议，购买多部作品版权等](https://www.ithome.com/0/669/585.htm)
> 概要: IT之家1 月 26 日消息，哔哩哔哩B站今日在港交所发布公告，公司与晋江原创订立综合合作框架协议，据此，集团与晋江原创同意就知识产权合作，包括但不限于集团购买多部作品 (包括文学作品) 的版权，并将......
### [【字幕】西蒙斯：教练中场时让我们做自己，与恩比德对位很有趣](https://bbs.hupu.com/57584510.html)
> 概要: ﻿【三叶屋出品】JR们如果本作品满意，可以点右下方推荐👍，能让更多人看到自己喜欢的球星！
### [严监管力度加大！年度经纪业务罚单近80张，违规炒股占大头，内控与营销罚单渐增](https://finance.sina.com.cn/jjxw/2023-01-26/doc-imycphuk4016442.shtml)
> 概要: 来源：财联社 监管对于券商经纪业务的关注度与处罚力度正不断加强。易董平台显示，2022年度，经纪业务方面的罚单共77张，其中下半年，4个月多月间...
### [《满江红》片方发文回应抄袭等传闻：无稽之谈](https://ent.sina.com.cn/m/c/2023-01-26/doc-imycphun0794209.shtml)
> 概要: 新浪娱乐讯 1月26日， 电影《满江红》官微针对“<满江红>被指幽灵场 、偷票房、买票房、资本操控、抄袭”等网络传言进行回应并严正声明：均为无稽之谈。对于上述传闻《满江红》各出品方正在收集证据，并已开......
### [怒了！《满江红》发声明：无稽之谈，造谣！腾讯股价冲破重要关口！恒指创去年4月以来新高](https://finance.sina.com.cn/china/gncj/2023-01-26/doc-imycphun0804102.shtml)
> 概要: 1月26日，港股市场迎来兔年的首个交易日，港股板块普遍飘红，主要指数强劲上涨。Wind数据显示，截至收盘，恒生指数涨超2.3%，创去年4月以来新高；恒生科技指数涨超4.2%...
### [波音再现季度亏损，计划增加飞机产量](https://www.ithome.com/0/669/591.htm)
> 概要: IT之家1 月 26 日消息，据华尔街日报报道，波音公司报告再次出现季度亏损，但面对供应链挑战，该公司仍计划今年增加飞机产量和交付量。美媒指出，波音公司连续第四年出现亏损，原因是面临商用飞机生产成本超......
### [港股兔年“开门红”，有望迎来“春季躁动” | 市场观察](https://www.yicai.com/news/101658141.html)
> 概要: 短期或受到大宗商品涨价和美联储加息影响。
### [中国城市快递业务量排行榜：金华蝉联第一，粤东小城升至第四](https://www.yicai.com/news/101658140.html)
> 概要: 根据国家邮政局近期公布的快递业务量前50城市情况表，金华（义乌）市、广州市、深圳市、揭阳市、杭州市、东莞市、上海市、汕头市、苏州市、泉州市位居前十。
### [特斯拉“创纪录成绩单”背后：频繁降价仍未完成目标，皆因马斯克不务正业？](https://finance.sina.com.cn/china/gncj/2023-01-26/doc-imycphuh8947193.shtml)
> 概要: 文|新浪财经 张俊 北京时间1月26日，特斯拉公布了2022年第四季度和全年财报。营收、净利润等财务指标均取得了不错的增长。 但好消息之下，也有忧患。
### [流言板罗马诺：热刺同意付波罗45m解约金，但分3期还是4期仍在拉扯](https://bbs.hupu.com/57586160.html)
> 概要: 虎扑01月26日讯 意大利天空体育著名记者Fabrizio Romano的最新报道，托特纳姆热刺和葡萄牙体育仍然在拉扯。热刺同意支付佩德罗-波罗4500万欧元的解约金，然而他们就分3期还是分4期和葡萄
### [【直播】Doinb：S11被SMK打爆我认，菜我不嘴硬，别的我不认](https://bbs.hupu.com/57586413.html)
> 概要: 【直播】Doinb：S11被SMK打爆我认，菜我不嘴硬，别的我不认
# 小说
### [百秘妖闻录](https://book.zongheng.com/book/919788.html)
> 作者：尘掌柜

> 标签：奇幻玄幻

> 简介：这一世的纷争经历，诡秘冒险，让我们一起见证吧。

> 章节末：第二百九十二章 争端之终（大结局）

> 状态：完本
# 论文
### [Similarity-Based Predictive Maintenance Framework for Rotating Machinery | Papers With Code](https://paperswithcode.com/paper/similarity-based-predictive-maintenance)
> 日期：30 Dec 2022

> 标签：None

> 代码：https://github.com/western-oc2-lab/similarity-based-predictive-maintenance-framework-for-rotating-machinery

> 描述：Within smart manufacturing, data driven techniques are commonly adopted for condition monitoring and fault diagnosis of rotating machinery. Classical approaches use supervised learning where a classifier is trained on labeled data to predict or classify different operational states of the machine. However, in most industrial applications, labeled data is limited in terms of its size and type. Hence, it cannot serve the training purpose. In this paper, this problem is tackled by addressing the classification task as a similarity measure to a reference sample rather than a supervised classification task. Similarity-based approaches require a limited amount of labeled data and hence, meet the requirements of real-world industrial applications. Accordingly, the paper introduces a similarity-based framework for predictive maintenance (PdM) of rotating machinery. For each operational state of the machine, a reference vibration signal is generated and labeled according to the machine's operational condition. Consequentially, statistical time analysis, fast Fourier transform (FFT), and short-time Fourier transform (STFT) are used to extract features from the captured vibration signals. For each feature type, three similarity metrics, namely structural similarity measure (SSM), cosine similarity, and Euclidean distance are used to measure the similarity between test signals and reference signals in the feature space. Hence, nine settings in terms of feature type-similarity measure combinations are evaluated. Experimental results confirm the effectiveness of similarity-based approaches in achieving very high accuracy with moderate computational requirements compared to machine learning (ML)-based methods. Further, the results indicate that using FFT features with cosine similarity would lead to better performance compared to the other settings.
### [Evaluation and Analysis of Different Aggregation and Hyperparameter Selection Methods for Federated Brain Tumor Segmentation | Papers With Code](https://paperswithcode.com/paper/evaluation-and-analysis-of-different)
> 概要: Availability of large, diverse, and multi-national datasets is crucial for the development of effective and clinically applicable AI systems in the medical imaging domain. However, forming a global model by bringing these datasets together at a central location, comes along with various data privacy and ownership problems. To alleviate these problems, several recent studies focus on the federated learning paradigm, a distributed learning approach for decentralized data. Federated learning leverages all the available data without any need for sharing collaborators' data with each other or collecting them on a central server. Studies show that federated learning can provide competitive performance with conventional central training, while having a good generalization capability. In this work, we have investigated several federated learning approaches on the brain tumor segmentation problem. We explore different strategies for faster convergence and better performance which can also work on strong Non-IID cases.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
