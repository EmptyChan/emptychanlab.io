---
title: 2020-12-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WinterBryce_EN-CN5613720629_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-30 21:07:29
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WinterBryce_EN-CN5613720629_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [Hebe谈侵权风波称会好好的 盼一起许愿S.H.E合体](https://ent.sina.com.cn/y/ygangtai/2020-12-30/doc-iiznezxs9671662.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道 田馥甄（Hebe）29日出席代言记者会，是她月初被爆与前东家华研国际闹出录音版权纠纷后，首度公开受访。她表示关于私人的事不多做回应，“我有感受大家的爱，我会好好的，谢谢歌迷......
### [金晨：和李一桐是大学同学 所以合作起来有默契](https://ent.sina.com.cn/v/m/2020-12-30/doc-iiznezxs9767946.shtml)
> 概要: 新京报讯（记者 刘玮）在江苏卫视热播剧中，金晨饰演理性、率真，甚至有些霸道的职场精英沈思怡。12月29日，金晨接受媒体采访时坦言，自己的性格还挺像沈思怡的，“就是很多事情必须要当下解决清楚，不想积压太......
### [乃木坂46参加红白彩排 希望传递充满力量的笑容](https://ent.sina.com.cn/s/j/2020-12-30/doc-iiznctke9326410.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 12月29日，偶像组合乃木坂46在东京涉谷NHK大厅参加“第71届NHK红白歌会”彩排，穿短裤和长外套秀美腿，当天她们演唱了《Route 246》，秋元真夏表示希望大家看......
### [日本票房：《鬼灭之刃》成日本历代票房榜新冠军](https://ent.sina.com.cn/m/f/2020-12-30/doc-iiznezxs9748288.shtml)
> 概要: 日本周末票房排行榜2020年12月19日—12月20日　　12月26日至12月27日日本国内电影票房榜中，《剧场版鬼灭之刃：无限列车篇》连续11周夺冠，由于82个电影院上映MX4D、4DX版，以及分发......
### [2020年你追过多少部国产剧？请打分](https://ent.sina.com.cn/v/m/2020-12-30/doc-iiznctke9291242.shtml)
> 概要: 2020年你追过多少部国产剧？请打分　　当国产剧“菜色齐全”地被端到观众面前，随之而来的就是水涨船高的评价机制。　　---------------　　2020年临近尾声，国产剧“交卷”。　　今年年初，......
### [金星助力舞剧《长城》开年公演 疫情下的普通人凸显“长城精神”](https://new.qq.com/omn/ENT20201/ENT2020123001281800.html)
> 概要: 腾讯娱乐讯（文/三禾）12月29日下午，大型原创现代舞剧《长城》在北京现代舞团举行新闻发布会，总导演高艳津子、作曲张志亮、制作人程青松、导演刘苗苗与《长城》全体舞者出席，宣布舞剧将于2021年1月5日......
### [一周收视率最高的综艺：《追光吧哥哥》跌至第三，榜首众星云集！](https://new.qq.com/omn/20201230/20201230A0D23I00.html)
> 概要: 对于电视剧作品，或者是综艺来说，收视率是非常重要的，这也能看出一档综艺，到底火不火，有多少人喜欢看，而不少观众对于播放量很清楚，收视率却比较陌生，那么就让我们来看下，一周收视率最高的综艺：《追光吧哥哥......
### [关晓彤跨年造型保暖接地气，后台军大衣裹羽绒服秒变时装](https://new.qq.com/omn/20201230/20201230A0725I00.html)
> 概要: 12月30日，关晓彤在个人社交平台分享了一组跨年晚会造型的美照。比起往年“美丽冻人”的性感裙装，今年关晓彤的look可谓是保暖十足。这不，她还写道：“军大衣裹羽绒服是对冰雪盛典的尊重”，真是只看字儿都......
### [杨幂晒三亚度假照，一双长腿都快伸出屏幕了，网友：往回P点](https://new.qq.com/omn/20201230/20201230A0878O00.html)
> 概要: 12月30日，杨幂在个人社交平台晒出一组美照，并附文写道：“今年冬天真的好冷啊，发俩图假装自己在夏天”。与此同时，对于即将到来的2021年，杨幂不忘送出祝福表示：就要跨年啦，每个人都要平安健康哦。  ......
### [女性题材再出新剧，冯小刚执导，主演阵容却被劝退](https://new.qq.com/omn/20201230/20201230A0GBG200.html)
> 概要: 女性话题一直都是热门，杨笠被人举报，再次挑起了大家对女性话题的探讨。2020年，许多女性话题的电视剧爆火，像《二十不惑》、《三十而已》，当然还有播出不久的刘诗诗和倪妮主演的《流金岁月》。这些都是火爆的......
# 动漫
### [【七海建人×吉良吉影】你的名字](https://news.dmzj.com/article/69749.html)
> 概要: 金发，上班族，作息规律，战斗力超高，向往平静生活，偶尔光顾面包店，每天都打骚气领带，请问你的名字是——？
### [距离播出还有11天！TV动画《悠哉日常大王》推出倒计时活动](https://news.dmzj.com/article/69756.html)
> 概要: 即将于2021年1月10日开始播出的TV动画《悠哉日常大王》第三季开始了开播倒计时企划。在这次的企划中，本作的官网和官方推特将在每天公开制作人员和声优绘制的图片。另外官方推特还将进行关注转推抽奖活动。
### [P站美图推荐——头戴式耳机特辑](https://news.dmzj.com/article/69754.html)
> 概要: 大大的耳机和娇小的美少女，这种对比令人欲罢不能啊——
### [艾莉排82名!?TC Candler评选2020年版美颜TOP100](https://news.dmzj.com/article/69750.html)
> 概要: 近日，电影评论网站TC Candler公开了2020年版“世界美颜TOP100”榜单。在这次的榜单中，以色列的女星雅尔谢尔比亚排名第一。另外有不少网友注意到，排在第82位的，竟然是游戏《最后生还者》中的艾莉！
### [鬼岛城堡，山治在三楼，甚平四楼，路飞索隆屋顶，这说明什么？](https://new.qq.com/omn/20201228/20201228A0G7HX00.html)
> 概要: 和之国战争正进入白热化阶段，赤鞘九侠被凯多打败，目前他们受伤严重，没有再战之力。锦卫门痛哭流涕，说什么很丢脸，就算死去也没有脸面见御田，他还抓着路飞的衣领说：“拜托你了，一定要打败凯多。”开战之前赤鞘......
### [神官对谢怜不友好？连续两次考上清华被劝退，难怪引起轩然大波](https://new.qq.com/omn/20201230/20201230A0GSPF00.html)
> 概要: 《天官赐福》在已经播出的内容之中主要讲述了谢怜第三次飞升之后去与君山处理诡新郎的故事，详略得当，画面精美，得来无数的好评。不过谢怜虽然第三次飞升了，但是其他的神官对他可是相当不友好，还发出了“怎么又是......
### [《魔法科高中的优等生》宣布动画化！兄妹齐上阵，名字你看清了吗？](https://new.qq.com/omn/20201228/20201228A0MZ2S00.html)
> 概要: 12月26日，《魔法科高中的优等生》宣布了2021年TV动画化的消息。这一消息是在TV动画《魔法科高校的劣等生 来访者篇》最终话播出后公开的。            并且发布了一个TV动画化的告知PV......
### [航海王：冥王雷利为什么会选择救路飞，从另一个角度看，或许很感人！](https://new.qq.com/omn/20201223/20201223A0IQN400.html)
> 概要: 冥王雷利号称罗杰的右手，是罗杰海贼团副队长，能成为海贼王的副船长，实力不是一般的强。任何一个海贼听闻雷利的名号都应该感到害怕和尊重。而冥王雷利却是路飞的师傅，教导了路飞使用霸气。           ......
### [蜘蛛侠漫画2021新战衣公布 科技感十足神似机器人](https://acg.gamersky.com/news/202012/1350721.shtml)
> 概要: 战衣由漫画家达斯汀·威弗 (Dustin Weaver)设计，他同时还为其绘制了62、63期的封面。
### [《崖上的波妞》明日上映 宫崎骏手写新年祝福](https://acg.gamersky.com/news/202012/1350781.shtml)
> 概要: 《崖上的波妞》将于明日上映，宫崎骏为中国观众手写新年祝福，
# 财经
### [国家药监局批准和记黄埔医药索凡替尼胶囊上市](https://finance.sina.com.cn/china/gncj/2020-12-30/doc-iiznctke9389236.shtml)
> 概要: 来源：国家药监局官网 国家药监局批准索凡替尼胶囊上市 近日，国家药品监督管理局通过优先审评审批程序批准和记黄埔医药（上海）有限公司申报的1类创新药索凡替尼胶囊（商...
### [云南贡山德贡公路对部分路段实行交通管制 行人、车辆禁止通行](https://finance.sina.com.cn/china/dfjj/2020-12-30/doc-iiznctke9385992.shtml)
> 概要: 原标题：云南贡山德贡公路对部分路段实行交通管制 行人、车辆禁止通行 28日，因当前冬季云南德（钦）贡（山）公路部分路段积雪、结冰严重，通行安全隐患大...
### [官方回应交医保被拒现金只能刷卡:人手不够 业务移交暂停电子支付](https://finance.sina.com.cn/china/dfjj/2020-12-30/doc-iiznezxs9806677.shtml)
> 概要: 原标题：官方回应交医保被拒现金只能刷卡：人手不够，业务移交暂停电子支付 近日，湖南益阳有市民称，交医保时工作窗口不接受现金和移动支付，只能刷卡。
### [调研报告：中国财政收支运行进一步回归常态](https://finance.sina.com.cn/roll/2020-12-30/doc-iiznctke9388181.shtml)
> 概要: 中新社北京12月30日电 （记者 赵建华）中国财政科学研究院30日在北京发布《2020年“地方财政经济运行”调研报告》（下称报告）。报告介绍，经济持续修复大背景下...
### [国常会：要求强化措施落实确保农民工按时足额拿到报酬](https://finance.sina.com.cn/china/gncj/2020-12-30/doc-iiznezxs9801902.shtml)
> 概要: 原标题：李克强主持召开国务院常务会议 听取保障农民工工资支付情况汇报 要求强化措施落实确保农民工按时足额拿到报酬等
### [国家发改委：12月以来中国累计发用电量同比增逾10%](https://finance.sina.com.cn/china/gncj/2020-12-30/doc-iiznctke9388629.shtml)
> 概要: 中国国家发展和改革委员会经济运行调节局局长李云卿30日表示，经济运行态势向好叠加强冷空气影响，近期中国用电需求快速增长，调度数据显示12月份以来累计发用电量同比增长...
# 科技
### [Facebook 抢占商标，PrestoSQL 无奈选择更名 Trino](https://segmentfault.com/a/1190000038738867)
> 概要: 今天，知名开源项目 Presto 的三位发起人宣布，将 PrestoSQL 项目的名字改为 Trino。该项目原本是由 Facebook 运营的，但 2019 年年初，Presto 团队的三位创始人离......
### [手写简易版 render 函数](https://segmentfault.com/a/1190000038739981)
> 概要: 前言在这个之前我们需要了解render是什么？可以去之前的文章里面看看《 搞懂vue-render函数（入门篇）》废话不多说，快快快 上车！！！定义一个对象先定义一个数据对象来作为我们的数据let d......
### [技嘉 RTX 3080 Ti/RTX 3060 显卡通过 EEC 认证](https://www.ithome.com/0/527/550.htm)
> 概要: IT之家12月30日消息 据外媒 videocardz 消息，技嘉的多款英伟达 RTX 3080Ti、3060 、3060Ti 显卡今日通过了俄罗斯的 EEC 认证。技嘉官方也表示，将发布全系 RTX......
### [苹果 M1、A14 设计对比：前者尺寸大了 37%](https://www.ithome.com/0/527/523.htm)
> 概要: IT之家 12 月 30 日消息 自从苹果推出 M1 以来，就有人好奇其与 A14 有什么不同。虽然两款 SoC 是基于共同的 CPU 微架构，但 M1 加入了 A14 所没有的额外片上功能，同时 C......
### [ACE: Apple Type-C Port Controller Secrets](https://blog.t8012.dev/ace-part-1/)
> 概要: ACE: Apple Type-C Port Controller Secrets
### [Graph Toy, an interactive graph visualizer using mathematical functions](http://memorystomp.com/graphtoy/)
> 概要: Graph Toy, an interactive graph visualizer using mathematical functions
### [设计沉思录 | 场景化实战-58招聘简历流程优化项目思考](https://www.tuicool.com/articles/iIZBzej)
> 概要: 编辑导读：对于招聘求职平台来说，简历投递是是求职者和招聘方之间一个承上启下的关键动作。本文从使用场景出发，梳理了58招聘简历的优化设计流程，与大家分享。你吃过10块钱一个的煮鸡蛋吗？2016年的十一黄......
### [开源｜qa_match更新啦——新增知识库半自动挖掘模块](https://www.tuicool.com/articles/zYzuArF)
> 概要: qa_match是58同城推出的一款基于深度学习的轻量级问答匹配工具，V1.0版本于2020年3月9日发布，2020年6月更新v1.1版本，可参见开源｜qa_match：一款基于深度学习的层级问答匹配......
# 小说
### [永生之太极仙尊](http://book.zongheng.com/book/132229.html)
> 作者：不流光

> 标签：竞技同人

> 简介：呃。

> 章节末：决定烂尾……

> 状态：完本
# 游戏
### [波士顿动力机器人庆贺新年：节奏感足、有点滑稽！](https://www.3dmgame.com/bagua/4117.html)
> 概要: 今日（12月30日），波士顿动力公布了一段接近3分钟的机器人跳舞视频，这些机器人伴随着《Do You Love Me?》的音乐旋律，看起来有点意思。机器人跳舞视频：从此次公开的影像细节来看，波士顿动力......
### [3DM轻松一刻第431期 有没有挂满丝袜的圣诞节啊](https://www.3dmgame.com/bagua/4120.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！>真是太重了 休息一下给个生路吧火法师这个怎么吃......
### [中国手机在印度依然受青睐销量上涨 小米卖得最好](https://www.3dmgame.com/news/202012/3805290.html)
> 概要: 近日据新加坡媒体报道，印度禁用多款中国App之后，中国手机依然受到印度消费者的青睐。当地市场数据显示，今年10月中国手机在印度的销售量比去年同期增加了170万部。该报道援引印度“ThePrint”网站......
### [TV动画《苍之骑士团》OP公布 2021年1月放送](https://www.3dmgame.com/news/202012/3805291.html)
> 概要: 世嘉旗下著名手机游戏《苍之骑士团》之前已经确定制作TV动画，预计将于2021年1月6日正式开播，目前官方已经公开了OP动画影像。TV动画《苍之骑士团》OP影像：《苍之骑士团》是由老牌日本游戏大厂世嘉与......
### [《使命召唤17：黑色行动冷战》追加3v3狙击战模式](https://www.3dmgame.com/news/202012/3805272.html)
> 概要: 《使命召唤17：黑色行动冷战》周二进行了更新中，追加了一个在《使命召唤16：现代战争》中首次普及的游戏模式——3V3狙击游戏。它通过限定玩家使用狙击步枪来改变常规的枪战模式。Treyarch Stud......
# 论文
### [Can Un-trained Neural Networks Compete with Trained Neural Networks at Image Reconstruction?](https://paperswithcode.com/paper/can-un-trained-neural-networks-compete-with)
> 日期：6 Jul 2020

> 标签：DENOISING

> 代码：https://github.com/MLI-lab/ConvDecoder

> 描述：Convolutional Neural Networks (CNNs) are highly effective for image reconstruction problems. Typically, CNNs are trained on large amounts of training images.
### [CoNCRA: A Convolutional Neural Network Code Retrieval Approach](https://paperswithcode.com/paper/concra-a-convolutional-neural-network-code)
> 日期：3 Sep 2020

> 标签：CODE SEARCH

> 代码：https://github.com/mrezende/concra

> 描述：Software developers routinely search for code using general-purpose search engines. However, these search engines cannot find code semantically unless it has an accompanying description.
