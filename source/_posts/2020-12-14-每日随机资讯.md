---
title: 2020-12-14-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PineGrosbeak_EN-CN5559552831_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-14 21:39:12
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PineGrosbeak_EN-CN5559552831_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [户田惠梨香松坂桃李闪婚 复盘两人秘密恋爱过程](https://ent.sina.com.cn/s/j/2020-12-14/doc-iiznezxs6848085.shtml)
> 概要: （文/枣）　　12月10日，　　今天也出了更多的报道，让我们复盘一下这一对到底是什么时候在一起的！　　2013年9月，他们在富士台的电视剧《花之锁》里面合作，这是他们第一次共演，在这部剧里，他们的角色......
### [组图：韩星李时言亮相 最后一次参加《我独》拍摄](http://slide.ent.sina.com.cn/z/v/slide_4_704_349732.html)
> 概要: 组图：韩星李时言亮相 最后一次参加《我独》拍摄
### [视频：多地出现自称辛巴客服形式诈骗 被骗金额自千元至十万元不等](https://video.sina.com.cn/p/ent/2020-12-14/detail-iiznezxs6834364.d.html)
> 概要: 视频：多地出现自称辛巴客服形式诈骗 被骗金额自千元至十万元不等
### [那英歌迷会发文抵制私生行为：私生不是粉](https://ent.sina.com.cn/s/m/2020-12-14/doc-iiznctke6361363.shtml)
> 概要: 新浪娱乐讯 12月14日，那英全球歌迷会在官方微博发文谴责私生粉丝称：“私生不是粉，抵制一切私生行为，超话亦会屏蔽与未公开行程相关的所有图片信息！”歌迷也纷纷在评论区支持：“支持！真爱姐的人不会这么过......
### [视频：时代少年团粉丝团发声明 禁止粉丝接送机及跟机等行为](https://video.sina.com.cn/p/ent/2020-12-14/detail-iiznezxs6876264.d.html)
> 概要: 视频：时代少年团粉丝团发声明 禁止粉丝接送机及跟机等行为
### [甄子丹晒全家福为儿子庆生，一家四口豪宅内合影，13岁甄济嘉穿西装超有范](https://new.qq.com/omn/20201214/20201214A08ZA300.html)
> 概要: 12月14日，甄子丹在社交平台晒出全家福为儿子庆生，附文“Happy 13th birthday James生日快乐，不敢相信你已是 13岁的少年了！爸爸祝你一生开心，充满力量和幸福！儿子，爱你！  ......
### [《追光吧哥哥》如何去油？想想郑爽的吐槽，找准“帅”点才是正道](https://new.qq.com/omn/20201214/20201214A0JIMO00.html)
> 概要: 纳兰惊梦/文明眼人都看得出来，《追光吧哥哥》分明追的是“浪姐”的脚步。只不过相较于“人间尤物”的“浪姐”们，一连好几个反倒是因为“人间油物”上了热搜。甚至许多人都觉得，郑爽在节目里的吐槽竟然是如此的真......
### [王栎鑫发文官宣离婚！对老婆称呼变室友，吴雅婷四处旅行疑疗伤](https://new.qq.com/omn/20201214/20201214A0IH6P00.html)
> 概要: 12月14日，王栎鑫发文官宣与妻子吴雅婷离婚，5年婚姻生活落下帷幕。            王栎鑫对老婆称呼也变为了亲爱的室友，他表示吴雅婷这些年辛苦了，称赞其是一个称职的好老婆好妈妈和好女儿，感谢吴......
### [“宝藏男孩”汪苏泷：频繁上综艺的背后，藏着“难以诉说”的辛酸](https://new.qq.com/omn/20201214/20201214A0JBSW00.html)
> 概要: 说起网络歌手，你会想起谁呢？许嵩、徐良、本兮、小凌还是汪苏泷呢？笔者的初中时代，就是伴随着这些人的歌声度过的。当年的QQ音乐三巨头，许嵩天天暗恋，徐良天天分手，只有汪苏泷，天天在恋爱。三人虽然风格各不......
### [太惨烈！他跳楼自杀报复变心女友，用生命毁灭对方星途](https://new.qq.com/omn/20201214/20201214A081KO00.html)
> 概要: 前几天港圈发生了一件轰动全港的大新闻。新闻女主角是我曾经写过的2020年度港姐选手廖慧仪            大家应该都有点印象吧？廖慧仪海选开始就是热门选手，曾经被港媒封为“四大热门”之一。   ......
# 动漫
### [青叶真司的主治医师接受媒体采访 京都地检准备起诉](https://news.dmzj.com/article/69563.html)
> 概要: 在12月11日涉嫌纵火烧毁京都动画第一工作室，造成36人死亡，33人受伤的犯罪嫌疑人青叶真司（42岁）的鉴定留置期结束了。
### [CDPR就《2077》优化问题向主机玩家道歉,并承诺退款](https://news.dmzj.com/article/69572.html)
> 概要: CDPR今日发布公告，就《赛博朋克2077》在主机平台上的表现向玩家道歉，官方表示会持续优化和修复游戏，下个补丁将在7天内推出。
### [动画《王者天下》第三季4月再开！新宣传图与PV公开](https://news.dmzj.com/article/69566.html)
> 概要: 根据原泰久原作制作的TV动画《王者天下》第三季宣布了将于2021年4月再开的消息。本作原定于今年4月播出，不过由于新冠疫情的影响，从第5话后宣布延期。随着这次新开播日期的确定，新的宣传图与PV也一并公开。
### [新漫周刊第94期 一周新漫推荐(20201214期)](https://news.dmzj.com/article/69575.html)
> 概要: 本期有勇者赫鲁库作者的同世界观新连载、转生异世界做猪、各类职场喜剧……少女漫画本期突然暴涨，多了特别多韩漫长条新连载。
### [《星期一的丰满》：连线备考 这画面谁能学下去](https://acg.gamersky.com/news/202012/1345666.shtml)
> 概要: 《星期一的丰满》这次久违的黑子酱终于登场了，现在已经进入到考前突击的时期，那个发现黑子酱秘密的同学正在和黑子酱连线中，不过感觉还是一个人安静的备考比较好。
### [《电锯人》动画先导视觉图 由MAPPA负责制作](https://acg.gamersky.com/news/202012/1345674.shtml)
> 概要: 知名动画制作公司MAPPA公布了《电锯人》动画先导视觉图，确认TV动画将由他们负责制作。
# 财经
### [吐鲁番疫情与乌鲁木齐和喀什地区疫情病毒不同源](https://finance.sina.com.cn/china/gncj/2020-12-14/doc-iiznezxs6911211.shtml)
> 概要: 12月14日21时，新疆维吾尔自治区吐鲁番市人民政府新闻办公室召开发布会。发布会上，吐鲁番市卫建委党组书记、副主任明国富介绍，吐鲁番此次疫情与乌鲁木齐和喀什地区疫情病...
### [明年高校毕业生将首破900万人 就业新空间在哪？](https://finance.sina.com.cn/china/2020-12-14/doc-iiznezxs6899392.shtml)
> 概要: 来源：中国经济网 受新冠肺炎疫情影响，今年高校毕业生就业受到了冲击。不过，教育部高校学生司副司长吴爱华近日透露，根据国家统计局统计数据，截至今年9月1日...
### [河南孟州市查处桑坡村高仿服饰店铺：有门店因销售假货成网红店](https://finance.sina.com.cn/china/dfjj/2020-12-14/doc-iiznezxs6909133.shtml)
> 概要: 新京报讯 12月14日，河南孟州市正在组织执法力量对桑坡村服饰市场进行全面检查，并查封多家销售高仿服饰的店铺。一名涉假商家称，“当地严查，大部分店铺已经关门。”
### [全国根治欠薪线索反映小程序开通 举报不受限](https://finance.sina.com.cn/china/gncj/2020-12-14/doc-iiznctke6485022.shtml)
> 概要: 原标题：举报不受限！全国根治欠薪线索反映小程序开通 新华社北京12月14日电（记者姜琳）为进一步畅通和拓宽欠薪维权渠道，国务院根治拖欠农民工工资工作领导小组办公室14...
### [上海第一批家政本科专业明年开始招生 上海妇联将提供助学金支持](https://finance.sina.com.cn/chanjing/cyxw/2020-12-14/doc-iiznctke6482187.shtml)
> 概要: 原标题：“本科保姆”真的来啦！上海第一批家政本科专业明年开始招生 作者：董川峰 编辑：董川峰 将更多的家政从业人员培养成大学本科生。
### [上海经信委赴江苏、山东考察调研核电产业](https://finance.sina.com.cn/roll/2020-12-14/doc-iiznezxs6900712.shtml)
> 概要: 原标题：上海经信委赴江苏、山东考察调研核电产业 e公司讯，上海经信委消息，12月9日至11日，上海市经济信息化工作党委书记陆晓春带队赴江苏连云港...
# 科技
### [让Vue3 Composition API 存在于你 Vue 以外的项目中](https://segmentfault.com/a/1190000038471330)
> 概要: 作者：陈大鱼头github：KRISACHAN作为新特性Composition API，在Vue3正式发布之前一段时间就发布过了。距文档介绍，Composition API是一组低侵入式的、函数式的 ......
### [使用GPU.js改善JavaScript性能](https://segmentfault.com/a/1190000038466873)
> 概要: 你是否曾经尝试过运行复杂的计算，却发现它需要花费很长时间，并且拖慢了你的进程？有很多方法可以解决这个问题，例如使用web worker或后台线程。GPU减轻了CPU的处理负荷，给了CPU更多的空间来处......
### [谷歌大批服务又宕机，5 个月内第 3 次](https://www.ithome.com/0/524/679.htm)
> 概要: 下午9:20:25 更新：已恢复。IT之家 12 月 14 日消息 谷歌旗下的 Gmail、Google Drive、Google Search 等服务出现宕机，用户无法正常使用相关服务，此事已经得到......
### [小米手环 5 固件更新：支持 24 小时睡眠监测，新增 4 张交通卡](https://www.ithome.com/0/524/665.htm)
> 概要: IT之家 12 月 14 日消息 据小米手环官微消息，小米手环 5 固件版本更新至 1.0.2.46 版本，另外小米运动 APP 也更新至了 4.8.0 版本。此次更新使得小米手环 5 实现了 24 ......
### [World’s biggest iceberg heads for disaster](https://graphics.reuters.com/CLIMATE-CHANGE/ICEBERG/yzdvxjrbzvx/index.html)
> 概要: World’s biggest iceberg heads for disaster
### [Human Clock](https://humanclock.com/)
> 概要: Human Clock
### [学会与无解共存](https://www.huxiu.com/article/399705.html)
> 概要: 本文来自微信公众号：caoz的梦呓（ID：caozsay），作者：caozsay，题图来自：视觉中国我们读书的时候，大多数情况下，每道题都会有一个答案。到了社会上，突然发现，不但答案没有那么明确和标准......
### [互联网行业的历史性时刻：一个时代的结束](https://www.huxiu.com/article/399858.html)
> 概要: 本文来自微信公众号：互联网怪盗团（ID：TMTphantom），作者：裴培，题图来自视觉中国今天上午，国家市场公布了对三起“未依法申报违法实施经营者集中案”的行政处罚决定：阿里巴巴投资收购银泰商业股权......
### [互联网反垄断打响第一枪，阿里腾讯被“顶格处罚”](https://www.tuicool.com/articles/Q3eeuqY)
> 概要: 12月14日，国家市场监督管理总局依据《反垄断法》第48条、49条作出处罚决定，对阿里巴巴投资有限公司、腾讯控股企业阅文集团和深圳市丰巢网络技术有限公司分别处以50万元人民币罚款的行政处罚。阿里巴巴、......
### [大众需要点评模式，但恐怕不再需要“大众点评”了](https://www.tuicool.com/articles/YZbaiqr)
> 概要: 12月12日，主题为“真实记录·用爱分享”的第四届大众点评会员年终盛典召开。官方数据显示，2020年，有近三千万的“评价LV”和“笔记达人”活跃在大众点评上，贡献内容超过了一亿条。相信在许多人的印象中......
# 小说
### [暗失之门](http://book.zongheng.com/book/729800.html)
> 作者：秦与娄

> 标签：悬疑灵异

> 简介：范星是被很多人讨厌加憎恨之人。在一次聚会上，所有人都议论纷纷，然而就在那晚后，事情发生了？古巷发现一具男尸，警方接到报案后，赶往现场，负责此案的是被刚刚调过来不久任职的探长丁翰，案件非常的离奇，丁翰想借助高人之手，一起破获这起谋杀案。

> 章节末：第一百章    暗失之门（尾声）

> 状态：完本
# 游戏
### [《王者天下》动画第3季确定21年4月再开 新预告公开](https://www.3dmgame.com/news/202012/3804150.html)
> 概要: 今年4月官方表示受新冠病毒疫情影响《王者天下》TV动画第3季第五话开始延期，12月14日今天官方宣布了新的开播日期为2021年4月，同时特别预告以及艺图公开，敬请期待。《王者天下》，是日本漫画家原泰久......
### [剧场版《鬼灭》票房突破302亿 离日本影史第一已不远](https://www.3dmgame.com/news/202012/3804151.html)
> 概要: 今日（12月14日），剧场版《鬼灭之刃无限列车篇》官方宣布：该作票房已经突破302亿日元，累计到场人数也已经突破了2253万人，距离已经日本影史票房第一的《千与千寻》已经很近了。官推截图：官方表示：剧......
### [eBay黄牛党通过倒卖主机和显卡赚了4300万美元](https://www.3dmgame.com/news/202012/3804125.html)
> 概要: 今年上市的RTX30系显卡，XSX/S和PS5次世代主机都备受人们关注，但这些新品上市后很快就断货了。供货如此紧张，主要原因是受疫情影响而导致产能不足，还有一部分原因是被黄牛党抢购一空。国外一名数据工......
### [前《死亡空间》开发者：想打造一个最吓人的游戏](https://www.3dmgame.com/news/202012/3804169.html)
> 概要: 今年的TGA，其中一个最大的惊喜便是前《死亡空间》老兵组成的团队公布了一个恐怖新作《The Callisto Protocol》。该团队由《死亡空间》创始人Glen Schofield领导，看起来想要......
### [阿里阅文丰巢违反反垄断法受罚 虎牙斗鱼合并被调查](https://www.3dmgame.com/news/202012/3804148.html)
> 概要: 据财联社消息，因违反《反垄断法》，阿里巴巴投资有限公司、阅文集团和丰巢科技合计被罚150万。报道指出，根据《反垄断法》规定，市场监管总局对阿里巴巴投资有限公司收购银泰商业（集团）有限公司股权、阅文集团......
# 论文
### [Dynamic Programming Encoding for Subword Segmentation in Neural Machine Translation](https://paperswithcode.com/paper/dynamic-programming-encoding-for-subword)
> 日期：3 May 2020

> 标签：MACHINE TRANSLATION

> 代码：https://github.com/xlhex/dpe

> 描述：This paper introduces Dynamic Programming Encoding (DPE), a new segmentation algorithm for tokenizing sentences into subword units. We view the subword segmentation of output sentences as a latent variable that should be marginalized out for learning and inference.
### [Topological Insights in Sparse Neural Networks](https://paperswithcode.com/paper/topological-insights-in-sparse-neural)
> 日期：24 Jun 2020

> 标签：None

> 代码：https://github.com/Shiweiliuiiiiiii/Sparse_Topology_Distance

> 描述：Sparse neural networks are effective approaches to reduce the resource requirements for the deployment of deep neural networks. Recently, the concept of adaptive sparse connectivity, has emerged to allow training sparse neural networks from scratch by optimizing the sparse structure during training.
