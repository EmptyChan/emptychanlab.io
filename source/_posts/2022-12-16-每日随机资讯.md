---
title: 2022-12-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DudhsagarFallsGoa_ZH-CN0466471017_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-16 23:16:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DudhsagarFallsGoa_ZH-CN0466471017_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [Salesforce：2022 客户连接体验报告](https://www.woshipm.com/user-research/5707362.html)
> 概要: 本文是一篇客户体验报告，从数据分析的角度，分析了客户对品牌的信任建立，逐渐个性化的客户需求，以及现今逐渐走向数字优先的客户体验，充分展现了客户在选择品牌时所偏好的关注点，为品牌发展、数字化的进步提供了......
### [【万字长文】PRD面面观，手把手带你写出优秀的PRD](https://www.woshipm.com/zhichang/5264380.html)
> 概要: 作为一名产品经理，撰写PRD文档是基本功之一，但依旧有很多同学写不好高质量的PRD。那么该如何写出高质量的PRD，应对后续工作呢？作者结合自己的实战，与你分享了一些技巧，希望对你有所帮助。需求文档PR......
### [一个视频涨粉百万，柳夜熙们能成为元宇宙的“船票”吗？](https://www.woshipm.com/operate/5707396.html)
> 概要: 技术的发展，元宇宙的到来，数字人开始成为了“新”流量密码。自前段时间“柳夜熙”爆火之后，目前发展得怎么样呢？本文将以“柳夜熙”为例，阐析数字人在元宇宙中发展的蓝海以及数字人自身发展的缺点，一起来看看......
### [“最昂贵”世界杯之后，卡塔尔怎么“回本”？](https://finance.sina.com.cn/world/2022-12-16/doc-imxwvmtc6605268.shtml)
> 概要: 【环球时报驻埃及特派记者 黄培昭 环球时报报道 记者潘晓彤 辛斌】卡塔尔世界杯已经临近尾声，赛场之外，外界目光也将转而聚焦于赛后卡塔尔经济的发展上。除了精彩赛事，卡塔尔世界杯持续引人关注的，莫过于迄今......
### [腾讯追米哈游，一路追到新加坡？](https://www.huxiu.com/article/735468.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国“不可否认，Web3 对新加坡‘投资热’起到极大助推作用，但对游戏行业而言，新加坡确实是国内游戏厂商从亚太地区走向全球化的跳板。”一位游戏发行商向虎嗅表示......
### [微软总裁称收购动视暴雪很公平 因为索尼独占更多](https://www.3dmgame.com/news/202212/3858532.html)
> 概要: 近日微软总裁兼副董事长Brad Smith表示，Xbox收购动视暴雪是公平的，因为索尼PlayStation独占游戏明显更多。据彭博社报道，Brad Smith在本周股东大会上说，微软之所以要以687......
### [36氪首发｜ 「珂睿科技」完成数千万A1轮融资，深化高端液相色谱仪及零部件自研](https://36kr.com/p/2044938184707330)
> 概要: 36氪首发｜ 「珂睿科技」完成数千万A1轮融资，深化高端液相色谱仪及零部件自研-36氪
### [爱奇艺宣布会员调价，涉及黄金VIP和星钻VIP](https://finance.sina.com.cn/tech/2022-12-16/doc-imxwvmsx2250092.shtml)
> 概要: 新浪科技讯 12月16日早间消息，昨日，爱奇艺宣布于12月16日0:00起对爱奇艺黄金VIP会员及星钻VIP会员订阅价格进行更新，调整后黄金VIP会员连续包月、连续包季、连续包年分别为25元、68元、......
### [ChatGPT迅速走红 其开发者OpenAI预计2024年营收达到10亿美元](https://finance.sina.com.cn/tech/it/2022-12-16/doc-imxwvmsx2251628.shtml)
> 概要: 新浪科技讯 北京时间12月16日早间消息，据报道，三位知情者透露，OpenAI最近告诉投资者，预计明年营收将会达到2亿美元，2024年将达到10亿美元。一位知情者称，最近OpenAI曾二次发售股票，公......
### [OPPO将在未来三年给一加单独投入100亿资金](https://finance.sina.com.cn/tech/tele/2022-12-16/doc-imxwvmsx2254011.shtml)
> 概要: 记者 | 林腾......
### [36氪首发 ｜「流马锐驰」获近亿元A+轮融资，产品已完成数十万量产落地](https://36kr.com/p/2045055048223752)
> 概要: 36氪首发 ｜「流马锐驰」获近亿元A+轮融资，产品已完成数十万量产落地-36氪
### [动画电影《BLUE GIANT》公开预告片](https://news.dmzj.com/article/76554.html)
> 概要: 根据石塚真一原作制作的剧场版动画《BLUE GIANT》公开了预告片与宣传图。
### [《暗黑4》实体典藏版预购开启 暴雪警告：会迅速卖光](https://www.3dmgame.com/news/202212/3858539.html)
> 概要: 《暗黑破坏神4》实体典藏版已于12月15日开启预购，限量发行50000份。尽管其价格有点高，要卖96.66美元(约合人民币673元)，但暴雪仍相信实体典藏版会迅速卖光的。《暗黑破坏神4》实体典藏版包含......
### [《深渊国度》首曝 国人团队打造的黑暗奇幻ARPG](https://www.3dmgame.com/news/202212/3858554.html)
> 概要: 12月15日，IGN首曝了来自国内团队制作的国产动作游戏《深渊国度》(Abyss world)，这是一款第三人称ARPG游戏，具有强烈的黑暗幻想风格。游戏的故事发生在一个名为“诺迪尼亚”的神秘世界，在......
### [寿屋《拳皇98》SNK美少女麻宫雅典娜1/7比例手办](https://news.dmzj.com/article/76557.html)
> 概要: 寿屋根据《拳皇98》中的女高中生麻宫雅典娜制作的1/7比例手办正在预订中。
### [情暖疫线，同心·共铸中国心价值7000万元物资助力抗疫](http://www.investorscn.com/2022/12/16/104861/)
> 概要: “疫情来得非常迅猛，医院两周前便收治了阳性患者，很多医护人员在被感染后，又会有新一轮补给人员替补上一线，可以用‘前仆后继’来形容，呼吸科被感染医务人员更是轻伤不下火线，带病坚持工作，真的非常不易。”......
### [P站美图推荐——楼梯特辑](https://news.dmzj.com/article/76558.html)
> 概要: 不知道为什么，总觉得楼梯是个自带故事性的场景呢……
### [探索科技与公益融合共生，TCL魏雪荣获“年度十大公益人物”](http://www.investorscn.com/2022/12/16/104863/)
> 概要: 12月15日,凤凰网行动者联盟2022公益盛典举办。TCL科技集团副总裁、TCL公益基金会理事长、华萌慈善基金会创始人魏雪荣获“年度十大公益人物”......
### [数字信息管理SaaS软件FlowUs息流：上线1年融资3轮,吸引60万用户](http://www.investorscn.com/2022/12/16/104864/)
> 概要: 近年来，SaaS领域仍被证明有巨大的想象空间和商业价值，数据显示今年SaaS市场规模将达到991亿元，未来三年市场将维持34%的复合增长率持续扩张。而SaaS领域中细分赛道也成为了抢滩点，如一站式生产......
### [「C位观察」AR专题系列(3)：见微知著，气象万千——微显示的路线探索——C位](https://36kr.com/p/1956922245317505)
> 概要: 「C位观察」AR专题系列(3)：见微知著，气象万千——微显示的路线探索——C位-36氪
### [BBA的禁脔，垂涎者不止蔚来](https://www.huxiu.com/article/743269.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 岚图汽车中大型轿车，是BBA的腹地。在很长一段时间里，当中国消费者需要购买一辆豪华品牌的中大型轿车时，他们的清单里必定会先出现这三款车：宝马......
### [我所经历的“踏空”式恢复](https://www.huxiu.com/article/743816.html)
> 概要: 本文来自微信公众号：首席人物观 （ID：sxrenwuguan），作者：未未，编辑：江岳，原文标题：《我在北京，经历“踏空”式恢复》，题图来自：视觉中国2022年12月14日，国家邮政局发布通告，督促......
### [李昇基称将音源结算金全部捐出 继续起诉Hook娱乐](https://ent.sina.com.cn/s/j/2022-12-16/doc-imxwwaqt9901460.shtml)
> 概要: 新浪娱乐讯 16日，Hook娱乐公司发表官方立场：“向因为这次事件而度过最艰难时期的李昇基致以真心的歉意。”随后表示，因为李昇基要求的数字和需要给他结算的金额差异太大，所以协商不成功，但己方不愿与李昇......
### [实在智能亮相首届全球数字贸易博览会，RPA数字员工勇夺先锋奖](http://www.investorscn.com/2022/12/16/104868/)
> 概要: 12月11日至14日，全球首届数字贸易博览会（以下简称“数贸会”）在浙江杭州举行。实在智能携实在RPA数字员工惊艳亮相展会，斩获首届全球数字贸易博览会先锋奖（DT奖），此外，实在RPA数字员工信创版及......
### [日本漫画家圣悠纪因病突然去世 享年72岁](https://acg.gamersky.com/news/202212/1548083.shtml)
> 概要: 日本漫画家圣悠纪因病于2022年10月30日突然去世，享年72岁。圣悠纪的代表作是《超人洛克》。
### [《三体》动画史强角色介绍 明天中午更新第三集](https://acg.gamersky.com/news/202212/1548099.shtml)
> 概要: 《三体》动画官方今日发布角色档案——史强。动画第三集将于明日11：00更新，敬请追番！
### [海外new things | 悉尼碳排放管理平台「Pathzero」A+轮融资860万澳元，帮助投资者跟踪投资组合的碳排放情况](https://36kr.com/p/2043972179823618)
> 概要: 海外new things | 悉尼碳排放管理平台「Pathzero」A+轮融资860万澳元，帮助投资者跟踪投资组合的碳排放情况-36氪
### [王鹤棣造型师辟谣 称被拍到的女工作人员是自己](https://ent.sina.com.cn/s/m/2022-12-16/doc-imxwwhwt6702000.shtml)
> 概要: 新浪娱乐讯 16日，有八卦媒体晒出王鹤棣和一名女生的合影，称昨晚GQ盛典，女友陪伴在王鹤棣旁边。随后，王鹤棣造型师发文辟谣，称该名女生是自己......
### [阔诺新连载哒！12月新连载漫画不完全指北第二期](https://news.dmzj.com/article/76562.html)
> 概要: 重生七次的侯爵长子决定向家族复仇、并把自己卖给了大公家的怪物公主！见习修女受委托与猜疑心极重的国王结婚，又将是什么样的爱情故事？请看本周指北！
### [经典游戏中看似毫无作用的跟班，培养出来后主角反倒成为累赘](https://www.ithome.com/0/661/684.htm)
> 概要: 《轩辕剑》系列中的机关术一直为玩家们所称道，将高科技通过这么一种形式进行呈现出来，并融入到华夏文明之中。估计也就只有《轩辕剑》系列做得到吧！当年第一次接触的版本就是《枫之舞》，因此对其中的机关人非常熟......
### [没完没了！爱奇艺VIP会员今起涨价：连续包月25元](https://www.3dmgame.com/news/202212/3858595.html)
> 概要: 如今，视频网站会员涨价似乎已经成了家常便饭，每隔一段时间就要涨一次。日前，爱奇艺发布爱奇艺VIP会员调价通知，称将于12月16日(今日)起，对爱奇艺VIP会员订阅价格进行更新。涨价后黄金VIP连续包月......
### [弥蒙高铁开通运营：设计时速 250 公里](https://www.ithome.com/0/661/695.htm)
> 概要: IT之家12 月 16 日消息，据中国铁路发布，在 12 月 16 日 9 时 40 分，D9856 次列车从红河站缓缓驶出，开往昆明站，弥勒至蒙自高速铁路（简称“弥蒙高铁”）正式开通运营。弥蒙高铁全......
### [陈慧琳与儿子出门发生交通意外 面部少许淤伤](https://ent.sina.com.cn/y/ygangtai/2022-12-16/doc-imxwwper6700533.shtml)
> 概要: 新浪娱乐讯 16日，据报道，陈慧琳与儿子今早由司机接送出门，座驾在嘉道理道与对面一辆私家车迎头相撞，两车车头损毁，司机及乘客均送院检查，据报陈慧琳司机撞到胸口，和大仔亦面部受伤。由于现场路面狭窄，意外......
### [苹果妥协！将允许用户从第三方商店下载软件](https://www.yicai.com/video/101626330.html)
> 概要: 当地时间12月14日，据媒体报道，为满足欧盟的严格新规，苹果计划于2024年允许其iPhone和iPad用户使用第三方应用商店替代App Store。
### [指数大小分化 后市市场风格如何演绎？](https://www.yicai.com/video/101626337.html)
> 概要: 指数大小分化 后市市场风格如何演绎？
### [盛新锂能申请在瑞交所上市丨汽车日评](https://www.yicai.com/news/101626348.html)
> 概要: 法拉第未来正在与投资者讨论筹集高达1.7亿美元的额外资本，目标是在2023年4月向客户交付FF 91 Futurist汽车。
### [中央经济工作会议在北京举行 习近平作重要讲话](https://www.yicai.com/news/101626391.html)
> 概要: 中央经济工作会议12月15日至16日在北京举行。
### [百度 Apollo 三域融通高阶智驾 ANP3.0 开启多城市泛化测试，2023 年中量产](https://www.ithome.com/0/661/710.htm)
> 概要: 感谢IT之家网友乌蝇哥的左手的线索投递！IT之家12 月 16 日消息，在全球智能汽车产业峰会（GIV2022）上，百度 Apollo 展示了一段三域融通高阶智驾产品 ANP3.0 的多城市泛化路测视......
### [真维斯官方清仓：加绒运动裤 / 卫衣 59.9 元、夹克 99.9 元](https://lapin.ithome.com/html/digi/661713.htm)
> 概要: 【真维斯官方 outlets 店】真维斯男士加绒加厚运动裤报价 109.9 元，限时限量 50 元券，实付 59.9 元包邮，领券并购买。使用最会买 App下单，预计还能再返 6.58 元，返后 53......
### [财政赤字应该由什么决定？](https://www.huxiu.com/article/744443.html)
> 概要: 本文来自微信公众号：每经头条 （ID：nbdtoutiao），嘉宾：林双林（北京大学中国公共财政研究中心名誉主任，曾任国务院医改专家咨询委员会委员），作者：胥帅，编辑：刘林鹏 宋红 董兴生，原文标题：......
### [A股上演“囤药”行情，“洗鼻第一股”爱朋医疗连续大涨或迎爆发](http://www.investorscn.com/2022/12/16/104879/)
> 概要: 随着全国各地新冠疫情管控政策逐步恢复正常，叠加目前已经进入流感等呼吸道疾病高发的季节，引发国内出现一股“囤药热”，各类流感药、退烧药、消炎药，甚至是抗原产品近期需求大幅增长......
### [郁亮在股东会再发声：政策的力度和广度都超出了预期](https://www.yicai.com/news/101626435.html)
> 概要: 万科认为经济活动回暖亦会让房地产行业受益。
### [流言板米哈家人发声：他曾勇敢地与病魔搏斗，会永远活在我们心中](https://bbs.hupu.com/56986489.html)
> 概要: 虎扑12月16日讯 今天多家意大利媒体报道了前博洛尼亚主帅、意甲足坛名宿米哈伊洛维奇因病去世，享年53岁，刚刚米哈的家人也通过安莎通讯社发布了声明。米哈的家人通过安莎社发布的声明中写道：“米哈伊洛维奇
### [GOG喜加一：《海洋之王》免费领取](https://www.3dmgame.com/news/202212/3858603.html)
> 概要: 动作角色扮演游戏《海洋之王》在GOG平台上开启了免费领取活动，时间截止至12月19日22点，感兴趣的玩家不要错过了。领取地址：点我进入《海洋之王》介绍：《海洋之王》是一款动作角色扮演游戏，场景设定在一......
### [中央经济工作会议：适时实施渐进式延迟法定退休年龄政策](https://finance.sina.com.cn/jjxw/2022-12-16/doc-imxwwxum6717084.shtml)
> 概要: 中央经济工作会议12月15日至16日在北京举行。 会议指出，落实落细就业优先政策，把促进青年特别是高校毕业生就业工作摆在更加突出的位置。
### [截图预测 佛山DRG.GK vs XYG，截图预测比分吧~](https://bbs.hupu.com/56986520.html)
> 概要: 截图预测，你心目中的比分吧！！！2022年12月17日，周六18:00 王者荣耀世界冠军杯1/4决赛（BO7全局BP）：佛山DRG.GK vs XYG世冠状态上升，DRG.GK能否续写去年世冠战绩？强
### [中央经济工作会议：要从制度和法律上把对国企民企平等对待的要求落下来](https://finance.sina.com.cn/jjxw/2022-12-16/doc-imxwwxui9946459.shtml)
> 概要: 中央经济工作会议12月15日至16日在北京举行。 会议指出，切实落实“两个毫不动摇”。针对社会上对我们是否坚持“两个毫不动摇”的不正确议论，必须亮明态度，毫不含糊。
### [林书豪晒个人接受采访视频：机会真的只会留给有准备的人](https://bbs.hupu.com/56986594.html)
> 概要: 广州队外援林书豪更新抖音，晒个人接受采访视频。配文：“机会真的只会留给有准备的人！”今日对阵宁波队是林书豪第二阶段首次出场，他在20分钟内12投7中，其中三分球3投1中，得到18分7篮板7助攻1抢断。
### [宁波队晒对阵广州集锦：是的！我们还需要狠狠地加油！](https://bbs.hupu.com/56986632.html)
> 概要: 2022-23赛季CBA常规赛第14轮，广州队104-68战胜宁波队。赛后宁波队晒本场球队集锦。配文：“是的！我们还需要狠狠地加油！”下一轮宁波队将对阵新疆队。 来源：  新浪微博
### [中央经济工作会议：多渠道增加城乡居民收入](https://finance.sina.com.cn/china/2022-12-16/doc-imxwwxum6725491.shtml)
> 概要: 中央经济工作会议12月15日至16日在北京举行。会议指出，着力扩大国内需求。要把恢复和扩大消费摆在优先位置。增强消费能力，改善消费条件，创新消费场景。
### [中央经济工作会议：做好岁末年初各项工作，强化市场保供稳价，加强煤电油气运调节](https://finance.sina.com.cn/china/2022-12-16/doc-imxwwxum6725510.shtml)
> 概要: 中央经济工作会议12月15日至16日在北京举行。 会议要求，要准确把握明年经济工作部署要求，敢担当，善作为，察实情，创造性抓好贯彻落实...
### [中央经济工作会议：明年要更好统筹疫情防控和经济社会发展，因时因势优化疫情防控措施](https://finance.sina.com.cn/china/2022-12-16/doc-imxwwxui9948316.shtml)
> 概要: 中央经济工作会议12月15日至16日在北京举行。会议强调，明年经济发展面临的困难挑战很多，要坚持系统观念、守正创新。要更好统筹疫情防控和经济社会发展...
# 小说
### [多情小僧](http://book.zongheng.com/book/1073599.html)
> 作者：自然随心

> 标签：武侠仙侠

> 简介：江湖情仇，名利风云！我爱我姐如何？我爱我妹如何？我爱我娘亲又如何？多情的小僧可有错？

> 章节末：大结局

> 状态：完本
# 论文
### [Gaussian Process regression over discrete probability measures: on the non-stationarity relation between Euclidean and Wasserstein Squared Exponential Kernels | Papers With Code](https://paperswithcode.com/paper/gaussian-process-regression-over-discrete)
> 日期：2 Dec 2022

> 标签：None

> 代码：https://github.com/acandelieri/waker

> 描述：Gaussian Process regression is a kernel method successfully adopted in many real-life applications. Recently, there is a growing interest on extending this method to non-Euclidean input spaces, like the one considered in this paper, consisting of probability measures. Although a Positive Definite kernel can be defined by using a suitable distance -- the Wasserstein distance -- the common procedure for learning the Gaussian Process model can fail due to numerical issues, arising earlier and more frequently than in the case of an Euclidean input space and, as demonstrated in this paper, that cannot be avoided by adding artificial noise (nugget effect) as usually done. This paper uncovers the main reason of these issues, that is a non-stationarity relationship between the Wasserstein-based squared exponential kernel and its Euclidean-based counterpart. As a relevant result, the Gaussian Process model is learned by assuming the input space as Euclidean and then an algebraic transformation, based on the uncovered relation, is used to transform it into a non-stationary and Wasserstein-based Gaussian Process model over probability measures. This algebraic transformation is simpler than log-exp maps used in the case of data belonging to Riemannian manifolds and recently extended to consider the pseudo-Riemannian structure of an input space equipped with the Wasserstein distance.
### [FETILDA: An Effective Framework For Fin-tuned Embeddings For Long Financial Text Documents | Papers With Code](https://paperswithcode.com/paper/fetilda-an-effective-framework-for-fin-tuned)
> 概要: Unstructured data, especially text, continues to grow rapidly in various domains. In particular, in the financial sphere, there is a wealth of accumulated unstructured financial data, such as the textual disclosure documents that companies submit on a regular basis to regulatory agencies, such as the Securities and Exchange Commission (SEC). These documents are typically very long and tend to contain valuable soft information about a company's performance. It is therefore of great interest to learn predictive models from these long textual documents, especially for forecasting numerical key performance indicators (KPIs). Whereas there has been a great progress in pre-trained language models (LMs) that learn from tremendously large corpora of textual data, they still struggle in terms of effective representations for long documents. Our work fills this critical need, namely how to develop better models to extract useful information from long textual documents and learn effective features that can leverage the soft financial and risk information for text regression (prediction) tasks. In this paper, we propose and implement a deep learning framework that splits long documents into chunks and utilizes pre-trained LMs to process and aggregate the chunks into vector representations, followed by self-attention to extract valuable document-level features. We evaluate our model on a collection of 10-K public disclosure reports from US banks, and another dataset of reports submitted by US companies. Overall, our framework outperforms strong baseline methods for textual modeling as well as a baseline regression model using only numerical data. Our work provides better insights into how utilizing pre-trained domain-specific and fine-tuned long-input LMs in representing long documents can improve the quality of representation of textual data, and therefore, help in improving predictive analyses.
