---
title: 2020-05-21-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.JeffHanson_EN-CN5617220543_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-21 22:02:13
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.JeffHanson_EN-CN5617220543_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：魏晨新婚校服写真曝光 与妻子甜笑摆pose尽显青春活力](http://slide.ent.sina.com.cn/y/slide_4_704_339084.html)
> 概要: 组图：魏晨新婚校服写真曝光 与妻子甜笑摆pose尽显青春活力
### [组图：朱亚文暴雨天穿着依旧清凉 戴黑框眼镜斯文雅痞](http://slide.ent.sina.com.cn/star/slide_4_704_339103.html)
> 概要: 组图：朱亚文暴雨天穿着依旧清凉 戴黑框眼镜斯文雅痞
### [视频：醒悟？阿娇离婚风波后露面 告诫大家不要迷失自我](https://video.sina.com.cn/p/ent/2020-05-21/detail-iircuyvi4221869.d.html)
> 概要: 视频：醒悟？阿娇离婚风波后露面 告诫大家不要迷失自我
### [47岁裘德洛6度当爸 与小15岁嫩妻“造人”成功](https://ent.sina.com.cn/s/u/2020-05-21/doc-iirczymk2760938.shtml)
> 概要: 新浪娱乐讯 据台媒，47岁的英国著名男星裘德洛（Jude Law）又要当爸了！英国《每日邮报》报导，裘德洛17日和小他15岁的嫩妻菲丽帕柯恩（Phillipa Coan）逛街，被拍到女方小腹明显隆起的......
### [网友重提阿娇不雅照 赖弘国霸气回怼护前妻](https://ent.sina.com.cn/s/h/2020-05-21/doc-iircuyvi4222891.shtml)
> 概要: 新浪娱乐讯 5月21日，据台媒，钟欣潼（阿娇）与“医界王阳明”赖弘国日前被曝离婚，两人目前进入分居、准备进入离婚阶段，14个月婚姻画下句点，赖弘国虽然积极展开新生活，但他一举一动仍备受瞩目，就有网友再......
### [春妮空降小沈龙直播间，女神依旧气质不俗，42岁被老公宠成公主](https://new.qq.com/omn/20200521/20200521A06MVL00.html)
> 概要: 春妮空降小沈龙直播间，女神依旧气质不俗，42岁被老公宠成公主
### [《清平乐》评分高走低开，网友为何失落了](https://new.qq.com/omn/20200521/20200521A031QE00.html)
> 概要: （文中资料图均为《清平乐》剧照或海报）精致而又漫长的电视剧《清平乐》终于在5月18日迎来大结局。一个半月来，《清平乐》常在微博热搜上看到，还原北宋历史风貌、背诵默写天团，主人公演技和后宫争斗、恋情，一......
### [疫期复课好紧张！张柏芝亲自接送儿子，准备防疫用品保驾护航](https://new.qq.com/omn/20200521/20200521A03NYL00.html)
> 概要: 疫期复课好紧张！张柏芝亲自接送儿子，准备防疫用品保驾护航
### [41岁罗志祥自称男孩，郭富城50岁觉得没长大，他们都只感动了自己](https://new.qq.com/omn/20200521/20200521A07CB800.html)
> 概要: 41岁罗志祥自称男孩，郭富城50岁觉得没长大，他们都只感动了自己
### [王一博的520提前剧透，对比前两年，差距太明显](https://new.qq.com/omn/20200521/20200521A075WJ00.html)
> 概要: 王一博的520提前剧透，对比前两年，差距太明显
# 动漫
### [P站美图推荐——太鼓桥特辑（二）](https://news.dmzj.com/article/67446.html)
> 概要: 在动森里面成为和风桥，就是红色的祭典一样的桥~
### [游戏王历史：从零开始的游戏王环境之旅第四期17](https://news.dmzj.com/article/67450.html)
> 概要: 距离禁卡制度导入的导入已经过了1年，终于将大部分禁止级的卡牌都进行了对应，环境迎来了非常安定的时期。【魔导弹射龟】等先攻1回杀牌组完全消失，以【No Chaos】为首的各种【Good Stuff】系牌组也缓慢的走向灭亡。
### [宠物情人之猫妖传，治愈系工业糖精](https://news.dmzj.com/article/67439.html)
> 概要: 这期就给大家推荐一部宠物变人的小言电视剧《你回来了》，由 金明洙/辛睿恩/徐志焄/姜勋/尹芮珠等主演，改编自韩国网络少女漫画《快过来》，漫画一度风靡互联网得到许多养宠物的读者喜爱，毕竟同为铲屎官实在是太有共鸣感啦！该剧与漫画一样讲述变身成人类的猫...
### [GSC《刀剑神域》亚丝娜创世神史提西亚粘土人开订](https://news.dmzj.com/article/67447.html)
> 概要: GSC根据TV动画《刀剑神域 Alicization War of Underworld》中的以在Underworld中的创世神史提西亚登场的亚丝娜制作的粘土人目前已经开订了。本作包括“普通颜”“战斗颜”“笑颜”三种表情，配有武器Radiant...
### [精灵宝可梦：小智向瑟蕾娜表白，约定夺冠就结婚，我的青春回来了](https://new.qq.com/omn/20200521/20200521A06UGB00.html)
> 概要: 精灵宝可梦又叫神奇宝贝或者宠物小精灵，是一部非常优秀的动漫，讲述的是主角小智一直在世界冒险，并且不断遇到新的小精灵的故事。但是天下没有不散的宴席，小智与女主角瑟雷娜经过了漫长的旅行之后，终于也到了分……
### [斗罗105集：武魂殿围剿“兔子精”，6个魂斗罗登场，母龙竟被吓懵](https://new.qq.com/omn/20200521/20200521A07H6E00.html)
> 概要: 全大陆“青年魂师大赛”终于结束了，史莱克学院打赢武魂殿学院之后，这一届比赛就彻底宣布完结。不过，比赛的结束不是终点，可以说它只是一个开始。因为比赛结束之后，小舞10万年魂兽的身份终于曝光。这意味着小……
### [泰迦奥特曼剧场版剧照流出：令迦单手挡必杀，动起来之后真香！](https://new.qq.com/omn/20200521/20200521A091N500.html)
> 概要: 由于疫情的缘故，泰迦奥特曼的剧场版迟迟没有上映。虽说如此，但也在海外流出了关于令迦奥特曼的部分剧照，起初的时候看到令迦的形象感觉一般，但一旦战斗起来之后给人的感觉还是相当帅气的！在这次的剧场版我们会……
### [小花仙：最帅的五位花仙精灵王，曼达第三，这两位都比曼达帅](https://new.qq.com/omn/20200521/20200521A04CRV00.html)
> 概要: 《小花仙》是一部高人气的国产动漫，经过数年的连载，如今第五季已经连载完。《小花仙》的前四季中，女主角都是夏安安，因此在很多人的心目中，夏安安就是小花仙的灵魂人物，所以当第五季主角换成雪城爱后，很多粉……
### [海贼王：好莱坞真人版海贼王，6月选角9月开拍！你怎么看？](https://new.qq.com/omn/20200521/20200521A075UO00.html)
> 概要: Netflix将推出《海贼王》的真人版剧集。制作人Marty Adelstein近日透露：该剧有希望在今年年内开拍，计划是在6月初开始人物选角，9月份开始制作。情报图片Adelstein声称：海贼王……
### [神奇宝贝：神秘消失的GS球，不仅坚不可摧，还与这三只神兽密切相关](https://new.qq.com/omn/20200521/20200521A08YR900.html)
> 概要: 在神奇宝贝世界中的谜题数不胜数，就好像现实世界的各种科学难题，无论人类如何探索，都无法穷尽。神奇宝贝的世界里，不仅仅存在未知的神奇宝贝们，还有各种未知的现象，例如在无印篇中神秘的海边少女，还有那只一……
### [动画《刃牙》大擂台赛全新中文预告 6月4日开播](https://acg.gamersky.com/news/202005/1290099.shtml)
> 概要: 网飞动画《刃牙》大擂台赛篇在今天（5月21日），公开了本作的正式中文预告，各路豪强悉数登场，纷纷展示强劲的肌肉和功夫
# 财经
### [贾樟柯：数字化生活 不能忽视老年人的需求](https://finance.sina.com.cn/wm/2020-05-21/doc-iirczymk2863562.shtml)
> 概要: 今年两会，全国人大代表、电影导演贾樟柯针对二三线城市和乡村普遍存在的老年人无法自如使用智能手机网络购物、线上缴费，以及开展网络社交活动等现实困难...
### [公积金改革之辩：存与废，降与不降，怎么降？](https://finance.sina.com.cn/china/gncj/2020-05-21/doc-iirczymk2869969.shtml)
> 概要: 来源：北京商报 住房公积金要不要取消？改革又将走向哪里……从激活房产市场到满足购房需求，历史的车轮滚滚推进，公积金的角色随之转换。
### [张连起：建议取消以增值税为计税依据的城建税与教育费附加](https://finance.sina.com.cn/china/gncj/2020-05-21/doc-iirczymk2868445.shtml)
> 概要: 原标题：每经专访全国政协常委张连起：建议取消以增值税为计税依据的城建税与教育费附加 每经记者 张钟尹  全国两会大幕开启。今年是决胜全面建成小康社会...
### [十三届全国人大三次会议议程](https://finance.sina.com.cn/china/gncj/2020-05-21/doc-iircuyvi4339904.shtml)
> 概要: 责任编辑：刘万里 SF014......
### [罗建红委员：应改变医生晋升“唯论文”倾向 改进评审制度](https://finance.sina.com.cn/china/gncj/2020-05-21/doc-iircuyvi4332979.shtml)
> 概要: 原标题：罗建红委员：应改变医生晋升“唯论文”倾向，改进评审制度 今年的全国两会，全国政协委员、农工浙江省委会主委、原浙江大学副校长罗建红带来一份《关于改进临床医师...
### [“双疫情”下如何重振猪产业？全国政协委员刘永好建言献策](https://finance.sina.com.cn/roll/2020-05-21/doc-iirczymk2860336.shtml)
> 概要: 原标题：“双疫情”下如何重振猪产业？全国政协委员刘永好建言献策（全国政协委员、新希望集团董事长刘永好） 华夏时报（www.chinatimes.net...
# 科技
### [Windows软件包管理器CLI（又名winget）](https://www.ctolib.com/microsoft-winget-cli.html)
> 概要: Windows软件包管理器CLI（又名winget）
### [采用Python实现的手动USB和BLE键盘](https://www.ctolib.com/makerdiary-python-keyboard.html)
> 概要: 采用Python实现的手动USB和BLE键盘
### [surfboard 一个用于现代音频特征提取的Python包](https://www.ctolib.com/novoic-surfboard.html)
> 概要: surfboard 一个用于现代音频特征提取的Python包
### [🤖💬实现了基于Transformer的文本转语音神经网络（TensorFlow 2）](https://www.ctolib.com/as-ideas-TransformerTTS.html)
> 概要: 🤖💬实现了基于Transformer的文本转语音神经网络（TensorFlow 2）
### [“不裁员”才是公司度过艰难时期的聪明选择](https://www.tuicool.com/articles/Zj2IRr2)
> 概要: 神译局是36氪旗下编译团队，关注科技、商业、职场、生活等领域，重点介绍国外的新技术、新观点、新风向。编者按：全球疫情发展带来不可避免的经济衰退，很多企业的收入锐减，不得不考虑裁员来度过危机。鲍勃•查普......
### [当 GoLand 遇到 Kubernetes：如何调试和运行 Go 程序](https://www.tuicool.com/articles/NruENvV)
> 概要: 这是我们使用 Docker，Docker Compose 或 Kubernetes 运行 Go 服务的系列文章的最后一部分。在这一部分中，我们将专注于使用 Kubernetes 集群时的运行和调试。虽......
### [精英直播来势汹汹，带货模式进阶换血](https://www.tuicool.com/articles/RRFVzur)
> 概要: 欢迎关注“创事记”的微信订阅号：sinachuangshiji文/李季 李觐麟来源：锌刻度（ID：znkedu）罗永浩又一次“翻车”了。通过他在直播间的推荐，不少粉丝在“花点时间”下单了520当天鲜花......
### [销售纯天然的茶叶，印度高端茶品牌「Teamonk」完成新一轮融资](https://www.tuicool.com/articles/M326zyz)
> 概要: Teamonk 的高端茶叶产品据外媒VCCircle报道，印度Teamonk完成新一轮融资，由 Lead Angels Network 投资，具体金额尚未披露。在此之前，Teamonk 曾获得100万......
### [网易游戏旗下全部80余款在线运营网游升级防沉迷系统：未成年用户每日限玩3小时](https://www.ithome.com/0/488/471.htm)
> 概要: 5月21日下午消息，网易游戏今日对外公布了关于未成年保护工作的最新进展。网易游戏表示，截止5月20日，已完成旗下所有在线运营的网络游戏防沉迷系统升级，共计80余款。此外，单机游戏和弱联网游戏也在加急调......
### [Xbox和Windows NT 3.5源代码被泄漏到网上](https://www.ithome.com/0/488/553.htm)
> 概要: IT之家5月21日消息 微软初代Xbox游戏机的源代码已经在网上泄露，同时泄露的还有Windows NT 3.5的代码。Xbox的源代码包括了该游戏机上的操作系统内核，是Windows 2000的定制......
### [京东零售CEO徐雷“直播首秀”将带货卖房，并揭晓“自营房产”业务](https://www.ithome.com/0/488/547.htm)
> 概要: 近段时间，直播卖房火爆，吸引很多网友的关注。有消息称，随着618的到来，5月22日上午11点，京东零售集团CEO徐雷将现身直播间。在直播中，京东不仅将带来1000套北京房源，揭晓京东全新自营新业务“自......
### [RedmiBook新品外观公布：16.1英寸屏幕放入15英寸机身](https://www.ithome.com/0/488/511.htm)
> 概要: IT之家5月21日消息 今天小米笔记本电脑放出了RedmiBook新品的预热海报，官方称要将16.1英寸屏幕放入15英寸机身中。小米笔记本官方微博表示，RedmiBook新品将于5月26日的下午两点的......
# 小说
### [末日天堂](http://book.zongheng.com/book/55920.html)
> 作者：燃烧的小卷

> 标签：科幻游戏

> 简介：病毒，丧尸横行，十大凶兽，令人类的生存希望变得更加渺茫！  新书《都市通灵王》发布，大家转道收藏一下吧，直通车挂着呢！

> 章节末：新书 破玄 发布

> 状态：完本
### [剑神进化](http://book.zongheng.com/book/669927.html)
> 作者：咫尺寸进

> 标签：奇幻玄幻

> 简介：这是一个破碎的世界，神主为救天地，以神躯缝补天地，终至神力枯竭而死，人间纷乱，诸神在浩劫中自身难保，纷纷陨落。神主神躯虽死，神灵却并未泯灭，为救苍生，融天地意志为一‘系统’，铺就一条快速成神的进化之路。苍天虽死，剑神不灭。虽一介蝼蚁浮游，却势要撼天动地。虽资质低位，却阻挡不住我进化成神。

> 章节末：第六十八章 真情 （大结局）

> 状态：完本
# 游戏
### [恐怖新作《夜、灯明》新情报公开：温柔少女登场](https://www.3dmgame.com/news/202005/3789056.html)
> 概要: 今日（5月21日），日本一官方公开了恐怖新游《夜、灯明》的一些新情报和一些新截图。该作将于2020年7月30日在任天堂Switch和PS4平台发售，以下为官方此次公开的新细节。新角色介绍：小夜子（CV......
### [电竞俱乐部G2推出专属口罩 定价117元收益全部捐出](https://ol.3dmgame.com/esports/12758.html)
> 概要: 电竞俱乐部G2推出专属口罩 定价117元收益全部捐出
### [除了《赛博朋克2077》 其他同风格游戏有何新意？](https://www.3dmgame.com/news/202005/3789083.html)
> 概要: 大雨滂沱的都市再加上醒目的霓虹灯，赛博朋克风格下的世界总是能为玩家带来一场视觉上的“盛宴”，《赛博朋克2077》自然也不会例外。CDPR公司对于《赛博朋克2077》这款游戏抱有“雄心”和“野望”，而该......
### [坂口博信新游《Fantasian》新开发图 城市大厅亮相](https://www.3dmgame.com/news/202005/3789051.html)
> 概要: 作为独立传奇级别游戏制作人，坂口博信低调的多，5月21日今天发推晒出了了旗下正在开发的新游戏《Fantasian》的最新开发图，游戏中的场景城市大厅亮相，一起来先睹为快。·目前关于这款新游戏《Fant......
### [运气也是实力！网友总结《鬼灭之刃》诡异爆火原因](https://www.3dmgame.com/news/202005/3789037.html)
> 概要: 5月18日《鬼灭之刃》漫画正式完结引发日本全网热搜，粉丝普遍感觉《鬼灭之刃》就像是昨天突然爆火起来，还没反应过来作者已经偃旗息鼓了，于是有网友总结《鬼灭之刃》诡异爆火的原因，一起来看看究竟怎么回事。·......
# 论文
### [Learning Constrained Dynamics with Gauss Principle adhering Gaussian Processes](https://paperswithcode.com/paper/learning-constrained-dynamics-with-gauss)
> 日期：23 Apr 2020

> 标签：GAUSSIAN PROCESSES

> 代码：https://github.com/AndReGeist/gp_squared

> 描述：The identification of the constrained dynamics of mechanical systems is often challenging. Learning methods promise to ease an analytical analysis, but require considerable amounts of data for training.
### [Motion-supervised Co-Part Segmentation](https://paperswithcode.com/paper/motion-supervised-co-part-segmentation)
> 日期：7 Apr 2020

> 标签：None

> 代码：https://github.com/AliaksandrSiarohin/motion-cosegmentation

> 描述：Recent co-part segmentation methods mostly operate in a supervised learning setting, which requires a large amount of annotated data for training. To overcome this limitation, we propose a self-supervised deep learning method for co-part segmentation.
### [Hierarchical Adaptive Contextual Bandits for Resource Constraint based Recommendation](https://paperswithcode.com/paper/hierarchical-adaptive-contextual-bandits-for)
> 日期：2 Apr 2020

> 标签：MULTI-ARMED BANDITS

> 代码：https://github.com/ymy4323460/HATCH

> 描述：Contextual multi-armed bandit (MAB) achieves cutting-edge performance on a variety of problems. When it comes to real-world scenarios such as recommendation system and online advertising, however, it is essential to consider the resource consumption of exploration.
