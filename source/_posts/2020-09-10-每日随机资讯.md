---
title: 2020-09-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ThailandWaterfall_FF_1920x1080_G_679699496_HD_EN-CN576321563.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-09-10 21:23:16
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ThailandWaterfall_FF_1920x1080_G_679699496_HD_EN-CN576321563.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [安室奈美惠引退2年再传复出 将英文名注册商标](https://ent.sina.com.cn/y/yrihan/2020-09-10/doc-iivhvpwy5865030.shtml)
> 概要: 新浪娱乐讯 据香港媒体报道，42岁日本跳舞天后安室奈美惠引退近两年，但粉丝依然对她不离不弃，本月16日安室引退纪念日，举行烟花秀并在网上转播。　　此外，安室奈美惠又被曝近日透过个人事务所，将英文名字“......
### [组图:马思纯坐观众席看综艺被指发福 回应"要知道被cue先瘦十斤"](http://slide.ent.sina.com.cn/z/v/w/slide_4_704_344943.html)
> 概要: 组图:马思纯坐观众席看综艺被指发福 回应"要知道被cue先瘦十斤"
### [组图：王思聪携新女友深夜撸串被偶遇 账单消费317块接地气](http://slide.ent.sina.com.cn/star/hr/slide_4_704_344941.html)
> 概要: 组图：王思聪携新女友深夜撸串被偶遇 账单消费317块接地气
### [视频：王菲马云隔空合唱《如果云知道》 歌词改编惹爆笑](https://video.sina.com.cn/p/ent/2020-09-10/detail-iivhuipp3502568.d.html)
> 概要: 视频：王菲马云隔空合唱《如果云知道》 歌词改编惹爆笑
### [视频：昆凌穿闪钻纱裙满满公主范 表示自己是送给周董最珍惜的礼物](https://video.sina.com.cn/p/ent/2020-09-10/detail-iivhvpwy5878091.d.html)
> 概要: 视频：昆凌穿闪钻纱裙满满公主范 表示自己是送给周董最珍惜的礼物
### [艺考学霸夏梦晒与袁泉同框照，两人长相太相似，不愧都是女神脸](https://new.qq.com/omn/20200910/20200910A0KJ2Z00.html)
> 概要: 艺考学霸夏梦晒与袁泉同框照，两人长相太相似，不愧都是女神脸
### [被全网打假，她真的不尴尬吗？](https://new.qq.com/omn/20200910/20200910A08TPC00.html)
> 概要: 点进这个热搜，我是真实的被尴尬到了。            ——社交平台被发现点赞其他明星黑帖了，明星：不是我，我账号被盗了，盗号的没有心！平台：不可能，我们的平台被盗号概率极低，可能是邮箱被盗了。邮......
### [超级企鹅联盟重启李晨再出征！血洒赛场仍无畏归来 听听他的心里话](https://new.qq.com/omn/SPO20200/SPO2020091001188000.html)
> 概要: 《2020超级企鹅联盟Super3：星斗场》热血重启，李晨再度出征，经历了2019年星斗场考验的他，将再次直面赛场上的激烈竞争，突破身心极致，为热爱而拼搏。在《2019年超级企鹅联盟Super3：星斗......
### [网易知乎互踢皮球，万茜神隐多日，低调发公益没再回应](https://new.qq.com/omn/20200910/20200910A0GC7J00.html)
> 概要: 今年最火的节目当属《乘风破浪的姐姐》，其中收益最大的姐姐当属万茜，变化最大的就是她的代言数量猛增。根据网友统计，万茜靠着《浪姐》预计可以进账上亿元。            在娱乐圈中红就意味着是非多，......
### [肖战鲜少露面却榜上有名，贡献率还稳居榜首，两部剧有望今年播出](https://new.qq.com/omn/20200910/20200910A0C0Q600.html)
> 概要: 肖战的热度竟然一直没有消退。            本来这段时间很少看到他的消息，因为肖战就此真的无人问津。谁知道隔天都打了自己的脸。肖战虽然鲜少露面依旧榜上有名，贡献率还稳居榜首，两部剧也有望在今年......
# 动漫
### [《转生史莱姆》系列宣布累计突破2000万部！](https://news.dmzj.com/article/68531.html)
> 概要: 《关于我转生成为史莱姆的那件事》宣布了系列关联书籍累计突破2000万部的消息，纪念突破2000万部的活动从本日（10日）开始了。
### [剧场版《怪物弹珠 路西法 绝望的黎明》预告篇](https://news.dmzj.com/article/68534.html)
> 概要: 剧场版动画《怪物弹珠 路西法 绝望的黎明》宣布了将于11月6日上映的消息，主宣传图和预告片一并公开。
### [涉谷号召万圣节不要聚集！正在商议举办虚拟空间的活动](https://news.dmzj.com/article/68527.html)
> 概要: 每年万圣节的时候，很多日本的年轻人都会穿上各种节日服装来到东京涉谷区街头参加万圣节活动。不过为了避免新冠病毒的传播，涉谷区在今年宣布了呼吁人们不要来聚集的方针。
### [amiami《偶像大师灰姑娘女孩》高森蓝子1/8比例手办](https://news.dmzj.com/article/68528.html)
> 概要: amiami根据《偶像大师灰姑娘女孩》中的高森蓝子制作的1/8比例手办目前已经开订了。本作采用了原作中“亲手制作的幸福”卡牌上的服装与造型。
### [一拳最新怪人战力图，波罗斯天花板无人撼动，赛大蛇花拳绣腿](https://new.qq.com/omn/20200910/20200910A0CQ5P00.html)
> 概要: 一拳超人是一部搞笑热血动漫，热血一般都是通过角色之间的肉搏来呈现的。既然有肉搏，那么必然有战力的高低之分。下面天师就来跟大家探讨一下，一拳超人中的最新战力排行。如果波罗斯的战斗力为100，那么其他人……
### [异火与祖符谁更强大？同为天地灵物，但实际上却有天差地别！](https://new.qq.com/omn/20200910/20200910A0FDR100.html)
> 概要: 如今，天蚕土豆的两本小说武动乾坤与斗破苍穹皆出了动漫版本，除去斗破苍穹第一季的诟病之外，第二季第三季以及番外篇，和现如今的武动乾坤，都可称得上一部优秀的动漫。无论是人物建模还是情节填充，做的都是可圈……
### [母亲永远停留在15岁，长大的儿子“犯下大错”，这部动漫太好看！](https://new.qq.com/omn/20200910/20200910A0CVU300.html)
> 概要: 母亲永远停留在15岁，长大的儿子“犯下大错”，这部动漫太好看！《朝花夕誓》这部动漫，故事发生在长生不老的金发一族。这里的人类长到15岁，样子就不会再改变了，之后的人生就是长生不老，但这也是愚蠢的人类……
### [阿银献祭后续来了！斗罗官微放照，唐昊单手抱娃迎战菊花关千寻疾](https://new.qq.com/omn/20200910/20200910A0AJDU00.html)
> 概要: 斗罗大陆动画迎来122集预告，武魂殿开始谋划围猎小舞的计划，唐三也抵达了昊天宗，让大家一睹为快。但惊喜并不只有这些，阿银献祭的后续来了！斗罗大陆官微放照，唐昊单手抱娃迎战菊花关与千寻疾。开播至今，唐……
### [B站老板陈睿二次元人设翻车，一首《高达00》ED，让他穿越时空](https://new.qq.com/omn/20200910/20200910A0EMDI00.html)
> 概要: 最近一位在B站唱歌的小姐姐俏俏的火了起来，她的B站账号叫做“我是瞳瞳啊”，这位小姐姐靠着她优秀的实力，在B站初次投稿就获得了多达33万以上的播放量，更是收获了非常多的粉丝，作为一名高达的粉丝当然也第……
### [猎奇漫画《冰棍中奖的诡谈》，有点重口味呀！](https://new.qq.com/omn/20200910/20200910A0CG5600.html)
> 概要: ...搬砖人的剧情解说：不能先吃一半再给店员吗？虽然是美女，但是感觉还是有点重口味呀。
### [漫改日剧《极主夫道》公开剧照 龙哥做饭气势十足](https://acg.gamersky.com/news/202009/1319758.shtml)
> 概要: 由人气漫画《极主夫道》改编的真人日剧将于10月11日开播，现在官方公开了龙哥做料理时候的剧照，效果也是相当的有气势。
### [《海贼王》990话情报：路飞再开四档 孤军求加入](https://acg.gamersky.com/news/202009/1319869.shtml)
> 概要: 《海贼王》990话情报公开，赤霄他们力战凯多，路飞和草帽团也全数登场，还有大妈、King和凌空六子这些全部都在同一个区域战斗，场面可谓是相当乱。
# 财经
### [胡春华：更好发挥大型龙头企业在农业现代化中的引领带动作用](https://finance.sina.com.cn/china/gncj/2020-09-10/doc-iivhuipp3626703.shtml)
> 概要: 原标题：胡春华：更好发挥大型龙头企业在农业现代化中的引领带动作用 中共中央政治局委员、国务院副总理胡春华10日在黑龙江哈尔滨主持召开大型农业龙头企业座谈会。
### [解读中央财经委员会新部署：以现代流通体系建设构建新发展格局](https://finance.sina.com.cn/china/gncj/2020-09-10/doc-iivhuipp3624968.shtml)
> 概要: 原标题：以现代流通体系建设支撑构建新发展格局——解读中央财经委员会新部署 新华社记者陈炜伟、魏玉坤、谢希瑶 “流通体系在国民经济中发挥着基础性作用，构建新发展格局...
### [交通运输部：加快推进现代国际物流供应链体系建设](https://finance.sina.com.cn/china/gncj/2020-09-10/doc-iivhuipp3615937.shtml)
> 概要: 原标题：交通运输部：加快推进现代国际物流供应链体系建设  9月10日，交通运输部部长李小鹏主持召开部务会，会议指出，加快现代化高质量国家综合立体交通网建设...
### [2020中国百强区榜单出炉 前十名广东独揽七席](https://finance.sina.com.cn/china/2020-09-10/doc-iivhvpwy5995010.shtml)
> 概要: 原标题：2020中国百强区榜单出炉 前十名广东独揽七席 什么是城区经济发展最强动能？ 每经记者 张蕊每经编辑 陈星 城区是新时代高质量发展的核心载体，也是城市群...
### [《中国儿童发展纲要》收官年 再议儿童发展工作的过去与未来](https://finance.sina.com.cn/china/gncj/2020-09-10/doc-iivhvpwy6002834.shtml)
> 概要: 原标题：研讨 | 《中国儿童发展纲要》收官年，再议儿童发展工作的过去与未来 来源：中国发展研究基金会 2020年，是我国完成脱贫攻坚，制定“十四五规划”...
### [韩正：加快扩大药品集中带量采购范围 加强中选产品质量监管](https://finance.sina.com.cn/china/gncj/2020-09-10/doc-iivhuipp3614898.shtml)
> 概要: 原标题：韩正：加快扩大药品集中带量采购范围，加强中选产品质量监管 韩正主持召开药品和高值医用耗材集中带量采购工作座谈会。韩正强调...
# 科技
### [Executors使用不当引起的内存溢出](https://segmentfault.com/a/1190000023960033)
> 概要: 线上服务内存溢出这周刚上班突然有一个项目内存溢出了，排查了半天终于找到问题所在，在此记录下，防止后面再次出现类似的情况。先简单说下当出现内存溢出之后，我是如何排查的，首先通过jstack打印出堆栈信息......
### [HMS Core 5.0：全球第三大移动应用生态破土而出](https://segmentfault.com/a/1190000024036107)
> 概要: 华为消费者业务 CEO 余承东在华为开发者大会上宣布，HMS 目前成为全球第三大移动应用生态，已有 180 万开发者、9.6 万应用集成 HMS，活跃用户达到 4.9亿，应用累计分发量达到 2610 ......
### [Design, edit and share custom terminal color schemes](https://terminal.sexy/)
> 概要: Design, edit and share custom terminal color schemes
### [Animal Populations Fell by 68% in 50 Years and It’s Getting Worse](https://www.bloomberg.com/news/articles/2020-09-09/climate-change-and-food-businesses-are-driving-environmental-failure-globally)
> 概要: Animal Populations Fell by 68% in 50 Years and It’s Getting Worse
### [华为鸿蒙，告别 PPT](https://www.huxiu.com/article/381511.html)
> 概要: 2020 年 9 月 15 日，对于大部分人来说，这可能只是一个普通的周二。但对于华为来说，这是禁令缓和期结束的日子，从这天以后，华为一切业务面临的，都将会是完全不同的光景。在这个关键的时间节点上，华......
### [引领中国房价的12个超级地段](https://www.huxiu.com/article/381310.html)
> 概要: 来源｜城市战争（ID：sunbushu123）作者｜孙不熟头图｜视觉中国我有一个习惯，就是每到一个城市，都想去这座城市房价最高的地段看看。因为从那里，大抵可以看出一个城市的“追求”是什么。如果房价至高......
### [Web产品的原型尺寸需要遵循什么规范？](https://www.tuicool.com/articles/q22aYf3)
> 概要: 编辑导读：在产品经理开始画Web产品原型之前，一定要清楚使用什么尺寸来画出原型的页面。本篇文章作者详细介绍了Web产品原型尺寸的常用规范和相关步骤，并对每个步骤需要注意的问题进行了梳理分析，与大家分享......
### [传“青春小酒”江小白再拿融资，它真的值130亿？](https://www.tuicool.com/articles/BbAvEf7)
> 概要: 创立于2012年的白酒品牌江小白又有融资消息了。9月8日，36氪援引信源消息称，江小白即将完成新一轮融资，华兴新经济基金领投，正心谷资本、Baillie Gifford、招银国际等机构跟投。爆料中没有......
# 小说
### [钚龙领域](https://m.qidian.com/book/1016201709/catalog)
> 作者：黑烟滚滚

> 标签：原生幻想

> 简介：当西隆穿越发现自己在十几岁时体型就超过自己两百多岁的银龙老妈时，他就发誓要为整个宇宙带来核平与娱乐！战争是一种陋习！凡是还在使用这种陋习的国家，都将迎接我的核平吐息！所有的战端都将消失！使用新的“战斗”来决定胜负！大陆英雄传说（卡牌游戏）！你值得拥有！多年以后，人们只要发生矛盾，便会原地摆出战斗场地，来一场大陆英雄传说对决。为了名望而努力修炼的人，他们不再大陆搞风搞雨，而是会参加西隆帝国的传奇大会，展现自己的才能。期望自己能够被看中，从而被加入大陆英雄传说的卡牌之中，名扬大陆，传颂千古！整个世界似乎都进入了核平时期。这是一个投错胎的变异钚龙，为宇宙带来核平与变革的时代！顺便公布下书友群吧：三三三六一九三六一.日常灌水。

> 章节总数：共375章

> 状态：完本
# 游戏
### [D3P TGS 2020新情报：《地球防卫军6》等作品参展](https://www.3dmgame.com/news/202009/3797145.html)
> 概要: 目前，D3P官方已经公开东京电玩展（TGS 2020）线上直播计划，此次《地球防卫军6》、《方块地球防卫军：世界兄弟》和《Maglam Lord》将参展，此次直播活动开始时间为北京时间2020年9月2......
### [中国民营企业500强榜单发布 华为第一苏宁第二](https://www.3dmgame.com/news/202009/3797133.html)
> 概要: 9月10日今天上午，由全国工商联主办的2020中国民营企业500强峰会10日在京举行，会上发布了2020中国民营企业500强榜单。华为投资控股有限公司以2019年营业收入8588.33亿元再次蝉联榜首......
### [Hottoys推出“毒液化钢铁侠”人偶 售价2680元](https://www.3dmgame.com/news/202009/3797155.html)
> 概要: 今日（9月10日），Hottoys推出了新品漫威动画《蜘蛛侠：最大毒液》为主题的Artist Collection 毒液化钢铁侠1:6比例珍藏人偶 (特别版) ，此款珍藏人偶霸气十足，突破传统框框。毒......
### [《魔兽世界：暗影国度》登陆界面曝光 冰冠堡垒霸气](https://ol.3dmgame.com/news/202009/28391.html)
> 概要: 《魔兽世界：暗影国度》登陆界面曝光 冰冠堡垒霸气
### [《原神》演示GIF：多种怪物外观及攻击方式公开](https://www.3dmgame.com/news/202009/3797152.html)
> 概要: 米哈游官方于今日（9月10日）公开了一大波《原神》的演示GIF，展示了游戏中多种怪物的外观及攻击方式。《原神》全球同步公测（iOS、Android、PC、PS4）将于9月28日正式开启，其PC版技术性......
# 论文
### [Diagnosing the Environment Bias in Vision-and-Language Navigation](https://paperswithcode.com/paper/diagnosing-the-environment-bias-in-vision-and-1)
> 日期：ICLR 2020

> 标签：None

> 代码：https://github.com/zhangybzbo/EnvBiasVLN

> 描述：Vision-and-Language Navigation (VLN) requires an agent to follow natural-language instructions, explore the given environments, and reach the desired target locations. These step-by-step navigational instructions are crucial when the agent is navigating new environments about which it has no prior knowledge.
### [PSConv: Squeezing Feature Pyramid into One Compact Poly-Scale Convolutional Layer](https://paperswithcode.com/paper/psconv-squeezing-feature-pyramid-into-one)
> 日期：13 Jul 2020

> 标签：REPRESENTATION LEARNING

> 代码：https://github.com/d-li14/PSConv

> 描述：Despite their strong modeling capacities, Convolutional Neural Networks (CNNs) are often scale-sensitive. For enhancing the robustness of CNNs to scale variance, multi-scale feature fusion from different layers or filters attracts great attention among existing solutions, while the more granular kernel space is overlooked.
