---
title: 2021-02-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DalmatianPelicans_EN-CN7342550146_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-02-23 21:35:38
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DalmatianPelicans_EN-CN7342550146_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [The Bombard Story](https://greatestadventurers.com/the-bombard-story/)
> 概要: The Bombard Story
### [《东京猫猫》完全新作《东京猫猫 NEW～♡》2022年开播](https://news.dmzj1.com/article/70184.html)
> 概要: 动画《东京猫猫》新作《东京猫猫 NEW～♡》宣布了将于2022年开始播出的消息，本作的宣传图和主要制作阵容也一并公开。
### [「狂赌之渊」漫画第83话彩页公开](http://acg.178.com//202102/408047523767.html)
> 概要: 由河本ほむら原作、尚村透作画的漫画「狂赌之渊」公开了第83话的彩页！漫画「狂赌之渊」于2014年03月22日在「月刊GANGAN JOKER」上开始连载，其外传漫画为「狂赌之渊 双」、「狂赌之渊 妄」......
### [动画电影「寻找见习魔女」Blu-ray特别封面公开](http://acg.178.com//202102/408047577998.html)
> 概要: 「小魔女DoReMi」20周年纪念动画电影「寻找见习魔女」公开了Blu-ray特别封面，该封面由马越嘉彦绘制。该系列商品将于4月2日发售，售价7,900日元（含税），特典包括特制小册子、收录未公开片段......
### [既然选不出来，不如全都要的少女漫](https://news.dmzj1.com/article/70185.html)
> 概要: 这期给大家推荐一部很古早画风的少女漫《这份凶爱是为天灾》，作者梦木みつる，红莲汉化组汉化，被封印的上古时代之神——四凶，复活的钥匙掌握在一位少女手中，妖艳的中国神话玄幻爱情故事。
### [动画《哥斯拉：奇点》公开全新视觉图 4月1日开播](https://acg.gamersky.com/news/202102/1364606.shtml)
> 概要: 全新哥斯拉动画《哥斯拉：奇点》在今天（2月23日），公开了全新的视觉图，宣布本作将于4月1日在日本开播。
### [组图：朱正廷黑衣Look潮范儿足 双手插兜反戴帽子酷帅不羁](http://slide.ent.sina.com.cn/star/slide_4_704_352991.html)
> 概要: 组图：朱正廷黑衣Look潮范儿足 双手插兜反戴帽子酷帅不羁
### [内部人士：《Elden Ring》80%会在2021年内发售](https://www.3dmgame.com/news/202102/3809141.html)
> 概要: 万代最近将《Elden Ring》PEGI评级更新为16，这可能是在暗示《Elden Ring》有新消息要来了。但近日，据前GameInformer高级编辑Imran Khan在直播中透露的消息，《E......
### [《海贼王》X《花花公子》COS特辑 女体路飞身材火辣](https://acg.gamersky.com/news/202102/1364575.shtml)
> 概要: 为纪念《海贼王》漫画连载1000话，官方特别推出了《海贼王》和日本《花花公子周刊》杂志联名特刊，让女星（模特）COS《海贼王》中的知名女性角色。
### [视频：曝陶虹车祸后遗症辞演新剧？刘敏涛将替换救场](https://video.sina.com.cn/p/ent/2021-02-23/detail-ikftssap8238672.d.html)
> 概要: 视频：曝陶虹车祸后遗症辞演新剧？刘敏涛将替换救场
### [双女主轮流说suki♡《青梅不输》漫画第2卷纪念PV公开](https://news.dmzj1.com/article/70186.html)
> 概要: 漫画《青梅竹马绝对不会输的恋爱喜剧》发售第2卷，纪念PV由水濑祈和佐仓绫音担任配音，内含大量“suki♡”
### [「咒术回战」第19话原画公开](http://acg.178.com//202102/408051906509.html)
> 概要: TV动画「咒术回战」已经播出到第19话。近日，官方公开了第19话原画。「咒术回战」是根据芥见下下笔下同名漫画改编的TV动画，讲述的是遭遇“诅咒”的虎杖悠仁与“特级咒物”融合之后的壮阔冒险故事......
### [凡妮莎斥MeekMill新歌不尊重科比:歌词麻木不仁](https://ent.sina.com.cn/y/youmei/2021-02-23/doc-ikftpnny9253969.shtml)
> 概要: 新浪娱乐讯 2月23日，Meek Mill近日的新歌《Don‘t Worry （RIP Kobe）》其中一句歌词引发争议，“Yeah， and if I ever lack， I’m goin’ ou......
### [微软仍在评估FPS Boost是否适用于前两代Xbox游戏](https://www.3dmgame.com/news/202102/3809165.html)
> 概要: 最近，微软为Xbox Series主机推出了Xbox FPS Boost功能，该功能可以有效的提高老游戏的帧数。不过该技术目前似乎仅重于Xbox One平台的游戏。项目管理负责人Jason Ronal......
### [视频：林志玲与老公首次合体拍广告 志玲全身照仍无孕相](https://video.sina.com.cn/p/ent/2021-02-23/detail-ikftpnny9267861.d.html)
> 概要: 视频：林志玲与老公首次合体拍广告 志玲全身照仍无孕相
### [教育部：中小学准备手机统一保管装置 禁止带入课堂](https://www.3dmgame.com/news/202102/3809167.html)
> 概要: 今年2月初，教育部发文要求加强中小学生手机管理，明确要求中小学生原则上不得将个人手机带入校园，不得使用手机布置作业或要求学生利用手机完成作业。今天教育部发布了《关于春季学期中小学教育教学有关工作情况》......
### [Man Rewrites SNES Racing Game to Improve Its Framerate Sevenfold](https://kotaku.com/hacker-rewrites-crappy-snes-racer-to-improve-its-frame-1846328191)
> 概要: Man Rewrites SNES Racing Game to Improve Its Framerate Sevenfold
### [浅谈 Vite 2.0 原理，依赖预编译，插件机制是如何兼容 Rollup 的？](https://www.tuicool.com/articles/JvumAff)
> 概要: 前言Hi，我是 ssh，春节过去了，躁动的心也该收一收，开始新一年的学习了。我目前就职于字节跳动的 Web Infra 团队，目前团队还很缺人（尤其是北京）。为此我组建了一个氛围特别好的招聘社群，大家......
### [等等小花画“粑粑冰淇淋”送爸爸，邓超无奈称陪伴是最长情的告白](https://new.qq.com/omn/20210223/20210223A09KJD00.html)
> 概要: 真是十级扎心啊！2月23日下午，邓超在个人社交平台分享了一段与儿女的互动日常。这次等等和小花特意给爸爸准备了一份“特殊礼物”，让一向淡定十足的超哥也差点“失语”，他还颇为无奈地写道：“陪伴是最长情的告......
### [组图：管栎顺毛造型秒变乖乖仔 对镜头打招呼眼神超温柔](http://slide.ent.sina.com.cn/star/slide_4_704_353012.html)
> 概要: 组图：管栎顺毛造型秒变乖乖仔 对镜头打招呼眼神超温柔
### [达利欧：判断泡沫的六大标准](https://www.huxiu.com/article/411043.html)
> 概要: 本文来自微信公众号：华尔街见闻（ID：wallstreetcn），作者：达利欧，编辑：曾心怡，头图来自：视觉中国美股涨至纪录高位，美债收益率飙升，通胀压力走高，美国市场“泡沫说”再次甚嚣尘上。现在到底......
### [朱宁：全球资产价格存在非常大的泡沫，或现大幅调整](https://www.huxiu.com/article/411050.html)
> 概要: 本文来自微信公众号：腾讯财经（ID：financeapp），作者：上海交大上海高级金融学院副院长、金融学教授朱宁，题图来自：视觉中国2021年全球市场普遍“开门红”之后，从美国到亚太市场，从原油到比特......
### [江苏卫视元宵荔枝灯会惊喜官宣，段奥娟黄霄雲新歌献唱](https://new.qq.com/omn/20210223/20210223A0CWKY00.html)
> 概要: 春晚圆满收官，元宵惊喜接力。大年初一播出的江苏卫视牛年春晚广受好评，获得了《光明日报》、新华网、中新网等媒体点赞，多网收视成绩亮眼，CSM63城平均收视1.54%，CSM35城平均收视1.62%，微博......
### [《你好，李焕英》票房超《复联 4》，列中国影史票房第四位](https://www.ithome.com/0/536/404.htm)
> 概要: IT之家 2 月 23 日消息 根据猫眼专业版的数据，截止发稿时，《你好，李焕英》总票房达到了 42.69 亿元，超过了 42.50 亿元的《复仇者联盟 4：终局之战》，列中国影史票房第四位。IT之家......
### [郑州昔日地标裕达国贸首拍流拍 围观者过万、无一报名](https://finance.sina.com.cn/china/gncj/2021-02-23/doc-ikftssap8335335.shtml)
> 概要: 原标题：郑州昔日地标裕达国贸首拍流拍 围观者过万、无一报名 证券时报网讯，据大河财立方报道，郑州裕达国际贸易中心首拍出现流拍。
### [多地瞄准综合性国家科学中心第五城 谁更有胜算？](https://finance.sina.com.cn/china/2021-02-23/doc-ikftpnny9322171.shtml)
> 概要: 原标题：多地瞄准综合性国家科学中心第五城，谁更有胜算？ 从区域分布来看，长三角、华北、珠三角已有综合性国家科学中心，整个中西部暂时空缺...
### [工信部征求限制无线充电功率意见 5G手机快充比拼将止步？](https://finance.sina.com.cn/jjxw/2021-02-23/doc-ikftssap8338147.shtml)
> 概要: 原标题：工信部征求限制无线充电功率意见，5G手机快充比拼将止步？ 使用5G手机，耗电量问题一直很困扰用户，手机厂商不得不绞尽脑汁往机身里塞更大容量的电池...
### [财经TOP10|货拉拉涉事司机被刑拘！致命6分钟与3次偏离路线](https://finance.sina.com.cn/china/caijingtop10/2021-02-23/doc-ikftpnny9325747.shtml)
> 概要: 【宏观要闻】 NO.1两会前瞻：中国经济恢复进行时 今年发展目标怎么定？ “其中最吸引眼球的指标依然是GDP增速” 刚刚过去的春节假期...
### [沪深广、京港澳磁悬浮落地尚需时日 三大城市群时空圈有望更紧密](https://finance.sina.com.cn/china/gncj/2021-02-23/doc-ikftpnny9325012.shtml)
> 概要: 原标题：沪深广、京港澳磁悬浮落地尚需时日，三大城市群“时空圈”有望更紧密 中国的高速磁悬浮交通系统研发已经取得突破，技术上可行，但从国土空间规划、成本投入...
### [长江保护法3月1日起实施：进入“十年休渔”的长江如何共抓大保护](https://finance.sina.com.cn/china/gncj/2021-02-23/doc-ikftssap8341219.shtml)
> 概要: 原标题：长江保护法3月1日起实施：进入“十年休渔”的长江如何共抓大保护 2月23日，全国人大常委会办公厅以长江保护法实施为主题，举行专题新闻发布会。
### [《罪恶装备：Strive》测试：PS5版原生4K/60FPS](https://www.3dmgame.com/news/202102/3809184.html)
> 概要: 万代发布了《罪恶装备》新作《罪恶装备：Strive》的新手视频，以为即将到来的BETA公测做准备。近日外媒VG Tech对BETA做了一次帧数测试（PS4,PS4 Pro和PS5），好消息是该作在三平......
### [中央文献研究室主编，《周恩来传》全 3 册 39.8 元](https://lapin.ithome.com/html/digi/536415.htm)
> 概要: 中央文献研究室主编，《周恩来传（1898-1976）》全3册报价69.8元，限时限量30元券，实付39.8元包邮，领券并购买。《艰苦卓绝周恩来（1898-1949）》是《周恩来生平》系列的第一本，全书......
### [香港武打明星郑佩佩女儿公布喜讯：混血儿子出生，非洲老公出镜](https://new.qq.com/omn/20210223/20210223A0E0DJ00.html)
> 概要: 2月23日，演员原子惠在网上晒出自己和老公一家三口的合影公布生子喜讯，引来网友祝福。            说起原子惠可能大家不是很熟悉，但说起她的母亲郑佩佩，我们想必都有印象。作为早年间邵氏电影公司......
### [国产DDR5内存：频率4800MHz起步 最高达128GB](https://www.3dmgame.com/news/202102/3809185.html)
> 概要: 国内内存制造厂商阿斯加特（ASGARD）推出了他们的首款DDR5内存，容量分别为32GB、64GB、128GB三种。宣传图上的是64GB版本，绿色PCB板，没有任何辅助散热。该款内存的频率为4800M......
### [杨紫李现再续前缘，被她的婚纱造型惊艳，与胡一天拼团婚礼引期待](https://new.qq.com/omn/20210223/20210223A0DWW400.html)
> 概要: 2月23日，杨紫的一组婚纱造型曝光，这一次所曝光的婚纱造型可以说是十分惊艳。并且这一次杨紫的婚纱造型也是极其的有意义，也可以说是为了与李现再续前缘所装扮的。杨紫李现再续前缘，被她的婚纱造型惊艳，与胡一......
### [唐嫣开工分享女儿近况，透露女儿兴趣广泛，过年期间买了拼图陪她玩耍](https://new.qq.com/omn/20210223/20210223A0E4HZ00.html)
> 概要: 2月23日，许久未露面的唐嫣正式开工，亮相某品牌直播间为其宣传，这也是唐嫣新年过后首次亮相，在家好好休息了一段时间，唐嫣的整个状态极佳，看上去并没有因为过年而增重，还是那么漂亮，气质超好。      ......
### [Shopee落子墨西哥：看东南亚电商巨头的拉美野望](https://www.tuicool.com/articles/BRfqmy6)
> 概要: 36氪出海网站上线了！获取更多全球商业相关资讯，请点击letschuhai.com。据路透社2月23日消息，东南亚电商平台 Shopee 已于日前在墨西哥推出了一款应用程序，正式进军墨西哥市场。据悉，......
# 小说
### [我和TAC50的相处时间](http://book.zongheng.com/book/904064.html)
> 作者：爱拍小八云

> 标签：竞技同人

> 简介：在经历了“异世界的魔王讨伐时空事件”后，2023年6月12日，小八云总算回到了已经有半年多没有回到的家乡惠州市惠东县并准备给自己放上一段时间的假期，然而这假期还不到一天的时候，一个来自加拿大的美裔“异瞳少女”的突然到访让小八云踏上了一场休假之前的后一次任务的旅程.......

> 章节末：本书后记

> 状态：完本
# 论文
### [Attentive Weights Generation for Few Shot Learning via Information Maximization](https://paperswithcode.com/paper/attentive-weights-generation-for-few-shot-1)
> 日期：CVPR 2020

> 标签：FEW-SHOT IMAGE CLASSIFICATION

> 代码：https://github.com/Yiluan/AWGIM

> 描述：Few shot image classification aims at learning a classifier from limited labeled data. Generating the classification weights has been applied in many meta-learning methods for few shot image classification due to its simplicity and effectiveness.
### [The growth equation of cities](https://pattern.swarma.org/paper?id=4556b1c4-29c3-11eb-863d-0242ac1a000c)
> 概要: The science of cities seeks to understand and explain regularities observed in the world’s major urban systems. Modelling the population evolution of cities is at the core of this science and of all urban studies. Quantitatively, the most fundamental problem is to understand the hierarchical
