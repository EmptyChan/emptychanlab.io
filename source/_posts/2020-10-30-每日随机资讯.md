---
title: 2020-10-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FishOwl_EN-CN6613947835_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-10-30 21:28:03
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FishOwl_EN-CN6613947835_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：余文乐借新办公室晒千万名画 引网友夸赞收藏眼光高](http://slide.ent.sina.com.cn/star/slide_4_704_347484.html)
> 概要: 组图：余文乐借新办公室晒千万名画 引网友夸赞收藏眼光高
### [组图：杨怡晒女儿小珍珠近照 浓眉大眼睫毛超长惹人喜爱](http://slide.ent.sina.com.cn/star/slide_4_704_347467.html)
> 概要: 组图：杨怡晒女儿小珍珠近照 浓眉大眼睫毛超长惹人喜爱
### [蔡徐坤整理微博关注列表 清空又重新关注忙碌一夜](https://ent.sina.com.cn/y/yneidi/2020-10-30/doc-iiznctkc8484237.shtml)
> 概要: 新浪娱乐讯 10月30日，有网友发现蔡徐坤凌晨三点上线，清理了自己微博账号的关注列表，直到四点半左右将所有的关注清零。但蔡徐坤并没有急着下线，而是又花了将近两个半小时的时间，又一个一个账号进行了重新关......
### [视频：周杰伦投资KTV被判侵权 需赔偿并删除侵权作品](https://video.sina.com.cn/p/ent/2020-10-30/detail-iiznezxr9019120.d.html)
> 概要: 视频：周杰伦投资KTV被判侵权 需赔偿并删除侵权作品
### [视频：不是王承渲！徐轸轸承认录青你时对冰清玉洁翻白眼](https://video.sina.com.cn/p/ent/2020-10-30/detail-iiznezxr8950446.d.html)
> 概要: 视频：不是王承渲！徐轸轸承认录青你时对冰清玉洁翻白眼
### [气走徐枫，断交芦苇，令柳岩做噩梦，十五年后，陈凯歌仍输在度量上](https://new.qq.com/omn/20201030/20201030A0BEIB00.html)
> 概要: 气走徐枫，断交芦苇，令柳岩做噩梦，十五年后，陈凯歌仍输在度量上
### [张柏芝晒儿子2岁生日礼物，放声狂笑超开心，未透露赠与人身份](https://new.qq.com/omn/20201030/20201030A0BHSE00.html)
> 概要: 张柏芝晒儿子2岁生日礼物，放声狂笑超开心，未透露赠与人身份
### [都是同样一颗头，为啥倪妮是“高级感”，她就得被嘲“胖头鱼”啊？！](https://new.qq.com/omn/20201029/20201029A0HN1D00.html)
> 概要: 曾凭借一款超显瘦的“辛芷蕾头”（锁骨短发），而火遍大江南北的理发店、养活了无数Tony老师的但这次网友们可不是夸她，而是群嘲……以下是活动现场的官方图，            不过这位姐的性格还挺有趣......
### [中国好声音：谢霆锋提前退出冠军争夺，悬念落在单依纯和潘虹之间](https://new.qq.com/omn/20201030/20201030A0AR8P00.html)
> 概要: 中国好声音：谢霆锋提前退出冠军争夺，悬念落在单依纯和潘虹之间
### [张艺谋小30岁妻子罕现身！穿棉服仍然苗条，面容姣好气质不输女星](https://new.qq.com/omn/20201030/20201030A0F1SH00.html)
> 概要: 张艺谋小30岁妻子罕现身！穿棉服仍然苗条，面容姣好气质不输女星
# 动漫
### [P站美图推荐——中式僵尸特辑](https://news.dmzj.com/article/69004.html)
> 概要: 请咬我，朝这里咬！（露脖子）
### [阔诺新连载哒！10月新连载漫画不完全指北第四期](https://news.dmzj.com/article/69015.html)
> 概要: 与被人看不起的无名女神缔结契约的我，究竟会成为什么样的人——？！那个总是来打扰我睡觉的学妹，其实是喜欢我——？！但是不要打扰我睡觉啊！！旅途的终点是希赖斯……会是什么呢？！请看本周不完全指北！
### [演员伊藤健太郎被捕 为白石由竹等角色配音的同名声优躺枪](https://news.dmzj.com/article/69010.html)
> 概要: 在昨天（29日）成为大家介绍过，出演过漫改电影《恶之华》，娱乐节目《海贼王我当定了TV》等作品的演员伊藤健太郎因肇事逃逸被警方逮捕的消息，同名声优的伊藤健太郎躺枪。
### [GSC《战翼的希格德莉法》六车宫古1/7比例手办开订](https://news.dmzj.com/article/69006.html)
> 概要: GSC根据目前正在播出TV动画的《战翼的希格德莉法》中的六车宫古制作的1/7比例手办开始预订了。本作是按照担任角色原案的藤真拓哉为手办化专门绘制的插图制作的，充满跃动感的造型展现了她活泼开朗的性格特点。
### [COS欣赏：月读·阿尔匹娜](https://new.qq.com/omn/20201030/20201030A09LPH00.html)
> 概要: 原作：假面骑士时王CN：王子摄影：松鼠妆面：寻衣服装：夜君后期：阿洛伊
### [黑夜有所斯女神动漫头像来袭](https://new.qq.com/omn/20201030/20201030A07H3U00.html)
> 概要: 长期医嘱: 保持乐观 永远温柔 .短期医嘱：活在当下 随遇而安.
### [鸣人吃过最贵的食物，拉面免费，最后一顿堪称无价！](https://new.qq.com/omn/20201030/20201030A04BPG00.html)
> 概要: 火影忍者这部动漫相信大家都很熟悉了，这是一部经典的热血动漫。虽然这部动漫已经完结，但是这部动漫的人气始终不减。今天小编给大家介绍的是主角漩涡鸣人，鸣人一生追求忍术同时还追求忍界美食。鸣人吃过最贵的食……
### [火影忍者：鸣人脸上为何自带“六道线”？原来他才是真正的九尾](https://new.qq.com/omn/20201030/20201030A05NC800.html)
> 概要: 《火影忍者》作为日本有名的热血漫，从完结到现在已有五年，感叹时光飞逝之时，火影迷依旧热衷于讨论其中的故事，甚至会在续作中找回曾经的感觉。而在博人传里，博人不仅继承了鸣人的意外性，也遗传了脸上的纹路，……
### [《新世纪福音战士》初号机2米高巨型手办登场 定价10万元](https://new.qq.com/omn/20201030/20201030A02WQL00.html)
> 概要: 又一款巨型手办登场，本次是来自超人气动漫《新世纪福音战士》的初号机，并没有完全站直已经超过了2米高，定价高达170万日元（10.9万元），一起来了解下。·镇宅之宝再添新品，秉承日商天价手办习俗，巨型……
### [秦时明月之沧海横流：掩日的四张脸那个会是真的？又是纵横合力击杀的目标](https://new.qq.com/omn/20201030/20201030A00F6400.html)
> 概要: 感谢大家的关注！原创未经允许禁止转载谢谢！随着《秦时明月之沧海横流》的上映，圈子里掀起一股泥石流。网友们不再讨论谁厉害了，反而从第一集开始就死活要知道这个掩日到底是谁。已经更新到了第七集了，想着有生……
# 财经
### [外汇局：推进高水平资本项目开放 提升跨境贸易投资便利化水平](https://finance.sina.com.cn/china/2020-10-30/doc-iiznezxr9024490.shtml)
> 概要: 国家外汇管理局党组学习传达贯彻党的十九届五中全会精神 2020年10月30日上午，国家外汇管理局党组书记、局长潘功胜主持召开党组会议...
### [32岁升正处、长期任职省委办公厅 这名“80后”任县委书记](https://finance.sina.com.cn/china/2020-10-30/doc-iiznezxr9021810.shtml)
> 概要: 据四川省凉山州宁南县政府官网消息：10月29日下午，召开全县领导干部大会，宣布管昭同志任中共宁南县委书记。 记者注意到，管昭是一名“80后”，出生于1980年1月...
### [中国财富管理50人论坛课题组谈十四五“双循环”新发展格局](https://finance.sina.com.cn/china/gncj/2020-10-30/doc-iiznezxr9018853.shtml)
> 概要: 以深化改革开放为抓手，推动经济高质量发展 ——构建“十四五”时期“双循环”新发展格局 中国财富管理50人论坛课题组 摘 要 2020年5月14日...
### [央行：加强和改善金融宏观调控 健全现代金融体系](https://finance.sina.com.cn/china/2020-10-30/doc-iiznezxr9025395.shtml)
> 概要: 人民银行党委召开会议认真传达学习党的十九届五中全会精神 2020年10月30日下午，人民银行党委召开会议，认真传达学习中国共产党第十九届中央委员会第五次全体会议精神...
### [山东：一批外包装核酸检测呈阳性进口冻猪肉流入](https://finance.sina.com.cn/china/dfjj/2020-10-30/doc-iiznezxr9025542.shtml)
> 概要: 原标题：山东：一批外包装核酸检测呈阳性进口冻猪肉流入 潍坊安丘发通告请有关市民自觉进行核酸检测 今天（10月30日），山东潍坊安丘市委统筹疫情防控和经济运行工作领导小...
### [南宁警方抓获贩卖“笑气”团伙：向娱乐场所的客人和学生加价销售](https://finance.sina.com.cn/china/2020-10-30/doc-iiznctkc8606845.shtml)
> 概要: 南宁警方抓获贩卖“笑气”团伙：向娱乐场所的客人和学生加价销售  新京报讯（记者 海阳）从江苏南通、安徽芜湖等地大量购买瓶装“笑气”...
# 科技
### [Linux常用命令集合(后端专用操作手册)](https://segmentfault.com/a/1190000037666881)
> 概要: Linux 常用命令集合(后端专用操作手册)Linux 的命令的主要分为对文件基本信息的操作、对文件与目录的操作命令、对用户和用户组的操作的命令、对磁盘管理的操作命令、以及 vim、yum、apt.在......
### [Elasticsearch 如何做到快速检索 - 倒排索引的秘密](https://segmentfault.com/a/1190000037658997)
> 概要: All problems in computer science can be solved by another level of indirection.”– David J. Wheeler“计......
### [华为将推出车载智慧屏，搭载鸿蒙系统，支持 HiCar 系统](https://www.ithome.com/0/516/683.htm)
> 概要: IT之家10月30日消息 在今日的华为国内发布会上，华为消费者业务手机产品线总裁何刚发表演讲，他表示，除了 500 万台新车会预装华为 HiCar，华为智选还将面向旧款车型推出车载智慧屏，后续安装之后......
### [天猫：华为 Mate 40 Pro 系列首批 20 秒售罄，双 11 期间将加大供货](https://www.ithome.com/0/516/735.htm)
> 概要: IT之家10月30日消息 今天下午，华为Mate 40系列发布会举行，华为正式发布了Mate 40和Mate 40 Pro、Mate 40 Pro+手机，华为Mate40系列延续Mate中轴对称设计，......
### [谁在赚1.8亿精神病人的钱？](https://www.huxiu.com/article/390608.html)
> 概要: 每每说起精神病，不知道有多少人脑子里浮现的还是那种在医院走廊里撞墙、嘶吼、打人的场景。殊不知，中国现在每8个人里，就会有1个精神病。实际上，除了精神分裂，精神疾病还有很多常见类型，抑郁症、躁狂症、强迫......
### [买完3080都喊亏，AMD的新显卡用价格砸懵了所有人](https://www.huxiu.com/article/390656.html)
> 概要: 本文来自微信公众号：差评（ID：chaping321），作者：托尼，头图来自：AMDAMD 最新的显卡发布会，逆天了！可能有小伙伴不太清楚这句话是什么意思，托尼先给大家来点儿前情提要：作为一家同时研发......
### [强化学习算法DeepCube，机器自行解决复杂魔方问题](https://www.tuicool.com/articles/FJVBZ3q)
> 概要: 译者：AI研习社（季一帆）双语原文链接：https://www.yanxishe.com/TextTranslation/2719前瞻我花了近一年的时间写《动手深度强化学习》一书，距离该书出版已经过去......
### [花小猪孙枢：希望满足更多用户需求 90后用户占六成](https://www.tuicool.com/articles/e6fQVjm)
> 概要: 花小猪打车总经理孙枢新浪科技讯 10月30日下午消息，滴滴今日举行花小猪打车媒体开放日，花小猪打车总经理孙枢表示，做花小猪的初衷是希望提供更实惠的网约车产品，让用户出行更安全更便捷；也作为司机的收入补......
# 小说
### [万古仙劫](http://book.zongheng.com/book/774111.html)
> 作者：浩辰羽

> 标签：奇幻玄幻

> 简介：万古天穹，世代传承。本为天穹的守护者，世代镇守天穹，护佑万灵，为此还献祭了八代先祖。然而，奈何异仙可守，人心难测，莫大的牺牲，换来的却是同族兄弟的“阴谋陷害”，落得家破人亡，血脉几乎尽断，唯独留下独根帝血，遗落下界诅咒之地，而此时异仙异动，到底谁能继续守护这片天穹.....

> 章节末：第0475章  完结

> 状态：完本
# 游戏
### [Steam现已开启万圣特卖活动 《死亡搁浅》新史低](https://www.3dmgame.com/news/202010/3800860.html)
> 概要: 随着万圣节的到来，Steam现已开启2020万圣节特卖活动，多款游戏新史低促销中。本次万圣节特卖活动时间为10月29日至11月2日，以下是特卖详情。steam商城页面截图：>>>>点我前往steam页......
### [《英雄联盟》KDA女团登上DAZED封面 画面唯美！](https://ol.3dmgame.com/news/202010/28994.html)
> 概要: 《英雄联盟》KDA女团登上DAZED封面 画面唯美！
### [网易阴阳师手游改编 电影《侍神令》定档大年初一](https://www.3dmgame.com/news/202010/3800868.html)
> 概要: 改编自网易手游《阴阳师》的奇幻电影《侍神令》，于今日（10月30日）发布全新海报，确认影片定档2021年大年初一上映。《侍神令》由陈坤、周迅、陈伟霆、屈楚萧、王丽坤、沈月、王紫璇、王悦伊主演。讲述了穿......
### [《黑相集：稀望镇》Steam特别好评 第三作预告片](https://www.3dmgame.com/news/202010/3800904.html)
> 概要: 开发商Supermassive和发行商万代发布了《黑相集》系列第三作《灰烬之屋（House of Ashes）》的预告片，定于2021年发售。从预告片看，这是一个发生在沙漠中的恐怖故事。预告片：这个预......
### [疫情题材惊悚片《鸣鸟》预告 迈克尔贝聚焦新型病毒](https://www.3dmgame.com/news/202010/3800878.html)
> 概要: 今日(10月30日)迈克尔·贝制作的疫情题材科幻惊悚片《鸣鸟》发布了全新预告，一起来欣赏下视频吧！全新预告：在预告中，一种袭击人类的病毒在全世界范围内造成了大量的伤亡与恐慌，而人类唯一的生存办法就是呆......
# 论文
### [Snoopy: Sniffing Your Smartwatch Passwords via Deep Sequence Learning](https://paperswithcode.com/paper/snoopy-sniffing-your-smartwatch-passwords-via)
> 日期：10 Dec 2019

> 标签：

> 代码：https://github.com/ChristopherLu/snoopy

> 描述：Demand for smartwatches has taken off in recent years with new models which can run independently from smartphones and provide more useful features, becoming first-class mobile platforms. One can access online banking or even make payments on a smartwatch without a paired phone.
### [SuperMix: Supervising the Mixing Data Augmentation](https://paperswithcode.com/paper/supermix-supervising-the-mixing-data)
> 日期：10 Mar 2020

> 标签：DATA AUGMENTATION

> 代码：https://github.com/alldbi/SuperMix

> 描述：In this paper, we propose a supervised mixing augmentation method, termed SuperMix, which exploits the knowledge of a teacher to mix images based on their salient regions. SuperMix optimizes a mixing objective that considers: i) forcing the class of input images to appear in the mixed image, ii) preserving the local structure of images, and iii) reducing the risk of suppressing important features.
