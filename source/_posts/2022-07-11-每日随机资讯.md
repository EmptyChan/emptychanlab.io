---
title: 2022-07-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BarcelonaPop_ZH-CN3687855585_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-11 21:43:43
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BarcelonaPop_ZH-CN3687855585_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [被网红营销遗忘的婴儿潮一代](https://www.woshipm.com/marketing/5521777.html)
> 概要: 编辑导语：现在的网红营销对象似乎都是围绕着年轻人，俗称Z世代或千禧一代，很少有人注意到婴儿潮一代。这篇文章作者介绍了关于婴儿潮一代的营销案例以及误解，感兴趣的小伙伴一起来看看吧~当您想到网红营销时，首......
### [00后拒绝了你的好友申请](https://www.woshipm.com/it/5521795.html)
> 概要: 编辑导语：相对于80后、90后，00后在群体价值观、社交习惯上演化出了不同的趋势。本篇文章围绕现在的00后社交展开了一系列的讨论，感兴趣的小伙伴们快来一起看看吧~“孤独的人是可耻的”，张楚在 1994......
### [管理：激发他人的热爱去达成你们共同目标](https://www.woshipm.com/zhichang/5521626.html)
> 概要: 编辑导语：在职场上，专业各有差别，不同领域对应不同技能包。而管理却有着共同的底层规律，哪怕行业不同、岗位不同，对应的管理底层逻辑都有相通之处。那么，管理到底是什么呢？本文作者系统地分享了对管理的见解，......
### [小红书终成字节心病](https://www.huxiu.com/article/601782.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国意气风发如字节跳动，却在三天前（ 7 月 8 日）悄咪咪上线了一款主攻种草赛道的独立 App “可颂”。事实上，自 2018 年上线种草 App “新草”......
### [Running an Open Source Home Area Network](https://xn--gckvb8fzb.com/running-an-open-source-home-area-network/)
> 概要: Running an Open Source Home Area Network
### [《电锯人》第二部连载预告图 恶魔猎人电次再度来袭](https://acg.gamersky.com/news/202207/1498286.shtml)
> 概要: 官方于近日公布了藤本树《电锯人》漫画第二部的杂志宣传页，该部漫画将于7月13日正式开启连载。
### [罗永浩：不看好苹果AR，新世界的霸主永远不是之前旧世界的霸主](https://finance.sina.com.cn/tech/2022-07-11/doc-imizmscv0965100.shtml)
> 概要: IT之家7月11日消息，昨日，罗永浩在交个朋友直播间官宣了其新创业公司ThinRedLine（细红线），目标为AR领域。同时，罗永浩在跟网友聊天中谈到了苹果AR......
### [向雅萌、ulike泼脏水，TriPollar初普美容仪中国代理商判刑7个月](http://www.investorscn.com/2022/07/11/101745/)
> 概要: 最近小家电圈很热闹，前有家用脱毛仪品牌Ulike发布声明称网络侵权案胜诉。后有家用美容仪品牌雅萌发布声明称，南京小鲸鲨信息科技有限公司（“TriPollar初普”中国代理商）及其相关负责人雇佣“网络水......
### [漫改电视动画「孤独摇滚！」公开最新主视觉图](http://acg.178.com/202207/451503614561.html)
> 概要: 近日，音乐题材漫改电视动画「孤独摇滚！」公开了最新的主视觉图，本作预计将于2022年10月正式开播。STAFF原作：はまじあき監督：斎藤圭一郎系列构成・脚本：吉田恵里香人设：けろりら制作：Clover......
### [长城人寿“以爱之光 极致陪伴”第十七届客户服务节盛大开幕](http://www.investorscn.com/2022/07/11/101746/)
> 概要: 7月10日，正值第十个“全国保险公众宣传日”之际，为了倾听客户心声，增进信赖与成长，长城人寿正式启动“第十七届客户服务节”，此次客服节主题为“以爱之光 极致陪伴”，活动围绕产品推广、服务升级、健康养老......
### [《泰拉瑞亚》公布图画小说系列 今年Q3推出第一期](https://www.3dmgame.com/news/202207/3846662.html)
> 概要: 《泰拉瑞亚》开发商Re-Logic宣布将联合50 Amp Productions推出泰拉瑞亚图画小说系列，其中第一册即将完成，官方还公布了一张概念封面图。泰拉瑞亚图画小说系列每本书将包含四个部分，这张......
### [《吸血鬼崛起》人称变换MOD 游戏体验完全刷新](https://www.3dmgame.com/news/202207/3846667.html)
> 概要: 近期好评游戏《吸血鬼崛起》一款新MOD《ModernCamera》日前推出，可以自由切换第一/第三人称视角，据开发者表示将完全刷新玩家的游戏体验，感兴趣的玩家可以关注下了。•《吸血鬼崛起》人称变换MO......
### [webpack 拆包：关于 splitChunks 的几个重点属性解析](https://www.tuicool.com/articles/UNNNJbe)
> 概要: webpack 拆包：关于 splitChunks 的几个重点属性解析
### [TV动画《孤独摇滚！》10月开播](https://news.dmzj.com/article/74901.html)
> 概要: TV动画《孤独摇滚》宣布了将于10月开始播出的消息。一张本作的角色宣传图也一并公开。
### [P站美图推荐——高跟鞋特辑（二）](https://news.dmzj.com/article/74902.html)
> 概要: 该怎么去形容你最贴切……
### [「组长女儿与照料专员」最新杂志彩页公开](http://acg.178.com/202207/451510315871.html)
> 概要: 电视动画「组长女儿与照料专员」公开了全新的杂志彩页，本作已于7月7日开始播出。STAFF原作：つきや(コミックELMO「組長娘と世話係」/マイクロマガジン社刊)导演：川崎逸朗系列构成：大知慶一郎角色设......
### [美食摄影/面包烘焙/餐饮食品食物蛋糕法棍贝果牛角拍摄](https://www.zcool.com.cn/work/ZNjA4NDc4OTY=.html)
> 概要: 品味美食，从“视觉”开始，用视觉...激发美食 “ 引销力 ”...-BIG 商业美食摄影，用心拍好每一道美食。-点击头像，查看联系方式。-注：不同显示器观看会有色差，以苹果显示器为准......
### [潮玩|AFTER ASH归星曲小队【科幻可动人偶】](https://www.zcool.com.cn/work/ZNjA4MTQ0NzI=.html)
> 概要: 为了开采D0497号星球的稀有矿物资源-火釉，人类建立了宇宙殖民都市“烬城”——因开采火釉时天空漂浮红色的灰尘而得名；长期暴露在灰尘中会使人的身体基因组被破坏，无法被治疗，俗称“釉毒”；如今，在资源即......
### [萌德因为心理健康问题 推迟世界巡回演唱会](https://ent.sina.com.cn/y/youmei/2022-07-11/doc-imizirav2847781.shtml)
> 概要: 新浪娱乐讯  据外媒报道，流行明星肖恩·门德斯（Sean Shawn Mendes）突然一脚刹车，靠边停下正高速行进中的世界巡唱大巴。　　上周五（7月8日），23岁的萌德告诉他的Instagram粉丝......
### [《海贼王》新剧场版最新海报 IMAX版8月6日同步上映](https://acg.gamersky.com/news/202207/1498359.shtml)
> 概要: 今天《海贼王》新剧场版《ONE PIECE FILM RED》发布了全新海报，官方宣布8月6日将同步上映IMAX版。
### [《名侦探柯南》1049话作画大崩坏 角色频频“变脸”](https://acg.gamersky.com/news/202207/1498364.shtml)
> 概要: 7月9日，在日本播出的《名侦探柯南》第1049话引爆了热烈议论，不过并不是因为剧情特别引人入胜，而是这一集的作画从头崩到尾。
### [认养一头牛 2022年618视觉设计分享](https://www.zcool.com.cn/work/ZNjA4NTE4NDQ=.html)
> 概要: 认养一头牛，一家为用户养牛的公司我们始终坚信，奶牛养得好，牛奶才会好我们始终坚持用户第一，做家人笑容的守护者2022年，认养一头牛与新华社联名，共同打造“牛奶新青年”的品牌内核，我们有一批真诚的养牛人......
### [TV动画《Dr.STONE》第三季来春播出](https://news.dmzj.com/article/74903.html)
> 概要: 根据稲垣理一郎、Boichi原作制作的TV动画《Dr.STONE》宣布了第三季名称为《Dr.STONE NEW WORLD》，于2023年春开播。
### [「海贼王」公开新作剧场版动画IMAX版视觉图](http://acg.178.com/202207/451517324137.html)
> 概要: 「海贼王」公开了新作剧场版动画「ONE PIECE FILM RED」的全新IMAX版视觉图，绘制了在晴天中歌唱的乌塔和在雨中站立的香克斯，以及十名身穿战斗服的草帽团成员。STAFF原作・综合制作人：......
### [「排球少年」×东武动物公园2022联动活动视觉图公开](http://acg.178.com/202207/451517596953.html)
> 概要: 近日，「排球少年」宣布将与东武动物公园展开联动，并公开了联动视觉图。「排球少年」是在「周刊少年Jump」2012年2月20日至2020年7月13日连载的漫画作品，讲述了男主角日向翔阳为了实现成为“小巨......
### [燃力士新锐功能饮料电商策略全案](https://www.zcool.com.cn/work/ZNjA4NTM0MDQ=.html)
> 概要: 燃力士（CELSIUS），是一款来自美国的中高端能量饮料，曾获得李嘉诚、邓紫棋等多项投资，截至目前荣获18项国际级奖项。2017年6月燃力士在美国纳斯达克挂牌上市。西九里帮助燃力士打造独特的新媒体沟通......
### [客服回应中国广电卡无法激活：系统升级原因，可退款](https://finance.sina.com.cn/tech/2022-07-11/doc-imizmscv1014727.shtml)
> 概要: IT之家7月11日消息，6月27日，中国广电启动5G网络服务，开启试商用，且开启了选号服务，意味着作为第四大电信运营商，中国广电正式面向5G用户提供网络服务......
### [恐怖冒险游戏《工房库：噩梦历险记》现已发售](https://www.3dmgame.com/news/202207/3846684.html)
> 概要: 90年代末超现实恐怖冒险游戏复刻版《GARAGE工房库：噩梦历险记》现已在Steam推出，售价70元，支持中文。精神治疗装置「工房库」让主角陷入了奇异的世界，扮演主角的你能夠顺利逃脱吗？游戏Steam......
### [马斯克回应：卖不卖都是你！](https://finance.sina.com.cn/tech/2022-07-11/doc-imizirav2869879.shtml)
> 概要: 新浪科技讯 北京时间7月11日下午消息，据报道，针对Twitter聘请律师准备起诉特斯拉CEO埃隆·马斯克（Elon Musk），马斯克今日以一种幽默的方式进行了回应......
### [从科幻到现实，聚焦脑机接口的核心技术与突破](http://www.investorscn.com/2022/07/11/101752/)
> 概要: 近年来，世界各国都将脑科学研究列入重点规划，美国政府与欧盟在2013年先后启动各自的“脑计划”，重点开展脑科学研究。中国“脑计划”正式诞生于2016年3月发布的“十三五”规划纲要，将“脑科学与类脑研究......
### [恒大物业今日召开内部会议 派发恒驰5销售考核指标](https://finance.sina.com.cn/tech/2022-07-11/doc-imizirav2871414.shtml)
> 概要: 记者 陈业......
### [How to Do Nothing with Nobody All Alone by Yourself (2014)](https://www.themarginalian.org/2014/10/24/how-to-do-nothing-with-nobody-all-alone-by-yourself/)
> 概要: How to Do Nothing with Nobody All Alone by Yourself (2014)
### [暗区突围上线倒计时2天！同名CG宣传片预告曝光](https://www.3dmgame.com/news/202207/3846689.html)
> 概要: 距离腾讯自主研发的真硬核射击手游《暗区突围》上线还有2天！《暗区突围》凭借充满细节的沉浸战场打造及“带物资突围”的差异化玩法，给予玩家沉浸的射击体验及战术竞技的博弈乐趣。7月13日全平台上线在即，游戏......
### [小S称大S支持其女儿变美 透露曾电话报告妈妈怀孕](https://ent.sina.com.cn/s/h/2022-07-11/doc-imizmscv1025475.shtml)
> 概要: 新浪娱乐讯 据台媒11日报道，小S以直率幽默作风闻名，深受大众喜爱，她在2005年与许雅钧结婚，婚后生了三个女儿，她不时透过社交平台分享家庭生活，可以发现三个孩子有着完全不一样的个性与风格，近日小S与......
### [腾讯 ROG 游戏手机 6 / Pro 明日发售：骁龙 8 + 加持，3999 元起](https://www.ithome.com/0/628/972.htm)
> 概要: IT之家7 月 11 日消息，腾讯 ROG 游戏手机 6 系列于 7 月 5 日正式发布，搭载了高通骁龙 8+Gen 1 处理器，采用矩阵式液冷散热 6.0，AirTrigger 肩键，个性视窗，搭载......
### [The Problem with RISC-V V Mask Bits](https://www.computerenhance.com/p/the-problem-with-risc-v-v-mask-bits)
> 概要: The Problem with RISC-V V Mask Bits
### [奇瑞 QQ 无界 Pro 开启预售：高通 6155 芯片 / 最长续航 408km，7.99 万元起](https://www.ithome.com/0/628/993.htm)
> 概要: IT之家7 月 11 日消息，据奇瑞官方消息，无界 Pro 现已开启预售，价格 7.99 万元起，即将于 8 月上市。IT之家了解到，作为 iCar 生态全新旗舰车型，无界 Pro 长宽高为 3402......
### [日本网友使用树叶制作《机动战士高达》系列中的夏亚](https://news.dmzj.com/article/74908.html)
> 概要: 近日，日本网友鸠野高嗣使用叶子制作的《机动战士高达》系列中的夏亚受到了不少网友的关注。从图中可以看出来，再现的是动画第10话中的场景。
### [如何走出“低生育率陷阱”？](https://www.huxiu.com/article/604940.html)
> 概要: 2022年7月11日，是第33个世界人口日，联合国人口基金将主题确定为“80亿人的世界：迈向有弹性的未来——抓住机遇，确保人人拥有权利和选择”。《国家卫生健康委办公厅关于组织开展2022年世界人口日宣......
### [钟薛高，投冷柜这个钱可能真省不了](https://www.tuicool.com/articles/qyAbyam)
> 概要: 钟薛高，投冷柜这个钱可能真省不了
### [Show HN: Colorvote.io – ranking all 16,777,216 sRGB web colors by popularity](https://www.colorvote.io)
> 概要: Show HN: Colorvote.io – ranking all 16,777,216 sRGB web colors by popularity
### [鲁大师发布上半年电脑排行：AMD 3995WX CPU 性能第一，RTX 3090 Ti 显卡性能第一](https://www.ithome.com/0/629/000.htm)
> 概要: IT之家7 月 11 日消息，今天，鲁大师发布了 2022 半年报电脑排行。首先。在台式机 CPU 性能榜上，第一名依然被 AMD Ryzen Threadripper Pro 3995WX 占据，前......
### [你真的了解霍乱吗？](https://www.huxiu.com/article/604978.html)
> 概要: 出品丨虎嗅医疗组作者丨苏北佛楼蜜题图丨视觉中国7月11日，湖北省武汉市武昌区卫生健康局在“大成武昌”发布《关于武汉大学一例霍乱病例处置情况的通报》。通报指出，7月9日晚，武昌区疾控中心接医院报告，武汉......
### [游戏王作者高桥和希死因为溺水 刑事案件可能性较低](https://acg.gamersky.com/news/202207/1498450.shtml)
> 概要: 日本冲绳县名护市保安署在今天（7月11日）正式公布了高桥和希的死因，根据司法解剖结果显示，高桥和希死因为溺死，刑事案件可能性较低。
### [《谭谈交通》全网下架，谭sir面临千万索赔？](https://www.huxiu.com/article/605012.html)
> 概要: 本文来自微信公众号：中新经纬 （ID：jwview），作者：常涛，编辑：雷宗润，审校：罗琨，原文标题：《〈谭谈交通〉突遭全网下架！涉事公司成立四年，还起诉过这些巨头》，题图来自：视觉中国近日，知名交通......
### [《大话降龙》](https://new.qq.com/omn/20220711/20220711A07W3Z00.html)
> 概要: 太白打算在天宫宝库拿几件上古宝物回去研究，恰巧碰到降龙，太白还能顺利拿到宝物吗？当然不会，太白照拿无误。看到好吃嘎嘣豆就往嘴里送，嘎嘣嘎嘣真好吃，吃完就噗呲！没直接把降龙熏晕了，倒是拉出了一个屁精！玉......
### [浅论国内数字藏品的技术安全问题](https://www.tuicool.com/articles/26FriuY)
> 概要: 浅论国内数字藏品的技术安全问题
### [敢对水门大打出手，面对鼬的时候，带土为什么那么谨慎？](https://new.qq.com/omn/20220711/20220711A086UI00.html)
> 概要: 动漫火影忍者中，宇智波一族可以说是整部动漫中最强的一个种族，但是宇智波一族却经历了一场非常大的浩劫，在那个夜晚，宇智波一族只剩下了两个已知的幸存者，但是没有人知道，其实宇智波一族还有另外一个幸存者，那......
### [面对日向一族的白眼，团藏就没有想法吗？](https://new.qq.com/omn/20220711/20220711A086Q500.html)
> 概要: 动漫《火影忍者》中，瞳术是一种非常特殊的力量，而作为瞳术中的代表，轮回眼，写轮眼和白眼可以说是瞳术中最强的存在，而这三种瞳术也都来自于一个人，查克拉之祖，大筒木辉夜。            虽然都是来......
### [高能电玩节再度来袭！游戏、硬件新品首发情报](https://www.3dmgame.com/news/202207/3846705.html)
> 概要: 新一期哔哩哔哩“高能电玩节”将于7月15日至8月15日期间举办，SIE、Square Enix、世嘉、Gamera Game、bilibili游戏、帕斯亚科技、轻语游戏等30余家游戏厂商将会参展，还有......
### [情绪趋谨慎，资金连续撤离新能源赛道股丨复盘论](https://www.yicai.com/video/101470388.html)
> 概要: 情绪趋谨慎，资金连续撤离新能源赛道股丨复盘论
### [夏联观察之勇士](https://bbs.hupu.com/54690745.html)
> 概要: 大聪明不是第一天打NBA，不过从去年4月11日至今，1年零3个月的时间里，他都没有正式在比赛中摸过球。三年级的榜眼来打夏季联赛，临场经验还未必赶得上各种联赛历练出的小球痞，勇士夏联又没个正经组织后卫，
### [债务危机化解耗时20个月！紫光集团称已完成股权交割](https://www.yicai.com/news/101470646.html)
> 概要: 从时间点上看，从危机化解工作到上述紫光集团的股权交割完成整整持续了20个月。
### [三亚消费券催热手机消费市场 256G版iPhone 13京东到手价只需5199元](http://www.investorscn.com/2022/07/11/101755/)
> 概要: 伴随暑期的到来，手机消费市场也将迎来新一轮的“放价”！7月11日起，由三亚市政府主办、市商务局、市旅文局、市旅游推广局承办的“助商惠民 乐享海南”三亚政府消费券互动第二季正式启动。用户可在京东App首......
### [贵上天的雪糕刺客，赶紧退退退](https://www.tuicool.com/articles/6n2INbi)
> 概要: 贵上天的雪糕刺客，赶紧退退退
### [余承东一语道破：华为小康紧密合作的原因](http://www.investorscn.com/2022/07/11/101757/)
> 概要: 日前，乘联会公布2022年6月新能源SUV销量榜，问界M5以7021的月销辆位列第8名，这是问界M5自今年3月份交付以来，连续3个月持续在新能源SUV销量排行榜前十名。在获得了市场的充分认可下，问界系......
### [曝苹果 AR / VR 设备 Micro OLED 面板将由三星显示供货](https://www.ithome.com/0/629/027.htm)
> 概要: IT之家7 月 11 日消息，一份新报告称，苹果已要求三星显示（Samsung Display ）开发 Micro OLED 面板，以在明年推出的 AR / VR 头戴式设备之前实现供应多元化。据 T......
### [6100万成交？张艺谋无锡豪宅出售引关注，其妻：没有急售](https://www.yicai.com/news/101470707.html)
> 概要: 这套房产或将带来超千万的收益。
### [销售低谷叠加偿债高峰，下半年仍需警惕房企违约风险](https://www.yicai.com/news/101470701.html)
> 概要: 违约事件频发，将继续影响房地产销售、投资、新开工、土地购置等方面预期。
### [杰威尔声明称未授权周杰伦音乐作品作为游戏赠品](https://ent.sina.com.cn/y/yneidi/2022-07-11/doc-imizmscv1067358.shtml)
> 概要: 新浪娱乐讯 11日，杰威尔在微博发表声明，回应近日网络游戏《天下3》推出活动赠送周董最新数字专辑《最伟大的作品》以及周董演唱会门票之事，杰威尔强调：“本公司从未授权该款游戏使用周杰伦之音乐作品，亦无授......
### [对话中金龙亮：科创板三周年，后备企业哪里来？](https://www.yicai.com/news/101470755.html)
> 概要: 过去三年上市的主要是存量，未来会迎来一批受科创板鼓舞而创立、受各路资本加持而壮大的科技企业。
### [大宗商品价格回落原材料成本压力缓解 中下游民营企业利润有望获得改善](https://finance.sina.com.cn/china/2022-07-11/doc-imizirav2925699.shtml)
> 概要: 21世纪经济报道见习记者 唐婧 北京报道 “公司上游原材料包含铜、铝等金属原料，相关大宗商品价格走势对于公司金属原材料采购价格有一定影响...
### [有侨胞在利比里亚一机场遭勒索小费，中使馆：已同负责人交涉，对方承诺将严禁此类行为](https://finance.sina.com.cn/jjxw/2022-07-11/doc-imizirav2925291.shtml)
> 概要: 原标题 有侨胞在利比里亚一机场遭勒索小费，中使馆：已同负责人交涉，对方承诺将严禁此类行为 据中国驻利比亚大使馆网站7月11日消息，近期...
### [港澳特区因台湾芒果外包装检出阳性而采取停售等措施 国台办回应](https://finance.sina.com.cn/china/gncj/2022-07-11/doc-imizmscv1072208.shtml)
> 概要: 原标题 港澳特区因台湾芒果外包装检出阳性而采取停售等措施 国台办回应 据海外网7月11日电 国台办发言人朱凤莲11日就涉台热点话题答记者问。
### [6月金融数据出炉：M2增速创5年半以来新高 专家预计短期内金融数据仍有望延续偏强走势](https://finance.sina.com.cn/china/gncj/2022-07-11/doc-imizirav2926238.shtml)
> 概要: 原标题 6月金融数据出炉：M2增速创5年半以来新高 专家预计短期内金融数据仍有望延续偏强走势 每经记者 肖世清每经编辑 陈旭 7月11日，央行发布2022年6月金融统计数据...
### [为协助四姐妹成为精灵女王，男主每天要对她们恶作剧](https://new.qq.com/omn/20220711/20220711A0B1U900.html)
> 概要: 精灵在动漫中是很常见的角色。一方面，她们有着区别于普通人的尖尖的耳朵；另一方面，明明有着上百岁的年龄，实际看起来却和十几岁的少女无异。这也使得精灵角色很受观众们的欢迎，并几乎成为异世界题材里必不可少的......
### [JR热议能抗能打状态正佳，怎么评价今晚369的发挥？](https://bbs.hupu.com/54692840.html)
> 概要: ﻿只能说现在的jdg还是团队配合不错的，369都成长可以说是非常明显。前两盘的英雄酒桶纳尔也是很想证明自己的，最后的自信老司机也是贼肉啊。
### [“进过方舱的不要、阳过的不要”？上海回应新冠康复者求职被歧视：应一视同仁对待，不贴标签不设槛](https://finance.sina.com.cn/jjxw/2022-07-11/doc-imizirav2928171.shtml)
> 概要: 原标题 “进过方舱的不要、阳过的不要”？上海回应新冠康复者求职被歧视：应一视同仁对待，不贴标签不设槛 近日，有媒体报道称，部分新冠肺炎康复者在求职时屡屡碰壁...
### [流言板JDG祝贺Hope联赛100胜：追风赶月莫停留，平芜尽处是春山](https://bbs.hupu.com/54692982.html)
> 概要: 虎扑07月11日讯 JDG祝贺Hope联赛100胜，以下是微博原文：恭喜JDG.Hope成功解锁联赛100胜成就追风赶月莫停留，平芜尽处是春山，继续加油！在今日JDG对阵RA的比赛中，Hope使用厄斐
### [卧谈会你有过去看心理医生的想法吗？](https://bbs.hupu.com/54693056.html)
> 概要: 兄弟们，昨天下午去看了一下心理医生，第一次去就感觉很爽，把很多堆积在心里很久的事情、积攒了很久的情绪向心理医生分享了出来。虽然问题可能还要去几次才能解决，但第一次敞开心扉分享出这些事情，感觉整个人的状
# 小说
### [阿尔法星球大毁灭](http://book.zongheng.com/book/1123911.html)
> 作者：枫叶青岩

> 标签：科幻游戏

> 简介：一个大学生被劫往阿尔法星，经历了与外星人恋爱和其它种种奇遇；目睹了那里生态环境的破坏、为争夺一个绿色海岛而发生的战争，以及由此引发的大毁灭。

> 章节末：第七十三章：后记

> 状态：完本
# 论文
### [Bayesian learning of effective chemical master equations in crowded intracellular conditions | Papers With Code](https://paperswithcode.com/paper/bayesian-learning-of-effective-chemical)
> 概要: Biochemical reactions inside living cells often occur in the presence of crowders -- molecules that do not participate in the reactions but influence the reaction rates through excluded volume effects. However the standard approach to modelling stochastic intracellular reaction kinetics is based on the chemical master equation (CME) whose propensities are derived assuming no crowding effects. Here, we propose a machine learning strategy based on Bayesian Optimisation utilising synthetic data obtained from spatial cellular automata (CA) simulations (that explicitly model volume-exclusion effects) to learn effective propensity functions for CMEs. The predictions from a small CA training data set can then be extended to the whole range of parameter space describing physiologically relevant levels of crowding by means of Gaussian Process regression. We demonstrate the method on an enzyme-catalyzed reaction and a genetic feedback loop, showing good agreement between the time-dependent distributions of molecule numbers predicted by the effective CME and CA simulations.
### [Leverage, Influence, and the Jackknife in Clustered Regression Models: Reliable Inference Using summclust | Papers With Code](https://paperswithcode.com/paper/leverage-influence-and-the-jackknife-in)
> 概要: Cluster-robust inference is widely used in modern empirical work in economics and many other disciplines. When data are clustered, the key unit of observation is the cluster. We propose measures of "high-leverage" clusters and "influential" clusters for linear regression models. The measures of leverage and partial leverage, and functions of them, can be used as diagnostic tools to identify datasets and regression designs in which cluster-robust inference is likely to be challenging. The measures of influence can provide valuable information about how the results depend on the data in the various clusters. We also show how to calculate two jackknife variance matrix estimators, CV3 and CV3J, as a byproduct of our other computations. All these quantities, including the jackknife variance estimators, are computed in a new Stata package called summclust that summarizes the cluster structure of a dataset.
