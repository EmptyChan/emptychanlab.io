---
title: 2021-05-22-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CapeofTossa_EN-CN5117176069_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-22 22:30:30
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CapeofTossa_EN-CN5117176069_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [微软开源 SimuLand，用于模拟网络攻击的测试实验室](https://www.oschina.net/news/142669/microsoft-simuland-simulated-cyberattacks)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>微软宣布推出一项 SimuLand 开源计划，旨在帮助世界各地的安全研究人员部署实验室环境，再现真实攻击场景中使用的知名技术；以及积极测......
### [红旗 Linux 桌面操作系统 v11 社区预览版 (0521) 更新](https://www.oschina.net/news/142663/redflag-linux-v11-0521-update)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>红旗 Linux 桌面操作系统 v11 社区预览版发布了 (0521) 更新。更新日志：较上一版本，红旗Linux桌面操作系统v11社区......
### [Apache Arrow 4.0.0 发布，内存数据交换格式](https://www.oschina.net/news/142662/apache-arrow-4-0-0-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Apache Arrow 4.0.0 现已发布，该版本涵盖了 3 个月的开发工作，包括来自 114 个不同贡献者的 711 个已解决的问......
### [PostgreSQL 14 Beta 发布，带来更多性能改进](https://www.oschina.net/news/142660/postgresql-14-beta-1-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>PostgreSQL 14 首个 Beta 版本发布，PostgreSQL 的开发者持续改进这个流行的开源 SQL 数据库服务器，使其性......
### [四十年来，那些被中国人鸡过的娃](https://www.huxiu.com/article/429699.html)
> 概要: 本文来自微信公众号：8字路口（ID：crosseight），作者：黄阿华，原文标题：《四十年来，那些被中国人鸡过的娃》，头图来自：视觉中国这是个晴朗的下午，阳光柔和地穿过落地窗，照进了一幢花园洋房。室......
### [曹县爆火背后，是大城市无处安放的乡愁](https://www.huxiu.com/article/429698.html)
> 概要: 本文来自微信公众号：20社（ID：quancaijing_20she），作者：马程，编辑：王晓玲，原文标题：《曹县爆火背后，是大城市无处安放的乡愁》，题图：视觉中国“有人在黑我大曹县。”刘飞说，这是他......
### [TV动画「世界最强暗杀者转生成异世界贵族」先导PV公开](http://acg.178.com/202105/415650834034.html)
> 概要: TV动画「世界最强暗杀者转生成异世界贵族」公开了先导PV，本作将于2021年10月开始播出。TV动画「世界最强暗杀者转生成异世界贵族」先导PVCASTルーグ：赤羽根健治ディア：上田麗奈タルト：高田憂希......
### [《守望先锋》首席角色设计师宣布离职 回忆团队过往](https://ol.3dmgame.com/news/202105/31135.html)
> 概要: 《守望先锋》首席角色设计师宣布离职 回忆团队过往
### [“杀死”那个中国神童](https://www.huxiu.com/article/429701.html)
> 概要: 本文来自微信公众号：摩登中产（ID：modernstory），作者：摩登中产，原文标题：《“杀死”那个中国神童》，题图来自：视觉中国一1977年冬天，纽约一家报纸收到消息：中国正在大范围选拔儿童，原因......
### [《半条命：Alyx》敌人原本可以识别玩家竖中指](https://www.3dmgame.com/news/202105/3814971.html)
> 概要: 《半条命：Alyx》在2020年上市之后，收获了来自玩家和媒体的一致褒奖，成为史上最佳的VR游戏之一。这款游戏中，玩家和场景的互动级别达到了空前程度，但在最终版里还是没有实现开发人员的所有设想。每款电......
### [电视动画「美妙世界」Blu-ray&DVD下卷封面插图公开](http://acg.178.com/202105/415652709455.html)
> 概要: 电视动画「美妙世界」公开了Blu-ray&DVD下卷封面插图，本商品将于2021年8月27日发售。 本系列商品Blu-ray售价为22,000日元（含税），DVD售价为19,800日元。封入特典为小册......
### [「魔道祖师」完结篇人物海报更新](http://acg.178.com/202105/415652956916.html)
> 概要: 改编自墨香铜臭创作的同名小说的国产动画「魔道祖师」官方更新了完结篇新人物海报，本次海报更新的人物是金子轩和江厌离。「魔道祖师」讲述了夷陵老祖魏无羡重生归来，与前世挚友蓝忘机重逢后，共同探寻前世真相的故......
### [BATJ的金融高管们，正在「默契」回归传统机构](https://www.tuicool.com/articles/NnquEjj)
> 概要: “五年前，那些有头有脸的互联网公司，给部分银行高管开出的薪资，至少是老东家的两倍甚至三倍以上。市场非常疯狂。”某业内人士向《AI金融评论》讲述道。那时候离钱、离风险最近的行业，分两极。一极是所有关于互......
### [「神奇女侠：黑与金」第一期变体封面公开](http://acg.178.com/202105/415654402996.html)
> 概要: 近日，画师Warren Louw公开了一张由其绘制的神奇女侠的个人单刊「神奇女侠：黑与金」（Wonder Woman Black & Gold）的第一期变体封面图。「神奇女侠」是美国DC漫画公司旗下的......
### [原力计划 跟我学Kafka:如何高效运维之主题篇 kafka系列第一篇，蕴含了笔者的学习方法，该系列将见...](https://www.tuicool.com/articles/A3ua2in)
> 概要: 摘要：对Kafka-topics命令详解，并从运维角度窥探Kafka关于topic的运作机制。作为一个Kafka初学者，需要快速成长，承担维护公司Kafka的重任，对Kafka的学习，我按照三步走策略......
### [How to Tell a Job Offer from an ID Theft Trap](https://krebsonsecurity.com/2021/05/how-to-tell-a-job-offer-from-an-id-theft-trap/)
> 概要: How to Tell a Job Offer from an ID Theft Trap
### [苹果 iPod Touch 2021 渲染图流出：线条硬朗，或秋季发布](https://www.ithome.com/0/552/949.htm)
> 概要: IT之家 5 月 22 日消息 外媒 MacRumors 的撰稿人 Steve Moser 表示，苹果新 iPod Touch 将会在今年秋季推出，以纪念 iPod 家族成立 20 周年。同时，Mos......
### [袁隆平逝世，回顾中国杂交水稻发展史](https://www.huxiu.com/article/397731.html)
> 概要: 虎嗅注：新华社消息，“杂交水稻之父”、中国工程院院士、“共和国勋章”获得者袁隆平，5月22日13点07分在湖南长沙逝世，享年91岁。袁隆平是我国研究与发展杂交水稻的开创者，也是世界上第一个成功利用水稻......
### [NNCP: Lossless Data Compression with Neural Networks (2019)](https://bellard.org/nncp/)
> 概要: NNCP: Lossless Data Compression with Neural Networks (2019)
### [苹果证实：HomePod/mini 将通过软件更新支持 Apple Music 无损音频](https://www.ithome.com/0/552/952.htm)
> 概要: IT之家5 月 22 日消息 根据苹果最新公布的苹果支持文件，HomePod 和 HomePod mini 将在未来的软件更新中获得对播放 Apple Music 无损音频的支持。初期，HomePod......
### [组图：央视六一晚会录制路透释出 宋亚轩严浩翔贺峻霖低调现身](http://slide.ent.sina.com.cn/star/slide_4_86512_356943.html)
> 概要: 组图：央视六一晚会录制路透释出 宋亚轩严浩翔贺峻霖低调现身
### [“逃出”实验室的比格们，重新开始一段“狗生”](https://www.tuicool.com/articles/EFnIVvj)
> 概要: 本文来自微信公众号：液态价值（ID：liquidvalue），作者：高敏，原文标题：《那些“逃出”实验室的狗狗，和它们重新开始的“狗生”》，头图来自：视觉中国从苏北跋涉了几百公里后，1273到了北京......
### [Robot paramedic carries out CPR in ambulance in UK first](https://eandt.theiet.org/content/articles/2021/05/robot-paramedic-carries-out-cpr-in-ambulance-in-uk-first/)
> 概要: Robot paramedic carries out CPR in ambulance in UK first
### [组图：钟丽缇二女儿穿露脐装跳舞 动作利落女团范十足](http://slide.ent.sina.com.cn/star/slide_4_86512_356948.html)
> 概要: 组图：钟丽缇二女儿穿露脐装跳舞 动作利落女团范十足
### [5199/5899 元，惠普战 X 锐龙版上新：R5 5600U、薯条标](https://www.ithome.com/0/552/968.htm)
> 概要: IT之家5 月 22 日消息 感谢IT之家网友热心线索投递，新款惠普战 X 笔记本电脑更新，三方店铺提供了 13/14/15 寸版本但官方店似乎只有 14/15.6 英寸版本，搭载 AMD R5 56......
### [组图：太甜！李艾高扎马尾笑眼弯弯 轻偎老公十指相扣甜skr人](http://slide.ent.sina.com.cn/star/slide_4_86448_356954.html)
> 概要: 组图：太甜！李艾高扎马尾笑眼弯弯 轻偎老公十指相扣甜skr人
### [组图：国士无双一路走好！袁隆平逝世享年91岁 众星发文缅怀](http://slide.ent.sina.com.cn/star/slide_4_704_356957.html)
> 概要: 组图：国士无双一路走好！袁隆平逝世享年91岁 众星发文缅怀
### [近日NBS内盘挖矿所得增量50%](https://www.btc126.com//view/167238.html)
> 概要: NBS官方消息称：最近大盘暴跌并伴有熊市来临的气息，但是NBS内盘挖矿收获反而越多，上周10000CNY每日挖矿所得NCN400枚，昨日所得升至600枚。NBS表示，“熊市参与挖矿可以为将来的上涨做好......
### [芒果台又一部扶贫剧火了，收视率遥遥领先，靳东抗剧能力真没话说](https://new.qq.com/omn/20210522/20210522A096J000.html)
> 概要: 2020年，是我国脱贫攻坚决胜的一年，在这一年实现了消除我国绝对贫困的成绩，全国人民生活水平实现了小康之家。为了庆祝与响应国家脱贫攻坚的号召，从2020年起，影视行业掀起了一股扶贫剧之风。有最火的《山......
### [Plotinus: A searchable command palette in every modern GTK+ application](https://github.com/p-e-w/plotinus)
> 概要: Plotinus: A searchable command palette in every modern GTK+ application
### [《快本》今晚停播 湖南卫视改播电影《袁隆平》](https://ent.sina.com.cn/tv/zy/2021-05-22/doc-ikmxzfmm4040439.shtml)
> 概要: 新浪娱乐讯 5月22日，“杂交水稻之父”、中国工程院院士、“共和国勋章”获得者袁隆平在湖南长沙逝世，享年91岁。湖南卫视随后宣布调整节目编排，电视剧《温暖的味道》及《快乐大本营》停播，20：00改为播......
### [BTC五分钟内上涨1.48%，现报$38,271.33](https://www.btc126.com//view/167242.html)
> 概要: BTC五分钟内上涨1.48%，上涨金额为559.33美元，其中欧易OKEx上现价为$38,271.33，请密切关注行情走向，注意控制风险。更多实时行情异动提醒，快在APP内添加自选，开启智能盯盘，快人......
### [KuCoin (库币)平台KLV短线拉升，日内上涨11.73%](https://www.btc126.com//view/167245.html)
> 概要: 据KuCoin(库币)平台行情显示，KLV短线持续拉升，最高触及0.08美元，目前价格为0.078美元，24小时涨幅达11.73%。近期行情波动较大，请做好风险控制......
### [《速度与激情9》国内票房超5亿 豆瓣评分仅5.6分](https://www.3dmgame.com/news/202105/3814989.html)
> 概要: 今天中午环球影业发布消息，昨日上映的《速度与激情9》电影，在中国内地的票房超过了5亿。然而影片的口碑似乎没有赶上急剧增长的票房。截止到目前，《速度与激情9》在豆瓣上的评分仅5.6分，相比来说2017年......
### [OpenSea接受加密艺术家Pak发行的ASH代币支付](https://www.btc126.com//view/167247.html)
> 概要: 官方消息，OpenSea宣布接受ASH支付。ASH代币由加密艺术家Pak发行......
### [吴磊健身有成效，手臂线条很好看，胸肌更有魅力](https://new.qq.com/omn/20210522/20210522A09ZIB00.html)
> 概要: 吴磊是名副其实的小童星，出道很早，而且也已经演了很多的影视剧作品，现在的吴磊是一名大学生，他的演技还是非常不错的，是很多新生代演员的榜样。吴磊不仅有演技，而且外在形象也非常不错。在娱乐圈发展，要磨练演......
### [荣耀赵明：目前搭载安卓 鸿蒙做得好或考虑采用](https://www.3dmgame.com/news/202105/3814990.html)
> 概要: 昨日荣耀CEO赵明在“2021高通技术与合作峰会”上透露，搭载高通骁龙778G的新款手机——荣耀50系列将在6月份全球首发。据悉，搭载骁龙778G的机型将是荣耀50系列。5月19日晚间，高通正式发布了......
### [谷歌 Chrome 浏览器将升级 PWA 应用：可在独立窗口中运行](https://www.tuicool.com/articles/6zqi2iy)
> 概要: IT之家 5 月 22 日消息 外媒 Windows Latest 报道，微软和谷歌都在努力开发新功能，以改善 Windows 10 和其他桌面平台上的网络应用程序体验。PWA 或网络应用是可以像本地......
### [京东自营 399 元：小米有品 × 小象趣丸六合一无线吸尘器 119 元](https://lapin.ithome.com/html/digi/552995.htm)
> 概要: 小米有品旗舰店出品，小象趣丸 C30 多功能无线吸尘器报价 379 元，下单立减 140 元，限时限量 120 元券，实付 119 元包邮，领券并购买。支持 3 期免息，赠运费险。使用最会买 App下......
### [芳文社まんがタイムきらら和叡山电车联动10周年企划6月12日开始实施](https://news.dmzj1.com/article/70971.html)
> 概要: 芳文社四格漫画杂志《まんがタイムきらら》和叡山电车的联动“きらら×きららPROJECT”10周年纪念企划将于6月12日开始实施。
### [“新物种”BOSS直聘赴美上市，互联网招聘行业可否重获资本热情](https://finance.sina.com.cn/china/2021-05-22/doc-ikmyaawc6911423.shtml)
> 概要: 三国时期，曹操不但是一方枭雄，更是当时最炙手可热的大雇主，相当于现代人眼中的大BOSS，正是曹阿瞒与众不同的人才观，才给自己带来了图霸天下...
### [钢铁板块持续降温，钢企下调价格，华菱钢铁跌超10%](https://finance.sina.com.cn/china/2021-05-22/doc-ikmxzfmm4050708.shtml)
> 概要: 原标题：钢铁板块持续降温，钢企下调价格，华菱钢铁跌超10% 实习生苗诗雨 记者李未来 尽管5月21日收盘时，钢铁板块仍有多股飘红，但在本周（5月14日-5月21日...
### [《昭和元禄落語心中》POP UP SHOP在池袋举办](https://news.dmzj1.com/article/70972.html)
> 概要: 《昭和元禄落語心中》POP UP SHOP在池袋举办
### [国产动画电影《大护法》宣布将于年内登陆日本](https://www.3dmgame.com/news/202105/3814991.html)
> 概要: 近日国产动画电影《大护法》官方微博宣布，电影《大护法》预定今年年内在日本上映，同时发布了一张日版海报。影片的日文片名为“DAHUFA 守護者と謎の豆人間”。这也是这部有着独特风格的“暴力美学”作品第一......
### [送别袁隆平｜明阳山殡仪馆工作人员：23日9点半可瞻仰遗容，24日上午举行追悼会](https://finance.sina.com.cn/china/2021-05-22/doc-ikmyaawc6916480.shtml)
> 概要: 5月22日晚8时，明阳山殡仪馆门前，排队给袁隆平献花的群众仍在增加。长沙市殡仪馆外排队献花的人群。 献花的人群中有长沙当地的市民也有从外地赶来的。
### [视频丨袁隆平接受采访珍贵视频曝光](https://finance.sina.com.cn/china/2021-05-22/doc-ikmyaawc6917186.shtml)
> 概要: 原标题：独家丨袁隆平接受采访珍贵视频曝光
### [伟人陨落举国哀悼之日，虞书欣上热搜被骂惨，道歉后还在蹭热度](https://new.qq.com/omn/20210522/20210522A0AZ4200.html)
> 概要: 5月22日，这一天对于所有国人而言，注定是无眠悲伤的，因为袁隆平和吴孟超两位伟人相继陨落，他们的光辉事迹将永远铭刻在共和国的丰碑之上，也会永远活在每个人的心中。            人死不能复生，作......
### [雨中夹道送别！“袁爷爷，一路走好”](https://finance.sina.com.cn/wm/2021-05-22/doc-ikmxzfmm4056662.shtml)
> 概要: 来源：央视财经 2021年5月22日13时07分 中国工程院院士 “共和国勋章”获得者袁隆平 在湖南长沙逝世，享年91岁 袁老最后一次“看”杂交稻 群众雨中挥泪痛别 应家属要求...
### [DK 1-0 MAD精彩集锦：前期劣势中期把握机会，DK拿下首局](https://bbs.hupu.com/43074686.html)
> 概要: 来源：  虎扑    标签：CanyonDWGMAD
### [《排球少年！！》日向和影山加入GSC“坐吧粘土人”](https://news.dmzj1.com/article/70973.html)
> 概要: 根据古馆春一原作改编的动画《排球少年!! TO THE TOP》中登场的日向翔阳、影山飞雄的“坐吧粘土人”发售
### [情报站外网热评DK 1-0 MAD：顶级天才继续支配着各支队伍](https://bbs.hupu.com/43074761.html)
> 概要: 虎扑05月22日讯 在MSI四强赛DK对阵MAD的比赛中，DK先下一城，赢下了比赛。赛后推特热评如下：Azael：无论今天的结果如何，我都非常开心！MAD或者DK对阵RNG的决赛都会让我们非常兴奋！任
### [200亿元市值蒸发，生长激素龙头放量跌停！怎么回事？](https://finance.sina.com.cn/china/2021-05-22/doc-ikmyaawc6918905.shtml)
> 概要: 5月21日，医药大白马、生长激素龙头长春高新放量跌停，逾200亿元市值蒸发，最新市值为1844亿元。当日下午，在“大哥”带动下，生长激素“二当家”安科生物也惨遭抛售...
### [剧情悬浮，演技翻车？国产爆款拍砸了，大失所望](https://new.qq.com/omn/20210522/20210522A0B2Y900.html)
> 概要: 文 | 十点电影原创最近，一部老妹儿心心念念的国产剧开播了。题材，演员，这中年戏骨和金马女配的规格配置那是相当吸睛，开播后广电脱水收视直接窜上黄金时代前三甲。            然而，万万没想到......
### [赛事视频夜晚的太阳;保护属于她的水晶，广寒宫的九尾之力，温柔驱强敌](https://bbs.hupu.com/43075015.html)
> 概要: 来源：  虎扑    标签：广州TTG武汉eStarPro
### [流言板若塔伤病没有预想的严重，利物浦稍后将对其伤势进行评估](https://bbs.hupu.com/43075057.html)
> 概要: 虎扑05月22日讯 在《回声报》的消息后，利物浦主帅克洛普证实，球队将在晚些时候评估若塔是否能赶上收官战对阵水晶宫的比赛。在本月早些时候利物浦战胜曼联的比赛中脚部受伤，缺席了多场比赛。之前克洛普曾表示
# 小说
### [晋末雄图](http://book.zongheng.com/book/526249.html)
> 作者：尚书台

> 标签：历史军事

> 简介：西晋末年，胡笳羌笛不绝，狼纛马蹄生烟。当此时，一个穿越而来的年轻人，用满腔热血，化作金戈长剑，黄沙百战，再铸河山！

> 章节末：第四百二十九章 辞旧迎新

> 状态：完本
# 论文
### [How well does surprisal explain N400 amplitude under different experimental conditions?](https://paperswithcode.com/paper/how-well-does-surprisal-explain-n400)
> 日期：9 Oct 2020

> 标签：None

> 代码：https://github.com/jmichaelov/does-surprisal-explain-n400

> 描述：We investigate the extent to which word surprisal can be used to predict a neural measure of human language processing difficulty - the N400. To do this, we use recurrent neural networks to calculate the surprisal of stimuli from previously published neurolinguistic studies of the N400.
### [MLatticeABC: Generic Lattice Constant Prediction of Crystal Materials using Machine Learning](https://paperswithcode.com/paper/mlatticeabc-generic-lattice-constant)
> 日期：30 Oct 2020

> 标签：None

> 代码：https://github.com/usccolumbia/MLatticeABC

> 描述：Lattice constants such as unit cell edge lengths and plane angles are important parameters of the periodic structures of crystal materials. Predicting crystal lattice constants has wide applications in crystal structure prediction and materials property prediction.
