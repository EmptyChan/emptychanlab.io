---
title: 2023-02-20-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Itaimbezinho_ZH-CN5641449623_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-20 22:18:37
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Itaimbezinho_ZH-CN5641449623_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [“我毕业五年存款5000”，算失败吗？](https://www.woshipm.com/it/5759053.html)
> 概要: 近期，一则讲述自己“毕业5年，存款5000”的视频火了，视频中两个主人公的经历也引起了争议。“毕业5年，存款不到5000元”真的算失败吗？他们经历过又正在经历着怎样的选择呢？本文作者找到了五位毕业五年......
### [元宇宙失速，行业红利期什么时候才来？](https://www.woshipm.com/it/5759337.html)
> 概要: 仅仅在一年多以前，元宇宙还是万众追捧的创新风口，但如今，ChatGPT成为了新的潮流代表，昔日宠儿已经成为了明日黄花。为什么元宇宙不香了，元宇宙还差点什么？欢迎对元宇宙、XR感兴趣的伙伴们阅读。在官宣......
### [2023，广告业还会好吗？](https://www.woshipm.com/marketing/5759729.html)
> 概要: 按照巴菲特的“企业护城河”概念，广告行业既没有转换成本，也没有规模效应，它靠什么发展并形成优势？2023年的广告行业发展前景如何？它又将向着怎样的方向发展？看了这篇文章，你或许会得到启发。巴菲特在投资......
### [碧桂园“幸免于难”](https://www.huxiu.com/article/798413.html)
> 概要: 作者｜Eastland头图｜视觉中国进入2023年，各界对房地产市场“回暖”的风声喜闻乐见，甚至有人喊出“三条红线，再见了！”严厉的调控迟早会告一段落，但野蛮生长的时代一去不返。恒大、碧桂园、万科、融......
### [电影网站显示《超级马里奥兄弟大电影》片长92分钟](https://www.3dmgame.com/news/202302/3863067.html)
> 概要: 看起来任天堂和照明娱乐合作的《超级马里奥兄弟大电影》时间长度和其他动画电影相差不多，来自国外电影网站给出的信息显示，这部电影的时长为92分钟。根据爱尔兰电影分级办公室的数据显示，《超级马里奥兄弟大电影......
### [特斯拉前CEO批马斯克：容易愤怒 喜欢随机解雇员工](https://finance.sina.com.cn/tech/it/2023-02-20/doc-imyhhxfz8467165.shtml)
> 概要: 新浪科技讯 北京时间2月20日早间消息，据报道，特斯拉创始人、前CEO马丁·艾伯哈德（Martin Eberhard）最近表示，如果公司仍由他掌控，情况可能会有所不同，在随机解雇员工方面，他会做得更少......
### [36氪首发 | 创业孵化器TURBO X完成Pre-A轮融资，科技产业链带动孵化服务革新](https://36kr.com/p/2135542558884224)
> 概要: 36氪首发 | 创业孵化器TURBO X完成Pre-A轮融资，科技产业链带动孵化服务革新-36氪
### [本质是团建活动？苹果举办线下人工智能峰会 ：未公布任何变革性产品技术](https://finance.sina.com.cn/tech/it/2023-02-20/doc-imyhhxhc5258892.shtml)
> 概要: 新浪科技讯 北京时间2月20日早间消息，据报道，苹果2月份在总部园区举办了线下的人工智能峰会。最新消息显示，此次峰会并没有预期中的开创性，更像是一场团建活动，而不是为了宣布任何新进展......
### [跃居影史第三！《阿凡达2》票房已超23亿美元](https://www.3dmgame.com/news/202302/3863069.html)
> 概要: 《阿凡达2》全球票房现已正式超过23.43亿美元，这意味着它超越了《泰坦尼克号》的22.42亿美元，成为影史票房最高的第三大电影。迪士尼分享了一段庆祝这一里程碑的视频，展示了来自世界各地的粉丝对《阿凡......
### [36氪首发 ｜ 「前晨汽车」完成超亿元B+轮融资，专注新能源商用车](https://36kr.com/p/2138799803599234)
> 概要: 36氪首发 ｜ 「前晨汽车」完成超亿元B+轮融资，专注新能源商用车-36氪
### [“移动生活实验室”出征仪式圆满完成，第一批移动文明探险家正式上路 | 最前线](https://36kr.com/p/2137785101617542)
> 概要: “移动生活实验室”出征仪式圆满完成，第一批移动文明探险家正式上路 | 最前线-36氪
### [美国科技初创企业陷入两难：IPO推迟、员工股权奖励到期](https://finance.sina.com.cn/tech/internet/2023-02-20/doc-imyhicpv3299965.shtml)
> 概要: 新浪科技讯 北京时间2月20日早间消息，据报道，如今，在科技行业低迷时期推迟了上市计划的科技初创企业正面临着一个新的困境：如何在并未按照计划完成首次公开募股（IPO）的情况下，处理那些股权奖励即将到期......
### [辛巴辛有志布控企业供应链体系，让辛选保持行业领先地位](http://www.investorscn.com/2023/02/20/105806/)
> 概要: 随着全民直播时代的到来，直播电商行业已然成为近年来高速发展的购物形态。然而面对令人眼花缭乱的产品与主播们，消费者最为关心的还是商品质量、价格和送货速度。在这些因素背后，其考验的仍是一家直播电商企业的供......
### [《银河铁道999》作者松本零士因病去世 享年85岁](https://acg.gamersky.com/news/202302/1568674.shtml)
> 概要: 据日媒报道，《银河铁道999》的原作者，日本著名漫画家松本零士因急性心力衰竭于2023年2月13日去世，享年85岁。
### [&gt;芝麻匠2022作品合集](https://www.zcool.com.cn/work/ZNjQwODAyMjg=.html)
> 概要: 2022年的个人作品以及部分商业作品节选按时间顺序排序第一部分商业项目节选第二部分个人作品展示......
### [第27届手冢治虫文化奖漫画大奖入围名单 2月第3周新闻汇总](https://news.dmzj.com/article/77197.html)
> 概要: 这次准备的是第27届手冢治虫文化奖公开漫画大奖最终候选作品，日本商标局拒绝“油库里实况”等商标的注册申请，评选漫画角色的Magademy Award公开各部门的入围角色，SE手游《圣剑传说 ECHOES of MANA》5月停服，最后还是惯例的新...
### [恩恩的恩平时光之旅 BACK TO YINPENG](https://www.zcool.com.cn/work/ZNjQwODMzNzY=.html)
> 概要: 给家乡文旅项目做的一套ip随着科技进步，社会不断发展所有城市和文化的融合进程前所未见在加速曾熟悉的场景，本土文化还有语言，正在被人们遗忘或许你会说不稀罕这个「落后」的城市，这些「落后」的文化或许你经常......
### [动画《【我推的孩子】Mother and Children》预告片](https://news.dmzj.com/article/77198.html)
> 概要: TV动画《【我推的孩子】》的90分钟第一话先行上映版《【我推的孩子】Mother and Children》公开了正式预告片。在这次的预告片中，使用了YOASOBI演唱的OP主题曲《偶像》的片段。
### [展示18米大高达的GUNDAM FACTORY YOKOHAMA延长开放时间](https://news.dmzj.com/article/77201.html)
> 概要: 在日本神奈川开设的展示18米高的可动高达的设施GUNDAM FACTORY YOKOHAMA，宣布了将开放时间延长1年的消息。展示时间将延至2024年3月31日。
### [《红霞岛》设计师：制作大型开放游戏是打破常规](https://www.3dmgame.com/news/202302/3863090.html)
> 概要: 近日，Arkane 的合作吸血鬼射击游戏《红霞岛》的制作设计师Ben Horne在接受IGN采访时表示，《红霞岛》一点也不像《求生之路》。“《求生之路》是一款很棒的游戏，但我们想要做出一些不同的东西......
### [极氪智慧工厂入围工信部奖项，广域铭岛数智赋能实力再获认可](http://www.investorscn.com/2023/02/20/105812/)
> 概要: 近期，工信部发布了2022年工业互联网试点示范名单，位于宁波前湾新区的极氪智慧工厂的5G全连接工厂项目成功入围......
### [光合组织推出“星火计划”，开放1000P公益算力扶持中小企业发展](http://www.investorscn.com/2023/02/20/105815/)
> 概要: 日前,光合组织正式启动“2023光合行动”。其中,面向中小企业,重磅推出涵盖1000P公益算力扶持的“星火计划“,全面降低算力、技术、成果转化三大门槛,推动数字化转型升级......
### [施成、汤龑联袂出击，国投瑞银景气驱动混合2月20日首发](http://www.investorscn.com/2023/02/20/105817/)
> 概要: 随着碳中和赛道政策端的持续发力，多层次产业链下的投资机会不断涌现。据悉，拟由施成与汤龑双基金经理掌管的国投瑞银景气驱动混合型证券投资基金（A类：017749；C类：017750）于2月20日—3月10......
### [【字幕】欧文：很高兴早早就被选中，我和詹姆斯从彼此身上学到很多](https://bbs.hupu.com/58124320.html)
> 概要: 【字幕】欧文：很高兴早早就被选中，我和詹姆斯从彼此身上学到很多
### [视频：迪丽热巴起诉网友侵权 此前工作室晒出维权通告](https://video.sina.com.cn/p/ent/2023-02-20/detail-imyhiumt7004397.d.html)
> 概要: 视频：迪丽热巴起诉网友侵权 此前工作室晒出维权通告
### [视频：时代少年团晒照记录海边度假 慵懒夏日清新少年上线](https://video.sina.com.cn/p/ent/2023-02-20/detail-imyhiump2961955.d.html)
> 概要: 视频：时代少年团晒照记录海边度假 慵懒夏日清新少年上线
### [视频：童星陈辰离世年仅38岁 曾在《小龙人》中扮演贝贝](https://video.sina.com.cn/p/ent/2023-02-20/detail-imyhiump2962108.d.html)
> 概要: 视频：童星陈辰离世年仅38岁 曾在《小龙人》中扮演贝贝
### [网约车巨头 Uber 将在印度引入 2.5 万辆电动汽车](https://www.ithome.com/0/674/603.htm)
> 概要: IT之家2 月 20 日消息，据路透社报道，Uber 负责人今日表示，该公司三年内将在印度引入 2.5 万辆电动汽车并用于顺风车（ride sharing）业务，这是该公司采用清洁汽车的第一步。Ube......
### [铁三角 AT-LP2022 六十周年纪念版黑胶唱机发布：全透明设计，售价 12350 元](https://www.ithome.com/0/674/610.htm)
> 概要: IT之家2 月 20 日消息，铁三角宣布推出 AT-LP2022 六十周年纪念版黑胶唱机，建议零售价 12350 元。该黑胶转盘一改传统黑胶唱机风格，号称灵感源自钟表陀飞轮机芯，唱片转盘与机身全透明设......
### [最终目标动画化！P.A.WORKS企划《沼活》开始漫画化众筹](https://news.dmzj.com/article/77206.html)
> 概要: P.A.WORKS的参加型企划《沼活》开始众筹漫画化资金的活动。这次众筹目标为100万日元，达成后会制作漫画第二卷。
### [国内显卡出货量暴跌42%！希望就在眼前了](https://www.3dmgame.com/news/202302/3863106.html)
> 概要: 据博板堂最新统计，2023年1月份，中国内地独立显卡品牌总出货量环比增长了9%左右，但是对比2022年1月同期大幅下滑了42%左右。据业内人士分析，1月份显卡市场整体低迷，加之疫情爆发、春节放假等因素......
### [36氪首发｜「智道合创」完成天使轮融资，聚焦新能源资产管理与数字化赋能](https://36kr.com/p/2132504312294401)
> 概要: 36氪首发｜「智道合创」完成天使轮融资，聚焦新能源资产管理与数字化赋能-36氪
### [广州也有“超长贷”了 房贷借款人最高年限可到85岁](https://finance.sina.com.cn/china/2023-02-20/doc-imyhiytr5251849.shtml)
> 概要: 来源：21世纪经济报道 记者 叶麦穗 广州报道 “超长贷”家族又添新成员， 广州的房贷借款人最高年限放宽至85岁。今日21世纪经济报道记者从多方了解到...
### [苹果被印度工厂“坑了”？](https://www.huxiu.com/article/799801.html)
> 概要: 本文来自微信公众号：时代周报 （ID：timeweekly），作者：马欢，编辑：梁励‍‍‍‍‍‍，原文标题：《苹果被印度工厂“坑了”？工作效率拖泥带水，良品率低，还有供应链难题》，头图来自：视觉中国印......
### [开年首月我国吸收外资超千亿元 同比增长14.5%](https://finance.sina.com.cn/jjxw/2023-02-20/doc-imyhkezm8385999.shtml)
> 概要: 新华社北京2月20日电（记者谢希瑶）商务部20日发布数据显示，2023年1月，全国实际使用外资金额1276.9亿元人民币，同比增长14.5%；折合190.2亿美元，同比增长10%。
### [河南去年常住人口减少11万，人口自然增长率62年来首现负增长](https://finance.sina.com.cn/jjxw/2023-02-20/doc-imyhkezm8393177.shtml)
> 概要: 人口大省河南公布了2022年全省人口数据情况。2月20日，据河南省统计局消息，2022年河南省常住人口总量继续下降，自然增长人口六十多年来首现负增长。
### [《蚁人3》北美开画创系列最高纪录](https://www.3dmgame.com/news/202302/3863107.html)
> 概要: 虽然《蚁人3》烂番茄新鲜度低至47%（和《永恒族》并列MCU最差），但北美首周末三天票房粗估1.04亿美元（四天1.18亿美元），创《蚁人》系列最高开画，2015年《蚁人》开画5722万美元，2018......
### [三星相机助手适配兼容更多搭载 One UI 5.1 的 Galaxy 手机](https://www.ithome.com/0/674/638.htm)
> 概要: IT之家2 月 20 日消息，三星此前推出了相机助手（Camera Assistant）应用程序，为 Galaxy 智能手机用户带来更精细的相机控制。该应用程序最初仅面向 Galaxy S22 系列推......
### [银行股走出久违大阳线，中国版巴III影响几何？](https://www.yicai.com/news/101680199.html)
> 概要: 或将节约银行资本约万亿元。
### [我们读上野千鹤子，不是因为“被男人伤害过”](https://www.huxiu.com/article/799838.html)
> 概要: 本文来自微信公众号：硬核读书会 （ID：hardcorereadingclub），作者：张文曦，编辑：王亚奇，头图来自：《坡道上的家》上野千鹤子又一次上了热搜。这次事件的缘起是日本学者上野千鹤子和“蔡......
### [三十岁还没有走到管理岗的人，后来都做了什么？](https://www.huxiu.com/article/799929.html)
> 概要: 本文来自微信公众号：瞎说职场（ID：HRInsight），作者：Sean Ye，题图来自：视觉中国知乎上有一个火热话题：“三十岁还没有走到管理岗的人，后来都做了什么？”很多年轻人在做职业规划时，都将升......
### [支持企业顺应新趋势、抢占新赛道！陈吉宁今天调研互联网企业发展](https://finance.sina.com.cn/jjxw/2023-02-20/doc-imyhkezm8428455.shtml)
> 概要: 市委书记陈吉宁今天（2月20日）下午在调研互联网企业发展时指出，发展数字技术、数字经济，是把握世界新一轮科技革命和产业变革的战略选择，是上海当好改革开放排头兵...
### [谷歌正使用 Jetpack Compose 逐步重写 Android 14 的设置应用](https://www.ithome.com/0/674/643.htm)
> 概要: 感谢IT之家网友吾爱317的线索投递！IT之家2 月 20 日消息，早在 2019 年，谷歌就推出了Jetpack Compose，这是一种使用 Kotlin 开发原生安卓应用的编写方式，抛弃了常规基......
### [上戏表演系考生须达一本线系误读，艺术生的文化课重要么？](https://www.yicai.com/news/101680333.html)
> 概要: 所以经过很多年的招生，艺术院校非常有经验地把分数线卡在一个合适的位置上，既有很多的选择余地，又有一定的专业的标准。
### [多省冒出“小龙”“小虎”城市，这5个城市去年增速超8%](https://finance.sina.com.cn/china/2023-02-20/doc-imyhkezp5220146.shtml)
> 概要: 甘肃金昌、福建宁德、浙江舟山、广西钦州、云南曲靖 在疫情等多种不利因素的冲击下，去年主要中心城市的经济增长不达预期，但是，一些中小城市却表现亮眼...
### [多省冒出“小龙”“小虎”城市，这5个城市去年增速超8%](https://www.yicai.com/news/101680338.html)
> 概要: 甘肃金昌、福建宁德、浙江舟山、广西钦州、云南曲靖
### [华为与江淮将合作造车？江淮汽车回应；中兴通讯回应“人员优化”传闻](https://www.yicai.com/news/101680357.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [新时代新深马！普渡机器人现场为选手打call，彰显新深马科技硬实力](http://www.investorscn.com/2023/02/20/105822/)
> 概要: 此前延期的2022年深马于2月19日7:30鸣枪开跑，开启今年深圳大型赛事活动的新篇章。本届深马由深圳市人民政府主办，深圳市文化广电旅游体育局、深圳市投资控股有限公司和深圳广播电影电视集团联合承办。以......
### [真兰仪表登陆创业板，华福证券四年后再获首单IPO](https://www.yicai.com/news/101680361.html)
> 概要: 上市首日，真兰仪表报收34.01元/股，涨26.9%。
### [流言板字母哥：选塔特姆时毫不犹豫，知道他出场就会拼命打球](https://bbs.hupu.com/58130190.html)
> 概要: 虎扑02月20日讯 NBA全明星正赛字母哥队以184-175战胜詹姆斯队，扬尼斯-阿德托昆博接受采访，谈到了选人阶段首选杰森-塔特姆。阿德托昆博说：“他是联盟最佳之一，是一名出色的得分手，也在今天展示
### [流言板JR锐评陈家剑高质量单杀，Breathe对阵LGD综合得分9.5](https://bbs.hupu.com/58130424.html)
> 概要: 虎扑02月20日讯 LPL春季常规赛2月20日第二场比赛，RNG以2比0横扫LGD，本场比赛Breathe选手发挥同样出色，首局操刀菲欧娜完成一次高质量单杀。JR用“居然敢放陈家剑”来夸赞他今日的表现
### [塔特姆赛后：梦想成真，带走以我最爱球员命名的奖杯](https://bbs.hupu.com/58130466.html)
> 概要: 来源：  虎扑
# 小说
### [记忆猎手](https://book.zongheng.com/book/1166399.html)
> 作者：香煎带鱼

> 标签：科幻游戏

> 简介：全义体改造推行的第二个纪年，拥有私人记忆变成了违法行为。从东城区苏醒的那一天起，他就是一个没有记忆的人。但他始终清楚，属于自己的东西，哪怕是抢，是颠覆整个联邦，也必须夺回来。如果你们祈祷霓虹永驻，那我便踩灭你们的霓虹。如果你们认为义体永生，那我便成为你们的终结。

> 章节末：终章（2）：原罪

> 状态：完本
# 论文
### [UDAAN - Machine Learning based Post-Editing tool for Document Translation | Papers With Code](https://paperswithcode.com/paper/udaan-machine-learning-based-post-editing)
> 概要: We introduce UDAAN, an open-source post-editing tool that can reduce manual editing efforts to quickly produce publishable-standard documents in different languages. UDAAN has an end-to-end Machine Translation (MT) plus post-editing pipeline wherein users can upload a document to obtain raw MT output. Further, users can edit the raw translations using our tool. UDAAN offers several advantages: a) Domain-aware, vocabulary-based lexical constrained MT. b) source-target and target-target lexicon suggestions for users. Replacements are based on the source and target texts lexicon alignment. c) Suggestions for translations are based on logs created during user interaction. d) Source-target sentence alignment visualisation that reduces the cognitive load of users during editing. e) Translated outputs from our tool are available in multiple formats: docs, latex, and PDF. Although we limit our experiments to English-to-Hindi translation for the current study, our tool is independent of the source and target languages. Experimental results based on the usage of the tools and users feedback show that our tool speeds up the translation time approximately by a factor of three compared to the baseline method of translating documents from scratch.
### [Local Contrast and Global Contextual Information Make Infrared Small Object Salient Again | Papers With Code](https://paperswithcode.com/paper/local-contrast-and-global-contextual)
> 日期：28 Jan 2023

> 标签：None

> 代码：https://github.com/wcyjerry/UCFnet

> 描述：Infrared small object detection (ISOS) aims to segment small objects only covered with several pixels from clutter background in infrared images. It's of great challenge due to: 1) small objects lack of sufficient intensity, shape and texture information; 2) small objects are easily lost in the process where detection models, say deep neural networks, obtain high-level semantic features and image-level receptive fields through successive downsampling. This paper proposes a reliable detection model for ISOS, dubbed UCFNet, which can handle well the two issues. It builds upon central difference convolution (CDC) and fast Fourier convolution (FFC). On one hand, CDC can effectively guide the network to learn the contrast information between small objects and the background, as the contrast information is very essential in human visual system dealing with the ISOS task. On the other hand, FFC can gain image-level receptive fields and extract global information while preventing small objects from being overwhelmed.Experiments on several public datasets demonstrate that our method significantly outperforms the state-of-the-art ISOS models, and can provide useful guidelines for designing better ISOS deep models. Codes will be available soon.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
