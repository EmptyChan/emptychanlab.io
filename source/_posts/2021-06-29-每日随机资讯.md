---
title: 2021-06-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RocksSeychelles_ZH-CN0105602892_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-06-29 23:08:49
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RocksSeychelles_ZH-CN0105602892_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [戴尔开源 Omnia，用于管理 HPC 和人工智能工作负载](https://www.oschina.net/news/148116/dell-announces-open-source-software-omnia)
> 概要: 戴尔公司近日宣布，其用于管理高性能计算（HPC）和人工智能工作负载的开源软件Omnia已经正式推出。虽然这一消息是近日才正式宣布的，但 Omnia 自今年年初以来就已经登陆GitHub了。Omnia ......
### [FIT2CLOUD 飞致云开源 DataEase 数据可视化分析平台](https://www.oschina.net/news/148119/fit2cloud-opensource-dataease)
> 概要: 2021年6月28日，FIT2CLOUD飞致云正式发布DataEase开源数据可视化分析平台。作为一款开源的数据可视化分析工具，DataEase（github.com/dataease）可以帮助用户快......
### [微软推出 Windows 11 首个预览版本](https://www.oschina.net/news/148115/the-first-insider-preview-for-windows-11)
> 概要: 微软现已在 Dev Channel 向 Insider 测试人员提供了首个  Windows 11 预览版本更新Build 22000.51。Build 22000.51 包含了一些微软在上周Wind......
### [Kubernetes 发布 2020 年社区年度报告](https://www.oschina.net/news/148240)
> 概要: 鉴于 Kubernetes 项目的增长和规模，现有的报告机制被证明是不够的和具有挑战性的。Kubernetes 是一个大型的开源项目。仅仅在主要的 k/kubernetes 仓库中就提交了超过 100......
### [全球最大免税巨头能否打破天花板魔咒？](https://www.huxiu.com/article/437457.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年6月25日，中国旅游集团旗下“中国中免”向港交所提交了上市申请。坊间传说将募集70亿至100亿美元，成为阿里回归香港以来规模最大的IPO，中金、瑞银担任联......
### [组图：少女时代徐贤晒庆生照比心 穿吊带衫皮肤白皙](http://slide.ent.sina.com.cn/star/k/slide_4_704_358621.html)
> 概要: 组图：少女时代徐贤晒庆生照比心 穿吊带衫皮肤白皙
### [互联网战争：不眠不休，无止无尽](https://www.huxiu.com/article/437735.html)
> 概要: 本文来自微信公众号：晚点LatePost（ID：postlate），作者：龚方毅、黄俊杰，编辑：黄俊杰，头图来自电影《明日边缘》能上市的中国互联网公司都在几个月里涌向股市。赚大钱的在、盈利无望的也在；......
### [动画电影「最喜欢电影的彭波小姐」主题曲原创动画MV公开](http://acg.178.com/202106/418932431482.html)
> 概要: 动画电影「最喜欢电影的彭波小姐」（酷爱电影的庞波小姐）主题曲「窓を開けて」的原创动画MV正式公开，该曲由CIEL演唱，平尾隆之负责监督、分镜及演出。「窓を開けて」原创动画MV主题曲歌唱：CIEL作词・......
### [TV动画「魔法科高校的优等生」第二弹PV公开](http://acg.178.com/202106/418933618757.html)
> 概要: TV动画「魔法科高校的优等生」于近日公开了第二弹PV。该TV动画将于2021年7月3日正式播出。「魔法科高校的优等生」第二弹PVSTAFF原作：佐島 勤＋森 夕(KADOKAWA刊)角色原案：石田可奈......
### [「FairyTale-Another-」睡美人手办开订](http://acg.178.com/202106/418934144281.html)
> 概要: 「FairyTale-Another-」睡美人手办开订，作品采用ABS、PVC材质，全高约260mm，售价为999元，预计将于2022年8月发售......
### [组图：angelababy赖冠霖剧组聚餐气氛热烈 新剧将演绎姐弟恋](http://slide.ent.sina.com.cn/tv/slide_4_704_358628.html)
> 概要: 组图：angelababy赖冠霖剧组聚餐气氛热烈 新剧将演绎姐弟恋
### [视频：网友偶遇伊能静带女儿现身迪士尼 真实素颜状态曝光](https://video.sina.com.cn/p/ent/2021-06-29/detail-ikqcfnca3899804.d.html)
> 概要: 视频：网友偶遇伊能静带女儿现身迪士尼 真实素颜状态曝光
### [刷 KPI or 真实贡献？如何客观评价华为对 Linux kernel 的贡献情况？](https://segmentfault.com/a/1190000040261462?utm_source=sf-homepage)
> 概要: 最近，Linux 邮件列表出现了一封名为《Please don't waste maintainers' time on your KPI grabbing patches (AKA, don't b......
### [VC也看不懂“明星项目”](https://www.huxiu.com/article/437765.html)
> 概要: 本文来自微信公众号：投中网（ID：China-Venture），作者：喜乐，题图来自：《华尔街》先讲一个真实案例。A和B是同一个细分赛道的两家综合能力都还不错的公司。A是标准的水下项目，团队研发能力较......
### [组图：李沁黑白色拼接裙轻盈飘逸 置身园林之间诠释东方美学](http://slide.ent.sina.com.cn/star/slide_4_86512_358632.html)
> 概要: 组图：李沁黑白色拼接裙轻盈飘逸 置身园林之间诠释东方美学
### [「游戏王 怪兽之决斗」欧贝利斯克的巨神兵手办开订](http://acg.178.com/202106/418940281542.html)
> 概要: 寿屋作品「游戏王 怪兽之决斗」欧贝利斯克的巨神兵手办开订，全长约350mm，主体采用ABS和PVC材料制造。该手办定价为21800日元（去税），约合人民币1248元，预计于2022年1月发售......
### [《神奇小子 爱莎在怪物世界》登陆Steam 售价103元](https://www.3dmgame.com/news/202106/3817833.html)
> 概要: 由G CHOICE, Monkey Craft开发的《神奇小子爱莎在怪物世界》现已登陆Steam平台，国区售价103元，支持简体中文，以下为相关介绍内容。Steam商城页面截图：小战士爱莎的冒险开始了......
### [激光眼失灵？迈入币圈后比特币暴跌40% NFL七冠王无奈问计网友](https://www.tuicool.com/articles/vIZBjuZ)
> 概要: 财联社（上海，编辑 潇湘）讯，美式橄榄球巨星、NFL七冠王得主汤姆·布雷迪在今年5月曾公开表示，自己是加密货币的“忠实信徒”，并认为比特币的涨势会走得更远。然而，这位北美史上最佳运动员的有力争夺者，在......
### [一文读懂比特币合成资产：如何释放比特币的DeFi价值？](https://www.tuicool.com/articles/QruaamQ)
> 概要: 毫无疑问，比特币是加密货币的统治者。然而，尽管比特币作为数字黄金取得了成功，但在其目前的状态下，它只是用于持有。幸运的是，现在有越来越多的用户可以用他们的比特币去做更多的事情，比如把它们用于其他区块链......
### [大厂取消“大小周”：谁在支持，谁在反对](https://www.tuicool.com/articles/nqMRjq6)
> 概要: 撰文 | 翟继茹、程梦玲编辑 | 包校千支持不支持“996”和“大小周”?这是互联网大厂员工最近都在纠结的问题。中国互联网企业“苦高强度加班久矣”，虽然抵制和批评“996”和“大小周”的声音从未休止，......
### [Code in ARM Assembly: Conditional Loops](https://eclecticlight.co/2021/06/29/code-in-arm-assembly-conditional-loops/)
> 概要: Code in ARM Assembly: Conditional Loops
### [《神偷奶爸》制作人正式加入任天堂董事会](https://www.3dmgame.com/news/202106/3817862.html)
> 概要: 电影和动画工作室Illumination的首席执行官Chris Meledandri今天正式加入任天堂董事会，担任外部董事。他于周二任天堂第81届年度股东大会上被推选并被官方任命于该职位。这位美国人最......
### [苏州地铁 5 号线可用数字人民币购票乘车](https://www.ithome.com/0/560/049.htm)
> 概要: IT之家6 月 29 日消息 据苏州发布，今日，苏州轨道交通 5 号线正式开通，采用 6 辆 B 型车编组，在国内率先实现了数字人民币 App 扫码购票乘车。苏州地铁 5 号线满载乘客数量 2034 ......
### [视频：洪金宝家中宴请好友状态不错 与老婆坐餐桌热聊心情好](https://video.sina.com.cn/p/ent/2021-06-29/detail-ikqcfnca3982507.d.html)
> 概要: 视频：洪金宝家中宴请好友状态不错 与老婆坐餐桌热聊心情好
### [政务可视化设计经验——思考方式](https://www.tuicool.com/articles/IV7JVbN)
> 概要: 编辑导语：在大数据时代，数据的重要性已经被各行各业所知晓并广泛应用。党政机关的数据量庞大且复杂，因此更需要借助数据可视化来推动数字化转型。基于此，本文作者为我们分享了他的关于可视化设计经验的思考方式，......
### [换电站上风口背后：蔚来不是最大玩家，特斯拉也曾入局](https://www.ithome.com/0/560/076.htm)
> 概要: 多年低调发展后，换电产业风口已至！今年，央视多次报道了工业和信息化部与国家能源局的换电试点计划，再次将换电的热度炒了起来。这背后是政府对换电产业的高度重视。除了工信部和国家能源局的试点计划，国家发展改......
### [Concrete: The material that's 'too vast to imagine'](https://www.bbc.com/future/article/20210628-concrete-the-material-that-defines-our-age)
> 概要: Concrete: The material that's 'too vast to imagine'
### [Portable Nuclear Reactor Program Sparks Controversy](https://www.nationaldefensemagazine.org/articles/2021/6/28/portable-nuclear-reactor-program-sparks-controversy)
> 概要: Portable Nuclear Reactor Program Sparks Controversy
### [腾讯音乐“迭代”：调人、做内容、打短视频](https://www.ithome.com/0/560/090.htm)
> 概要: 腾讯音乐娱乐集团（TME）已经进入大步快跑的阶段，以适应短视频社交娱乐时代腹背受敌的市场竞争局面。今年 4 月，紧随腾讯 PCG 的重大调整，TME 方面也进行了一轮组织上的变革，原 CEO 彭迦信担......
### [微软 Win11 新变化：文件资源管理器“命令栏”和上下文菜单上手体验](https://www.ithome.com/0/560/091.htm)
> 概要: IT之家6 月 29 日消息 今天凌晨，微软面向 Dev 开发频道发布了第一个 Windows 11 Insider Preview 版本，即 Build 22000.51!  微软表示，随着我们在未......
### [索尼或将收购《恶魔之魂重制版》开发商蓝点游戏](https://www.3dmgame.com/news/202106/3817886.html)
> 概要: SIE在官宣收购《死亡回归》开发商Housemarque之后，PlayStation日本发布了庆祝消息，但配图却是“欢迎蓝点工作室加入大家庭”，这似乎提前泄露了索尼还将收购《恶魔之魂：重制版》的开发商......
### [《千古玦尘》被说成“不祥之物”的凤染，真实身份比天后还尊贵](https://new.qq.com/omn/20210629/20210629A0CJJJ00.html)
> 概要: 周冬雨、许凯主演的仙侠剧《千古玦尘》开分4.8，目前已涨到5.1分，虽然很多网友在吐槽，可是这部剧的播放量却一天比一天高，最高已经达到了单日破亿的成绩，真是令人刮目相看。            相信很......
### [【iG vs LGD】LPL赛事没品图：怪英雄炸弹人，真好玩！](https://bbs.hupu.com/43947642.html)
> 概要: 【iG vs LGD】LPL赛事没品图：怪英雄炸弹人，真好玩！
### [Virtual DOM is pure overhead (2018)](https://svelte.dev/blog/virtual-dom-is-pure-overhead)
> 概要: Virtual DOM is pure overhead (2018)
### [马嘉祺高考失利持续发酵，知名编剧隔空嘲讽，学霸人设遭官媒痛批](https://new.qq.com/omn/20210629/20210629A0CLD000.html)
> 概要: 在郑爽薪酬曝光后，“一爽”几乎快被网友玩坏了，甚至还有粉丝将其作为一个计量单位来进行调侃。可没想到才过了没多久，又一个计量单位诞生了，这一次轮到了马嘉祺，更有粉丝调侃“一马”等于多少分？       ......
### [美股三大指数小幅高开，道指道指涨0.36%](https://www.btc126.com//view/174383.html)
> 概要: 美股三大指数小幅高开，道指涨0.36%，标普500涨0.13%，纳指涨0.06%......
### [美股区块链板块早盘走强 Marathon Patent涨超10%](https://www.btc126.com//view/174384.html)
> 概要: 行情显示，美股区块链板块早盘走强，Marathon Patent涨超10%，Riot Blockchain涨9%，MicroStrategy涨7.82%，嘉楠科技涨7.28%，第九城市涨5.9%，亿邦......
### [广东韶新高速全面通车：韶关进入惠州、深圳3小时“经济圈”](https://finance.sina.com.cn/china/dfjj/2021-06-29/doc-ikqciyzk2622642.shtml)
> 概要: 6月29日，广东韶关至新丰的高速公路正式通车运营。韶新高速全长85.29公里，是广东省首条PPP模式投资建设的高速公路，即政府出资与社会资本出资共同建设的项目。
### [世界超算最新排名出炉！日本“富岳”三连冠](https://www.3dmgame.com/news/202106/3817888.html)
> 概要: 全球最新的超级计算机排名出炉，在TOP10榜单上变化不大，日本富岳凭借着领先第二名3倍的算力继续霸榜，不过从技术趋势来看，却有了新的变化。在德国法兰克福举行的国际超级计算大会上，2021第57版世界T......
### [深圳任命20余名市政府组成部门“一把手”](https://finance.sina.com.cn/china/dfjj/2021-06-29/doc-ikqciyzk2624203.shtml)
> 概要: 原标题：深圳任命20余名市政府组成部门“一把手” 来源：南方+ 6月29日，深圳市七届人大常委会第二次会议表决通过人事任免案，高圣元等22名同志被任命为深圳市政府秘书长和市...
### [超30城公积金缴存方案变了！买不买房都要看→](https://finance.sina.com.cn/wm/2021-06-29/doc-ikqcfnca4025039.shtml)
> 概要: •世界银行：预计今年中国经济增长8.5% •前5月我国服务贸易进出口总值同比增长3.7% •超30个城市公布公积金缴存政策调整方案 •我国新冠疫苗接种突破12亿剂次 【联播数说】...
### [Daymak宣布其Spiritus电动汽车今天开始挖比特币等加密货币](https://www.btc126.com//view/174387.html)
> 概要: 据U.Today消息，加拿大轻型电动汽车制造商Daymak宣布，其三轮车Daymak Spiritus的第一个原型车今天将开始挖比特币、以太坊、狗狗币和其他加密货币。该过程将在公司网站上公开播放，观众......
### [这部不一样的主旋律，为何又好哭又好看？](https://new.qq.com/omn/20210629/20210629A0DAHU00.html)
> 概要: 《革命者》不是我们熟悉的全盘视角，而聚焦李大钊先生最后的几十个小时。通过回忆，以各路角色和李大钊的关系为辐射原点，完成时代图景的描摹和信念情感的传递。数次被打动。核心当然是历史中，如今想从影视剧观众的......
### [福建：不存在泄题、试卷被窃的情况](https://finance.sina.com.cn/jjxw/2021-06-29/doc-ikqciyzk2625886.shtml)
> 概要: “福建省教育考试院”微信公众号 “福建省教育考试院”微信公众号6月29日发布关于2021年中考地理生物试卷相关问题的情况说明。 福建省2021年中考所有科目考试于6月27日17时45分...
### [小S自曝疏于保养！还不如女儿爱洗澡，网友：林志玲可不会偷懒](https://new.qq.com/omn/20210629/20210629A0DCJH00.html)
> 概要: 6月29日，小S在个人社交平台上发文分享了亲子日常，自曝自己疏于保养，而且从孩子身上得到爱干净讲卫生的动力，一时间引发不少网友讨论。            小S透露自己女儿每天都要上课8小时，还要写作......
### [国家医保局通报10起违规结算医保基金典型案件](https://finance.sina.com.cn/china/2021-06-29/doc-ikqcfnca4026934.shtml)
> 概要: 原标题：国家医保局通报10起违规结算医保基金典型案件 6月29日，国家医保局通报了2021年第四期违规结算医保基金典型案件，共10例。
### [流言板全能表现！乔治近三场场均可以得到30.3分14.7篮板6.7助攻](https://bbs.hupu.com/43948889.html)
> 概要: 虎扑06月29日讯 今日，快船以116-102击败太阳，把大比分扳成2-3。赛后，StatMuse更新推特，晒出快船球员保罗-乔治近三场的数据统计与球迷分享。近三场比赛，乔治分别可以得到41分13篮板
### [《大红狗克里弗》首部中字预告 9月17日北美上映](https://www.3dmgame.com/news/202106/3817889.html)
> 概要: 派拉蒙电影官方发布了改编自同名畅销童书的真人电影《大红狗克里弗》首部预告，该片定档北美9月17日上映，大萌狗将登上大银幕，敬请期待！《大红狗克里弗》首部预告：一夜之间，暖萌乖巧的小可爱变身体型巨大的大......
### [流言板沪媒：申花重伤两将首阶段中超难复出，将和青岛队约战热身](https://bbs.hupu.com/43948999.html)
> 概要: 虎扑06月29日讯 据上海媒体《新闻晨报》报道，正在大连集训的申花球员彭欣力已从酒店健身房移师球场，跟随康复师进行慢跑等恢复性训练。另外，曾诚恢复状况也良好，不过两人很难赶上首阶段中超比赛。在中超联赛
### [刘语熙：超级生气！今晚没有心情预测了，德国和瑞典胜出](https://bbs.hupu.com/43949053.html)
> 概要: 刘语熙：我怎么消失了？去哪了？ 来源：  虎扑
### [香港特区政府新任政务司司长李家超：履职尽责维护香港繁荣稳定](https://finance.sina.com.cn/china/dfjj/2021-06-29/doc-ikqciyzk2628352.shtml)
> 概要: 原标题：专访香港特区政府新任政务司司长李家超：履职尽责维护香港繁荣稳定 6月29日，在香港国安法实施一周年前夕，香港特区政府新任政务司司长李家超接受总台记者专访...
### [数据：目前86%的ETH持有者处于盈利状态](https://www.btc126.com//view/174392.html)
> 概要: 据加密分析平台 IntoTheBlock 提供的数据显示，截至 6 月 29 日，约有 86% 的ETH持有者处于盈利状态，Cardano（ADA）持有者处于盈利状态的比例为 45%。尽管最近加密货币......
# 小说
### [比邻星纪元](http://book.zongheng.com/book/874785.html)
> 作者：仓央雪儒

> 标签：科幻游戏

> 简介：在“末日宿命论”的阴影笼罩下，王者大陆上的各方势力为了掌控明日方舟，而展开冰与火之间的较量，谁最终能获得重启方舟的能量密匙，最终成为守护王者大陆未来的王者呢？

> 章节末：章二百五十三：维宙的遁去（大结局）

> 状态：完本
# 论文
### [IITK@LCP at SemEval 2021 Task 1: Classification for Lexical Complexity Regression Task](https://paperswithcode.com/paper/iitk-lcp-at-semeval-2021-task-1)
> 日期：2 Apr 2021

> 标签：None

> 代码：https://github.com/neilrs123/Lexical-Complexity-Prediction

> 描述：This paper describes our contribution to SemEval 2021 Task 1: Lexical Complexity Prediction. In our approach, we leverage the ELECTRA model and attempt to mirror the data annotation scheme.
### [SPECTRE: Defending Against Backdoor Attacks Using Robust Statistics](https://paperswithcode.com/paper/spectre-defending-against-backdoor-attacks)
> 日期：22 Apr 2021

> 标签：None

> 代码：https://github.com/SewoongLab/spectre-defense

> 描述：Modern machine learning increasingly requires training on a large collection of data from multiple sources, not all of which can be trusted. A particularly concerning scenario is when a small fraction of poisoned data changes the behavior of the trained model when triggered by an attacker-specified watermark.
