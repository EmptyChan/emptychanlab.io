---
title: 2021-10-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Hatshepsut_ZH-CN4516192627_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-10-16 22:09:39
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Hatshepsut_ZH-CN4516192627_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Elastic 收购持续分析创业公司 Optimyze](https://www.oschina.net/news/164436/elastic-buy-optimyze)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>大数据分析和搜索引擎 Elasticsearch 的开发商Elastic近日宣布收购了创业公司Optimyze，该公......
### [苹果加入 Blender 基金会](https://www.oschina.net/news/164438/apple-joins-blender-development-fund)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>Blender 基金会宣布苹果以Patron 赞助者身份加入了 Blender 基金会的 Development F......
### [OpenBSD 7.0 发布，第 51 个 OpenBSD 版本](https://www.oschina.net/news/164429/openbsd-7-0-released)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>OpenBSD 是一个专注于代码正确和文档准确且关注安全的操作系统，其强调可移植性、标准化、正确性、前摄安全性以及集......
### [Android Developers 发布完整的 Android 基础课程](https://www.oschina.net/news/164448/android=developers-announce-complete-android-basic-course)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>Android Developers 发文表示，完整的Kotlin Android 基础课程现已推出。该课程最初发布......
### [From Æthelflæd to Ælfthryth: The Idea of Queenship in Tenth-Century England](https://jhiblog.org/2021/10/11/from-aethelflaed-to-aelfthryth-the-idea-of-queenship-in-tenth-century-england/)
> 概要: From Æthelflæd to Ælfthryth: The Idea of Queenship in Tenth-Century England
### [TV动画《食锈末世录》公开第一弹PV](https://news.dmzj.com/article/72461.html)
> 概要: TV动画《食锈末世录》第1弹PV公开，将于2022年1月开播。
### [TV动画《秘密内幕～女警的反击～》预告公开，于明年开播！](https://news.dmzj.com/article/72462.html)
> 概要: TV动画《秘密内幕～女警的反击～》公开第一弹PV，将于明年1月开播。
### [“没文化”的韩国，凭什么能征服全世界？](https://www.huxiu.com/article/464161.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 | 渣渣郡题图 | 《杀人回忆》电影截图本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事......
### [组图：戚薇露肩粉色泡泡袖配长裙优雅神秘 海洋球尽显童话格调](http://slide.ent.sina.com.cn/star/slide_4_86512_362705.html)
> 概要: 组图：戚薇露肩粉色泡泡袖配长裙优雅神秘 海洋球尽显童话格调
### [组图：养生“霸总”上线！吴磊出席晚宴自带保温杯并接热水](http://slide.ent.sina.com.cn/star/slide_4_86512_362713.html)
> 概要: 组图：养生“霸总”上线！吴磊出席晚宴自带保温杯并接热水
### [外卖骑手，是怎么陷入平台用工暗网的？](https://www.huxiu.com/article/464032.html)
> 概要: 骑手们每天飞驰在路上，跟时间赛跑，为外卖市场带来巨大收益，然而，当他们的权益需要保障时，却讶异地发现，他们找不到可以直接维权的用人单位。在一张庞大且异常复杂的用工暗网下，骑手们被不断外包出去，甚至失去......
### [动画「魔法科高校的优等生」Blu-ray&DVD第三卷封面使用插图公开](http://acg.178.com/202110/428352081215.html)
> 概要: 电视动画「魔法科高校的优等生」公开了由角色设计·山本亮友绘制的Blu-ray&DVD第三卷封面使用插图，该商品将于11月24日开始发售。该商品完全生产限定版Blu-ray售价8800日元（含税，下同）......
### [宫野真守出演Amazon Prime Video新CM](https://news.dmzj.com/article/72463.html)
> 概要: 宫野真守出演Amazon Prime Video新CM。
### [「白沙的水族馆」x横滨八景岛海岛乐园合作视觉图公开](http://acg.178.com/202110/428354375388.html)
> 概要: 近日，「白沙的水族馆」宣布与横滨八景岛海岛乐园进行联动，并公开了合作视觉图。「白沙水族馆」以冲绳南城市的一家将要闭馆的海洋馆为舞台，讲述了代理馆长海咲野括和在东京失去了梦想的原偶像宫泽风花相遇后发生的......
### [Theory of Computation](https://ocw.mit.edu/courses/mathematics/18-404j-theory-of-computation-fall-2020/)
> 概要: Theory of Computation
### [轻小说「欢迎来到实力至上主义的教室 二年级生篇」第5卷彩图公开](http://acg.178.com/202110/428355202317.html)
> 概要: 轻小说「欢迎来到实力至上主义的教室 二年级生篇」公开了第5卷的彩页插图，该卷将于10月25日发售。「欢迎来到实力至上主义的教室 二年级生篇」是由衣笠彰梧创作、トモセ シュンサク负责插画的轻小说作品，是......
### [末日MMO《浩劫前夕》发售日公布 新增次世代版本](https://www.3dmgame.com/news/202110/3825889.html)
> 概要: 发行商 MyTona 和开发商 Fntastic 宣布，开放世界生存大型多人在线游戏 《浩劫前夕》 将于 2022 年 6 月 22 日通过 Steam 推出 PC 版。该游戏还将登陆 PlaySta......
### [PPI创新高，CPI延续下滑，这意味着什么？](https://www.huxiu.com/article/464261.html)
> 概要: 本文来自微信公众号：泽平宏观（ID：zepinghongguan），作者：任泽平，原文标题：《PPI创新高，CPI延续下滑》，题图来自：视觉中国中国9月CPI同比上涨0.7%，预期涨0.8%，前值涨0......
### [3D剧场版《攻壳SAC_2045》“巴特”角色PV 11月12日上映](https://www.3dmgame.com/news/202110/3825891.html)
> 概要: 今日（10月16日），万代发布3D剧场版《攻壳机动队SAC_2045》“巴特”角色PV，该角色CV是著名配音演员大塚明夫，本片将于 11月12日 在日本地区上映，内容为 Netflix原创动画第1季+......
### [《Apex英雄》角色动作表情导致游戏崩溃 官方回应](https://www.3dmgame.com/news/202110/3825892.html)
> 概要: 《Apex英雄》中，玩家可以装备许多不同的客制化物品，包括武器皮肤、角色皮肤、动作表情、角色语音等等。其中就包括个人名片的详细定制，像是成就、角色的名片背景、名片动作。近日，有许多玩家表示，玩家名片如......
### [孔庙祈福 + 金榜题名：晨光考试 13 件套 9.9 元新低速囤](https://lapin.ithome.com/html/digi/580886.htm)
> 概要: 【晨光官方旗舰店】晨光 孔庙祈福 考试 13 件套日常售价 14.9 元，今日可领 5 元大促券，实付 9.9 元包邮：天猫晨光文具 考试 13 件套装孔庙祈福 + 金榜题名券后 9.9 元起领 5 ......
### [「咒术回战0」公开最新商品图](http://acg.178.com/202110/428366504966.html)
> 概要: 近日，剧场版动画「咒术回战0」官方公开了最新商品图，其中绘制了乙骨忧太、五条悟等将登场的角色。「咒术回战」是日本漫画家芥见下下创作的漫画作品，于2018年3月开始连载。根据漫画改编的电视动画由MAPP......
### [真有你的亚马逊 宫野真守出演Amazon视频全新CM](https://acg.gamersky.com/news/202110/1430604.shtml)
> 概要: Amazon Prime Video官方于今日公布了全新CM短片，由日本知名声优宫野真守出演。
### [华为马洪波：目前移动通信领域自动驾驶网络大致在 L2 和 L3 之间](https://www.ithome.com/0/580/892.htm)
> 概要: IT之家10 月 16 日消息，在 2021 全球移动宽带论坛（Global MBB Forum）期间，华为无线 SingleOSS 产品线总裁马洪波指出，通过引入智能化走向网络自治已成为 5G 时代......
### [到2022年底，广东将全面实行免费婚检孕检](https://finance.sina.com.cn/china/dfjj/2021-10-16/doc-iktzqtyu1786757.shtml)
> 概要: 原标题：到2022年底，广东将全面实行免费婚检孕检 据广东卫健委网站消息，近日，广东省卫生健康委、广东省民政厅、广东省妇女儿童工作委员会办公室联合发布《统筹推进免费...
### [北京市智能网联汽车政策先行区开启，百度 Apollo 获无人化道路测试通知书](https://www.ithome.com/0/580/908.htm)
> 概要: IT之家10 月 16 日消息，昨日，北京市智能网联汽车政策先行区（简称政策先行区）正式开放无人化测试场景，并举办《北京市智能网联汽车政策先行区无人化道路测试管理实施细则》（简称《实施细则》）发布会......
### [组图：李易峰超黑look穿搭简约大气 窗边剪影黑白氛围感满满](http://slide.ent.sina.com.cn/star/slide_4_86512_362721.html)
> 概要: 组图：李易峰超黑look穿搭简约大气 窗边剪影黑白氛围感满满
### [“70后”女副市长被18名企业老板“围猎”　被处分后还受贿50万](https://finance.sina.com.cn/china/dfjj/2021-10-16/doc-iktzqtyu1798872.shtml)
> 概要: 来源：政知圈 撰文|熊颖琪 10月16日，微信公众号“廉洁泸州”发布《收受贿赂260多万元，女副市长顶风作案》警示教育片，讲述了“70后”女官员——遂宁射洪市原副市长...
### [安徽自贸试验区揭牌一年来新增注册企业9927家](https://finance.sina.com.cn/china/dfjj/2021-10-16/doc-iktzscyy0037760.shtml)
> 概要: 新华社合肥10月16日电（记者胡锐）记者从近日召开的安徽省政府新闻发布会上获悉，安徽自贸试验区自去年9月24日揭牌以来，新增注册企业9927家，今年前8个月实现进出口额976...
### [Kanidm: A simple, secure and fast identity management platform](https://github.com/kanidm/kanidm)
> 概要: Kanidm: A simple, secure and fast identity management platform
### [视频：刘诗诗卷发高马尾造型元气满满 穿高跟鞋破怀二胎传闻](https://video.sina.com.cn/p/ent/2021-10-16/detail-iktzscyy0040754.d.html)
> 概要: 视频：刘诗诗卷发高马尾造型元气满满 穿高跟鞋破怀二胎传闻
### [超前点播，取消就完事了？](https://finance.sina.com.cn/china/2021-10-16/doc-iktzscyy0044201.shtml)
> 概要: 来源：中国新闻周刊 很多东西都要推倒重来 国庆假期期间，爱奇艺率先宣布取消剧集超前点播，连带着也取消了“会员专属”的贴片广告。腾讯、优酷随后跟进...
### [视频：赵丽颖庆生现场照曝光 发带搭格子短裙俏皮复古](https://video.sina.com.cn/p/ent/2021-10-16/detail-iktzscyy0042652.d.html)
> 概要: 视频：赵丽颖庆生现场照曝光 发带搭格子短裙俏皮复古
### [连马斯克也来“蹭”《原神》？](https://www.huxiu.com/article/464317.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜黄青春题图｜网友PS谁也不会想到，埃隆·马斯克（Elon Musk）与二次元游戏《原神》竟然“暧昧”了起来。10月14日，火箭公司SpaceX首席执行官、特斯拉汽车C......
### [腾讯、红杉又将收获一家IPO公司，估值超270亿](https://www.tuicool.com/articles/IFfaQrA)
> 概要: 【猎云网（微信：）北京】10月16日报道（文/韩文静）近日，圆心科技正式向港交所主板递交上市申请，高盛和中信证券为其联席保荐人。圆心科技是一家医疗科技公司，专注于患者的整个医疗服务周期。自2015年成......
### [杨利伟：空间站建成后将进入不少于 10 年的运营期，未来我国将立足近地空间，去探索更深远的太空](https://www.ithome.com/0/580/924.htm)
> 概要: IT之家10 月 16 日消息，据央视新闻报道，今日，在北京航天飞行控制中心，中国载人航天工程副总设计师杨利伟接受了记者采访。杨利伟在接受采访时表示，三位航天员后面将首先进行生活和通讯设置，例如饮水区......
### [神十三成功径向对接核心舱 离不开这个“功臣”](https://www.tuicool.com/articles/BVbU7vI)
> 概要: 人民网北京10月16日电 （记者周晶）10月16日，在入轨约6.5小时后，神舟十三号载人飞船搭载3名航天员与天和核心舱完成了全自主径向快速交会对接。这是我国载人飞船在太空实施的首次径向快速交会对接。这......
### [澳门：10月19日起，解除关闭部份娱乐场所的特别措施](https://finance.sina.com.cn/china/dfjj/2021-10-16/doc-iktzscyy0047650.shtml)
> 概要: 原标题：澳门：10月19日起，解除关闭部份娱乐场所的特别措施 @澳门特区发布 微博10月16日消息，澳门已连续11天无新增有社区传播风险的新冠肺炎病例...
### [Pizzatime: Throw a pizza party for your remote team](https://www.pizzatime.xyz/)
> 概要: Pizzatime: Throw a pizza party for your remote team
### [《兰陵王》上映8年，集齐了玛丽苏、恶毒女配、痴情男二等因素](https://new.qq.com/omn/20211016/20211016A09A9H00.html)
> 概要: 现在很多剧大家都越来越喜欢什么疯批病娇男，忠犬腹黑的小奶狗，其实早在八年前，就有一部神剧把现在最流行的因素全部都糅合在了一起，那就是由冯绍峰和林依晨主演的古装神剧《兰陵王》，看完这部剧只能说编剧大大的......
### [实探吉利百亿投资下的西安工厂：696台机器人来造车，年产能36万辆](https://www.tuicool.com/articles/EVfi6ze)
> 概要: 编者按：本文来自36氪「未来汽车日报」，（微信公众号ID：auto-time），作者：吴晓宇。来源：未来汽车日报作者 | 吴晓宇编辑 | 王妍四年前，“自主一哥”的生产基地版图拓展至西安。2017年7......
### [“诺兰御用”演员迈克尔·凯恩宣布退休 转型当作家](https://www.3dmgame.com/news/202110/3825911.html)
> 概要: 88岁的英国演员迈克尔·凯恩宣布退出演艺圈，9月17日在加拿大上映的《最佳销售员》（Best Sellers）将是他最后一部电影。《最佳销售员》讲述为了挽救一家精品出版社，一位雄心勃勃的年轻编辑(普拉......
### [演员、导演和老板，“200亿先生“吴京的三面人生](https://new.qq.com/omn/20211016/20211016A09TXL00.html)
> 概要: 作者/翟子瑶编辑/包校千因国庆档《长津湖》与《我和我的父辈》的票房表现，让吴京再次登上热搜。爱国主义色彩的硬汉形象，是吴京近几年在荧幕前集中塑造的主要标签。这个出生于武术世家，自幼习武的“功夫小子”，......
### [视频专栏课 | Python网络爬虫与文本分析](https://www.tuicool.com/articles/zIfeMfr)
> 概要: 课程邀请卡为何要学Python？在科学研究中，数据的获取及分析是最重要的也是最棘手的两个环节！在前大数据时代，一般使用实验法、调查问卷、访谈或者二手数据等方式，将数据整理为结构化的表格数据，之后再使用......
### [神舟十三号成功发射，美国主流媒体刷屏关注，罕见“花式称赞”……](https://finance.sina.com.cn/china/gncj/2021-10-16/doc-iktzqtyu1831143.shtml)
> 概要: 神舟十三号成功发射，美国主流媒体刷屏关注，罕见“花式称赞”…… 来源：冰汝看美国 北京时间10月16日凌晨，神舟十三号载人飞船成功发射，在美国各大主流媒体被刷屏了。
### [杏树意外觉醒最强力量，大佬们表示很好奇，然而这力量却用来唱歌](https://new.qq.com/omn/20211016/20211016A0A7MO00.html)
> 概要: 今年十月新番中，卖歌番甚多，前有SELECTION PROJECT，后有视觉监狱，前者则是偶像选拔，后者则是吸血鬼男团，自从这部番开播之后就有人说是战姬绝唱精神续作，同样都是以“歌声就是力量”为核心点......
### [《DOTA2》新英雄公布 《龙之血》第二季预告](https://ol.3dmgame.com/esports/13529.html)
> 概要: 《DOTA2》新英雄公布 《龙之血》第二季预告
### [打脸来得太快！刚说不会买劳斯莱斯，王思聪就被曝开该品牌带美女](https://new.qq.com/omn/20211016/20211016A0ACBK00.html)
> 概要: 刚怼完劳斯莱斯，王思聪就带俩美女外出，开的依旧是劳斯莱斯——引言。这两天劳斯莱斯翻车的消息，引起了全国车主的热烈讨论。风头出尽的国民老公王思聪，也是送劳斯莱斯上热搜的最佳推手。但刚说完以后不会再买劳斯......
### [唐三对战小白竟然用出黄金昊天锤？没有七位一体，他是如何做到的](https://new.qq.com/omn/20211016/20211016A0AIF300.html)
> 概要: 在斗罗大陆动漫178集中，小白一出场就跟唐三他们打起来了。小白的实力很强大，即便是七怪联手都被她一人吊打。她变成魔魂大白鲨本体，随便一拍就能将六怪给一网打尽了。他们用出武魂真身都没有作用，小白实力碾压......
### [九位华人入榜百大DJ，中国电音新力量](https://new.qq.com/omn/20211016/20211016A0AO8L00.html)
> 概要: 文 | 曾亚丹由DJ Mag评选的2021百大DJ排行10月14日在荷兰阿姆斯特丹AFAS Live揭晓结果，David Guetta连续两年获得第一，Martin Garrix和Armin Van ......
### [你觉得《海贼王》会烂尾吗？](https://new.qq.com/omn/20211016/20211016A0AO5V00.html)
> 概要: 《海贼王》现在火热程度确实还在，不过那是一直以来积攒的口碑，实际上近期尤其和之国篇，剧情水平已经大幅度下降，就连很多铁杆海迷都忍不住吐槽，现在的内容好无聊，进展缓慢主次不清晰。从97年连载至今，可以说......
### [唐三获得海神传承很厉害？唐晨：曾孙，那是我玩剩下的！](https://new.qq.com/omn/20211016/20211016A0AOS600.html)
> 概要: 在斗罗大陆中，获得神祇的传承是一件可遇不可求的机遇，多少封号斗罗苦苦追求一生，也没能得到神的赏识，最终只能带着一腔悲愤和一身修为埋入黄土。而唐三和史莱克七怪之所以成为了传奇，就是因为他们在最后都成功通......
# 小说
### [弑神阁](http://book.zongheng.com/book/1006166.html)
> 作者：王府十一少

> 标签：都市娱乐

> 简介：十五岁，凌羽枫便少小离家，独自闯荡。十八岁，凌羽枫一手创立的弑神阁崛起。二十岁，不灭战神名号震惊全球，凌羽枫带领弑神阁成员，立下无数战功，赫赫威名！

> 章节末：第五百六十五章：大结局5

> 状态：完本
# 论文
### [Generalization in Text-based Games via Hierarchical Reinforcement Learning | Papers With Code](https://paperswithcode.com/paper/generalization-in-text-based-games-via)
> 日期：21 Sep 2021

> 标签：None

> 代码：None

> 描述：Deep reinforcement learning provides a promising approach for text-based games in studying natural language communication between humans and artificial agents. However, the generalization still remains a big challenge as the agents depend critically on the complexity and variety of training tasks.
### [ElasticFace: Elastic Margin Loss for Deep Face Recognition | Papers With Code](https://paperswithcode.com/paper/elasticface-elastic-margin-loss-for-deep-face)
> 日期：20 Sep 2021

> 标签：None

> 代码：None

> 描述：Learning discriminative face features plays a major role in building high-performing face recognition models. The recent state-of-the-art face recognition solutions proposed to incorporate a fixed penalty margin on commonly used classification loss function, softmax loss, in the normalized hypersphere to increase the discriminative power of face recognition models, by minimizing the intra-class variation and maximizing the inter-class variation.
