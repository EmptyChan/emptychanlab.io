---
title: 2021-05-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LimerickDay_EN-CN2355968409_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-12 22:17:50
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LimerickDay_EN-CN2355968409_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [《2021中国开源发展蓝皮书》发布](https://www.oschina.net/news/141137)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>开源软件以开放、共享、协同的新型生产方式，成为全球信息技术发展的强大推动力。回顾过去的一年，开源在全世界范围内迎来了新时期的大发展。来自......
### [Antrea 加入 CNCF 沙箱](https://www.oschina.net/news/141213/antrea-joins-cncf-sandbox)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>今天，我们很高兴地宣布，CNCF TOC 已经接受 Antrea 作为沙箱项目。这对于刚刚发布 1.0 版本的 Project Antr......
### [Rust 2021 版本计划发布](https://www.oschina.net/news/141124/rust-2021-edition-plan)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Rust 开发团队宣布Rust 语言的第三个版本 Rust 2021 计划于今年 10 月发布。Rust 1.0 的发布确立了 "稳定前......
### [网易有道 ASR 团队斩获 Interspeech 2021 算法竞赛两项冠军](https://www.oschina.net/news/141214/asr-interspeech-2021)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>在近期举行的Interspeech 2021会议上，主办方开展了“非母语儿童语音识别”的特别会议，并发布专项数据集用于算法评测竞赛，旨在......
### [视频：逼迫脱团单飞？吴青峰称苏打绿休团是林暐哲逼的](https://video.sina.com.cn/p/ent/2021-05-12/detail-ikmyaawc4742768.d.html)
> 概要: 视频：逼迫脱团单飞？吴青峰称苏打绿休团是林暐哲逼的
### [轻量级工具Vite到底牛在哪, 一文全知道](https://segmentfault.com/a/1190000039985669?utm_source=sf-homepage)
> 概要: 转载请注明出处：葡萄城官网，葡萄城为开发者提供专业的开发工具、解决方案和服务，赋能开发者。时下大热的vue框架又来了新开发环境构建工具——Vite,今天我们一起来了解一下这个新成员。背景与工作方式在过......
### [AirGradient: DIY Air Quality Monitoring with Open-Source Hardware & Firmware](https://www.airgradient.com/diy/)
> 概要: AirGradient: DIY Air Quality Monitoring with Open-Source Hardware & Firmware
### [全球倒数，生育率从6到1.3，中国经历了什么？](https://www.huxiu.com/article/427252.html)
> 概要: 本文来自微信公众号：八点健闻（ID：HealthInsight），作者：方澍晨、于焕焕，责编：徐卓君、陈思，陈鑫对此文亦有贡献，头图来自：视觉中国万众瞩目的中国第七次人口普查数据终于出炉。此前，有舆论......
### [组图：杨钰莹50岁生日收大束心形玫瑰 捧脸卖萌自称只有25](http://slide.ent.sina.com.cn/y/w/slide_4_704_356494.html)
> 概要: 组图：杨钰莹50岁生日收大束心形玫瑰 捧脸卖萌自称只有25
### [组图：48岁郭德纲穿大裤衩配黑布鞋排练 动作灵活显可爱](http://slide.ent.sina.com.cn/star/slide_4_704_356497.html)
> 概要: 组图：48岁郭德纲穿大裤衩配黑布鞋排练 动作灵活显可爱
### [给许知远和易立竞写段子的人](https://www.huxiu.com/article/427046.html)
> 概要: 本文来自微信公众号：贵圈-腾讯新闻（ID：entguiquan），作者：韦洛昔，头图来自：《吐槽大会》截图四月下旬，脱口秀演员Rock在阿那亚演出，路过小镇上的单向街书店。两个月前，在《吐槽大会》的舞......
### [1600元!《高达》扎古头部变绿色烧水壶 真脑洞周边](https://acg.gamersky.com/news/202105/1387341.shtml)
> 概要: 以《机动战士高达》扎古头部为灵感的铁水壶推出了新品，这次的是绿色扎古茶壶。售价27500日元含税，合人民币约1622元，6月正式发售。
### [《最终幻想7：重制版》预算高达1.4亿美元](https://www.3dmgame.com/news/202105/3814244.html)
> 概要: 根据杰富瑞投资银行经理Atul Goyal，SE《最终幻想7：重制版》预算高达1.4亿美元。据外媒消息，《最终幻想7》原版游戏制作费用4000万美元，考虑到当时是上世纪90年代制作，这已经是一个不小的......
### [苹果在英国遭集体诉讼 对用户收费过高或赔21亿美元](https://www.3dmgame.com/news/202105/3814245.html)
> 概要: 近日据外媒彭博社报道，苹果在英国伦敦遭集体诉讼。诉讼指出苹果在应用商店App Store的购买交易过程中，向2000万英国用户收取的费用过高。对苹果而言，这又是一起让其头疼的法律纠纷。原告方表示苹果收......
### [我在上海买新房的血泪史](https://www.huxiu.com/article/427357.html)
> 概要: 2021年，对上海购房人而言很特殊。二手房的涨幅，房东的疯狂“跳价”，让新房市场愈发火爆。随着积分摇号政策的出台，每个“打新者”都成了热锅上的蚂蚁。从此，买新房不再拼资产、拼运气，而是拼积分。一条发出......
### [宫原拓也为「白箱」矢野艾莉卡绘制的画像公开](http://acg.178.com//202105/414788046934.html)
> 概要: 近日，动画业界人士宫原拓也社交媒体上公开了自己为「白箱」矢野艾莉卡绘制的画像，服装主题为女仆装！宫原拓也曾以作画监督的身份参与过「美妙射击社」、「刀剑神域外传 Gun Gale Online」和「弹丸......
### [Enhancing Photorealism Enhancement](https://intel-isl.github.io/PhotorealismEnhancement/)
> 概要: Enhancing Photorealism Enhancement
### [乐高《老友记》公寓套装5月上市 可还原经典场景](https://www.3dmgame.com/news/202105/3814248.html)
> 概要: 目前，乐高官方公布了《老友记》公寓套装组合，玩家可以通过该组合还原剧中的各种场景，此次套装组合预计将于2021年5月19日上市，售价149.99美元，国行版定价 1399 元。宣传影像：据悉，此次乐高......
### [腾讯QQ上线全新小黄脸表情！“狗头”也变绿了](https://www.3dmgame.com/news/202105/3814250.html)
> 概要: 近日腾讯QQ又上线了一批全新的小黄脸表情，包括拒绝、嫌弃、打Call、变形、仔细分析、崇拜、比心、庆祝等。一同上线的还有绿色狗头表情“菜狗”，这表情特别突出，动态更搞笑!“菜狗”一般是指玩游戏玩的太烂......
### [渡濑悠宇漫画「革神语」恢复连载](http://acg.178.com//202105/414790241496.html)
> 概要: 曾因作者身体问题，以及早期编辑与作者意向不和等原因，暂停连载5年半的漫画「革神语」将恢复连载。暂停连载期间，作者渡濑悠宇重新绘制了漫画前12卷剧情，恢复连载之后是基于重绘版剧情进行的后续内容......
### [「画江湖之换世门生」第二季今日播出  终极海报发布](http://acg.178.com//202105/414791509332.html)
> 概要: 今日（5月12日），「画江湖之换世门生」第二季正式播出，官方还发布了终极海报。「画江湖之换世门生」是北京若森数字联合钟氏出品的一部近现代三维动画，是「画江湖」系列的第四部作品，讲述了无宗会的念家二少爷......
### [时隔五年半！渡濑悠宇的漫画《革神语》连载再开！](https://news.dmzj1.com/article/70859.html)
> 概要: 渡濑悠宇的漫画《革神语》时隔五年半将于5月19日发售的《周刊少年Sunday》25号上连载再开！
### [《PROMISE·CINDERELLA》最新刊与特别PV公开！](https://news.dmzj1.com/article/70860.html)
> 概要: 橘幸子《PROMISE·CINDERELLA》最新卷发售，公开特别PV
### [漫画杂志「周刊少年Magazine」24号封面公开](http://acg.178.com//202105/414798990856.html)
> 概要: 漫画杂志「周刊少年Magazine」24号封面图正式公开，封面漫画为新连载「英戦のラブロック」，该漫画的第一话彩页也一并公开。「周刊少年Magazine」是讲谈社出版的少年漫画杂志，于每周三发售......
### [北山宏光被拍约会内田理央 恋情曝光一年进展顺利](https://ent.sina.com.cn/s/j/2021-05-12/doc-ikmxzfmm1999384.shtml)
> 概要: 北山宏光被拍约会内田理央 恋情曝光一年进展顺利
### [时隔三年！舞台剧《灵能百分百》第三弹上演](https://news.dmzj1.com/article/70862.html)
> 概要: 根据ONE原作改编的舞台剧《灵能百分百》第三弹《激突！爪第7支部》确定将于2021年8月6日~15日期间在东京上演。
### [《射雕英雄传》出全了英文版，国外读者并不买账？](https://www.huxiu.com/article/427402.html)
> 概要: 本文来自微信公众号：界面文化（ID：BooksAndFun），作者： 姚冰淳，编辑：姜妍，原文标题：《历时四年<射雕英雄传>出全了英文版，但国外读者似乎并不买账》，头图来自：《射雕英雄传》在金庸去世两......
### [How image search works at Dropbox](https://dropbox.tech/machine-learning/how-image-search-works-at-dropbox)
> 概要: How image search works at Dropbox
### [Algorithmic fatigue](https://www.reaktor.com/blog/algorithmic-fatigue-is-real-heres-how-to-combat-it/)
> 概要: Algorithmic fatigue
### [剧本杀到底还能活多久？我们和店家、玩家、平台、IP方聊了聊](https://www.tuicool.com/articles/n2QFb2u)
> 概要: 编者按：本文来自微信公众号“深响”（ID:deep-echo），作者：静林，36氪经授权发布。2014年，何探长偶然在一家密室逃脱店的墙上看到了一幅《死穿白》的海报。剧本杀此时还叫“谋杀之谜”，全国流......
### [谁来为 HTC 的万元 VR 一体机买单](https://www.ithome.com/0/551/059.htm)
> 概要: 5 月 12 日，HTC 在 HTC VIVE 虚拟生态大会（V²EC 2021）上官宣发布了 VR 一体机新品 VIVE FOCUS 3 和 PC VR 新品 VIVE PRO 2。值得注意的是，H......
### [腾讯公开“自动驾驶方法”相关专利：可提高低功耗下自动驾驶控制的实时性](https://www.ithome.com/0/551/062.htm)
> 概要: IT之家5 月 12 日消息 企查查 App 显示，5 月 11 日，腾讯科技（深圳）有限公司公开“基于人工智能的自动驾驶方法、装置、设备、介质及车辆”专利，公开号为 CN112784885A。专利说......
### [分享一些MSI小组赛有趣的数据](https://bbs.hupu.com/42848200.html)
> 概要: 1、DFM以83.3%的一血率力压75%的RNG位居榜首；2、在场均时长上，四大赛区有三支挤进了前四，只有RNG以25分钟排名倒数第一，与外卡赛区有来有回固然丢脸，不过侧面也说明四大赛区的队伍在前期陷
### [风力发电机上有避雷针？谁能告诉我它藏哪了](https://www.tuicool.com/articles/AvUzIv6)
> 概要: 本文来自微信公众号：科学大院（ID：kexuedayuan），作者：城明辰，中国科学院计算机网络信息中心监制，头图来自：视觉中国近期，受强对流天气影响，南方大范围地区遭到暴雨、大风和雷电袭击。高高的建......
### [《天竺鼠车车》软糯质感手办开始预约 7月正式发售](https://news.dmzj1.com/article/70867.html)
> 概要: 《天竺鼠车车》的软糯质感手办决定于2021年7月在购物网站vvstore上发售，售价为每只1408日元含税（约合人民币83元），今天开始接受预约。
### [被向佐Diss后，药水哥吓得直接“下跪”道歉？还学狗叫：放过我吧](https://new.qq.com/omn/20210512/20210512A0CO3R00.html)
> 概要: 自从药水哥和向佐这个事情爆发之后，热搜都是充满一股欢乐的气息。事情的缘由也是非常简单，药水哥为了装逼在喊话李连杰老师的时候，被网友带偏了方向。网友在评论中留言药水哥欺负老年人，你可能不知道他的徒弟是谁......
### [一鞋两穿，爱登堡男士防滑厚底真皮凉拖鞋 39 元（减 50 元）](https://lapin.ithome.com/html/digi/551101.htm)
> 概要: 一鞋两穿，爱登堡男士防滑厚底真皮凉拖鞋报价 89 元，限时限量 50 元券，实付 39 元包邮，领券并购买。使用最会买 App下单，预计还能再返 8.58 元，返后 30.42 元包邮，点击下载最会买......
### [上市热潮背后，互联网医疗赛道迎变局？](https://www.tuicool.com/articles/6vQFzqI)
> 概要: 编者按：本文来自微信公众号“向善财经”（ID:IPOxscj），作者：向善财经，36氪经授权发布。近日，新浪科技发布小道消息，称医联计划于6月份递交招股书申请IPO。在医联之前，已经有好几家互联网医疗......
### [演艺人员向粉丝进行商业集资将受行业自律惩戒](https://ent.sina.com.cn/s/m/2021-05-12/doc-ikmxzfmm2058151.shtml)
> 概要: 新浪娱乐讯 5月12日，中国演出行业协会针对近期个别综艺选秀节目中出现为支持偶像“出道”而产生的“集资打投”、“倒奶事件”等乱象，暴露出过度消费、非理性“应援”等问题发布公告：　　由演出行业演艺人员本......
### [太嚣张！林更新现在已经自称十九亿少女的梦，多出的十亿有你吗](https://new.qq.com/omn/20210512/20210512A0CY4U00.html)
> 概要: 真有你的。最近，林更新入驻了快手平台，账号的名称竟然是十九亿少女的梦。            其实早在之前的采访中，林更新就表示自己现在是19亿少女的梦，不再是9亿少女的梦。不愧是林更新，就是这么自信......
### [DeFi新玩法 | 一文了解新型算法稳定币Malt](https://www.tuicool.com/articles/26FFBvM)
> 概要: 在过去的一年中，出现了许多新的算法稳定币。这些项目中的大多数要么将其代币锚定某个实际价值，例如1美元（例如ESD，以及最近的FEI），要么不挂钩但相对稳定，例如RAI和OHM。这些项目的目标是提供一种......
### [张雪迎发长文回应姐姐骂粉丝，全篇袒护姐姐让粉丝换位思考，令人心寒](https://new.qq.com/omn/20210512/20210512A0D4K300.html)
> 概要: 最近张雪迎的姐姐引发了争议，姐姐是张雪迎的经纪人，在群里发了很多负能量的话，还骂了正在生病的粉丝，时隔一天，张雪迎也发长文回应了自己姐姐的事情。            张雪迎称自己的工作比较慢，事情当......
### [vivo TWS 2 真无线降噪耳机现身天猫，5 月 20 日开启预售](https://www.ithome.com/0/551/107.htm)
> 概要: IT之家5 月 12 日消息 vivo 近日在天猫旗舰店上线了 TWS 2 真无线降噪蓝牙耳机的预约页面，该耳机将于5 月 20 日开启预售，6 月 1 日正式开售。从图片可以看出，该耳机采用了入耳式......
### [最高法：加强反垄断审判工作 维护公平竞争市场秩序](https://finance.sina.com.cn/china/bwdt/2021-05-12/doc-ikmxzfmm2071040.shtml)
> 概要: 最高法：加强反垄断审判工作 维护公平竞争市场秩序 财联社5月12日讯，今日上午，最高人民法院召开反垄断审判工作专家座谈会，最高人民法院党组书记、院长周强强调...
### [最高法：加强反垄断审判工作 维护公平竞争市场秩序](https://finance.sina.com.cn/china/2021-05-12/doc-ikmyaawc4937495.shtml)
> 概要: 原标题：最高法：加强反垄断审判工作 维护公平竞争市场秩序 中新经纬客户端5月12日电 据最高人民法院官网消息，5月12日上午，最高人民法院召开反垄断审判工作专家座谈会...
### [流言板北京击败新疆晋级决赛阶段，新疆将参加附加赛](https://bbs.hupu.com/42852432.html)
> 概要: 虎扑05月12日讯 全运会男篮U22预赛第5个比赛日，北京69-61战胜新疆，预赛取得4胜1负的战绩，晋级决赛阶段。新疆预赛2胜3负，将参加附加赛，与吉林、天津、河南、福建、河北争夺最后一个晋级决赛阶
### [网信办发布征求意见稿，汽车数据安全立规](https://finance.sina.com.cn/china/bwdt/2021-05-12/doc-ikmxzfmm2074267.shtml)
> 概要: 原标题：网信办发布征求意见稿，汽车数据安全立规 5月12日，国家互联网信息办公室（下称“国家网信办”）发布《汽车数据安全管理若干规定（征求意见稿）》（下称“意见稿”）...
### [M2增速同比大降3个百分点！社融增量仅1.85万亿，4月信贷社融不及预期，有何深意？这些细节值得关注](https://finance.sina.com.cn/china/2021-05-12/doc-ikmyaawc4937971.shtml)
> 概要: 一季度信贷“开门红”效应消退后，疫情之下的高基数效应开始显现，使得4月金融数据低于预期。 5月12日，人民银行发布的4月金融数据和社会融资统计数据显示...
### [流言板英雄联盟赛事数据发布MSI第六日最佳选手：C9.Perkz](https://bbs.hupu.com/42852709.html)
> 概要: 虎扑05月12日讯 英雄联盟赛事数据发布MSI第六日最佳选手。原文如下：【MSI Day6 最佳选手：C9.Perkz】首局Perkz在队伍劣势时，面对强大对手DK临危不乱，在前期被针对的情况下敢打敢
### [到手的房子“飞了”、有人可能要“进去了”！南京严打“假人才真炒房”](https://finance.sina.com.cn/china/dfjj/2021-05-12/doc-ikmyaawc4942305.shtml)
> 概要: 到手的房子“飞了”、有人可能要“进去了”！南京严打“假人才真炒房” 每经记者 包晶晶 “上海单身买房太难了，不如落户杭州。” “杭州落户不划算，房子都是万人摇不好买...
### [速汇金与Coinme达成合作，允许用户在速汇金实体网点买卖比特币](https://www.btc126.com//view/164610.html)
> 概要: 据Decrypt消息，周三，美国大型汇款公司速汇金宣布与比特币ATM公司Coinme建立合作伙伴关系，将允许用户在速汇金的实体网点买卖比特币。这项合作将使美国可以购买比特币的实体网点数量增加近一倍。据......
### [安领国际拟透过证券型代币发行集资](https://www.btc126.com//view/164614.html)
> 概要: 据格隆汇消息，安领国际(01410.HK)公告，公司正在筹备由公司的直接全资附属公司Green Radar Holdings Limited进行拟议证券型代币发行。GR Holdings进行拟议发行旨......
### [美股开盘，三大股指集体低开](https://www.btc126.com//view/164615.html)
> 概要: 美股开盘，三大股指集体低开，道指跌1.33%，纳指跌0.47%，标普500指数跌0.72%......
### [国产剧迷惑行为又来了](https://new.qq.com/omn/20210512/20210512A0F2N000.html)
> 概要: 全新解锁更多好玩的版块~~~            昨天和大家分析了《遇龙》，除去主演爆笑的演技，剧本逻辑也拉垮。但是，今天刷到了一部剧，台词的震惊程度让小剧觉得《遇龙》的台词起码还算正常。     ......
### [太空题材建造游戏《基地一号》现已在Steam发售](https://www.3dmgame.com/news/202105/3814294.html)
> 概要: 太空基地题材建造游戏《基地一号》现已在Steam推出，国区售价75元，现发售特惠，售价60元，自带简体中文。截止到目前《基地一号》在Steam上的评价为“褒贬不一”。《基地一号》是一款带有RPG元素的......
### [生态环境部召开长江、渤海入河入海排污口整治工作推进会](https://finance.sina.com.cn/china/2021-05-12/doc-ikmxzfmm2078065.shtml)
> 概要: 原标题：生态环境部召开长江、渤海入河入海排污口整治工作推进会 证券时报网12日讯，近日，生态环境部以视频方式召开长江、渤海入河入海排污口整治工作推进会...
### [流言板天津、吉林、新疆、河南、福建、河北参加附加赛](https://bbs.hupu.com/42853136.html)
> 概要: 虎扑05月12日讯 全运会男篮U22预赛最后一个比赛日结束，天津、吉林、新疆、河南、福建、河北6支球队晋级附加赛阶段。附加赛将于5月15日开始，附加赛获得第1名的球队将晋级决赛阶段，第2-5名为本届全
### [HT突破39 USDT](https://www.btc126.com//view/164622.html)
> 概要: 火币行情显示，HT突破39USDT，现报39.0001 USDT，24H涨幅16.84%......
# 小说
### [变成血族是什么体验](https://m.qidian.com/book/1015525869/catalog)
> 作者：神行汉堡

> 标签：都市生活

> 简介：“变成吸血鬼是什么体验？”向坤没想到，这个不久前在知乎上被他把答案当成故事来看的问题，现在居然可以用亲身经历来回答了。

> 章节总数：共678章

> 状态：完本
# 论文
### [BUDA: Boundless Unsupervised Domain Adaptation in Semantic Segmentation](https://paperswithcode.com/paper/buda-boundless-unsupervised-domain-adaptation)
> 日期：2 Apr 2020

> 标签：DOMAIN ADAPTATION

> 代码：https://github.com/valeoai/buda

> 描述：In this work, we define and address "Boundless Unsupervised Domain Adaptation" (BUDA), a novel problem in semantic segmentation. BUDA set-up pictures a realistic scenario where unsupervised target domain not only exhibits a data distribution shift w.r.t.
### [A Single Frame and Multi-Frame Joint Network for 360-degree Panorama Video Super-Resolution](https://paperswithcode.com/paper/a-single-frame-and-multi-frame-joint-network)
> 日期：24 Aug 2020

> 标签：SUPER RESOLUTION

> 代码：https://github.com/lovepiano/SMFN_For_360VSR

> 描述：Spherical videos, also known as \ang{360} (panorama) videos, can be viewed with various virtual reality devices such as computers and head-mounted displays. They attract large amount of interest since awesome immersion can be experienced when watching spherical videos.
