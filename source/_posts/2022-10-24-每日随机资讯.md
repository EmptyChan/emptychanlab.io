---
title: 2022-10-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MarienburgZell_ZH-CN4562312386_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-24 21:53:40
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MarienburgZell_ZH-CN4562312386_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [SegmentFault 思否正式开源问答社区软件 Answer](https://segmentfault.com/a/1190000042672978)
> 概要: 今日，SegmentFault 思否团队正式对外开源问答社区软件 Answer。你可以免费使用 Answer 高效地搭建一个问答社区，并用于产品技术问答、客户支持、用户交流等场景。作为国内领先的新一代......
### [让AI学会“发自内心的笑”，需要几步？](https://www.woshipm.com/ai/5652963.html)
> 概要: 还记得谷歌的软件工程师Lemoine声称谷歌的AI拥有知觉力这件事吗？这件事引起了巨大的舆论争议，可能也象征着人工智能的研究来到分岔口：我们到底是该设计一个只会算法非常蠢笨没有情绪的AI，还是设计一个......
### [塞利格曼（PERMA）模型：令人难忘的体验框架](https://www.woshipm.com/user-research/5653666.html)
> 概要: 怎样的用户体验才是令人难忘的？本篇文章介绍了一个令人难忘的体验框架，这个框架基于正向心理学，最初是为旅游而开发的，但也可以应用到产品设计中，快来看看它究竟是怎样的吧。你的公司可能有一些产品策略文档，其......
### [写文案，能细则细！](https://www.woshipm.com/copy/5653097.html)
> 概要: 不同程度的文案会让阅读者有着不同的感受，抽象的文字千篇一律，文案的细节万里挑一。文案的细节往往是最打动人的，那么该如何写出细节化的文案？作者分享了一些自己的方法。有2条文案：文案一：我们从铁观音故乡的......
### [编译openWrt](https://segmentfault.com/a/1190000042674263)
> 概要: 注意不要用 root 用户进行编译国内用户编译前最好准备好梯子默认登陆IP 192.168.1.1 密码 password编译命令首先装好 Linux 系统，推荐 Debian 11 或 Ubuntu......
### [快递业大并购：京东、极兔突围 中通、圆通防守](https://finance.sina.com.cn/tech/internet/2022-10-24/doc-imqmmthc1885338.shtml)
> 概要: 21世纪经济报道 贺泓源 北京报道......
### [Vue.js 组件编码规范](https://segmentfault.com/a/1190000042676743)
> 概要: Vue.js 组件编码规范目标本规范提供了一种统一的编码规范来编写Vue.js代码。这使得代码具有如下的特性：其它开发者或是团队成员更容易阅读和理解。IDEs 更容易理解代码，从而提供高亮、格式化等辅......
### [大屏可视化图形展示二](https://segmentfault.com/a/1190000042677599)
> 概要: 话不多说，上图上代码一、环形动态旋转图分为两部分，左边的echarts图和中间动画效果echarts配置代码export const types = function (data;value) {  ......
### [顽皮狗：现在和将来的游戏 会继续登陆PC](https://www.3dmgame.com/news/202210/3854433.html)
> 概要: 上周顽皮狗在PC上完成了首秀，其作品《神秘海域：盗贼遗产合集》登陆了Steam和Epic，当然今后还会有更多游戏登陆PC。《最后的生还者1》正在移植到PC平台，顽皮狗有意将更多游戏移植到PC。在Pla......
### [马斯克身家今年已蒸发1100多亿美元，超谷歌谢尔盖总身家](https://finance.sina.com.cn/tech/it/2022-10-24/doc-imqqsmrp3544439.shtml)
> 概要: 新浪科技讯 北京时间10月24日消息，今年特斯拉股价下跌，马斯克身家已经蒸发1100多亿美元，比谷歌联合创始人谢尔盖·布林（Sergey Brin）总身家1070亿美元还多......
### [B站加码直播带货首战双11：上线购物专区，全量放开“小黄车”](https://finance.sina.com.cn/tech/internet/2022-10-24/doc-imqqsmrp3547152.shtml)
> 概要: 文|翟元元......
### [国产特斯拉降价：Model 3起售价26.59万，Model Y起售价28.89万](https://finance.sina.com.cn/tech/it/2022-10-24/doc-imqmmthc1883666.shtml)
> 概要: 新浪科技讯 10月24日早间消息，据特斯拉官方微博，即日起，中国大陆地区特斯拉在售Model 3 及Model Y 售价调整，调整后Model 3车型起售价为265900元人民币；特斯拉Model Y......
### [焦虑的职场人，把读MBA当解药](https://www.huxiu.com/article/693658.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：王敏，编辑：向小园，题图来自：视觉中国陷入职场焦虑的打工人，开始想靠MBA“逆天改命”。MBA，即工商管理硕士，拥有一定工作经验的......
### [动画《鲁邦三世Zero》12月开播：少年鲁邦的故事](https://acg.gamersky.com/news/202210/1529631.shtml)
> 概要: 由大河内一楼编剧的《鲁邦三世》完全新作动画《鲁邦三世：Zero》正式公开，全6话，将于2022年12月开播。
### [【Z-UNIVERSE】](https://www.zcool.com.cn/work/ZNjI1MTA0NTI=.html)
> 概要: 作为小Z原创者之一借站酷16周年之际探索小Z更多的可能性赋予更多的造型、性格、故事、他们都在不同的宇宙中创造超前的未来！我称之为Z-UNIVERSE......
### [“金”喜人人可领！领峰贵金属优惠赠金与您共享狂欢时刻](http://www.investorscn.com/2022/10/24/103660/)
> 概要: 美国9月CPI超预期至8.2%，金价近半个月以来连连下探。高通胀紧压下美联储或将再度加息，黄金多头或将遭遇惨烈冲击?聚焦最近黄金行情，领峰贵金属(http://alingfeng.top/d8bGNh......
### [《山河伏妖录》DLC“山河苍生”发售 新角色新门派](https://www.3dmgame.com/news/202210/3854450.html)
> 概要: 多人联机策略冒险游戏《山河伏妖录》新DLC“山河苍生”现已在Steam正式发售，国区定价6元，内容包含了两个新可用角色：孟钟和木柯，以及多个新门派和武功。Steam商店页面：点击此处新可用角色孟钟：背......
### [SPG](https://www.zcool.com.cn/work/ZNjI1MTE4MjQ=.html)
> 概要: _无题_PS孝康作品（SPG）2022.10.24......
### [《明日方舟：黎明前奏》正式PV 10月29日B站独播](https://acg.gamersky.com/news/202210/1529687.shtml)
> 概要: 游戏改编动画《明日方舟：黎明前奏》正式PV已于昨晚正式公开。
### [BBC长寿科幻剧《神秘博士》预告，舒提·盖特瓦成首位黑人主演](https://new.qq.com/rain/a/20221024A02D6H00)
> 概要: 新京报讯 10月24日，由英国BBC出品的科幻电视剧《神秘博士》第14季发布预告，将于2023年开播。舒提·盖特瓦将饰演第14任博士，成为该剧首位黑人博士，“多娜”（《神秘博士》剧中人物，在第四季陪博......
### [9月全球热门手游收入排行 《王者》第一《原神》第二](https://www.3dmgame.com/news/202210/3854458.html)
> 概要: 今日(10月24日)Sensor Tower公布9月全球热门移动游戏收入排行榜，腾讯《王者荣耀》位居全球第一，米哈游《原神》位居第二。今年9月份全球手游玩家在App Store和Google Play......
### [商业插画&海报设计 之三](https://www.zcool.com.cn/work/ZNjI1MTMxNjQ=.html)
> 概要: 好久没发小插画，趁年底发一小波谢谢观赏点赞哦......
### [《海贼王》104卷单行本封面公开 五档路飞占据C位](https://acg.gamersky.com/news/202210/1529693.shtml)
> 概要: 《海贼王》漫画104卷单行本封面高清版公开，五档路飞、桃之助粉色的龙和哭泣的大和构成了这次的封面。
### [《每天和每天不一样》系列第36辑](https://www.zcool.com.cn/work/ZNjI1MTM1MzI=.html)
> 概要: 努力每天一副偶尔会天窗但是还在坚持的每天系列就这样悠哉悠哉的画到了680副了啦！10月的天气是北方难得的清爽，天高云淡，不湿不燥。每天还是在不停的画着，治愈着自己有点浮躁的心情。吃些应季的水果，总是会......
### [组图：刘诗诗携吴奇隆与剧组人员聚餐 一路牵手搂腰显甜蜜恩爱](http://slide.ent.sina.com.cn/star/w/slide_4_704_376929.html)
> 概要: 组图：刘诗诗携吴奇隆与剧组人员聚餐 一路牵手搂腰显甜蜜恩爱
### [几十个人看直播，TikTok带货“无能”](https://www.huxiu.com/article/693674.html)
> 概要: 燃次元（ID:chaintruth）原创，作者：侯燕婷，编辑：饶霞飞，题图来自：Dave拍摄复刻了抖音电商模式，TikTok电商在海外看上去势头很猛，但其直播带货效果不佳。从去年2月开始，TikTok......
### [全片80%的演员由盲人出演，惊艳了柏林电影节](https://new.qq.com/rain/a/20221024V03OIQ00)
> 概要: 全片80%的演员由盲人出演，惊艳了柏林电影节
### [鲁豫翻身记：领笑员，也没那么“高危”嘛！](https://new.qq.com/rain/a/20221024A03SZO00)
> 概要: 今年娱乐圈最“高危”的职业要数《脱口秀大会》里的领笑员了。周迅，那英……之前颇有口碑的明星纷纷在这里折戟翻车。可偏偏网上受到群嘲最多的鲁豫，在领笑员的岗位上扳回一城。            从废话大王......
### [《明日方舟：黎明前奏》动画版预告 10月28日开播](https://www.3dmgame.com/news/202210/3854473.html)
> 概要: 根据人气游戏改编，《明日方舟：黎明前奏》动画版正式预告第二弹公开，本作预定 10月28日开播， 10 月 29 日在 Bilibili 独家网上发布，敬请期待。• 电视动画《明日方舟：黎明前奏》改编自......
### [“鸟人”当红，安踏有志于新中产？](https://www.huxiu.com/article/693987.html)
> 概要: 出品｜虎嗅商业消费组作者｜昭晰编辑｜苗正卿题图｜视觉中国“未来，始祖鸟在经营策略上会全面对标爱马仕，买经典款需要配货。”安踏集团8月的一场活动上，一名高管兴致勃勃地宣布道。始祖鸟，价格逼近奢侈品的户外......
### [领峰贵金属是什么网站？黄金投资业界领先平台](http://www.investorscn.com/2022/10/24/103669/)
> 概要: 领峰贵金属自建立以来一直稳居行业领军地位，为全球投资人提供贵金属、大宗商品的一站式投资服务。平台以专业、诚信、高效闻名业内，黄金、白银产品交易成本业内至优，多空双向投资操作方便快捷，且平台所有交易均受......
### [“香港论坛·科学建未来”圆满结束，多位科学家共同探讨未来科学](http://www.investorscn.com/2022/10/24/103670/)
> 概要: “香港论坛·科学建未来”圆满结束，多位科学家共同探讨未来科学
### [视频：翟潇闻片场模仿狗仔配音 语气生动有趣逗笑众人](https://video.sina.com.cn/p/ent/2022-10-24/detail-imqqsmrp3603654.d.html)
> 概要: 视频：翟潇闻片场模仿狗仔配音 语气生动有趣逗笑众人
### [双面绒暖暖的：Kappa 家居服 99 元 5 折再发车（京东 199 元）](https://lapin.ithome.com/html/digi/648551.htm)
> 概要: 天猫【Kappa 内衣旗舰店】Kappa 双面绒家居服日常售价为 309 元，下单领取 210 元优惠券，到手价为 99 元。天猫Kappa 双面绒家居服男 / 女款可选券后 99 元领 210 元券......
### [8万就能买房，不是在鹤岗](https://www.huxiu.com/article/693951.html)
> 概要: 本文来自微信公众号：新周刊 （ID：new-weekly），作者：刘婷，编辑：晏非，头图来自：视觉中国2022年，法拍房成了楼市热点。互联网浪潮下，昔日非标产品已完成教育市场的过程，成为普通购房者触手......
### [欧阳娜娜干爹再婚！40岁TAE晒婚礼现场照幸福洋溢，曾被赞为泰国林志颖](https://new.qq.com/rain/a/20221024A05U1S00)
> 概要: 10月24日，泰国演员TAE（唐宸禹），也就是欧阳娜娜的干爹，分享了自己在22日举办的婚礼的现场照，并感恩道道：Thank God for Blessing us。            TAE今年已......
### [亨达外汇：特拉斯火速辞职，英镑再次承压](http://www.investorscn.com/2022/10/24/103673/)
> 概要: 亨达外汇为亨达集团旗下提供金融业务的投资窗口，致力为来自全球各地的直接投资华人提供优质的外汇、贵金属及差价合约(CFD)等产品的买卖服务，同时亨达外汇平台还有专业的分析师助阵，为投资者提供市场信息分析......
### [NVIDIA害惨了游戏党？他们才是被老黄坑怕的人！](https://www.3dmgame.com/news/202210/3854484.html)
> 概要: 看着最近老黄发布的 RTX 40 系显卡，等等党好像还是没有迎来曙光。你看看，已经有不少网友在网上给 AMD 提建议，让他们搞快点，早点干掉NVIDIA了。过去几年，游戏佬的日子真的是挺折磨的。先是2......
### [TCL科技前三季盈利19.5亿元，面板新一轮涨价已启](http://www.investorscn.com/2022/10/24/103676/)
> 概要: 10月24日晚间,TCL科技(000100.SZ)发布2022年三季报。报告期内,公司实现营业收入1265.1亿元,净利润19.5亿元,归属于上市公司股东净利润2.8亿元,经营现金流净额126.6亿元......
### [10月24日这些公告有看头](https://www.yicai.com/news/101571600.html)
> 概要: 10月24日晚间，多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [同方计算机（清华同方）加入开放麒麟 openKylin 社区](https://www.ithome.com/0/648/592.htm)
> 概要: IT之家10 月 24 日消息，据开放麒麟 openKylin 社区官方消息，近日，同方计算机有限公司签署了 openKylin 社区 CLA (Contributor License Agreeme......
### [北向净流出180亿、港股跌超6%，外资卖压还有多大？](https://www.yicai.com/news/101571689.html)
> 概要: 过去两周，国际机构的卖出速度有所加快，不过总体仓位已到了较低位置。
### [全球金属期货迎来大抄底，铜铝镍锡谁最“热门”？](https://www.yicai.com/news/101571674.html)
> 概要: 供给端持续扰动
### [我们使用量子隧穿来感受气味？](https://www.ithome.com/0/648/596.htm)
> 概要: 当你呼吸时，气味分子会被吸入鼻子，然后被一层粘液捕获，最后被带到鼻腔顶部一个称为嗅觉上皮的区域。这个区域包含特殊的受体位点，这些受体位点可以检测分子并向大脑发送信号。我们知道，我们对气味的感觉是由气味......
### [苏泊尔：前三季度净利润 13.09 亿元，同比增长 5.47%](https://www.ithome.com/0/648/597.htm)
> 概要: IT之家10 月 24 日消息，浙江苏泊尔股份有限公司今日发布 2022 年第三季度报告，前三季度净利润 13.09 亿元，同比增长 5.47%；三季报拟 10 派 12.5 元，合计派发现金股利 1......
### [逆势见真金，这些军工股获资金净流入居前丨一手数据](https://www.yicai.com/video/101571764.html)
> 概要: 10月24日，国防军工逆势领涨市场，板块获得资金净流入超30亿元，同样位居各行业之首。
### [第三轮“爱购上海”电子消费券报名10月25日0点开始](https://finance.sina.com.cn/china/2022-10-24/doc-imqmmthc1968997.shtml)
> 概要: 根据《2022年“爱购上海”电子消费券发放活动方案》，上海将发放第三轮“爱购上海”电子消费券。 报名时间为2022年10月25日00：00至10月27日24：00。
### [楼市出现新风向，南海里水片区活跃度领跑佛山](https://finance.sina.com.cn/china/2022-10-24/doc-imqmmthc1971150.shtml)
> 概要: 受大环境影响，今年的房地产市场“金九银十”成色略显不足。但随着风险事件的妥善化解，以及从中央到地方的一系列政策措施，部分城市房地产市场逐渐释放出向好的信号。
### [组图：周迅王一博三搭合作《我的朋友》 两人穿工服对视氛围感足](http://slide.ent.sina.com.cn/film/w/slide_4_704_376958.html)
> 概要: 组图：周迅王一博三搭合作《我的朋友》 两人穿工服对视氛围感足
### [郑州今年第三批集中供地：23宗地块总起拍价172.7亿元](https://finance.sina.com.cn/jjxw/2022-10-24/doc-imqmmthc1971051.shtml)
> 概要: 10月22日，河南省郑州市公共资源交易中心发布今年第三批次集中供地公告，本次共推出23宗涉宅用地，共计占地面积114.1万平方米，规划建面333.4万平方米，总起始价172...
### [A股港股生物科技公司股价暴跌，游资撤离行业估值回归](https://www.yicai.com/news/101571790.html)
> 概要: 生物技术公司的下跌已经持续了一段时间。这是由于游资主导的生物技术公司泡沫已经退去，估值也回归正常合理水平。
### [9月份，54城新房价格下跌](https://finance.sina.com.cn/jjxw/2022-10-24/doc-imqmmthc1972368.shtml)
> 概要: 澎湃新闻记者 计思敏 10月24日，国家统计局发布2022年9月份70个大中城市商品住宅销售价格变动情况。9月份，70个大中城市中商品住宅销售价格下降城市个数增加...
### [张宁再砍22分5篮板5助攻帮助山西战胜福建，请JRs继续吹！](https://bbs.hupu.com/56084519.html)
> 概要: CBA常规赛第7轮山西对阵福建的比赛刚刚结束。经过四节鏖战，山西队119-94大胜福建队。其中山西锋卫摇摆人张宁出战40分钟，16投7中得到22分5篮板5助攻，正负值高达+21。比赛中，张宁突入内线遭
### [流言板克雷桑vs广州队全场数据：射门2次进2球，获评9.3分全场第一](https://bbs.hupu.com/56084577.html)
> 概要: 虎扑10月24日讯 中超联赛第19轮补赛，山东泰山主场3-0战胜广州队，克雷桑全场数据如下：射门2次，射正2次，打进2球尝试过人8次，成功4次（全场第一）触球83次，传球成功率74%创造佳机会1次，关
### [中国县域省外流入人口排行榜：义乌每2人就有1人来自外省](https://finance.sina.com.cn/china/gncj/2022-10-24/doc-imqmmthc1973025.shtml)
> 概要: 一般而言，经济发达的地区就业机会多，对流动人口就会形成强大的吸引力。在我国，一些经济发达的县域也吸引了大量外来人口。
### [流言板特雷-杨：AJ-格里芬就像是一块海绵，我为他感到兴奋](https://bbs.hupu.com/56084670.html)
> 概要: 虎扑10月24日讯 黄蜂在常规赛中126-109击败老鹰。赛后，老鹰球员特雷-杨接受了媒体采访。谈到队友AJ-格里芬的表现，特雷-杨说：“这令人兴奋，他就像是一块海绵，学习新事物，他想要在踏上赛场的每
### [赛场速递内线支柱！余嘉豪攻防两端送出精彩表现！](https://bbs.hupu.com/56084764.html)
> 概要: 赛场速递内线支柱！余嘉豪攻防两端送出精彩表现！
# 小说
### [绝命善玉师](https://book.zongheng.com/book/1215171.html)
> 作者：一手闲人

> 标签：都市娱乐

> 简介：一贫如洗的败家子陆晓齐，天生拥有敏锐的辨玉能力。他自以为凡人，却不知一朝出庐，众生苏醒！夜黑渡魂，世间谁有不平事？黑契在手，不跟人类做交易！大善大恶一念之间，翻天覆地目中无人！

> 章节末：第一百七十四章   感言

> 状态：完本
# 论文
### [FCDSN-DC: An Accurate and Lightweight Convolutional Neural Network for Stereo Estimation with Depth Completion | Papers With Code](https://paperswithcode.com/paper/fcdsn-dc-an-accurate-and-lightweight)
> 日期：14 Sep 2022

> 标签：None

> 代码：https://github.com/thedodo/fcdsn-dc

> 描述：We propose an accurate and lightweight convolutional neural network for stereo estimation with depth completion. We name this method fully-convolutional deformable similarity network with depth completion (FCDSN-DC). This method extends FC-DCNN by improving the feature extractor, adding a network structure for training highly accurate similarity functions and a network structure for filling inconsistent disparity estimates. The whole method consists of three parts. The first part consists of fully-convolutional densely connected layers that computes expressive features of rectified image pairs. The second part of our network learns highly accurate similarity functions between this learned features. It consists of densely-connected convolution layers with a deformable convolution block at the end to further improve the accuracy of the results. After this step an initial disparity map is created and the left-right consistency check is performed in order to remove inconsistent points. The last part of the network then uses this input together with the corresponding left RGB image in order to train a network that fills in the missing measurements. Consistent depth estimations are gathered around invalid points and are parsed together with the RGB points into a shallow CNN network structure in order to recover the missing values. We evaluate our method on challenging real world indoor and outdoor scenes, in particular Middlebury, KITTI and ETH3D were it produces competitive results. We furthermore show that this method generalizes well and is well suited for many applications without the need of further training. The code of our full framework is available at: https://github.com/thedodo/FCDSN-DC
### [Augmentation-Free Self-Supervised Learning on Graphs | Papers With Code](https://paperswithcode.com/paper/augmentation-free-self-supervised-learning-on)
> 概要: Inspired by the recent success of self-supervised methods applied on images, self-supervised learning on graph structured data has seen rapid growth especially centered on augmentation-based contrastive methods. However, we argue that without carefully designed augmentation techniques, augmentations on graphs may behave arbitrarily in that the underlying semantics of graphs can drastically change.