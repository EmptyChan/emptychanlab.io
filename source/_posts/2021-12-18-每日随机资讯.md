---
title: 2021-12-18-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WinterRoofs_ZH-CN5091303265_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-12-18 22:28:50
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WinterRoofs_ZH-CN5091303265_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [CakePHP 4.3.3 现已可用](https://www.oschina.net/news/174336/cakephp-4-3-3-released)
> 概要: CakePHP 4.3.3 现已可用，这是 4.3 分支的维护版本，修复了一些社区报告的问题。Bugfixes改进了 OAuth 签名遇到 SSL 错误时的错误处理方式。修正了 Router::rev......
### [pgAdmin 4 v6.3 发布，PostgreSQL 开源图形化管理工具](https://www.oschina.net/news/174339/pgadmin-4-6-3-released)
> 概要: pgAdmin 是 PostgreSQL 领先的开源图形化管理工具。pgAdmin 4 旨在满足新手和有经验的 Postgres 用户的需求，提供强大的图形界面，简化了数据库对象的创建、维护和使用。这......
### [Apache Log4j 2.17.0 发布，解决第三个 DoS 漏洞](https://www.oschina.net/news/174424/apache-log4j-2-17-0-released)
> 概要: Apache Log4j 2.17.0 版本已正式发布，解决了被发现的第三个安全漏洞 CVE-2021-45105。Apache Log4j2 版本 2.0-alpha1 到 2.16.0 没有防止 ......
### [11月 VS Code 的 Java 新功能和编码策略](https://www.oschina.net/news/174344/new-features-of-java-in-vscode-2021-11)
> 概要: 微软整理并公布了 2021年11月 VSCode 中Java 基础开发相关的最新功能，以及与应对编码问题的一些解决策略：项目管理 – 弃用“.project”等元数据文件导入新的 Java 项目时，V......
### [Postgres is a great pub/sub and job server (2019)](https://webapp.io/blog/postgres-is-the-answer/)
> 概要: Postgres is a great pub/sub and job server (2019)
### [抖音电商，落地成“盒”](https://www.huxiu.com/article/482569.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜黄青春题图 | 东方IC千呼万唤始出来，抖音盒子终于低调上线了。12月16日，抖音电商独立App“抖音盒子”正式上线iOS、Android系统，并登陆各大应用市场。官......
### [Melting glaciers may produce thousands of kilometers of new salmon habitat](https://www.science.org/content/article/melting-glaciers-may-produce-thousands-kilometers-new-salmon-habitat)
> 概要: Melting glaciers may produce thousands of kilometers of new salmon habitat
### [组图：网传王力宏出轨By2孙雨？By2工作室发否认声明后又报警](http://slide.ent.sina.com.cn/y/slide_4_704_364668.html)
> 概要: 组图：网传王力宏出轨By2孙雨？By2工作室发否认声明后又报警
### [玩家自制作品《星际火狐：事件视界》公布实机演示](https://www.3dmgame.com/news/202112/3831148.html)
> 概要: UndyingNephalim今日发布《星际火狐：事件视界》（Star Fox: Event Horizon）的实机演示视频。这是一款使用《自由空间》开放引擎制作的玩家自制免费游戏。这段视频展示了游戏......
### [《DNF：决斗》游戏介绍视频 MP、HP、基本招式等](https://www.3dmgame.com/news/202112/3831150.html)
> 概要: 今日（12月18日），格斗游戏《地下城与勇士：决斗》公布四段游戏介绍视频，其分别是HP介绍、MP介绍、基本动作介绍以及特殊的MP与HP转换系统介绍。HP介绍：角色遭到攻击时会受到两种类型的伤害，其中一......
### [组图：周杰伦个人社交网站取关王力宏 章泽天点赞李靓蕾长文](http://slide.ent.sina.com.cn/y/slide_4_704_364674.html)
> 概要: 组图：周杰伦个人社交网站取关王力宏 章泽天点赞李靓蕾长文
### [TV动画《间谍过家家》新PV公开 爽快家庭喜剧来袭](https://acg.gamersky.com/news/202112/1446366.shtml)
> 概要: TV动画《间谍过家家》公开了最新的PV，各种真实身份超酷的特工组成的“一家人”热闹的登场了。官方还公开了新的声优阵容，种崎敦美和早见沙织将为阿尼亚和约尔配音。
### [战术FPS《严阵以待》抢先体验 Steam今日上线](https://www.3dmgame.com/news/202112/3831155.html)
> 概要: 由 VOID Interactive 独立开发发行的战术拟真 FPS《严阵以待（Ready or Not）》今天（12 月 18 日）以抢先体验形式登陆 Steam 商店。游戏国区售价 125 元，暂......
### [近距离感受超老龄化社会](https://www.huxiu.com/article/482852.html)
> 概要: 本文来自微信公众号：太阳照常升起（ID：The_sun_also_rise），作者：慕峰，题图来自：视觉中国我在中国北方一座小城待了快一个月了，这座城市的老龄化程度位于全国前列。从全球来看，人类社会的......
### [TV动画「白沙的水族馆」完结贺图公布](http://acg.178.com/202112/433796297537.html)
> 概要: TV动画「白沙的水族馆」于近日正式完结，总作画监督秋山有希和角色原案U35公开了其绘制的完结贺图。「白沙的水族馆」是由P.A.WORKS制作的原创电视动画作品，于2021年7月8日开始播出。本作讲述了......
### [动画《游戏王 Go Rush!!》PV 首位宇宙人主角登场](https://acg.gamersky.com/news/202112/1446384.shtml)
> 概要: 《游戏王》系列第8部动画《游戏王 Go Rush!!》公开了预告和视觉图，本作的舞台在六叶町，还有首位宇宙人主角登场，动画将于2022年4月开播。
### [动画「间谍过家家」公开第2弾宣传PV](http://acg.178.com/202112/433798697647.html)
> 概要: 近日，动画「间谍过家家」官方公开了本作第2弹宣传PV，并宣布动画将分割为2季度放送，动画第1季将在2022年4月播出。动画「间谍过家家」第2弾宣传PVCV：ロイド・フォージャー：江口拓也アーニャ・フォ......
### [TV动画《死神 千年血战篇》PV公开 2022年10月开播](https://news.dmzj.com/article/73094.html)
> 概要: TV动画《死神 千年血战篇》今日（12月18日）特报PV公开，预定2022年10月开播。
### [《电锯人》TV动画最新PV 漫画第2部2022年初夏连载](https://news.dmzj.com/article/73095.html)
> 概要: TV动画《电锯人》今日（12月18日）官网公开了最新PV，将于2022年开始播出，同时《电锯人》漫画第2部将于2022年初夏于《少年jump+》上开启连载。
### [本月玩什么 | 光环：无限、雷霆一号、翼星求生](https://www.tuicool.com/articles/MRbaQrJ)
> 概要: Matrix 首页推荐Matrix 是少数派的写作社区，我们主张分享真实的产品体验，有实用价值的经验与思考。我们会不定期挑选 Matrix 最优质的文章，展示来自用户的最真实的体验和观点。文章代表作者......
### [良笑社「咒术回战」五条悟手办开订](http://acg.178.com/202112/433804548985.html)
> 概要: 近日，良笑社公开了手办系列「POP UP PARADE」的最新企划，本次推出的角色是来自动画「咒术回战」中的特级咒术师「五条悟」。该手办附赠可替换表情配件「眼罩脸」和「露脸脸」，售价3,900日元，现......
### [TV动画《间谍过家家》第2弹PV 上半季2022年4月开播](https://news.dmzj.com/article/73096.html)
> 概要: TV动画《间谍过家家》第2弹PV预告公开，动画为半年番分割2季度播出，分别于2022年4月和2022年10月播出。
### [「灌篮高手」完全新作剧场版动画公开新情报](http://acg.178.com/202112/433805126595.html)
> 概要: 「灌篮高手」完全新作剧场版动画公开了新视觉图、制作人员、湘北队成员信息，本作由东映动画制作，井上雄彦担任导演、编剧，将于2022年秋季上映。「灌篮高手」是日本漫画家井上雄彦创作的以高中篮球为题材的少年......
### [Libmobi: C library for handling eBook formats](https://github.com/bfabiszewski/libmobi)
> 概要: Libmobi: C library for handling eBook formats
### [程序员瑞士军刀：各种结构的转换工具](https://www.tuicool.com/articles/673mQzN)
> 概要: 大家好，我是 polarisxu。上次推荐的 「Go网址导航」，其中收录了一项：转换器。网址是：https://transform.tools/。这是一个开源项目，项目地址：https://githu......
### [组图：汪小菲发文否认出轨 妈妈张兰直播称身正不怕影子斜](http://slide.ent.sina.com.cn/star/slide_4_704_364695.html)
> 概要: 组图：汪小菲发文否认出轨 妈妈张兰直播称身正不怕影子斜
### [The Universe Is Expanding Faster Than It Should Be](https://www.nationalgeographic.com/science/article/the-universe-is-expanding-faster-than-it-should-be)
> 概要: The Universe Is Expanding Faster Than It Should Be
### [《一拳超人》重制版200话：埼玉上线 饿狼大爆发](https://acg.gamersky.com/news/202112/1446406.shtml)
> 概要: 《一拳超人》重制版200话来了，本次更新也相当快，而且还有40多页，感觉村田又爆肝了。埼玉在消失很久之后终于上来了，King终于可以放心了。
### [由苹果Logo设计师操刀，蒙牛22年来首次更换品牌Logo](https://www.tuicool.com/articles/MbIzUjm)
> 概要: 今天上午，蒙牛正式官宣启用全新设计的品牌Logo（下图右）：这也是蒙牛自1999年创立至今的22年来，首次进行品牌Logo升级焕新。在官方介绍中提到，此次蒙牛启用的全新品牌Logo是由曾设计出苹果公司......
### [官方宣布《双城之战》第一季免费看 我第二季呢？](https://acg.gamersky.com/news/202112/1446433.shtml)
> 概要: 《英雄联盟》官方于今日宣布，英雄联盟动画《双城之战》第1季全九集已经免费上线。
### [《地平线：零之曙光》PC版新补丁制作由Nixxes负责](https://www.3dmgame.com/news/202112/3831180.html)
> 概要: Nixxes Software在被索尼收购之后，已经开始投入工作，帮助Guerrilla games制作《地平线：零之曙光》的PC版补丁。最新的1.11.1版补丁将修复PC版本中的多个bug。最新版补......
### [特斯拉为新款 Model S / X 推出道路主动降噪功能，通过麦克风、扬声器配合实现](https://www.ithome.com/0/593/256.htm)
> 概要: IT之家12 月 18 日消息，特斯拉正在为新款 Model S 和 Model X 推出其主动降低道路噪音的功能，这是 2021.44.5 系统更新的功能之一。官方介绍称：“您的车辆现在有能力在粗糙......
### [浑水做空中概股11年：封过神，翻过车](https://www.huxiu.com/article/482905.html)
> 概要: 本文来自微信公众号：时代周报（ID：timeweekly），作者：宁鹏，原文标题：《“搅局者”浑水做空11年：封过神，翻过车，是天使还是魔鬼？》，头图来自：视觉中国12日16日晚，一份针对贝壳的做空报......
### [《三国杀》今日登陆 Steam / 蒸汽平台](https://www.ithome.com/0/593/258.htm)
> 概要: 感谢IT之家网友Cloud米米的线索投递！IT之家12 月 18 日消息，今天，游卡《三国杀》正式登陆 Steam 与蒸汽平台，同时上线“三国杀 × 刺客伍六七”等联名活动。IT之家了解到，《三国杀》......
### [王安石诞辰一千周年：他的变法为何注定失败](https://www.huxiu.com/article/482914.html)
> 概要: 本文来自微信公众号：冰川思享号（ID：icereview），作者：陈季冰，原文标题：《王安石变法，为何避免不了失败命运》，头图来自：视觉中国宋神宗熙宁八年（1075年）初春二月，半赋闲于江宁（现南京）......
### [极氪汽车宣布今日起“极分”可抵扣充电费：10 极分可抵扣 1 元](https://www.ithome.com/0/593/263.htm)
> 概要: IT之家12 月 18 日消息，据 极氪 ZEEKR 公众号，极氪汽车宣布，即日起，极分可抵扣充电费，包括自建充电站，第三方充电站，电极送服务（预计 12 月 28 日上线）。IT之家了解到，极分是极......
### [王力宏“花田里犯了错”，人设崩塌他还坑了谁？](https://new.qq.com/rain/a/20211218A07Z5J00)
> 概要: 杨光子睿王力宏也塌房了。12月17日晚，刚刚官宣离婚几天，其前妻李靓蕾在微博上爆料称王力宏虽然今年塌房的明星不少，，还是让很多网友惊掉了下巴。受惊的不只是吃瓜的网友，还有与王力宏有商业合作的企业们。在......
### [要有趣，还要有灵魂，雪糕老字号如何玩转IP营销？|新消费观察](https://www.tuicool.com/articles/qERVbme)
> 概要: “你们这（西装店）应该不接受我这种非人类的客人吧”、“但我这种尺寸应该不好搞吧”……在抖音，有几十万人在看一只戴着红围脖的“北极熊”碎碎念。在这个名为“我叫熊小白”的账号主页中，有着自我人格的“熊小白......
### [视频：范玮琪卷入王力宏风波？黑人受访称以球赛为重 未替妻子澄清](https://video.sina.com.cn/p/ent/2021-12-18/detail-ikyamrmy9806369.d.html)
> 概要: 视频：范玮琪卷入王力宏风波？黑人受访称以球赛为重 未替妻子澄清
### [视频：求锤得锤环节到！李靓蕾喊话BY2工作室称我提供证据](https://video.sina.com.cn/p/ent/2021-12-18/detail-ikyakumx4979933.d.html)
> 概要: 视频：求锤得锤环节到！李靓蕾喊话BY2工作室称我提供证据
### [你们究竟有多恨《雄狮少年》？豆瓣8.3的国漫之光，却被骂成渣](https://new.qq.com/omn/20211218/20211218A08USZ00.html)
> 概要: 01.从万众期待到备受谴责在12月17日，国产动画《雄狮少年》正式在全国院线上映了，该作在很长一段时间里都被视作为2021年度最受期待的国产动画，尤其对于广东佛山这边的观众来说，那可真叫一个接地气的精......
### [张维迎：要实现公平市场，第一次分配至关重要](https://finance.sina.com.cn/roll/2021-12-18/doc-ikyamrmy9816325.shtml)
> 概要: 中新网北京12月18日电 （杨诗涵）“三亚·财经国际论坛”18日在线上线下同步举行，北京大学国家发展研究院教授张维迎在参与论坛时称，收入分配最重要的是保证市场公平。
### [343 工作室：《光环：无限》活动将更易获得道具和装饰，且不用氪金](https://www.ithome.com/0/593/273.htm)
> 概要: IT之家12 月 18 日消息，343 工作室在近日确认，《光环：无限》即将到来的“武士”活动将会让玩家更容易获得游戏内装饰品，并且不用氪金。在此前的《光环：无限》第一个活动中，许多装饰品只能通过付费......
### [小柔COS《英雄联盟》金克丝，还原度爆表，哪边才是原画？](https://new.qq.com/omn/20211218/20211218A09BBA00.html)
> 概要: 《英雄联盟》是一款十分受欢迎的游戏，如今还出了动漫《英雄联盟：双城之战》，里面的角色也受到了不少玩家的喜爱。就COS了《英雄联盟》中的角色，还原度可以说是十分地高了。            服装、道具......
### [6.7亿元！麦当劳前CEO退还高额薪酬！原因竟是→](https://finance.sina.com.cn/china/gncj/2021-12-18/doc-ikyakumx4993016.shtml)
> 概要: 来源：央视财经 当地时间16日，美国麦当劳公司发布声明，确认公司前CEO也就是首席执行官伊斯特布鲁克，已经返还高达1.05亿美元（约合人民币6.7亿元）的现金和股票薪酬。
### [湖北一高速桥梁侧翻，曾有公司对事发处加固！事故致3死4伤](https://finance.sina.com.cn/china/2021-12-18/doc-ikyamrmy9819079.shtml)
> 概要: 原标题：湖北一高速桥梁侧翻，曾有公司对事发处加固！事故致3死4伤 来源：南方都市报 12月18日下午，位于湖北省鄂州市境内的沪渝高速沪渝向转大广高速匝道桥发生桥梁侧翻...
### [2022年待播动画一览，你打算追哪部？](https://new.qq.com/omn/20211218/20211218A09FMQ00.html)
> 概要: 进击的巨人 最终章 Part.2    1月9日            死神 千年血战篇 10月            电锯人            辉夜大小姐想让我告白第三季 4月         ......
### [突发！万亿“中植系”掌门人、毛阿敏丈夫解直锟离世，享年61岁！身家260亿](https://finance.sina.com.cn/china/gncj/2021-12-18/doc-ikyamrmy9818367.shtml)
> 概要: 12月18日晚间，据中植企业集团微信公号消息，中共党员、中植企业集团创始人解直锟先生，因心脏病突发抢救无效，于2021年12月18日9时40分在北京逝世，享年61岁。
### [《最终幻想起源：天堂的陌生人》新演示和截图公开](https://www.3dmgame.com/news/202112/3831184.html)
> 概要: 光荣发布了《最终幻想起源：天堂的陌生人》部分角色的声优和介绍，同时公开了游戏的“连锁取消”演示。视频：《最终幻想起源：天堂的陌生人》的故事以被黑暗支配的科内利亚王国为舞台，围绕着杰克一行人恢复水晶光辉......
### [李靓蕾长文锤王力宏，完美优质偶像人设崩塌](https://new.qq.com/rain/a/20211218A09KN700)
> 概要: 今年的娱乐圈真是分手的分手，离婚的离婚。12月15日，台媒曝王力宏与李靓蕾离婚，并称原因是婆媳不合，但立马被王力宏经纪人辟谣。不过才辟谣没多久，王力宏直接回应表示已和李靓蕾提出离婚申请，“但是我们永远......
### [美盛文化8天7板背后：元宇宙光环难掩内控缺陷，实控人违规占资被独董硬刚](https://finance.sina.com.cn/roll/2021-12-18/doc-ikyakumx4994412.shtml)
> 概要: 原标题：美盛文化8天7板背后：元宇宙光环难掩内控缺陷，实控人违规占资被独董硬刚 还是元宇宙香 康美药业事件后，A股独立董事终于不甘心只做花瓶了。
### [突发！毛阿敏丈夫解直锟离世 控制8家A股上市公司 未来谁接班？](https://finance.sina.com.cn/jjxw/2021-12-18/doc-ikyamrmy9821586.shtml)
> 概要: 原标题：突发！毛阿敏丈夫解直锟离世 控制8家A股上市公司 未来谁接班？ 北京商报 北京商报消息，一代资本大鳄落幕！12月18日，中植企业集团在官微发布讣告显示...
### [8年婚姻3个孩子，一句“遗憾”就想全身而退？他还是低估了李靓蕾](https://new.qq.com/rain/a/20211218A09PZY00)
> 概要: ，这句话来形容娱乐圈中的一些明星，真的是再合适不过了。12月15日，娱乐圈有一则离婚消息登上了热搜，而这次的男女主人公分别是歌手王力宏和他的妻子李靓蕾。            刚开始为了能够降低舆论，......
### [二代养成男团：一场底色纯粹的商业实验](https://new.qq.com/rain/a/20211218A09TA700)
> 概要: |霜绛12月14日，时代少年团2021“火力全开演唱会”在延期半个月后上线，粉丝热情不减，当日热搜刷屏，并于今天上午在三个音乐平台上线LIVE专辑。同时，《乌托邦少年》专辑新歌《男儿歌》也已经在QQ音......
### [海贼王：凯多7次战败18次入狱，第1次战败入狱情报揭晓，实在太浪](https://new.qq.com/omn/20211218/20211218A09SXD00.html)
> 概要: 《海贼王》中，众所周知，四皇是伟大航路后半段的帝皇人物。四个海贼皇帝可是凌驾于新世界之上的强者，每一个都是不可多得的一方巨擘，有着各种各样辉煌而不可匹敌的强大战绩，可以谓之最强存在。         ......
# 小说
### [异能崩坏史](http://book.zongheng.com/book/1016640.html)
> 作者：竹由

> 标签：奇幻玄幻

> 简介：希腊诸神的辉煌遗落凡尘。少年重拾昔日辉煌，以不死肉身之躯，比肩神明之力。在宇宙其他法则的入侵下，人类现代文明的经典物理法则将会变得如何脆弱！科技和异能的碰撞，诡异之力和神力的交锋，尽在此处。本书以希腊神话为力量体系，设定新奇，内含scp克鲁苏，宗教神话等西幻元素，绝对不是打怪升级的套路文。作者是江南的粉丝，现在已经是罂粟花，对江南《龙族》意难平，江南老贼爬，作者自己拿笔写故事，写读者想看的故事。

> 章节末：尾声  关于她和遗憾

> 状态：完本
# 论文
### [Controlled Evaluation of Grammatical Knowledge in Mandarin Chinese Language Models | Papers With Code](https://paperswithcode.com/paper/controlled-evaluation-of-grammatical)
> 日期：22 Sep 2021

> 标签：None

> 代码：None

> 描述：Prior work has shown that structural supervision helps English language models learn generalizations about syntactic phenomena such as subject-verb agreement. However, it remains unclear if such an inductive bias would also improve language models' ability to learn grammatical dependencies in typologically different languages.
### [Reconstructing Cosmic Polarization Rotation with ResUNet-CMB | Papers With Code](https://paperswithcode.com/paper/reconstructing-cosmic-polarization-rotation)
> 日期：20 Sep 2021

> 标签：None

> 代码：None

> 描述：Cosmic polarization rotation, which may result from parity-violating new physics or the presence of primordial magnetic fields, converts $E$-mode polarization of the cosmic microwave background (CMB) into $B$-mode polarization. Anisotropic cosmic polarization rotation leads to statistical anisotropy in CMB polarization and can be reconstructed with quadratic estimator techniques similar to those designed for gravitational lensing of the CMB.
