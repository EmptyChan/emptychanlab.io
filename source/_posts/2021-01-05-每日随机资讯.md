---
title: 2021-01-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RedFrontMacaw_EN-CN5690511619_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-05 21:38:07
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RedFrontMacaw_EN-CN5690511619_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：《晴雅集》被下线后郭敬明现身机场 低头疾走似情绪失落](https://video.sina.com.cn/p/ent/2021-01-05/detail-iiznctkf0272798.d.html)
> 概要: 视频：《晴雅集》被下线后郭敬明现身机场 低头疾走似情绪失落
### [周深工作室成立北京分公司 持股比例为95%](https://ent.sina.com.cn/y/yneidi/2021-01-05/doc-iiznezxt0662642.shtml)
> 概要: 新浪娱乐讯 5日，据企查查APP显示，上海劲焱文化传媒有限公司北京分公司于12月7日成立，公司所属行业为商务服务业，企业经营范围为企业管理。股权穿透显示，周深为该公司最终受益人，持股比例为95%。(责......
### [组图：刘亦菲穿粉色抹胸大秀好人身材 头戴皇冠天鹅颈优越](http://slide.ent.sina.com.cn/star/slide_4_704_350760.html)
> 概要: 组图：刘亦菲穿粉色抹胸大秀好人身材 头戴皇冠天鹅颈优越
### [视频：林心如一个动作透露与霍建华相处之道 一般夫妻做不到](https://video.sina.com.cn/p/ent/2021-01-05/detail-iiznezxt0691816.d.html)
> 概要: 视频：林心如一个动作透露与霍建华相处之道 一般夫妻做不到
### [周迅谈饰演屠呦呦感受：很紧张 压力挺大的](https://ent.sina.com.cn/v/m/2021-01-05/doc-iiznctkf0252536.shtml)
> 概要: 新浪娱乐讯 近日，周迅接受央视采访，谈及了在新片《功勋》中饰演屠呦呦的感受。周迅表示：“当时小平（王小平）老师和晓龙（郑晓龙）导演说，屠呦呦想请你来演，我是很紧张的，压力其实挺大的，因为对那个世界我太......
### [《有翡》陈若轩：剧组超有爱，赵丽颖很会照顾人，王一博半夜加餐还吃不胖](https://new.qq.com/omn/20210105/20210105A0DOXM00.html)
> 概要: 腾讯娱乐《一线》 作者：三禾上一次与陈若轩对话，还是在《九州·缥缈录》热播之际，他饰演因庶出而饱受冷落的贵族公子“姬野”，剧情极虐，再联想到之前在《心理罪》《九州·天空城》里身世坎坷的角色，他不禁苦笑......
### [奚梦瑶加入Baby名媛闺蜜圈，合影不在C位，手托下巴凹造型](https://new.qq.com/omn/20210105/20210105A0F3GT00.html)
> 概要: 奚梦瑶加入Baby名媛闺蜜圈，合影不在C位，手托下巴凹造型
### [金莎受访谈听章子怡点评后内心愤怒不爽，强调：不喜欢被攻击人格！](https://new.qq.com/omn/20210105/20210105A03VIY00.html)
> 概要: 近日，金莎在某访谈中讲述了在综艺节目《我就是演员3》听到章子怡对自己评价后的感受，她用了分别用了“不爽”、“愤怒”这两个词来来表达。之后更直接的说，“原本期待的是别人对我专业的评价，纠正坏毛病以及欠缺......
### [余文乐晒一家三口骑车放风，坦言这两年很不如意，感谢妻儿相伴](https://new.qq.com/omn/20210105/20210105A0204800.html)
> 概要: 1月5日凌晨，余文乐在社交平台上分享了一家三口，并回顾人生，感慨自己18岁就开始工作了，这么多年都没怎么休息过，因为他知道有工作就是福气，必须好好把握。但六叔坦言这两年是自己最困难、人生最不如意的两年......
### [鞠婧祎近照引争议，嘴歪下巴尖到能戳人，右脸凹陷被疑整容后遗症](https://new.qq.com/omn/20210105/20210105A0EBAY00.html)
> 概要: 1月5日，鞠婧祎为新歌录制的宣传视频引起了网友热议。当天，鞠婧祎身穿紫色针织衫，妆容精致，一头长卷发梳理得十分好看，不过，从动态中看，鞠婧祎的下巴异常的尖，和往日里圆润的下巴比起来差距不小。     ......
# 动漫
### [With Fans《关于我转生成为史莱姆的那件事》利姆露手办](https://news.dmzj.com/article/69800.html)
> 概要: With Fans！根据人气TV动画《关于我转生成为史莱姆的那件事》中的主人公利姆露制作的1/7比例手办目前已经开订了。本作采用了利姆露身穿以黑色为主色调的新服装拔刀时的样子。
### [漫画人物的觉醒，原来我只是个纸片人](https://news.dmzj.com/article/69796.html)
> 概要: 这期小编给大家推荐的漫画是《我怎么可能是BL漫画里的主角啊！》，作者山野でこ，和去年爆火韩剧《偶然发现的一天》有些设定上差不多，都是突然有一天发现自己所认为的现实，并不是真实存在的，自己也不是现实中的人物，身边会突然出现漫画对白框，大家都只是个纸...
### [游戏王历史：从零开始的游戏王环境之旅第六期12](https://news.dmzj.com/article/69804.html)
> 概要: “暗之诱惑”这一具有游戏王OCG代表性的暗属性辅助卡参战后，以【同调不死】为首的各种暗属性牌组都得到了强化。并且对活用抽卡加速的牌组——【超级抽卡Lidar】的开发速度也得到了显著提升，其影响力远超正常辅助卡的身份。【同调不死】开始支配环境的同月...
### [金田一作者金成阳三郎参与！手游《伦敦迷宫谭》冬季开服](https://news.dmzj.com/article/69801.html)
> 概要: Sumzap宣布了手机游戏《伦敦迷宫谭》目前已经开始事前预约，预计今冬开服的消息。
### [《进击的巨人》漫画将于4月完结 最终卷6月9日发售](https://acg.gamersky.com/news/202101/1352088.shtml)
> 概要: 谏山创创作的漫画《进击的巨人》即将迎来完结，《别册少年Magazine》的推特宣布《进击的巨人》漫画将在4月6日发售的《别册少年Magazine》迎来最终话，11年半的连载结束。
### [《悠哉日常大王》第三季新预告 1月10日开播](https://acg.gamersky.com/news/202101/1352222.shtml)
> 概要: TV动画《悠哉日常大王》第三季公开了全新预告，并且宣布本作将于1月10日开播。大家又可以看到各位可爱妹子的悠哉日常生活了。
### [《西游记之再世妖王》公开新海报 再定档5月1日](https://acg.gamersky.com/news/202101/1352075.shtml)
> 概要: 国产动画电影《西游记之再世妖王》宣布再定档，同时公开了公开了定档海报，本作将于2021年5月1日上映。
### [动画电影《白蛇：缘起》续作获准拍摄 小青为主角](https://acg.gamersky.com/news/202101/1352191.shtml)
> 概要: 据国家电影局备案公示显示，动画电影《白蛇：缘起》的续作《白蛇2：青蛇劫起》已经备案并且获准拍摄。《白蛇2》的故事主角为青蛇小青。
# 财经
### [“十三五”我国新办涉税市场主体5745万户 广东等五省占比超四成](https://finance.sina.com.cn/china/dfjj/2021-01-05/doc-iiznctkf0321102.shtml)
> 概要: 原标题：“十三五”我国新办涉税市场主体5745万户，日均超3万户，广东等五省占比超四成 每经记者 张钟尹 近日，国家税务总局发布报告显示，“十三五”期间...
### [南京火锅店排队号炒至2000元 外卖小哥：到底是有多好吃啊](https://finance.sina.com.cn/china/dfjj/2021-01-05/doc-iiznezxt0744366.shtml)
> 概要: 来源：1818黄金眼 【#南京火锅店排队号炒至2000元# 外卖小哥：到底是有多好吃啊？】刚结束的节假日，南京新街口某网红火锅店排队号被炒至2000元。外卖跑腿小哥表示搞不懂！
### [河北宣布立即进入战时状态 近3日共报告本土确诊19例、无症状40例](https://finance.sina.com.cn/china/dfjj/2021-01-05/doc-iiznctkf0320300.shtml)
> 概要: 原标题：河北宣布立即进入战时状态！1月2日至4日共报告本土确诊19例、无症状40例 每经编辑 步静 从1月2日报告首例病例至1月4日24时，河北省共报告本土确诊病例19例...
### [管涛：如何看待人民币升值对我国外贸出口的影响](https://finance.sina.com.cn/china/gncj/2021-01-05/doc-iiznctkf0322865.shtml)
> 概要: 作者 | 管涛中银证券全球首席经济学家 来源 |《中国外汇》2021年第1期 要点 本轮人民币升值对出口的影响主要体现在财务冲击而非竞争力打击。
### [浙江百名考生考研违规被处理 名单公布](https://finance.sina.com.cn/china/gncj/2021-01-05/doc-iiznctkf0324990.shtml)
> 概要: 来源：中国蓝新闻 【#浙江百名考生考研违规被处理# ，名单公布】1月4日，浙江省教育考试院发布《浙江省2021年全国硕士研究生招生考试违规考生处理公告》...
### [海外人才加速回流：归国求职数量暴增七成 新一线城市花式揽才](https://finance.sina.com.cn/china/gncj/2021-01-05/doc-iiznezxt0740446.shtml)
> 概要: 原标题：海外人才加速回流：归国求职数量暴增七成，新一线城市“花式”揽才 海归就业就像再过一次“独木桥”。 在国内煎熬了数月后，王筱婷（化名）终于可以松上一口气了。
# 科技
### [Node.js 服务性能翻倍的秘密（一）](https://segmentfault.com/a/1190000038817174)
> 概要: 前言用过 Node.js 开发过的同学肯定都上手过 koa，因为他简单优雅的写法，再加上丰富的社区生态，而且现存的许多 Node.js 框架都是基于 koa 进行二次封装的。但是说到性能，就不得不提到......
### [万恶的前端内存泄漏及万善的解决方案](https://segmentfault.com/a/1190000038816646)
> 概要: 本文在github做了收录github.com/Michael-lzg…最近收到测试人员的反馈说我们开发的页面偶现卡死，点击无反应的情况，特别是打开页面较久的时候发生概率较高。打开任务管理器，看到内存......
### [中国移动：本公司将继续在纽约证交所上市及交易](https://www.ithome.com/0/528/402.htm)
> 概要: IT之家 1 月 5 日消息 1 月 5 日下午消息，中国移动发布公告称，目前，本公司将继续在纽约证交所上市及交易。纽约证交所监管部门将继续评估该行政命令（如本公司日期为 2020 年 11 月 13......
### [华为 Mate 20 系列等 14 款设备开启 EMUI 11 正式版升级](https://www.ithome.com/0/528/417.htm)
> 概要: IT之家 1 月 5 日消息 根据华为官方的消息，华为 Mate 20 系列共计 14 款设备的 EMUI 11 正式版升级，用户可以通过【服务】App—服务—升级尝鲜 进行升级。IT之家了解到，本次......
### [Understanding Connections and Pools](https://sudhir.io/understanding-connections-pools/)
> 概要: Understanding Connections and Pools
### [Don’t Share That. Yet](https://www.descript.com/)
> 概要: Don’t Share That. Yet
### [长期主义者，都有这5个特质](https://www.tuicool.com/articles/quq6ryr)
> 概要: 编者按：本文来自微信公众号“管理的常识”（ID:Guanlidechangshi），作者：二七，36氪经授权发布。距离长期主义一词的流行，几近2年过去了。顶级高手，依旧坚信。各界大佬，持续践行。到底什......
### [中国首档亲子新年大秀——儿童内容产业的IP还能怎么做？](https://www.tuicool.com/articles/mMZF7nV)
> 概要: “欢迎来到故事工厂童年是唯一的信仰快乐是我的魔杖带你穿越逆着光……”2021年1月1日，由凯叔讲故事与深圳卫视联手打造的，中国首档亲子主题跨年秀“2021凯叔魔幻童话之夜”圆满落下帷幕。大秀打破了以往......
# 小说
### [天下圣道](http://book.zongheng.com/book/121603.html)
> 作者：二水化石

> 标签：武侠仙侠

> 简介：何为圣，享人间香火者为圣，受万民敬仰者为圣。☆新书《大地之皇》已发，作者公告下有链接，求收藏、红票、点击和各种支持。☆＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊书友群：（群1）126169399（群2）62246603＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊（有一年未断更经历，老书《长生大帝》（近253万字），请各位新老书友多多支持。）

> 章节末：第三十六章  没有尽头的野心

> 状态：完本
# 游戏
### [休闲解谜新游《逃出百慕大》本月登陆Steam 有中文](https://www.3dmgame.com/news/202101/3805616.html)
> 概要: 根据开发商Yak & Co公开的消息，休闲解谜冒险新游《逃出百慕大（Down in Bermuda）》预计将于1月14日登陆Steam，该作支持中文，以下为官方公开的游戏介绍内容。Steam商城页面截......
### [《悠哉日常大王》第三季新CM公开 1月10日放送](https://www.3dmgame.com/news/202101/3805638.html)
> 概要: 根据官推公开的消息，TV动画《悠哉日常大王》第3季CM已经公开，该作将于2021年1月10日放送。电视动画《悠哉日常大王》改编自日本漫画家あっと原作的同名漫画。故事以有着优美自然风景的乡下学校旭丘分校......
### [追光动画《白蛇2：青蛇劫起》已立项 法海将会现身](https://www.3dmgame.com/news/202101/3805642.html)
> 概要: 据国家电影局官网公示信息显示，由追光动画推出的动画电影《白蛇2：青蛇劫起》现已立项通过，该片为《白蛇：缘起》的续作。剧情梗概：南宋末年，小白为救许仙水漫金山，终被法海压在雷峰塔下。小青则意外被法海打入......
### [英特尔正式确认将停产300系列主板芯片组](https://www.3dmgame.com/news/202101/3805664.html)
> 概要: 英特尔已经宣布，从明年1月28日起将不再交付300芯片组系列。从昨天开始，基于Z390，Z370，H370，B360，H310，B365，H310D和移动QMS380主板的300系列产品正式进入停产周......
### [《荣誉勋章：超越巅峰》开发人员展示场景设计对比图](https://www.3dmgame.com/news/202101/3805640.html)
> 概要: 最近，Respawn Entertainment开发人员Alexa Kim在推特上分享了VR射击游戏《荣誉勋章：超越巅峰》的场景设计图像。电子游戏中，场景设计是个并不容易的工作，而在VR中更是难上加难......
# 论文
### [Group Heterogeneity Assessment for Multilevel Models](https://paperswithcode.com/paper/group-heterogeneity-assessment-for-multilevel)
> 日期：6 May 2020

> 标签：CALIBRATION

> 代码：https://github.com/topipa/group-heterogeneity-paper

> 描述：Many data sets contain an inherent multilevel structure, for example, because of repeated measurements of the same observational units. Taking this structure into account is critical for the accuracy and calibration of any statistical analysis performed on such data.
### [Forest R-CNN: Large-Vocabulary Long-Tailed Object Detection and Instance Segmentation](https://paperswithcode.com/paper/forest-r-cnn-large-vocabulary-long-tailed)
> 日期：13 Aug 2020

> 标签：INSTANCE SEGMENTATION

> 代码：https://github.com/JialianW/Forest_RCNN

> 描述：Despite the previous success of object analysis, detecting and segmenting a large number of object categories with a long-tailed data distribution remains a challenging problem and is less investigated. For a large-vocabulary classifier, the chance of obtaining noisy logits is much higher, which can easily lead to a wrong recognition.
