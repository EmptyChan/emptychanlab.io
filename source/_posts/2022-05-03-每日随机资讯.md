---
title: 2022-05-03-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.VanBlooms_ZH-CN6370306779_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=35
date: 2022-05-03 23:00:33
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.VanBlooms_ZH-CN6370306779_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=35)
# 新闻
### [蚂蚁互动图形引擎 Oasis Engine v0.7 发布](https://www.oschina.net/news/194055/oasis-engine-0-7-released)
> 概要: 蚂蚁图形引擎 Oasis Engine 0.7 版本已发布，Oasis Engine 是一个移动优先的高性能 Web 图形引擎，被广泛应用在支付宝五福、打年兽等各种互动业务中的图形引擎。0.7 版本在......
### [英特尔聘请 Linux/BSD 性能专家 Brendan Gregg](https://www.oschina.net/news/194059/brendan-gregg-intel)
> 概要: 近日，英特尔 CTO Greg Lavender 在推特宣布，Brendan Gregg 已加入英特尔的软件团队，将专注于英特尔 xPU 计算领域方面的工作。Brendan Gregg 在 Linux......
### [DBeaver 22.0.4 发布，可视化数据库管理平台](https://www.oschina.net/news/194050/dbeaver-22-0-4-released)
> 概要: DBeaver 是一个免费开源的通用数据库工具，适用于开发人员和数据库管理员。DBeaver 22.0.4 发布，更新内容如下：SQL editor：修复了在新行中出现分号的问题修正了客户端命令解析的......
### [Edge 超越 Safari 成为桌面端第二大浏览器](https://www.oschina.net/news/194056/edge-market-share-second-desktop)
> 概要: 根据市场研究机构 Statcounter 截止 2022 年 4 月的统计数据，基于 Chromium 的 Microsoft Edge 浏览器在桌面端的市场占有率超越了 Safari，排名仅次于 C......
### [困局下的小红书，社区电商之路何去何从？](http://www.woshipm.com/it/5412670.html)
> 概要: 编辑导语：提到“种草”，相信大家第一个想到就是小红书。作为内容社区，小红书目前也面临着“种草”困局：用户在小红书上种草，却在其它平台购买产品。在这种情况下，小红书又应该如何破局呢？曾经，无数网友热衷于......
### [五四的话题营销，不燃怎YOUNG！](http://www.woshipm.com/marketing/5421378.html)
> 概要: 编辑导语：五四青年节，是对青年人进行话题营销的好时机，但也容易翻车，需要注意好相关尺度。那么，五四青年节该如何做话题营销呢？作者总结了十个角度的内容，希望对你有所启发，一起来看看。新一期的五分钟话题营......
### [1.6万字深度研究：接连创造高估值、高增长神话的PLG SaaS](http://www.woshipm.com/it/5420603.html)
> 概要: 编辑导语：众多估值超百亿美金的独角兽无一例外都使用了PLG模式，PLG模式为何如此受大家追捧，造就了企业级SaaS行业一个又一个高估值、高增长的神话？本文深入探究，一文带你搞懂PLG模式，看SaaS未......
### [火影：鸣人最终为什么放弃了小樱？原因只有男人才懂](https://new.qq.com/omn/20220328/20220328A0ACPL00.html)
> 概要: 在火影忍者早期，鸣人是非常喜欢小樱的，即使小樱一直不拿他当人，他还是一如既往地保持着舔狗本色。            但是到了火影中期，鸣人对小樱的态度却忽然转了个180度的急弯，在小樱主动表白的情况......
### [视频：阿娇前夫带未婚妻国外旅游 女方孕肚明显改口称一家三口](https://video.sina.com.cn/p/ent/2022-05-03/detail-imcwiwst5274551.d.html)
> 概要: 视频：阿娇前夫带未婚妻国外旅游 女方孕肚明显改口称一家三口
### [反垄断利剑再次砍向苹果！](https://finance.sina.com.cn/tech/2022-05-03/doc-imcwiwst5283152.shtml)
> 概要: 作者：杜玉......
### [欧盟指控苹果滥用Apple Pay市场支配地位](https://finance.sina.com.cn/tech/2022-05-03/doc-imcwiwst5309415.shtml)
> 概要: 财联社5月3日讯（编辑 史正丞）当地时间周一，欧盟委员会官网发布声明称，初步认定苹果公司的Apple Pay涉嫌滥用市场支配地位以取得自营业务的竞争优势......
### [《古墓丽影》系列已售出8800万份](https://www.3dmgame.com/news/202205/3841562.html)
> 概要: 游戏业界目前还在处于收购风波之中，在最近Embracer集团收购Crystal Dynamics、Eidos-Montréal、Square Enix Montréal工作室的报告中，官方透露了《古墓......
### [这轮抗疫，几个特大城市为何反差巨大](https://www.huxiu.com/article/545315.html)
> 概要: 本文来自微信公众号：文化纵横（ID：whzh_21bcr），作者：陈锋 侯同佳，原文标题：《这轮抗疫的一个微妙悖论， 或许能解释几个特大城市为何反差巨大 | 文化纵横》，头图：视觉中国面对新一轮疫情，......
### [圣斗士：海魔女曾轻松打倒了金牛，他到底什么水平？其实不强](https://new.qq.com/omn/20220424/20220424A05WJH00.html)
> 概要: 在动漫《圣斗士海皇篇》里，加隆放出了波塞冬，7名海斗士觉醒，圣域与海皇之间的战斗爆发。海斗士一共有7名，其中加隆客串了一个，而其他人大多数都实力不强。不过有位海斗士却非常强，而且地位很高，他就是有名的......
### [马斯克妈妈称马斯克14岁就有投资眼光，向她推荐股票赚了两倍](https://finance.sina.com.cn/tech/2022-05-03/doc-imcwipii7768520.shtml)
> 概要: 在马斯克发推分享选股经验后，他的妈妈梅耶·马斯克透露，马斯克从14岁就开始向她提供投资建议了......
### [《Apex英雄手游》5月在全球发售 登陆安卓和iOS](https://www.3dmgame.com/news/202205/3841564.html)
> 概要: 重生娱乐宣布《Apex英雄手游》将于5月在全球发售，登陆安卓和iOS设备。《Apex英雄手游》由重生娱乐专门团队和腾讯光子工作室群联合开发，后者开发了《绝地求生手游》。和PC、主机版一样，《Apex英......
### [动画「转生贤者的异世界生活 ～取得副职业并成为世界最强～」公开第二弹PV](http://acg.178.com/202205/445545022084.html)
> 概要: TV动画「转生贤者的异世界生活 ～取得副职业并成为世界最强～」公开了最新的第二弹PV，本作将于2022年7月正式开播。「转生贤者的异世界生活 ～取得副职业并成为世界最强～」第二弹PVSTAFF原作：進......
### [WhatsApp 将很快支持聊天列表查看状态更新](https://www.tuicool.com/articles/3uq6Nr6)
> 概要: WhatsApp 将很快支持聊天列表查看状态更新
### [动画「自称贤者弟子的贤者」BD第二卷封面公开](http://acg.178.com/202205/445546722189.html)
> 概要: TV动画「自称贤者弟子的贤者」公开了Blu-ray&DVD第二卷的封面图。第二卷Blu-ray售价为14,300日元（含税），DVD售价为12,100日元（含税），收录了动画第5-8话的内容，将于5月......
### [出售西方工作室后 SE将继续专注于区块链的投资](https://www.3dmgame.com/news/202205/3841584.html)
> 概要: 在将大部分西方工作室出售给瑞典公司Embracer Group后，SE将继续专注于区块链，并且该公司还称，出售工作室这一举措将有助于其区块链的投资。瑞典公司Embracer Group昨日（5月2日）......
### [「群青的幻想曲」角色曲「青い記憶」试听动画公开](http://acg.178.com/202205/445549963762.html)
> 概要: 由Lay-duce负责制作的原创动画「群青的幻想曲」近日公开了角色曲「青い記憶」的试听动画，该曲收录于第二卷BD中，将于7月6日发售。「青い記憶」试听动画CAST有村优：矢野奖吾风波骏：土屋神叶天音·......
### [ReoNa单曲「Someday」抒情版MV公开](http://acg.178.com/202205/445550597152.html)
> 概要: 近日，动漫歌手ReoNa公开了单曲「Someday」的抒情版MV，该曲收录于EP「Naked」之中，该EP将于5月11日发售。「Someday」抒情版MV......
### [书法字体设计｜第182回](https://www.zcool.com.cn/work/ZNTk1ODI2NDQ=.html)
> 概要: 书法字体设计｜第182回
### [底线是如何被击穿的？](https://www.huxiu.com/article/545375.html)
> 概要: 本文来自微信公众号：呦呦鹿鸣（ID：youyouluming99），作者：呦呦鹿鸣黄志杰，头图来自：视觉中国很多很多年之后，发生在2022年5月1日上海新长征福利院的这个故事，将依然会被人提起，因为在......
### [投完上百个抖音全案后，我总结了价值百万的抖音选号模型，CAFE科学选号方法论](https://www.tuicool.com/articles/ZfY7Zb2)
> 概要: 投完上百个抖音全案后，我总结了价值百万的抖音选号模型，CAFE科学选号方法论
### [习近平回信勉励广大航天青年 弘扬“两弹一星”精神载人航天精神](https://www.yicai.com/news/101400435.html)
> 概要: 习近平强调，建设航天强国要靠一代代人接续奋斗。
### [42家A股上市银行一季报全扫描：营收净利增速回落，已有大行降低拨备率](https://www.yicai.com/news/101400457.html)
> 概要: 拨备覆盖率处于最高位的邮储银行“打头阵”。
### [Emacs for Professionals](http://tilde.town/~ramin_hal9001/emacs-for-professionals/index.html)
> 概要: Emacs for Professionals
### [《龙珠斗士Z》官方宣布 正在对游戏的平衡性进行更新](https://www.3dmgame.com/news/202205/3841589.html)
> 概要: 对战型格斗游戏《龙珠斗士Z》官方今日宣布，工作室正在对游戏的平衡性进行更新，包括今年2月发布的“生化人21号（白衣）”战士DLC。此次更新发布时间及详细信息将在稍后公布。《龙珠斗士Z》是万代南梦宫联合......
### [我国的历史周期，走到哪了？](https://www.huxiu.com/article/545283.html)
> 概要: 本文来自微信公众号：培风客 （ID：peifengke），作者：奥德修斯的凝望，原文标题：《历史的周期走到哪了？｜中国篇》，头图来自：视觉中国“大部分存在规律的事情，在任何一个给定的时间点，往前面看一......
### [2021年还有这些航司盈利，股价涨最多的竟是它](https://www.yicai.com/news/101400471.html)
> 概要: 在已经披露2022年一季报的上市航司中，没有一家获得盈利，并且亏损都是同比扩大。
### [江西夫妇开5000家水果店，百果园要IPO了](https://www.tuicool.com/articles/jaiQRnz)
> 概要: 江西夫妇开5000家水果店，百果园要IPO了
### [《海贼王》1048话情报：火龙凯多降临 传次郎斩大蛇](https://acg.gamersky.com/news/202205/1480078.shtml)
> 概要: 《海贼王》1048话情报公开！
### [商业化能拯救中国人的早餐吗？](https://www.huxiu.com/article/545507.html)
> 概要: 本文来自微信公众号：赤潮AKASHIO（ID：AKASHIO），作者：司马徒林，编辑：评论尸、使马之人，原文标题：《小小早餐，难倒打工人》，题图来自：视觉中国不知不觉间，我们的早餐已经变得越来越简单......
### [真人电影《霸权动画！》公布新预告](https://news.dmzj.com/article/74304.html)
> 概要: 真人电影《霸权动画！》公布了一段新预告片，在这支预告中，我们可以看到多部东映经典动画的影像。
### [TV动画《转生贤者的异世界生活》公布第2弹PV](https://news.dmzj.com/article/74305.html)
> 概要: TV动画《转生贤者的异世界生活～获得第二职业并成为世界最强》公布第2弹PV,作品将于7月播出。
### [写好游戏营销文案（连载一）：选好核心标签，赢在竞争起跑线上](https://www.tuicool.com/articles/7NNZRvQ)
> 概要: 写好游戏营销文案（连载一）：选好核心标签，赢在竞争起跑线上
### [TVB小生宣布离巢：捡回一条命，但事业没了](https://new.qq.com/rain/a/20220503A003WL00)
> 概要: 留不住人的TVB又走了两个熟悉面孔。一个是著名的医生专业户郭田葰。看名字估计没人认识，但一看脸就很面善了。对港圈略熟悉的小伙伴对他的故事也都了解一点，入行前是正儿八经的专业医生来着。         ......
### [3000 美元起，索尼 QD-OLED 画谛系列电视 A95K 海外售价公布](https://www.ithome.com/0/616/462.htm)
> 概要: IT之家5 月 3 日消息，索尼在今年 1 月份全球发布的 QD-OLED 画谛系列电视 A95K 国行即将发布，目前已经公布了海外售价，可供大家提前参考。索尼 A95K 电视的 55 英寸版起价为3......
### [机械革命无界 16 笔记本新配置上架：i5-12500H / 2.5K 屏，首发 4799 元](https://www.ithome.com/0/616/464.htm)
> 概要: IT之家5 月 3 日消息，今年 1 月份，机械革命推出了无界 16 大屏轻薄本，搭载 14 核 i7-12700H，核显配置首发 5299 元。现在，机械革命又上架了 i5-12500H 版本，同样......
### [《奇巧计程车》舞台剧制作决定！](https://news.dmzj.com/article/74306.html)
> 概要: 动画《奇巧计程车》确定将推出舞台剧《ODD TAXI 金刚石（钻石）不会受伤》,7月14~18日在东京开演。
### [“创业板借壳第一股”过会，历经302天，承诺4年利润56亿元](https://www.yicai.com/news/101400611.html)
> 概要: 此次普丽盛顺利过会，也是2019年《关于修改〈上市公司重大资产重组管理办法〉的决定》允许符合国家战略的高新技术产业和战略性新兴产业资产在创业板重组上市后的首家创业板借壳交易过会企业。
### [“拼单”买私募基金？监管提示防范新诈骗陷阱](https://www.yicai.com/news/101400617.html)
> 概要: 深圳证监局指出，非法机构或人员以拼单代为申购私募证券投资基金产品、提供荐股服务收取指导费等事项为由，诱导投资者转款，骗取投资者资金
### [保险业数字化加速，去年全球保险科技融资额大增近1倍](https://www.yicai.com/news/101400655.html)
> 概要: 在数据安全强监管叠加寿险业转型的背景下，寿险业如何更好地进行数字化转型？
### [18 款同价：Dickies 高低帮帆布鞋 89 元清仓（门店 499 元）](https://lapin.ithome.com/html/digi/616475.htm)
> 概要: 【Dickies 鞋类 outlets 折扣店】（淘宝企业店铺）断码清仓：Dickies 男女高低帮帆布鞋报价 239 元，叠加淘宝 55 划算节跨店每满 200 元减 15 元优惠，限时限量 135......
### [Will Hare replace C? Or Rust? Or Zig? Or anything else?](https://harelang.org/blog/2022-05-02-what-is-hares-scope/)
> 概要: Will Hare replace C? Or Rust? Or Zig? Or anything else?
### [Buttondown is a small, elegant tool for producing newsletters](https://buttondown.email/)
> 概要: Buttondown is a small, elegant tool for producing newsletters
### [分析师：SE卖掉西方工作室 主因是漫威授权](https://www.3dmgame.com/news/202205/3841604.html)
> 概要: 近日业内分析师David Gibson透漏SE决定卖掉西方工作室的主因是漫威授权。从2020年起，SE一直在公开谈及《漫威复仇者联盟》和《漫威银河护卫队》商业表现不佳，尤其是前者给公司带来了重大损失......
### [吴世春“五四青年节”寄语“时代后浪”：有定力、有办法、坚持、能忍耐、抗煎熬，一定会迎来光明的未来](https://finance.sina.com.cn/china/2022-05-03/doc-imcwiwst5411991.shtml)
> 概要: “五·四”青年节到来之际，多位重磅财经界大咖为青年送上祝福寄语。 梅花创投创始合伙人吴世春表示：在市场寒冬中坚定对未来乐观的信仰，相信常识，相信走正道的力量...
### [辛利军“五四青年节”寄语“时代后浪”：请不要轻易放弃，请坚守正道](https://finance.sina.com.cn/china/2022-05-03/doc-imcwipii7858905.shtml)
> 概要: “五·四”青年节到来之际，多位重磅财经界大咖为青年送上祝福寄语。 京东零售CEO辛利军表示：19年来，于京东来讲，每一份客户给予的荣耀都来自于每一位员工日积月累的踏实苦...
### [张文“五四青年节”寄语“时代后浪”：期待风华正茂的“时代后浪”，奋勇当先投身硬科技创新](https://finance.sina.com.cn/china/2022-05-03/doc-imcwipii7859077.shtml)
> 概要: “五·四”青年节到来之际，多位重磅财经界大咖为青年送上祝福寄语。 壁仞科技创始人、董事长、CEO张文表示：科学技术进入底层创新时代...
### [张燕生“五四青年节”寄语“时代后浪”：2022年很像1978年，是一个新阶段的开局起步之年](https://finance.sina.com.cn/china/2022-05-03/doc-imcwiwst5413774.shtml)
> 概要: “五·四”青年节到来之际，多位重磅财经界大咖为青年送上祝福寄语。 中国国际经济交流中心首席研究员张燕生表示：2022年很像1978年，是一个新阶段的开局起步之年。
### [俄罗斯竭力避免违约，但前路仍困难重重](https://finance.sina.com.cn/money/future/fmnews/2022-05-03/doc-imcwipii7860528.shtml)
> 概要: 来源：金十数据 俄罗斯违约风险暂时解除，接下来焦点将转移到5月底…… 近期，俄罗斯400亿美元的国际债券及其违约的可能性成为了市场焦点。
### [俞敏洪：新东方业务受疫情影响 但还有钱处理意外情况](https://finance.sina.com.cn/china/gncj/2022-05-03/doc-imcwiwst5414181.shtml)
> 概要: 中新财经5月3日电 3日，俞敏洪在个人公众号“老俞闲话”上发文表示，和其他企业一样，新东方的业务也受到疫情影响。新东方全国各地的学生，几乎都转移到了在线课堂。
### [华硕推出新款外接式 DVD 光驱：内置 USB-C 伸缩收纳线，239 元](https://www.ithome.com/0/616/484.htm)
> 概要: IT之家5 月 3 日消息，今年 2 月份，华硕发布了 ZenDrive V1M 外接式 DVD 刻录机，兼容Windows 11和 macOS，售价 44.90 欧元（约 324.18 元人民币）......
### [视频：意外的惊喜！昆凌透露因买便秘药发现怀三胎](https://video.sina.com.cn/p/ent/2022-05-03/detail-imcwipii7864145.d.html)
> 概要: 视频：意外的惊喜！昆凌透露因买便秘药发现怀三胎
### [视频：周杰伦发文称“六七月发新专辑” 网友问哪年的六七月？](https://video.sina.com.cn/p/ent/2022-05-03/detail-imcwiwst5417699.d.html)
> 概要: 视频：周杰伦发文称“六七月发新专辑” 网友问哪年的六七月？
### [A Philosophy of Software Design – Book Summary and Notes](https://elvischidera.com/2022-04-29-philosphy-software-design/)
> 概要: A Philosophy of Software Design – Book Summary and Notes
### [站酷摄影大赏-五一小长假 九图制霸朋友圈](https://www.zcool.com.cn/activity/ZNTg0.html)
> 概要: 站酷摄影大赏-五一小长假 九图制霸朋友圈
### [高手坐堂—左佐带你领略字里风景](https://www.zcool.com.cn/activity/ZNTY0.html)
> 概要: 高手坐堂—左佐带你领略字里风景
# 小说
### [巡狩江山](http://book.zongheng.com/book/696294.html)
> 作者：伴卿一醉

> 标签：历史军事

> 简介：新书《隐迹在都市中的神》已经发布，欢迎新老读者继续品评。感谢朋友们的支持，努力写好每一章节。

> 章节末：第四百四十四节 大结局

> 状态：完本
# 论文
### [Recovering Hölder smooth functions from noisy modulo samples | Papers With Code](https://paperswithcode.com/paper/recovering-holder-smooth-functions-from-noisy)
> 日期：2 Dec 2021

> 标签：None

> 代码：None

> 描述：In signal processing, several applications involve the recovery of a function given noisy modulo samples. The setting considered in this paper is that the samples corrupted by an additive Gaussian noise are wrapped due to the modulo operation.
### [Coupled Iterative Refinement for 6D Multi-Object Pose Estimation | Papers With Code](https://paperswithcode.com/paper/coupled-iterative-refinement-for-6d-multi)
> 日期：26 Apr 2022

> 标签：None

> 代码：None

> 描述：We address the task of 6D multi-object pose: given a set of known 3D objects and an RGB or RGB-D input image, we detect and estimate the 6D pose of each object. We propose a new approach to 6D object pose estimation which consists of an end-to-end differentiable architecture that makes use of geometric knowledge. Our approach iteratively refines both pose and correspondence in a tightly coupled manner, allowing us to dynamically remove outliers to improve accuracy. We use a novel differentiable layer to perform pose refinement by solving an optimization problem we refer to as Bidirectional Depth-Augmented Perspective-N-Point (BD-PnP). Our method achieves state-of-the-art accuracy on standard 6D Object Pose benchmarks. Code is available at https://github.com/princeton-vl/Coupled-Iterative-Refinement.
