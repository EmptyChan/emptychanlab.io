---
title: 2022-11-21-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BorromeanIslands_ZH-CN0480730115_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-11-21 22:13:54
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BorromeanIslands_ZH-CN0480730115_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [IT产品进入订阅制经营：SaaS产品的会员制收费会面临严峻考验](https://www.woshipm.com/operate/5683975.html)
> 概要: 现在许多产品的会员制收费项目琳琅满目，不仅有普通会员还有超级会员，不仅有个人版、个人企业版还有企业版会员。不禁让人怀疑，我们真的需要这么为一个产品的这么多会员付费吗，我们是不是可以去找其他收费更加简单......
### [品牌盯上“她经济”的红利，却不懂女性？](https://www.woshipm.com/it/5684463.html)
> 概要: 如今，“她经济”已经成为了一股热潮，各大品牌都在打“更懂女性”的营销牌。但这些品牌真的懂女性吗？本篇文章就以运动服饰、汽车和酒水市场为例，分析这些品牌们是通过什么来吸引女性消费的，感兴趣的朋友快来看看......
### [如果可以重来，我不会离开大厂](https://www.woshipm.com/zhichang/5684015.html)
> 概要: 每个人可能都有自己的职业选择和人生规划，比如有的人选择离开大厂，去其他地方寻找更多商机；也有的人开始了自主创业，试图在创业道路上开辟属于自己的赛道……那么，那些离开大厂的年轻人们，现在都过得怎么样了......
### [常见 JavaScript 设计模式 — 原来这么简单](https://segmentfault.com/a/1190000042854744)
> 概要: 设计模式设计模式总共有23种，但在前端领域其实没必要全部都去学习，毕竟大部分的设计模式是在JavaScript中占的比重并不是那么大，本文会列举出一些JavaScript常见的、容易被忽视的设计模式，......
### [精读《维护好一个复杂项目》](https://segmentfault.com/a/1190000042854909)
> 概要: 现在许多国内互联网公司的项目都持续了五年左右，美国老牌公司如 IBM 的项目甚至持续维护了十五年，然而这些项目却有着截然不同的维护成本，有的公司项目运作几年后维护成本依然与初创期不大，可以保持较为高效......
### [士气集团 SHIQI MECHANICAL TOYS](https://www.zcool.com.cn/work/ZNjI5ODI3NDA=.html)
> 概要: 三个月的学习过程，学到了很多以及学会了如何创作属于自己的作品。感谢士气大家庭以及四位老师@小田仙人@林超黑Tt@拾伍小弟@非常严肃的Joker。最开心的是认识到了很多志同道合的朋友同样的特别感谢每一位......
### [快速构建页面结构的 3D Visualization](https://segmentfault.com/a/1190000042855387)
> 概要: 对 Chrome 扩展功能熟悉的小伙伴，可能都有用过 Chrome 的 3D 展示页面层级关系这个功能。可以通过控制台 --> 右边的三个小点 --> More Tools --> Layers打开......
### [除了男足，中国元素霸占了世界杯](https://www.huxiu.com/article/719955.html)
> 概要: 出品 | 虎嗅商业消费组作者 | 周月明编辑 | 苗正卿题图 | 视觉中国“阿根廷的球衣与门票一样，都很难买。”张雷雷告诉虎嗅。他是梅西的粉丝，由于本届世界杯很可能是这位超级巨星职业生涯的最后一届世界......
### [马斯克考虑扩大推特裁员范围，瞄准销售和合作伙伴团队](https://finance.sina.com.cn/tech/internet/2022-11-21/doc-imqqsmrp6934486.shtml)
> 概要: 新浪科技讯 北京时间11月21日早间消息，据报道，知情人士透露，埃隆·马斯克（Elon Musk）最早将于本周一对推特展开进一步裁员......
### [暴雪CEO回应网易续约事件：会尽其所能让中国玩家玩上暴雪游戏](https://finance.sina.com.cn/tech/internet/2022-11-21/doc-imqqsmrp6936844.shtml)
> 概要: IT之家11月21日消息，暴雪娱乐创始人，现任部门总裁MikeMorhaime就近日“暴雪网易无法续约”一事进行了回复，他还承诺“暴雪将尽其所能确保中国的玩家可以玩到我们的游戏”......
### [光荣注册多款元宇宙商标：元宇宙仁王三国志](https://www.3dmgame.com/news/202211/3856561.html)
> 概要: 近日有外媒发现，Square Enix于11月10日在日本注册了“Paranormasight”以及“波多比亚连续杀人事件”商标。《波多比亚连续杀人事件》是由《勇者斗恶龙》系列创作者堀井雄二设计的冒险......
### [除了“笑死”，我不知道说什么了](https://www.huxiu.com/article/719914.html)
> 概要: 本文来自微信公众号：锐见Neweekly （ID：app-neweekly），作者：马路天使，头图来自：《九品芝麻官》剧照如今，我们的线上交流似乎来到了“通话膨胀”时代。只消打开你的微信列表，在聊天记......
### [《辉夜大小姐》剧场版预告公开 圣诞节的恋爱战争](https://acg.gamersky.com/news/202211/1539881.shtml)
> 概要: 动画《辉夜大小姐想让我告白：初吻不会结束》正式预告公开，将于12月17日于日本上映。
### [万字干货——如何用数据做餐饮创业？](https://36kr.com/p/2010719327692421)
> 概要: 万字干货——如何用数据做餐饮创业？-36氪
### [10年时间，16万篇公开文章，我们发现了中国最值钱的662家公司数字化的秘密 | 数字时氪·深度研究](https://36kr.com/p/2010779673896582)
> 概要: 10年时间，16万篇公开文章，我们发现了中国最值钱的662家公司数字化的秘密 | 数字时氪·深度研究-36氪
### [声优岭内智美不再从事声优工作](https://news.dmzj.com/article/76268.html)
> 概要: I'm Enterprise宣布了事务所所属声优岭内智美将于12月31日后，不再从事声优的工作。
### [36氪首发 | AI 生成3D数字服装潮玩，AVAR 成立一年完成三轮融资](https://36kr.com/p/2006615459062665)
> 概要: 36氪首发 | AI 生成3D数字服装潮玩，AVAR 成立一年完成三轮融资-36氪
### [P站美图推荐——吊带袜特辑（三）](https://news.dmzj.com/article/76269.html)
> 概要: 真是个好发明啊！
### [《三体-破壁人.罗辑》绘制过程分享](https://www.zcool.com.cn/work/ZNjI5ODc0MzI=.html)
> 概要: 致敬三体，致敬大刘，三体同人插画第六弹为面壁者罗辑画一幅图，一个被星空仰望的男人~妻子和女而的离开，让他的梦被残酷的现实撕成碎片，爱和恨化作对抗一切的力量，罗辑走在冰湖上，即将要破解宇宙真相。真的恨佩......
### [沙县小吃再掀波澜，淳百味的变革之路](http://www.investorscn.com/2022/11/21/104398/)
> 概要: 在过去的20年里，沙县小吃以“润物细无声”的方式开遍大江南北，即使是在饮食差异巨大的北方市场，扁肉和拌面也变得“所向披靡”......
### [筑路数字经济，科大讯飞全球1024开发者节见证AI“头雁”的力量](http://www.investorscn.com/2022/11/21/104399/)
> 概要: 近日，第五届世界声博会暨2022科大讯飞全球1024开发者节在安徽合肥成功举办。今年大会以“AI向新 数智万物”为主题，设置了发布会、科博会展、AI开发者大赛、1024人才会和AI公益行等多个板块......
### [以进口朱顶红切入中高端花卉市场，「巴特花艺」将希望提供更多优质花卉 | 早期项目](https://36kr.com/p/2009713983677825)
> 概要: 以进口朱顶红切入中高端花卉市场，「巴特花艺」将希望提供更多优质花卉 | 早期项目-36氪
### [Spring Cloud OpenFeign调用流程](https://segmentfault.com/a/1190000042858566)
> 概要: 上一节给大家分享了Spring Cloud OpenFeign的启动流程，接下来给大家分享一下调用流程。话不多说，咱们直接开始。视频：https://www.bilibili.com/video/BV......
### [银发无忧，智慧适老 ——渤海人寿开通“银发直通热线”](http://www.investorscn.com/2022/11/21/104403/)
> 概要: 近日，渤海人寿“银发直通热线”正式开通，该功能可精准识别60周岁（含）以上来电客户，并在30s内直接转接至人工客服为其提供专属服务，是渤海人寿不断提升保险服务“适老化”水平的又一次有益探索......
### [漫画《勿言推理》真人电影化 菅田将晖继续担任主演](https://news.dmzj.com/article/76273.html)
> 概要: 由田村由美创作的漫画《勿言推理》宣布了真人电影化决定的消息。本作与真人电视剧版相同，依然将由菅田将晖担任主演。
### [中国过气电视剧，正在日本下岗再就业](https://www.huxiu.com/article/720429.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。194部、7.52万小......
### [幼儿园的娃打麻将！Jump+首部麻将漫画《雀儿》开始连载](https://news.dmzj.com/article/76275.html)
> 概要: 由平冈一辉创作的新作漫画《雀儿》开始在少年Jump+上连载。《雀儿》讲述了在一所以麻将为绝对正义的幼儿园中的故事。
### [【字幕】戴维斯：球队现在信心十足，想要在菲尼克斯以一场胜利开启客场之旅](https://bbs.hupu.com/56563885.html)
> 概要: 【字幕】戴维斯：球队现在信心十足，想要在菲尼克斯以一场胜利开启客场之旅
### [百度爱采购能给中小企业带来哪些变化](http://www.investorscn.com/2022/11/21/104414/)
> 概要: 大环境下的经济动荡和互联网时代的发展给中小企业带来了不小的影响，很多老板尝试去改变，把受创的企业经济拉动起来，但事与愿违又找不到问题所在，不如跳出问题考虑真正的影响因素是什么......
### [《海岛大亨6》DLC《新边疆》 12月2日Steam发售](https://www.3dmgame.com/news/202211/3856604.html)
> 概要: 海岛管理策略游戏《海岛大亨6》DLC《新边疆》将于12月2日登陆Steam平台，12月5日登陆PS4、PS5、Xbox One和Xbox Series X/S主机平台，支持中文，运行DLC需要游戏本体......
### [世界最大电子图书馆Z-Library站主被捕 侵权诈骗洗钱](https://www.3dmgame.com/news/202211/3856605.html)
> 概要: 习惯去找免费书籍、论文的小伙伴们今后可没那么舒服了，美国 一家号称世界最大电子图书馆Z-Library的两名站主已经被捕，自2009年创站以来，运营长达13年之久如今已经落幕。•在免费电子书界，Z-L......
### [视频：王艺瑾《无法再靠近》MV上线 在欧阳娜娜展览取景拍摄](https://video.sina.com.cn/p/ent/2022-11-21/detail-imqqsmrp7013317.d.html)
> 概要: 视频：王艺瑾《无法再靠近》MV上线 在欧阳娜娜展览取景拍摄
### [三星 Galaxy S20 FE（Exynos 990 版）海外推送安卓 13 / One UI 5.0 正式版](https://www.ithome.com/0/655/502.htm)
> 概要: 感谢IT之家网友你不是真性情的线索投递！IT之家11 月 21 日消息，两周前，三星面向 Galaxy S20、Galaxy S20 + 和 Galaxy S20 Ultra 发布了稳定版安卓 13 ......
### [北证50亮相首日开门红：逾九成成份股收涨，利好还有哪些](https://www.yicai.com/news/101601429.html)
> 概要: 5股涨超5%，森萱医药涨逾9%。
### [接手几率与多大？据报道腾讯已与暴雪接触至少一年](https://www.3dmgame.com/news/202211/3856608.html)
> 概要: 11月17日，暴雪和网易发布公告宣布合作协议到期，即将于2023年1月23日暂停大多数暴雪游戏在中国大陆的运营服务，包括《魔兽世界》、《炉石传说》、《魔兽争霸3》、《守望先锋》、《风暴英雄》、《暗黑破......
### [真真好吃 + 开袋即食：德州乡盛五香鸡 14 元 / 只近期好价](https://lapin.ithome.com/html/digi/655507.htm)
> 概要: 天猫【乡盛食品旗舰店】乡盛五香鸡（360g~420g）*2 只日常售价为 54.9 元，下单领取 25 元优惠券，到手价为 29.9 元 2 只，折合每只仅需 14.95 元。天猫德州 乡盛五香扒鸡（......
### [工信部：1-10 月电信业务收入累计完成 13215 亿元，同比增长 8%](https://www.ithome.com/0/655/508.htm)
> 概要: IT之家11 月 21 日消息，今天工信部网站发布了 2022 年 1－10 月份通信业经济运行情况。1－10 月份，信息通信行业运行平稳。电信业务收入保持稳步增长，电信业务总量较快增长；“物超人”步......
### [易会满再强调中介专业性：“把优秀的公司选出来是水平”](https://www.yicai.com/news/101601448.html)
> 概要: 过于关注“可批性”，对“可投性”重视不够，甚至还有的“带病闯关”。
### [孙俪火了自己心态会否不平衡?陶昕然回应被赞清醒](https://ent.sina.com.cn/v/m/2022-11-21/doc-imqqsmrp7022875.shtml)
> 概要: 新浪娱乐讯 近日，《甄嬛传》“安陵容”的扮演者陶昕然在受访时谈到，出演《甄嬛传》后，自己经常被问到孙俪、谭松韵都火了只有她不火，她表示：“如果我天天跟孙俪比，就只能跳楼了，不可以这样去想问题。”网友也......
### [IT之家实测，高通骁龙 8 Gen 2 旗舰芯片多款跑分成绩公布](https://www.ithome.com/0/655/537.htm)
> 概要: 11 月 16 日，高通全新骁龙旗舰产品 —— 第二代骁龙 8 移动平台正式发布，与第一代相比在各方面都得到了提升。骁龙 8 Gen 2 型号为 SM8550-AB，使用 4 纳米工艺制造，新处理器采......
### [一个餐饮老板关店前的24小时](https://www.huxiu.com/article/696533.html)
> 概要: 本文来自微信公众号：全民故事计划 （ID：quanmingushi），口述：老秦，撰文：张公子，原文标题：《疫情下，一个餐饮老板关店前的24小时》，头图来自：作者供图因为一场巧合，我进入了上海街边一家......
### [600亿，“河北首富”又拿下一个IPO](http://www.investorscn.com/2022/11/21/104415/)
> 概要: 来源 | 投资家（ID：touzijias）......
### [黒五促销：亚马逊英国XSS主机直降60英镑](https://www.3dmgame.com/news/202211/3856613.html)
> 概要: 亚马逊英国已经开始了Xbox Series S黑色星期五降价促销活动。XSS在英国平常售价为250英镑，现在在亚马逊上仅售190英镑。这意味着黒五促销降价60英镑，折扣力度非常大。不过似乎Xbox S......
### [多地全力冲刺四季度，机构建议2023年追求5%以上经济增速](https://finance.sina.com.cn/roll/2022-11-21/doc-imqmmthc5466427.shtml)
> 概要: 21世纪经济报道记者 周潇枭 北京报道 近期推出的“优化疫情防控20条”较大地提振了市场预期。不过，近期一些城市疫情感染数量有攀升趋势，当地防疫政策有所趋严...
### [市场需要新主线引领？](https://www.yicai.com/video/101601641.html)
> 概要: 市场需要新主线引领？
### [潘功胜：全球宏观环境可能由“大缓和”走向“高波动”，人民币资产避险属性日益凸显](https://finance.sina.com.cn/china/gncj/2022-11-21/doc-imqqsmrp7038052.shtml)
> 概要: 人民币债券在全球资产配置中呈现较好分散化效果。 “今年以来，全球主要国家债券普遍收益率上升、价格下跌，人民币债券成为少数价格稳定的金融资产。
### [以快制快，尽快遏制疫情扩散蔓延——二十条优化措施热点问答](https://www.yicai.com/news/101601649.html)
> 概要: 优化调整防控措施不是放松防控，更不是放开、“躺平”，而是适应疫情防控新形势和新冠病毒变异的新特点，进一步提升防控的科学性、精准性。
### [央行将面向6家商业银行推出2000亿元“保交楼”贷款支持计划](https://finance.sina.com.cn/jjxw/2022-11-21/doc-imqqsmrp7039185.shtml)
> 概要: 11月21日，人民银行、银保监会联合召开全国性商业银行信贷工作座谈会，研究部署金融支持稳经济大盘政策措施落实工作。 人民银行党委委员、副行长潘功胜...
### [刚刚，一行两会重磅发声，信息量巨大！](https://finance.sina.com.cn/china/2022-11-21/doc-imqqsmrp7040787.shtml)
> 概要: 来源：国是直通车 正在举行的2022金融街论坛年会上，金融监管部门一行两会主要负责人现身，详解中国金融政策，其中有关房地产市场的政策引发关注。
### [流言板英雄出少年！贝林厄姆成欧文后英格兰世界杯进球最年轻球员](https://bbs.hupu.com/56568528.html)
> 概要: 虎扑11月21日讯 世界杯小组赛首轮，贝林厄姆进球，英格兰队1-0伊朗队。根据统计，现年19岁145天的贝林厄姆成为了迈克尔-欧文之后，在世界杯赛场上代表英格兰队破门的最年轻球员。此前1998年世界杯
### [流言板马奎尔头球助攻，萨卡抽射破门！英格兰2-0伊朗](https://bbs.hupu.com/56568549.html)
> 概要: 虎扑11月21日讯 马奎尔头球助攻，萨卡抽射破门！英格兰2-0伊朗世界杯小组赛第一轮英格兰对阵伊朗。比赛第43分钟，马奎尔助攻萨卡破门，英格兰两球领先。   来源： 虎扑
### [妮基·艾考克斯因病去世 曾出演《邪恶力量》](https://ent.sina.com.cn/v/u/2022-11-21/doc-imqmmthc5470033.shtml)
> 概要: 新浪娱乐讯 美国女演员妮基·艾考克斯，在与白血病搏斗了一年多后于昨日离世，享年47岁，她的家人在社交网络证实了这一消息。据悉，曾出演《邪恶力量》里的女恶魔Meg Masters。(责编：小5)......
### [全文|央行副行长宣昌能在2022金融街论坛年会上的讲话：更好建设金融稳定长效机制 守住不发生系统性风险底线](https://finance.sina.com.cn/china/gncj/2022-11-21/doc-imqmmthc5470232.shtml)
> 概要: 更好建设金融稳定长效机制 守住不发生系统性风险底线 ——人民银行副行长宣昌能在2022金融街论坛年会上的讲话 （2022年11月21日） 尊敬的各位来宾，朋友们： 晚上好！
### [两个半小时高规格座谈会，央行银保监会释放了哪些信号？](https://www.yicai.com/news/101601671.html)
> 概要: 2023年3月31日前，央行将向商业银行提供2000亿元免息再贷款，用于支持“保交楼”，封闭运行、专款专用。
### [流言板1球4助！卢克-肖代表英格兰队最近5次大赛出场，制造5球](https://bbs.hupu.com/56568656.html)
> 概要: 虎扑11月21日讯 世界杯小组赛首轮，卢克-肖助攻贝林厄姆进球，英格兰队目前3-0伊朗队。根据统计，在卢克-肖代表英格兰的最近5次大赛（欧洲杯+世界杯）出场中，他贡献了1球4助，直接参与制造了5粒进球
# 小说
### [彻元](https://book.zongheng.com/book/1168195.html)
> 作者：妖精吃俺一棒

> 标签：历史军事

> 简介：彻，摧毁也！在大元最强大的时候，把它送进坟墓，才算痛快！滚滚铁骑！漫卷亚欧！人类所有已知世界都在马鞭之下颤抖！如果是你，在这个时候，是选择屈服？还是，亡命一搏？反正，林彻，宁愿灿烂的死去，也不愿为四等人！

> 章节末：470.峡谷战结

> 状态：完本
# 论文
### [Crosslinguistic word order variation reflects evolutionary pressures of dependency and information locality | Papers With Code](https://paperswithcode.com/paper/crosslinguistic-word-order-variation-reflects)
> 概要: Languages vary considerably in syntactic structure. About 40% of the world's languages have subject-verb-object order, and about 40% have subject-object-verb order. Extensive work has sought to explain this word order variation across languages. However, the existing approaches are not able to explain coherently the frequency distribution and evolution of word order in individual languages. We propose that variation in word order reflects different ways of balancing competing pressures of dependency locality and information locality, whereby languages favor placing elements together when they are syntactically related or contextually informative about each other. Using data from 80 languages in 17 language families and phylogenetic modeling, we demonstrate that languages evolve to balance these pressures, such that word order change is accompanied by change in the frequency distribution of the syntactic structures which speakers communicate to maintain overall efficiency. Variability in word order thus reflects different ways in which languages resolve these evolutionary pressures. We identify relevant characteristics that result from this joint optimization, particularly the frequency with which subjects and objects are expressed together for the same verb. Our findings suggest that syntactic structure and usage across languages co-adapt to support efficient communication under limited cognitive resources.
### [MiRANews: Dataset and Benchmarks for Multi-Resource-Assisted News Summarization | Papers With Code](https://paperswithcode.com/paper/miranews-dataset-and-benchmarks-for-multi)
> 概要: One of the most challenging aspects of current single-document news summarization is that the summary often contains 'extrinsic hallucinations', i.e., facts that are not present in the source document, which are often derived via world knowledge. This causes summarization systems to act more like open-ended language models tending to hallucinate facts that are erroneous.
