---
title: 2021-09-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BenagilCave_ZH-CN0480408879_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-09-17 21:19:25
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BenagilCave_ZH-CN0480408879_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [实至名归！优麒麟荣获中国信通院《可信开源项目》评估证书](https://www.oschina.net/news/160755)
> 概要: 2021 年 9 月 17 日，在中国信息通信研究院（简称“中国信通院”）主持召开的“云计算开源产业联盟（OSCAR）开源产业大会”上，优麒麟开源项目荣获中国信通院颁发的“可信开源项目”评估证书。麒麟......
### [微软确认 Windows 11 将不支持大多数虚拟机](https://www.oschina.net/news/160652/microsoft-announce-win11-dont-support-most-vm)
> 概要: 微软确认现在需要支持 TPM 2.0 才能在虚拟机上运行 Windows，这意味着用户已无法将 Windows 11 安装在较旧的备用 PC 或笔记本电脑上。在 Windows 11 Insider ......
### [2021 年开源安装包下载量将超 2.2 万亿，开源攻击增长 650%](https://www.oschina.net/news/160643/2021-state-of-the-software-supply-chain)
> 概要: Sonatype 发布了最新的2021 年软件供应链状况报告，共研究了 10 万个生产应用和 400 万个由开发者进行的组件迁移，以及与 Java（Maven Central）、JavaScript（......
### [Vivaldi 4.2 发布，新增隐私友好型搜索引擎并改进翻译功能](https://www.oschina.net/news/160641/vivaldi-4-2-released)
> 概要: Vivaldi 是基于 Chromium 的网络浏览器，近日 Vivaldi 4.2 正式发布，更新内容如下：改进翻译功能翻译功能最初是在 2021 年 6 月发布的 Vivaldi 4.0 中引入的......
### [社区团购，一地鸡毛](https://www.huxiu.com/article/456977.html)
> 概要: 来源｜光子星球（ID：TMTweb）作者｜何芙蓉，编辑｜吴先之头图｜视觉中国“现在用户薅平台的羊毛越来越难了，根本留不住用户。”先后做过橙心优选与多多买菜团长的张肖告诉光子星球，两个月前在社区团购最淡......
### [靠“做图”撑起一只估值400亿美元的超级独角兽](https://www.huxiu.com/article/457064.html)
> 概要: 出品｜虎嗅科技组作者｜张雪封面｜Canva可画，图释：Canva可画联合创始人Melanie Perkins、Cliff Obrecht和Cameron Adams有些沉寂的SaaS领域终于迎来了一个......
### [视频：AKB48 Team SH合作“丽调”传人陆锦花引期待](https://video.sina.com.cn/p/ent/2021-09-17/detail-iktzscyx4730752.d.html)
> 概要: 视频：AKB48 Team SH合作“丽调”传人陆锦花引期待
### [漫威漫画「圣剑」第24期变体封面公开](http://acg.178.com/202109/425846083023.html)
> 概要: 近日，漫威漫画公开了「圣剑」第24期的变体封面，此次封面由菲律宾画师画师Rian Gonzales女士独家绘制，封面人物为「英国队长」贝琪·布拉多克、李千欢、牌皇以及雷克多。本期将于2021年10月6......
### [漫画「黑执事」第31卷封面公开](http://acg.178.com/202109/425848209907.html)
> 概要: 近日，漫画「黑执事」公开了第31卷封面。「黑执事」是日本漫画家枢梁在「月刊GFantasy」2006年10月号开始连载的漫画，讲述了与恶魔签订契约的少爷夏尔·凡多姆海恩与管家塞巴斯蒂安因自身的家族身份......
### [《真女神转生5》恶魔介绍：影之国女王丝卡蒂](https://www.3dmgame.com/news/202109/3823869.html)
> 概要: 世嘉公开了《真女神转生5》恶魔“丝卡蒂”介绍宣传片，丝卡蒂是凯尔特神话中的黑暗女神，点击此处了解往期日更恶魔，本作将于11月11日登陆Switch平台，支持中文。《真女神转生5》恶魔“丝卡蒂”介绍：恶......
### [iPhone 13残血版A15少了一个GPU核心 性能暴降25%](https://www.3dmgame.com/news/202109/3823870.html)
> 概要: 尽管新发布的iPhone 13系列、iPad mini 6均搭载了A15处理器，但严格来说只有iPhone 13 Pro/Pro Max是满血版。这是因为，iPhone 13 mini/13采用的是4......
### [Walkman Archive](http://walkman-archive.com/gadgets/index.html)
> 概要: Walkman Archive
### [「神奇蜘蛛侠」第74期变体封面公开](http://acg.178.com/202109/425849003636.html)
> 概要: 近日，漫威漫画公开了「神奇蜘蛛侠」第74期的变体封面，此次封面由画师Federico Vicentini独家绘制，封面人物为玛丽·简·沃森和她的男友「蜘蛛侠」彼得·帕克。「蜘蛛侠」本名彼得·帕克，是美......
### [《命运》系列1.87亿玩家总游玩时间近110万年](https://www.3dmgame.com/news/202109/3823871.html)
> 概要: 在最新的Bungie博客文章中，Bungie透露了一些有关《命运》系列状态的统计数据。自2014年推出首部游戏以来，该系列的两款游戏已经拥有超过1.87亿独立玩家，总共游玩了98亿小时。而100亿小时......
### [困在出租屋里的大城市年轻人](https://www.huxiu.com/article/457045.html)
> 概要: 本文来自微信公众号：中国青年研究（ID：china-youth-study），作者：项军、刘飞，原文标题：《特大城市青年房租客的结构、境遇与心态》，头图来自：《我在他乡挺好的》截图租房问题尚未构成住房......
### [组图：杜之克离开TVB 曾制作《使徒行者2》等多部合拍剧](http://slide.ent.sina.com.cn/tv/slide_4_704_361664.html)
> 概要: 组图：杜之克离开TVB 曾制作《使徒行者2》等多部合拍剧
### [翻译团交易还是留下：NBA的顶级交易目标们](https://bbs.hupu.com/45246167.html)
> 概要: 应当交易走他们还是留下他们？对于拥有已经出现在交易流言中或可能在赛季中的某个时刻出现在交易流言中的球员的那些NBA球队，这是他们需要考虑的问题。保留上述两类球员的利弊是什么？他们什么时候可以作为自由球
### [组图：张元英新冠痊愈后首晒照 穿花裙面色红润状态好](http://slide.ent.sina.com.cn/star/k/slide_4_704_361686.html)
> 概要: 组图：张元英新冠痊愈后首晒照 穿花裙面色红润状态好
### [「薄樱鬼」新作OVA动画全三章预告PV公开](http://acg.178.com/202109/425859919894.html)
> 概要: 「薄樱鬼」新作OVA动画公开了全三章预告PV，该作将于2021年11月起，连续3月每月上线一章。「薄樱鬼」新作OVA全三章预告PV新宣传绘：第一章「茅花流し、雲隠れの刻」2021年11月13日第二章「......
### [吕布出场率独一档，胜率即将突破51，这英雄是否有些超标了？](https://bbs.hupu.com/45246787.html)
> 概要: 吕布出场率直接断层独一档，胜率突破51，这英雄大家觉得强度如何？是否有些超标了？削弱的话，应该削弱哪些方面呢？
### [DappRadar报告：这个夏天的链上流量由游戏和NFT主导](https://www.tuicool.com/articles/BBZfyyz)
> 概要: 原文来源：Dappradar原文作者：Pedro Herrera编译：Katie 辜DappRadar区块链用户行为报告这次将重点放在游戏上。在这份报告中，我们将分析全球趋势，包括用户统计数据和连通性......
### [焦点分析丨我们迄今为止对“中视频”的理解，可能都是错的](https://www.tuicool.com/articles/iuuE32R)
> 概要: 距离“中视频”的概念首次被提出已经过去了一年，在9月15日的第五届西瓜PLAY好奇心大会上，西瓜视频总裁任利锋再次为赛道“标点”，并称“中视频已经迎来多屏时代”。到底什么才是中视频呢？一年前任利锋阐释......
### [《一人之下4》碧游村篇定档924！](https://new.qq.com/omn/ACF20210/ACF2021091700706300.html)
> 概要: 动画《一人之下4》是原作漫画中人气最高的篇章之一。“碧游村”篇不但集合了冯宝宝、张楚岚、王也、诸葛青等人气角色，更有陈朵、王震球、肖自在、马仙洪等一众极具魅力的新角色登场！今天，动画终终终终于宣布将于......
### [萌美主播陪看番  虎牙直播《狐妖小红娘·月红2》专场开启](https://new.qq.com/omn/ACF20210/ACF2021091700720000.html)
> 概要: 超人气动画《狐妖小红娘》首部特别篇《月红2》目前正在腾讯视频独家热播中！作为月红篇的延续，《月红2》揭秘了东方月初和涂山红红二人在正片中未曾讲述的那段空白记忆。500年前他们二人大战背后有着怎样的爱恨......
### [EuroBSDCon 2021](https://2021.eurobsdcon.org/about/program/)
> 概要: EuroBSDCon 2021
### [《DOTA2》官宣：TI10 门票 9 月 22 日发售，决赛票约 1530 元](https://www.ithome.com/0/576/135.htm)
> 概要: IT之家9 月 17 日消息 今日，V社官方正式公布了《DOTA2》TI10 国际邀请赛的现场门票开售日期，将于 2021 年 9 月 22 日开售，决赛门票约 1530 元。DOTA2 国际邀请赛将......
### [全世界开了最多星巴克的城市，已经饱和了吗？](https://www.huxiu.com/article/457159.html)
> 概要: 题图来自视觉中国2000年5月，上海第一家星巴克落户淮海路力宝广场，虽然这并不是星巴克在中国的第一家店，但在随后的20多年里，上海迅速成为全球星巴克门店最多的城市，现在几乎是曾经排名第一的城市——韩国......
### [《怪猎:崛起》本社联动第4弹 “莱西”牙猎犬外观](https://www.3dmgame.com/news/202109/3823892.html)
> 概要: 卡普空今日（9月17日）宣布了《怪物猎人：崛起》本社联动第四弹——洛克人“莱西”牙猎犬外观服装。宣传视频：在《怪物猎人：崛起》完成联动活动任务就可以获得能化身为《洛克人11》角色“莱西”的牙猎犬外观装......
### [9月16日12亿美元ETH从中心化交易所撤出，日流出量创新高](https://www.tuicool.com/articles/uABZfae)
> 概要: 价值超过10亿美元的以太坊在24小时内从中心化交易所移出，随着许多交易场所的供应减少，导致人们猜测以太坊的价格即将上涨。根据加密分析提供商IntoTheBlock分享的数据，9月16日，价值12亿美元......
### [Shenandoah in OpenJDK 17: Sub-millisecond GC pauses](https://developers.redhat.com/articles/2021/09/16/shenandoah-openjdk-17-sub-millisecond-gc-pauses)
> 概要: Shenandoah in OpenJDK 17: Sub-millisecond GC pauses
### [视频：张檬为金恩圣庆生大方示爱 海边烟火中亲密拥吻](https://video.sina.com.cn/p/ent/2021-09-17/detail-iktzqtyt6588468.d.html)
> 概要: 视频：张檬为金恩圣庆生大方示爱 海边烟火中亲密拥吻
### [《一人之下》第四季9月24日播出！临时工齐聚碧游村](https://acg.gamersky.com/news/202109/1424529.shtml)
> 概要: 《一人之下》第四季“碧游村篇”的最新预告，并宣布该季动画将于9月24日在播出。
### [《一人之下4》定档9月24日！成都嘉年华直播即将开启](https://new.qq.com/omn/ACF20210/ACF2021091700799900.html)
> 概要: 好消息好消息！中秋小长假终于迈着愉快的步伐离我们越来越近了，《一人之下4》也在粉丝们的激情催更中定档了！9月24日即将独家登陆腾讯视频！中秋除了赏月、吃月饼等传统活动之外，这个假期还有一场浪漫的二次元......
### [视频：黄宗泽时隔4年重返TVB 主演《法证先锋5》下月开拍](https://video.sina.com.cn/p/ent/2021-09-17/detail-iktzscyx4825109.d.html)
> 概要: 视频：黄宗泽时隔4年重返TVB 主演《法证先锋5》下月开拍
### [分布式数字身份：通往未来的钥匙？](https://www.tuicool.com/articles/jYfmiqj)
> 概要: “身份”是每个人出生就伴有的印记，根据时空的不同，人们拥有不同的身份属性，而人们的身份属性存在着唯一的证明关系。身份认同是组织形成的必要条件，在现实世界中，通常通过物理介质以卡片化的形式给予身份的的制......
### [中国脑计划正式启动：首年拨款经费超 30 亿元，整体规模达数百亿](https://www.ithome.com/0/576/166.htm)
> 概要: IT之家9 月 17 日消息 据第一财经报道，9 月 16 日，科技部网站正式发布科技创新 2030“脑科学与类脑研究”重大项目 2021 年度项目申报指南的通知（下称“指南”），涉及 59 个研究领......
### [iPhone 13 发售在即，拼多多百亿补贴直降 500！](https://www.ithome.com/0/576/187.htm)
> 概要: IT之家9 月 17 日消息，苹果近日正式发布了iPhone 13/Pro 系列四款手机，以及全新的iPad、iPad mini。目前，苹果中文官网已经更新，新品价格全部曝光。众望所归的拼多多百亿补贴......
### [A Go Package for Building Progressive Web Apps](https://go-app.dev/)
> 概要: A Go Package for Building Progressive Web Apps
### [转生史莱姆47：米莉姆演戏，十大魔王只有两人看透，萌王两次错过](https://new.qq.com/omn/20210917/20210917A0DFWC00.html)
> 概要: 转生史莱姆第二季就快要结束了，应该只剩最后一集“八星魔王”了。克莱曼胸有成竹、机关算尽，最后是一场空。米莉姆从始至终都是在演戏，克莱曼没看出来，很多魔王都没看出来。            十大魔王中，......
### [流言板鲍尔默：Intuit是完美满足我们对科技创新等要求的合作伙伴](https://bbs.hupu.com/45250504.html)
> 概要: 虎扑09月17日讯 今日，快船官方和Intuit共同宣布双方达成了为期23年的战略合作伙伴关系，快船未来新球馆的独家冠名权将归Intuit所有，新球馆将被命名为Intuit Dome。Intuit是一
### [消息称小米云流量将于 9 月 30 日停止服务](https://www.ithome.com/0/576/199.htm)
> 概要: IT之家9 月 17 日消息 据网友反馈，小米云流量 QQ 群运营人员对外发布称，小米云流量 App 将于 9 月 30 日停止服务。首先非常感谢米粉用户，在 3 年多的时间里对云流量的大力支持，在次......
### [《我的砍价女王》口碑爆棚，吴谨言全程撒糖不断，林更新甜到发齁](https://new.qq.com/omn/20210912/20210912A09H7S00.html)
> 概要: 林更新，吴谨言的热播剧《我的砍价女王》成功出圈，点播率直接破亿，虽然有不少营销号对其吐槽，但恰恰说明，该剧的影响力对其他影视剧都略胜一筹，不然的话，也不会有这么多人关注，而且讽刺的是几乎都是看完才说不......
### [21次上春晚，从“小品王”到黯然退场，赵本山经历了什么？](https://new.qq.com/omn/20210917/20210917A0ERKI00.html)
> 概要: 2010年，《乡村爱情3》播出，赵本山带着徒弟们参加由专家学者组成的研讨会。2年前，赵本山带着《乡村爱情2》参加过这样的会议。当年，赵本山非常谦虚，他称在座的专家都是医生：“大家今天都是来给我和《乡村......
### [财经TOP10|重磅！中国正式提出申请加入CPTPP 有何重要意义？](https://finance.sina.com.cn/china/caijingtop10/2021-09-17/doc-iktzscyx4866813.shtml)
> 概要: 【政经要闻】 NO.1 重磅！中国正式提出申请加入CPTPP 有何重要意义 据商务部网站，9月16日，中国商务部部长王文涛向《全面与进步跨太平洋伙伴关系协定》（CPTPP）保存方新...
### [女星嫁入豪门住什么房？郭晶晶家坐拥泳池海滩，王艳家住故宫边](https://new.qq.com/omn/20210917/20210917A0ET7C00.html)
> 概要: 圈中有不少女明星嫁入豪门， 嫁入财力雄厚的豪门之后，自然也就住上了豪门标配的豪宅。虽然豪宅都是豪华奢侈的，但装修、景致等都是各有不同。王艳是大家熟悉的女演员，她在《还珠格格》中饰演的晴儿一角，温柔大方......
### [高能电玩节：横板动作游戏《听风者也》新实机演示](https://www.3dmgame.com/news/202109/3823924.html)
> 概要: 国产抗倭题材横板动作游戏《听风者也》新实机演示公布，本作由纸老虎工作室开发，Gamera Game发行，游戏将于今年9月23日发售。视频：游戏杂糅了写意画、水彩画、油画技法等多种表现手法的国风美术，采......
### [福建厦门市海沧区将启动第二轮全员新冠病毒核酸检测](https://finance.sina.com.cn/china/dfjj/2021-09-17/doc-iktzscyx4865223.shtml)
> 概要: 原标题：福建厦门市海沧区将启动第二轮全员新冠病毒核酸检测 福建厦门市海沧区应对新冠肺炎疫情工作指挥部刚刚发布〔2021〕第6号通告...
### [从带娃到带队，“爹系队长”张晋的出圈之路](https://new.qq.com/omn/20210917/20210917A0EV9N00.html)
> 概要: 自从《披荆斩棘的哥哥》这个节目开播之后，不瞒大家说，南风每个周五都会准时蹲守，每期更新必上热搜，哥哥们瞬间在全网刷屏，这排面不得不说一句：哥哥们真·实火无疑。节目更新到现在，每一位哥哥都给大家留下了深......
### [北京第二批宅地出让补充公告：竞得人需提交购地资金来源说明](https://finance.sina.com.cn/china/gncj/2021-09-17/doc-iktzscyx4865905.shtml)
> 概要: 澎湃财讯 9月17日，北京市规划自然资源委官方发布了针对二批次用地出让的补充公告，就高标准商品住宅建设方案评选内容和评分标准、购地资金审查要求...
### [华北空管局：北京两场节前航班高峰将在9月18日](https://finance.sina.com.cn/china/gncj/2021-09-17/doc-iktzscyx4865902.shtml)
> 概要: 澎湃财讯 9月17日，澎湃新闻记者从华北空管局获悉，今年中秋小长假期间，首都、大兴两机场计划进出港航班4550架次，其中首都机场计划进出港航班2600架次...
### [上海内部渠道6折买新房？海归硕士“杀熟”骗房款打赏主播](https://finance.sina.com.cn/jjxw/2021-09-17/doc-iktzqtyt6631080.shtml)
> 概要: 原标题：上海内部渠道6折买新房？海归硕士“杀熟”骗房款打赏主播 亲戚热情介绍可6折买房，岂料买到的“新房”竟是租来的，而买房钱竟被打赏主播消耗一空。
### [流言板40岁前实德外援马丁打破尘封近50年纪录，贝巴社媒向其致敬](https://bbs.hupu.com/45250928.html)
> 概要: 虎扑09月17日讯 近日，前大连实德外援马丁-坎布罗夫打进了个人在保加利亚顶级联赛的第254粒进球，就此超越佩塔尔-日科夫在1974年退役时创下的253球纪录，成为历史射手王。2010年夏窗，坎布罗夫
### [端掉一个黑嘴，十万骗子失业？网红荐股被判19年，揭开“盘后票”黑幕](https://finance.sina.com.cn/chanjing/cyxw/2021-09-17/doc-iktzscyx4867265.shtml)
> 概要: 贝壳财经原创出品 9月16日晚，据央视《焦点访谈》报道，近日，一起操纵证券市场的犯罪案件作出宣判，主犯吴承泽因操纵证券市场罪被判处有期徒刑8年...
# 小说
### [封神战史](http://book.zongheng.com/book/912412.html)
> 作者：洵溪

> 标签：武侠仙侠

> 简介：商朝末年，各个方国混战，众仙或独自苟活，辛苦修仙，或加入战争，得个人间富贵，但战死则只能封为神魂。为了富贵，众仙是加入冒险抑或独善其身？

> 章节末：426 二女之争

> 状态：完本
# 论文
### [SwinIR: Image Restoration Using Swin Transformer | Papers With Code](https://paperswithcode.com/paper/swinir-image-restoration-using-swin)
> 日期：23 Aug 2021

> 标签：None

> 代码：https://github.com/jingyunliang/swinir

> 描述：Image restoration is a long-standing low-level vision problem that aims to restore high-quality images from low-quality images (e.g., downscaled, noisy and compressed images). While state-of-the-art image restoration methods are based on convolutional neural networks, few attempts have been made with Transformers which show impressive performance on high-level vision tasks.
### [Identification of Driver Phone Usage Violations via State-of-the-Art Object Detection with Tracking | Papers With Code](https://paperswithcode.com/paper/identification-of-driver-phone-usage)
> 日期：5 Sep 2021

> 标签：None

> 代码：https://github.com/carrell-ncl/windscreen2

> 描述：The use of mobiles phones when driving have been a major factor when it comes to road traffic incidents and the process of capturing such violations can be a laborious task. Advancements in both modern object detection frameworks and high-performance hardware has paved the way for a more automated approach when it comes to video surveillance.
