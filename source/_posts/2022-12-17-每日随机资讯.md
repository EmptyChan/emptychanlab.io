---
title: 2022-12-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GlacierGoats_ZH-CN0764810245_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-17 19:07:05
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GlacierGoats_ZH-CN0764810245_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [接一个第三方支付，开发说要2个月？](https://www.woshipm.com/pd/5708060.html)
> 概要: 很多产品在进行支付的时候，都有第三方支付的选项，这些年第三方支付的方式也在不断增加。这就要求产品在设计支付渠道时，要考虑到拓展性。本文作者对此进行了分析，希望对你有帮助。产品：我们近期需要增加一个支付......
### [双十二没落史：从绿叶到鸡肋的一生](https://www.woshipm.com/it/5708293.html)
> 概要: 相比于双十一和618的声势浩大，双十二在一片静默中悄无声息地结束了。消费者经历了多个购物节的洗礼，已经没有了太大的购物欲望，社交媒体上也甚少关于双十二的讨论。本文作者对这届双十二展开了分析，与你分享......
### [玛莎拉蒂X波司登：4299元的羽绒服，你买吗？](https://www.woshipm.com/marketing/5707901.html)
> 概要: 品牌联名在如今已经不是新鲜事，一些恰到好处的联名甚至能引发消费者的抢购，比如在今年火爆的椰树和瑞幸的联名款咖啡。但不是每一个品牌联名都能赢得消费者认可，翻车的也不在少数。本文作者围绕品牌联名展开分析，......
### [招聘广告确认 Guerrilla正开发《地平线》合作游戏](https://www.3dmgame.com/news/202212/3858606.html)
> 概要: 泄露者曾表示Guerrilla正在开发一款开放世界多人合作游戏，现在开发商似乎也证明了这一点。油管UP主JorRaptor今日发现在Guerrilla官网上有许多招聘岗位都与这款游戏有关。其中首席设计......
### [中央经济工作会议：支持住房改善、有效防范化解优质头部房企风险](https://www.yicai.com/news/101626549.html)
> 概要: 在今年的中央经济工作会议中，“着力扩大国内需求”也被摆在了明年经济工作的首位，并明确“支持住房改善”。
### [《托尔图加：海盗传说》1月19日登陆各大平台](https://www.3dmgame.com/news/202212/3858609.html)
> 概要: 发行商Kalpyso Media和开发商Gaming Minds Studios宣布，海盗战略冒险游戏《托尔图加：海盗传说》（Tortuga:A Pirate’s Tale）将于2023年1月19日登......
### [音乐格斗游戏《摇滚之神》发行日敲定4/18](https://www.3dmgame.com/news/202212/3858610.html)
> 概要: 发行商Modus Games和开发商ModusStudios Brazil宣布，音乐节奏/格斗混合游戏《摇滚之神》将于2023年4月18日登陆PlayStation 5、Xbox Series、Pla......
### [冬天一到，电动车又成你爹了](https://www.huxiu.com/article/741590.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | Teslarati这个冬天，瑞士的“电动爹”很可能要被禁止上路了。事情是这样的。据《每日电讯报》报道称，瑞士官员已经起草了一项限制电力使用的......
### [搞甲醇汽车，为什么不挣钱？](https://36kr.com/p/2047436796711944)
> 概要: 搞甲醇汽车，为什么不挣钱？-36氪
### [情怀瞬间拉满 《艾尔登法环》32位主机降质版演示](https://www.3dmgame.com/news/202212/3858616.html)
> 概要: 油管频道Rustic Games BR分享视频，展示《艾尔登法环》在32位PlayStation或土星机上的画面效果。视频还原的不仅是画面效果，在玩法和视角上也让人瞬间找回那个年代的感觉。《艾尔登法环......
### [鸟山明《沙漠大冒险》动画化预告 预计将于明年推出](https://acg.gamersky.com/news/202212/1548236.shtml)
> 概要: 万代南梦宫宣布，将对鸟山明原创漫画《沙漠大冒险 SAND LAND》进行动画化，最新预告已发布，一起来看看。
### [先瑞达并购启示录：港股18A的逆市而生](https://36kr.com/p/2047403370910981)
> 概要: 先瑞达并购启示录：港股18A的逆市而生-36氪
### [卖口罩的江苏公司，抓紧冲击IPO](https://36kr.com/p/2047547527386376)
> 概要: 卖口罩的江苏公司，抓紧冲击IPO-36氪
### [屡遭黑公关，十八数藏披露六大谣言以正视听](http://www.investorscn.com/2022/12/17/104881/)
> 概要: 12月9日，新华网刊发报道，称数字藏品企业TOP30榜单企业“十八数藏”披露，数字藏品行业存在“不良竞争”现象，值得有关部门关注......
### [权威认可！坚持提升消费者体验，追觅科技获CCCCA服务口碑奖!](http://www.investorscn.com/2022/12/17/104882/)
> 概要: 12月16日,由中国信息协会客户联络中心分会、客户观察主办的2022 年（第七届）中国客户联络中心行业发展年会（以下简称“年会”）隆重举行。年会期间，2022年度中国客户联络中心奖（CCCCA）入选榜......
### [表热清颗粒——具有地方特色新冠肺炎防治民族药](http://www.investorscn.com/2022/12/17/104883/)
> 概要: 被誉为“中国四大民族医药”之一的傣医药历史悠久，早在2500年前的《贝叶经》中就有相关记载。两千多年来傣族人民在与疾病斗争的过程中，总结积累了丰富的防病治病的经验方药，逐渐形成了一套系统完善、独具民族......
### [追觅科技荣获“星球奖2022”两项大奖，实力撑起国货品牌新高度](http://www.investorscn.com/2022/12/17/104884/)
> 概要: 12月17日，星球奖BrandStar Awards 2022（以下简称「星球奖2022」）获奖名单公布。中国创新科技品牌追觅科技荣获“年度品牌奖-年度行业新消费品牌”和“年度品牌奖-年度出海品牌”两......
### [芯源微推出浸没式高产能涂胶显影机新品，已通过客户端验证](https://www.ithome.com/0/661/784.htm)
> 概要: 感谢IT之家网友航空先生的线索投递！IT之家12 月 17 日消息，芯源微是国内涂胶显影细分领域龙头。该公司今日宣布，在公司成立二十周年之际召开新品发布会，推出最新一代的前道浸没式高产能涂胶显影机 F......
### [德国为什么唯美国马首是瞻？](https://www.huxiu.com/article/744712.html)
> 概要: 本文来自微信公众号：秦朔朋友圈 （ID：qspyq2015），作者：汤拯，原文标题：《德国为什么唯美国马首是瞻？——从三个罕为人知的视角重审中德合作》，头图来自：视觉中国21世纪以来，美国政治精英在不......
### [TV动画《北海道辣妹金古锥》公开新视觉图](https://news.dmzj.com/article/76564.html)
> 概要: TV动画《北海道辣妹金古锥》公开新视觉图!
### [漫画作品《夜樱家的大作战》宣布TV动画化](https://news.dmzj.com/article/76565.html)
> 概要: 漫画作品《夜樱家的大作战》宣布TV动画化!
### [Netflix 上周播放榜：《哈里王子与梅根》成首播周观看时长最高的纪录片](https://www.ithome.com/0/661/804.htm)
> 概要: IT之家12 月 17 日消息，据 Netflix 官方消息，在上周，热播剧《星期三》依旧是观看时长第一的剧集。此外，新上线的《哈里王子与梅根》成为 Netflix 首播周观看时长最高的纪录片。IT之......
### [《火影忍者：佐助烈传》将动漫化！明年1月开播](https://acg.gamersky.com/news/202212/1548288.shtml)
> 概要: 《火影忍者 佐助烈传》将动漫化。
### [“新国货榜样”秋林里道斯：老字号如何发光？](https://www.yicai.com/news/101626704.html)
> 概要: “新国货榜样”秋林里道斯：老字号如何发光？
### [我们与17位CFO聊了聊，现在大家日子好过吗？](https://www.huxiu.com/article/745029.html)
> 概要: 在经历了持续的疫情防控和大宗商品波动后，企业正在面临什么样的境况？现金流是否见底？采取了哪些措施降本增效？本文来自微信公众号：经济观察报 （ID：eeo-com-cn），作者：杜涛，题图来自：视觉中国......
### [新能源跑出一家百亿独角兽](https://36kr.com/p/2047874659390725)
> 概要: 新能源跑出一家百亿独角兽-36氪
### [TV动画《勇者死了！》公开第1弹PV](https://news.dmzj.com/article/76566.html)
> 概要: TV动画《勇者死了!》公开第1弹PV.
### [传输网络中的“易容术”——PWE3](https://www.ithome.com/0/661/814.htm)
> 概要: 01、“钱很多”的疑惑王老板经营着一家叫 PTN（Packet Transport Network 分组传送网）的公司，这家公司主要是以光纤作为媒介，提供各种语音和数据业务的传输技术。PTN 公司凭借......
### [腾龙镜头型号 B028 佳能卡口固件更新：改善半按快门键时 AF 的工作](https://www.ithome.com/0/661/816.htm)
> 概要: IT之家12 月 17 日消息，腾龙发布了 18-400mm F / 3.5-6.3 Di II VC HLD （型号 B028）佳能卡口的固件更新。腾龙今年 10 月在官网公布了 18-400mm ......
### [Steam Deck次世代机型将对屏幕和电池进行优化](https://www.3dmgame.com/news/202212/3858649.html)
> 概要: Steam Deck现已正式发售，而Valve针对这款机器推出了90多个升级补丁，修补bug并持续优化玩家体验。那么这台掌机的未来会是怎样呢?在接受媒体采访时，Steam Deck设计师Lawrenc......
### [视频：岳云鹏曝全家感染新冠 称头痛欲裂还经常放屁](https://video.sina.com.cn/p/ent/2022-12-17/detail-imxwyscm0516490.d.html)
> 概要: 视频：岳云鹏曝全家感染新冠 称头痛欲裂还经常放屁
### [视频：张靓颖对“主动想阳”公开道歉 提醒网友防护避免二次感染](https://video.sina.com.cn/p/ent/2022-12-17/detail-imxwyscr6792934.d.html)
> 概要: 视频：张靓颖对“主动想阳”公开道歉 提醒网友防护避免二次感染
### [视频：陈慧琳与13岁儿子遭遇车祸 经纪人报平安回应两人伤情](https://video.sina.com.cn/p/ent/2022-12-17/detail-imxwysci3739312.d.html)
> 概要: 视频：陈慧琳与13岁儿子遭遇车祸 经纪人报平安回应两人伤情
### [刘尚希：对明年的经济困难要充分估计，回到正常增长轨道需要很强动力](https://finance.sina.com.cn/roll/2022-12-17/doc-imxwyscq0031518.shtml)
> 概要: 12月17日，在2023《财经》年会上，中国财政科学研究院院长刘尚希表示，要充分估计明年的经济困难和经济风险。当前经济有不少亮点，但形势难以令人乐观...
### [重磅！“准冠军”基金经理万家黄海：价值股行情至少持续两年！只有集中才能获得超额收益](https://finance.sina.com.cn/wm/2022-12-17/doc-imxwyscq0032346.shtml)
> 概要: 年末将至，离基金业绩排位赛结束还有不到半个月。Wind数据显示，截至12月16日，今年以来回报排名前三的是由“黑马”基金经理黄海管理的三只产品——万家宏观择时多策略...
### [金融稳定法明年有望出台](https://finance.sina.com.cn/roll/2022-12-17/doc-imxwyscq0039631.shtml)
> 概要: 转自：中证网 e公司讯，中国人民银行副行长刘国强12月17日在由中国国际经济交流中心举办的2022-2023中国经济年会上表示，在党中央关怀下...
### [“习酒创立70年·故事习酒” 全球华语大赛征文启事](https://finance.sina.com.cn/china/gncj/2022-12-17/doc-imxwyscm0519082.shtml)
> 概要: 中国诗歌网 “感恩奋进新征程，同心筑梦向未来。”在贵州习酒70年波澜壮阔的历史进程中，习酒香飘世界，从酱香白酒核心产区赤水河谷出发，一路芬芳流淌，远渡关山重楼...
### [中央重磅文件明确互联网医疗服务可用医保支付！还有哪些症结未解](https://finance.sina.com.cn/china/gncj/2022-12-17/doc-imxwyscq0051043.shtml)
> 概要: 目前，互联网医疗的医保支付主要局限在医保定点医疗机构的线上平台，而并非在第三方问诊平台。 当下，互联网医疗机构已加入到新冠防治的“主战场”...
### [中央重磅文件明确互联网医疗服务可用医保支付！还有哪些症结未解](https://www.yicai.com/news/101626768.html)
> 概要: 目前，互联网医疗的医保支付主要局限在医保定点医疗机构的线上平台，而并非在第三方问诊平台。
### [扩大内需战略规划纲要利好风光公用环保领域，行业未来如何发展](https://www.yicai.com/news/101626765.html)
> 概要: 《纲要》分别从需求侧和供给侧对风光公用环保领域做出指导，再次强调了能源安全、产业链安全以及绿色低碳消费的重要性。
### [韩文秀：明年我国经济可能总体回升 形成独立向上运行轨迹 |  权威解读](https://www.yicai.com/video/101626786.html)
> 概要: 中央财经委员会办公室分管日常工作的副主任韩文秀在会上表示，我国经济韧性强、潜力大、活力足，长期向好的基本面没有变，资源要素条件可支撑。综合研判，明年世界经济增速可能会明显下滑，而我国经济可能总体回升，从而形成一个独立的向上运行的轨迹。
### [一图流火舞、王昭君互秀，酷偕夏侯惇脱节反打，被集火击杀](https://bbs.hupu.com/56998680.html)
> 概要: 虎扑12月17日讯 2022王者世冠KIC四分之一决赛第二日，佛山DRG.GK对阵XYG第一局比赛中，火舞、王昭君互秀，酷偕夏侯惇脱节反打，被集火击杀   来源： 虎扑    标签：XYG佛山GK
### [流言板温格召开FIFA技术研讨会，讨论中场绞杀、中锋革新、定位球](https://bbs.hupu.com/56998763.html)
> 概要: 虎扑12月17日讯 FIFA技术总监阿尔塞纳-温格和技术顾问克林斯曼当地时间12月17号召开官方的FIFA研讨会，对于本届世界杯的一些数据进行了分析。温格和FIFA技术组将本届杯赛的逼抢称为“中场绞杀
### [流言板范弗利特谈欧文绝杀：并不惊讶，向他致敬](https://bbs.hupu.com/56998863.html)
> 概要: 虎扑12月17日讯 今天猛龙116-119被篮网绝杀。赛后，猛龙后卫弗雷德-范弗利特接受了记者的采访。谈到凯里-欧文的绝杀，范弗利特说：“我并不惊讶。就那个球而言，他是世界上最强的。你必须要向他致敬。
### [2022德玛西亚杯D4：LML 1-0 LYA](https://bbs.hupu.com/56998905.html)
> 概要: LML更胜一筹拿下关键一分，保留晋级希望。 来源：  新浪微博
# 小说
### [怪谈降临](http://book.zongheng.com/book/1103663.html)
> 作者：桀桀叹息着

> 标签：悬疑灵异

> 简介：“不是吧，又碰见厉鬼啦！”主角吴用发出痛苦呐喊。他无意中进入黑暗世界，其中布满厉鬼怪谈：血皮剪刀手，镜鬼，红衣女人，无面女......面对恐怖威胁，他该何去何从......当现实受到黑暗世界影响后，厉鬼横生，他又会做出怎样的斗争。

> 章节末：一些想说的话

> 状态：完本
# 论文
### [Distance Learner: Incorporating Manifold Prior to Model Training | Papers With Code](https://paperswithcode.com/paper/distance-learner-incorporating-manifold-prior)
> 日期：14 Jul 2022

> 标签：None

> 代码：https://github.com/microsoft/distance-learner

> 描述：The manifold hypothesis (real world data concentrates near low-dimensional manifolds) is suggested as the principle behind the effectiveness of machine learning algorithms in very high dimensional problems that are common in domains such as vision and speech. Multiple methods have been proposed to explicitly incorporate the manifold hypothesis as a prior in modern Deep Neural Networks (DNNs), with varying success. In this paper, we propose a new method, Distance Learner, to incorporate this prior for DNN-based classifiers. Distance Learner is trained to predict the distance of a point from the underlying manifold of each class, rather than the class label. For classification, Distance Learner then chooses the class corresponding to the closest predicted class manifold. Distance Learner can also identify points as being out of distribution (belonging to neither class), if the distance to the closest manifold is higher than a threshold. We evaluate our method on multiple synthetic datasets and show that Distance Learner learns much more meaningful classification boundaries compared to a standard classifier. We also evaluate our method on the task of adversarial robustness, and find that it not only outperforms standard classifier by a large margin, but also performs at par with classifiers trained via state-of-the-art adversarial training.
### [Multi-task head pose estimation in-the-wild | Papers With Code](https://paperswithcode.com/paper/multi-task-head-pose-estimation-in-the-wild-1)
> 概要: We present a deep learning-based multi-task approach for head pose estimation in images. We contribute with a network architecture and training strategy that harness the strong dependencies among face pose, alignment and visibility, to produce a top performing model for all three tasks. Our architecture is an encoder-decoder CNN with residual blocks and lateral skip connections. We show that the combination of head pose estimation and landmark-based face alignment significantly improve the performance of the former task. Further, the location of the pose task at the bottleneck layer, at the end of the encoder, and that of tasks depending on spatial information, such as visibility and alignment, in the final decoder layer, also contribute to increase the final performance. In the experiments conducted the proposed model outperforms the state-of-the-art in the face pose and visibility tasks. By including a final landmark regression step it also produces face alignment results on par with the state-of-the-art.
