---
title: 2023-02-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MauiWhale_ZH-CN6664793962_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-19 21:27:08
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MauiWhale_ZH-CN6664793962_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [抖音外卖，前途光明，困难更多](https://www.woshipm.com/it/5758614.html)
> 概要: 随着抖音外卖在北上广内测，不少消费者已经率先尝试了在抖音上点外卖，压力明显给到了美团和饿了么这边。抖音外卖能走多远呢？本文作者对抖音外卖进行了分析，希望对你有帮助。2023年开年，没曾想互联网的战争首......
### [不敢裸辞、降薪跳槽，这个春招有多冷？](https://www.woshipm.com/zhichang/5758505.html)
> 概要: 马上就要到金三银四了，你有想要换工作的意愿吗？职场在经历了三年的低潮期，会焕发出新机吗？本文作者采访了几位职场人士，来看看他们各自的故事，也许会给你带来一些启发。又到一年“金三银四”，度过了疫情三年寒......
### [禁网18天，第127次点开视频软件](https://www.woshipm.com/user-research/5756899.html)
> 概要: #本文为人人都是产品经理《原创激励计划》出品。走在路上，随处可见“低头族”，在餐厅里，甚至能看到一对情侣低着头玩各自的手机。从什么时候开始，手机占据了我们全部的视线？网上曾经有个问题，给你足够的钱，一......
### [仰望银河背后，吉利是真着急了](https://www.huxiu.com/article/798604.html)
> 概要: 出品丨虎嗅汽车组作者丨周到编辑丨张博文头图丨《英雄本色》太阳系里，已经快放不下中国汽车品牌了。2月16日，吉利汽车集团官宣了中高端新能源汽车系列品牌——“吉利银河”，并宣布将在2月23日在杭州举行品牌......
### [帮网友给文俊辉发语音表白权顺荣 张大大发文道歉](https://ent.sina.com.cn/y/yneidi/2023-02-19/doc-imyhepyz4776546.shtml)
> 概要: 新浪娱乐讯 近日，张大大直播间连麦的粉丝说想认识权顺荣，张大大直接让她给文俊辉发语音表白，引起粉丝热议。随后，张大大微博发文表示：“文弟弟一直很好也很有礼貌，下了直播后感觉是不是太打扰他了又给他补上了......
### [《辐射4》新MOD加入DLSS2、FSR2.0和XeSS支持](https://www.3dmgame.com/news/202302/3863033.html)
> 概要: 本月初，作者PureDark为《辐射4》发布新MOD，让这款游戏支持NVIDIA DLSS2、AMD FSR 2.0和Intel XeSS功能。根据MOD作者介绍，这款DLSS/FSR2/XeSS M......
### [ChatGPT这一战，科技巨头将重新洗牌](https://36kr.com/p/2136697415859972)
> 概要: ChatGPT这一战，科技巨头将重新洗牌-36氪
### [下周关注丨2月LPR报价将公布，这些投资机会最靠谱](https://finance.sina.com.cn/roll/2023-02-19/doc-imyheyqx8659147.shtml)
> 概要: 新一期LPR将公布；下周629亿市值限售股解禁；沪市首批年报将出炉；美联储将公布2月货币政策会议纪要。 【重磅新闻】 新一期LPR将公布 2月20日...
### [王毅同布林肯会面，谈了飞艇事件](https://finance.sina.com.cn/china/2023-02-19/doc-imyhfewx5426039.shtml)
> 概要: 外交部网站、新华社 当地时间2月18日，中共中央政治局委员、中央外事工作委员会办公室主任王毅在出席慕尼黑安全会议期间，应美方请求...
### [开放世界冒险《Tchia》预览视频 推荐RTX 2060](https://www.3dmgame.com/news/202302/3863037.html)
> 概要: 在IGN Fan Fest活动上，开放世界冒险游戏《Tchia》游戏总监带来了新的实机预览视频，深度展示了《Tchia》广泛的穿越和探索选择，独特的灵魂跳跃机制，该机制可让玩家控制你碰到的任何物体或动......
### [联想拒绝“漂绿”](https://www.huxiu.com/article/797481.html)
> 概要: 出品 | 虎嗅ESG组作者 | 张小予头图 | 视觉中国本文是#ESG进步观察#系列第012篇文章本次观察关键词：净零排放目标、供应链管理近日，联想集团发布了净零排放目标(Net-Zero)路线图，并......
### [杨丞琳下班后粉丝大声应援：我是欠你们钱吗？](https://ent.sina.com.cn/y/ygangtai/2023-02-19/doc-imyheyqt1459565.shtml)
> 概要: 新浪娱乐讯 近日，粉丝在杨丞琳下班后应援大喊“杨丞琳！”杨丞琳听到后：“请问我是欠你们钱吗？你们是来讨债的吗？”下班后粉丝温柔打招呼，杨丞琳：“我还是欠你们钱好啦”。(责编：漠er凡)......
### [ChatGPT能够颠覆医疗AI吗？](https://36kr.com/p/2137999357233537)
> 概要: ChatGPT能够颠覆医疗AI吗？-36氪
### [独生子女不能全部继承父母遗产？答案来了！](https://finance.sina.com.cn/china/2023-02-19/doc-imyheyqz5482638.shtml)
> 概要: 来源：法治日报 作者：朱雨晨 朱婵婵 母亲突然去世， 父亲早已失智， 为了厘清母亲遗产的继承份额， 上海女子宋颖不得不 自己将“自己”告上了法庭。
### [山内一典发布《GT赛车7》1.29版四辆新车剪影](https://www.3dmgame.com/news/202302/3863040.html)
> 概要: 《GT赛车7》的1.29更新档将于2月21日发布，并加入PSVR2支持。如今系列制作人山内一典证实，这次更新档还会加入其它内容，并承诺这将会是一次“大”更新。按照惯例，在游戏更新之前山内一典都会发布推......
### [订购PSVR2的美国用户已开始收到发货通知](https://www.3dmgame.com/news/202302/3863041.html)
> 概要: 部分提前订购PSVR2的用户已经开始在网络上分享发货通知邮件。目前美国地区的大量消费者已经收到发货邮件通知，接下来世界其它地区也将陆续发货。多位用户分享发货邮件截图，其中显示预计送达时间为2月22日......
### [中国公司全球化周报｜电竞运营商英雄体育VSPO募资2.65亿美元；难做的直播电商业务，Instagram选择退出](https://36kr.com/p/2137314850834562)
> 概要: 中国公司全球化周报｜电竞运营商英雄体育VSPO募资2.65亿美元；难做的直播电商业务，Instagram选择退出-36氪
### [《如懿传》小聚合照曝光 童谣李纯张佳宁等出镜](https://ent.sina.com.cn/2023-02-19/doc-imyhfewr1337923.shtml)
> 概要: 新浪娱乐讯 2月19日，黄澜微博晒照，配文：“昨天《如懿传》小范围欢聚，大家长久不见都很亲切。想念一起工作的日子，从北京开机到内蒙拍草原，又转去江南拍水戏，最后扎在横店，八个多月几千场戏啊。“好怀念我......
### [刘洁任黄冈市代市长](https://finance.sina.com.cn/china/2023-02-19/doc-imyhfewx5449027.shtml)
> 概要: 来源：政事儿 据湖北《黄冈日报》消息，2月18日，黄冈市第六届人民代表大会常务委员会第八次会议通过，接受李军杰同志辞去黄冈市人民政府市长职务的请求...
### [特斯拉追尾后续：体验店称销售受影响；威马汽车否认千万级项目“智慧停车”靠黄牛刷单](https://www.yicai.com/news/101678966.html)
> 概要: 洞悉行业趋势，把握产业需求，一财EV观察，点击「听新闻」，一键收听。
### [北交所混合做市将上线，首批36只标的股是如何被选中的？](https://www.yicai.com/news/101679025.html)
> 概要: 做市商详解首批标的股遴选标准。
### [必应版 ChatGPT 竟爱上用户并引诱其离婚，微软：别问 15 个以上问题，不然它会疯](https://www.ithome.com/0/674/374.htm)
> 概要: 联网后，新必应会爆粗、会写黄文、会 PUA 人类了，但是如此放飞、如此有「人味」的必应，让网友简直爱不释手，直呼太喜欢！ChatGPT 彻底放飞了！接入必应后的 ChatGPT 变了，如今它会开黄腔，......
### [设计时速 350 公里，渝昆高铁川渝段“四电”工程正式启动](https://www.ithome.com/0/674/375.htm)
> 概要: IT之家2 月 19 日消息，据“西南铁路”公众号消息，重庆至昆明高速铁路（简称“渝昆高铁”）川渝段“四电”工程于 2 月 18 日正式启动，为全线贯通奠定了坚实基础。“四电”工程是高铁建设中通信、信......
### [《地平线：西之绝境》数据 玩家在水下待了两千多年](https://www.3dmgame.com/news/202302/3863043.html)
> 概要: 《地平线：西之绝境》发售已经有一年时间了。近日Guerrilla Games在网络上分享了本作的部分统计数据。其中一个非常有意思的地方在于，玩家在这款游戏中水下探索时间的总时长达到了2330年。在PS......
### [“我毕业五年存款5000”，算失败吗？](https://www.huxiu.com/article/798807.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：唐亚华 李秋涵 邹帅 王敏，编辑：李秋涵，头图来自：视觉中国毕业五年的时候，你存款多少？最近，一则标题为《我毕业5年，存款5000......
### [马云澳洲“走亲戚”](https://www.huxiu.com/article/798055.html)
> 概要: 作者：王芳洁，头图来自：视觉中国马云又去澳大利亚了。他第一次去，已经是38年前了。有网友看见照片调侃，马总肯定是去买房子的。我好奇查了一下，2022年，澳大利亚全国房屋价值指数下降5.3%。有投行表示......
### [谷歌没“Money”了](https://36kr.com/p/2136967489996932)
> 概要: 谷歌没“Money”了-36氪
### [ChatGPT加速虚拟人蜕变？“有趣的灵魂”正向我们走来](https://www.yicai.com/video/101679084.html)
> 概要: ChatGPT加速虚拟人蜕变？“有趣的灵魂”正向我们走来
### [《中国奇谭》今日非会员收官 官方微博发图庆贺](https://acg.gamersky.com/news/202302/1568570.shtml)
> 概要: 《中国奇谭》今日非会员收官，官方微博发海报庆贺了这一时刻。
### [《霍格沃茨之遗》暂无开发 DLC 的计划，新内容还得等等](https://www.ithome.com/0/674/395.htm)
> 概要: IT之家2 月 19 日消息，根据《哈利波特》改编的游戏《霍格沃茨之遗》近日火热发售，霸榜了 Steam 的周销量榜。据开发商 Avalanche Software 的最新消息，《霍格沃茨之遗》暂无开......
### [经济复苏快递行业迎增长，顺丰1月数据展现积极信号](http://www.investorscn.com/2023/02/19/105805/)
> 概要: 2月19日午间,顺丰披露2023年首份成绩单。1月,顺丰营业收入210.85亿元。其中,速运物流业务营业收入为163.24亿元,同比增长-5.53%;业务量为9.57亿票,同比增长-3.24%......
### [TV动画《关于我转生成为史莱姆的那件事》第3季2024年春播出](https://news.dmzj.com/article/77192.html)
> 概要: 根据川上泰树、伏濑原作轻小说改编的TV动画《关于我转生成为史莱姆的那件事》宣布了第三季将于2024年春播出。
### [2月19日这些公告有看头](https://www.yicai.com/news/101679141.html)
> 概要: 2月19日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [20元人民币“华中渔翁”去世，盘点各版人民币劳动者人物原型](https://finance.sina.com.cn/jjxw/2023-02-19/doc-imyhfvup8564798.shtml)
> 概要: 2月18日，据央视新闻消息，广西桂林阳朔县漓江景区发布视频称，20元人民币上的渔夫的原型黄全德于18日凌晨4时离世，享年94岁。他曾被网友称为20元人民币背景中“华中渔翁”的...
### [硅料厂商步步加码下游业务，特变电工拟40亿投建分布式项目](https://www.yicai.com/news/101679145.html)
> 概要: 硅料厂的野心不止于上游材料
### [动画《在异世界获得超强能力的我，在现实世界照样无敌》4月6日播出](https://news.dmzj.com/article/77194.html)
> 概要: TV动画《在异世界获得超强能力的我，在现实世界照样无敌～等级提升改变人生命运～》将于4月6日播出，官网今日（2月19日）公布了最新PV、声优阵容、制作等情报消息。
### [Z 世代，出走京东](https://www.ithome.com/0/674/416.htm)
> 概要: 作者丨习习编辑丨文八娘近日，“京东 31 亿元拿地”词条登上微博热搜。被传疑似京东主体竞得地块总价 31.12 亿元，位于亦庄核心区，正对京东集团总部；且据公开信息，此地块“多规合一”，集商业、住宅、......
### [流言板半场数据：巴黎控球率44%，射门5-11，射正3-4，角球2-4](https://bbs.hupu.com/58107097.html)
> 概要: 虎扑02月19日讯 巴黎圣日耳曼与里尔的比赛半场结束，目前巴黎2-1领先。在上半场，由姆巴佩为球队首开纪录，内马尔扩大领先优势。【半场数据】（居左为巴黎）控球率 44%——56%射门 5——11射正
### [流言板内马尔半场贡献传射，2次关键传球，2次成功过人+1次被犯规](https://bbs.hupu.com/58107195.html)
> 概要: 虎扑02月19日讯 巴黎圣日耳曼与里尔的比赛半场结束，目前巴黎2-1领先。在上半场，内马尔贡献传射。【内马尔半场数据】进球 1助攻 1触球 29传球 15/20（75%）关键传球 2创造机会 1过人（
### [流言板穆勒Ins：不能去抱怨昨天发生的事情，结果很艰难还需努力](https://bbs.hupu.com/58107264.html)
> 概要: 虎扑02月19日讯 德甲第21轮比赛中，拜仁客场2-3不敌门兴，结束了各项赛事连续20场不败。赛后，本场比赛为球队首发登场仅16分钟的穆勒更新Instagram。穆勒在Ins中写道：现在不能去抱怨昨天
### [流言板布朗：不惊讶独行侠交易来欧文，期待东欧组合表现](https://bbs.hupu.com/58107352.html)
> 概要: 虎扑02月19日讯 今天凯尔特人球员杰伦-布朗接受了记者的采访。谈到独行侠通过交易得到凯里-欧文，布朗说：“我并没觉得惊讶。我觉得我们都等着看欧文和东契奇的组合会如何。我觉得，这个组合令人激动，肯定很
# 小说
### [都市里面有个修真者](http://book.zongheng.com/book/1033367.html)
> 作者：曹目人

> 标签：武侠仙侠

> 简介：一个普通人突然得到上界至强法宝该怎么办(⊙o⊙)！

> 章节末：第七十四章  结局

> 状态：完本
# 论文
### [Invariants for neural automata | Papers With Code](https://paperswithcode.com/paper/invariants-for-neural-automata)
> 日期：4 Feb 2023

> 标签：None

> 代码：https://github.com/TuringMachinegun/Turing_Neural_Networks

> 描述：Computational modeling of neurodynamical systems often deploys neural networks and symbolic dynamics. A particular way for combining these approaches within a framework called vector symbolic architectures leads to neural automata. An interesting research direction we have pursued under this framework has been to consider mapping symbolic dynamics onto neurodynamics, represented as neural automata. This representation theory, enables us to ask questions, such as, how does the brain implement Turing computations. Specifically, in this representation theory, neural automata result from the assignment of symbols and symbol strings to numbers, known as G\"odel encoding. Under this assignment symbolic computation becomes represented by trajectories of state vectors in a real phase space, that allows for statistical correlation analyses with real-world measurements and experimental data. However, these assignments are usually completely arbitrary. Hence, it makes sense to address the problem question of, which aspects of the dynamics observed under such a representation is intrinsic to the dynamics and which are not. In this study, we develop a formally rigorous mathematical framework for the investigation of symmetries and invariants of neural automata under different encodings. As a central concept we define patterns of equality for such systems. We consider different macroscopic observables, such as the mean activation level of the neural network, and ask for their invariance properties. Our main result shows that only step functions that are defined over those patterns of equality are invariant under recodings, while the mean activation is not. Our work could be of substantial importance for related regression studies of real-world measurements with neurosymbolic processors for avoiding confounding results that are dependant on a particular encoding and not intrinsic to the dynamics.
### [CEN-HDR: Computationally Efficient neural Network for real-time High Dynamic Range imaging | Papers With Code](https://paperswithcode.com/paper/cen-hdr-computationally-efficient-neural)
> 日期：10 Feb 2023

> 标签：None

> 代码：https://github.com/steven-tel/cen-hdr

> 描述：High dynamic range (HDR) imaging is still a challenging task in modern digital photography. Recent research proposes solutions that provide high-quality acquisition but at the cost of a very large number of operations and a slow inference time that prevent the implementation of these solutions on lightweight real-time systems. In this paper, we propose CEN-HDR, a new computationally efficient neural network by providing a novel architecture based on a light attention mechanism and sub-pixel convolution operations for real-time HDR imaging. We also provide an efficient training scheme by applying network compression using knowledge distillation. We performed extensive qualitative and quantitative comparisons to show that our approach produces competitive results in image quality while being faster than state-of-the-art solutions, allowing it to be practically deployed under real-time constraints. Experimental results show our method obtains a score of 43.04 mu-PSNR on the Kalantari2017 dataset with a framerate of 33 FPS using a Macbook M1 NPU.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
