---
title: 2021-02-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Parrotfish_EN-CN1212515803_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-02-19 21:50:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Parrotfish_EN-CN1212515803_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [陈思诚但凡有贾玲一半真心，《唐探3》也不至于崩成这样](https://www.huxiu.com/article/410142.html)
> 概要: 本文来自微信公众号：宅总有理（ID：zmrben115），作者：宅少，原文标题：《陈思诚要有贾玲一半真心，<唐探3>也不至于崩成这样》，头图：《唐探3》海报“二流的作品，比广为流传的作品活得还久，其中......
### [TV动画「后空翻!!」第3弹PV公开](http://acg.178.com//202102/407697604793.html)
> 概要: 体操题材原创TV动画「后空翻!!」（バクテン!!）公开了第3弹PV，该动画将于2021年4月播出。TV动画「后空翻!!」第3弹PVCAST双叶翔太郎：土屋神叶美里良夜：石川界人七ヶ浜政宗：小野大辅築馆......
### [《陈情令》音乐人林海取关小27岁娇妻 曾发文护妻](https://ent.sina.com.cn/y/yneidi/2021-02-19/doc-ikftssap6889764.shtml)
> 概要: 新浪娱乐讯 2月19日，有网友发现音乐家林海已经取关小他27岁的娇妻，女方关注列表也没有林海，疑似感情出现问题。　　据悉，2017年林海突然离婚后，曾在好友高晓松的帮助下高调征婚，林海还表示自己的邮箱......
### [《银魂 THE FINAL》决战之后影像公开 欢乐日常再来](https://acg.gamersky.com/news/202102/1363619.shtml)
> 概要: 动画电影《银魂 THE FINAL》目前正在热映中，到场观看人数已经突破了100万人。官方公开了新的本篇影像，展现了银时等人在和虚最终决战之后的日常生活，搞笑风格依旧。
### [卡普空3月举办特别直播 介绍《怪猎崛起》等新情报](https://www.3dmgame.com/news/202102/3808862.html)
> 概要: 今日（2月19日），卡普空官方宣布将于北京时间3月8日晚上10点，3月9日晚上7点举办《怪物猎人》特别直播节目，届时会公开有关《怪物猎人：崛起》，《怪物猎人物语2》的新情报。在3月9日的直播活动中，卡......
### [动画「剃须。然后捡到女高中生」PV第2弹公开](http://acg.178.com//202102/407703620236.html)
> 概要: 近日，动画「剃须。然后捡到女高中生」官方公开了第2弹宣传PV，其中介绍了男主角吉田与荻原沙优等几位主要女角色的相遇故事。本作将于2021年4月播出。「剃须。然后捡到女高中生」PV第2弹CAST吉田：興......
### [知情人曝吴秀波今后将不再从事演员职业](https://ent.sina.com.cn/s/m/2021-02-19/doc-ikftpnny7923850.shtml)
> 概要: 新浪娱乐讯 近日，吴秀波被女友敲诈案宣判，女友陈昱霖被判有期徒刑三年，缓刑三年，并处罚金人民币十万元。新浪娱乐从接近吴秀波的知情人处获悉，吴秀波今后将不再从事演员职业：“按照相关法律，陈昱霖原本可能判......
### [「命运之夜——天之杯3：春之歌」最新PV公开 数字特绘封面解禁](http://acg.178.com//202102/407703841012.html)
> 概要: 近日，「Fate」系列剧场版动画「命运之夜——天之杯3：春之歌」公开了最新宣传PV，其中展现了卫宫士郎与言峰绮礼进行激烈冲突的场景，并将该场景设计为数字特绘封面呈现出来。该剧场版动画将于3月31日正式......
### [《狐妖小红娘》落地济南共赏牛郎织女鹊桥相会 探访最古老电影院](https://new.qq.com/omn/ACF20210/ACF2021021900657000.html)
> 概要: 我住长江头，君住长江尾，夜夜思君不见君，共饮长江水。对于相爱的恋人而言，最怕的莫过于分别，但同时分别又会让相许变得更加珍重，就像越陈越香的酒。中国的古典爱情故事总少不了对“分别”的描述，而这其中属《牛......
### ["七朵花"赵小侨宣布怀孕 曾历经2次试管失败](https://ent.sina.com.cn/s/h/2021-02-19/doc-ikftpnny7981860.shtml)
> 概要: 新浪娱乐讯 据台媒报道，41岁的赵小侨与老公刘亮佐结婚3、4年以来，一直尝试怀孕，人工受孕过程辛苦，1月中出席记者会时才说，若今年再不成功就要放弃，没想到19日就宣布好消息：“致所有一路以来关心我的朋......
### [净化评论区的Jigsaw有害评论过滤器, 每天接收超5亿次处理请求](https://www.tuicool.com/articles/Bn6nUf7)
> 概要: 编者按：本文来自微信公众号“将门创投”（ID:thejiangmen），作者：让创新获得认可，36氪经授权发布。From: VentureBeat；编译: Shelly这个时代的网络环境鱼龙混杂，有时......
### [《吸血鬼：避世血族 纽约同僚》现已加入官方中文](https://www.3dmgame.com/news/202102/3808883.html)
> 概要: 根据开发商Draw Distance公开的新消息，《吸血鬼：避世血族纽约同僚》（Vampire: The Masquerade - Coteries of New York）现已加入官方中文支持。在1......
### [《暴雪街机合集》通过评级 包含《失落维京人》三作](https://www.3dmgame.com/news/202102/3808886.html)
> 概要: 和往年一样，在暴雪嘉年华正式开幕之前，总会充斥着各种爆料。有网友发现一款名为《Blizzard Arcade Collection（暴雪街机合集）》的PC平台游戏在今日（2月19日）通过评级，看起来很......
### [Checked C](https://github.com/microsoft/checkedc)
> 概要: Checked C
### [漫画《滑头鬼之孙》作者椎桥宽开始连载新作！](https://news.dmzj1.com/article/70153.html)
> 概要: 漫画《滑头鬼之孙》的作者椎桥宽在本日（19日）发售的ULTRA JUMP3月号（集英社）上，开始了新作《岩元前辈的推荐》的连载。
### [《圣女的魔力是万能的》4月播出 PV、音乐情报公开](https://news.dmzj1.com/article/70155.html)
> 概要: TV动画《圣女的魔力是万能的》将于4月开始播出，先导PV、制作人员以及音乐的情报都已经公开了。
### [口碑不错：Vstarry 纯棉休闲裤 38.9 元大促（立减 30 元）](https://lapin.ithome.com/html/digi/535765.htm)
> 概要: 石磨水洗+柔软细腻：Vstarry 纯棉休闲裤 日常售价68.9元，可领限量30元券，实付38.9元包邮，领券并购买。有9款可选，需自行购买运费险。之家不少小伙伴都下单过此裤吧，反馈都还不错。一乖给家......
### [视频：网传少女时代将完整回归？SM娱乐回应称暂未决定](https://video.sina.com.cn/p/ent/2021-02-19/detail-ikftssap7161243.d.html)
> 概要: 视频：网传少女时代将完整回归？SM娱乐回应称暂未决定
### [下一趟旅程是？《终钥之歌》首部官方小说《终钥时代》完结！](https://new.qq.com/omn/ACF20210/ACF2021021901041800.html)
> 概要: 没坑、没鸽，全程高能又精彩！《终钥之歌》首部官方主线小说——《终钥时代》于2月17日正式完结！年过完了，追的小说竟然也一起完结了？！还有比这更惨的事情嘛……但对于《终钥之歌》的读者们而言，一个旧时代的......
### [Uber drivers are workers not self employed, Supreme Court rules](https://www.bbc.co.uk/news/business-56123668)
> 概要: Uber drivers are workers not self employed, Supreme Court rules
### [游戏王历史：从零开始的游戏王环境之旅第六期24](https://news.dmzj1.com/article/70160.html)
> 概要: “光之援军”的日本之行让【光道】得到了大幅度强化、瞬间成为了环境中屈指可数的顶级牌组。为了对抗这样的【光道】，以【次元艾托斯】为首的各种Meta牌组站了出来，但从环境整体来看还是【光道】更有优势。游戏正在遭受“光污染”的10月，出现了一些些许动摇...
### [84岁谢贤近照曝光，身体硬朗状态好，曾被疯传脑中风去世](https://new.qq.com/omn/20210219/20210219A0BKVS00.html)
> 概要: 2月19日，台媒曝出谢霆锋父亲、知名港星谢贤的近况，84岁的谢贤虽然年事已高，但这几年还是传出不少风流事，像和小女友COCO的藕断丝连被人津津乐道。            最离谱的当属这两年越传越凶的......
### [阔太金巧巧称后悔来浪姐，没镜头还翻红失败？求圈内人给机会就业](https://new.qq.com/omn/20210219/20210219A0BOEK00.html)
> 概要: 2月19日下午，阔太金巧巧在某社交平台上发长文回顾《乘风破浪的姐姐2》的经历。并配文称“总结成一句话：我后悔来这个节目了，因为：想翻红失败”，看来金巧巧有些后悔来参加浪姐。            在金......
### [小米造车，图什么？](https://www.huxiu.com/article/410330.html)
> 概要: 题图 | 富贵“小米又双叒叕要造车啦！”一条看起来非常似曾相识的消息，再次经由数码圈、汽车圈等媒体的发酵，引发了众多的讨论。2月19日，据《晚点LatePost》报道，称根据多个消息源，已经确定小米将......
### [百度又被相信了？](https://www.tuicool.com/articles/JZ32M3y)
> 概要: 编者按：本文来自微信公众号“市界”（ID:ishijie2018），作者：李楠，编辑：李曙光，36氪经授权发布。20岁这一年，百度开始重返互联网企业的中心舞台。从2018年5月陆奇出走百度，至今过去3......
### [ROG 游戏手机 5 将于 3 月 10 日发布](https://www.ithome.com/0/535/802.htm)
> 概要: IT之家 2 月 19 日消息 华硕官网上线倒计时，ROG 游戏手机 5 将于 3 月 10 日发布。IT之家了解到，之前 Geekbench 网站显示，这款智能手机将运行 Android 11，搭载......
### [郭晓东让老婆赶紧回家做饭惹争议，程莉莎上线护夫：两口子的幽默](https://new.qq.com/omn/20210219/20210219A0C6I700.html)
> 概要: 《浪姐2》新一期开播，金巧巧、程莉莎以及阿兰等淘汰，金巧巧长文回应后悔上《浪姐2》，因为翻红失败，输在没有流量节目中镜头太少，只有几分钟的镜头。            程莉莎希望“有一天，我还想重新站......
### [哈里梅根夫妇宣布不以王室成员身份重回英国王室](https://ent.sina.com.cn/s/u/2021-02-19/doc-ikftssap7255308.shtml)
> 概要: BBC最新消息，萨塞克斯公爵和公爵夫人（哈里和梅根夫妇）告诉英国女王伊丽莎白二世，他们不会以王室成员的身份回王室工作。白金汉宫也证实，哈里和梅根将不再履行王室职责，哈里将放弃他的荣誉军衔。(责编：小5......
### [中央深改委：统筹制定2030年前碳排放达峰行动方案](https://finance.sina.com.cn/china/2021-02-19/doc-ikftpnny8241672.shtml)
> 概要: 原标题：中央深改委：统筹制定2030年前碳排放达峰行动方案 证券时报网讯，据央视新闻消息，中共中央总书记、国家主席、中央军委主席...
### [中央深改委：加快优质医疗资源扩容和区域均衡布局](https://finance.sina.com.cn/china/2021-02-19/doc-ikftpnny8242153.shtml)
> 概要: 原标题：中央深改委：加快优质医疗资源扩容和区域均衡布局 证券时报网讯，据央视新闻消息，中共中央总书记、国家主席、中央军委主席...
### [3DM轻松一刻第463期 大家怎么会不想看女装呢？](https://www.3dmgame.com/bagua/4319.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！新春快乐科技，改变世界要加油穿衣服果然地位不一样......
### [韩国DJ模特Henney福利图 长相乖巧却身材饱满](https://www.3dmgame.com/bagua/4321.html)
> 概要: 今天为大家带来的是韩国DJ Henney福利图。妹子上大学时主修会计学，获得AICPA认证后，就职于韩国的一家投资公司。工作无聊，就按自己的爱好做了DJ兼模特。Henney清纯可爱身材饱满，给人以邻家......
### [房贷持续严监管：广州排查同年申请经营贷+按揭](https://finance.sina.com.cn/china/gncj/2021-02-19/doc-ikftssap7274076.shtml)
> 概要: 原标题：独家ㅣ房贷持续严监管：广州排查同年申请经营贷+按揭 深圳按揭批贷仍未确定总价基准 春节假期之后，广州等地仍在继续排查房抵经营贷违规流入楼市的情况...
### [银保监会将迎“一正四副”主席格局：肖远企或升任副主席](https://finance.sina.com.cn/roll/2021-02-19/doc-ikftssap7276086.shtml)
> 概要: 原标题：银保监会将迎“一正四副”主席格局：肖远企或升任副主席 分管领域基本确定 21世纪经济报道记者近日多方获悉，现年54岁的银保监会首席风险官...
### [外汇局：2020年我国经常账户顺差2989亿美元](https://finance.sina.com.cn/roll/2021-02-19/doc-ikftssap7274476.shtml)
> 概要: 新华社北京2月19日电（记者刘开雄）国家外汇管理局19日发布国际收支平衡表初步数据显示，2020年，我国经常账户顺差2989亿美元，与同期国内生产总值（GDP）之比为2%...
### [骗保将重罚 这个县的县委书记、医保局副局长也因骗保事件被免](https://finance.sina.com.cn/china/gncj/2021-02-19/doc-ikftpnny8256495.shtml)
> 概要: 原标题：骗保将重罚！这个县的县委书记、医保局副局长也因骗保事件被免！ 据中国政府网19日消息，《医疗保障基金使用监督管理条例》（简称管理条例）已经2020年12月9日国务...
### [热依扎：《山海情》和孩子拯救了我…](https://new.qq.com/omn/20210219/20210219A0DAFJ00.html)
> 概要: 很少有电视剧播着播着，豆瓣评分不降反升的，确实是部非常优秀非常好看的剧。            开播时，豆瓣评分9.2，等到剧完结，评分稳定在了9.4，超过了正午之前的《琅琊榜》和《父母爱情》。   ......
### [《你好，李焕英》火了，陈赫却因为两个离婚的女人，被骂个不停](https://new.qq.com/omn/20210219/20210219A0DBDV00.html)
> 概要: 平心而论，你觉得陈赫在《你好，李焕英》中演的冷特，咋样？在电影中，贾玲，沈腾都是老面孔了，不足为“特”。张小斐是新人，反正是新面孔，也就不能太多评头论足。只有陈赫，作为男二，作为不老不新的二线演员。让......
# 小说
### [临界血线](http://book.zongheng.com/book/505223.html)
> 作者：天晨

> 标签：都市娱乐

> 简介：漫天的火雨，肆虐的惊雷，谁才是这一时代的主宰？妖媚的九尾，霸道的青龙，谁能动彻于九天之上？在异能和武学交织的世界里，他一路走来，历经生死与心痛，蓦然回首，仍旧飒然一人，踏上自我救赎的道路......QQ群：713475303，欢迎大家一起探讨

> 章节末：新书《时间制御者》预告

> 状态：完本
# 论文
### [Kernel Smoothing, Mean Shift, and Their Learning Theory with Directional Data](https://paperswithcode.com/paper/kernel-smoothing-mean-shift-and-their)
> 日期：23 Oct 2020

> 标签：None

> 代码：https://github.com/zhangyk8/DirMS

> 描述：Directional data consist of observations distributed on a (hyper)sphere, and appear in many applied fields, such as astronomy, ecology, and environmental science. This paper studies both statistical and computational problems of kernel smoothing for directional data.
### [UniLMv2: Pseudo-Masked Language Models for Unified Language Model Pre-Training](https://paperswithcode.com/paper/unilmv2-pseudo-masked-language-models-for)
> 日期：28 Feb 2020

> 标签：LANGUAGE MODELLING

> 代码：https://github.com/microsoft/unilm

> 描述：We propose to pre-train a unified language model for both autoencoding and partially autoregressive language modeling tasks using a novel training procedure, referred to as a pseudo-masked language model (PMLM). Given an input text with masked tokens, we rely on conventional masks to learn inter-relations between corrupted tokens and context via autoencoding, and pseudo masks to learn intra-relations between masked spans via partially autoregressive modeling.
