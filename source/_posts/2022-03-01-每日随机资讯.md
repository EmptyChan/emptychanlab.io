---
title: 2022-03-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ZugspitzeGipfelstation_ZH-CN6120971585_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-03-01 23:05:03
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ZugspitzeGipfelstation_ZH-CN6120971585_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Go 语言新提案「arena」：优化内存分配](https://www.oschina.net/news/184539/go-proposal-arena)
> 概要: Go 语言社区正在讨论名为「arena」的新提案。根据提案的介绍，「Go arena」用于优化内存分配。arena 是一种从连续的内存区域分配一组内存对象的方法，其优点是从 arena 分配对象通常比......
### [Linux 内核从 C89 迁移到 C11 新进展](https://www.oschina.net/news/184534/linux-kernel-c89-to-c11)
> 概要: 内核开发者 Arnd Bergmann 发出了一个新的补丁，允许 Linux 内核在指定 C11 的 GNU 方言时默认使用“-std=gnu11”。这样一来，内核将允许使用不错的 C99/C11 功......
### [因体验不佳，Mozilla 将 Ideas 平台推倒重做](https://www.oschina.net/news/184537/mozilla-shut-down-its-ideas-platform)
> 概要: Mozilla Ideas 是 Mozilla 在 2021 年 6 月推出的平台，用于改善 Mozilla 与 Firefox 社区之间的沟通。Firefox 用户可以在该平台上发表如何改进 Fir......
### [JeecgBoot 低代码平台，成功入选 2021 科创中国·开源创新榜](https://www.oschina.net/news/184604)
> 概要: 近日，中国科学技术协会对2021年“科创中国”榜单遴选结果进行了公示。JeecgBoot低代码开发平台入选2021“科创中国”开源创新榜，此次上榜代表着“科创中国”对于JeecgBoot低代码平台在开......
### [Promnesia – A journey in fixing browser history (2020)](https://beepb00p.xyz/promnesia.html)
> 概要: Promnesia – A journey in fixing browser history (2020)
### [“造富液体”逆袭之战](https://www.huxiu.com/article/501135.html)
> 概要: 作者｜Eastland头图｜IC photo2022年1月17日，天赐材料(002709.SZ)发布业绩预告称，2021年净利润21亿至23亿，同比增长294%~332%。由于“不及预期”，天赐股价跌......
### [保险柜钥匙插门上，大学生单枪匹马盗取淘宝11亿用户手机号](https://segmentfault.com/a/1190000041478431?utm_source=sf-homepage)
> 概要: Hello，大家好，我是民间发明家，覃健祥。我申请了20多项发明专利，听到这个数字，有的朋友觉得好厉害但又不明白这些发明和我们的日常生活有什么关系，有的甚至问我是不是为了资助奖励申请了一堆凑数的专利......
### [来自开源世界的超级码丽，中国开源码力榜权威发布！](https://segmentfault.com/a/1190000041478447?utm_source=sf-homepage)
> 概要: 2022 年 1 月 13 日，SegmentFault 思否联合长期耕耘与推广开源文化的开源社共同推出了《2021 中国开源先锋 33 人》榜单，一众开源先锋上榜，一时间引发了行业的广泛关注与讨论......
### [虚渊玄x荒木哲郎剧场版《泡泡》中文预告 阵容豪华](https://acg.gamersky.com/news/202203/1463714.shtml)
> 概要: 由虚渊玄担任编剧，荒木哲郎导演的原创动画电影《泡泡》，在今天（3月1日）公开了正式中文预告，预告中公开了大量的新画面。
### [志村贵子&青木俊直公开「歌剧少女！！」漫画10周年纪念贺图](http://acg.178.com/202203/440099883365.html)
> 概要: 今年是漫画「歌剧少女」开启连载的第10周年，志村贵子与青木俊直两名漫画家公开了为其绘制的纪念贺图，绘制了渡边更纱等角色。「歌剧少女！！」最初于2012年在「少年JUMP改」上连载，后在2015年移刊至......
### [恋爱喜剧漫画《村井之恋》真人日剧化决定！](https://news.dmzj.com/article/73731.html)
> 概要: 恋爱喜剧漫画《村井之恋》真人日剧化决定！3月26日先行配信，4月5日正式开播。
### [有趣的女人，你成功的引起了我的注意](https://news.dmzj.com/article/73732.html)
> 概要: 这期给大家推荐一部狗粮漫《我的娇妻》，作者八科こむぎ，讲述男主被一位陌生女子叫住结婚的故事。
### [《如果30岁还是处男，似乎就能成为魔法师》电影版新预告片公开](https://news.dmzj.com/article/73733.html)
> 概要: 根据丰田悠的同名漫画改编的电影《如果30岁还是处男，似乎就能成为魔法师》公开了最新预告片，将于2022年4月8日上映。
### [乔治·马丁老爷子发博客安利《艾尔登法环》](https://www.3dmgame.com/news/202203/3836942.html)
> 概要: 近日，《冰与火之歌》作者、《艾尔登法环》世界架构设计者乔治·R·R·马丁在自己博客上，发布了一段简短的《艾尔登法环》安利文章。全文如下：等待已经结束，酝酿多年的《艾尔登法环》于上周正式发售，在游戏界掀......
### [「因为被认为并非真正的伙伴而被赶出了勇者的队伍，所以来到边境悠闲度日」第10卷封面公开](http://acg.178.com/202203/440104048184.html)
> 概要: 近日，角川书库公开了漫画「因为被认为并非真正的伙伴而被赶出了勇者的队伍，所以来到边境悠闲度日」的第10卷封面图，该卷将于4月1日发售。漫画「因为被认为并非真正的伙伴而被赶出了勇者的队伍，所以来到边境悠......
### [P站美图推荐——宝可梦朱紫特辑](https://news.dmzj.com/article/73734.html)
> 概要: 宝可梦第九世代！宝可梦朱紫！2022年冬发售！（骂完主角建模记得买（小声）
### [漫画「蓝色监狱」第18卷封面公开](http://acg.178.com/202203/440105301779.html)
> 概要: 漫画「蓝色监狱」公开了最新卷第18卷的封面图，该卷将于2022年3月17日正式发售。「蓝色监狱」（ブルーロック）是金城宗幸原作、ノ村优介负责作画的足球题材漫画作品。根据漫画改编的同名电视动画由8 Bi......
### [《柯南：万圣节的新娘》全新婚礼图 安室透帅气登场](https://acg.gamersky.com/news/202203/1463798.shtml)
> 概要: 剧场版动画《名侦探柯南：万圣节的新娘》公开了新的婚礼视觉图，这次登场的是警校五人组，安室透身穿浅色礼服身居中央。
### [「Re：从零开始的异世界生活」拉姆·生日会2021版手办开订](http://acg.178.com/202203/440114671920.html)
> 概要: 近日，「Re：从零开始的异世界生活」拉姆·生日会2021版手办正式开启预订，作品采用ABS、PVC材质，全高约240mm，日版售价20350日元（含税），约合人民币1119元，预计将于2022年12月......
### [《末日地带-幸存者版》登陆次世代主机 5月19日上线](https://www.3dmgame.com/news/202203/3836971.html)
> 概要: 近日，Gentlymad Studios工作室宣布《末日地带：与世隔绝-幸存者版》将于5月19日登陆PS5/Xbox Series。根据官方的表述目前本作在主机上应该只会推出数字版。登陆主机的“幸存者......
### [组图：许玮甯邱泽与昆凌等聚会照曝光 众人比耶笑对镜头状态好](http://slide.ent.sina.com.cn/star/slide_4_86512_366728.html)
> 概要: 组图：许玮甯邱泽与昆凌等聚会照曝光 众人比耶笑对镜头状态好
### [波瑠主演日剧公开海报预告片 宣布追加佐藤大树等](https://ent.sina.com.cn/jp/2022-03-01/doc-imcwiwss3559739.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 波瑠主演日剧《一个坠入爱河的人 ~司汤达的恋爱论~》公开海报预告片，宣布追加演员EXILE/FANTASTICS from EXILE TRIBE的佐藤大树、马场园梓、阿......
### [杨幂肖像权维权胜诉 侵权方需道歉并赔偿3万元](https://ent.sina.com.cn/s/m/2022-03-01/doc-imcwiwss3561434.shtml)
> 概要: 新浪娱乐讯 据企查查APP显示，近日，杨幂与武汉一公司网络侵权责任纠纷的案件一审判决书公开。因被告未经原告允许，于某平台网店上，大量、公开使用载有原告肖像的图片等进行商业广告宣传。杨幂提出诉讼请求，判......
### [好莱坞明星纷纷表态支持乌克兰 奇异博士：必须立即行动起来](https://new.qq.com/rain/a/20220301A0607T00)
> 概要: 持续多日的俄乌冲突引发了全世界的关注，很多好莱坞明星也纷纷就此事表态，他们的态度基本都是支持乌克兰，哪怕是拥有俄罗斯国籍的动作巨星史蒂文·西格尔，也表示“我将两者视为一个家庭”。好莱坞动作巨星史蒂文·......
### [传闻：海总饰演《疯狂麦克斯：弗瑞奥萨》头号反派](https://www.3dmgame.com/news/202203/3836982.html)
> 概要: 《纽约时报》记者Kyle Buchanan在所著的新书《Blood, Sweat & Chrome: The Wild and True Story of Mad Max: FuryRoad》中透露著......
### [米仓凉子从花瓶到收视女王 豪爽可爱吸粉无数](https://ent.sina.com.cn/jp/2022-03-01/doc-imcwiwss3571448.shtml)
> 概要: （文/盐）　　　　但是你可能不知道，她其实一直被疾病困扰，患上了“低髓液压综合症”。　　由于大脑和脊髓周围的髓液不断减少，患者会出现头痛、晕眩等症状，米仓凉子甚至说自己连路都走不直了，跑步或转圈之后会......
### [漫谈6G，为时过早？](https://www.huxiu.com/article/502659.html)
> 概要: 本文来自微信公众号：新眸（ID：xinmouls），作者：叶静，题图来自：视觉中国自5G逐渐规模应用后，关于6G的讨论就层出不穷。从2018年国内着手研究6G，到纽约大学、加州大学进行太赫兹6G方向预......
### [又又又被段永平加仓，是时候抄底腾讯了吗？](https://www.tuicool.com/articles/bMn2Y3Q)
> 概要: 2月28日，有网友在社交媒体发评论：腾讯控股又到了大道底，并@大道无形我有型（"小霸王"、"步步高"创始人、知名投资人段永平）。段永平回复：低过我上次买的价钱了，那明天再买点。睡前下个单，希望明天成交......
### [华为MatePad Pro 10.8英寸开售：2999元 预装鸿蒙](https://www.3dmgame.com/news/202203/3836999.html)
> 概要: 为了满足更多用户的使用需求，日前，华为推出了6GB+128GB Wi-Fi版本的华为MatePad Pro 10.8英寸。从华为官方商城获悉，全新华为MatePad Pro 10.8英寸已于今日正式开......
### [被“答辩”折磨的互联网打工人](https://www.tuicool.com/articles/yYremmN)
> 概要: 答辩，是许多互联网公司评判转正、绩效晋升的关键环节，有人为PPT熬秃了头，有人为讲演磨破了嘴。在这场答辩游戏里，处处是表演，处处是角力。一些玩法在答辩场内，更多规则在场外。一个发现让人瞬间紧张。整个公......
### [杨洋斑驳光影大片释出 赤足穿西装苏感满点](https://new.qq.com/rain/a/20220301A0983900)
> 概要: 近日，杨洋登《出色WSJ.》三月刊封面。封面中，杨洋赤足穿西装，慵懒随性，带来了一丝初春的温暖气息。            在采访中，杨洋表示，他对于角色的选择可以说出发点很单纯，“这个角色够不够吸引......
### [Huawei MatePad Paper – eInk Tablet](https://consumer.huawei.com/en/tablets/matepad-paper/)
> 概要: Huawei MatePad Paper – eInk Tablet
### [视频：《人世间》收官曝特辑 雷佳音辛柏青宋佳殷桃讲述角色背后](https://video.sina.com.cn/p/ent/2022-03-01/detail-imcwipih6046917.d.html)
> 概要: 视频：《人世间》收官曝特辑 雷佳音辛柏青宋佳殷桃讲述角色背后
### [哪吒汽车 2 月交付量 7117 台同比增长 255%，前两个月 18126 台同比增长 332%](https://www.ithome.com/0/605/429.htm)
> 概要: IT之家3 月 1 日消息，哪吒汽车今日公布了 2 月的交付量。2 月共交付 7117 台，同比增长 255%。1-2 月累计交付量 18126 台，同比增长 332%。上个月，哪吒汽车品牌所属公司合......
### [百度2021年营收1245亿元 同比增长16%](https://www.3dmgame.com/news/202203/3837002.html)
> 概要: 百度发布了截至2021年12月31日的第四季度及全年未经审计的财务报告。第四季度，百度实现营收331亿元，市场平均预期322亿元，归属百度的净利润（非美国通用会计准则）达到41亿元，市场平均预期29亿......
### [up推荐｜盘点2D国漫中那些贱嗖嗖的主角](https://new.qq.com/omn/20220301/20220301A0AJOQ00.html)
> 概要: 各位鹅友，周二到啦又到了每周的up推荐时间本周up推荐鹅少带大家盘点2D国漫中那些贱嗖嗖的男主和有趣的玩梗片段有没有你的宝藏up上榜呢？                        UP：凌光君兰【......
### [Fly.io (YC W20) Is Hiring Laravel Specialists](https://fly.io/blog/fly-io-is-hiring-laravel-specialists/)
> 概要: Fly.io (YC W20) Is Hiring Laravel Specialists
### [中企乌克兰投资往事](https://www.tuicool.com/articles/MZnuUnz)
> 概要: 本文来自微信公众号：风暴眼工作室（ID：qiyanglu4hao），作者：风暴眼工作室，原文标题：《风暴眼｜中企乌克兰投资往事：华为小米占优 航企曾因乌方毁约损超45亿美元》，头图来自：视觉中国202......
### [熊出没中光头强有着天壤之别的四个造型，网友：很难再见到了](https://new.qq.com/omn/20220301/20220301A0AKNZ00.html)
> 概要: 熊出没中光头强有着天壤之别的四个造型，网友：很难再见到了。            第一个有着天壤之别的造型：拖拖拉拉斯基形态这是光头强很特别的一个造型，乍看之下像一个绅士，但是细看又觉得像是一位魔术师......
### [深圳福田区因疫情封控向民众免费赠送 Keep＋腾讯视频会员，在家隔离也要好好锻炼身体](https://www.ithome.com/0/605/435.htm)
> 概要: IT之家3 月 1 日消息，由于多种原因，现在深圳、东莞的部分地区已经有居民已经进入居家隔离状态，甚至有深圳的小伙伴为此叫苦。据深圳商报，自上周起，由福田区公共文化体育发展中心“隔空投送”的暖心文体礼......
### [电池级碳酸锂均价突破 50 万元 / 吨，同比涨幅超 510%](https://www.ithome.com/0/605/436.htm)
> 概要: 3 月 1 日，百川盈孚数据显示，电池级碳酸锂交易均价较前一日再涨 5000 元 / 吨，达 50.04 万元 / 吨，部分地区均价甚至已达 51 万元 / 吨。与上年同期相比，均价涨幅超过 510%......
### [【天官赐福】如果不知为何而活，那就为我活下去吧](https://new.qq.com/omn/20220301/20220301A0B0S800.html)
> 概要: 为你，灯明三千，为你，花开满城，为你，所向披靡。为你所向披靡。为你花开满城，为你灯明三千。天官赐福,百无禁忌。我愿供灯千盏，照彻长夜，即便飞蛾扑火，也无所畏惧。但我不愿因为做了对的事情而低头。弱水三千......
### [只顾吃肉的造车新势力，活不到下一集](https://www.huxiu.com/article/502954.html)
> 概要: 出品 | 虎嗅汽车组作者 | 张博文头图 | IC Photo每月 1 日，是各家造车新势力晒交付数量的时间。蔚来汽车交付新车 6131 辆，同比增长9.9%。理想汽车交付新车 8414 辆，同比增长......
### [动漫女主角的呆毛被拔掉之后会发生什么不得了的事？](https://new.qq.com/omn/20220301/20220301A0BFS500.html)
> 概要: 最近几天一直都在更新动漫推荐类的文章，今天聊一期动漫杂谈，说说二次元中经常能看到的呆毛。呆毛，也就是头顶上竖起来的那撮头发。视个体差异，呆毛有长有短；在形状上也千奇百怪。这种不合常理的东西，在现实中除......
### [全国首部公共数据地方性法规落地浙江 聚焦公共数据全生命周期](https://finance.sina.com.cn/china/dfjj/2022-03-01/doc-imcwipih6068902.shtml)
> 概要: 原标题：全国首部公共数据地方性法规落地浙江 聚焦公共数据全生命周期 21世纪经济报道记者 郑植文 上海报道 《浙江省公共数据条例》于今日起施行...
### [百度去噪：长期主义的胜利](https://www.tuicool.com/articles/FRvaAfv)
> 概要: "商业化落地的“大爆发前夜”。"作者｜樟稻编辑｜伊页在这个信息丰富的时代，往往需要很大的毅力去理解公司，但对于眼光长远的投资者来说，更需要忽略短期的噪音。沃伦·巴菲特就是这样一位罕见的投资者，凭借耐心......
### [孙洁委员：建议扩大税延养老险的优惠力度，降低领取时应缴税额](https://finance.sina.com.cn/jjxw/2022-03-01/doc-imcwipih6069404.shtml)
> 概要: 养老的话题越发成为各界关注的重点。 3月1日，全国政协委员、对外经济贸易大学国家对外开放研究院研究员孙洁向澎湃新闻表示，在今年两会上...
### [Launch HN: Onboard (YC W22) – Automate and streamline customer onboarding](https://onboard.io/)
> 概要: Launch HN: Onboard (YC W22) – Automate and streamline customer onboarding
### [Steam 1 月最热游戏榜，包括《战神》《怪物猎人：崛起》和多款国产游戏](https://www.ithome.com/0/605/454.htm)
> 概要: IT之家3 月 1 日消息，Steam 今日公布了1 月份最热新品游戏榜，官方根据游戏收入和玩家数，选出 20 款最热门的新游戏、5 款最热门的免费新游戏。该榜单包括 1 月最热游戏、最热免费游戏、最......
### [王毅应约同乌克兰外长库列巴通电话](https://finance.sina.com.cn/jjxw/2022-03-01/doc-imcwipih6071458.shtml)
> 概要: 新华社北京3月1日电3月1日，国务委员兼外长王毅应约同乌克兰外长库列巴通电话。 库列巴介绍了乌俄首轮谈判情况，表示结束战事是乌方的最优先任务...
### [深圳高校“抢人”了！百万科研经费延揽这类人才](https://finance.sina.com.cn/china/dfjj/2022-03-01/doc-imcwiwss3630371.shtml)
> 概要: 原标题：深圳高校“抢人”了！百万科研经费延揽这类人才 为进一步吸引和鼓励在自然科学、工程技术等方面已取得较好成绩的海外优秀青年学者（含非华裔外籍人才）回国（来华）...
### [刚刚，楼市重磅！热点大城突然取消“认房又认贷”，鼓励投亲养老！啥信号？](https://finance.sina.com.cn/china/dfjj/2022-03-01/doc-imcwiwss3631572.shtml)
> 概要: 原标题：刚刚，楼市重磅！热点大城突然取消“认房又认贷”，鼓励投亲养老！啥信号？ 作 者丨李莎 张敏 孔海丽 3月1日，郑州发布《郑州市人民政府办公厅关于促进房地产业良性...
### [联合国官员呼吁各国携手治理海洋塑料污染](https://finance.sina.com.cn/jjxw/2022-03-01/doc-imcwiwss3631146.shtml)
> 概要: 新华社内罗毕3月1日电（记者黎华玲 白林）第五届联合国环境大会续会2月28日至3月2日在肯尼亚首都内罗毕举行。联合国秘书长海洋事务特使彼得·汤姆森表示...
# 小说
### [女神的至尊神婿](http://book.zongheng.com/book/900051.html)
> 作者：孤澈

> 标签：都市娱乐

> 简介：曾被人设下骗局，输尽百万家产；他发誓在那里跌倒，就在那里爬起来；三年沉默，一朝爆发，他誓要拿回曾经失去的一切！

> 章节末：第490章 结束也是开始 (完结）

> 状态：完本
# 论文
### [Robust Self-Supervised Audio-Visual Speech Recognition | Papers With Code](https://paperswithcode.com/paper/robust-self-supervised-audio-visual-speech)
> 日期：5 Jan 2022

> 标签：None

> 代码：None

> 描述：Audio-based automatic speech recognition (ASR) degrades significantly in noisy environments and is particularly vulnerable to interfering speech, as the model cannot determine which speaker to transcribe. Audio-visual speech recognition (AVSR) systems improve robustness by complementing the audio stream with the visual information that is invariant to noise and helps the model focus on the desired speaker.
### [Adversarial Attacks on ML Defense Models Competition | Papers With Code](https://paperswithcode.com/paper/adversarial-attacks-on-ml-defense-models)
> 日期：15 Oct 2021

> 标签：None

> 代码：None

> 描述：Due to the vulnerability of deep neural networks (DNNs) to adversarial examples, a large number of defense techniques have been proposed to alleviate this problem in recent years. However, the progress of building more robust models is usually hampered by the incomplete or incorrect robustness evaluation.
