---
title: 2020-07-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WinchesterCrypt_EN-CN4083575558_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-07-15 22:58:17
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WinchesterCrypt_EN-CN4083575558_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [王祖蓝cos葫芦娃被判侵权？工作室发声明回应](https://ent.sina.com.cn/s/h/2020-07-15/doc-iivhuipn3024704.shtml)
> 概要: 新浪娱乐讯 7月15日凌晨，王祖蓝工作室发声明回应......
### [时代少年团公开私生视频 私生多处蹲守成员受困扰](https://ent.sina.com.cn/y/yneidi/2020-07-15/doc-iivhvpwx5562151.shtml)
> 概要: 新浪娱乐讯 7月15日，时代少年团官博晒出一段未完成的旅行策划视频。视频中，成员们开心外出，但因途中遭遇私生跟车，旅行策划被迫取消，只能暂回公司，失望不已。此外，私生还在韩国舞蹈室门口、北京公司门口蹲......
### [布兰妮母亲提交财政监护申请 要求协助女儿理财](https://ent.sina.com.cn/y/youmei/2020-07-15/doc-iivhvpwx5514230.shtml)
> 概要: 新浪娱乐讯 当地时间7月14日，美国歌手布兰妮·斯皮尔斯的母亲琳恩向洛杉矶县法院提交了涉及她女儿数百万信托基金和财务决定的法律文件，要求参与到女儿的理财决策中。　　      去年5月，布兰妮母亲在一......
### [林丹谈与李宗伟关系：全满贯的含金量因他而高](https://ent.sina.com.cn/s/m/2020-07-15/doc-iivhuipn3099249.shtml)
> 概要: 新浪娱乐讯 近日，在节目中邓亚萍采访林丹时，问及了他和李宗伟的关系。林丹表示两个人17，8岁就是开始打比赛，那个时候还是小青年，火药味很浓，可能比赛结束了，两个人还会互相盯着对方。但是如今已经打了十几......
### [组图：郎朗用"小白船"手机壳紧跟潮流 见跟拍面带微笑亲和力十足](http://slide.ent.sina.com.cn/star/slide_4_704_341944.html)
> 概要: 组图：郎朗用"小白船"手机壳紧跟潮流 见跟拍面带微笑亲和力十足
### [周星驰很多经典镜头都是抄袭？香肠嘴、蛤蟆功、如来神掌都不是他原创](https://new.qq.com/omn/20200715/20200715V0IQ7400.html)
> 概要: 周星驰很多经典镜头都是抄袭？香肠嘴、蛤蟆功、如来神掌都不是他原创
### [最“高冷”的女星，不跟张国荣打招呼，认识刘德华三年才和他说话](https://new.qq.com/omn/20200715/20200715A0S8NB00.html)
> 概要: 最“高冷”的女星，不跟张国荣打招呼，认识刘德华三年才和他说话
### [肖战被曝拍新剧，已顺利杀青，未受风波影响，拍摄现场很低调谦虚](https://new.qq.com/omn/20200715/20200715A0PRLC00.html)
> 概要: 肖战因为风波一事闹得过于“凶”，而导致他丢掉了不少工作，近期也是出现了反转又反转的一系列事情，在7月14日晚，肖战工作室也因为风波，再次出现表态，直接道歉的态度，也引来不少好感。随着晨小晨事件被扒，各......
### [张翰新剧正式开机！传言被破，女主不是杨紫而是最近才上热搜的她](https://new.qq.com/omn/20200715/20200715A0PFNE00.html)
> 概要: 张翰新剧正式开机！传言被破，女主不是杨紫而是最近才上热搜的她
### [张大奕小三风波过后，董花花已转型成女强人，视察新公司装修情况](https://new.qq.com/omn/20200715/20200715A0KF6100.html)
> 概要: 张大奕小三风波过后，董花花已转型成女强人，视察新公司装修情况
# 动漫
### [舞台剧《我的英雄学院》两名相关人士被确诊感染新冠](https://news.dmzj.com/article/67956.html)
> 概要: 舞台剧《我的英雄学院》The “Ultra” Stage宣布了由于参演的相关人士被新冠病毒感染而延期公演的消息。
### [强势肌肉女攻与傲娇体弱男的反差爱恋](https://news.dmzj.com/article/67949.html)
> 概要: 这期给大家推荐一部弱受男暗恋健美女生的漫画《隔壁的女汉子》，作者亚乃アメ助，讲述肌肉女和傲娇男的浪漫喜剧爱情故事。
### [【伪OP】远坂大小姐想让我告白~主从间的恋爱头脑战~](https://news.dmzj.com/article/67951.html)
> 概要: 甜甜恋爱番OP首曝！再次强调：弓凛是真的！朋友们，把“般配”两个字打在公屏上，好吗？
### [plum《摇曳露营Δ》各务原抚子迷你手办](https://news.dmzj.com/article/67957.html)
> 概要: plum根据《摇曳露营Δ》中的各务原抚子制作的无比例迷你手办即将于16日开订。本作全高仅有7cm，采用了抚子举着富士山的吉祥物向前跑的造型。而且还可以和另行发售的志摩凛装饰在一起。
### [《恋与制作人》动画7月15日晚腾讯首播 心动初遇](https://acg.gamersky.com/news/202007/1304543.shtml)
> 概要: 未播先热的TV动画《恋与制作人》将于今晚（7月15日）11点正式登陆腾讯视频平台。讲述了“你”和他的初遇瞬间。
# 财经
### [国务院：重点支持高校毕业生、返乡农民工创业就业](https://finance.sina.com.cn/china/2020-07-15/doc-iivhuipn3220420.shtml)
> 概要: 国务院推动以新动能支撑保就业：高校毕业生、返乡农民工创业就业，将免费提供一定比例孵化基地 每经记者 张怀水每经编辑 陈旭 7月15日...
### [国务院：专项债可用于防灾减灾项目建设 严禁用于置换存量债务](https://finance.sina.com.cn/china/2020-07-15/doc-iivhuipn3218767.shtml)
> 概要: 21观债丨国务院：专项债可用于防灾减灾项目建设，严禁用于置换存量债务 来源：21世纪经济报道 7月15日召开的国常会指出，要用好地方政府专项债券，加强资金和项目对接...
### [大行加码小微贷款：利率低至4% 严防资金违规进入房市](https://finance.sina.com.cn/china/2020-07-15/doc-iivhuipn3220045.shtml)
> 概要: 大行加码小微贷款：利率低至4%，防骗贷买房有门道 2020年是实行“两增两控”、加强银行支持小微普惠的第三年。国有大行近两年来对小微普惠业务持续发力...
### [最高法发布全国法院审理债券纠纷案件座谈会纪要（全文）](https://finance.sina.com.cn/china/2020-07-15/doc-iivhvpwx5592328.shtml)
> 概要: 最高法发布全国法院审理债券纠纷案件座谈会纪要（全文） 来源：最高人民法院 为保护债券市场投资人的合法权利，强化对债券发行人的信用约束...
### [13部门发文支持新业态发展 激活消费市场带动扩大就业](https://finance.sina.com.cn/china/2020-07-15/doc-iivhuipn3220777.shtml)
> 概要: 允许购买优秀在线课程资源，符合条件“互联网+”医疗服务费可纳入医保……支持新业态带动就业出“大招” 每经记者 张钟尹每经编辑 陈旭 新经济迎来新机会！
### [交通运输部发布新客规：提升经营者经营自主权](https://finance.sina.com.cn/china/2020-07-15/doc-iivhvpwx5596756.shtml)
> 概要: 交通运输部发布新客规：提升经营者经营自主权 新华社北京7月15日电（记者魏玉坤）记者15日获悉，交通运输部近日修订发布了《道路旅客运输及客运站管理规定》...
# 科技
### [基于OpenCV/Numpy实现用手势控制赛车游戏](https://www.ctolib.com/ChoudharyChanchal-game_control.html)
> 概要: 基于OpenCV/Numpy实现用手势控制赛车游戏
### [Ant Design Blazor - 基于 Ant Design 和 Blazor 的企业级组件库](https://www.ctolib.com/ant-design-blazor-ant-design-blazor.html)
> 概要: Ant Design Blazor - 基于 Ant Design 和 Blazor 的企业级组件库
### [用yolov3实现的面向任意对象的检测（附带一些技巧）。](https://www.ctolib.com/ming71-rotate-yolov3.html)
> 概要: 用yolov3实现的面向任意对象的检测（附带一些技巧）。
### [QTool: 深度神经网络模型量化算法的集合](https://www.ctolib.com/blueardour-model-quantization.html)
> 概要: QTool: 深度神经网络模型量化算法的集合
### [抖音横屏切入PC端，藏着字节跳动怎样的野心？](https://www.tuicool.com/articles/mmuamyB)
> 概要: 图片来源@视觉中国文 | 松果财经曾经的抖音以一己之力将短视频行业推向顶峰，一时间风头无两，巨头们望尘莫及，但近日抖音开始内测三分钟视频，试图以横屏视频切入pc端和平板。曾在移动端带起竖屏小视频潮流的......
### [产品问答 | 入职一家公司，你的选择依据是什么？](https://www.tuicool.com/articles/vMfQ3uR)
> 概要: 产品问答是一个全新的栏目，每周会在社区问答中选取3个认可数高的问题。关于这些问题，欢迎留言分享你的见解，每期都会抽取优质留言送出邀请码~-1-入职一家公司，你的选择依据是什么？有人说年轻人都应该去大公......
### [阿里、腾讯、美团“聚餐”：餐饮SaaS市场沦为巨头饭局？](https://www.tuicool.com/articles/V3YnErj)
> 概要: 文：互联网江湖 作者：刘志刚疫情影响之下，投资萎靡，企业服务级市场反倒是迎来了春天。数据显示，今年一季度，新经济领域的634起投资，企业服务级市场占据了六分之一。今年四月份，素有独角兽捕手之称的朱啸虎......
### [Dynamically generated stats for your GitHub readme](https://www.tuicool.com/articles/a6BZJ3Z)
> 概要: GitHub Readme StatsGet dynamically generated GitHub stats on your readmes!·Report Bug·Request Featur......
### [华硕推出高达机动战士版 Wi-Fi 6 路由器](https://www.ithome.com/0/497/927.htm)
> 概要: IT之家 7 月 15 日消息 根据华硕的消息，华硕和高达联合推出了高达机动战士版 WiFi6 路由器 RT-AX82U 和 RT-AX86U。华硕表示，机动战士路由 RT-AX86U，贴合高达人物夏......
### [ThinkPad X13 锐龙版上架：8 核 R7 + 高色域屏，5999 元](https://www.ithome.com/0/497/946.htm)
> 概要: IT之家7月15日消息 在ThinkPad T14 锐龙版上架之后，ThinkPad X13 锐龙版也来了，R5 PRO 4650U+16GB 内存 +512GB SSD 售价5499元，R7 PRO......
### [vivo 新智能制造中心即日起正式启用：年产量超 7000 万台](https://www.ithome.com/0/497/962.htm)
> 概要: IT之家 7 月 15 日消息 vivo 通过官方微博宣布，vivo 新智能制造中心即日起正式启用，年产量超 7000 万台。据悉，vivo 制造中心位于广东省东莞市长安镇，占地面积 21.6 万㎡，......
### [麻烦不断：巴基斯坦有人申诉 “封杀”TikTok](https://www.ithome.com/0/497/968.htm)
> 概要: IT之家 7 月 15 日消息 据巴基斯坦《黎明报》报道，巴基斯坦已有人向拉合尔高等法院提出民事杂项申诉，要求立即禁用短视频应用 TikTok。报道称，律师 Nadeem Sarwar 代表一名公民提......
# 小说
### [崛起复苏时代](https://m.qidian.com/book/1013232100/catalog)
> 作者：极地风刃

> 标签：异术超能

> 简介：灵气复苏，物种秩序破裂，人类是否还能站在生物链的顶端，这是一次艰难的考验。（已有完本老书《都市至强者降临》，大家可以看一下。）本书书友群950996381，有兴趣的书友可以加一下。

> 章节总数：共1267章

> 状态：完本
# 游戏
### [Xbox Serie X快速架构介绍 开启新世代游戏体验](https://www.3dmgame.com/news/202007/3793004.html)
> 概要: 微软发布一则宣传视频，介绍了Xbox快速架构，在此架构下Xbox Serie X的游戏速度和性能将得到提升，一切变得更快更棒。Xbox Velocity架构宣传视频：Xbox Series X设计初衷......
### [新拍卖会超珍宝可梦卡拍出9万美元天价 世界仅存7张](https://www.3dmgame.com/news/202007/3793035.html)
> 概要: 据外媒报道，在近日举行的美国宏吉拍卖公司(Heritage Auctions)拍卖会上，一张名为《No.1训练师》的超珍贵宝可梦卡拍出9万美元天价，而这张卡片世界仅存7张。·美国宏吉拍卖公司(Heri......
### [《尘埃5》新预告片 展示美国、巴西和摩洛哥赛道](https://www.3dmgame.com/news/202007/3793077.html)
> 概要: Codemasters今日发布了《尘埃5》的新预告片“特性”，Codemasters还确认就像Xbox One版免费升级至XSX版那样，PS4版也可以免费升级至PS5版，无论数字版还是实体版游戏都支持......
### [PC《牧场物语矿石镇》特别好评 情怀之作、定价偏高](https://www.3dmgame.com/news/202007/3793023.html)
> 概要: 今日（7月15日），《牧场物语：重聚矿石镇》正式登陆Steam平台，国区售价280元，目前总体评价为“特别好评”，以下为具体细节。Steam商城页面截图：在《牧场物语：重聚矿石镇》中，名为“矿石镇”的......
### [感知惊艳，细节尽显！微星PAG272URV显示器上市](https://3c.3dmgame.com/show-42-12822-1.html)
> 概要: 近年来，微星科技在显示器领域不断深耕，推出了不少广受好评的产品；而就在7月14日，微星科技又推出了一款Optix PAG272URV显示器，主要面向高阶游戏和设计师人群，让我们一起来看看这款产品都有哪......
# 论文
### [Big GANs Are Watching You: Towards Unsupervised Object Segmentation with Off-the-Shelf Generative Models](https://paperswithcode.com/paper/big-gans-are-watching-you-towards)
> 日期：8 Jun 2020

> 标签：SALIENCY DETECTION

> 代码：https://github.com/anvoynov/BigGANsAreWatching

> 描述：Since collecting pixel-level groundtruth data is expensive, unsupervised visual understanding problems are currently an active research topic. In particular, several recent methods based on generative models have achieved promising results for object segmentation and saliency detection.
