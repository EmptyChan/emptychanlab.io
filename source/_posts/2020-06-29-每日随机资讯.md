---
title: 2020-06-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ArganGoats_EN-CN3637463283_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-06-29 21:16:25
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ArganGoats_EN-CN3637463283_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：《阿乐的早读刻》邢菲朗诵《秘密》](https://video.sina.com.cn/p/ent/2020-06-29/detail-iircuyvk0950181.d.html)
> 概要: 视频：《阿乐的早读刻》邢菲朗诵《秘密》
### [视频：老公与女总裁亲密合影引质疑 吕一晒照回应婚变传闻](https://video.sina.com.cn/p/ent/2020-06-29/detail-iirczymk9476202.d.html)
> 概要: 视频：老公与女总裁亲密合影引质疑 吕一晒照回应婚变传闻
### [李光洁跳女团舞关节直响 笑问killing part杀不杀](https://ent.sina.com.cn/s/m/2020-06-29/doc-iirczymk9488817.shtml)
> 概要: 新浪娱乐讯 6月28日，李光洁的妻子隋雨蒙在微博晒出一段老公跳《无价之姐》的视频，并发文：“披荆斩棘有点费劲，还是别报名了”。视频中李光洁十分吃力却热情地跳着舞，在扭动肩膀与腰部的部分中身体关节发出了......
### [视频：《乘风破浪的姐姐》蓝盈莹“野心写在脸上最有魅力”](https://video.sina.com.cn/p/ent/2020-06-29/detail-iirczymk9534929.d.html)
> 概要: 视频：《乘风破浪的姐姐》蓝盈莹“野心写在脸上最有魅力”
### [《孝利家民宿》不拍第三季 李孝利称没有隐私很累](https://ent.sina.com.cn/tv/zy/2020-06-29/doc-iircuyvk0968233.shtml)
> 概要: 新浪娱乐讯 韩国人气艺人李孝利表示将不会出演《孝利家民宿3》。　　28日，李孝利进行了直播，与粉丝们进行了实时互动沟通。直播中李孝利对提到《孝利家民宿3》的粉丝们说：“这个好像有点困难，家应该成为我和......
### [女演员刘琳的心酸往事](https://new.qq.com/omn/20200628/20200628A03OC100.html)
> 概要: | 知了作品，期待您的点赞，转发，评论~~~            近日，电视剧《隐秘的角落》热播，演员各个都是演技派，让观众看了直呼过瘾。由刘琳饰演的“周春红”更是将一个母亲的角色演绎得恰到好处，其......
### [谭松韵失去金主？李现强行蹭热度？高天鹤被仝卓拉下水？](https://new.qq.com/omn/20200629/20200629A0G83S00.html)
> 概要: 仝卓出事之后但凡芒果有点风吹草动网友们都能跟仝卓扯上关系，比如昨天高天鹤被P掉的事，瓜友们纷纷猜测是不是他之前因为仝卓出事网友们顺藤摸瓜扒出了作弊一事，导致他也因为这些黑点被芒果除名。这次高天鹤被P掉......
### [牛萌萌因毒入狱，周杰形象大反转，一个人的“逆商”是有多重要](https://new.qq.com/omn/20200629/20200629A0DJA700.html)
> 概要: 逆商是可以通过训练提升的，要教孩子学会正确面对困厄，以成长的心态、务实的态度，选择自己的人生。这两天最大的瓜，实锤。先是，之后。双方一来一往较上了劲儿。一边穷追猛打连发3条爆料，一边贴出了自己的工时清......
### [李子柒端午节视频遭举报下架？韩网怒怼爆料者为假冒“韩国网友”](https://new.qq.com/omn/20200629/20200629A0BESM00.html)
> 概要: 每个视频超过十万评论和转发，每发一条视频都有无数死忠粉为她点赞，视频刚上线就能看到满屏的弹幕，李子柒绝对是国内网络红人中的顶流。                        然而近日有人在八卦论坛爆......
### [黄璐回应点名于正秦岚内情，称不怕得罪人，无辜躺枪的刘芸发声了](https://new.qq.com/omn/20200629/20200629A0LASL00.html)
> 概要: 6月29日，刘芸发文称：“于老师和秦岚都是我的好朋友，真的不希望这件陈年旧事牵连到我的朋友们，吃瓜群众都散了吧，键盘侠们也请你们都消停点吧，情绪发泄有很多种渠道，不应该建立在伤害和影响别人的基础上。”......
# 动漫
### [P站美图推荐——车窗特辑](https://news.dmzj.com/article/67803.html)
> 概要: 窗中人，窗上影，窗外景，你关注的究竟是那边呢？​
尾图福利向预警x
### [游戏王历史：从零开始的游戏王环境之旅第四期28](https://news.dmzj.com/article/67805.html)
> 概要: 以“大宇宙”为代表的全体除外卡牌的诞生，给当时的Metagame环境带来了不少影响。对【黄泉帝】【拉怪Chaos】的打击最大，但这还不足以让它们衰退，所以实际的影响比看上去要小的多。在逐渐复杂化的游戏平衡面前，3月份出台了第四期最后的制限改订措施...
### [《GUNDAM A》上高达导演富野由悠季的奇妙访谈](https://news.dmzj.com/article/67804.html)
> 概要: 最新一期《GUNDAM A》上对富野由悠季进行了长达7页的采访，在此次采访中大光头依旧是惊人之语频出。
### [真人电影《东京卍复仇者》宣布延期上映](https://news.dmzj.com/article/67802.html)
> 概要: 根据和久井健原作拍摄的真人电影《东京卍复仇者》宣布了延期上映的消息，新的上映日期将在决定后另行公开。这次延期是受到新冠病毒所带来的疫情的影响。
### [一拳超人番外11：埼玉受邦古邀请吃龙虾，“认真一筷”解决踢馆者](https://new.qq.com/omn/20200629/20200629A0P2LE00.html)
> 概要: 关注海学家，专注热血漫！喜欢一拳超人的伙伴们，漫画月更是不是等得超难熬，再加上作者又几个月没更新最新剧情了，没得看一拳，那番外篇是不是被你忽略了呢……一拳超人的番外篇也是不可忽略的哦，一拳的番外篇和……
### [佘诗曼一身花花裙，优雅甜美又可爱！](https://new.qq.com/omn/20200629/20200629A049VQ00.html)
> 概要: 佘诗曼一身花花裙，优雅甜美又可爱！
### [放映机漫画：过敏症](https://new.qq.com/omn/20200624/20200624A02Y7L00.html)
> 概要: 放映机漫画：过敏症
### [动画片因人物“染发”被举报价值导向有问题，目前已停播并整改](https://new.qq.com/omn/20200629/20200629A0JUGY00.html)
> 概要: 近日，一部动画片《菲梦少女2》遭到网友举报，举报的理由稍微有点“无厘头”。按理说，像《菲梦少女2》这类讲述友谊、青春、梦想、奋斗的正能量动画，应该也是小朋友可以放心观看的动画片，怎么就被举报了呢？原……
### [《海贼王》凯多儿子像御田，御田儿子像凯多，真就抱错孩子了？](https://new.qq.com/omn/20200629/20200629A00MY300.html)
> 概要: 在《海贼王》近期的“和之国篇”中，四皇凯多和光月御田可以说是一对死敌，在当初光月御田跟着罗杰出海的时候，凯多帮助大蛇夺取了和之国的统治权。而在御田回国之后，他隐忍了数年最终还是和凯多决一死战，只可惜……
### [图集：风靡海外的女性Cosplay，看看你最钟意哪一款](https://new.qq.com/omn/20200629/20200629A03JWA00.html)
> 概要: 你能看出她们扮演的什么角色吗？我是“万象之昼夜观察”，欢迎关注留言，更多精彩内容为您呈现。
### [《星期一的丰满》：欢乐游戏时光 好身材是好帮手](https://acg.gamersky.com/news/202006/1300354.shtml)
> 概要: 本周《星期一的丰满》带大家来到充满欢乐的游戏厅，爱酱对各种游戏都有着很高的热情，在桌游的对抗中爱酱的好身材又一次发挥了重要的作用，充当了好帮手。
### [动画片因角色染发被举报 频道停播并对其整改](https://acg.gamersky.com/news/202006/1300486.shtml)
> 概要: 近日，有观众举报金鹰卡通的动画片《菲梦少女2》里有染发、穿得花里胡哨在舞台上表演换装等问题，批评其价值导向有问题。
# 财经
### [今日财经TOP10|美国拟定新制裁20家中企 涉中国航天科工等](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iirczymk9598543.shtml)
> 概要: 【宏观要闻】 NO.1 美国防部拟定新制裁20家中企名单 外交部：坚决反对 俄罗斯卫星通讯社29日消息，中国外交部发言人办公室在答复卫星通讯社关于美国防部拟定可施加制裁中企...
### [中基协：数据显示居民资金转向证券投资基金](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iircuyvk1068349.shtml)
> 概要: 中国证券投资基金业协会发布了2019年私募基金登记备案及投资运作概况，数据显示，居民资金转向证券投资基金。截至2019年末，居民（含私募基金管理人员工跟投）在私募证券投...
### [官方回应：“检察官称‘收受贿赂不办事保证道德底线＇”](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iircuyvk1070859.shtml)
> 概要: 原标题：官方回应：“检察官称‘收受贿赂不办事保证道德底线” 6月29日，针对“检察官当庭称：收受贿赂不办事，说明保证了道德底线”，盘锦市检察院发布《关于盘锦市大洼区人民...
### [国家卫健委专家组专家回应“在公共卫生间被传染的病例”问题](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iircuyvk1068083.shtml)
> 概要: 原标题：国家卫健委专家组专家回应“在公共卫生间被传染的病例”问题 6月29日，在北京市召开新冠肺炎疫情防控工作新闻发布上，国家卫健委专家组专家...
### [沪苏通铁路7月1日开通运营 助推长三角区域一体化发展](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iircuyvk1068084.shtml)
> 概要: 原标题：沪苏通铁路7月1日开通运营 助推长三角区域一体化发展 上海至苏州至南通铁路（以下简称沪苏通铁路）将于7月1日开通运营，助推长江三角洲区域一体化发展...
### [上周北京新建住宅成交回落 哪五个楼盘抢收超2亿？](https://finance.sina.com.cn/china/gncj/2020-06-29/doc-iirczymk9592790.shtml)
> 概要: 原标题：上周北京新建住宅成交回落 哪五个楼盘抢收超2亿？ 上周，北京新建住宅市场成交额保持高位，排名年内周度成交额第三。成交额排名前20楼盘中，非限竞房占据主力。
# 科技
### [将文本文件转换为手写的PDF文件](https://www.ctolib.com/sharanya02-Text-file-to-handwritten-pdf-file.html)
> 概要: 将文本文件转换为手写的PDF文件
### [Bootstrap 5 & Material Design 2.0 UI KIT](https://www.ctolib.com/mdbootstrap-mdb-ui-kit.html)
> 概要: Bootstrap 5 & Material Design 2.0 UI KIT
### [一个强大的CLI，让你的pod可以在线使用，而不需要暴露一个k8服务](https://www.ctolib.com/narendranathreddythota-podtnl.html)
> 概要: 一个强大的CLI，让你的pod可以在线使用，而不需要暴露一个k8服务
### [手势解锁 vue-lock](https://www.ctolib.com/wxStart-vue-lock.html)
> 概要: 手势解锁 vue-lock
### [Has GitHub been down more since its acquisition by Microsoft?](https://www.tuicool.com/articles/vAZNRbV)
> 概要: Two years ago, on June 4th of 2018, Microsoft announced its acquisition of GitHub, unicorn darling o......
### [乘风破浪的姐姐爱读什么书，我翻了下万茜的书单种草了这几本](https://www.tuicool.com/articles/eUVFVz7)
> 概要: 要说当下最红的综艺，那肯定是《乘风破浪的姐姐》，每逢节目更新，必定霸屏微博热搜，让人有种不看姐姐似乎就要脱离社交圈的恐惧感。万茜初舞台表演，吉他弹唱“敬你”30位姐姐呢，各有所爱，而我身边打call最......
### [OAM 深入解读：使用 OAM 定义与管理 Kubernetes 内置 Workload](https://www.tuicool.com/articles/7Vre22i)
> 概要: 作者 | 周正喜  阿里云技术专家  爱好云原生，深度参与 OAM 社区大家都知道，应用开放模型 Open Application Model（OAM） 将应用的工作负载（Workload）分为三种—......
### [深陷会计丑闻，“欧洲支付宝”Wirecard 引发多重问题](https://www.ithome.com/0/495/214.htm)
> 概要: 北京时间 6 月 29 日晚间消息，据国外媒体报道，德国支付巨头 Wirecard 戏剧性的失宠，让德国的企业治理和行业监管成为了人们关注的焦点。由于账上 19 亿欧元（21 亿美元）现金不翼而飞，W......
### [迅雷宣布 2000 万美元股票回购计划](https://www.ithome.com/0/495/217.htm)
> 概要: 6 月 29 日晚间消息，迅雷发布公告称，董事会已批准一项计划，根据该计划，公司可在未来 12 个月内回购不超过 2000 万美元的股票。根据股票回购计划，可根据市场情况并根据适用条件，不时在公开市场......
### [爆料：苹果 iPhone 12 Pro 将支持 120/240fps 4K 视频拍摄](https://www.ithome.com/0/495/220.htm)
> 概要: YouTube 频道 EverythingApplePro 和爆料者 Max Weinbach 纷纷表示，“iPhone 12”可能会新增两种新的拍摄模式。具体来说，新的模式据说支持 120fps 和......
### [GitHub 宕机两小时，众多软件开发人员受影响](https://www.ithome.com/0/495/215.htm)
> 概要: IT之家 6 月 29 日消息 今天晚上，GitHub 再次出现了宕机事故，刚刚已经恢复正常，宕机时间持续了 2 小时左右。这家微软旗下的服务通过 Git 提供版本控制，并为软件开发提供托管服务，由于......
# 小说
### [新世纪剑侠](http://book.zongheng.com/book/200478.html)
> 作者：FERRDOMK

> 标签：科幻游戏

> 简介：基因觉醒，原本弱小的他化身为杀戮机器。　　不屈意志，助他快意恩仇，驰骋天下。　　一条强者的进化之路，　　一段传承的英雄传说。　　杀戮中，他是睥睨一切的众神之神，　　纷乱间，他是主宰天下的一代剑侠！　　——————————————————— 新书《弑神之地》上传，望大家捧场~

> 章节末：最终章  一切的一切原来只是个开始

> 状态：完本
# 游戏
### [港服7月PS+会免游戏公开 《NBA 2K20》等三款游戏](https://www.3dmgame.com/news/202006/3791918.html)
> 概要: 港服7月PS+会免游戏公开，《NBA 2K20》、《古墓丽影：崛起》、《Erica》等三款游戏，总值943港币。此次限免截止到2020年8月3日......
### [小米大师系列电视官宣：OLED屏/120Hz、音画巅峰](https://www.3dmgame.com/news/202006/3791895.html)
> 概要: 今日上午（6月29日），小米宣布了旗下首款超高端电视新品，命名为大师系列，定于7月2日下午2点发布。小米称，感动人心的音画体验，只为最挑剔的眼睛耳朵。海报中还出现了OLED、120Hz、Gaming（......
### [好莱坞将拍摄武汉疫情电影 将在中国拍摄 片名待定](https://www.3dmgame.com/news/202006/3791893.html)
> 概要: 据好莱坞报道者、Deadline、The Wrap等多家外媒报道，曾为《大空头》《爆炸新闻》等影片担任编剧的查尔斯·兰道夫，即将为SK Global公司(《摘金奇缘》《赴汤蹈火》) 自编自导一部关于武......
### [《轩辕剑柒》公布高清海报 6月30日或有爆料](https://www.3dmgame.com/news/202006/3791872.html)
> 概要: 轩辕剑柒官方微博公布了一张高清海报，并暗示农历五月初十（6月30日）或有消息公布。以下为微博原文内容：官博君为大家送上昨天在发布会上公开海报的高清大图。刚才同事神秘兮兮地问我：农历五月初十是个好日子吗......
### [死亡搁浅PC、PS4不能跨平台联机 无MOD开发套件](https://www.3dmgame.com/news/202006/3791910.html)
> 概要: 《死亡搁浅》PC版将于7月14日发售，小岛工作室的首席技术官酒本海旗男和传播部负责人齐藤昭义在接受外媒GameSpark的采访中透露了部分关于PC版游戏的情报。官方虽然不打算为游戏的Mod提供开发套件......
# 论文
### [Machine Reading Comprehension: The Role of Contextualized Language Models and Beyond](https://paperswithcode.com/paper/machine-reading-comprehension-the-role-of)
> 日期：13 May 2020

> 标签：MACHINE READING COMPREHENSION

> 代码：https://github.com/cooelf/AwesomeMRC

> 描述：Machine reading comprehension (MRC) aims to teach machines to read and comprehend human languages, which is a long-standing goal of natural language processing (NLP). With the burst of deep neural networks and the evolution of contextualized language models (CLMs), the research of MRC has experienced two significant breakthroughs.
