---
title: 2022-12-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TangleCreekFalls_ZH-CN4281148652_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-07 22:33:35
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TangleCreekFalls_ZH-CN4281148652_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [只用一个白天，全中国的网友都被骗了](https://www.woshipm.com/ai/5698564.html)
> 概要: AI 的发展速度正在加快，并逐渐打开了我们的想象空间，比如最近 OpenAI 发布的 ChatGPT——一款自然语言生成式 AI，就吸引了大量网友试用与讨论。那么风靡全网的 ChatGPT 究竟是什么......
### [从全局角度看产品立项（附立项模版）](https://www.woshipm.com/pd/5687886.html)
> 概要: 立项说明书一份表述项目完整规划的文件，用于给公司高层/投资方判断这个项目是否值得投入。作者在本文分享了立项说明书的模板供我们参考，让我们一起来看看吧。立项说明书一份表述项目完整规划的文件，用于给公司高......
### [洞察人性，解决本质问题](https://www.woshipm.com/user-research/5697843.html)
> 概要: 每个人的潜意识里都有着自己固有的习惯，利用情绪与人性价值，产品可以更加容易俘获用户的芳心。本文结合相关案例，与大家谈谈关于人性的几点，洞察人性是人类的根本诉求，诉求不同需要满足的痛点也不相同，产品也需......
### [苹果放弃生产“L5级”全自动驾驶汽车](https://finance.sina.com.cn/tech/it/2022-12-07/doc-imqmmthc7278156.shtml)
> 概要: 财联社12月7日电，据知情人士透露，苹果公已经降低了其未来推出“L5级”全自动驾驶计划的野心，并将该汽车的目推出日期推迟了约一年，至2026年。在过去的几个月里，这个在公司内部被称为Titan的汽车项......
### [千万别花钱，陪李一男做造车梦](https://www.huxiu.com/article/734836.html)
> 概要: 出品 | 虎嗅汽车组作者 | 张博文2015年6月，李一男所创办的牛电科技发布了旗下首款产品——小牛智能电动踏板车N1，售价区间3999元 - 4999元，比当时市面上常规的电单车贵出一倍。然而，这场......
### [微软收购动视暴雪迎来最后程序：会见FTC主席](https://finance.sina.com.cn/tech/internet/2022-12-07/doc-imqqsmrp8819167.shtml)
> 概要: 新浪科技讯 北京时间12月7日早间消息，据报道，知情人士透露，微软高管将于当地时间周三会见美国联邦贸易委员会（FTC）主席莉娜·可汗（Lina Khan），就微软收购动视暴雪做最后陈述......
### [拟推迟至2026年发布](https://finance.sina.com.cn/tech/it/2022-12-07/doc-imqmmthc7281497.shtml)
> 概要: 新浪科技讯 北京时间12月7日早间消息，据报道，苹果公司计划于2026年面向消费市场发布苹果品牌汽车，其目标是将这款产品的售价控制在10万美元以内，并吸引更加广泛的客户......
### [线上买药爆单了](https://www.huxiu.com/article/734899.html)
> 概要: 此系列为一名外卖女骑手，在送餐路上真实的人间观察。近期系列相关文章链接详见文末，感兴趣的读者可点击链接查阅。本文来自微信公众号：陌生人肖像计划（ID：inlens），作者：几何小姐姐，头图来自：视觉中......
### [36氪首发 | 「博联众科」获数千万元融资，加速围术期监测与口腔数字化系统建设](https://36kr.com/p/2030872639171847)
> 概要: 36氪首发 | 「博联众科」获数千万元融资，加速围术期监测与口腔数字化系统建设-36氪
### [杨国福集团COO陈烁：餐饮连锁空间广阔，企业如何稳步前进？  | WISE2022「适者生存」新消费峰会](https://36kr.com/p/2031999681391874)
> 概要: 杨国福集团COO陈烁：餐饮连锁空间广阔，企业如何稳步前进？  | WISE2022「适者生存」新消费峰会-36氪
### [总集篇《灰原哀物语》预告公开 1月6日日本上映](https://acg.gamersky.com/news/202212/1545113.shtml)
> 概要: 名侦探柯南剧场版《黑铁的鱼影》预热TV总集篇《灰原哀物语~黑铁的神秘列车》预告公开。
### [36氪出海·CLUB纳新｜12/16在深圳，邀您参与“出海欧洲”主题线下闭门会](https://36kr.com/p/2032503737265411)
> 概要: 36氪出海·CLUB纳新｜12/16在深圳，邀您参与“出海欧洲”主题线下闭门会-36氪
### [咪奥 宠物食品品牌和包装全案设计](https://www.zcool.com.cn/work/ZNjMyMzEzOTY=.html)
> 概要: 咪奥 宠物食品品牌和包装全案设计创新猫粮独立品牌，主打全价无谷猫粮系列产品辅以肉制品冻干混合添加技术，减轻爱宠消化负担，促进健康成长。科学的混合配比方案让爱宠可以全周期食用，让猫咪毛发更柔顺亮着。sl......
### [求购OpenAI股份（ChatGPT的母公司）；转让持有Space X、Shein公司股份的基金份额｜资情留言板第72期](https://36kr.com/p/2033420631223553)
> 概要: 求购OpenAI股份（ChatGPT的母公司）；转让持有Space X、Shein公司股份的基金份额｜资情留言板第72期-36氪
### [《义贼侦探诺斯里》首支PV公开 12月22日发售](https://www.3dmgame.com/news/202212/3857850.html)
> 概要: 游戏厂商Aquaplus今日公布了《传颂之物》的衍生游戏新作《义贼侦探 诺斯里》的最新PV，本作预计2022年12月22日发售，登陆PS4和Switch平台，税后4180日元。游戏PV游戏故事的背景发......
### [事关你我！医保目录改革这件事，一定要关注](http://www.investorscn.com/2022/12/07/104705/)
> 概要: 医疗保障是民生保障的重要内容，群众期盼、社会关注，与每个人的生活息息相关，最新一轮医保目录调整已于今年7月1日正式启动申报程序，并将在今年年底前公布结果，明年落地执行。自2018年至今，医保药品目录调......
### [创业20年，珀莱雅逆势增长到底靠什么？](http://www.investorscn.com/2022/12/07/104706/)
> 概要: 创业20年，珀莱雅从“送给妈妈”的水乳套装变身国货之光......
### [业务回归、投资补短，新华保险内在价值不容低估](http://www.investorscn.com/2022/12/07/104708/)
> 概要: 12月5日，A股保险股集体上涨。截至下午收盘，板块内成分股普遍上涨，其中新华保险领涨，涨幅为7.53%，港股也取得了7.5%的涨幅。自11月下旬获中国宝武钢铁集团有限公司（以下简称“中国宝武”）增持后......
### [模拟经营游戏《餐饮大亨》现已发售 Steam多半好评](https://www.3dmgame.com/news/202212/3857872.html)
> 概要: 模拟经营游戏《餐饮大亨》现已在Steam平台发售，现在购买享9折优惠，只需64.8元，支持中文。以第一人称视角开设您梦想中的餐厅，创造与众不同的外观和内饰，聘请最好的服务员和厨师，创造一个人们喜欢在这......
### [《海贼王》1069话情报：五档路飞VS觉醒路奇](https://acg.gamersky.com/news/202212/1545084.shtml)
> 概要: 《海贼王》1069话情报公开，面对路奇的攻击，路飞再次使出了五档的新形态。贝加庞克说“橡胶果实”从未被记载在古老的“恶魔果实大全”上。
### [NCT127将于明年1月回归 发布新包装版专辑](https://ent.sina.com.cn/y/yrihan/2022-12-07/doc-imqmmthc7331102.shtml)
> 概要: 新浪娱乐讯 韩国男团NCT 127将于明年1月带着新专辑回归。　　NCT 127的经纪公司SM娱乐今天宣布，NCT 127正在以1月发售为目标准备新的Repackage专辑，希望广大粉丝多多关注并期待......
### [建信基金：港股中期价值凸显，后续看好两类投资机会](http://www.investorscn.com/2022/12/07/104710/)
> 概要: 11月，港股在连跌19个月后迎来大幅反弹，前半个月累计涨幅一度接近25%。建信基金认为，今年以来港股的弱势表现是海内外多重因素交织的结果，此轮反弹或与这些因素的缓释有关。当前港股估值处于历史较低区间，......
### [消息称微软将开发一个“超级应用” 灵感或也来自微信](https://www.3dmgame.com/news/202212/3857884.html)
> 概要: 不可否认的是，微信的确是个非常成功的综合通信应用，12月7日新消息，据外媒报道称，微软在考虑构建一个“Super App（超级应用）”，将购物、通讯、网络搜索、新闻和其他服务综合在一个一站式智能手机应......
### [风波中的货拉拉：越算越难的三笔账](https://www.huxiu.com/article/734456.html)
> 概要: 本文来自微信公众号：晚点LatePost （ID：postlate），作者：沈方伟，采访：沈方伟 陈晶，编辑：管艺雯，头图来自：视觉中国货拉拉在 2021 年的市场占有率（交易额维度）就超过了 50%......
### [日本手游周年庆引发吐槽 你见过类似的奇葩活动吗？](https://news.dmzj.com/article/76459.html)
> 概要: 今天一早，日推网民们突然见证了一幅颇为魔幻的景象。大量用户将他们的头像换成了一个男人痛哭的图案，但究竟发生了什么大事，令人完全摸不着头脑。
### [《邻家的天使大人把我变成了废人这档事》椎名真昼手办化](https://news.dmzj.com/article/76460.html)
> 概要: TV动画《邻家的天使大人把我变成了废人这档事》中的角色椎名真昼，将要推出两款周边产品。其中一款是的FURYU景品，另外一款是FURYU旗下的低价格手办品牌TENITOL的商品。
### [在2022年的双十一，看到穿越周期的品牌范式？](http://www.investorscn.com/2022/12/07/104719/)
> 概要: K型时代,是吴晓波上一轮年终秀里预言的2022年商业趋势。在他看来,一个行业的利润,很可能只集中在少数能应对变化、实现创新的企业中。这意味着企业面对的是只有向上、向下两种方向的K型时代......
### [视频：老牌公司天意影视将破产清算 曾发行《士兵突击》等剧](https://video.sina.com.cn/p/ent/2022-12-07/detail-imqmmthc7349105.d.html)
> 概要: 视频：老牌公司天意影视将破产清算 曾发行《士兵突击》等剧
### [视频：比伯与妻子海莉同框出街 亲密并肩同行一路热聊](https://video.sina.com.cn/p/ent/2022-12-07/detail-imqqsmrp8888101.d.html)
> 概要: 视频：比伯与妻子海莉同框出街 亲密并肩同行一路热聊
### [基于Electron框架全面重做 全新Linux版QQ开启公测](https://www.3dmgame.com/news/202212/3857889.html)
> 概要: 不久前，腾讯QQ项目组曾发布预告，宣布QQ for Linux新版本即将开启公测。现在，新的Linux版QQ已经开启公测，不过目前仅支持x86架构，arm64架构还仍在适配中。与此前极为简陋的Linu......
### [涉案金额超1亿日元！游戏制作人中裕司被再逮捕](https://news.dmzj.com/article/76462.html)
> 概要: 东京地方检察院特搜部在7日宣布了对原SE员工中裕司（57岁）和佐崎泰介（39岁）再逮捕的消息。
### [动画《Fate/strange Fake -Whispers of Dawn-》延期播出](https://news.dmzj.com/article/76463.html)
> 概要: 原定于在2022年12月31日播出的《Fate Project 年末特别节目》中公开的动画《Fate/strange Fake -Whispers of Dawn-》，宣布了延期播出的消息。
### [马斯克将推特办公室改成卧室 方便员工通宵工作](https://www.3dmgame.com/news/202212/3857894.html)
> 概要: 今年10月，马斯克成功拿下推特，长达半年之久的收购拉锯战终于结束。自收购以来，马斯克采取了一系列的改革措施，也让其受到了广泛的关注。此前，马斯克向推特员工发出最后通牒，告诉他们要么接受一种“极其硬核”......
### [三大指数涨跌不一，医药股爆发“以岭药业”创历史新高](https://www.yicai.com/video/101616821.html)
> 概要: 三大指数涨跌不一，医药股爆发“以岭药业”创历史新高
### [12月8日起恢复运营，上海迪士尼乐园搜索量瞬时涨幅超900%](https://www.yicai.com/news/101617068.html)
> 概要: 上海迪士尼乐园恢复运营的消息发布后，各大旅游平台的迪士尼关键词搜索量瞬间翻倍增长。
### [广州宣布即日起落实“新十条”，防疫药品生产储备情况如何？](https://www.yicai.com/news/101617128.html)
> 概要: 目前，广州全市7家重点药品生产企业的储存量已经达到了1000万盒，同时，已经储备了30天的生产原料。
### [用于芯片生产的 pellicle 防护膜出现短缺，导致产品涨价](https://www.ithome.com/0/659/429.htm)
> 概要: IT之家12 月 7 日消息，据 The Elec 报道，用于晶圆制造中 Arf 和 Krf 光刻工艺的 pellicle 防护膜供不应求。这是因为中国每天都有更多的无晶圆厂公司崛起，而由于芯片制造商......
### [联发科天玑 8200 定档明日发布，由 iQOO Neo7 SE 首发搭载](https://www.ithome.com/0/659/431.htm)
> 概要: IT之家12 月 7 日消息，联发科官宣将于明日（12 月 8 日）发布天玑 8200 移动平台，与 iQOO 11 和 iQOO Neo7 SE 手机同一天，甚至可能会一同发布。iQOO Neo7 ......
### [全文发布！即日起，北京实行十条优化疫情防控工作措施](https://www.yicai.com/news/101617244.html)
> 概要: 北京疫情防控工作领导小组办公室、首都严格进京管理联防联控协调机制办公室决定，自即日起，北京市实行以下十条优化疫情防控工作措施。
### [东北药王浮沉史：有人破产，有人入狱](https://www.huxiu.com/article/735474.html)
> 概要: 本文来自微信公众号：时代财经APP （ID：tf-app），作者：文若楠，编辑：朱白，题图来自：视觉中国（图为2010年，正意气风发的朱吉满）20世纪80年代末，国内迎来第一波下海经商潮，千千万万不甘......
### [漫步者推出 M25 桌面音箱：支持蓝牙 5.3/ USB 一线连，首发 129 元](https://www.ithome.com/0/659/438.htm)
> 概要: IT之家12 月 7 日消息，漫步者现已推出新款 M25 桌面音箱，首发 129 元，12 月 10 日开卖。IT之家了解到，漫步者 M25 采用了双声道设计，搭载了 52mm 全频单元。连接方面，这......
### [医药板块普涨，熊去氧胆酸、胸腺法新等概念受关注](https://www.yicai.com/news/101617249.html)
> 概要: 警惕炒作
### [财经TOP10|东方铁塔被质疑利益输送，尚德摄像头考勤引争议，威马汽车三年亏174亿](https://finance.sina.com.cn/china/caijingtop10/2022-12-07/doc-imqmmthc7376300.shtml)
> 概要: 【政经要闻】 NO.1 中央政治局会议定调明年经济工作：大力提振市场信心 更好统筹疫情防控和经济社会发展，更好统筹发展和安全，全面深化改革开放，大力提振市场信心。
### [北京疫情发布会：进返京人员不再查核酸 继续提供社会面免费核酸](https://finance.sina.com.cn/china/gncj/2022-12-07/doc-imqqsmrp8912872.shtml)
> 概要: 钟东波介绍，对进返京人员不再执行查验核酸检测阴性证明和健康码等防控措施，抵京后不再执行落地三天三检，按照本市有关防控规定执行。
### [医学专家谈新冠丨陈健：抗原检测能否代替核酸检测？](https://finance.sina.com.cn/china/2022-12-07/doc-imqqsmrp8912867.shtml)
> 概要: 记者提问：抗原检测能否代替核酸检测？比如在医院能使用抗原检测来确诊吗？ 广州市疾病预防控制中心免疫规划部主任医师陈健：医院不以抗原检测结果来确诊病人是否感染了新...
### [习近平主席开启沙特之行 外交部发言人介绍有关情况](https://finance.sina.com.cn/china/2022-12-07/doc-imqqsmrp8916665.shtml)
> 概要: 新华社北京12月7日电（记者冯歆然）应沙特阿拉伯王国国王萨勒曼邀请，国家主席习近平于12月7日至10日赴沙特利雅得出席首届中国-阿拉伯国家峰会...
### [小米预售 EA Pro 86 英寸电视，到手价 5999 元](https://www.ithome.com/0/659/441.htm)
> 概要: 感谢IT之家网友很宅很怕生的线索投递！IT之家12 月 7 日消息，小米新款 EA Pro 86 英寸电视正在预售中，12 月 10 日到手 5999 元。IT之家了解到，小米 EA Pro 86 英......
### [上海优化外来人员防控措施，迪士尼乐园搜索量瞬时暴涨近10倍](https://finance.sina.com.cn/jjxw/2022-12-07/doc-imqmmthc7376499.shtml)
> 概要: 12月7日晚间，上海发布进一步优化调整疫情防控的相关措施，其中包括优化调整来沪返沪人员防控措施。措施提到，8日零时起，来沪返沪人员抵沪后不再实施...
### [卧谈会大学里发生过最有意思的一件事是？](https://bbs.hupu.com/56837556.html)
> 概要: XDM，你们大学里发生过最有意思的一件事是什么？
### [流言板世界杯8强各俱乐部剩余人数：曼市双雄并列第一，巴黎第二](https://bbs.hupu.com/56837670.html)
> 概要: 虎扑12月07日讯 卡塔尔世界杯1/4的决赛将在明晚进行。而英国媒体也统计了每个俱乐部贡献球员的榜单，其中，曼市双雄11人位居榜首。1﻿.曼城、曼联：11人3﻿.巴黎：8人4﻿.拜仁、阿贾克斯：7人6
### [流言板王睿泽全场8投5中，得到14分8篮板3助攻](https://bbs.hupu.com/56837745.html)
> 概要: 虎扑12月07日讯 2022-23赛季CBA常规赛第10轮，青岛男篮以98-97击败山东男篮，青岛球员王睿泽全场得到14分8篮板。全场，王睿泽出战34分09秒，8投5中，三分球5投4中，得到14分8篮
### [流言板赵嘉义全场比赛投篮4中2，得到9分2篮板1助攻](https://bbs.hupu.com/56837769.html)
> 概要: 虎扑12月07日讯 2022-23赛季CBA常规赛第10轮，青岛男篮98-97险胜山东男篮。此役青岛球员赵嘉义出场21分钟，投篮4中2，罚球5中5，得到9分2篮板1助攻。   来源： 虎扑
# 小说
### [高武27世纪](https://m.qidian.com/book/1014218975/catalog)
> 作者：草鱼L

> 标签：高武世界

> 简介：27世纪。人道昌盛，武道辉煌！养老院树荫下，胖老头用手里的核桃，弹崩了一场浩劫，他笑的像个弥勒佛。南街26号，有个小保安，他用奶茶弹出了冰魄奶针，地上多了一具奶茶味尸体，他一直想换个工作，可学历不高。年轻女孩的眼睛很亮，像八心八箭的水钻，她喜欢用头撞卡车，嘴里常说一句：下一辆。一些当兵的，在他们的墓碑上，刻着一句话：这几年，乱世如麻，苍生太苦，愿未来的你们，拥有锦绣年华！还有一些当兵的，也刻着一句话：如果未来天堂相见，请你告诉我：国泰民安，山河犹在！……深夜。苏越扛着十几袋水泥，蛙跳到山顶，他与孤独为伴。在神州流传着一句古话：天道酬勤。苏越坚信这句话。自强不息的神州人，贯彻酬勤之道，薪火相传，傲骨铮铮。……已经完本500万字小说，天天万更，人品保证！企鹅群：777376496

> 章节总数：共723章

> 状态：完本
# 论文
### [Deep Movement Primitives: toward Breast Cancer Examination Robot | Papers With Code](https://paperswithcode.com/paper/deep-movement-primitives-toward-breast-cancer)
> 概要: Breast cancer is the most common type of cancer worldwide. A robotic system performing autonomous breast palpation can make a significant impact on the related health sector worldwide. However, robot programming for breast palpating with different geometries is very complex and unsolved. Robot learning from demonstrations (LfD) reduces the programming time and cost. However, the available LfD are lacking the modelling of the manipulation path/trajectory as an explicit function of the visual sensory information. This paper presents a novel approach to manipulation path/trajectory planning called deep Movement Primitives that successfully generates the movements of a manipulator to reach a breast phantom and perform the palpation. We show the effectiveness of our approach by a series of real-robot experiments of reaching and palpating a breast phantom. The experimental results indicate our approach outperforms the state-of-the-art method.
### [Dual Quaternion Ambisonics Array for Six-Degree-of-Freedom Acoustic Representation | Papers With Code](https://paperswithcode.com/paper/dual-quaternion-ambisonics-array-for-six)
> 概要: Spatial audio methods are gaining a growing interest due to the spread of immersive audio experiences and applications, such as virtual and augmented reality. For these purposes, 3D audio signals are often acquired through arrays of Ambisonics microphones, each comprising four capsules that decompose the sound field in spherical harmonics. In this paper, we propose a dual quaternion representation of the spatial sound field acquired through an array of two First Order Ambisonics (FOA) microphones. The audio signals are encapsulated in a dual quaternion that leverages quaternion algebra properties to exploit correlations among them. This augmented representation with 6 degrees of freedom (6DOF) involves a more accurate coverage of the sound field, resulting in a more precise sound localization and a more immersive audio experience. We evaluate our approach on a sound event localization and detection (SELD) benchmark. We show that our dual quaternion SELD model with temporal convolution blocks (DualQSELD-TCN) achieves better results with respect to real and quaternion-valued baselines thanks to our augmented representation of the sound field. Full code is available at: https://github.com/ispamm/DualQSELD-TCN.
