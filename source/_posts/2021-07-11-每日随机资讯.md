---
title: 2021-07-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SpiralAloe_ZH-CN5594814833_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-07-11 21:39:34
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SpiralAloe_ZH-CN5594814833_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Ubuntu 21.10 每日构建版已搭载 GNOME 40](https://www.oschina.net/news/149879/gnome-40-in-ubuntu-21-10-first-look)
> 概要: 开源软件供应链点亮计划，等你来！>>>GNOME 40 终于来到了 Ubuntu 21.10 每日构建版。虽然GNOME 40已正式发布了一段时间，但 Ubuntu 并没有在上一个版本更新 (Ubun......
### [USB 低延迟音频支持重新合并到 Linux 5.14](https://www.oschina.net/news/149875/usb-low-latency-audio-support-merge-into-linux-5-14)
> 概要: 开源软件供应链点亮计划，等你来！>>>降低 USB 音频驱动延迟的补丁在 Linux 5.14 中重新提交并已合并。上周，SUSE 的 Linux 声音子系统维护者 Takashi Iwai 提交了新......
### [粉丝自制《古墓丽影：黑暗天使》劳拉新3D模型](https://www.3dmgame.com/news/202107/3818687.html)
> 概要: 今年5月，DSOGaming曾介绍过粉丝利用虚幻4引擎自制《古墓丽影：黑暗天使》的消息。现在关于该项目又有新截图和视频可以分享，展示了女主角劳拉克劳馥的新3D模型。由爱好者FreakRaider和Ko......
### [杨丞琳零点为李荣浩庆生：陪你一起纵横四海](https://ent.sina.com.cn/y/ygangtai/2021-07-11/doc-ikqciyzk4718624.shtml)
> 概要: 新浪娱乐 7月11日零点，杨丞琳掐点发博为李荣浩庆生：“陪你一起纵横四海，生日快乐My boy。”这已是杨丞琳连续7年发博为老公庆生，十分甜蜜。(责编：小万)......
### [角川新机甲策略JRPG《Relayer》Anlnitak角色介绍](https://www.3dmgame.com/news/202107/3818688.html)
> 概要: 角川游戏日前为即将推出的机甲策略JRPG《Relayer》公布了新视频/图像以及细节信息。这次介绍的是游戏中的角色Alnitak以及为其配音的声优大河元气。Alnitak是太空海盗“星际协会”的秘书、......
### [盗笔动画如约而至，原著恐怖画面情景重现](https://news.dmzj.com/article/71497.html)
> 概要: 念念不忘必有回响，早年间众多稻米都希望开发动画制作，在经历了无数次正篇和番外的真人影视化后，《秦岭神树》动画正式上线。当初筹备阶段消息一出，许多粉丝就翘首期盼，虽然秦岭神树是没有原著铁三角的一本书，但也是众多粉丝心目中不能忘记的童年阴影，整体的恐...
### [Using IceWM and a Raspberry Pi as my main PC, sharing my theme, config and some](https://raymii.org/s/blog/Using_IceWM_and_sharing_my_config_and_tips_tricks.html)
> 概要: Using IceWM and a Raspberry Pi as my main PC, sharing my theme, config and some
### [TV动画《进化的果实》最新PV公开](https://news.dmzj.com/article/71498.html)
> 概要: TV动画《进化的果实》确定2021年10月开播，最新PV公开
### [原创动画《Visual Prison》第1弹PV公开](https://news.dmzj.com/article/71499.html)
> 概要: 上松范康×A1 Pictures制作原创动画《Visual Prison》确定2021年10月开播，第1弹PV公开
### [都2021年了，国内怎么还有野鸡大学？](https://www.huxiu.com/article/440353.html)
> 概要: 本文来自微信公众号：勿以类拒（ID：nfccmzk），作者：李波，编辑：秋雨，原文标题：《辛辛苦苦十二年，一朝考到野鸡大学》，题图来自视觉中国“野鸡大学”，许多大学生都用这个词吐槽母校。事实上，社会的......
### [How to manually merge two Apple IDs to one (2020)](https://www.brianstucki.com/blog/how-to-manually-merge-two-apple-ids-to-one/)
> 概要: How to manually merge two Apple IDs to one (2020)
### [组图：经超夫妇机场互靠肩膀撒娇 小李琳戴蝴蝶结秀美腿超减龄](http://slide.ent.sina.com.cn/star/slide_4_704_359165.html)
> 概要: 组图：经超夫妇机场互靠肩膀撒娇 小李琳戴蝴蝶结秀美腿超减龄
### [原创电视动画「Visual Prison」第1弹PV公开](http://acg.178.com/202107/419966549062.html)
> 概要: 由上松范康原作、A-1 Pictures负责动画制作的原创电视动画「Visual Prison」公开了第1弹PV，该动画将于2021年10月开始播出。「Visual Prison」第1弹PVSTAFF......
### [“海王”再就业，在抖音小红书教女生谈恋爱](https://www.tuicool.com/articles/QjmaUvR)
> 概要: 视频加载中，请稍候...欢迎关注“新浪科技”的微信订阅号：techsina见习作者 | 路俊迪编辑 | 吴娇颖来源：开菠萝财经“那一刻，我觉得空气都凝固了，只有他一个人站在地铁车厢的中间。”米粒从没想......
### [网曝刘浩存妈妈培训班因下腰不当致学员下肢瘫痪](https://ent.sina.com.cn/s/m/2021-07-11/doc-ikqciyzk4769882.shtml)
> 概要: 新浪娱乐讯 近日，有网友爆料刘浩存妈妈所办的舞蹈培训班疑似曾因不正当的教导，导致一位女孩下肢瘫痪终身残疾，引发热议。疑似曾涉及的民事案件显示，被告训练“下腰”时，对可能造成10岁以下儿童脊椎损伤的危险......
### [00后杀进职场，第一件事是“挑老板”](https://www.huxiu.com/article/440367.html)
> 概要: 本文来自：深燃（shenrancaijing），作者：唐亚华、邹帅、李秋涵、王敏、宛其、周继凤，头图来自：《实习生》剧照你可能不知道，最早的一批“00后”，今年暑假就要正式踏入职场了。“00后”出生在......
### [剧场版动画「高达G之复国运动：来自宇宙的遗产」公开正式PV](http://acg.178.com/202107/419969898805.html)
> 概要: 剧场版动画「高达G之复国运动：来自宇宙的遗产」于近日公开了正式PV，本作将于2021年7月22日公开上映。「高达G之复国运动：来自宇宙的遗产」正式PVCASTベルリ・ゼナム：石井马克アイーダ・スルガン......
### [钟镇涛发文为张学友庆生：60是另一开始 爱你兄弟](https://ent.sina.com.cn/s/h/2021-07-11/doc-ikqcfnca6177477.shtml)
> 概要: 新浪娱乐讯 7月10日是张学友60岁生日。7月11日，钟镇涛通过微博发文为张学友庆生：“从学友仔，到学友哥，到歌神张学友，为我们带来了无数靓歌，60是另一开始，踏进人生另一阶段，  希望你继续为大家演......
### [「蔚蓝反射/澪」后半篇OP主题曲「アトック」无字幕动画MV公开](http://acg.178.com/202107/419973921647.html)
> 概要: 根据Gust工作室开发的同名RPG游戏改编，由J.C.STAFF制作的电视动画「蔚蓝反射/澪」于近期公开了后半篇OP主题曲「アトック」的无字幕动画MV，动画已于4月10日开始播出。「アトック」无字幕动......
### [如何在现代硬件上虚拟化Intel版本的Mac OS X Tiger](https://www.ithome.com/0/562/128.htm)
> 概要: 前言2006 年，苹果开始给 Macintosh 计算机更换 Intel 处理器，与此同时，系统软件也需要更新为 x86 版本。于是从 10.4.4 起，Mac OS X Tiger 便正式开始支持 ......
### [《大富翁 10》将于暑期登陆 Switch 平台](https://www.ithome.com/0/562/134.htm)
> 概要: IT之家7 月 11 日消息 软星科技在 3 月份宣布，2019 年 10 月在 Steam 平台首发的《大富翁 10》全球销量已突破 70 万套，并计划登陆任天堂 Switch 平台。在今日的 Bi......
### [跨石滩、过草地、负重12kg，UC伯克利等研发新型机器人运动算法，实时快速适应变化环境](https://www.tuicool.com/articles/rQfAFjz)
> 概要: 说到腿式机器人，机器之心以前介绍过不少，如能跑、能跳、能跳绳、跨越障碍的 Spot 机器狗，不用摄像头和激光雷达凭感觉「越野」的 ANYmal 机器人、会翻跟斗的 MIT 机器人，等等。这些腿式机器人......
### [特斯拉Model Y狂降7万威力巨大！官网直接瘫痪](https://www.3dmgame.com/news/202107/3818713.html)
> 概要: 从6月汽车销量来看，中国新能源汽车市场仍然呈井喷式增长。6月新能源狭义乘用车销量23万辆，同比增长177.7%，上半年累计销量达100.7万，同比增长220.9%。除了新能源汽车销量暴涨外，近日，特斯......
### [Pantera 合伙人：从衍生品平台 SynFutures 读懂 DeFi 数字衍生品的未来](https://www.tuicool.com/articles/MbuInib)
> 概要: SynFutures 是「期货合约领域里的 Uniswap」，用户只需点击几下即可轻松上架自己的期货合约，而且能以免许可的方式购买所需的衍生品合约。原文标题： 《Synthetic Derivativ......
### [三星正在研发平面超透镜，可使手机摄像头更薄](https://www.ithome.com/0/562/146.htm)
> 概要: IT之家7 月 11 日消息 据韩国媒体 The Elec 消息，三星电机公司的一名高管在本周三表示，该公司正在研发一种超透镜（Metalens）。这种镜片看起来是平面结构，但是其表面遍布纳米颗粒，因......
### [“美嘉”滤镜碎一地，李金铭直播自称倒贴600万](https://new.qq.com/omn/20210711/20210711A050F800.html)
> 概要: 《爱情公寓》里的陈美嘉是一个单纯善良又可爱的女孩子，美嘉的饰演者李金铭这些年虽然不再演戏，但每当提起这个角色，大众心中对她都是充满喜爱的。这一年来李金铭很少更新动态，原来是随大流直播带货去了。就在最近......
### [Show HN: A tiny Web browser implementation with V8 JavaScript engine and Rust](https://github.com/lmt-swallow/puppy-browser/)
> 概要: Show HN: A tiny Web browser implementation with V8 JavaScript engine and Rust
### [马斯克宣布将提高猛禽火箭发动机的生产率，年产 800 台以上](https://www.ithome.com/0/562/149.htm)
> 概要: IT之家7 月 11 日消息 据外媒消息，SpaceX 公司 CEO 埃隆・马斯克 7 月 10 日在推特发文表示，该公司将提高猛禽（Raptor）火箭发动机的产量。这种发动机专为 Starship ......
### [《奥特曼英雄传》新形象：刘备战袍·泰迦奥特曼](https://acg.gamersky.com/news/202107/1405091.shtml)
> 概要: 《奥特曼英雄传》新形象公开，刘备战袍·泰迦奥特曼亮相。
### [漫画「吃货女仆」第3卷封面公开](http://acg.178.com/202107/419993066011.html)
> 概要: 漫画「吃货女仆」公开了第3卷封面插图，该卷将于2021年8月11日正式发售。「吃货女仆」是由日本漫画家前屋进创作的漫画作品，讲述了本是英国豪宅女仆的雀独自一人来到日本生活，并迷恋上日本美食的故事......
### [MySQL锁系统总结](https://www.tuicool.com/articles/3QZFbue)
> 概要: InnoDB锁简介InnoDB支持多种粒度的锁，按照粒度来分，可分为表锁（LOCK_TABLE）和行锁（LOCK_REC）。一般的锁系统都会有共享锁和排他锁的分类，共享锁也叫读锁，排他锁也叫写锁。加在......
### [996、大小周盛行 专家称超一半猝死者为中青年人](https://www.3dmgame.com/news/202107/3818732.html)
> 概要: 猝死主要分为心源性、肺源性、脑源性等几大类，其中心源性，也就是与心脏疾病相关的猝死占比高达80%专家表示，当前有超过一半的猝死者为中青年人，主要是跟年轻人的生活压力、工作方式、亚健康状态有关。当患有一......
### [Kerberos: Open-Source Video Surveillance](https://kerberos.io/product/open-source/)
> 概要: Kerberos: Open-Source Video Surveillance
### [Beefy Finance已经集成到DeBank平台](https://www.btc126.com//view/176392.html)
> 概要: 官方消息，Beefy Finance已经集成到DeBank平台......
### [解码“她经济” 绽放“她力量” 2021她经济高峰论坛在海口成功举办](https://finance.sina.com.cn/china/gncj/2021-07-11/doc-ikqciyzk4855411.shtml)
> 概要: 随着消费升级时代的来临，“她经济”近年来飞速增长，女性消费已然成为助推经济发展的新动能。根据埃森哲的数据，中国拥有近4亿年龄在20岁至60岁的女性消费者...
### [特大暴雨预警致北京711航班取消329航班延误](https://finance.sina.com.cn/china/2021-07-11/doc-ikqciyzk4856368.shtml)
> 概要: 原标题：特大暴雨预警致北京711航班取消329航班延误 中央气象台发布雷电黄色预警信号，预计7月11日夜间至12日白天，北京将迎来今年入汛以来最大降雨。
### [演员森川葵确诊感染新冠肺炎 目前居家疗养](https://ent.sina.com.cn/s/j/2021-07-11/doc-ikqcfnca6253129.shtml)
> 概要: 新浪娱乐讯 据日媒11日报道，演员森川葵确诊感染新冠。　　森川葵所属公司发布公告，称森川9日出现发热症状，当天实施核酸检测后，10日确认为阳性，现在正居家疗养。(责编：Mia)......
### [暴雨预警升级 中国气象局启动Ⅲ级应急响应](https://finance.sina.com.cn/china/2021-07-11/doc-ikqcfnca6254807.shtml)
> 概要: 原标题：暴雨预警升级 中国气象局启动Ⅲ级应急响应 中央气象台今天（11日）18时升级发布暴雨橙色预警和强对流天气黄色预警。经综合研判...
### [暴雨来袭 要注意哪些安全事项？](https://finance.sina.com.cn/china/2021-07-11/doc-ikqcfnca6255421.shtml)
> 概要: 原标题：暴雨来袭 要注意哪些安全事项？
### [加密电商平台Shopping.io与唯链达成合作，支持使用VET购物](https://www.btc126.com//view/176396.html)
> 概要: 官方消息，支持加密货币支付的电商平台Shopping.io近日与公链平台唯链（VeChain）达成合作。VeChain持有者可以在Shopping.io电子商务平台使用其VET代币，并在亚马逊、Eba......
### [突发：扬子江药业董事长徐镜人心梗在新疆伊犁离世 享年77岁](https://finance.sina.com.cn/china/2021-07-11/doc-ikqcfnca6255261.shtml)
> 概要: 突发！扬子江药业董事长徐镜人心梗在新疆伊犁离世，享年77岁 据健康时报刚刚消息，“扬子江药业董事长徐镜人在新疆伊犁离世。”7月11日，多名知情人士向记者介绍。
### [雷雨来临 首都机场截至20时共取消航班83架次](https://finance.sina.com.cn/china/2021-07-11/doc-ikqcfnca6255789.shtml)
> 概要: 原标题：雷雨来临 首都机场截至20时共取消航班83架次 预计北京首都国际机场今天（11日）20时40分至22时40分将出现雷雨天气，主要影响终端区西南部。
### [TVB《宝宝大过天》真实反映怀孕女性的艰辛，令妈妈观众感同身受](https://new.qq.com/omn/20210711/20210711A087TK00.html)
> 概要: 本文编辑剧透社：林纯纯儿未经授权严禁转载，发现抄袭者将进行全网投诉正在热播的《宝宝大过天》在下周会进入结局篇，该剧自播出以来，有不少情节贴近现实，让妈妈级的观众产生共鸣。            尤其是......
### [集锦亚冠：卡伊克任意球破门，电讯体工1-0卡雅FC](https://bbs.hupu.com/44163259.html)
> 概要: 集锦亚冠：卡伊克任意球破门，电讯体工1-0卡雅FC
### [窦骁超护女友，大赞何超莲：笑着说最狠的话，可以哦](https://new.qq.com/omn/20210711/20210711A088LB00.html)
> 概要: 由何超莲参与的首档潮流类节目《这就是潮流》已经公开了一组前期的花絮预告，从预告中就能够看出来看似很甜美的何超莲，点评起来可以说是相当不留情面。何超莲本人更是自称：我何超莲点评向来都是有话直说，不是我毒......
### [中世纪第一人称RPG《骑士与歹徒》上架Steam](https://www.3dmgame.com/news/202107/3818736.html)
> 概要: 中世纪第一人称剑术动作游戏《骑士与歹徒（Knights & Outlaws）》上架Steam，支持简体中文。预告片：《骑士与歹徒》是一个故事驱动的RPG冒险游戏，讲述的是一个农民决定改变自身命运的故事......
### [《中国医生》里易烊千玺特邀出演，欧豪特别出演，两者什么区别？](https://new.qq.com/omn/20210711/20210711A088QY00.html)
> 概要: 43位明星出演的电影《中国医生》，它的票房没让人失望，从首映日起就稳居票房榜第一位置，3天累计票房成功突破3亿，堪称暑期档第一部票房大卖的新片，算是给这个档期开了个好头。            影片中......
### [情报站LCK官推发文：祝贺Keria达成LCK生涯1500助攻成就](https://bbs.hupu.com/44163393.html)
> 概要: 虎扑07月11日讯 LCK官方推特发布推文。祝贺T1辅助选手Keria达成LCK生涯1500助攻成就。 来源：  Twitter    标签：Keria
### [半场两球落后零射门，国安将以一场惨败告别亚冠？](https://bbs.hupu.com/44163424.html)
> 概要: 亚冠小组赛I组末轮，北京国安半场0-2落后于川崎前锋，在射门次数上也是0比22的悬殊对比，国安最后一战会否又是一场惨败？
### [Reef Finance与跨链流动性网络Elk Finance达成合作](https://www.btc126.com//view/176399.html)
> 概要: 官方消息，跨链DeFi操作系统Reef Finance近日宣布与跨链流动性网络Elk Finance达成合作。Elk Finance将在Reef Chain上构建......
### [hi中国区启动大会将于7月13日在山东青岛召开](https://www.btc126.com//view/176400.html)
> 概要: 据官方消息，7月13日，hi.com中国区启动大会将在山东青岛召开。本次大会由hi.com主办，易昊资本、大豪超算协办。届时，国内区块链专家、投资机构代表、易昊资本战略合作伙伴、hi.com官宣合作社......
### [流言板回家！7年前的今天，勒布朗-詹姆斯宣布自己将重返骑士](https://bbs.hupu.com/44163541.html)
> 概要: 虎扑07月11日讯 骑士资讯媒体Cavs Nation官方推特发推回顾了一项骑士队史的重要时刻。“7年前的今天，勒布朗-詹姆斯宣布他将重返骑士。”该媒体制图表示。“勒布朗-詹姆斯回归，只为完成他要将冠
# 小说
### [十国千娇](http://book.zongheng.com/book/382294.html)
> 作者：西风紧

> 标签：历史军事

> 简介：五代十国后期，赵匡胤还只是中级校尉，这时一名禁军小队长就已经知道他陈桥兵变、杯酒释兵权的故事了。大家都还有机会，况且小队长对赵家将来的干法也不是很赞同……

> 章节末：第九百一十二章 阔海扬帆（结尾）

> 状态：完本
# 论文
### [AdaPT-GMM: Powerful and robust covariate-assisted multiple testing | Papers With Code](https://paperswithcode.com/paper/adapt-gmm-powerful-and-robust-covariate)
> 日期：30 Jun 2021

> 标签：None

> 代码：https://github.com/patrickrchao/AdaPTGMM_Experiments

> 描述：We propose a new empirical Bayes method for covariate-assisted multiple testing with false discovery rate (FDR) control, where we model the local false discovery rate for each hypothesis as a function of both its covariates and p-value. Our method refines the adaptive p-value thresholding (AdaPT) procedure by generalizing its masking scheme to reduce the bias and variance of its false discovery proportion estimator, improving the power when the rejection set is small or some null p-values concentrate near 1.
### [Heatmap Regression via Randomized Rounding](https://paperswithcode.com/paper/heatmap-regression-via-randomized-rounding)
> 日期：1 Sep 2020

> 标签：FACE ALIGNMENT

> 代码：https://github.com/baoshengyu/H3R

> 描述：Heatmap regression has become the mainstream methodology for deep learning-based semantic landmark localization, including in facial landmark localization and human pose estimation. Though heatmap regression is robust to large variations in pose, illumination, and occlusion in unconstrained settings, it usually suffers from a sub-pixel localization problem.
