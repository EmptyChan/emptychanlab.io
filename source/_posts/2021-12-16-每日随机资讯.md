---
title: 2021-12-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WernigerodeWeihnachtsmarkt_ZH-CN1081480865_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-12-16 22:05:07
---


![Bing 每日壁纸](https://www4.bing.com/th?id=OHR.WernigerodeWeihnachtsmarkt_ZH-CN1081480865_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [恒生电子加入欧拉开源社区，基于欧拉开源操作系统推出 Light-CORE 解决方案](https://www.oschina.net/news/174074)
> 概要: 近日，恒生电子股份有限公司（以下简称 “恒生电子”）签署了 CLA(Contributor License Agreement，贡献者许可协议)，正式加入欧拉开源社区。目前，恒生电子与欧拉开源社区开展......
### [电子前沿基金会评 Chrome Manifest V3：损害隐私、安全和创新](https://www.oschina.net/news/173949/googles-manifest-v3-still-hurts)
> 概要: EFF（电子前沿基金会）近日发文表示将与 Google 对抗到底，并试图让 Google 重新考虑 Chrome Manifest V3。EFF 认为 Google 正在推行的这个 API 会阻碍 C......
### [开源轻量级 IM 框架 MobileIMSDK v6.1.2 发布！](https://www.oschina.net/news/174045/mobileimsdk-6-1-2-released)
> 概要: 一、更新内容简介本次更新为次要版本更新，进行了若干优化（更新历史详见：码云 Release Nodes）。可能是市面上唯一同时支持UDP+TCP+WebSocket三种协议的同类开源IM框架。二、Mo......
### [论坛直播 | CNCC2021——开源操作系统生态共建与人才培养](https://www.oschina.net/news/173967)
> 概要: 操作系统是信息技术产业的底座，决定数字基础设施发展水平。当前，开源引领新一代信息技术创新发展，以开源开放模式加速操作系统生态共建逐步成为产业共识。人才作为技术的载体、创新的根本，在科技创新和产业发展中......
### [100行代码实现React核心调度功能](https://segmentfault.com/a/1190000041126142?utm_source=sf-homepage)
> 概要: 大家好，我卡颂。想必大家都知道React有一套基于Fiber架构的调度系统。这套调度系统的基本功能包括：更新有不同优先级一次更新可能涉及多个组件的render，这些render可能分配到多个宏任务中执......
### [The longest train journey – in 2021](https://jonworth.eu/the-longest-train-journey-in-the-world-in-2021/)
> 概要: The longest train journey – in 2021
### [Swift Playgrounds 4](https://www.apple.com/swift/playgrounds/)
> 概要: Swift Playgrounds 4
### [面试被问TopK问题，可以这样优雅的解答](https://segmentfault.com/a/1190000041127350?utm_source=sf-homepage)
> 概要: 前言hello，大家好，我是bigsai哥哥，好久不见，甚是想念哇🤩！今天给大家分享一个TOPK问题，不过我这里不考虑特别大分布式的解决方案，普通的一道算法题。首先搞清楚，什么是topK问题？topK......
### [F# Good and Bad](https://danielbmarkham.com/f-good-and-bad/)
> 概要: F# Good and Bad
### [Crypto wallet security as seen by security engineers](https://www.cossacklabs.com/blog/crypto-wallets-security/)
> 概要: Crypto wallet security as seen by security engineers
### [NicoNico x Pixiv 2021网络流行语100结果公开！](https://news.dmzj.com/article/73076.html)
> 概要: “‘网络流行语100’年度大奖2021表彰式”于12月15日在NicoNico上举办了。今年摘得年度大奖桂冠的是手游《赛马娘 Pretty Derby》。
### [Amiami公开2021年度手办大奖名单](https://news.dmzj.com/article/73077.html)
> 概要: 日本大型手办销售平台Amiami，公开了表彰“卖得最好的手办”的“Amiami手办大奖2021”排行榜。本次榜单是以预计在2021年年内发售的手办为对象，根据销售数量与金额等数据进行排序的排行榜。根据结果GSC的粘土人五条悟排名第一。
### [游戏《黎之轨迹2 -CRIMSON SiN-》2022年秋季发售](https://news.dmzj.com/article/73078.html)
> 概要: 日本Falcom宣布了《轨迹》系列最新作《黎之轨迹2 -CRIMSON SiN-》（PS5 / PS4）将于2022年秋发售的消息。本作的宣传图也一并公开。
### [NS版《那由多之轨迹》2022年发售](https://news.dmzj.com/article/73079.html)
> 概要: 日本Falcom宣布了NS版的《那由多之轨迹》将于2022年春发售的消息。本作也将成为由日本Falcom自己发行的首款NS游戏。
### [「JOJO」衍生漫画「クレイジーDの悪霊的失恋」最新杂志封面图公开](http://acg.178.com/202112/433619931171.html)
> 概要: 近日，「JOJO的奇妙冒险」系列衍生漫画作品「クレイジーDの悪霊的失恋」（不灭的疯狂钻石的恶灵的失恋）公开了最新杂志封面图，绘制的角色为“疯狂钻石”替身使者东方仗助和“皇帝”替身使者荷尔荷斯。「クレイ......
### [杂志「OPERA」第82期封面公开](http://acg.178.com/202112/433622682544.html)
> 概要: 杂志「OPERA」公开了第82期的封面图，本期封面绘制的是漫画「同级生」的新作番外篇「ふたりぐらし」（二人同居）。「同级生」是由中村明日美子创作的一部校园爱情漫画作品，描绘了少年们之间的温暖友情和朦胧......
### [实践F.I.R.E.的信徒们](https://www.huxiu.com/article/482133.html)
> 概要: 本文来自微信公众号：出色WSJ中文版（ID：WSJmagazinechina），作者：徐雯，摄影：武老白、Rivers，编辑：Lyra，原文标题：《内卷与躺平之间，他们选择适可而止》，头图来源：出色W......
### [「剃须。然后捡到女高中生」公开荻原沙优夜祭ver.新商品插画](http://acg.178.com/202112/433624471876.html)
> 概要: 动画「剃须。然后捡到女高中生」公开了C99新商品插画和商品样图，本次绘制的是身着浴衣的女主角荻原沙优（夜祭ver.）。电视动画「剃须。然后捡到女高中生」（ひげを剃る。そして女子高生を拾う。）改编自醋青......
### [「宿命回响：命运节拍」公开粉丝突破10万庆祝贺图](http://acg.178.com/202112/433625779521.html)
> 概要: 昨日（12月15日）晚间，「宿命回响：命运节拍」官方宣布粉丝数量突破10万人，并公开了庆祝贺图。「宿命回响：命运节拍」是由MAPPA、MADHOUSE负责制作的原创电视动画作品，为DeNA推出的混合媒......
### [《破晓传说》庆祝传说系列26周年 感谢玩家支持](https://www.3dmgame.com/news/202112/3830992.html)
> 概要: 今日，《破晓传说》官方推特发文庆祝传说系列26周年纪念，推文中感谢玩家在这26年路途中的一路陪伴。官方表示自己做梦也不会想到会拥有这么好的游戏社区，并希望玩家在今后能继续支持传说系列。传说系列是由南梦......
### [总制片人谈风起洛阳选角:接触王一博是在陈情令前](https://ent.sina.com.cn/v/m/2021-12-16/doc-ikyamrmy9310371.shtml)
> 概要: 新京报资深记者 张赫　　改编自马伯庸小说《洛阳》的古装悬疑剧正在热播中，剧方选择了黄轩、王一博、宋茜担纲主演，看似充分且精准地考量了观众对于演技、市场、流量的诸多需求，但仍未避免外界对于角色适配度，以......
### [梵高风过关游戏《星夜骑士》今日发售 支持中文](https://www.3dmgame.com/news/202112/3831011.html)
> 概要: 由Peace Blvd Games工作室创作的梵高油画主题横版过关游戏《星夜骑士（Starry Knight）》于今日（12月16日）正式在Steam上发售，游戏支持简体中文配音及字幕，价格为人民币1......
### [生田绘梨花举办毕业演唱会 因秋元真夏的信流泪](https://ent.sina.com.cn/2021-12-16/doc-ikyamrmy9340358.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 12月15日，偶像组合乃木坂46在神奈川横滨ARENA举行“乃木坂46生田绘梨花毕业演唱会”，队长秋元真夏给将于12月31日从组合毕业的生田绘梨花写信表达感谢，让她现场忍......
### [组图：《雪滴花》金智秀手挽丁海寅 新剧即将开播](http://slide.ent.sina.com.cn/tv/k/slide_4_704_364627.html)
> 概要: 组图：《雪滴花》金智秀手挽丁海寅 新剧即将开播
### [国内厂商推出《仙剑》赵灵儿、白茉晴人偶 售价599元](https://www.3dmgame.com/news/202112/3831014.html)
> 概要: 近日，国内著名潮流玩具厂商泡泡玛特推出了新品《仙剑奇侠传》赵灵儿、白茉晴人偶，现已上架泡泡玛特天猫旗舰店，每个售价599元，将于12月16日晚10点正式开卖。仙剑奇侠传 白茉晴 x Viya Doll......
### [《海贼王》1035话情报：索隆VS烬 阎王三刀龙](https://acg.gamersky.com/news/202112/1445800.shtml)
> 概要: 《海贼王》1035话情报公开，在骷髅头左脑塔的“游廊之战”中，山治战胜了奎因。另一边，索隆和烬的战斗还在继续。
### [牛姐采访谈与碧昂斯竞争关系 坦率表示钦佩对方](https://ent.sina.com.cn/y/youmei/2021-12-16/doc-ikyamrmy9349922.shtml)
> 概要: 新浪娱乐讯 据外媒报道，乐坛传奇花蝴蝶玛丽亚·凯莉（ Mariah Carey ） 对与另一位天后级人物碧昂斯（Beyoncé）之间的潜在竞争不感兴趣。　　12月15日星期三，52岁的凯莉在E！采访中......
### [世界上第一条短信将首次被拍卖：内容为“圣诞快乐”，价格高达上百万元](https://www.ithome.com/0/592/841.htm)
> 概要: IT之家12 月 16 日消息，据英国《镜报》16 日消息，世界上第一条短信于 1992 年 12 月发出，而近 30 年后，这条信息将再次创造纪录，它将以 17 万英镑（约合人民币 143 万元）被......
### [运营近20年 经典网游《QQ堂》宣布明年停运](https://www.3dmgame.com/news/202112/3831025.html)
> 概要: 于 2004 年由腾讯公司推出的炸弹人网游《QQ堂》于今天（12 月 16 日）在官网上宣布了即将停止运营的通知。官方宣布游戏将于 2022 年 4 月 20 日停止运营，当天早上 11 点将关闭游戏......
### [消息称英特尔 CEO 已同台积电高管会面，但谈判并未如愿](https://www.ithome.com/0/592/849.htm)
> 概要: 本周开始接任英特尔 CEO 之后首次亚洲之行的帕特・基辛格（Pat Gelsinger），已同台积电高层会面，但两家公司之间的谈判，并未像英特尔期望那样的进行。从外媒的报道来看，帕特・基辛格在当地时间......
### [《黑相集》开发商正在开发一款未公布的多人游戏](https://www.3dmgame.com/news/202112/3831029.html)
> 概要: 根据外媒gamesradar的发现，《直到黎明》和《黑相集》系列的开发商Supermassive正在开发一款未公布的多人游戏。gamesradar发现中Supermassive工作室的招聘页面上，有一......
### [年终盘点：进化中的直播带货，财富神话与流量泡沫齐飞](https://www.tuicool.com/articles/nUfyee2)
> 概要: 几经更迭的直播带货，自疫情之后，便呈现出井喷式发展的势态，并迅速成为各大平台和品牌的标配。但随着“全民直播”风起，隐藏在光环之下的种种矛盾也开始浮出水面。这一年，薇娅、李佳琦等头部主播与品牌间的关系开......
### [《死神》千年血战篇播放日期确定 2022年10月开播](https://acg.gamersky.com/news/202112/1446004.shtml)
> 概要: TV动画《死神》千年血战篇将于2022年10月开播。
### [又烂又怂，“他”凭什么当主角？](https://new.qq.com/rain/a/20211216A09YDY00)
> 概要: 这是几年前，青年编导孔大山拍摄的短剧《法制未来时》中的金句。这部不到十分钟的短剧，用夸张的黑色幽默，讽刺了“文艺片闷死人”这种现象。            之所以要提这个段子，是因为它非常适合今天的主......
### [宇宙“嘴”王者遇上超新星！马伯骞担任《大王饶命》星推官](https://new.qq.com/omn/ACF20211/ACF2021121600744400.html)
> 概要: 吐槽界的王者是谁？就是他！用「嘴」怼上宇宙最强的吕树！歌手界的「异势力」是谁？就是他！马伯骞！正在腾讯视频独家播出的《大王饶命》今日公布由歌手、演员、策展人马伯骞担任《大王饶命》星推官！三次元大咖撞上......
### [小米小爱同学“AI 字幕”即将支持日语和韩语翻译](https://www.ithome.com/0/592/877.htm)
> 概要: 感谢IT之家网友梵修的线索投递！IT之家12 月 16 日消息，小米社区发布公告，小爱同学“AI 字幕”日韩内测开启招募，即将支持日语和韩语翻译。在观看日剧、韩综、或与外国人拨打微信 / Zoom 等......
### [解读李子柒品牌：重新认识IP消费品](https://www.tuicool.com/articles/vANVN3r)
> 概要: 李子柒（本名李佳佳）与其所属公司微念的诉讼在即，舆论场上围绕双方孰是孰非的讨论已持续了近150天。此前，开菠萝财经曾在《》一文中提及，一方是拥有千万量级粉丝的红人，另一方是共同打造李子柒IP的微念公司......
### [产品“无”之道（四）——框架篇](https://www.tuicool.com/articles/iAVVFzB)
> 概要: 编辑导语：产品从0到1的过程中，角色框架扮演着必不可少的部分。产品经理需要结合业务场景和使用场景来考虑背后的信息价值。在本篇内容里，作者通过3个案例为大家详细介绍产品“无”之道中的角色框架，希望能给你......
### [《英雄联盟》2021 德玛西亚杯小组赛赛程公布，12 月 17 日 12:00 正式打响](https://www.ithome.com/0/592/884.htm)
> 概要: IT之家12 月 16 日消息，2021 德玛西亚杯将于 12 月 17 日至 12 月 26 日在上海虹桥天地演艺中心（LPL 主场）举办，来自 LPL、LDL 和 4 个大众赛事渠道的 24 支战......
### [做网红，马斯克应该来中国](https://www.tuicool.com/articles/ymIRVv7)
> 概要: 图片来源@视觉中国文 | 市值榜，作者｜文若善，编辑｜赵元马斯克又不按常理出牌了。前几天，他在社交媒体称，正在考虑辞掉当前的工作，成为一名全职的“influencer”，influencer译文是意见......
### [李一男造车抢在华为前](https://www.huxiu.com/article/482358.html)
> 概要: 本文来自微信公众号：字母榜（ID：wujicaijing），作者：薛亚萍，题图来自：视觉中国“华为前太子”李一男正式杀入新能源汽车阵营，与老东家在造车领域再次狭路相逢。12月15日，李一男创业新项目牛......
### [浪漫气息 头像集图](https://new.qq.com/omn/20211216/20211216A0BEPF00.html)
> 概要: 浪漫气息头像集图                                                                                            ......
### [周丽淇带娃逛海洋公园，玩得比儿子还开心，2岁Morris神似傅程鹏](https://new.qq.com/rain/a/20211216A0BJQQ00)
> 概要: 12月16日，42岁女星周丽淇（周励淇）在社交网分享一组亲子游玩照，罕见曝光2岁儿子Morris的近况。近段时间，Niki返港复出后一直在剧组拍戏，难得放假空闲亦是趁着天气晴朗带娃出门去户外运动，享受......
### [时隔两年LO圈顶流谢安然再出国风COS，还放言：不心动算我输](https://new.qq.com/omn/20211216/20211216A0BX9E00.html)
> 概要: 此前通过《创造营2020》被大众熟知的谢安然，曾经凭借“从150斤瘦到90斤”上过热搜，她其实是洛丽塔圈顶级LO娘，号称是“LO圈顶级种草姬”，年纪轻轻的她在圈内的知名度很高，只要是她安利过的服饰，销......
### [全年外贸形势如何？商务部：进出口结构将进一步优化 贸易大国地位更加巩固](https://finance.sina.com.cn/china/gncj/2021-12-16/doc-ikyakumx4589545.shtml)
> 概要: 原标题：全年外贸形势如何？商务部答每经问：进出口结构将进一步优化 贸易大国地位更加巩固 ◎预计全年外贸进出口结构将进一步优化，高质量发展加速推进...
### [让新市民住有所居，上海1.85万套保障性租赁住房集中开工](https://finance.sina.com.cn/china/gncj/2021-12-16/doc-ikyakumx4590178.shtml)
> 概要: 原标题：让新市民住有所居，上海1.85万套保障性租赁住房集中开工 上海今明两年计划建设筹措保障性租赁住房24万套（间）。 上海保障性租赁住房建设的脚步还在进一步加快。
### [周杰伦晒与儿子看风景父爱爆棚，为昆凌掌镜自夸拍照技术好](https://new.qq.com/rain/a/20211216A0CDUO00)
> 概要: 周杰伦又双叒叕去度假了，12月16日，他在社交平台上久违晒出自己和儿子的同框照，并中英并用地说道：chill一下。            不知这是山间的豪华酒店，还是朋友的豪宅，房间时常敞亮，透过超大......
### [伊莎贝尔·于佩尔获柏林电影节终身成就奖](https://ent.sina.com.cn/m/f/2021-12-16/doc-ikyakumx4590161.shtml)
> 概要: 新浪娱乐讯 12月16日，柏林电影节宣布将授予法国女演员伊莎贝尔·于佩尔终身成就奖“荣誉金熊奖”，明年2月15日颁发。本届影展也将在经典单元放映于佩尔的6部代表作。　　伊莎贝尔·于佩尔曾主演《钢琴教师......
### [上海大力发展保障性租赁住房，企业积极行动](https://finance.sina.com.cn/china/gncj/2021-12-16/doc-ikyamrmy9416352.shtml)
> 概要: 原标题：上海大力发展保障性租赁住房，企业积极行动 上海为企业进行租赁市场布局提供了良好支持。 2017年7月，上海首块只租不售的纯租赁住宅用地成交...
### [《长三角市场监管一体化发展“十四五”规划》正式发布！带你一图读懂](https://finance.sina.com.cn/jjxw/2021-12-16/doc-ikyakumx4592988.shtml)
> 概要: 原标题：《长三角市场监管一体化发展“十四五”规划》正式发布！带你一图读懂 记者16日从上海市场监管部门了解到，日前，上海市、江苏省、浙江省...
### [本以为是治愈番，看完发现是致郁番，这部6年前的动画首集就掀起热议](https://new.qq.com/omn/20211216/20211216A0CNW700.html)
> 概要: 治愈番和致郁番，这两个词不仅是读音相同，就连实际区分起来也有些困难。尤其是那些催泪向的作品，到底是属于治愈还是致郁的类型，就挺让人犯难的。除了区分困难之外，有些动漫还喜欢在这两种风格之间相互切换。以至......
### [杜淳双腿跪地陪女儿爬行！小蛋饺穿纸尿裤好可爱，豪宅内摆满奢侈玩偶](https://new.qq.com/rain/a/20211216A0COWA00)
> 概要: 近日，知名演员杜淳在社交平台晒出了自己陪女儿“小蛋饺”爬行的画面，并配文“陪宝贝一起锻炼爬行”。视频中，杜淳不断鼓励女儿继续往前爬，画面十分有爱。            杜淳穿着短袖和黑色居家服，趴在......
### [日本首相称目前没有计划出席北京冬奥会 外交部回应](https://finance.sina.com.cn/jjxw/2021-12-16/doc-ikyamrmy9419582.shtml)
> 概要: 【环球网报道】2021年12月16日，外交部发言人汪文斌主持例行记者会，部分内容如下。 路透社记者：日本首相称，他目前没有计划出席北京冬奥会。中方对此有何回应？
### [国家发展改革委建立违背市场准入负面清单案例归集和通报制度](https://finance.sina.com.cn/jjxw/2021-12-16/doc-ikyakumx4594483.shtml)
> 概要: 原标题：国家发展改革委建立违背市场准入负面清单案例归集和通报制度 新华社北京12月16日电（记者谢希瑶、安蓓）《国家发展改革委关于建立违背市场准入负面清单案例归集和...
# 小说
### [直播之狩猎荒野](https://m.qidian.com/book/1020816023/catalog)
> 作者：土土士

> 标签：都市生活

> 简介：北美深水钓鱼王大赛；阿尔泰山巅追逐雪豹；澳洲烈火杀巨型蝙蝠；加拿大暴雪械搏棕熊！我是王奎，一名户外荒野主播，职业猎人，而我的开局，仅仅只有一个人，一把刀，和一条……犯过命案的狼狗！已完本万订精品《直播之荒野挑战》，书荒可看。

> 章节总数：共666章

> 状态：完本
# 论文
### [SGM3D: Stereo Guided Monocular 3D Object Detection | Papers With Code](https://paperswithcode.com/paper/sgm3d-stereo-guided-monocular-3d-object)
> 日期：3 Dec 2021

> 标签：None

> 代码：None

> 描述：Monocular 3D object detection is a critical yet challenging task for autonomous driving, due to the lack of accurate depth information captured by LiDAR sensors. In this paper, we propose a stereo-guided monocular 3D object detection network, termed SGM3D, which leverages robust 3D features extracted from stereo images to enhance the features learned from the monocular image.
### [Deceive D: Adaptive Pseudo Augmentation for GAN Training with Limited Data | Papers With Code](https://paperswithcode.com/paper/deceive-d-adaptive-pseudo-augmentation-for)
> 日期：12 Nov 2021

> 标签：None

> 代码：None

> 描述：Generative adversarial networks (GANs) typically require ample data for training in order to synthesize high-fidelity images. Recent studies have shown that training GANs with limited data remains formidable due to discriminator overfitting, the underlying cause that impedes the generator's convergence.
