---
title: 2021-10-06-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SWColorado_ZH-CN2381176407_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-10-06 22:52:25
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SWColorado_ZH-CN2381176407_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Andrew Yang Founds the Forward Party](https://www.forwardparty.com/)
> 概要: Andrew Yang Founds the Forward Party
### [动漫情侣头像｜小众情头](https://new.qq.com/omn/20210926/20210926A0EGVC00.html)
> 概要: 图源网络侵删 原截或原创看到可评论注明一键分享投币评论转发欢迎白嫖加关注......
### [PostgreSQL 14 on Kubernetes](https://blog.crunchydata.com/blog/postgresql-14-on-kubernetes)
> 概要: PostgreSQL 14 on Kubernetes
### [育碧喜加一！PC版《幽灵行动》初代游戏免费送](https://www.3dmgame.com/news/202110/3825102.html)
> 概要: 在庆祝《幽灵行动》发售20周年直播活动上，育碧公布了“20周年大放送”活动。从10月6日至10月11日，玩家可以在育碧Connect PC上免费领取《幽灵行动》游戏和《幽灵行动：荒野》DLC“魅影坠落......
### [组图：unine毕业一年 陈宥为胡春杨等重回宿舍合照团魂依旧](http://slide.ent.sina.com.cn/star/slide_4_704_362359.html)
> 概要: 组图：unine毕业一年 陈宥为胡春杨等重回宿舍合照团魂依旧
### [Selina承认和"小鲜肉"有联系 想和杨洋拍爱情片](https://ent.sina.com.cn/s/h/2021-10-06/doc-iktzqtyt9892211.shtml)
> 概要: 新浪娱乐讯 5日Selina任家萱出席慈善记者会，表示因为最近在拍摄恐怖片很心累，想拍爱情片。问到最想合作谁时，任家萱脸红表示很想合作杨洋。　　此外，她感情现况也跟着曝光。日前被拍到和小鲜肉聚餐，被问......
### [TV动画《斗神机G's Frame》10月12日放送决定！](https://news.dmzj.com/article/72372.html)
> 概要: 原创10月新番《斗神机G's Frame》终于在近日公布了主视觉图及声优信息，将于10月12日播出。
### [组图：李现刘芮麟餐厅聚餐被偶遇 三人点两份牛排引惊叹](http://slide.ent.sina.com.cn/star/slide_4_704_362360.html)
> 概要: 组图：李现刘芮麟餐厅聚餐被偶遇 三人点两份牛排引惊叹
### [TV动画《怪人开发部的黑井津》公开先导PV！](https://news.dmzj.com/article/72373.html)
> 概要: TV动画《怪人开发部的黑井津》宣布于2022年1月8日播出，近日公开了主宣传图、先导PV。
### [鬼灭之刃第二季上映日期公布，鬼化堕姬阵容曝光，音柱三妻亮相！](https://new.qq.com/omn/20211003/20211003A05EAA00.html)
> 概要: 鬼灭之刃自无限列车篇结束后，开始公布第二季游郭篇动画制作内容。截止目前已经时隔过去了半年之久，如今终于等到了官方制定的上映时间。这下小伙伴们就不用在继续苦苦等待了，此次游郭篇上映之前，还有无限列车篇T......
### [TV动画《玫瑰之王的葬队》公开主视觉图及预告！](https://news.dmzj.com/article/72374.html)
> 概要: 动画《玫瑰之王的葬队》在遇到延期问题后，终于宣布于2022年1月播出，并连续两季度播出。
### [Win11打游戏更好了？这2项功能直接让CPU倒退一代](https://www.3dmgame.com/news/202110/3825121.html)
> 概要: Windows11已经于昨天正式开始了推送，相信许多朋友都已经升级体验一番。此前就有媒体报道，Windows 11在游戏方面有小幅提升，可以带来更稳定的游戏性能表现。但是，没想到却在Windows 1......
### [原来，剧本杀的本质是社交](https://www.tuicool.com/articles/2mqyqiu)
> 概要: 关于剧本杀的创业，究竟有哪些门道？采写 | 万鸣宇编辑 | 靖宇这是一种靠故事驱动，年轻人三五成群在特定剧情空间里体验角色扮演的娱乐方式。据说，过去三年，全国的剧本杀线下门店数量翻了十几倍，2020 ......
### [日厂推出高性能游戏坐垫 人体工学高科技缓解腰痛](https://www.3dmgame.com/news/202110/3825122.html)
> 概要: 对于玩家来说，长时间游戏导致腰酸背痛似乎很常见，近日日厂西川宣布专门针对腰痛玩家设计推出了新款高性能游戏坐垫，多种人体工学高科技可以缓解玩家久坐腰痛。·日厂西川设计的新款游戏坐垫“GZ-200JP”是......
### [《废土3》最终DLC与“科罗拉多合集”现已正式发售](https://www.3dmgame.com/news/202110/3825125.html)
> 概要: 好评系列游戏最新作《废土3》今日迎来最终剧情DLC“神圣大爆炸邪教”。这款DLC现已登陆PC、PS4、PS5、Xbox One和Xbox Series X/S平台。另外，《废土3：科罗拉多合集》也已面......
### [微软现向所有 Win11 用户推出全新“画图”应用](https://www.ithome.com/0/579/194.htm)
> 概要: IT之家10 月 6 日消息 微软上周向部分测试通道推送了全新的“画图”应用，但仅有部分用户拥有。随着 Windows 11 正式版的到来，采用 WinUI 界面的全新“画图”应用（版本号 11.21......
### [ERP系统解决方案之推导过程](https://www.tuicool.com/articles/YNneqmA)
> 概要: 编辑导语：ERP系统的有效建构有助于企业内部资源进行系统化管理，而随着市场环境的变化，行业对ERP系统的需求也在增大。ERP软件开发商也需要针对变化的市场来进行功能架构上的调整和优化。本篇文章里，作者......
### [Android 平台的颜值标杆：Material You 应用大赏](https://www.tuicool.com/articles/eeaQNrB)
> 概要: 如果说要罗列出 Android 12 最令人印象深刻的几大变化，Google 基于全新设计语言 Material You 为 Google Pixel 打造的这套新 UI 一定是其中之一。在 Pixe......
### [调查显示：多数苹果用户对 iPhone 13 和 Apple Watch S7 并不满意](https://www.ithome.com/0/579/196.htm)
> 概要: IT之家10 月 6 日消息 据 MacRumors 报道，SellCell 的一项新调查结果显示，苹果设备用户对iPhone 13系列和 Apple Watch Series 7 大多不感兴趣。IT......
### [摩托罗拉 Moto E40 售价曝光：两款配色，约 1200 元](https://www.ithome.com/0/579/199.htm)
> 概要: IT之家10 月 6 日消息 摩托罗拉在上月推出了 Moto E20，这是一款运行Android11 Go 的手机。接着，一款名为 Moto E40 的手机曝光，配备 6.5 英寸 720p+ 显示屏......
### [当年轻人不再相信“嫁给爱情”](https://www.huxiu.com/article/461500.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水题图 | 日剧《失恋巧克力职人》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。有句老......
### [被老师留堂、被单位劝退，新诺奖得主的人生太精彩](https://www.huxiu.com/article/461499.html)
> 概要: 本文来自微信公众号：果壳（ID：Guokr42），作者：翻翻、小毛巾、李小葵、luna，编辑：麦麦，头图来自：视觉中国克劳斯·哈塞尔曼（Klaus Hasselmann）是昨晚新鲜出炉的诺贝尔奖得主......
### [How to Build a Low-Tech Solar Panel?](https://solar.lowtechmagazine.com/2021/10/how-to-build-a-low-tech-solar-panel.html)
> 概要: How to Build a Low-Tech Solar Panel?
### [华为到底是不是车企的朋友？](https://www.huxiu.com/article/461506.html)
> 概要: 本文来自微信公众号：界面新闻（ID：wowjiemian），作者：陆柯言，编辑：文姝琪，头图来自：视觉中国华为到底是不是车企的朋友？可能没有车企能斩钉截铁地回答这个问题。仅仅是对外强调“不造车”，华为......
### [周洁最后的日子：不惜重金包机回到故乡上海](https://ent.sina.com.cn/s/m/2021-10-06/doc-iktzscyx8197552.shtml)
> 概要: 在浦东医院隔离治疗的第五天，舞蹈家周洁因病离开人世，最终实现了她自己“叶落归根，回到家乡，回到祖国”的宿愿。　　一位熟悉她的作家说：“周洁是一定要回到上海来才会离去的。她在美国是不会瞑目的，因为对她来......
### [组图：刘亚仁领衔釜山电影节阵容 朴素丹宋仲基担任主持人](http://slide.ent.sina.com.cn/film/k/slide_4_704_362373.html)
> 概要: 组图：刘亚仁领衔釜山电影节阵容 朴素丹宋仲基担任主持人
### [香港特区行政长官林郑月娥：未来十年香港可兴建约33万个公屋](https://finance.sina.com.cn/jjxw/2021-10-06/doc-iktzqtyt9975630.shtml)
> 概要: 新华社香港10月6日电（记者王茜）香港特区行政长官林郑月娥6日在特区立法会发表她任期内的第五份施政报告时表示，未来十年香港可兴建约330000个公屋。
### [国庆节假期南方电网负荷电量大增](https://finance.sina.com.cn/roll/2021-10-06/doc-iktzqtyt9979997.shtml)
> 概要: 中新社广州10月6日电 （王华 蓝旺）中国南方电网公司6日表示，今年国庆节假期期间，全网负荷、电量同比均大幅增加，南方电网公司多措并举，全力以赴保障电力供应。
### [头像集图 你也一定会是某个人的奇迹。](https://new.qq.com/omn/20211002/20211002A06ZXC00.html)
> 概要: 头像集图你也一定会是某个人的奇迹。                                                                                   ......
### [《两不疑》三皇叔准备造反，徐钰女王好般配，皇上的宫斗水平很高](https://new.qq.com/omn/20211006/20211006A055DQ00.html)
> 概要: 《两不疑》的官方动画第一季下半部，每周三都会更新，本周播到了第19集。这部动漫每一集虽然时长有点短，不过剧情紧凑信息量很多。皇后徐钰还在外征战，转眼间就到了过年的时候了，这可能是帝后这么多年以来，第一......
### [千万片酬不香了？顶流明星考公务员，娱乐圈不好混了](https://new.qq.com/omn/20211006/20211006A09IHC00.html)
> 概要: 说起考公务员，这些年可以说是相当热门，因为现在大学毕业生越来越多，就业压力也越来越大。尤其一场疫情下来，让很多人认识到“铁饭碗”的重要性，公务员平时可能收入不如企业高，但是关键是胜在稳定。考公务员的大......
### [国庆假期返程，各地防疫政策最新梳理→](https://finance.sina.com.cn/jjxw/2021-10-06/doc-iktzscyx8218549.shtml)
> 概要: @中国新闻网 准备返程的朋友注意！ 国庆假期接近尾声，各地即将迎来返程高峰，全国多个城市公布了最新防疫政策—— 北京市：国内中高风险地区人员暂不允许进京...
### [纸书式微，头部民营出版商何以悄然壮大](https://finance.sina.com.cn/china/2021-10-06/doc-iktzscyx8218960.shtml)
> 概要: 头部民营出版商们在过去十年并未被时代淘汰，反而悄然成长壮大，这与近十年来出版行业的市场化改革、资本的进入等因素有关
### [《战地2042》BETA公测视频 PC版128人大战](https://www.3dmgame.com/news/202110/3825142.html)
> 概要: 《战地2042》BETA公测现已面向预购玩家开启，测试中将展出全新的地图与玩法、壮观的天候变化，以及更灵活的技能武器系统。下面是外媒IGN带来的PC版128人大战（64v64，征服模式）。IGN 12......
### [诺贝尔化学奖得主：物理教室太冷，化学教室暖和，我决定转去化学系](https://finance.sina.com.cn/china/2021-10-06/doc-iktzscyx8220157.shtml)
> 概要: 当时时间10月6日上午，2021年诺贝尔化学奖（Nobel Prize in Chemistry）揭晓。 瑞典皇家科学院宣布，将该奖项授予了本杰明·李斯特（Benjamin...
### [《长津湖》票房破30亿，连续6天单日票房破4亿](https://finance.sina.com.cn/jjxw/2021-10-06/doc-iktzscyx8222745.shtml)
> 概要: 新京报讯（记者 滕朝）据灯塔专业版实时数据显示，截至10月6日21时19分，电影《长津湖》上映第7天，票房突破30亿，连续6天单日票房破4亿元，暂居内地影史票房榜第13位。
### [European Parliament Calls for a Ban on Facial Recognition](https://www.politico.eu/article/european-parliament-ban-facial-recognition-brussels/)
> 概要: European Parliament Calls for a Ban on Facial Recognition
### [最年轻的“武英级”武术运动员，比甄子丹还厉害，吴樾为何不火？](https://new.qq.com/omn/20211006/20211006A0A8EM00.html)
> 概要: 相信很多朋友都喜欢看《叶问4》，特别是叶问和万会长对打的那一段，咏春对太极，可谓是龙争虎斗，难分伯仲，近3分钟的打戏让人看得目不转睛，酣畅淋漓。            叶问的扮演者甄子丹和万会长的扮演......
### [产能无法满足需求，台积电将优先考虑不囤积芯片的公司订单](https://www.ithome.com/0/579/238.htm)
> 概要: IT之家10 月 6 日消息 台积电正采取各种措施应对芯片持续短缺。此前，价格上涨和其他措施（如优先考虑苹果等公司）都在跟进，但根据最新的采访，台积电将优先考虑没有囤积芯片的公司订单。台积电董事长刘德......
### [我们为什么永远为脱口秀感动？](https://new.qq.com/omn/20211006/20211006A0AEOA00.html)
> 概要: 不知道大家发现没有？这季《脱口秀大会》“准冠军”多到有点通货膨胀的程度了。这才刚进行到半决赛，我们大老师就已经自行颁发了三四五六七…不知道多少个“冠军”了。            倒也不是大老师“少见......
### [一篇文章分享九本书，一定对你有启发](https://www.tuicool.com/articles/mM3uQnj)
> 概要: 以下这九本书，思路可以串起来,希望对你有启发01 用行动中的实际问题，验证是否有问题。在《科学创业》中叫做验证性思维。我们在做事之前的问题不一定是真正的问题。先行动起来，遇到的问题会逐步解决或者说不再......
# 小说
### [大道盘古](http://book.zongheng.com/book/1107046.html)
> 作者：弘尘落

> 标签：奇幻玄幻

> 简介：别浪费时间，此书很烂。

> 章节末：第499章 最后战（完结）

> 状态：完本
# 论文
### [Track without Appearance: Learn Box and Tracklet Embedding with Local and Global Motion Patterns for Vehicle Tracking | Papers With Code](https://paperswithcode.com/paper/track-without-appearance-learn-box-and)
> 日期：13 Aug 2021

> 标签：None

> 代码：https://github.com/gaoangw/TC_tracker

> 描述：Vehicle tracking is an essential task in the multi-object tracking (MOT) field. A distinct characteristic in vehicle tracking is that the trajectories of vehicles are fairly smooth in both the world coordinate and the image coordinate.
### [Planning from video game descriptions | Papers With Code](https://paperswithcode.com/paper/planning-from-video-game-descriptions)
> 日期：1 Sep 2021

> 标签：None

> 代码：https://github.com/ignaciovellido/vgdl-pddl

> 描述：This project proposes a methodology for the automatic generation of action models from video game dynamics descriptions, as well as its integration with a planning agent for the execution and monitoring of the plans. Planners use these action models to get the deliberative behaviour for an agent in many different video games and, combined with a reactive module, solve deterministic and no-deterministic levels.
