---
title: 2021-10-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.NewtonPumpkins_ZH-CN2560195971_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-10-27 22:53:03
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.NewtonPumpkins_ZH-CN2560195971_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Visual Studio 2022 针对 Unreal Engine 进行优化](https://www.oschina.net/news/166027/vs2022-unreal-engine)
> 概要: Visual Studio 2022 将于11 月 8 日正式发布。不过微软已经在过去几个月发布了多个 VS 2022 预览版，相信开发者已经了解新版带来的变化。例如 Visual Studio 20......
### [Pyston 路线图发布，将实现 64 位 ARM 并继续提高性能](https://www.oschina.net/news/166019/pyston-20211027-roadmap)
> 概要: Pyston 官方发布了最新的路线图，确定了其近期的开发重点。Pyston 最初是由 Dropbox 于 2014 - 2017 年开发的基于开源 JIT 的 Python 实现，但之后 Dropbo......
### [树莓派 4 获得 Vulkan 1.1 一致性认证](https://www.oschina.net/news/166023/raspberry-pi-obtain-vulkan-1-1-conformance)
> 概要: 树莓派Raspberry官方宣布，其获得了 Khronos 对 Raspberry Pi 4授予的 Vulkan 1.1 一致性认证。自 Raspberry Pi 4宣布符合 Vulkan 1.0 标......
### [单核心 IOPS 突破 1000 万，Linux 5.16 有望大幅提升 I/O 性能](https://www.oschina.net/news/166022/linux-io-10m-iops)
> 概要: Jens Axboe是 Linux block 子系统的负责人，目前就职于 Facebook，他也因开发了 IO_uring 而闻名，他对 IO_uring 的不断开发与优化使得 Linux 系统的 ......
### [吴京，13年前导演的第一部电影《狼牙》，你还记得吗？](https://new.qq.com/rain/a/20211027A000D700)
> 概要: 前几天，在成龙国际动作电影周上，吴京被成龙羡慕（嫉妒恨），已经成为笑话。                                    但成龙，是大哥，还是大哥。吴京在成龙面前，只能是小弟，而......
### [P站美图推荐——金木犀特辑](https://news.dmzj.com/article/72568.html)
> 概要: 绽放在秋日暖阳里的金木犀，为空气带来香甜的味道，让世界仿佛都盖上一层橙色的滤镜
### [剧场版动画《怪盗女王喜欢马戏团》公开制作阵容](https://news.dmzj.com/article/72569.html)
> 概要: 剧场版动画《怪盗女王喜欢马戏团》公开了主宣传图，本作的主要制作阵容也一并公开。其中负责动画制作的是EAST FISH STUDIO，傳沙织担任导演，国泽真理子负责剧本，河岛久美子负责角色设计。
### [偶像类企划《Shine Post》宣布动画将于2022年夏开播](https://news.dmzj.com/article/72570.html)
> 概要: 跨媒体企划《Shine Post》宣布了动画将于2022年夏季开始播出的消息，作品的PV和MV也一并公开。
### [灵光一闪！帮你使用Vue，搞定无法解决的“动态挂载”](https://segmentfault.com/a/1190000040870566?utm_source=sf-homepage)
> 概要: 在一些特殊场景下，使用组件的时机无法确定，或者无法在Vue的template中确定要我们要使用的组件，这时就需要动态的挂载组件，或者使用运行时编译动态创建组件并挂载。今天我们将带大家从实际项目出发，看......
### [两位中国人，决定去太空挖矿](https://www.huxiu.com/article/466845.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自电影《火星救援》。2020年的全球火星大会（Mars Society Convention），人们几乎只记住了马斯克洋洋洒洒一小时讲话里，关于“2024年把一艘......
### [33部Kirara动画化作品人气投票结果发表！](https://news.dmzj.com/article/72578.html)
> 概要: 最近日本网站Netorabo调查队开展了一次“《MangaTimeKirara》动画化的作品中你最喜欢哪一个？”的投票活动。
### [Go 1.18 will embed source version information into binaries](https://utcc.utoronto.ca/~cks/space/blog/programming/GoVersionOfYourSource)
> 概要: Go 1.18 will embed source version information into binaries
### [《咒术回战》剧场版公布最新海报 纯爱之战一触即发](https://acg.gamersky.com/news/202110/1433246.shtml)
> 概要: 《咒术回战》的剧场版《咒术回战 0》公布了最新海报，主角团全员以及被脑花夺舍之前的夏油杰登场。
### [轻小说「再一次的异世界召唤」官方宣布TV动画化](http://acg.178.com/202110/429304107280.html)
> 概要: 近日，轻小说「再一次的异世界召唤」（異世界召喚は二度目です）官方宣布了本作TV动画化的消息，并放出了销量突破70万部的贺图。「再一次的异世界召唤」是一部由岸本和叶创作的异世界题材幻想轻小说。剧情讲述的......
### [冯小刚电影公社发生工商变更 华谊兄弟疑退出](https://ent.sina.com.cn/2021-10-27/doc-iktzscyy2030015.shtml)
> 概要: 新浪娱乐讯 据天眼查显示，近日，海南观澜湖华谊冯小刚文化旅游实业有限公司发生工商变更，原股东华谊兄弟（天津）实景娱乐有限公司退出，目前仍担任副董事长。　　海南观澜湖华谊冯小刚文化旅游实业有限公司为观澜......
### [春场葱公开剧场版「五等分的新娘」宣传贺图](http://acg.178.com/202110/429307101394.html)
> 概要: 昨日（10月26日），「五等分的新娘」公开了剧场版将于2022年初夏在日本地区上映的消息，原作者春场葱公开了其为剧场版「五等分的新娘」绘制的宣传贺图。漫画「五等分的新娘」于「周刊少年Magazine」......
### [TV动画「Shine Post」主视觉图公布](http://acg.178.com/202110/429307182998.html)
> 概要: 由骆驼所创作的轻小说「Shine Post」于近日宣布了动画化的消息，官方公布了TV动画「Shine Post」的主视觉图。该作预计将于2022年夏季（7月）播出。CAST：青天国春：铃代纱弓玉城杏夏......
### [Roy Fielding's Misappropriated REST Dissertation (2020)](https://twobithistory.org/2020/06/28/rest.html)
> 概要: Roy Fielding's Misappropriated REST Dissertation (2020)
### [「东京卍复仇者」公开半间修二生日贺图](http://acg.178.com/202110/429311836106.html)
> 概要: 今日（10月27日）是「东京卍复仇者」半间修二的生日，官方公开了为其绘制的生日贺图。「东京卍复仇者」是由日本漫画家和久井健创作的漫画作品，漫画改编的同名电视动画由LIDEN FILMS负责制作，已于2......
### [张鲁一聂远硬核较量 谍战剧《前行者》风云迭起](https://ent.sina.com.cn/v/m/2021-10-27/doc-iktzqtyu3831073.shtml)
> 概要: 新快报讯 记者梁燕芬报道 红色谍战剧《前行者》目前正在北京卫视热播，张鲁一和聂远变身一对信仰不同的生死兄弟，围绕“零号文件”“营救行动”“忏悔录”等诸多事件，两人展开了一场场硬核较量。　　《前行者》聚......
### [中式“政经大戏”开锣 《突围》演绎国企改革风云](https://ent.sina.com.cn/v/m/2021-10-27/doc-iktzscyy2069275.shtml)
> 概要: 4年前，周梅森编剧的电视剧播出，成为现象级爆款。近日，由周梅森编剧，靳东、闫妮、黄志忠领衔主演的现实主义电视剧登陆东方卫视东方剧场，开播第一天只凭一集就拿下21个热搜。不少观众表示，虽然才播出了几集，......
### [早期项目 | 想做一款专属女性的酒，「梅花里」认为女性饮酒需求远未被满足](https://www.tuicool.com/articles/BnEvaeF)
> 概要: 低度酒赛道曾在去年一度走红，后来因为产品同质化严重、不容易打出壁垒而不再被看好，由于线上场景受限，有些靠直播和流量带起来的新锐品牌，销量也逐渐滑落。梅花里创始人陈颖睿告诉36氪，做消费品还是要回归产品......
### [豆瓣App删除用户手机相册图片？回应称无法读取，正调查](https://www.tuicool.com/articles/A7FrumV)
> 概要: 近日，有网友曝出其保存在手机相册的图片，无故被豆瓣App删掉，需要从回收站内才能找回图片，引发热议。10月27日，豆瓣方面向南都记者表示，尚未证实该名网友的豆瓣App用户身份。豆瓣App部分历史版本在......
### [羽生田挙武手写信回应恋情 称参加创造营时是单身](https://ent.sina.com.cn/tv/zy/2021-10-27/doc-iktzscyy2088138.shtml)
> 概要: 新浪娱乐讯 10月27日，有网友称《创造营》日本选手羽生田拳武和日本女模特恋爱，两人曾多次在相同地点打卡，包括比赛时两人也在谈恋爱。对此，羽生田拳武发文澄清此事，强调自己在参加比赛时是单身。之后，由于......
### [中芯深圳扩产背后：电源管理 IC 转 12 英寸疑云再起](https://www.ithome.com/0/583/155.htm)
> 概要: 一纸项目公示，将“电源管理 IC 转 12 英寸”这一老生常谈的课题，再次摆至台面 —— 根据深圳市坪山区投资推广服务署近日发布的中芯国际深圳扩产项目公示，项目产品定位于 12 英寸 28nm 及以上......
### [东北菜，才是中产阶级轻食的祖宗](https://www.huxiu.com/article/467425.html)
> 概要: 本文来自微信公众号：网易上流（ID：heyupflow），作者：一只狒狒，编辑：豌豆，头图来自：视觉中国最近北方已经入冬了，上流君本就粗壮的腿子在秋裤的包裹下更加不忍直视，于是，上流君开始了吃草的日子......
### [鱿鱼游戏巨型人偶亮相首尔:亲身体验"123木头人"](https://ent.sina.com.cn/v/j/2021-10-27/doc-iktzqtyu3883348.shtml)
> 概要: 10月25日，韩国首尔一家公园展出韩剧《鱿鱼游戏》中巨型人偶，吸引了大批游客。有游客身穿该剧同款绿色运动服前来参观，并与人偶合影。该人偶是剧中“123木头人”游戏的关键道具，高度约为4米。(责编：小5......
### [山姆与家乐福：注定有的一场风波](https://www.huxiu.com/article/467160.html)
> 概要: 出品 | 虎嗅金融组作者 | 周月明题图 | 视觉中国一封“深夜致歉信”，撕开了商超巨头“内卷”的一角。10月22日，家乐福中国首家会员店在上海浦东新区正式开业，然而开业当晚，官方微博就发“致歉信”称......
### [李子柒何以被逼到对簿公堂？](https://www.huxiu.com/article/467309.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜黄青春题图｜绿洲@李子柒这两天，停更三个多月的李子柒成了微博热搜常客，先是#李子柒公司起诉微念#在昨天（10月26日）冲上热搜，接着#杭州微念申请李子柒商标被全部驳回......
### [Animoca Brands联合创始人：元宇宙需开放资产，NFT将提供真正的数字产权](https://www.tuicool.com/articles/EFzmIni)
> 概要: PANews官方｜2021-10-27 18:48NFT将会成为未来的趋势，会成为非常重要的一个重要的组成部分，因为它可以跟我们生活的方方面面来连接。10月27日，由万向区块链实验室主办的2021第七......
### [Redmi Note 11 Geekbench 跑分出炉：搭载天玑 810](https://www.ithome.com/0/583/198.htm)
> 概要: IT之家10 月 27 日消息，红米 Redmi Note 11 系列手机将于 10 月 28 日 19:00 正式发布，官方此前表示该系列将首发天玑 920 芯片。系列的基础款 Redmi Note......
### [Show HN: Kit55, a Desktop Web Builder GUI – Jekyll, Next, WordPress Alternative](https://stack55.com/)
> 概要: Show HN: Kit55, a Desktop Web Builder GUI – Jekyll, Next, WordPress Alternative
### [基于BERT生成对抗样本](https://www.tuicool.com/articles/NfiEryu)
> 概要: 论文《BAE：BERT-based Adversarial Examples for Text Classification》地址：https://arxiv.org/abs/2004.01970介绍......
### [2799 元起，大疆 DJI Action 2 全场景运动相机正式发布：支持多形态磁吸拓展](https://www.ithome.com/0/583/204.htm)
> 概要: IT之家10 月 27 日消息，大疆今日晚间正式发布了DJI Action 2 全场景运动相机，提供多形态磁吸拓展选项，适配多样拍摄场景，售价 2799 元起。相比上一代 Osmo Action，大疆......
### [Apple is ready to admit it was wrong about the future of laptops](https://www.theverge.com/22734645/apple-macbook-pro-2021-ports-magsafe-touch-bar-usb-c-future)
> 概要: Apple is ready to admit it was wrong about the future of laptops
### [这波新剧感觉不错](https://new.qq.com/rain/a/20211027A0E64C00)
> 概要: 全新解锁更多好玩的版块~~~            UU们，昨天东方卫视也公布了明年的剧综片单。和平台片单相比，卫视片单的阵容更加强大。有实力演员胡歌、陆毅、黄轩、张鲁一、万茜、王丽坤等人。同时，大家......
### [即使都是明星带货，原来也是有区别的？](https://new.qq.com/rain/a/20211027A0ED3200)
> 概要: 直播带货持续火爆，昨晚李佳琪和薇娅的直播间卖出了令人咋舌的天价销售额。                        火爆的行情，使得很多明星也都加入了各个直播平台带货的行列，这一波直播带货大潮一直没......
### [元气森林被“薅羊毛”？买36瓶气泡水仅付款十几元！店铺产品全部下架](https://finance.sina.com.cn/china/2021-10-27/doc-iktzqtyu3905637.shtml)
> 概要: 元气森林被“薅羊毛”？买36瓶气泡水仅付款十几元！店铺产品全部下架…… 来源：国际金融报 买36瓶气泡水，仅花了不到15元！元气森林官方淘宝店出了“乌龙”，损失不小。
### [英伟达市值超越台积电，专家称元宇宙题材是关键](https://www.ithome.com/0/583/205.htm)
> 概要: 英伟达日前股价大涨 6.7%，市值攀高至 6,179.25 亿美元，一举超越台积电。专家表示，英伟达搭上元宇宙题材，市盈率拉高，是市值超越台积电的关键。美股不少企业财报表现亮眼及消费者信心反弹，收盘大......
### [李光洁：演员不应该有所谓的舒适圈](https://new.qq.com/rain/a/20211027A0EGQH00)
> 概要: 从电影《峰爆》《我和我的父辈》到剧集《再见那一天》《功勋》，李光洁近日频频以新角色同观众见面，好评不断，亦打动了不少观众。李光洁能够感受到由于人生阅历的日渐丰富，塑造出来的角色愈发立体。他将自身的经历......
### [陕西检察机关依法对赵永军涉嫌受贿案提起公诉](https://finance.sina.com.cn/china/gncj/2021-10-27/doc-iktzscyy2144414.shtml)
> 概要: 原标题：陕西检察机关依法对赵永军涉嫌受贿案提起公诉 日前，长安银行股份有限公司原党委书记、董事长赵永军（正厅级）涉嫌受贿罪一案，经陕西省人民检察院指定管辖...
### [从动画中走出来的神仙cos，小舞被官方认可，云韵气质和美貌并存](https://new.qq.com/omn/20211027/20211027A0EIUV00.html)
> 概要: 现在的coser真的是越来越厉害了，无论cos的角色有多难，驾驭难度都多高，她们都能原原本本还原角色的美貌，甚至直接让角色从动画中走出来。看到她们出神入化的cos作品，我们只有一个字可以形容，那就是绝......
### [王勇在中央企业做好今冬明春能源电力保供工作座谈会上强调全力做好保障能源供应工作](https://finance.sina.com.cn/china/2021-10-27/doc-iktzqtyu3907831.shtml)
> 概要: 王勇在中央企业做好今冬明春能源电力保供工作座谈会上强调积极担当履责贡献央企力量全力做好保障能源供应工作 新华社北京10月27日电国务委员王勇27日到国家电网公司调研并...
### [国家发改委、市场监管总局联合开展煤炭现货市场价格专项督查 严厉查处违法违规行为](https://finance.sina.com.cn/china/gncj/2021-10-27/doc-iktzscyy2143398.shtml)
> 概要: 原标题：国家发展改革委、市场监管总局联合开展煤炭现货市场价格专项督查 严厉查处违法违规行为 为认真贯彻落实党中央、国务院关于煤炭保供稳价的系列决策部署...
### [公安部：争取2021年底前全面推行电子驾驶证](https://finance.sina.com.cn/jjxw/2021-10-27/doc-iktzscyy2143490.shtml)
> 概要: 原标题：公安部：争取2021年底前全面推行电子驾驶证 新华社北京10月27日电（记者任沁沁）记者27日从公安部获悉，截至目前，驾驶证电子化已覆盖全国138个城市...
### [解读《中国应对气候变化的政策与行动》白皮书：推动碳达峰、碳中和目标如期实现](https://finance.sina.com.cn/china/2021-10-27/doc-iktzscyy2143951.shtml)
> 概要: 中国应对气候变化的政策与行动（全文） 推动碳达峰、碳中和目标如期实现——解读《中国应对气候变化的政策与行动》白皮书 新华社北京10月27日电题：推动碳达峰...
# 小说
### [我不登天](http://book.zongheng.com/book/910991.html)
> 作者：沅芷

> 标签：奇幻玄幻

> 简介：一名警察和医生的搞笑生涯，死去活来中挣扎前行。不羡仙魔乱舞，只恋人间四月天。山花烂漫，我何必登天。

> 章节末：第九十五章 欺负外地“人”

> 状态：完本
# 论文
### [Predicting COVID-19 Patient Shielding: A Comprehensive Study | Papers With Code](https://paperswithcode.com/paper/predicting-covid-19-patient-shielding-a)
> 日期：1 Oct 2021

> 标签：None

> 代码：None

> 描述：There are many ways machine learning and big data analytics are used in the fight against the COVID-19 pandemic, including predictions, risk management, diagnostics, and prevention. This study focuses on predicting COVID-19 patient shielding -- identifying and protecting patients who are clinically extremely vulnerable from coronavirus.
### [Automated Essay Scoring Using Transformer Models | Papers With Code](https://paperswithcode.com/paper/automated-essay-scoring-using-transformer)
> 日期：13 Oct 2021

> 标签：None

> 代码：None

> 描述：Automated essay scoring (AES) is gaining increasing attention in the education sector as it significantly reduces the burden of manual scoring and allows ad hoc feedback for learners. Natural language processing based on machine learning has been shown to be particularly suitable for text classification and AES.
