---
title: 2022-09-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Aracari_ZH-CN0383753817_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-09-12 21:30:44
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Aracari_ZH-CN0383753817_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [“超长西瓜”出圈，短视频特效生意还能走多远？](https://www.woshipm.com/it/5599276.html)
> 概要: 从单纯辅助视频表达到变成视频的重要内容特色，短视频特效的优势在当下逐渐凸显。各大平台逐渐有多款特效出圈，猩猩脸、奶瓶面膜、柠檬头……近期抖音的“超长西瓜”特效则带来了新的模仿潮流。短视频特效为什么能吸......
### [2019，我转行，劝老大放弃私域；2022了，来解新链路](https://www.woshipm.com/marketing/5597854.html)
> 概要: 2019年是抖音崛起的一年，也是私域破圈的一年，当时的私域项目都在经历着寒冬。而如今，抖音引流、视频引流、直播引流，基本就是标配。在现在这种情形下，如何用私域方式去做链路营销呢？本文作者对此进行了分析......
### [干货分享：交易所钱包管理系统设计](https://www.woshipm.com/pd/5598976.html)
> 概要: 作为web3世界的核心枢纽，交易所托管着大量加密资产，它的钱包管理也关系到大量用户的资产安全。本文作者从产品设计的视角，分享了交易所钱包管理系统的组成、业务流程，一起来看一下吧。交易所作为web3世界......
### [视频：演员李易峰因多次嫖娼被抓 警方通报已被行政拘留](https://video.sina.com.cn/p/ent/2022-09-12/detail-imqmmtha6951793.d.html)
> 概要: 视频：演员李易峰因多次嫖娼被抓 警方通报已被行政拘留
### [媒体：谨防“天价培训”误导考研培训行业发展](https://finance.sina.com.cn/tech/internet/2022-09-12/doc-imqqsmrn8746556.shtml)
> 概要: 教育部日前发布通知，2023年考研的初试时间为2022年12月24日至25日。近年来，考研热持续升温，报名人数和报录比屡创新高，2022年报名人数达457万人。据研究机构预测，2023年考研全国报名人......
### [家装表现案例](https://www.zcool.com.cn/work/ZNjE4NzA5NzY=.html)
> 概要: 简约家装案例表现......
### [俞敏洪评李国庆：没有坏心 脑子有时候“缺根弦”](https://finance.sina.com.cn/tech/internet/2022-09-12/doc-imqqsmrn8747703.shtml)
> 概要: 日前，俞敏洪抖音直播间播出了“俞敏洪对话李国庆”，俞敏洪谈及了自己对李国庆的看法......
### [当下，我们最需要的是确定性](https://www.huxiu.com/article/659308.html)
> 概要: 来源｜秦朔朋友圈（ID：qspyq2015）作者｜秦朔头图｜视觉中国在充满不确定的变化中，要有效提升未来预期的确定性，需要主客观的双重努力。从主观上，企业要认清变化，反躬自省，梳理战略，提升能力。从客......
### [腾讯增持后育碧捍卫独立性 全面出售已无可能](https://finance.sina.com.cn/tech/internet/2022-09-12/doc-imqqsmrn8747974.shtml)
> 概要: 在三年前结束了与前股东威望迪（Vivendi）集团的长期抗争后，法国最大的视频游戏发行商育碧对中国游戏公司腾讯张开双臂......
### [理想汽车总裁沈亚楠一周减持100万股，套现超9000万元](https://finance.sina.com.cn/tech/it/2022-09-12/doc-imqmmtha6971871.shtml)
> 概要: 蓝鲸汽车 王林......
### [豆瓣2.2分，《东八区的先生们》差在哪儿？](https://www.huxiu.com/article/659250.html)
> 概要: 本文来自微信公众号：知著网 （ID：covricuc），作者：妙哇种子，原文标题：《〈东八区的先生们〉：在油腻、悬浮和冒犯女性中收获差评》，题图来自：《东八区的先生们》8月31日，男性群像剧《东八区的......
### [小长假探店：苹果旧机型降价，iPhone14 Pro引关注](https://www.yicai.com/news/101533796.html)
> 概要: iPhone 14要等到9月16日正式上市日才能到货，届时有购买需求的用户可以一睹为快。最新发布的苹果手表也尚未登陆各大门店。
### [PSV模拟器模拟器已可在Steam Deck掌机上运行](https://www.3dmgame.com/news/202209/3851438.html)
> 概要: 近日有国外大神成功在掌机Steam Deck上运行PSV模拟器“Vita 3K”。如果能顺利运行PSV模拟器的话，Steam Deck上可以运行的游戏库容量又获得大幅扩充。PSV还支持Netflix串......
### [Java 默认可变性：“万亿美元级别的错误” | InfoQ 访谈](https://www.tuicool.com/articles/7VNjeyi)
> 概要: Java 默认可变性：“万亿美元级别的错误” | InfoQ 访谈
### [人到中年，疯狂学技能](https://www.huxiu.com/article/659350.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：王敏、唐亚华、李秋涵、邹帅，题图来自：视觉中国你的职业越老越吃香吗？对于医生、律师、教师等传统职业来说，这不是一个难回答的问题，但......
### [罗布乐思宣布加入元宇宙事业 目标革新3D广告项目](https://www.3dmgame.com/news/202209/3851442.html)
> 概要: 目前世界最大的多人在线创作游戏分享平台罗布乐思于美国当地时间9月9日举行的开发者会议上宣布，罗布乐思加入元宇宙事业，目标是 推出革新性的3D投入感类型广告项目，敬请期待。•《Roblox》是一款兼容了......
### [【字幕】米切尔骑士前景预测：假动作投篮杀入禁区，身高太矮防守极差](https://bbs.hupu.com/55511638.html)
> 概要: 听译字幕：陈拓   审核：骁深刻   三叶屋字幕组
### [华为加持下问界 M5 / M7 供不应求：赛力斯第三工厂开工建设，明年 7 月建成](https://www.ithome.com/0/640/407.htm)
> 概要: 感谢IT之家网友根-本不可能的线索投递！IT之家9 月 12 日消息，在华为秋季发布会上，AITO 品牌首款纯电车型问界 M5 EV 正式发布。IT之家了解到，问界 M5 EV 基于赛力斯纯电驱智能平......
### [安倍vs女王：国葬不可怕，谁撞谁尴尬](https://www.huxiu.com/article/659407.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童题图丨yahoo本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。带着结局回顾历史，我们总......
### [手写书法字形丨贰拾柒](https://www.zcool.com.cn/work/ZNjE4NzQ3MjQ=.html)
> 概要: 九月份手写书法字形-工具：羊毫丨狼毫丨兼毫-纸张：生宣丨毛边纸-后期：Photoshop丨 Illustrator......
### [任天堂：《斯普拉遁 3》发售 3 天后日本销量突破 345 万份](https://www.ithome.com/0/640/421.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家9 月 12 日消息，今日，任天堂官方宣布，9 月 9 日发售的任天堂 Switch 游戏《斯普拉遁 3》日本销量已达 345 万份。这是任天堂 Swit......
### [小队战术游戏新作《异形：黑暗后裔》实机演示公布](https://www.3dmgame.com/news/202209/3851449.html)
> 概要: 迪士尼在近日举行的D23博览会上公布了新作《异形：黑暗后裔》（Aliens: Dark Descent）的最新实机演示。《异形：黑暗后裔》是一款战术小队游戏，玩家控制殖民军小队，渗透进入被异形入侵的M......
### [TV动画《死神 千年血战篇》第2弹PV公开 10月10日播出](https://news.dmzj.com/article/75536.html)
> 概要: ​TV动画《死神 千年血战篇》第2弹PV公开，2022年10月10日播出，此次动画将分四季度播出，动画制作进度将至原作漫画完结。
### [林跑跑零食超市IP形象](https://www.zcool.com.cn/work/ZNjE4NzU2MDg=.html)
> 概要: 林跑跑：一款以零食市场商场作为虚拟基础定位的人物IP形象。概念灵感来源于童年的泡泡糖回忆，头带卡通竹蜻蜓鸭舌帽，身穿橘黄色连帽卫衣、黄色短裤，脚穿大头鞋，塑造一个充满淘气外貌和漫画气息、对零食无比喜爱......
### [卖疯了的特斯拉，已经不愿意参加车展了](https://finance.sina.com.cn/china/2022-09-12/doc-imqqsmrn8790490.shtml)
> 概要: 转自：中国新闻周刊 最快的扩张速度 不把自己当车企的特斯拉 为啥能“火”？ 刚刚过去的8月，缺席成都车展却去参加了服贸会的特斯拉，又交出一个惊人的销量数字。
### [任天堂《斯普拉遁3》发售3天日本销量突破345万份创纪录](https://news.dmzj.com/article/75537.html)
> 概要: 任天堂宣布《斯普拉遁3》（又名：Splatoon 3，喷射战士3）发售3天日本销量突破345万份，刷新系列记录，同时创下了日本首发3天的Switch软件最高销量记录。
### [意想不到！抖音今年最赚钱的主播竟然是他…](https://www.tuicool.com/articles/NJRZFja)
> 概要: 意想不到！抖音今年最赚钱的主播竟然是他…
### [上市仅三天 《斯普拉遁3》日本市场销量突破345万套](https://www.3dmgame.com/news/202209/3851451.html)
> 概要: 任天堂今日宣布，新作《斯普拉遁3》上市仅三天，在日本国内的销量就已经突破345万套。这也是迄今为止日本首发三天销量最高的NS平台游戏。由于这只统计到日本销量，因此《斯普拉遁3》的全球销量还要更高一些......
### [TV动画《不道德公会》新PV与主视觉图公开 10月5日播出](https://news.dmzj.com/article/75538.html)
> 概要: TV动画《不道德公会》新PV与主视觉图公开，2022年10月5日播出。
### [初创公司的战略真的对吗？从“内容创业”到“内容产品”创业攻略](https://www.tuicool.com/articles/Qf6ZJ3I)
> 概要: 初创公司的战略真的对吗？从“内容创业”到“内容产品”创业攻略
### [黄嘉千现身打离婚官司 律师代夏克立念给家人的信](https://ent.sina.com.cn/s/h/2022-09-12/doc-imqmmtha7018652.shtml)
> 概要: 新浪娱乐讯 据媒体报道，艺人黄嘉千与夏克立跨国婚姻走向尽头，双方也透过法律将终结16年婚姻。士林地院家事法庭排定今（12日）开庭，黄嘉千身穿黑底洋装与律师赖芳玉一同出庭，面对媒体提问则淡淡以“谢谢关心......
### [本周“超声波第一股”等10新股齐发，“潜力”爆棚？](https://www.yicai.com/news/101532757.html)
> 概要: 本周“超声波第一股”等10新股齐发，“潜力”爆棚？
### [扎波罗热核电站完全停运！](https://finance.sina.com.cn/china/2022-09-12/doc-imqqsmrn8799192.shtml)
> 概要: 澎湃新闻记者 南博一 当地时间9月11日，乌克兰国家核电公司在社交媒体发布消息，称扎波罗热核电站正在运行的最后一台动力机组中断与乌国家电网连接...
### [“煤飞色舞”，地产爆发！继续上车还是另有主线？](https://www.yicai.com/news/101533976.html)
> 概要: “煤飞色舞”，地产爆发！继续上车还是另有主线？
### [“超长西瓜”出圈，短视频特效生意还能走多远？](https://www.tuicool.com/articles/nUF7V3e)
> 概要: “超长西瓜”出圈，短视频特效生意还能走多远？
### [鹅绒更保暖，雪中飞连帽羽绒服 199 元清仓（减 340 元）](https://lapin.ithome.com/html/digi/640443.htm)
> 概要: 反季清仓：雪中飞男士白鹅绒连帽羽绒服报价 539 元，限时限量 340 元券，实付 199 元包邮，领券并购买。使用最会买 App下单，预计还能再返 21.89 元，返后 177.11 元包邮，点击下......
### [王者荣耀：挑战者杯小组赛9月13日前瞻推荐比分预测](https://bbs.hupu.com/55515221.html)
> 概要: 17:00   GOG  vs  EMC  A组GOG于8月28日KGL夏季赛总决赛遭EMC让三追四，输掉比赛。巅峰对决中emc队伍状态拉满，11分38秒上高团双方蒙恬位于阵前，GOG辉辉的蒙恬先被打
### [杨幂发文谈生日愿望：对得起自己 对得起时光](https://ent.sina.com.cn/s/m/2022-09-12/doc-imqqsmrn8809836.shtml)
> 概要: 新浪娱乐讯 9月12日是杨幂36岁生日，晚间，她在微博发文谈生日愿望：“对得起自己，对得起时光，本命年加油！”并谢谢大家的生日祝福。粉丝纷纷热情留言为其庆生。(责编：小5)......
### [天然气短缺，法国酸奶厂或停产、德国民众囤积电暖器......俄罗斯：俄方不会亏本供应石油天然气](https://finance.sina.com.cn/china/2022-09-12/doc-imqmmtha7033639.shtml)
> 概要: 每经编辑 张锦河 据央视新闻12日消息，由于冬季可能面临天然气短缺，法国酸奶业或遭受重创。对大多数酸奶厂商而言，天然气供应中断意味着无法生产。
### [连花清瘟三重优势抗击新冠肺炎](http://www.investorscn.com/2022/09/12/102905/)
> 概要: 目前，多地出现疫情，提醒大家防疫之弦不能放松，除了做好日常防护外，对于每个家庭来说，备好对症药物，以备不时之需，尤为关键。国务院应对新冠肺炎联防联控机制科研攻关组曾筛选出有效防治新冠肺炎的“三方三药”......
### [提醒！明日北京早高峰将迎严重拥堵，交通压力堪比节前](https://finance.sina.com.cn/china/2022-09-12/doc-imqmmtha7035233.shtml)
> 概要: 明天（9月13日），星期二，中秋假期后第一个工作日，机动车限行尾号3和8，预计早晚高峰时段受通勤、通学出行集中影响，交通压力突出，交通指数峰值或达8...
### [英特尔初步确认：第 13 代酷睿 Raptor Lake i9-13900KS 芯片高达 6GHz，还有 8GHz 超频](https://www.ithome.com/0/640/451.htm)
> 概要: 感谢IT之家网友华南吴彦祖、OC_Formula的线索投递！IT之家9 月 12 日消息，根据泄露的报告，英特尔第 13 代 Raptor Lake 发布即将到来，因此，大量信息正在四处传播。据称有关......
### [流言板挺进8强！丹尼斯-施罗德晒照庆祝德国队取得胜利](https://bbs.hupu.com/55516359.html)
> 概要: 虎扑09月12日讯 丹尼斯-施罗德更新Ins，晒出一组赛场照庆祝德国队挺进8强。“四分之一决赛🇩🇪💪🏀！”丹尼斯-施罗德附文道。在昨日德国以85-79击败黑山的比赛中，丹尼斯-施罗德得到了22
### [中秋出游成今年小长假最火：“月色经济”等多元消费成新趋势](https://finance.sina.com.cn/jjxw/2022-09-12/doc-imqmmtha7039155.shtml)
> 概要: 转自：扬子晚报网2022年9月11日，安徽省铜陵市，市民在游玩合影。中新社发 陈晨 摄 这个中秋小长假，周边游火热。跨界融合的各种新玩法成为旅游消费的新增长点。
### [流言板媒体评未来名人堂球员：现役球员可能入选组，罗斯是例外](https://bbs.hupu.com/55516433.html)
> 概要: 虎扑09月12日讯 近日，The Athletic记者Rob Peterson撰文展望未来的名人堂球员（每个类别内排名均不分先后）。现役球员也许可以入选：拉马库斯-阿尔德里奇、布拉德利-比尔、布雷克-
### [美国牵头搞IPEF，印度为何不加入四支柱中“贸易谈判”？](https://www.yicai.com/news/101534031.html)
> 概要: “像印度是轻易不会松口的。”
### [管涛：美联储还会犯“错”吗？︱汇海观涛](https://www.yicai.com/news/101534047.html)
> 概要: 这次，美联储准备容忍经济衰退，会不会又是大衰退呢？果真如此，则美国经济和市场尚难言底。
### [Twitter回应马斯克终止交易 称其没有违反交易义务](https://www.3dmgame.com/news/202209/3851461.html)
> 概要: Twitter公司周一表示，向一名举报人支付的款项没有违反其被埃隆·马斯克以440亿美元收购的任何条款，此前世界首富将此举作为取消交易的另一个理由。马斯克的律师在周五给Twitter的信中说，Twit......
# 小说
### [鸿蒙圣座](https://book.zongheng.com/book/327064.html)
> 作者：巫山观海

> 标签：武侠仙侠

> 简介：三千大千世界，十万中天世界，无数小世界，合为一体，是为诸天万界。天地之间，共圣座四十有九，得之可成圣，四十八圣人已归位，独余最后一张鸿蒙圣座。重回万年前，前世的遗憾，我要尽皆弥补，前世的美好，我要紧紧守护，最重要的是，踏上鸿蒙圣座，成就万古圣人，无灾无劫，不死不灭。

> 章节末：第六百七十七章  亿万宇宙（大结局）

> 状态：完本
# 论文
### [Efficient implementation of incremental proximal-point methods | Papers With Code](https://paperswithcode.com/paper/efficient-implementation-of-incremental)
> 概要: Model training algorithms which observe a small portion of the training set in each computational step are ubiquitous in practical machine learning, and include both stochastic and online optimization methods. In the vast majority of cases, such algorithms typically observe the training samples via the gradients of the cost functions the samples incur. Thus, these methods exploit are the \emph{slope} of the cost functions via their first-order approximations. To address limitations of gradient-based methods, such as sensitivity to step-size choice in the stochastic setting, or inability to exploit small function variability in the online setting, several streams of research attempt to exploit more information about the cost functions than just their gradients via the well-known proximal framework of optimization. However, implementing such methods in practice poses a challenge, since each iteration step boils down to computing a proximal operator, which may not be easy. In this work we provide efficient algorithms and corresponding implementations of proximal operators in order to make experimentation with incremental proximal optimization algorithms accessible to a larger audience of researchers and practitioners, and in particular to promote additional theoretical research into these methods by closing the gap between their theoretical description in research papers and their use in practice. The corresponding code is published at https://github.com/alexshtf/inc_prox_pt.
### [GLEAN: Generative Latent Bank for Image Super-Resolution and Beyond | Papers With Code](https://paperswithcode.com/paper/glean-generative-latent-bank-for-image-super)
> 日期：29 Jul 2022

> 标签：None

> 代码：https://github.com/open-mmlab/mmediting

> 描述：We show that pre-trained Generative Adversarial Networks (GANs) such as StyleGAN and BigGAN can be used as a latent bank to improve the performance of image super-resolution. While most existing perceptual-oriented approaches attempt to generate realistic outputs through learning with adversarial loss, our method, Generative LatEnt bANk (GLEAN), goes beyond existing practices by directly leveraging rich and diverse priors encapsulated in a pre-trained GAN. But unlike prevalent GAN inversion methods that require expensive image-specific optimization at runtime, our approach only needs a single forward pass for restoration. GLEAN can be easily incorporated in a simple encoder-bank-decoder architecture with multi-resolution skip connections. Employing priors from different generative models allows GLEAN to be applied to diverse categories (\eg~human faces, cats, buildings, and cars). We further present a lightweight version of GLEAN, named LightGLEAN, which retains only the critical components in GLEAN. Notably, LightGLEAN consists of only 21% of parameters and 35% of FLOPs while achieving comparable image quality. We extend our method to different tasks including image colorization and blind image restoration, and extensive experiments show that our proposed models perform favorably in comparison to existing methods. Codes and models are available at https://github.com/open-mmlab/mmediting.
