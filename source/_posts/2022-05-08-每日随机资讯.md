---
title: 2022-05-08-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MomJoey_ZH-CN1642006600_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=40
date: 2022-05-08 23:09:07
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MomJoey_ZH-CN1642006600_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=40)
# 新闻
### [GAN 之父、苹果机器学习总监 Ian Goodfellow 不满复工政策，选择辞职](https://www.oschina.net/news/194745/apple-director-of-machine-learning-resigns)
> 概要: 外媒 The Verge 科技记者Zoë Schiffer爆料称，苹果机器学习总监 Ian Goodfellow 因不满公司的复工政策而离开公司。他在给员工的一份说明中说道：“我坚信，对我的团队来说，......
### [Goscript：Rust 实现的 Go 语言规范](https://www.oschina.net/news/194738/goscript-wasm)
> 概要: Anaconda 开发商最近发布了PyScript，这是一个可以让开发者在 HTML 中混写 Python 代码的框架，有人将其称为“Python 版 JSP”。事实上，PyScript 底层采用了 ......
### [Spring Boot Admin 2.6.7 发布，Spring Boot 应用程序的管理 UI](https://www.oschina.net/news/194727/spring-boot-admin-2-6-7-released)
> 概要: Spring Boot Admin 是一个用于管理 Spring Boot 应用程序的管理界面，Spring Boot Admin 2.6.7 正式发布，该版本中的变化包括：4fb3c4c6- 将 S......
### [GCC 12.1 已发布](https://www.oschina.net/news/194724/gcc-12-released)
> 概要: 五月是 GCC 1.0 发布的 35 周年，同时 GCC 12.1 也已正式发布，GCC 12.1 是一个重要的功能版本，包含新的 C/C++ 功能到 Fortran 和 Ada 等其他语言的持续工作......
### [所有人的归宿都是带货？刘畊宏也逃不过！](http://www.woshipm.com/it/5428561.html)
> 概要: 编辑导语：如今，各平台的广告层出不穷，广告的花样也十分多样化。其本质其实还是为了带货。近期，“健身狂魔”刘畊宏也走上了“带货”的道路。难道所有人的归宿都是带货吗？本篇文章中作者结合实际经验围绕“带货”......
### [学管理必须知道的七项原则！](http://www.woshipm.com/pd/5406910.html)
> 概要: 编辑导语：如何才能做好产品管理，并在持续的产品优化迭代之旅中，保证产品设计不脱离产品愿景和产品策略？也许，你可以看看这篇文章。本篇文章里，作者就总结了产品管理过程中可借鉴参考的七条原则，一起来看一下吧......
### [内容社区困住小红书](http://www.woshipm.com/it/5428557.html)
> 概要: 编辑导语：如今，“网红经济”已经发展成一种势不可挡的互联网风口，越来越多的人选择成为职业网红。然而在快速发展的背景下，网红经济的弊端也无法掩盖。本文以小红书为例，分析内容社区目前的困境，一起来看一下吧......
### [花13元就能改IP属地？IP代理浮出水面](https://finance.sina.com.cn/tech/2022-05-08/doc-imcwiwst6156523.shtml)
> 概要: 海外博主的IP归属地却在国内，各省吃喝玩乐博主的IP归属地都在湖南……随着IP归属地功能的上新，在各网红大V惨遭翻车的同时，IP代改业务也随之出现，北京青年报记者发现甚至最低花费13元就可以更改IP归......
### [多地出台外卖员防疫新规](https://finance.sina.com.cn/tech/2022-05-08/doc-imcwipii8637038.shtml)
> 概要: 中新财经5月8日电(记者 吴涛)近日，北京市有关部门通报了一例外卖配送员感染新冠病毒的情况，至检测阳性为止，该外卖员接了500余单，涉及38家餐馆。不少人担心，疫情下，还能收快递、点外卖吗......
### [网红猎手雷彬艺：寻找下一个刘畊宏](https://finance.sina.com.cn/tech/2022-05-08/doc-imcwiwst6184427.shtml)
> 概要: 来源：经济参考报......
### [首次开源这一强大算法，我们向Meta致敬](https://www.huxiu.com/article/549065.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自the VergeFacebook改名Meta后，“财务厄运”并未因此终止，但技术作风却一如既往的大胆。虽然自2022年2月以来，公司股价已下跌30% ，市值损......
### [Web Development for Beginners – A Curriculum](https://github.com/microsoft/Web-Dev-For-Beginners)
> 概要: Web Development for Beginners – A Curriculum
### [你可以给游戏库分类了！Epic加入过滤器功能](https://www.3dmgame.com/news/202205/3841909.html)
> 概要: Epic商城的功能正在缓慢地落地，Epic游戏商城进化的速度比我们预想地慢了很多。Epic不再大张旗鼓地宣传加入的每个新功能，但Epic商城用户当然要知道每个功能性更新。最近的更新已经上线，包括为Ep......
### [吉利李书福欲拿下魅族打造机车“超级协同”，或已进入交割阶段](https://finance.sina.com.cn/tech/2022-05-08/doc-imcwiwst6207211.shtml)
> 概要: 张兴旺 中国证券报......
### [《F1 22》新预告展示迈阿密国际赛道预热场景](https://www.3dmgame.com/news/202205/3841913.html)
> 概要: Codemasters新作《F1 22》今日再曝最新预告片，展示迈阿密国际赛道的场景，这也是该系列游戏将于今年加入的最新赛道。在之前的预告片中，我们看到的主要是各种弯道和DRS区域，而今天这款预告片的......
### [动画「Love All Play」追加声优鸟海浩辅、泰勇気](http://acg.178.com/202205/445976660223.html)
> 概要: 羽毛球题材动画「Love All Play」公布了两位新追加的声优——鸟海浩辅（伊达晴臣 役）和泰勇気（宫城莲 役）。本作现正在热播中。CAST水嶋亮：花江夏树榊翔平：阿座上洋平松田航輝：古川慎東山太......
### [杂志「anan」最新「间谍过家家」背面封面公开](http://acg.178.com/202205/445977991317.html)
> 概要: 动画「间谍过家家」在杂志「anan」最新一期上的背面封面图公开，绘制的是男主角劳埃德·福杰。本期杂志将收录的内容包括声优访谈、制片人对谈，以及OP&ED主题曲演唱者Official髭男dism和星野源......
### [汪小菲回应开饭店赔本：解封后还有5家店要开业](https://ent.sina.com.cn/s/m/2022-05-08/doc-imcwiwst6227292.shtml)
> 概要: 新浪娱乐讯 近日，有媒体爆料汪小菲在直播中声泪俱下，感叹目前自己在当下的状态下太难了，还称汪小菲投资了1个亿开了3家店都成了赔本买卖。　　面对种种传闻，汪小菲在5月7日晚发文回应，否认餐饮店经营惨淡一......
### [漫画「吸血鬼马上死」第21卷封面公开](http://acg.178.com/202205/445979818871.html)
> 概要: 漫画「吸血鬼马上死」公开了最新卷第21卷的封面图，该卷将于2022年6月8日发售。「吸血鬼马上死」（吸血鬼すぐ死ぬ）是盆之木至创作的漫画作品，在「周刊少年Champion」2015年30号上开始连载......
### [「咒术回战」乙骨忧太可动手办开订](http://acg.178.com/202205/445981085762.html)
> 概要: MEGAHOUSE作品「咒术回战」乙骨忧太可动手办开订，全长约150mm（含底座），主体采用ABS和PVC材料制造。该手办定价为6600日元（含税），约合人民币337元，预计于2022年10月发售......
### [居家办公，我被公司用摄像头“盯梢”](https://www.huxiu.com/article/549228.html)
> 概要: 燃次元（ID:chaintruth）原创，作者：马舒叶、曹杨、谢中秀、陶淘、余超婧、孔月昕、吕敬之、张琳，编辑：曹杨，题图来自：视觉中国居家办公，或已成为这届打工人的常态。“起床就能办公”、“充分节省......
### [窃取超级计算机Dojo机密技术、拿「假」电脑糊弄检查，特斯拉将前工程师告上法庭](https://www.tuicool.com/articles/3maMzu7)
> 概要: 窃取超级计算机Dojo机密技术、拿「假」电脑糊弄检查，特斯拉将前工程师告上法庭
### [《即使如此依旧步步进逼》OP、ED分别由花泽香菜和中村栞奈担当](https://news.dmzj.com/article/74344.html)
> 概要: 根据山本崇一朗原作改编的TV动画《即使如此依旧步步进逼》的OP和ED日前发表。
### [TV动画《间谍过家家》公开新声优](https://news.dmzj.com/article/74345.html)
> 概要: 改编自远藤达哉原作的TV动画《SPY×FAMILY》新增加藤原夏海、加藤英美里、佐藤花、冈村明香出演。
### [FuRyu《在魔王城中说晚安》恶魔熊玩偶开始预约](https://news.dmzj.com/article/74346.html)
> 概要: FuRyu《在魔王城中说晚安》恶魔熊玩偶开始预约
### [《极主夫道》真人电影新剧照公开 6月3日上映](https://www.3dmgame.com/news/202205/3841925.html)
> 概要: 根据人气漫画改编，曾被网飞制作成电视剧以及动画，《极主夫道》真人电影即将于6月3日上映，5月8日今天是母亲节，官方公开了最新剧照，展示了作品中的向日葵的“母亲“美久的场景，一起来先睹为快。•经典漫画原......
### [古·彩](https://www.zcool.com.cn/work/ZNTk2NzUwNDA=.html)
> 概要: 古·彩
### [Take-Two CEO个人奖金与旗下游戏微交易收入挂钩](https://www.3dmgame.com/news/202205/3841926.html)
> 概要: 像《GTAOL》里面的微交易内容已经成为当前电子游戏厂商的重要商业模式，而且短期内没有要改变的迹象。根据一份续约合同细节显示，如果玩家在游戏里买道具、货币和皮肤所花的钱越多，那么Take-Two CE......
### [大厂中年人新流行：精准逃顶，下场创业](https://www.huxiu.com/article/549214.html)
> 概要: 本文来自微信公众号：投中网 （ID：China-Venture），作者：张雪，编辑：张丽娟，头图来自：视觉中国王龙刚出来创业，就在短短半年内拿到了两轮来自一线机构的融资。但回过头看，一方面是因为他自己......
### [Show HN: Thanks to BeAware, Deaf iPhone users have a free alerting device (OSS)](https://www.deafassistant.com/)
> 概要: Show HN: Thanks to BeAware, Deaf iPhone users have a free alerting device (OSS)
### [视频：老公带儿子畅游海洋公园 胡杏儿母亲节前夕隔空告白](https://video.sina.com.cn/p/ent/2022-05-08/detail-imcwiwst6270550.d.html)
> 概要: 视频：老公带儿子畅游海洋公园 胡杏儿母亲节前夕隔空告白
### [砍权益、送软件，小鹏的促销你学不会](https://www.huxiu.com/article/548367.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 视觉中国“免费的大餐要中午吃，因为早晚会凉。”5月6日，小鹏汽车官方微信发布消息称，综合考虑疫情及供应链等外部因素影响，将于2022年5月9......
### [曝一加 Ace 新机本月发布：搭载天玑 8100 与 LCD 高刷屏，未来还将推 K 系列手机](https://www.ithome.com/0/617/256.htm)
> 概要: 感谢IT之家网友蓝海岸Nibiru、迷一样的菠萝、幻雪之影的线索投递！IT之家5 月 8 日消息，一加近日入网了一款型号为 PGZ110 的新机，搭载天玑 8100，采用 6.59 英寸 FHD + ......
### [视频：杨幂罕见谈及家人之间关系 称更看重彼此独立性](https://video.sina.com.cn/p/ent/2022-05-08/detail-imcwiwst6272409.d.html)
> 概要: 视频：杨幂罕见谈及家人之间关系 称更看重彼此独立性
### [上海快递第一批复工复产“白名单”公布](https://finance.sina.com.cn/china/2022-05-08/doc-imcwipii8723149.shtml)
> 概要: 据上海市邮政管理局消息，当前，本市疫情防控工作正处于“逆水行舟、不进则退”的关键时期和吃劲阶段。本市主要品牌企业分拣中心已陆续投入运营...
### [全球半导体头部设备商高管：芯片设备也开始出现“缺芯”现象](https://www.ithome.com/0/617/262.htm)
> 概要: 据《华尔街日报》5 月 6 日报道，全球半导体头部设备商高管最近都表示，芯片设备也开始出现“缺芯”现象。最近几个月，用于芯片制造的机器设备需要更长时间的交付期。在疫情初期，从提交订单到收到设备需要几个......
### [【Python】新华字典 (bushi](https://www.tuicool.com/articles/IZzYVfA)
> 概要: 【Python】新华字典 (bushi
### [5月8日这些公告有看头](https://www.yicai.com/news/101405307.html)
> 概要: 5月8日晚间，多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [星巴克销售额下降23%！为什么年轻人看不上星巴克了？](https://finance.sina.com.cn/china/gncj/2022-05-08/doc-imcwiwst6282056.shtml)
> 概要: 来源：咖啡工房 美国当地时间5月3日，星巴克发布了（截至2022年4月3日为期13周的）2022财年第二季度关键财务数据中显示：星巴克海外市场业绩惨淡...
### [李强在杨浦检查疫情防控工作，要求以更大决心更果断措施狠抓攻坚突破、拔点清面](https://finance.sina.com.cn/china/2022-05-08/doc-imcwiwst6281153.shtml)
> 概要: 今天（5月8日），市委书记李强在杨浦区检查疫情防控攻坚行动推进情况时指出，落实疫情防控军令状，关键是把各项防控工作做到底、做到位...
### [本轮疫情累报727例本土感染者 一图看懂北京疫情现状](https://finance.sina.com.cn/china/2022-05-08/doc-imcwipii8727310.shtml)
> 概要: 4月22日至5月8日15时，北京市累计报告727例本土感染者。各区有多少病例？北京市现有多少个中高风险地区，分布在哪些区？现有多少个封控区、管控区？
### [不良人：破案了，为什么不良帅那么强悍大唐还是覆灭了，原来这样](https://new.qq.com/omn/20220508/20220508A080WC00.html)
> 概要: 印象中距离《画江湖之不良人》第五季完结已经有快一个月了，就在我都快淡忘了这部作品的时候，官方授权的不良人手游号却突然开始频繁的活跃了。这里可能有人会说，不良人动漫跟不良人手游有关系？表面看没啥关系，我......
### [大厂员工被高薪宠坏了？](https://www.tuicool.com/articles/Qrq2Ina)
> 概要: 大厂员工被高薪宠坏了？
### [一篇带给你Web前端算法面试题](https://www.tuicool.com/articles/fia6Rzm)
> 概要: 一篇带给你Web前端算法面试题
### [【若鸿小剧场第390期】愿所有母亲，节日快乐！](https://new.qq.com/omn/20220508/20220508A08U5X00.html)
> 概要: 这糟老头坏得很啊武神主宰第230集预告                                    时隔多年，再度联手！无上神帝第168集预告                        ......
### [Show HN: Prof Simon Wright Captain Future – In Your Raspberry Pi](https://dck-one.medium.com/professor-simon-wright-726a7003b226)
> 概要: Show HN: Prof Simon Wright Captain Future – In Your Raspberry Pi
### [TCL 直播称美的空调“连个风扇都不如”被判诋毁和虚假宣传，赔 30 万元](https://www.ithome.com/0/617/277.htm)
> 概要: 感谢IT之家网友蓝海岸Nibiru的线索投递！IT之家5 月 8 日消息，最高人民法院主管的《人民法院报》5 月 6 日刊发禅城法院案例《直播中片面比对同类商品构成商业诋毁 ——TCL 中山公司被判赔......
### [美股抛售潮愈演愈烈，这个数据会成市场转折点吗](https://www.yicai.com/news/101405369.html)
> 概要: 华尔街又经历了动荡的五个交易日。
### [两大中药企业“联姻”，华润三九拟29亿元“吃下”昆药集团](https://www.yicai.com/news/101405359.html)
> 概要: 两大中药企业之间的“联姻”，可以说是一场各取所需的交易。
### [上海市邮政快递业第一批复工复产“白名单”发布](https://finance.sina.com.cn/china/2022-05-08/doc-imcwipii8740123.shtml)
> 概要: 来源：国际金融报 今天，上海市邮政管理局发布了上海市邮政快递业第一批复工复产“白名单”。市邮管局表示，本市主要品牌企业分拣中心已陆续投入运营...
### [海贼王1016集：路飞比肩罗杰，索隆一刀斩杀普罗米修斯，强悍](https://new.qq.com/omn/20220508/20220508A09AZC00.html)
> 概要: 海贼王1016集已经更新，这一集的内容很精彩，和之国战争已经进入高潮部分，超新星已经来到鬼岛顶层，他们要与四皇决战了。九侠在对战凯多的战斗中失败了，他们完全不是凯多的对手，武士的力量也是不堪一击，即使......
### [Wealthy Americans are buying second passports as a 'plan B'](https://www.businessinsider.com/wealthy-americans-buy-second-passports-amid-covid-politics-climate-change-2022-5)
> 概要: Wealthy Americans are buying second passports as a 'plan B'
### [Take Two将在一周后公布2022财年财报](https://www.3dmgame.com/news/202205/3841942.html)
> 概要: Take Two将在一周后公布2022财年第四季度和2022整个财年的业绩。值得一提的是，根据今年2月的展示，Take Two还要在这次的财报中公布2022财年到2024财年发售游戏阵容（部分）。或许......
### [上海为企业开辟紧急复工通道！关键岗位可“点式复工”](https://www.yicai.com/news/101405419.html)
> 概要: “点式复工是一个很宝贵的通道，类似 ‘120 ’通道一样，是给企业救急用的。”
### [博人传248集：继神乐之后，又一位“新忍刀七人众”的成员下线了](https://new.qq.com/omn/20220508/20220508A09YGY00.html)
> 概要: 在《博人传》动画此前的剧情中，雾隐村的天才小强神乐已经领便当下线了。虽然神乐只是动画组原创的角色，但他的下线还是引起了很多观众的讨论，毕竟他的人气还是挺高的，不但是天才小强，而且还是雾隐村下一代水影的......
### [AAA 级抑菌，网易严选精梳棉平角裤 4 条 59 元（京东 78 元）](https://lapin.ithome.com/html/digi/617286.htm)
> 概要: 【网易严选旗舰店】AAA 级抑菌，网易严选男士精梳棉平角裤 4 条报价 89 元，限时限量 30 元券，实付 59 元包邮，领券并购买。可选精梳棉 4 条装或者莫代尔 3 条装。使用最会买 App下单......
### [史上最贵！美国财团收购英超切尔西俱乐部](https://www.yicai.com/news/101405427.html)
> 概要: 阿布出局。
### [女生19岁开户弘业期货血亏835万，13年后法院重审这样判](https://www.yicai.com/news/101405429.html)
> 概要: 仅获赔167万
### [力保供应链产业链稳定运转！上海市邮政快递业首批复工复产白名单发布](https://finance.sina.com.cn/china/2022-05-08/doc-imcwipii8751617.shtml)
> 概要: 力保供应链产业链稳定运转！上海市邮政快递业首批复工复产“白名单”发布 宋薇萍 严曦梦 随着全国互认通行证的陆续发放、快递企业被纳入“白名单”...
# 小说
### [最强提取系统](http://book.zongheng.com/book/1071147.html)
> 作者：我是一个小村娃

> 标签：奇幻玄幻

> 简介：普通人生活在小世界中，各大宗门为了庇佑人族将人放在小世界中，从中招收弟子，回到外界对抗妖兽。外界更是凶残，妖兽横行无人能够制衡。是一方真正的修行大世界，人类与妖兽各占半壁江山，将整个真武大陆瓜分开来。在真武大陆以后，还有一处仙界，为何真武大陆上妖兽与人类势如水火。就是因为那个仙界出现了问题，倒是人族与妖族大战。持续了几万年，战火纷飞，人们苦不堪言。

> 章节末：第六百五十九章 大结局

> 状态：完本
# 论文
### [NeuS: Neutral Multi-News Summarization for Mitigating Framing Bias | Papers With Code](https://paperswithcode.com/paper/neus-neutral-multi-news-summarization-for-1)
> 日期：11 Apr 2022

> 标签：None

> 代码：None

> 描述：Media framing bias can lead to increased political polarization, and thus, the need for automatic mitigation methods is growing. We propose a new task, a neutral summary generation from multiple news headlines of the varying political spectrum, to facilitate balanced and unbiased news reading. In this paper, we first collect a new dataset, obtain some insights about framing bias through a case study, and propose a new effective metric and models for the task. Lastly, we conduct experimental analyses to provide insights about remaining challenges and future directions. One of the most interesting observations is that generation models can hallucinate not only factually inaccurate or unverifiable content, but also politically biased content.
### [Local Intensity Order Transformation for Robust Curvilinear Object Segmentation | Papers With Code](https://paperswithcode.com/paper/local-intensity-order-transformation-for)
> 日期：25 Feb 2022

> 标签：None

> 代码：None

> 描述：Segmentation of curvilinear structures is important in many applications, such as retinal blood vessel segmentation for early detection of vessel diseases and pavement crack segmentation for road condition evaluation and maintenance. Currently, deep learning-based methods have achieved impressive performance on these tasks. Yet, most of them mainly focus on finding powerful deep architectures but ignore capturing the inherent curvilinear structure feature (e.g., the curvilinear structure is darker than the context) for a more robust representation. In consequence, the performance usually drops a lot on cross-datasets, which poses great challenges in practice. In this paper, we aim to improve the generalizability by introducing a novel local intensity order transformation (LIOT). Specifically, we transfer a gray-scale image into a contrast-invariant four-channel image based on the intensity order between each pixel and its nearby pixels along with the four (horizontal and vertical) directions. This results in a representation that preserves the inherent characteristic of the curvilinear structure while being robust to contrast changes. Cross-dataset evaluation on three retinal blood vessel segmentation datasets demonstrates that LIOT improves the generalizability of some state-of-the-art methods. Additionally, the cross-dataset evaluation between retinal blood vessel segmentation and pavement crack segmentation shows that LIOT is able to preserve the inherent characteristic of curvilinear structure with large appearance gaps. An implementation of the proposed method is available at https://github.com/TY-Shi/LIOT.
