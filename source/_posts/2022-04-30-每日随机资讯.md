---
title: 2022-04-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WiedehopfElbe_ZH-CN6286311611_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=45
date: 2022-04-30 21:54:24
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WiedehopfElbe_ZH-CN6286311611_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=45)
# 新闻
### [Apache Superset 1.5.0 发布，现代化数据工具](https://www.oschina.net/news/193709/apache-superset-1-5-0-released)
> 概要: Apache Superset 1.5.0 现已发布。Apache Superset 是一款现代化的开源数据工具，用于数据探索和数据可视化。它提供了简单易用的无代码可视化构建器和声称是最先进的 SQL......
### [Linux 5.19 支持经 Zstd 压缩过的固件](https://www.oschina.net/news/193718/zstd-firmware-linux-5-19-next)
> 概要: Linux 5.19 内核即将合并可选的 Zstd 固件压缩支持选项。从透明文件系统压缩到使用 Zstd 压缩内核映像，Linux 内核越来越多地使用 Zstd。谈到对经 Zstd 压缩过的固件的支持......
### [Fedora 36 再度跳票 ，计划在 5 月中旬发布](https://www.oschina.net/news/193717/fedora-36-will-released-on-may-10)
> 概要: Fedora Linux 36 又又又跳票啦！Fedora 36 原定的目标发布日期是 4 月 19 日，最晚日期是 4 月 26 日。现在两个日期都已经过去了，现在定在 5 月 10 日发货（有问题......
### [报告：开发人员每周只有约 10 小时的"深度工作"时间](https://www.oschina.net/news/193720/state-of-engineering-time-2022)
> 概要: 软件工具公司 Retool 最新发布了一份针对 600 名软件工程师和经理的调查报告，就“软件工程师们实际上的时间分配、兴趣所在、认为构建过程中最令人沮丧的部分，以及真正花在编写代码上的时间”等方面进......
### [如何正确判断需求、痛点？如何打造爆款？｜「2022互联网人破圈计划」世界读书日特别活动](http://www.woshipm.com/pd/5416997.html)
> 概要: 当我们天天埋头于繁忙的工作，琐碎的事项，开始变得麻木，感到疲惫时，别忘了，我们还可以静下心来，读一本好书，放松一下自己，在阅读中汲取养分，充实自己。为了迎接4月23日世界读书日，上周「2022互联网人......
### [HMI没入门？看这一篇就够了！](http://www.woshipm.com/pd/5418690.html)
> 概要: 编辑导语：HMI是人机交互的展现形式之一，是人与车之间的信息交互的特定名词，而HMI设计就是为人与车之间的交互带来良好的体验。什么是HMI呢？HMI设计师如何有效的提升人与车之间的交互体验感受呢？本文......
### [“刘畊宏女孩”背后的居家健身，市场潜力有多大？](http://www.woshipm.com/it/5418974.html)
> 概要: 编辑导读：因为疫情居家的你们，最近有没有在跟刘畊宏学习跳操？这位抖音新晋健身教练，在短短七天就增长了千万粉丝，实在令人惊叹。背后的居家健身市场是不是大有可为？本文作者对此进行了分析，一起来看看吧。《数......
### [知网知否？](https://finance.sina.com.cn/tech/2022-04-30/doc-imcwipii7268517.shtml)
> 概要: 本报记者 郑丹 北京报道......
### [国内首例NFT侵权案](https://finance.sina.com.cn/tech/2022-04-30/doc-imcwiwst4826650.shtml)
> 概要: 本报记者 郑瑜 北京报道......
### [三大协会发《倡议》 NFT监管法律空白待解](https://finance.sina.com.cn/tech/2022-04-30/doc-imcwiwst4838125.shtml)
> 概要: 本报记者 李玉洋 李正豪 上海报道......
### [SHEIN凶猛，TikTok下沉](https://www.huxiu.com/article/539587.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜unsplash虎嗅注：从 Shopee、Lazada 到后起之秀 SHEIN、TikTok，新一轮企业出海热潮席卷而来。尤其 RCEP 协定生效后，东南亚与中国......
### [被作家起诉 知网“哭穷”：表示赔不起1200亿](https://www.tuicool.com/articles/ZreM7fE)
> 概要: 被作家起诉 知网“哭穷”：表示赔不起1200亿
### [热搜第一！微信将有新功能，网红可能要“露馅”了！](https://finance.sina.com.cn/tech/2022-04-30/doc-imcwiwst4854224.shtml)
> 概要: 来源：中国经济网......
### [《间谍过家家》今晚9点大陆首播 上线B站腾讯爱奇艺](https://acg.gamersky.com/news/202204/1479734.shtml)
> 概要: TV动画《间谍过家家》将于今日晚9点上线哔哩哔哩、腾讯视频以及爱奇艺平台。
### [「间谍过家家」全新商品插图公开](http://acg.178.com/202204/445284642141.html)
> 概要: 「间谍过家家」公开了新商品的插图，本次绘制的是一家三口——劳埃德、约尔和阿尼亚。电视动画「间谍过家家」改编自远藤达哉创作的同名漫画作品，由WIT STUDIO和CLOVERWORKS负责制作，于202......
### [TV动画「Lycoris Recoil」第一弹PV公布](http://acg.178.com/202204/445284796145.html)
> 概要: TV动画「Lycoris Recoil」的第一弹PV现已公布，本作将于2022年7月播出。TV动画「Lycoris Recoil」第一弹PVSTAFF导演：足立慎吾故事原案：アサウラ角色设计：いみぎむ......
### [《胡闹搬家》发售两周年 破百万销量](https://www.3dmgame.com/news/202204/3841443.html)
> 概要: 近日，物理模拟搬家游戏《胡闹搬家（Moving Out）》发推宣布游戏突破百万销量里程碑，同时本作还迎来了发售两周年，游戏支持简体中文，感兴趣的玩家点击此处进入商店页面。《胡闹搬家》是一款无厘头的物理......
### [TV动画「白领羽球部」公开Blu-ray全四卷封面使用插图](http://acg.178.com/202204/445287681377.html)
> 概要: 近日，TV动画「白领羽球部」公开了Blu-ray全四卷的封面使用插图，各卷详细信息如下。「第一卷」发售日：2022年4月27日售价：9,900日元（含税）收录内容：第1-3话封入特典：使用角色设计·ま......
### [多图直击|一财记者带逛巴菲特股东大会：参会需出具新冠疫苗接种证明、展会规模缩小](https://www.yicai.com/news/101399045.html)
> 概要: 大会要求所有与会者出具新冠疫苗接种证明，参会者可通过登录CLEAR系统，上传疫苗接种记录，获取数字疫苗卡。
### [动画电影「奇奇与蒂蒂：救援突击队」全新海报公开](http://acg.178.com/202204/445298449990.html)
> 概要: 动画电影「奇奇与蒂蒂：救援突击队」发布了全新海报，影片采用了真人&动画结合的方式，将于5月20日上线Disney+播出。约翰·木兰尼、安迪·萨姆伯格、琪琪·莱恩等将参与本作配音，另外「美女与野兽」中的......
### [用动作捕捉当草稿！漫画《没有载入魔法史的伟人》开始连载](https://news.dmzj.com/article/74298.html)
> 概要: 由《魔王学院的不适任者 ～史上最强的魔王始祖，转生就读子孙们的学校～》的原作者秋担任原作，外野负责作画的漫画《没有载入魔法史的伟人 ～因为被认为是没用的研究而被魔法省解雇，结果独占了新魔法的权利～》开始连载。
### [SOL Internatinal《小林家的龙女仆》康娜1/6比例手办开订](https://news.dmzj.com/article/74299.html)
> 概要: SOL Internatinal根据《小林家的龙女仆》中的康娜制作的“猫龙”版1/6比例手办目前已经开订了。本作再现了康娜变成有猫耳猫尾和猫爪时的样子，可以和之前发售的托尔猫龙版手办装饰在一起。
### [百万主播的流变：起步于九堡，星散向全国｜互联网变局](https://www.tuicool.com/articles/zANze2v)
> 概要: 百万主播的流变：起步于九堡，星散向全国｜互联网变局
### [原创动画《Lycoris Recoil》公开PV](https://news.dmzj.com/article/74300.html)
> 概要: 原创TV动画《Lycoris Recoil》公开了第一弹PV。在这次的PV中，首次公开了动画本篇的片段。另外，本作第一话至第三话的先行上映会，也将于6月12日举办。
### [图片故事：摄影记者镜头下的方舱生活](https://www.yicai.com/image/101399088.html)
> 概要: None
### [Beginner-friendly guide to use Tailwind CSS with Jekyll](https://mzrn.sh/2022/04/09/starting-a-blank-jekyll-site-with-tailwind-css-in-2022/)
> 概要: Beginner-friendly guide to use Tailwind CSS with Jekyll
### [PS Plus高级会员复古游戏泄露 昔日经典陆续回归](https://www.3dmgame.com/news/202204/3841457.html)
> 概要: 索尼的重制版PS Plus已经近在眼前了，但到目前玩家仍然有很多东西没弄明白，特别是高等级订阅会员的游戏库范围最是受人关注。虽然索尼官方的口风很紧，但面向高级会员的首批复古游戏已经开始在PSN上泄露......
### [单月暴跌近14%，美国科技股危险了？](https://www.huxiu.com/article/544007.html)
> 概要: 本文来自微信公众号：华尔街见闻 （ID：wallstreetcn），作者：韩旭阳，编辑：位宇祥，原文标题：《单月暴跌近14%！美国科技股上一次这么跌的时候雷曼刚倒闭》，头图来自：《大空头》剧照美国科技......
### [组图：Lisa金发碎花裙春日随拍好甜酷 戴黑色棒球帽元气十足](http://slide.ent.sina.com.cn/star/slide_4_86512_369105.html)
> 概要: 组图：Lisa金发碎花裙春日随拍好甜酷 戴黑色棒球帽元气十足
### [《蜡笔小新 我与博士的暑假》实机公布 5月4日上线](https://www.3dmgame.com/news/202204/3841459.html)
> 概要: 今日（4月30日），油管博主“巴哈姆特电玩疯”公布一段NS中文版《蜡笔小新 我与博士的暑假 ～永不结束的七日之旅～》实机演示视频，该作将于5月4日推出亚洲版，支持中文语音（台配和港配）、韩文语音、大陆......
### [《哥谭骑士》过审评级 或仅登陆本世代主机](https://www.3dmgame.com/news/202204/3841461.html)
> 概要: 游戏博主“Idle Sloth”消息，华纳游戏《哥谭骑士》已在我国台湾省通过审核并被评为15+的分级，但据审核文件显示，《哥谭骑士》仅标记为PS5 和 Xbox Series X|S版，该作预计今年1......
### [精致露营风，吹鼓了谁的钱包？](https://www.huxiu.com/article/544048.html)
> 概要: 出品 | 虎嗅商业消费组作者 | 周月明题图 | 视觉中国露营，成为了五一高频词。“一地难求”“一营难购”是很多消费者遇到的烦恼。来自出行及旅游平台的数据显示，从4月的清明假期开始，围绕露营地、露营装......
### [广州疫情最新通报：新增39例本土阳性感染者，对进出白云机场高风险人群赋红码](https://www.yicai.com/news/101399141.html)
> 概要: 截至目前，本次疫情累计报告感染者63例，均收治在广州医科大学附属市八医院，无重症和危重症感染者，目前病情稳定。
### [Zaplib: A Startup Post Mortem](https://zaplib.com/docs/blog_post_mortem.html)
> 概要: Zaplib: A Startup Post Mortem
### [英伟达 H100“Hopper”卡现已上市：配备 80GB 显存，超 24 万元](https://www.ithome.com/0/616/136.htm)
> 概要: IT之家4 月 30 日消息，Hermitage Akhiabara 报道称，日本首家 HPC 零售商以 4745950 日元（约 24.16 万元人民币）的高价出售 NVIDIA 最新的 Hoppe......
### [视频：五一假期首日票房已破千万 北京全市影院暂时关闭](https://video.sina.com.cn/p/ent/2022-04-30/detail-imcwipii7381358.d.html)
> 概要: 视频：五一假期首日票房已破千万 北京全市影院暂时关闭
### [组图：刘雯分享初夏街头写真 着碎花裙搭牛仔裤时尚前卫](http://slide.ent.sina.com.cn/star/slide_4_86512_369108.html)
> 概要: 组图：刘雯分享初夏街头写真 着碎花裙搭牛仔裤时尚前卫
### [村镇银行“无法线上取现”背后的诈骗、失联之疑](https://www.huxiu.com/article/543844.html)
> 概要: 本文来自微信公众号：金融橙 （ID：Me-Finance），作者：胡艳明，原文标题：《【调查】豫皖五家村镇银行“无法线上取现”背后的诈骗、失联之疑》，头图来自：视觉中国“异常”是从4月19日开始被发现......
### [“三通一达”顺丰德邦纳入第二批白名单，上海快递业复工复产提速](https://www.yicai.com/news/101399185.html)
> 概要: 不少上海居民都陆续收到了此前在京东或天猫淘宝上下单的包裹。
### [海信视像、创维集团首季营收均过百亿 多元化路径不一](https://www.yicai.com/news/101399193.html)
> 概要: 今年一季度，中国彩电市场下滑超一成，多元化是海信视像、创维集团一季度营收过百亿的关键。
### [Top Navy Admiral Wants Rust-Free Ships](https://gcaptain.com/rusting-fleet-top-us-navy-admiral-cno-rust/)
> 概要: Top Navy Admiral Wants Rust-Free Ships
### [《刺猬索尼克2》成北美影史票房最高游戏改编电影](https://www.3dmgame.com/news/202204/3841466.html)
> 概要: 《刺猬索尼克2》上映20天后北美累计1.49亿美元，超过《刺猬索尼克》的1.48亿，成为北美影史票房最高的游戏改编电影。《刺猬索尼克2》已确定引进中国内地，档期待定。北美票房较高的游戏改编电影《神秘海......
### [俄罗斯安加拉-1.2 火箭首次飞行成功，将一颗卫星送上天](https://www.ithome.com/0/616/157.htm)
> 概要: 感谢IT之家网友情系半生nh的线索投递！IT之家4 月 30 日消息，俄国防部消息称，当地时间 4 月 29 日，“安加拉-1.2”轻型运载火箭搭载一颗卫星从普列谢茨克航天发射场成功发射升空，将俄罗斯......
### [如何用工业设计「比心」？](https://www.tuicool.com/articles/I7BVbmz)
> 概要: 如何用工业设计「比心」？
### [《武动乾坤》第三季终极PV登场！一切，该有个了结了！](https://new.qq.com/omn/20220430/20220430A09RGU00.html)
> 概要: 大荒的真相——《武动乾坤》第三季终极PV大荒往事如风，或正或邪皆逝去。今朝旧事重提，往来天骄齐聚此。一切，该有个了结了！，记得准时赴约更多番剧资讯，尽在腾讯视频动漫，还不快点个关注......
### [OneSignal (YC S11) Is Hiring Engineers to Democratize Mobile Customer Engagement](https://onesignal.com/careers)
> 概要: OneSignal (YC S11) Is Hiring Engineers to Democratize Mobile Customer Engagement
### [某瓣评分9.3，《夏日重现》被指侮辱女性，终究是题材受众的不同](https://new.qq.com/omn/20220430/20220430A09SV100.html)
> 概要: 同样是因为这种题材整体上的稀缺性，也能够让这种题材的作品出现后获得不少的关注。而涉及到这种元素的，往往都带着一丝悬疑性质，所以观众在观看的时候就能够感觉到一种刺激感。            在今年的四......
### [中国人保一季度净利润同比下滑12.9%，董事会抛出180亿元资本补血计划](https://finance.sina.com.cn/china/gncj/2022-04-30/doc-imcwiwst4953723.shtml)
> 概要: 记者| 苗艺伟 近日，中国人保（601319）陆续发布了一季度财报和股东大会多项议案。 一季度数据显示，当期内，中国人保实现营业收入1895.68亿元，同比增长9...
### [声优要绑定角色吗？《原神》声优拒绝绑定，引起玩家不满](https://new.qq.com/omn/20220430/20220430A0A0KM00.html)
> 概要: 对于声优和角色的关系，相信大部分小伙伴都没有一个统一的意见。有的人觉得声优应该和角色绑定，有的人却认为角色不应该和声优混为一谈。《原神》的一个声优，因为拒绝和《原神》里的角色早柚绑定，不想要被别人称为......
### [材料价格暴涨侵蚀利润空间 宁德时代凭什么获得长期成功？](https://finance.sina.com.cn/china/gncj/2022-04-30/doc-imcwipii7401256.shtml)
> 概要: 4月29日，全球动力电池龙头企业宁德时代（300750）披露了2022年一季报。财报显示，今年一季度，宁德时代实现营收486.78亿元，同比增长153.97%；归母净利润14.93亿元...
### [永辉超市年报、一季报上演“冰火两重天” 新领导层能否带领永辉走出泥潭](https://finance.sina.com.cn/china/gncj/2022-04-30/doc-imcwipii7400508.shtml)
> 概要: 下载新浪财经APP，查看更多资讯和大V观点 财联社|新消费日报4月30日讯（记者 李丹昱）随着社区团购热度渐退，传统商超获得了意思喘息的机会。
### [IDC：Q1 全球平板电脑需求下降，苹果、三星、亚马逊、联想、华为销量前五](https://www.ithome.com/0/616/165.htm)
> 概要: IT之家4 月 30 日消息，国际数据分析公司 IDC 现发布了最新的研究报告，评估了今年第一季度全球平板电脑市场的趋势和状况。IDC 表示，2022 年第一季度全球平板电脑出货量达 3840 万台，......
### [StreamNative刘昱：职业开源人的向上之路](https://www.tuicool.com/articles/jaeyUjm)
> 概要: StreamNative刘昱：职业开源人的向上之路
### [保障五一小长假居家就餐，北京70余家海底捞门店开通外卖、自提业务](https://finance.sina.com.cn/china/gncj/2022-04-30/doc-imcwiwst4956479.shtml)
> 概要: 下载新浪财经APP，查看更多资讯和大V观点 根据北京市疫情防控最新要求，“五一”劳动节（5月1日至5月4日）全市餐饮暂停堂食，海底捞北京各餐厅...
### [四川达州：生育二孩或三孩家庭购买第二套新房，可视为首套房](https://finance.sina.com.cn/jjxw/2022-04-30/doc-imcwiwst4959865.shtml)
> 概要: 4月28日，四川省达州市人民政府办公室发布《关于印发达州市促进房地产市场良性循环健康发展十条措施的通知》，有效期至2022年12月31日。
### [湖北黄石：购买黄石城区新房给予50%契税补贴](https://finance.sina.com.cn/china/gncj/2022-04-30/doc-imcwipii7405749.shtml)
> 概要: 财联社4月30日电，湖北省黄石市住建局、财政局等六部门4月29日联合发布《关于促进黄石城区房地产业平稳健康发展的通知》，通知明确，将进一步降低购房成本...
### [测不准的阿波连同学：来堂同学变胖了，阿波连真的十项全能啊](https://new.qq.com/omn/20220430/20220430A0AODQ00.html)
> 概要: 最新一集《测不准的阿波连同学》，我们看到了胖胖的来堂同学，以及家政学习全能的阿波连同学，嗯，恐怖如斯。            1、对战阿波连是一个经常与时代流行落伍的人这天她玩了一个类似于“宝可梦”的......
### [太阳爆发 X 级大耀斑，国家空间天气监测预警中心首次向国际民航组织发布咨询报](https://www.ithome.com/0/616/169.htm)
> 概要: IT之家4 月 30 日消息，据中国气象局网站，4 月 20 日 12 时前后，太阳爆发 X 级大耀斑，国家卫星气象中心（国家空间天气监测预警中心）作为国际民航组织（ICAO）全球空间天气中心的当值中......
### [深天马、维信诺一季度业绩增速放缓 加码投资OLED](https://www.yicai.com/news/101399275.html)
> 概要: 中国中小尺寸显示面板行业加速从液晶（LCD）向OLED拓展转型。
### [Z-Up潜力创作者100天加油计划](https://www.zcool.com.cn/event/designerrights/)
> 概要: Z-Up潜力创作者100天加油计划
### [火影忍者-创作之火，生生不息](https://www.zcool.com.cn/activity/ZNTc2.html)
> 概要: 火影忍者-创作之火，生生不息
# 小说
### [十方乾坤](http://book.zongheng.com/book/752142.html)
> 作者：神出古异

> 标签：武侠仙侠

> 简介：仙元末年，万道争锋，一心想拜入第一仙门，却身怀魔道祖师所留奇书，顺是天意，逆为我命，此生无悔来过。（古异第二本剧情流仙侠，书友群：76089602 微信公众号：神出古异。）

> 章节末：新书《神霄之上》已发布！

> 状态：完本
# 论文
### [TorMentor: Deterministic dynamic-path, data augmentations with fractals | Papers With Code](https://paperswithcode.com/paper/tormentor-deterministic-dynamic-path-data)
> 日期：7 Apr 2022

> 标签：None

> 代码：None

> 描述：We propose the use of fractals as a means of efficient data augmentation. Specifically, we employ plasma fractals for adapting global image augmentation transformations into continuous local transforms. We formulate the diamond square algorithm as a cascade of simple convolution operations allowing efficient computation of plasma fractals on the GPU. We present the TorMentor image augmentation framework that is totally modular and deterministic across images and point-clouds. All image augmentation operations can be combined through pipelining and random branching to form flow networks of arbitrary width and depth. We demonstrate the efficiency of the proposed approach with experiments on document image segmentation (binarization) with the DIBCO datasets. The proposed approach demonstrates superior performance to traditional image augmentation techniques. Finally, we use extended synthetic binary text images in a self-supervision regiment and outperform the same model when trained with limited data and simple extensions.
### [Linear, or Non-Linear, That is the Question! | Papers With Code](https://paperswithcode.com/paper/linear-or-non-linear-that-is-the-question)
> 日期：14 Nov 2021

> 标签：None

> 代码：None

> 描述：There were fierce debates on whether the non-linear embedding propagation of GCNs is appropriate to GCN-based recommender systems. It was recently found that the linear embedding propagation shows better accuracy than the non-linear embedding propagation.
