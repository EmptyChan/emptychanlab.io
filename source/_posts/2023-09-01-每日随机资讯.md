---
title: 2023-09-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TurkeyTailMush_ZH-CN9683744281_1920x1080.webp&qlt=50
date: 2023-09-01 16:14:03
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TurkeyTailMush_ZH-CN9683744281_1920x1080.webp&qlt=50)
# 新闻
### [Taking Statins After 'Bleeding' Stroke Could Help Prevent Another Stroke](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx7797597300849&cate_id=-1)
> 概要: 新的研究表明，在出血性中风或脑内出血后服用降胆固醇的他汀类药物，可能会降低由血栓引起的中风的风险......
### [Rapid Medical™ Gains Approval in China for the World’s Only Adjustable Thrombectomy Device](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1312683223158&cate_id=-1)
> 概要: TIGERTRIEVER™旨在加速血栓整合并减少缺血性卒中中的血管损伤......
### [Digging deeper into how vaccines work against parasitic disease](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1707971209320&cate_id=-1)
> 概要: 科学家们已经在动物研究中确定了他们开发的预防皮肤病利什曼病的疫苗的有效性，最有希望的候选疫苗的第一阶段人体试验计划正在进行中......
### [Are Artificial Sweeteners Safe for People With Diabetes?](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx8252142413952&cate_id=-1)
> 概要: 有些甜味剂可能有健康风险，所以最好保持摄入量适度......
### [In Primate Study, Antibody Treatment Prevents Organ Rejection After Transplant](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx5236320397426&cate_id=-1)
> 概要: 一项针对非人类灵长类动物的新研究表明，使用人造单克隆抗体有可能帮助预防移植后的器官排斥反应......
### [项目实战：供应商生命周期管理设计](https://www.woshipm.com/share/5893173.html)
> 概要: 为了更好地管理和优化企业与供应商之间的关系，我们需要基于业务与合作实质，做好供应商的生命周期管理。那么，怎么才能做好供应商生命周期管理呢？本文作者便分享了生命周期管理以及供应商信息变更流程的设计思路，......
### [私域团队的坑，100%的企业都踩过](https://www.woshipm.com/operate/5895705.html)
> 概要: 如今，大家都在做私域，但做的怎么样，每家都不一样。本文主要在讲大部分企业踩过的私域团队的坑，对当前私域人才困境进行了分析，并总结企业招人标准，希望对你有所帮助。最近也与很多朋友进行了交流，被问到最多的......
### [“搭子”快被玩坏了](https://www.woshipm.com/user-research/5895999.html)
> 概要: 前几个月，“搭子”突然就火了，这种通过社交平台发出邀约，快速找到人应邀，完成单次活动的形式，成了2023年最受年轻人欢迎的社交关系。不少年轻人对“搭子”存在美好想象，但事实真的如此吗？当负面案例开始出......
### [专访盛松成：需兼顾商业银行净息差与存量房贷利率调降](https://www.yicai.com/news/101849063.html)
> 概要: 能稳住市场就是有效。
### [使用 Phoenix LiveView 构建 Instagram (1)](https://segmentfault.com/a/1190000044171577)
> 概要: 使用PETAL(Phoenix、Elixir、TailwindCSS、AlpineJS、LiveView)技术栈构建一个简化版的Instagram Web应用程序<!--more-->更好的学习方法是......
### [这个 AI 机器人会怼人，它是怎么做到的？](https://segmentfault.com/a/1190000044173072)
> 概要: 近期，机器人“Ameca”接入了 Stable Diffusion，它一边与旁边的人类工程师谈笑风生，一边熟练地用马克笔在白板上画出一只简笔的猫，最后还在白板右下角签名。当 Ameca 询问工程师是否......
### [马斯克的自动驾驶直播够震撼吗？](https://finance.sina.com.cn/tech/2023-09-01/doc-imzkeepi8929891.shtml)
> 概要: 毫不夸张地说，马斯克是乔布斯之后美国最懂营销的商界领袖......
### [喜忧参半的万科中报：销售额降速收窄，何时迎拐点？](https://finance.sina.com.cn/tech/2023-09-01/doc-imzkeepn8016555.shtml)
> 概要: 文 | 新浪财经 徐苑蕾......
### [半年盘点｜“三桶油”营收全面下滑，但仍揽下超1800亿元利润](https://www.yicai.com/news/101849185.html)
> 概要: 净利润方面，“三桶油”合计实现净利润1841.4亿元，中国石油是其中唯一一家实现净利润正增长的企业。
### [WePlay不信邪，再次强攻美国市场](https://36kr.com/p/2412030569374469)
> 概要: WePlay不信邪，再次强攻美国市场-36氪
### [文心一言开放首日，回答网友3342万个问题](http://www.investorscn.com/2023/09/01/110157/)
> 概要: 8月31日0点至24点，百度官方平台数据显示，短短24小时内，文心一言共计回复网友超3342万个问题......
### [《星空》Xbox版将有官方Mod支持 玩家可添加新星球](https://www.3dmgame.com/news/202309/3876685.html)
> 概要: 早先Todd Howard曾表示《星空》将是Mod作者的天堂。近日B社营销主管Pete Hines确认，《星空》XSX/S版将获得官方Mod支持，这意味着PC和Xbox玩家都能获得Mod乐趣。Pete......
### [《和MIKU一起锻炼》预告上线 明年春季发售](https://www.3dmgame.com/news/202309/3876688.html)
> 概要: 今日（9月1日），Imagineer官方公布有氧拳击系列新作《Fit Boxing feat.初音未来-和MIKU一起锻炼-》，售价待定，2024年春季发售，登陆switch平台。宣传片：视频截图：......
### [小米新经营策略持续奏效，上半年盈利接近去年全年](https://finance.sina.com.cn/tech/internet/2023-09-01/doc-imzkekvi5641750.shtml)
> 概要: 炒股就看金麒麟分析师研报，权威，专业，及时，全面，助您挖掘潜力主题机会......
### [《三位一体5 发条阴谋》发售日预告 售价108元](https://www.3dmgame.com/news/202309/3876692.html)
> 概要: 今日（9月1日），发行商 THQ Nordic 和开发商Frozenbyte发布了《三位一体5：发条阴谋》的发售日预告，截止到发稿前，本作已获得67个评价，好评率94%，综合评价“特别好评”，Stea......
### [《GIVEN》动画系列公布回顾PV](https://news.dmzj.com/article/79088.html)
> 概要: 《GIVEN》新作续篇动画确定为剧场版2部作，官方公布系列回顾PV。
### [Netflix《阴阳师》公开最新情报！定档11月28日](https://news.dmzj.com/article/79090.html)
> 概要: Netflix《阴阳师》公开主演声优，将于11月28日上线。
### [ASML最新回应：已向荷兰政府提出光刻系统出口许可证申请](https://www.yicai.com/news/101849403.html)
> 概要: 在新的出口管制条例下，今年年底前ASML仍然能够履行与客户签订的合同。
### [组图：张雨绮成立新公司 为其取名叫“绮开得胜”](http://slide.ent.sina.com.cn/star/slide_4_704_388489.html)
> 概要: 组图：张雨绮成立新公司 为其取名叫“绮开得胜”
### [219 元仅限今日：华为手环 8 / NFC 版官方开学季限时狂促](https://www.ithome.com/0/716/241.htm)
> 概要: 华为手环 8、华为手环 8 NFC 于今年 4 月发布，标准版售价 269 元，NFC 版售价 309 元。今日京东自营 / 天猫官方旗舰店均直降至 219 元起，活动仅限 9 月 1 日：京东自营京......
### [组图：迪丽热巴九宫格彩绘plog 元气小巴趣味日常尽收眼底](http://slide.ent.sina.com.cn/star/w/slide_4_704_388491.html)
> 概要: 组图：迪丽热巴九宫格彩绘plog 元气小巴趣味日常尽收眼底
### [TV动画《葬送的芙莉莲》公开正式PV！](https://news.dmzj.com/article/79091.html)
> 概要: TV动画《葬送的芙莉莲》公开正式PV，9月29日开播。
### [「e公司观察」经纬纺机主动退市，有相对更多的“主动”意味](http://www.investorscn.com/2023/09/01/110163/)
> 概要: 今天，经纬纺机因选择“主动退市”成为市场焦点，公司股价复牌后一字涨停......
### [金地集团:上半年新增土储85万平米,总投资119亿元](http://www.investorscn.com/2023/09/01/110164/)
> 概要: 8月30日,金地集团披露2023年半年报。报告显示,2023年以来,金地集团进一步加强了一二线城市的投资布局,先后在在上海、杭州、南京、西安、东莞等城市获取了优质土储资源,新获取项目总投资额约119亿......
### [P站美图推荐——马特辑（二）](https://news.dmzj.com/article/79092.html)
> 概要: 来一期不带赛马娘的马特辑
### [即氪运动融资丨雪族科技融资4000万元，开发户外运动「大众点评」](https://36kr.com/p/2412813108110341)
> 概要: 即氪运动融资丨雪族科技融资4000万元，开发户外运动「大众点评」-36氪
### [组图：陈乔恩晒图分享惬意慢生活 戴针织草帽捧花甜笑超治愈](http://slide.ent.sina.com.cn/star/w/slide_4_704_388492.html)
> 概要: 组图：陈乔恩晒图分享惬意慢生活 戴针织草帽捧花甜笑超治愈
### [联想发布 ThinkVision27 3D 显示器：支持裸眼 3D 显示](https://www.ithome.com/0/716/252.htm)
> 概要: IT之家9 月 1 日消息，联想今日在 IFA 2023 展会上发布了 ThinkVision27 3D 显示器，售价 2999 欧元（IT之家备注：当前约 23692 元人民币），2024 年 2 ......
### [智己汽车扩大辅助驾驶覆盖范围：新增 4 省 6 城，全国高速高架 NOA 年底全部开通](https://www.ithome.com/0/716/253.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家9 月 1 日消息，智己汽车今日宣布，旗下智能辅助驾驶 NOA 覆盖范围再度扩大，新增了 4 省 6 城。具体来看，此次新增了包括湖北省、河南省、陕西省、......
### [轻盈保暖白鸭绒：雅鹿男士轻薄羽绒服 99 元官方发车](https://lapin.ithome.com/html/digi/716260.htm)
> 概要: 天猫【雅鹿男装旗舰店】雅鹿 男士轻薄羽绒服 日常售价 178 元，今日可领 79 元大额券，实付 99 元包邮。此款充绒量 70g 左右，适合 5℃左右穿着：天猫雅鹿 男士轻薄羽绒服联名 / 立领 8......
### [【西海岸】下赛季在湖人继续保持强势表现？里夫斯：我只关心赢球](https://bbs.hupu.com/61936774.html)
> 概要: 【西海岸】下赛季在湖人继续保持强势表现？里夫斯：我只关心赢球
### [联想Legion GO掌上PC定价公布：699美元起](https://www.3dmgame.com/news/202309/3876700.html)
> 概要: 联想目前终于证实公布了其首款 Windows 系统掌上电脑 Legion GO。这款掌机将于 10月发售，售价 699 美元起。配备了 8.8 英寸 QHD Plus 屏幕、AMD Ryzen Z1 ......
### [北京、上海、河南、福建……多地公布首套房贷利率下限](https://www.yicai.com/news/101849627.html)
> 概要: 上海、北京、深圳、吉林、天津等多地纷纷公布首套商业性个人住房贷款利率下限。
### [36氪首发｜聚焦AIGC for Code，「硅心科技aiXcoder」完成A+轮融资，加速行业落地](https://36kr.com/p/2412980674470918)
> 概要: 36氪首发｜聚焦AIGC for Code，「硅心科技aiXcoder」完成A+轮融资，加速行业落地-36氪
### [沙盒冒险《整蛊鸦》正式预告 将登陆主机和PC](https://www.3dmgame.com/news/202309/3876702.html)
> 概要: 此前曾开发了《整蛊白云》的独立开发商 Unbound Creations 正式公布了其最新作品《整蛊鸦（JustCrow Things）》的首支预告片，并宣布了游戏将登陆 PS5、PS4、Xbox S......
### [“2023中国电子商务大会”9月2日在京开幕](http://www.investorscn.com/2023/09/01/110167/)
> 概要: 【亿邦原创】2023中国电子商务大会将于9月2日-3日在北京国家会议中心举办，作为中国国际服务贸易交易会（服贸会）高峰论坛之一，大会由商务部、北京市人民政府共同主办，商务部电子商务和信息化司、北京市商......
### [领克汽车在宁波成立销售公司](http://www.investorscn.com/2023/09/01/110171/)
> 概要: 【#领克汽车在宁波成立销售公司#】......
### [【篮凤凰】里夫斯：老詹可能看到球迷标牌了，我可以拿这个跟他开玩笑](https://bbs.hupu.com/61937784.html)
> 概要: 【篮凤凰】里夫斯：老詹可能看到球迷标牌了，我可以拿这个跟他开玩笑
### [青藏高原保护法施行，从严制定环境准入清单、布局科技创新平台](https://www.yicai.com/news/101849703.html)
> 概要: 当前，青藏高原仍然面临极端气候增多、冰川退缩、冻土消融、生物多样性退化等挑战，经济发展过程中保护与发展的矛盾仍然突出。
### [流言板TTG发文：今日外场应援选手为帆帆，一起为甜甜糕加油助威！](https://bbs.hupu.com/61937952.html)
> 概要: 虎扑09月01日讯 TTG发布公告：今日外场应援选手为帆帆，一起为甜甜糕助威！原文如下：今日外场应援surprise选手为帆帆哦 18:15我们不见不散一起来为甜甜糕加油助威吧！   来源： 新浪微博
### [世界杯速递欧洲豪强之争！波格丹突破上篮打进运动战首球](https://bbs.hupu.com/61937993.html)
> 概要: 世界杯速递欧洲豪强之争！波格丹突破上篮打进运动战首球
### [正宁大葱里的“致富密码”](https://finance.sina.com.cn/jjxw/2023-09-01/doc-imzkevme7768730.shtml)
> 概要: 正宁大葱里的“致富密码”每日甘肃网9月1日讯 据陇东报报道（通讯员师亚萍）时下，走进正宁县宫河镇彭姚川村，漫山遍野都是郁郁葱葱的大葱，一行行整齐排列，长势喜人。
### [重庆市綦江区市场监管局“四个到位”护航秋季开学季](https://finance.sina.com.cn/jjxw/2023-09-01/doc-imzkevmc5513074.shtml)
> 概要: 中国质量新闻网讯（詹雨 谢旺江）为全力保障广大师生饮食安全，净化校园及周边市场环境，近日，重庆市綦江区市场监管局积极作为统筹部署...
### [川渝四地市场监管局联合整治加油站计量作弊](https://finance.sina.com.cn/jjxw/2023-09-01/doc-imzkevme7769620.shtml)
> 概要: 中国质量新闻网讯（周兴宇 谢旺江）8月24日至25日，重庆市荣昌区与四川省泸州市、内江市、资阳市四地市场监管局，开展联合整治加油站计量作弊专项行动。
### [流言板潇洒预测今日赛果：再来一个巅峰对决，狼队 3-4 TTG](https://bbs.hupu.com/61938024.html)
> 概要: 虎扑09月01日讯 潇洒预测今日赛果：再来一个巅峰对决，狼队 3-4 TTG原文如下：今天再来一个巅峰对决如何   来源： 新浪微博    标签：广州TTG重庆狼队
### [印刷板材第一股或将诞生，强邦新材去年收入超15亿元｜专精快报](https://36kr.com/p/2413073015088130)
> 概要: 印刷板材第一股或将诞生，强邦新材去年收入超15亿元｜专精快报-36氪
### [镶黄旗供电开展“故事廉廉看，猜猜他是谁”  挑战答题活动](https://finance.sina.com.cn/jjxw/2023-09-01/doc-imzkevmc5512784.shtml)
> 概要: 中国能源新闻网讯（赵静 胡博）为进一步增强党员干部职工廉洁意识，筑牢拒腐防变思想防线，近日，镶黄旗供电公司开展“故事廉廉看，猜猜他是谁”挑战答题活动...
### [媒体报道“广西开展社会评价，梧州群发短信让群众回答‘满意’引质疑”，当地通报](https://finance.sina.com.cn/jjxw/2023-09-01/doc-imzkevme7769846.shtml)
> 概要: 来源：梧州发布公告近日，有媒体发布报道，称“广西开展社会评价，梧州群发短信让群众回答‘满意’引质疑”。市委、市政府高度重视，立即成立专门调查组对此事开展调查...
# 小说
### [前期发育后期浪](https://book.zongheng.com/book/911021.html)
> 作者：减脂狂魔

> 标签：奇幻玄幻

> 简介：哈哈哈哈！第一本书，慢慢进步，只为分享我的世界！太初世界，强者无数，美女如云！楚天英穿越至此，游历世间，偶然发现了这个世界的一些秘密。神是什么？魔又由何而生？这一切都隐隐只向一百万年前！据传百万年前，太初世界的封神之路就已断绝。此间真相也已经被埋在了历史之中！一百万年前究竟发生了什么！请君稍等，且看太初衍世如何为您还原一个真相！

> 章节末：第一百五十七章 秘境开启

> 状态：完本
# 论文
### [Hard Patches Mining for Masked Image Modeling | Papers With Code](https://paperswithcode.com/paper/hard-patches-mining-for-masked-image-modeling)
> 日期：12 Apr 2023

> 标签：None

> 代码：https://github.com/haochen-wang409/hpm

> 描述：Masked image modeling (MIM) has attracted much research attention due to its promising potential for learning scalable visual representations. In typical approaches, models usually focus on predicting specific contents of masked patches, and their performances are highly related to pre-defined mask strategies. Intuitively, this procedure can be considered as training a student (the model) on solving given problems (predict masked patches). However, we argue that the model should not only focus on solving given problems, but also stand in the shoes of a teacher to produce a more challenging problem by itself. To this end, we propose Hard Patches Mining (HPM), a brand-new framework for MIM pre-training. We observe that the reconstruction loss can naturally be the metric of the difficulty of the pre-training task. Therefore, we introduce an auxiliary loss predictor, predicting patch-wise losses first and deciding where to mask next. It adopts a relative relationship learning strategy to prevent overfitting to exact reconstruction loss values. Experiments under various settings demonstrate the effectiveness of HPM in constructing masked images. Furthermore, we empirically find that solely introducing the loss prediction objective leads to powerful representations, verifying the efficacy of the ability to be aware of where is hard to reconstruct.
### [Learnability Enhancement for Low-light Raw Denoising: Where Paired Real Data Meets Noise Modeling | Papers With Code](https://paperswithcode.com/paper/learnability-enhancement-for-low-light-raw)
> 日期：13 Jul 2022

> 标签：None

> 代码：https://github.com/megvii-research/pmn

> 描述：Low-light raw denoising is an important and valuable task in computational photography where learning-based methods trained with paired real data are mainstream. However, the limited data volume and complicated noise distribution have constituted a learnability bottleneck for paired real data, which limits the denoising performance of learning-based methods. To address this issue, we present a learnability enhancement strategy to reform paired real data according to noise modeling. Our strategy consists of two efficient techniques: shot noise augmentation (SNA) and dark shading correction (DSC). Through noise model decoupling, SNA improves the precision of data mapping by increasing the data volume and DSC reduces the complexity of data mapping by reducing the noise complexity. Extensive results on the public datasets and real imaging scenarios collectively demonstrate the state-of-the-art performance of our method.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
