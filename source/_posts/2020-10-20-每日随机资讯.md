---
title: 2020-10-20-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BLNC_EN-CN6837155119_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-10-20 20:59:18
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BLNC_EN-CN6837155119_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [好事将近？马国明称与女友汤洛雯有结婚生子共识](https://ent.sina.com.cn/s/h/2020-10-20/doc-iiznctkc6526433.shtml)
> 概要: 新浪娱乐讯 据香港媒体报道，视帝马国明拿奖后，工作、爱情一帆风顺，对将来继续“佛系“未有要求太多。与汤洛雯爱得甜蜜的他透露结婚、生子有共识：“自己都喜欢小朋友的。“更笑言与郑嘉颖、罗仲谦、钟嘉欣拍剧时......
### [北京音乐家协会成立电子键盘学会 王小玮任会长](https://ent.sina.com.cn/y/yneidi/2020-10-20/doc-iiznctkc6644124.shtml)
> 概要: 新浪娱乐讯 10月20日，北京音乐家协会电子键盘学会成立大会在北京召开，会上宣布青年电子管风琴演奏家、器乐组合玖月奇迹成员王小玮担任将担任会长。(责编：漠er凡)......
### [炎亚纶称自己没资格扮演受害者 被问感情秒答六字](https://ent.sina.com.cn/s/h/2020-10-20/doc-iiznctkc6519948.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，炎亚纶10月19日出席加盟索尼音乐记者会，提到曾被亏“没成名曲“，因此心生芥蒂，又因2年前爆发感情风波，不自觉地进入低潮潮，直至投身创作找到情绪出口，同步寻求咨询师协助，一......
### [组图：孟美岐穿黑色西装配贝雷帽复古时髦 脚踩尖头皮鞋气场足](http://slide.ent.sina.com.cn/y/slide_4_704_346937.html)
> 概要: 组图：孟美岐穿黑色西装配贝雷帽复古时髦 脚踩尖头皮鞋气场足
### [组图：刘涛主持金鹰奖后亮相机场 西装叠穿卫衣酷气十足](http://slide.ent.sina.com.cn/star/slide_4_704_346930.html)
> 概要: 组图：刘涛主持金鹰奖后亮相机场 西装叠穿卫衣酷气十足
### [李现自称不是偶像是演员，不抽烟不喝酒，每晚坚持健身超自律](https://new.qq.com/omn/20201020/20201020A0CVI500.html)
> 概要: 去年夏天，李现和同班同学杨紫合作《亲爱的热爱的》热播，他从一个鲜为人知的小演员，一跃成为一线当红明星，成为了万千少女心目中的“七月现男友”，人气暴涨。和现在的小鲜肉不同，李现不仅拥有好看的皮囊，还拥有......
### [关晓彤拍大片秀纤细美腿，卷发少女气息满满，网友：鹿晗好福气](https://new.qq.com/omn/20201020/20201020A09OZY00.html)
> 概要: 关晓彤拍大片秀纤细美腿，卷发少女气息满满，网友：鹿晗好福气
### [任达华商演保安围成墙，五米之内无人近身，本人谈笑风生不受影响](https://new.qq.com/omn/20201020/20201020A0CKVF00.html)
> 概要: 10月19日，有网友晒出了任达华在广东参加某品牌商演的视频，此次活动吸引了很多观众前去围观。从视频中来看，当天的安保措施十分严格。在舞台的周边处，保安们手牵着手形成一道严实的人墙，让五米之内的现场群众......
### [50岁工藤静香针织帽搭卫衣，手插兜单膝跪地帅气；木村光希剪刘海像姐姐](https://new.qq.com/omn/20201020/20201020A05EBI00.html)
> 概要: 据日本媒体10月20日报道，歌姬工藤静香于社交平台发布一组摆拍，休闲时髦穿搭似原宿街头年轻女孩，苗条身材令众网友羡慕不已。            50岁的静香身穿黑色连帽长款卫衣，下搭修身长裤和黑色靴......
### [“太子妃”里升职最快的竟然是他！](https://new.qq.com/omn/20201020/20201020A08US500.html)
> 概要: 10月的影视圈，最火演员是谁？彭昱畅必须拥有姓名！彭彭最近真是牛大发了，哪哪都有他，彭彭天天见。            国庆档三部电影他都在主演名单，《一点就到家》三男主之一，两部大片《夺冠》和《我和......
# 动漫
### [《名侦探柯南》1万日元手办等1年等成邪神像 商家同意退款](https://news.dmzj.com/article/68896.html)
> 概要: 《名侦探柯南》和SuperGroupies合作推出的水晶球目前已经发货了。不过尽管本商品售价9800日元（不含税），而且等了一年后才到货，但很多顾客在拿到手后发现，里面人物造型与宣传图相距甚远。
### [剧场版动画《紫罗兰永恒花园》观影人数突破100万](https://news.dmzj.com/article/68900.html)
> 概要: 从9月18日开始上映的剧场版动画《紫罗兰永恒花园》宣布了在10月18日观影人数突破100万人的消息。为感谢大家的支持，官方决定追加文件夹作为观影特典。
### [《托马斯和朋友们》新作剧场版2021年上映！](https://news.dmzj.com/article/68895.html)
> 概要: 《托马斯和朋友们》系列新剧场版《托马斯和朋友们 来吧！未来的发明展！》宣布了将于2021年春上映的消息。
### [GSC《熊熊勇闯异世界》优奈POP UP PARADE手办](https://news.dmzj.com/article/68898.html)
> 概要: GSC根据目前正在播出中的TV动画《熊熊勇闯异世界》中的优奈制作的POP UP PARADE系列手办目前已经开订了。本作在保持了POP UP PARADE系列产品相对低的价格，全高17～18cm的高度，预约后4个月左右发售等的特点的同时，再现了优...
### [死神：一角为了不当队长而隐瞒卍解，可他真的有当队长的实力吗？](https://new.qq.com/omn/20201020/20201020A07IUO00.html)
> 概要: 《死神》里始解是每个副队长以上都必须掌握的基础技能，甚至有些拥有席位的死神也会始解，可是卍解，是成为队长的必要条件。一角为何会隐藏实力，他的自信哪里来？一角的全名叫斑目一角，一个光头，生前不知道，死……
### [龙珠超65话情报：悟空的仁慈成就了魔罗，自在极意魔罗诞生](https://new.qq.com/omn/20201020/20201020A088MG00.html)
> 概要: Hello！各位小伙伴大家好，龙珠超65话漫画的情报已经更新，当悟空决定以地球人的身份战斗时，比鲁斯就感到有不好的预感，而比鲁斯的这一预感也很准确，那就是魔罗也完成了自在极意功。悟空在实习天使梅尔斯……
### [B站UP被指责“工地日语”！嫌日本人日语不好，我迷惑了！](https://new.qq.com/omn/20201020/20201020A04UHE00.html)
> 概要: 随着B站逐渐发展壮大，B站其实已经不再是以前那个纯二次元网站了。现在的B站其实已经变成了和“油管”类似的以用户投稿为主的综合视频网站。因此也有很多外国UP主入驻B站，可能是因为B站本身有着“日本动画……
### [摔坏的手办被老奶奶重新安装，好像比原来更有味道了](https://new.qq.com/omn/20201020/20201020A0C1EZ00.html)
> 概要: 现在喜欢二次元的人多了，经常会购买一些手办在家里。因为是自己喜欢的手办，每次把玩都非常小心。但对家长来说，手办就是一个玩偶，没什么特别，把玩就比较粗心了，也经常会有网友分享，自己心爱的手办被家人弄坏……
### [凭啥明哥能果实觉醒但四皇与大将却不行？或许他们缺少了这些资质](https://new.qq.com/omn/20201020/20201020A06T2K00.html)
> 概要: 【前言】我们都知道《海贼王》里的恶魔果实不计其数，果实能力强者也不胜枚举，然而我们都知道即便是将剧场版里的巴雷特算在内，整部《海贼王》里觉醒了果实能力的人物也才两位：多弗朗明哥、道格拉斯巴雷特。而且……
### [秦时明月：沧海逆流创造新纪录，豆瓣评分仅6.8，竟比龙腾万里还烂！](https://new.qq.com/omn/20201020/20201020A09QC100.html)
> 概要: 大家好，北冥特摄漫评，带你看不一样的秦时明月。秦时明月系列一直是国漫里面非常出色的作品，是很多人的童年神作，因为是原创剧情，因此到现在都没有完结，反而剧情越来越精彩了，这也是以秦国时期的故事为原型的……
### [《数码宝贝》剧场版新中文预告 踏上最后的冒险之旅](https://acg.gamersky.com/news/202010/1330030.shtml)
> 概要: 《数码宝贝：最后的进化》是《数码宝贝》开播20周年纪念作品，讲述的是八神太一与石田大和成长为22岁时发生的故事。主要声优为花江夏树，竹内顺子，樱井孝宏和细谷佳正。
### [网友票选《鬼灭之刃》最有魅力角色 祢豆子没进前十](https://acg.gamersky.com/news/202010/1329956.shtml)
> 概要: 日本网站《gooランキング》进行了一个“鬼灭之刃最有魅力角色”的票选排行，现在结果已经公开，来看看上榜的都是哪些角色，你喜欢的角色是第几名呢。
# 财经
### [“十三五”减税费7.6万亿 超2009年全国财政收入](https://finance.sina.com.cn/roll/2020-10-20/doc-iiznctkc6672224.shtml)
> 概要: 原标题：“十三五”减税费7.6万亿，超2009年全国财政收入 今年2.5万亿元规模空前的减税降费政策正快速兑现，前8个月已经实际减税降费已超1.8万亿元。
### [上海市委中心组学习民法典 李强：让法治成为城市治理的闪亮名片](https://finance.sina.com.cn/china/dfjj/2020-10-20/doc-iiznctkc6673166.shtml)
> 概要: 原标题：上海市委中心组学习民法典，李强要求让法治成为城市治理的闪亮名片 上海市委中心组今天下午（10月20日）举行学习会，听取中国法学会副会长...
### [深圳老外贸人尝试新招数 “云上广交会”成效初现](https://finance.sina.com.cn/china/dfjj/2020-10-20/doc-iiznctkc6671967.shtml)
> 概要: “本届广交会相较于上一届还是有喜人的一面。内部数据显示，截至10月19日的总数据已经与127届10天总数据基本持平。”燕加隆家居建材股份有限公司总监孙翔如是说。
### [周星驰对赌失败拖欠巨款？律师发声明斥不实报道](https://finance.sina.com.cn/china/gncj/2020-10-20/doc-iiznezxr7091778.shtml)
> 概要: 针对近日网络流传周星驰“因电影对赌失败，拖欠投资方巨额债务”的报道，北京市京师律师事务所受委托发布声明。 周星驰（资料图） 周星驰委托的律师事务所发声明周星驰委托的...
### [西门子大中华区总裁兼CEO：我们已经是一家中国企业](https://finance.sina.com.cn/chanjing/gsnews/2020-10-20/doc-iiznctkc6673714.shtml)
> 概要: 来源：国是直通车 全球本土化 现在看来，新冠肺炎疫情持续的时间可能要比人们想象中长得多。这起全球大流行的疾病对企业特别是跨国公司来说究竟意味着什么？
### [报告：2021年毕业生去这三个行业涨薪最多 国企受青睐](https://finance.sina.com.cn/china/gncj/2020-10-20/doc-iiznezxr7091313.shtml)
> 概要: 原标题：报告：2021年毕业生去这三个行业涨薪最多，国企受青睐 疫情对于大学生毕业生就业的负面影响正在逐步消退，2021届高校毕业生的就业难度将小于上一届。
# 科技
### [前端 BUG 录 - 因数组排序造成的卡顿](https://segmentfault.com/a/1190000037455206)
> 概要: 前端 BUG 录 - 因 lodashjs debounce 去抖优化造成的 bug这两个 BUG 其实是同一个 BUG，怎么说呢？两个都没错，错就错在同时使用了。因为我没处理边界，导致我会给一个大数......
### [一文看懂ArrayList](https://segmentfault.com/a/1190000037536858)
> 概要: 前言很久之前写过一篇有关HashMap的文章：一文看懂HashMap，反响不错。本来手后面是想写篇文章来介绍ArrayList，后来事情多就忘了，今天就来好好聊聊ArrayList。正文ArrayLi......
### [特斯拉：会向法院对造谣微博用户提起诉讼](https://www.ithome.com/0/514/651.htm)
> 概要: IT之家10月20日消息 针对今天传的沸沸扬扬的 “特斯拉 Model 3 降价至 19.9 万元”的传闻，特斯拉官方今日直接表示这是谣言，并将向法院提起诉讼。这个微博用户发布的关于我们价格的内容是谣......
### [外媒爆料：AMD RX 6000 旗舰显卡总板功耗为 320 W](https://www.ithome.com/0/514/617.htm)
> 概要: IT之家 10 月 20 日消息 根据外媒 Igor'sLAB 的最新报道，AMD RX 6000 的旗舰型号 “Navi 21 XT”的总板功率约为 320W。外媒表示，Navi 21 XT 型号的......
### [OPPO、华为们为什么都开始造电视了？](https://www.huxiu.com/article/388420.html)
> 概要: 本文来自微信公众号：极客公园（ID：geekpark），作者：茜茜，原文标题：《手机厂商为什么都开始造电视了？》，头图来自：视觉中国OPPO 开发者大会上留下的悬念终于揭晓了。10 月 19 日晚，O......
### [一个瓶盖能让饮料“身价”暴涨吗？](https://www.huxiu.com/article/388437.html)
> 概要: 本文来自微信公众号：FBIF食品饮料创新（ID：FoodInnovation），原标题《猫咪瓶盖让三得利关注度暴涨500倍？小瓶盖玩出大创意！》，作者：Tutu（Edmund），编辑：Bobo，题图来......
### [游戏史中的习惯成自然：“GG”到底是怎么来的？](https://www.tuicool.com/articles/mQ3QRj3)
> 概要: 编者按：本文来自微信公众号“触乐”（ID:chuappgame），作者：王亦般，36氪经授权发布。“智商杯”是一群《星际争霸2》项目退役老选手组织的水友赛性质的比赛。在智商杯上，老选手们常常在劣势时退......
### [学会这6点，教你制作高效且吸引人的数据可视化](https://www.tuicool.com/articles/BRnYJfM)
> 概要: 编辑导语：数据可视化，就是准确、高效、精简而全面地传递信息和知识，它能将不可见的数据现象转化为可见的图形符号，并且利用合适的图表直截了当且清晰而直观地表达出来，加深和强化受众对于数据的理解和记忆。那么......
# 小说
### [魔力小子](http://book.zongheng.com/book/133298.html)
> 作者：没屋顶

> 标签：奇幻玄幻

> 简介：...

> 章节末：第四十三章

> 状态：完本
# 游戏
### [《莱莎2》主线通关需40小时 PS5版对应4K加载更快](https://www.3dmgame.com/news/202010/3800042.html)
> 概要: 《莱莎的炼金工房》系列制作人细井顺三近日接受了4gamers的采访，讲述了《莱莎的炼金工房2》与其前作的差异之处。本次采访要点如下：- 续作中莱莎的人物模组并没有太大改变，下巴等部位是一样的- 剧情中......
### [Goodsmile莱莎手办再版 售价1310元 肉腿120%还原](https://www.3dmgame.com/news/202010/3800014.html)
> 概要: 今日（10月20日）Goodsmile官方宣布“莱莎”1/7手办再版，现已上架Goodsmile天猫旗舰店，售价1310元，全高24cm，将于2021年5月正式出货。Goodsmile天猫旗舰店：点我......
### [2020胡润中国百富榜揭晓 马云身价4000亿蝉联首富](https://www.3dmgame.com/news/202010/3800022.html)
> 概要: 日前，胡润通过视频的形式揭晓了2020胡润中国百富榜。胡润表示尽管年初有新冠疫情肆虐，但中国头部富豪的资产并未受到影响，反而是体量增加了10万亿，是上世纪90年代发榜以来财富增长最高的一年。此次的前三......
### [休闲策略新游《拳击特工》10月登PC 支持远程同乐](https://www.3dmgame.com/news/202010/3800020.html)
> 概要: 根据开发商兼发行商GrosChevaux公开的相关消息，适合2-4人的休闲策略游戏《拳击特工》（Unspottable）目前已经上架Steam商城页面，该作将于2020年10月22日登陆Steam平台......
### [新版PS商城页面泄露 《Share Factory》将登陆PS5](https://www.3dmgame.com/news/202010/3800009.html)
> 概要: 日前索尼曾通过邮件告知玩家，PlayStation网上商城即将迎来改版，新版PS网上商城上线之后，用户将不再允许通过网页和移动端购买PS3、PSV和PSP平台的游戏和附加内容，除此之外，新版PS网上商......
# 论文
### [Is Everything Fine, Grandma? Acoustic and Linguistic Modeling for Robust Elderly Speech Emotion Recognition](https://paperswithcode.com/paper/is-everything-fine-grandma-acoustic-and)
> 日期：7 Sep 2020

> 标签：EMOTION RECOGNITION

> 代码：https://github.com/gizemsogancioglu/elderly-emotion-SC

> 描述：Acoustic and linguistic analysis for elderly emotion recognition is an under-studied and challenging research direction, but essential for the creation of digital assistants for the elderly, as well as unobtrusive telemonitoring of elderly in their residences for mental healthcare purposes. This paper presents our contribution to the INTERSPEECH 2020 Computational Paralinguistics Challenge (ComParE) - Elderly Emotion Sub-Challenge, which is comprised of two ternary classification tasks for arousal and valence recognition.
### [Block Model Guided Unsupervised Feature Selection](https://paperswithcode.com/paper/block-model-guided-unsupervised-feature)
> 日期：5 Jul 2020

> 标签：FEATURE SELECTION

> 代码：https://github.com/ZilongBai/KDD2020BMGUFS

> 描述：Feature selection is a core area of data mining with a recent innovation of graph-driven unsupervised feature selection for linked data. In this setting we have a dataset $\mathbf{Y}$ consisting of $n$ instances each with $m$ features and a corresponding $n$ node graph (whose adjacency matrix is $\mathbf{A}$) with an edge indicating that the two instances are similar.
