---
title: 2023-01-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RumeliHisari_ZH-CN0185820275_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-01-12 21:53:36
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RumeliHisari_ZH-CN0185820275_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [2023年，关于直播带货的三个思考](https://www.woshipm.com/it/5728965.html)
> 概要: 直播带货赛道正在逐渐挥别“野蛮生长”，这大概是大多数人的共同认知，那么综合来看，告别“野蛮生长”之后，更健康的直播电商生态又应具备着什么样的表现？作为后发者的视频号，又能不能解决后续成长路径上出现的问......
### [微信要把抖音踢出局？视频号和小游戏想“赢家通吃”](https://www.woshipm.com/it/5729301.html)
> 概要: 微信与抖音的小游戏业务博弈，结果如何？这篇文章介绍了小游戏产品在微信和抖音两个平台各自的发展历程，围绕业务迭代、变动对目前的发展情况做出了总结。推荐对游戏营销，微信小游戏运营等感兴趣的童鞋阅读。本文作......
### [群发1000条信息没一个人应答，用户运营的窘境该如何破？](https://www.woshipm.com/operate/5727658.html)
> 概要: 对于用户运营人员而言，需要频繁地与用户进行沟通，但当你的沟通让用户产生了厌烦，转化便会上不去。对于不同的用户，我们需要分层运营。作者分享了相关的用户运营策略，希望能够帮助你解决用户运营上的窘境。说起用......
### [爱奇艺App开始限制电视投屏：黄金VIP只支持480P投屏](https://finance.sina.com.cn/tech/mobile/n/n/2023-01-12/doc-imxzvqme2169860.shtml)
> 概要: 据不少网友反馈，今日爱奇艺 App 开始对投屏功能作出限制，之前黄金 VIP 会员支持最高 4K 清晰度投屏，现在只能选最低的 480P 清晰度，要想进行 4K 投屏必须购买白金 VIP 会员......
### [2022年国内手机市场回顾：寒气传递到每个厂商，华为捅破天](https://finance.sina.com.cn/tech/mobile/n/n/2023-01-12/doc-imxzwrwn9161020.shtml)
> 概要: 一年有春、夏、秋、冬四季，循环交替，有冷有暖，但对于今年的国内手机市场则只有一个字—冷......
### [工信部：明确十三项重点任务，促工业经济平稳增长](https://finance.sina.com.cn/tech/roll/2023-01-12/doc-imxzwrws2100826.shtml)
> 概要: 1月11日，全国工业和信息化工作会议在北京召开。会议指出，2022年工业经济总体回稳向好，预计2022年规模以上工业增加值同比增长3.6%；制造业增加值占GDP比重为28%，比上年提高0.5个百分点......
### [36氪首发 |「大侠找光」平台完成A轮融资，利用数字化手段服务光伏项目落地](https://36kr.com/p/2083578724973056)
> 概要: 36氪首发 |「大侠找光」平台完成A轮融资，利用数字化手段服务光伏项目落地-36氪
### [《JOJO的奇妙冒险》第9部先行图公开 主角形象曝光](https://acg.gamersky.com/news/202301/1556145.shtml)
> 概要: 《JOJO的奇妙冒险》第9部漫画《JOJOLANDS》预览图公开了，主角的形象曝光。之前荒木飞吕彦在最新杂志访谈中透露本作的主角是乔瑟夫·乔斯达的后裔。
### [2022十大值得一看华语剧](https://www.huxiu.com/article/767866.html)
> 概要: 本文来自微信公众号：新周刊 （ID：new-weekly），作者：李路修、许漫漫、曾宝气，头图来自：《人世间》前段时间，我们总结了《2022十大华语片》，而与电影市场的萧条相比，2022年的华语剧甚至......
### [转生和风幻想剧，出师未捷身先死](https://news.dmzj.com/article/76818.html)
> 概要: 这期给大家推荐一部奇幻少年漫《四百四鬼》，作者もち，新双鱼座汉化组汉化，讲述很久很久以前，疾病的原因被称为“鬼”。桃太郎一行前去讨伐鬼，但是等待他们的会是奇妙的际遇的故事。
### [好想你IP形象设计](https://www.zcool.com.cn/work/ZNjM2MzM3Mjg=.html)
> 概要: - 年前最后一发，借作品的大红大黄喜庆之色提前祝酷友们过个好年呀哈哈~- 2022是好是坏已成过往，2023大家一起加油！大展宏“兔”呀~- 这次IP形象设计作品是以好想你的品牌文化“好”字为出发点进......
### [蜂巧资本屠铮：人民币精品VC没有消失](https://36kr.com/p/2084357938690825)
> 概要: 蜂巧资本屠铮：人民币精品VC没有消失-36氪
### [「2022 SHOWREEL」 MOTION DESIGN 个人动态总结](https://www.zcool.com.cn/work/ZNjM2MzQ0NTY=.html)
> 概要: 主要是年中开始的动态周练除最后一Part有2个工作镜头外其它皆为个人原创练习不做商用2023保持输出，谢谢观看~......
### [海外new things | 印度金融技术初创「KreditBee」D+轮融资1亿美元，估值达到6.8亿美元](https://36kr.com/p/2078189640142856)
> 概要: 海外new things | 印度金融技术初创「KreditBee」D+轮融资1亿美元，估值达到6.8亿美元-36氪
### [《大雪海的卡纳》宣传片 动画电影第二季已确定](https://www.3dmgame.com/news/202301/3860387.html)
> 概要: 人气动画《大雪海的卡纳》正在热播中，官方日前公布了最新无字ED宣传片，由GReeeeN演绎的《孩子气》收录其中，目前新动画电影以及TV动画第二季已经确定制作，敬请期待。·《大雪海的卡纳》是日本漫画家弐......
### [「我信跃浪逐新」幸福之窗当红不让，中信银行陪你红红火火迎新年！](http://www.investorscn.com/2023/01/12/105358/)
> 概要: 刚刚举办了「琵琵琶琶新春音乐会」的中信银行,在迎春之际温暖不断,这一次带着「锦鲤」红红火火地来了!中信银行在北京、上海两地三家支行打造了三个地标性的立体艺术橱窗装置——「我信跃浪逐新」幸福之窗,北京更......
### [《一拳超人》重制版222话：吹雪被挟制 龙卷暴怒](https://acg.gamersky.com/news/202301/1556213.shtml)
> 概要: 《一拳超人》重制版222话终于公开了，在英雄协会新大楼的下方，神秘组织月读的人想要带走赛克斯去做研究，吹雪出现阻止，之后龙卷也出现了。
### [《蚁人3》新预告解析：蚁人牺牲？内地引进有戏吗？](https://news.dmzj.com/article/76821.html)
> 概要: 引进，搞快点！
### [剧场版《EVA：终》疑似官宣引进 日本票房突破100亿](https://acg.gamersky.com/news/202301/1556267.shtml)
> 概要: @新世纪福音战士_中国 今天发博：再见了，所有的新世纪福音战士。疑似暗示《新世纪福音战士新剧场版：终》引进内地。
### [视频：王凯回应被催进组 想给顾南亭介绍女朋友](https://video.sina.com.cn/p/ent/2023-01-12/detail-imxzxanh8944592.d.html)
> 概要: 视频：王凯回应被催进组 想给顾南亭介绍女朋友
### [上海市消保委：爱奇艺APP限制投屏做法不厚道](https://ent.sina.com.cn/v/m/2023-01-12/doc-imxzxann2185153.shtml)
> 概要: 据不少网友反馈，爱奇艺App开始对投屏功能作出限制，之前黄金VIP会员支持最高 4K 清晰度投屏，现在只能选最低的480P清晰度，要想进行4K投屏必须购买白金VIP会员。不少网友表示，480P清晰度太......
### [nofans · 2022年度精选集丨品牌x包装x产品x页面设计](https://www.zcool.com.cn/work/ZNjM2MzU5MTY=.html)
> 概要: 2022年，弹指白驹。今年的设计实践与思考，一直在朝着——“真实有效的美学与商业价值平衡的系统性品牌设计”这个目标努力，审慎地对待每一个设计项目。这一年中，在对国内外一些新兴的品牌视觉进行剖析思考中越......
### [金鼎资本：开启中国CVC产业投资时代](https://36kr.com/p/2084564481948163)
> 概要: 金鼎资本：开启中国CVC产业投资时代-36氪
### [《进击的巨人》音乐剧预告公开 现场效果很迷幻](https://acg.gamersky.com/news/202301/1556319.shtml)
> 概要: 《进击的巨人》音乐剧即将于1月14日在日本东京上演，现在官方公开了本作的舞台效果预告，音乐剧的展现形式和舞台布景、灯光相互辉映，展现出一种独特的迷幻色彩。
### [71,941台！极氪001超额完成目标，2023款全能进化，售价30万起至38.6万](http://www.investorscn.com/2023/01/12/105361/)
> 概要: 【2023年1月9日，杭州】 近日，极氪智能科技宣布旗下豪华猎装轿跑——极氪001 2022年累计交付71,941台，并且在12月份持续蝉联“30万以上中国品牌纯电车型销量冠军”。同时，基于SEA浩瀚......
### [商务部答一财：愿为推动国际旅游业发展和世界经济复苏作出积极贡献](https://www.yicai.com/news/101648569.html)
> 概要: 长期以来，中国是全球最主要的入境旅游目的地和出境旅游客源国之一。
### [推特广告收入下跌严重 或考虑出售15亿用户名创收](https://www.3dmgame.com/news/202301/3860425.html)
> 概要: 自从马斯克收购推特以来，这家互联网巨头的日子就不好过了，其广告收入也下跌严重。近日《纽约时报》发布新报告称，推特正在考虑出售用户名，以提高公司的收入。据悉，推特的用户名是@符号后的一串字符，这串字符被......
### [动画导演讨论敏感场景限制 你觉得动画限制放宽了吗？](https://news.dmzj.com/article/76828.html)
> 概要: 前段时间，《机动战士高达 水星的魔女》的“拍番茄”场景引发了玩梗热潮。不过要说去年10月番中的出血量排名，夺得榜首的还要数藤本树的《电锯人》。
### [我国将全面推进6G技术研发 网速比5G提升100倍](https://www.3dmgame.com/news/202301/3860429.html)
> 概要: 据央视新闻报道，工信部最近会议上不仅透露了我国5G的进展，还提到2023年将全面推进6G技术研究。记者从11日在京召开的全国工业和信息化工作会议上获悉，我国累计建成开通5G基站超过230万个，新型数据......
### [智能窗口联动 《海贼王：时光旅诗》奇幻场景欣赏](https://www.3dmgame.com/news/202301/3860431.html)
> 概要: 科技生活新产品“智能窗口”相信已经有不少追求前卫的小伙伴用上了，日前著名窗口型智能显示器 Atmoph Window 2联动 游戏新作《海贼王：时光旅诗》，预定2月发售联动版，可以让用户在家里随时欣赏......
### [寿屋《轨迹》系列蕾恩·布莱特1/8比例手办](https://news.dmzj.com/article/76829.html)
> 概要: 寿屋根据《轨迹》系列中的蕾恩·布莱特制作的1/8比例手办目前正在预订中。本作再现了《闪之轨迹4 -THE END OF SAGA-》中蕾恩·布莱特拿着镰刀的样子。
### [虚构的《噬亡村》，真实的日本食人风俗](https://www.huxiu.com/article/768291.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。最近这一个月，各大流媒体扎堆儿上了挺多好......
### [Canalys：2022 年全球 PC 出货量下降 16% 至 2.85 亿台，联想依然领跑](https://www.ithome.com/0/667/398.htm)
> 概要: IT之家1 月 12 日消息，继 IDC 之后，数据咨询机构 Canalys 同样也公布了其 2022 年全球 PC 报告。分析师指出，全球个人电脑市场在 2022 年结束时表现低迷，第四季度台式机和......
### [宏碁推出新款 Aspire 3 笔记本：搭载 i3-N305 8 小核处理器，480 美元起](https://www.ithome.com/0/667/404.htm)
> 概要: IT之家1 月 12 日消息，宏碁现已率先推出搭载 i3-N305 8 小核处理器的笔记本，型号为 Aspire 3，售价 480 美元（约 3250 元人民币）起。规格方面，宏碁 Aspire 3 ......
### [单片独立包装 + 灭菌版：可孚 N95 口罩 1.2 元 / 片官旗发车](https://lapin.ithome.com/html/digi/667405.htm)
> 概要: 天猫【可孚医疗器械旗舰店】可孚 N95 口罩 30 片 + 10 片日常售价为 83.22 元，下单折后 74.9 元，领取 25 元优惠券，到手价为 49.9 元 40 片，折合每片 1.24 元近......
### [收购威马的Apollo出行什么来历？背后股东有何厚铧、李嘉诚](https://www.yicai.com/news/101648749.html)
> 概要: 收购威马“一战成名”，Apollo出行背后全是“大佬”。
### [“1888万”天价彩礼？当地回应：将发通告！刚刚，在岸人民币突破6.75关口！多股尾盘闪崩，啥情况？](https://finance.sina.com.cn/china/gncj/2023-01-12/doc-imxzxtke2183629.shtml)
> 概要: 来源：证券时报 今日，A股维持震荡整理走势，沪指尾盘翻红，创业板指表现相对强势；恒生指数跟随A股走势，但恒生科技指数表现疲弱，盘中一度跌近3%。
### [B站游戏年度榜单：魂系重启《堕落之主》中字预告](https://www.3dmgame.com/news/202301/3860419.html)
> 概要: 在今天（1 月 12 日）举办的 Bilibili游戏年度排行活动中，2014年发布的魂系游戏《堕落之王》的重启之作《堕落之主》亮相，分享了中字预告。事实上该预告是 12 月份 TGA 年度游戏展上发......
### [2022年最愚蠢的车，让丰田造出来了](https://www.huxiu.com/article/767811.html)
> 概要: 出品丨虎嗅汽车组作者丨周到编辑丨张博文头图丨视觉中国岁末年初，回想过去展望未来，总有一辆车会让你会心一笑。不过也注定有辆车，让你深感愚蠢以至于被逗乐。就像听到了脱口秀大会上王建国的谐音梗那样，在大笑之......
### [等不及的第一批游客，已经飞出国门了](https://www.huxiu.com/article/767993.html)
> 概要: 本文来自微信公众号：财经故事荟（ID：cjgshui），作者：何惜金，编辑：万天南，头图来自：视觉中国2022年12月26日，前出境游领队西西，一夜无眠。这天晚上，国家卫健委宣布，自1月8日起，全国对......
### [梦百合快充床垫和梦百合朗怡床垫究竟有何区别？请收好这份攻略](http://www.investorscn.com/2023/01/12/105372/)
> 概要: 随着消费水平的不断提升，越来越多的消费者，开始逐步关注起“睡眠健康”领域。在这些消费者的观念里，睡眠需求与“衣食住行”等基础需求一样，是每个人不可缺少的一个重要生活要素。倘若睡眠质量无法得到有效提升，......
### [名创优品叶国富内部讲话曝光：市场是抢回来的！](http://www.investorscn.com/2023/01/12/105376/)
> 概要: 据消息人士透露，名创优品集团董事会主席兼首席执行官叶国富在近日的内部会议上号召：“市场是靠我们抢回来的！有成绩是好事，但我们要时刻保持头脑清醒、保持行动上的迅速，要敢打仗，更要打胜仗！”......
### [汪文斌：希望世卫组织能够科学理性判断中方防控成效，客观公正对外发声](https://finance.sina.com.cn/jjxw/2023-01-12/doc-imxzxtke2231565.shtml)
> 概要: 2023年1月12日外交部发言人汪文斌主持例行记者会，以下为部分实录。 日本共同社记者：世界卫生组织总干事谭德塞11日在记者会上呼吁各国报告更加详细可靠的数据。
### [联想拯救者刃 9000K&7000K 官宣首批搭载 4070 Ti 显卡](https://www.ithome.com/0/667/422.htm)
> 概要: IT之家1 月 12 日消息，联想拯救者官方今日宣布，拯救者刃 9000K&7000K 首批搭载 4070 Ti 显卡，可选配置持续扩展。IT之家了解到，RTX 4070 Ti 显卡拥有 7680 C......
### [郑嘉颖官宣三胎得子：谢谢大家的关心，母子平安](https://ent.sina.com.cn/s/h/2023-01-12/doc-imxzxtkm2499233.shtml)
> 概要: 新浪娱乐讯 12日，郑嘉颖发微博官宣三胎得子，“谢谢大家的关心，母子平安”。今日早前，港媒爆料郑嘉颖老婆陈凯琳昨天入院，生下三胎儿子。据悉，两人于2018年8月在巴厘岛完婚，2019年2月大儿子出生，......
### [国务院国资委召开地方国资委负责人会议 深化国资国企改革推进高质量发展](https://finance.sina.com.cn/china/2023-01-12/doc-imxzxtke2255226.shtml)
> 概要: 国务院国资委召开地方国资委负责人会议 深化国资国企改革推进高质量发展 为全面建设社会主义现代化国家开好局起好步作出新贡献
### [国资委：2023年做好债务、房地产、金融、投资、安全环保等重点领域风险防控 牢牢守住不发生重大风险的底线](https://finance.sina.com.cn/china/2023-01-12/doc-imxzxtkm2501327.shtml)
> 概要: 国务院国资委召开地方国资委负责人会议 深化国资国企改革推进高质量发展 为全面建设社会主义现代化国家开好局起好步作出新贡献
### [银行理财不一样的开门红：今年投资者转而求稳](https://www.yicai.com/news/101648882.html)
> 概要: 在前两年，开门红期间办理不仅能申购到收益率更高的产品，还会有额外小礼品。
### [A股还能反弹吗？](https://www.yicai.com/video/101648912.html)
> 概要: A股还能反弹吗？
### [来啦！北京经开区即将发放2023年汽车消费补贴](https://finance.sina.com.cn/jjxw/2023-01-12/doc-imxzxxsc8983545.shtml)
> 概要: 来源：亦庄发布 2022年12月31日24点，经开区“好车亦城 ”消费券携近22亿元亮眼成绩单完美收官！ 为进一步巩固“好车亦城”消费券带来的良好消费势头...
### [万纬物流与厦门建发股份有限公司签署战略合作协议](http://www.investorscn.com/2023/01/12/105379/)
> 概要: 1月10日，万纬物流与厦门建发股份有限公司（以下简称“建发股份”）在厦门建发国际大厦举行战略合作协议签约仪式......
### [流言板高登投篮26中10，全场得到28分4篮板2助攻](https://bbs.hupu.com/57380460.html)
> 概要: 虎扑01月12日讯 2022-23赛季CBA常规赛第25轮，广东队123-96战胜北控队。此役北控队外援高登出场35分钟，投篮26中10，其中三分9中4，罚球5中4，得到28分4篮板2助攻。   来源
### [投屏看内容先交钱，收费、规则层层“套娃”](https://www.yicai.com/news/101648922.html)
> 概要: 电视端与投屏需另外付费此次引发网友大规模质疑。在行业人士看来，涉及电视端的内容播放，主要受监管层面因素影响。
### [流言板爱德华兹：我们幻想活塞会拱手送出胜利](https://bbs.hupu.com/57380559.html)
> 概要: 虎扑01月12日讯 森林狼以118-135不敌活塞。赛后，森林狼球员安东尼-爱德华兹接受了记者的采访。活塞目前联盟战绩仅为12胜33负，为东部倒数第二。而森林狼20胜22负，为西部第十。对此，爱德华兹
### [TES战队季前启航赛Vlog：尽兴战斗，欢乐启航](https://bbs.hupu.com/57380595.html)
> 概要: 滴滴，季前启航赛Vlog请查收；尽兴战斗，欢乐启航！ 来源：  新浪微博
### [赛场速递关键先生！王一方连续两记抛投稳定优势](https://bbs.hupu.com/57380597.html)
> 概要: 赛场速递关键先生！王一方连续两记抛投稳定优势
# 小说
### [我成了修真界扛把子](http://book.zongheng.com/book/1062318.html)
> 作者：麻黄桂枝

> 标签：奇幻玄幻

> 简介：如果有一天你发现自己并不是现代人，而是被某一个古老的大陆利用法术转移过来的，那么回到这片大陆，你会如何思考自己接下来的人生？如果是大陆上所有的大佬都是你的靠山，然而，你自己是一个超级大佬，你会如何选择接下来的道路，是选择成群美男、过上快活似神仙的日子，还是在异世称王称霸？一朝特种兵，突然被打破三观，被告知世界上还有其他大陆的存在，并且这个大陆还是玄幻那一卦的，她该如何处之？前替身留下来的渣男茶女，小问题，轻松收拾前替身留下来的废柴形象，小问题，轻松改写前替身留下来的腹黑王爷，小...额，这并不是小问题，也不是前替身留下来的，那还是看看、看看再说吧！

> 章节末：帝425章  大结局下

> 状态：完本
# 论文
### [Comparing the Utility and Disclosure Risk of Synthetic Data with Samples of Microdata | Papers With Code](https://paperswithcode.com/paper/comparing-the-utility-and-disclosure-risk-of)
> 日期：2 Jul 2022

> 标签：None

> 代码：https://github.com/clairelittle/psd2022-comparing-utility-risk

> 描述：Most statistical agencies release randomly selected samples of Census microdata, usually with sample fractions under 10% and with other forms of statistical disclosure control (SDC) applied. An alternative to SDC is data synthesis, which has been attracting growing interest, yet there is no clear consensus on how to measure the associated utility and disclosure risk of the data. The ability to produce synthetic Census microdata, where the utility and associated risks are clearly understood, could mean that more timely and wider-ranging access to microdata would be possible. This paper follows on from previous work by the authors which mapped synthetic Census data on a risk-utility (R-U) map. The paper presents a framework to measure the utility and disclosure risk of synthetic data by comparing it to samples of the original data of varying sample fractions, thereby identifying the sample fraction which has equivalent utility and risk to the synthetic data. Three commonly used data synthesis packages are compared with some interesting results. Further work is needed in several directions but the methodology looks very promising.
### [LAMDA-SSL: Semi-Supervised Learning in Python | Papers With Code](https://paperswithcode.com/paper/lamda-ssl-semi-supervised-learning-in-python)
> 日期：9 Aug 2022

> 标签：None

> 代码：https://github.com/ygzwqzd/lamda-ssl

> 描述：LAMDA-SSL is open-sourced on GitHub and its detailed usage documentation is available at https://ygzwqzd.github.io/LAMDA-SSL/. This documentation introduces LAMDA-SSL in detail from various aspects and can be divided into four parts. The first part introduces the design idea, features and functions of LAMDA-SSL. The second part shows the usage of LAMDA-SSL by abundant examples in detail. The third part introduces all algorithms implemented by LAMDA-SSL to help users quickly understand and choose SSL algorithms. The fourth part shows the APIs of LAMDA-SSL. This detailed documentation greatly reduces the cost of familiarizing users with LAMDA-SSL toolkit and SSL algorithms.
