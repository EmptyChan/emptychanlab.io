---
title: 2022-07-21-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.AbbeyGardens_ZH-CN4831631801_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-21 22:24:47
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.AbbeyGardens_ZH-CN4831631801_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [TikTok网红营销干货：Description描述文案怎么写更容易爆？](https://www.woshipm.com/copy/5533465.html)
> 概要: 编辑导语：一个爆款短视频的爆火，离不开一个好的Description（描述）。本文作者分享了写Tiktok描述文案的具体思路方法，列举了七个写描述文案的技巧，感兴趣的小伙伴一起来看看吧。在TikTok......
### [网易云音乐｜情感驱动设计的制胜法宝之产品体验](https://www.woshipm.com/pd/5532617.html)
> 概要: 编辑导语：情感驱动设计对于用户体验来说十分重要，能有效增加用户粘性和提高用户留存率。本文作者以网易云音乐这款产品为案例，分析了其情感驱动设计方面的产品体验，列举具体案例讲述了其吸引用户的设计点等，感兴......
### [互联网大厂为何都爱“送外卖”？](https://www.woshipm.com/it/5533056.html)
> 概要: 编辑导语：随着外卖行业的不断发展壮大，点外卖已然成为很多人的用餐方式，互联网大厂纷纷涌入外卖赛道，开展外卖业务。本文作者分析了互联网大厂爱“送外卖”的原因，讲述了当下外卖行业的发展以及各大厂之间的竞争......
### [【JS 逆向百例】某公共资源交易网，公告 URL 参数逆向分析](https://segmentfault.com/a/1190000042200757)
> 概要: 声明本文章中所有内容仅供学习交流，抓包内容、敏感网址、数据接口均已做脱敏处理，严禁用于商业用途和非法用途，否则由此产生的一切后果均与作者无关，若有侵权，请联系我立即删除！逆向目标目标：某地公共资源交易......
### [经济日报评论：收购魅族吉利不只是造手机](https://finance.sina.com.cn/tech/it/2022-07-21/doc-imizmscv2840835.shtml)
> 概要: 来源：经济日报......
### [恒大造车，真的太可笑了](https://www.huxiu.com/article/613572.html)
> 概要: 出品｜虎嗅汽车组作者｜周到编辑｜张博文头图｜视觉中国每当你觉得这个世界已经足够魔幻的时候，总有人翻着跟头蹦出来整活。7月20日晚6点，隶属于恒大新能源汽车投资控股集团的恒驰汽车，在线上召开了首届“恒驰......
### [高管解读](https://finance.sina.com.cn/tech/it/2022-07-21/doc-imizirav4724383.shtml)
> 概要: 相关新闻：......
### [50个虚拟人只要599元，“捏脸”大军抢占虚拟人](https://finance.sina.com.cn/tech/internet/2022-07-21/doc-imizirav4730740.shtml)
> 概要: 文/谭丽平 陈睿雅......
### [腾讯将关闭“幻核”：上线未满一年，为国内最大数字藏品平台之一](https://finance.sina.com.cn/tech/internet/2022-07-21/doc-imizmscv2858361.shtml)
> 概要: 记者/司林威......
### [深圳枭瞳参与制作《曙光英雄》新解禁9P](https://www.zcool.com.cn/work/ZNjEwMTU0MTY=.html)
> 概要: 深圳枭瞳参与制作《曙光英雄》新解禁9P
### [贰婶手写--奇妙的中国汉字【拙字集陆】](https://www.zcool.com.cn/work/ZNjEwMTU5MjQ=.html)
> 概要: 贰婶手写--奇妙的中国汉字【拙字集陆】......
### [动画「新米炼金术师的店铺经营」第二弹PV及主视觉图公开](http://acg.178.com/202207/452370727604.html)
> 概要: 电视动画「新米炼金术师的店铺经营」发布了第二弹PV及主视觉图，本作将于2022年10月开始播出。「新米炼金术师的店铺经营」第二弹PV主视觉图：MUSIC片头曲：大西亜玖璃「はじまるウェルカム」片尾曲：......
### [组图：59岁叶童携老公参加富豪聚会 久违现身剪超短发皱纹明显](http://slide.ent.sina.com.cn/star/slide_4_86512_372890.html)
> 概要: 组图：59岁叶童携老公参加富豪聚会 久违现身剪超短发皱纹明显
### [动画电影《二郎神之深海蛟龙》先导预告 近期上映](https://www.3dmgame.com/news/202207/3847448.html)
> 概要: 今日（7月21日），由王君执导的《二郎神之深海蛟龙》曝光先导预告，讲述封神之战后，封神榜中还封缄着众多强大的妖邪元神，妄图窃取封神榜，因此一场围绕着二郎神杨戬的惊天阴谋就此展开。本片有望于近期上映。预......
### [《马的故事：翡翠谷牧场》上线Steam 11月4日发售](https://www.3dmgame.com/news/202207/3847464.html)
> 概要: 由aesir interactive开发的开发世界动作冒险游戏《马的故事：翡翠谷牧场》现已上线Steam，预计于11月4日发售，支持中文。游戏中，玩家将踏上迷人的骑马探险之旅，探索翡翠谷的奥秘！游戏预......
### [Edison Chen Koon Hei (Fan art)](https://www.zcool.com.cn/work/ZNjEwMTc5MjA=.html)
> 概要: Here are some renders of a 3D portrait exercise I was doing of Edison ChenI hope you like it......
### [蕉绿象Panana-怪诞马戏](https://www.zcool.com.cn/work/ZNjEwMTc5NjA=.html)
> 概要: 几个月前，小象莫莉的事情牵动全网千万人的心，如今，经过大家的不懈努力，小象莫莉已经平安回家。本该无忧无虑快乐成长的年纪，却被带上枷锁，学习各种本不属于它的表演，这只小象莫莉得救了，那更多的莫莉呢？Pa......
### [组图：李钟硕最新简历照公开 多个风格彰显帅气形象](http://slide.ent.sina.com.cn/star/k/slide_4_704_372892.html)
> 概要: 组图：李钟硕最新简历照公开 多个风格彰显帅气形象
### [「伊甸星原」漫画第200话纪念杂志彩页公开](http://acg.178.com/202207/452373280319.html)
> 概要: 杂志「周刊少年Magazine」公开了漫画「伊甸星原」第200话的纪念彩页，本作改编动画第二季目前正在制作中。「伊甸星原」（EDENS ZERO）是日本漫画家、「妖精的尾巴」作者·真岛浩创作的少年漫画......
### [森下suu发布为「5分後にときめくラスト」绘制封面](http://acg.178.com/202207/452373361594.html)
> 概要: 日本漫画家、「日日蝶蝶」的作者·森下suu发布了为爱情故事短篇小说集「5分後にときめくラスト」绘制的封面图。该小说集收录了发布在小说投稿网站「エブリスタ」上的作品中的爱情故事，由河出书房新社出版发行，......
### [TV动画《大雪海的凯纳》公开PV](https://news.dmzj.com/article/75005.html)
> 概要: TV动画《大雪海的凯纳》公开了一段PV。在这次的PV中，可以看到男女主角的相遇，以及介绍世界观的场景等内容。
### [寺川爱美单曲「かかった魔法はアマノジャク」Live影像公开](http://acg.178.com/202207/452376508299.html)
> 概要: 近日，知名声优歌手寺川爱美公开了单曲「かかった魔法はアマノジャク」的Live影像，系列专辑「AIMI SOUND」普通版定价3300日元，已于7月13日正式发售。「かかった魔法はアマノジャク」Live......
### [TikTok力不从心？](https://www.huxiu.com/article/613528.html)
> 概要: 出品 | 虎嗅商业消费组作者 | 周月明编辑 | 苗正卿题图 | 视觉中国“我们已向官方内部高层求证，TikTok不会收缩欧美地区业务，但确实需要重组。”国内最早一批入局TikTok的资深玩家林令告诉......
### [P站美图推荐——玫瑰特辑](https://news.dmzj.com/article/75007.html)
> 概要: “最初的玫瑰在苏醒/它们的芬芳是畏葸的/如一丝寂寂悄悄的笑。”
### [New Lisp-Stat Release](https://lisp-stat.dev/blog/releases/)
> 概要: New Lisp-Stat Release
### [西班牙大学推免费VR游戏 虚拟空间随意组装电脑](https://www.3dmgame.com/news/202207/3847479.html)
> 概要: 如何更加直观高效的学会组装电脑？VR虚拟模拟显然是个好办法，近日来自西班牙的布尔戈斯大学研发团队宣布免费推出VR游戏《PC Virtual LAB》，可以让玩家在VR空间里毫无顾忌的练习组装电脑。•《......
### [街霸诞生35周年开发者访谈 春丽大粗腿紧身裤秘话](https://www.3dmgame.com/news/202207/3847487.html)
> 概要: 格斗游戏史上最有名作品之一的《街霸》今年迎来诞生35周年，日媒采访了当年的制作者，谈到了不少开发秘闻，一起来学习下。·源起。系列主人公隆与拳的三大必杀技人尽皆知，实际上在1987年的初代时就已经登场，......
### [前海人寿、宝能系“翻脸”，南玻A控制权之争再起](https://www.yicai.com/news/101481544.html)
> 概要: 无论是是陈琳、程细宝等人，还是前海人寿，实际上都是“一家人”。
### [一文读懂 BizDevOps：数字化转型下的技术破局](https://www.tuicool.com/articles/BZjaueU)
> 概要: 一文读懂 BizDevOps：数字化转型下的技术破局
### [网友评选偶像级可爱女声优 伊藤美来排名第一](https://news.dmzj.com/article/75010.html)
> 概要: 与过去只活跃于幕后不同，如今的声优除了需要为角色配音，来到前台演出等也成为了必修课。既然要登台，相貌自然也随之成为了影响声优人气的原因之一。近日有日本网友，就对“偶像级可爱女声优”进行了人气投票。
### [《一拳超人》重制版新213话：众神觉醒 认真打喷嚏](https://acg.gamersky.com/news/202207/1501643.shtml)
> 概要: 《一拳超人》重制版新213话公开，生气了的琦玉开始认真殴打饿狼，这一话的剧情实在是高能，大家还是来看看吧。
### [商务部：中韩自贸协定第二阶段谈判取得实质性进展](https://www.yicai.com/news/101481632.html)
> 概要: 中韩以负面清单模式开展高水平服务贸易和投资自由化磋商。
### [格林循环：材料循环技术创新生力军](http://www.investorscn.com/2022/07/21/101963/)
> 概要: 据了解，江西格林循环产业股份有限公司（下称“格林循环”）IPO申报已于2021年7月23日获深圳证券交易所受理。拟发行不超过，32,110.6940万股，拟募资投资年拆解 15 万吨“非补贴类”电子电......
### [光源资本许银川：“替代、降本、创新”，中国半导体设备材料行业国产化的“三阶梯”](http://www.investorscn.com/2022/07/21/101965/)
> 概要: 随着下游半导体需求增长和国产替代进程加快，中国半导体设备和材料行业正进入黄金发展期。本篇文章概括了光源资本许银川在半导体设备材料行业的投资观点，许银川认为，半导体行业增长速度受到国产化渗透率、新增产能......
### [《电锯人》官方微博正式开通 动画预计今年年内播出](https://acg.gamersky.com/news/202207/1501737.shtml)
> 概要: 今日，《电锯人》官方微博正式开通，官方账号目前尚未发布任何内容。
### [一个韩国邪教，为何能害死安倍？](https://www.huxiu.com/article/613754.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。“为什么我们的媒体不敢......
### [中国绿色领军企业——格林美，即将登陆瑞交所](http://www.investorscn.com/2022/07/21/101967/)
> 概要: 中国绿色领军企业——格林美，即将登陆瑞交所
### [国产 RPG 游戏《昭和米国物语》开发商获美国公司 A 轮融资，将开设新工作室](https://www.ithome.com/0/630/842.htm)
> 概要: IT之家7 月 21 日消息，国产游戏工作室“铃空游戏”的 RPG 游戏《昭和米国物语》此前凭借浓烈的 B 级片风格、另类的主题吸引了不少人的目光，该游戏的背景设定在了沦为日本经济和文化“殖民地”的美......
### [网红书店倒闭潮下，谈谈日本茑屋书店的生意经](https://www.tuicool.com/articles/ZbQNZfq)
> 概要: 网红书店倒闭潮下，谈谈日本茑屋书店的生意经
### [歌剧《樱兰高校男公关部ƒ》12月上演](https://news.dmzj.com/article/75013.html)
> 概要: 根据叶鸟螺子原作改编的歌剧《樱兰高校男公关部ƒ》宣布了将于12月在日本的东京和大阪上演。本作的宣传图与角色定妆照也一并公开。
### [组图：汤唯暗黑系大片冷艳高级 湿发造型出镜散发迷人气质](http://slide.ent.sina.com.cn/star/slide_4_86512_372915.html)
> 概要: 组图：汤唯暗黑系大片冷艳高级 湿发造型出镜散发迷人气质
### [百亿加密货币迎来“做空阻击战”，嗜血鲨群难逃“铁壁铜墙”](https://www.tuicool.com/articles/FnAZjmy)
> 概要: 百亿加密货币迎来“做空阻击战”，嗜血鲨群难逃“铁壁铜墙”
### [一天新增114万例，奥密克戎是最“毒”病毒吗？](https://www.huxiu.com/article/613863.html)
> 概要: 出品丨虎嗅医疗组作者丨苏北佛楼蜜题图丨视觉中国4月23日，一架从肯尼亚内罗毕起飞的国际航班KQ880抵达广东省广州市白云国际机场，新的变体随着航班登陆国土。在所有旅客均被转移至隔离酒店进行14天常规医......
### [贝特瑞董事长涉嫌内幕交易，50亿定增黄了？年薪765万，身家近4亿](https://www.tuicool.com/articles/ziMnE3n)
> 概要: 贝特瑞董事长涉嫌内幕交易，50亿定增黄了？年薪765万，身家近4亿
### [上汽集团：智己 L7 电动汽车开启批量交付，预计月底将突破 1000 台，售价 36.88 万元起](https://www.ithome.com/0/630/876.htm)
> 概要: IT之家7 月 21 日消息，据上汽集团发布，今日，智己 L7 新车批量抵达全国智己汽车体验及交付服务中心，进入用户批量加速交付。预计至 7 月底，智己 L7 交付量将突破 1000 台；8 月份的交......
### [华硕 ProArt 创 16 2022 笔记本开卖：i7-12700H + RTX 3060，9999 元](https://www.ithome.com/0/630/881.htm)
> 概要: IT之家7 月 21 日消息，本月早些时候，华硕发布了 ProArt 创 16 2022，搭载了 12 代酷睿 H 系列处理器和 RTX 30 系列显卡，今晚 8 点正式开卖。IT之家了解到，华硕 P......
### [A Crack in the Linux Firewall](https://www.randorisec.fr/crack-linux-firewall/)
> 概要: A Crack in the Linux Firewall
### [衰退交易来去匆匆，全球通胀尚未见顶](https://www.yicai.com/news/101481959.html)
> 概要: 年初至今，全球金融市场经历了通胀交易、滞胀交易、再到衰退交易，但通胀高烧不退贯穿始终。
### [以房换房、半价买房、稳赚不赔，深圳楼盘营销好拼！](https://www.yicai.com/news/101481993.html)
> 概要: 继“以房换房”之后，深圳非商品住宅性质的居住型产品再次打出“稳赚不赔”的广告。
### [Notation as a Tool of Thought](https://www.jsoftware.com/papers/tot.htm)
> 概要: Notation as a Tool of Thought
### [财政部等集中公开2021年度部门决算](https://finance.sina.com.cn/jjxw/2022-07-21/doc-imizmscv2959219.shtml)
> 概要: 中国财政部等中央部门7月21日集中公开2021年度部门决算。这是中央部门连续第12年向社会公开部门决算。 今年中央部门决算公开内容包括部门概况、部门决算表...
### [景俊海：切实提高科学精准防控水平 以最短时间最小成本控制疫情](https://finance.sina.com.cn/china/2022-07-21/doc-imizirav4839488.shtml)
> 概要: 7月21日，省委书记景俊海以视频形式主持召开省疫情防控领导小组会议。他强调，要深入贯彻习近平总书记关于高效统筹疫情防控和经济社会发展的重要讲话重要指示精神...
### [外交部：中欧班列的“朋友圈”持续扩大](https://finance.sina.com.cn/china/2022-07-21/doc-imizmscv2960366.shtml)
> 概要: 参考消息网7月21日报道 据“外交部发言人办公室”微信公众号消息，在7月21日外交部例行记者会上，有记者提问：近日，中国首条中欧班列线路开行破万列。
### [有基础病接种新冠疫苗安全吗？中疾控专家释疑老年人接种热点问题](https://www.yicai.com/news/101482104.html)
> 概要: 接种单位规范操作也是安全的重要保证，做到健康状况询问和接种禁忌核查、知情告知和接种后的留观等工作。
### [Amazon to Acquire One Medical](https://press.aboutamazon.com/news-releases/news-release-details/amazon-and-one-medical-sign-agreement-amazon-acquire-one-medical/)
> 概要: Amazon to Acquire One Medical
### [广州：独生子女父母计划生育奖励继续实行](https://finance.sina.com.cn/china/gncj/2022-07-21/doc-imizmscv2966246.shtml)
> 概要: 文/羊城晚报全媒体记者 林清清 7月21日，记者从广州市政府官网获悉，广州市人民政府办公厅近日印发《广州市计划生育奖励和特别扶助办法》（以下简称《办法》）...
### [季报揭晓最大公募REITs盈利能力，中交REIT 79天创收超1亿元](http://www.investorscn.com/2022/07/21/101968/)
> 概要: 随着基金二季报陆续披露，一些备受市场关注的基金产品也迎来盈利能力的检验。7月21日，华夏基金披露华夏中国交建REIT(场内简称：中交REIT508018)二季报，这只首募曾吸金超1500亿元（比例配售......
### [普及介入诊疗促新疆医疗水平高质量发展,应人民诉求护生命安康](http://www.investorscn.com/2022/07/21/101970/)
> 概要: 普及介入诊疗促新疆医疗水平高质量发展,应人民诉求护生命安康
### [美剧《哥斯拉大战泰坦》新卡司公布](https://www.3dmgame.com/news/202207/3847512.html)
> 概要: Kurt Russel和他的儿子Wyatt Russel将加盟苹果和传奇影业即将到来的真人剧《哥斯拉大战泰坦（Godzilla and the Titans）》。据Deadline报道，Kurt和Wy......
### [银保监会：河南、安徽5家村镇银行客户资金垫付正在稳步推进中](https://finance.sina.com.cn/jjxw/2022-07-21/doc-imizmscv2964998.shtml)
> 概要: 在21日的新闻发布会上，银保监会相关负责人表示，河南、安徽5家村镇银行账外业务客户资金垫付工作正在稳步推进中。 银保监会新闻发言人 綦相：河南...
### [1017次封禁！7月20日篮球板块被封禁用户公示](https://bbs.hupu.com/54846104.html)
> 概要: 7月20日篮球版块（NBA+CBA）共1017次封禁，其中304次封禁时长为3天及以上。小助手注意到很多JR被封禁是因为跨区嘲讽引战，球队专区为分区球迷聚集地，千万不要跨区违规噢！各位JR看球时要保持
### [流言板孔德或住在卢卡库空出的房子里；加泰媒体坚称巴萨还有机会](https://bbs.hupu.com/54846135.html)
> 概要: 虎扑07月21日讯 根据Goal.com西班牙分部记者Manolo Nieto和Francisco Rico的联合报道，塞维利亚和切尔西在孔德同意的情况下进行了进一步的接触，而孔德也没有随队前往季前赛
### [六合一像素？三星“Hexa²pixel”商标曝光，消息称用于 Galaxy S23 Ultra 影像部分](https://www.ithome.com/0/630/907.htm)
> 概要: IT之家7 月 21 日消息，三星的下一代旗舰手机 Galaxy S23 系列有望迎来相机升级，尤其是超大杯 Galaxy S23 Ultra 预计会将最新的光学技术引入其摄像头部分。根据 TM Vi......
### [流言板哈姆：沃格尔是现象级的教练，上赛季的阵容缺乏运动能力](https://bbs.hupu.com/54846200.html)
> 概要: 虎扑07月21日讯 湖人主帅达尔文-哈姆近日接受了专访。谈到球队新赛季的阵容变化，哈姆说：“看看上赛季的阵容吧。我绝没有不尊重弗兰克（沃格尔）。我曾经在园区里和弗兰克相处了一段时间，也对他有所了解。他
### [流言板米兰正式知会法兰克福将开启恩迪卡谈判，预计不低于2000万](https://bbs.hupu.com/54846207.html)
> 概要: 虎扑07月21日讯 根据德国媒体图片报法兰克福跟队记者的联合报道—周三的问题是：恩迪卡和AC米兰有什么关系吗？但到了周四：嗨还真有！图片报的消息源透露，AC米兰高层已经正式通知法兰克福体育总监克洛舍他
### [大猿魂，有没有一种可能，敖雪的母亲才是无敌之龙？](https://new.qq.com/omn/20220721/20220721A0BDIQ00.html)
> 概要: 据小K得到的消息，在54话时古龙自爆牺牲了，也不知道是不是真的，小K没有去考证，如果是这样的话，那么剧情的走向会有所改变。之前有人说无敌之龙指的是年轻时的古龙，确实这个概率非常高，看看古龙现在的身体颜......
# 小说
### [女博士的贴身保镖](http://book.zongheng.com/book/1035042.html)
> 作者：烨子苏

> 标签：都市娱乐

> 简介：：华夏神秘组织龙魂小队的队长，一代兵王，曾为华夏国完成过多个超高难度级别的任务。后因触犯禁忌被踢出小队，执行特殊任务，实力高强，性格坚韧，爱国，玩世不恭的外表下有着一颗重情义的心。

> 章节末：第824章 See You Again

> 状态：完本
# 论文
### [End-to-End Image-Based Fashion Recommendation | Papers With Code](https://paperswithcode.com/paper/end-to-end-image-based-fashion-recommendation)
> 概要: In fashion-based recommendation settings, incorporating the item image features is considered a crucial factor, and it has shown significant improvements to many traditional models, including but not limited to matrix factorization, auto-encoders, and nearest neighbor models. While there are numerous image-based recommender approaches that utilize dedicated deep neural networks, comparisons to attribute-aware models are often disregarded despite their ability to be easily extended to leverage items' image features. In this paper, we propose a simple yet effective attribute-aware model that incorporates image features for better item representation learning in item recommendation tasks. The proposed model utilizes items' image features extracted by a calibrated ResNet50 component. We present an ablation study to compare incorporating the image features using three different techniques into the recommender system component that can seamlessly leverage any available items' attributes. Experiments on two image-based real-world recommender systems datasets show that the proposed model significantly outperforms all state-of-the-art image-based models.
### [Tevatron: An Efficient and Flexible Toolkit for Dense Retrieval | Papers With Code](https://paperswithcode.com/paper/tevatron-an-efficient-and-flexible-toolkit)
> 概要: Recent rapid advancements in deep pre-trained language models and the introductions of large datasets have powered research in embedding-based dense retrieval. While several good research papers have emerged, many of them come with their own software stacks. These stacks are typically optimized for some particular research goals instead of efficiency or code structure. In this paper, we present Tevatron, a dense retrieval toolkit optimized for efficiency, flexibility, and code simplicity. Tevatron provides a standardized pipeline for dense retrieval including text processing, model training, corpus/query encoding, and search. This paper presents an overview of Tevatron and demonstrates its effectiveness and efficiency across several IR and QA data sets. We also show how Tevatron's flexible design enables easy generalization across datasets, model architectures, and accelerator platforms(GPU/TPU). We believe Tevatron can serve as an effective software foundation for dense retrieval system research including design, modeling, and optimization.
