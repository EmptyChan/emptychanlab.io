---
title: 2020-12-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeaFort_EN-CN8553015223_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-23 22:16:47
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeaFort_EN-CN8553015223_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [邓伦回应粉丝不喜欢郭敬明:没有能力去左右的事情](https://ent.sina.com.cn/s/m/2020-12-23/doc-iiznezxs8461702.shtml)
> 概要: 新浪娱乐讯 邓伦在参加综艺时，首度回应了粉丝不喜欢郭敬明一事。当主持人问道“如果粉丝因为不喜欢郭导而不去看《晴雅集》，你有什么话想对他们说”。　　邓伦回应称，“这是大家的角度和看法不同，不管大家被什么......
### [组图：《最初的相遇，最后的别离》将收官 林更新盖玥希终破悬案](http://slide.ent.sina.com.cn/slide_4_86512_350163.html)
> 概要: 组图：《最初的相遇，最后的别离》将收官 林更新盖玥希终破悬案
### [曼走不送！张颜齐休息室门口贴奥特曼当门神](https://ent.sina.com.cn/y/yneidi/2020-12-23/doc-iiznctke8047785.shtml)
> 概要: 新浪娱乐讯 12月23日上午，张颜齐微博秀出录制综艺节目时，在休息室门口贴的奥特曼吐槽表情包，玩梗说道：“奥特曼当门神，保不了平安发财，但能保证吐槽于无形”。粉丝也纷纷调侃道：“想你了奥特曼大侠”，“......
### [视频：被指送女儿名牌包？林心如晒表情包回应“是我的”](https://video.sina.com.cn/p/ent/2020-12-23/detail-iiznezxs8413238.d.html)
> 概要: 视频：被指送女儿名牌包？林心如晒表情包回应“是我的”
### [组图：林志玲红色薄纱裙秀玲珑曲线 对镜比心甜美可人](http://slide.ent.sina.com.cn/star/slide_4_704_350179.html)
> 概要: 组图：林志玲红色薄纱裙秀玲珑曲线 对镜比心甜美可人
### [洗米嫂为公公庆82岁大寿，戴名贵珠宝满场跑，懒理刘碧丽生第4胎](https://new.qq.com/omn/20201223/20201223A0HZEZ00.html)
> 概要: 12月23日，洗米嫂陈慧玲在个人社交账号上晒出一组生日会的照片。原来，这是她公公“波哥”，也就是洗米华父亲的82岁大寿。夫妻俩特意办了一场隆重的寿宴为其庆祝。            曝光的照片中，洗米......
### [湖南卫视声明回应争议，反对主持人收礼物，何炅多年人设恐崩掉？](https://new.qq.com/omn/20201223/20201223A0JRW300.html)
> 概要: 湖南卫视作为国内综艺大户，推出多档知名综艺节目，像《快乐大本营》以及《天天向上》这两档节目影响了很多青少年的成长，不得不说湖南卫视芒果台在综艺节目方面确实做的很不错。随着综艺节目的走红同时也走红了很多......
### [14岁李嫣扎双麻花辫，清纯可人有少女感，读全英文书籍秀画功！](https://new.qq.com/omn/20201223/20201223A0HWZW00.html)
> 概要: 今天，娱乐圈的瓜吃了一波又一波，最大的瓜就是周迅和高圣远离婚的消息。就在数月前，李亚鹏和小19岁女友海哈金喜恋情曝光，娱乐真是个圈，周迅离婚了，前男友李亚鹏找到新欢，二人还已见了家长。李亚鹏在北京多次......
### [武艺晒自拍后秒删，还同时晒出快本送的行李箱，里面装满了礼物](https://new.qq.com/omn/20201223/20201223A0JVBS00.html)
> 概要: 说到武艺相信大家并不陌生，毕竟武艺不仅是一位歌手还是一位演员和主持人，仅仅才30岁的他可以说在娱乐圈也是混的非常不错的艺人了，尤其是当年他参加湖南卫视的选秀节目《快乐男声》给观众和粉丝留下了深刻的印象......
### [《小红花》预售超过5000万，谋女郎热度不如千玺，粉丝为爱捐发](https://new.qq.com/omn/20201223/20201223A0JUHS00.html)
> 概要: 易烊千玺、刘浩存领衔主演，韩延执导的电影《送你一朵小红花》截至目前预售票房已经超过5000万，而整部影片最吸引观众的就是易烊千玺饰演的男孩韦一航。            《送你一朵小红花》预售破500......
# 动漫
### [剧场版《美少女战士Eternal》前篇公开角色PV！](https://news.dmzj.com/article/69682.html)
> 概要: 剧场版动画《美少女战士Eternal》前篇公开角色PV，将于2021年1月8日在日本上映。
### [P站美图推荐——银发美少年特辑](https://news.dmzj.com/article/69678.html)
> 概要: 我不是针对某一个人，我是说在座的各位都是白毛控！（再次）
### [真嗣家今天的饭](https://news.dmzj.com/article/69675.html)
> 概要: ​是美食番。都是美食番。都可以是美食番。
### [TV动画《EX-ARM》新PV！1月10日开始播出](https://news.dmzj.com/article/69683.html)
> 概要: 根据HiRock、古味慎也原作制作的TV动画《EX-ARM》公开了新的PV。在这段PV中，使用了Dizzy Sunfist演唱的ED主题曲《Diamonds Shine》。另外还可以听到部分主要角色语音，并看到多段动作场景。
### [美图欣赏：孙允珠，烟蓝色忧郁魅力束腰经典衣裙](https://new.qq.com/omn/20201223/20201223A0J2JO00.html)
> 概要: 美图欣赏：孙允珠，烟蓝色忧郁魅力束腰经典衣裙......
### [美图欣赏：孙允珠，烟火初杏梅雨淅沥彩绣古典裙](https://new.qq.com/omn/20201223/20201223A0J1VV00.html)
> 概要: 美图欣赏：孙允珠，烟火初杏梅雨淅沥彩绣古典裙......
### [和小动物一起温暖过冬！《水豚汤馆》动画定档1月6日](https://new.qq.com/omn/ACF20201/ACF2020122300906400.html)
> 概要: 又到了起床靠毅力，洗澡靠勇气的季节了！窗外寒风凛冽，冰天雪地，这种时候有什么比窝在暖洋洋的被窝里，看一部温馨治愈的动画更惬意的呢？2021年1月6日起在腾讯视频独家开播的《水豚汤馆》，就是这样一部为冬......
### [一人之下507话，白鸮：唐门刺杀忍众是英雄，那我杀了他们呢？](https://new.qq.com/omn/20201223/20201223A0JA0300.html)
> 概要: 我们知道上周更新的《一人之下》漫画506话之中，白鸮在见识了莫名居士吴曼的改变之后，自己也想像他那样，这样既不会伤害到什么人，他的欲望也得到了释放，也算是完美的达到了白鸮的要求，让他好好的爽快了一下......
### [斗罗135，小舞竟然有腹肌，这身材太完美，唐三解决人面魔蛛报仇](https://new.qq.com/omn/20201223/20201223A06OYL00.html)
> 概要: 一百三十五集斗罗大陆动画已经更新，相比上一集的动画传达出来的悲伤情感，这一集动画传达出来的感情更加多一些。在这集动画中，除了唐三的悲伤以外，还有唐三的愤怒。            唐三把相思断肠红喂给......
### [还有这样卖片？鬼灭之刃与魔道祖师贱卖，但为什么都从大陆发货？](https://new.qq.com/omn/20201223/20201223A0JE5600.html)
> 概要: 对于听说过日本正版DVD或BD价格的漫迷，都知道一卷的价格很贵，以目前TV动画DVD均价5000日元计算，一卷收录2话左右的内容，一季度12话，全套下来少则都要3万日元（1900RMB），更不用说追求......
### [《EVA新剧场版：终》推IMAX版 2021年1月23日上映](https://acg.gamersky.com/news/202012/1348780.shtml)
> 概要: 剧场版动画《新世纪福音战士新剧场版：终》，宣布将推出IMAX版，上映时间和普通版一样，2021年1月23日上映。
### [剧场版《美少女战士Eternal》前篇新PV 4战士齐变身](https://acg.gamersky.com/news/202012/1348825.shtml)
> 概要: 剧场版动画《美少女战士Eternal》前篇公开了新PV，展示了4位美少女战士，水野亚美、火野丽、木野真琴、爱野美奈子四战士变身的画面。
### [吉卜力《阿雅与魔女》推出预告片 与魔女共同生活](https://acg.gamersky.com/news/202012/1348697.shtml)
> 概要: 吉卜力3DCG动画《阿雅与魔女》推出北美预告片，讲述了女主阿雅被女巫收养之后发生的故事。
### [日媒公开日本历代漫画总销量 《鬼灭之刃》位列第8](https://acg.gamersky.com/news/202012/1348710.shtml)
> 概要: 现在有日本媒体统计了日本历代漫画发行总销量，那么《鬼灭之刃》的漫画总销量究竟如何呢？榜单的第一名果然还是强到很难超越。
### [《通灵王》完全新作动画全新PV 林原惠演唱主题曲](https://acg.gamersky.com/news/202012/1348590.shtml)
> 概要: 《通灵王》完全新作动画公开了全新的PV和视觉图，并且还公布了追加角色声优。《通灵王》完全新作动画的OP和ED歌曲都将由林原惠演唱，动画将于2021年4月开播。
# 财经
### [湖南卫视回应主持团收礼：正进行全面调查](https://finance.sina.com.cn/china/gncj/2020-12-23/doc-iiznezxs8546720.shtml)
> 概要: 原标题：湖南卫视回应主持团收礼：正进行全面调查 12月23日晚，湖南卫视官方微博发布声明： 近日，网上出现涉及湖南卫视相关主持人收取粉丝礼物的舆情，对此...
### [福建调整提高工伤保险定期待遇水平](https://finance.sina.com.cn/china/gncj/2020-12-23/doc-iiznezxs8551945.shtml)
> 概要: 原标题：福建调整提高工伤保险定期待遇水平 新华社福州12月23日电（记者吴剑锋）福建省人社厅日前下发通知，对全省参保工伤职工伤残津贴、生活护理费...
### [北京一科技公司合伙冒充白富美与陌生人网恋实施诈骗 28人被刑拘](https://finance.sina.com.cn/china/dfjj/2020-12-23/doc-iiznctke8131695.shtml)
> 概要: 新京报讯（记者 刘名洋） 12月23日，新京报记者从北京市通州区公安分局获悉，警方于近日打掉一横跨北京、江苏等四省市的“杀猪盘”电信网络诈骗团伙，控制51名涉案人员...
### [苏州出台落户新政：租房也能落户](https://finance.sina.com.cn/china/dfjj/2020-12-23/doc-iiznctke8131543.shtml)
> 概要: 原标题：租房也能落户！这个“网红”城市出台落户新政 近期，国内多个大城市密集推出户籍改革政策，放宽落户门槛。昨天，江苏省苏州市印发《进一步推动非户籍人口在城市落户...
### [重庆交通建设三年累计完成投资2615亿元](https://finance.sina.com.cn/china/gncj/2020-12-23/doc-iiznctke8127888.shtml)
> 概要: 原标题：重庆交通建设三年累计完成投资2615亿元 中新网重庆12月23日电 （韩璐 李绿桐）记者23日从重庆市交通局获悉，重庆交通建设三年行动计划已实现收官。
### [宁波新增无症状感染者为顺义居民 公司与小区已封闭管理](https://finance.sina.com.cn/jjxw/2020-12-23/doc-iiznezxs8551295.shtml)
> 概要: 原标题：宁波新增无症状感染者为顺义居民，公司与小区已封闭管理 （健康时报记者 李超然 孔天骄 陈琳辉）12月23日下午，宁波市通报新增一例无症状感染者...
# 科技
### [JS数组中 forEach() 和 map() 的区别](https://segmentfault.com/a/1190000038421334)
> 概要: 译者：前端小智作者：Ashish Lahoti来源：odingnconcepts点赞再看，微信搜索【大迁世界】关注这个没有大厂背景，但有着一股向上积极心态人。本文GitHubhttps://githu......
### [记一次前端"vscode插件编写实战"超详细的分享会(建议收藏哦)(下篇)](https://segmentfault.com/a/1190000038617902)
> 概要: 这里的序号承接上文十一. hover的效果(官网例子模糊)想象一下鼠标悬停在一个语句上就会浮现出你的提示信息;是不是还挺酷的;说干就干;但还是忍不住吐槽一下官网的例子太粗糙了.hover就不用去定义命......
### [新卖点：三星 Galaxy S21 Ultra 通过 FCC 认证，确认支持 S Pen](https://www.ithome.com/0/526/320.htm)
> 概要: IT之家 12 月 23 日消息 三星 Galaxy S21 Ultra 支持 S Pen 的传闻已经有很长时间了，前段时间，一位三星公司高管暗示了这一猜测是真实的。现在 Galaxy S21 Ult......
### [欲打破微软垄断：统信软件牵手华为、阿里等发起操作系统生态联盟](https://www.ithome.com/0/526/302.htm)
> 概要: “一颗种子埋到土里面，不但需要阳光和空气，也需要大的环境，树的成长过程当中会经历风霜雨雪，这非常像生态体系建设的过程。”在 12 月 23 日举办的首次 UOS 生态大会上，统信软件总经理刘闻欢用一棵......
### [Medo: A modern UHD 4K open source Media editor for Haiku OS in less than 1.44 Mb](https://discuss.haiku-os.org/t/announcement-haiku-media-editor-r1-0-0-beta-1/10218)
> 概要: Medo: A modern UHD 4K open source Media editor for Haiku OS in less than 1.44 Mb
### [In 1980s Los Angeles, a bank was robbed every hour (2019)](https://crimereads.com/the-rise-and-fall-of-the-bank-robbery-capital-of-the-world/)
> 概要: In 1980s Los Angeles, a bank was robbed every hour (2019)
### [通过数据分析，如何挖掘出优质的内容营销故事？](https://www.tuicool.com/articles/eQZFfme)
> 概要: 编辑导语：在企业里，对于业务人员来说，数据分析是非常重要的，通过数据分析可以帮助相关人员挖掘需求；一个好的数据分析可以清楚的让我们看见想要表达的数据，提高工作效率；本文作者分享了关于数据分析中怎么挖掘......
### [最前线 | 辛巴燕窝事件处罚结果：辛巴个人快手账号封禁60天，辛巴家族主播均连坐封禁](https://www.tuicool.com/articles/vInEzan)
> 概要: 12月23日晚间，快手电商发布了辛巴燕窝事件的官方处罚结果，辛巴个人账号被封停60天，而不是此前媒体报道的永久封禁。此外，包括涉事主播“时大漂亮”在内的辛巴系主播均受到不同程度的封停处罚。就在快手电商......
# 小说
### [妙手心医](https://m.qidian.com/book/1010457704/catalog)
> 作者：陈家三郎

> 标签：都市生活

> 简介：新书《来自未来的连线》已发布，请多多支持！“林杰先生，作为世界上最年轻的诺贝尔生理学奖获得者，请谈一下您的感想？”林杰看着眼前不时亮起的闪光灯，缅怀道：“在十九岁之前，我只是一个数着日子等死的少年，改变我命运的是那一例手术。”“那例手术带给我的，不仅仅是新生……”老书《超人类进化》已经完本，欢迎点阅！书友群：624618413

> 章节总数：共1181章

> 状态：完本
# 游戏
### [卡游《GateRuler》26日发售 玩家不顾新冠挤爆店铺](https://www.3dmgame.com/news/202012/3804812.html)
> 概要: 日本玩家对于卡牌游戏的热情有多高？一起来了解下，倍受期待的全新TCG卡游《GateRuler》即将于12月26日发售，位于秋叶原的某卡牌连锁商店被围的水泄不通，全然不顾严峻的疫情,都是前来预购卡片的玩......
### [2020年全球游戏总值1749亿美元 中国玩家7.2亿](https://www.3dmgame.com/news/202012/3804827.html)
> 概要: 外媒GamesIndustry.biz以数字图表的形式总结了2020年这一年全球各地的游戏市场情况，包括全球游戏市场价值，美国、日本、英国最畅销游戏，MTC上最高和最低评分的游戏等等。GamesInd......
### [盖尔·加朵：《神奇女侠3》别讲过去的事了](https://www.3dmgame.com/news/202012/3804828.html)
> 概要: 《神奇女侠1984》这周末在美国影院和HBO Max同时“上映”，目前预售票房大约为480万美元。虽然全球票房疲软，《神奇女侠3》希望不大，但女主角盖尔·加朵被问到时还是表示：“希望下部电影安排在当代......
### [《寂静岭》之父新作首曝：恐怖主题 PC是首要平台](https://www.3dmgame.com/news/202012/3804830.html)
> 概要: 据IGN报道，《寂静岭》之父外山圭一郎成立了新公司Bokeh Game Studio，正在开发一款恐怖主题的动作冒险游戏，“这将安抚他之前作品的粉丝”，计划在2023年发售。IGN报道细节汇总如下：开......
### [《Among Us》IGN评分9分：该类型游戏的一次突破](https://www.3dmgame.com/news/202012/3804776.html)
> 概要: 《Among us》无疑是今年最具人气的游戏之一，不仅在TGA2020夺得最佳多人游戏和最佳移动游戏，11月月活玩家数量更是达到了5亿。IGN也对这款游戏做出了评测，并给出了9分的成绩。IGN评测：I......
# 论文
### [IndoNLU: Benchmark and Resources for Evaluating Indonesian Natural Language Understanding](https://paperswithcode.com/paper/indonlu-benchmark-and-resources-for)
> 日期：11 Sep 2020

> 标签：NATURAL LANGUAGE UNDERSTANDING

> 代码：https://github.com/indobenchmark/indonlu

> 描述：Although Indonesian is known to be the fourth most frequently used language over the internet, the research progress on this language in the natural language processing (NLP) is slow-moving due to a lack of available resources. In response, we introduce the first-ever vast resource for the training, evaluating, and benchmarking on Indonesian natural language understanding (IndoNLU) tasks.
### [Renovating Parsing R-CNN for Accurate Multiple Human Parsing](https://paperswithcode.com/paper/renovating-parsing-r-cnn-for-accurate-1)
> 日期：ECCV 2020

> 标签：HUMAN PARSING

> 代码：https://github.com/soeaver/RP-R-CNN

> 描述：Multiple human parsing aims to segment various human parts and associate each part with the corresponding instance simultaneously. This is a very challenging task due to the diverse human appearance, semantic ambiguity of different body parts, and complex background.
