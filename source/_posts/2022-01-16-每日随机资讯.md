---
title: 2022-01-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BoguraChili_ZH-CN2707755390_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-01-16 19:55:36
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BoguraChili_ZH-CN2707755390_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Ubuntu 22.04 LTS 将使用 Linux 5.15 内核](https://www.oschina.net/news/178724/ubuntu-22-04-lts-will-use-linux-5-15-kernel)
> 概要: 默认情况下，Ubuntu 22.04 LTS 将附带Linux 5.15 内核。上周我们报导了Ubuntu 22.04 计划使用 GNOME 42，但仍以 GTK3 应用为主体。在此消息的 Ubunt......
### [Wine 7.0-rc6 现已发布，修复 47 个问题](https://www.oschina.net/news/178712/wine-7-0-rc6-released)
> 概要: Wine 7.0-rc6 现已推出，这预计将是最终的 7.0 之前的最后一个候选版本，因此代码处于冻结状态，仅修复了一些错误。39342Thunder 在没有原生 riched20 的情况下崩溃421......
### [开源 Raspberry Pi 图形驱动程序增加了双缓冲区模式](https://www.oschina.net/news/178720/v3d-v3dv-double-buffer)
> 概要: Mesa 的 V3D 和 V3DV 驱动分别为较新的 Broadcom VideoCore 硬件提供了开源的 OpenGL 和 Vulkan 驱动支持，实现了双缓冲模式。正如phoronix所述，此举......
### [JetBrains 公布 WebStorm 2022.1 路线图](https://www.oschina.net/news/178721/webstorm-2022-1-roadmap)
> 概要: JetBrains  公布了 WebStorm 2022.1 版本的路线图，此版本预计将在 3 月底发布，主要聚焦于以下内容：远程开发。在 WebStorm 2021.3 中，JetBrains 推出......
### [超人气漫画改编，大战校园丧尸](https://news.dmzj.com/article/73353.html)
> 概要: 2022年网飞开年新剧《僵尸校园》将于1月28日上线平台，由 尹灿荣、朴持厚、曹怡贤、朴所罗门、刘仁秀等人主演，改编自朱东根的人气网络漫画《极度恐慌》。讲述本应平凡的校园，却传出了骇人听闻的事件！被丧尸病毒肆虐的一所高中里与世隔绝的人和想要营救他...
### [19 岁少年入侵 25 辆特斯拉汽车；开发者因无报酬破坏 NPM 开源库；Rust 1.58.0 发布 | 思否周刊](https://segmentfault.com/a/1190000041291287?utm_source=sf-homepage)
> 概要: 40s 新闻速递苹果将于 3 月或 4 月举行发布会：推 iPhone SE3开发者因无报酬故意破坏知名 NPM 开源库，引发热议苹果 2021 年总共向开发者支付 600 亿美元，创下新纪录H2 数......
### [内容仅供参考！动画《这个医师超麻烦》新PV](https://news.dmzj.com/article/73354.html)
> 概要: 根据“精心发酵”原作制作的TV动画《这个医师超麻烦》宣布了将于4月发售的消息。本作的新PV也一并公开。
### [日本宣布在梦洲建设以EVA为原型的多功能电线杆](https://news.dmzj.com/article/73355.html)
> 概要: ​日本关西电力宣布，将开始多功能电线杆“smart pole”的实际运行试验。
### [日本网友投票以“成为小说家”作品为原作的动画排行](https://news.dmzj.com/article/73356.html)
> 概要: 近几年来，日本每季度的动画中，都会有多部以小说投稿网站“成为小说家”上的作品为原作改编的作品。近日，有日本网站就请网友对这些动画进行了人气投票。
### [电动车，没有新革命](https://www.huxiu.com/article/490973.html)
> 概要: 出品｜虎嗅汽车组作者｜梓楠法师编辑｜张博文“我们会成为下一个宁德时代”。一位固态电池企业研发负责人在与虎嗅交流时，对公司研发出固态电池后的场景作出畅想。行业设想中的固态电池，可以破除液态电池车的里程焦......
### [海贼王1037话：你以为赢了凯多就是四皇？天真，人家那叫海贼王！](https://new.qq.com/omn/20220115/20220115A0B36Y00.html)
> 概要: 海贼不识王路飞，便称英雄也枉然！大家好，我乌龙君又双叒叕来了~            NO.1【和之国战场】为什么乌龙君会说路飞会变成准海贼王呢？首先，我们应该先了解一下和之国这一战究竟是什么格局。很......
### [2022年中国互联网十大预测](https://www.huxiu.com/article/490996.html)
> 概要: 本文来自微信公众号：卫夕指北（ID：weixizhibei），作者：卫夕，题图来自：视觉中国2022年中国互联网会走向何方？很显然，对于整个互联网行业而言，2022年将充满着众多不确定性，卫夕尝试梳理......
### [打破纪录！《战神4》Steam在线峰值超6.5万人](https://www.3dmgame.com/news/202201/3833407.html)
> 概要: 据SteamDB显示，在发售一天后，《战神4》Steam在线峰值有所上涨，从之前的4.9万攀升到了当前的6.5万（65403），打破了早前由《地平线：零之曙光》保持的记录-5.6万在线峰值。值得一提的......
### [RTX 3090+Reshade光追运行《怪物猎人：崛起》演示](https://www.3dmgame.com/news/202201/3833411.html)
> 概要: 《怪物猎人：崛起》近日正式登陆PC平台，油管频道Digital Dreams发布一段视频，展示了在8K分辨率下使用Reshade Ray Tracing运行游戏的效果。Digital Dreams使用......
### [电视动画「这个医师超麻烦」公开第一弹正式PV](http://acg.178.com/202201/436297276139.html)
> 概要: 近日，电视动画「这个医师超麻烦」公开了第一弹正式PV，本作预计将于2022年4月正式开播。「这个医师超麻烦」第一弹正式PVSTAFF原作：丹念に発酵导演：中西伸彰系列构成・脚本：志茂文彦角色设计：菊永......
### [中国玩家让G胖给其女儿起名 G胖：Luna可以吗](https://www.3dmgame.com/news/202201/3833413.html)
> 概要: 近日，有中国玩家晒出一篇自己让G胖（Gabe Newell）给自己的女儿起英文名的帖子，G胖也亲自进行了进行了回复，G胖表示了恭喜，孩子可以用Luna这个名字。这名玩家在发给G胖邮件中称，我是一名普通......
### [爷青回！央视52集年更番《新围棋少年》宣传片公布](https://acg.gamersky.com/news/202201/1453205.shtml)
> 概要: 《斗破苍穹》等作品的音乐总监魏小涵在微博分享了《新围棋少年》的宣传片。
### [《开端》8.2开分，刘涛演警花被吐槽拉胯](https://new.qq.com/rain/a/20220116A00D1S00)
> 概要: 开年最受关注的国产剧，当属。目前更新至8集，豆瓣超九万人评论，开分高达8.2，给了2022年国产剧一个漂亮的“开端”。                        这部剧，由大名鼎鼎的正午阳光出品，......
### [Show HN: Brew.fm – An alliance of artists who remix and edit each other's tracks](https://www.brew.fm/)
> 概要: Show HN: Brew.fm – An alliance of artists who remix and edit each other's tracks
### [中国台湾初创公司Frontier推出基于人工智能和区块链技术的数字面料系统](https://www.tuicool.com/articles/VvIn6b3)
> 概要: 在刚刚结束的美国消费电子展（CES）上，中国台湾的初创公司Frontier展示了与深圳的E-Elements（依元素科技）合作推出的 “Fabric Meta Chip”（织物元芯片），旨在通过人工智......
### [首个播放量破 100 亿的 YouTube 视频诞生，竟然是儿歌](https://www.tuicool.com/articles/2YBzyiQ)
> 概要: IT之家 1 月 16 日消息，首个播放量破 100 亿的 YouTube 视频诞生了，来自儿童教育品牌 Pinkfong 的《Baby Shark Dance》获得了这一成就。从视频来看，这首儿歌确......
### [Carbon dating is getting a major reboot (2020)](https://www.nature.com/articles/d41586-020-01499-y)
> 概要: Carbon dating is getting a major reboot (2020)
### [玩家自制《战神4》PC版MOD 可自行调整FOV数值](https://www.3dmgame.com/news/202201/3833424.html)
> 概要: 《战神4》PC版第一批MOD现已推出，其中有一款调整FOV的MOD相当实用。KingKrouchy使用Cheat Engine对游戏的FOV进行了调整。不过在调整之后，由于采用了一镜到底，游戏里的过场......
### [「Comic百合姫」2022年3月号封面公开](http://acg.178.com/202201/436304131450.html)
> 概要: 漫画杂志「Comic百合姫」公开了2022年3月号的封面图，本期杂志将于1月18日发售，封面由知名画师一色所绘制。「Comic百合姫」是一迅社所发行的月刊百合漫画杂志，于每月18日发售......
### [「失格纹的最强贤者」ED主题曲动画MV公开](http://acg.178.com/202201/436304567061.html)
> 概要: 根据进行诸岛创作的同名轻小说改编，J.C.STAFF负责制作的电视动画「失格纹的最强贤者」近期公开了ED主题曲「Day of Bright Sunshine」的动画MV，该作已于1月8日开始播出。「D......
### [奥斯卡欠韦斯·安德森一个“最佳对称奖”](https://www.huxiu.com/article/490993.html)
> 概要: 出品｜虎嗅青年文化组作者｜黄瓜汽水题图｜电影《法兰西特派》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。世界上有两个文艺青年最......
### [组图：邓超梦见被孙俪训斥减肥改口叫长官 孙俪：地位有所提升](http://slide.ent.sina.com.cn/star/slide_4_704_365588.html)
> 概要: 组图：邓超梦见被孙俪训斥减肥改口叫长官 孙俪：地位有所提升
### [印度提议从 10 月起所有生产的乘用车至少配备六个安全气囊](https://www.ithome.com/0/598/797.htm)
> 概要: 印度是世界上交通事故死亡人数最多的国家之一，路透报道称该国于周六提议，从 10 月 1 日起，所有生产的乘用车都必须配备至少六个安全气囊。▲ 图源：路透除了所有汽车已经要求的两个安全气囊之外，政府希望......
### [裸辞的尽头是打工](https://www.huxiu.com/article/491012.html)
> 概要: 本文来自燃次元（ID:chaintruth），作者：谢中秀、曹杨、张琳、孔月昕，编辑：谢中秀，题图来自：视觉中国“打工是不可能打工的，这辈子不可能打工的”、“世界那么大，我想去看看”……在过去很长一段......
### [TV动画《派对浪客诸葛孔明》PV公开 孔明转生唱歌？](https://acg.gamersky.com/news/202201/1453258.shtml)
> 概要: TV动画《派对浪客诸葛孔明》PV公开，讲述了北伐失败，在五丈原遗憾而终的诸葛亮，以年轻的肉体转生到在现代的日本涩谷。
### [低过百亿补贴：大牌德运进口奶粉 2 斤 44.5 元狂促](https://lapin.ithome.com/html/digi/598803.htm)
> 概要: 【德运食品旗舰店】德运调制乳粉（全脂） 1kg 售价 79 元，今日下单 2 件立打 9 折，叠加 40 元限量券 + 凑单品后，实付 92.1 元包邮。奶粉折合 89 元，每袋低至 44.5 元好价......
### [只用了 3 天，我就出版了一本新书](https://www.tuicool.com/articles/YZnMFzY)
> 概要: 今天跟你们说个事儿，顺便送送书。可能有些读者已经知道了，我参与创作的一本关于产品经理成长的新书已经出版了。说出来你们可能不信，我只用了 3 天时间就出版了一本新书。可能这是我有史以来出版速度最快的一本......
### [Rewatch: Stargate SG-1 (2019)](https://macmanx.com/2019/07/22/rewatch-stargate-sg-1/)
> 概要: Rewatch: Stargate SG-1 (2019)
### [「游戏王 怪兽之决斗」黑魔导手办开订](http://acg.178.com/202201/436310182416.html)
> 概要: MEGAHOUSE作品「游戏王 怪兽之决斗」黑魔导手办开订，全长约230mm（含底座），主体采用ABS和PVC材料制造。该手办定价为17380日元（含税），约合人民币966元，预计于2022年7月发售......
### [细数《开端》13个细节和2处BUG，第四集暗藏引爆炸弹特写](https://new.qq.com/rain/a/20220116A043JM00)
> 概要: 1月11日上线播出的《开端》豆瓣开分8.2，截止到发稿超过11万网友打分，其中打四星和五星好评的人数占比超过80%，口碑非常不错。            这样的口碑，一方面得益于原著小说本身故事扎实、......
### [组图：偶遇袁咏仪在深圳做义工 戴口罩手套防护到位](http://slide.ent.sina.com.cn/star/slide_4_704_365592.html)
> 概要: 组图：偶遇袁咏仪在深圳做义工 戴口罩手套防护到位
### [视频：刘烨晒诺一霓娜话剧舞台照 点评孩子们表现太惊喜](https://video.sina.com.cn/p/ent/2022-01-16/detail-ikyamrmz5518821.d.html)
> 概要: 视频：刘烨晒诺一霓娜话剧舞台照 点评孩子们表现太惊喜
### [视频：张卫健回忆演戏经历 坦言刘德华是自己的救命恩人](https://video.sina.com.cn/p/ent/2022-01-16/detail-ikyamrmz5519312.d.html)
> 概要: 视频：张卫健回忆演戏经历 坦言刘德华是自己的救命恩人
### [从东南亚、非洲市场看，网赚产品出海如何实现爆炸式增长？](https://www.tuicool.com/articles/MBzMn2m)
> 概要: 随着国内互联网经济形势的下滑，互联网行业也整体进入了寒冬时期。出海也成为了很多互联网公司的重要选择。那么如何做好海外运营，也就成了出海企业最关心的问题之一。本文将通过本人亲身做海外的经验分享，和大家聊......
### [DC或为动画剧《蝙蝠车总动员》推出电子游戏产品](https://www.3dmgame.com/news/202201/3833442.html)
> 概要: DC今年将面向学龄前儿童受众群体推出动画剧《蝙蝠车总动员》。而根据商标信息显示，除了动画剧之外，还有一些相关内容和周边商品正在策划中，其中包括《蝙蝠车总动员》的电子游戏。在最近由华纳兄弟娱乐为《蝙蝠车......
### [《左肩有你》导演发文称已完成剪辑:但愿一切顺利](https://ent.sina.com.cn/v/m/2022-01-16/doc-ikyamrmz5539910.shtml)
> 概要: 新浪娱乐讯 1月16日，《左肩有你》导演发长文称已完成剪辑工作，“所有拍摄素材搭建好、通过监制陈导一遍遍修改认可、我自己最理想的状态，如监制所说，若不是平台许可，没有剧能让我花那么多时间定剪”。他也表......
### [天津确诊1例新冠肺炎危重病例：为肺癌患者，未接种疫苗](https://finance.sina.com.cn/china/2022-01-16/doc-ikyamrmz5541202.shtml)
> 概要: 今天的新闻发布会上，天津通报了1例新冠肺炎危重病例。就此问题，记者随即联系了市卫健委有关部门。 据了解，该危重病人61岁，居住于津南区辛庄镇林锦花园。
### [忍者世界一共有多少个忍村？除了五大忍村之外，你还知道多少个？](https://new.qq.com/omn/20220115/20220115A0AWV800.html)
> 概要: 忍者世界有多少个忍者村？除了五大忍村，你还能记起多少？在动漫火影忍者中，忍者们都是来自于不同的村子，比如说主角漩涡鸣人来自于木叶村，而风影我爱罗则是来自于砂隐村，那么在动漫之中，忍者世界一共包含了多少......
### [仁宝电脑桃园工厂员工确诊新冠肺炎，现已暂停生产](https://www.ithome.com/0/598/840.htm)
> 概要: 仁宝今（16）日发布公告称，该公司桃园平镇厂因两名员工确诊，今日对平镇厂员工及 1 月 8 日至 1 月 15 日相关进出人员进行全员筛检，并立即启动紧急应变及防疫措施，本事件对公司财务业务无重大影响......
### [美检察官拟撤诉陈刚案，“中国行动”取消遥遥无期](https://finance.sina.com.cn/china/2022-01-16/doc-ikyakumy0720192.shtml)
> 概要: 美国司法部表示，最近事态发展促使司法部重新评估这些起诉案件，并基于公正考虑撤销起诉 文|《财经》记者 王晓枫  美国当地时间1月15日，媒体援引政府消息人士称...
### [全球缺芯潮带动台湾出口创新高](https://finance.sina.com.cn/china/2022-01-16/doc-ikyakumy0720243.shtml)
> 概要: 台积电总裁魏哲家预测，2022年全球芯片代工将可能继续增长20% 文|《财经》记者 蔡婷贻  1月13日台湾芯片制造业商台积电发布2021年四季度营收报告，单季营收达157.4亿美元...
### [How Tumblr Became Popular for Being Obsolete](https://www.newyorker.com/culture/infinite-scroll/how-tumblr-became-popular-for-being-obsolete)
> 概要: How Tumblr Became Popular for Being Obsolete
### [银保监会今年首张罚单：中信保诚给予保单约定外利益被罚30万](https://finance.sina.com.cn/jjxw/2022-01-16/doc-ikyakumy0719912.shtml)
> 概要: 银保监会开出2022年首张罚单。 1月14日，银保监会发布的2020年1号罚单显示，中信保诚人寿保险有限公司（以下简称中信保诚人寿）向投保人、被保险人...
### [天津确诊1例危重病例：为肺癌患者，入院时体温38℃，神志不清，呼之不应](https://finance.sina.com.cn/china/2022-01-16/doc-ikyakumy0722669.shtml)
> 概要: 1月16日，据央视新闻消息，今天的新闻发布会上，天津通报了1例新冠肺炎危重病例。就此问题，记者随即联系了天津市卫健委有关部门。
### [CES 新品：LG / ROG 推出新款 42 英寸 OLED 电视 / 显示器](https://www.ithome.com/0/598/843.htm)
> 概要: IT之家1 月 16 日消息，在今年 CES 上，LG 推出了旗下最小的 42 英寸 OLED 电视，ROG 则是推出 42 英寸 OLED 大屏显示器。42 英寸的显示新品类介于传统显示器和电视之间......
### [汤加火山喷发后，中国大使馆初步核实：暂无我公民伤亡报告](https://finance.sina.com.cn/china/2022-01-16/doc-ikyakumy0729095.shtml)
> 概要: 南太平洋岛国汤加的洪阿哈阿帕伊岛14日和15日发生火山喷发，首都努库阿洛法观测到海啸。1月16日，据外交部领事保护中心官方微博消息，经中国驻汤加大使馆初步核实...
# 小说
### [无敌剑域](http://book.zongheng.com/book/435710.html)
> 作者：青鸾峰上

> 标签：奇幻玄幻

> 简介：如果杀人不是为了装逼，那将毫无意义；如果活着不是为了装逼，那还不如死了。杀，就杀他个尸横遍野，装，就装他个巅峰不败！——新书《一剑独尊》已发布，欢迎大家去阅读，在纵横直接搜索《一剑独尊》 便可。微信公众号：直接搜：青鸾峰上。青鸾读者群：571783824

> 章节末：2796章：新书《一剑独尊》发布！！

> 状态：完本
# 论文
### [Learning Neural Templates for Recommender Dialogue System | Papers With Code](https://paperswithcode.com/paper/learning-neural-templates-for-recommender)
> 日期：25 Sep 2021

> 标签：None

> 代码：None

> 描述：Though recent end-to-end neural models have shown promising progress on Conversational Recommender System (CRS), two key challenges still remain. First, the recommended items cannot be always incorporated into the generated replies precisely and appropriately.
### [Learning Personal Food Preferences via Food Logs Embedding | Papers With Code](https://paperswithcode.com/paper/learning-personal-food-preferences-via-food)
> 日期：29 Oct 2021

> 标签：None

> 代码：None

> 描述：Diet management is key to managing chronic diseases such as diabetes. Automated food recommender systems may be able to assist by providing meal recommendations that conform to a user's nutrition goals and food preferences.
