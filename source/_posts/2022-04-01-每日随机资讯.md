---
title: 2022-04-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HawaMahalJaipur_ZH-CN3863273823_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-04-01 21:51:31
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HawaMahalJaipur_ZH-CN3863273823_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [深度操作系统 20.5 发布](https://www.oschina.net/news/189214/deepin-20-5-released)
> 概要: 深度操作系统 20.5 现已发布，升级Stable内核至5.15.24，修复底层漏洞，进一步提升系统兼容性和安全性，功能层面上积极响应社区用户反馈的需求，开发并集成了大量实用功能。人脸识别新增人脸识别......
### [IE 退役倒计时 3 个月，微软改进 Edge 中 IE 模式](https://www.oschina.net/news/189203/enhancements-microsoft-edge-for-ie-mode)
> 概要: 微软曾于去年 5 月份宣布，Internet Explorer 11 桌面应用程序将于 2022 年 6 月 15 日退役；同时将从某些 Windows 10 版本中淘汰 Internet Explo......
### [Zulip 5.0 已发布，开源团队协作工具](https://www.oschina.net/news/189189/zulip-5-0-released)
> 概要: Zulip 是一个开源团队协作工具，一款专为实时和异步对话而设计的现代团队聊天应用程序，支持快速搜索、拖放文件上传、图像预览、组私人消息、可听通知、错过电子邮件消息提醒与桌面应用等。最新发布的Zuli......
### [Pigsty 1.4 正式发布](https://www.oschina.net/news/189194/pigsty-1-4-released)
> 概要: Pigsty v1.4 现已正式发布。全新的模块化架构：四大内置模块 INFRA，NODES，PGSQL，REDIS 可以独立使用并自由组合；新增时序数据仓库 MatrixDB 部署与监控支持；新建设......
### [桌面研究怎么做？这是我想分享的几点实操心得](http://www.woshipm.com/pd/5378688.html)
> 概要: 编辑导语：桌面研究指的是不进行一手资料的实地调研，而是直接通过电脑、杂志、书籍、文档、互联网搜索等现有二手资料进行分析和研究的方法，这种方法在实际的用户研究项目中经常会用到。本文作者基于之前的项目经历......
### [社交网络里，根本没有社交](http://www.woshipm.com/it/5378659.html)
> 概要: 编辑导语：如今的社交网络，似乎越来越不能给人带来快乐。那么，二十年前的互联网社交又是什么样子的？本文作者带大家回顾了过去国内互联网有趣的地方，以及当时社交平台平等的交流氛围，并进一步探讨了“归属感”不......
### [热度褪去，我们聊聊元宇宙](http://www.woshipm.com/it/5377923.html)
> 概要: 从“Facebook”重塑品牌到“Meta”遇冷、从百度内测“希壤”到“希壤”无法拯救百度、从元宇宙概念股票疯涨到一泻千里、从“啫喱”一跃App Store排行榜第一名到主动下架，元宇宙到底是个什么宇......
### [The smallest and worst HDMI display](https://mitxela.com/projects/ddc-oled)
> 概要: The smallest and worst HDMI display
### [iQOO x 站酷「IP设计」](https://www.zcool.com.cn/work/ZNTg5Mzg0NTI=.html)
> 概要: 从设计之初我就思考，如何避开刻意感的设计，让IP角色有一种符合品牌气质的自然感，不用去刻意思考，他具体搭配什么武器或者工具。这种气质应该是直观的。希望你可以看见：速度/力量/和探索的可能。——————......
### [王传福“混”得很好](https://www.huxiu.com/article/518479.html)
> 概要: 作者｜Eastland头图｜视觉中国2022年3月29日盘后，比亚迪（00259.SZ）发布的《2021年度报告》显示：财年营收2161.4亿、同比增长38.02%；全年汽车销量72.1万台、同比增长......
### [一年总结——Q水浒1-80](https://www.zcool.com.cn/work/ZNTg5NDE1Njg=.html)
> 概要: 一年了，小小总结一下。2021.3——2022.3从去年3月份开始创作《Q版水浒传108将系列人物》如今转眼已过一年，这一年内把时间和精力都全身心的放在了创作Q版水浒人物上。如今终于坚持画完了80人......
### [TV动画「相合之物」第二弹PV公布](http://acg.178.com/202204/442778155268.html)
> 概要: TV动画「相合之物」公布了第二弹PV，本作将于2022年4月6日播出。TV动画「相合之物」第二弹PVTV动画「相合之物」改编自浅野りん著作的同名漫画作品，于2021年4月19日宣布动画化的消息，由En......
### [2022年E3游戏展停办！开始为2023年的活动做准备](https://news.dmzj.com/article/74031.html)
> 概要: ESA宣布了2022年E3游戏展停办，正在准备2023年的活动的消息。这次取消的原因，依然是受到了疫情的影响。
### [90后00后遗嘱关注虚拟财产 数字的“我”留下的是什么？](https://finance.sina.com.cn/tech/2022-04-01/doc-imcwipii1774160.shtml)
> 概要: 记者 陈洁......
### [DLsite宣布推出新感觉MMOARPG游戏《DLsite world》](https://news.dmzj.com/article/74032.html)
> 概要: 日本同人作品销售平台DLsite宣布将要发售原创新感觉MMOARPG游戏《DLsite world》的消息。目前，这款游戏已经开始开服前预约。玩家可以先行生成自己的形象，并获得新的力量。
### [贰婶手写--奇妙的中国汉字【拙字集·贰】](https://www.zcool.com.cn/work/ZNTg5NDMzNDg=.html)
> 概要: 【拙字集·贰】......
### [大妹子家愚人节活动](https://news.dmzj.com/article/74033.html)
> 概要: 愚人节快乐！本条新闻为活动用，参与的同学不要忘了看看活动规则哟！
### [8款居家生产力好物，我再也不想去公司了](https://www.huxiu.com/article/518281.html)
> 概要: 图 ｜unsplash居家一时爽，一直在家……可能不一定爽。受疫情影响，相信大部分的打工仔都体验过了居家办公的生活模式，做不完的工作和与休息模糊的边界，打破了许多人心中“线上办公”的滤镜。疫情仍未结束......
### [动画《政宗君的复仇》疑似新动画企划进行中](https://news.dmzj.com/article/74034.html)
> 概要: 根据竹冈叶月、Tiv原作制作的TV动画《政宗君的复仇》公开了第三季制作决定的愚人节插图。在这次的插图中，可以看到安达垣爱姬在看到小岩井吉乃正在设置第三季制作决定的看板时，吐槽“第三季之前要先制作第二季”的样子。
### [《天穗之咲稻姬》在Steam国区降价 标准版为148元](https://www.3dmgame.com/news/202204/3839433.html)
> 概要: 由Marvelous制作的和风ARPG《天穗之咲稻姬》在Steam国区等部分亚洲地区再次大幅降价，游戏标准版从263元降至148元，豪华版从350元降至199元。游戏Steam页面：点击此处该游戏曾在......
### [「我的青春恋爱物语果然有问题。」官方发布4月1日愚人节贺图](http://acg.178.com/202204/442783810174.html)
> 概要: 今天（4月1日）是愚人节，「我的青春恋爱物语果然有问题。」官方发布了一个“可信度极高”的愚人节企划页面以及相关贺图，贺图上绘制的是“长大成人”之后的一色伊吕波和比企谷八幡。2041年6月8日，全新TV......
### [李书福挑战不可能](https://www.huxiu.com/article/519748.html)
> 概要: 出品 | 虎嗅汽车组作者 | 张博文头图 | IC Photo造车圈有个共识。从零到一，打造一个新品牌难，但比起打造新品牌，捡一个老品牌做品牌复兴更难。即便如今挂着迈巴赫标的奔驰是富豪们的心头好，奔驰......
### [动画「公主连结！Re:Dive」第2季版权绘公开](http://acg.178.com/202204/442784938705.html)
> 概要: 近日，动画「公主连结Re:Dive」公开了第二季的最新版权绘图，本次绘制的是佩可莉姆和凯露。电视动画「公主连结Re:Dive」改编自同名游戏作品，由CygamesPictures负责动画制作。第一季已......
### [Infinite Mac: An Instant-Booting Quadra in the Browser](https://blog.persistent.info/2022/03/blog-post.html)
> 概要: Infinite Mac: An Instant-Booting Quadra in the Browser
### [零跑汽车：3月交付10059台](https://finance.sina.com.cn/tech/2022-04-01/doc-imcwipii1804790.shtml)
> 概要: 新浪科技讯 北京时间4月1日午间消息，据“零跑汽车”微信公众号，零跑汽车3月交付超万台大关，达10059台，环比增长193%，月交付量同比增速连续12个月超200%，2022年第一季度累计交付量同比增......
### [加薪留不住农民工](https://www.huxiu.com/article/519914.html)
> 概要: 本文作者：王芳洁，编辑：杨羽，头图来自：视觉中国前几天，在快手拥有23.7万粉丝的“农民工川哥”发了一条视频。“在工地上干了三十年，今年52岁了，再干两年上了60岁，人家就不要啰。”话虽然是笑着说的，......
### [剧场版「Free!–the Final Stroke–」后篇预告PV公开](http://acg.178.com/202204/442792781689.html)
> 概要: 剧场版动画「Free!–the Final Stroke–」后篇公开了预告PV，本作将于4月22日在日本上映。「Free!–the Final Stroke–」后篇预告PVCAST七瀬遙：島﨑信長橘真......
### [两企业开发运营QQ“自动抢红包”软件，被判赔偿腾讯70万](https://finance.sina.com.cn/tech/2022-04-01/doc-imcwipii1820964.shtml)
> 概要: 澎湃新闻4月1日从杭州市中级法院获悉，该院日前审结一起涉及QQ“自动抢红包”软件的不正当竞争案，判决被告百豪公司、古馨公司停止侵权、登报消除影响，并赔偿腾讯公司经济损失（含合理费用）计70万元......
### [视频：法国摄影师德马舍利耶去世 曾拍过无数明星与超模](https://video.sina.com.cn/p/ent/2022-04-01/detail-imcwiwss9374168.d.html)
> 概要: 视频：法国摄影师德马舍利耶去世 曾拍过无数明星与超模
### [《时代》2022全球最具影响力100家企业 Tiktok上榜](https://www.3dmgame.com/news/202204/3839453.html)
> 概要: 近日美国《时代周刊》(TIME)杂志公布了“2022年全球最具影响力的100家企业”榜单，TikTok和比亚迪都位列榜单中。2022年榜单有三个全球封面，聚焦于上榜公司的CEO或高层管理人员，今年登上......
### [​富印新材料完成1.5亿元C轮融资，深创投及旗下安庆红土基金领投](https://www.tuicool.com/articles/EV7JRfq)
> 概要: ​富印新材料完成1.5亿元C轮融资，深创投及旗下安庆红土基金领投
### [Rustc_codegen_GCC can now bootstrap rustc](https://blog.antoyo.xyz/rustc_codegen_gcc-progress-report-10)
> 概要: Rustc_codegen_GCC can now bootstrap rustc
### [哪吒汽车：3月交付量1.2万辆](https://finance.sina.com.cn/tech/2022-04-01/doc-imcwipii1840316.shtml)
> 概要: 新浪科技讯 4月1日下午消息，哪吒汽车公布最新交付数据，3月交付量1.2万辆，同比增长270%；一季度累计交付量3.02万辆，同比增长305%......
### [【白梦小岛】一](https://www.zcool.com.cn/work/ZNTg5NTMxMTY=.html)
> 概要: 这是2022年开始的一个新系列。我想想把我所有的幻想都画出来。梦有好的也有坏的。但都是值得纪念的......
### [美国释放石油储备欲稳油价,业内:把原油变成汽油或成挑战](https://www.yicai.com/news/101368408.html)
> 概要: 3月31日，美国宣布从5月起，将持续6个月释放战略储备原油，释放数量达到100万桶/天，相当于总共释放1.8亿桶原油。对此，IME行业的全球负责人Peter McNally称，把这些原油变成汽油，可能将是个挑战。
### [冒险游戏《凯蒂梦游喵喵仙境》现已发售 支持中文](https://www.3dmgame.com/news/202204/3839464.html)
> 概要: 由ARTillery开发的指向点击冒险游戏《凯蒂梦游喵喵仙境》现已在Steam平台发售，游戏原价50元，发售特惠只需42.5元，支持中文。一起在游戏中帮助小凯蒂从奇妙的喵喵仙境猫咪世界里回到家中吧！游......
### [英特尔锐炫旗舰桌面显卡参数曝光：频率可达 2250MHz，GPU 功耗 175W](https://www.ithome.com/0/610/901.htm)
> 概要: IT之家4 月 1 日消息，日前，英特尔发布了锐炫移动显卡，同时公布了锐炫桌面显卡限量版的外观，并表示它将在今夏发布。虽然英特尔没有公布这款桌面独显的规格，但外媒 3DCenter 发现，英特尔在介绍......
### [长安奔奔 E-Star 电动汽车涨价，国民版多彩款上涨 4000 元](https://www.ithome.com/0/610/903.htm)
> 概要: IT之家4 月 1 日消息，长安新能源昨天发布了关于车型价格调整的说明，长安奔奔 E-Star 涨价了，国民版多彩款上涨 4000 元。受上游原材料价格上涨等因素影响，长安新能源宣布对旗下车型奔奔 E......
### [英特尔计划收购软件开发商 Granulate，预计今年二季度完成](https://www.ithome.com/0/610/905.htm)
> 概要: IT之家4 月 1 日消息，今日，英特尔公布了收购 Granulate 云解决方案公司的协议。后者是一家总部位于以色列的实时持续优化软件开发商。英特尔表示，收购 Granulate能够助力云与数据中心......
### [视频：不小心曝光Selina男友正脸 黄子佼称百密一疏已经道歉](https://video.sina.com.cn/p/ent/2022-04-01/detail-imcwiwss9411497.d.html)
> 概要: 视频：不小心曝光Selina男友正脸 黄子佼称百密一疏已经道歉
### [海马汽车：将在海南自贸港投入约 2000 台氢能源汽车进行运营推广](https://www.ithome.com/0/610/920.htm)
> 概要: 集微网消息 4 月 1 日，海马汽车在投资者互动平台表示，公司规划于十四五期间，在海南自贸港逐步投入约 2000 台氢能源汽车进行示范运营推广。针对水制氢的光伏站建设进度方面，海马汽车表示，公司海口基......
### [周杰伦持有的NFT被盗且很快被倒卖 价值超300万](https://www.3dmgame.com/news/202204/3839474.html)
> 概要: 今日(4月1日)“亚洲天王”周杰伦在Instagram上发文称，其持有的无聊猿BAYC#3738 NFT已被盗，疑似被钓鱼攻击。据悉，该NFT在2022年1月由黄立成赠送给周杰伦。据Opensea与E......
### [榊英雄作品因性侵丑闻取消上映 电影遭网友抵制](https://ent.sina.com.cn/2022-04-01/doc-imcwiwss9418046.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 3月31日，榊英雄导演原定4月15日上映的电影《双闪灯》官网宣布取消上映，由于导演强迫多名女星发生关系的丑闻曝光，作品受到负面影响不得已做出这个决定。　　榊英雄3月9日被......
### [《心居》中的海清教你如何科学吵架，越吵关系越亲密](https://new.qq.com/rain/a/20220324V08ZA200)
> 概要: 《心居》中的海清教你如何科学吵架，越吵关系越亲密
### [留抵退税新政落地首日,上海500余家企业到款超16亿元](https://www.yicai.com/news/101368608.html)
> 概要: 截至15时，上海全市共计500余户符合条件的企业收到留抵退税款16.55亿元。
### [「看不见」的万亿市场：冰山下的身心灵](https://www.tuicool.com/articles/n6vAvqj)
> 概要: 「看不见」的万亿市场：冰山下的身心灵
### [杨元庆给马斯克上一课](https://www.tuicool.com/articles/umMjimN)
> 概要: 杨元庆给马斯克上一课
### [Help YC pick the best startups as a member of the YC Software team](https://www.ycombinator.com/companies/y-combinator/jobs/1x2BVnj-product-engineer-admissions)
> 概要: Help YC pick the best startups as a member of the YC Software team
### [这是昨晚500万人云蹦迪的“背后主谋”](https://www.tuicool.com/articles/IriyUzb)
> 概要: 这是昨晚500万人云蹦迪的“背后主谋”
### [宝马X5国产售价直降10万元，豪华中大型SUV市场将生变？](https://www.yicai.com/news/101368629.html)
> 概要: 相较于此前的进口版本，新车的起售价格降低了近10万元。
### [国务院国资委就《中央企业合规管理办法》公开征求意见](https://finance.sina.com.cn/china/gncj/2022-04-01/doc-imcwipii1886005.shtml)
> 概要: 原标题：国务院国资委关于《中央企业合规管理办法》公开征求意见的通知 为深入贯彻习近平法治思想，落实全面依法治国战略部署，进一步推动中央企业切实加强合规管理...
### [唐彬森的出海野心：抵抗压制，元气森林冲到可口可乐、百事可乐的老家赚钱](https://finance.sina.com.cn/china/gncj/2022-04-01/doc-imcwiwss9442268.shtml)
> 概要: 对于“四大核心成员之一的柳甄已离职”消息，元气森林方面已向记者证实了此事。 2022年3月31日，元气森林相关负责人向《时代周报》记者谈到...
### [陈尔真：隔离点感染者情况平稳 应对重症有充分预案](https://www.yicai.com/news/101368719.html)
> 概要: 上海感染人数连续两天下滑，但仍维持在高位。近三天感染人数累计超过1.6万例，其中无症状感染者超过1.5万例，占比接近94%。
### [三部门释放直播营利强监管信号，MCN、直播平台影响几何？](https://finance.sina.com.cn/china/gncj/2022-04-01/doc-imcwiwss9442239.shtml)
> 概要: 近年来，互联网高速发展促进了社会灵活就业，网络直播是服务经济中较为突出的新行业。然而直播行业的野蛮生长，导致平台管理不到位、主播偷税漏税等乱象丛生。
### [李克强同欧洲理事会主席米歇尔、欧盟委员会主席冯德莱恩共同主持第二十三次中国－欧盟领导人会晤](https://finance.sina.com.cn/china/2022-04-01/doc-imcwipii1889069.shtml)
> 概要: 李克强同欧洲理事会主席米歇尔、欧盟委员会主席冯德莱恩共同主持第二十三次中国-欧盟领导人会晤时强调加强对话协调 深化务实合作维护和平安宁 稳定世界经济
### [3月新势力销量出炉：哪吒排名第二，零跑追赶“蔚小理”](https://www.yicai.com/news/101368735.html)
> 概要: 造车新势力第一梯队竞争日渐白热化。
### [PC游戏秀将于今年6月12日回归](https://www.3dmgame.com/news/202204/3839483.html)
> 概要: 外媒PC Gamer宣布他们举办的游戏展“PC游戏秀（PC Gaming Show）”将在今年6月12日回归。自2015年首次举办以来，PC游戏秀已经举办了7届。PC Gamer表示今年的PC游戏秀将......
### [证监会公布20大典型违法案例，信披违规9宗，永煤债等在列](https://www.yicai.com/news/101368746.html)
> 概要: 与蓝山科技欺诈发行相关的4家中介被点名。
### [视频丨习近平：希望欧方形成自主的对华认知 奉行自主的对华政策](https://finance.sina.com.cn/china/gncj/2022-04-01/doc-imcwipii1893232.shtml)
> 概要: 原标题：独家视频丨习近平：希望欧方形成自主的对华认知 奉行自主的对华政策 国家主席习近平4月1日晚在北京以视频方式会见欧洲理事会主席米歇尔和欧盟委员会主席冯德莱恩。...
### [胡春华：确保完成玉米水稻稳产保供目标任务](https://finance.sina.com.cn/china/2022-04-01/doc-imcwipii1893408.shtml)
> 概要: 新华社北京4月1日电 全国玉米水稻生产工作推进电视电话会议1日在京召开。中共中央政治局委员、国务院副总理胡春华出席会议并讲话。
# 小说
### [阴阳神婿](http://book.zongheng.com/book/945014.html)
> 作者：焚书坑己

> 标签：都市娱乐

> 简介：文能提笔安天下，武能上马定乾坤。拳头讲的是道理，耳光扇的是真理。

> 章节末：完本感言

> 状态：完本
# 论文
### [DirecFormer: A Directed Attention in Transformer Approach to Robust Action Recognition | Papers With Code](https://paperswithcode.com/paper/direcformer-a-directed-attention-in)
> 日期：19 Mar 2022

> 标签：None

> 代码：None

> 描述：Human action recognition has recently become one of the popular research topics in the computer vision community. Various 3D-CNN based methods have been presented to tackle both the spatial and temporal dimensions in the task of video action recognition with competitive results. However, these methods have suffered some fundamental limitations such as lack of robustness and generalization, e.g., how does the temporal ordering of video frames affect the recognition results? This work presents a novel end-to-end Transformer-based Directed Attention (DirecFormer) framework for robust action recognition. The method takes a simple but novel perspective of Transformer-based approach to understand the right order of sequence actions. Therefore, the contributions of this work are three-fold. Firstly, we introduce the problem of ordered temporal learning issues to the action recognition problem. Secondly, a new Directed Attention mechanism is introduced to understand and provide attentions to human actions in the right order. Thirdly, we introduce the conditional dependency in action sequence modeling that includes orders and classes. The proposed approach consistently achieves the state-of-the-art (SOTA) results compared with the recent action recognition methods, on three standard large-scale benchmarks, i.e. Jester, Kinetics-400 and Something-Something-V2.
### [DU-GAN: Generative Adversarial Networks with Dual-Domain U-Net Based Discriminators for Low-Dose CT Denoising | Papers With Code](https://paperswithcode.com/paper/du-gan-generative-adversarial-networks-with)
> 日期：24 Aug 2021

> 标签：None

> 代码：https://github.com/hzzone/du-gan

> 描述：LDCT has drawn major attention in the medical imaging field due to the potential health risks of CT-associated X-ray radiation to patients. Reducing the radiation dose, however, decreases the quality of the reconstructed images, which consequently compromises the diagnostic performance.
