---
title: 2021-11-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ElephantGiving_ZH-CN9743352473_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-30 21:59:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ElephantGiving_ZH-CN9743352473_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [MaxKey 单点登录认证系统 v3.1.1GA 发布](https://www.oschina.net/news/171343/maxkey-3-1-1-ga)
> 概要: OSCHINA 2021 中国开源开发者问卷启动 | 填问卷送好礼>>>>>English|中文概述MaxKey单点登录认证系统，谐音马克思的钥匙寓意是最大钥匙,是业界领先的企业级IAM身份管理和认证......
### [警惕！双平台挖矿僵尸网络 Sysrv-hello 盯上用户 GitLab 服务器](https://www.oschina.net/news/171436)
> 概要: OSCHINA 2021 中国开源开发者问卷启动 | 填问卷送好礼>>>>>报告编号：B6-2021-112903报告来源：360高级威胁研究分析中心报告作者：360高级威胁研究分析中心更新日期：20......
### [APISIX-Datadog 插件发布，助力用户提高系统的可观测性](https://www.oschina.net/news/171364)
> 概要: OSCHINA 2021 中国开源开发者问卷启动 | 填问卷送好礼>>>>>随着应用开发的复杂度增加，监控成为了应用的一个重要组成部分。及时、准确的监控既能满足快速迭代的周期性需求，又能够确保应用的稳......
### [版本通告｜Apache Doris 0.15 Release 版本正式发布！](https://www.oschina.net/news/171366)
> 概要: OSCHINA 2021 中国开源开发者问卷启动 | 填问卷送好礼>>>>>亲爱的社区小伙伴们，历时数个月精心打磨，我们很高兴地宣布，Apache Doris 于 2021 年 11 月 29 日迎来......
### [搞懂 Kubernetes 准入控制（Admission Controller）](https://segmentfault.com/a/1190000041037182?utm_source=sf-homepage)
> 概要: 大家好，我是张晋涛。在我之前发布的文章《云原生时代下的容器镜像安全》（系列）中，我提到过 Kubernetes 集群的核心组件 --  kube-apiserver，它允许来自终端用户或集群的各组件与......
### [前端er，什么时候，你想写一个 HTTP 服务器？](https://segmentfault.com/a/1190000041037884?utm_source=sf-homepage)
> 概要: 前端 er，什么时候，你想写一个 HTTP 服务器？当你第一次接触工程化的项目时，看到项目控制台正在 building，过一会突然跳出一个 URL 地址，你点开它居然是你刚写好的网页，好神奇。当你接后......
### [FREEing《鬼灭之刃》炼狱杏寿郎1/4比例手办](https://news.dmzj.com/article/72906.html)
> 概要: FREEing根据《鬼灭之刃》中的炼狱杏寿郎制作的1/4比例B-style系列手办目前已经开订了。本作采用了炼狱杏寿郎使用奥义“玖之型·炼狱”前的一瞬间的样子。身上受的伤和队服的污迹等也一并再现了出来。
### [P站美图推荐——睡衣特辑（二）](https://news.dmzj.com/article/72908.html)
> 概要: 宽松舒适温暖毛绒的睡衣搭配无防备美少女，best match！
### [日本网友评选留下了“心理创伤”的5部动画作品](https://news.dmzj.com/article/72909.html)
> 概要: 近日，日本ORICON NEWS就在官方推特上，请网友选出了能留下“心理创伤”的动画。根据网友的反馈，《Another》、《寒蝉鸣泣之时》、《魔法少女小圆》、《学园孤岛》和《战栗杀机》五部作品登榜。
### [TV动画《最游记RELOAD -ZEROIN-》公开宣传PV](https://news.dmzj.com/article/72910.html)
> 概要: TV动画《最游记RELOAD -ZEROIN-》宣布了将于2022年1月6日开始播出的消息。本作的新PV也一并公开。
### [4x Smaller, 50x Faster](https://blog.asciinema.org/post/smaller-faster/)
> 概要: 4x Smaller, 50x Faster
### [日本发行商表示 PlayStation用户正逐渐向PC过渡](https://www.3dmgame.com/news/202111/3829535.html)
> 概要: 二十多年来，PlayStation一直是日本游戏主要发行的平台，尤其是一些小型的游戏项目。然而最近，日本国内受PS5供应限制的影响，游戏软件的销量也成下降趋势。一位不愿透露姓名的日本发行商高管表示索尼......
### [有赞CEO白鸦：公域投放是赌场逻辑，视频号未来一年会增长很好](https://www.tuicool.com/articles/3EjueyJ)
> 概要: 作者|张超 编辑|罗丽娟国货消费火热的当下，越来越多的国产新品牌受到市场欢迎。根据天猫发布的双11数据，11月1日，有275家新品牌平均连续3年翻倍增长，90个新品牌连续3年蝉联细分赛道第一，这届新品......
### [CD box sets are wonderful](https://smackeyacky.blogspot.com/2021/11/cd-box-sets-are-wonderful.html)
> 概要: CD box sets are wonderful
### [视频：王岳伦三天约会两女子 被拍四次发三次声明态度大转变](https://video.sina.com.cn/p/ent/2021-11-30/detail-ikyamrmy5914424.d.html)
> 概要: 视频：王岳伦三天约会两女子 被拍四次发三次声明态度大转变
### [微信最后还是妥协了](https://www.huxiu.com/article/477265.html)
> 概要: 题图来自：视觉中国微信公众号正密集地尝试着一系列的产品新动作。据文章《微信“地震式”改版，新一轮洗牌开始了？》披露，微信在内容算法上动作越来越大。在披露的灰度测试版本中，订阅的内容会被折叠，同时在折叠......
### [尾田荣一郎绘制三刀流娜美 真美女剑豪登场](https://acg.gamersky.com/news/202111/1441756.shtml)
> 概要: 尾田荣一郎绘制的“最新梦之一图”公开了，三刀流娜美登场。娜美带着三把刀登场，十分帅气。
### [「未闻花名」ED「secret base ~君がくれたもの~」10周年版本试听公开](http://acg.178.com/202111/432242921642.html)
> 概要: 由A-1 Pictures制作的原创电视动画「未闻花名」近期公开了ED主题曲「secret base ~君がくれたもの~」10周年版本试听片段，系列BD包含了动画全11话和剧场版动画的全部内容，定价为......
### [雨宫天单曲「ロンリーナイト・ディスコティック」MV短片公开](http://acg.178.com/202111/432243529400.html)
> 概要: 近期，知名声优雨宫天公开了单曲「ロンリーナイト・ディスコティック」的MV短片，该曲收录于专辑「-BLUE-/-RED-」，作词作曲均为雨宫天本人，专辑将于2022年1月5日发售。「ロンリーナイト・ディ......
### [CG电影《FF15：王者之剑》4K重制版2月9日发售](https://www.3dmgame.com/news/202111/3829564.html)
> 概要: 史艾曾经于2016年推出的CG电影《最终幻想15：王者之剑》讲述了《最终幻想15》的外传故事，11月30日今天官方宣布本片将会4K重制，珍藏蓝光盒子确定2022年2月9日发售，敬请期待。《最终幻想15......
### [《光环：无限》实体版国外已经偷跑 小心被剧透](https://www.3dmgame.com/news/202111/3829567.html)
> 概要: 经过多年的开发和长时间的跳票，《光环：无限》终于将于12月8日发售。但是在发售之前，《光环：无限》实体版已经开始在国外偷跑了。据游戏分析师NauroNL在推特上公布的消息，目前，《光环：无限》实体版已......
### [「月与莱卡与吸血公主」Blu-ray BOX上卷内层封面公开](http://acg.178.com/202111/432244761113.html)
> 概要: 电视动画「月与莱卡与吸血公主」公开了Blu-ray BOX上卷的内层封面图，是由かれい绘制的三人组——伊琳娜、列夫和阿妮雅。「月与莱卡与吸血公主」Blu-ray BOX上卷收录了一共185分钟的内容，......
### [组图：《爸爸去哪儿》第五季萌娃重聚 逛动物园玩游戏十分欢乐](http://slide.ent.sina.com.cn/z/v/w/slide_4_704_364142.html)
> 概要: 组图：《爸爸去哪儿》第五季萌娃重聚 逛动物园玩游戏十分欢乐
### [AI打王者、星际争霸……你还不懂背后技术？这有一份游戏AI综述](https://www.tuicool.com/articles/7VrENrf)
> 概要: 机器之心报道机器之心编辑部游戏中的人工智能所面临的技术、挑战和机遇。人机游戏有着悠久的历史，已经成为验证人工智能关键技术的主流。图灵测试可以说是人类首次进行人机对抗测试，这激发了研究人员设计各类 AI......
### [GSC逆转裁判成步堂龙一御剑怜侍粘土人 售价308元](https://www.3dmgame.com/news/202111/3829571.html)
> 概要: GSC今日（11月30日）推出了《逆转裁判》系列成步堂龙一和御剑怜侍的粘土人，11月30日起开始预约，单个售价5500日元，约合人民币308元。成步堂龙一粘土人拥有三种表情：标准、骄傲、冷汗，御剑怜侍......
### [「白沙的水族馆」C99新商品使用插画公开](http://acg.178.com/202111/432252143218.html)
> 概要: 电视动画「白沙的水族馆」公开了C99新商品的使用插画及商品图，商品包括B1挂画、等身大小的挂画、亚克力立牌和透明文件夹等。「白沙的水族馆」是由P.A.WORKS制作的原创电视动画，于2021年7月8日......
### [三倍速烹饪 万代推《高达》夏亚专用魔蟹造型砂锅](https://acg.gamersky.com/news/202111/1441831.shtml)
> 概要: 万代模型部于近日推出了夏亚专用魔蟹头部造型的砂锅。
### [中国音乐节二十年：寻找下一个乌托邦](https://www.huxiu.com/article/477342.html)
> 概要: 本文来自微信公众号：猛犸工作室（ID：MENGMASHENDU），作者：郭梓昊、石恩泽，编辑：马妮，头图来自：视觉中国“无趣。”从业20年的音乐人钟立民有些不屑。近些年，音乐节主题越来越花里胡哨，演出......
### [《30岁魔法师》将拍电影版 原班人马再续恋爱故事](https://ent.sina.com.cn/2021-11-30/doc-ikyamrmy5967760.shtml)
> 概要: 《到了30岁还是处男，似乎会变成魔法师》改编自丰田悠销量超过180万部的人气漫画，赤楚卫二饰演的上班族安达清到了三十岁还保持处男之身，因此获得了“只要接触，就能读取人心的魔法”，从而听到了同事黑泽优一......
### [视频：李湘王岳伦商业版图已无关联 婚变当月变更股权](https://video.sina.com.cn/p/ent/2021-11-30/detail-ikyamrmy5971145.d.html)
> 概要: 视频：李湘王岳伦商业版图已无关联 婚变当月变更股权
### [组图：玉木宏出席活动 谈成为父亲后更注重环保](http://slide.ent.sina.com.cn/star/jp/slide_4_704_364148.html)
> 概要: 组图：玉木宏出席活动 谈成为父亲后更注重环保
### [元宇宙创始人并不看好元宇宙 并不适合所有人](https://www.3dmgame.com/news/202111/3829592.html)
> 概要: 创造最初元宇宙的人，网游《Second Life》设计师Philip Rosedale，对他的发明并没有很高的期望。虽然最近许多人都加入了这股潮流，但他认为，元宇宙不会取得多大的成功。Rosedale......
### [《镜·双城》X虎牙直播专场开启，与超人气主播相约云荒！](https://new.qq.com/omn/ACF20211/ACF2021113000491500.html)
> 概要: 由企鹅影视、腾讯影业、腾讯动漫共同出品的《镜·双城》动画，正在腾讯视频独家热播中！十三年前，《镜·双城》原著可是赚足了无数书迷的感动与眼泪，十三年后，盛大的云荒史诗又在荧幕上再度燃起锋烟。高耸入云的伽......
### [P1推出猫女1/3全新雕像 哥谭魅影俏皮眨眼](https://acg.gamersky.com/news/202111/1441889.shtml)
> 概要: 玩具制造商Prime1Studio于近日公布了DC漫画角色猫女的全新雕像。
### [Managing Risks in Research](https://alastairreid.github.io/research-risks/)
> 概要: Managing Risks in Research
### [《JOJO石之海》12月1日B站独播 一次性全集放送](https://acg.gamersky.com/news/202111/1441898.shtml)
> 概要: 根据哔哩哔哩番剧官方的消息，动画《JOJO的奇妙冒险：石之海》将于12月1日哔哩哔哩独家播出，并且是一次性全集播出。
### [言情杂志，90后告别的躲猫猫游戏](https://www.tuicool.com/articles/UvAz2ue)
> 概要: 文 | 观娱象限，作者 | 琢介 ，编辑 | 缈秒短短一周时间，言情杂志巨头“魅丽文化”旗下的两本杂志便相继宣布停刊。11月16日，《桃之夭夭》停刊，8天后《飞言情》休刊。而在去年4月，另一本青春杂志......
### [三星已有两条 LCD 生产线转向 OLED 仅牙山 LCD 生产线仍在运行](https://www.ithome.com/0/589/678.htm)
> 概要: 11 月 29 日消息，据国外媒体报道，在明年就将退出 LCD 面板市场的三星显示，已经多条生产线转向 OLED 面板，目前仅有一条 LCD 面板生产线仍在运行。从外媒的报道来看，三星显示的 LCD ......
### [朗科推出绝影 DDR5 RGB 内存条：4800MHz，时序 CL40](https://www.ithome.com/0/589/681.htm)
> 概要: IT之家11 月 30 日消息，今天，朗科发布绝影 DDR5 RGB 内存条，首批推出 4800MHz 16GX2 套装。据介绍，绝影 DDR5 RGB 内存条采用全新电镀银工艺的马甲，对称式设计，拥......
### [新能源补贴都肥了谁？](https://www.huxiu.com/article/477472.html)
> 概要: 出品｜虎嗅汽车组作者｜梓楠法师人们总有一种错觉，认为新能源补贴缩短了强者和弱者的差距。但事实上，新能源汽车的补贴，本就偏向综合能力更强的车企。工信部和财政部每年都会发一份关于新能源补贴的清单，这份清单......
### [不买房的年轻人：在这里生活的是我，不是房东](https://www.huxiu.com/article/477447.html)
> 概要: 本文来自微信公众号：青年志Youthology（ID：openyouthology001），编辑：璐璐、Sharon，头图来自：《欢乐颂》“家”字上的“宀”，最早在甲骨文中就意味着有墙有顶的房屋。“富......
### [首发 3499 元，小米米家冰箱对开门尊享版 540L 开售，搭载 8 英寸触控屏](https://www.ithome.com/0/589/688.htm)
> 概要: IT之家11 月 30 日消息，日前，小米推出米家冰箱尊享版 540L 对开门型号，搭载 8 英寸触控屏，内置小爱同学。这款冰箱 11 月 30 日 20:00 点首发开卖，预约到手价 3499 元......
### [一秒都不舍得快进，这期《时光音乐会》又让我“破防”了](https://new.qq.com/rain/a/20211130A0AS5800)
> 概要: 《时光音乐会》第六期——            众时光音乐人将共同演绎张杰的音乐人生，用歌声带观众们一起走进张杰的音乐时光。跨越音乐时光17年，张杰回到了2004年，回到了音乐人生的起点。当那句“你好......
### [巨兴茂娱圈逆袭记：从丑角到知名导演，迎娶美娇妻，7年财产过亿](https://new.qq.com/rain/a/20211130A0B04M00)
> 概要: 1990年，李少红导演在拍摄《血色清晨》时，有一个小男孩的角色迟迟未曾定下。正当她发愁时，一个个头矮小眯缝眼，活像个“小老鼠”的小孩闯入她的视线。            踏遍铁鞋无觅处,得来全不费功夫......
### [Show HN: Improve your Python regex skills with 75 interactive exercises](https://github.com/learnbyexample/py_regular_expressions/tree/master/interactive_exercises)
> 概要: Show HN: Improve your Python regex skills with 75 interactive exercises
### [MCN的“傀儡”游戏](https://www.tuicool.com/articles/r67j2eM)
> 概要: “这一行变化的确实很快，不过新的东西也多，未来还有很长的路可以走。”“我对MCN行业的未来还是比较看好的，现在多个平台对内容的需求都在增加。”“虽然烧钱很厉害，但行业钱景也是很乐观的。”这是三位MCN......
### [现象级作品归来，中芭再度复排《奥涅金》](https://new.qq.com/rain/a/20211130A0BAP500)
> 概要: 现象级作品归来，中芭再度复排《奥涅金》
### [“他打12345，我有一百个法子对付他”社区书记，停职！](https://finance.sina.com.cn/china/2021-11-30/doc-ikyakumx1208396.shtml)
> 概要: “他打12345投诉，我有一百个法子对付他，我有一百个法子回复他。”郭女士表示，该男子为社区党支部书记韩立广。 11月29日，有网友发布录音称...
### [连夜狂追12集，25岁小花终于证明自己，章若楠这次要爆？](https://new.qq.com/rain/a/20211130A0BLSY00)
> 概要: 有着悬疑惊悚氛围，推理搞笑偶像元素等多种类型糅杂的当代都市剧《女心理师》仍在热播中，虽然开播不久剧集口碑就呈现出了严重的两极化现象，但也同时证明了该作品的热度确实够高，不管是好评还是差评，都是基于对作......
### [财经TOP10|远东集团被依法查处后，董事长表态反对“台独”](https://finance.sina.com.cn/china/caijingtop10/2021-11-30/doc-ikyamrmy6035213.shtml)
> 概要: 【政经要闻】 NO.1 刘鹤：对于明年中国经济，我们抱有充足信心 11月30日，中共中央政治局委员、国务院副总理刘鹤应邀以视频方式出席第九届中欧论坛汉堡峰会并发表主旨演讲...
### [上海轨道交通上半年日均客流963万人次，较2019年同期下降7.4%](https://finance.sina.com.cn/china/gncj/2021-11-30/doc-ikyakumx1210806.shtml)
> 概要: 原标题：上海轨道交通上半年日均客流963万人次，较2019年同期下降7.4% 证券时报网讯，据上海市交通委指挥中心11月30日消息，在轨道交通方面...
### [应急管理部公布3起违规收费典型案例](https://finance.sina.com.cn/jjxw/2021-11-30/doc-ikyamrmy6035600.shtml)
> 概要: 原标题：应急管理部公布3起违规收费典型案例 根据应急管理部党委党史学习教育“我为群众办实事”活动安排，按照《国务院办公厅关于进一步规范行业协会商会收费的通知》（国办...
### [惠建林已任江苏省委统战部部长](https://finance.sina.com.cn/china/dfjj/2021-11-30/doc-ikyakumx1212526.shtml)
> 概要: 原标题：惠建林已任江苏省委统战部部长 刚刚当选为江苏省委常委的惠建林，其职务分工现已明确。 近日，江苏省委统战部“江苏统一战线”网站“部领导介绍”一栏更新显示...
### [同居第四年分手，女方买的房和车男方竟要分走一半？这事儿法院判了……](https://finance.sina.com.cn/china/gncj/2021-11-30/doc-ikyamrmy6036583.shtml)
> 概要: 原标题：同居第四年分手，女方买的房和车男方竟要分走一半？这事儿法院判了…… 杭州市西湖区人民法院微信公众号11月29日披露了这样一起案件： 同居第4年分了手，男方认为...
### [斗罗大陆，领先唐三，千仞雪即成神，太快弊端很明显](https://new.qq.com/omn/20211130/20211130A0BX6700.html)
> 概要: hello ，大家好，我是小白            目前《斗罗大陆》的剧情进程已经过半，史莱克七怪的海神试炼已经进行到了第四考，完成海神试炼也只是时间问题与此同时，比比东和千仞雪也在成神路上不断修炼......
### [大口吃肉，爽！周家口招牌牛肉 100g×5 袋 59.6 元](https://lapin.ithome.com/html/digi/589701.htm)
> 概要: 五香酱卤/鲜嫩大块，周家口招牌牛肉 100g/袋报价 29.9 元，叠加满 119.6 元减 45 元 + 拍 4 送 1优惠，限时限量 15 元券，拍 4 件共发 5 袋实付 59.6 元包邮，领券......
### [海贼王1034话情报：标题是山治VS奎因，奎因会使用杰尔马的战斗技能](https://new.qq.com/omn/20211130/20211130A0C04600.html)
> 概要: 海贼王1034话情报迎来第一次更新，尾田已经连续更新三周时间，下周休刊是必定的。和之国战争进入决战阶段，路飞与凯多的战斗还没有分出胜负，现在轮到干部之间的战斗。从标题就可以看出，这一话的主要内容还是讲......
### [眷思量：镜玄为了养活丽娘去卖艺，替丽娘撑场面，有霸总的感觉了](https://new.qq.com/omn/20211130/20211130A0AYPC00.html)
> 概要: 在《眷思量》动漫中，丽娘是一介凡人，虽说与神仙们一同生活在思量岛，但说到底依旧不招人待见。幸亏有镜玄一直默默地守护着她，能让她在思量岛上安全无虞。而在《眷思量》的番外漫画中，两人的日常则格外地甜。  ......
# 小说
### [大帝图](http://book.zongheng.com/book/1047484.html)
> 作者：道爷爱梦游

> 标签：奇幻玄幻

> 简介：万物生与寰宇内，长帆远去，何物染碧空造化尽生天地间，山水汇聚，我在天地中浊酒入喉，背剑高歌人间事醉眼人间，举杯真应笑我这是一个修行者与凡人的世界这是一个寻找与生存的故事

> 章节末：大结局，告一段落

> 状态：完本
# 论文
### [Comparison of single and multitask learning for predicting cognitive decline based on MRI data | Papers With Code](https://paperswithcode.com/paper/comparison-of-single-and-multitask-learning)
> 日期：21 Sep 2021

> 标签：None

> 代码：None

> 描述：The Alzheimer's Disease Assessment Scale-Cognitive subscale (ADAS-Cog) is a neuropsychological tool that has been designed to assess the severity of cognitive symptoms of dementia. Personalized prediction of the changes in ADAS-Cog scores could help in timing therapeutic interventions in dementia and at-risk populations.
### [Estimating Expected Calibration Errors | Papers With Code](https://paperswithcode.com/paper/estimating-expected-calibration-errors)
> 日期：8 Sep 2021

> 标签：None

> 代码：https://github.com/euranova/estimating_eces

> 描述：Uncertainty in probabilistic classifiers predictions is a key concern when models are used to support human decision making, in broader probabilistic pipelines or when sensitive automatic decisions have to be taken. Studies have shown that most models are not intrinsically well calibrated, meaning that their decision scores are not consistent with posterior probabilities.
