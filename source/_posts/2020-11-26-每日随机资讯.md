---
title: 2020-11-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ChipmunkonThanksgiving_EN-CN5812463250_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-11-26 21:44:54
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ChipmunkonThanksgiving_EN-CN5812463250_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [丝芭文化强调是赵嘉敏独家经纪公司 仍在合约期内](https://ent.sina.com.cn/s/2020-11-26/doc-iiznctke3350196.shtml)
> 概要: 新浪娱乐讯 26日，丝芭文化再发声明强调仍是赵嘉敏的独家经纪公司，与她仍在合约履行期限内，任何第三方擅自与其进行演艺合作均是扰乱行业的不正当竞争行为，将会法律维权。25日晚，赵嘉敏称与丝芭传媒的近经纪......
### [马拉多纳尸检结果为自然死亡：没有暴力侵袭迹象](https://ent.sina.com.cn/s/u/2020-11-26/doc-iiznctke3325007.shtml)
> 概要: 新浪娱乐讯 当地时间25日上午，阿根廷球星马拉多纳在其位于阿根廷的家中突发心梗去世，享年60岁。《奥莱报》表示，“没有任何办法再挽救迭戈的生命”。为了查明马拉多纳的死因。当地时间11月25日18点，当......
### [黄健翔向C罗道歉 曾指责其悼念马拉多纳用词错误](https://ent.sina.com.cn/s/m/2020-11-26/doc-iiznctke3335059.shtml)
> 概要: 新浪娱乐讯 北京时间26日凌晨，足坛巨星马拉多纳去世，C罗第一时间发文悼念，但黄健翔却对C罗的用词进行“纠错”：“请你把（有史以来最好的球员）‘之一’二字拿掉。大家都知道你很棒。但是此时此刻，没有别人......
### [组图：宝黛相会!林青霞晒与62版林黛玉合影 粉丝P剧照获赞](http://slide.ent.sina.com.cn/star/w/slide_4_86512_348848.html)
> 概要: 组图：宝黛相会!林青霞晒与62版林黛玉合影 粉丝P剧照获赞
### [组图：韩星高敏诗申秀铉出席《不表白的理由》发布会](http://slide.ent.sina.com.cn/tv/k/slide_4_704_348852.html)
> 概要: 组图：韩星高敏诗申秀铉出席《不表白的理由》发布会
### [为什么我们应该恭喜新上海人杨超越？](https://new.qq.com/omn/20201126/20201126A0EKRI00.html)
> 概要: 昨天（11月25日），“杨超越落户上海”冲上了热搜，成为了一个充满争议的公共事件。事情也不并不复杂，11月24日，上海自贸区临港管委会公布了新一批“特殊人才引进名单”，杨超越在列。          ......
### [音乐人祭拜高以翔后于墓地倒地而亡，高以翔经纪公司回应：不认识但非常难过](https://new.qq.com/omn/20201126/20201126A0BVYA00.html)
> 概要: 11月27日是高以翔去世一周年，无论是家人还是粉丝，始终无法忘掉这个阳光的大男孩，但上天似乎要把“恶作剧”进行到底，一场突如其来的事故，让高以翔猝逝又蒙上一层阴影。            台媒26日报......
### [人设彻底崩塌！赖冠霖抽烟吐痰风波后，机场骂路人：你们俩是不是有病](https://new.qq.com/omn/20201126/20201126V0930H00.html)
> 概要: 人设彻底崩塌！赖冠霖抽烟吐痰风波后，机场骂路人：你们俩是不是有病
### [下一个孟子义？黄奕倪虹洁拍戏擅自改台词，尔冬升现场发飙罢拍](https://new.qq.com/omn/20201126/20201126V0AECT00.html)
> 概要: 下一个孟子义？黄奕倪虹洁拍戏擅自改台词，尔冬升现场发飙罢拍
### [张歆艺生娃后感到被强烈改造，坦言羡慕袁弘没变化，自己的幸福代价很大](https://new.qq.com/omn/20201126/20201126V06FJ600.html)
> 概要: 张歆艺生娃后感到被强烈改造，坦言羡慕袁弘没变化，自己的幸福代价很大
# 动漫
### [2020年，我看到了最让我无法接受的动画子世代……](https://news.dmzj.com/article/69357.html)
> 概要: 我以为，在看过了《博人传》、《半妖的夜叉姬》等一系列以经典动画子世代为主角的续作后，我的接受能力已经大幅提高，但是没想到，2020年11月，一部作品的新图却让我彻底破防……
### [冲浪题材动画《WAVE!!》1月TV开播！新PV公开](https://news.dmzj.com/article/69362.html)
> 概要: 于10月连续上映三部曲的剧场版动画《WAVE!!》的TV版，宣布了将于2021年1月11日开始播出的消息。配合着TV版放送决定，新主宣图与PV也一并公开。在PV中，收录了TV动画的OP主题曲《刺激冲浪男孩！》的片段。
### [原创TV动画《剧偶像》1月5日开播！新宣传图公开](https://news.dmzj.com/article/69355.html)
> 概要: 原创TV动画《剧偶像》宣布了将于2021年1月5日开始在AT-X等电视台播出的消息，本作的主宣传图也一并公开。
### [Phat!《少女前线》FN57芬芬历险记1/7比例手办](https://news.dmzj.com/article/69363.html)
> 概要: Phat!根据人气手游《少女前线》中的FN57制作的1/7比例手办目前已经开订了。本作采用了“童话之日”主题皮肤中登场的“芬芬历险记”的FN57的造型，武器和宠物也一并进行了细致的再现。
### [一代球王马拉多纳离世，影响力曾达二次元，是这个超强角色的原型](https://new.qq.com/omn/20201126/20201126A08XPU00.html)
> 概要: #足球小将#导语：阿根廷当地时间11月25日，阿根廷足球巨星迭戈·马拉多纳在布宜诺斯艾利斯的家中因心脏骤停去世，享年60岁。作为阿根廷人几代的精神支柱，一代球王马拉多纳的形象值得被铭记到很多人的心里……
### [进击的巨人：吉克的命运像是高槻泉，身体被控制成为超强武器](https://new.qq.com/omn/20201126/20201126A06BED00.html)
> 概要: 《进击的巨人》和《东京吃货》虽然剧情发展天差地别，但是有不少相似的设定，比如出现了以人为食的怪物，主角又是可以介于二者之间的存在。《进击的巨人》中的吉克命运和人设就像极了《东京吃货》的高槻泉，我们来……
### [火影忍者：为什么说长大后才明白最后鸣人选择了雏田的原因？](https://new.qq.com/omn/20201126/20201126A09VVN00.html)
> 概要: 火影忍者这部动漫相信大家都很熟悉了，这是一部经典的热血动漫。虽然这部动漫已经完结，但是这部动漫的人气始终不减。今天小编给大家讲解的是主角鸣人和日向雏田的爱情故事，其实二者的爱情在火影前期就已经开始了……
### [晓组织实力垫底的迪达拉和蝎，真的只是颜值担当吗？](https://new.qq.com/omn/20201126/20201126A0776700.html)
> 概要: 火影忍者这部动漫相信大家都很熟悉了，这是一部经典的热血动漫。虽然这部动漫已经完结，但是这部动漫的人气始终不减。今天小编给大家介绍的是晓组织，晓组织可以说是火影动漫中人气最高的反派组织了，而在晓组织中……
### [《海贼王》路飞的草帽共有4个人戴过，其中3个都是女的，只有他例外](https://new.qq.com/omn/20201126/20201126A09LOJ00.html)
> 概要: 《海贼王》堪称目前世界漫画史上的巅峰之作，在全世界都拥有着许多的粉丝，作者尾田荣一郎先生也凭借着这部作品成为了世界上赫赫有名的漫画家。《海贼王》自从1999年开始连载，至今已经整整21年了。大家都知……
### [弗利沙一直纠结于使用龙珠让自己长高，身高真的那么重要吗？](https://new.qq.com/omn/20201126/20201126A07K0G00.html)
> 概要: 导语龙珠中有一个非常著名的反派，他的名字叫做弗利沙，弗利沙的个子非常矮小，但是实力却非常恐怖，曾经让孙悟空吃了不少苦头，如果孙悟空不能变成超级赛亚人的话，早就被弗利沙杀死了，足见弗利沙的恐怖。但是弗……
### [宫崎骏经典动画《崖上的波妞》确定引进 档期待定](https://acg.gamersky.com/news/202011/1340890.shtml)
> 概要: 宫崎骏经典动画电影《崖上的波妞》在今天（11月26日）宣布将引进中国内地上映，并且公开了中文海报，目前本作还未公布档期。
### [D站创始人获刑三年三个月 因侵犯著作权罪](https://acg.gamersky.com/news/202011/1341145.shtml)
> 概要: 11月26日，D站相关人员涉嫌侵犯著作权案在上海市徐汇区人民法院公开开庭审理。
### [剧场版动画《冰上的尤里》新预告 长发维克托亮相](https://acg.gamersky.com/news/202011/1341135.shtml)
> 概要: 剧场版动画《冰上的尤里：ICE ADOLESCENCE》，官方公开了剧场版动画的特报视频，长发维克托登场亮相，真是超级帅。
### [《宝可梦：超梦的逆袭》全新预告 皮卡丘萌力满格](https://acg.gamersky.com/news/202011/1340934.shtml)
> 概要: 众人期待的精灵宝可梦系列第22部剧场版《宝可梦：超梦的逆袭 进化》将于12月4日全国上映，萌化无数人的精灵宝可梦即将再度登上银幕！
# 财经
### [哈尔滨“六稳”“六保”措施落地见效 三季度GDP同比增长4.3%](https://finance.sina.com.cn/china/dfjj/2020-11-26/doc-iiznezxs3869565.shtml)
> 概要: 原标题：三季度GDP同比增长4.3%！哈尔滨“六稳”“六保”措施落地见效 三季度当季黑龙江省哈尔滨市地区生产总值同比增长4.3%；规上工业增加值同比增长8.3%。
### [发改委：力争年底前出台措施 解决老年人运用智能技术问题](https://finance.sina.com.cn/china/bwdt/2020-11-26/doc-iiznctke3449391.shtml)
> 概要: 原标题：发改委：力争年底前出台措施，解决老年人运用智能技术问题国家发展改革委秘书长赵辰昕（徐想 摄） 11月26日，国新办举行《关于切实解决老年人运用智能技术困难实施...
### [香港特区政府增加“优秀人才入境计划”配额](https://finance.sina.com.cn/china/dfjj/2020-11-26/doc-iiznctke3447671.shtml)
> 概要: 原标题：香港特区政府增加“优秀人才入境计划”配额 新华社香港11月26日电 香港特区政府26日公布，“优秀人才入境计划”（简称“优才计划”）的配额由每年1000名增加至2000名。
### [深圳市地方金融监督管理局披露"深圳市扶持金融科技发展若干措施"](https://finance.sina.com.cn/china/dfjj/2020-11-26/doc-iiznezxs3874188.shtml)
> 概要: 深圳市地方金融监督管理局披露《深圳市扶持金融科技发展若干措施（征求意见稿）》，鼓励供应链金融发展，对使用区块链等金融科技手段...
### [首席经济学家前瞻2021：中国经济将持续复苏](https://finance.sina.com.cn/china/gncj/2020-11-26/doc-iiznezxs3877326.shtml)
> 概要: 原标题：首席经济学家前瞻2021：中国经济将持续复苏 作者：石尚惠 全球疫情步入下半场，中国经济在全球最先走上复苏之路后，明年又将怎样？
### [财经TOP10|老人被高科技抛弃如何解决？发改委、央行支招](https://finance.sina.com.cn/china/caijingtop10/2020-11-26/doc-iiznezxs3877385.shtml)
> 概要: 【宏观要闻】 NO.1 央行：既保持流动性合理充裕 又坚决不搞“大水漫灌” 今日，央行发布2020年第三季度中国货币政策执行报告，报告称，下一阶段...
# 科技
### [响应式网页中的高度设计，你认真的吗？](https://segmentfault.com/a/1190000038172070)
> 概要: 作者：Ahmad Shadeed译者：前端小智来源：ishadeed点赞再看，养成习惯本文GitHubhttps://github.com/qq44924588...上已经收录，更多往期高赞文章的分类......
### [程序员小技巧带来高效率系列之（一）：好用的vim 块操作 技巧 和 %V块内替换](https://segmentfault.com/a/1190000038285739)
> 概要: 日期作者版本备注2020-11-26dingbinv1.0vim是几乎所有程序员日常coding必用工具，甚至是很多非程序员同学日常文本编辑必用工具。如何提高vim编辑文本或代码效率，是很多程序员非常......
### [流媒体设备大战流媒体网络，吸引眼球是关键](https://www.ithome.com/0/521/599.htm)
> 概要: 当《神奇女侠 1984（Wonder Woman 1984）》在圣诞节上映时，HBO Max 的大多数订阅用户都可以在家中观看这部电影。然而最重要的是，目前，HBO Max 的订阅用户还不包括那些使用......
### [华为：2015 年 Mate 系列每条生产线需要 97 个人，现在 Mate 40 只需要 14 个人](https://www.ithome.com/0/521/590.htm)
> 概要: IT之家 11 月 26 日消息 华为云计算技术有限公司董事长、华为云业务总裁郑叶来在《财经》年会上表示，“我们生产的 Mate 系列，通过精细化成本管理，2015 年每条生产线需要 97 个人，今天......
### [How fast does interpolation search converge?](https://lemire.me/blog/2020/11/25/how-fast-does-interpolation-search-converge)
> 概要: How fast does interpolation search converge?
### [Origin of the trefoil radiation warning sign](https://www.orau.org/ptp/articlesstories/radwarnsymbstory.htm)
> 概要: Origin of the trefoil radiation warning sign
### [致打工人：方向不对，越努力越累](https://www.huxiu.com/article/395865.html)
> 概要: 本文来自微信公众号：书单（ID：BookSelection），作者：笔下长青，编辑：燕妮，题图来自：视觉中国美国著名的《科学》杂志，曾经报道过这样一个新闻：2005年，一个叫珍妮的女生在自家后院，发现......
### [“完美日记”背后的“天使投资日记”](https://www.huxiu.com/article/396160.html)
> 概要: 虎嗅机动资讯组作品作者 | 竺晶莹题图 | 受访者提供每当穿梭在机场，看到布满国际大牌的广告灯箱，方爱之都不免会想“什么时候能在这上面看到完美日记”。作为真格基金创始合伙人兼 CEO，方爱之在完美日记......
### [被嫌弃的程序员的35岁](https://www.tuicool.com/articles/ZVRBVbV)
> 概要: 编者按：本文来自微信公众号“AI前线”（ID:ai-front），作者：刘燕，36氪经授权发布。35岁，逃不掉中年危机？程序员的 35 岁“魔咒”不知道从什么时候起，35 岁变成了一个很“残酷”的年龄......
### [蔚来社区 | 双积分体系产品分析](https://www.tuicool.com/articles/3ie2Inf)
> 概要: 导语：用户社区作为积分体系的服务对象，大量被后入局的创新者使用；作为与用户高频接触的场所，不管是蔚来小鹏，还是小米，都是为产品积累产品口碑，传达品牌理念起了很大的作用；在分析积分体系前，希望通过建立用......
# 小说
### [逆流时光](http://book.zongheng.com/book/978563.html)
> 作者：北落南风

> 标签：都市娱乐

> 简介：时光是一场旅行，每个人都是过客。再活一次，顾前不要活的像前世那样。曾经年少的最爱，这一次，他不要放手，要陪她度过余生。曾经受过的伤，他要一一的讨回来。曾经欺负过他的人，他要一一把账算回来。......这一辈子，他不要平凡，他要伟大。

> 章节末：抱歉，写不下去了

> 状态：完本
# 游戏
### [育碧《舞力全开2021》联动拳头KDA曲目演示公开](https://www.3dmgame.com/news/202011/3802877.html)
> 概要: 《舞力全开 2021》与 Riot Games 的《英雄联盟》虚拟乐团 K/DA 携手合作，该乐团的新单曲「DRUM GO DUM」由 Aluna、Wolftyla 和 Bekuh BOOM 演唱，并......
### [苹果和脸书几乎没有竞争 为什么彼此看不上对方？](https://www.3dmgame.com/news/202011/3802893.html)
> 概要: 据报道，一些知名的大企业，他们之所以成为竞争对手，是因为有着相同的业务，如可口可乐和百事可乐，波音和空客，麦当劳和汉堡王。从这一角度讲，在全球科技市场，有两家公司之间的“过节”就显得耐人寻味，那就是F......
### [《糖豆人：终极淘汰赛》第三赛季主题为冬季淘汰赛](https://www.3dmgame.com/news/202011/3802846.html)
> 概要: 此前，《糖豆人：终极淘汰赛》官方推特曾放出一张第三赛季截图，并表示将在之后透露更多第三赛季的消息。今日（11.25），《糖豆人》官方推特发布了第三赛季的海报。并宣布，第三赛季的主题为“冬季淘汰赛”。不......
### [《麻布仔大冒险》IGN 8分：关卡设计可爱富有创意](https://www.3dmgame.com/news/202011/3802871.html)
> 概要: 《麻布仔大冒险》已于11月12日随PS5一起正式发售，IGN也为本作行进了评测，给出了8分的分数。评测视频：IGN评分：8分 优秀《麻布仔大冒险》是一个迷人的《小小大星球》衍生产品，其创造工具换成更直......
### [宫崎骏《崖上的波妞》确认引进内地 具体档期待定](https://www.3dmgame.com/news/202011/3802856.html)
> 概要: 继2018年的《龙猫》，2019年的《千与千寻》之后，又一部宫崎骏导演的动作电影确认引进内地上映。这一次将与观众们见面的是《崖上的波妞》（此前大家都将其称之为《悬崖上的金鱼姬》，此次院线上映版的译名略......
# 论文
### [Differentially Private Mixed-Type Data Generation For Unsupervised Learning](https://paperswithcode.com/paper/differentially-private-mixed-type-data-1)
> 日期：6 Dec 2019

> 标签：

> 代码：https://github.com/DPautoGAN/DPautoGAN

> 描述：In this work we introduce the DP-auto-GAN framework for synthetic data generation, which combines the low dimensional representation of autoencoders with the flexibility of Generative Adversarial Networks (GANs). This framework can be used to take in raw sensitive data, and privately train a model for generating synthetic data that will satisfy the same statistical properties as the original data.
### [Convolutional Character Networks](https://paperswithcode.com/paper/convolutional-character-networks)
> 日期：ICCV 2019

> 标签：SCENE TEXT DETECTION

> 代码：https://github.com/MalongTech/research-charnet

> 描述：Recent progress has been made on developing a unified framework for joint text detection and recognition in natural images;but existing joint models were mostly built on two-stage framework by involving ROI pooling;which can degrade the performance on recognition task. In this work;we propose convolutional character networks;referred as CharNet;which is an one-stage model that can process two tasks simultaneously in one pass.
