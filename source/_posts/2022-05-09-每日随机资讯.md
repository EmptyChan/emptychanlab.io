---
title: 2022-05-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GoremeNationalPark_ZH-CN1861727385_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=35
date: 2022-05-09 21:43:00
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GoremeNationalPark_ZH-CN1861727385_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=35)
# 新闻
### [图片素材网站系统，PicHome 个人版 1.0 发布](https://www.oschina.net/news/194900/pichome-1-0-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>欧奥 PicHome 个人版1.0 发布具体更新内容1.支持升级到团队版，可在站点设置->授权信息界面查看2.bill......
### [Solon 1.7.6 发布，更现代感的应用开发框架](https://www.oschina.net/news/194898/solon-1-7-6-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>相对于 Spring Boot 和 Spring Cloud 的项目启动快 5 ～ 10 倍qps 高 2～ 3 倍运......
### [Ubuntu 创始人解释为什么 Ubuntu 不支持 Flatpak](https://www.oschina.net/news/194891/ubuntu-wont-support-flatpak-anytime-soon)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>上个月，Ubuntu 22.04 LTS正式发布。在发布当日举办的庆祝活动中，Ubuntu 创始人 Mark Shut......
### [Netty 4.1.77.Final 发布](https://www.oschina.net/news/194873/netty-4-1-77-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>Netty 是一个异步事件驱动的网络应用框架，主要用于可维护的高性能协议服务器和客户端的快速开发。Netty 4.1......
### [居家办公，我更忙了](http://www.woshipm.com/it/5430456.html)
> 概要: 编辑导语：在常态化疫情防控的当下，居家办公、云办公等创新模式不断出现，这些模式可以让大家更自由地支配自己的时间，也许会让生活过得更丰富，也许会面临一些新的问题。这篇文章讲述了五个年轻人居家办公后的故事......
### [NFT系统简介](http://www.woshipm.com/it/5428974.html)
> 概要: 编辑导语：NFT即非同质化代币，近两年来，发展状况愈演愈烈、十分火热，它顺应了年轻人的喜好，成功吸引了年轻一代的追求。本篇文章对NFT进行了详细的、系统的介绍，希望能给您带来帮助。一起来看看吧。一、N......
### [北京24小时保供战：互联网抢菜大考](http://www.woshipm.com/it/5430749.html)
> 概要: 编辑导语：4月24日，北京掀起了一轮抢菜狂潮，此后由于疫情反复，各个平台也都经历着“抢菜大考”。疫情之下，互联网买菜平台们都是怎么做的呢？在这场团战背后又暴露出了什么问题？一起来看一下吧。过去几天，北......
### [刀子里找糖磕！赵露思深情告白杨洋，抱得对方喘不上气爱到骨子里](https://new.qq.com/rain/a/20220508V0AQGZ00)
> 概要: 刀子里找糖磕！赵露思深情告白杨洋，抱得对方喘不上气爱到骨子里
### [Emerging evidence that mindfulness can sometimes increase selfish tendencies](https://www.bbc.com/worklife/article/20220302-how-mindfulness-can-make-you-a-darker-person)
> 概要: Emerging evidence that mindfulness can sometimes increase selfish tendencies
### [倍安聪宠物羊奶粉](https://www.zcool.com.cn/work/ZNTk2ODQ4MTI=.html)
> 概要: 宠物食品市场被资本长期看好，近年的数据显示宠物的家长们给自家宠物的花销逐年快速攀升，而放眼国内市场，宠物食品行业销量的头部位置长期被国外品牌所占据。国内的宠物食品产业长期缺乏产品的标准化、品牌的辨识度......
### [没有哪一代人真正放弃](https://www.huxiu.com/article/549454.html)
> 概要: 本文来自微信公众号：格隆汇APP （ID：hkguruclub），作者：万连山，头图来自：视觉中国前两天正好是青年节，现在过了，激素散去，我们再来聊聊关于年轻人的话题。不知道有多少人看过，今年五四青年......
### [2套五一系列海报设计](https://www.zcool.com.cn/work/ZNTk2ODYwMDQ=.html)
> 概要: 刚整理好......
### [Meta元宇宙：人类新社区还是深度监控“抢钱工具”？](https://finance.sina.com.cn/tech/2022-05-09/doc-imcwipii8815968.shtml)
> 概要: 新浪科技讯 北京时间5月9日上午消息，据报道，虚拟现实头戴设备问世多年，许多青少年也买了这种数码设备，但是他们是否用类似智能手机的方式来使用头戴设备呢？到目前为止，答案是否定的......
### [漫画「花薰凛然」第二卷封面公开](http://acg.178.com/202205/446062069322.html)
> 概要: 漫画「花薰凛然」公开了最新卷第二卷的封面图及店铺购入特典插画，该卷于今日（5月9日）正式发售。「花薰凛然」（薫る花は凛と咲く）是三香見サカ创作、讲谈社出版的校园恋爱漫画作品。本作讲述了来自聚集了笨蛋的......
### [轻小说「我旁边座位上的昔日偶像，离不开我的制作」第一卷封面公开](http://acg.178.com/202205/446062188101.html)
> 概要: 富士见Fantasia文库公开了轻小说新作「我旁边座位上的昔日偶像，离不开我的制作」（隣の席の元アイドルは、俺のプロデュースがないと生きていけない）第一卷的封面图。本卷售价为748日元（含税），将于2......
### [P站美图推荐——白色水手服特辑](https://news.dmzj.com/article/74348.html)
> 概要: 让我看看2220年了谁还在天天听拿去吧水手服.mp3原来是我自己
### [“新日本英雄宇宙”举行首次展览活动 5月13日开始](https://www.3dmgame.com/news/202205/3841979.html)
> 概要: 《新·哥斯拉》《新·福音战士剧场版：终》《新·奥特曼》《新·假面骑士》4部作品的梦幻合作项目“新·日本英雄宇宙”首次快闪展览活动将在日本东京举行，活动时间为5月13日至8月1日（每日11:00-20:......
### [动画「我家女友可不止可爱呢」第五话播出感谢绘公开](http://acg.178.com/202205/446065460451.html)
> 概要: 电视动画「我家女友可不止可爱呢」第五话已于5月8日首播，参与原画的STAFF·たなかまこ公开了其绘制的第五话播出感谢绘。电视动画「我家女友可不止可爱呢」改编自真木萤五创作的同名漫画作品，由动画工房负责......
### [赔偿数额过高？知网上诉后被依法驳回](https://finance.sina.com.cn/tech/2022-05-09/doc-imcwipii8840117.shtml)
> 概要: 来源：长江日报......
### [联合国开发计划署二级品牌视觉设计合集1](https://www.zcool.com.cn/work/ZNTk2OTIzMDA=.html)
> 概要: 联合国开发计划署二级品牌视觉设计合集1。其中包括：Asia and Pacific Regional innovation center (亚太区域创新中心)Corporate Performance......
### [TrySail单曲「はなれない距離」完整版MV公开](http://acg.178.com/202205/446069871427.html)
> 概要: 近日，声优组合TrySail公开了单曲「はなれない距離」的完整版MV，该曲同为电视动画「测不准的阿波连同学」的OP主题曲，同名专辑将于6月8日发售。「はなれない距離」完整版MV......
### [大量中国联通用户凌晨被“异常扣费”！法律专家：应公告说明并道歉](https://finance.sina.com.cn/tech/2022-05-09/doc-imcwiwst6404906.shtml)
> 概要: 被各种商家营销套路侵害权益？买到的商品出故障投诉无门？ 黑猫投诉平台全天候帮您解决消费难题【消费遇纠纷，就上黑猫投诉】......
### [最赚钱健身博主：25岁，抖音抢下她](https://www.huxiu.com/article/549927.html)
> 概要: 本文来自微信公众号：投资界（ID：pedaily2012），作者：杨文静，头图来源：帕梅拉B站相册图片就在刘畊宏女孩突破6000万时，健身界最强风向标——帕梅拉，进军抖音。5月2日，帕梅拉正式进驻抖音......
### [组图：网曝张亮儿子天天疑似谈恋爱 网友直呼“佳偶天橙be了”](http://slide.ent.sina.com.cn/star/slide_4_704_369483.html)
> 概要: 组图：网曝张亮儿子天天疑似谈恋爱 网友直呼“佳偶天橙be了”
### [贵圈｜派对舞王张朝阳变物理老师？曾拍半裸杂志照，与高圆圆孙楠登山](https://new.qq.com/rain/a/20220508A055B100)
> 概要: * 版权声明：腾讯新闻出品内容，未经授权，不得复制和转载，否则将追究法律责任很久没听到张朝阳的消息了，直到最近，他因直播上物理课出圈。视频中，58岁的张朝阳留着寸头，通常穿衬衫，面对镜头讲述太阳半径的......
### [知情人士：比亚迪长沙工厂已在停产整顿](https://finance.sina.com.cn/tech/2022-05-09/doc-imcwiwst6425791.shtml)
> 概要: 财联社5月9日讯（记者 徐昊），在4月完成月销十万的比亚迪，又以“排放门”事件登上了“热搜”......
### [《每天和每天不一样》系列第28辑](https://www.zcool.com.cn/work/ZNTk2OTQ1NDA=.html)
> 概要: 努力每天一副偶尔会天窗但是还在坚持的每天系列不经意的画到了520副O(∩_∩)O哈哈~。一转眼就到了夏天，到处都是蔓延的绿色。最近也没有天窗，保持着每日一副的节奏啦。这次的图图里有没有正好适合你的状态......
### [重冈大毅主演东京电视台深夜剧 与入山法子合作](https://ent.sina.com.cn/jp/2022-05-09/doc-imcwiwst6421810.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道，杰尼斯WEST重冈大毅主演《午夜天鹅》内田英治导演执导东京台深夜剧《与雪女同行吃蟹》，入山法子饰演神秘名流之妻，其他参演演员还有胜村政信、久保田纱友和渊上泰史等。　　该剧改......
### [《AI：梦境档案》官方推出在线ARG“隐藏蝙蝠”](https://www.3dmgame.com/news/202205/3841999.html)
> 概要: 更新：目前已有两个谜题出现再推特上，分别由两名失踪人员发布。谜题的答案来自于官方发布的“Bats489”视频中提供的对应线索。视频如下：已经有人制作了对照表格，如下图所示：目前的两条谜题已经被迅速解出......
### [Deepnote (YC S19) is hiring engineers to build a better Jupyter notebook](https://deepnote.com/join-us)
> 概要: Deepnote (YC S19) is hiring engineers to build a better Jupyter notebook
### [云曦大辫子瞩目，火灵儿迫切需要加强，她好可惜，大乱真相是这样](https://new.qq.com/omn/20220509/20220509A06S8L00.html)
> 概要: 在《完美世界》动漫最新的剧情之中，石昊已经离开石村来到了火国，因为他听说这里来了很多域外的英杰，而且他来火国也是要看看火灵儿。总的来说动漫还是按照原著来的，只是这进度未免太快了，而且为了进度还删了一部......
### [《名侦探柯南》女角色人气投票公开 灰原哀人气王](https://acg.gamersky.com/news/202205/1481337.shtml)
> 概要: 之前剧场版《名侦探柯南：万圣节的新娘》推出了上映纪念活动，《名侦探柯南》主要女角色人气投票。现在评选结果揭晓。
### [别被键盘侠混淆了视线：外资不等于外企](https://www.tuicool.com/articles/2muIvyZ)
> 概要: 别被键盘侠混淆了视线：外资不等于外企
### [斗破苍穹年番最新爆料公布，52集全部备案通过，最快8月可以播出](https://new.qq.com/omn/20220509/20220509A06VS000.html)
> 概要: 哈喽，大家好，我是木子。印象中距离《斗破苍穹三年之约》完结已经有一段时间了，不少网友也都纷纷在催促着《斗破苍穹年番》的播出。            那么在这次爆料信息中有哪些重要消息呢？《斗破苍穹年番......
### [So You Wanna Be a Chef – By Bourdain (2010)](https://web.archive.org/web/20210225165109/https://ruhlman.com/so-you-wanna-be-a-chef-by-bourdain-2/)
> 概要: So You Wanna Be a Chef – By Bourdain (2010)
### [一条鱼的选择](https://news.dmzj.com/article/74354.html)
> 概要: 这周要介绍来自菊光為成的《进化异鱼》，本作讲述了高中生晴海和奇怪的少女在海边相遇后的故事。
### [榜一大哥消失在2022](https://www.huxiu.com/article/550104.html)
> 概要: 本文来自微信公众号：剁椒TMT（ID：ylwanjia），作者：西西弗，头图来源：IC photo“大家给榜一大哥点点关注！”在每场直播中，“榜一大哥”是最有排面的称呼。它不仅意味着得到主播更多的尊重......
### [VTuber药子发布有关抱枕的争议言论 炎上后离开事务所](https://news.dmzj.com/article/74355.html)
> 概要: 美国VTuber药子近日在推特上发表了“为什么有印有衣冠不整的10到15岁角色的抱枕在销售？这些儿童色情产品太恶心了，明明卖这些应该是违法的，无法理解为什么要把这些正当化”等言论。
### [合肥获批建设国家级互联网骨干直联点，将显著改善安徽用户上网感知](https://www.ithome.com/0/617/407.htm)
> 概要: IT之家5 月 9 日消息，安徽省通信管理局宣布，4 月 14 日，合肥国家级互联网骨干直联点建设申请获得工业和信息化部批复，这是“十四五”时期安徽省首个国家级信息通信基础设施落地项目。据公告，合肥国......
### [宝马将推出纯电平台 Neue Klasse，全新电动 3 系 2025 年推出](https://www.ithome.com/0/617/412.htm)
> 概要: IT之家5 月 9 日消息，宝马在第一季度财报电话会议上表示，将推出 Neue Klasse 电动车平台，且该平台目前正在开发中。宝马首席执行官 Oliver Zipse 在该公司 5 月 5 日的第......
### [Farewell;Etaoin Shrdlu (1978) video](https://archive.org/details/FarewellEtaoinShrdlu)
> 概要: Farewell;Etaoin Shrdlu (1978) video
### [视频：张亮发长文回应天天谈恋爱 谴责对孩子的网暴行为](https://video.sina.com.cn/p/ent/2022-05-09/detail-imcwipii8901728.d.html)
> 概要: 视频：张亮发长文回应天天谈恋爱 谴责对孩子的网暴行为
### [当新奥特曼与红绿灯结合在一起后：物理加灯，毫无违和感！](https://new.qq.com/omn/20220509/20220509A09DR800.html)
> 概要: 新奥特曼的电影在这个月就会和大家见面，不过国内想要看到这个电影的话应该要等到具体的bd发售之后才有可能了。毕竟以现在目前这种情况，引进新奥特曼电影的可能性基本上为0。值得一提的是这次新奥特曼的设计是没......
### [母亲节再放剧照，《极主夫道》真人版能否重振漫改荣光？](https://new.qq.com/omn/20220509/20220509A09IUY00.html)
> 概要: 文|一天青鱼    图|来源网络众所周知，日本的动漫改编真人电影市场就像一处泥潭。原作漫画越是出彩，观众就会越挑剔也就越容易翻车。而漫改真人电影最容易成功的就要数“漫改喜剧”这一类型，如《银魂》和《我......
### [社区团购，留给拼多多的时间还多吗？](https://www.tuicool.com/articles/nuIRreJ)
> 概要: 社区团购，留给拼多多的时间还多吗？
### [剧场版动画《擅长捉弄人的高木同学》预告片](https://news.dmzj.com/article/74357.html)
> 概要: 根据山本崇一朗原作制作的剧场版动画《擅长捉弄人的高木同学》公开了一段预告片。在这次的视频中，可以看到高木和西片为猫寻找主人的片段。
### [嘴部VR设备官方演示 超声波模拟各种触感](https://www.3dmgame.com/news/202205/3842019.html)
> 概要: 此前，我们报道了科学家开发出了一款全新的VR设备，可以通过超声波在用户的嘴部模拟各种触觉。随后官方发布了一个演示和讲解视频。官方演示视频：在视频中，开发公司 Future Interfaces Gro......
### [吉利手机新公司成立，经营范围含互联网直播服务](https://www.ithome.com/0/617/433.htm)
> 概要: 感谢IT之家网友机智喵的线索投递！IT之家5 月 9 日消息，根据信息显示，湖北星纪时代信息技术有限公司、湖北星纪时代网络技术有限公司成立，注册资本均为一千万元，由王勇担任两家公司法人代表，由吉利手机......
### [Pixel 6a 即将发布，消息称谷歌有望重返印度手机市场](https://www.ithome.com/0/617/434.htm)
> 概要: IT之家5 月 9 日消息，在过去的几个月里，谷歌的新机 Pixel 6a 已经被爆料者们曝光的差不多了，泄露的渲染图和规格描述了对谷歌即将推出的 Pixel 6a 的期待。与去年的 Pixel 5a......
### [快手谨慎布局，线上卖房又迎“新巨头”](https://www.yicai.com/news/101406274.html)
> 概要: 短视频巨头盯上房产交易。
### [国潮寻味 | 炒货里的徽派江湖](https://www.tuicool.com/articles/YbumIby)
> 概要: 国潮寻味 | 炒货里的徽派江湖
### [谁在捏造特斯拉刹车门](https://www.huxiu.com/article/550379.html)
> 概要: 出品 | 虎嗅汽车组作者 | 张博文头图 | IC Photo特斯拉刹车门的事情，又出新进展了。2020 年 8 月 12 日，温州车主陈先生称，在距离停车场100米左右的距离，自己的特斯拉失控加速，......
### [马斯克：若不扭转出生率下降 日本将会“消失”](https://www.3dmgame.com/news/202205/3842024.html)
> 概要: 针对日本人口减少，近日刚刚收购推特的马斯克5月8日评论称，“除非出现变化致出生率超过死亡率，否则日本最终会不复存在。”马斯克的言论很快在日本网络上引发广泛讨论。日本共同社英文网此前报道称，日本总务省4......
### [美国监管部门加码整治海运业“过度收费”，要对滞期费下手？](https://www.yicai.com/news/101406414.html)
> 概要: 拜登在今年的国情咨文中就表示，将治理海运企业向美国企业和消费者过度收费的问题。
### [财报到期叠加疫情反复，IPO中止企业仍超百家](https://www.yicai.com/news/101406433.html)
> 概要: “受疫情影响，无法在规定时限内完成尽职调查、回复审核问询”
### [沪指失守5日线 弱势震荡将延续？](https://www.yicai.com/news/101406319.html)
> 概要: None
### [北京发现个别核酸检测机构报告不准确,专家分析有哪些原因](https://www.yicai.com/news/101406470.html)
> 概要: 北京方面通报称，在近期的飞行监督检查中发现，个别核酸检测机构存在送检不及时、报告不准确、实验室管理不严格等问题。
### [广告素材优选算法在内容营销中的应用实践](https://www.tuicool.com/articles/7F73qqy)
> 概要: 广告素材优选算法在内容营销中的应用实践
### [穆迪报告：个人养老金计划中银行将获益最大](https://www.yicai.com/news/101406504.html)
> 概要: 当个人养老金规模达到10万亿元时，银行业每年可增加约500亿元收入。
### [专家解读：全国统一大市场不是统一计划，不是自我循环的封闭性市场](https://finance.sina.com.cn/jjxw/2022-05-09/doc-imcwipii8928554.shtml)
> 概要: 中共中央、国务院近日发布的《关于加快建设全国统一大市场的意见》，引发各界关注。 5月7日，在中国宏观经济论坛（CMF）主办的“‘政府-市场’职能归位与全国统一大市场构建”...
### [胡春华强调 积极努力稳住外贸外资基本盘](https://finance.sina.com.cn/china/2022-05-09/doc-imcwipii8929222.shtml)
> 概要: 新华社北京5月9日电 全国促进外贸外资平稳发展电视电话会议9日在京举行。中共中央政治局委员、国务院副总理胡春华出席会议并讲话。
### [珠三角跃升10万亿级城市群：5年里“再造”了一个深圳，都市圈牵引发展新裂变](https://finance.sina.com.cn/china/dfjj/2022-05-09/doc-imcwiwst6484319.shtml)
> 概要: 南方财经全媒体见习记者梁施婷 记者郑玮 实习生高靖琳 广州报道“五一”期间，广州地铁7号线顺德段正式开通，再添一条连接广佛两市的轨道交通大动脉。
### [Xbox在线DRM受到批评 玩家连续4天无法启动游戏](https://www.3dmgame.com/news/202205/3842027.html)
> 概要: Xbox的在线DRM政策受到批评，原因是服务器宕机导致一些主机用户4天无法启动所购买的游戏。5月6日，周五，Xbox客服团队首次证实，在用户抱怨无法购买或启动游戏以及启动云游戏后，其服务器出现了严重的......
### [银保监会副主席梁涛：持续加大金融对实体经济支持力度](https://finance.sina.com.cn/china/2022-05-09/doc-imcwipii8930621.shtml)
> 概要: 原标题：稳字当头 干在实处•权威访谈丨持续加大金融对实体经济支持力度 如何发挥金融的力量，应对经济下行压力，助企纾困，稳定宏观经济大盘？
### [如何发挥好货币政策工具的总量和结构双重功能？央行这么说......](https://finance.sina.com.cn/china/2022-05-09/doc-imcwiwst6487140.shtml)
> 概要: 21世纪经济报道记者 边万莉 北京报道 下一阶段货币政策的主要思路是什么？5月9日，央行发布的《2022年第一季度中国货币政策执行报告》（以下简称报告）从总量和结构两个方...
### [信号！“主动应对，提振信心”，央行重磅报告透露货币政策新走向，海外政策收紧效应已显现…来看三大要点](https://finance.sina.com.cn/wm/2022-05-09/doc-imcwipii8932305.shtml)
> 概要: 5月9日，人民银行发布《2022年第一季度中国货币政策执行报告》（下称《报告》），相比于上季度，本季度报告对于下阶段的货币政策安排更加强调纾困困难行业、脆弱群体...
# 小说
### [我是丧尸帝王](http://book.zongheng.com/book/1037612.html)
> 作者：无良策

> 标签：科幻游戏

> 简介：这是一个老色批穿越成丧尸，在末世把妹的沙雕故事，妹子确实不少，但身为一个丧尸却什么都做不了，最后把他活生生逼成了丧尸帝王……群：775655957

> 章节末：第八十八章 大结局

> 状态：完本
# 论文
### [Ray Tracing-Guided Design of Plenoptic Cameras | Papers With Code](https://paperswithcode.com/paper/ray-tracing-guided-design-of-plenoptic)
> 日期：9 Mar 2022

> 标签：None

> 代码：None

> 描述：The design of a plenoptic camera requires the combination of two dissimilar optical systems, namely a main lens and an array of microlenses. And while the construction process of a conventional camera is mainly concerned with focusing the image onto a single plane, in the case of plenoptic cameras there can be additional requirements such as a predefined depth of field or a desired range of disparities in neighboring microlens images. Due to this complexity, the manual creation of multiple plenoptic camera setups is often a time-consuming task. In this work we assume a simulation framework as well as the main lens data given and present a method to calculate the remaining aperture, sensor and microlens array parameters under different sets of constraints. Our ray tracing-based approach is shown to result in models outperforming their pendants generated with the commonly used paraxial approximations in terms of image quality, while still meeting the desired constraints. Both the implementation and evaluation setup including 30 plenoptic camera designs are made publicly available.
### [Does non-linear factorization of financial returns help build better and stabler portfolios? | Papers With Code](https://paperswithcode.com/paper/does-non-linear-factorization-of-financial)
> 日期：6 Apr 2022

> 标签：None

> 代码：None

> 描述：A portfolio allocation method based on linear and non-linear latent constrained conditional factors is presented. The factor loadings are constrained to always be positive in order to obtain long-only portfolios, which is not guaranteed by classical factor analysis or PCA. In addition, the factors are to be uncorrelated among clusters in order to build long-only portfolios. Our approach is based on modern machine learning tools: convex Non-negative Matrix Factorization (NMF) and autoencoder neural networks, designed in a specific manner to enforce the learning of useful hidden data structure such as correlation between the assets' returns. Our technique finds lowly correlated linear and non-linear conditional latent factors which are used to build outperforming global portfolios consisting of cryptocurrencies and traditional assets, similar to hierarchical clustering method. We study the dynamics of the derived non-linear factors in order to forecast tail losses of the portfolios and thus build more stable ones.
