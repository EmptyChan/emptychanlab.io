---
title: 2022-11-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.McKenzieRiverTrail_ZH-CN3786429850_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-11-17 21:55:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.McKenzieRiverTrail_ZH-CN3786429850_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [携程、途家、木鸟……做民宿如何选择合适渠道？](https://www.woshipm.com/operate/5679136.html)
> 概要: 民宿已经成为越来越多旅游者青睐的住宿方式，那么作为民宿运营或者民宿房东，如果想让民宿的入住率有所提升，可以通过哪些线上渠道来进行宣传和布局？本篇文章里，作者针对民宿运营如何选合适渠道的这件事儿进行了解......
### [作为意志和表象的Web3世界](https://www.woshipm.com/it/5679486.html)
> 概要: Web3的出现和发展，给予了那些期待新世界的人们一个真实的希望。尽管其影响力的全貌仍需要我们一点耐心去等待，但是Web3或许能成为这个新时代里关于“希望”的某个代名词。Web3是什么？说得多了, 似乎......
### [看双十一大促成绩，宠物经济穿越周期了吗？](https://www.woshipm.com/marketing/5681539.html)
> 概要: 今年双十一期间，宠物经济是为数不多交出亮眼成绩的赛道。越来越多年轻人愿意把时间和金钱花在宠物上，所以也让我们的宠粮开始卷了起来。作者结合相关数据，分析了宠粮为何会成为新型经济主力，一起来看看吧。这届反......
### [DJI AVATA -大疆新品悬念海报](https://www.zcool.com.cn/work/ZNjI5MjIyNDQ=.html)
> 概要: #商业项目为大疆新品Avata发布前设计的悬念海报，将新品的一些细节特征隐藏到画面中。前前后后修改了很多版 最后呈现的效果 个人还是比较满意。最后的展示大图建议点击预览-最终以上线版为准-Clinet......
### [玩型填空 | 广汽丰田“威能骇客”IP形象设计](https://www.zcool.com.cn/work/ZNjI5MjQxOTY=.html)
> 概要: Collect......
### [SE两名员工因涉嫌内部交易 遭东京检察院逮捕](https://www.3dmgame.com/news/202211/3856306.html)
> 概要: 据TBS报道，东京地方检察院刚刚逮捕了两名在大型游戏公司Square Enix工作员工佐崎泰介（38）与铃木文章（40）。据悉，两位嫌疑人因涉嫌进行股票内部交易违反日本的《金融商品交易法》而被地方检察......
### [《柯南：万圣节的新娘》终极预告 安室透柯南再联手](https://acg.gamersky.com/news/202211/1538637.shtml)
> 概要: 柯南最新剧场版《名侦探柯南：万圣节的新娘》发布终极预告，影片即将于11月18日全国上映。
### [舞台剧《飙速宅男》续篇2023年夏上演](https://news.dmzj.com/article/76233.html)
> 概要: 根据渡边航原作编排的舞台剧《飙速宅男》的续篇将于2023年夏季上演。这次的续篇将沿用今年7月上演的舞台剧《飙速宅男The Cadence！》的演出阵容，西田夏特纳担任导演、剧本与比赛演出方法创作，鯨井康介负责演出，岛村龙乃介、砂川脩弥等人出演。
### [索尼影业进军原创动画 首弹冲方丁名作《再见地球》](https://www.3dmgame.com/news/202211/3856319.html)
> 概要: 由WOWOW、索尼影业以及美国动画平台巨头Crunchyroll三家企业联合策划，进军原创动画制作，首弹作品选定了著名动画剧作家冲方丁的名作《再见地球》，敬请期待。•本次的三巨头联合动画制作策划宗旨是......
### [《索尼克：未知边境》将提供更多免费更新 持续到明年](https://www.3dmgame.com/news/202211/3856321.html)
> 概要: 《索尼克：未知边境》自发售以来，已经收到了包含《怪物猎人》在内的多个免费DLC内容更新，而现在根据开发商世嘉的说法，《索尼克：未知边境》未来将添加更多免费内容。世嘉表示：“世嘉也很高兴地宣布，它计划通......
### [暴雪背刺网易，魔兽“后妈”难寻](https://www.huxiu.com/article/716466.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国一场事先张扬的“分手”终于尘埃落定，犹如这个凛冬泼向网易的一盆“冷水”。今天（ 11 月 17 日）上午，动视暴雪的正式“分手声明”赶在网易财报前发出——......
### [皮特·戴维森与艾米丽恋情曝光 二人被拍甜蜜拥抱](https://ent.sina.com.cn/s/u/2022-11-17/doc-imqmmthc4948336.shtml)
> 概要: 新浪娱乐讯 近日，有媒体拍到皮特·戴维森和超模艾米丽·拉塔科夫斯基甜蜜拥抱，证实二人恋情，此前多家媒体报道二人正在交往。　　据悉，两人都在今年夏季恢复单身，艾米丽·拉塔科夫斯基和丈夫 Sebastia......
### [著名企业家何帮喜爱心助学，情暖清北学子求学之路](http://www.investorscn.com/2022/11/17/104332/)
> 概要: 近日，随着我校2022届6名毕业生致民银国际控股集团有限公司何帮喜董事长的一封“感谢信”被公开，无为籍著名企业家何帮喜先生巨资奖掖后学的消息在广大师生中引发热烈反响......
### [“清华妈妈语录”是许多人每日必饮毒鸡汤](https://www.huxiu.com/article/716773.html)
> 概要: 本文来自微信公众号：那些原本是废话的常识 （ID：feihuayuchangshi），作者：叶克飞，编辑：二蛋，题图来自：视觉中国近来，印有“清华妈妈语录”的相框摆件、墙纸在电商平台销量不菲。一些买家......
### [SE员工内幕交易 因涉嫌违反日本金融商品交易法被逮捕](https://news.dmzj.com/article/76240.html)
> 概要: 日本东京地方检察院特搜部逮捕了两名涉嫌违反日本金融商品交易法的男性。
### [《柯南》漫画作者论“无聊”漫画 难读和无聊没两样](https://acg.gamersky.com/news/202211/1538776.shtml)
> 概要: 大家曾经看过哪些让你觉得“无聊”的漫画呢？以《名侦探柯南》闻名的日本漫画家青山刚昌，最近就因为一句评论“无聊”漫画的发言，成为日本网络上的焦点。
### [2022年FIFA世界杯官方授权商品“胜利荣耀、幸运金球”于鸟巢盛大发布](http://www.investorscn.com/2022/11/17/104338/)
> 概要: 2022年FIFA世界杯官方授权商品“胜利荣耀、幸运金球”于鸟巢盛大发布
### [鲁大师Pro：可延展性及多场景应用，望打造新增长极](http://www.investorscn.com/2022/11/17/104340/)
> 概要: 11月16日，鲁大师（03601.HK）发布公告，其首款SaaS产品「鲁大师Pro软件」已被应用在超10个行业领域，并成功实现为大型企业赋能......
### [粤港澳大湾区互联互通，香港优质券商蓄势待发](http://www.investorscn.com/2022/11/17/104341/)
> 概要: 据《北京商报》报导,中国香港与内地金融市场的互惠合作将会更进一步,香港财经代表在会上谈论未来金融发展时表示,会争取于明年第二季度为香港上市发行人增设人民币股票交易柜台,实践粤港澳大湾区互联互通。同时香......
### [【Mr.Woohoo】品牌全案案例分享](https://www.zcool.com.cn/work/ZNjI5MzEyMDQ=.html)
> 概要: Collect......
### [WAVE《碧蓝航线》火奴鲁鲁1/7比例手办开订](https://news.dmzj.com/article/76241.html)
> 概要: WAVE根据《碧蓝航线》中的火奴鲁鲁制作的1/7比例手办正在预订中。本作再现了“两人的夏日祭”中的额服装，配音笑颜和害羞颜两种表情。
### [网易和暴雪娱乐分手在即，双方代价几何？](https://www.yicai.com/news/101597749.html)
> 概要: 游戏行业相关人士透露，“续约失败可能是暴雪在谈续约时，要求更高的分成比例，网易为难。”暴雪的产品依然有大量铁粉，这是暴雪的底牌，但铁粉也在变老和流失，这个底气其实是不足的。
### [【传声筒】克莱近10场持续低迷，勇士应该让担任替补角色吗？](https://bbs.hupu.com/56493395.html)
> 概要: 克莱·汤普森过去10场比赛的投篮数据:17投6中、三分球9投3中16投6中，三分球13投5中13投3中，三分球7投2中18投6中，三分10投3中24投10中，三分球15投7中19投7中，三分球12投4
### [双11手机销售数据整体下滑35% 苹果销量营收第一](https://www.3dmgame.com/news/202211/3856350.html)
> 概要: 今年的双11已经过去了，似乎没有往年的火热，不少手机厂商都宣布自己拿到了多个平台的第一，创造了新的记录，然而他们并没有公布手机具体的销量数据，现在调研机构Strategy Analytics给出结果了......
### [2亿人的血压在三天内经历了两次大反转](https://www.huxiu.com/article/716989.html)
> 概要: 出品 | 虎嗅医疗组作者 | 陈广晶编辑 | 陈伊凡头图 | 视觉中国你高血压吗？2亿多人的答案在三天之内经历了两次大反转。11月13日，国家血管病中心、中国医师协会等多家学术机构联合发布了他们共同制......
### [重磅：Light Mark发布全球培育钻石标杆性产品倍璨](http://www.investorscn.com/2022/11/17/104345/)
> 概要: 11月16日，以“重塑闪耀，加倍璀璨”为主题的Light Mark倍璨钻石上市发布会在上海Light Mark小白光宇航中心圆满召开......
### [视频：新恋情？秦牛正威回京后与男子挽手被拍 两人一同回某小区](https://video.sina.com.cn/p/ent/2022-11-17/detail-imqqsmrp6563392.d.html)
> 概要: 视频：新恋情？秦牛正威回京后与男子挽手被拍 两人一同回某小区
### [三星 Galaxy A52 海外推送安卓 13 / One UI 5.0 正式版](https://www.ithome.com/0/654/644.htm)
> 概要: IT之家11 月 17 日消息，在向 Galaxy A33 5G、Galaxy A53 5G、Galaxy A73 5G、Galaxy M32 5G 和 Galaxy M52 5G 推出安卓 13 正......
### [国内首个治疗耳聋的基因疗法临床试验启动，预计下月完成首例患者入组](https://www.yicai.com/news/101597928.html)
> 概要: 对于明确病因的遗传性耳聋，基因治疗被视为是理想的治疗方式，即以正常的基因弥补缺陷的基因，一次给药从根本上恢复或改善听力，实现听觉功能的持久性恢复，且更加接近自然的声音。
### [TV动画《冰剑的魔术师将会统一世界》新PV](https://news.dmzj.com/article/76244.html)
> 概要: TV动画《冰剑的魔术师将会统一世界》公开了一段新PV。在这次的PV中，使用了内田真礼演唱的ED主题曲的片段。
### [《一拳超人》重制版219话:吹雪华丽登场 英雄的日常](https://acg.gamersky.com/news/202211/1538930.shtml)
> 概要: 《一拳超人》重制版219话公开，这一话的内容不多，主要市英雄们的日常生活。吹雪再次盛装登场，她打算要去见一个人，并且说很可能会发生打斗。
### [视频：陆毅鲍蕾结婚16年仍甜蜜！一家四口逛商场 14岁贝儿好高挑](https://video.sina.com.cn/p/ent/2022-11-17/detail-imqmmthc4982903.d.html)
> 概要: 视频：陆毅鲍蕾结婚16年仍甜蜜！一家四口逛商场 14岁贝儿好高挑
### [如何防止“一封了之”“一放了之”？ 国务院联防联控机制回应来了](https://www.yicai.com/news/101597931.html)
> 概要: 目前，相关部门正在制定加快推进新冠病毒疫苗接种的方案。
### [上海启动“满格上海”行动计划，2023 年底全市 5G 网络覆盖率超过 90%](https://www.ithome.com/0/654/664.htm)
> 概要: IT之家11 月 17 日消息，上海市通信管理局日前印发《5G 网络能级提升“满格上海”行动计划》（以下简称《满格上海计划》）。《满格上海计划》提出，到 2023 年底，实现 5G 基站规模超过 7......
### [《决胜荒野 3》11 月 24 日B站首播，“德爷” 与顶尖求生专家共赴荒野](https://www.ithome.com/0/654/668.htm)
> 概要: IT之家11 月 17 日消息，据B站官方消息，由B站与 Discovery 联合出品的《决胜荒野 3》将在 11 月 24 日起，每周四、周五 18:00 B站全球首播。据介绍，该纪录片由 EdSt......
### [短期能否继续重点做多信创和医药板块？](https://www.yicai.com/video/101598058.html)
> 概要: 短期能否继续重点做多信创和医药板块？
### [净利下降72%！什么拖了英伟达三季报后腿？](https://www.huxiu.com/article/717174.html)
> 概要: 出品|虎嗅科技组作者|陈伊凡头图|视觉中国在硅谷科技巨头笼罩在业绩下行的寒冬下时，芯片巨头英伟达（NASDAQ:NVDA)）发布了营收三季度财报。北京时间11月17日，英伟达发布截至10月31日的20......
### [无锡在京举办文旅招商推介会，两地头部旅游企业签订客源互送协议](https://finance.sina.com.cn/jjxw/2022-11-17/doc-imqqsmrp6582440.shtml)
> 概要: 新京报贝壳财经讯（记者王真真）乘着旅游利好消息不断的东风，11月17日，无锡市文化广电和旅游局在北京举办了以“乐游无锡 梦华江南”为主题的无锡文化旅游招商推介会。
### [调查丨南宁买房可免费坐10年地铁背后，市场去库存压力仍大](https://finance.sina.com.cn/china/gncj/2022-11-17/doc-imqmmthc4998803.shtml)
> 概要: “买房可以免费乘坐10年地铁。”近日，广西南宁某楼盘推出的福利活动引起网络热议。 红星资本局注意到，这一活动由广西南宁轨道地产集团有限责任公司（以下简称：南宁轨道地...
### [粮食丰、能源稳——今年以来我国强化粮食安全保障、提升能源保供能力观察](https://finance.sina.com.cn/china/2022-11-17/doc-imqqsmrp6584485.shtml)
> 概要: 新华社北京11月17日电题：粮食丰、能源稳——今年以来我国强化粮食安全保障、提升能源保供能力观察 新华社记者戴小河、于文静 国家统计局最新数据显示...
### [移远通信基于联发科 T830 芯片发布全新 5G R16 模组 RG620T：8 天线 + Wi-Fi 7 双道并发](https://www.ithome.com/0/654/693.htm)
> 概要: IT之家11 月 17 日消息，物联网整体解决方案供应商移远通信推出基于联发科 MediaTek T830 平台的全新 5G R16 模组 RG620T。RG620T 提供针对北美市场的 RG620T......
### [流言板罗伯逊：萨拉赫是克洛普执教时期，俱乐部的最佳引援](https://bbs.hupu.com/56496691.html)
> 概要: 虎扑11月17日讯 利物浦后卫罗伯逊在接受采访时表示，萨拉赫是克洛普执教时期，利物浦签下的最伟大的引援。“说实在话，我们队内有许多强者，俱乐部签下过不少顶级球星，比如范戴克，还有阿利松，但最佳引援肯定
### [央行反洗钱局王静：年底将实现对全国1500家法人机构的反洗钱检查全覆盖](https://finance.sina.com.cn/jjxw/2022-11-17/doc-imqqsmrp6585221.shtml)
> 概要: 金融安全是国家安全的重要组成部分，也是国家的重要竞争力。11月17日，2022第二届陆家嘴国家金融安全峰会暨第十二届中国反洗钱高峰论坛在上海召开...
### [温彬：10月财政收入和支出增速双双走高](https://finance.sina.com.cn/roll/2022-11-17/doc-imqmmthc5002958.shtml)
> 概要: 作者：温彬 ▪ 孙莹 明年财政政策将保持对经济恢复必要的支持力度，并着力增强有效性和可持续性。 10月份，全国一般公共预算收入同比增长15.7%，增速较上月加快7...
### [暴雪“分手”，网易“巨震”丨一只热股](https://www.yicai.com/video/101598124.html)
> 概要: 暴雪“分手”，网易“巨震”丨一只热股
### [流言板德容：踢世界杯是球员的最大成就；有范加尔在我们充满信心](https://bbs.hupu.com/56497026.html)
> 概要: 虎扑11月17日讯 ​卡塔尔世界杯的比赛将在下周正式打响。而在接受FIFA官网的采访时，巴萨中场，荷兰国脚德容谈到了自己参加世界杯的感受。此外，他也谈到了荷兰队在本届世界杯的目标。从个人角度来看，你参
### [天赋满满！北京队球员曾凡博晒扣篮视频](https://bbs.hupu.com/56497044.html)
> 概要: 北京队球员曾凡博更新抖音，晒自己换手扣篮视频。CBA新赛季至今，曾凡博代表北京出战3场，场均出场18.3分钟，能得到11.7分4篮板。本赛季常规赛第一阶段，北京队以5胜4负排名第七。 来源：  虎扑
### [报告称黄牛机器人对PS5渐渐失去兴趣](https://www.3dmgame.com/news/202211/3856371.html)
> 概要: 一名机器人探测专家暗示，黄牛机器人正渐渐对PS5失去兴趣。Netacea发布了一篇关于被黄牛机器人盯上的五大产品最新季度指数，这一期报道的2022年第三季度。报告指出PS5不再出现在榜单的前三名，这在......
# 小说
### [最强妖孽](http://book.zongheng.com/book/524571.html)
> 作者：厄夜怪客

> 标签：都市娱乐

> 简介：新书：我要做阎罗，已经发布！！

> 章节末：新书《我要做阎罗》已经发布

> 状态：完本
# 论文
### [Privacy-Preserving Face Recognition with Learnable Privacy Budgets in Frequency Domain | Papers With Code](https://paperswithcode.com/paper/privacy-preserving-face-recognition-with)
> 日期：15 Jul 2022

> 标签：None

> 代码：https://github.com/Tencent/TFace

> 描述：Face recognition technology has been used in many fields due to its high recognition accuracy, including the face unlocking of mobile devices, community access control systems, and city surveillance. As the current high accuracy is guaranteed by very deep network structures, facial images often need to be transmitted to third-party servers with high computational power for inference. However, facial images visually reveal the user's identity information. In this process, both untrusted service providers and malicious users can significantly increase the risk of a personal privacy breach. Current privacy-preserving approaches to face recognition are often accompanied by many side effects, such as a significant increase in inference time or a noticeable decrease in recognition accuracy. This paper proposes a privacy-preserving face recognition method using differential privacy in the frequency domain. Due to the utilization of differential privacy, it offers a guarantee of privacy in theory. Meanwhile, the loss of accuracy is very slight. This method first converts the original image to the frequency domain and removes the direct component termed DC. Then a privacy budget allocation method can be learned based on the loss of the back-end face recognition network within the differential privacy framework. Finally, it adds the corresponding noise to the frequency domain features. Our method performs very well with several classical face recognition test sets according to the extensive experiments.
### [Reinforcement Learning-based Placement of Charging Stations in Urban Road Networks | Papers With Code](https://paperswithcode.com/paper/reinforcement-learning-based-placement-of)
> 概要: The transition from conventional mobility to electromobility largely depends on charging infrastructure availability and optimal placement.This paper examines the optimal placement of charging stations in urban areas. We maximise the charging infrastructure supply over the area and minimise waiting, travel, and charging times while setting budget constraints. Moreover, we include the possibility of charging vehicles at home to obtain a more refined estimation of the actual charging demand throughout the urban area. We formulate the Placement of Charging Stations problem as a non-linear integer optimisation problem that seeks the optimal positions for charging stations and the optimal number of charging piles of different charging types. We design a novel Deep Reinforcement Learning approach to solve the charging station placement problem (PCRL). Extensive experiments on real-world datasets show how the PCRL reduces the waiting and travel time while increasing the benefit of the charging plan compared to five baselines. Compared to the existing infrastructure, we can reduce the waiting time by up to 97% and increase the benefit up to 497%.
