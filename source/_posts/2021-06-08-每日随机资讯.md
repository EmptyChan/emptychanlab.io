---
title: 2021-06-08-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CortezJacks_EN-CN8905911415_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp&w=1920&h=1080&rs=1&c=4
date: 2021-06-08 21:15:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CortezJacks_EN-CN8905911415_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp&w=1920&h=1080&rs=1&c=4)
# 新闻
### [WWDC2021 亮点：iPad 支持编译 APP、Xcode Cloud、Universal Control](https://www.oschina.net/news/145080/wwdc-2021)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>在刚刚结束的 WWDC2021 上，苹果宣布了 iOS 15、iPadOS 15 以及 macOS Monterey。具体的更新内容这里......
### [Qt 6.2 功能冻结，更多 Qt5 模块移植到 Qt6](https://www.oschina.net/news/145079/qt-6-2-features-freeze)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>根据邮件列表显示，Qt 6.2 已经进入功能冻结状态。在去年年底Qt 6.0 发布后，Qt表示将压缩今年的发布周期，因此在Qt 6.1刚......
### [DevUI —— 华为云开源的企业级 UI 组件库](https://www.oschina.net/news/145082/devui-on-gotc)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>DevUI 是华为云开源的企业级 UI 组件库，它从华为云 DevCloud 研发工具体系孵化而来。其官网的介绍写道：“DevUI De......
### [EdgeX Foundry 成立四周年！](https://www.oschina.net/news/145167/edgex-foundry-four-anniversary)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>作者：Keith Steele，IOTech 首席执行官，LF Edge Board 和 EdgeX 技术指导小组的成员。文章简介：短短......
### [开发者点评一切，快来分享你的技术「好物」！思否 · 开发者点评等你来玩](https://segmentfault.com/a/1190000040140927?utm_source=sf-homepage)
> 概要: 选美食好店有大众点评，下载 APP 有 App Store，选购高性价比好物有什么值得买，那挑选好用的开发者工具呢？别慌！SegmentFault 思否最新产品——思否 · 开发者点评来帮忙。什么是开......
### [WWDC 2021 ：大改动基本没有，但细节感人](https://segmentfault.com/a/1190000040141000?utm_source=sf-homepage)
> 概要: 6 月 8 日凌晨 1 点，WWDC 2021 线上正式开场，话不多说，我们直接来看看这场大会公布了哪些信息？ios 15Facetime 支持空间音频、视频人像模式Facetime 这次的改动算是很......
### [FaceTime on the web requires Chrome or Edge, no mention of Firefox](https://www.apple.com/macos/monterey-preview/features/#footnote-3)
> 概要: FaceTime on the web requires Chrome or Edge, no mention of Firefox
### [淘系自研前端研发工具 AppWorks 正式发布](https://segmentfault.com/a/1190000040142501?utm_source=sf-homepage)
> 概要: 经过了一年的迭代， 近 2 个月集中开发， AppWorks 在近日正式发布。AppWorks地址：https://appworks.site/AppWorks 是社区受到开发者广泛关注的 VS Co......
### [组图：王鸥镂空连衣裙配珍珠耳环优雅贵气 光脚坐窗边美腿抢镜](http://slide.ent.sina.com.cn/tv/slide_4_704_357673.html)
> 概要: 组图：王鸥镂空连衣裙配珍珠耳环优雅贵气 光脚坐窗边美腿抢镜
### [60年中日美数据对比：文科，会拖累国家经济增速吗？](https://www.huxiu.com/article/433427.html)
> 概要: 新财富追溯近60年的数据发现，美国、日本的文理科生比值整体上均呈现下降趋势，但国家经济发展政策调整、产业优化升级，并未对文理科生比值产生绝对影响，文科生多少与经济增速并无直接关系。陷入“中等收入陷阱”......
### [剧场版动画「BanG Dream! Episode of Roselia」主题曲集CM公布](http://acg.178.com/202106/417118669794.html)
> 概要: 剧场版动画「BanG Dream! Episode of Roselia」公布了主题曲集的CM影像，该商品将于6月30日正式发售。「BanG Dream! Episode of Roselia」主题曲......
### [Microids在法国里昂设工作室 专注于冒险游戏开发](https://www.3dmgame.com/news/202106/3816154.html)
> 概要: Microids今天（6月8日）宣布在法国里昂开设了一家名为Microids Studio Lyon新游戏工作室。Microids宣布该工作室将专注于新的冒险游戏。在揭牌仪式之后，公司任命行业资深人士......
### [Australian Federal Police and FBI nab underworld figures using encrypted app](https://www.abc.net.au/news/2021-06-08/fbi-afp-underworld-crime-bust-an0m-cash-drugs-murder/100197246)
> 概要: Australian Federal Police and FBI nab underworld figures using encrypted app
### [USB-C接口能一统快充江湖吗？](https://www.huxiu.com/article/433445.html)
> 概要: 本文来自微信公众号：爱范儿（ID：ifanr），作者：杜沅傧，原文标题：《USB-C 接口支持 240W 供电后，它会一统快充江湖吗？》，题图来自：unsplash以目前来看，在所有消费电子产品的接口......
### [「无限滑板」新周边商品图公开](http://acg.178.com/202106/417128611944.html)
> 概要: 动画「无限滑板」于近日公开了最新周边宣传图和商品图，本次公开的周边为历和兰加戒指吊坠，预计7月26日公开发售......
### [洪卓立片场晕倒昏迷 霍汶希回应称其正在休息中](https://ent.sina.com.cn/v/h/2021-06-08/doc-ikqciyzi8418781.shtml)
> 概要: 新浪娱乐讯 近日，洪卓立在拍戏时突然抽搐晕倒昏迷，从高处摔下，随即被送医急救。经纪人霍汶希回应表示现在他已回香港，在家里休息中，也有再去医院全面检查一次，检查的报告有待回复。　　霍汶希称，洪卓立头部检......
### [鼓点震乐谷 IP接长龙，2021上海音乐谷动漫游园会成功举办](https://new.qq.com/omn/ACF20210/ACF2021060801620900.html)
> 概要: 6月5日下午14点，“2021上海音乐谷动漫游园会”在国家音乐产业基地、国家AAA级旅游景区和本市重点文化创意产业聚集区——上海音乐谷正式开启。十多个国内外动漫游戏IP，在音乐谷的4条主要街区向动漫爱......
### [高野一深新作「Gene Bride（基因新娘）」新图公开](http://acg.178.com/202106/417130596040.html)
> 概要: 今日（6月8日），「我的少年」作者高野一深新作「Gene Bride」（基因新娘）开启新连载。官方公布了杂志图、作者贺图和漫画新插图。杂志图：作者贺图：漫画新插图：......
### [TV动画「寒蝉鸣泣之时 卒」开播日期公开](http://acg.178.com/202106/417130719325.html)
> 概要: TV动画「寒蝉鸣泣之时 卒」将于2021年7月1日开播，初次第1集、第2集连播。CAST：前原圭一：保志总一朗龙宫礼奈：中原麻衣园崎魅音：雪野五月园崎诗音：雪野五月北条沙都子：金井美香古手梨花：田村由......
### [组图：《萌探探探案》Angelababy还原青蛇造型精致美艳](http://slide.ent.sina.com.cn/z/v/slide_4_704_357706.html)
> 概要: 组图：《萌探探探案》Angelababy还原青蛇造型精致美艳
### [万代《超级机器人大战》NS系促销 最大优惠41%](https://www.3dmgame.com/news/202106/3816181.html)
> 概要: 万代南梦宫日前宣布，旗下经典战棋游戏《超级机器人大战》NS系3部游戏促销活动于6月7日～6月20日期间开启，最大优惠41%，一起来了解下。·《超级机器人大战》NS系促销：官方活动页。·本次参与促销活动......
### [韩联社：孙兴慜纪念章将在下月面世](https://bbs.hupu.com/43463561.html)
> 概要: 据韩联社报道，为表彰孙兴慜在世界足坛的优异成绩以及提升韩国在国际足坛的影响力，特发售其纪念章。纪念章分硬币型和纸币型两款，在韩国顶级货币设计师的设计下，以孙兴慜球场上动感表现形象为元素巧妙布局。并应孙
### [高考试题泄露，5G 该不该背锅](https://www.ithome.com/0/556/224.htm)
> 概要: 大家好，我是小枣君。一年一度的高考，昨天又开始了。这几天，网上到处都是关于高考的新闻。和以往一样，有忘记带准考证的，有跑错考场的，有警察叔叔送考的，大家应该见怪不怪了。但是有一条新闻，成功地吸引了小枣......
### [比特币成法定货币，这个国家的赛博朋克梦能实现吗？](https://www.tuicool.com/articles/veaAzq2)
> 概要: 在赛博朋克谜的理想世界里，最适合做法定货币的是什么？比特币或许会高票当选。近日，这个赛博朋克梦距离现实前所未有的近。6 月 6 日，萨尔瓦多总统 Nayib Bukele 在迈阿密比特币 2021 大......
### [乘联会：5 月新能源乘用车零售销量达 18.5 万辆，五菱、比亚迪、特斯拉分列前三](https://www.ithome.com/0/556/233.htm)
> 概要: IT之家6 月 8 日消息 乘联会今天发布 2021 年 5 月份全国乘用车市场分析，2021 年 5 月乘用车市场零售达到 162.3 万辆，同比 2020 年 5 月增长 1.0%，而且相较 20......
### [制片人谈白敬亭马思纯cp感 赞两人演活米佧邢克垒](https://ent.sina.com.cn/v/m/2021-06-08/doc-ikqcfnaz9863891.shtml)
> 概要: 新浪娱乐讯 6月7日，新浪娱乐“新浪潮论坛：尊重原著 尊重观众——IP剧打破流量定律的出圈之道”在沪举行。制片人邓晓华谈到该剧改编，透露16年就买了小说，19年才开机，剧本做了3年，经历了2次，男主的......
### [马斯克对压铸机的执着，在遥远的中国掀起一场行业风暴](https://www.huxiu.com/article/433591.html)
> 概要: 来源｜远川科技评论（ID：kechuangych）作者：刘芮头图｜视觉中国2021年2月2日，在位于Space X在德克萨斯发射火箭的基地附近，马斯克与著名汽车拆解测评专家桑迪·门罗（Sandy Mu......
### [​少即是多，懂得“简单化”方能俘获客户的心](https://www.tuicool.com/articles/rqIfyqy)
> 概要: 编者按：本文来自微信公众号“哈佛商业评论”（ID:hbrchinese），36氪经授权发布。现代消费者每天面临数以百计——如果不是数以千计的话——的选择。阅读何书，何处购物，购买何物。这些决定中的每一......
### [直播间狂卖1.5亿，谁在撑起千亿珠宝市场？](https://www.tuicool.com/articles/3uIRve3)
> 概要: 图片来源@视觉中国文 | 创业最前线，作者 | 付艳翠，编辑 | 冯羽这届年轻人出门的标准动作，已经从对照镜子看衣服搭配是否合理，变成对着镜子时不时调整项链、耳环，再伸出手打量一下手链和戒指的搭配是否......
### [有没有一本“男人圣经”能让人变成硬汉？](https://www.huxiu.com/article/433592.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。因为每天睡觉的时候都会夹着毛绒玩具，我的......
### [武汉考生拍照上传小猿搜题，在线教育再遇教育公平性争论](https://www.ithome.com/0/556/244.htm)
> 概要: 今日，武汉市黄陂区教育局官方微博 @黄陂教育 发布通报，针对网传考生吴某某将高考数学题拍照上传小猿搜题一事，对违规考生作出给予取消此次考试资格，其所报名参加考试的各阶段、各科成绩无效的处理，并根据后续......
### [Fastly Outage](https://fastly.com/)
> 概要: Fastly Outage
### [《欢乐颂3》前脚官宣，蒋欣新剧后脚跟住，鹅厂准备好收割收视了](https://new.qq.com/omn/20210608/20210608A0A04K00.html)
> 概要: 说起《欢乐颂》相信大家也是再熟悉不过了，由正午阳光出品的这部剧作，前两部都收获了不错的热度和收视成绩。而这一次《欢乐颂3》的官宣还是引来了不少关注。            侯亮平担任总制片人，简川訸担......
### [年轻人多在「反算法」，没想到 Spotify 先站出来了](https://www.ithome.com/0/556/247.htm)
> 概要: 作为煎饼果子圣地，天津人评价最高的煎饼店，一定不是人们趋之若鹜的网红店，而是「我家楼下那家」。人们对于音乐的喜好，和天津人对煎饼果子的爱有异曲同工之处，好友热情分享过来的音乐，通常会被你归为「垃圾」......
### [韩媒曝BIGBANG将完整回归 成员已与YG完成续约](https://ent.sina.com.cn/y/yrihan/2021-06-08/doc-ikqciyzi8480590.shtml)
> 概要: 新浪娱乐讯  8日，据韩媒报道，不仅太阳正在准备BIGBANG完全体回归中，GD权志龙、 大声、TOP都已完成和YG的续约，也正在准备BIGBANG的回归。 　　今年4月份，权志龙在采访中问及近况，他......
### [汪小菲，里外不是人](https://new.qq.com/omn/20210608/20210608A0AUI300.html)
> 概要: 大S（徐熙媛）和汪小菲的10年情断，其中缘由在网上展开了激烈的讨论，有人说是汪小菲和大S两个人的性格不合，有人说是两人分居太久，更有人说是大S移情别恋。其实，不论哪种情况，汪小菲把自己搞得太狼狈，里外......
### [韩美娟直播痛哭惹争议，从网红走到娱乐圈，他是怎么一步步翻车的？](https://new.qq.com/omn/20210608/20210608A0AYSQ00.html)
> 概要: 近日，知名网红、创造营4参赛选手韩美娟（韩佩泉）在担任“拾光盛典”主持人后，频频在网络上引发热议。其中，尤以肖战粉丝对韩美娟担任主持人一事意见很大，毕竟早前韩美娟就曾有过抵制肖战的言论，此次又与肖战参......
### [AutoML框架概览](https://www.tuicool.com/articles/jyUzQjR)
> 概要: 今天来闲聊一下之前看过的一些 autoML 框架。对于 autoML 偏理论部分的阐述，可以参考我之前的专栏文章：字节：走马观花AutoML​zhuanlan.zhihu.comAutoGluon第一......
### [《我是大神仙》完结追番专场！斗鱼仙女主播陪你看动画！](https://new.qq.com/omn/ACF20210/ACF2021060801943500.html)
> 概要: 穿越仙界的“界面通道”即将关闭，但仙界传奇永不完结！跟着智慧担当，重情又重义的时江修仙成长，大闹仙界；在善良端庄周紫雨和古灵精怪苏月儿之间摇摆纠结；与主角们共同经历了仙考、泰岳门修炼、保护仙药谷、铸灵......
### [Huobi交割和期权合约暂停生成新周期合约](https://www.btc126.com//view/170292.html)
> 概要: 据 Huobi 官方消息，由于近期市场波动较大，为了保护投资者权益，提高其他周期合约盘口流动性和深度，降低合约交易可能带来的风险，Huobi Futures 币本位交割合约将暂停生成新的次季度合约，期......
### [SubGame正式上线MDEX并开通交易](https://www.btc126.com//view/170294.html)
> 概要: 据官方消息，波卡生态平行链SubGame（SGB）已于近期注入流动性，现已正式上线MDEX交易所并开通交易，用户可通过MDEX兑换SGB。SubGame是面向全球众多开发者参与波卡生态建设的游戏和支付......
### [剑桥科学家：石墨烯将让硬盘数据存储量提高10倍](https://www.3dmgame.com/news/202106/3816204.html)
> 概要: 剑桥大学的科学家与来自埃克塞特大学、印度、瑞士、新加坡和美国的团队合作，发表了一项关于使用石墨烯将HDD容量提高十倍的研究报告。石墨烯在HDD中的特殊应用是针对碳基涂层（COC），也就是用于保护盘片免......
### [Chainalysis：去年美国交易员通过BTC交易获得41亿美元利润](https://www.btc126.com//view/170299.html)
> 概要: 据彭博社消息，比特币的迅速崛起造就了大量的加密货币百万富翁，而美国投资者正从中攫取最大份额的财富。根据区块链分析公司Chainalysis估计，美国交易员去年通过比特币交易获得了41亿美元的利润，是排......
### [Changes to Docker Hub Autobuilds](https://www.docker.com/blog/changes-to-docker-hub-autobuilds/)
> 概要: Changes to Docker Hub Autobuilds
### [上海发布重磅规划：看齐国际自由贸易港 重点发展这一重要业务](https://finance.sina.com.cn/jjxw/2021-06-08/doc-ikqcfnaz9894037.shtml)
> 概要: 原标题：看齐国际自由贸易港，上海发布重磅规划，重点发展这一重要业务 6月8日，上海自贸区临港新片区管委会举行“离岸贸易高质量发展推进会”。
### [【咪咕圆桌】19年过去了，你还记得当年停课看世界杯的场景么？](https://bbs.hupu.com/43468022.html)
> 概要: 【咪咕圆桌】19年过去了，你还记得当年停课看世界杯的场景么？
### [视频|“一路象北”火出中国 多国媒体报道：“中国野象群正成为国际明星”](https://finance.sina.com.cn/china/2021-06-08/doc-ikqcfnaz9894083.shtml)
> 概要: 原标题：【视频】“一路象北”火出中国，多国媒体报道：“中国野象群正成为国际明星” 【环球网报道】 正在云南迁徙的野象群不光在国内出名了，还成了“国际明星”...
### [上海闵行区主城区南部板块规划：严格控制新增住宅总量](https://finance.sina.com.cn/jjxw/2021-06-08/doc-ikqcfnaz9893951.shtml)
> 概要: 原标题：上海闵行区主城区南部板块规划：严格控制新增住宅总量 6月8日，上海市规划和自然资源局发布《关于上海市闵行主城区南部板块单元规划（含重点公共基础设施专项规划...
### [【直播】西卡：EDG这个队伍有个优点能忍，有时候能忍到下一把](https://bbs.hupu.com/43468162.html)
> 概要: 【直播】西卡：EDG这个队伍有个优点能忍，有时候能忍到下一把
### [日本美女Coser桃兎もも完美还原jojo立 身材太棒](https://www.3dmgame.com/bagua/4624.html)
> 概要: 不管是看过或没看过《JOJO的奇妙冒险》的人，应该听说过一些jojo梗，其中包括人类无法完美复制的“jojo立”。不过最近日本Coser桃兎もも完美还原了好多种jojo立。桃兎もも的身材比例很好，不仅......
### [3DM速报： CDPR投资者希望CEO辞职](https://www.3dmgame.com/news/202106/3816206.html)
> 概要: 欢迎来到今日的三大妈速报三分钟带你了解游戏业最新资讯大家好，我是米瑟《彩虹六号：异种》确认E3公开首部实机演示；CD Projekt投资者希望CEO辞职1、《彩虹六号：异种》E3放实机，“波斯王子”又......
### [视频|大妈嫌让座慢辱骂乘客事件后续来了 警方：行拘](https://finance.sina.com.cn/china/dfjj/2021-06-08/doc-ikqciyzi8494746.shtml)
> 概要: 原标题：警探号丨大妈嫌让座慢辱骂乘客事件后续来了 警方：行拘 几天前，一女子在乘坐公交车时大骂其他乘客的场景被网友拍下并传播。
### [广州全市密闭娱乐场所暂停营业，包括影院、剧院、KTV等](https://finance.sina.com.cn/china/dfjj/2021-06-08/doc-ikqcfnaz9896620.shtml)
> 概要: 原标题：广州全市密闭娱乐场所暂停营业，包括影院、剧院、KTV等 来源：广州市文化广电旅游局 广州市文化广电旅游局6月7日晚发布关于进一步加强文化广电旅游业疫情防控工作...
### [人才储备爆棚？继国家队之后山东又成U20国脚大户](https://bbs.hupu.com/43468340.html)
> 概要: 今天，中国足协公布了U20国足的第三期集训名单，山东泰山共有8名适龄球员入选，加上之前已经有7名国脚随国家队参加世界杯预选赛，山东泰山近期两次成为国脚大户，人才储备充足。纵观目前国内各家俱乐部，山东泰
### [广州：体育场馆、星级酒店等场所即日起实行扫码进场](https://finance.sina.com.cn/jjxw/2021-06-08/doc-ikqciyzi8496385.shtml)
> 概要: 原标题：广州市新型冠状病毒肺炎防控指挥部办公室文化旅游防控组秘书处关于加强文化旅游体育场所疫情防控的通知 来源：广州市文化广电旅游局网站...
### [隐私项目Tor获ZOMG赠款用于编码语言升级](https://www.btc126.com//view/170304.html)
> 概要: 据Coindesk消息，Zcash技术咨询委员会ZOMG（Zcash Open Major Grants）今日宣布，他们将授予隐私项目Tor 67万美元的赠款，以继续开发Tor客户端的Rust编码语言......
### [向苗阜道歉，感谢苗阜不追究经济赔偿，高峰师弟这一次老实了](https://new.qq.com/omn/20210608/20210608A0CJVI00.html)
> 概要: 说到苗阜和马腾翔，熟悉相声的人应该都不陌生。两个人都是扎根在大西北的相声演员。只不过，苗阜的青曲社越做越大，而马腾翔自己的社团几乎没有了消息。前一段，苗阜主导西安相声节的时候，马腾翔就不断地发文，可是......
# 小说
### [重生野性时代](https://m.qidian.com/book/1013026063/catalog)
> 作者：王梓钧

> 标签：都市生活

> 简介：从2018回到1993，记忆中，那一年的夏天很热。

> 章节总数：共739章

> 状态：完本
# 论文
### [None](https://paperswithcode.com/paper/mlp-mixer-an-all-mlp-architecture-for-vision)
> 日期：4 May 2021

> 标签：None

> 代码：https://github.com/google-research/vision_transformer

> 描述：Convolutional Neural Networks (CNNs) are the go-to model for computer vision. Recently, attention-based networks, such as the Vision Transformer, have also become popular.
### [Initial Classifier Weights Replay for Memoryless Class Incremental Learning](https://paperswithcode.com/paper/initial-classifier-weights-replay-for)
> 日期：31 Aug 2020

> 标签：INCREMENTAL LEARNING

> 代码：https://github.com/EdenBelouadah/class-incremental-learning

> 描述：Incremental Learning (IL) is useful when artificial systems need to deal with streams of data and do not have access to all data at all times. The most challenging setting requires a constant complexity of the deep model and an incremental model update without access to a bounded memory of past data.
