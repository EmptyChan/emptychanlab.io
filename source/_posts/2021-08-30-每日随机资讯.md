---
title: 2021-08-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MayonVolcano_ZH-CN0183039911_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-08-30 21:23:11
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MayonVolcano_ZH-CN0183039911_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [前国际奥委会主席罗格去世 享年79岁](https://ent.sina.com.cn/s/u/2021-08-30/doc-iktzqtyt2897041.shtml)
> 概要: 北京时间8月30日消息，国际奥委会（IOC）宣布前国际奥委会主席雅克·罗格伯爵去世，享年79岁。　　雅克·罗格伯爵1942年出生于比利时根特，曾是一名运动员，并参加了1968年、1972年和1976年......
### [歌手LiSA全日本巡演活动再开！8月第5周新闻汇总](https://news.dmzj.com/article/72033.html)
> 概要: 这次准备的是LiSA演唱会活动再开，下一部漫画大奖2021公开最终获奖作品，漫画《月刊少女野崎君》喜迎连载10周年，美国调查公司动画市场供不应求，以及最后的新作动画相关消息。
### [P站美图推荐——睡裙特辑](https://news.dmzj.com/article/72034.html)
> 概要: 雪白的睡裙和慵懒的表情，毫无防备的姿态，成功入选最佳心动瞬间top3（个人）
### [无止境的恶性竞争：小心“高退出壁垒”的行业](https://www.huxiu.com/article/452149.html)
> 概要: 本文来自微信公众号：思想钢印（ID：sxgy9999），作者：人神共奋，题图来自：视觉中国一、进入壁垒与退出壁垒很多投资者喜欢“进入壁垒高”的行业或产品，这个概念的提出者施蒂格勒认为，“进入壁垒”是一......
### [“熄火”的盲盒，正在以更大的野心收割年轻人](https://www.huxiu.com/article/449812.html)
> 概要: 出品 | 虎嗅商业、消费与机动组作者 | 苗正卿题图 | 视觉中国每个年轻人可能都有过被盲盒支配的疯狂，而投资人对盲盒的疯狂在2月份的香港股市达到顶峰：“盲盒第一股”泡泡玛特的市值冲到了1250亿。两......
### [The Fatiguing Effects of Camera Use in Virtual Meetings pdf](https://psycnet.apa.org/fulltext/2021-77825-003.pdf)
> 概要: The Fatiguing Effects of Camera Use in Virtual Meetings pdf
### [Nvui: A NeoVim GUI written in C++ and Qt](https://github.com/rohit-px2/nvui)
> 概要: Nvui: A NeoVim GUI written in C++ and Qt
### [GSC《初音未来NT》初音未来粘土人](https://news.dmzj.com/article/72040.html)
> 概要: GSC根据《初音未来New Type》制作的初音未来粘土人即将于31日开始预订。本作有通常颜、笑颜、仰视颜三种表情，另外还有音符、迷你包装和两手抱在一起的配件可以选择。
### [球迷模仿球星经典时刻，简直灵魂附体](https://bbs.hupu.com/45005499.html)
> 概要: 球迷模仿球星经典时刻，简直灵魂附体
### [75岁狄龙近照曝光 儿子驳斥假消息称父亲身体很好](https://ent.sina.com.cn/s/h/2021-08-30/doc-iktzscyx1158331.shtml)
> 概要: 新浪娱乐讯 近日，狄龙儿子谭俊彦在微博晒出父亲狄龙和儿子的爷孙合影，祝贺爸爸满75岁、儿子满4岁，一张照片是戴着帽子的狄龙抱着孙子，另一张则是狄龙牵着孙子散步的背影照；照片中狄龙一脸和蔼可亲的模样，也......
### [F:NEX公开《白砂的Aquatope》空空琉、风花手办彩色原型](https://news.dmzj.com/article/72042.html)
> 概要: F:NEX公开了目前正在制作中的《白砂的Aquatope》空空琉、风花1/7比例手办的彩色原型。另外此原型目前也在秋叶原进行展示。手办展现了动画宣传图中，二人沉浸在美丽在海底世界的样子。
### [秦牛正威发长文谈与吴亦凡恋情风波 称将彻底放下](https://ent.sina.com.cn/s/m/2021-08-30/doc-iktzqtyt2932710.shtml)
> 概要: 秦牛正威发长文谈与吴亦凡恋情风波 称将彻底放下
### [《赛博朋克2077》1.3更新后出新Bug 银手面目全非](https://www.3dmgame.com/news/202108/3822367.html)
> 概要: 《赛博朋克2077》1.3补丁已经发布，为游戏带来额外内容，并对游戏平衡性进行了调整，修复了大量Bug等。但有人打上新补丁后却发现，游戏里又出现了一些新的和意想不到的Bug，甚至对强尼·银手也造成了影......
### [组图：李敏镐被曝新恋情 与momoland妍雨看电影庆生](http://slide.ent.sina.com.cn/star/k/slide_4_704_360907.html)
> 概要: 组图：李敏镐被曝新恋情 与momoland妍雨看电影庆生
### [视频：赵丽颖低调现身带儿子逛商场 母子俩玩手指游戏画面温馨](https://video.sina.com.cn/p/ent/2021-08-30/detail-iktzscyx1187664.d.html)
> 概要: 视频：赵丽颖低调现身带儿子逛商场 母子俩玩手指游戏画面温馨
### [《宝可梦：皮卡丘和可可的冒险》国内定档9月10日](https://www.3dmgame.com/news/202108/3822369.html)
> 概要: 今日（8月30日），全球知名IP宝可梦系列剧场版《宝可梦：皮卡丘和可可的冒险》正式宣布定档9月10日在全国上映，同时公布了定档海报和定档预告。《宝可梦：皮卡丘和可可的冒险》定档预告：本次发布的定档预告......
### [「仙剑奇侠传」阿奴粘土人开订](http://acg.178.com/202108/424292478786.html)
> 概要: 出自经典PRG游戏「仙剑奇侠传」中的苗族的少女阿奴化身粘土人登场。商品全高100mm，采用了PVC、ABS材质；包含可替换表情「普通脸」、「悲伤脸」和「生气脸」，配件有武器「木杖」、「笛子」与「桃花林......
### [「东京卍复仇者」黑川伊佐那官方生日贺图公开](http://acg.178.com/202108/424294320004.html)
> 概要: 今日（8月30日）是「东京卍复仇者」中角色·黑川伊佐那的生日，官方公开了为其绘制的生日贺图，祝黑川生日快乐！「东京卍复仇者」是日本漫画家和久井健创作的漫画作品，根据漫画改编的同名电视动画由LIDEN ......
### [一句话概括基层工作生态](https://www.huxiu.com/article/452196.html)
> 概要: 本文来自微信公众号：行业研习（ID：hangyeyanxi），作者：林辉煌，头图来自：《山海情》剧照“一个人干”基层干部都说自己很忙，5+2，白加黑，那些工作就像中年男人的烦恼一样，一个还没有解决，另......
### [《皮卡丘和可可的冒险》定档9月10日 就决定是你了](https://acg.gamersky.com/news/202108/1419573.shtml)
> 概要: 《宝可梦：皮卡丘和可可的冒险》定档9月10日，监督矢岛哲生特意为大家献上亲笔手写信，还有中国版海报。
### [DockerHub再现百万下载量黑产镜像，小心你的容器被挖矿](https://www.tuicool.com/articles/IZB7Fzb)
> 概要: 近年来云原生容器的应用越来越流行，统计数据显示高达47%生产环境容器镜像会来源于公用仓库1，Docker Hub作为全球最大的公共容器镜像仓库，已然成为整个云原生应用的软件供应链安全重要一环，其镜像的......
### [The Oasis of Palmyra](https://www.laphamsquarterly.org/home/oasis-palmyra)
> 概要: The Oasis of Palmyra
### [「Visual Prison」第2弹短剧「ECLIPSE」公开](http://acg.178.com/202108/424299222663.html)
> 概要: 由上松范康负责原作，A-1Pictures负责制作的原创电视动画「Visual Prison」公开了第2弹短剧「ECLIPSE」，该作将于2021年10月播出。「Visual Prison」第2弹短剧......
### [硅谷提出租赁机器人新方案，帮小型工厂一年节省数万美元](https://www.tuicool.com/articles/7rAfamv)
> 概要: 智东西（公众号：zhidxcom）编译 | 杨畅编辑 | Panken智东西8月30日消息，根据调查，路透社发现美国硅谷科技产业区针对美国小型工厂劳动力短缺给出了新的解决方案：租赁机器人。租赁机器人模......
### [什么是合成资产？](https://www.tuicool.com/articles/y26B7bJ)
> 概要: 区块链上的去中心化金融正变得越来越流行。随着这种流行，出现了新形式的资产，以满足更广泛的用户群的需求。这些新资产中最重要的一个类别就是合成资产（Synthetic Assets）。合成资产并不是区块链......
### [36氪独家 | 护肤品品牌「林清轩」完成数亿元B轮融资，估值10个月涨超3倍](https://www.tuicool.com/articles/yEVfIv6)
> 概要: 文 | 国佳佳    编辑 | 董洁36氪获悉，护肤品品牌「林清轩」近日完成数亿元B轮融资，本轮融资由未来资本领投，老股东海纳亚洲创投基金SIG、碧桂园创投继续加码，杭州源琛等跟投。此次融资距离A轮不......
### [《辐射4》大型MOD《Sim Settlements 2》DLC公布](https://www.3dmgame.com/news/202108/3822391.html)
> 概要: B社经典游戏《辐射4》虽然已经发售多时，不过各种MOD依然让玩家们欲罢不能，日前民间大型MOD《Sim Settlements 2》作者宣布，将推出该MOD的DLC《Chapter 2: Gunner......
### [「大正处女御伽话 一起走过的春夏秋冬」封面公开](http://acg.178.com/202108/424307072744.html)
> 概要: 漫画「大正处女御伽话」的首部改编小说「大正处女御伽话 一起走过的春夏秋冬」封面图正式公开，本作由はむばね创作，描绘了夕月和珠彦二人在四个季节发生的四篇故事。此外，书中还收录了原作漫画作者桐丘さな绘制的......
### [外媒绘制华为卷轴屏手机渲染图，外观类似 Mate X](https://www.ithome.com/0/572/408.htm)
> 概要: IT之家8 月 30 日消息 外媒 LetsGoDigital 表示，华为正在开发一款卷轴屏的智能手机，拥有可拉出的大屏，屏幕可覆盖整个正面和背面的大部分区域，针对这款机型，他们基于专利绘制了一系列渲......
### [国家新闻出版署：未成年人平均每周仅可玩 3 小时网络游戏](https://www.ithome.com/0/572/415.htm)
> 概要: IT之家8 月 30 日 近日有不少家长反映，一些青少年沉迷网络游戏严重影响了正常的学习生活和身心健康，甚至导致一系列社会问题，使许多家长苦不堪言，成了民心之痛。因此国家新闻出版署在新学期开学之际印发......
### [第一批95后基民，还没有割肉](https://www.huxiu.com/article/452315.html)
> 概要: 本文来自微信公众号：新周刊（ID：new-weekly），作者：Echo，题图来自：视觉中国最近一段时间的基金市场，大概只能用一个字形容——惨。“医药配白酒，抱头哭一宿”，广大基民心头涌起的无力感之巨......
### [Three Clocks Are Better Than One](https://www.tigerbeetle.com/post/three-clocks-are-better-than-one)
> 概要: Three Clocks Are Better Than One
### [LG UltraGear GP9 游戏扬声器发布：提供更好的游戏沉浸感，约 3049 元](https://www.ithome.com/0/572/423.htm)
> 概要: IT之家8 月 30 日消息 据外媒 Devdiscourse 报道，LG 最新发布了 UltraGear GP9 游戏扬声器，主要目标人群为游戏玩家，可以在游戏时为玩家提供更高水平的沉浸感。该扬声器......
### [十年4162场，这是郭德纲启用张九南的原因，德云斗笑社的团宠](https://new.qq.com/omn/20210830/20210830A0BEAF00.html)
> 概要: 随着德云斗笑社第二季的开播，张九南的人气一下子达到顶峰，与此同时他也面临了诸多的非议，人红是非多这句话确实是真理。            有观众认为张九南的风格太闹腾了，就像舞台上说相声的烧饼一样，这......
### [在龙珠系列动漫中，曾经的反派如今怎么样了？比克直接改行当保姆](https://new.qq.com/omn/20210830/20210830A0BPR000.html)
> 概要: 龙珠系列动漫是日本漫画家鸟山明的得意之作，而在这个系列的动漫之中，除了主角悟空等人之外，在反派角色的刻画上，鸟山明也是用尽了心思，从龙珠到龙珠Z，动漫中也是接连出现了很多反派角色，那么，这些反派角色的......
### [关于螺旋手里剑这个忍术，为什么一开始丢不出去，后来就可以了？](https://new.qq.com/omn/20210830/20210830A0BPUB00.html)
> 概要: 动漫火影忍者是日本漫画家岸本齐史的代表作品，这部作品在世界范围内的影响也可以说是首屈一指，即便是已经完结了这么长的时间，热度也丝毫不弱于《海贼王》等还在连载的动漫，堪称是动漫史上的一部神作。     ......
### [六道仙人身为忍者始祖，为何处理不好儿子的问题？作为父亲不合格](https://new.qq.com/omn/20210830/20210830A0BPSP00.html)
> 概要: 动漫火影忍者是大家都非常喜欢的一部日本动漫作品，而在世界范围内，火影忍者这部动漫的影响力也是首屈一指的，这部动漫讲述了忍者始祖六道仙人所创造的忍者世界发生的故事，不过，关于六道仙人，其实还有一件让人很......
### [三七互娱：已投资优质 VR 内容研发商 Archiact，布局 VR/AR 内容生态](https://www.ithome.com/0/572/441.htm)
> 概要: IT之家8 月 30 日消息 三七互娱今日在投资者互动平台表示，公司已投资了优质的 VR 内容研发商 Archiact 布局内容生态等，不断完善 VR/AR 领域的整体布局。三七互娱还表示，一方面，公......
### [果然出事了！特别篇《斗破苍穹》还没上映，就被观众骂惨了](https://new.qq.com/omn/20210830/20210830A0CAQL00.html)
> 概要: 2021年夏季，众多国漫就像高温一样热火朝天地在播放着，如《不良人》、《斗罗大陆》等都得到了不错的好评，而《斗破苍穹》第4季也刚好结束。大家都知道，这《斗破苍穹》的动漫基本就按着原著在进行，而整个作品......
### [Chainlink发布跨链互操作性协议CCIP](https://www.btc126.com//view/184760.html)
> 概要: 8月30日，Chainlink发布跨链互操作性协议（CCIP），旨在实现去中心化跨链消息传递和通证转移。据悉，CCIP是跨链通信新的开源标准，目的是在几百个公链和私有链网络之间建立通用的连接，让本来孤......
### [Blockchain.com 平台总交易量突破一万亿美元](https://www.btc126.com//view/184762.html)
> 概要: 加密货币钱包和交易平台Blockchain.com首席财务官 Macrina Kgil 称，在平台上交易的加密货币超过了1万亿美元。自2012年以来，已经处理了近三分之一的比特币网络交易，其中大部分交......
### [习近平：加强反垄断反不正当竞争监管力度](https://finance.sina.com.cn/china/2021-08-30/doc-iktzqtyt3068592.shtml)
> 概要: 原标题：习近平主持召开中央全面深化改革委员会第二十一次会议强调 加强反垄断反不正当竞争监管力度 完善物资储备体制机制深入打好污染防治攻坚战 李克强王沪宁韩正出席...
### [倡议建设“空中丝绸之路”创新示范区 服贸会论坛上提前“剧透”](https://finance.sina.com.cn/jjxw/2021-08-30/doc-iktzscyx1299658.shtml)
> 概要: 原标题：倡议建设“空中丝绸之路”创新示范区 服贸会论坛上提前“剧透” 新京报快讯（记者 陈琳）8月30日，记者从“空中丝绸之路”国际合作峰会组委会获悉...
### [北京市地方金融监管局：正在编制金融业“十四五”规划 推动出台绿色金融等政策措施](https://finance.sina.com.cn/roll/2021-08-30/doc-iktzscyx1301987.shtml)
> 概要: 原标题：北京市地方金融监管局：正在编制金融业“十四五”规划 推动出台绿色金融等政策措施 上证报中国证券网讯（记者 张琼斯）北京市地方金融监督管理局党组成员...
### [上海科创中心建设报告出炉，基础研究、三大产业有何亮点](https://finance.sina.com.cn/roll/2021-08-30/doc-iktzscyx1303055.shtml)
> 概要: 原标题：上海科创中心建设报告出炉，基础研究、三大产业有何亮点 截至2020年底，上海有37家企业在科创板上市，占科创板首发募资总额的36%，位列全国第一。
### [新作+DLC连发 Gamera Game公布多款游戏情报](https://www.3dmgame.com/news/202108/3822415.html)
> 概要: 8月30日晚间，发行了《戴森球计划》《了不起的修仙模拟器》《港诡实录》《烟火》等游戏的国内独立游戏发行商Gamera Game，举办了一场名为“杀疯了的独立游戏发行公司Gamera Game九月要发行......
### [这也行？蔡徐坤被质疑歌曲“卖期货”：专辑卖了8000万，4个月过去只出了5首！回应来了](https://finance.sina.com.cn/china/gncj/2021-08-30/doc-iktzscyx1306915.shtml)
> 概要: 偶像发新歌，粉丝买专辑本是很正常的现象。 但近日有歌迷发现，自己偶像的专辑卖了8000万，竟有一半的歌还没出… 图片来源：QQ音乐 专辑卖了8000万...
### [沈阳二手房陷入“困局”:15万套库存高居不下 位置不好的挂一年卖不了](https://finance.sina.com.cn/china/gncj/2021-08-30/doc-iktzscyx1305001.shtml)
> 概要: 原标题：沈阳二手房陷入“困局”:15万套库存高居不下 位置不好的挂一年卖不了 来源：中国房地产报 沈阳二手房库存人均占比远超天津和重庆，链家统计的在售二手房数量排名上...
### [越南美女NguyenPhung福利图 赛博老婆你值得拥有](https://www.3dmgame.com/bagua/4834.html)
> 概要: 今天为大家带来的是越南正妹NguyenPhung的福利图，她来自越南胡志明市，本身是设计师，姣好的身材和美貌与她自己设计的硬核赛博概念服装格外搭配。NguyenPhung一身科技感元素也让她获得了不少......
### [十年前打妻子，如今打女儿，他这是家暴成瘾？](https://new.qq.com/omn/20210830/20210830A0DAEE00.html)
> 概要: “家暴只有零次和无数次”“疯狂英语”的创始人李阳，十年前被曝家暴妻子，如今又被曝出殴打女儿。李阳的前妻Kim在社交平台发表了一篇长文，控诉李阳殴打两人的女儿，并且威胁女儿不能将这件事告诉别人。    ......
### [交易总额超1亿美元的NFT收藏品系列已达10个](https://www.btc126.com//view/184772.html)
> 概要: 8月30日消息，据最新数据显示，交易总额超过1亿美元的NFT收藏品系列已达10个，分别是：......
### [深圳：拟放宽数字产品市场准入，加速数字人民币落地](https://www.btc126.com//view/184776.html)
> 概要: 8月30日，随着《深圳经济特区数字经济产业促进条例（草案）》提请深圳市七届人大常委会第三次会议审议，深圳将鼓励数字产品消费，把优质数字产品打造成为深圳消费的金字招牌，对符合条件的产品在销售环节予以支持......
### [李小萌晒孕期美照，和老公王雷穿情侣装，双手抚摸孕肚又美又温柔](https://new.qq.com/omn/20210830/20210830A0CYNW00.html)
> 概要: 李小萌晒孕期美照，和老公王雷穿情侣装，双手抚摸孕肚又美又温柔
### [朱茵晒哪吒头造型清纯可爱，获黄贯中夸赞，夫妻俩罕见秀恩爱](https://new.qq.com/omn/20210830/20210830A0DJJP00.html)
> 概要: 8月30日，朱茵晒出了一张哪吒头造型的照片，照片中的她穿着一条花裙子，扎着哪吒头，十分减龄，网友纷纷感叹：岁月对朱茵也太温柔了，为什么过去了这么多年，依旧这么好看，都没有什么变化。          ......
### [流言板经典父子局？帕斯卡尔晒与布里奇斯合影：我儿子是个杀手](https://bbs.hupu.com/45015585.html)
> 概要: 虎扑08月30日讯 爵士球员埃里克-帕斯卡尔今日更新Instagram晒出与太阳球员米卡尔-布里奇斯的合影（见新闻配图）。配文写道：“我的儿子是个杀手！！我的哥们是个秘密，但这很酷！”根据此前的相关报
### [流言板跃跃欲试！小南斯晒身披开拓者11号球衣照：让我们开始吧](https://bbs.hupu.com/45015597.html)
> 概要: 虎扑08月30日讯 开拓者球员小拉里-南斯今日更新Instagram晒出一张身披开拓者11号球衣的合成照（见新闻配图）。配文写道：“撕裂之城，让我们开始吧🤟🏽”根据此前的相关报道，小南斯在三方交易
### [流言板这球用得有年头了！努尔基奇晒照：每一天我都心怀感激](https://bbs.hupu.com/45015594.html)
> 概要: 虎扑08月30日讯 开拓者球员优素福-努尔基奇今日更新Instagram晒出一组训练照（见新闻配图）。配文写道：“每一天，我都感觉心怀感激🙏🏻❤️”2020-21赛季常规赛，努尔基奇场均出场23.
# 小说
### [终极教师](http://book.zongheng.com/book/347511.html)
> 作者：柳下挥

> 标签：都市娱乐

> 简介：方炎原是太极世家传人，因为不堪忍受一个野蛮女人的暴力欺负而翘家逃跑，弃武从文成为一所私立学校的高中语文老师。于是，一个传奇教师就此诞生！终极教师量身订制的手游《终极酷跑》上线啦，欢迎下载：http://download.gyyx.cn/Default.ashx?typeid=978&netType=1

> 章节末：老柳新书《逆鳞》已经发布！

> 状态：完本
# 论文
### [MT-ORL: Multi-Task Occlusion Relationship Learning | Papers With Code](https://paperswithcode.com/paper/mt-orl-multi-task-occlusion-relationship)
> 日期：12 Aug 2021

> 标签：None

> 代码：https://github.com/fengpanhe/mt-orl

> 描述：Retrieving occlusion relation among objects in a single image is challenging due to sparsity of boundaries in image. We observe two key issues in existing works: firstly, lack of an architecture which can exploit the limited amount of coupling in the decoder stage between the two subtasks, namely occlusion boundary extraction and occlusion orientation prediction, and secondly, improper representation of occlusion orientation.
### [On Procedural Adversarial Noise Attack And Defense | Papers With Code](https://paperswithcode.com/paper/on-procedural-adversarial-noise-attack-and)
> 日期：10 Aug 2021

> 标签：None

> 代码：https://github.com/momo1986/adversarial_example_simplex_worley

> 描述：Deep Neural Networks (DNNs) are vulnerable to adversarial examples which would inveigle neural networks to make prediction errors with small per- turbations on the input images. Researchers have been devoted to promoting the research on the universal adversarial perturbations (UAPs) which are gradient-free and have little prior knowledge on data distributions.
