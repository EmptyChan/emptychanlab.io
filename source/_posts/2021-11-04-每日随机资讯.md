---
title: 2021-11-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FoleysBridge_ZH-CN4338959688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-04 23:03:31
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FoleysBridge_ZH-CN4338959688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Laravel 8.69.0 发布，PHP 开发框架](https://www.oschina.net/news/167183/laravel-8-69-0-released)
> 概要: Laravel v8.69.0 已发布，主要更新如下：新特性改进异常处理的内容协商 (#39385)添加了对 MariaDB 的 SKIP LOCKED 的支持 (#39396)自定义将字符串转换为 ......
### [Firefox 94 发布，六种限时提供的季节性配色](https://www.oschina.net/news/167182/firefox-94-released)
> 概要: Firefox 94 现已发布，具体更新内容如下：Newv94 中包含了六种限时提供的有趣的季节性配色，用户可以找到一种颜色来适应（或提升）自己的每一种心情。Firefox macOS 现在使用 Ap......
### [Canonical 发布为英特尔 IoT 设备优化的 Ubuntu](https://www.oschina.net/news/167191/ubuntu-intel-iotg)
> 概要: Canonical 近日宣布，他们已经发布了为英特尔物联网（IoT、Internet of Things）设备优化的 Ubuntu Core 和 Ubuntu Desktop 20.04 LTS。经过......
### [Dapr 作为孵化项目加入 CNCF](https://www.oschina.net/news/167278/darp-joined-in-cncf)
> 概要: 我们很高兴地宣布，这周，Dapr 作为孵化项目加入 CNCF。自最初启动以来，Dapr 项目就明确表示，该项目最终将捐赠给一个与供应商无关的基金会。该项目最近成立了一个指导和技术委员会，通过确保各供应......
### [声优事务所Pro Fit停业！后续成立新事务所](https://news.dmzj.com/article/72651.html)
> 概要: 哈喽(,,･∀･)ﾉ゛鸭❤欢迎再次光临，花卷儿与卷毛的测评小栏目《老大的快乐我不懂》系列~，这期是“饮料”特辑，再一次斥巨资买了好几箱零度饮料和甜酒，男女都适用，小酌一杯（小孩子不要学）。
### [任职三年后 GitHub CEO Nat Friedman 宣布即将卸任，回归创业根源](https://segmentfault.com/a/1190000040909653?utm_source=sf-homepage)
> 概要: 2018 年，微软宣布正式收购GitHub，微软副总裁 Nat Friedman 担任GitHub 的新 CEO。2021 年 11 月 3 日，GitHub 首席执行官Nat Friedman在任职......
### [漫画《生者的行进》系列累计突破100万部](https://news.dmzj.com/article/72654.html)
> 概要: 日本网站appmedia总结了目前已经宣布将在2022年冬播出或者继续播的动画作品。截止到4日，共有36部作品。以下就是各个作品的名称和宣传图。
### [中国互联网20年“流量-变现”演化史](https://www.huxiu.com/article/469460.html)
> 概要: 本文来自微信公众号：黄有璨（ID：owen_hyc），作者：黄有璨，头图来自：视觉中国今天，在我们谈论互联网时，我们有必要清晰地知道——参考36氪的说法，互联网在中国已经可以被分为“消费互联网”和“产......
### [动漫情侣头像｜颜不如初，人不如故](https://new.qq.com/omn/20211102/20211102A0CYAV00.html)
> 概要: 图源网络侵删  原截或原创看到可评论注明一键分享投币评论转发欢迎白嫖加关注......
### [轻小说读者在作者的鼓励下创作出获大奖的小说](https://news.dmzj.com/article/72659.html)
> 概要: 近日，创作了《弱势角色友崎同学》的作家屋久悠树，在推特上分享了一名自己的中学生粉丝的，从想写轻小说但什么都写不出来，到获得网文大奖，作品很可能将在明年出版的故事。
### [手游《哈利波特：巫师联盟》2022年1月停服](https://news.dmzj.com/article/72660.html)
> 概要: Niantic宣布了手机游戏《哈利波特：巫师联盟》将于2022年1月31日停服的消息。从12月6日起，作品将从App Store，Google Play，Galaxy Store上下架，氪金功能也一并停止。
### [「阿柴枸杞」圣诞限定款手办开订](http://acg.178.com/202111/429993678284.html)
> 概要: 近日，「阿柴枸杞」圣诞限定款手办正式开启预订，作品采用树脂材质，全高约140mm，售价为268元，预计将于2021年12月20日发售......
### [轻小说「公主殿下的家庭教师」第10卷第一弹彩插公开](http://acg.178.com/202111/429996744668.html)
> 概要: 近日，轻小说「公主殿下的家庭教师」公开了第10卷的第一弹彩页插图，该卷将于11月20日发售。「公主殿下的家庭教师」是七野りく创作、cura插画、富士见Fantasia文库所属的轻小说作品......
### [动画「斗破苍穹」公开最新福利壁纸](http://acg.178.com/202111/430002197912.html)
> 概要: 今日（11月4日），国产动画「斗破苍穹」特别篇·叁「斗破苍穹三年之约」公开了最新福利壁纸。本次公开的壁纸有长发飘飘的纳兰嫣然、凝聚火莲的萧炎，还有最后登场的经费战士。「斗破苍穹三年之约」是阅文集团、企......
### [《健身拳击2》推出免费更新 新教练“Guy”上线](https://www.3dmgame.com/news/202111/3827485.html)
> 概要: 《健身拳击2》推出了免费更新，新教练“Guy”现已正式上线，这是该作的第10位教练，CV是绿川光。《健身拳击2》免费更新介绍视频：“Guy”是在《健身拳击2》登场角色中从未出现过的新型教练，用一句话概......
### [COD18及《战区》太平洋战场CG预告 11月5日发售](https://www.3dmgame.com/news/202111/3827488.html)
> 概要: 今日（11月4日），动视发布《使命召唤18：先锋》及《战区》太平洋战场CG预告，该作将于11月5日发售，登陆PC/PS4/PS5/X1/XSS/XSX平台。预告片：视频截图：......
### [「白雪公主」真人电影巫后角色将由盖尔·加朵出演](http://acg.178.com/202111/430006030326.html)
> 概要: 今日（11月4日），根据国外媒体的消息，以色列著名演员盖尔·加朵将加盟迪士尼真人电影「白雪公主」饰演邪恶的皇后。盖尔加朵最为人熟知的角色便是DC知名女性超级英雄「神奇女侠」。本片改编自迪士尼上世纪三十......
### [林峯：从影以来 拍《星辰大海》我哭得最厉害](https://ent.sina.com.cn/v/m/2021-11-04/doc-iktzqtyu5368415.shtml)
> 概要: ■新快报记者 徐绍娜　　励志时代剧正在热播，剧集开播前就因一段“上头”的预告片引发关注，在剧中贡献了不少“咆哮”镜头的林峯更被指是马景涛附体。近日，林峯接受记者采访，澄清角色并非“咆哮帝”，只是有太多......
### [An oral history of Bank Python](https://calpaterson.com/bank-python.html)
> 概要: An oral history of Bank Python
### [4K小米激光影院2发布:全球首发杜比视界 特惠12999元](https://www.3dmgame.com/news/202111/3827496.html)
> 概要: 11月4日，小米激光影院2正式发布，它是全球首款支持杜比视界的投影产品，原价14999元，双11到手价12999元，立省2000元。小米激光影院2采用0.23:1 大景深超短焦镜头设计，机身距离墙面仅......
### [《海贼王》1031话情报：若我失去人性 由你杀掉我](https://acg.gamersky.com/news/202111/1435376.shtml)
> 概要: 《海贼王》1031话情报公开，愤怒的大妈吃掉自己一年的寿命得到更强的量，来对付基德和罗。在另一边，山治发现自己身体的问题，是杰马尔的战斗服导致的。
### [如何成为中国首富？](https://www.huxiu.com/article/469711.html)
> 概要: 出品 | 虎嗅汽车组作者 | 梓楠法师中国资本市场的风水，终于转到汽车业。最近，胡润研究院发布了2021年胡润百富榜。在今年的榜单中，汽车业富豪的排名均有所上涨。其中，长城汽车的魏建军夫妇以2180亿......
### [《奥特曼英雄传》系列商品PV公布 财团最新收割技术](https://acg.gamersky.com/news/202111/1435516.shtml)
> 概要: 奥特曼中国官方微博公布了万代最新的《奥特曼英雄传》系列商品PV。
### [岚演唱会电影召开舞台问候活动 四名成员惊喜重聚](https://ent.sina.com.cn/2021-11-04/doc-iktzscyy3631555.shtml)
> 概要: 11月3日......
### [某《火凤燎原》主题剧本杀谎称有授权 东立官方打假](https://acg.gamersky.com/news/202111/1435521.shtml)
> 概要: FB《水镜八奇》剧本杀谎称自己得到东立出版社《火凤燎原》授权，官方出面打假辟谣。
### [碳水使人快乐，到底什么是碳水化合物？](https://www.huxiu.com/article/469667.html)
> 概要: 本文选自中国工程院院刊《Engineering》2021年第6期，来源：A Review of the Design and Architecture of Starch-Based Dietary ......
### [组图：藤冈靛山田裕贵等出席试映会 配音新动画电影将上映](http://slide.ent.sina.com.cn/star/jp/slide_4_704_363358.html)
> 概要: 组图：藤冈靛山田裕贵等出席试映会 配音新动画电影将上映
### [恩智浦 CEO ：工业自动化领域半导体短缺严重，未得到媒体足够重视](https://www.ithome.com/0/584/769.htm)
> 概要: IT之家11 月 4 日消息，2021 年 11 月 2 日，恩智浦 CEO Kurt Sievers 对公司 2021 年第三季度的财报进行了一些解读。有分析师就工业互联网、供应链紧张等问题进行了提......
### [视频：周震南父亲公司违反电力法被处罚 罚款约23万元](https://video.sina.com.cn/p/ent/2021-11-04/detail-iktzqtyu5404913.d.html)
> 概要: 视频：周震南父亲公司违反电力法被处罚 罚款约23万元
### [《特种部队：蛇眼起源》特辑打造系列最强打戏](https://acg.gamersky.com/news/202111/1435547.shtml)
> 概要: 由美国派拉蒙影片公司出品的动作冒险大片《特种部队：蛇眼起源》今日发布动作特辑，揭秘炫酷打戏的诞生。
### [DeFi十月回顾：Avalanche与Fantom锁仓量高速增长，算法稳定币项目全面复苏](https://www.tuicool.com/articles/7RBn2aI)
> 概要: 蒋海波｜2021-11-4 17:26多链策略使Curve总锁仓量突破200亿美元，Abracadabra这类建立在现有DeFi基础设施之上的项目也很可能得到应用。随着市场的回暖，BTC、ETH等相继......
### [吉吉回应称女儿不能没有父亲 尽可能美渣和平相处](https://ent.sina.com.cn/y/youmei/2021-11-04/doc-iktzscyy3647526.shtml)
> 概要: 新浪娱乐讯  据外媒报道，26岁的超级名模吉吉·哈迪德（Gigi Hadid）没打算和前男友泽恩·马利克（Zayn Malik）一刀两断老死不相往来，虽然两人的确分了手。因为在吉吉看来，女儿不能没有父......
### [新剧首播｜《和平之舟》不凡启航！陈坤张天爱乘风破浪上演惊险救援](https://new.qq.com/rain/a/20211104A09HEA00)
> 概要: 天气转凉日居家煲剧时今日新鲜物料送达请各位追剧鹅查收！                        主演：陈坤、张天爱、张萌、尹昉11月4日腾讯视频全网首播会员22点跟播卫视，非会员次日22点转免他......
### [领势 MX5503 MESH 组网路由发布： AX5400 速率，三只 2499 元](https://www.ithome.com/0/584/784.htm)
> 概要: IT之家11 月 4 日消息，不久前 Linksys 领势发布了 MX5502 组网路由器，今日再次发布了 MX5503 型号，三只装首发售价 2499 元。该产品支持 WiFi 6，具有 AX540......
### [字节跳动、菜鸟相继“闻香”，这个百亿级“香味”赛道有何看点？](https://www.tuicool.com/articles/MjmEBrf)
> 概要: 继螺蛳粉、奶茶、美瞳、口腔护理后，香水即将成为互联网巨头在新消费领域下一个攻掠的“城池”。日前，菜鸟与国货香氛潮牌“气味图书馆”达成战略合作，双方将在线上线下渠道共用一盘货、供应链数字化管理、供应链的......
### [BAT“屠版”元宇宙：重金投资、招兵买马、注册商标，大搞基建造硬件](https://www.ithome.com/0/584/791.htm)
> 概要: 万万没想到，BAT 的元宇宙版图已经这么大了。现在无论是参加活动还是听讲座，大家见面第一句都是“嗨，你今天（听说）元宇宙了吗？”，元宇宙已经成为了科技圈和投资圈业内人士见面必谈话题之一。目前，行业内主......
### [贝索斯：20亿美金拿去花，别再骂我不管地球了](https://www.huxiu.com/article/469769.html)
> 概要: 出品 | 虎嗅科技组作者 | 袁漪琳题图 | IC photo亚马逊公司创始人贝索斯上周末坐着价值6500万美元的湾流G650ER喷气式飞机赴英国格拉斯哥参加联合国气候变化大会COP26。来自全球20......
### [腾讯公开三款自研芯片 分别面向AI、视频和网络处理](https://www.3dmgame.com/news/202111/3827526.html)
> 概要: 在过去的几年里，越来越多的企业倾向于自己开发芯片，应用于旗下的产品或业务。在消费市场上，苹果以自研芯片取代了英特尔x86处理器，先后推出了M1、M1 Pro和M1 Max，性能不断提高。部分拥有庞大数......
### [有了资本的助推，我家门口的炸串店就是下一个喜姐！](https://www.tuicool.com/articles/EN7BRzU)
> 概要: “天下武功、唯快不破”，这用来形容近几年新消费再适合不过了。过去，一个新消费成长起来大概需要10年左右，而现在，平均只要六七年就可以走向资本市场，快的只需两三年。近日，网红炸串品牌喜姐炸串完成了2.9......
### [LibreWolf – A fork of Firefox, focused on privacy, security and freedom](https://librewolf-community.gitlab.io/)
> 概要: LibreWolf – A fork of Firefox, focused on privacy, security and freedom
### [鸣人小时候真的很惨吗？岸本齐史：佐助比他惨3倍](https://new.qq.com/omn/20210814/20210814A06UT500.html)
> 概要: 在岸本早期的采访中，主持人曾经问他：岸本回答：            那么岸本这话说得有没有道理呢？我们来对比一下鸣人和佐助的苦难。首先鸣人和佐助都是父母双亡，但是鸣人根本就不知道自己爹妈是谁，也没感......
### [Can't Get You Out of My Head](https://en.wikipedia.org/wiki/Can%27t_Get_You_Out_of_My_Head_(TV_series))
> 概要: Can't Get You Out of My Head
### [58岁叶童在街边吃饭被偶遇，瘦成皮包骨，坚持33年的婚姻令人唏嘘](https://new.qq.com/rain/a/20211104A07FRP00)
> 概要: 1992年，《新白娘子传奇》的播出打破了各种收视纪录，后来它也成为中国近几十年重播次数最多的港台电视剧，几乎每年都重播，每个女孩小时候都模仿过白娘子的造型，也都会唱几句里面的主题曲，这部剧影响了几代人......
### [俄美女Cos《巫师3》叶奈法 穿蕾丝薄纱性感妩媚](https://www.3dmgame.com/bagua/4968.html)
> 概要: 今天为大家带来的是俄罗斯美女Natalia(ins：narga_lifestream)的《巫师3》叶奈法COS作品。妹子来自俄罗斯圣彼得堡，从2008年开始接触COSPLAY。她不仅是一位COSER，......
### [失去子宫，爱而不得，折腾了大半辈子，终是自己面对了所有](https://new.qq.com/rain/a/20211104A0CITX00)
> 概要: 《女性箴言》一书中，有这样一句话：“平庸的生活使人感到一生不幸，波澜万丈的人生才能使人感到生存的意义。”每个人都想要一份岁月静好的日子，可谁都无法拥有完美人生，谁都不能罢免人生的苦难，感情中的爱而不得......
### [Best Practices for Connecting to NTP Servers](https://labs.ripe.net/author/christer-weinigel/best-practices-for-connecting-to-ntp-servers/)
> 概要: Best Practices for Connecting to NTP Servers
### [Google 自研芯片启示录：不为跑分，只为打磨体验](https://www.tuicool.com/articles/r6Rv2aZ)
> 概要: 站在 2021 年底，回顾今年的智能手机发展趋势，你会想到哪些关键词呢？如果是从手机硬件来讲，或许更多人会第一个想到的，会是关于「自研芯片」在 2021 年的井喷式爆发：不仅有小米旗下的澎湃处理器时隔......
### [冯提莫这首歌，2018年火遍全网，3年后评论破3万，远超邓紫棋的歌](https://new.qq.com/rain/a/20211104A0D6U200)
> 概要: 标题：冯提莫这首歌，2018年火遍全网，3年后评论破3万，远超邓紫棋歌曲说起冯提莫来，想必大家都不陌生。自从从一名主播转行为歌手后，冯提莫的事业也越来越好了。            现今的冯提莫，不光......
### [男子结婚邀请同事被举报谋取不正当利益，公司：建议离职](https://finance.sina.com.cn/china/2021-11-04/doc-iktzscyy3683014.shtml)
> 概要: 来源：钱江晚报 阳先生是重庆人，今年28岁，是一家儿童用品公司的监察员。9月19日，他和妻子在重庆举办了婚礼，邀请了一些同事吃酒席。
### [长三角一体化上升为国家战略三周年：科创引擎，发挥对全国高质量发展的核心支撑作用](https://finance.sina.com.cn/china/gncj/2021-11-04/doc-iktzqtyu5448262.shtml)
> 概要: 原标题：长三角一体化上升为国家战略三周年：科创引擎，发挥对全国高质量发展的核心支撑作用 21世纪经济报道记者许秋莲、易佳颖、卜羽勤 上海报道...
### [“米袋子”“菜篮子”产品能否量足价稳？——农业农村部有关司局负责人回应百姓关注热点话题](https://finance.sina.com.cn/jjxw/2021-11-04/doc-iktzscyy3683216.shtml)
> 概要: 新华社北京11月4日电题：“米袋子”“菜篮子”产品能否量足价稳？——农业农村部有关司局负责人回应百姓关注热点话题 近一段时间以来，粮食...
### [西班牙驻华大使德斯卡亚：进博会对西班牙企业至关重要 解决气候变化问题各国义不容辞](https://finance.sina.com.cn/china/gncj/2021-11-04/doc-iktzscyy3685270.shtml)
> 概要: 原标题：南财对话|独家专访西班牙驻华大使德斯卡亚：进博会对西班牙企业至关重要 解决气候变化问题各国义不容辞 21世纪经济报道记者 施诗北京报道 11月4日晚上...
### [老哥都说好：小雷先生铝合金支架 8 元秒杀（京东 19 元）](https://lapin.ithome.com/html/digi/584818.htm)
> 概要: 【罗马仕同公司品牌 小雷先生】小雷先生平板 + 手机通用支架 日常售价 19.9 元，今日冲量大促，可领限量 10 元券，实付 9.9 元好价。材质为铝合金 + ABS 材料，不到 10 元能买到这种......
### [外企高管盛赞习近平演讲：备受鼓舞，坚定扩大在华投资发展信心](https://finance.sina.com.cn/china/2021-11-04/doc-iktzqtyu5451976.shtml)
> 概要: 11月4日晚，国家主席习近平以视频方式出席第四届中国国际进口博览会开幕式并发表题为《让开放的春风温暖世界》的主旨演讲。习近平强调...
### [中美就应对气候变化开展对话交流](https://finance.sina.com.cn/china/gncj/2021-11-04/doc-iktzscyy3686765.shtml)
> 概要: 原标题：中美就应对气候变化开展对话交流 中国气候变化事务特使解振华，中国COP26代表团团长、生态环境部副部长赵英民，同美国总统气候问题特使约翰·克里在《联合国气候变...
# 小说
### [致命死因](http://book.zongheng.com/book/1016542.html)
> 作者：雪渐凉

> 标签：悬疑灵异

> 简介：死亡并不可怕，可怕的是很多人不知道正走在死亡的路上，只要我们找到致死的原因，才能躲避灾祸。

> 章节末：第五十六章   裴初夏还没死

> 状态：完本
# 论文
### [Physics-based Deep Learning | Papers With Code](https://paperswithcode.com/paper/physics-based-deep-learning)
> 日期：11 Sep 2021

> 标签：None

> 代码：None

> 描述：This digital book contains a practical and comprehensive introduction of everything related to deep learning in the context of physical simulations. As much as possible, all topics come with hands-on code examples in the form of Jupyter notebooks to quickly get started.
### [Dual-Camera Super-Resolution with Aligned Attention Modules | Papers With Code](https://paperswithcode.com/paper/dual-camera-super-resolution-with-aligned)
> 日期：3 Sep 2021

> 标签：None

> 代码：https://github.com/Tengfei-Wang/DualCameraSR

> 描述：We present a novel approach to reference-based super-resolution (RefSR) with the focus on dual-camera super-resolution (DCSR), which utilizes reference images for high-quality and high-fidelity results. Our proposed method generalizes the standard patch-based feature matching with spatial alignment operations.
