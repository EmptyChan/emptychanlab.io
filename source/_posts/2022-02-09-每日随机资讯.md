---
title: 2022-02-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SevenSistersCliffs_ZH-CN5362127173_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-09 22:02:43
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SevenSistersCliffs_ZH-CN5362127173_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [OpenMandriva Lx 4.3 发布，默认使用 PipeWire](https://www.oschina.net/news/181616/openmandriva-lx-4-3-released)
> 概要: OpenMandriva Lx 是从 Mandriva Linux 衍生出来的 Linux 发行版，近日 OpenMandriva Lx 4.3 正式发布，该版本继续使用 LLVM/Clang 来构建......
### [Chrome OS 98 发布，没有采用全新的 Logo](https://www.oschina.net/news/181621/chrome-os-98-released)
> 概要: 经过短暂的延迟，Chrome OS 98 稳定版正式推出。新版本带来了一些变化，其中包括新的表情符号、新增的虚拟桌面快捷键，以及全面修改的语言设置菜单。当然也包括一些与最近发布的 Chrome 98 ......
### [LibreOffice 支持 WebP 图像格式](https://www.oschina.net/news/181627/libreoffice-webp)
> 概要: 根据 LibreOffice 代码仓库的提交记录，即将在今年夏末发布的 LibreOffice 7.4 会添加对 WebP 图像格式的支持。早在 2017 年，用户就已要求在 LibreOffice ......
### [英特尔加入 RISC-V 国际基金会，并设立 10 亿美元的基金用于发展 RISC-V](https://www.oschina.net/news/181630/intel-riscv)
> 概要: 早在 2006 年，英特尔就放弃了基于 ARM 的 CPU 开发项目，当时他们以约 6 亿美元的价格将 XScale PXA 技术出售给 Marvell Technology Group。当时的想法是......
### [探讨一下To C营销页面服务端渲染的必要性以及其背后的原理](https://segmentfault.com/a/1190000041376841?utm_source=sf-homepage)
> 概要: 最近无论是在公司还是自己研究的项目，都一直在搞 H5 页面服务端渲染方面的探索，因此本文来探讨一下服务端渲染的必要性以及其背后的原理。先来看几个问题To C 的 H5 为什么适合做 SSRTo C的营......
### [从服务端生成Excel电子表格（GcExcel + SpreadJS）](https://segmentfault.com/a/1190000041377778?utm_source=sf-homepage)
> 概要: 在服务端生成Excel电子表格，除了使用 Node.js + SpreadJS 外，葡萄城官方推荐使用 SpreadJS + GcExcel。该方案不仅能够解决批量绑定数据源并导出Excel、批量修改......
### [The Kelly criterion: How to size bets (2019)](https://explore.paulbutler.org/bet/)
> 概要: The Kelly criterion: How to size bets (2019)
### [教考热背后，偷偷辞职的教师们](https://www.huxiu.com/article/495977.html)
> 概要: 今年教资报考人数突破1100万，5年前这个数字是260万，几年间增长近5倍，很多人为之欢呼——教师行业的春天来了。可另一面，部分青年教师也受制于高强度的教学任务、拉扯的师生关系和职场环境，偷偷走上了辞......
### [2022全英音乐奖获奖名单揭晓 Adele斩获三项大奖](https://ent.sina.com.cn/y/yneidi/2022-02-09/doc-ikyakumy4961607.shtml)
> 概要: 新浪娱乐讯 北京时间2月9日凌晨 2022年全英音乐奖举行，Silk Sonic获国际乐队/组合奖，Adele获年度专辑、歌手和歌曲三项大奖，碧梨Billie Eilish获年度最佳国际歌手，黄老板E......
### [奥斯卡提名揭晓：《犬之力》领跑，两对恋人同时入围四个表演奖项](https://new.qq.com/rain/a/20220209A032IY00)
> 概要: 封面新闻记者 杨帆2022奥斯卡颁奖典礼将于3月27日举行。2月9日，美国电影艺术与科学学院公布提名名单，《犬之力》获12个提名领跑，《沙丘》10提紧随其后。日本电影《驾驶我的车》入围最佳影片、导演、......
### [「平家物语」动画官方书将于3月11日发售](http://acg.178.com/202202/438374174173.html)
> 概要: 近日，动画「平家物语」宣布将推出官方书籍「わたしたちが描いたアニメーション「平家物語」」，将在3月11日开始发售，售价为2200日元（含税），预计包含112页。该书由导演山田尚子、角色原案高野文子著作......
### [动画电影《哆啦A梦 大雄的宇宙小战争2021》新PV](https://news.dmzj.com/article/73540.html)
> 概要: 因新冠疫情而延期约一年上映的动画电影《哆啦A梦 大雄的宇宙小战争2021》公开了一段特别视频。在这段视频中，分别收录了新旧两作的主题曲和本篇映像。
### [文春炮：声优山崎遥与鼓手SHiN热恋中！](https://news.dmzj.com/article/73542.html)
> 概要: 在人气游戏《偶像大师 百万现场》中为春日未来配音的声优山崎遥（30岁），正在和鼓手SHiN（33岁）交往。
### [「刀剑神域」结城明日奈手办开订](http://acg.178.com/202202/438376100697.html)
> 概要: F：NEX作品「刀剑神域」结城明日奈手办开订，全长约465mm，主体采用ABS和PVC材料制造。该手办定价为168300日元（含税），约合人民币9278元，预计于2022年10月发售......
### [上海明确商业广告代言活动负面清单 劣迹艺人在列](https://ent.sina.com.cn/s/m/2022-02-09/doc-ikyamrmz9801737.shtml)
> 概要: 近日，为进一步规范商业广告代言活动，坚持正确广告宣传导向，保障消费者合法权益，上海市市场监管局制定发布《商业广告代言活动合规指引》，明确21项负面清单。　　据悉，《指引》将“利用发生违法犯罪、违反公序......
### [「Comptiq」2022年3月号封面公开](http://acg.178.com/202202/438376620857.html)
> 概要: 杂志「Comptiq（コンプティーク）」公开了2022年3月号的封面图，封面主要角色来自于「公主连结!Re:Dive」，本期杂志将于2月10日发售。「Comptiq（コンプティーク）」是日本角川书店发......
### [脸书Meta市值暴跌至美国第八 已被英伟达赶超](https://www.3dmgame.com/news/202202/3835307.html)
> 概要: 近期扎克伯格的日可是不好过，连续几日股价暴跌已经令投资者们信心尽失欲哭无泪，据2月9日最新消息，Facebook母公司Meta股价连续第四个交易日下滑，过去一周的巨大跌幅已使该公司市值落后于芯片制造商......
### [自选攻略对象！《五等分的花嫁》游戏第二弹6月发售](https://news.dmzj.com/article/73545.html)
> 概要: 根据春场葱原作制作的游戏《五等分的花嫁》宣布了将于6月2日发售的消息。本作对应PS4和NS平台。游戏分为数字版、通常版和限定版。
### [《大雄的宇宙小战争2021》特别PV 新宇宙冒险超精彩](https://acg.gamersky.com/news/202202/1458610.shtml)
> 概要: 哆啦A梦系列新剧场版《哆啦A梦：大雄的宇宙小战争2021》将于3月4日在日上映，官方在今天公开了一支特别预告。
### [B站回应员工猝死：扩招1000人 加强关注员工健康](https://www.3dmgame.com/news/202202/3835316.html)
> 概要: 昨晚(2月8日)B站官方微博发表长文，回应了“武汉ai审核组组长猝死”事件，“沉痛哀悼一个生命的逝去。祝愿所有人健康、平安。”B站表示猝死员工“暮色木心”作为图文审核组的代组长，即便在春节假期，内容安......
### [我为什么还是主张提高收买妇女儿童罪的刑罚？](https://www.huxiu.com/article/496549.html)
> 概要: 本文来自微信公众号：罗翔说刑法（ID：luoxiangshuoxingfa），作者：罗翔，头图来自：《盲山》剧照我一直主张提高收买被拐卖妇女、儿童罪的刑罚，实现形式上的买卖同罪同罚，这无关热点还是冰点......
### [华硕推出以RTX3080为外形的键帽 售价高达399元](https://www.3dmgame.com/news/202202/3835330.html)
> 概要: 虽然现在因为各种原因，许多玩家无法以正常价格购买到英伟达RTX 3080显卡，但这并不妨碍他们为信仰充值。近日华硕推出了一款以RTX 3080为外形的键帽：华硕ROG Strix Touchstone......
### [Facebook的“万亿崩落”，是未来巨型平台的预言](https://www.huxiu.com/article/496562.html)
> 概要: 本文来自微信公众号：极客公园（ID：geekpark），作者：靖宇，头图来自：视觉中国有眼尖的人发现，Facebook 创始人马克·扎克伯格在财报会后接受媒体专访时，眼圈红红好像哭过。扎克伯格解释说，......
### [艾斯纳奖最佳画师绘制「星球大战：欧比-旺」变体封面公开](http://acg.178.com/202202/438389513755.html)
> 概要: 近日，漫威漫画公开了星战全新5期迷你漫画系列「星球大战：欧比-旺」第一期的变体封面。此封面由美国艾斯纳漫画奖最佳封面画师Peach Momoko桃桃子女士独家绘制，登场人物是绝地武士「欧比-旺·肯诺比......
### [高桥一生柴崎幸合作日剧《无形》 一起追踪罪犯](https://ent.sina.com.cn/2022-02-09/doc-ikyamrmz9846032.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 高桥一生主演柴崎幸出演的TBS日剧《无形》将于四月开播，这是高桥一生首次主演TBS日剧，饰演为解决案件可以不择手段的刑警，与第四次合作的柴崎幸成为搭档。　　高桥一生饰演的......
### [Open Salary and Equity Database](https://topstartups.io)
> 概要: Open Salary and Equity Database
### [史上第二大比特币盗窃案告破！“雌雄大盗”落网冻结“赃款”价值36亿美元](https://www.tuicool.com/articles/eqiimiz)
> 概要: 2016年8月，在一个月黑风高的夜晚，数字货币交易所Bitfinex像往日一样平静。突然“雌雄大盗”从天而降，对交易所一通大肆洗劫，带着119754个比特币扬长而去。这宗案件成为仅次于日本“Mr Go......
### [《哆啦A梦》冷知识：外星人的语言竟然是摩斯电码](https://acg.gamersky.com/news/202202/1458655.shtml)
> 概要: 有日本网友发现，《哆啦A梦》漫画中这些“外星语言”其实是摩斯电码，分别代表着不同的日文假名，解读就能得知意思。
### [首发 9999 元，ROG 幻 16 2022 款今晚开售：i7-12700H + RTX 3060，2K 165Hz 屏](https://www.ithome.com/0/602/253.htm)
> 概要: IT之家2 月 9 日消息，2 月 10 日 0 点，ROG 幻 16 2022 款笔记本将正式开售，i7-12700H + RTX 3060 首发价 9999 元。IT之家了解到，ROG 幻 16 ......
### [软银孙正义：没料到出售 Arm 给英伟达会遭到如此强大的反对](https://www.ithome.com/0/602/260.htm)
> 概要: 据日经新闻报道，软银集团会长兼社长孙正义 8 日接受采访表示，并未料到出售 Arm 会遭到如此存在强烈的反对。孙正义称，此次原计划以最多 400 亿美元出售。其中三分之二是与英伟达换股，其估值曾提高至......
### [设计十诫](https://www.tuicool.com/articles/Z7bMfmI)
> 概要: 优秀的设计师应该有所为有所不为。文章来源：葱爷ID：congyenanian作者：葱爷编辑：卝生葱爷目前算不上是一个成功的设计师，所以没能力教大家怎么做才能成功，但好歹在一线城市的设计圈摸爬滚打了近1......
### [NetWalker勒索软件成员被判80个月监禁](https://www.tuicool.com/articles/ieAbAfn)
> 概要: 近期，加拿大男子Sebastian Vachon-Desjardins因参与NetWalker勒索攻击，被判处6年零八个月监禁。涉案法官表示，尽管Desjardins到案后有帮助受害者追回损失并配合调......
### [卡普空擅自使用摄影师素材案和解！原告已经提出撤诉申请](https://news.dmzj.com/article/73550.html)
> 概要: 在约半年前，摄影师Judy A. Juracek因为游戏公司卡普空在游戏中，擅自使用了自己拍摄的照片素材而将卡普空告上了法院。目前，这起案件有了新进展。双方达成了和解，不过双方都没有对和解金等情况进行说明。
### [视频：谷爱凌刘昊然同框旧照引热议 两人曾一起合作拍广告](https://video.sina.com.cn/p/ent/2022-02-09/detail-ikyamrmz9867870.d.html)
> 概要: 视频：谷爱凌刘昊然同框旧照引热议 两人曾一起合作拍广告
### [星巴克使用过期食材：最终被罚137万](https://www.3dmgame.com/news/202202/3835354.html)
> 概要: 2月9日消息，天眼查显示，近日上海星巴克咖啡经营有限公司无锡震泽路店、无锡昌兴大厦店因通过篡改、撤换、撕毁调制的食品原料保质期标签等方式。使用过期食品原料、经营过期食品、擦拭食品接触面的毛巾未能实现专......
### [10岁娃10天花9800元买奥特曼卡片 商家：家长没管好](https://www.3dmgame.com/news/202202/3835355.html)
> 概要: 奥特曼卡片是很多孩子的“心头好”，今日，一则10岁娃10天花9800元买奥特曼卡片的消息登上微博热搜，引发网友热议。据@都市报道 官微消息，2月7日，河南驻马店段女士10岁的儿子10天花了9800元买......
### [央视报道短视频侵权《长津湖》等，呼吁多方配合保护影视版权](https://www.ithome.com/0/602/289.htm)
> 概要: 2 月 9 日消息，2 月 8 日及 9 日，央视中文国际频道《今日环球》、财经频道《经济信息联播》《正点财经》节目分别以“短视频侵权事件频发”“整治短视频侵权”“关注短视频侵权新规”为题，从热门电影......
### [组图：电影《无名》剧照曝光 梁朝伟王一博黑白色调下氛围感拉满](http://slide.ent.sina.com.cn/film/slide_4_86512_366231.html)
> 概要: 组图：电影《无名》剧照曝光 梁朝伟王一博黑白色调下氛围感拉满
### [推特 Twitter 测试视频倍速播放功能，支持 0.25 ~ 2 倍速调节](https://www.ithome.com/0/602/305.htm)
> 概要: IT之家2 月 9 日消息，目前，大多数 Twitter 推特用户只能创建 140 秒的视频，官方还没有放开限制的打算。不过，推特即将支持视频倍速播放。推特官方宣布，部分安卓和网页端用户正在测试视频倍......
### [一个私募掌门人的“不告而别”](https://www.huxiu.com/article/496701.html)
> 概要: 本文来自微信公众号：资事堂（ID：Fund2019），作者：郑孝杰，编辑：袁畅，题图来自：视觉中国2022年1月10日深夜，环懿基金（私募）的实控人、基金经理高杉在滨江外滩夜跑后失踪未归。近一个月后，......
### [The urine revolution: how recycling pee could help to save the world](https://www.nature.com/articles/d41586-022-00338-6)
> 概要: The urine revolution: how recycling pee could help to save the world
### [从0到1剖析并编码实现短链系统](https://www.tuicool.com/articles/ia2iQzz)
> 概要: 短链很常见，在互联网营销场景以及移动端信息传播等场景下起着重要的作用。同时，也是经常被来拿考察选手系统设计水平的一个场景。对于服务端研发，关于前端访问时的长短转换，其实只要知道有30X重定向基本也就可......
### [欠发达的粤北一县引才何以“硕博扎堆”？和平县：人才资金大增](https://finance.sina.com.cn/china/dfjj/2022-02-09/doc-ikyamrmz9900662.shtml)
> 概要: 澎湃新闻记者 林珏瑶 近日，广东河源和平县面向国内外引进82名各类高学历人才，吸引了包括31名全日制博士研究生、700多名全日制硕士研究生在内的810多人报名。
### [新规！个人存取现金超5万，要登记！跟普通人有啥关系？刚刚，央行回应](https://finance.sina.com.cn/jjxw/2022-02-09/doc-ikyamrmz9901322.shtml)
> 概要: “个人存取现金超5万元要登记”！近日，一则消息在网友的热议中成为焦点，多次登上微博热搜。 自2022年3月1日起，央行等三部门发布的《金融机构客户尽职调查和客户身份资料及...
### [European researchers achieve fusion energy record](https://www.euro-fusion.org/news/2022/european-researchers-achieve-fusion-energy-record/)
> 概要: European researchers achieve fusion energy record
### [工业企业数据传递乐观信号，2021年四季度收入和利润增速企稳](https://finance.sina.com.cn/roll/2022-02-09/doc-ikyamrmz9902561.shtml)
> 概要: 2月9日，人民银行调查统计司公布对5000户工业企业财务状况的调查数据显示，2021年四季度，工业企业收入和利润增速企稳。应收款增速提升的同时账期缩短...
### [官宣了！这类人工资不低于当地公务员→](https://finance.sina.com.cn/wm/2022-02-09/doc-ikyakumy5076053.shtml)
> 概要: 来源：央视财经 教育部2月8日发布2022年工作要点，共涉及6大类35个要点，其中落实教育优先发展战略地位、深入推进“双减”和推进义务教育优质均衡发展等成为大家关注的焦点。...
### [个人存取现金超5万要登记，对你影响有多大？央行最新回应来了！](https://finance.sina.com.cn/china/2022-02-09/doc-ikyakumy5075999.shtml)
> 概要: 个人存取现金超5万元需登记资金来源的消息近日登上微博热搜。2月9日，人民银行有关司局负责人表示，个人现金存取相关规定不会影响居民正常现金存取款业务...
### [海贼王1040话：粉丝希望尾田让反派获胜，大妈的魅力超越罗、基德](https://new.qq.com/omn/20220209/20220209A0BHWQ00.html)
> 概要: 大家好，我是小蜘蛛。随着海贼王和之国剧情的不断深入，基本上现在的战斗算是已经板上钉钉了，两大四皇算是在和之国彻底折戟沉沙。本来应该算是一个皆大欢喜的场面，但是奈何如今看来很多的观众都是十分的不买账的，......
### [上海市委副书记、市长龚正：上海今年要加大力度夯实税基、厚植税源 决不征收“过头税”](https://finance.sina.com.cn/china/gncj/2022-02-09/doc-ikyakumy5078054.shtml)
> 概要: 上证报中国证券网讯（记者 宋薇萍）上海市财政审计税务工作会议2月9日召开。上海市委副书记、市长龚正表示，要坚持稳字当头、稳中求进，坚持系统观念，增强工作的系统性...
# 小说
### [树枝扎根时代](http://book.zongheng.com/book/1153032.html)
> 作者：北回志

> 标签：历史军事

> 简介：特殊一代人从天真到成熟，奋斗出一个无悔人生。

> 章节末：作者的话

> 状态：完本
# 论文
### [POCO: Point Convolution for Surface Reconstruction | Papers With Code](https://paperswithcode.com/paper/poco-point-convolution-for-surface)
> 日期：5 Jan 2022

> 标签：None

> 代码：None

> 描述：Implicit neural networks have been successfully used for surface reconstruction from point clouds. However, many of them face scalability issues as they encode the isosurface function of a whole object or scene into a single latent vector.
### [Multi-Stage Episodic Control for Strategic Exploration in Text Games | Papers With Code](https://paperswithcode.com/paper/multi-stage-episodic-control-for-strategic-1)
> 日期：4 Jan 2022

> 标签：None

> 代码：None

> 描述：Text adventure games present unique challenges to reinforcement learning methods due to their combinatorially large action spaces and sparse rewards. The interplay of these two factors is particularly demanding because large action spaces require extensive exploration, while sparse rewards provide limited feedback.
