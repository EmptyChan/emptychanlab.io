---
title: 2020-12-08-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RoccaCalascio_EN-CN7367341484_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-08 21:29:37
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RoccaCalascio_EN-CN7367341484_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：张艺兴《扫黑风暴》新路透曝光 穿警服梳大背头正气满满](http://slide.ent.sina.com.cn/tv/slide_4_704_349426.html)
> 概要: 组图：张艺兴《扫黑风暴》新路透曝光 穿警服梳大背头正气满满
### [小S秀恩爱网友又提家暴 罕见严肃回应](https://ent.sina.com.cn/s/h/2020-12-08/doc-iiznezxs5754680.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，小S（徐熙娣）与许雅钧结婚多年来鲜少晒恩爱，但她6日突然在社交网站说：“不了解我人生的人，我平常懒得解释！但我老公是世界上最爱我、最疼我、心中只有我的男人！”未料，事后引来......
### [组图：某编剧控诉王思聪公司拖欠版权费 下跪索要未果](http://slide.ent.sina.com.cn/tv/slide_4_704_349419.html)
> 概要: 组图：某编剧控诉王思聪公司拖欠版权费 下跪索要未果
### [香蕉影业CEO韦翔东回应王思聪拖欠版权费](https://ent.sina.com.cn/s/m/2020-12-08/doc-iiznezxs5873284.shtml)
> 概要: 12月7日，编剧刘晓峰发文控诉王思聪香蕉影业，称其拖欠奖金及版权费用。刘晓峰在2019年凭借剧本《天才编剧》获得了香蕉影业举办的“第一届香蕉新编剧圆梦计划”比赛二等奖，并获得80万元奖金。但香蕉影业在......
### [组图：李嘉欣爬山锻炼自在又惬意 素颜自拍肌肤光泽很细腻](http://slide.ent.sina.com.cn/star/slide_4_704_349417.html)
> 概要: 组图：李嘉欣爬山锻炼自在又惬意 素颜自拍肌肤光泽很细腻
### [单日播放量“吓人”的剧，《大秦赋》4040万才第3，榜首断层实至名归！](https://new.qq.com/omn/20201208/20201208A0EY2000.html)
> 概要: NO.5《燕云台》日播量：2512万《燕云台》是唐嫣孕后复出的首部电视剧，很多观众都非常喜欢唐嫣在剧中的古装扮相。不得不说，《燕云台》是一部非常值得追的古装剧，全员演技在线加上相当精彩的剧情，让不少观......
### [“疫情下”收视却跌入低谷，《儿科医生》为何没能吃到“医疗剧”红利？](https://new.qq.com/omn/20201208/20201208A0G05C00.html)
> 概要: 年初的一场“疫情”席卷全球，也彻底将国内外医疗剧题材激活。据不完全统计，今年国内市场陆续播出、加“翻红”的医疗剧多达6部，这个数字是往年医疗剧上新数量的三倍有余。乘着风口，原本以为聚焦“儿科医生”视角......
### [应采儿陈小春盼儿子上清华，7岁儿子却不识中文，被嘲：浪费名额](https://new.qq.com/omn/20201208/20201208A0ADH800.html)
> 概要: 应采儿陈小春盼儿子上清华，7岁儿子却不识中文，被嘲：浪费名额应采儿儿子转学上海考清华，7岁还不认识中文，被网友吐槽占名额12月7日，据媒体报道近日陈小春应采儿夫妻曾出席内地某商业活动现场，同时还有陆毅......
### [《鹿鼎记》后新《神雕》和新《天龙》也被嘲了，选角一部比一部差](https://new.qq.com/omn/20201208/20201208A0GEER00.html)
> 概要: 今年的翻拍剧对于观众们十分不友好，对于原作者和原著更是连尊重都没剩下多少。张爱玲的《半生缘》生生被拍成了老年版，原本的悲剧内核被演员们生生演绎出了一种荒诞的喜剧感。            在《情深缘起......
### [黄圣依带货能力差？收10万费用仅卖出5件商品，还被指迟到半小时](https://new.qq.com/omn/20201208/20201208A0GC9600.html)
> 概要: 12月8日上午，据媒体报道，近日有商家在网络上爆料黄圣依的带货能力太差，直播半个小时的时间坑位费就高达10万元，但是当天仅仅只卖出了五个保温杯。            并且商家还爆料黄圣依在直播的时候......
# 动漫
### [GSC《咒术回战》五条悟粘土人开始预订](https://news.dmzj.com/article/69500.html)
> 概要: GSC根据《咒术回战》中的角色五条悟制作的粘土人目前已经开订的。本作带有“通常颜”、“认真颜”和战斗时的“发火颜”，另外配有摘下眼罩的“放下头发”零件，可以配合“墨镜”、“喜久福”零件，重现多种居中场景。
### [高桥弘《热血高校》迎来连载30周年！](https://news.dmzj.com/article/69502.html)
> 概要: 《热血高校》迎来连载30周年，漫画《KIKU》新装版，《热血高校》写真集一并发售。
### [个人制作动画《便携笔记战士》正式版公开](https://news.dmzj.com/article/69501.html)
> 概要: 推特网友denntisinn个人使用3DS应用《便携笔记3D》制作的动画《便携笔记战士》公开了正式版本。在本作中，讲述了主人公们作为“便携笔记战士”与强大敌人战斗的故事。
### [TV动画《碧蓝航线 微速前行》新PV](https://news.dmzj.com/article/69495.html)
> 概要: TV动画《碧蓝航线 微速前行》宣布了将于2021年1月11日在TOKYO MX、BS11开始播出的消息，新的PV也一并公开。在这次公开的PV中，可以看到主人公们游泳、上课等日常生活。
### [一人之下：米二起名有多“鬼才”，楚岚是个谐音梗，冯宝宝原型是太监？](https://new.qq.com/omn/20201208/20201208A04UHV00.html)
> 概要: 《一人之下》原名《异人》是由漫画家创造的人气漫画，并已经进行了完结了三季的制作，目前第四季也在紧锣密鼓的制作中。米二作为一个非常考究的漫画作者，为了更好的完成这部作品，曾经踏遍各大道观只为还原最真实……
### [民间恐怖故事漫画之马疯子外传](https://new.qq.com/omn/20201208/20201208A049V800.html)
> 概要: --END--
### [《火影忍者》晓组织实力排行榜，大蛇丸仅第四，第一名因为打女人领了盒饭](https://new.qq.com/omn/20201208/20201208A09I8L00.html)
> 概要: Top11：绝绝是大筒木辉夜的意志产物，主要就是收集情报，也没上过真正的战场，只能作为辅助。Top10：小南她的侦查能力十分强大，但是打架这种东西，女孩子确实不太需要拥有这种能力，不过能加入到晓组织……
### [开播拿到9.9分！史诗级动画《进击的巨人》播出第4季](https://new.qq.com/omn/20201208/20201208A00XGG00.html)
> 概要: 过年了！过年了！史诗级的作品《进击的巨人》第4季在千呼万唤中开播了！《进击的巨人》讲述了一个关于“墙内外末世人生”的奇幻故事，所架构的世界观里，渴望自由一直是人类的最原始本能，然而幸存的人类因墙外的……
### [无能娜娜：新的遇害者出现了，只不过凶手却不是娜娜](https://new.qq.com/omn/20201208/20201208A0GFUG00.html)
> 概要: 对于娜娜来说，橘慎学长的出现毫无疑问是一个重要的转折点。在此之前，她光是应对京谷就已经很勉强了，更不用说又新增了一位智力不属于自己的对手。从两人的接触中就能看出，娜娜几乎是全程都处于被动的状态。虽说屡......
### [《进击的巨人》最终季第一集剧透大结局，法尔克成为关键人物！](https://new.qq.com/omn/20201208/20201208A0G2G000.html)
> 概要: 对于很多漫画来说，“动画组剧透”已经是个老生常谈的话题，毕竟动画是有声音有色彩的，观众们常常能抓住其中的细节，推测出连漫画都未公布的内容。当然《进击的巨人》也是如此，单是那几个OP就剧透了不知多少信息......
### [剧场版《银魂 THE FINAL》新预告 特典联动鬼灭之刃](https://acg.gamersky.com/news/202012/1343885.shtml)
> 概要: 《银魂》全新剧场版动画《银魂 THE FINAL》，即将于2021年1月8日在日本上映，官方在12月8日公开了本作的最新预告，大量全新的画面公开。
### [网飞《变形金刚》动画第二章中文预告 12月30日开播](https://acg.gamersky.com/news/202012/1343927.shtml)
> 概要: 网飞动画《变形金刚：塞伯坦之战三部曲》第二章《地球崛起》公开了中文正式预告，擎天柱带领幸存的博派对抗名为“佣兵”的独立变形金刚组织，还要与威震天搏命。
### [动画《碧蓝航线：微速前行》PV 小可爱的轻松日常](https://acg.gamersky.com/news/202012/1343956.shtml)
> 概要: 《碧蓝航线》TV动画《碧蓝航线：微速前行》公开了本作的最新PV，一群小可爱带来全新的画面。《碧蓝航线：微速前行》将于2021年1月11日开播。
# 财经
### [重庆一境外输入病例出院后在机场检测时复阳](https://finance.sina.com.cn/china/dfjj/2020-12-08/doc-iiznctke5479099.shtml)
> 概要: 原标题：重庆一境外输入病例出院后在机场检测时复阳 据江津区新型冠状病毒感染的肺炎疫情防控工作领导小组办公室最新消息，今年9月...
### [外交部副部长郑泽光召见美国驻华使馆临时代办](https://finance.sina.com.cn/china/gncj/2020-12-08/doc-iiznezxs5889377.shtml)
> 概要: 原标题：外交部副部长郑泽光召见美国驻华使馆临时代办 12月8日，中国外交部副部长郑泽光召见美国驻华使馆临时代办傅德恩，就美国务院宣称将制裁中国全国人大常委会14位副委...
### [南京楼市迎“开盘潮” 价格平稳认购火爆](https://finance.sina.com.cn/china/dfjj/2020-12-08/doc-iiznctke5475497.shtml)
> 概要: 原标题：买1套房约等于赚100万！“万人摇”与“零登记盘”共存 经历过金九银十的传统旺季，临近年底，多地楼市迎来开盘潮。在南京、杭州等地，也不乏有“千人摇”的红盘出现...
### [安徽金寨女副县长蔡黎丽邀请昭苏贺娇龙：骑上你的天马来打卡](https://finance.sina.com.cn/china/dfjj/2020-12-08/doc-iiznctke5475131.shtml)
> 概要: 原标题：安徽金寨女副县长蔡黎丽邀请昭苏贺娇龙：骑上你的天马来打卡 新疆伊犁哈萨克自治州昭苏县副县长贺娇龙因策马雪原引起广泛关注，澎湃新闻注意到...
### [深圳全球招商狂揽超7800亿元项目 先行示范区利好效应释放](https://finance.sina.com.cn/china/dfjj/2020-12-08/doc-iiznezxs5896280.shtml)
> 概要: 原标题：深圳全球招商狂揽超7800亿元项目，先行示范区利好效应释放 12月8日下午，深圳举办了2020年度全球招商大会，洽谈签约项目涉及投资总额超7800亿元。
### [财经TOP10|李克强：今年中国经济有把握实现正增长](https://finance.sina.com.cn/china/caijingtop10/2020-12-08/doc-iiznezxs5899460.shtml)
> 概要: 【宏观要闻】 NO.1 李克强：今年中国经济有把握实现正增长 国务院总理李克强12月8日上午在人民大会堂视频会见国际货币基金组织总裁格奥尔基耶娃。
# 科技
### [视频处理，让video活灵活现](https://segmentfault.com/a/1190000038413078)
> 概要: Web 开发者们一直以来想在 Web 中使用音频和视频，但早些时候，传统的 Web 技术不能够在 Web 中嵌入音频和视频，所以一些像 Flash、Silverlight 的专利技术在处理这些内容上变......
### [JVM垃圾回收器及算法原理](https://segmentfault.com/a/1190000038406091)
> 概要: 有情怀，有干货，微信搜索【三太子敖丙】关注这个不一样的程序员。本文GitHubhttps://github.com/JavaFamily已收录，有一线大厂面试完整考点、资料以及我的系列文章。前言大家在......
### [华为 MateStation B515 商用台式机正式发布：高配 AMD 锐龙 7 4700G 处理器，23.8 英寸护眼屏，多屏协同](https://www.ithome.com/0/523/594.htm)
> 概要: IT之家 12 月 8 日消息 华为正式发布 HUAWEI MateStation B515 商用台式机，采用小机箱设计，搭载了新一代 8 核 7nm AMD 锐龙处理 AMD 4000G 系列，集成......
### [79 元半年，学生新用户专享 1 核 2G 百度云服务器](https://www.ithome.com/0/523/601.htm)
> 概要: IT之家12月8日消息  百度智能云近期开启了年终狂欢大促，学生新用户选购云服务器，低至 74 元 / 半年。针对学生用户，百度智能云推出入门型云服务器和普及型云服务器，其中入门型 1 核 2G ，半......
### [Cemetery of Soviet Computers](https://rusue.com/cemetery-of-soviet-computers/)
> 概要: Cemetery of Soviet Computers
### [The Man Who Found Forrest Fenn's Treasure](https://www.outsideonline.com/2419429/forrest-fenn-treasure-jack-stuef)
> 概要: The Man Who Found Forrest Fenn's Treasure
### [中国互联网人冲向美股](https://www.huxiu.com/article/398567.html)
> 概要: 本文来自微信公众号：硅星人（ID：guixingren123），作者：An，编辑：Vicky Xiao，原标题《隔着时差“拼杀”，中国互联网人冲向美股》，头图来自：视觉中国“你有看到NIO昨天涨了多少......
### [54起，85%在中国：资本为何都涌进了化妆品行业？](https://www.huxiu.com/article/398626.html)
> 概要: 本文来自微信公众号：青眼（ID：qingyanwh），作者：建安，原文标题：《54起！今年这些资本最爱投化妆品》，题图来自：视觉中国昨日，服务资生堂、毛戈平等品牌的新型广告营销机构瑟尚完成数千万元A轮......
### [排查指南 | 当 mPaaS 小程序提示“应用更新错误（1001）”时](https://www.tuicool.com/articles/mQzmUbY)
> 概要: 问题描述APP 启动 mPaaS 小程序弹出 toast 信息："应用更新错误"。原因分析调用MDS小程序更新接口之后，没有拉到对应的小程序信息，就会返回1001。mPaaS 框架在打开一个小程序应用......
### [彭博：阿里巴巴折戟欧洲 真的想全球化吗？](https://www.tuicool.com/articles/Jr2MFjF)
> 概要: 阿里巴巴集团今年的“双十一”销售额达到750亿美元，堪称火爆。但是在阿里寻求扩张的欧洲，双十一受到冷落。彭博社发文称，阿里对欧洲电商和云计算市场寄予厚望，但是进展十分有限。面对亚马逊、微软等已在欧洲耕......
# 小说
### [贞观闲王](http://book.zongheng.com/book/609483.html)
> 作者：盛世天下

> 标签：历史军事

> 简介：坐一次飞机，竟然会遇到超级气流，意外穿越到大唐！既来之则安之，那就做个悠闲的穿越者吧。酿酿酒，做美食，抄抄书，优哉游哉的大唐生活。

> 章节末：第四百四十七章 大唐行天下

> 状态：完本
# 游戏
### [方块游戏喜加一 RPG游戏《新剑侠传奇》免费领取](https://www.3dmgame.com/news/202012/3803643.html)
> 概要: 方块游戏平台又给大家送福利了！玩家们可以在该平台免费领取国产单机RPG游戏《新剑侠传奇》。玩家只需要登陆方块账号，就能免费领取游戏。领取地址：点击进入《新剑侠传奇》是由珠海云游科技有限公司研发的一款武......
### [TGA 2020玩家之声票选：《对马岛之鬼》大优势获胜](https://www.3dmgame.com/news/202012/3803684.html)
> 概要: TGA 2020“玩家之声（PLAYER'S VOICE）”奖项的投票通道现已正式关闭，所以尽管官方还没有正式发布最终结果，但我们已经可以从投票页面的占比数，确认TGA 2020“玩家之声”的奖项获得......
### [英国罪犯从车上盗窃PS5事件频发 今年有27起类似案件](https://www.3dmgame.com/news/202012/3803642.html)
> 概要: 之前，在英国就曾经发生PS5的盗窃、抢劫事件，但是英国甚至有犯罪分子在卡车运输PS5的时候从后面偷走这些抢手的次世代主机抢走，而今年已经发生了27起。据英国《泰晤士报》报道，今年英国发生了至少27起盗......
### [前亚洲第1美女克拉拉、包贝尔新片《大红包》定档](https://www.3dmgame.com/news/202012/3803675.html)
> 概要: 由李克龙执导，包贝尔、克拉拉等主演的喜剧电影《大红包》发布定档预告，将于2021年1月29日上映。讲述包贝尔被职场欺压被女友甩，还因为人情债随了几十万红包，好兄弟出谋划策，为回收份子钱出馊主意策划假婚......
### [为啥N卡和A卡都缺货？因为GDDR6显存的供应不足](https://www.3dmgame.com/news/202012/3803698.html)
> 概要: 在NVIDIA发布新的RTX 30系列显卡，AMD也发布了RX 6000系列显卡之后，正常来说双方粉丝都免不了一番嘴炮，但今年的规模则意外的小，因为两边都没货，现在这情况买得到新显卡就很不错了。根据c......
# 论文
### [Connecting Embeddings for Knowledge Graph Entity Typing](https://paperswithcode.com/paper/connecting-embeddings-for-knowledge-graph-1)
> 日期：ACL 2020

> 标签：ENTITY TYPING

> 代码：https://github.com/Adam1679/ConnectE

> 描述：Knowledge graph (KG) entity typing aims at inferring possible missing entity type instances in KG, which is a very significant but still under-explored subtask of knowledge graph completion. In this paper, we propose a novel approach for KG entity typing which is trained by jointly utilizing local typing knowledge from existing entity type assertions and global triple knowledge from KGs.
### [UnifiedQA: Crossing Format Boundaries With a Single QA System](https://paperswithcode.com/paper/unifiedqa-crossing-format-boundaries-with-a)
> 日期：2 May 2020

> 标签：LANGUAGE MODELLING

> 代码：https://github.com/allenai/unifiedqa

> 描述：Question answering (QA) tasks have been posed using a variety of formats, such as extractive span selection, multiple choice, etc. This has led to format-specialized models, and even to an implicit division in the QA community.
