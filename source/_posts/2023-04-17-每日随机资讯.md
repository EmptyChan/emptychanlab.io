---
title: 2023-04-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MinouLighthouse_ZH-CN7940024247_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-17 22:34:04
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MinouLighthouse_ZH-CN7940024247_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [引导学生“硬刚”GPT，海外教师花式应对“作弊神器”](https://www.woshipm.com/ai/5807754.html)
> 概要: 在ChatGPT等AI大模型应用出现之后，教育行业的从业者们曾有过担心，因为学生们可能会借助GPT进行论文“作弊”。不过现在，教育界人士也开始找到GPT的正面用途与更多作用途径了。一起来看看作者的分析......
### [大模型涌现前，巨头涌入AI广告](https://www.woshipm.com/marketing/5807525.html)
> 概要: 市场上有关生成式AI的讨论已经越来越多，不少互联网企业、乃至互联网大佬也在加码布局AI大模型的应用开发。那么，AI技术还会在哪些方面打开我们的想象空间？或许AI营销是一个方面。一起来看看本文对AI营销......
### [阿里在欧洲再造了一个“天猫+小红书”](https://www.woshipm.com/it/5807496.html)
> 概要: 电商赛道上的玩家竞争愈发激烈，不少大厂也在海外陆续上线新的电商平台。其中，阿里就在去年12月底上线了Miravia，这个电商平台，目前已在西班牙市场取得了不错的成绩。那么，Miravia的发展和运营路......
### [马斯克关闭推特免费API，美国天气预报和地铁等公共服务帐号瘫了](https://finance.sina.com.cn/stock/usstock/c/2023-04-17/doc-imyqrvus6736428.shtml)
> 概要: 马斯克关闭推特免费API，美国天气预报和地铁等公共服务帐号瘫了
### [月薪过万，沉迷“捡破烂”的深圳人](https://www.huxiu.com/article/1203364.html)
> 概要: 本文来自微信公众号：深圳微时光 （ID：szdays），作者：Murtius，原文标题：《月薪过万，沉迷“捡破烂”的深圳人：方法找对，全屋家电都能免费》，头图来自：视觉中国“求洗衣机+衣柜+桌子+椅子......
### [翻车？《海贼王》真人剧集内部试映后反响不佳](https://www.3dmgame.com/news/202304/3867223.html)
> 概要: 网飞《海贼王》真人版剧集在首次试映后受到了打击。据好莱坞知名新闻媒体推特账号Divinity Seeker，观众对这部备受期待的电视剧的私下揭幕反应不佳，主要原因是糟糕的CGI质量和令人困惑的情节。网......
### [降价战又来了！余承东的“车企消失论”要提早应验了？|次世代车研所](https://finance.sina.com.cn/tech/it/2023-04-17/doc-imyqrvup7121189.shtml)
> 概要: 降价战又来了！余承东的“车企消失论”要提早应验了？|次世代车研所
### [揭秘OpenAI“红军”：聘请专家攻击ChatGPT 解决技术伦理问题](https://finance.sina.com.cn/tech/internet/2023-04-17/doc-imyqrvuu9155162.shtml)
> 概要: 揭秘OpenAI“红军”：聘请专家攻击ChatGPT 解决技术伦理问题
### [溜溜哥“造车”：年轻人的“入门级大玩具”是如何诞生的？](https://36kr.com/p/2214784065090178)
> 概要: 溜溜哥“造车”：年轻人的“入门级大玩具”是如何诞生的？-36氪
### [赋能数字体育，天博sports携手格子军团开启数字时代新征程](http://www.investorscn.com/2023/04/17/106895/)
> 概要: 随着互联网时代的快速发展，数字体育与传统体育的关系也愈发紧密，但以什么样的形式将二者进行有机融合，是体育行业和传统体育内容行业共同面临的难题......
### [尚智×久美惠｜法兰克福香肠系列包装｜食品包装设计](https://www.zcool.com.cn/work/ZNjQ5MTYzMzY=.html)
> 概要: Collect......
### [一图读懂贝特瑞2022年度报告](http://www.investorscn.com/2023/04/17/106898/)
> 概要: 一图读懂贝特瑞2022年度报告
### [解决“司机内卷”，才是提升运价的关键](http://www.investorscn.com/2023/04/17/106900/)
> 概要: 一提起现在的低运价，货车司机们可谓是怨声载道。跑一趟去除油费、过路费所剩无几，这日子还怎么过？那么低运价是如何形成的呢？中国物流与采购联合会近日发布的《2022年货车司机从业状况调查报告》（简称《报告......
### [PALO-028咖啡品牌全案设计/VI设计/包装设计/空间设计](https://www.zcool.com.cn/work/ZNjQ5MTc4MjA=.html)
> 概要: 项目名称丨PALO-028咖啡品牌全案设计项目地址丨成 都项目时间丨2023-2-26设计团队丨ONEUIN万有引力品牌设计-品牌指导BRAND GUIDANCE：大强品牌规划BRAND PLAN：大......
### [周杰伦起诉网易不正当竞争 今日法院开庭](https://ent.sina.com.cn/y/ygangtai/2023-04-17/doc-imyqsaas9159346.shtml)
> 概要: 新浪娱乐讯  4月17日，周杰伦起诉网易不正当竞争案在杭州市滨江区人民法院开庭。据公开资料，腾讯曾于2019年起诉网易侵权周杰伦音乐，法院判决网易云音乐赔偿原告合理开支共计85万元......
### [47届讲谈社漫画赏最终15部入围作品发表 4月第三周新闻汇总](https://news.idmzj.com/article/77772.html)
> 概要: 本周新闻汇总带来第47届讲谈社漫画赏最终15部入围作品发表、日本举办首届AI画展、 《间谍过家家》获日本漫画家协会奖漫画部门大奖、《火影忍者》角色人气投票结果公布等消息。
### [《弱势角色友崎君》第二季公布先导PV](https://news.idmzj.com/article/77774.html)
> 概要: TV动画《弱势角色友崎君》第二季公布了先导视觉图及PV。
### [人类历史上最可悲的世界首富](https://www.huxiu.com/article/1208639.html)
> 概要: 本文来自微信公众号：巨潮商业评论（ID：tide-biz），作者：杨旭然，头图来自：视觉中国LV老板阿诺特的财富数字，在不久前超过了马斯克，首次成为世界首富。这可能是历年世界首富名单中最特别的一个。其......
### [第三篇 | AOAO嗷嗷的一直恐龙](https://www.zcool.com.cn/work/ZNjQ5MTkwNDQ=.html)
> 概要: AOAO是怪兽公园的第六只怪，它是一只组合怪，设计的主题是恐龙，怪兽里怎么能没有恐龙呢？于是我们就设计了一只奇怪的恐龙。AOAO由四部分组成，两个可互换驾驶员，两段身体，四部分分别都长了脸，可以组合摆......
### [作品整理|曾经非常喜欢用华丽而悲戚的红色](https://www.zcool.com.cn/work/ZNjQ5MTkwODA=.html)
> 概要: 整理一波以前画的一些红色系的插画有小说封面插图、影视剧海报、也有随笔而画痴迷一些绚烂华丽却又悲怆浓烈的红......
### [《火影终极风暴羁绊》鸣人佐助新形态演示 2023发售](https://www.3dmgame.com/news/202304/3867250.html)
> 概要: 《火影忍者 终极风暴羁绊》发布了“重粒子模式”漩涡鸣人和“支撑之影”宇智波佐助的实机演示视频，该作将于2023年发售，登陆PS5、PS4、Xbox Series X|S、Xbox One、Switch......
### [数理逻辑解码生命奇门遁甲，正本清源易见中华文运百年](http://www.investorscn.com/2023/04/17/106904/)
> 概要: 易经作为中华文化瑰宝，一直吸引着众多爱好者，也在“大浪淘沙始见金”的过程中，涌现了一批有着深刻认识和独到见解的学者。其中，周正易正是代表之一......
### [将ChatGPT超能力融入全屋智能，博联智能开启智能家居行业无限想象！](http://www.investorscn.com/2023/04/17/106908/)
> 概要: 随着ChatGPT火爆全球,医疗、教育、金融、娱乐等各个领域掀起应用浪潮,新一代AI技术方向带来了应用场景的拓宽和延展。BroadLink博联作为智能家居行业领军企业率先实现全屋智能和ChatGPT的......
### [香港电影“青黄不接”的最大原因，在于敬老和顽固](https://www.huxiu.com/article/1210864.html)
> 概要: 本文来自微信公众号：Ifeng电影（ID：movie-bigbang），作者：二十二岛主，原文标题：《迈入新一个十年的香港金像奖：依然顽固，依然“青黄不接”》，题图来自：视觉中国在一片哗然中，第41届......
### [世嘉宣布以7.762亿美元收购《愤怒的小鸟》开发商](https://www.3dmgame.com/news/202304/3867258.html)
> 概要: 4月17日，世嘉正式宣布以7.762亿美元（7.06亿欧元）收购《愤怒的小鸟》开发商Rovio Entertainment，世嘉以每股9.25欧元和每股期权1.48欧元收购Rovio所有已发行股票和期......
### [海外new things | 针对外科医生的财富管理平台「Forme Financial」A轮融资1200万美元，发布正式版产品“Earned”](https://36kr.com/p/2209209865450882)
> 概要: 海外new things | 针对外科医生的财富管理平台「Forme Financial」A轮融资1200万美元，发布正式版产品“Earned”-36氪
### [姜丹尼尔公司举报恶意攻击者 称其已收到法律处罚](https://ent.sina.com.cn/y/yrihan/2023-04-17/doc-imyqssyf1541019.shtml)
> 概要: 新浪娱乐讯 韩国艺人姜丹尼尔的经纪公司今天宣布已有恶意攻击姜丹尼尔的网民受到法律处罚。　　姜丹尼尔的经纪公司今天宣布，公司长期监控网上对姜丹尼尔的恶意攻击并主动搜集相关证据，此前已向司法机关举报了一批......
### [YG否认朴彩英与姜栋元恋情 曾被曝同框项链传绯闻](https://ent.sina.com.cn/y/yrihan/2023-04-17/doc-imyqssyf1548803.shtml)
> 概要: 新浪娱乐讯 4月17日下午，YG娱乐公司表示，“BLACKPINK成员Rosé朴彩英（26岁）和演员姜栋元（42岁）的恋爱传闻不是事实。”　　ROSÉ 所属公司YG娱乐17日发表官方立场“之前说过不能......
### [世界最强“星舰”即将登场，马斯克能否改写太空探索？](https://www.huxiu.com/article/1212353.html)
> 概要: 本文来自微信公众号：凤凰网科技（ID：ifeng_tech），综合整理：箫雨，题图来自：视觉中国北京时间4月17日消息，美国东部时间周一9点（北京时间周一21点），SpaceX的“星际飞船”将迎来它的......
### [剧场版动画《Collar×Malice deep cover》PV公开](https://news.idmzj.com/article/77777.html)
> 概要: 剧场版动画《Collar×Malice -deep cover-》公开初预告！视觉图也一并公开。
### [今晚调价！国内成品油价将迎年内最大涨幅，下轮调价或将继续上涨](https://finance.sina.com.cn/jjxw/2023-04-17/doc-imyqssyf1578605.shtml)
> 概要: 今晚（4月17日）24时将迎来今年的第八轮调价窗口期。 据大宗商品资讯机构金联创测算，截至4月17日，即本调价周期的最后一个工作日，参考原油品种均价为82.84美元/桶...
### [10余个省市开启密集调研，有哪些关键词值得被关注？](https://finance.sina.com.cn/china/2023-04-17/doc-imyqssyk9094629.shtml)
> 概要: 来源：财联社 财联社4月17日讯（记者 郭松峤）近日，北京、天津、安徽、湖南等10余个省市开启实地调研，考察领域各有侧重。据财联社观察...
### [海外New Things |利用AI保障在线交易安全，「Oscilar」自筹2000万美元](https://36kr.com/p/2219260858544771)
> 概要: 海外New Things |利用AI保障在线交易安全，「Oscilar」自筹2000万美元-36氪
### [RTX 4060/Ti严重缩水：除了显存 还有一点没法看](https://www.3dmgame.com/news/202304/3867269.html)
> 概要: RTX 4070之后，NVIDIA新卡的下一站自然是更主流的RTX 4060 Ti、RTX 4060。此前有消息称，它俩将在5月份发布，目前看也有可能会在5月底的台北电脑展上。TechPowerUp显......
### [淄博发布规范经营者价格行为提醒告诫书：不得虚假标价、模糊标价](https://finance.sina.com.cn/china/2023-04-17/doc-imyqssyc6667577.shtml)
> 概要: 来源：淄博日报 春暖花开好时节，旅游旺季随之而来，宾馆住宿、饭店餐饮、交通运输等行业需求量大幅增加。为营造我市良好消费环境，进一步规范市场价格秩序...
### [4月17日这些公告有看头](https://www.yicai.com/news/101733120.html)
> 概要: 4月17日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [CP29本子画师周边参展 我推视频遭某站批量下架](https://news.idmzj.com/article/77779.html)
> 概要: CP29大量本子画师周边参展、我推的孩子视频遭某站下架
### [已推出掌上超声硬件产品，「医银」布局AI超声诊断云平台](https://36kr.com/p/2219314193674884)
> 概要: 已推出掌上超声硬件产品，「医银」布局AI超声诊断云平台-36氪
### [湖北再出“30条”稳经济，重金补贴亚洲首个货运机场开拓新航线](https://www.yicai.com/news/101733143.html)
> 概要: 年内力争累计开通4～5条国际货运航线。
### [酒吧模拟器《Brewpub Simulator》公布新预告，年内发售、支持中文](https://www.ithome.com/0/687/027.htm)
> 概要: 感谢IT之家网友手写的从前的线索投递！IT之家4 月 17 日消息，酒吧模拟器《Brewpub Simulator》公开了新宣传片，预计 2023 年发售，支持简体中文，目前在 Steam 平台有试玩......
### [沪指创出年内新高，下一目标位前高3424？](https://www.yicai.com/video/101733166.html)
> 概要: 4月17日，上证指数单边上行创年内新高。市场二八切换明显，低估蓝筹发力能否力助指数后市再创新高？
### [3699 元，一加 Ace 2 原神定制礼盒发布：提供多种收藏级限定周边](https://www.ithome.com/0/687/034.htm)
> 概要: 感谢IT之家网友偏科骚黄4100只眼、软媒新友1933769、肖战割割、华南吴彦祖的线索投递！IT之家4 月 17 日消息，一加 Ace 2原神定制礼盒现已发布，18GB+512GB 版本售价 369......
### [国内电池厂开工率有望5月回升，碳酸锂二季度或触10万元/吨后反弹](https://finance.sina.com.cn/jjxw/2023-04-17/doc-imyqsxhc1553297.shtml)
> 概要: 锂电池关键材料碳酸锂价格暴跌不止，已经引发了业内人士对行业发展的担忧。 4月17日，澎湃新闻记者从部分电池厂商和行业人士处了解到...
### [MLF“小幅”加量续做，释放了什么信号？](https://finance.sina.com.cn/china/2023-04-17/doc-imyqsxhh9074245.shtml)
> 概要: 作者：余嘉欣 来源：金融时报 4月17日，中国人民银行发布公告称，为维护银行体系流动性合理充裕，当日开展1700亿元中期借贷便利（MLF）操作和200亿元公开市场逆回购操作...
### [【IT之家开箱】一加Ace 2原神定制礼盒开箱，厨王+灶神满满都是红火的璃月国风味](https://www.ithome.com/0/687/039.htm)
> 概要: 不知大家是否还记得，今年2月1日一加官宣与《原神》达成三年战略合作，未来的联动已在有序落实推进，这不第三款一加与原神的联动新品来了！IT之家已经拿到了这款一加Ace 2原神定制礼盒，接下来就给大家带来......
### [静音按键，前行者 Q5 无线充电鼠标 29 元（减 30 元）](https://lapin.ithome.com/html/digi/687046.htm)
> 概要: 静音按键，前行者 Q5 无线充电鼠标 日常售价 59 元，今日可领 30 元新品冲量券，实付 29 元包邮。天猫静音按键，前行者 Q5 无线充电鼠标券后 29 元领 30 元券▼ 京东官方旗舰店同款报......
### [本周指数能否站稳3400点？](https://www.yicai.com/video/101733248.html)
> 概要: 本周指数能否站稳3400点？
### [苹果在印度年销售额达到60亿美元 同比增长50%](https://www.3dmgame.com/news/202304/3867271.html)
> 概要: 据报道，截至今年3月底，苹果过去一年在印度的销售额达到近60亿美元，创历史新高。知情人士称，截至3月底，苹果过去一年在印度的营收约为60亿美元，较上年同期的41亿美元增长近50%。相比之下，苹果之前已......
### [国家级都市圈再扩容：东北“破0”，南强北弱局面能否改变](https://www.yicai.com/news/101733293.html)
> 概要: 在明确沈阳为第九个国家级都市圈之后，第八个国家级都市圈是谁，引发各界猜测，其中对于是郑州都市圈的猜测比较多。
# 小说
### [我才不是学院派](http://book.zongheng.com/book/949092.html)
> 作者：密云星

> 标签：奇幻玄幻

> 简介：当别人产生快乐这种情绪且与我有关的时候，我就获得了可以换取变强的筹码。然而这个代价也是有的——我失去了快乐的能力。当别人产生悲伤这种情绪时，我就有了可以兑换万物的货币。然而也是同样的有代价——我失去了悲伤的能力。当别人。。。。每当觉醒一种情绪，叶封都变得更加的强大。然而对应的情绪就会逐渐的消失。也不知这种奇怪的代价是好是坏。。。“情绪喜收集成功，情绪值加一。”这是一个游离在体系之外的人。这是一个强大到溢出人类标准的人。这是一个痛苦于自己的强大的人。

> 章节末：第一百一十七章 破局人

> 状态：完本
# 论文
### [Unified Functional Hashing in Automatic Machine Learning | Papers With Code](https://paperswithcode.com/paper/unified-functional-hashing-in-automatic)
> 日期：10 Feb 2023

> 标签：None

> 代码：https://github.com/google-research/unified_functional_hashing

> 描述：The field of Automatic Machine Learning (AutoML) has recently attained impressive results, including the discovery of state-of-the-art machine learning solutions, such as neural image classifiers. This is often done by applying an evolutionary search method, which samples multiple candidate solutions from a large space and evaluates the quality of each candidate through a long training process. As a result, the search tends to be slow. In this paper, we show that large efficiency gains can be obtained by employing a fast unified functional hash, especially through the functional equivalence caching technique, which we also present. The central idea is to detect by hashing when the search method produces equivalent candidates, which occurs very frequently, and this way avoid their costly re-evaluation. Our hash is "functional" in that it identifies equivalent candidates even if they were represented or coded differently, and it is "unified" in that the same algorithm can hash arbitrary representations; e.g. compute graphs, imperative code, or lambda functions. As evidence, we show dramatic improvements on multiple AutoML domains, including neural architecture search and algorithm discovery. Finally, we consider the effect of hash collisions, evaluation noise, and search distribution through empirical analysis. Altogether, we hope this paper may serve as a guide to hashing techniques in AutoML.
### [Directed Graph Auto-Encoders | Papers With Code](https://paperswithcode.com/paper/directed-graph-auto-encoders)
> 概要: We introduce a new class of auto-encoders for directed graphs, motivated by a direct extension of the Weisfeiler-Leman algorithm to pairs of node labels. The proposed model learns pairs of interpretable latent representations for the nodes of directed graphs, and uses parameterized graph convolutional network (GCN) layers for its encoder and an asymmetric inner product decoder. Parameters in the encoder control the weighting of representations exchanged between neighboring nodes. We demonstrate the ability of the proposed model to learn meaningful latent embeddings and achieve superior performance on the directed link prediction task on several popular network datasets.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
