---
title: 2021-08-22-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.OlympicCoast_ZH-CN0827844876_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-08-22 21:27:17
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.OlympicCoast_ZH-CN0827844876_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [OpenSSH 8.7 发布](https://www.oschina.net/news/156720/openssh-8-7-released)
> 概要: OpenSSH 8.7 已发布，OpenSSH 是 100％ 完整的 SSH 协议 2.0 版本的实现，并且包括 sftp 客户端和服务器支持，它用于远程登录的主要连接工具。OpenSSH 对所有流量......
### [KDE Plasma 5.23 将引入了新的概览效果](https://www.oschina.net/news/156721/kde-plasma-introduce-a-new-overview)
> 概要: 本周的 KDE Plasma 开发进展已经发布，其中值得关注的一点是基于 QML 的新概览效果已合并。目前，该概览效果会向用户显示所有打开的窗口，就像现有的 “当前窗口” 效果一样，并且不会使非活动窗......
### [Go 编译器已默认启用 -G=3，支持泛型](https://www.oschina.net/news/156729/generics-enabled-by-default-in-go-tip)
> 概要: Go 项目代码仓库昨日提交和合并的一个 PR显示，Go 语言已在 cmd/compile 中默认启用 -G=3。根据描述，此 PR 将 cmd/compile 的 -G flag 的默认值从 0 改为......
### [中国系统加入龙蜥社区，共建国际领先开源社区](https://www.oschina.net/news/156730)
> 概要: 近日，中国系统正式加入OpenAnolis（龙蜥）社区，与行业生态伙伴共建OpenAnolis社区，共同打造国际领先的开源操作系统。中国电子系统技术有限公司（CESTC ，简称中国系统）旗下中国电子云......
### [Strict Tables – Column type constraints in SQLite - Draft](https://www.sqlite.org/draft/stricttables.html)
> 概要: Strict Tables – Column type constraints in SQLite - Draft
### [个人信息保护法，拉开“反杀熟”大幕](https://www.huxiu.com/article/450266.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自视觉中国特此感谢庄帆律师对本文的贡献。2018年~2021年，在不少人经历了半夜起床发现智能音箱处于“唤醒录音状态”，为N个app、门禁和闸门贡献了自己的人脸与......
### [组图:贺峻霖晒妈妈深夜秀美食照 和网友云分享称有福同享有难同当](http://slide.ent.sina.com.cn/y/slide_4_704_360622.html)
> 概要: 组图:贺峻霖晒妈妈深夜秀美食照 和网友云分享称有福同享有难同当
### [Police helicopter's 100mph chase with 'super sophisticated' drone near Tucson](https://www.thedrive.com/the-war-zone/42021/radio-transmissions-from-police-helicopters-chase-of-bizarre-craft-over-tucson-add-to-mystery)
> 概要: Police helicopter's 100mph chase with 'super sophisticated' drone near Tucson
### [组图：谢娜二胎产后复工路透曝光 穿着宽松戴帽子口罩包裹严实](http://slide.ent.sina.com.cn/z/v/slide_4_704_360625.html)
> 概要: 组图：谢娜二胎产后复工路透曝光 穿着宽松戴帽子口罩包裹严实
### [组图：惊艳！央视9位主持人古装亮相 一秒变装大秀国风之美](http://slide.ent.sina.com.cn/z/v/slide_4_704_360626.html)
> 概要: 组图：惊艳！央视9位主持人古装亮相 一秒变装大秀国风之美
### [猛男专属超萌动漫女生头像](https://new.qq.com/omn/20210811/20210811A0ENIF00.html)
> 概要: 所有图片均来自于网络各处......
### [「ani・ani」第8期封面公开](http://acg.178.com/202108/423595860960.html)
> 概要: 动画「我让最想被拥抱的男人给威胁了。」的衍生刊物「ani・ani」公开了最新第8期的封面图，本次封面绘制的是西条高人。「我让最想被拥抱的男人给威胁了。」是日本漫画家樱日梯子创作的漫画作品，讲述了连续5......
### [7月Dapp行业概述：热度升温，每日约140多万独立用户](https://www.tuicool.com/articles/Aj22qu7)
> 概要: 2021年7月再次证明了去中心化应用行业令人兴奋且快速的发展步伐。玩赚游戏的兴起，以及高价值非同质化代币（NFT）收藏的建立，推动了整个行业的发展。然而，去中心化金融（DeFi）的竞争仍在继续升温，部......
### [漫画《全职猎人》连载休刊已超过1000天](https://news.dmzj.com/article/71957.html)
> 概要: 由富㭴义博创作的日本少年漫画作品《全职猎人》距离上一次更新，休刊已超过1000天。
### [声优日高里菜确诊感染新冠病毒 ​​​​](https://news.dmzj.com/article/71958.html)
> 概要: 据日媒消息日本声优日高里菜确诊感染新冠病毒，这一消息也得到了日高里菜所在事务所确认。
### [《雷神之锤》复刻版发布实体版本 但并无Xbox版](https://www.3dmgame.com/news/202108/3821796.html)
> 概要: 微软和Bethesda近日正式公布了《雷神之锤》的复刻版本，且直接面向主机、PC和云平台的Xbox游戏通行证发布。不过，Xbox和PC平台的实体版爱好者可能就有点不开心了，Limited Run为《雷......
### [《豆子煞星》游戏版公布 连剧情都会变的roguelike](https://www.3dmgame.com/news/202108/3821798.html)
> 概要: 2009年上映的动画片《永远的豆子煞星》于2018年在油管平台上传了4K完整版，随即再度掀起网络热潮。截至目前，这部电影在油管上的播放量已经达到3058万。而豆子煞星工作室也利用了这一热度，即将推出一......
### [TRIGGER扳机社十周年纪念视频 官方超燃混剪](https://news.dmzj.com/article/71959.html)
> 概要: 今天（8月22日）TRIGGER扳机社发布了十周年纪念超燃混剪视频，一起来看看吧。
### [中止上市，比亚迪半导体的无妄之灾](https://www.huxiu.com/article/450288.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔8月21日消息，创业板发行上市审核信息公开网站显示，深交所已于8月18日中止比亚迪半导体股份有限公司发行上市审核。原因是发行人律师北京市天元律师事务所被中国证监会立......
### [《喋血街头：脑损伤》十月发布试玩 最新预告片公布](https://www.3dmgame.com/news/202108/3821800.html)
> 概要: 一样，计划复制经典FPS游戏比如《毁灭战士》和《永远的毁灭公爵》这样的游戏机制。《喋血街头》初代游戏开发工作室Running With Scissors将与波兰厂商Hyperstrange合作开发新作......
### [ClariS单曲「ケアレス」MV短片公开](http://acg.178.com/202108/423602597707.html)
> 概要: 近日，动漫歌手组合 ClariS公开了单曲「ケアレス」的MV短片。该曲同为剧场版动画「魔法纪录 魔法少女小圆外传 第二季 -觉醒前夜-」的OP主题曲，同名专辑将于9月15日发售。「ケアレス」MV短片......
### [「月刊Afternoon」10月号封面公开](http://acg.178.com/202108/423603071186.html)
> 概要: 漫画杂志「月刊Afternoon」10月号封面图公开，封面主要角色来自于漫画「Q、恋ってなんですか?」。「Q、恋ってなんですか?」是Fiok Lee创作的漫画作品，讲述了不擅长与人交流的公司职员青井和......
### [苹果VSEpic邮件：Tim Cook计划在Mac发更多3A作品](https://www.3dmgame.com/news/202108/3821801.html)
> 概要: 苹果与Epic两家之间官司于今年5月正式结束，但案件本身尚未结案，法官在审查4500 多页证词后尚未作出裁决。著名游戏媒体The Verge根据案件中涉及的电子邮件发现了相关公司的新料，我们从材料中选......
### [第一！科大讯飞再度刷新Cityscapes世界纪录](https://www.tuicool.com/articles/NFfmEnQ)
> 概要: 近日，科大讯飞凭借在道路目标检测领域多年的技术探索，刷新了Cityscapes 3D目标检测任务的全球最好成绩，得到检测分数（DS）42.9，取得了该项评测的第一名。此次也是科大讯飞继2017年、20......
### [漫画「樱井同学想被注意到」第3卷封面公开](http://acg.178.com/202108/423609425314.html)
> 概要: 漫画「樱井同学想被注意到」公开了第3卷的封面图，该卷将于8月27日正式发售。「樱井同学想被注意到」（桜井さんは気づいてほしい）是あきのそら创作的漫画作品，讲述了想要被注意到的女主角·樱井同学，总是做一......
### [小米决定解决部分手机的接近传感器问题](https://www.ithome.com/0/570/787.htm)
> 概要: IT之家8 月 22 日消息 众所周知，一款手机中配备了大量传感器，例如重力传感器、红外传感器、指纹传感器、霍尔传感器、气压计、陀螺仪、指南针、环境光传感器、接近光传感器、Camera 激光对焦传感器......
### [EXO成员XIUMIN新冠痊愈 新音乐剧9月7日再启](https://ent.sina.com.cn/y/yrihan/2021-08-22/doc-ikqcfncc4317865.shtml)
> 概要: 新浪娱乐讯 8月22日，据韩媒，SM公司当天下午宣布EXO成员XIUMIN金珉锡新冠痊愈。另外，由他参演的音乐剧《Hadestown》中出现了包括XIUMIN在内的23名确诊患者，因此开幕日期被推迟......
### [TRIGGER发布10周年纪念混剪 请继续突破天际吧！](https://acg.gamersky.com/news/202108/1417507.shtml)
> 概要: TRIGGER发布10周年纪念混剪，感谢大家的支持，扳机社未来还会继续发射动画的子弹。
### [谷歌 Android 12 新特性：Android Auto 驾驶功能将被谷歌助理的驾驶模式取代](https://www.ithome.com/0/570/796.htm)
> 概要: IT之家8 月 22 日消息 据外媒 GSMArena 报道，谷歌Android 12操作系统的 beta 测试人员报告称，手机上原有的AndroidAuto 驾驶功能将被谷歌语音助理的驾驶模式取代......
### [初任 leader，如何管理 5 人小团队？](https://www.huxiu.com/article/450313.html)
> 概要: 本文来自微信公众号：有瞰学社（ID：gh_a43f58185aee），作者：陶程，题图来自视觉中国先说明一下，虽然这篇是写团队管理，但我自己并没有大团队管理经验。这篇文章我主要是结合自己过去在管理小团......
### [Xbox 负责人谈《雷神之锤》：该作品改变了游戏行业的发展](https://www.ithome.com/0/570/800.htm)
> 概要: IT之家8 月 22 日消息  在近日举办的 Quakecon 2021 活动上，微软 Xbox 负责人的 Phil Spencer 谈到了《雷神之锤》，以及该游戏对整个行业产生的积极影响。Phil ......
### [视频：央视收视密码又多一位？主持人马凡舒直拍被夸女团爱豆](https://video.sina.com.cn/p/ent/2021-08-22/detail-ikqcfncc4333176.d.html)
> 概要: 视频：央视收视密码又多一位？主持人马凡舒直拍被夸女团爱豆
### [未来三天这些地方还有强降雨：本轮暴雨从何来？如何应对？气象专家解读](https://finance.sina.com.cn/wm/2021-08-22/doc-ikqcfncc4338979.shtml)
> 概要: 未来三天，这些地方还有强降雨！河南红色预警！两地启动Ⅰ级响应！本轮暴雨从何来？如何应对？气象专家解读→ 来源：国际金融报 8月22日，中央气象台继续发布暴雨橙色预警...
### [5元就能代发一条笔记 起底小红书医美利益链](https://finance.sina.com.cn/china/2021-08-22/doc-ikqciyzm2948028.shtml)
> 概要: 5元就能代发一条笔记，起底小红书医美利益链 小红书上的医美笔记究竟有多少是真实的？这家意欲打造产品体验分享社区的平台是否尽到了审核责任？
### [海南影视业井喷：一年狂开3000家公司 拍摄基地排期已到明年](https://finance.sina.com.cn/chanjing/2021-08-22/doc-ikqciyzm2950089.shtml)
> 概要: 来源：时代周报 眼下海南的影视产业热闹得不可思议 8月正午的海南，烈日洒在椰树上。超过35°的高温之下，街道上行人寥寥无几。 坐落于海口市郊区的冯小刚电影公社却不同...
### [最新：郑州四地降级 全国现有高中风险区10+63个](https://finance.sina.com.cn/china/2021-08-22/doc-ikqciyzm2951183.shtml)
> 概要: 郑州发布消息，自2021年8月22日18时起，将管城回族区航海东路街道金盾未来花园、金水区南阳新村街道开元小区、金水区丰庆路街道中鼎花园...
### [招聘数据显蛛丝马迹，苹果正“密谋增兵”家居产品](https://www.ithome.com/0/570/814.htm)
> 概要: 尽管苹果口风很严，但招聘数据“泄露”了不少秘密。近日外媒报道称，从招聘数据来看，苹果正在开发一系列新的家居产品和服务，同时也为iPhone 13发布前，可能激增的零售客户做准备。根据 GlobalDa......
### [疫苗接种多久后免疫功能下降？钟南山最新研判来了](https://finance.sina.com.cn/wm/2021-08-22/doc-ikqcfncc4348934.shtml)
> 概要: 来源：国是直通车 继续苗苗苗 扬州新增本土确诊病例连续7天为个位数，上海浦东机场再现疫情。8月20、8月21日两天，上海新增本土确诊病例5例。
### [张家港行政执法用上“区块链+公证”全过程记录](https://www.btc126.com//view/183207.html)
> 概要: 8月22日消息，为有效提高综合行政执法的公开性、透明性，张家港市锦丰镇应用“区块链+公证”行政执法全过程记录，为规范行政执法再添“安全锁”。据介绍，“区块链+公证”行政执法全过程记录项目主要涉及安全生......
### [复盘本轮疫情多地暴发始末，防控结果为何天壤之别？](https://finance.sina.com.cn/china/gncj/2021-08-22/doc-ikqcfncc4357027.shtml)
> 概要: 原标题：复盘本轮疫情多地暴发始末，防控结果为何天壤之别？ 防控结果的迥异情况，照出了防控政策的“行之”有效与“不行之”则无效的背后原因。
### [Hydrogen lobbyist quits, slams oil companies’ “false claims” about blue hydrogen](https://arstechnica.com/tech-policy/2021/08/ex-lobbyist-slams-blue-hydrogen-says-it-would-lock-in-fossil-fuel-dependence/)
> 概要: Hydrogen lobbyist quits, slams oil companies’ “false claims” about blue hydrogen
### [Lowkey (YC S18) is hiring a Senior Software Engineer who likes games](https://lowkeygg.notion.site/Lowkey-Job-Board-39a26c1b4a00493fadc26249185df748)
> 概要: Lowkey (YC S18) is hiring a Senior Software Engineer who likes games
### [安全公司：谷歌从Play Store中下架8款伪装成加密货币云挖矿平台的恶意APP](https://www.btc126.com//view/183210.html)
> 概要: 根据网络安全公司Trend Micro的一份报告，谷歌已经从其Play Store中下架了8个伪装成加密货币云挖矿应用的恶意移动应用程序。根据该报告，这些恶意应用欺骗受害者观看广告，为订阅服务平均每月......
### [2D回合制策略游戏《Stones Keeper》上架Steam](https://www.3dmgame.com/news/202108/3821823.html)
> 概要: 2D回合制策略游戏《Stones Keeper》上架了Steam，支持英文和俄语，不支持中文。在游戏中玩家要控制自己的飞行城堡，加入到尖叫狮鹫骑士团对抗入侵你的不死势力的战役中。试着去了解你周围世界的......
### [一图流蓝Buff：什么都不如家庭重要，溜了溜了](https://bbs.hupu.com/44890651.html)
> 概要: 一图流蓝Buff：什么都不如家庭重要，溜了溜了
### [马龙教学乒乓球发球，三种方法手到擒来，网友：你以为你能教会我？](https://new.qq.com/omn/20210822/20210822V09RD500.html)
> 概要: 马龙教学乒乓球发球，三种方法手到擒来，网友：你以为你能教会我？
### [很庆幸，大张伟活成了“大张伟”，而不是周星驰](https://new.qq.com/omn/20210822/20210822A09SU100.html)
> 概要: 2011年，大张伟做客《今夜有戏》。当郭德纲问到择偶标准时，向来嘻哈的大张伟有些认真地列出三条：“就是白胖，跟年画娃娃一样喜庆。”“然后得理解我，懂我脆弱的那种。”“最好比我大3岁，抱金砖嘛。”   ......
### [张雨绮否认男友吃软饭，称节目组恶意剪辑宣布退出，被指太恋爱脑](https://new.qq.com/omn/20210822/20210822A09UQ400.html)
> 概要: 随着综艺《女儿们的恋爱4》热播，张雨绮男友李柄熹被许多网友指责“吃软饭”，引起不小的争议。            8月22日，张雨绮发文称自己只是以前辈的身份给出建议，并不是所谓的“他伸手，我清醒”，......
### [临危受命的Junjia能否帮助EDG完成让二追三的壮举？](https://bbs.hupu.com/44891006.html)
> 概要: junjia也算是上来拼命的啊，要把edg从悬崖边拉回来。不知道能不能像当年bengi一样连翻三盘，如果这样那确实是壮举了。
### [34岁张雨绮被小男友磨得没脾气：当年有多彪悍，如今就有多温柔](https://new.qq.com/omn/20210822/20210822A09Y4600.html)
> 概要: 清话本小说《八洞天》将怕老婆归为三类：势怕、理怕、情怕。“势怕”有三：一是畏妻之贵，仰其伐阅；二是畏妻之富，资其财贿；三是畏妻之悍,避其打骂。“理怕”又有三：一是敬妻之贤,景其淑范；二是服妻之才,钦其......
### [董事长悬赏千万，要把女CEO送进监狱](https://www.tuicool.com/articles/RJf6nmy)
> 概要: 作者：李亦儒来源：商业人物（ID：biz-leaders）如果心理学家想要找一个草根企业家作为案例分析，多益网络董事长徐波可能是非常好的样本。他初中辍学，现在是身价280亿元的游戏公司董事长。他发公开......
### [我们生活在两个世界：地图不是疆域，言辞也不是事实](https://www.tuicool.com/articles/mAreMbJ)
> 概要: 暴雨之后的郑州，在老家房顶上远眺，周围鸦雀无声。一个词代表的往往不是一个意思，而是一个意义区间，比如「语言」这个词，并不仅仅代表我们对外说话。每天早上醒来，其实就有大量的「语言」充斥着我们的大脑 ——......
### [【直播】微笑：也还好其实就当BO1打呗，输了也还有复活甲，有实力还能打回来](https://bbs.hupu.com/44891430.html)
> 概要: 【直播】微笑：也还好其实就当BO1打呗，输了也还有复活甲，有实力还能打回来
### [数据：BTC已实现价格突破2万美元，创历史新高](https://www.btc126.com//view/183214.html)
> 概要: Glassnode数据显示，BTC已实现价格（Realized Price）达到20,397.66美元，创历史新高......
### [印度数字钱包公司PhonePe获7亿美元融资 腾讯参投](https://www.btc126.com//view/183215.html)
> 概要: 8月22日消息，印度电子支付及数字钱包公司PhonePe再次获得了3.5亿美元的融资。除Flipkart母公司沃尔玛领投之外，腾讯、老虎环球也参与了投资，至此，新一轮7亿美元融资计划完成。本次3.5亿......
### [赛后重庆QG 4-1 武汉ES，Fly马超惊天偷家，重庆QG挺进总决赛](https://bbs.hupu.com/44891529.html)
> 概要: 虎扑08月22日讯 Game5：重庆QGhappy蓝色方，武汉eStarPro红色方【Ban/Pick】【高光时刻】又是熟悉的暴君团，重庆QG技高一筹赢下二换四团战中路爆发大乱战，重庆QG再次打出一换
# 小说
### [大业宏图：1954年的中国](http://book.zongheng.com/book/880994.html)
> 作者：刘国新

> 标签：历史军事

> 简介：研究国史是神圣的事业，一定要投入真感情。也就是说，不仅仅要把研究国史看成是一项工作，有科学严谨的研究方法和研究态度，更要把研究国史看成是一份神圣的事业，一份值得投入精力、倾注感情的事业。有了这份深厚的感情，才能有研究的动力和出发点，也才能取得经得住时间检验的科研成果。《读点国史：辉煌年代国史丛书》由一批国史研究领域的专家担纲撰写，他们有专业背景，曾承担过国家级重大课题，也都有个人的研究著述，形成学风严谨、功力扎实的品格。我相信这套丛书是他们用心写就的。

> 章节末：结语

> 状态：完本
# 论文
### [AGKD-BML: Defense Against Adversarial Attack by Attention Guided Knowledge Distillation and Bi-directional Metric Learning | Papers With Code](https://paperswithcode.com/paper/agkd-bml-defense-against-adversarial-attack)
> 日期：13 Aug 2021

> 标签：None

> 代码：https://github.com/hongw579/agkd-bml

> 描述：While deep neural networks have shown impressive performance in many tasks, they are fragile to carefully designed adversarial attacks. We propose a novel adversarial training-based model by Attention Guided Knowledge Distillation and Bi-directional Metric Learning (AGKD-BML).
### [Full-Duplex Strategy for Video Object Segmentation | Papers With Code](https://paperswithcode.com/paper/full-duplex-strategy-for-video-object)
> 日期：6 Aug 2021

> 标签：None

> 代码：https://github.com/GewelsJI/FSNet

> 描述：Appearance and motion are two important sources of information in video object segmentation (VOS). Previous methods mainly focus on using simplex solutions, lowering the upper bound of feature collaboration among and across these two cues.
