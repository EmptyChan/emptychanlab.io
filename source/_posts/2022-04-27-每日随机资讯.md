---
title: 2022-04-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SvalbardSun_ZH-CN6108396467_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-04-27 22:23:18
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SvalbardSun_ZH-CN6108396467_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [谷歌向 CNCF 捐赠 Istio](https://www.oschina.net/news/193192/google-istio-cncf)
> 概要: 在向 CNCF 捐赠 Knative后，谷歌和 Istio 指导委员会又宣布已将 Istio 项目提交给 CNCF，供其考虑作为一个孵化项目。“这对 Istio 及其社区来说是一个重要的里程碑，我们很......
### [Wine 项目尝试切换至 GitLab](https://www.oschina.net/news/193189/wine-gitlab-experiment)
> 概要: Wine 项目负责人 Alexandre Julliard 表示，其团队为 Wine 的开发工作配置了一个 GitLab 实例，目前尚处于试验阶段。据称，此举旨在优化开发者在 Wine 上的工作流程......
### [谷歌“数据安全”，要求开发者公开其应用收集的数据信息](https://www.oschina.net/news/193193/google-data-safety)
> 概要: 据外媒techdator介绍，为了让用户做出明智的决定，谷歌正在其应用商店 Play Store 中添加一个新的数据安全板块，在该板块中，开发人员需要展示他们的应用程序收集了哪些数据信息、与谁共享数据......
### [Windows 11 的“上帝模式”可以查看所有设置选项](https://www.oschina.net/news/193196/windows-11-god-mode)
> 概要: Windows 11 有一个名为“上帝模式”的控制面板，这是一个基于文件资源管理器的高级配置页面，可以访问所有设置项，包含高级工具、功能和任务。上帝模式的设置项视用户具体使用情况而定，笔者的系统有 2......
### [速览WETV流媒体出海产品](http://www.woshipm.com/evaluating/5153606.html)
> 概要: 编辑导语：随着各种智能设备的新增和互联网的高速发展，OTT行业也在不断发展中，本篇文章作者分析了WETV流媒体出海产品的相关内容，从项目背景到产品分析、产品体验都有详细的分析思考，一起来学习一下吧。一......
### [疫情下的电商大考](http://www.woshipm.com/it/5413689.html)
> 概要: 编辑导语：近三年来，各大行业都受到了疫情的影响，电商行业也不例外，本篇文章作者分享了疫情之下电商平台的应对策略，讲述了电商平台以及互联网企业在疫情下的反应等，一起来看一下吧。4月24日，周日，是五一假......
### [如何验证产品与市场匹配度？](http://www.woshipm.com/pd/5413660.html)
> 概要: 编辑导语：PMF指的是产品市场契合度，意味着在一个良好的市场中拥有能够满足该市场的产品。这是无数产品经理在完成MVP之后，希望进一步达成的第二个目标。这篇文章通过三个方面（找到PMF、判断产品与市场到......
### [Show HN: Baseten – Build ML-powered applications](https://www.baseten.co/)
> 概要: Show HN: Baseten – Build ML-powered applications
### [2020“上海设计10x10”全球大奖展](https://www.gogoup.com/study?courseId=GMjU0OA==&festId=16337?utm_source=zcool&utm_campaign=feed_01&utm_content=2548&utm_term=zb)
> 概要: 2020“上海设计10x10”全球大奖展
### [动画「约会大作战」第四季第四弹PV公开](http://acg.178.com/202204/445022917713.html)
> 概要: 电视动画「约会大作战」第四季公开了第四弹PV，该作现正在播出中。「约会大作战」第四季第四弹PVMUSIC片头曲：富田美憂「OverR」片尾曲：sweetARMS「S.O.S」CAST五河士道：島崎信長......
### [《高达 库库鲁斯·多安的岛》正式预告 男人的终极之战](https://acg.gamersky.com/news/202204/1478757.shtml)
> 概要: 剧场动画《机动战士高达：库库鲁斯·多安的岛》正式预告公开，本作将于6月3日上映。
### [《吸血鬼:避世血族血猎》新预告 即将登录PS5和PC](https://www.3dmgame.com/news/202204/3841206.html)
> 概要: 《吸血鬼：避世血族血猎》发布了新视觉概念预告片和叙事预告片，该作是一款免费大逃杀游戏，背景设置在布拉格，玩家将沉浸在吸血鬼部族之间的残酷战争中。运用你的超自然力量、武器和智慧追捕并智胜你的对手，主宰夜......
### [漫画「化物语」第17卷普通版&特装版封面公开](http://acg.178.com/202204/445025431197.html)
> 概要: 近日，漫画「化物语」官方公开了由作者大暮维人绘制的第17卷普通版&特装版封面图。漫画「化物语」改编自日本轻小说家西尾维新原作的「物语系列」轻小说的第一部「化物语」，由大暮维人负责作画，讲述了在现代日本......
### [Google Scanned Objects: A High-Quality Dataset of 3D Scanned Household Items](https://arxiv.org/abs/2204.11918)
> 概要: Google Scanned Objects: A High-Quality Dataset of 3D Scanned Household Items
### [「间谍过家家」第三话纪念头像公开](http://acg.178.com/202204/445025614469.html)
> 概要: 电视动画「间谍过家家」公开了第三话的纪念头像，动画第三话已于4月23日播出。动画「间谍过家家」改编自远藤达哉创作的漫画作品，由WIT STUDIO和CLOVERWORKS负责制作，于2022年4月9日......
### [游戏改编动画《圣剑传说》预告公开 2022年开播](https://acg.gamersky.com/news/202204/1478808.shtml)
> 概要: 游戏改编动画《圣剑传说：玛娜传奇 -The Teardrop Crystal-》公开了预告、视觉图等详情，动画将于2022年年内开播。
### [动作冒险游戏《粘液》Steam版4月29日发售 支持中文](https://www.3dmgame.com/news/202204/3841221.html)
> 概要: 3D动作冒险游戏《粘液》Steam版即将于4月29日发售，支持中文，此前该作已登录了WindowsPC商城，Xbox One，Xbox Series X|S，并加入了Game Pass。Steam商店......
### [「魔鬼恋人」月浪辛官方生日贺图公开](http://acg.178.com/202204/445029139254.html)
> 概要: 今日（4月27日）是「魔鬼恋人」月浪辛的生日，官方为其公开了2022年生日贺图。月浪辛是电视动画「DIABOLIK LOVERS MORE,BLOOD」中的人物。是始祖幸存的两兄弟中的次男，作为始祖有......
### [陈坤白宇全阵容亮相！三国古装谍战悬疑剧《风起陇西》终极预告](https://new.qq.com/rain/a/20220427V03Z8700)
> 概要: 陈坤白宇全阵容亮相！三国古装谍战悬疑剧《风起陇西》终极预告
### [豆瓣：“用户注册需提供身份证号码”报道不实 平台不存储人脸信息](https://finance.sina.com.cn/tech/2022-04-27/doc-imcwipii6732573.shtml)
> 概要: 新浪科技讯 4月27日上午消息，豆瓣近日更新个人信息保护政策。有报道称“豆瓣新政策规定用户注册需实名制，需提供身份证号码、人脸信息。此外，台湾、港澳台居民若注册豆瓣用户，需要手持台湾居民来往大陆通行证......
### [80岁网红大爷怒赞刘畊宏毽子操：谁做谁健美，但也存在隐患](https://new.qq.com/rain/a/20220427V03N2900)
> 概要: 80岁网红大爷怒赞刘畊宏毽子操：谁做谁健美，但也存在隐患
### [Epic老板称NFT市场诈骗泛滥 不法分子趁机捞钱](https://www.3dmgame.com/news/202204/3841229.html)
> 概要: 近日Epic Games老板Tim Sweeney接受Fast Company采访，对目前科技和游戏领域最热门问题，即数字经济、加密货币等发表自己的看法。这位亿万富翁相信数字商品将变得流行并且利润丰厚......
### [Studio Colorido与Netflix共同制作新作动画电影](https://news.dmzj.com/article/74265.html)
> 概要: Studio Colorido宣布了和Netflix共同制作新作电影的消息。本作将由柴山智隆担任导演，预计将于2024年上映。
### [荣耀CEO赵明：网传“被离职”了 仍是荣耀终端有限公司的董事和CEO](https://finance.sina.com.cn/tech/2022-04-27/doc-imcwiwst4296546.shtml)
> 概要: 新浪科技讯 4月27日午间消息，针对荣耀CEO赵明卸任总经理的消息，27日，荣耀CEO赵明在微博上回应称：仍是荣耀终端有限公司的董事和CEO......
### [漫织·护肤详情页](https://www.zcool.com.cn/work/ZNTk0OTEzNjA=.html)
> 概要: 时隔小半年，整理了一些最近的项目，都是护肤品类的~去年年底和今年年初的东西~*最后一个松子酒非商业稿，仅做设计交流~素材来源于网络，如有侵权请联系删除~品牌方：   漫织项目产品：精华油/精华水/精华......
### [动画《机动战士高达 库库鲁斯·德安之岛》预告](https://news.dmzj.com/article/74266.html)
> 概要: 动画电影《机动战士高达 库库鲁斯·德安之岛》公开了一段预告片。在这次的预告片中，可以看到阿姆罗·雷来到无人岛等的场景。
### [报道称2K正在开发《火箭联盟》摩托车版竞品](https://www.3dmgame.com/news/202204/3841239.html)
> 概要: 据报道，2K Games 正在寻找开发者 Psyonix 的热门游戏《火箭联盟》，目前正在开发自己的足球游戏，名为《Gravity Goal》，据说这将把汽车换成类似《TRON》的摩托车。该说法来自我......
### [林奕含逝世五周年：房思琪如何影响了我们的世界？](https://www.huxiu.com/article/540807.html)
> 概要: 本文来自微信公众号：界面文化 （ID：BooksAndFun），作者：徐鲁青，编辑：黄月，题图来自：《房间》作家林奕含逝世已经五年了。2018年1月26日，林奕含写作的《房思琪的初恋乐园》简体版发售，......
### [大疆：合规风险复审期间，将暂时中止在俄乌的所有商业活动](https://finance.sina.com.cn/tech/2022-04-27/doc-imcwiwst4307493.shtml)
> 概要: 4月27日，澎湃新闻记者从大疆方面获悉，大疆正在对全球业务进行合规风险复审。在复审期间，大疆将暂时中止在俄罗斯和乌克兰的所有商业活动。大疆正在就此事与相关的客户、合作伙伴及其他相关方进行沟通。（澎湃新......
### [Shot on iPhone 13 Pro Max-20220427](https://www.zcool.com.cn/work/ZNTk0OTIxNTI=.html)
> 概要: 这是近半年因工作关系去过的地方。拍摄地包括内蒙古、哈尔滨、三亚、丽江、上海、台州、杭州、惠州、等地。均使用iPhone 13 Pro Max手机拍摄，图片较多请耐心缓冲......
### [电击大王28周年！两部新作漫画开始连载](https://news.dmzj.com/article/74267.html)
> 概要: 月刊Comic电击大王（KADOKAWA）在本日（27日）发售的6月号上，迎来了创刊28周年的纪念。本号将开始《被左迁的无能王子想隐藏实力》和《刀剑神域 Kiss and Fly》两部漫画的连载。
### [BellFine《刺猬索尼克》索尼克SoftB手办开订](https://news.dmzj.com/article/74268.html)
> 概要: BellFine根据《刺猬索尼克》中的索尼克制作的SoftB系列手办开订目前已经开订了。本作保留了SoftB系列的全高约30cm的大尺寸的特征，再现了索尼克又帅气又可爱的样子。
### [马伯庸小说改编、三国谍战剧《风起陇西》预告，4月27日奕决高下](https://new.qq.com/rain/a/20220425V0AOM300)
> 概要: 马伯庸小说改编、三国谍战剧《风起陇西》预告，4月27日奕决高下
### [《风起陇西》发布片尾曲MV：新裤子激情演绎《我不要熄灭在风中》](https://new.qq.com/rain/a/20220426V04HHT00)
> 概要: 《风起陇西》发布片尾曲MV：新裤子激情演绎《我不要熄灭在风中》
### [FFmpeg now supports JPEG XL](https://git.videolan.org/?p=ffmpeg.git;a=commit;h=0008c159562b877700cd9b7c96f941de4ee69af5)
> 概要: FFmpeg now supports JPEG XL
### [腾讯音乐公关负责人朋友圈发文回应](https://finance.sina.com.cn/tech/2022-04-27/doc-imcwiwst4314778.shtml)
> 概要: 相关新闻：网易云音乐诉腾讯音乐不正当竞争 指控后者非法盗播歌曲、批量化冒名洗歌......
### [巷子里的猫很自由 但却没有归宿](https://www.zcool.com.cn/work/ZNTk0OTU1ODg=.html)
> 概要: 巷子里的猫很自由 但却没有归宿
### [SM回应艺人威胁摩托车女司机传闻：不是朴灿烈](https://ent.sina.com.cn/s/j/2022-04-27/doc-imcwipii6771540.shtml)
> 概要: 新浪娱乐讯 据韩媒27日报道，针对EXO的朴灿烈被传涉嫌威胁驾驶摩托车女司机的消息，SM娱乐方面反驳称“不是灿烈”。　　SM娱乐对朴灿烈涉嫌威胁驾驶一事强烈反驳，称“该视频中的人物不是灿烈，车辆也与灿......
### [视频：国台办点赞刘畊宏 称其带动两岸民众健身热潮](https://video.sina.com.cn/p/ent/2022-04-27/detail-imcwiwst4330769.d.html)
> 概要: 视频：国台办点赞刘畊宏 称其带动两岸民众健身热潮
### [修炼三重境界，三只松鼠如何叩开高质量发展大门？](https://www.tuicool.com/articles/vERzYvJ)
> 概要: 修炼三重境界，三只松鼠如何叩开高质量发展大门？
### [36氪专访｜两周狂卷5000万粉丝，刘畊宏背后的推手做了什么？](https://www.tuicool.com/articles/nQRbqqz)
> 概要: 36氪专访｜两周狂卷5000万粉丝，刘畊宏背后的推手做了什么？
### [小鹏汽车回应 P7 两月全车断电两次：多次建议回店维修，愿理性协商解决](https://www.ithome.com/0/615/469.htm)
> 概要: 4 月 27 日下午消息，近日，小鹏前员工、小鹏车主在社交媒体发布标题为《致何小鹏大师兄，夏珩师兄的一封信 》的文章，称其驾驶的小鹏 P7 多次出现全车断电、车辆离线情况。对此小鹏汽车回应表示多次建议......
### [MIT researchers develop a paper thin loudspeaker](https://news.mit.edu/2022/low-power-thin-loudspeaker-0426)
> 概要: MIT researchers develop a paper thin loudspeaker
### [约六成美国人检出新冠抗体，专家为何不建议个人做抗体检测？](https://www.yicai.com/news/101395381.html)
> 概要: 有无抗体对于个人防护措施不应该有任何差别，即便有抗体，也并不意味着能够提供保护，避免再感染，仍然需要通过接种疫苗以及做好个人防护来减少再感染的风险。
### [“老实人”在中国车市活不下去](https://www.huxiu.com/article/541127.html)
> 概要: 出品丨虎嗅汽车组作者丨李文博编辑丨周到头图丨电影《让子弹飞》如果把时间拨回到半年前，应该没有人会想到，连续四年在中国单一市场销量超过150万台的日本第二大汽车制造商本田，会发布一个严谨到有些枯燥的电动......
### [14家车企上险量接近腰斩，黑色4月中这家却一枝独秀](https://www.yicai.com/news/101395557.html)
> 概要: 疫情下各大车企4月销量几乎全线遭遇“滑铁卢”。
### [金马路核按钮步步高，溧阳路买入富临精工丨龙虎榜](https://www.yicai.com/vip/news/101395572.html)
> 概要: 龙虎榜失真，源深路还是折腾房地产，而葛老大买入中国海油近1.9亿元。这些股坐等抬轿>>>
### [​创业板大涨超5% 短期能否展现弹性？](https://www.yicai.com/news/101395508.html)
> 概要: None
### [雷蛇将于2022年5月在港交所退市 或在美国重新上市](https://www.3dmgame.com/news/202204/3841266.html)
> 概要: 近日，雷蛇（Razer）在香港联合证券交易所退市迈出了重要的一步。雷蛇与要约人OUROBOROS (I) INC.联合发布声明，于2022年4月26日，批准以协议安排方式私有化建议(该计划)的决议案已......
### [物联网时代，设计师必看的智能家居科普](https://www.tuicool.com/articles/b2iEZnz)
> 概要: 物联网时代，设计师必看的智能家居科普
### [安记食品五连板后连跌四天，一季度盈利降八成](https://www.yicai.com/news/101395597.html)
> 概要: 受疫情影响，安记食品去年的海外业务营收略有下滑。
### [第六批冻猪肉收储即将启动，业内预计为本轮猪周期最后一次](https://www.yicai.com/news/101395649.html)
> 概要: 目前猪价已经出现触底反弹的迹象，农业农村部预计6月份生猪生产大概能够达到盈亏平衡点。
### [胡厚崑实录：华为只是汽车行业新人所以年销 30 万很难实现，请多一些宽容和理解](https://www.ithome.com/0/615/524.htm)
> 概要: 感谢IT之家网友来一份拌饭的线索投递！IT之家4 月 27 日消息，在第 19 届华为全球分析师大会上，华为轮值董事长胡厚崑在谈及华为终端 BG CEO 余承东此前立下汽车年销 30 万的目标时称，这......
### [马斯克买下推特后，特斯拉一夜跌去1260亿美元](https://www.huxiu.com/article/541314.html)
> 概要: 本文来自微信公众号：极客公园 （ID：geekpark），作者：周永亮，编辑：郑玄，头图来自：视觉中国马斯克最近有点“背”，按下葫芦起了瓢。4 月 26 日，特斯拉股价大幅下跌 12% 到 876.4......
### [《决胜点：网球锦标赛》2022 年 7 月 7 日发售，首日加入 XGP](https://www.ithome.com/0/615/533.htm)
> 概要: IT之家4 月 27 日消息，今天，游戏开发商 Kalypso 宣布，《决胜点：网球锦标赛（Matchpoint – Tennis Championships）》确定将于 2022 年 7 月 7 日......
### [北京石景山区1人核酸阳性 详情公布](https://finance.sina.com.cn/jjxw/2022-04-27/doc-imcwipii6824139.shtml)
> 概要: 4月27日，北京市石景山区疾控中心报告，在26日的第一轮区域核酸筛查中，发现1管“十混一”阳性。所涉及10人经复检，其中石景山区1人鉴定结果为奥密克戎变异株阳性。
### [北京石景山通报1例阳性 请涉及这些场所人员立即报告](https://finance.sina.com.cn/jjxw/2022-04-27/doc-imcwiwst4377749.shtml)
> 概要: 4月27日，石景山区疾控中心报告，在26日的第一轮区域核酸筛查中，发现1管“十混一”阳性。所涉及10人经复检，其中石景山区1人鉴定结果为奥密克戎变异株阳性。
### [北京生活物资供应情况如何？权威回应来了！网友：囤货？可以但没必要](https://finance.sina.com.cn/jjxw/2022-04-27/doc-imcwiwst4379836.shtml)
> 概要: 为防控疫情，北京部分区域提升管控措施。有居民担心，北京的肉蛋奶蔬菜水果供应能跟得上吗？ 在今天（27日）的北京疫情发布会上，相关部门介绍...
### [北京大兴区新增1例确诊病例 风险点位公布→](https://finance.sina.com.cn/jjxw/2022-04-27/doc-imcwiwst4380914.shtml)
> 概要: 据“北京大兴”消息，2022年4月27日，经对大兴区一名新冠肺炎确诊病例流调，发现如下风险点位： （总台央视记者 许梦哲）
### [税务总局：退税缓税减税降费超1万亿元](https://finance.sina.com.cn/roll/2022-04-27/doc-imcwiwst4379476.shtml)
> 概要: 第一财经从国家税务总局了解到，今年截至4月20日，税务部门累计已为企业减轻税费负担和增加现金流1万亿元以上。这些红利为企业特别是小微企业及个体工商户纾困解难...
### [联合创新推出 Mini LED 显示器 M2U：4K 分辨率 / HDR1000，首发 3499 元](https://www.ithome.com/0/615/537.htm)
> 概要: IT之家4 月 27 日消息，联合创新 INNOCN 推出了其首款 Mini LED 专业显示器--INNOCN M2U，搭载 27 寸 Mini LED 技术的 IPS 面板，3840*2160 的......
### [秦刚出席美国会代表团首次访华50周年纪念活动](https://finance.sina.com.cn/china/2022-04-27/doc-imcwiwst4382164.shtml)
> 概要: 中国驻美国大使秦刚26日以线上形式出席在北京举行的美国会代表团首次访华50周年纪念活动并致辞，他指出纪念活动的目的是以史为鉴，推动中美关系保持正确航向，行稳致远。
### [跨链协议 Celer 宣布与 StarkWare 合作推出 ZK Rollup 版的 Layer2 finance](https://www.tuicool.com/articles/iARfMnj)
> 概要: 跨链协议 Celer 宣布与 StarkWare 合作推出 ZK Rollup 版的 Layer2 finance
### [《二十二》导演郭柯新片《来日皆方长》杀青](https://ent.sina.com.cn/m/c/2022-04-27/doc-imcwipii6827701.shtml)
> 概要: 新浪娱乐讯 纪录电影《二十二》导演郭柯的新片《来日皆方长》宣布于4月26日杀青。　　据悉这是一部故事片+纪录片（《小天》）组成的电影。备案信息上显示的故事梗概：年过古稀的王叔因病进入临终阶段，但他没有......
# 小说
### [锦衣](https://m.qidian.com/book/1028116749/catalog)
> 作者：上山打老虎额

> 标签：两宋元明

> 简介：如果一个人不幸回到了天启六年。此时大厦将倾，阉党横行，百官倾轧，民不聊生。党争依旧还在持续。烟雨江南中，才子依旧作乐，佳人们轻歌曼舞。流民们衣不蔽体，饥饿已至极限。辽东的后金铁骑已然磨刀霍霍，虎视天下。而恰在此时，张静一鱼服加身，绣春刀在腰。他成为了这个时代，以凶残和暴力而闻名天下的锦衣卫校尉。在这个不讲理的时代，恰恰成为了最不需讲道理的人。

> 章节总数：共830章

> 状态：完本
# 论文
### [RangeUDF: Semantic Surface Reconstruction from 3D Point Clouds | Papers With Code](https://paperswithcode.com/paper/rangeudf-semantic-surface-reconstruction-from)
> 日期：19 Apr 2022

> 标签：None

> 代码：None

> 描述：We present RangeUDF, a new implicit representation based framework to recover the geometry and semantics of continuous 3D scene surfaces from point clouds. Unlike occupancy fields or signed distance fields which can only model closed 3D surfaces, our approach is not restricted to any type of topology. Being different from the existing unsigned distance fields, our framework does not suffer from any surface ambiguity. In addition, our RangeUDF can jointly estimate precise semantics for continuous surfaces. The key to our approach is a range-aware unsigned distance function together with a surface-oriented semantic segmentation module. Extensive experiments show that RangeUDF clearly surpasses state-of-the-art approaches for surface reconstruction on four point cloud datasets. Moreover, RangeUDF demonstrates superior generalization capability across multiple unseen datasets, which is nearly impossible for all existing approaches.
### [EDTER: Edge Detection with Transformer | Papers With Code](https://paperswithcode.com/paper/edter-edge-detection-with-transformer)
> 日期：16 Mar 2022

> 标签：None

> 代码：None

> 描述：Convolutional neural networks have made significant progresses in edge detection by progressively exploring the context and semantic features. However, local details are gradually suppressed with the enlarging of receptive fields. Recently, vision transformer has shown excellent capability in capturing long-range dependencies. Inspired by this, we propose a novel transformer-based edge detector, \emph{Edge Detection TransformER (EDTER)}, to extract clear and crisp object boundaries and meaningful edges by exploiting the full image context information and detailed local cues simultaneously. EDTER works in two stages. In Stage I, a global transformer encoder is used to capture long-range global context on coarse-grained image patches. Then in Stage II, a local transformer encoder works on fine-grained patches to excavate the short-range local cues. Each transformer encoder is followed by an elaborately designed Bi-directional Multi-Level Aggregation decoder to achieve high-resolution features. Finally, the global context and local cues are combined by a Feature Fusion Module and fed into a decision head for edge prediction. Extensive experiments on BSDS500, NYUDv2, and Multicue demonstrate the superiority of EDTER in comparison with state-of-the-arts.
