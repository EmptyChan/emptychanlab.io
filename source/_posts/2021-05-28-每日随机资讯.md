---
title: 2021-05-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CowbirdsEgg_EN-CN3599107265_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-28 21:48:22
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CowbirdsEgg_EN-CN3599107265_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Oracle Linux 8.4 发布](https://www.oschina.net/news/143462/oracle-linux-8-4-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Oracle 已面向 64 位英特尔和 AMD(x86_64) 以及 64 位 Arm(aarch64) 平台发布了Oracle Lin......
### [蚂蚁集团自研数据库 OceanBase 将开源](https://www.oschina.net/news/143474/oceanbase-opensource-come-soon)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>《新浪科技》独家获悉，蚂蚁集团自研数据库 OceanBase 将于近期开源代码，时间最早可锁定至6月1日。以下是新浪科技的报道原文：新浪......
### [Windows 软件包管理器 1.0 正式发布](https://www.oschina.net/news/143478/windows-package-manager-1-0-ga)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>微软在近日举办的 Build 2021 上宣布 Windows 软件包管理器 —— Windows Package Manager 1......
### [Google 表示 Chrome 91 性能提高了 23%](https://www.oschina.net/news/143463/chrome-performance-up-23-percent)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Google发文表示，通过新的 V8 JavaScript 编译器Sparkplug和short builtins机制，近日更新的 Ch......
### [“小米模式”造不好车](https://www.huxiu.com/article/427209.html)
> 概要: 作者｜OdinAsgard近来，各种公司跨界进入新能源汽车行业，但属于年轻人的高潮是在今年三月份：“给年轻人带来第一款手机”的小米宣布要投入 1000 亿元人民币造车。更让年轻人兴奋的还在4月初，小米......
### [Virtuozzo’s Mature Linux Distribution VzLinux Now Available to Public](https://www.virtuozzo.com/connect/details/blog/view/virtuozzos-mature-linux-distribution-vzlinux-now-available-to-public.html)
> 概要: Virtuozzo’s Mature Linux Distribution VzLinux Now Available to Public
### [京东裂变背后：大企业分拆上市进行时｜钛媒体深度](https://www.tuicool.com/articles/neU7Vjf)
> 概要: 图片来源@视觉中国5月28日，京东物流（02618.HK）成功在港交所挂牌上市，开盘报46.05港元/股，较发行价上涨14.10%，总市值接近2800亿港元。这是京东集团旗下第二家分拆上市的公司。早在......
### [你是那个要来上海的friends吗？《兽娘动物园》新作！](https://shouyou.3dmgame.com/news/55711.html)
> 概要: 不知不觉，距离动画中的浮莲子们在加帕里公园的第一次冒险，已经过去了4年。对于平均寿命只有12—20岁的野生薮猫来说，4年的光阴就相当于人类的整个青春。屏幕中的她选择与作为人类的我们一路走来，共同经历许......
### [《索尼克：色彩终极版》预购现已开启 Epic售价249元](https://www.3dmgame.com/news/202105/3815400.html)
> 概要: 今天公布的《索尼克：色彩终极版》，即之前发售的《索尼克：色彩》复刻版本现已开放预购。目前该游将在全平台推出，实体版预购还将赠送儿童索尼克钥匙挂件。数字版预购已登陆Epic商店、微软商店以及PS商店。其......
### [翻译团粗中有细：灰熊队是如何用掩护为篮下的瓦兰丘纳斯创造机会的？](https://bbs.hupu.com/43203381.html)
> 概要: 注：本文全文约2100词，大概需花费6分钟阅读。如果觉得这篇文章还不错，也请点击一下右下角推荐，给同为JR的译者一些鼓励。当这个可靠的大个子更多地参与到进攻中时，灰熊队会获得更多的创造力。除了在低位转
### [TV动画「转生恶役只好拔除破灭旗标」第二季第2弹正式PV公开](http://acg.178.com/202105/416172382343.html)
> 概要: TV动画「转生恶役只好拔除破灭旗标」第二季公开了第2弹正式PV和新视觉图，追加了CAST：鸟海浩辅，该作将于2021年7月开播。TV动画「转生恶役只好拔除破灭旗标」第二季第2弹正式PV新视觉图：CAS......
### [「偶像大师 灰姑娘女孩」10周年主视觉图公开](http://acg.178.com/202105/416172728526.html)
> 概要: 「偶像大师 灰姑娘女孩」于近日公开了10周年主视觉图。「偶像大师 灰姑娘女孩」是根据BANDAI NAMCO Games（现：BANDAI NAMCO Entertainment）开发的同名社交类手机......
### [视频：林心如深夜给小海豚做卤肉饭 笑称是女儿最爱的料理](https://video.sina.com.cn/p/ent/2021-05-28/detail-ikmxzfmm5157758.d.html)
> 概要: 视频：林心如深夜给小海豚做卤肉饭 笑称是女儿最爱的料理
### [视频：邓超开豪车陪孙俪逛街 两人有说有笑仍似热恋](https://video.sina.com.cn/p/ent/2021-05-28/detail-ikmxzfmm5158028.d.html)
> 概要: 视频：邓超开豪车陪孙俪逛街 两人有说有笑仍似热恋
### [动画《转生恶役只好拔除破灭旗标》第二季PV](https://news.dmzj1.com/article/71033.html)
> 概要: TV动画《转生恶役只好拔除破灭旗标…X》公开了新的宣传图和PV。在这段PV中，可以看到主人公再度被绑架的片段。
### [在坏企业文化面前，再好的生意模式都不堪一击](https://www.huxiu.com/article/431075.html)
> 概要: 本文来自微信公众号：锦缎（ID：jinduan006），作者：大道稳行，题图来自：视觉中国2017年6月13日，美国电信公司威瑞森（Verizon）宣布，44.8亿美元收购雅虎旗下核心网路事业的交易正......
### [漫画「不屈的佐诺」广播剧化决定公布](http://acg.178.com/202105/416180480951.html)
> 概要: 由しっけ绘制的漫画「不屈的佐诺」宣布决定制作广播剧，该作将于2021年8月27日发布。CAST出戸：天﨑滉平佐诺：河西健吾......
### [任正非整风讲话：加快对平庸干部的淘汰](https://www.huxiu.com/article/431088.html)
> 概要: 本文来自微信公众号：蓝血研究（ID：lanxueyanjiu），作者：任正非，头图来自：视觉中国一、任正非在干部管理工作汇报会议上的讲话一、干部管理工作一定要对准贡献这个目标，在贡献面前人人平等。总干......
### [Mars Helicopter Lands Safely After Serious In-Flight Anomaly](https://spectrum.ieee.org/automaton/robotics/space-robots/mars-helicopter-lands-safely-after-serious-inflight-anomaly)
> 概要: Mars Helicopter Lands Safely After Serious In-Flight Anomaly
### [熬最晚的夜、吃最贵的补品，千亿规模功能性食品收的是智商税？](https://www.tuicool.com/articles/vYv6Jri)
> 概要: 编者按：本文来自微信公众号“时代周报”（ID:timeweekly），作者：穆瑀宸，36氪经授权发布。眼睛看久了屏幕酸涩，吃点叶黄素果冻；睡不好，吃颗褪黑素软糖；大吃大喝前，要来一粒“热控片”控制热量......
### [贾伟：因痛而生，打造真正的产品思维](https://www.tuicool.com/articles/Z7BfqmB)
> 概要: 2014年，我异常清晰地记得那是一个周六下午，电视里播放着动画片，通过感受自己孩子的痛苦，在自责的知觉中我顿悟到产品必须顺应痛点需求做出改变。那天，我和父亲在家中陪两个孩子看动画片，恬静地品味着与一岁......
### [漫威《月光骑士》剧集官宣男主角！](https://news.dmzj1.com/article/71037.html)
> 概要: 漫威确认奥斯卡·伊萨克将出演剧集《月光骑士》。
### [还能用中文写代码？东北话：我先来整一个](https://www.tuicool.com/articles/7Rvimeb)
> 概要: “会Python的人，工作都不会太差”。同事都在学编程，而我依旧卡在了英语这一关……面对满屏连不成句子的英文字，我就想问一问发明编程的人：fine;thank you;and you?“好优美的中国话......
### [TV动画「平稳世代的韦驮天们」第二弹PV公开](http://acg.178.com/202105/416186145403.html)
> 概要: 近日，TV动画「平稳世代的韦驮天们」公开了第二弹PV，该作改编自クール教信者著作的同名漫画作品，由MAPPA负责动画制作，将于2021年7月在富士电视台播出。TV动画「平稳世代的韦驮天们」第二弹PV「......
### [两个前暴雪背景工作室合作 将用UE5开发新RTS](https://www.3dmgame.com/news/202105/3815422.html)
> 概要: 前暴雪员工组建的工作室“冰霜巨人（Frost Giant）”于今日发表公告称，将与暴雪创始人迈克·莫汉（Mike Morhaime）成立的工作室“梦天堂（Dreamheaven）”合作，开发一款全新的......
### [组图：Twins扫楼星光梯大片氛围感足 直播对视一个甜一个美](http://slide.ent.sina.com.cn/y/slide_4_704_357210.html)
> 概要: 组图：Twins扫楼星光梯大片氛围感足 直播对视一个甜一个美
### [视频:《哪吒》被诉侵权案一审宣判：未构成实质性相似](https://video.sina.com.cn/p/ent/2021-05-28/detail-ikmyaawc8060327.d.html)
> 概要: 视频:《哪吒》被诉侵权案一审宣判：未构成实质性相似
### [星推官阿云嘎邀你入局！《盗墓笔记秦岭神树》吴邪角色歌发行！](https://new.qq.com/omn/ACF20210/ACF2021052800738600.html)
> 概要: 根据国民IP《盗墓笔记》改编的首部动画作品《盗墓笔记秦岭神树》正在腾讯视频火热独播中，故事迎来大高潮，终极谜题即将揭晓，值此激动人心的时刻，小三爷吴邪的角色歌——《入局》赶来报道，而为这首吴邪角色歌献......
### [出家的“中国第一神童”：少年班的天才们去哪儿了？](https://www.huxiu.com/article/431176.html)
> 概要: 本文来自微信公众号：往事叉烧（ID：wschashao），作者：叉少，题图来自：视觉中国1977年，中科院副院长方毅收到了一封推荐信，信中写道：“我之所以要写这封信，目的是为国家选贤，因为我从未见过这......
### [修仙大佬驾到《我是大神仙》时江CV夏侯落枫空降酷狗直播！](https://new.qq.com/omn/ACF20210/ACF2021052800767000.html)
> 概要: 每周四10点，腾讯视频独家热播的《我是大神仙》动画第二季已经突破11亿播放量！“平平无奇”的少年时江在仙界展开一场又一场热血有趣的冒险之旅，也收获了越来越多仙友的喜爱。动画第一季完美收官后，第二季更是......
### [《轨迹》系列手办化企划进行中！即推出黎恩与亚尔缇娜手办](https://news.dmzj1.com/article/71041.html)
> 概要: ​寿屋公开了《轨迹》系列手办化企划的部分作品图片。这次公开的是《闪之轨迹》系列中的主人公黎恩·舒华泽的未上色原型，和主要角色之一的亚尔缇娜·奥莱恩的彩色样品。
### [xwonder是青3出道团名?疑似爱奇艺程序员发文否认](https://ent.sina.com.cn/music/zy/2021-05-28/doc-ikmyaawc8077084.shtml)
> 概要: 新浪娱乐讯 5月28日，有爆料称《青你3》出道团确定团名为XWONDER。新浪综艺就网传信息向爱奇艺方求证，对方称该名称为爱奇艺虚拟制作的软件商标。疑似爱奇艺程序员发博回应，“想不到有一天我们开发的软......
### [比粉丝倒牛奶更浪费的，是炒币](https://www.ithome.com/0/554/184.htm)
> 概要: 先是三大行业协会发公告禁止虚拟货币交易，后是国务院金融委会议要求打击比特币挖矿和交易行为，足见高层对于虚拟货币态度坚决。某乎上有人问如何看待这一系列事，我看就一句话 ——防范个体风险变成社会风险。一般......
### [3999 元，乐视 G65S 开启预售：4K 全面屏](https://www.ithome.com/0/554/198.htm)
> 概要: IT之家5 月 28 日消息 本月早些时候，乐视公布了乐视超级电视首款 Mini LED 量子点产品 letv M65，以及另外三款超级电视： G55S 售价 3699 元、G65S 4299 元、G......
### [阔诺新连载哒！5月新连载漫画不完全指北第四期](https://news.dmzj1.com/article/71046.html)
> 概要: 受人欢迎被人倾慕的美少女，可只有一个人不为她的可爱所动——？！到充满幽灵的废墟试胆，没想到幽灵变成了…JK？！在荒废的街道，自称魔法师的男人和少年的旅行，他们能找到别的人类吗？！
### [晚间课堂！曲绍斌指导示范如何把控传球时机](https://bbs.hupu.com/43213376.html)
> 概要: 晚间课堂！曲绍斌指导示范如何把控传球时机
### [Agricultural Pest That Relies on Bacteria to Overcome Plant Defenses](https://www.tus.ac.jp/en/mediarelations/archive/20210527_1035.html)
> 概要: Agricultural Pest That Relies on Bacteria to Overcome Plant Defenses
### [华为鸿蒙 HarmonyOS 开发者 beta3 2.0.0.115 推送，更新万能卡片](https://www.ithome.com/0/554/210.htm)
> 概要: IT之家5 月 28 日消息 华为鸿蒙 OS 2.0 系统是首个真正为全场景时代打造的分布式操作系统，支持跨设备协同、大小屏互动、极速配网、自适应 UX、可视可说 AI 赋能语音交互、开发者开源等。随......
### [微信官方：微信圈子将停止运营](https://www.ithome.com/0/554/213.htm)
> 概要: IT之家5 月 28 日消息 2019 年 12 月，微信官方宣布，微信搜索正式升级为“微信搜一搜”。与此同时，微信搜一搜出现了“圈子”。今天微信官方宣布，微信圈子将于 2021 年 12 月 28 ......
### [Cortex (YC W20) Is Hiring Founding Engineers](https://www.workatastartup.com/jobs/29595)
> 概要: Cortex (YC W20) Is Hiring Founding Engineers
### [MDEX（HECO版BSC版）董事会Boardroom新增MDX单币质押](https://www.btc126.com//view/168344.html)
> 概要: MDEX（HECO版&BSC版）董事会Boardroom新增MDX单币质押。据MDEX.COM官方数据显示，截止今日20:20，MDEX（BSC版）董事会Boardroom新增MDX单币质押APY高达......
### [韩国模特申才恩福利图：人妻诱惑 尺度大很性感](https://www.3dmgame.com/bagua/4601.html)
> 概要: 今天为大家带来的是韩国模特申才恩(신 재 은 Zenny)的福利图。她那可爱的脸蛋配上性感的身材，真是杀伤力极大。2019年她宣布自己结婚了，但没有公布老公的身份。虽然女神已婚让很多粉丝伤透了心，但婚......
### [《血污：夜之仪式》销量超百万 续作开发中](https://www.3dmgame.com/news/202105/3815446.html)
> 概要: 《血污：夜之仪式》的续作看起来正在开发中。《血污：夜之仪式》发行商505 Games的母公司Digital Bros在最新发布的财报中提到一个“第二版”正在“开发当中”。需要注意的是，虽然“第二版”是......
### [美国说唱歌手Soulja Boy考虑推出加密代币Souljacoin](https://www.btc126.com//view/168346.html)
> 概要: 据U.Today消息，美国说唱歌手DeAndre Cortez Way （Soulja Boy）在推特发文表示，正在考虑制作一种以他的名字命名的加密货币Souljacoin。他指出，“是时候”启动它了......
### [上海住房公积金制度建立30年 累计缴存13300亿元](https://finance.sina.com.cn/jjxw/2021-05-28/doc-ikmxzfmm5247200.shtml)
> 概要: 原标题：上海住房公积金制度建立30年，累计缴存13300亿元 1991年，上海率先在全国建立住房公积金制度。经过30年探索和实践，上海住房公积金制度从无到有、从小到大...
### [54岁周海媚晒近照，皮肤紧致似少女，至今单身不结婚只因想要自由](https://new.qq.com/omn/20210528/20210528A0DVZN00.html)
> 概要: 5月28日下午，周海媚在某短视频网站上更新了一则动态，晒出了自己的近况，她的状态也是引发了网友们的热议。            视频中已经55岁的周海媚皮肤白皙细腻，紧致光滑，一头齐刘海让她更显年轻可......
### [深圳高层次人才、新引进人才租房和生活补贴业务将有调整](https://finance.sina.com.cn/jjxw/2021-05-28/doc-ikmyaawc8110916.shtml)
> 概要: 原标题：深圳高层次人才、新引进人才租房和生活补贴业务将有调整 来源：读特客户端 读特客户端5月28日消息，近日，深圳通过打出人才政策“组合拳”...
### [20年铁粉因病去世，李秉宪向剧组请假，赶往丧礼现场吊唁粉丝](https://new.qq.com/omn/20210528/20210528A0DXVP00.html)
> 概要: 5月28日，据韩媒报道，韩国影帝李秉宪在与朴叙俊、朴宝英等拍摄电影《水泥乌托邦》（Concrete Utopia）期间，李秉宪听闻一位支持他长达20多年的忠实粉丝过世的消息，因此临时向剧组告假，前往丧......
### [BITFORCE暂停向中国大陆用户出售云算力及提供矿机采购等服务](https://www.btc126.com//view/168349.html)
> 概要: 据官方消息，为积极响应国家政策，BITFORCE将暂停向中国大陆用户出售云算力、提供矿机代购及矿机托管等相关服务。国内矿场陆续关闭，BITFORCE自成立发展以来为全球用户提供了优质的服务，深受用户信......
### [开局就爆，秦昊新剧又贡献出神级演技，《隐秘的角落》后又一爆款](https://new.qq.com/omn/20210528/20210528A0E02600.html)
> 概要: 盼望着，盼望着，秦昊的粉丝们终于迎来振臂高呼的时刻，继去年宝藏神剧《隐秘的角落》之后，由秦昊主演的精品短剧集《猎狼者》总算如期开播，该剧立意突破创新，是我国首部反盗猎题材的电视剧作，从而被广大观众誉为......
### [“双碳”目标如何实现？专家又抛出了“绿氢”这个方案](https://finance.sina.com.cn/china/2021-05-28/doc-ikmyaawc8111124.shtml)
> 概要: 原标题：“双碳”目标如何实现？专家又抛出了“绿氢”这个方案 专家认为，氢能被视为21世纪最具发展潜力的清洁能源，但并不是所有的氢能都没有污染...
### [伏明霞长女迎成人礼，爸爸送两百平豪宅，梁锦松为女儿提前备嫁妆](https://new.qq.com/omn/20210528/20210528A0E56Y00.html)
> 概要: 42岁“跳水皇后”伏明霞自从2002年7月闪嫁大其27岁的“打工皇帝”梁锦松之后，夫妻二人相敬如宾，于浅水湾豪宅过着“半隐居”生活，鲜少在公众视野露面，极为低调。婚后，伏明霞为丈夫梁锦松先后诞下一女两......
### [这个7岁的“会”长出多少“慧”？](https://finance.sina.com.cn/china/gncj/2021-05-28/doc-ikmxzfmm5249343.shtml)
> 概要: 原标题：这个7岁的“会”长出多少“慧”？ 新华社贵阳5月28日电 新华社记者向定杰、潘德鑫 转眼间，中国国际大数据产业博览会已经走过7个年头。
### [Zenlink SlotVault将深度整合Bifrost SALP，为插槽拍卖提供首个流动性衍生品挖矿产品](https://www.btc126.com//view/168350.html)
> 概要: Zenlink官方今日宣布，其打造的“PLO 即挖矿”产品SlotVault将深度整合Bifrost插槽竞拍衍生品协议SALP，为Kusama和Polkadot插槽拍卖提供首个流动性衍生品挖矿产品......
### [流言板皇马所有高管今日罕见齐聚开会，讨论新任主帅的问题](https://bbs.hupu.com/43215749.html)
> 概要: 虎扑05月28日讯 根据西班牙方面消息，今天皇马主席佛罗伦蒂诺，总经理Jose Angel Sanchez，第一副主席Pedro Lopez，皇马董事，佛罗伦蒂诺的弟弟Enrique，以及所有的董事会
### [香港地产商送福利：打新冠疫苗，抽价值1000万房产！](https://finance.sina.com.cn/china/dfjj/2021-05-28/doc-ikmxzfmm5251500.shtml)
> 概要: 原标题：地产商送福利：打新冠疫苗，抽价值1000万房产！为了鼓励接种，这座城市“拼了”！ 每经编辑 孙志成 在寸土寸金的香港，拥有一套属于自己的房子...
### [福建立法4岁以下儿童乘车必用安全座椅](https://finance.sina.com.cn/china/dfjj/2021-05-28/doc-ikmxzfmm5250454.shtml)
> 概要: 【#福建立法4岁以下儿童乘车必用安全座椅#】昨天（27日），福建省人大常委会会议表决通过《福建省儿童乘坐机动车使用安全座椅的规定》...
### [赛事视频 大鹅五人先后倒下，RW侠波澜不惊直推水晶](https://bbs.hupu.com/43216033.html)
> 概要: 来源：  虎扑    标签：杭州LGD大鹅RW侠
# 小说
### [都市剑说](https://m.qidian.com/book/1011447634/catalog)
> 作者：华表

> 标签：都市异能

> 简介：异界回归的李白，专职斩妖除魔，从心理到生理专治各种不服。作者是中央八局关注第四类人员，主角是第五类人员，侬晓得了伐！

> 章节总数：共1779章

> 状态：完本
# 论文
### [Multi-Scale Progressive Fusion Network for Single Image Deraining](https://paperswithcode.com/paper/multi-scale-progressive-fusion-network-for)
> 日期：24 Mar 2020

> 标签：RAIN REMOVAL

> 代码：https://github.com/kuihua/MSPFN

> 描述：Rain streaks in the air appear in various blurring degrees and resolutions due to different distances from their positions to the camera. Similar rain patterns are visible in a rain image as well as its multi-scale (or multi-resolution) versions, which makes it possible to exploit such complementary information for rain streak representation.
### [Generating Natural Language Attacks in a Hard Label Black Box Setting](https://paperswithcode.com/paper/generating-natural-language-attacks-in-a-hard)
> 日期：29 Dec 2020

> 标签：ADVERSARIAL TEXT

> 代码：https://github.com/RishabhMaheshwary/hard-label-attack

> 描述：We study an important and challenging task of attacking natural language processing models in a hard label black box setting. We propose a decision-based attack strategy that crafts high quality adversarial examples on text classification and entailment tasks.
