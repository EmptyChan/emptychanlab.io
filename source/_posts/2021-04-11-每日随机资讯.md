---
title: 2021-04-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.YoshinoyamaSpring_EN-CN8456726688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-04-11 16:40:13
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.YoshinoyamaSpring_EN-CN8456726688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [《延禧攻略》回忆杀！吴谨言晒与谭卓秦岚重聚照](https://ent.sina.com.cn/v/m/2021-04-11/doc-ikmxzfmk6097854.shtml)
> 概要: 新浪娱乐讯 4月10日晚，吴谨言微博晒出与谭卓、秦岚合照，配文：“在宫外聚一次太不容易了”。据悉，三人曾分别出演大热剧《延禧攻略》中的魏璎珞、高贵妃、富察容音。(责编：山水)......
### [Don’t hire top talent; hire for weaknesses](https://benjiweber.co.uk/blog/2021/04/10/dont-hire-top-talent-hire-for-weaknesses/)
> 概要: Don’t hire top talent; hire for weaknesses
### [Brave disables Chromium FLoC features](https://github.com/brave/brave-core/pull/8468)
> 概要: Brave disables Chromium FLoC features
### [阿里“重罚”，马云“过关”？](https://www.huxiu.com/article/420894.html)
> 概要: 本文来自微信公众号：凤凰WEEKLY财经（ID：fhzkzk），作者：唐吉，编辑：王毕强，原文标题：《阿里“重罚”，马云“过关”？》，题图：IC photo高达182.28亿元的巨额罚单，也没能打破马......
### [组图：官宣恋情？伊能静19岁儿子晒与女生合照 甜喊其“女友”](http://slide.ent.sina.com.cn/star/slide_4_86512_355165.html)
> 概要: 组图：官宣恋情？伊能静19岁儿子晒与女生合照 甜喊其“女友”
### [真的存在可以“穿越牛熊”的投资策略吗？](https://www.tuicool.com/articles/z6bmyen)
> 概要: 本文来自微信公众号：界面新闻（ID：wowjiemian），作者：刘沥泷，编辑：宋烨珺原文标题：《“我为什么总是反向指标”？A股策略周期大揭秘》，题图来自：视觉中国“一心一意做打板接力，亏得一塌糊涂；......
### [The Cursed Computer Iceberg Meme](https://suricrasia.online/iceberg/)
> 概要: The Cursed Computer Iceberg Meme
### [如果外卖APP上线盲盒，可行吗？](https://www.tuicool.com/articles/qYz6Jfz)
> 概要: 编辑导读：作为一名干饭人，每天都在思考下一餐要吃什么。如果外卖APP上能够上线盲盒，岂不是就能解决点餐困难症了？我们在天天问和小伙伴讨论了这个问题，一起来看看他们有什么精彩回答吧~吾日三省吾身：早餐吃......
### [中国奠定 “清洁科技 2.0 时代”，有望出现下一个 “风口上的猪”](https://www.ithome.com/0/545/355.htm)
> 概要: 随着各国政府对气候问题愈加重视以及人们环保意识的不断增强，硅谷的科技公司似乎也找到了除开造车的另一个发力点——清洁科技。马丁 · 罗希森一度是太阳能行业的先驱，带领着一家由谷歌创始人投资的创业公司。这......
### [功德圆满 《文明6》即将公布最终免费更新档计划](https://www.3dmgame.com/news/202104/3812238.html)
> 概要: 《文明6》是一款2016年发售的游戏，到现在上市都已经快五年了，但还在保持着内容更新。Firaxis Games对这款回合制策略游戏的售后支持让人简直挑不出毛病。但天下无不散之筵席，《文明6》的售后支......
### [组图：杜江晒与儿子冲浪画面 7岁嗯哼彰显超高运动天赋](http://slide.ent.sina.com.cn/star/w/slide_4_86512_355169.html)
> 概要: 组图：杜江晒与儿子冲浪画面 7岁嗯哼彰显超高运动天赋
### [远程办公之后，亚马逊与摩根大通复工计划凸显写字楼市场不确定性](https://www.ithome.com/0/545/358.htm)
> 概要: 北京时间 4 月 11 日上午消息，据报道，在很多人眼中，科技公司更喜欢远程工作，银行更偏爱现场办公，但亚马逊和摩根大通最近对远程工作的表态却打破了这种固有认知。亚马逊对员工表示，该公司计划恢复 “办......
### [动画「极主夫道」决定制作第二部分](http://acg.178.com//202104/412106435681.html)
> 概要: 漫改网络动画「极主夫道」宣布将制作第二部分，该动画的第一部分现已上线Netflix，全5集。「极主夫道」是由大野康介创作的漫画作品，讲述了传说中的最强黑道、不死身的“龙”，在金盆洗手后成为了家庭主夫的......
### [守护梦想燃烧热血，治愈系的感人故事](https://news.dmzj1.com/article/70591.html)
> 概要: 2021年韩国第一季度收尾剧《像蝴蝶一样飞》播出，豆瓣评分高达8.9，该剧由同名漫画改编，朴仁焕、宋江、罗文姬、洪承熙等主演，讲述了年过70岁开始挑战跳芭蕾的老人和23岁在梦想面前踌躇彷徨的年轻芭蕾舞演员，通过芭蕾建立友情的成长故事。
### [霸主雄火龙登场《怪物猎人：崛起》4月末免费更新](https://www.3dmgame.com/news/202104/3812243.html)
> 概要: 卡普空于4月11日今天公开了旗下超人气游戏《怪物猎人：崛起》预定4月末开启免费更新的概要预告，一起来了解下。·《怪物猎人：崛起》刚刚于3月26日登陆Switch独占发售，短短几日全球出货量已经突破50......
### [「偶像活动」藤堂尤里卡手办开订](http://acg.178.com//202104/412109397908.html)
> 概要: MEGAHOUSE作品「偶像活动」藤堂尤里卡手办开订，高约230mm，主体采用ABS、PVC材料制造。该手办定价为20350日元（含税），约合人民币1216元，预计于2021年9月发售......
### [2021年投资的十大加密货币](https://www.tuicool.com/articles/imQzqu2)
> 概要: 加密货币几乎总是旨在抵制政府的开发和控制，尽管该行业的这一基本特征随着其日益普及而受到抨击。此外，比特币价格一直保持在50,000美元以上，这表明加密货币领域吸引了很多兴趣。苹果，谷歌，特斯拉，三星，......
### [肖战《玉骨遥》生图公开，颜值离开精修叫人看懵，这真有30岁吗](https://new.qq.com/omn/20210409/20210409A09IO700.html)
> 概要: 肖战绝对是现在娱乐圈中的“顶流男星”代表，纵使之前肖战的粉丝闹出所谓的“227事件”，可基于肖战目前身具的商业价值，他在娱乐圈中的发展依旧很不错。前段时间肖战发布长文替粉丝道歉后也算正式重回娱乐圈，在......
### [官宣！超人亨利·卡维尔晒与女友合照公布新恋情](https://ent.sina.com.cn/s/u/2021-04-11/doc-ikmxzfmk6163053.shtml)
> 概要: 新浪娱乐讯 11日，英国男演员亨利·卡维尔晒出与女友的照片甜蜜认爱，大赞女友美丽又聪明。据悉，前几天亨利·卡维尔被拍到与女友漫步在英国伦敦街头。卡维尔一手牵着女友，一手牵着狗，这是他首次带新女友出现在......
### [终于相信上镜胖10斤了，看到离开精修的关晓彤后：再也不叫她高大壮](https://new.qq.com/omn/20210407/20210407A02M4U00.html)
> 概要: 除了是童星演员之外，其实关晓彤还是一位星二代，出生艺术之家的她从小就接触了演艺道路，并且还出演了不少的影视作品，虽然年纪不大，但是还是获得了不少观众们的赞赏，而且也给大众留下了深刻的印象。长大后的关晓......
### [South African variant can break through Pfizer vaccine, Israeli study says](https://www.reuters.com/article/us-health-coronavirus-israel-study/south-african-variant-can-break-through-pfizer-vaccine-israeli-study-says-idUSKBN2BX0JZ)
> 概要: South African variant can break through Pfizer vaccine, Israeli study says
### [漫画版《Vivy -Fluorite Eye's Song-》正式开始连载](https://news.dmzj1.com/article/70592.html)
> 概要: 改编自4月原创新番《Vivy -Fluorite Eye's Song-》的同名漫画已经于昨日（4月10日）正式在MAG Garden旗下Web漫画网站MAGCOMI开始了连载。
### [初音未来线上演唱会将于2021年6月6日举办](http://acg.178.com//202104/412118929749.html)
> 概要: 初音未来的线上演唱会「HATSUNE MIKU EXPO 2021 Online」确定将于2021年6月6日举办。官方表示，考虑到时差问题，演唱会将在一天内播放三次，全球观众都可以随时观看。演唱会播出......
### [漫画杂志「Manga Time Kirara」公开五月号封面](http://acg.178.com//202104/412119532885.html)
> 概要: 漫画杂志「Manga Time Kirara」官推公开了五月号的封面图，封面角色为「一起一起这里那里」的主角御庭摘希。原文：「Manga Time Kirara」五月号封面为异识老师绘制的漫画「一起一......
### [Elastic 认证（ECE）2021 年 7 月版本升级解读](https://www.tuicool.com/articles/qE7Zreu)
> 概要: 1、官方说明变更以官方文档为准：https://www.elastic.co/cn/training/certification/faq2、版本变更Elastic Certified Engineer......
### [4月15日上映！水原希子主演百合犯罪片《她》公布正片片段](https://news.dmzj1.com/article/70593.html)
> 概要: 根据中村珍原作漫画《群青》改编的真人电影《她（彼女）》将于4月15日在Netflix上映。今天，官方公布了一段正片片段。
### [央视揭露：“大师”号称会改运势，上千人买手机号转运被骗](https://www.ithome.com/0/545/386.htm)
> 概要: IT之家4月11日消息 据央视财经报道，一个所谓的大师，声称只要使用了她为你量身定制的手机号，好运就会接踵而来。据悉，这位 “大师”李某，在重庆市开了一家名为 “今飞扬”的公司。在被害人初步购买了定制......
### [我国高铁不晕车原因揭晓：夹层玻璃无畸变，驾驶员技术过硬](https://www.ithome.com/0/545/387.htm)
> 概要: IT之家4月11日消息 今日 @中国铁路 官方微博发表文章，详细介绍了我国高铁列车不晕车的原因。日常生活中有人乘坐时速 80 公里的轿车都会晕车，然而高铁速度超过 300 公里，却依然不会感到晕车。这......
### [《罗德岛编年史》2022年春发售 收录11部经典名作](https://www.3dmgame.com/news/202104/3812261.html)
> 概要: D4Enterprise社日前宣布，收录了11部经典名作的合集游戏《罗德岛战记：编年史》预定2022年春发售，登陆PC平台，一起来了解下。·《罗德岛战记：编年史》共计收录11部经典名作，其中《罗德岛战......
### [银行卡大量冻结冲击义乌外贸 网传义乌警方公开信：拒绝过度执法](https://finance.sina.com.cn/world/2021-04-11/doc-ikmyaawa9056867.shtml)
> 概要: 银行卡大量冻结冲击义乌外贸，网传义乌警方公开信：拒绝过度执法、选择性执法 商户们遇到的银行卡冻结问题 与近两年持续推动的“断卡行动”有关 图/图虫创意...
### [全国多地在行动：疫苗接种车开进村 还有最美接种点打针又打卡](https://finance.sina.com.cn/china/2021-04-11/doc-ikmxzfmk6197498.shtml)
> 概要: 原标题：全国多地在行动！超方便！疫苗接种车开进村！还有“最美接种点”，打针又“打卡”！ 新冠疫苗大规模接种工作正在有序地推进当中，为了方便市民接种疫苗，在海南海口...
### [国内厂商携手孩之宝 推出可自动变形的擎天柱机器人](https://www.3dmgame.com/news/202104/3812263.html)
> 概要: 很多人都拥有过属于自己的变形金刚玩具，虽然掰来掰去地变形有些费劲，但能玩到跟动画片里一样的东西，那份满足感还是很强烈的。但是接下来如果钱到位的话，恐怕变形金刚玩具在变形的时候就不用自己掰了，而且最新产......
### [国家卫健委：我国新冠病毒疫苗接种总剂次数为全球第二](https://finance.sina.com.cn/china/gncj/2021-04-11/doc-ikmxzfmk6198667.shtml)
> 概要: 国家卫健委：我国新冠病毒疫苗接种总剂次数为全球第二 北京日报客户端|记者 杨绪军 孙乐琪 4月11日下午，国务院联防联控机制召开新闻发布会...
### [“千万人口俱乐部”要增员？南京济南青岛长沙喊出了口号](https://finance.sina.com.cn/jjxw/2021-04-11/doc-ikmyaawa9058499.shtml)
> 概要: 原标题：“千万人口俱乐部”要增员？南京济南青岛长沙喊出了口号 人口规模是城市集聚和辐射能力的体现，也是当前城市竞争的一大基础。
### [《莎木3》新MOD登场 替换蓝帝配音为系列原声版](https://www.3dmgame.com/news/202104/3812264.html)
> 概要: 经典游戏新篇《莎木3》已经登陆多个平台，众所周知，其英语版配音中，Ren与蓝帝的配音更换，而近日一款新MOD将原配音替换回来，感兴趣的玩家可以关注下了。作为日本泰斗级游戏制作人铃木裕的代表作，《莎木》......
### [吴尊友谈瑞丽疫情：常态化防控措施不能因春暖花开而松懈](https://finance.sina.com.cn/china/2021-04-11/doc-ikmyaawa9058737.shtml)
> 概要: 4月11日，中国疾控中心首席流行病学专家吴尊友在国务院联防联控机制新闻发布会上介绍瑞丽此轮新冠疫情带来的两点启示。
### [岳云鹏要“打假”？是文化有限，还是想挑战行规](https://new.qq.com/omn/20210411/20210411A05MJF00.html)
> 概要: 4月11日，德云社相声演员岳云鹏在社交平台发文，对自己购买的手机提出了异议。            岳云鹏称，自己买的时候，手机空间标注为128G，可是到手一看才112G，这其中的价格差距谁来买单。 ......
### [组图：李诞挎大包拖行李箱独自现身 穿“一脚蹬”休闲范十足](http://slide.ent.sina.com.cn/star/slide_4_86448_355190.html)
> 概要: 组图：李诞挎大包拖行李箱独自现身 穿“一脚蹬”休闲范十足
### [刘芸带62岁妈妈参加综艺，气质出众颜值高，母女同框宛如姐妹](https://new.qq.com/omn/20210411/20210411A05SUQ00.html)
> 概要: 4月11日，某卫视官宣一档新节目《妈妈你真好看》，这档综艺节目聚焦50岁以上的中年女性，首期嘉宾阵容官宣之后饱受热议，其中有大家熟知的女艺人：刘雯、刘芸、李斯丹妮、蓝盈莹、宋妍霏、张馨予、孙千、孙怡、......
### [科兴中维：疫苗可能很快就获得世卫组织的紧急使用授权](https://finance.sina.com.cn/china/2021-04-11/doc-ikmxzfmk6203051.shtml)
> 概要: 4月11日，北京科兴中维公司公共关系总监刘沛诚表示，今年春节期间，科兴中维已经完成了世卫组织紧急使用授权的生产现场的核查，近期世卫组织发言人也表示...
# 小说
### [终极教师](http://book.zongheng.com/book/347511.html)
> 作者：柳下挥

> 标签：都市娱乐

> 简介：方炎原是太极世家传人，因为不堪忍受一个野蛮女人的暴力欺负而翘家逃跑，弃武从文成为一所私立学校的高中语文老师。于是，一个传奇教师就此诞生！终极教师量身订制的手游《终极酷跑》上线啦，欢迎下载：http://download.gyyx.cn/Default.ashx?typeid=978&netType=1

> 章节末：老柳新书《逆鳞》已经发布！

> 状态：完本
# 论文
### [Fast Network Embedding Enhancement via High Order Proximity Approximation](https://paperswithcode.com/paper/fast-network-embedding-enhancement-via-high)
> 日期：‏‏‎ ‎ 2020

> 标签：DIMENSIONALITY REDUCTION

> 代码：https://github.com/benedekrozemberczki/karateclub

> 描述：Many Network Representation Learning (NRL) methods have been proposed to learn vector representations for vertices in a network recently. In this paper, we summarize most existing NRL methods into a unified two-step framework, including proximity matrix construction and dimension reduction.
### [Co-occurrences using Fasttext embeddings for word similarity tasks in Urdu](https://paperswithcode.com/paper/co-occurrences-using-fasttext-embeddings-for)
> 日期：22 Feb 2021

> 标签：WORD EMBEDDINGS

> 代码：https://github.com/usamakh20/wordEmbeddingsUrdu

> 描述：Urdu is a widely spoken language in South Asia. Though immoderate literature exists for the Urdu language still the data isn't enough to naturally process the language by NLP techniques.
