---
title: 2021-03-21-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HallesWood_EN-CN6355536404_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-03-21 21:13:41
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HallesWood_EN-CN6355536404_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [动视暴雪新一轮裁员将影响其欧洲发行办公室](https://www.3dmgame.com/news/202103/3810919.html)
> 概要: 动视暴雪将进行新一轮裁员，这将影响其欧洲发行办公室。目前，法国、德国、荷兰、西班牙和英国地区的办公室正在进行咨询。据GI.biz报道，动视暴雪正计划将其欧洲发行业务整合到英国。裁员不会影响任何开发工作......
### [“人性第一”](https://www.huxiu.com/article/416386.html)
> 概要: 虎嗅机动资讯组作品作者 | 竺晶莹题图 | Google技术与人，谁将被驯服？这是个问题。2017年12月，在整个冬季稳步攀爬的比特币价格迎来大幅飙升，首次突破1万美元。短短两周后，一枚比特币等值于1......
### [究竟什么是爱？](https://www.huxiu.com/article/416393.html)
> 概要: 本文来自微信公众号：近似于透明的深蓝（ID：derBlau），作者：郑轶vanessa，原文标题：《究竟什么是爱？Vol.1 阴阳之道：我们对性别能量的认知偏差和误解》，头图来源：《丹麦女孩》我打算尝......
### [刘晓丹：热市场中的冷思考](https://www.huxiu.com/article/416398.html)
> 概要: 本文来自微信公众号：晨壹投资（ID：gh_d6f3b02e787d），作者：刘晓丹，头图来源：晨壹投资2021年1月8日，晨壹投资第一次投资人年度会议于三亚召开。创始人刘晓丹女士在会议上发表了主题演讲......
### [森下suu绘制的「EVA」绫波丽公开](http://acg.178.com//202103/410290617199.html)
> 概要: 「日日蝶蝶」作者森下suu公开了其绘制的「EVA」绫波丽。绫波丽是动画「EVA」中的主要角色，是EVA零号机的专属驾驶员和第一适格者，拥有与明日香截然相反的冷淡性格......
### [组图：这是紧张吗？林依晨回答婚姻问题30秒摸四次鼻子](http://slide.ent.sina.com.cn/star/slide_4_704_354228.html)
> 概要: 组图：这是紧张吗？林依晨回答婚姻问题30秒摸四次鼻子
### [Spring Native Beta来了，原生更香！](https://www.tuicool.com/articles/mIBZVzR)
> 概要: 作者 | Spring 官方博客译者 | 张卫滨策划 | 万佳Spring 团队日前发布了 Spring Native Beta 版。通过 Spring Native，Spring 应用将有机会以 G......
### [动画人saisapphire新绘制「无限滑板」喜屋武历&驰河兰加公开](http://acg.178.com//202103/410292705898.html)
> 概要: 动画人saisapphire公开最新绘制的「无限滑板」喜屋武历&驰河兰加。「无限滑板」是由bones负责制作的原创电视动画，已于2021年1月10日开始播出，目前仍在热播中......
### [A New Lens on Understanding Generalization in Deep Learning](https://ai.googleblog.com/2021/03/a-new-lens-on-understanding.html)
> 概要: A New Lens on Understanding Generalization in Deep Learning
### [组图：赵丽颖黑长直大片展现纯粹与魅惑 眼神坚定深沉气场全开](http://slide.ent.sina.com.cn/star/slide_4_704_354229.html)
> 概要: 组图：赵丽颖黑长直大片展现纯粹与魅惑 眼神坚定深沉气场全开
### [漫画《音乐爱好者》真人电影化决定！2021年秋季公映](https://news.dmzj1.com/article/70397.html)
> 概要: "手塚治虫文化赏"得奖漫画家·asouakira《音乐爱好者》真人电影化决定！
### [扎导曾向华纳建议拍摄《原子侠》：由中国演员主演](https://www.3dmgame.com/news/202103/3810938.html)
> 概要: 近日，扎克·施耐德在接受了外媒《娱乐周刊》采访时谈及了自己预想中的“正义联盟”续集。除此之外他还表示，自己曾建议华纳拍摄由中国演员主演的《原子侠》。在本次采访中，娱乐周刊的记者询问扎克·施耐德，他原本......
### [东京奥运会放弃接纳海外观众 损失超1500亿日元](https://www.3dmgame.com/news/202103/3810939.html)
> 概要: 据NHK报道，当地时间3月20日晚，东京奥组委主席桥本圣子表示，东京奥运会正式决定放弃接纳海外普通观众，这是五方磋商后一致达成的意见。据日本媒体报道，这一决定将带来超过1500亿日元的损失。东京奥组委......
### [多核系统的负载均衡](https://www.tuicool.com/articles/jmURBbV)
> 概要: 何为多核多核大致可分为同构（SMP）和异构（AMP）两种，如果使用Hyperthreading技术，则一个CPU core可以有两个逻辑线程，这两个逻辑线程共享ALU, FPU和L2 Cache等。从......
### [中国移动采购330万台小米Redmi K40/Pro系列手机](https://www.3dmgame.com/news/202103/3810941.html)
> 概要: 自从Redmi K40系列开售以来，其凭借1999元起极具竞争力的价格以及同级别无短板的配置受到众多消费者争相抢购，每轮发售几乎都是“秒”售罄。甚至有“米粉”在全家和朋友的帮助下，抢了几轮也依然抢不到......
### [郭麒麟的四次下跪，一次比一次精彩，郭德纲乐了！](https://new.qq.com/omn/20210321/20210321A00DY100.html)
> 概要: 中国古代一直有跪拜礼的传统，特别是对于父母或者是恩师。但到了现代之后，这种文化传统渐渐被抛弃，庄严的跪拜礼反而成了献媚，软骨头的代名词。而在2016年的钢丝节上，德云社正在举行筱字科拜师仪式，郭德纲也......
### [Nzyme v1.0.0: Free and Open WiFi Defense System](https://www.nzyme.org/post/nzyme-v1-0-0-kyle-canyon-has-been-released)
> 概要: Nzyme v1.0.0: Free and Open WiFi Defense System
### [设计师该如何进行“情境探究”](https://www.tuicool.com/articles/FBzUzaQ)
> 概要: 编辑导语：对于设计师来说，在设计前期对于用户的调研是非常重要的，产品的设计在很大程度上影响了用户体验，所以前期设计师会进行一些调研活动；本文作者分享了关于设计师该怎么进行调研，我们一起来了解一下。在笔......
### [TV动画「再见了我的克拉默」新视觉图公开](http://acg.178.com//202103/410305192898.html)
> 概要: 电视动画「再见了我的克拉默」公开了新“曽志崎緑”视觉图，动画讲述了一个15岁高中生少女周防堇与足球缔下不解之缘的故事，本作将于4月4日开始放送。CAST恩田 希：岛袋美由利越前佐和：若山诗音周防すみれ......
### [小鹏 P7 测试车照片曝光：含多个 LiDar 激光雷达](https://www.ithome.com/0/541/253.htm)
> 概要: IT之家3月21日消息 根据微博用户 @理想嘉 - 大 D 消息，近日小鹏 P7 测试车的照片被网友拍到，车顶装有支架，安装有激光雷达。车头两侧也具备 LiDar 激光雷达。微博用户表示，通过询问业内......
### [组图：李沁世界睡眠日晒睡袍自拍 头发蓬松凌乱慵懒俏皮](http://slide.ent.sina.com.cn/star/slide_4_704_354235.html)
> 概要: 组图：李沁世界睡眠日晒睡袍自拍 头发蓬松凌乱慵懒俏皮
### [漫画「杜鹃的婚约」第六卷封面公开](http://acg.178.com//202103/410306475948.html)
> 概要: 漫画「杜鹃的婚约」作者吉河美希公开了本作第六卷的封面图，该册将于4月16日发售。「杜鹃的婚约」是由讲谈社出版的恋爱漫画，讲述了男女主出生时被抱错，后来二人却偶然被安排婚约的故事......
### [George Bass has died](https://www.nytimes.com/2021/03/19/us/george-bass-dead.html)
> 概要: George Bass has died
### [新型降智互动网剧，便利店爱情奇遇记](https://news.dmzj1.com/article/70398.html)
> 概要: 2021年1月份韩国出品新型网剧《暧昧的便利店》，被叫做愿望成就互动式网剧，女主为主线和不同类型的男孩子开展故事线，就像是国内的橙光游戏的感觉，不过变成了动态视频剧，根据支线内容观众可以选择支持哪对儿cp，然后结局由观众投票决定。
### [每天几颗美滋滋：天虹牌巴旦木仁 1 斤 39.9 元（立减 20 元）](https://lapin.ithome.com/html/digi/541260.htm)
> 概要: 【天虹牌旗舰店】巴旦木仁 500g 59.9元，限时限量20元券，实付39.9元包邮。天虹的坚果很不错，且是60年的香港老牌。本次有原味与淡盐味可选，一乖比较喜欢淡盐味：天猫天虹牌 巴旦木仁 500g......
### [组图：关颖参加名媛聚会 富士康老板娘曾馨莹穿破洞裤坐C位](http://slide.ent.sina.com.cn/star/slide_4_704_354239.html)
> 概要: 组图：关颖参加名媛聚会 富士康老板娘曾馨莹穿破洞裤坐C位
### [Godot Engine Web Editor](https://editor.godotengine.org/releases/latest/)
> 概要: Godot Engine Web Editor
### [《创造营》第一次顺位排名发布，李嘉祥淘汰，向赞多道歉表示对不起父母](https://new.qq.com/omn/20210321/20210321A05K2Z00.html)
> 概要: 《创造营》第一次顺位排名发布，李嘉祥淘汰，向赞多道歉表示对不起父母在3月20日晚上，《创造营》发布了第一次顺位的排名，近乎一半的学员离开，大家应该都是挺不舍的，而在昨晚最令人意外的应该就是利路修得了第......
### [央行行长易纲：碳中和约束下，有两方面任务格外紧迫](https://www.huxiu.com/article/416433.html)
> 概要: 虎嗅注：本文系3月20日，中国人民银行行长易纲在中国发展高层论坛圆桌会上发表了《用好正常货币政策空间，推动绿色金融发展》的讲话全文。他强调，当前要实施好稳健的货币政策，支持稳企业保就业，持续打好防范化......
### [3.8g 乳蛋白，金典纯牛奶梦幻盖 10 盒 39.9 元（京东 76.9 元）](https://lapin.ithome.com/html/digi/541279.htm)
> 概要: 3.8g乳蛋白，伊利金典纯牛奶梦幻盖250mL×10盒报价59.9元，限时限量20元券，实付39.9元包邮，领券并购买。使用最会买App下单，预计还能再返 4.38 元，返后 35.52 元包邮，点击......
### [《司藤》还在热播，张彬彬又一新剧来袭，看到演员阵容又是爆款](https://new.qq.com/omn/20210321/20210321A06GNQ00.html)
> 概要: 《司藤》还在热播最近热播的电视剧《司藤》备受观众的喜欢，剧中的两位主角张彬彬和景甜无论是戏里还是剧外都很甜，两位演员凭借着这部剧收获了很多的人气，可以说两位演员像是小说中的走出来的角色本人，换一种角度......
### [简易版Vuex源码实现](https://www.tuicool.com/articles/aYve22z)
> 概要: Vuex使用1、注册Vuex插件import Vuex from vuexVue.use(Vuex)2、创建Store实例export default new Vuex.Store({  state:......
### [《青你3》皇族已经出现，陷抄袭风波不回应，第一次排名仅次于余景天](https://new.qq.com/omn/20210321/20210321A06JWV00.html)
> 概要: 已经开播了一个多月的《青春有你3》于昨晚播出了第一次顺位排名，淘汰了58位练习生，其中有不少没有被大家发现的宝藏男孩，不过大家的关注点肯定不会是在下位圈的，而是助力值占了全部助力值百分之四十五的上位圈......
### [POCO F3 曝光：外观类似于 Redmi K40](https://www.ithome.com/0/541/282.htm)
> 概要: IT之家3月21日消息 小米将于 3 月 30 日在印度发布 POCO X3 Pro 系列机型。在 X3 Pro 推出之前，另一款 POCO F3 的已经在网上被曝光。知名爆料者 @ishanagar......
### [他塌房后只敢让妈妈来求饶？算了吧](https://new.qq.com/omn/20210321/20210321A06PK100.html)
> 概要: 近日，某正在参加选秀、未出道的男爱豆魏宏宇因被爆料私生活混乱而陷入争议，引发粉丝塌房。                        事件过去半个月，魏宏宇的母亲发文，代表魏宏宇向公众道歉。魏宏宇妈妈......
### [动画《ARIA The BENEDIZIONE》超特报映像公开！](https://news.dmzj1.com/article/70399.html)
> 概要: 《水星领航员》新作动画『ARIA The BENEDIZIONE』超特报映像公开
### [学区房崩了？上海教育新政掀“大风暴” 知名老破小骤降60万](https://finance.sina.com.cn/china/2021-03-21/doc-ikknscsi9161076.shtml)
> 概要: 学区房崩了？上海教育新政掀“大风暴”，知名老破小骤降60万，这个城市也出手了！ 曾经5个月暴涨43%的上海学区房要凉了？ 近日，上海市教委公布了新的高中阶段学校招生录取改...
### [触目惊心：黄河大堤内多处现大量死猪 有的已风化成骨架](https://finance.sina.com.cn/china/2021-03-21/doc-ikkntiam6002807.shtml)
> 概要: 触目惊心！黄河大堤内多处现大量死猪，有的已风化成骨架 据半月谈公众号3月21日消息，日前，记者在黄河内蒙古达拉特旗段发现，大堤内有不少死猪...
### [动画《半妖的夜叉姬》手办开订](https://news.dmzj1.com/article/70400.html)
> 概要: 动画《半妖的夜叉姬》手办登场
### [中央政法委：任何妄图利用彩礼骗钱等违法行为 都将付出应有代价](https://finance.sina.com.cn/china/2021-03-21/doc-ikknscsi9160650.shtml)
> 概要: 原标题：中央政法委：任何妄图利用彩礼骗钱、恶意炒作墓地价格等违法行为 都将付出应有的代价 近日，江西某银行传出的“彩礼贷”和云南昆明某陵园和银行传出的“墓地贷”引起网...
### [《生化8》吸血鬼夫人身高展示 两位女主播相形见绌](https://www.3dmgame.com/news/202103/3810962.html)
> 概要: 正在举行的PlayStation直播节目上，《生化危机：村庄》 吸血鬼夫人的身高再次成为直播的话题之一，其2.9米的身高让台上的两位女主播感叹不已。《生化危机8：村庄》PS日版将于5月8日发售，分为D......
### [国务院发话：持健康通行“绿码”可有序出行 各地不得擅自加码](https://finance.sina.com.cn/wm/2021-03-21/doc-ikknscsi9162858.shtml)
> 概要: 国务院发话！可有序出行，各地不得擅自加码！ 21日，国务院联防联控机制举行新闻发布会，介绍新冠疫苗安全性、有效性等有关情况。
### [官方回应黄河大堤死猪事件：确实存在 正对类似情况进行全面排查](https://finance.sina.com.cn/china/2021-03-21/doc-ikkntiam6006962.shtml)
> 概要: 原标题：官方回应黄河大堤死猪事件：确实存在，正对类似情况进行全面排查 3月21日，半月谈微信公众号刊发了题为《触目惊心！黄河大堤内哪来这么多死猪？》的报道。
### [“惊奇队长”布丽·拉尔森呼吁停止对亚裔仇恨](https://ent.sina.com.cn/m/f/2021-03-21/doc-ikknscsi9165184.shtml)
> 概要: 新浪娱乐讯 21日，“惊奇队长”布丽·拉尔森连发多条推文，呼吁停止对亚裔的仇恨，“我对本周发生在佐治亚州的种族主义和厌女的大规模枪击事件感到恶心。对亚裔美国人社群的袭击并不鲜见。我们不能允许另一种愚蠢......
### [延迟退休如何落地？有哪些影响？你最关心的都在这里](https://finance.sina.com.cn/wm/2021-03-21/doc-ikkntiam6008918.shtml)
> 概要: 3月12日公布的“十四五”规划和二〇三五年远景目标纲要明确提出将逐步延迟法定退休年龄。相关政策如何落地？延迟退休实施后，对年轻人、大龄劳动者有什么影响？
# 小说
### [最强真神](http://book.zongheng.com/book/907473.html)
> 作者：唯幻

> 标签：奇幻玄幻

> 简介：易子骞从乱葬废墟中走出，修太上神篇，掌诅咒之力，手握一把刀，杀遍诸天之恶！走向一条充满杀伐的真神之路！道阻且长，挡我者死！

> 章节末：第三百三十章  如期而至

> 状态：完本
# 论文
### [On (Emergent) Systematic Generalisation and Compositionality in Visual Referential Games with Straight-Through Gumbel-Softmax Estimator](https://paperswithcode.com/paper/on-emergent-systematic-generalisation-and)
> 日期：19 Dec 2020

> 标签：None

> 代码：https://github.com/Near32/ReferentialGym

> 描述：The drivers of compositionality in artificial languages that emerge when two (or more) agents play a non-visual referential game has been previously investigated using approaches based on the REINFORCE algorithm and the (Neural) Iterated Learning Model. Following the more recent introduction of the \textit{Straight-Through Gumbel-Softmax} (ST-GS) approach, this paper investigates to what extent the drivers of compositionality identified so far in the field apply in the ST-GS context and to what extent do they translate into (emergent) systematic generalisation abilities, when playing a visual referential game.
### [RNN Training along Locally Optimal Trajectories via Frank-Wolfe Algorithm](https://paperswithcode.com/paper/rnn-training-along-locally-optimal)
> 日期：12 Oct 2020

> 标签：None

> 代码：https://github.com/YunYunY/FW_RNN_optimizer

> 描述：We propose a novel and efficient training method for RNNs by iteratively seeking a local minima on the loss surface within a small region, and leverage this directional vector for the update, in an outer-loop. We propose to utilize the Frank-Wolfe (FW) algorithm in this context.
