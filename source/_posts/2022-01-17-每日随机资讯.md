---
title: 2022-01-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RydalWater_ZH-CN2787617470_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-01-17 22:32:52
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RydalWater_ZH-CN2787617470_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Cemu 模拟器计划 2022 年开源，旨在支持 Linux](https://www.oschina.net/news/178813/cemu-2022-open-source)
> 概要: 任天堂 Wii U 视频游戏模拟器 Cemu 计划在今年进行开源，并且还将致力于 Linux 支持和相关的跨平台改进。根据介绍，Cemu 是一个 Wii U 视频游戏机模拟器，自 2015 年以来一直......
### [GCC 12 已准备好抵御基于 Unicode 的 Trojan Source 攻击](https://www.oschina.net/news/178812/gcc-12-prevent-trojan-source-attacks)
> 概要: 此前我们报导了Unicode 算法漏洞“Trojan Source”几乎影响所有编程语言，利用Unicode 的控制字符，可对程序的源代码进行重新排序，从而在编译时产生另一种结果。但即将发布的 GCC......
### [因无人愿参与开发，Apache Ambari 项目即将退役](https://www.oschina.net/news/178820/apache-ambari-will-be-moved-to-the-attic)
> 概要: 1月11日，Apache Ambari 项目的开发者之一： Jayush Luniya 发送了一封邮件，提议将 Apache Ambari 项目移至“阁楼”，即将项目搁置，不再开发。在邮件中，Jayu......
### [youtube-dl 官网托管平台被三大唱片公司起诉](https://www.oschina.net/news/178819/youtube-dl-uberspace-sue)
> 概要: 索尼、环球和华纳三大唱片公司现在正在起诉德国托管平台 Uberspace；他们认为，youtube-dl 规避了 YouTube 的"rolling cipher"技术，而德国法院在 2017 年认定......
### [Serverless 背景下，一部分“前端工程师”会转变为“应用交付工程师”](https://segmentfault.com/a/1190000041294273?utm_source=sf-homepage)
> 概要: 大家好，我是杨成功。这是我的 2022 年第一篇文章。一直在想写些什么比较好，既然是新年，新年新气象，写点技术展望的想法是不是更合适？于是这篇文章的标题，也就是本文的核心思想出来了：Serverles......
### [P站美图推荐——绝对领域特辑（四）](https://news.dmzj.com/article/73360.html)
> 概要: 虽然最近几年不再被人提起，但是人类对于过膝袜与裙子中间露出的皮肤的追求依然刻在DNA之中，绝对领域赛高！
### [TV动画《组长女儿与照料专员》公开PV](https://news.dmzj.com/article/73361.html)
> 概要: TV动画《组长女儿与照料专员》公开了第一弹PV。在这段PV中，可以看到被称为“樱树组的恶魔”的主人公雾岛透，被任命照顾组长女儿樱树八重花等的场景。
### [能用js实现的最终用js实现，Shell脚本也不例外](https://segmentfault.com/a/1190000041295297?utm_source=sf-homepage)
> 概要: 大家好，我是秋风。今天来讨论一个牛逼的项目 ——zx ，1个月增长15000 star， 成为了2021年度明星项目排行榜第一。zx 到底是什么呢？我们可以从官网的介绍看到，一个能更方便地写脚本的工具......
### [漫画大奖2022公开提名作品名单](https://news.dmzj.com/article/73363.html)
> 概要: 一年一度的漫画大奖公开了2022年度的提名作品名单。本次入围提名的有……
### [线程池如何观测？这个方案让你对线程池的运行情况了如指掌！](https://segmentfault.com/a/1190000041296797?utm_source=sf-homepage)
> 概要: 今天我们来聊一个比较实用的话题，动态可监控可观测的线程池实践。这是个全新的开源项目，作者提供了一种非常好的思路解决了线程池的可观测问题。这个开源项目叫：DynamicTp地址在文章末尾。写在前面稍微有......
### [PLUM《请问您今天要来点兔子吗》心爱万圣节版手办](https://news.dmzj.com/article/73365.html)
> 概要: PLUM根据《请问您今天要来点兔子吗？ BLOOM》中的心爱制作的Halloween Fantasy版手办目前已经开订了。本作采用了心爱身穿可爱的魔女服装，在夜空中飞行的样子。
### [CDPR在测试《赛博朋克2077》2022年首个更新](https://www.3dmgame.com/news/202201/3833464.html)
> 概要: 《赛博朋克2077》1.3版更新发布后，CDPR就宣布之后的更新和免费DLC延期到2022年。几个月过去了，CDPR的大动作没有出现。后来网上传出了《赛博朋克2077》1.5版内容，CDPR澄清传言，......
### [仁井学公开「更衣人偶坠入爱河」喜多川海梦新绘](http://acg.178.com/202201/436383770207.html)
> 概要: 日本知名动画原画师、作画监督仁井学老师公开了其最新绘制的「更衣人偶坠入爱河」人物插画，本次绘制的是女主角·喜多川海梦。喜多川海梦是福田晋一创作的漫画「更衣人偶坠入爱河」及其衍生作品中的女主角，金发红瞳......
### [知名Coser Enako上综艺节目被吐槽 COS装扮很古怪](https://acg.gamersky.com/news/202201/1453397.shtml)
> 概要: 超人气COSER Enako最近以平常普通装扮的模样上了综艺节目，主持人滨田雅功吐槽说她平常的样子明明比较好看，为什么还要戴那种古怪的假发呢？
### [「陛下，您的心声泄露了!」漫画第一卷宣传CM公开](http://acg.178.com/202201/436390185707.html)
> 概要: 根据シロヒ创作的同名轻小说改编，みまさか绘制的漫画「陛下，您的心声泄露了!」近期公开了第一卷的宣传CM，旁白由细谷佳正和早见沙织担任，该卷正在发售中。「陛下，您的心声泄露了!」漫画第一卷宣传CM......
### [「LoveLive!SuperStar!!」Liella!组合首张专辑封面&艺人写真公开](http://acg.178.com/202201/436390718965.html)
> 概要: 「LoveLive!SuperStar!!」官方公开了Liella!组合首张专辑「What a Wonderful Dream!!」的封面和艺人写真。本商品售价为3,300日元（含税），将于2022年......
### [新丽传媒的2021:精品多元创"新",影视双驱发"丽"](https://ent.sina.com.cn/2022-01-17/doc-ikyamrmz5668090.shtml)
> 概要: 新浪娱乐讯 由中央电视台、企鹅影视、新丽电视、响巢传媒出品，炉火映画承制，根据烽火戏诸侯作品《雪中悍刀行》改编的电视剧《雪中悍刀行》已在近期收官。而截止收官当日，该剧在视频网站总播放量高达52.8亿次......
### [动画「杜鹃的婚约」第一弹正式PV及主视觉图公开](http://acg.178.com/202201/436392048918.html)
> 概要: 电视动画「杜鹃的婚约」公开了第一弹正式PV及主视觉图，同时追加声优木村良平、日笠阳子、森川智之和有贺由树子。该动画将于2022年4月开始播出。动画「杜鹃的婚约」第一弹正式PVSTAFF原作：吉河美希（......
### [支付宝五福活动抢先开始了！原来今年可以提前集](https://www.3dmgame.com/news/202201/3833494.html)
> 概要: 还有半个月时间就将迎来农历新年，而生活中的年味也随着春节将近而越来越浓。而说到过年，有一件事相信大家绝对不会忘记，那就是支付宝的“集五福”活动。虽然瓜分的红包金额从最开始的上百块变成了去年的几块钱，但......
### [肯德基、麦当劳在焦虑什么？](https://www.huxiu.com/article/491128.html)
> 概要: 本文作者： 邹帅，编辑：唐亚华，头图来自：视觉中国95后小沙回忆，10多年前，父母会说“你这次考试考一百分，我就带你去吃肯德基。”如今，每次在吃什么上犯难的时候，小沙和朋友就会说“要不就吃肯德基算了......
### [王老吉，“山寨”自己图个啥？](https://www.huxiu.com/article/491211.html)
> 概要: 本文来自微信公众号：新零售商业评论（ID：xinlingshou1001），作者：响马，头图来自：视觉中国在零售行业，营销方法总是像转转盘一样重复着来，“百家姓”营销便是其中的典型案例。2021年辞旧......
### [QPrompt：一款为视频创作者提供的自由开源的提词器 | Linux 中国](https://www.tuicool.com/articles/NZNJFfJ)
> 概要: 提词器可以提供视觉提示，甚至是完整的文本，这样演讲者就可以在讲话时接受提示。你可能已经看到新闻读者使用提词器。来源：https://linux.cn/article-14187-1.html作者：Ab......
### [《鬼灭之刃》遊郭篇新视觉图 迎战堕姬兄妹超帅](https://acg.gamersky.com/news/202201/1453431.shtml)
> 概要: 《鬼灭之刃》第二季游郭篇正在热播中，官方公共开了ufotable绘制的第2弹主视觉图，以及视觉图的视频。视觉图中宇髄天元、灶门炭治郎迎战妓夫太郎、堕姬兄妹。
### [漫画大师水岛新司去世 代表作棒球漫画《大饭桶》](https://www.3dmgame.com/news/202201/3833498.html)
> 概要: 漫画大师、《大饭桶》作者水岛新司于本月10日因肺炎在日本东京的一家医院去世，享年82岁。水岛新司出生于日本新潟市。1958年作为漫画家出道，陆续发表了以棒球为主题的作品。其中1972年开始连载棒球漫画......
### [从心理学角度解读《误杀2》：为何劫匪林日朗会获得人质的支持？](https://new.qq.com/rain/a/20211219A06OAT00)
> 概要: 从心理学角度解读《误杀2》：为何劫匪林日朗会获得人质的支持？
### [一颗心脏引发的大案，肖央对峙任达华，《误杀2》这波双王炸！](https://new.qq.com/rain/a/20211218V06U8G00)
> 概要: 一颗心脏引发的大案，肖央对峙任达华，《误杀2》这波双王炸！
### [Some ways DNS can break](https://jvns.ca/blog/2022/01/15/some-ways-dns-can-break/)
> 概要: Some ways DNS can break
### [SNK授权野兽国《拳皇98》八神庵手办 售价约166元起](https://www.3dmgame.com/news/202201/3833507.html)
> 概要: 近日，娱乐体验品牌野兽国旗下“梦-精选”收藏系列，最新推出SNK授权游戏《拳皇98》收藏线，首款商品为“八神庵，售价彩盒版港币203.15元（约合人民币166元），一般版港币228.58元（约合人民币......
### [《咒术回战》剧场版上映3周半 票房收入超85亿日元](https://acg.gamersky.com/news/202201/1453469.shtml)
> 概要: 截止到1月16日，《咒术回战》剧场版《咒术回战0》票房上映了24天，目前票房收入为85亿7642万9150日元，共有628万8404人到场观看。
### [潮流电子借力二次元的危与机](https://www.tuicool.com/articles/73MNvyz)
> 概要: 打着“猛男要入的猫耳耳机”标签的妖舞，凭借外形与二次元结合俘获了一些Z世代的芳心，成为了许多宅男宅女竞相入手的一款耳机产品，同时，也受到资本的青睐，完成了千万级别Pre-A轮融资，而此次融资是由小米集......
### [信维通信：目前 UWB 芯片主要被海外厂商垄断](https://www.ithome.com/0/598/982.htm)
> 概要: 集微网消息，1 月 16 日，信维通信在投资者互动平台表示，目前 UWB 芯片主要被海外厂商垄断。公司已与 NXP 达成合作，公司可以提供 UWB 天线、UWB 模组、UWB 开发套件、UWB 附件等......
### [€2.6M spent for a book at auction, believed they would own the IP](https://twitter.com/garybrannan/status/1482866885989478411)
> 概要: €2.6M spent for a book at auction, believed they would own the IP
### [视频：周杰伦晒昆凌照片庆祝结婚七周年 高调表白妻子不忘自夸](https://video.sina.com.cn/p/ent/2022-01-17/detail-ikyamrmz5725956.d.html)
> 概要: 视频：周杰伦晒昆凌照片庆祝结婚七周年 高调表白妻子不忘自夸
### [倪虹洁：已离婚，有孩子，热恋中](https://new.qq.com/rain/a/20220117A05VG200)
> 概要: 上个月18号，电影《爱情神话》在上海举行首映礼。从当时到场观众的即时反馈来看，《爱情神话》的大火已初见端倪。但我们更好奇首映礼上的一个小插曲，饰演格洛瑞亚的倪虹洁，在那个满满当当的千人大厅，向观众大方......
### [Python on z/OS – creating a C extension](https://colinpaice.blog/2022/01/15/python-on-z-os-creating-a-c-extension/)
> 概要: Python on z/OS – creating a C extension
### [《约会大作战》第四季全新PV公开 2022年4月开播](https://acg.gamersky.com/news/202201/1453570.shtml)
> 概要: 《约会大作战》第四季公开了全新的PV，新的战斗即将开始，诸位美少女全力迎战。动画将于2022年4月开播。
### [5年内超15000家创业公司死亡，它们到底经历了什么？](https://www.huxiu.com/article/491322.html)
> 概要: 本文来自微信公众号：DT财经（ID：DTcaijing），作者：董道力，编辑：唐也钦，数据：董道力，设计：戚桐珲，头图来自：视觉中国差不多两年前，我们系统分析过创业公司的死亡。那时候“资本寒冬”给市场......
### [宋智雅手写信道歉 承认部分衣物是假货](https://ent.sina.com.cn/s/j/2022-01-17/doc-ikyamrmz5748610.shtml)
> 概要: 新浪娱乐讯 1月17日，《单身即地狱》中的女嘉宾宋智雅被韩国网友扒出珠宝首饰和衣服疑似是假货，随后freezia宋智雅方面回应假货争议，称：“目前还没有要官方说明的部分”。晚间，宋智雅本人在个人社交网......
### [中国科大校友陈伟杰（9304）当选国际光学工程学会（SPIE）会士](https://www.ithome.com/0/599/013.htm)
> 概要: IT之家1 月 17 日消息，据中国科学技术大学发布，近日，国际光学工程学会（SPIE）公布了新当选会士名单，我校 9304 校友陈伟杰当选。▲ 陈伟杰（9304）陈伟杰（9304），1993 年考入......
### [京东推出京东云无线宝 AX6600 雅典娜众测路由器：高通 5 核处理器，829 元](https://www.ithome.com/0/599/015.htm)
> 概要: IT之家1 月 17 日消息，现有网友发现，京东云推出了一款新的京东云无线宝智能路由器 —— 京东云-AX6600 雅典娜，宣称 1000 元内 AX6600 全球首发限量（众测）版，采用高通五核 W......
### [理想 X01 路测谍照曝光：续航超 800 公里，售价约 50 万元](https://www.ithome.com/0/599/020.htm)
> 概要: 1 月 17 日消息，日前一组理想 X01 路测谍照曝光，从网上流传的谍照来看，新车定位大型 SUV，采用了低风阻轮毂，新车的前后部分都采用了相对简洁的设计。▲ 理想 X01 谍照据悉，新车搭载了全新......
### [三只松鼠：困在比营销更大的局里](https://www.tuicool.com/articles/VBrEnub)
> 概要: 摘要：三只松鼠的线下之路走得异常坎坷。作者/费尔南多  编辑/薛向一个靠营销起家的品牌，跌倒在营销上。新年伊始，三只松鼠（300783.SZ）眯眯眼模特做不雅动作被质疑辱华事件还没结束，就被网友发现其......
### [Deepnote (YC S19) is hiring to build a better data science notebook (Europe)](https://deepnote.com/join-us)
> 概要: Deepnote (YC S19) is hiring to build a better data science notebook (Europe)
### [宋智雅彻底翻车！承认穿假货并道歉，爆火后到中国捞金频惹争议](https://new.qq.com/rain/a/20220117V0B3KY00)
> 概要: 宋智雅彻底翻车！承认穿假货并道歉，爆火后到中国捞金频惹争议
### [凡人修仙传新预告，火凤凰堪称全场最佳，一个细节却暴露是半成品](https://new.qq.com/omn/20220117/20220117A0C62300.html)
> 概要: 真是没有想到，《凡人修仙传》动漫才刚更新，隔了一天这就又更新了。不过这次更新的却只是预告而不是正片，不免让人大失所望，但是看完预告之后，很多观众的心却再被提了起来。究其原因就是这预告足够良心，居然把越......
### [为何很多人开始讨厌云韵？路人角色主宰剧情，强行恰饭才最恶心](https://new.qq.com/omn/20220117/20220117A0CBIJ00.html)
> 概要: 自从《斗破苍穹》特别篇上线，云韵这个角色就引起很多漫迷的关注，毕竟她的形象建模也着实符合漫迷们心中的气质。            但是“沙漠之吻”的那一段剧情，也让很多原著党非常地担心，让云韵和萧炎如......
### [用魔法打败魔法：“链上天眼2.0”如何助力破获加密货币犯罪？](https://www.tuicool.com/articles/2aeMNjr)
> 概要: 出品|三言财经加密货币犯罪相关案件近年来呈高发趋势，不法分子利用人们追求财富的心理，通过宣传“炒币”投机可暴富大肆收敛钱财，对大众财产安全造成严重威胁。有媒体报道称，截至2021年12月，我国数字货币......
### [张艺兴发晚安电台纪念亮相十周年：梦想没有终点](https://ent.sina.com.cn/y/yneidi/2022-01-17/doc-ikyakumy0945762.shtml)
> 概要: 新浪娱乐讯 1月17日，张艺兴在微博发布晚安电台纪念亮相十周年。视频中，他回忆了初次站上舞台时“忐忑紧张”的心情，他也表示那时就给自己定下了一个又一个目标，直到现在仍旧在追梦的路上，“在我的心里梦想没......
### [剑悟的身份光，与大古的设定完全不同，难怪一直没提及他父亲！](https://new.qq.com/omn/20220117/20220117A0CPZ100.html)
> 概要: 在特利迦第一集当中，相信大家都会有一个疑问，那就是为什么只有剑悟的母亲登场了，却没有父亲的戏份，不管是从人物的背景设定，以及剧情中的细节来看，都没有表明剑悟的父亲是已经离世了的，对其也没有任何的描述，......
### [博人传232集：博人沦为龙套，原创剧情不出彩，动画人气下滑](https://new.qq.com/omn/20220117/20220117A0D8NN00.html)
> 概要: 日本动漫火影忍者《博人传》动画更新到232集，漩涡鸣人和奈良鹿丸让雷门电气来火影办公室，雷门电气是第五小队的队长，队友是梅塔尔李和结乃岩部。漩涡鸣人给雷门电气安排一个B级任务，负责运送傀儡到水之国的矿......
### [网曝广东一车友会年会节目低俗露骨 主办方：车友带来的气氛组，互动时有人起哄砸钱导致失控](https://finance.sina.com.cn/china/gncj/2022-01-17/doc-ikyamrmz5778529.shtml)
> 概要: 来源：九派新闻 【网曝#广东一车友会年会节目低俗露骨# 主办方：车友带来的气氛组，互动时有人起哄砸钱导致失控】1月16日，广东顺德，网传一车友年会互动节目低俗...
### [詹姆斯·古恩想花5亿美元拍摄《银河护卫队3》](https://www.3dmgame.com/news/202201/3833538.html)
> 概要: 《银河护卫队》系列导演詹姆斯·古恩表示，如果按照自己的方式拍摄《银河护卫队3》，那么他需要5亿美元。《银河护卫队3》故事发生在《复联4》之后，隶属于MCU第四阶段，有望在2023年上映。近日詹姆斯·古......
### [视频：宋智雅手写信道歉 承认部分衣物是假](https://video.sina.com.cn/p/ent/2022-01-17/detail-ikyakumy0953342.d.html)
> 概要: 视频：宋智雅手写信道歉 承认部分衣物是假
### [陕西西安新城区18日起有序恢复生产生活秩序](https://finance.sina.com.cn/jjxw/2022-01-17/doc-ikyamrmz5780475.shtml)
> 概要: 原标题：陕西西安新城区18日起有序恢复生产生活秩序 1月17日，陕西西安新城区疫情防控指挥部发布通告称，自2022年1月18日起，逐步有序恢复全区生产生活秩序。
### [无人机扎堆天安千树扰民？警方：需实名登记报备飞行计划](https://finance.sina.com.cn/jjxw/2022-01-17/doc-ikyamrmz5780060.shtml)
> 概要: 澎湃新闻资深记者朱奕奕记者巩汉语普陀警方发现无报备飞行器。 上海市公安局供图 2022年1月17号19时许，普陀公安分局长寿路派出所民警通过视频巡逻观察到...
### [增8.1%！我国GDP总量首破110万亿](https://finance.sina.com.cn/china/gncj/2022-01-17/doc-ikyamrmz5780744.shtml)
> 概要: 每经记者 李可愚 1月17日上午，2021年中国经济“成绩单”正式揭晓。 国家统计局网站发布的消息称，经初步核算，2021年全年国内生产总值（GDP）1143670亿元，按不变价格计算...
### [MLF利率下降10个基点 释放什么信号？](https://finance.sina.com.cn/china/gncj/2022-01-17/doc-ikyakumy0955348.shtml)
> 概要: 每经记者 肖世清 1月17日，央行公告称，为维护银行体系流动性合理充裕，当天开展1年期中期借贷便利（MLF）操作7000亿元和7天期逆回购操作1000亿元，中标利率分别为2...
### [国企改革三年行动今年收官，杜绝“纸面”改革和“数字”改革](https://finance.sina.com.cn/china/gncj/2022-01-17/doc-ikyamrmz5780636.shtml)
> 概要: 原标题：国企改革三年行动今年收官，杜绝“纸面”改革和“数字”改革 作者：祝嫣然 各地、各中央企业要将改革是否促进了企业内部治理机制、用人机制...
# 小说
### [宋士](http://book.zongheng.com/book/924942.html)
> 作者：浮沉的命运

> 标签：历史军事

> 简介：新书《大明匹夫》已发布北宋末年，大宋朝廷腐败，金人南下侵宋。大宋境内，白骨累累，尸积如山。宽袍束带、志得意满、东华门唱名的士大夫；怒发冲冠、慷慨激昂、报国无门的爱国志士；留得青楼薄姓名、浅斟低唱、放荡不羁的落魄才子；腐朽无能，寡廉鲜耻，让人扼腕长叹、仰天长啸的赵宋皇室......王峰来到这风雨飘摇的乱世，从河南府大莘店的一介乡民，开始他挽狂澜于既倒的传奇一生......

> 章节末：第73章 黎明

> 状态：完本
# 论文
### [Roto-translated Local Coordinate Frames For Interacting Dynamical Systems | Papers With Code](https://paperswithcode.com/paper/roto-translated-local-coordinate-frames-for)
> 日期：28 Oct 2021

> 标签：None

> 代码：None

> 描述：Modelling interactions is critical in learning complex dynamical systems, namely systems of interacting objects with highly non-linear and time-dependent behaviour. A large class of such systems can be formalized as $\textit{geometric graphs}$, $\textit{i.e.}$, graphs with nodes positioned in the Euclidean space given an $\textit{arbitrarily}$ chosen global coordinate system, for instance vehicles in a traffic scene.
### [Closed-form discovery of structural errors in models of chaotic systems by integrating Bayesian sparse regression and data assimilation | Papers With Code](https://paperswithcode.com/paper/closed-form-discovery-of-structural-errors-in)
> 日期：1 Oct 2021

> 标签：None

> 代码：None

> 描述：Models used for many important engineering and natural systems are imperfect. The discrepancy between the mathematical representations of a true physical system and its imperfect model is called the model error.
