---
title: 2020-10-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.AmericanFlyer_EN-CN0572496177_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-10-10 21:11:06
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.AmericanFlyer_EN-CN0572496177_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [杨丞琳晒狗狗照片cue老公：狗界李荣浩](https://ent.sina.com.cn/y/ygangtai/2020-10-10/doc-iivhvpwz1197042.shtml)
> 概要: 新浪娱乐讯 10月9日，杨丞琳在社交平台限时动态晒出一张狗狗的照片，并@李荣浩写道：“狗界李荣浩”。只见照片中的狗狗黄白相间，坐在地上用忧郁的小眼神望向远方，与李荣浩颇有几分神似。(责编：漠er凡)......
### [视频：李现入围2020釜山电影节亚洲内容奖最佳男主角 未来可期](https://video.sina.com.cn/p/ent/2020-10-10/detail-iivhuipp8886236.d.html)
> 概要: 视频：李现入围2020釜山电影节亚洲内容奖最佳男主角 未来可期
### [组图：钟丽缇穿运动服搭小短裙造型可爱 甩发跳舞表演欲超强](http://slide.ent.sina.com.cn/star/slide_4_704_346398.html)
> 概要: 组图：钟丽缇穿运动服搭小短裙造型可爱 甩发跳舞表演欲超强
### [组图：张韶涵戴方格帽子可爱时髦 穿毛呢外套牛仔裤保暖到位](http://slide.ent.sina.com.cn/y/slide_4_704_346421.html)
> 概要: 组图：张韶涵戴方格帽子可爱时髦 穿毛呢外套牛仔裤保暖到位
### [贾青删博后被曝曾在公路中央跳舞 交警将再次核查](https://ent.sina.com.cn/s/m/2020-10-10/doc-iivhvpwz1223215.shtml)
> 概要: 新浪娱乐讯 10月10日，演员贾青在公路中拍照遭交警警告一事，持续引发关注。有网友称：贾青等人除了拍照外，还曾与多人在涉事公路跳舞。中卫交警支队对此回应：“这个我们会尽量核查下，如果真的有违反交通法规......
### [又一顶流男星因隐婚翻车了，网友扒出三代，力证实锤](https://new.qq.com/omn/20201010/20201010A0DO0N00.html)
> 概要: 又一顶流男星因隐婚翻车了，网友扒出三代，力证实锤
### [昆凌海滩度假笑容甜美，身材完全不像生过孩子，女儿打扮时髦](https://new.qq.com/omn/20201010/20201010A0BWSC00.html)
> 概要: 昆凌海滩度假笑容甜美，身材完全不像生过孩子，女儿打扮时髦            10月10日，昆凌在个人社交平台上分享了一组和友人出海游玩的照片，在老公视角下的昆凌笑容灿烂，她扎着丸子头很有少女感。 ......
### [熊黛林40岁生日穿运动衣晒身材，方媛尬舞遭翻车，俩人同上热搜](https://new.qq.com/omn/20201010/20201010A0EHBD00.html)
> 概要: 熊黛林40岁生日穿运动衣晒身材，方媛尬舞遭翻车，俩人同上热搜
### [林心如新剧路透曝光！演技熟练一次到位，忙收工回家陪小海豚？](https://new.qq.com/omn/20201010/20201010A0DPKH00.html)
> 概要: 林心如新剧路透曝光！演技熟练一次到位，忙收工回家陪小海豚？
### [70岁的郑少秋，50岁的焦恩俊，那些年的古装美男都怎么样了？](https://new.qq.com/omn/20201010/20201010A0AM7F00.html)
> 概要: 要说cp粉过年有多喜悦呢？去广场看看严屹宽乔振宇互圈后，大家有多激动就知道了。            有人说只要活得久什么都能等到，有人说有梦想谁都了不起！            早在十几年前，大家都......
# 动漫
### [P站美图推荐——中长发特辑](https://news.dmzj1.com/article/68801.html)
> 概要: 脖子以下肩胛骨以上，不是披肩发胜似披肩发，可以成熟可以稚气的好发型
### [剧场版动画《东京七姐妹：我们化作青空》2021年春上映](https://news.dmzj1.com/article/68799.html)
> 概要: 在之前宣布延期的剧场版动画《东京七姐妹：我们化作青空》宣布了将于2021年早春上映的消息。官方也再次对让粉丝长时间等待而进行了道歉。
### [剧场版动画《Phantom Trigger》公开预告片](https://news.dmzj1.com/article/68797.html)
> 概要: 剧场版动画《Phantom Trigger》的续篇“眺望星星的人”公开了预告片。在这段预告片中，使用了由黑崎真音演唱的OP《幻想的轮舞》和由南条爱乃演唱的ED《泪流不止》。
### [歌与人偶！Key新跨媒体企划今秋开始](https://news.dmzj1.com/article/68800.html)
> 概要: 根据Key开发室的官方消息，一个以“歌与人偶”为主题的新跨媒体企划即将于今秋开始。这次的新企划将于10月26日的第五次Key官方直播中公开详细消息。另外在这次直播中，还将宣布一款继《星之梦》、《Harmonia》之后的游戏新作。
### [崩坏3：“崩坏冒险谭”活动CG放送！](https://new.qq.com/omn/20201010/20201010A04HFL00.html)
> 概要: 勇者与魔王的故事已经结束，但德尔塔的故事却仍在继续…… 《崩坏 3》周年庆全服活动「崩坏冒险谭」CG，请舰长收下~
### [《哆啦A梦》新剧场版确定引进 中文预告公开](https://acg.gamersky.com/news/202010/1327509.shtml)
> 概要: 《哆啦A梦：大雄的新恐龙》动画电影确定将引进国内大陆地区，官方公布了中文版预告片，该片目前尚未定档。
### [《银魂》最后的剧场版预告公开 2021年1月8日上映](https://acg.gamersky.com/news/202010/1327594.shtml)
> 概要: 华纳兄弟电影公布了《银魂 THE FINAL》剧场版动画电影的预告片，并宣布该片将于2021年1月8日上映。
# 财经
### [兰蔻、YSL、美宝莲……这些商标入选上海重点商标保护名录](https://finance.sina.com.cn/china/2020-10-10/doc-iivhvpwz1304764.shtml)
> 概要: 原标题：兰蔻、YSL、美宝莲……这些商标入选上海重点商标保护名录 10月10日，上海市知识产权局官网发布《第七批上海市重点商标保护名录》，46件重点商标入选。
### [人民币汇率预期平稳 远期售汇业务外汇风险准备金率再度下调为0](https://finance.sina.com.cn/china/jrxw/2020-10-10/doc-iivhvpwz1304838.shtml)
> 概要: 新京报贝壳财经讯（记者 程维妙）据央行10月10日公告，今年以来，人民币汇率以市场供求为基础双向浮动，弹性增强，市场预期平稳，跨境资本流动有序...
### [江西援鄂医疗队研究成果：戴眼镜或可降低感染新冠肺炎的风险](https://finance.sina.com.cn/china/gncj/2020-10-10/doc-iivhuipp8927283.shtml)
> 概要: 原标题：江西援鄂医疗队研究成果：戴眼镜或可降低感染新冠肺炎的风险 每经编辑 周宇翔 “南昌大学第二附属医院医疗服务”微信公号10月10日发布题为《全球百家媒体报道江西援...
### [马云湖畔5周年课堂回顾：做好企业的“上三路”和“下三路”](https://finance.sina.com.cn/china/gncj/2020-10-10/doc-iivhvpwz1305986.shtml)
> 概要: 原标题：马云：做好企业的“上三路”和“下三路” | 湖畔5周年课堂回顾 来源：湖畔大学 大家好，这里是湖畔大学5周年特别策划专栏——“重返课堂”系列，从今天开始...
### [福建省委：坚决拥护中央对张志南开除党籍开除公职处分决定](https://finance.sina.com.cn/china/dfjj/2020-10-10/doc-iivhuipp8927549.shtml)
> 概要: 原标题：福建省委：坚决拥护中央对张志南开除党籍开除公职处分决定 据《福建日报》消息，10月9日，福建省委常委会召开会议，传达中央纪委国家监委给予张志南开除党籍和公职...
### [北京楼市 降价与涨价齐飞](https://finance.sina.com.cn/china/dfjj/2020-10-10/doc-iivhuipp8922312.shtml)
> 概要: 来源： 壹地产 上个月，刚上市的贝壳找房把一份新房推广计划递到了北京开发商手上。这是开发商们基本都无法拒绝的四项活动： 66折房源一套； 85折房源30套；...
# 科技
### [禁止浏览器 title 属性的默认效果](https://segmentfault.com/a/1190000022925737)
> 概要: 正好回答了一个问题，浏览器哪个事件里可以 禁止 浏览器对title显示默认行为？。今天整理一下相关的测试 demo。分析问题先找一下关键词，从里面找一下解决问题的思路。ssrtitle默认行为seo自......
### [坚持写技术博客一年能有多少收获！](https://segmentfault.com/a/1190000037406055)
> 概要: 作者：小傅哥博客：https://bugstack.cn沉淀、分享、成长，让自己和他人都能有所收获！😄让人怪不好意思的，小傅哥的粉丝破万了！😁10.1假期期间，公众号粉丝终于破万！是的，终于！因为与各......
### [富士康创投出售 63 万股阿里巴巴 ADS，再获利 1.2 亿美元](https://www.ithome.com/0/512/890.htm)
> 概要: 10 月 10 日消息，10 月 9 日，鸿海公司在向交易所提交的文件中显示，子公司富士康创投控股（Foxconn Ventures）卖出 63 万股阿里巴巴的 ADS，交易日期是 10 月 8 日，......
### [特斯拉与快速充电公司 Fastned 合作，开设德国最大快速充电中心](https://www.ithome.com/0/512/899.htm)
> 概要: 10 月 10 日消息，据国外媒体报道，电动汽车制造商特斯拉和荷兰快速充电公司 Fastned 以及 Seed & Greet Bakery 一起开设了德国最大的快速充电中心 Ladepark Kre......
### [视频号冲刺式改版](https://www.huxiu.com/article/386388.html)
> 概要: 本文来自微信公众号：新榜（ID：newrankcn），作者：松露、小八，原文标题：《视频号冲刺式改版！内测直播，与公众号双向打通，开启小商店带货》，题图来自：视觉中国昨天（10月9日），有消息称微信正......
### [OKR，我们可能做错了](https://www.huxiu.com/article/386392.html)
> 概要: 本文来自微信公众号：混沌大学（ID：hundun-university），分享嘉宾：陶慧刚（乐活大学创始人），编辑：混沌大师兄，题图来自：视觉中国大家好，我是混沌大师兄。现在OKR（Objective......
### [B站为梦想窒息](https://www.tuicool.com/articles/7BRjQbQ)
> 概要: 本文来自微信公众号：非凡油条（ID：ffyoutiao），作者：豆腐乳儿，策划：老油条儿，排版：养乐多，美工：小面，题图来自：视觉中国B站成长的天空何在？最近，一部叫做《风犬少年的天空》的网剧以独播形......
### [顺风车、酒旅地推、共享电单车、在线教育的十一黄金周之战](https://www.tuicool.com/articles/IjiYzan)
> 概要: 头图 | 视觉中国欢迎关注“创事记”的微信订阅号：sinachuangshiji文/杨业擘 陈桥辉 王琳 周逸斐 周晓奇 李晓蕾来源/Tech星球(ID：tech618)2020年，“五一”黄金周没能......
# 小说
### [冷冻侠](http://book.zongheng.com/book/724612.html)
> 作者：钱学礼

> 标签：科幻游戏

> 简介：2068年，第三次世界大战爆发，战斗的对立面却不是人类与人类，而是人类与拟态机器人，即人工智能生命。AI的使命，本该是保护与帮助弱小的人类，却最终演变成人类最危险的敌人，人类面临种族灭绝的危机，黎明的曙光是否能够再次降临这颗蓝色的星球？一切尽请阅读《冷冻侠》！

> 章节末：第一百四十一章 疯狂凯撒

> 状态：完本
# 游戏
### [Steam喜加一！《刺猬索尼克2》现已可免费领取](https://www.3dmgame.com/news/202010/3799232.html)
> 概要: 世嘉经典平台游戏《刺猬索尼克2（Sonic The Hedgehog 2）》目前可在Steam免费领取，活动截止到10月20日凌晨1点。《刺猬索尼克2》2011年1月27日发售，登陆了Steam，是一......
### [传台积电已获对华为供货许可 但仍难解手机芯片危机](https://www.3dmgame.com/news/202010/3799261.html)
> 概要: 9日晚，集微网引知情人士称，继AMD、Intel之后，台积电也已从美国商务部获得许可证，能够继续向华为供应一部分成熟工艺产品。该知情人士透露，台积电获得的许可证主要涵盖采用成熟工艺制造的产品，手机So......
### [剧场版《银魂 THE FINAL》预告释出 最终决战将打响](https://www.3dmgame.com/news/202010/3799248.html)
> 概要: 今日（10月10日）是《银魂》主人公坂田银时的生日，官方也借此机会公开了剧场版《银魂 THE FINAL》的预告片，剧场版主题曲由SPYAIR担当，曲名“辙～Wadachi”。预告片：新剧场版《银魂 ......
### [PS5上市当天即可游玩海量PS4游戏 且画面经过强化](https://www.3dmgame.com/news/202010/3799229.html)
> 概要: 虽然发售日和价格都已经确定，不过随着11月发售日的临近，打算购买PS5的用户还有一个关心的问题一直没有得到答复，那就是向下兼容功能，换句话来说就是能玩到多少老的PS4游戏。昨天深夜，索尼PS官方通过博......
### [阴阳师改编电影《晴雅集》预告及海报 定档12月25日](https://www.3dmgame.com/news/202010/3799247.html)
> 概要: 由郭敬明执导，赵又廷、邓伦、王子文、春夏、汪铎主演的电影《晴雅集》定档12月25日，发布首个预告以及角色海报。在预告中邓伦裸上身造型亮相，展现肌肉线条。此前导演郭敬明在采访中提到，邓伦在拍摄该片时曾断......
# 论文
### [EfficientPS: Efficient Panoptic Segmentation](https://paperswithcode.com/paper/efficientps-efficient-panoptic-segmentation)
> 日期：5 Apr 2020

> 标签：PANOPTIC SEGMENTATION

> 代码：https://github.com/DeepSceneSeg/EfficientPS

> 描述：Understanding the scene in which an autonomous robot operates is critical for its competent functioning. Such scene comprehension necessitates recognizing instances of traffic participants along with general scene semantics which can be effectively addressed by the panoptic segmentation task.
### [Learning Inverse Depth Regression for Multi-View Stereo with Correlation Cost Volume](https://paperswithcode.com/paper/learning-inverse-depth-regression-for-multi)
> 日期：26 Dec 2019

> 标签：STEREO MATCHING

> 代码：https://github.com/GhiXu/CIDER

> 描述：Deep learning has shown to be effective for depth inference in multi-view stereo (MVS). However, the scalability and accuracy still remain an open problem in this domain.
