---
title: 2023-01-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RedMangrove_ZH-CN4083989028_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-01-27 21:44:53
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RedMangrove_ZH-CN4083989028_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [《无名》为何争议大](https://www.huxiu.com/article/777968.html)
> 概要: 本文来自微信公众号：毒眸（ID：DomoreDumou），作者：念芜，编辑：扣马，原标题：《程耳和王一博，谁辜负了春节档？》，头图来自：《无名》海报今年春节档，陷入了史无前例的大混战。易烊千玺、王一博......
### [互联网炫富论：抖音与小红书的两种注脚](https://www.woshipm.com/it/5735833.html)
> 概要: 在互联网上，从来就不缺乏带有“富贵”属性的人，占据流量C位的是财富，财富是永远的流量密码，但流量又是积累财富的途径，二者相辅相成。不同的平台展现财富的差异不同，本文对小红书和抖音上这类炫富风格5图文或......
### [数字化时代银行为什么要提高用户体验](https://www.woshipm.com/user-research/5735698.html)
> 概要: 随着金融科技的发展，少数银行网点也逐渐迈入数字化，但依旧做不好用户体验。本文结合城商行零售客户客群划分案例，探讨银行网点如何提高用户体验感受，希望对你有所启发。在金融科技的冲击下，尤其是近年来移动支付......
### [动态时局下如何做好工具产品](https://www.woshipm.com/pd/5735576.html)
> 概要: 在如今的动态时局下，如何不受单个产品维度的局限，适应好不同的场景，做好过渡和承接呢？本文作者以工具产品为例，分析工具产品如何立足，希望能给你带来一些帮助。近期很多朋友聊天交流时候提及如何拓展自己的产品......
### [视频：杨祐宁宣布老婆怀二胎 晒宝宝超音波影片显兴奋](https://video.sina.com.cn/p/ent/2023-01-27/detail-imycpxrx1800794.d.html)
> 概要: 视频：杨祐宁宣布老婆怀二胎 晒宝宝超音波影片显兴奋
### [心理惊悚游戏《洛蕾塔》2月16日登陆PC平台](https://www.3dmgame.com/news/202301/3861291.html)
> 概要: 发行商DANGEN Entertainment和开发者Yakov Butuzoff宣布，心理惊悚游戏《洛蕾塔》（Loretta）的PC版将于2月16日在Steam、GOG和其他主要商店推出。PlayS......
### [《满江红》上映第6天总票房破23亿](https://ent.sina.com.cn/m/c/2023-01-27/doc-imycqqpr1424902.shtml)
> 概要: 新浪娱乐讯 据灯塔专业版，电影《满江红》上映第6天 ，总票房破23亿。 (责编：kita)......
### [《红霞岛》联合开发商和索尼合作 开发《地平线》系列](https://www.3dmgame.com/news/202301/3861297.html)
> 概要: 《红霞岛》《霍格沃茨之遗》背后的联合开发商Gobo宣布他们和索尼旗下工作室Guerrilla Games合作，共同开发《地平线》系列游戏。这家位于英国的开发商日前正在招募人手。官推上写道：“我们很高兴......
### [春节全国总票房破60亿：《流浪地球2》《满江红》霸榜](https://www.3dmgame.com/news/202301/3861301.html)
> 概要: 截至2023年1月26日22时36分，2023年春节档（1月21日至1月27日），总票房又刷新新高，突破60亿元。《满江红》、《流浪地球2》、《熊出没·伴我“熊芯”》依然分列春节档票房前三位，且前两部......
### [财季盈利2.65亿英镑 捷豹路虎实现销量营收全面提升](https://www.yicai.com/news/101658235.html)
> 概要: 伴随芯片供应问题的缓解以及产量的稳步攀升，本财季捷豹路虎全球营收达60亿英镑，盈利2.65亿英镑，息税前利润率进一步增长至3.7%。
### [《荣耀战魂》新英雄公布 异乡人阵营再添女将](https://www.3dmgame.com/news/202301/3861303.html)
> 概要: 《荣耀战魂》今日在直播活动中宣布，手持权杖和盾牌的女性新英雄Afeera将于2月2日加入游戏。另外，《荣耀战魂》标准版将从2月2日至2月9日开放全平台免费体验，试玩期间的进度和英雄将在正式购买之后保留......
### [《弩级战队H×EROS》作者北田龙马Jump+开新连载](https://news.dmzj.com/article/76936.html)
> 概要: 根据偷跑消息，创作过《弩级战队H×EROS》等作品的漫画家北田龙马，将于2月5日在Jump+上开始新连载《梦结局 我们在梦中相恋》。
### [《死亡空间：重制版》与《红霞岛》都将采用D加密](https://www.3dmgame.com/news/202301/3861304.html)
> 概要: 《死亡空间：重制版》将于明日正式发售。虽然在Steam页面中并没有加密方式的相关情报，但外媒DSOGaming证实，本作仍将采用D加密技术。除此之外，Arkane新作《红霞岛》也将使用D加密技术。《死......
### [日本政府调整新冠防疫政策 CM等活动有望解除人数限制](https://news.dmzj.com/article/76937.html)
> 概要: 根据日本媒体报道，日本将于5月8日将新冠病毒感染从之前的第二类传染病，下调为与流感相同的第五类传染病。是否继续佩戴口罩，可以由个人自主决定。
### [FuRyu《无职转生》艾莉丝花精灵版手办](https://news.dmzj.com/article/76938.html)
> 概要: FuRyu的低价手办品牌TENITOL根据《无职转生》中的艾莉丝制作的花精灵版手办正在预订中。本作采用了艾莉丝身穿原创礼服的造型，全高约21cm。
### [2022年项目合集#14](https://www.zcool.com.cn/work/ZNjM3MTk2OTY=.html)
> 概要: Collect......
### [中国公司全球化周报｜微软裁员万人，科技公司裁员潮继续；中国取消外贸备案手续，所有企业自动获得进出口权利](https://36kr.com/p/2104273152196745)
> 概要: 中国公司全球化周报｜微软裁员万人，科技公司裁员潮继续；中国取消外贸备案手续，所有企业自动获得进出口权利-36氪
### [泉州外贸人过了个安稳年，新的订单保卫战即将打响](https://www.yicai.com/news/101658285.html)
> 概要: 古来素有世界海洋贸易的中心的泉州，外贸复苏的迹象日益显现。
### [APENFT Art Dream Fund - 后人类时代](https://www.zcool.com.cn/work/ZNjM3MjAxNTI=.html)
> 概要: 从人工智能、生物工程、基因技术到纳米技术，各种科学技术的繁荣发展带来新的生命形式的出现，预示着特定的“人类”概念由此终结并引领人类进入到一个全新的后人类时代。正如《爱，死亡和机器人》里，当机器人和创造......
### [B 社漫画风格的节奏动作《Hi-Fi Rush》现已发售，Steam 好评率 98%](https://www.ithome.com/0/669/651.htm)
> 概要: IT之家1 月 27 日消息，三上真司工作室 Tango Gameworks 公开了新作《Hi-Fi RUSH》，现已在 XSX|S、PC 平台推出，首发支持 XGP，支持中文。这是一款漫画风格的节奏......
### [微软宣布为 Win11 21H2 设备开启自动更新到 22H2 版本](https://www.ithome.com/0/669/654.htm)
> 概要: IT之家1 月 27 日消息，微软Windows 1121H2 即将在 10 月 10 日后结束支持。微软表示，将面向 Windows 11 21H2 家庭版和专业版的设备开启自动更新到 Window......
### [【字幕】恩比德：约基奇的两个MVP实至名归，下场会竭尽全力战胜掘金](https://bbs.hupu.com/57597325.html)
> 概要: 【字幕】恩比德：约基奇的两个MVP实至名归，下场会竭尽全力战胜掘金
### [丰田陷两难，先干掉章男](https://www.huxiu.com/article/778299.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博头图｜丰田章男微博“燃油车失魅力，混动车成追忆，电动车没啥戏，氢能源下不为例。”这，便是成立于 1937 年，距今已有 86 年历史的日本汽车公司——丰田汽车（TOYOTA......
### [希捷 22TB、24TB 硬盘即将面世，后续还将推出 30TB、50TB 产品](https://www.ithome.com/0/669/662.htm)
> 概要: IT之家1 月 27 日消息，希捷昨日公布了截至 2022 年 12 月 30 日的2023 财年第二财季报告，营收和非 GAAP 每股收益略高于预期。希捷 2023 财年 Q2 营收达18.87 亿......
### [县城，年轻人的新致富圣地？](https://www.huxiu.com/article/778304.html)
> 概要: 本文来自微信公众号：时代财经APP（ID：tf-app），作者：徐晓倩，编辑：史成超，原文标题：《卷出内伤的大厂人，决定回县城创业》，头图来自：《去有风的地方》2022年，被迫频繁跳槽的张栋决定结束漂......
### [民宿房价动辄上涨数十倍，“报复性消费”之下警惕竭泽而渔](https://www.yicai.com/news/101658350.html)
> 概要: 整个民宿行业的回暖，不能仅仅依靠一两个节假日，而是有赖于更多人消费信心的回归，以及经济的不断恢复。
### [猫头鹰推出 AM5 平台 NH-L9a 下压式散热器，仅 37mm 高](https://www.ithome.com/0/669/672.htm)
> 概要: 感谢IT之家网友华南吴彦祖、OC_Formula的线索投递！IT之家1 月 27 日消息，猫头鹰现已推出 AMD AM5 平台的超薄下压散热器 NH-L9a-AM5，普通版 44.90 美元（当前约 ......
### [66岁的丰田章男将卸任丰田汽车社长](https://www.yicai.com/news/101658397.html)
> 概要: 丰田章男当年刚担任丰田汽车社长时是53岁，而佐藤恒治今年刚好也是53岁。
### [四季度营收同比下降28%，英特尔季度营收已创四连跌](https://www.yicai.com/news/101658402.html)
> 概要: 英特尔预计2023年第一季度营收在105至115亿美元之间，同样低于此前市场预期的140亿美元。
### [撒狗粮!张若昀为唐艺昕《今生也是第一次》打call](https://ent.sina.com.cn/v/m/2023-01-27/doc-imycrmtr4828919.shtml)
> 概要: 新浪娱乐讯 1月27日，唐艺昕主演新剧《今生也是第一次》开播，张若昀转发老婆微博为她打call，并配文：“我倒要看看脑洞有多大”。　　网友们纷纷在评论区表示嗑到了：“真夫妻就是甜”“今日初六宜吃糖”......
### [拿《狂飙》洗眼睛，当心上瘾](https://www.huxiu.com/article/778229.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜《狂飙》海报2023 开年，差点被烂剧腌入味的观众被剧版《三体》和《狂飙》堵住了嘴。尤其《狂飙》的口碑，在网上划出了一条“高开飙走”的惊艳弧线，一个小细节、一句台......
### [飞机剧烈颠簸下坠，乘客惊恐尖叫录下视频留遗言，国航刚刚回应了](https://finance.sina.com.cn/jjxw/2023-01-27/doc-imycrrzk0484973.shtml)
> 概要: 每经编辑 毕陆名 近日，有网友在社交平台上发布视频称，在乘坐西安飞往温州的航班时，飞机在高空中遭遇强气流发生剧烈颠簸。客舱内发出阵阵尖叫，机组人员不断安抚乘客。
### [最新公布！春节国内岀游3.08亿人次，恢复至2019年同期88.6%！岀境游也暴增，春节消费强势复苏](https://finance.sina.com.cn/china/gncj/2023-01-27/doc-imycrrzm7924981.shtml)
> 概要: 热门旅游目的地客流如织、各地商圈人头攒动……作为2020年以来首个不再倡导“就地过年”的春节，兔年春节假期，旅游和商业等消费市场均强势复苏，交出三年来最亮眼的成绩单。
### [《流浪地球2》突破21亿，“孙公子”大赚](https://finance.sina.com.cn/china/gncj/2023-01-27/doc-imycrwii7804074.shtml)
> 概要: 来源于21世纪商业评论，作者徐秋芳 李雅洁 1月24日，《流浪地球2》上映第3天，票房突破10亿元。至27日18时，票房已经超过21亿元。 电影大卖，投资方赚得盆满钵满。
### [春节假期消费图鉴：境内热门景点游客接待超疫前水平 线下餐饮消费修复相对较快](https://finance.sina.com.cn/china/gncj/2023-01-27/doc-imycrwii7799636.shtml)
> 概要: 来源：财联社 编者按：国金宏观赵伟团队整理的春节假期消费数据显示，旅客发送方面，公共交通旅客发送达2019年五成、小客车出行超2019年水平；旅游接待方面...
### [流言板不服气！湖人球迷晒威廉森、浓眉赛季数据：AD被抢劫了](https://bbs.hupu.com/57601421.html)
> 概要: 虎扑01月27日讯 今日，NBA官方公布全明星首发阵容。蔡恩-威廉森力压安东尼-戴维斯，入选西部全明星首发。对此，一位湖人球迷截图晒照了威廉森和戴维斯的赛季场均数据，并写道：“AD被抢劫了。”本赛季至
### [赛后HLE 0-2 KT，流沙移形恕瑞玛之怒，KT龙魂制胜力克HLE](https://bbs.hupu.com/57601450.html)
> 概要: G﻿ame2：蓝色方：HLE，红色方：KTHLE禁用：猫咪，瑞兹，扇子妈，鳄鱼，武器大师HLE选用：Kingen剑姬、Clid猪妹、Zeka永恩、Viper女警、Life拉克丝KT禁用：卢锡安，大树，
### [中国游客到了！部长接机+1](https://finance.sina.com.cn/china/gncj/2023-01-27/doc-imycrwim4580331.shtml)
> 概要: 新春迎老友 菲律宾热情欢迎中国游客 “我们从东北来，感觉非常开心。这次旅游将给我们留下一段难忘的回忆。”来自辽宁沈阳的游客闻鹏一下飞机就感受到菲律宾人民的热情。
### [流言板C罗效应！利雅得胜利的官方Ins粉丝数从86万涨至1250万](https://bbs.hupu.com/57601630.html)
> 概要: 虎扑01月27日讯 2022年12月31日，C罗加盟沙特俱乐部利雅得胜利。距今已经过了将近一个月，而利雅得胜利的官方社交账号粉丝数，也迎来了爆炸性的增长。这个月内，利雅得胜利的官方Instagram粉
# 小说
### [科学修炼法则](http://book.zongheng.com/book/856322.html)
> 作者：破头男孩

> 标签：奇幻玄幻

> 简介：“诶！升级还需要四单位的灵气啊，你现在可能不能停下！”“很好，你今天进步很快。”“这里的数据显示，你最近修炼心不在焉啊，灵气供能非常紊乱。”“没有天赋没有关系，重要是科学修炼的方法。”钟杨对着台下的弟子说道。来自异界的科学研究者，能否给以灵为尊的大陆带来新的生机？

> 章节末：第三十二章   这是我的世界！

> 状态：完本
# 论文
### [Multi-stage Distillation Framework for Cross-Lingual Semantic Similarity Matching | Papers With Code](https://paperswithcode.com/paper/multi-stage-distillation-framework-for-cross-2)
> 日期：Findings (NAACL) 2022

> 标签：None

> 代码：https://github.com/KB-Ding/Multi-stage-Distillaton-Framework

> 描述：Previous studies have proved that cross-lingual knowledge distillation can significantly improve the performance of pre-trained models for cross-lingual similarity matching tasks. However, the student model needs to be large in this operation. Otherwise, its performance will drop sharply, thus making it impractical to be deployed to memory-limited devices. To address this issue, we delve into cross-lingual knowledge distillation and propose a multi-stage distillation framework for constructing a small-size but high-performance cross-lingual model. In our framework, contrastive learning, bottleneck, and parameter recurrent strategies are combined to prevent performance from being compromised during the compression process. The experimental results demonstrate that our method can compress the size of XLM-R and MiniLM by more than 50\%, while the performance is only reduced by about 1%.
### [Context Matters for Image Descriptions for Accessibility: Challenges for Referenceless Evaluation Metrics | Papers With Code](https://paperswithcode.com/paper/context-matters-for-image-descriptions-for)
> 概要: Few images on the Web receive alt-text descriptions that would make them accessible to blind and low vision (BLV) users. Image-based NLG systems have progressed to the point where they can begin to address this persistent societal problem, but these systems will not be fully successful unless we evaluate them on metrics that guide their development correctly. Here, we argue against current referenceless metrics -- those that don't rely on human-generated ground-truth descriptions -- on the grounds that they do not align with the needs of BLV users. The fundamental shortcoming of these metrics is that they cannot take context into account, whereas contextual information is highly valued by BLV users. To substantiate these claims, we present a study with BLV participants who rated descriptions along a variety of dimensions. An in-depth analysis reveals that the lack of context-awareness makes current referenceless metrics inadequate for advancing image accessibility, requiring a rethinking of referenceless evaluation metrics for image-based NLG systems.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
