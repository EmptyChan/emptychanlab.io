
---
title: 2022-07-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.OludenizTurkey_ZH-CN3467496108_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-10 22:44:30
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.OludenizTurkey_ZH-CN3467496108_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [互联网人到最后拼的是体力](https://www.woshipm.com/it/5519071.html)
> 概要: 编辑导语：人生就像一场马拉松，互联网人从一开始就在赛道上。都说35岁是一个分水岭，许多互联网人由此退出，原因不是能力不行了，更多的是体力跟不上了。拼体力的互联网人，现状如何了？互联网人到最后拼的是体力......
### [一种B端交易管制系统的设计思路](https://www.woshipm.com/pd/5520558.html)
> 概要: 编辑导语：在B端电商交易中，一些商品的交易涉及到了法律问题，这时便需要在系统中避免交易有法律问题的商品，于是有了商品交易管制系统。那么，b端交易管制系统应该怎么设计呢？一起来看一下吧。B端电商交易中，......
### [增长产品的3个必备思维：回报后置、精细化、破局](https://www.woshipm.com/operate/5462128.html)
> 概要: 编辑导语：增长产品除了要研究各种玩法功能、规划用户策略外，更重要是从底层逻辑出发，锻炼3种必备思维——回报后置、精细化、破局。本文作者对这三种思维进行了分析，希望能给你带来帮助。对于增长产品来说，除了......
### [Is the Smart Grid All Hot Air?](https://austinvernon.site/blog/smartgrid.html)
> 概要: Is the Smart Grid All Hot Air?
### [AMD公布GPU芯片新专利 推动优化游戏中的着色器](https://www.3dmgame.com/news/202207/3846596.html)
> 概要: AMD最近公布了一项专利，将渲染的负载分散到多个GPU芯片组中。这样一来，一个游戏场景将被划分为单独的块，并分配给小芯片，以优化游戏中着色器的利用率。AMD公司公布的新专利为该公司计划在未来几年内利用......
### [Help me identify possible tracking device found in my car](https://gist.github.com/jwbee/90e32362fd24b1a233b882ffa7950616)
> 概要: Help me identify possible tracking device found in my car
### [清华大学教授的课程笔记：什么是工业设计？](https://www.huxiu.com/article/603141.html)
> 概要: 本文为2021年8月29日，柳冠中老师《设计创新：从造物到谋事的思维重塑》课程笔记，来自微信公众号：混沌学园 （ID：hundun-university），作者：柳冠中 清华大学首批文科资深教授，编辑......
### [马斯克能如愿吗？关键在这六个字](https://finance.sina.com.cn/tech/2022-07-10/doc-imizirav2654581.shtml)
> 概要: 编译 / 友亚......
### [Bungie正在筹划未来几十年的《命运》宇宙](https://www.3dmgame.com/news/202207/3846598.html)
> 概要: 自《命运1》2014年发售以来，已经过去了将近8年时间。在这段时间里，玩家迎来了《命运1》《命运2》的6个大型资料片。大约2年后，Bungie对《命运》宇宙的十年计划将终结。然而，Bungie确认《命......
### [庵野秀明《新·假面骑士》新剧照 主演池松壮亮登场](https://acg.gamersky.com/news/202207/1498148.shtml)
> 概要: 《新·假面骑士》官推晒出了新剧照，池松壮亮出演的本乡猛亮相。
### [心理学“可重复性危机”，该如何破解？](https://www.huxiu.com/article/603958.html)
> 概要: 本文来自微信公众号：Neugeist（ID：Neugeist），作者：Jamieson&amp、Pexman，译者：Xhaiden，编辑：杨银烛，原文标题：《超越可重复性危机，我们需要更强大的心理学理......
### [组图：INTO1成员伯远机场出发 留中分发型造型清爽](http://slide.ent.sina.com.cn/star/slide_4_704_372347.html)
> 概要: 组图：INTO1成员伯远机场出发 留中分发型造型清爽
### [时之宇宙志](https://www.zcool.com.cn/work/ZNjA4MzY4NDg=.html)
> 概要: 正在参与：「时空履行者」百丽时尚鞋履美学设计大赛......
### [动画「影宅」第二季最新版权绘公布](http://acg.178.com/202207/451422128127.html)
> 概要: 电视动画「影宅」公开了第二季的最新版权绘，绘制的是埃米莉可和芭比。本作第二季现正在热播中。STAFF原作：ソウマトウ（集英社「週刊ヤングジャンプ」連載）导演：大橋一輝系列构成：大野敏哉角色设计：日下部......
### [「刀剑乱舞」陆奥守吉行手办开订](http://acg.178.com/202207/451423253615.html)
> 概要: GOODSMILE作品「刀剑乱舞」陆奥守吉行手办开订，全长约230mm，主体采用ABS和PVC材料制造。该手办定价为28600日元（去税），约合人民币1404元，预计于2023年6月发售......
### [iPhone 14最新售价曝光  国内或要涨至6799元起](https://www.3dmgame.com/news/202207/3846609.html)
> 概要: 虽然数据统计显示上半年大家购买手机的热情似乎不高，618国内手机销量同比下滑了超10%，不过也不会影响苹果手机的定价策略。一方面需要新的促销手段来降价提振消费热情，另一方面也得有新品推出，调动消费欲......
### [Spring 核心概念](https://www.tuicool.com/articles/NNvYRjz)
> 概要: Spring 核心概念
### [「天官赐福」谢怜2022官方生日贺图公开](http://acg.178.com/202207/451423987141.html)
> 概要: 「天官赐福」动画官方公开了谢怜2022年的生日贺图，祝太子殿下农历生辰快乐~动画「天官赐福」改编自墨香铜臭创作的同名小说作品，讲述了太子殿下谢怜与鬼王花城之间的动人故事。本作第一季于2020年10月3......
### [「杜鹃的婚约」后半季PV公开](http://acg.178.com/202207/451424166427.html)
> 概要: 根据吉河美希的同名漫画改编，由SHIN-EI动画和SynergySP联合制作的电视动画「杜鹃的婚约」近日公开了该作的后半季PV，动画将于7月23日播出。「杜鹃的婚约」后半季PV......
### [名作漫改《租借女友》日剧新卡司 雨宮天确定出演](https://www.3dmgame.com/news/202207/3846610.html)
> 概要: 根据人气动漫改编的《租借女友》日剧刚刚于7月开播，日前官方公布了新角色声优，雨宮天确定出演剧中出现的游戏中的角色声优，敬请期待。•《租借女友》讲述了关于“租赁女朋友”的故事，仅用了一个月，就被女朋友甩......
### [这一次，孙宇晨会放过巴菲特吗？](http://www.investorscn.com/2022/07/10/101735/)
> 概要: 最后一届“巴菲特午餐”快开饭了......
### [DSCC：预计 AMOLED 材料市场将于 2026 年达到 29 亿美元规模](https://www.ithome.com/0/628/829.htm)
> 概要: IT之家7 月 10 日消息，DSCC 最新报告显示，用于所有应用的 AMOLED 材料的营收预计将以 18% 的年增长率从 2020 年的 10.7 亿美元（约 71.69 亿元人民币）规模增长到 ......
### [Tik Tok海外直播电商失败？为啥抖音电商模式海外走不通？](https://www.tuicool.com/articles/7rMJziy)
> 概要: Tik Tok海外直播电商失败？为啥抖音电商模式海外走不通？
### [ACL‘22杰出论文：Prompt范式有bug！](https://www.tuicool.com/articles/ZRb6bqi)
> 概要: ACL‘22杰出论文：Prompt范式有bug！
### [Skyrim together: mod to play online](https://github.com/tiltedphoques/TiltedEvolution)
> 概要: Skyrim together: mod to play online
### [三个印度人改变压缩算法，一意孤行整个暑假，却因“太简单”申不到经费](https://www.ithome.com/0/628/840.htm)
> 概要: 世界上最好用的压缩软件是什么？微信。这个段子想必很多人都听过。一张几兆的图片，经微信一发，立马降到几百 kb。△ 如果是有损压缩画质会下降（右图天空有波纹）虽说这是个吐槽，但 u1s1，图片视频压缩其......
### [“2022奥特曼日”新贺图公布 初代、德凯酷炫亮相](https://acg.gamersky.com/news/202207/1498196.shtml)
> 概要: 今日（7月10日）是日本每年一度的“奥特曼日”。为庆祝这一盛事，圆谷特邀奥特曼首席设计师后藤正行绘制了今年的“奥特曼日”贺图。
### [时空履行者-“梦想家”](https://www.zcool.com.cn/work/ZNjA4Mzk3ODA=.html)
> 概要: 正在参与：「时空履行者」百丽时尚鞋履美学设计大赛......
### [梦幻联动！权志龙给金秦禹留言“怎么这么漂亮”](https://ent.sina.com.cn/k/2022-07-10/doc-imizirav2746915.shtml)
> 概要: 新浪娱乐讯 权志龙在社交平台给金秦禹留言“怎么这么漂亮”，金秦禹则回复：“只是因为是哥哥的弟弟啊。”网友：梦幻联动了......
### [美国6月CPI或再冲高，拜登中东行如何影响油价丨本周外盘看点](https://www.yicai.com/news/101469549.html)
> 概要: 本周美联储将公布经济状况褐皮书，拜登则将开启中东之行。
### [三星新款 G8 旗舰显示器上架：4K 240Hz HDR2000，11999 元](https://www.ithome.com/0/628/854.htm)
> 概要: IT之家7 月 10 日消息，三星 S32BG854NC 旗舰电竞显示器现已上架，4K 1000R 240Hz Mini LED HDR2000 规格，售价 11999 元。IT之家曾报道，早在今年 ......
### [一片骂声中，钟薛高估值超过40亿了](https://www.huxiu.com/article/604025.html)
> 概要: “钟薛高成了情绪出口，恰恰说明了其品牌力势能之强。”本文来自微信公众号：投中网 （ID：China-Venture），作者：张楠，原文标题：《听LP说，钟薛高估值超过40亿了》，题图来自：视觉中国夏天......
### [Linux 笔记本 System76 Lemur Pro 用上 12 代酷睿，售价 1149 美元起](https://www.ithome.com/0/628/856.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家7 月 10 日消息，System76 Lemur Pro 是一款轻薄的 Linux 笔记本电脑，配备 14 英寸全高清显示屏和 73 Wh 电池，并支持......
### [武动乾坤：小貂的身份属于妖域中的霸主，身边的随从都是转轮境](https://new.qq.com/omn/20220629/20220629A0D34J00.html)
> 概要: 大荒古碑入口已经开启，无尽机遇在一面等着蜂拥而至的人们前去探寻，小貂在这集虽然没有露面，但里面肯定有一份机缘是属于它的，从目前已经出来的剧情看，我们可以知道小貂来头不小，但目前只是妖灵状态，实力百不存......
### [剧场版《佐佐木与宫野》2023年上映](https://news.dmzj.com/article/74897.html)
> 概要: 根据春园翔漫画原作改编的TV动画确定将于2023年推出剧场版动画《毕业篇》，短篇动画《平野与键浦》也将一并公开。
### [潮玩盲盒IP设计丨【裂痕夏耶·伤痕之旅】](https://www.zcool.com.cn/work/ZNjA4NDExMDQ=.html)
> 概要: 【裂痕夏耶·伤痕之旅】——万物皆有裂痕，那是光照进来的地方还在宇宙混沌的时期，零星的几道光束轻飘飘照落在这灰蒙蒙的世界里，夏耶从黑暗泥泞之间醒来，脑袋上的裂痕一阵阵地钝痛着，它呆呆望着错杂朦胧的光影，......
### [TikTok ‘frog army’ stunt could have grave consequences, experts warn](https://www.theguardian.com/technology/2022/jul/10/tiktok-frog-army-stunt)
> 概要: TikTok ‘frog army’ stunt could have grave consequences, experts warn
### [又一巨头入局元宇宙，AR/VR产业步入高速增长期，蓝思科技提前布局占得先机](http://www.investorscn.com/2022/07/10/101736/)
> 概要: 近期，苹果即将在明年发布AR/MR头显的消息让市场异常兴奋。据知名分析师郭明錤透露，苹果的AR/VR头显产品预计在明年的二季度发布。此前，Meta、谷歌也在AR/VR的硬件和内容上相继发力，不断研发新......
### [消费电子预期见底，新业务迎来收获期，蓝思科技的盈利拐点或来临](http://www.investorscn.com/2022/07/10/101737/)
> 概要: 近日，蓝思科技在小米核心供应商表彰大会上再次荣获2021年度“最佳合作奖”，其凭借在智能终端的整机组装实力、核心外观元器件上独一无二的设计美学理念、全面的交付速度和交付质量，后续的延伸发展空间引人期待......
### [动画《死神少爷与黑女仆》第二季最新PV公开](https://news.dmzj.com/article/74898.html)
> 概要: 根据井上小春原作改编的TV动画《死神少爷与黑女仆》公开了第二季放送决定PV。
### [TV动画《杜鹃的婚约》第二部分公布PV和视觉图](https://news.dmzj.com/article/74899.html)
> 概要: 根据吉河美希原作改编的TV动画《杜鹃的婚约》公布了第二部分的视觉图、PV、主题歌等相关信息。
### [下半年硅料还要涨价？光伏产业上下游博弈将更为剧烈](https://www.yicai.com/news/101469643.html)
> 概要: 明年，硅料厂商将告别超额利润期，将进入合理利润时代
### [组图：偶吧生快！孔刘43岁庆生照曝光 带棒球帽打扮休闲](http://slide.ent.sina.com.cn/star/k/slide_4_704_372371.html)
> 概要: 组图：偶吧生快！孔刘43岁庆生照曝光 带棒球帽打扮休闲
### [李克强在福建考察时强调 落实政策稳市场主体 推进改革开放激活力 切实稳就业保民生](https://finance.sina.com.cn/china/2022-07-10/doc-imizmscv0920603.shtml)
> 概要: 新华社福州7月10日电 7月7日至8日，中共中央政治局常委、国务院总理李克强在福建省委书记尹力、省长赵龙陪同下，在福州、泉州考察。
### [高速上 奥迪车失去动力之后……](https://finance.sina.com.cn/china/2022-07-10/doc-imizirav2772909.shtml)
> 概要: 来源：1818黄金眼 杭州绍兴路上有一家浙江奥通奥迪4S店。廖先生这辆奥迪A6L新能源车，是2020年5月份买的，落地价45万。7月3号，他开车从富阳回萧山。
### [传统计算机视觉技术落伍了吗？](https://www.tuicool.com/articles/uqqq6nE)
> 概要: 传统计算机视觉技术落伍了吗？
### [iPhone 14的价格可能比iPhone 13高100美元](https://www.3dmgame.com/news/202207/3846630.html)
> 概要: 2022年的iPhone更新对消费者来说可能是一次昂贵的体验，一位分析家称，iPhone 14系列的价格可能会有多达100美元的上涨。苹果公司庞大的供应链正在努力为今年秋季的iPhone更新做准备。然......
### [突发：杭州核酸常态化改为3天1次，多个大城市加密检测频次](https://finance.sina.com.cn/china/gncj/2022-07-10/doc-imizmscv0923699.shtml)
> 概要: 持续加强“场所码”扫码核验工作，严格执行入场必扫，扫码必验，验码必严。 7月10日，杭州发布《关于调整常态化核酸检测频次的通告》，决定自7月11日零时起...
### [突发：杭州核酸常态化改为3天1次，多个大城市加密检测频次](https://www.yicai.com/news/101469673.html)
> 概要: 持续加强“场所码”扫码核验工作，严格执行入场必扫，扫码必验，验码必严。
### [狼队赛后发文：很遗憾今天的比赛以0:3的比分战败，对不起一直以来在屏幕前支持我们](https://bbs.hupu.com/54679198.html)
> 概要: 【2022KPL夏季赛常规赛第二轮 】重庆狼队 0-3 武汉eStarPro3分钟:我方集火秒掉夏侯惇;5分钟:团战我方被零换二;6分钟:我方秒掉鲁班七号;10分钟:团战一换一，随后马超收掉娜可露露;
### [上海首个核酸产业园将开工，系生物医药产业布局新动作](https://finance.sina.com.cn/china/2022-07-10/doc-imizirav2777466.shtml)
> 概要: 澎湃新闻记者 陈少颖 近日，有消息传出，上海首个核酸产业园将于7月中旬在上海杭州湾经济技术开发区正式开工，打造“东方美谷·生命信使”核酸产业生态圈。
### [预防新冠的中和抗体药来了，专家：药物无法保证100%不被感染，应优先接种疫苗](https://finance.sina.com.cn/roll/2022-07-10/doc-imizmscv0927296.shtml)
> 概要: 来源：每日经济新闻 每经记者：林姿辰每经编辑：文多 近日，新冠中和抗体药物Evusheld落地海南博鳌乐城国际医疗旅游先行区，适用于成人和青少年（年龄≥12岁且体重≥40kg）的...
### [多地提高医保最低缴费年限，主要影响这两类人群](https://www.yicai.com/news/101469688.html)
> 概要: “男职工满30年，女职工满25年”的最低缴费年限比目前多地执行的缴费年限要长，地方上制定了相应的过渡政策。
### [卸载了抖音，也逃不过人情和技术的大网](https://www.huxiu.com/article/604338.html)
> 概要: “我们的技术显然已经超越了人性，这让人不由得毛骨悚然。”——阿尔伯特·爱因斯坦本文来自微信公众号：商隐社 （ID：shangyinshecj），作者：里普，部分内容参考锡南·阿拉尔所著《炒作机器》，原......
### [我靠充值当武帝开播，男主穷得很，却被忽悠充值变强，内容挺搞笑](https://new.qq.com/omn/20220710/20220710A0829800.html)
> 概要: 一：我靠充值当武帝开播，男主是穷鬼，却要靠充值变强？现在大家经常吐槽一个词，叫“钞能力”，指的就是花钱，特别是现在各种氪金游戏越来越多啊，我这种穷鬼不充钱很难玩，今天想和大家安利的这部动漫叫《我靠充值......
### [流言板赵嘉仁、王丽丽分别入选三人篮球亚洲杯男女组最佳阵容](https://bbs.hupu.com/54679679.html)
> 概要: 虎扑07月10日讯 FIBA三人篮球亚洲杯落幕，官方公布男女组最佳阵容：中国球员王丽丽与赵嘉仁分别入选。王丽丽本届亚洲杯场均能得到4.8分，赵嘉仁场均能得到5.4分。本届三人篮球亚洲杯中国女篮获得冠军
### [总统官邸变“游乐场”，纸杯糖包成奢侈品：斯里兰卡已国家破产](https://www.yicai.com/news/101469701.html)
> 概要: 没有外汇、油气和食物，约626万斯里兰卡人三餐不继。
### [流言板青岛男篮寄语吕俊虎：不断向上，未来可期！](https://bbs.hupu.com/54680005.html)
> 概要: 虎扑07月10日讯 FIBA三人篮球亚洲杯中国男篮20-18险胜菲律宾获得季军。赛后CBA青岛男篮发文祝贺。青岛男篮写道：“中国男队棒棒哒！”并寄语队内球员吕俊虎：“不断向上，未来可期！”中国男篮数据
### [流言板阿布力克木：打进中超首球很开心，感谢教练组给我首发机会](https://bbs.hupu.com/54680040.html)
> 概要: 虎扑07月10日讯 中超联赛第10轮，浙江队2-0广州城，赛后浙江队球员阿布力克木接受媒体采访。打进中超首球的感受感觉很开心，没想到这么快进首球，特别开心，感谢教练组的信任，给我首发机会。你觉得你身上
# 小说
### [我为七个妹妹破苍穹](http://book.zongheng.com/book/1082319.html)
> 作者：蜀中夜话

> 标签：奇幻玄幻

> 简介：他有七个妹妹。红妹妹神陆首富，橙妹妹九天洞主，黄妹妹玲珑剑神，绿妹妹通天医王，青妹妹神国女将，蓝妹妹文倾天下，紫妹妹第一美人。他叫韦天破。这是他为七个妹妹破苍穹的故事。

> 章节末：第259章 大结局

> 状态：完本
# 论文
### [An Empirical Study Of Self-supervised Learning Approaches For Object Detection With Transformers | Papers With Code](https://paperswithcode.com/paper/an-empirical-study-of-self-supervised)
> 概要: Self-supervised learning (SSL) methods such as masked language modeling have shown massive performance gains by pretraining transformer models for a variety of natural language processing tasks. The follow-up research adapted similar methods like masked image modeling in vision transformer and demonstrated improvements in the image classification task. Such simple self-supervised methods are not exhaustively studied for object detection transformers (DETR, Deformable DETR) as their transformer encoder modules take input in the convolutional neural network (CNN) extracted feature space rather than the image space as in general vision transformers. However, the CNN feature maps still maintain the spatial relationship and we utilize this property to design self-supervised learning approaches to train the encoder of object detection transformers in pretraining and multi-task learning settings. We explore common self-supervised methods based on image reconstruction, masked image modeling and jigsaw. Preliminary experiments in the iSAID dataset demonstrate faster convergence of DETR in the initial epochs in both pretraining and multi-task learning settings; nonetheless, similar improvement is not observed in the case of multi-task learning with Deformable DETR. The code for our experiments with DETR and Deformable DETR are available at https://github.com/gokulkarthik/detr and https://github.com/gokulkarthik/Deformable-DETR respectively.
### [Contextual Semantic Parsing for Multilingual Task-Oriented Dialogues | Papers With Code](https://paperswithcode.com/paper/contextual-semantic-parsing-for-multilingual)
> 概要: Robust state tracking for task-oriented dialogue systems currently remains restricted to a few popular languages. This paper shows that given a large-scale dialogue data set in one language, we can automatically produce an effective semantic parser for other languages using machine translation.
