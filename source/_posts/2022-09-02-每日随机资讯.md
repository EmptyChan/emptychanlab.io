---
title: 2022-09-02-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeitanLimania_ZH-CN3831790369_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-09-02 22:40:59
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeitanLimania_ZH-CN3831790369_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [在 Vue 中为什么不推荐用 index 做 key](https://segmentfault.com/a/1190000042415654)
> 概要: 尤大在vue 2.x的文档中明确指出：建议尽可能在使用v-for时提供keyattribute，除非遍历输出的 DOM 内容非常简单，或者是刻意依赖默认行为以获取性能上的提升。尤大的建议说白了就是说：......
### [老铁主播困局：快手的焦虑，辛巴缓解不了](https://www.woshipm.com/it/5586490.html)
> 概要: 在快手主播圈中，骂战、声讨、约架是以往的常规操作，后来因炒作出格被整治才有所收敛。然而这次快手主播们突然抱团突袭抖音，这是为什么呢？老铁文化，还能继续吗？万万没想到，快手一哥辛巴抖音首播，却送给官方一......
### [互联网正在上演一场“接梗大戏”](https://www.woshipm.com/it/5586592.html)
> 概要: 近年来，互联网文学不断演绎发展，从废话文学、凡尔赛文学到如今的带走吧文学、爹味文学，造梗正在成为一种常态，形成了一套工作流。由此，有人跑出了一条冷门赛道，聚焦互联网文学，开发出小程序，专治不知道如何接......
### [闲鱼电商走到了十字路口](https://www.woshipm.com/it/5586709.html)
> 概要: 近年来，大众对于商品的剩余价值十分注重，二手商品的转卖的需求日益上涨，二手平台闲鱼也受到了大众的青睐。但就在闲鱼多轮探索中，还是发现尚存许多问题，闲鱼电商若想在未来能够实现新阶段的突破，还需将转型变革......
### [二十四节气之瘦金体系列](https://www.zcool.com.cn/work/ZNjE3MjcyODA=.html)
> 概要: 二十四节气之瘦金体系列*此系列参考楷书“瘦金体”而创作，至今已好些时间了，过程修改了多少版本已不可计。就以中国传统风格字形记录自然节律变化之名，作为阶段性的成果展示吧......
### [Notes on the SQLite DuckDB Paper](https://simonwillison.net/2022/Sep/1/sqlite-duckdb-paper/)
> 概要: Notes on the SQLite DuckDB Paper
### [日本车企要跑了？](https://www.huxiu.com/article/649390.html)
> 概要: 出品丨虎嗅汽车组作者丨李文博编辑丨周到头图丨视觉中国最近，有两条关于汽车供应链的新闻，似乎让全球汽车工业感受到了一股别样的风。第一条：日本《产经新闻》近日报导，本田汽车计划重组全球零件供应链，考虑中国......
### [8月哪吒持续领跑新势力，“蔚小理”被二线反超会是常态吗](https://finance.sina.com.cn/tech/it/2022-09-02/doc-imqmmtha5587191.shtml)
> 概要: 记者 吴遇利......
### [时代的眼泪？“有妖气”将关停！CEO“妖气君”已离职](https://finance.sina.com.cn/tech/internet/2022-09-02/doc-imqmmtha5589061.shtml)
> 概要: 每经记者 温梦华    每经编辑 段炼 文多 易启江......
### [垂直电商再度集体溃败，盛极而衰后该如何续命](https://finance.sina.com.cn/tech/roll/2022-09-02/doc-imizmscv8758975.shtml)
> 概要: 寺库全资子公司上海寺库电子商务有限公司在被冻结1100万财产后，日前又新增一则破产审查案件，再次引发投资者对于寺库前景的担忧。一时间，业内有关“奢侈品电商第一股已凉凉”的讨论开始甚嚣尘上......
### [2018-2022《阿颓の大冒険》作品总结-舞美/赛事篇](https://www.zcool.com.cn/work/ZNjE3MjkxNDQ=.html)
> 概要: Collect......
### [《中华人民共和国反电信网络诈骗法》等法律案获得通过](https://finance.sina.com.cn/tech/tele/2022-09-02/doc-imqmmtha5598641.shtml)
> 概要: 十三届全国人大常委会第三十六次会议今天（2日）上午在京举行闭幕会。会议表决通过了《中华人民共和国反电信网络诈骗法》、新修订的《中华人民共和国农产品质量安全法》等......
### [imi-zookeeper 正式发布——imi 框架微服务配置中心开发进度 20220902](https://segmentfault.com/a/1190000042425313)
> 概要: 进度说明（20220902）imi-zookeeper 目前实现了配置中心的功能，由于 imi-config-center 抽象出了接口，所以使用上和 imi-nacos、imi-etcd 非常类似......
### [Evaluation of TikTok vs. Instagram Reels](https://www.surgehq.ai//blog/tiktok-vs-instagram-reels-personalized-human-evaluation)
> 概要: Evaluation of TikTok vs. Instagram Reels
### [组图：电影《援军明日到达》开机 于和伟杨洋万茜等主演亮相](http://slide.ent.sina.com.cn/film/slide_4_704_374653.html)
> 概要: 组图：电影《援军明日到达》开机 于和伟杨洋万茜等主演亮相
### [法治日报：将天价片酬治理纳入法治化轨道](https://ent.sina.com.cn/s/m/2022-09-02/doc-imizmscv8784421.shtml)
> 概要: 来源：法治日报　　近日，国家广播电视总局起草了《广播电视和网络视听节目制作经营管理规定（征求意见稿）》（以下简称《管理规定》），在片酬管理问题上，明确规定：从事节目制作经营活动的机构，应合理确定节目制......
### [疑似《潜行者2》早期开发截图曝光 2023年发售](https://www.3dmgame.com/news/202209/3850755.html)
> 概要: 近日，俄语社交媒体VK用户“Вестник «Того Самого Сталкера»”公布了数张疑似《潜行者2：切尔诺贝利之心》早期开发截图，该用户声称有人黑入开发团队GSC Game World......
### [先解决团队存在的问题，再解决问题本身](https://www.tuicool.com/articles/JbUn2mY)
> 概要: 先解决团队存在的问题，再解决问题本身
### [周五福利囧图云飞系列 美女老师的课都不认真听么](https://www.3dmgame.com/bagua/5541.html)
> 概要: 转眼又到周五了，一起来看看小编准备的福利囧图。美女老师的课都不认真听吗？这件衣服穿着肯定贼凉快！快看外面有个身材超棒的小姐姐！哎呦，你这一拳捶我心里来了。这是什么加速？一个失误身上纹的是什么名副其实割......
### [网易伏羲挖掘机器人亮相：键鼠手柄操控 堪比老司机](https://www.3dmgame.com/news/202209/3850763.html)
> 概要: 9月1日，网易伏羲挖掘机器人亮相世界人工智能大会，这也是它首次对外曝光。网易伏羲挖掘机器人是通过智能挖掘系统将传统工程机械升级为机器人，把挖掘机的感知数据实时回传到远程客户端, 并支持鼠键、手柄等设备......
### [做梦都想背房贷的年轻人](https://www.huxiu.com/article/651627.html)
> 概要: 本文来自微信公众号：孟大明白（ID：mengdamingbai），作者：大璐璐，题图来自：《二十不惑2》剧照《二十不惑2》看到十多集，姜小果可以说是国产剧里最不像女主角的女主角，理智正经，爱工作爱加班......
### [【混剪】科比震撼入场，一直都是球场那颗最亮的星](https://bbs.hupu.com/55382871.html)
> 概要: 如果你也喜欢科比，那就加个关注吧，每天分享关于科比的精彩内容！
### [网易《第五人格》赛事生态品牌分享（上）](https://www.zcool.com.cn/work/ZNjE3MzM3OTY=.html)
> 概要: 《第五人格》是由网易开发的非对称性对抗竞技类游戏，通过不断拓展赛事规模及赛事内容，构建了一个完善的赛事体系（COA世界赛、IVL职业塞、等第三方授权赛事）。本次将从《第五人格》全球赛事、顶级赛事、次级......
### [余额宝XEDGX雷蛇 热血有余 为爱而聚](https://www.zcool.com.cn/work/ZNjE3MzQyNjA=.html)
> 概要: Client : 余额宝-----------------------------------------Agency : 点睛科技Producerr : 张金涛Art Director : 罗怀阳S......
### [上赛季湖人最强的球员！威斯布鲁克湖人时期高光混剪](https://bbs.hupu.com/55383847.html)
> 概要: 上赛季湖人最强的球员！威斯布鲁克湖人时期高光混剪
### [恒天财富郭开香：企业权益资产配置时代已然到来](http://www.investorscn.com/2022/09/02/102766/)
> 概要: 近日，本年度最后一期企投家成长营在苏州举办，在本次结业模块上，我们聚焦“资产全球化与财富管理”，学习如何把脉全球资产配置趋势，掌握各类资产的投资策略和分析框架。本次活动特邀东吴证券副董事长陈李、擅长企......
### [金电联行董事长范晓忻当选北京市工商联执委会常务委员](http://www.investorscn.com/2022/09/02/102767/)
> 概要: 8月28日至29日，北京市工商业联合会第十五次代表大会在京召开。北京市委副书记、市长陈吉宁出席开幕式并讲话。北京市委常委、统战部部长游钧，市人大常委会副主任李颖津，市政协副主席杨艺文，市政协副主席王红......
### [《催眠麦克风》回顾5年历史特别视频发布](https://news.dmzj.com/article/75445.html)
> 概要: “催眠麦克风”将在今日迎来五周年活动，特别纪念公开视频“催眠麦克风 5 周年特别电影”已发布。
### [淘宝拒绝“灌流量”](https://www.huxiu.com/article/651887.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国“我们不希望灌流量，而是形成正循环的流量逻辑。内容做得好，可以形成更好的停留时长。”阿里巴巴淘宝直播事业群总经理道放（程道放）说。9月1日，淘宝直播盛典在......
### [组图：张元英上班图释出青春有活力 麻花辫搭学院风造型好清新](http://slide.ent.sina.com.cn/star/slide_4_86512_374668.html)
> 概要: 组图：张元英上班图释出青春有活力 麻花辫搭学院风造型好清新
### [下一代能源什么样？“藻”知道 | 硬科技融资周报](https://www.tuicool.com/articles/v2YfQfM)
> 概要: 下一代能源什么样？“藻”知道 | 硬科技融资周报
### [又一家开发商指控PQube未能支付发行商保证金](https://www.3dmgame.com/news/202209/3850779.html)
> 概要: 今天（9 月 2 日），3D 动作游戏《Aeterno Blade2》的开发工作室Corecell 指控英国游戏发行商PQube Games 扣留了其所应付的资金。Corecell 在推特上发表声明称......
### [TCL 推出新款 6 系列 Roku Mini LED 电视：4K+144Hz，起售价 4823 元](https://www.ithome.com/0/638/697.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家9 月 2 日消息，随着 Mini LED 技术的不断发展，厂商们都在积极推出 Mini LED 电视。在近日的柏林国际电子消费品展览会（IFA）上，TC......
### [水母智能设计平台荣登《财富》中国最佳设计榜](http://www.investorscn.com/2022/09/02/102771/)
> 概要: 水母智能设计平台荣登《财富》中国最佳设计榜
### [解锁元宇宙海钓“初体验”，一梦江湖x象山旅游集团开启新鲜联动](http://www.investorscn.com/2022/09/02/102772/)
> 概要: 恰逢9月象山开渔时节，《一梦江湖》携手象山旅游集团，在9月2日-9月22日带来了一场活色生“象”的新鲜联动。并通过线上、线下多措并举的形式，以电商带动象山助渔，帮助提高渔民收入。而江湖少侠也将首次在游......
### [巴黎贝甜有点甜](https://www.huxiu.com/article/651810.html)
> 概要: 作者｜周超臣头图｜视觉中国巴黎贝甜被罚上了热搜。日前，据天眼查App显示，8月12日，巴黎贝甜（Paris Baguette）关联公司上海艾丝碧西食品有限公司因违反食品安全法，被上海市市场监督管理局罚......
### [株式会社宝可梦起诉6家公司 称游戏《口袋妖怪复刻》侵权](https://news.dmzj.com/article/75447.html)
> 概要: 株式会社宝可梦起诉了包括中南文化在内的6家公司，称游戏《口袋妖怪复刻》侵犯了株式会社宝可梦持有的游戏著作权，要求被告6公司停止游戏的开发、运营、宣传等侵权行为，在媒体上进行公开赔礼道歉，并赔偿经济损失5亿人民币。
### [Shopee让谁失望了？](https://www.tuicool.com/articles/RjIzeuq)
> 概要: Shopee让谁失望了？
### [叙事冒险游戏《格尔达：寒冬之火》现已在Steam发售](https://www.3dmgame.com/news/202209/3850790.html)
> 概要: 轻度RPG要素叙事冒险游戏《格尔达：寒冬之火》（Gerda: A Flame in Winter）现已在Steam平台发售，现在购买享9折优惠，只需151.2元，支持中文。游戏中，你将扮演来自一座丹麦......
### [4 口味尝鲜装：外星人电解质水 4.5 元猫超发车（商超 6.5 元）](https://lapin.ithome.com/html/digi/638714.htm)
> 概要: 【天猫超市】外星人电解质水 500ml*6 瓶日常售价为 92 元，下单立减 46 元，领取 5 元优惠券，商品详情页大概率弹 5 元福袋，到手价为 36 元 8 瓶，折合 4.5 元 / 瓶。5 元......
### [Australian Signals Directorate coin code cracked by 14yo in 'just over an hour'](https://www.abc.net.au/news/2022-09-02/asd-50-cent-code-cracked-by-14yo-tasmanian-boy/101401978)
> 概要: Australian Signals Directorate coin code cracked by 14yo in 'just over an hour'
### [TV动画《黄金神威》公开第四期新PV](https://news.dmzj.com/article/75452.html)
> 概要: TV动画《黄金神威》宣布了第四期将于10月3日播出的消息。本作的新PV也一并公开。在这次的PV中，收录了ALI演唱的OP主题曲《NEVER SAY GOODBYE feat. Mummy-D》的片段。
### [中航集团新董事长到任，民航新一轮人事调整启动](https://www.yicai.com/news/101526101.html)
> 概要: 此次国航和东航先后调整一把手也意味着，民航的新一轮人事调整又开始了。
### [中兴通讯：智能网联车载终端产品累计出货已超百万台](https://www.ithome.com/0/638/753.htm)
> 概要: IT之家9 月 2 日消息，根据中兴通讯披露的投资者关系活动记录，在 8 月 30 日的业绩说明会上，中兴通讯表示，从全球角度看，未来五年全球运营商资本开支年复合增长率约 2.4%，所以运营商资本开支......
### [新漫周刊第111期 一周新漫推荐(20220902期)](https://news.dmzj.com/article/75453.html)
> 概要: 本期章鱼P没有一定承受能力的成年人请在小孩的指导和监督下观看，深井冰情侣、AI大哥幸运E二哥和圣母三妹、山岳怪谈合集、天狗大姐姐×小正太、安娜科穆宁娜这个作者很有趣，腰斩的咒剑姬和妖物其实剧情也还可以，以及短篇推荐大家多看看。
### [直击WAIC2022｜旷视唐文斌：AI落到产业，技术、硬件与空间三者缺一不可](https://www.tuicool.com/articles/67VbIvU)
> 概要: 直击WAIC2022｜旷视唐文斌：AI落到产业，技术、硬件与空间三者缺一不可
### [阿里再度涉足房地产、落子上海滩，联手复星拿下“王炸”地块](https://www.yicai.com/news/101526100.html)
> 概要: 这宗百亿级地块的成交价未公开，但地方政府要求未来至少产生264亿元税收。
### [北交所迎交易机制创新，两融交易细则公开征求意见](https://www.yicai.com/news/101526145.html)
> 概要: 与沪深市场两融相比，北交所两融制度有两方面差异。
### [创业板上半年成绩单:营收合计1.5万亿,超8成实现盈利](https://www.yicai.com/news/101526152.html)
> 概要: 创业板今年上半年近五成公司净利润同比实现正增长。
### [英特尔锐炫显卡全阵容曝光：移动、桌面、工作站、数据中心全覆盖](https://www.ithome.com/0/638/758.htm)
> 概要: IT之家9 月 2 日消息，今天，外媒 VideoCardz 曝光了英特尔锐炫 GPU 的全阵容型号，移动、桌面、工作站、数据中心全覆盖。移动端Intel Arc A770MIntel Arc A73......
### [LCS赛后越南赛区GAM以3-0击败TS，成为第十四支锁定S12资格的队伍](https://bbs.hupu.com/55388786.html)
> 概要: 虎扑09月02日讯 在刚刚结束的VCS越南赛区夏季赛败者组决赛的比赛中，GAM直落三局横扫Team Secret进入决赛。他们将在决赛面对SGB。与此同时GAM也成为越南赛区第二支，全球第十四支锁定2
### [Apple refuses to produce information regarding The App Association (ACT)](http://www.fosspatents.com/2022/09/apple-refuses-to-cooperate-with-us.html)
> 概要: Apple refuses to produce information regarding The App Association (ACT)
### [搭载光峰科技激光显示，峰米S5激光投影仪即将上市](http://www.investorscn.com/2022/09/02/102775/)
> 概要: 随着科技进步与人们生活水平的日益提高,家庭影音娱乐迎来了数字化、大屏化的新升级,投影仪尤其是激光投影凭借更沉浸的观影效果与性价比优势逐渐被广大消费者所青睐。成为越来越多家庭里不可或缺的娱乐观影设备......
### [税务部门密集公布个税查处案件 意味着什么？](https://finance.sina.com.cn/chanjing/cyxw/2022-09-02/doc-imizmscv8872288.shtml)
> 概要: 经济观察网 记者 杜涛9月2日，江苏苏州市税务局稽查局、广西壮族自治区南宁市税务局第一稽查局、湖北恩施州税务局第二稽查局又曝光了3起自然人纳税人未依法办理个人所得税...
### [江苏：企业每招用1名应届生，最高补助1500元！](https://finance.sina.com.cn/china/gncj/2022-09-02/doc-imizmscv8873004.shtml)
> 概要: 原标题：每招用1名应届生，最高补助1500元！ 来源：江苏新闻 记者今天（9月2日）获悉，江苏省人力资源社会保障厅、省教育厅、省财政厅日前转发《人力资源社会保障部办公厅...
### [从A股中报看“业绩地图”：吉林、黑龙江、广西降幅最大](https://www.yicai.com/news/101526259.html)
> 概要: 哪些地区公司业绩增长，哪些地区公司业绩下滑幅度大？
### [内蒙古取消优惠电价，中国低廉成本补贴全球光伏发电时代宣告终结](https://finance.sina.com.cn/chanjing/2022-09-02/doc-imizmscv8874922.shtml)
> 概要: 记者|马悦然 继云南之后，内蒙古也宣布取消优惠电价政策。 9月1日，内蒙古自治区发展和改革委员会官网发布取消优惠电价政策的通知。
### [杭州一高端网红盘遭业主联名投诉，开发商回应来了……](https://finance.sina.com.cn/china/gncj/2022-09-02/doc-imqmmtha5708843.shtml)
> 概要: 每经记者 包晶晶每经编辑 陈梦妤 新房一房难求的杭州，整体中签率约7.5%的网红盘杭源御潮府遭遇了投诉。 业主代表提出6大诉求，开发商滨江集团则组织了5次沟通会...
### [河南罗山县：“一人买房全家帮”，允许先提取公积金再申请贷款](https://finance.sina.com.cn/china/gncj/2022-09-02/doc-imqmmtha5709650.shtml)
> 概要: 澎湃财讯 据河南省信阳市罗山县人民政府网站消息，9月1日，该县发布《罗山县人民政府办公室关于印发罗山县促进房地产市场平稳健康发展若干措施（试行）的通知》。
### [JR热议打拳还是放假，这是一个问题——截图预测V5 vs LNG](https://bbs.hupu.com/55390969.html)
> 概要: 这是冒泡赛败者组第一轮哦，所以谁输谁放假所以就是要么Doinb赛后打拳，要么放假……Rookie和Doinb之间必有一人进不了世界赛……啊……然后赢的人还得4号跟RNG打！比赛时间：9月3日 17:0
# 小说
### [柯学验尸官](https://m.qidian.com/book/1021708634/catalog)
> 作者：河流之汪

> 标签：衍生同人

> 简介：【这是柯南同人】本书全称：《刚穿越到柯学世界就被名侦探指认为头号犯罪嫌疑人，身为验尸官的我现在一点不慌》

> 章节总数：共700章

> 状态：完本
# 论文
### [Exploring Point-BEV Fusion for 3D Point Cloud Object Tracking with Transformer | Papers With Code](https://paperswithcode.com/paper/exploring-point-bev-fusion-for-3d-point-cloud)
> 日期：10 Aug 2022

> 标签：None

> 代码：https://github.com/jasonkks/pttr

> 描述：With the prevalence of LiDAR sensors in autonomous driving, 3D object tracking has received increasing attention. In a point cloud sequence, 3D object tracking aims to predict the location and orientation of an object in consecutive frames given an object template. Motivated by the success of transformers, we propose Point Tracking TRansformer (PTTR), which efficiently predicts high-quality 3D tracking results in a coarse-to-fine manner with the help of transformer operations. PTTR consists of three novel designs. 1) Instead of random sampling, we design Relation-Aware Sampling to preserve relevant points to the given template during subsampling. 2) We propose a Point Relation Transformer for effective feature aggregation and feature matching between the template and search region. 3) Based on the coarse tracking results, we employ a novel Prediction Refinement Module to obtain the final refined prediction through local feature pooling. In addition, motivated by the favorable properties of the Bird's-Eye View (BEV) of point clouds in capturing object motion, we further design a more advanced framework named PTTR++, which incorporates both the point-wise view and BEV representation to exploit their complementary effect in generating high-quality tracking results. PTTR++ substantially boosts the tracking performance on top of PTTR with low computational overhead. Extensive experiments over multiple datasets show that our proposed approaches achieve superior 3D tracking accuracy and efficiency.
### [Online-compatible Unsupervised Non-resonant Anomaly Detection | Papers With Code](https://paperswithcode.com/paper/online-compatible-unsupervised-non-resonant)
> 概要: There is a growing need for anomaly detection methods that can broaden the search for new particles in a model-agnostic manner. Most proposals for new methods focus exclusively on signal sensitivity.