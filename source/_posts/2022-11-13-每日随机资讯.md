---
title: 2022-11-13-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.IsarwinkelSylvenstein_ZH-CN2963187862_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-11-13 20:11:16
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.IsarwinkelSylvenstein_ZH-CN2963187862_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [需求评审过不了？这里有几个可行方法](https://www.woshipm.com/it/5676755.html)
> 概要: 作为产品经理，需求评审环节是非常重要的，遇上会沟通的技术皆大欢喜，遇上沟通困难的技术也在所难免。本文作者总结了一些产品与技术在需求评审各个阶段中，可能会出现沟通困难的场景、复盘思路和应对建议，希望能给......
### [To B 内容营销 | 如何避免自嗨选题，提升功能DAU等效果指标？](https://www.woshipm.com/operate/5674051.html)
> 概要: 如果没有办法直接对接用户，就难免会对用户的需求与当前的关注点有所偏差，自然也达不到客户想要的内容要求。那么，ToB企业应当如何通过间接接触客户，也能看到第一需求？作者总结了几点经验，希望对To B 内......
### [数字营销将成为B端营销主流](https://www.woshipm.com/marketing/5676709.html)
> 概要: 传统B端营销遇到了诸多的挑战，如何精准营销、提升营销效率、降低销售门槛呢？本文作者认为，借助数字营销，可以实现精准营销，并对如何实现精准营销进行了分析，一起来看一下吧。数字化转型本质是为客户创造价值，......
### [工信部：2021年我国虚拟现实投融资规模涨幅超过100%](https://finance.sina.com.cn/tech/internet/2022-11-13/doc-imqmmthc4360052.shtml)
> 概要: 以“VR让世界更精彩——VR点亮元宇宙”为主题的2022世界VR产业大会昨天（12日）在江西南昌开幕......
### [中国航天员首次在轨迎来货运飞船 2小时！天舟五号创最快交会对接纪录](https://finance.sina.com.cn/tech/it/2022-11-13/doc-imqqsmrp5956723.shtml)
> 概要: 本报记者 刘苏雅......
### [法治日报：如何防止直播“翻车”](https://finance.sina.com.cn/tech/internet/2022-11-13/doc-imqqsmrp5957723.shtml)
> 概要: □　法治日报 记者 黄辉......
### [2022年双十一收官 平台在流量焦虑中告别GMV数据比拼](https://finance.sina.com.cn/tech/internet/2022-11-13/doc-imqmmthc4360181.shtml)
> 概要: 财联社|新消费日报11月12日讯（研究员 梁又匀），随着时钟走向11月12日的0时，这届让消费者和商家都略感疲惫的双11大促，终于宣告结束......
### [被骂“日本鬼子”的河北男孩，在日本成了黑帮老大](https://www.huxiu.com/article/708295.html)
> 概要: 出品 | 那個NG作者 | 渣渣郡采访 | 渣渣郡、木子童题图｜IG特别感谢@乾 哲义为采访提供的帮助。本文首发于虎嗅青年文化组公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下......
### [哭到泪崩绝妙反转，亲情+爱情双重故事线](https://news.dmzj.com/article/76183.html)
> 概要: 一部在韩国热播流量超高，在中国查无此剧严重被低估的小众韩剧《耀眼》，由金惠子、韩志旼、南柱赫、孙浩俊等人主演，2019年上线共12集。因为男主南柱赫以前出演过《学校2016》《举重妖精金福珠》在国内红极一时，按道理说粉丝众多必然流量大，更是穿越奇...
### [RTX 4090插紧原生16Pin线后：玩游戏仍然烧毁](https://www.3dmgame.com/news/202211/3855972.html)
> 概要: 民间报告的RTX 4090“自燃”烧毁已经有几十起之多，从10月24日公开的第一例算起已经过去20天，但NVIDIA和AIC厂商们都没有给出明确的调查结论。最近一次关于RTX 4090烧毁的热烈讨论肇......
### [袁咏仪晒牵手对比照为儿子庆生 称魔童为张青少年](https://ent.sina.com.cn/2022-11-13/doc-imqmmthc4374282.shtml)
> 概要: 新娱乐讯 11月12日，袁咏仪分享为儿子庆生的照片，并配文：十指紧扣，张青少年，Happy 16th Birthday！网友：看着魔童长大啦~......
### [扎克伯格的说辞：我裁员至少不像马斯克那么乱](https://www.3dmgame.com/news/202211/3855981.html)
> 概要: 近期多个互联网巨头都开启疯狂裁员模式，北京时间11月13日消息，在对Meta员工进行了上万人裁员后，马克·扎克伯格(Mark Zuckerberg)在周五的员工大会上分享了一些鼓舞人心的想法：至少我们......
### [为避免交叉感染，多家医院不再向社会提供“愿检尽检”服务](https://finance.sina.com.cn/china/2022-11-13/doc-imqmmthc4379906.shtml)
> 概要: 多家医院近期发布公告称，为杜绝“愿检尽检”人群与医疗机构内人员交叉感染事件，根据有关要求，取消院内核酸“愿检尽检”，仅向院内工作人员...
### [降薪跳槽的打工人：钱少了，快乐回来了](https://www.huxiu.com/article/712814.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：王敏、唐亚华、李秋涵、邹帅，编辑：邹帅，头图来自：视觉中国接近年底，在当下的职场环境中，跳槽不一定是个可以任性做出的决定。而降薪跳......
### [罗志祥演唱会跌落升降台 经纪人称胫骨撞到两次](https://ent.sina.com.cn/s/2022-11-13/doc-imqmmthc4380079.shtml)
> 概要: 新浪娱乐讯 据媒体报道，歌手罗志祥在高雄举办《罗志祥2022年演唱会》，一开场由30名舞者震撼登场，之后罗志祥从舞台下方弹跳而出，没想到才出来讲一句话，就突然消失，而从大屏幕看到，罗志祥已经跌落升降台......
### [郑恺苗苗一家农场游玩 2岁女儿乖巧可爱](https://ent.sina.com.cn/s/2022-11-13/doc-imqqsmrp5978590.shtml)
> 概要: 新浪娱乐讯 近日，郑恺娇妻苗苗久违在个人社交平台分享了一段生活vlog，一家人其乐融融游农场，郑化身超级奶爸，2岁女儿出镜，乖巧可爱......
### [《辉夜大小姐》作者宣布引退：其实我不擅长画画](https://acg.gamersky.com/news/202211/1537329.shtml)
> 概要: 《辉夜大小姐想让我告白》的作者赤坂明在漫画正式完结后宣布隐退，今后将以原撰稿作者的身份活动。
### [多地购房优惠政策期限延长 “稳楼市”政策持续发力](https://finance.sina.com.cn/jjxw/2022-11-13/doc-imqqsmrp5994752.shtml)
> 概要: 连日来，已经有浙江省义乌市、河南省开封市、浙江省绍兴市越城区等多个地区延长购房优惠政策期限，其中义乌市等部分地区已经将购房契税补贴延长至年底。
### [上海全面贯彻落实国务院联防联控机制要求，进一步优化疫情防控措施](https://finance.sina.com.cn/jjxw/2022-11-13/doc-imqmmthc4393164.shtml)
> 概要: 近日，国务院联防联控机制发布了《关于进一步优化新冠肺炎疫情防控措施 科学精准做好防控工作的通知》。上海市及时启动研究、认真贯彻执行...
### [上海全面贯彻落实国务院联防联控机制要求，进一步优化疫情防控措施](https://www.yicai.com/news/101592611.html)
> 概要: 对密切接触者，管理措施调整为“5天集中隔离+3天居家隔离”；将风险区调整为“高、低”两类。
### [天猫超3000万围观菜鸟送货上门，一场268小时的“治愈系直播”收官](http://www.investorscn.com/2022/11/13/104218/)
> 概要: “快递小哥治好了我的精神内耗。”11月12日零点，伴随着第14届天猫双11收官，一场超11天共268小时的全球快递“云监工”直播也正式落幕。从10月31日晚八点开播以来，这场“治愈系直播”已被人民日报......
### [孙宇晨救市FTX尝试获彭博社关注报道](http://www.investorscn.com/2022/11/13/104219/)
> 概要: 11月11日晚间，对于波场TRON创始人、Huobi Global顾问委员会成员孙宇晨尝试“拆弹”FTX危机的举措，彭博社对孙宇晨进行了连线采访，并发布了一篇题为《在为 FTX 提供援助后，加密货币企......
### [孙宇晨huobi入职36天“小作文12连发”诚意回应热点](http://www.investorscn.com/2022/11/13/104220/)
> 概要: 近期，FTX事件影响不断发酵，作为行业中为数不多的常青树，波场TRON创始人、HuobiGlobal顾问委员会成员孙宇晨的一举一动备受关注。面对市场种种声音，孙宇晨再次以入职Huobi 36天“小作文......
### [正浩EcoFlow双11火力全开，稳居三大电商平台No.1](http://www.investorscn.com/2022/11/13/104221/)
> 概要: 今年的双11正式收官,户外露营热带动户外电源行业发展迅猛,正浩EcoFlow再次展现了行业头部品牌的实力,根据其刚刚公布的官方战报显示,正浩EcoFlow 稳居天猫、京东、抖音三大电商平台双11 户外......
### [京东11.11企业业务总战报：企业服务类订单量同比增长221%](http://www.investorscn.com/2022/11/13/104222/)
> 概要: 2022京东11.11再次刷新纪录,成交额不断走高的背后,是京东以 “实在价格、实在商品、实在服务”带动消费潜力持续释放,与合作伙伴共同筑就的高质量实体经济的增长......
### [《我与机器子》TV动画正式预告 12月4日开播](https://www.3dmgame.com/news/202211/3855988.html)
> 概要: 人气搞笑漫画《我与机器子》的TV动画即将于12月4日开播，今天官方公布了正式预告，一起来先睹为快。《我与机器子》是宫崎周平于2020年7月6日发售的《周刊少年Jump》2020年第31号起连载的漫画......
### [主板IPO周报：首创证券拿批文，A股上市券商添丁](https://www.yicai.com/news/101592630.html)
> 概要: 上海建科、天利股份下周上会。
### [小派发布“全真互联网终端”Pimax Portal：掌机、VR 头显、主机形态随意切换](https://www.ithome.com/0/653/468.htm)
> 概要: 感谢IT之家网友OC_Formula的线索投递！IT之家11 月 13 日消息，11 月 10 日上午 10 点，小派科技召开了 Pimax Frontier 海外发布会，推出了“全真互联网终端”Pi......
### [G20峰会将召开，美国零售数据考验经济韧性丨外盘看点](https://www.yicai.com/news/101592631.html)
> 概要: 英国10月CPI剑指新高，阿里巴巴、网易等中概股披露业绩。
### [王者归来：世界的张伟丽夺回金腰带](https://www.huxiu.com/article/712966.html)
> 概要: 虎嗅注：北京时间11月13日中午，UFC（终极格斗冠军赛）281在美国纽约麦迪逊广场花园进行。中国选手张伟丽战胜美国现任冠军埃斯帕扎，重夺女子草量级金腰带。本文来自微信公众号：翻滚坚果RollingN......
### [打破信息孤岛！疫情推升航空业互联需求，消费互联网何时走上“云端”？](https://finance.sina.com.cn/china/2022-11-13/doc-imqmmthc4402749.shtml)
> 概要: 随着国际航空市场的逐步复苏，航空业将如何与数字化擦出火花？ 国际航空运输协会（IATA）最新预计，虽然全球局部航空市场仍然受到限制，但总体客运量增长速度快于预期。
### [“球星”李铁的商业版图：每一件生意都与足球有关](https://www.huxiu.com/article/712951.html)
> 概要: 李铁怎么了？本文来自微信公众号：19号商研社 （ID：time_biz），作者：李馨婷、涂梦莹，编辑：洪若琳，头图来自：视觉中国前国足主帅李铁，再次卷入舆论漩涡。11月12日晚，新浪体育称，前国足主帅......
### [比亚迪汽车安卓 / iOS 版 6.4 发布：新增小程序钥匙授权，微信扫码可开车](https://www.ithome.com/0/653/482.htm)
> 概要: IT之家11 月 13 日消息，比亚迪汽车 App iOS 和安卓版近期更新至 6.4.0 版本，其中安卓版新增小程序钥匙授权，优化部分功能体验；iOS 版新增小程序钥匙授权，新增锁屏小组件（需手机系......
### [TV动画《我与机器妹》PV公开 2022年12月4日播出](https://news.dmzj.com/article/76184.html)
> 概要: 由宫崎周平创作连载于《周刊少年JUMP》的人气搞笑漫画《我与机器妹》TV动画PV与主视觉图公开，2022年12月4日开始播出。
### [《公主准则》动画电影第三章定档 正式预告公开](https://www.3dmgame.com/news/202211/3855996.html)
> 概要: 总计6章的人气TV动画全新剧场版系列、《公主准则Crown Handler》动画电影第三章官方宣布定档，预定2023年4月7日上映，同时正式预告海报公开，一起来先睹为快。•《公主准则》的故事舞台在19......
### [剧场版动画《Princess Principal》第3章预告公开](https://news.dmzj.com/article/76185.html)
> 概要: 剧场版动画《Princess Principal Crown Handler》第3章预告PV与主视觉图公开，2023年4月7日于日本上映。
### [11.12 iq 巅峰榜榜单 鹿小鸣成为iq首个2500](https://bbs.hupu.com/56424758.html)
> 概要: 11.12 iq 巅峰榜榜单 鹿小鸣成为iq首个2500选手 顶级射手鹿小鸣，昨晚成功突破2500，昨晚还是连胜上的，几乎把把是mvp，看来公孙离削弱对他们没影响嘛？安q妖刀也是昨晚突破了2500，射
### [Gurman：苹果 iPhone AirDrop 隔空投送新选项将在全球推出，“对所有人开放 10 分钟”](https://www.ithome.com/0/653/495.htm)
> 概要: IT之家11 月 13 日消息，近期，苹果 iOS 16.1.1 和 iOS 16.2 Beta 2 测试版为国行 iPhone 机型 AirDrop（隔空投送）增加了新的“对所有人开放 10 分钟”......
### [广州海珠社会面连续五日无新增本土感染，防控如何调整](https://www.yicai.com/news/101592745.html)
> 概要: 正组织开展核酸筛查，根据筛查结果对涉疫区域管控措施进行调整。
### [TV动画《我的百合乃工作是也！》2023年春播出](https://news.dmzj.com/article/76186.html)
> 概要: 由未幡创作的漫画改编的TV动画《我的百合乃工作是也！》将于2023年春季播出，同时声优阵容追加田中美海、小市真琴、田村由香里、濑户麻沙美。
### [下周解禁市值重上千亿！办公软件龙头金山办公居首 占比近五成](https://finance.sina.com.cn/china/2022-11-13/doc-imqmmthc4421301.shtml)
> 概要: 来源：北京商报 时隔六周，A股周限售股解禁市值重上千亿元。东方财富Choice数据显示，11月14日-18日这一周，A股市场上将有69只个股的24.59亿股限售股将迎来解禁。
### [一套搞定你的脸：杰威尔洗面奶 + 健肤水 + 凝露套装 29.9 元（减 80 元）](https://lapin.ithome.com/html/digi/653502.htm)
> 概要: 【杰威尔化妆品旗舰店】洁面乳 + 健肤水 + 水凝露，杰威尔男士护肤套装报价 109.9 元，下单立减 20 元，限时限量 60 元券，实付 29.9 元包邮，领券并购买。前两款套装此价，后三款套装略......
### [联想新款游戏本泄露 搭载Core i7-13700HX](https://www.3dmgame.com/news/202211/3855999.html)
> 概要: 前几天我们提到雷蛇的新款游戏笔记本搭载了Core i9 13900HX，今天又一款搭载了Core i7 13700HX的联想笔记本被Geekbench泄露。从泄露的配置图中可以看到，Core i7 1......
### [流言板比卢普斯：东契奇是最难防的人之一，很难有针对性的策略](https://bbs.hupu.com/56425794.html)
> 概要: 虎扑11月13日讯 开拓者以112-117不敌独行侠，开拓者主教练昌西-比卢普斯接受采访。谈到卢卡-东契奇，比卢普斯说：“他是联盟中最难防的人之一，你可以为得分手、组织者或者内线球员准备针对性的策略，
### [流言板康利：我们有很多优秀的球员可以出场，并不是外界说的重建](https://bbs.hupu.com/56425975.html)
> 概要: 虎扑11月13日讯 爵士球员迈克-康利接受了采访。谈到本赛季的爵士战绩，康利说：“我们并不是外界说的那样，我们有很多优秀的球员可以出场，这我从赛季第一天就知道了，这并不是重建。”“我们明白，这支球队没
### [卢卡-东契奇当选JRs评选今日最佳球员](https://bbs.hupu.com/56426011.html)
> 概要: 今日的JRs评选已经结束，卢卡-东契奇以56.6%的选票，当选今日JRs评选今日最佳球员。今日，独行侠以117-112战胜开拓者。本场比赛，东契奇出战37分钟，22投13中，其中三分球5投1中，罚球1
### [重磅经济数据发布在即，专家分析四季度经济走势](https://www.yicai.com/news/101592797.html)
> 概要: 进入四季度，虽然我们面临疫情多点散发和外部环境进一步恶化的影响，但二十大的召开会产生较好的政策红利，使市场预期趋于稳定，让市场主体的行为方向得到更多确定性。
# 小说
### [无敌剑魂](http://book.zongheng.com/book/628887.html)
> 作者：铁马飞桥

> 标签：奇幻玄幻

> 简介：天有九重，人有九转，剑有九心！一代帝王身遭惨死，携九绝剑，诸天灭地！我既生，自当一剑平天下，九绝出，天地乱，鸿蒙诀，无双情！公众号搜索《铁马飞桥》每日都有更新，请点击关注！

> 章节末：第两千六百四十三章  新书《太荒吞天诀》

> 状态：完本
# 论文
### [Multimodal Multi-objective Optimization: Comparative Study of the State-of-the-Art | Papers With Code](https://paperswithcode.com/paper/multimodal-multi-objective-optimization)
> 日期：11 Jul 2022

> 标签：None

> 代码：https://github.com/wenhua-li/comparativestudyofmmop

> 描述：Multimodal multi-objective problems (MMOPs) commonly arise in real-world problems where distant solutions in decision space correspond to very similar objective values. To obtain all solutions for MMOPs, many multimodal multi-objective evolutionary algorithms (MMEAs) have been proposed. For now, few studies have encompassed most of the recently proposed representative MMEAs and made a comparative comparison. In this study, we first review the related works during the last two decades. Then, we choose 12 state-of-the-art algorithms that utilize different diversity-maintaining techniques and compared their performance on existing test suites. Experimental results indicate the strengths and weaknesses of different techniques on different types of MMOPs, thus providing guidance on how to select/design MMEAs in specific scenarios.
### [Blockwise Sequential Model Learning for Partially Observable Reinforcement Learning | Papers With Code](https://paperswithcode.com/paper/blockwise-sequential-model-learning-for)
> 概要: This paper proposes a new sequential model learning architecture to solve partially observable Markov decision problems. Rather than compressing sequential information at every timestep as in conventional recurrent neural network-based methods, the proposed architecture generates a latent variable in each data block with a length of multiple timesteps and passes the most relevant information to the next block for policy optimization.