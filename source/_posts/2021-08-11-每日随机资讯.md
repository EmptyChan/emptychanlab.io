---
title: 2021-08-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DinoShower_ZH-CN1791773864_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-08-11 23:09:54
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DinoShower_ZH-CN1791773864_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [PgBouncer 1.16.0 发布，可在实例中实时加载 TLS 设置](https://www.oschina.net/news/154768/pgbouncer-1-16-0-released)
> 概要: PgBouncer 是一个开源的、轻量级的、用于 PostgreSQL 的连接池。它可以为一个或多个数据库建立连接池，并通过 TCP 和 Unix domain sockets 为客户提供服务。PgB......
### [英特尔为其 C/C++ 编译器全面采用 LLVM](https://www.oschina.net/news/154778/intel-adoption-of-llvm-complete-icx)
> 概要: 英特尔的长期编译器专家 James Reinders 在一篇博客中透露，他们将在下一代英特尔 C/C++ 编译器中使用 LLVM 开源基础架构；并分享了一些相关信息。“LLVM 有助于我们实现为英特尔......
### [RT-Thread 国产 MCU 移植贡献活动开启](https://www.oschina.net/news/154833)
> 概要: 2020年下半年开始，史无前例的芯片缺货潮拉开大幕。供需失衡之下，芯片的交期和价格不断拉升，其中以国外大牌MCU最为夸张，一度出现价格上涨几十倍，有钱买不到货的局面。在此背景下，不少中小型终端制造企业......
### [EventMesh v1.2.0 发布，新增 7 名 contributor（务必阅读！！！）](https://www.oschina.net/news/154889/eventmesh-1-2-0-released)
> 概要: 作者：薛炜明这个版本是EventMesh进入apache孵化的首个版本，我非常荣幸能够担任本次EventMesh这个大版本的Release Manager。在这个版本中，社区贡献者们完成了大量的优化和......
### [纯情又天真的狗粮，管够！](https://news.dmzj.com/article/71841.html)
> 概要: 本周直播预告
### [TikTok overtakes Facebook as most downloaded app](https://asia.nikkei.com/Business/Technology/TikTok-overtakes-Facebook-as-world-s-most-downloaded-app)
> 概要: TikTok overtakes Facebook as most downloaded app
### [游戏《天穗之咲稻姬》推出官方后传小说](https://news.dmzj.com/article/71846.html)
> 概要: 美少男的喉结···斯哈斯哈
### [任天堂东京宣布临时停业！发现1名感染新冠病毒员工](https://news.dmzj.com/article/71849.html)
> 概要: 根据魔夜峰央原作制作的真人电影《飞翔吧！埼玉》宣布了续篇《飞翔吧！埼玉2（暂定）》制作决定的消息。本作预计将于2022年上映。
### [宁德时代有妖气](https://www.huxiu.com/article/447458.html)
> 概要: 作者｜Eastland头图｜宁德官网2021年7月30日，中央政治局会议要求纠正“运动式减碳”，给缺乏统筹规划、不经科学论证、脱离实际的冒进苗头敲响了警钟。资本市场对新能源概念股的疯狂炒作，部分个股严......
### [猩猩当女主的TV动画《进化之实》公开PV](https://news.dmzj.com/article/71852.html)
> 概要: 根据美红、U35原作制作的TV动画《进化之实 不知不觉中踏上胜利的人生》公开了新的PV和宣传图。在这次的宣传图中，可以看到进化前和进化后的男女主角。在PV中收录了主人公等人穿越到异世界等的场景。
### [为张文宏“免疫”](https://www.huxiu.com/article/447505.html)
> 概要: 本文来自微信公众号：量子学派（ID：quantumschool），作者：傅丽叶，本文首发时间：2020年3月31日，头图来自：视觉中国一该给张文宏先生“免疫”了，他会成为某些人的眼中钉与肉中刺。在疫情......
### [组图：陈伟霆MV橘色发型曝光 身穿白T清爽帅气氛围感十足](http://slide.ent.sina.com.cn/y/slide_4_704_360272.html)
> 概要: 组图：陈伟霆MV橘色发型曝光 身穿白T清爽帅气氛围感十足
### [组图：34岁韩星朴河宣亮相上班路 粉绿撞色搭配亮眼](http://slide.ent.sina.com.cn/star/k/slide_4_704_360273.html)
> 概要: 组图：34岁韩星朴河宣亮相上班路 粉绿撞色搭配亮眼
### [漫威《假如？》首集IGN6分：配音、动画质量不高](https://acg.gamersky.com/news/202108/1414263.shtml)
> 概要: 漫威全新动画剧集《What if...？》现已正式上映，IGN为该剧集首集给出了6分评价
### [《R6：围攻》Y6S3主题公布  水晶卫士](https://www.3dmgame.com/news/202108/3820971.html)
> 概要: 《彩虹六号：围攻》官方推特账号发布了一段关于游戏第6年第3赛季的短视频。该视频中包含了本季的名称Crystal Guard（暂译水晶卫士），并在视频中显示了一个由水晶碎片制成的角色。这个预告片包含一些......
### [龙珠：那个无敌的男人又来了](https://new.qq.com/omn/20210811/20210811A05P2U00.html)
> 概要: 龙珠：那个无敌的男人又来了
### [《刺客信条：英灵殿》DLC围攻巴黎最快今晚解锁](https://www.3dmgame.com/news/202108/3820972.html)
> 概要: 《刺客信条》官方推特发布了《刺客信条：英灵殿》DLC围攻巴黎详细解锁时间表，购买了季票的玩家最快今天（8月11日）晚上就能玩到。根据这份时间表，购买了季票的育碧Connect、Epic和Xbox用户在......
### [【佳作】HG亚斯塔禄，无涂装旧化](https://new.qq.com/omn/20210811/20210811A05R1700.html)
> 概要: 作者：放荡的小河HG亚斯塔禄，无涂装旧化。懒得拍制作流程图，大家随便看看效果图，吐吐槽就好......
### [【新手村】巴巴托斯，本想村不收的……](https://new.qq.com/omn/20210811/20210811A05QUK00.html)
> 概要: 作者：Zer0-27做旧埋汰巴巴托斯，进行了简单的渍洗，消光和旧化粉，第三个mg了，特别喜欢巴巴托斯，认为巴巴托斯不应该是全新的，所以做了一个比较重口味的，还望大佬们海涵，手法不是很成熟，也在慢慢练习......
### [网传《艾尔登法环》PC配置或是假的 跟大镖客2一样？](https://www.3dmgame.com/news/202108/3820973.html)
> 概要: 之前有外媒曝光了《艾尔登法环》PC配置需求，整体而言比较亲民。玩家仅需配备i5-2500K的CPU以及GTX 770显卡即可运行这款游戏，推荐i7-4770K和GTX 1060 6GB，但容量需求比较......
### [韩国大型金融公司GME加入RippleNet以扩展从韩国到泰国的支付业务](https://www.btc126.com//view/181276.html)
> 概要: Ripple宣布韩国大型金融公司GlobalMoneyExpress（GME）已通过与SBIRippleAsia（SBIHoldings和Ripple的合资企业）的合作，加入Ripple的全球金融网络......
### [旅游预定平台Travala接受稳定币UST支付](https://www.btc126.com//view/181277.html)
> 概要: 8月11日消息，旅游预定平台Travala接受稳定币UST支付，用户将能够使用UST预订超过230个国家的220多万家酒店、600多家航空公司和4万多个旅游活动......
### [New Constructs CEO：Coinbase的市值明显被高估了](https://www.btc126.com//view/181278.html)
> 概要: 8月11日消息，Coinbase周二公布财报，收入超过了分析师的预期，但一家美国投资研究公司仍然认为这家美国最大的加密货币交易所的市值明显被高估了。位于纳什维尔的公司New Constructs的首席......
### [藤子不二雄全集电子版9月上线 哆啦A梦等陆续公开](https://acg.gamersky.com/news/202108/1414288.shtml)
> 概要: 日本知名漫画家藤子·F·不二雄绘制过很多漫画，官方也推出了藤子·F·不二雄大全集，现在这套大全集即将推出电子版，将于9月3日正式上线。
### [黑客：已决定归还资产，不再创建DAO组织](https://www.btc126.com//view/181284.html)
> 概要: 8 月 11 日，攻击Poly Network的黑客在区块高度 13001631 转账中表示，已决定归还资产，不再创建 DAO 组织。同时，在描述中，黑客自称为传奇......
### [「LoveLive!SuperStar!!」第3话插曲「Tiny Stars」动画MV公开](http://acg.178.com/202108/422656898112.html)
> 概要: 近日，由日升动画、Lantis和「电击G's magazine」联合企划，日升动画制作的电视动画「LoveLive!SuperStar!!!」公开了第3话插入曲「Tiny Stars」的动画MV，本曲......
### [Default disappearing messages](https://signal.org/blog/disappearing-by-default/)
> 概要: Default disappearing messages
### [纯爱动画《乔西的虎与鱼》定档 8月20日浪漫上映](https://acg.gamersky.com/news/202108/1414283.shtml)
> 概要: 由骨头社制作的纯爱动画《乔西的虎与鱼》正式定档8月20日登陆内地院线。
### [藤子·F·不二雄大全集电子版9月3日上线](http://acg.178.com/202108/422658693177.html)
> 概要: 为纪念藤子·F·不二雄博物馆开馆10周年，官方宣布藤子·F·不二雄大全集电子版将于9月3日上线，首弹作品包括「哆啦A梦」、「Q太郎」、「小超人帕门」、「超能力魔美」等各部分前卷。藤子·F·不二雄（19......
### [北村匠海新电影票房大爆 演艺生涯再添浓重一笔](https://ent.sina.com.cn/s/j/2021-08-11/doc-ikqcfncc2233115.shtml)
> 概要: （文/枣）　　　　　　而担当这部电影主演的北村匠海，因为这部电影再次获得了实绩。　　　　　　　　之后，2008年拍摄了《太阳与海的教室》、　　　　　　同时兼顾戏剧和音乐，北村匠海也都获得了不错的成绩：......
### [Australia is becoming a surveillance state](https://ia.acs.org.au/article/2021/australia-is-becoming-a-surveillance-state.html)
> 概要: Australia is becoming a surveillance state
### [漫画杂志「COMIC Be」vol.98 9月号封面公开](http://acg.178.com/202108/422666439472.html)
> 概要: 漫画杂志「COMIC Be」vol.98 2021年9月号封面图正式公开，封面主要角色来自于永井三郎创作的漫画作品「深潭回廊」，该商品将于2021年8月13日发售，由ふゅーじょんぷろだくと负责出版......
### [谢大脚：象牙山的美丽传说](https://www.huxiu.com/article/447638.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水题图 | 东方IC本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。如果象牙山是东北黑土......
### [著名AR游戏开发商Niantic收购3D扫描应用团队Toolbox AI](https://www.tuicool.com/articles/V7JfY3U)
> 概要: 8月10日，著名AR游戏开发商 Niantic 正式宣布，公司已正式收购3D扫描移动应用开发团队 Toolbox AI，其创始人 Keith Ito 也将加入 Niantic 团队。据了解，Toolb......
### [「无限滑板」X「Atfes」宣传绘与周边商品图公开](http://acg.178.com/202108/422669172871.html)
> 概要: 近日，「Atfes」店铺公开了与「无限滑板」进行联动的活动宣传绘和相关的周边商品情报，本次周边包括角色贴纸、勋章、书签、钥匙扣、亚力克板等等。这些商品已经在Atfes官网开启预订......
### [《扫毒2》被指涉嫌抄袭 刘德华等遭索赔近1亿元](https://ent.sina.com.cn/m/c/2021-08-11/doc-ikqcfncc2250242.shtml)
> 概要: 因认为刘德华监制并主演的影片《扫毒2》在人物设置、叙事结构、故事背景、人物关系、重要情节、结局表达、人物职业、拍摄表现手法、人物海报设计、片名设计、服装设计等诸多方面几乎是“完全”抄袭成都环球博纳文化......
### [陈露好友王萌再发长文：既然锤了就锤到底](https://ent.sina.com.cn/s/m/2021-08-11/doc-ikqciyzm0855363.shtml)
> 概要: (责编：小5)......
### [Plants, Heavy Metals, and the Lingering Scars of World War I](https://www.atlasobscura.com/articles/zone-rouge-plant-growth)
> 概要: Plants, Heavy Metals, and the Lingering Scars of World War I
### [CSS3媒体查询media_queries响应式布局入门指南知识点总结](https://www.tuicool.com/articles/Iz2imeQ)
> 概要: 什么是媒体查询我们知道，我们给网页HTML写样式用的主要是CSS（层叠样式表）语言的规则，比如：盒模型设置宽高背景色什么的。目前我们使用的最新版本的层叠样式表主要是CSS第三版，也就是CSS3，CSS......
### [微软 Surface Laptop 3 锐龙版获得 8 月固件更新](https://www.ithome.com/0/568/715.htm)
> 概要: IT之家8 月 11 日消息 微软已经陆续为多款 Surface 系列产品推送了固件更新，今日，Surface Laptop 3 锐龙版获得了 8 月固件更新。本次固件更新主要修补了一些漏洞，并提高了......
### [现在的日漫，为什么优秀作品越来越少了？](https://new.qq.com/omn/20210811/20210811A0542P00.html)
> 概要: 动漫看得多了，应该能发现，大部分动漫都带着“宅气”，从题材、情节到人物处理都有一种浓厚的“动漫特色”，原因是动漫的观众主体是青少年，这些都是为他们量身定制的风格。但有少数作品的“宅气”很少。之前看了一......
### [《刺客信条：英灵殿》全新 DLC“围攻巴黎”明日正式推出](https://www.ithome.com/0/568/729.htm)
> 概要: IT之家8 月 11 日消息 根据育碧官方消息，《刺客信条：英灵殿》第二部扩展内容”围攻巴黎”将于明日（8 月 12 日）正式推出，官方发布了一个预告片。IT之家了解到，此次扩展包包含于季票，也可单独......
### [《向往的生活》素质很差的一个嘉宾，室内当众抽烟，黄磊全程无视](https://new.qq.com/omn/20210811/20210811A0DANF00.html)
> 概要: 《向往的生活》素质很差的一个嘉宾，室内当众抽烟，黄磊全程无视网络上火爆的综艺有许多，大家提起比较多的几档综艺节目，亲子类的《爸爸去哪了》，选秀类的《创造营2020》、《青春有你2》，还有让大家笑声不断......
### [雷军的造车“米家军”：潜伏十年，下注38家公司](https://www.tuicool.com/articles/BRBZNja)
> 概要: 昨晚，雷军发表了一场超过了三个小时的演讲，将小米成立10年来的困难和成绩都做了一次梳理，也让人了解到了小米光环下更多的故事。但在激情讲了三个小时后，雷军并没有提一句关于造车的事情。不过，作为雷军“这辈......
### [华米 Amazfit GTR 2/GTR 2e/GTR 2 eSIM/GTS 2e 迎来一大波表盘更新](https://www.ithome.com/0/568/735.htm)
> 概要: IT之家8 月 11 日消息 华米宣布，Amazfit GTR 2/GTR 2e/GTR 2 eSIM/GTS 2e 四款产品迎来一波表盘大更新，用户可打开 App 查看及下载使用。华米去年 9 月发......
### [同样是扮嫩，杨洋热巴赵丽颖演高中生却大获好评！是观众双标吗？](https://new.qq.com/omn/20210811/20210811A0EJQ600.html)
> 概要: 同样是扮嫩，杨洋热巴赵丽颖演高中生却大获好评！是观众双标吗？            自从《如懿传》里周迅演的少女和《上阳赋》里章子怡演的14岁少女，以及《大秦赋》里张鲁一的15岁少年之后，很多观众都开......
### [BCH链常用接口使用](https://www.tuicool.com/articles/uyEv2eA)
> 概要: BCH链常用接口使用注：以下rpc接口测试均在开发网络完成区块链APIgetblockcount 获取最新区块高度getblockhash 获取指定高度区块的哈希(#getblockh...BCH链常......
### [18岁体操正妹MarinaGonzález福利图 美腿很吸睛](https://www.3dmgame.com/bagua/4795.html)
> 概要: 今天为大家带来的是西班牙体操选手Marina González Lara的福利图。尽管她没在本届奥运会取得理想成绩，但是年仅18岁的她，一定会为下届巴黎奥运会积累不少经验。MarinaGonzále......
### [国内首个 700M 5G NR 基站在上海建成](https://www.ithome.com/0/568/754.htm)
> 概要: IT之家8 月 11 日消息 据证券时报报道，上海经信委消息，为开展 5G 广播技术验证和联调联试，2021 年 6 月，国家广播电视总局科技司、中国广播电视网络集团有限公司按计划在上海进行 5G N......
### [河南卫辉：蓄滞洪区住房按照水毁损失的70%补偿](https://finance.sina.com.cn/jjxw/2021-08-11/doc-ikqciyzm0904374.shtml)
> 概要: 原标题：河南卫辉：蓄滞洪区住房按照水毁损失的70%补偿 河南日报微信公号8月11日消息，自7月22日以来，根据防汛减灾工作需要，卫辉市良相坡...
### [《碟中谍7》曝光全新片场照 目前仍在拍摄中](https://www.3dmgame.com/news/202108/3821023.html)
> 概要: 近日， 汤姆·克鲁斯近主演的《碟中谍7》曝光全新片场照，导演克里斯托夫·迈考利介绍本片的第一副导演（AD）玛丽·博尔丁(Mary Boulding)。《碟中谍7》是博尔丁第一任担任AD的电影，拍照当天......
### [南京禄口街道办事处原主任等3人被查](https://finance.sina.com.cn/wm/2021-08-11/doc-ikqcfncc2302640.shtml)
> 概要: 原标题：南京禄口街道办事处原主任等3人被查 8月11日晚，南京市纪委监委通报：江宁区禄口街道党工委原副书记、办事处原主任朱茂胜，江宁区禄口街道陆纲社区党委书记...
### [河南卫辉公布蓄滞洪区补偿对象及范围、补偿标准](https://finance.sina.com.cn/china/gncj/2021-08-11/doc-ikqcfncc2303545.shtml)
> 概要: 原标题：河南卫辉公布蓄滞洪区补偿对象及范围、补偿标准 来源：央视新闻客户端 据河南卫辉市防汛抗旱指挥部消息：自7月22日以来，根据防汛减灾工作需要，卫辉市良相坡...
### [中信保诚孙浩中：当前新能源估值不便宜 但高增速相对确定](https://finance.sina.com.cn/china/gncj/2021-08-11/doc-ikqciyzm0907127.shtml)
> 概要: 8月11日消息，今日A股储能、钠离子等概念板块暴涨，新能源方向依旧备受瞩目。有市场声音称，虽然新能源存在巨大成长空间，但是短期估值偏高，存在降温风险。
### [霍尊竟是当代陈世美，其父回应极力护儿，却被网友嘲讽：遗传你](https://new.qq.com/omn/20210811/20210811A0FVHD00.html)
> 概要: 今年娱乐圈可谓是被明星私生活事件闹得沸沸扬扬。对此，一些网友甚至做出神评“我就像个在瓜田里上蹿下跳的猹，一时之间竟不知从何吃起。”评论着实令人捧腹大笑，但却真实反映出圈内的瓜太多了。这不，吴亦凡事件才......
### [广州商务楼宇等级不再“自称”，楼宇经济浪潮从中心向周边辐射](https://finance.sina.com.cn/china/gncj/2021-08-11/doc-ikqciyzm0907797.shtml)
> 概要: 原标题：广州商务楼宇等级不再“自称”，楼宇经济浪潮从中心向周边辐射 楼宇经济进入新一轮高质量发展期，将进一步推动产业结构调整...
### [流言板迪马：尤文与迪巴拉第二次续约谈判进展顺利，但还没谈年薪](https://bbs.hupu.com/44722687.html)
> 概要: 虎扑08月11日讯 根据意大利天空体育记者迪马济奥的最新报道，今天下午尤文管理层与迪巴拉经纪人进行了第二次续约谈判，虽然整个过程进展顺利，但双方并未提及具体年薪数字。8月7日迪巴拉经纪人Jorge A
### [心动4：马子佳最后一次约的是小孔？两人一起卸妆，或上演反转](https://new.qq.com/omn/20210811/20210811A0G0P200.html)
> 概要: 《心动的信号4》已经播到第八期，素人嘉宾们开启两天一夜郊外游，享受旅行的同时，也到了最后确定心意的时候，陈思铭方彬涵这对“方程式”CP稳稳的，虽然大熊一直对方彬涵有好感，多次示好，但是方彬涵已经心有所......
### [补上二手房“阴阳合同”漏洞！上海房贷将实施“三价就低” 原则](https://finance.sina.com.cn/wm/2021-08-11/doc-ikqcfncc2306549.shtml)
> 概要: 在提高房贷利率之后，上海地区再次出台了针对信贷端调控的房贷“补丁”。 记者今日从多家中介以及相关银行处确认，上海二手房申请房贷将参考合同网签价、银行评估价...
### [流言板是否参加24奥运？库里：一直都在想，让我们看看会发生什么](https://bbs.hupu.com/44722803.html)
> 概要: 虎扑08月11日讯 勇士后卫斯蒂芬-库里接受采访，谈到自己是否会参加2024年的奥运会。库里说：“我一直都在想这件事。”“显然，我希望能够再有一次可以决定自己打或者不打的机会，从过去一个赛季发生的一切
### [流言板古特比：泰山和沧州共同创造一场精彩比赛；预祝国足好运](https://bbs.hupu.com/44722928.html)
> 概要: 虎扑08月11日讯 中超第一阶段末轮比赛，沧州雄狮1-2不敌泰山队，第二阶段将进入保级组。赛后，沧州雄狮主帅古特比向即将参加12强赛的国足送上了祝福。古特比赛后表示：“恭喜山东队在第一阶段获得了小组第
### [流言板戈登：上赛季的一切像一阵风一样，让我觉得是时候重建了](https://bbs.hupu.com/44722971.html)
> 概要: 虎扑08月11日讯 今天，火箭球员埃里克-戈登接受了采访。谈到上赛季时，戈登说：“这一切就像一阵旋风一样，伤病和其他各种各样的干扰充满了一切，这让我有些惊讶的感觉到，可能是时候进行重建让阵容年轻化了。
# 小说
### [亡灵机甲](http://book.zongheng.com/book/656768.html)
> 作者：一意尘虚

> 标签：奇幻玄幻

> 简介：机甲小子意外穿越，来到了充满奇幻的异大陆；在这里斗气魔法盛行，兽人精灵矮人魔族横行；且看天才小子如何一步步的改装机甲，从人走上异界巅峰。

> 章节末：第四百五十四章：陷阱

> 状态：完本
# 论文
### [Transfer Learning with Graph Neural Networks for Short-Term Highway Traffic Forecasting](https://paperswithcode.com/paper/transfer-learning-with-graph-neural-networks)
> 日期：17 Apr 2020

> 标签：TIME SERIES

> 代码：https://github.com/tanwimallick/TL-DCRNN

> 描述：Highway traffic modeling and forecasting approaches are critical for intelligent transportation systems. Recently, deep-learning-based traffic forecasting methods have emerged as state of the art for a wide range of traffic forecasting tasks.
### [Analysis of Models for Decentralized and Collaborative AI on Blockchain](https://paperswithcode.com/paper/analysis-of-models-for-decentralized-and)
> 日期：14 Sep 2020

> 标签：SENTIMENT ANALYSIS

> 代码：https://github.com/microsoft/0xDeCA10B

> 描述：Machine learning has recently enabled large advances in artificial intelligence, but these results can be highly centralized. The large datasets required are generally proprietary; predictions are often sold on a per-query basis; and published models can quickly become out of date without effort to acquire more data and maintain them.
