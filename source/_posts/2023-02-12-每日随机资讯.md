---
title: 2023-02-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BoobyDarwinDay_ZH-CN9917306809_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-12 21:59:33
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BoobyDarwinDay_ZH-CN9917306809_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [低频且重服务的蔚来汽车，是如何做用户运营的？](https://www.woshipm.com/operate/5751213.html)
> 概要: 巨大的行业差异和用户需求结构的不确定，对于新势力车企做用户运营并没有造成发展的障碍，而是一种全新的营销方式。本文就蔚来汽车的用户运营进行总结分析，谈谈传统用户运营的变更，希望对你有所帮助。那天上午，无......
### [用AI加强产品设计·识别篇——如何利用文字识别（OCR）、语音识别（ASR）和机器翻译（NMT）提升用户体验](https://www.woshipm.com/pd/5750503.html)
> 概要: 作为一名产品经理，超自然地满足用户需求，用户体验就会更好。在日常中，大家很难将自己看不懂的文字或者不同APP的一些信息直接转化到这个APP中，这时候使用一点AI是我们的最佳选择。本文带你快速地了解到如......
### [家里的电视，从老人到小孩都玩不明白](https://www.woshipm.com/it/5748129.html)
> 概要: #本文为人人都是产品经理《原创激励计划》出品。最近家里的电视总是频频登上热搜，不是被吐槽遥控器多、使用麻烦，就是吐槽会员捆绑严重、能看的节目寥寥无几。曾经，电视的使用频率不亚于手机，是国人家里必备电器......
### [为什么日本陪酒女比高启强更爱《孙子兵法》？](https://www.huxiu.com/article/791292.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。世界上没有人能比强哥更......
### [《Lycoris Recoil 莉可丽丝》宣布推出新作动画](https://www.3dmgame.com/news/202302/3862496.html)
> 概要: 原创动画《Lycoris Recoil 莉可丽丝》稍早于日本举行的活动中，宣布将推出新作动画的消息。《Lycoris Recoil 莉可丽丝》故事描述，平稳的日子底下却隐藏着秘密。将犯罪防范于未然的秘......
### [追返4000万物业费，“上海最牛业委会”是怎样炼成的？](https://www.huxiu.com/article/791583.html)
> 概要: 本文来自微信公众号：真叫卢俊 （ID：zhenjiaolujun0426），作者：乔不丝，原文标题：《这大概是上海最牛业委会了吧》，头图来自：视觉中国这两天上海的这个小区又火了，你一定听说过它的大名，......
### [广井王子《Sakura Ignoramus》2月27日正式运营](https://www.3dmgame.com/news/202302/3862499.html)
> 概要: 日前由广井王子策划的新作手游《Sakura Ignoramus》官方日前确定了运营日期，将于2月27日正式运营，事先注册已经开启，游戏关键字依然是美少女、樱花与战棋，感兴趣的玩家可以关注下了。《Sak......
### [慕名观看！成年人允许涩涩](https://news.dmzj.com/article/77104.html)
> 概要: 最近很无聊，不如看点儿刺激感官的网剧吧！
### [DC冈导解释《神奇女侠》剧版 世界观一如《权游》](https://www.3dmgame.com/news/202302/3862506.html)
> 概要: 日前DC工作室联合负责人詹姆斯·冈恩宣布DC宇宙（DCU）开幕，第一章代号《众神与怪物（Gods and Monsters）》，敬请期待。·詹姆斯·冈恩同时表示正在策划制作《神奇女侠》电视剧版《Par......
### [2023年，深圳两会在即，这些经济热点值得关注！](https://finance.sina.com.cn/china/2023-02-12/doc-imyfmatu2472535.shtml)
> 概要: 南方财经全媒体见习记者李金萍 深圳报道2023年，全面贯彻落实党的二十大精神的开局之年，决定深圳经济社会发展进程的深圳两会也将在本月13日正式召开...
### [《中国奇谭》今日收官！上美影厂还有三部新作](https://acg.gamersky.com/news/202209/1517979.shtml)
> 概要: 动画《中国奇谭》于今日正式播出第八集，这部豆瓣评分9.2的优秀国漫迎来正式收官。
### [炒期货炒成卖苹果！期货大V抄底“翻车”被迫现货交割，本人：已售300多吨](https://finance.sina.com.cn/jjxw/2023-02-12/doc-imyfmats5720844.shtml)
> 概要: 近日，一则“资深期货大V炒苹果期货翻车”的消息引起了广泛关注。界面新闻联系到了这一事件的主角林先生（化名）。他表示，本来想“入市抄底”，没想到事情走向越来越“离谱”。
### [全面“通关”后首个周末 内地旅客访港消费助经济复苏](https://finance.sina.com.cn/china/2023-02-12/doc-imyfmhzs2495739.shtml)
> 概要: 中新社香港2月12日电 （记者 魏华都 戴小橦）内地与香港恢复全面“通关”后的首个周末，大批内地旅客来港，到上水、尖沙咀等热门购物区“扫货”...
### [千万吨级炼化一体化项目！中国石油广东石化项目顺利投产](https://finance.sina.com.cn/money/future/roll/2023-02-12/doc-imyfmhzq5711701.shtml)
> 概要: 中国石油今天（2月12日）发布消息，千万吨级炼化一体化项目——广东石化炼化项目乙烯装置顺利产出合格产品，进入全面试生产阶段。
### [【众生相】湖人迎来交易后首场胜利：连胜从此刻开始，季后赛不再遥不可及了？](https://bbs.hupu.com/57921666.html)
> 概要: 湖人109-103战胜勇士，迎来大交易后的首胜！湖人官推赛后更新话题状态：#LakersWin·WE’RE BACK😭我们回来了·Jared Vanderbilt is the DPOY范德比尔特是
### [分析：推特前 1000 名广告主中超一半停止投放广告](https://www.ithome.com/0/672/835.htm)
> 概要: 感谢IT之家网友华南吴彦祖、乌蝇哥的左手的线索投递！IT之家2 月 12 日消息，根据数字营销分析公司 Pathmatics 向 CNN 提供的数据，去年 9 月份在 Twitter 排名前 1000......
### [医疗健康行业周报 | 字节健康开启抖音直播卖药；赛得康获数千万元种子轮投资](https://36kr.com/p/2128348828937476)
> 概要: 医疗健康行业周报 | 字节健康开启抖音直播卖药；赛得康获数千万元种子轮投资-36氪
### [华硕 ROG 银翼人体工学电竞椅上架，到手价 4699 元](https://www.ithome.com/0/672/842.htm)
> 概要: 感谢IT之家网友OC_Formula的线索投递！IT之家2 月 12 日消息，上个月华硕 ROG 玩家国度召开了 2023 新品发布会，推出了多款外设装备，其中就包括 ROG 银翼人体工学电竞椅，目前......
### [Opera新版本集成ChatGPT：一键即可生成网页内容摘要](https://www.3dmgame.com/news/202302/3862518.html)
> 概要: Opera浏览器在测试版中尝试整合ChatGPT，基于AI提供一种工具功能。在测试版中，用户可以在侧边栏随时呼出ChatGPT，并通过它来一键生成当前正在阅读的网页的内容摘要。对于用户来说，该功能能够......
### [这颗病房监护行业的新星，引得英特尔、思科、戴尔三巨头争先入场！](https://36kr.com/p/2128089626143744)
> 概要: 这颗病房监护行业的新星，引得英特尔、思科、戴尔三巨头争先入场！-36氪
### [如何理解中年“废物论”](https://www.huxiu.com/article/792441.html)
> 概要: 面对冷峻现实，沉浸在“废物世界”中自我麻醉，只会更容易滑向现实与理想互相撕扯的精神深渊。本文来自微信公众号：知著网（ID：covricuc），作者：克西莉亚，原文标题：《中年“废物论”：在异化反思中打......
### [【赛后复盘】2023/02/12 济南RW-成都AG第一局赛后复盘（纯分析）](https://bbs.hupu.com/57923062.html)
> 概要: 【GAME 1】RW蓝色方、AG红色方【Pa 1-阵容分析】Ban/Pick注：禁用英雄为两轮禁用环节先后顺序，选用英雄后数字为选用环节先后顺序；RW 禁用：蒙恬大乔、蒙犽伽罗（以Ban/Pick顺序
### [TV动画《莉可丽丝》宣布新作动画制作决定](https://news.dmzj.com/article/77114.html)
> 概要: TV动画《莉可丽丝》宣布新作动画制作决定。
### [《数码宝贝》25周年纪念新企划“DIGIMON SEEKERS”公开](https://news.dmzj.com/article/77116.html)
> 概要: 《数码宝贝》25周年纪念新企划“DIGIMON SEEKERS”公开。
### [全家福！《中国奇谭》各集导演送上共10张收官贺图](https://acg.gamersky.com/news/202302/1565471.shtml)
> 概要: 动画《中国奇谭》今日完结，官方送上10张导演收官贺图，感谢每一位支持该动画的观众朋友。
### [《灌篮高手》韩国票房日系第二 仅次《你的名字》](https://www.3dmgame.com/news/202302/3862525.html)
> 概要: 文化交流是国际友好关系的可靠桥梁，截至2月11日的最新统计，《灌篮高手》新动画电影《THE FIRST SLAM DUNK》在韩国观影人数突破273万人，超越《哈尔的移动城堡》成为日本动画电影影史第二......
### [北交所两融业务明起开闸](https://finance.sina.com.cn/jjxw/2023-02-12/doc-imyfmtrt2734232.shtml)
> 概要: 从融资融券交易细则下发到全网测试，历时数月，北交所两融业务将在2月13日正式启动。据了解，北交所两融业务首批纳入标的共有诺思兰德（430047）、华岭股份等56只个股...
### [视频：杨子为黄圣依庆生甜蜜合照 高调示爱称妻子为“公主”](https://video.sina.com.cn/p/ent/2023-02-12/detail-imyfmtru9523489.d.html)
> 概要: 视频：杨子为黄圣依庆生甜蜜合照 高调示爱称妻子为“公主”
### [视频：张新成泰国度假被偶遇 穿红色夏威夷衬衫少年感十足](https://video.sina.com.cn/p/ent/2023-02-12/detail-imyfmtrt2747487.d.html)
> 概要: 视频：张新成泰国度假被偶遇 穿红色夏威夷衬衫少年感十足
### [视频：陈乔恩与老公马来西亚度假 路边摊吃炒粉与店家亲切合照](https://video.sina.com.cn/p/ent/2023-02-12/detail-imyfmtrp9127403.d.html)
> 概要: 视频：陈乔恩与老公马来西亚度假 路边摊吃炒粉与店家亲切合照
### [延迟退休到70岁的日本，如今怎么样了](https://www.huxiu.com/article/792486.html)
> 概要: 本文来自：时代周报，作者：马欢，头图来自：视觉中国73岁的大岛良每天凌晨1点多就起床了，简单收拾一下后，便开着卡车出发，去往东京周边的菜市场上，将各类新鲜蔬菜打包装车，运给东京的餐馆。这并不是一份轻松......
### [“微信头像褪色”登顶微博热搜，官方回应称“相关功能已在优化中”](https://www.ithome.com/0/672/859.htm)
> 概要: IT之家2 月 12 日消息，“微信头像褪色”于今日登上了微博热搜第一，有网友反映微信头像出现褪色的情况，用了一段时间的头像照片与原图相比清晰度和色彩变暗淡了。也有网友猜测，是由于压缩画质导致的。对此......
### [《异修罗》TV动画化决定](https://news.dmzj.com/article/77117.html)
> 概要: 梶裕贵·上田丽奈 主役《异修罗》TV动画化决定。
### [雅本化学入选“中证苏州创新50指数”](http://www.investorscn.com/2023/02/12/105709/)
> 概要: 2月10日，在苏州在迎来第201家A股上市公司之际，苏州市促进上市公司高质量发展推进会上正式发布了中证苏州创新50指数，雅本化学(300261.SZ)成功入选......
### [ChatGPT热炒，后市怎么走？两市成交热情再度下降，还有哪些板块值得关注](https://www.yicai.com/news/101672482.html)
> 概要: ChatGPT热炒，后市怎么走？两市成交热情再度下降，还有哪些板块值得关注
### [麦迪科技投资18亿进入光伏产业，“业务双轮驱动战略”正式启航](http://www.investorscn.com/2023/02/12/105710/)
> 概要: “双碳”背景下，光伏行业发展日新月异，麦迪科技致力于护航人的健康，已启动第二发展曲线，关注并致力于环境健康，从进入光伏业务领域入手。开拓双主业的麦迪科技（603990）顺应时代东风，进入新能源光伏，谋......
### [上市猪企1月续亏，今年猪价有望先抑后扬？](https://www.yicai.com/news/101672491.html)
> 概要: 结合上市猪企扩产节奏，板块有望进入增价平和正常盈利年份。
### [社论：抓牢平台经济发展新机遇](https://www.yicai.com/news/101672499.html)
> 概要: 面对新机遇，政策支持固然重要，更重要的是相关市场主体突破瓶颈，找到更大的发展空间。
### [再度惊吓市场，“日本伯南克”政策立场难捉摸](https://www.yicai.com/news/101672513.html)
> 概要: 在央行行长人选中，此前市场认定的黑田东彦继任者——雨宫正佳拒绝担任日本央行行长一职，日本转而拟提名现年71岁的日本央行货币政策委员会前成员植田和男担任下一任日本央行行长。
### [填补国内技术空白！“中国芯”新一代 94GHz 高频高性能超距毫米波雷达发布](https://www.ithome.com/0/672/880.htm)
> 概要: 感谢IT之家网友航空先生的线索投递！IT之家2 月 12 日消息，据河北交通投资集团消息，2 月 7 日，助力智慧交通 ——“中国芯”新一代高频高性能超距毫米波雷达新技术新成果发布会在北京成功举办。河......
### [管涛：关注超级美元周期滋生的新美元泡沫｜汇海观涛](https://www.yicai.com/news/101672519.html)
> 概要: 若随着货币紧缩政策的滞后效果显现，美国经济增速放缓、失业率上升，强势美元或会重新引起美国民众的关注和非议。
### [流言板滕哈格：桑乔状态好就该首发，马奎尔是我们的重要一员](https://bbs.hupu.com/57928364.html)
> 概要: 虎扑02月12日讯 北京时间今天晚上22点，英超第23轮，曼联将在客场对阵利兹联，开赛前，曼联主帅滕哈格接受了曼联电视台的采访。瓦拉内与利桑德罗没能首发一方面是轮换的需要，我有很多比赛要照顾到，另一方
### [流言板比斯利晒照：很棒的胜利！让我所有的打铁都滚蛋吧](https://bbs.hupu.com/57928688.html)
> 概要: 虎扑02月12日讯 今日，湖人109-103客场击败勇士。赛后，湖人后卫马利克-比斯利更新Instagram，晒出一组个人赛场照。“💜💛很棒的胜利！#湖人秀 #让我所有的打铁都滚蛋”比斯利写道。本
# 小说
### [穿到天龙去修仙](https://book.zongheng.com/book/1103672.html)
> 作者：我是你九弟

> 标签：武侠仙侠

> 简介：一个小商人因着急送货而出了一场车祸，魂穿金庸天龙平行世界。真的有平行空间吗？这个世界有没有仙？通过武功是否可以求得长生？人力有时穷真的不能打破吗？求真之路是否会放弃一切？让我们重温经典一起跟着主角开启穿越之旅！

> 章节末：第八百一十一章、大结局（二）

> 状态：完本
# 论文
### [IFRNet: Intermediate Feature Refine Network for Efficient Frame Interpolation | Papers With Code](https://paperswithcode.com/paper/ifrnet-intermediate-feature-refine-network)
> 概要: Prevailing video frame interpolation algorithms, that generate the intermediate frames from consecutive inputs, typically rely on complex model architectures with heavy parameters or large delay, hindering them from diverse real-time applications. In this work, we devise an efficient encoder-decoder based network, termed IFRNet, for fast intermediate frame synthesizing. It first extracts pyramid features from given inputs, and then refines the bilateral intermediate flow fields together with a powerful intermediate feature until generating the desired output. The gradually refined intermediate feature can not only facilitate intermediate flow estimation, but also compensate for contextual details, making IFRNet do not need additional synthesis or refinement module. To fully release its potential, we further propose a novel task-oriented optical flow distillation loss to focus on learning the useful teacher knowledge towards frame synthesizing. Meanwhile, a new geometry consistency regularization term is imposed on the gradually refined intermediate features to keep better structure layout. Experiments on various benchmarks demonstrate the excellent performance and fast inference speed of proposed approaches. Code is available at https://github.com/ltkong218/IFRNet.
### [CSRCZ: A Dataset About Corporate Social Responsibility in Czech Republic | Papers With Code](https://paperswithcode.com/paper/csrcz-a-dataset-about-corporate-social)
> 日期：5 Jan 2023

> 标签：None

> 代码：https://github.com/erionc/csrcz-stats

> 描述：As stakeholders' pressure on corporates for disclosing their corporate social responsibility operations grows, it is crucial to understand how efficient corporate disclosure systems are in bridging the gap between corporate social responsibility reports and their actual practice. Meanwhile, research on corporate social responsibility is still not aligned with the recent data-driven strategies, and little public data are available. This paper aims to describe CSRCZ, a newly created dataset based on disclosure reports from the websites of 1000 companies that operate in Czech Republic. Each company was analyzed based on three main parameters: company size, company industry, and company initiatives. We describe the content of the dataset as well as its potential use for future research. We believe that CSRCZ has implications for further research, since it is the first publicly available dataset of its kind.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
