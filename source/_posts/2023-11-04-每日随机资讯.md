---
title: 2023-11-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BisonSnow_ZH-CN2483472629_1920x1080.webp&qlt=50
date: 2023-11-04 21:25:30
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BisonSnow_ZH-CN2483472629_1920x1080.webp&qlt=50)
# 新闻
### [浅析潮流电商独角兽——得物App](https://www.woshipm.com/evaluating/5933825.html)
> 概要: 随着互联网的飞速发展，作为年轻潮流一代的喜爱——得物，它最大的特点就是具有彰显个性、时尚、潮流。它不仅是众多一线大牌和潮流品牌运营发售的首选平台，也是国内明星主理品牌聚集地，还是国潮文化的推动者。下面......
### [如何提升用户体验-用我们来找茬的视角](https://www.woshipm.com/share/5934052.html)
> 概要: 产品的效果是由用户体验来做决定的。下面这篇文章是笔者以智联招聘为例子，整理分享关于如何提升用户体验的相关内容，大家一起接着往下看看吧！一、背景描述笔者最近在找工作，今天在智联招聘注册了账号，把线下写好......
### [知乎：故事会的下半场](https://www.woshipm.com/it/5934412.html)
> 概要: 在《为有暗香来》这部故事被改编为影视剧后，大众对知乎网文、知乎IP改编似乎有了更进一步的探究欲。毕竟，网络文学发展得如火如荼，在多位玩家已下场的这条赛道上，知乎凭借什么可以来分一杯羹？知乎的网文之路，......
### [被“限高”的实控人，“跌跌不休”的股价，正威新材有点烦丨股市演义](https://www.yicai.com/video/101895599.html)
> 概要: 被“限高”的实控人，“跌跌不休”的股价，正威新材有点烦丨股市演义
### [《最终幻想7：重生》已获韩国评级 适合15岁以上玩家](https://www.3dmgame.com/news/202311/3880886.html)
> 概要: 史克威尔艾尼克斯新作《最终幻想7：重生》已在韩国完成分级。根据2023年11月3日新提交的清单显示，韩国游戏分级与管理委员会已经对《最终幻想7：重生》进行分级。根据韩国游戏评级分类，该游戏被评为15+......
### [商务部：中国消费市场潜力巨大、空间广阔，为广大外资企业带来新的发展机遇](https://www.yicai.com/news/101895673.html)
> 概要: 11月3日，商务部副部长盛秋平在上海主持召开汽车及消费品专场、服务贸易专场外资企业圆桌会，听取外资企业在华问题诉求及意见建议。
### [中央金融工作会议释放新信号；长期资金入市又有新举措丨一周热点回顾](https://www.yicai.com/news/101895674.html)
> 概要: 其他热点还有：10月制造业PMI重回荣枯线以下，时隔6年茅台提价20%。
### [《黑暗之魂2》画质MOD发布 最大限度保留原版效果](https://www.3dmgame.com/news/202311/3880897.html)
> 概要: MOD作者libretro针对《黑暗之魂2》发布了新版画质MOD，消除纹理LOD突然载入现象，并大幅增加绘图距离。MOD作者表示，这款作品采用了非常严谨的方式进行优化，不求改变游戏的外观，不添加额外的......
### [出海周刊69期｜跨境电商在深圳，从辉煌到坍塌、再到重建 / TikTok Shop重押美区](https://36kr.com/p/2502502872507779)
> 概要: 出海周刊69期｜跨境电商在深圳，从辉煌到坍塌、再到重建 / TikTok Shop重押美区-36氪
### [在杭州，我们看到了阿里AI生态的近百种应用 | 焦点分析](https://36kr.com/p/2502287054464392)
> 概要: 在杭州，我们看到了阿里AI生态的近百种应用 | 焦点分析-36氪
### [小米双11开门红支付金额破131亿元 位列国产手机第一](https://www.3dmgame.com/news/202311/3880905.html)
> 概要: 双11开门红活动截止昨晚23:59:59已经正式结束，小米第一时间晒出了成绩单。据官方公布，小米双11开门红全渠道支付金额破131亿元。同时，在多个平台都拿下了国产手机当之无愧的冠军，遥遥领先。天猫：......
### [汽车手机跨界又一案例，大众和这家手机大厂“牵手”了](https://www.yicai.com/news/101895773.html)
> 概要: 手机大厂能不能“拯救”大众的智能座舱能力？
### [微软听取员工意见 重新恢复免费XGP终极版福利](https://www.3dmgame.com/news/202311/3880913.html)
> 概要: 微软原计划在明年 1 月份取消为其 23.8 万名员工中的大多数员工提供的免费 Xbox Game Pass 终极版员工福利。但该消息引发了许多员工的不满，因此该公司撤回了这一决定。根据我们昨天报道，......
### [漫步者 NEW-X PRO 光冷能量音箱上架：支持 65W 快充，1399 元](https://www.ithome.com/0/730/058.htm)
> 概要: 感谢IT之家网友很宅很怕生的线索投递！IT之家11 月 4 日消息，漫步者花再 NEW-X PRO 光冷能量音箱现已上架，售价 1399 元。IT之家整理漫步者 NEW-X PRO 光冷能量音箱介绍如......
### [《COD现代战争3》上市前夕宣布删除武器调校功能](https://www.3dmgame.com/news/202311/3880919.html)
> 概要: 在下周的正式发售之前，Sledgehammer Games发布了一篇博客，详细介绍了游戏的升级系统，以及游戏中自定义武器内容的情况。然而，最为详细的自定义武器方式“武器调校”将不会在今年的游戏中出现......
### [8家消费公司拿到新钱，脑白金开咖啡快闪店，超七成受访者愿意选择月子中心｜创投大视野](https://36kr.com/p/2503486081131909)
> 概要: 8家消费公司拿到新钱，脑白金开咖啡快闪店，超七成受访者愿意选择月子中心｜创投大视野-36氪
### [第六届进博会即将开幕，今年这个“生意”大家抢着做](https://www.yicai.com/news/101895854.html)
> 概要: 原本更多还停留在想法和愿景阶段的事，今年逐渐变成了落地的现实，成为了今年多家展商竞相展示的实力。
### [酷冷至尊 Synk X 沉浸式体感椅上架：可将声音转化为振动，7999 元](https://www.ithome.com/0/730/094.htm)
> 概要: IT之家11 月 4 日消息，酷冷至尊去年 10 月份发布了 Synk X 沉浸式体感椅，可将声音转化为振动。现在，这款产品已经上架天猫，标价 7999 元。IT之家整理酷冷至尊 Synk X 沉浸式......
### [极度未知 Vision S 幻景电脑摄像头开售：支持 4K 拍摄，1499 元](https://www.ithome.com/0/730/095.htm)
> 概要: 感谢IT之家网友很宅很怕生的线索投递！IT之家11 月 4 日消息，极度未知 Vision S 幻景电脑摄像头开售，售价 1499 元。Vision S 采用索尼 800 万像素传感器，支持 4K@3......
### [城事 | 最高资助1亿元！今天，徐州这场峰会向全球人才发出真诚邀请](https://finance.sina.com.cn/wm/2023-11-04/doc-imztnmri0036004.shtml)
> 概要: 11月4日，2023淮海人才峰会召开，11位两院院士，60余所高校院所领导、学院及相关部门负责人，200余名海内外高层次人才代表、青年大学生代表...
### [盛时欢聚日丨如约而至，欢聚每一日](http://www.investorscn.com/2023/11/04/111575/)
> 概要: 对于盛时集团而言,每一位盛时会员都是盛时来之不易的朋友。盛时能够占据国内接近20%的市场份额,不仅是盛时这么多年以来的沉淀和创新的结果,也离不开盛时每一位会员的支持的信任。盛时一直在拉近与品牌、会员的......
### [广东恩平发生4.3级地震 尚无人员伤亡、房屋倒塌报告](https://finance.sina.com.cn/jjxw/2023-11-04/doc-imztnfih5674841.shtml)
> 概要: 转自：​央视新闻客户端11月04日19时24分，广东省江门市恩平市（北纬22.08度，东经112.25度）发生4.3级地震，震源深度8千米。记者从恩平市政府了解到，截至4日20时许...
### [丁俊晖奥沙利文明日开杆，度小满助力斯诺克国际锦标赛上演巅峰时刻](http://www.investorscn.com/2023/11/04/111576/)
> 概要: 2023世界斯诺克国际锦标赛将于11月5日在天津市人民体育馆正式开赛。作为世界最高级别的斯诺克赛事之一，斯诺克国际锦标赛汇聚了72名世界顶尖球员参与，将奉献一场场精彩的斯诺克盛宴，带给天津球迷一次近距......
### [香港举办嘉年华活动吸引全球人才](https://finance.sina.com.cn/jjxw/2023-11-04/doc-imztnmre5546194.shtml)
> 概要: 转自：新华网 新华社香港11月4日电（记者梁文佳）“创新香港-国际人才嘉年华2023”（秋季）活动4日在香港亚洲国际博览馆开幕。此次活动将持续两日，集招聘、论坛为一体...
### [海南省启动2023届高校毕业生就业质量调查，涉及这些内容→](https://finance.sina.com.cn/jjxw/2023-11-04/doc-imztnmrh2322524.shtml)
> 概要: 海南日报记者易宗平 海南省近段时间已启动2023届高校毕业生就业质量调查工作，就业部门希望毕业生群体予以关注和支持。这是11月3日...
### [《符文工厂 5》Steam 国区价格永降：282 → 218 元](https://www.ithome.com/0/730/096.htm)
> 概要: IT之家11 月 4 日消息，《符文工厂 5》Steam 国区价格永降，标准版从 282 元降至 218 元，豪华版从 338 元降至 278 元。IT之家注意到，《符文工厂 5》于 7 月 14 日......
### [当你“双十一”准备买买买，这家企业在上链接！](https://finance.sina.com.cn/jjxw/2023-11-04/doc-imztnmrk5359676.shtml)
> 概要: 转自：上观新闻一年一度的“双十一”电商大促到来，在位于嘉定工业区汇源路55号的中广国际广告创意产业园内，入驻电商企业正铆足干劲，全力以赴迎接挑战。
### [流言板JR综评9.5分对面居然敢四放洛？MISSING获对阵KT比赛MVP](https://bbs.hupu.com/622838138.html)
> 概要: 虎扑11月04日讯 S13全球总决赛八强赛KT以3比1击败KT。JR用“对面居然敢四放洛？”来点评MISSING本场比赛的表现。最终JR给到MISSING综合9.5的评分。MISSING获对阵KT系列
### [流言板JR锐评力挽狂澜摧毁KR电竞，对阵KT比赛Ruler得分9.1](https://bbs.hupu.com/622838165.html)
> 概要: 虎扑11月04日讯 S13全球总决赛八强赛KT以3比1击败KT。JR用“力挽狂澜摧毁KR电竞”来点评Ruler本场比赛的表现。最终JR给到Ruler综合9.1的评分。点击链接给本场选手、教练评分以下是
### [流言板JR锐评盲僧玩得像我黑铁兄弟，对阵KT比赛Kanavi得分7.5](https://bbs.hupu.com/622838201.html)
> 概要: 虎扑11月04日讯 S13全球总决赛八强赛KT以3比1击败KT。JR用“盲僧玩得像我黑铁兄弟！”来点评Kanavi本场比赛的表现。最终JR给到Kanavi综合7.5的评分。点击链接给本场选手、教练评分
### [流言板潘帕：哈兰德让我想起范巴斯滕，但前者是射手而后者更全能](https://bbs.hupu.com/622838207.html)
> 概要: 虎扑11月04日讯 此前，在2023年的《法国足球》杂志评选中，曼城前锋哈兰德荣获了盖德-穆勒奖（最佳前锋），并在金球奖评选中名列第二。而《队报》也采访了多位传奇前锋，他们也对哈兰德进行了评价。其中，
### [韩网翻译Ruler：很难预测四强对手，他们都很出色，不想与之交手](https://bbs.hupu.com/622838214.html)
> 概要: 虎扑11月4日讯 2023全球总决赛四分之一决赛第三场JDG以3比1战胜KT晋级四强，赛后Ruler选手接受PCS流赛后采访，原视频翻译如下：（未经允许禁止转载，搬运截图）Q：恭喜你们获得比赛的胜利，
# 小说
### [权清天下](http://book.zongheng.com/book/1154047.html)
> 作者：一言难尽说不得

> 标签：历史军事

> 简介：无系统、无背景、无武功的三无穿越者朱申远穿越到了一个刚中举的举人身上，还没等他高兴，在他中举的当晚有人造反。好巧不巧的他和反贼中的一个人相像，所以由于长相原因，他做了反贼替身。更惊喜的这是康熙年间，为了活下去他不得不当了天下最大的反贼，命运安排的游戏开始了，哪知道他玩得了最后一关。凡日月所照皆为朕之江山，这是他穿越后得的一种病，不过他却不打算治好。彼得西伯利亚朕收藏了？你赞成还是反对？我……赞成，你把家伙拿开。印地安，不，这是殷地安，你看我们皮肤是一个色的。肤色黑的叔叔道，大王，俺们是亲戚啊，我是有证据的，郑和到过这的。快小伙们上窝窝头。三哥笑说，大王其实我们也是亲戚，唐僧来过了，这恒河水其实是女儿河。去！你大爷的，我不可能有你这种亲戚。

> 章节末：第八十八章    大结局

> 状态：完本
# 论文
### [Brain-inspired neural circuit evolution for spiking neural networks](https://pattern.swarma.org/paper?id=99c4719a-5811-11ee-813d-0242ac17000d)
> 概要: In biological neural systems, different neurons are capable of self-organizing to form different neural circuits for achieving a variety of cognitive functions. However, the current design paradigm of spiking neural networks is based on structures derived from deep learning. Such structures are
### [GLFF: Global and Local Feature Fusion for Face Forgery Detection | Papers With Code](https://paperswithcode.com/paper/glff-global-and-local-feature-fusion-for-face)
> 日期：16 Nov 2022

> 标签：None

> 代码：https://github.com/littlejuyan/GLFF

> 描述：With the rapid development of deep generative models (such as Generative Adversarial Networks and Auto-encoders), AI-synthesized images of the human face are now of such high quality that humans can hardly distinguish them from pristine ones. Although existing detection methods have shown high performance in specific evaluation settings, e.g., on images from seen models or on images without real-world post-processings, they tend to suffer serious performance degradation in real-world scenarios where testing images can be generated by more powerful generation models or combined with various post-processing operations. To address this issue, we propose a Global and Local Feature Fusion (GLFF) to learn rich and discriminative representations by combining multi-scale global features from the whole image with refined local features from informative patches for face forgery detection. GLFF fuses information from two branches: the global branch to extract multi-scale semantic features and the local branch to select informative patches for detailed local artifacts extraction. Due to the lack of a face forgery dataset simulating real-world applications for evaluation, we further create a challenging face forgery dataset, named DeepFakeFaceForensics (DF^3), which contains 6 state-of-the-art generation models and a variety of post-processing techniques to approach the real-world scenarios. Experimental results demonstrate the superiority of our method to the state-of-the-art methods on the proposed DF^3 dataset and three other open-source datasets.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
