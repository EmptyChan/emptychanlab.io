---
title: 2020-10-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RestormelCastle_EN-CN5344913777_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-10-07 20:10:13
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RestormelCastle_EN-CN5344913777_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：小花陪妈妈伏案练习毛笔字 孙俪直呼和女儿犹如好闺蜜](http://slide.ent.sina.com.cn/star/w/slide_4_86512_346296.html)
> 概要: 组图：小花陪妈妈伏案练习毛笔字 孙俪直呼和女儿犹如好闺蜜
### [王一博摔车后对手车队疑欢呼庆祝 尹正发文抱不平](https://ent.sina.com.cn/y/yneidi/2020-10-07/doc-iivhvpwz0778028.shtml)
> 概要: 新浪娱乐讯 10月7日，王一博参加摩托车赛在转弯时，因对手胡通明急于内线超车，距离太近被带倒，错失了第二。有网友爆料称，在胡通明因自己失误将王一博带倒摔车后，疑似胡通明车队成员在赛场后台鼓掌欢呼，引发......
### [组图：浪姐团综路透七人同框无互动 黄龄专注玩手机徐峥成亮点](http://slide.ent.sina.com.cn/z/v/slide_4_704_346305.html)
> 概要: 组图：浪姐团综路透七人同框无互动 黄龄专注玩手机徐峥成亮点
### [组图：周冬雨青涩旧照曝光 长发飘逸笑容甜美动人](http://slide.ent.sina.com.cn/star/slide_4_704_346314.html)
> 概要: 组图：周冬雨青涩旧照曝光 长发飘逸笑容甜美动人
### [组图：赵薇21岁继子恋情甜蜜 女友身材性感模样不输明星](http://slide.ent.sina.com.cn/star/slide_4_704_346316.html)
> 概要: 组图：赵薇21岁继子恋情甜蜜 女友身材性感模样不输明星
### [孙俪假日与6岁女儿一起写字讨论，小花乖巧懂事，被妈妈视为好闺蜜](https://new.qq.com/omn/20201007/20201007A069FF00.html)
> 概要: 由于国庆期间邓超和孙俪都有工作在身，所以这个假期夫妻俩并未晒出带着一双儿女外出游玩的画面。10月7日，难得休整在家的孙俪久违晒出母女日常，晒图并用文字记录了她与女儿小花妹妹一起练字的情景。      ......
### [张翰走神下意识看杨超越，被海涛戳穿后害羞到喷水，女方都不好意思了](https://new.qq.com/omn/20201006/20201006V0E5ZD00.html)
> 概要: 张翰走神下意识看杨超越，被海涛戳穿后害羞到喷水，女方都不好意思了
### [赵丽颖晒照为冯绍峰庆生，甜蜜称呼老公为二叔，剧里爱称沿用至今](https://new.qq.com/omn/20201007/20201007A063B700.html)
> 概要: 10月7日，知名女星赵丽颖晒出了剧照为老公冯绍峰庆生，并配文道：二叔，生日快乐。在庆生的同时，赵丽颖还替冯绍峰宣传了新剧。            冯绍峰、赵丽颖夫一直对于他们的家庭和婚姻生活保持着低调......
### [赵本山前妻女儿罕见露面，打扮朴素不像富家女，与丈夫带孩子出门](https://new.qq.com/omn/20201007/20201007A07K3J00.html)
> 概要: 近日，一名长期拍摄“刘老根大舞台”的网友，发布了赵本山与前妻所生女儿赵玉芳的近照。            赵玉芳的身材略微发福，留着长发，随意扎成了马尾，刘海非常长，垂下来遮挡脸部。         ......
### [何炅私密行程曝光，揭露了一个残酷真相！网友：活该他红25年](https://new.qq.com/omn/20201007/20201007A0APDN00.html)
> 概要: 10月6日，有网友在网上曝光了何炅十月份的行程表。            从表中我们可以看到，从月初到月底，何炅几乎没有个人休息时间——值得一提的是，这份时间表并不是全面的，何老师有部分行程还未补充上......
# 动漫
### [《哆啦A梦 伴我同行2》11月20日上映 正式预告视频公开](https://news.dmzj.com/article/68775.html)
> 概要: 3DCG动画电影《哆啦A梦 伴我同行2》的新上映日决定为11月20日了。同时，正式的海报视觉图、预告片以及主题歌情报都公开了。
### [新作TV动画《哥斯拉S.P＜奇点＞》2021年4月开始播出](https://news.dmzj.com/article/68774.html)
> 概要: 《哥斯拉》系列的完全新作TV动画决定制作了，将以《哥斯拉S.P＜奇点＞》为题，于2021年4月开始在TOKYO MX等频道播出。
### [剧场版《FGO神圣圆桌领域卡美洛·前篇》正式预告解禁](https://news.dmzj.com/article/68776.html)
> 概要: 《剧场版Fate/Grand Order -神圣圆桌领域卡美洛-前篇Wandering；Agateram》的正式预告影像解禁了。
### [秦时明月系列：各路神兵利器盘点，鲨齿乃剑中克星，越王八剑表现出色](https://new.qq.com/omn/20201005/20201005A00IHN00.html)
> 概要: 城市很大，生活很小，爱上阅读，才能看到不一样的精彩世界。国漫扛把子《秦时明月》系列和姐妹篇《天行九歌》由玄机科技制作，剧中出现了许多高手，而这些高手大多都拥有神兵利器，下面就来盘点一下剧中出现并有过……
### [短篇恐怖漫画：很灵验的网红姻缘树！](https://new.qq.com/omn/20201007/20201007A0522J00.html)
> 概要: 作者：酋长来源：微博漫画
### [一拳超人：囚禁龙卷团伙揭晓，闪光又被暗杀组织盯上](https://new.qq.com/omn/20201007/20201007A0630J00.html)
> 概要: 在重制版曝光了爆破形象后，一拳超人原作版也迎来了信息量爆炸的更新，在one更新的原作版第130话中，多个反派组织和英雄故事支线开启，以至于读者看得眼花缭乱，不知道故事接下来将如何发展，那么接下来小鳗……
### [《姜子牙》引争议：国漫崛起不能只靠收割情怀](https://new.qq.com/omn/20201007/20201007A098C600.html)
> 概要: ——本文系红网第六届全国大学生“评论之星”选拔赛参赛作品截至10月3日0时，《姜子牙》票房破7亿，刷新中国电影市场动画电影首周票房纪录。观众对电影评价褒贬不一，不少观众认为《姜子牙》是国漫的又一力作……
### [万事屋＆真选组集结，剧场版《银魂 THE FANAL》最后配音结束！](https://new.qq.com/omn/20201007/20201007A05Z9B00.html)
> 概要: 日本剧场版动画《银魂 THE FANAL》将于 2021年1月8日上映。2020年10月初，制作组完成了声优的后期配音录制，并于10月6日公布了万事屋声优的寄语，和一些宣传图。本文对坂田银时的声优—……
### [数码宝贝第二季人气极低的原因，除了主角性格，丑应该也是一个重要原因吧！](https://new.qq.com/omn/20201002/20201002A05TCW00.html)
> 概要: 数码宝贝系列一直是90后网友们心目中的美好回忆。众观数码宝贝第一部到第五部，基本上每部都拥有相当高的人气支撑，唯独数码宝贝第二部比其他四部人气略差。关于第二部人气为什么这么差有很多原因，但是其实和合……
### [《哆啦A梦：伴我同行2》正式PV 菅田将晖演唱主题歌](https://acg.gamersky.com/news/202010/1326901.shtml)
> 概要: 电影《哆啦A梦：伴我同行2》，在今天（10月7日）公开了本作的正式预告，《哆啦A梦：伴我同行2》的主题歌曲也公开了，是由菅田将晖演唱的《虹》。
### [哥斯拉动画《哥斯拉:奇点》确定制作 2021年4月开播](https://acg.gamersky.com/news/202010/1326895.shtml)
> 概要: 《哥斯拉》完全新作TV动画《哥斯拉：奇点》确定制作，预计将于2021年4月在TOKYO MX上开播，网飞将会先行播出。官方还公开了角色图和哥斯拉的概念图。
### [《FGO》神圣圆桌领域剧场版新PV 全员登场令人期待](https://acg.gamersky.com/news/202010/1326888.shtml)
> 概要: 剧场版动画《Fate/Grand Order：神圣圆桌领域卡美洛》前篇，在今日（10月7日）公开了本作的正式预告。本次的预告中公开了全新的画面。
# 财经
### [互联网公司的两万人陷阱](https://finance.sina.com.cn/china/2020-10-07/doc-iivhuipp8388371.shtml)
> 概要: 原标题：互联网公司的两万人陷阱 来源：乱翻书 互联网公司员工到两万人后，常见动作是CEO写信发文，整肃管理团队，在公司搞整风运动。
### [武汉市委书记王忠林诚邀高校毕业生留汉：这里是创新创业理想之地](https://finance.sina.com.cn/china/2020-10-07/doc-iivhuipp8383606.shtml)
> 概要: 原标题：王忠林诚邀高校毕业生留汉：这里是校城融合的丰厚沃土、创新创业的理想之地 来源：长江日报 10月7日，后疫情时代武汉理工大学高质量发展战略合作暨合并组建20周年...
### [两年6倍的医疗龙头被盯上 科技龙头透露产品价格全线上涨](https://finance.sina.com.cn/china/2020-10-07/doc-iivhvpwz0776774.shtml)
> 概要: 原标题：两年6倍的医疗龙头被盯上，科技龙头透露产品价格全线上涨！节后机构潜在目标曝光（名单） 9月份机构调研个股数量有490多只...
### [甘薇晒子女自我激励别怕困难 疑默认自己是受害者](https://finance.sina.com.cn/china/2020-10-07/doc-iivhuipp8385624.shtml)
> 概要: 10月7日甘薇晒出女儿的古装扮相，同时鼓励自己“有你们在我身边，任何困难也不怕”。 甘薇在和网友互动时，有网友评论中写下，“作为女人难道她不是受害者吗？
### [冯仑：写给女儿的9个建议](https://finance.sina.com.cn/china/2020-10-07/doc-iivhvpwz0766383.shtml)
> 概要: 原标题：冯仑：写给女儿的9个建议 口述：冯仑 御风集团董事长 万通集团创始人 注：本文节选自冯仑在蜻蜓FM开设的音频专栏《不确定时代的生存法则》...
### [2020年诺贝尔化学奖揭晓！2位女性分享奖项](https://finance.sina.com.cn/china/2020-10-07/doc-iivhvpwz0774276.shtml)
> 概要: 原标题：2020年诺贝尔化学奖揭晓！2位女性分享奖项 中新网10月7日电 据诺贝尔奖官网消息，当地时间7日中午，瑞典皇家科学院将2020年诺贝尔化学奖授予Emmanuelle...
# 科技
### [2020.8前端找工作记录](https://segmentfault.com/a/1190000027083997)
> 概要: 记录一下自8月中旬离职之后准备以及找工作的经历;会提到个人感受到的招聘情况和一些前端面试题.会按照以下几个点来展开.背景信息(个人相关)整体找工作的感受整体节奏(时间安排)面试题整理(含算法题)感悟和......
### [算法工程狮二、数学基础 线性代数](https://segmentfault.com/a/1190000027082942)
> 概要: 线性代数内容都很连贯，整体就是  行列式-->矩阵-->n维向量-->线性方程组-->相似对角型-->二次型 。行列式就是一个值，行列式为0则对应线性方程组有多解，且对应矩阵不可逆，若为0则解唯一。n......
### [AOC 推出 15.6 英寸便携式触摸屏显示器 16T2：USB-C 连接，可当作移动电源](https://www.ithome.com/0/512/451.htm)
> 概要: IT之家10月7日消息  冠捷（AOC）今日发布了一款 15.6 英寸使用 USB-C 连接的便携式触摸屏显示器 16T2，支持通过 USB-C 接口或 micro HDMI 接口连接。▲ 图源 AO......
### [Chrome 86 稳定版发布：引入大量与密码相关的移动版安全改进](https://www.ithome.com/0/512/445.htm)
> 概要: IT之家10月7日消息  谷歌今天向稳定频道推送了 Chrome 86 版本更新，新版本包括大量安卓和 iOS 平台的安全增强功能，并为 PC 引入了文件系统和 WebCodecs API 等大量开发......
### [印度公路远远超越中国，真的假的？](https://www.huxiu.com/article/386028.html)
> 概要: 来源｜地球知识局（ID：diqiuzhishiju）作者｜乃一姆头图｜Malcolm P Chapman / Shutterstock近几年来，中国的基建发展举世瞩目，高速公路网总长度已经跃居世界第一......
### [被游戏塑造的一代：沉沦手游的孩子长大了会怎样？](https://www.huxiu.com/article/386013.html)
> 概要: 题图来自视觉中国，本文来自微信公众号：中国新闻周刊（ID：chinanewsweekly），本刊记者：李明子，发于2020.9.28总第966期《中国新闻周刊》韩薇是一个有俩娃的80后妈妈，大儿子今年......
### [从抽象类开始，详解责任链模式](https://www.tuicool.com/articles/ai2mErb)
> 概要: 大家好，欢迎大家阅读设计模式专题。今天我们继续介绍新的设计模式，和上次的链式模式不同，这一次要介绍的责任链模式不仅仅在Python当中有，在很多其他的语言当中同样支持，比如Java。Python和Ja......
### [产品经理必须了解的App三大技术框架](https://www.tuicool.com/articles/N3EvArr)
> 概要: 在项目中，设计师往往需要权衡商业目标、用户体验和技术实现三者之间的关系来做设计方案。“世上没有完美的设计，因为你最终能做的就是在各种关系之间取得平衡”——Paul Rand（美国著名设计师）文章分为五......
# 小说
### [爱丽丝漫游仙境](http://book.zongheng.com/book/681811.html)
> 作者：英刘易斯·卡罗尔

> 标签：评论文集

> 简介：《爱丽丝镜中奇遇》说的是小姑娘爱丽丝下完象棋，对镜子里的东西非常好奇，以至穿镜而入，进入了奇妙的镜中世界。在这里，整个世界就是一个大棋盘，小姑娘爱丽丝不过是这个棋盘中的一个小兵。在这个世界里，她从自己所处的棋格开始，一步步前行，每走一步都会有奇妙的遭遇。等到她终于走到第八格，当了王后之后，又遇到了意想不到的事情……

> 章节末：原版插图欣赏

> 状态：完本
# 游戏
### [《监狱建筑师》12日之前免费游玩 更享二折优惠](https://www.3dmgame.com/news/202010/3799060.html)
> 概要: 理论上，《监狱建筑师》是2012年发售的作品，不过它在Steam抢先体验的alpha阶段停留了几年。今天是这款游戏作为正式版本上市的五周年纪念，为了庆祝，开发商决定让这款游戏在接下来的五天时间里可以免......
### [任天堂明晨树屋直播 带来《塞尔达无双》细节内容](https://www.3dmgame.com/news/202010/3799041.html)
> 概要: 来自任天堂官方发布的最新消息，他们将于北京时间10月8日凌晨1点举办“任天堂树屋直播”活动，在本次直播中将带来《皮克敏3：豪华版》的深度研讨和《塞尔达无双：灾厄启示录》的游戏演示细节。关于《皮克敏3：......
### [《魔兽世界》暗影国度生存指南 部分物品将绝版](https://ol.3dmgame.com/news/202010/28719.html)
> 概要: 《魔兽世界》暗影国度生存指南 部分物品将绝版
### [《通勤地铁战》正式版发售 打造繁荣地铁网络](https://www.3dmgame.com/news/202010/3799056.html)
> 概要: 《通勤地铁战》是SquarePlay Games推出的地铁管理模拟游戏新作。在经历了15个月的Steam平台抢先体验之后，这款游戏终于在今天迎来了1.0正式版本。在基本模式上，《通勤地铁战》遵循了经典......
### [伦敦也能信仰之跃？《看门狗：军团》季票角色公开](https://www.3dmgame.com/news/202010/3799050.html)
> 概要: 《看门狗：军团》将在10月29日正式发布，不过在10月6日，育碧让我们了解到了一些未来的计划，还有一些《看门狗：军团》季票的情报。《看门狗：军团》新预告：上个月我们了解到，《看门狗》初代主角艾登·皮尔......
# 论文
### [Do Adversarially Robust ImageNet Models Transfer Better?](https://paperswithcode.com/paper/do-adversarially-robust-imagenet-models)
> 日期：16 Jul 2020

> 标签：TRANSFER LEARNING

> 代码：https://github.com/MadryLab/robustness

> 描述：Transfer learning is a widely-used paradigm in deep learning, where models pre-trained on standard datasets can be efficiently adapted to downstream tasks. Typically, better pre-trained models yield better transfer results, suggesting that initial accuracy is a key aspect of transfer learning performance.
### [Interactive Text Graph Mining with a Prolog-based Dialog Engine](https://paperswithcode.com/paper/interactive-text-graph-mining-with-a-prolog)
> 日期：31 Jul 2020

> 标签：None

> 代码：https://github.com/yuce/pyswip

> 描述：On top of a neural network-based dependency parser and a graph-based natural language processing module we design a Prolog-based dialog engine that explores interactively a ranked fact database extracted from a text document. We reorganize dependency graphs to focus on the most relevant content elements of a sentence and integrate sentence identifiers as graph nodes.
