---
title: 2022-07-20-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MoonPhases_ZH-CN3779272016_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-20 21:15:47
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MoonPhases_ZH-CN3779272016_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [看本质，产品经理认知升级的四大公式](https://www.woshipm.com/pmd/5532431.html)
> 概要: 编辑导语：当你在产品经理岗位上呆了1年、2年甚至更长时间之后，你会从这个岗位及工作内容中收获什么？如果你仍想继续在产品经理岗位上继续修行，你又可以秉承着什么样的认知及思维方式，实现成长进阶？本篇文章里......
### [视频号商业化提速：推出视频号小店，上线原生feeds广告，100万起投](https://www.woshipm.com/operate/5532046.html)
> 概要: 编辑导语：现如今，视频号发展得如火如荼，开始向多方面开拓。这篇文章作者详细分析了近日视频号更新了哪些创新的功能，感兴趣的小伙伴一起来看看吧~视频号商业化提速中。7月13日，视频号发布公告称将推出“视频......
### [失去的“雪糕自由”，成了压倒骆驼的最后一根稻草](https://www.woshipm.com/it/5531388.html)
> 概要: 编辑导语：如今，“雪糕自由”难以实现，互联网新贵雪糕第五年，人们开始趋于冷静，不再茫从，跟风去购买。贵价雪糕成为人们眼中的刺客，内卷的雪糕江湖，是时候停下来了吗？品牌的终极之战在哪里？如果问现在的年轻......
### [马斯克先失一局：推特收购案审判定于10月进行](https://finance.sina.com.cn/tech/internet/2022-07-20/doc-imizmscv2627134.shtml)
> 概要: 北京时间7月20日凌晨消息，据报道，关于特斯拉CEO埃隆·马斯克（Elon Musk）收购推特案的审判定于10月进行......
### [#今晚打老虎#28天创意banner打卡#day11](https://www.zcool.com.cn/work/ZNjA5OTUyNjg=.html)
> 概要: 正在参与：「28天」创意Banner打卡挑战......
### [从原生 JavaScript 到 React](https://segmentfault.com/a/1190000042196411)
> 概要: 从头开始理解 React作者：Stéphane Bégaudeau于 2018 年 10 月 1 日React 是一个用于构建用户界面的 JavaScript 框架。它可用于通过动态操作页面内容来创建......
### [经济日报：有序推进公共领域车辆全面电动化试点](https://finance.sina.com.cn/tech/it/2022-07-20/doc-imizmscv2646514.shtml)
> 概要: 近日，工信部网站公布第十三届全国人大五次会议第6894号建议的答复。工信部表示，下一步，将进一步提升充换电基础设施保障能力，启动开展公共领域车辆全面电动化城市试点、新能源汽车下乡等活动，促进新能源汽车......
### [高管解读](https://finance.sina.com.cn/tech/internet/2022-07-20/doc-imizmscv2665459.shtml)
> 概要: 相关新闻：Netflix第二季度营收79.7亿美元：净利润同比增长6.5%......
### [盘点美国科技行业大裁员和冻结招聘潮：苹果、微软、谷歌...](https://finance.sina.com.cn/tech/internet/2022-07-20/doc-imizmscv2669577.shtml)
> 概要: 新浪科技讯 北京时间7月20日早间消息，据报道，目前的美国科技行业，面临了一系列不确定因素，比如通货膨胀、俄乌冲突，以及挥之不去的新冠疫情，许多科技公司已经重新考虑招聘计划，一些公司暂时冻结招聘，取消......
### [倒在酷暑中的外卖骑手，和失踪的高温补贴](https://www.huxiu.com/article/612867.html)
> 概要: 本文来自微信公众号：新周刊APP（ID：gh_e662088418b6），作者：戈多，原文标题：《中暑、缺水、事故，外卖骑手倒在高温下丨深度》，头图来自：视觉中国7月13日傍晚，一则关于“骑手小哥得热......
### [Amber Group守护数字领域信任基石，获国际知名保险机构承保](http://www.investorscn.com/2022/07/20/101929/)
> 概要: 据海外媒体报道,近年来,随着比特币、瑞波币、莱特币等为代表的数字资产价格大幅度波动变幻,加密货币逐渐进入公众的视野,而区块链作为前沿技术和机遇风口,使得数字资产越来越吸引人们的投资兴趣,从而受到传统投......
### [武动乾坤：林动所在天玄大陆出现过哪些强者，各自击杀了多少异魔](https://new.qq.com/omn/20220709/20220709A06YQQ00.html)
> 概要: 除去远古八主之外，远古时期还有哪些强者？远古时期，异魔皇带领异魔族大肆进攻天玄大陆，为了保卫自己的家园，天玄大陆的众多强者在符祖的带领下，最终成功击退异魔族，虽说在这场战斗之中，符祖和远古八主发挥着十......
### [杂志「月刊Comic Alive」2022年9月号封面公开](http://acg.178.com/202207/452284071585.html)
> 概要: 漫画杂志「月刊Comic Alive」公开了2022年9月号的封面图，绘制的是电视动画「欢迎来到实力至上主义的教室」第二季。电视动画「欢迎来到实力至上主义的教室」改编自衣笠彰梧著作、无限轨道插画的同名......
### [“No convincing evidence” that depression is caused by low serotonin levels](https://www.bmj.com/content/378/bmj.o1808)
> 概要: “No convincing evidence” that depression is caused by low serotonin levels
### [AV导演穿越到异世界！漫画《异世界AV》发售单行本](https://news.dmzj.com/article/74993.html)
> 概要: 漫画《异世界AV～魔王大人对H的视频很感兴趣～》发售了单行本第一卷。在本作中，讲述了AV导演古都京太郎在摄影时，被传送到了人类与魔族进行战争的异世界后的故事。
### [安奈儿股份回购和近亿元增资研发设计子公司，彰显发展信心](http://www.investorscn.com/2022/07/20/101935/)
> 概要: 7月19日晚间，安奈儿（002875.SZ）披露了回购股份和向全资子公司增资的公告。公司拟在未来一年内利用资金总额不低于4000万元、不超过8000万元自有资金，以不超过10元/股的价格回购公司股份，......
### [P站美图推荐——电车特辑（二）](https://news.dmzj.com/article/74995.html)
> 概要: 一个自带故事感的地点，四舍五入也是一期JK特辑呢！
### [金赛药业坚持创新驱动 关注儿童健康发展](http://www.investorscn.com/2022/07/20/101936/)
> 概要: 7月18日，“2022健康中国发展大会”在北京召开，本次大会以“共建共享 全民健康”为宗旨，采取线上线下相结合的方式举行。在下午举办的“健康中国·医者先行”平行论坛上，金赛药业总经理金磊就我国儿童健康......
### [实在太热了！谷歌和甲骨文英国服务器高温故障](https://www.3dmgame.com/news/202207/3847373.html)
> 概要: 昨天（7月19日），位于英国的谷歌和甲骨文 Oracle云服务器因高温天气导致的冷却故障而停机。英国近期经历了40摄氏度的创纪录高温，两家公司都将服务器停机意外归咎于气温。谷歌在其谷歌云状态页面上指出......
### [海贼王电影《FILM RED》插曲宣传片 8月6日上映](https://www.3dmgame.com/news/202207/3847376.html)
> 概要: 超人气漫画《海贼王》全新动画电影《海贼王 FILM RED》将于8月6日上映，7月20日今天官方公布了最新插曲《UTAKATA催眠曲》的宣传片，由Ado演唱，一起欣赏下。•《海贼王 FILM RED》......
### [深度剖析：如何做好详情页文字排版？](https://www.zcool.com.cn/article/ZMTQyODYyOA==.html)
> 概要: 深度剖析：如何做好详情页文字排版？
### [「联盟空军航空魔法音乐队 光辉魔女」杂志彩页公布](http://acg.178.com/202207/452289441167.html)
> 概要: 电视动画「联盟空军航空魔法音乐队 光辉魔女」公布了在杂志「Newtype」上的最新彩页，本作已于2022年7月3日开始播出。STAFF原作：島田フミカネ&Projekt World Witches导演......
### [「异世界药局」ED主题曲无字幕MV公开](http://acg.178.com/202207/452290188747.html)
> 概要: 由keepout负责插画，高山理图创作同名轻小说改编，diomedéa负责制作的电视动画「异世界药局」近期公开了ED主题曲「白雨」的无字幕动画MV，动画已于7月10日开始播出。「白雨」无字幕MV......
### [轻小说《英雄教室》2023年开播！Actas负责制作](https://news.dmzj.com/article/74997.html)
> 概要: 新木伸创作的轻小说《英雄教室》宣布了TV动画化决定的消息。本作预计将于2023年播出。
### [《海贼王》1054话情报：炎帝！局势大混乱](https://acg.gamersky.com/news/202207/1500995.shtml)
> 概要: 在休刊一段时间后，《海贼王》1054话情报终于公开了，本集中各方都有新消息传来，真是相当热闹。
### [《被解雇的暗黑士兵(30多岁)开始了慢生活的第二人生》动画化](https://news.dmzj.com/article/74998.html)
> 概要: 漫画《被解雇的暗黑士兵(30多岁)开始了慢生活的第二人生》宣布了动画化决定的消息。本作预计将于2023年1月开始播出。
### [TV动画「英雄教室」视觉图公开](http://acg.178.com/202207/452293798730.html)
> 概要: 改编自新木伸原作、森沢晴行插画同名小说的动画「英雄教室」发布了一张视觉图，本作预计将于2023年年内开播。STAFF原作：新木伸/イラスト：森沢晴行「英雄教室」（集英社ダッシュエックス文庫刊）导演：川......
### [Cooling related failure (in Google London DC)](https://status.cloud.google.com/incidents/XVq5om2XEDSqLtJZUvcH)
> 概要: Cooling related failure (in Google London DC)
### [“文化唐 星起航 ” 春城遇见大唐财富「717首届唐人节」](http://www.investorscn.com/2022/07/20/101939/)
> 概要: 2022年，是大唐财富“企业文化践行年”。7月17日，首届“唐人节”暨大唐财富企业文化节在春城昆明璀璨登场。企业文化节的设立创财富管理行业先河，是全体大唐人自己的节日......
### [小而美的魂系像素爽游《塔尼蚀：神之堕落》前瞻](https://www.3dmgame.com/news/202207/3847381.html)
> 概要: 由独立游戏厂商Actual Nerds开发，Mastiff和Neverland Entertainement发行的2D横版动作游戏《塔尼蚀：神之堕落》近日宣布游戏将于7月27日正式上线。富有创新的爽快......
### [没想清楚千万别创业](https://www.huxiu.com/article/613140.html)
> 概要: 本文来自微信公众号：刘润 （ID：runliu-pub），作者：观点：刘润，主笔：万青，题图来自：《中国合伙人》剧照前些日子，看到一个同学的问题：我太累了，在公司认真做事，不如会“表演”，会“PMP”......
### [网红斥530万美元购宝可梦卡 申请最贵吉尼斯纪录](https://www.3dmgame.com/news/202207/3847384.html)
> 概要: 美国顶级网红油管主播Logan Paul之前就曾经花费巨资购买到假冒宝可梦卡引发话题热议，近日再出惊人之举，斥530万美元巨资购得珍贵宝可梦卡，还表示将发行NFT，同时已经申请世界最贵交易的宝可梦卡吉......
### [Being on call sucks](https://bobbiechen.com/blog/2022/7/20/being-on-call-sucks)
> 概要: Being on call sucks
### [鲲域2021-2022年度showreel(硬盘被猫吞了,刚恢复数据)](https://www.zcool.com.cn/work/ZNjEwMDMwNjQ=.html)
> 概要: 鲲域2021-2022年度showreel(硬盘被猫吞了,刚恢复数据)
### [时尚Cosmo X刘雯](https://www.zcool.com.cn/work/ZNjEwMDM3MDA=.html)
> 概要: 摄影：许闯Trunk策划&统筹：阿三刘创意&视觉监制：滕雪菲形象：Lillian_v编辑：GoHyoJin撰文：木棉布布化妆：何磊MQstudio发型：刘雪孟制片&美术置景：不完美工作室🙋🙋🙋服装助理......
### [黑客组织“8220”将云僵尸网络发展到超过 30,000 台主机](https://www.tuicool.com/articles/vYJ3mmm)
> 概要: 黑客组织“8220”将云僵尸网络发展到超过 30,000 台主机
### [组图：张柏芝晒与大儿子看电影合照 两人亲密依偎笑言是二人世界](http://slide.ent.sina.com.cn/star/slide_4_86512_372861.html)
> 概要: 组图：张柏芝晒与大儿子看电影合照 两人亲密依偎笑言是二人世界
### [藤本树漫画《再见绘梨》确认引进 将推出中文实体书](https://acg.gamersky.com/news/202207/1501280.shtml)
> 概要: 中信出版集团旗下的墨狸工作室官方微博发布消息，得到了日本漫画家藤本树绘制的漫画《再见绘梨》的简体中文授权，即将推出《再见绘梨》简体中文实体书。
### [视频：林峯吴卓羲披荆斩棘路透曝光 二人久违同框状态佳](https://video.sina.com.cn/p/ent/2022-07-20/detail-imizirav4641066.d.html)
> 概要: 视频：林峯吴卓羲披荆斩棘路透曝光 二人久违同框状态佳
### [试驾前，别着急买理想L9](https://www.huxiu.com/article/608858.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 理想汽车坐拥203.4万微博粉丝的理想汽车CEO李想，比何小鹏（104.5万粉丝）和李斌（49.8万粉丝）有着更为广泛的社交媒体影响力。但稍......
### [离职就离职，有什么好晒的？](https://www.huxiu.com/article/612749.html)
> 概要: 本文来自微信公众号：锐见Neweekly （ID：app-neweekly），作者：咩霸，头图来自：《平凡的荣耀》‍谁能想到，“晒离职”也能成为一股热潮呢？如今，在小红书上搜“离职”，马上就能跳出10......
### [王一博救不了SKG](https://www.tuicool.com/articles/vu2YjeY)
> 概要: 王一博救不了SKG
### [为凸显美少女主角故意丑化配角？新动画人设引争议](https://acg.gamersky.com/news/202207/1501319.shtml)
> 概要: 动画《Lycoris Recoil》的人设在受访时透露，制作组要求他不要把两位女主角以外的人物设计得太美。这种以长相美丑区别角色重要性的做法瞬间掀起了网友的议论。
### [11980 元，帝瓦雷推出 Phantom II 雾水蓝珍藏版蓝牙小音箱](https://www.ithome.com/0/630/607.htm)
> 概要: IT之家7 月 20 日消息，帝瓦雷 Phantom II 雾水蓝珍藏版蓝牙小音箱现已上架，售价 11980 元。IT之家了解到，这款蓝牙音响由帝瓦雷与著名的意大利制作人 LuigiTozzi 合作打......
### [铁铬液流电池有望成为储能界新宠，振华股份优势凸显](http://www.investorscn.com/2022/07/20/101947/)
> 概要: 近期，储能板块再度成为资本市场的焦点，自4月27日以来该板块累计反弹幅度达到60%以上。其中，作为储能界的新宠，液流电池近期尤为受到市场关注，包括全钒液流电池、铁铬液流电池等等，攀钢钒钛作为钒液流电池......
### [华为拟发行 40 亿元中期票据，年内融资规模已达去年 2 倍](https://www.ithome.com/0/630/611.htm)
> 概要: IT之家7 月 20 日消息，昨日，华为在银行间市场披露 2022 年第四期中期票据申购说明，说明显示本期债务融资工具期限 3 年，发行规模为 40 亿元，无基本承销额。申购期为 7 月 20 日 9......
### [中三省半年报：生产总值超6万亿，居民收入增速跑赢GDP](https://www.yicai.com/news/101480401.html)
> 概要: 随着惠企纾困稳增长系列政策效果逐步显现和中三省疫情得到有效控制，中三省经济触底反弹、边际改善，呈平稳回升态势。
### [AMD 晒锐龙 7000 处理器 / AM5 平台高清照](https://www.ithome.com/0/630/613.htm)
> 概要: 感谢IT之家网友爱干啥干啥的线索投递！IT之家7 月 20 日消息，今天，AMD 在其推特平台账号上晒出了锐龙 7000 处理器和 AM5 平台的照片。AMD 在推文中强调了 AM5 平台的几个特性：......
### [Show HN: I built an app that helps JavaScript developers optimize their websites](https://buhalbu.com)
> 概要: Show HN: I built an app that helps JavaScript developers optimize their websites
### [组图：INTO1成员尹浩宇现身机场 穿白T搭牛仔裤清爽阳光](http://slide.ent.sina.com.cn/star/slide_4_86512_372867.html)
> 概要: 组图：INTO1成员尹浩宇现身机场 穿白T搭牛仔裤清爽阳光
### [本周Fami通游戏评分出炉《时空勇士 》33分](https://www.3dmgame.com/news/202207/3847411.html)
> 概要: 本周Fami通杂志游戏评分出炉（2022年7月20日），本次共测评了三款游戏，分别是《时空勇士》、《红魔城传说绯色的交响曲》和《苍蓝雷霆钢伏特3》。《时空勇士》 8/9/8/8 合计33分点评现在玩也......
### [华硕推出新款无畏 Pro 14 笔记本：i5 + RTX 3050，6399 元](https://www.ithome.com/0/630/626.htm)
> 概要: 感谢IT之家网友会飞的小笼包的线索投递！IT之家7 月 20 日消息，华硕现已推出新款无畏 Pro 14 系列笔记本，搭载 12 代酷睿标压处理器和 RTX 3050 显卡，首发售价如下：i5-125......
### [两年从0到2亿！辣卤食品品牌王小卤的小红书快速起量投放玩法！](https://www.tuicool.com/articles/y6JzYb2)
> 概要: 两年从0到2亿！辣卤食品品牌王小卤的小红书快速起量投放玩法！
### [7月20日这些公告有看头](https://www.yicai.com/news/101480498.html)
> 概要: 7月20日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [「灰度报告」2022年的熊市何时结束？](https://www.tuicool.com/articles/nIjyA3V)
> 概要: 「灰度报告」2022年的熊市何时结束？
### [疫苗是否应打第四针？上海卫健委主任这样回应](https://finance.sina.com.cn/jjxw/2022-07-20/doc-imizmscv2796381.shtml)
> 概要: 根据7月20日国家卫健委数据，7月19日，新增本土阳性感染者数量达到935人，单日新增逼近1000例。面对国内新冠疫情的反弹，接种疫苗仍然是最有效的预防手段。
### [央行：6月债券市场共发行各类债券65996.8亿元](https://finance.sina.com.cn/china/gncj/2022-07-20/doc-imizirav4674203.shtml)
> 概要: 央视网消息：央行网站7月20日公布2022年6月份金融市场运行情况，具体内容如下。 一、债券市场发行情况 6月份，债券市场共发行各类债券65996.8亿元。
### [指数三连阳收复3300点 能否继续反弹？](https://www.yicai.com/news/101480519.html)
> 概要: 指数三连阳收复3300点 能否继续反弹？
### [上半年重要农产品供应充足 农民增收形势向好](https://finance.sina.com.cn/jjxw/2022-07-20/doc-imizmscv2797987.shtml)
> 概要: 新华社北京7月20日电 题：上半年重要农产品供应充足 农民增收形势向好 于文静、胡璐、欧立坤 大暑近、农事忙。大暑节气即将到来，各地正有序推进夏种夏管...
### [侠客岛：退休也逃不掉！判错案就得终身追责](https://finance.sina.com.cn/wm/2022-07-20/doc-imizirav4679112.shtml)
> 概要: 【岛叔说】退休也逃不掉！判错案就得终身追责 侠客岛公众号 新闻发布会现场（图源：最高检微博） 最高人民检察院近日通报，2021年...
### [文旅部：做好疫情防控“层层加码”整治工作 做好发生疫情地区游客安置疏散工作](https://finance.sina.com.cn/china/gncj/2022-07-20/doc-imizmscv2802674.shtml)
> 概要: 全国文化和旅游行业安全生产暨疫情防控工作电视电话会议召开。 7月20日，文化和旅游部召开全国文化和旅游行业安全生产暨疫情防控工作电视电话会议...
### [国企改革三年行动，职业经理人建设有这些成效](https://www.yicai.com/news/101480678.html)
> 概要: 截至2021年底，已开展经理层成员任期制和契约化管理的子企业占比分别达到97.3%和94.7%。
### [连平：下半年房地产市场将迎来小阳春](https://www.yicai.com/news/101480662.html)
> 概要: 在现有的政策框架下，此轮房地产市场回暖力度比较有限。
### [某站动画结束后，为何弹幕会刷“盖亚”？是乱玩梗吗？](https://new.qq.com/omn/20220720/20220720A0AZX900.html)
> 概要: 小伙伴们看新番的时候，总会发现每一集结束时，弹幕都会刷“盖亚”二字。不懂梗的人可能不知道“盖亚”是什么意思，也不知道弹幕为何会刷它，甚至会觉得有人在恶意玩梗刷屏，下面来详细分析下，如下：       ......
### [复盘国足0-3韩国：比分或许是最体面的数据，射门数1-23！](https://bbs.hupu.com/54827208.html)
> 概要: 或许朱辰杰的乌龙球给予球队太大的打击，11人退防的战术最终毁于自己的失误，让人一声叹息。面对着没有旅欧球员，甚至部分国内联赛名将都没有到场的韩国队，国足选拔队不仅仅输了一个0-3，更是射门数1-23处
### [流言板保罗-本托：我们本可以进更多球，球员今天状态不是很好](https://bbs.hupu.com/54827231.html)
> 概要: 虎扑07月20日讯 东亚杯第一轮，韩国3-0中国，赛后韩国队主教练保罗-本托接受采访。他表示：“本场取胜对我们来说是非常重要的，我认为我们配得上这场胜利。我赛前预料到了对手会集中注意力在防守上，我们这
### [流言板布伦森：在很长一段时间里，我以为我永远不会离开达拉斯](https://bbs.hupu.com/54827260.html)
> 概要: 虎扑07月20日讯 尼克斯球员杰伦-布伦森在今日接受了媒体采访。谈到离开达拉斯，布伦森说：“在很长一段时间里，我以为我永远不会离开达拉斯，我认为达拉斯是我整个职业生涯的家，这是一个很棒的地方，是一个我
### [流言板北控球员李玮颢晒比赛照：继续努力，继续学习](https://bbs.hupu.com/54827318.html)
> 概要: 虎扑07月20日讯 CBA夏季联赛排位赛，北控不敌深圳取得第6名。今日，北控球员李玮颢更博晒此役比赛照。配文：“继续努力，继续学习。”此役李玮颢砍下14分10篮板3盖帽。   来源： 新浪微博
# 小说
### [姿势男的无限奇妙之旅](http://book.zongheng.com/book/267125.html)
> 作者：RTT

> 标签：科幻游戏

> 简介：以真气铸就强健的躯体，用波纹打造不灭的心灵。让替身带来逆转的好运，转佛光普度恶魔的苦厄。催战意以驱无边的冷寂，运神力施救伙伴的迷茫。真男人，坚不可摧！这是只属于石松宏一人的热血物语、成长传奇。终将有一天，他会坐上热血神州‘三皇五帝’的真神宝座！

> 章节末：第325章：终末之章 冰之无限

> 状态：完本
# 论文
### [Vector Quantisation for Robust Segmentation | Papers With Code](https://paperswithcode.com/paper/vector-quantisation-for-robust-segmentation)
> 日期：5 Jul 2022

> 标签：None

> 代码：https://github.com/ainkaransanthi/vector-quantisation-for-robust-segmentation

> 描述：The reliability of segmentation models in the medical domain depends on the model's robustness to perturbations in the input space. Robustness is a particular challenge in medical imaging exhibiting various sources of image noise, corruptions, and domain shifts. Obtaining robustness is often attempted via simulating heterogeneous environments, either heuristically in the form of data augmentation or by learning to generate specific perturbations in an adversarial manner. We propose and justify that learning a discrete representation in a low dimensional embedding space improves robustness of a segmentation model. This is achieved with a dictionary learning method called vector quantisation. We use a set of experiments designed to analyse robustness in both the latent and output space under domain shift and noise perturbations in the input space. We adapt the popular UNet architecture, inserting a quantisation block in the bottleneck. We demonstrate improved segmentation accuracy and better robustness on three segmentation tasks. Code is available at \url{https://github.com/AinkaranSanthi/Vector-Quantisation-for-Robust-Segmentation}
### ["I'm sorry to hear that": finding bias in language models with a holistic descriptor dataset | Papers With Code](https://paperswithcode.com/paper/i-m-sorry-to-hear-that-finding-bias-in)
> 概要: As language models grow in popularity, their biases across all possible markers of demographic identity should be measured and addressed in order to avoid perpetuating existing societal harms. Many datasets for measuring bias currently exist, but they are restricted in their coverage of demographic axes, and are commonly used with preset bias tests that presuppose which types of biases the models exhibit. In this work, we present a new, more inclusive dataset, HOLISTICBIAS, which consists of nearly 600 descriptor terms across 13 different demographic axes. HOLISTICBIAS was assembled in conversation with experts and community members with lived experience through a participatory process. We use these descriptors combinatorially in a set of bias measurement templates to produce over 450,000 unique sentence prompts, and we use these prompts to explore, identify, and reduce novel forms of bias in several generative models. We demonstrate that our dataset is highly efficacious for measuring previously unmeasurable biases in token likelihoods and generations from language models, as well as in an offensiveness classifier. We will invite additions and amendments to the dataset, and we hope it will help serve as a basis for easy-to-use and more standardized methods for evaluating bias in NLP models.
