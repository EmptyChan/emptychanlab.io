---
title: 2023-04-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.EuropeFromISS_ZH-CN0722816540_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-12 22:00:26
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.EuropeFromISS_ZH-CN0722816540_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [我们用文心一言，做了张单曲](https://www.woshipm.com/ai/5804314.html)
> 概要: 前段时间，百度上线了大语言模型文心一言，还展示了文心一言在文学创作和商业文案创作领域的能力。那么，文心一言能写好兼具文学跟商业色彩的歌词吗？怎么利用关键词让文心一言写出一首还不错的歌词？本文作者用文心......
### [硬件产品创新全流程概述](https://www.woshipm.com/operate/5804185.html)
> 概要: 由于需求、技术和市场瞬息万变，硬件企业不能仅仅依靠现有产品维持市场地位，必须源源不断地开发新产品。一个成功的硬件产品开发，需要经历什么流程呢？又要如何创新？一起来看一下吧。硬件产品难，硬件企业也难。不......
### [长视频格局生变，优爱腾芒B能否走出无限竞争？](https://www.woshipm.com/it/5803868.html)
> 概要: 前几日，抖音和腾讯视频之间达成合作的官宣说明，似乎再一次消融了长短视频之间的长久隔阂，而最近引发互联网圈热切讨论的AIGC热潮，也可能将给在线视频领域带来新的颠覆与机遇。不如一起来看看作者的分析和解读......
### [美国工会反对为Waymo与Aurora提供无人驾驶卡车豁免权](https://finance.sina.com.cn/stock/usstock/c/2023-04-12/doc-imyqancp6378393.shtml)
> 概要: 新浪科技讯 北京时间4月12日早间消息，美东时间周二，美国一主要运输工会表示，反对向Alphabet旗下的自动驾驶部门Waymo和自动驾驶技术公司Aurora Innovation的豁免请求。此前，这......
### [上诉法庭下令重审“惠普欺诈股东案”：隐瞒违规销售操作](https://finance.sina.com.cn/stock/usstock/c/2023-04-12/doc-imyqancs7235268.shtml)
> 概要: 新浪科技讯 北京时间4月12日早间消息，美东时间周二，美国一上诉法庭下令基层法庭重新审判有关惠普公司的一宗股东诉讼。在此案中，惠普被控欺诈和误导股东，即在2015年和2016年悄悄地用无法盈利的手段来......
### [Tech100 | 业内首创柔性索驱动工业机器人，「行远科技」正在拓展更多应用场景](https://36kr.com/p/2210613226304129)
> 概要: Tech100 | 业内首创柔性索驱动工业机器人，「行远科技」正在拓展更多应用场景-36氪
### [对话沈抖：百度做云业务，绝不会走阿里、华为的“老路”](https://finance.sina.com.cn/tech/internet/2023-04-12/doc-imyqancp6384750.shtml)
> 概要: 对话沈抖：百度做云业务，绝不会走阿里、华为的“老路”
### [版式设计原理 线的属性特征！](https://www.zcool.com.cn/work/ZMTUzNjU4NA==.html)
> 概要: 版式设计原理 线的属性特征！
### [11年考研，小镇青年八战清华](https://www.huxiu.com/article/1144847.html)
> 概要: 本文来自微信公众号：看天下实验室（ID：vistaedulab），作者：石悦欣、赖钰，编辑：沈佳音，头图来自：视觉中国2022年12月26日，最后一门专业课考试结束，袁博是最后一个离开考场的人。试卷都......
### [求购Space X、Open AI老股；转让持有Neuralink、某头部自动驾驶公司的基金份额｜资情留言板第89期](https://36kr.com/p/2211741658756484)
> 概要: 求购Space X、Open AI老股；转让持有Neuralink、某头部自动驾驶公司的基金份额｜资情留言板第89期-36氪
### [变强是不是就能有朋友？](https://news.idmzj.com/article/77737.html)
> 概要: 这期给大家推荐一部科幻漫画《侵略好意》，作者犬背九二郎，冰夜汉化组汉化，讲述孤单一人的她渴望交朋友的故事。
### [台积电们，都顶不住了？](https://www.huxiu.com/article/1145382.html)
> 概要: 本文来自微信公众号：半导体行业观察 （ID：icbank），作者：穆梓，题图来自：视觉中国在晶圆代工领域，中国台湾是其中最举足轻重的角色。根据Digitimes研究中心数据，在2022年，台湾四大晶圆......
### [OpenAI推出漏洞赏金计划 最高赏金2万美元](https://www.3dmgame.com/news/202304/3866875.html)
> 概要: 新浪科技今日消息，OpenAI周二在一篇博客文章中写道，它推出了一项漏洞赏金计划，人们可以通过该计划报告他们在使用其人工智能产品（ChatGPT）时发现的弱点、漏洞或安全问题。OpenAI表示，他们正......
### [《元气囝仔》真人电视剧化决定！](https://news.idmzj.com/article/77738.html)
> 概要: 吉野五月的漫画《元气囝仔》决定拍摄真人电视剧。
### [“元宇宙”游戏开发商Gamefam劳工纠纷已达成和解](https://www.3dmgame.com/news/202304/3866879.html)
> 概要: Gamefam 成立于 2019年，致力于开发《罗布乐思》、《堡垒之夜》和 《我的世界》的内容。其作品包括《罗布乐思》中的《索尼克速度模拟》、《堡垒之夜》里的《Puma：Go Crazy Arena》......
### [百利好环球：原油产品怎么投资？先了解这些知识！](http://www.investorscn.com/2023/04/12/106793/)
> 概要: 原油作为全球成交量较大的投资品种,其自身价值不言而喻,有广泛的市场关注度和较高的投资热度。从长远来看,原油投资是一种趋势投资,也是一种价值投资。百利好环球小编认为投资原油是一种高风险高收益的投资机会,......
### [百惠证券：数字人民币高速发展 提升金融市场竞争力](http://www.investorscn.com/2023/04/12/106794/)
> 概要: 移动支付方式在中国内地已经十分盛行，根据人民银行的统计，有赖于科网巨头的大力推动，市民移动支付的习惯在2022年普及度已高达90%，「无现金生活」已正式启动。百惠证券注意到，这个趋势亦激化了数字人民币......
### [我国养老金问题如何缓解](https://www.yicai.com/news/101727929.html)
> 概要: 1.  我国养老金支付目前由政府、企业和个人商业养老保险的三大支柱构成。4.  在人口老龄化趋势不可逆转的情况下，减缓养老金缺口的日益扩大的唯一增收节支手段只能是延迟退休和机关事业单位与企业职工的养老金并轨。
### [共享中国大市场发展机遇，中免集团600平米展位精彩亮相第三届消博会](http://www.investorscn.com/2023/04/12/106799/)
> 概要: 4月11日至15日，第三届中国国际消费品博览会（以下简称“消博会”）在海南海口举办，中免集团作为全球最大旅游零售商深度参与，携手历峰集团打造600平米展位，展示国际消费品精品新潮流、新趋势......
### [人气漫画《元气团仔》确定将改编真人电视剧](https://www.3dmgame.com/news/202304/3866900.html)
> 概要: 今年 5 月号的史克威尔·艾尼克斯的《月刊少年GANGAN》杂志日前宣布，吉野五月的漫画《元气团仔》将获得真人电视剧改编。漫画以同一期开始的限时回归连载。漫画最早于 2009 年在 SE 的线上漫画网......
### [昆仑万维与阿里云达成战略合作 将新建智算中心支撑大模型创新](https://36kr.com/p/2212002241443207)
> 概要: 昆仑万维与阿里云达成战略合作 将新建智算中心支撑大模型创新-36氪
### [【CASEKOO一些图】](https://www.zcool.com.cn/work/ZNjQ4NTgwMDg=.html)
> 概要: 也是很久没发图了，最近是存了一点。。有些好点的图各位大哥说想晚点发。。。先发一些手机壳和一些通配项目给大家看看。渲染师：@洒家叶良辰 @骆文涛 @77Dannier @小小吴哈（高级渲染师） @是困困......
### [Norman Walsh](https://www.zcool.com.cn/work/ZNjQ4NTgyMDg=.html)
> 概要: 「Norman Walsh 在 1961 年成立于英国博尔顿（Bolton），是英国著名的践行「经典复古风格」的户外运动跑鞋品牌，从二十世纪六十年代开始就用最优质的材料制作适用于不同体育项目的训练鞋，......
### [冒险经营游戏《漂浮的努西法拉岛》登陆Steam](https://www.3dmgame.com/news/202304/3866908.html)
> 概要: 冒险经营游戏《Floating Islands of Nucifera》（《漂浮的努西法拉岛》）现已登陆Steam平台，特惠43元，支持中文，感兴趣的玩家可以由此进入Steam商店。漂浮的努西法拉岛是......
### [铁骑——自行车广告](https://www.zcool.com.cn/work/ZNjQ4NTgzNzY=.html)
> 概要: Play VideoPlayreplay 10 secondsforward 10 secondsCurrent Time0:00/Duration Time0:00Progress: NaN%Pla......
### [又禁露天烧烤，有必要吗？](https://www.huxiu.com/article/1148282.html)
> 概要: 本文来自微信公众号：新周刊 （ID：new-weekly），作者：土卫六，编辑：刘车仔，校对：黄思韵，图源：视觉中国城市生活，离不了烧烤的烟火气，如何降解这些“烟”，才是治理污染的途径。最近山西晋城因......
### [海外new things | 食品技术初创「Aqua Cultured Foods」融资550万美元，用真菌发酵工艺制造海鲜替代蛋白](https://36kr.com/p/2204119403655815)
> 概要: 海外new things | 食品技术初创「Aqua Cultured Foods」融资550万美元，用真菌发酵工艺制造海鲜替代蛋白-36氪
### [霉霉或因失恋演唱会上流泪 粉丝怀疑分手早有暗示](https://ent.sina.com.cn/y/youmei/2023-04-12/doc-imyqciic5918219.shtml)
> 概要: 新浪娱乐讯 流行巨星泰勒·斯威夫特与相恋六年的英国男演员乔·阿尔文分手的消息，占据了这两天的娱乐头条。　　之前霉霉和阿尔文将近半年没有被拍到在一起，外界丝毫没有多想，因为两人相处的这些年，合体的镜头少......
### [专家找到北京堵车罪魁祸首 网约车每天上路12小时](https://www.3dmgame.com/news/202304/3866910.html)
> 概要: 堵车是任何一个大城市难以根治的顽疾，我国不但是人口最多的国家之一，也是全球最大的汽车消费市场之一。所以，在很多城市，堵车成为了不少人日常生活中最为困扰的事情之一。这一点，相信很多居住在北京、上海等一线......
### [京东家电家居推出三大专属扶持举措，打开佛山产业带增长新空间](http://www.investorscn.com/2023/04/12/106806/)
> 概要: 4月12日，由新华社民族品牌工程办公室主办，新华网、京东承办的“特色产业集群中国行·走进佛山”大会在佛山召开。在佛山市商务局、佛山市工业和信息化局的支持下，京东立足为实体经济高质量发展不断拓展新的增量......
### [AI焦虑潮下，打工人的抵抗、转向、破局](https://www.huxiu.com/article/1148078.html)
> 概要: 作者：黄小艺，头图来自：视觉中国一股“AI让人下岗”的焦虑，正在蔓延。不同于区块链、元宇宙和Web3，2023年的这股AI浪潮真正席卷了所有人——在大厂、大佬和投资人们为船票激烈角逐的同时，Midjo......
### [涨得比黄金还猛，翡翠适合普通人投资吗？](https://finance.sina.com.cn/china/2023-04-12/doc-imyqciik4005420.shtml)
> 概要: 来源：中新经纬 中新经纬4月12日电 （李自曼 马静）“最近一到周末人就特别多。我们店铺位于广州的工厂每天都有成交，出货量都翻倍啦，之前一个月也成交不了几次。
### [比亚迪 4 月 18 日上海车展举行仰望品牌发布会，百万级国产超跑即将登场](https://www.ithome.com/0/686/032.htm)
> 概要: 感谢IT之家网友猪肉拉面的线索投递！IT之家4 月 12 日消息，比亚迪将于 4 月 18 日在上海车展举行仰望品牌发布会，现在还有 6 天。从海报来看，届时将推出两款全新车型，包括百万级国产超跑 —......
### [被曾可妮要求"尊重"花钱改的名字 朱洁静郑重回应](https://ent.sina.com.cn/s/m/2023-04-12/doc-imyqciie8771846.shtml)
> 概要: 新浪娱乐讯 4月12日，朱洁静为参加新一季《乘风破浪2023》 的曾可妮加油，并且直呼原名“曾妮”，曾可妮幽默回应：“朱姐，尊重一下我花钱改的名字，妮妮会加油的。”俩人的可爱互动引发热议后，朱洁静发微......
### [京东零售取消事业部制 对家电厂家影响几何？](https://finance.sina.com.cn/jjxw/2023-04-12/doc-imyqcpre7105742.shtml)
> 概要: 4月12日，京东零售取消事业部制的消息在家电业界引发关注和讨论。业内人士认为，京东应对外部挑战，删除繁文缛节，此次变革有助于组织效率提升、在内部形成合力。
### [伦敦发展促进署CEO西特伦：伦敦的剧院、商店、餐厅都非常想念中国游客](https://www.yicai.com/news/101728732.html)
> 概要: 西特伦说，我们预计从今年春末夏初到秋天，伦敦的旅客数量将出现激增。
### [戴尔新款 XPS 13 Plus 笔记本上架：i7-1360P + 3.5K OLED 屏，15999 元](https://www.ithome.com/0/686/042.htm)
> 概要: IT之家4 月 12 日消息，戴尔去年推出的旗舰轻薄本 XPS 13 Plus 现已迎来配置升级，新款搭载了 i7-1360P，内存升级到 LPDDR5-6000 规格。戴尔中国官网现已上架新款 XP......
### [TV动画《AYAKA ‐あやか‐》角色PV第二弹公开](https://news.idmzj.com/article/77744.html)
> 概要: 在TV动画《AYAKA ‐あやか‐》中，梶原岳人饰演福分茶太郎，榊原优希饰演天乃夜胡。福分茶太郎和天乃夜胡、鸟海浩辅饰演的鞍马春秋3人的角色PV也公开了。
### [华硕推出 B760 天选主板 DDR5 版：售价 1449 元](https://www.ithome.com/0/686/047.htm)
> 概要: IT之家4 月 12 日消息，华硕在今年 2 月份推出了 TX GAMING B760M WIFI D4 天选主板，售价 1299 元。现在，华硕推出了这款主板的 DDR5 版本，售价 1449 元......
### [2023年能源工作：保供稳价，单位GDP能耗降低2%左右](https://www.yicai.com/news/101728737.html)
> 概要: 近三年的年度能源工作指导意见中，保供、稳价的重要性逐渐凸显。
### [《赛马娘 ROAD TO THE TOP》第一话预告公开](https://news.idmzj.com/article/77745.html)
> 概要: 将从4月16日开始，每周日21时（日本时间）播出的《赛马娘 ROAD TO THE TOP》公开了第一话《梦的开始》的预告视频和场景片段。
### [“五一”旅游订单激增200%， 文旅板块会否迎来上涨行情？](https://www.yicai.com/news/101728817.html)
> 概要: 二季度向来是旅游旺季，包含清明、“五一”、端午三个公众假期，叠加今年疫情后复苏趋势强劲，整体旅游板块热度持续走高。
### [海昌海洋公园携手沙特投资部，共同研究沙特发展与文旅业态投资新机遇](http://www.investorscn.com/2023/04/12/106812/)
> 概要: 沙特阿拉伯当地时间4月11日，沙特阿拉伯投资部与海昌海洋公园，在沙特阿拉伯签署谅解备忘录，双方之间将建立合作关系，共同研究在沙特发展与文旅业态有关的投资机会，并联合宣布计划开发沙特阿拉伯王国首个大型海......
### [最高赚超200%！巴菲特“爆买”日本股票，原因是……](https://finance.sina.com.cn/china/2023-04-12/doc-imyqcpqz5812059.shtml)
> 概要: 来源：国际金融报 全球罕见的低估值，被认为是日本市场吸引投资者的魅力之一。 92岁的“股神”巴菲特时隔11年再度访问日本。 4月11日...
### [一周一张？国际投行又领巨额罚单！](https://finance.sina.com.cn/china/2023-04-12/doc-imyqcpra8654530.shtml)
> 概要: 来源：中国基金报 距离本月4日高盛因错误标记“做多做空”被罚300万美元仅过去一周，美东时间4月11日又有消息称，高盛因违反指数掉期交易相关标准，在价格披露上存在问题...
### [上海电影：公司的AI业务目前尚处在初步探索阶段 尚未对公司产生实际利润贡献](https://www.yicai.com/news/101728913.html)
> 概要: 公司的AI业务目前尚处在初步探索阶段，尚未对公司产生实际利润贡献。
### [韩正会见美国英特尔公司首席执行官帕特·格尔辛格](https://finance.sina.com.cn/china/2023-04-12/doc-imyqctxe3889121.shtml)
> 概要: 新华社北京4月12日电（记者刘华）4月12日，国家副主席韩正在北京会见美国英特尔公司首席执行官帕特·格尔辛格。 韩正表示，中国始终坚持改革开放的基本国策...
### [微星发布三款 RTX 4070 显卡，买魔龙型号送《原子之心》](https://www.ithome.com/0/686/078.htm)
> 概要: IT之家4 月 12 日消息，微星今日发布三款 RTX 4070 显卡，分别是魔龙、万图师 3X 和万图师 2X。GAMING TRIO 魔龙系列微星表示，GAMING TRIO 仍然保留了魔龙标志性......
### [李昇基回应结婚争议 称妻子父母贪污巨款是误报](https://ent.sina.com.cn/s/j/2023-04-12/doc-imyqctxe3901033.shtml)
> 概要: 新浪娱乐讯 12日，李昇基发长文回应结婚争议，他表示妻子李多寅的父母操纵股价贪污260亿韩元一事是误报，目前已经起诉相关媒体。对于粉丝、周围好友反对他结婚，希望他想想未来，他认为：“怎么能够因为父母的......
# 小说
### [异世界怪谈](https://book.zongheng.com/book/911066.html)
> 作者：知鸟亦知鱼

> 标签：悬疑灵异

> 简介：这是一场穿越维度的冒险。当人们的精神力挣脱了束缚，我们便只能被迫在一场场追逐中寻找生存的机会。是猎人，还是猎物，已然由不得你选择。生命，本就是一场非公平的冒险。又有谁会知道，逃出生天是否是正确答案呢。

> 章节末：第743章 画手与梦境（终章）

> 状态：完本
# 论文
### [Language Supervised Training for Skeleton-based Action Recognition | Papers With Code](https://paperswithcode.com/paper/language-supervised-training-for-skeleton)
> 日期：10 Aug 2022

> 标签：None

> 代码：https://github.com/martinxm/lst

> 描述：Skeleton-based action recognition has drawn a lot of attention for its computation efficiency and robustness to lighting conditions. Existing skeleton-based action recognition methods are typically formulated as a one-hot classification task without fully utilizing the semantic relations between actions. For example, "make victory sign" and "thumb up" are two actions of hand gestures, whose major difference lies in the movement of hands. This information is agnostic from the categorical one-hot encoding of action classes but could be unveiled in the language description of actions. Therefore, utilizing action language descriptions in training could potentially benefit representation learning. In this work, we propose a Language Supervised Training (LST) approach for skeleton-based action recognition. More specifically, we employ a large-scale language model as the knowledge engine to provide text descriptions for body parts movements of actions, and propose a multi-modal training scheme by utilizing the text encoder to generate feature vectors for different body parts and supervise the skeleton encoder for action representation learning. Experiments show that our proposed LST method achieves noticeable improvements over various baseline models without extra computation cost at inference. LST achieves new state-of-the-arts on popular skeleton-based action recognition benchmarks, including NTU RGB+D, NTU RGB+D 120 and NW-UCLA. The code can be found at https://github.com/MartinXM/LST.
### [Deep Reinforcement Learning for Computational Fluid Dynamics on HPC Systems | Papers With Code](https://paperswithcode.com/paper/deep-reinforcement-learning-for-computational)
> 概要: Reinforcement learning (RL) is highly suitable for devising control strategies in the context of dynamical systems. A prominent instance of such a dynamical system is the system of equations governing fluid dynamics. Recent research results indicate that RL-augmented computational fluid dynamics (CFD) solvers can exceed the current state of the art, for example in the field of turbulence modeling. However, while in supervised learning, the training data can be generated a priori in an offline manner, RL requires constant run-time interaction and data exchange with the CFD solver during training. In order to leverage the potential of RL-enhanced CFD, the interaction between the CFD solver and the RL algorithm thus have to be implemented efficiently on high-performance computing (HPC) hardware. To this end, we present Relexi as a scalable RL framework that bridges the gap between machine learning workflows and modern CFD solvers on HPC systems providing both components with its specialized hardware. Relexi is built with modularity in mind and allows easy integration of various HPC solvers by means of the in-memory data transfer provided by the SmartSim library. Here, we demonstrate that the Relexi framework can scale up to hundreds of parallel environment on thousands of cores. This allows to leverage modern HPC resources to either enable larger problems or faster turnaround times. Finally, we demonstrate the potential of an RL-augmented CFD solver by finding a control strategy for optimal eddy viscosity selection in large eddy simulations.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
