---
title: 2021-08-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeaSwallow_ZH-CN1134903878_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-08-26 22:52:55
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeaSwallow_ZH-CN1134903878_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [分布式监控系统 WGCLOUD v3.3.4 发布，新增指令下发批量执行能力](https://www.oschina.net/news/157385/wgcloud-3-3-4-released)
> 概要: HMS Core Insights第六期【华为帐号服务-打造全场景安全帐号体系】正在直播>>>>>WGCLOUD是一款集成度较高的分布式运维监控系统，具有易部署、易上手使用、轻量、高效等特点，serv......
### [JumpServer 堡垒机 v2.13.0 发布，支持飞书认证和消息通知，新增会话录像水印功能](https://www.oschina.net/news/157384/jumpserver-2-13-0-released)
> 概要: HMS Core Insights第六期【华为帐号服务-打造全场景安全帐号体系】正在直播>>>>>8月23日，JumpServer开源堡垒机正式发布v2.13.0版本。在这一版本中，JumpServe......
### [Confluence OGNL 注入漏洞通告](https://www.oschina.net/news/157446)
> 概要: HMS Core Insights第六期【华为帐号服务-打造全场景安全帐号体系】正在直播>>>>>报告编号：B6-2021-082601报告来源：360CERT报告作者：360CERT更新日期：202......
### [WebStorm 2021.2.1 发布，大幅改进对 Vue 的支持](https://www.oschina.net/news/157332/webstorm-2021-2-1-released)
> 概要: HMS Core Insights第六期【华为帐号服务-打造全场景安全帐号体系】正在直播>>>>>WebStorm 2021.2.1 是 WebStorm 2021.2 的第一个错误修复更新，此次更新......
### [《水星领航员》首次官方展示会开展！展示大量贵重的动画资料](https://news.dmzj.com/article/71999.html)
> 概要: 根据天野梢原作制作的动画《水星领航员》的首次官方展示会“水星领航员 The MEMORIA”从26日起正式开展。在这次的展示会上，展出了天野梢创作的美丽插图，佐藤顺一导演的制作笔记，动画的名场景等展品。
### [P站美图推荐——画师荻pote特辑](https://news.dmzj.com/article/72003.html)
> 概要: 荻pote老师最擅长的就是画大眼睛美少女，排行榜常客不用说，个人画集也在2月17日发售了。他最具特色的除去大眼睛以外，可能就是画面的故事感与氛围塑造极佳。如果看到最后一张图，你一定会为荻pote老师的成长所折服
### [【全篇剧透】漫威《假如…？》：对于粉丝向，不必抱太大期望](https://news.dmzj.com/article/72005.html)
> 概要: 漫威又推出了新的原创作品，这一次是动画剧集，相比真人版，自由度更高，脑洞也更大。不过这呈现效果，倒是让我有点失望了……
### [传说系列发表新作手游《Tales of Luminaria》](https://news.dmzj.com/article/72007.html)
> 概要: 万代南梦宫在线上活动gamescom Opening Night Live 2021中，发表了《传说系列》新作手游《Tales of Luminaria》的消息。
### [它才是顺丰和菜鸟的一生之敌](https://www.huxiu.com/article/451269.html)
> 概要: 来源：爱思考的柚子（ID：gh_c899544adc0a）作者：彭程题图：IC photo极兔是一家神秘的快递公司，网络传言他们和PDD有不可明言的特殊关系。2020年极兔冲入国内市场，打了一场很猛的......
### [动漫情侣头像｜小众情头](https://new.qq.com/omn/20210823/20210823A0EXCL00.html)
> 概要: 图源网络侵删 原截或原创看到可评论注明一键分享投币评论转发欢迎白嫖加关注......
### [The dream of carbon air capture edges toward reality](https://e360.yale.edu/features/the-dream-of-co2-air-capture-edges-toward-reality)
> 概要: The dream of carbon air capture edges toward reality
### [视频：小S风波后回应何时复工：再过阵子吧，随缘](https://video.sina.com.cn/p/ent/2021-08-26/detail-ikqciyzm3675395.d.html)
> 概要: 视频：小S风波后回应何时复工：再过阵子吧，随缘
### [从牛油果到松茸，城市中产到底什么时候才肯认穷](https://www.huxiu.com/article/451307.html)
> 概要: 本文来自微信公众号：猛犸工作室（ID：MENGMASHENDU），作者：黎广，编辑：马妮，头图来自：视觉中国8月是云南传统的松茸季，除了起个大早上山搜刮“头茬”松茸的采菌人，远道而来尝鲜的城市中产们同......
### [《光环：无限》Steam 248元 推荐RTX 2070显卡](https://www.3dmgame.com/news/202108/3822115.html)
> 概要: 《光环：无限》现已在Steam开启预购，战役剧情248元，多人模式免费，支持简体中文和中文配音，Steam版定于2021年12月8日发售，支持跨平台联机，支持手柄游玩。此外，Steam页面还公布了游戏......
### [《咩咩启示录》发布预告片 预计2022年发售](https://www.3dmgame.com/news/202108/3822118.html)
> 概要: 近日，Devolver Digital在科隆游戏展发布《咩咩启示录》预告片，该作目前Steam页面已经上线，计划该作将于明年正式发售，感兴趣的玩家点击此处进入商店页面。预告片：<br />游戏介绍：《......
### [漫画「伤痕小酒馆」决定制作真人剧](http://acg.178.com/202108/423947928526.html)
> 概要: 近日，漫画「伤痕小酒馆」决定制作真人剧，主演为日本知名女星原田知世，本作预计将于2021年10月8日播出。「伤痕小酒馆」讲述了一个只有受伤的人才能到达的小酒馆，酒馆的老板用各种美味的食物与音乐，治愈客......
### [今日份动漫男生头像！](https://new.qq.com/omn/20210817/20210817A08DUC00.html)
> 概要: 所有图片均来自于网络各处......
### [音乐NFT 一朵带刺的玫瑰......](https://www.tuicool.com/articles/J7RZ3un)
> 概要: 随着多家知名企业参与到NFT数字藏品制作领域，各种各样不同类型的NFT数字藏品层出不穷。近日，在国内NFT数字藏品也扩张到了音乐领域，随着某知名音乐平台正式发售歌曲的纪念版NFT数字藏品后，音频NFT......
### [「堀与宫村」Blu-ray＆DVD豪华全卷收纳BOX封面公开](http://acg.178.com/202108/423952874522.html)
> 概要: 近日，TV动画「堀与宫村」官方公开了本作Blu-ray＆DVD豪华全卷收纳BOX封面图，由原作萩原大辅绘制。漫画「堀与宫村」讲述了看起来很时尚、却是持家女的堀京子，与看似是宅男、其实满身是刺青和很多耳......
### [再演职场女性 孙俪:“苏筱”和“房似锦”大不同](https://ent.sina.com.cn/v/m/2021-08-26/doc-ikqciyzm3709985.shtml)
> 概要: 由刘进执导，孙俪、赵又廷领衔主演的现实主义职场话题剧正在东方卫视热播，该剧讲述造价师苏筱（孙俪饰）充满热血色彩的职场进阶之路。　　近日，孙俪接受采访透露，苏筱在成长过程中，会徘徊、碰壁，“眼里不揉沙，......
### [「永恒族：灭霸崛起」单刊变体封面公开](http://acg.178.com/202108/423956410416.html)
> 概要: 近日，漫威漫画公开了全新单刊「永恒族：灭霸崛起」的变体封面，此次封面由画师Dustin Weaver独家绘制。本刊将联动「永恒族」本卷，由Kieron Gillen负责剧本创作，Dustin Weav......
### [「IDOLiSH7」第3季第9话「Re:vale」先行图公开](http://acg.178.com/202108/423958926540.html)
> 概要: 电视动画「IDOLiSH7」第3季「IDOLiSH7 Third BEAT!」公开了第9话「Re:vale」的先行图。「IDOLiSH7 Third BEAT!」改编自同名手游，由TROYCA负责制作......
### [孙俪：不修边幅是剧中真实人设 不喜欢被叫孙一条](https://ent.sina.com.cn/v/m/2021-08-26/doc-ikqciyzm3718620.shtml)
> 概要: 由孙俪、赵又廷主演的都市题材职场剧正在热播，这是孙俪继之后再次参与都市题材职场剧的演出，而这次由她饰演的造价师苏筱，却与上一次的房似锦大为不同。　　　　在《理想之城》的故事里，造价师苏筱带着理想主义的......
### [电池大王不好当啊](https://www.huxiu.com/article/451445.html)
> 概要: 出品 | 虎嗅汽车组作者 | 流浪法师在大宗商品价格飙升的当下，宁德时代找到了新的发力点。8月25日盘后，宁德时代发布2021年上半年年报，营收440.75亿元，净利润44.8亿元。由于去年同期受到疫......
### [章泽天在刘强东老家成立新公司 注册资本为1000万](https://ent.sina.com.cn/s/m/2021-08-26/doc-ikqcfncc5120237.shtml)
> 概要: 新浪娱乐讯 8月24日，据天眼查App显示，宿迁天资企业管理合伙企业（有限合伙）、宿迁天际企业管理合伙企业（有限合伙）成立，执行事务合伙人均为章泽天全资持股的江苏天韵企业管理有限公司，经营范围包括企业......
### [2195元！伊藤润二“富江”绝美限量雕像 今晚开预售](https://acg.gamersky.com/news/202108/1418757.shtml)
> 概要: 伊藤润二漫画《富江》雕像今晚开启预订。
### [P1S《街头霸王5》春丽1/4雕像 售价约6155元](https://www.3dmgame.com/news/202108/3822145.html)
> 概要: 今日（8月26日），著名模型厂商Prime1Studio推出了新品《街头霸王5》春丽1/4雕像，售价949美元（约合人民币6155元），预计于2022年11月-2023年2月之间出货。春丽雕像高达77......
### [《海贼王》1023话情报：“一模一样”](https://acg.gamersky.com/news/202108/1418835.shtml)
> 概要: 《海贼王》1023话情报公开，索隆和山治依旧在和烬跟奎因战斗，另一边变成大人的桃之助将要和路飞一起从凯多手中夺回和之国。
### [富士电机追加 400 亿日元扩产功率半导体，未来或再加 500 亿](https://www.ithome.com/0/571/755.htm)
> 概要: 日本富士电机（Fuji Electric）计划追加 400 亿日元（3.65 亿美元）投资，扩产功率半导体。据悉，该公司生产的功率半导体主要用于空调、电动汽车等产品的电力系统。据日经亚洲评论报道，富士......
### [五菱宏光 MINIEV 马卡龙将推“冰蓝色”版](https://www.ithome.com/0/571/761.htm)
> 概要: IT之家8 月 26 日消息 五菱宏光 MINIEV 马卡龙是宏光 MINIEV 的升级车型，于 4 月份上市，新车在外观、内饰及配置上均有所升级，并拥有柠檬黄、牛油果绿、白桃粉等配色，标配主驾安全气......
### [《心动4》完结后，小孔停止与节目组互动，“快乐星球”也无宣传](https://new.qq.com/omn/20210826/20210826A0ACW500.html)
> 概要: 任何一档节目，都有让大家不满意的地方，在《心动4》节目中，工作人员每次宣传，都带着马董和橙子的话题，让大家误以为他们两个就是最终cp。或许是希望太高，导致后期马董选择小孔，引起了大批观众的不满。这类综......
### [从拉窗帘事件浅谈小兰的“善良”，柯南剧中两种价值观的碰撞](https://new.qq.com/omn/20210826/20210826A0AM4R00.html)
> 概要: 在名柯中有这么一个剧情场景，本意应是为了增加毛利兰这个女主角色的正向形象设定，可结果却适得其反，反让她背上了不少的争议，这就是柯南TV版动画中的“侦探事务所挟持事件”，而那个场景就是毛利兰主动为犯人拉......
### [Federal government to expand use of facial recognition despite growing concerns](https://www.washingtonpost.com/technology/2021/08/25/federal-facial-recognition-expansion/)
> 概要: Federal government to expand use of facial recognition despite growing concerns
### [荣耀手表 GS 3 官方实拍图曝光：流光经典、竞速先锋](https://www.ithome.com/0/571/789.htm)
> 概要: IT之家8 月 26 日消息 今日，荣耀官方曝光了荣耀手表 GS 3 的实拍图，包括流光经典与竞速先锋等版本。IT之家了解到，荣耀手表 GS 3 是荣耀首款搭载心率监测解决方案“8 通道心率 AI 引......
### [一篇文章，彻底搞懂品牌出海新玩法](https://www.tuicool.com/articles/vQjaIjm)
> 概要: 近年来，中国品牌处于“出海”新风口，品牌出海越来越火。据统计，2020年出口总额约占GDP的15%，而且这个比例每年都在增长。其实，中国参与全球化协作，已经有非常长一段时间了，这波“跨境电商”的大热，......
### [UK to overhaul privacy rules in post-Brexit departure from GDPR](https://www.theguardian.com/technology/2021/aug/26/uk-to-overhaul-privacy-rules-in-post-brexit-departure-from-gdpr)
> 概要: UK to overhaul privacy rules in post-Brexit departure from GDPR
### [华为 HiLink：香山体脂称 18 元上新狂促（立减 50 元）](https://lapin.ithome.com/html/digi/571793.htm)
> 概要: 【活动随时结束】香山 x 华为 HiLink 体脂称日常售价 69.9 元，今日可领 50 元大促券，实付 19.9 元包邮：天猫香山 x 华为 HiLink 体脂秤磨砂钢化玻璃面板券后 19.9 元......
### [被多家媒体点名，一日之内多个热搜，娱乐圈的“天”要变了？](https://new.qq.com/omn/20210826/20210826A0CHUW00.html)
> 概要: 爱奇艺牵头取消偶像选秀、央视新闻点评“天府少年团”、光明日报辣评日报耽改剧、湖南卫视主持人田源被爆骚扰……一日之内连上多个热搜，娱乐圈乱象是时候大整顿了。            娱乐圈明星的超高收入已......
### [董明珠不用直播了](https://www.tuicool.com/articles/B7NRzuA)
> 概要: 去年董明珠忙着去全国各地做直播卖空调，仿佛要把直播变成新渠道。但今年到现在，只在武汉、韶关做了两场直播带货，而且已经是几个月前的事了，其余时间似乎又忙着活在各种热搜和头条里。先是加盟综艺《初入职场的我......
### [粉丝犯错偶像买单，赵丽颖差点被官方树典型，如今总算发声了](https://new.qq.com/omn/20210826/20210826A0D3DJ00.html)
> 概要: 粉丝跟偶像之间的关系总是非常微妙，尤其是现在随着网络的快速发展，偶像跟粉丝之间的距离越来越近，所以他们经常会以各种不同的方式互动，可正因如此偶像就应该做好正确的引导作用，千万不要因为粉丝对自己的过度喜......
### [贾磊、邵化谦和媒体人周鹏解读，周琦为何要离开新疆](https://bbs.hupu.com/44949442.html)
> 概要: 来源：（@中国篮球-微博）
### [Adding Optimistic Locking to an API](https://www.moderntreasury.com/journal/designing-ledgers-with-optimistic-locking)
> 概要: Adding Optimistic Locking to an API
### [NFT音乐平台Royal完成1600万美元融资](https://www.btc126.com//view/184108.html)
> 概要: 8月26日消息，NFT音乐平台Royal完成1600万美元种子轮融资，Paradigm和Founders Fund领投。Royal由电子舞蹈音乐艺术家和NFT支持者3 LAU创立，Royal为歌迷提供......
### [组图：多家娱乐公司艺人工作室发布理智追星倡议书](http://slide.ent.sina.com.cn/star/w/slide_4_704_360788.html)
> 概要: 组图：多家娱乐公司艺人工作室发布理智追星倡议书
### [使用 Flink Hudi 构建流式数据湖](https://www.tuicool.com/articles/26vmu2I)
> 概要: ▼ 关注「Flink 中文社区」，获取更多技术干货▼摘要：本文介绍了 Flink Hudi 通过流计算对原有基于 mini-batch 的增量计算模型不断优化演进。用户可以通过 Flink SQL 将......
### [山东菏泽牡丹区政协原副主席杨明欣接受审查调查](https://finance.sina.com.cn/china/dfjj/2021-08-26/doc-ikqcfncc5207340.shtml)
> 概要: 原标题：山东菏泽牡丹区政协原副主席杨明欣接受审查调查 据菏泽市纪委监委网站消息，菏泽市牡丹区政协原副主席杨明欣涉嫌严重违纪违法...
### [九个关键词读懂习近平总书记承德之行](https://finance.sina.com.cn/china/gncj/2021-08-26/doc-ikqcfncc5207578.shtml)
> 概要: 联播+8月23日至24日，习近平总书记来到河北省承德市，深入国有林场、文物保护单位、农村、社区等进行调研。 在考察中总书记强调，要全面落实党中央决策部署...
### [浦东两专项“十四五”规划发布，将打造三大世界级产业集群](https://finance.sina.com.cn/china/gncj/2021-08-26/doc-ikqcfncc5209112.shtml)
> 概要: 原标题：浦东两专项“十四五”规划发布，将打造三大世界级产业集群 南财全媒体记者 吴霜 上海报道 8月26日，《浦东新区产业发展“十四五”规划》（以下简称《产业规划》）和《...
### [光荣公布《怪物农场1&2 DX》 12月9日发售](https://www.3dmgame.com/news/202108/3822166.html)
> 概要: 光荣公布了《怪物农场1&2 DX》，将于12月9日在全球各地发售，登陆PC Steam，Switch和iOS。游戏不支持中文。“你的童年回忆怪物农场#又回来了！培养专属于你的怪兽、与朋友们进行对战吧......
### [全球最大乐高乐园度假区今起正式动工，预计2024年开园](https://finance.sina.com.cn/china/dfjj/2021-08-26/doc-ikqciyzm3813310.shtml)
> 概要: 原标题：全球最大乐高乐园度假区今起正式动工，预计2024年开园 8月26日，界面新闻获悉，默林娱乐集团今日宣布，全球最大乐高乐园®度假区正式投建，落户深圳。
### [xSigma与Maxim合作推出NFT市场MaximNFT](https://www.btc126.com//view/184110.html)
> 概要: 财经报道，纳斯达克上市公司正康国际股份有限公司子公司xSigma与Maxim签订合作协议，推出独家NFT市场MaximNFT。根据协议条款，xSigma的开发商和NFT设计师将运营NFT平台，该平台将......
### [【直播】泪目！刺痛扎心发言：不知道为什么会变成这样子，我真的很想打比赛！](https://bbs.hupu.com/44950198.html)
> 概要: 【直播】泪目！刺痛扎心发言：不知道为什么会变成这样子，我真的很想打比赛！
### [陈延年魂穿乔二强，张晚意又一次让观众破防](https://new.qq.com/omn/20210826/20210826A0E8FO00.html)
> 概要: 正在播出的《乔家的儿女》第四集中有一段剧情，大哥乔一成（白宇 饰）数落顽皮的弟弟乔二强（张晚意 饰），“你迟早有一天要吃牢饭，要上刑场。”原本逗趣的兄弟互动却勾起了观众的回忆，弹幕满屏的“上辈子上过刑......
### [今年以来山东公安机关已找到失踪和被拐人员206名](https://finance.sina.com.cn/china/gncj/2021-08-26/doc-ikqciyzm3814529.shtml)
> 概要: 原标题：今年以来 山东公安机关已找到失踪和被拐人员206名 8月26日，记者从山东省政府新闻办新闻发布会获悉：目前，山东省公安厅建立“我为群众办实事”事项清单...
### [23人被追责 山西公布阜生煤业“10·20”较大瓦斯爆炸事故调查结果](https://finance.sina.com.cn/china/gncj/2021-08-26/doc-ikqcfncc5211042.shtml)
> 概要: 原标题：23人被追责 山西公布阜生煤业“10·20”较大瓦斯爆炸事故调查结果 日前，总台记者从山西煤矿安全监察局官网了解到，山西煤矿安全监察局公布了山西潞安集团左权阜生煤...
### [流言板队报：若姆巴佩离队，巴黎将追逐哈兰德、博格巴或卡马文加](https://bbs.hupu.com/44950415.html)
> 概要: 虎扑08月26日讯 《队报》当地时间8月26日透露，巴黎圣日尔曼虽然拒绝了皇家马德里对姆巴佩1.6亿欧元的报价，但已经开始着手针对姆巴佩今夏离队的可能性制定应对方案。如果巴黎能够从姆巴佩的潜在交易中获
### [流言板千万不要错过！今夜12:00，新赛季欧冠抽签暨欧足联年度颁奖](https://bbs.hupu.com/44950420.html)
> 概要: 虎扑08月26日讯 今天晚间12：00，新赛季欧冠联赛小组赛抽签暨欧足联颁奖典礼即将拉开大幕，拜仁、皇马、阿贾克斯、米兰能否造就死亡之组，集齐30座欧冠奖杯？若日尼奥能否荣膺欧足联年度最佳球员？精彩就
### [Netki与Polymath合作，为Polymesh区块链引入身份验证](https://www.btc126.com//view/184112.html)
> 概要: 8月26日消息，总部位于洛杉矶的KYC-AML监管技术提供商Netki目前正与区块链技术提供商Polymath合作，以将区块链特定的身份验证引入Polymesh。Polymesh是Polymath专门......
### [Constellation Network将为美国空军提供数据共享的区块链安全保障](https://www.btc126.com//view/184113.html)
> 概要: 财经报道，Constellation Network已获得一份合同，为美国空军和其商业伙伴之间的数据共享提供安全保障......
### [Fami通新一周销量榜 《健身环大冒险》重登榜首](https://www.3dmgame.com/news/202108/3822167.html)
> 概要: Fami通公开新一期日本游戏软硬件销量，本周榜首被榜单常客《健身环大冒险》夺得，《对马岛之鬼：导演剪辑版》均上榜，除此以外，其余均为Switch游戏。硬件方面，Switch继续引领榜单。榜单详情：软件......
# 小说
### [重生之我为魔](http://book.zongheng.com/book/1078256.html)
> 作者：心跳它说

> 标签：都市娱乐

> 简介：主人公，扭转时空而来，弥补遗憾，前世经历过三次冲击，达到A级巅峰，知晓诸多隐秘，于第三次冲击中期，探索潜渊古战场时重生，为人目的性极强，认准的目标绝不放手，无论是感情还是修行。

> 章节末：第449章　转瞬即逝

> 状态：完本
# 论文
### [A New Journey from SDRTV to HDRTV | Papers With Code](https://paperswithcode.com/paper/a-new-journey-from-sdrtv-to-hdrtv)
> 日期：18 Aug 2021

> 标签：None

> 代码：https://github.com/chxy95/hdrtvnet

> 描述：Nowadays modern displays are capable to render video content with high dynamic range (HDR) and wide color gamut (WCG). However, most available resources are still in standard dynamic range (SDR).
### [MicroNet: Improving Image Recognition with Extremely Low FLOPs | Papers With Code](https://paperswithcode.com/paper/micronet-improving-image-recognition-with)
> 日期：12 Aug 2021

> 标签：None

> 代码：https://github.com/liyunsheng13/micronet

> 描述：This paper aims at addressing the problem of substantial performance degradation at extremely low computational cost (e.g. 5M FLOPs on ImageNet classification). We found that two factors, sparse connectivity and dynamic activation function, are effective to improve the accuracy.
