---
title: 2022-05-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DuckHen_ZH-CN6493617016_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-05-04 22:51:04
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DuckHen_ZH-CN6493617016_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [开源终端 PuTTY 官方版在 Microsoft Store 上架](https://www.oschina.net/news/194146/putty-ms-store)
> 概要: PuTTY 开发者 Simon Tatham 已将最新版本 PuTTY 上架至Microsoft Store，用户可直接搜索以进行安装。PuTTY 是一款集成虚拟终端、系统控制台和网络文件传输为一体的......
### [昇思 MindSpore 1.7：易用灵活新起点，带给开发者新体验！](https://www.oschina.net/news/194139/mindspore-1-7-released)
> 概要: 经过社区开发者们的辛勤耕耘，我们马不停蹄地给大家递上昇思MindSpore最新的1.7版本。在此版本中我们持续提升框架易用性，简化安装过程，提供更丰富的文档和指导视频，帮助开发者快速上手；同时还提供了......
### [Kubernetes 1.24 正式发布](https://www.oschina.net/news/194188/kubernetes-1-24-released)
> 概要: Kubernetes 1.24 已正式发布，这也是 2022 年的首个大版本更新。1.24 总共有 46 项功能变化，其中包含：14 项增强功能已升级为稳定状态15 项增强功能正在进入测试阶段13 项......
### [Calibre 5.42 发布，功能强大的开源电子书工具](https://www.oschina.net/news/194137/calibre-5-42-released)
> 概要: Calibre 开源项目是 Calibre 官方出的电子书管理工具。它可以查看，转换，编辑和分类所有主流格式的电子书。Calibre 是个跨平台软件，可以在 Linux、Windows 和 macOS......
### [B端产品设计中，如何找准产品定位？](http://www.woshipm.com/pd/5421054.html)
> 概要: 编辑导语：产品在进入市场的过程中，往往需要找准自身定位，后续产品经理才好搭建对应的设计策略。然而B端产品在设计过程中，要如何才能找准产品定位呢？本篇文章里，作者就这个问题做了系统阐述，一起来看一下吧......
### [支付问题解决不了？不如从“账”上寻找突破口！](http://www.woshipm.com/pd/5420957.html)
> 概要: 编辑导语：作为支付产品经理，你可能会时常遇到一些业务变化或者难以解决的支付问题，这个时候，你不妨从账务上寻找一下解决方案，因为账务其实牵扯到了支付产品的方方面面。本篇文章里，作者就该话题做了阐述，一起......
### [用户体验思维7大陷阱，你了解吗？](http://www.woshipm.com/user-research/5421994.html)
> 概要: 编辑导语：产品设计的目标之一便是达成更加完善的用户体验，进而提升用户粘性，推动后续用户的留存和转化。然而，不少产品经理或运营人却仍会在用户体验优化上踩坑。本篇文章里，作者总结了用户体验思维的七大陷阱，......
### [马斯克计划最快三年内再让推特上市](https://finance.sina.com.cn/tech/2022-05-04/doc-imcwipii7896845.shtml)
> 概要: 据报道，马斯克表示，在将推特私有化后，他可能会在不久之后再次将其公开上市。该报道称，马斯克告诉潜在投资者，他计划在完成推特440亿美元私有化交易后，最快三年内再次将其进行首次公开募股。他预计私有化交易......
### [AMD第一季度营收58.87亿美元 净利润同比增长42%](https://finance.sina.com.cn/tech/2022-05-04/doc-imcwiwst5459733.shtml)
> 概要: 新浪科技讯 北京时间5月4日早间消息，AMD今天公布了该公司的2022财年第一季度财报。报告显示，AMD第一季度营收为58.87亿美元，与上年同期的34.45亿美元相比增长71%，与上一季度的48.2......
### [其称推特对普通用户永远免费](https://finance.sina.com.cn/tech/2022-05-04/doc-imcwiwst5462190.shtml)
> 概要: 新浪科技讯 北京时间5月4日早间消息，特斯拉CEO埃隆·马斯克（Elon Musk）今日发推称，Twitter对普通用户永远是免费的，但可能会对商业和政府用户收取一定费用。此前有报道称，若推特成功私有......
### [饭圈，从圈养到收割](https://www.huxiu.com/article/545798.html)
> 概要: 来源｜非凡油条（ID：ffyoutiao）作者｜小笼包头图｜视觉中国平台推动更多粉丝社群的构建2021年6月，中央网信办正式启动为期2个月的“清朗·‘饭圈’乱象整治”专项行动。这次行动以5类“饭圈”乱......
### [刘强东减持京东健康股份 持股比例从68.94%降至68.66%](https://finance.sina.com.cn/tech/2022-05-04/doc-imcwiwst5468531.shtml)
> 概要: 据香港交易所股权披露资料显示，京东健康的大股东刘强东在5月3日以平均每股49港元的价格出售约500万股京东健康股票，于4月29日以平均每股50.67港元的价格出售384万股股票。刘强东的持股比例从68......
### [TV动画「间谍过家家」公开最新官方壁纸](http://acg.178.com/202205/445626806329.html)
> 概要: 近日，TV动画「间谍过家家」公开了最新的官方壁纸。动画「间谍过家家」改编自远藤达哉创作的漫画作品，由WIT STUDIO和CLOVERWORKS负责制作。本作讲述了一名男性特工与女性暗杀者结婚，并领养......
### [「风都侦探」全新宣传绘公布](http://acg.178.com/202205/445627133486.html)
> 概要: 电视动画「风都侦探」于近日公开了全新的宣传绘，本作预计将于2022年8月1日正式开播。CAST左翔太郎：細谷佳正フィリップ：内山昂輝ときめ：関根明良鳴海亜樹子：小松未可子照井竜：古川慎万灯雪侍：小野大......
### [美任前总裁谈论工会问题：这不是以前那个任天堂](https://www.3dmgame.com/news/202205/3841626.html)
> 概要: 美国任天堂近日因对待兼职和合同工的问题而受到了抨击。美任（NoA）也对其中一项投诉做出了回应，表示公司“完全致力于”为员工和承包商提供一个欢迎和支持的环境，并提到了公司如何认真地对待就业问题。尽管如此......
### [100-year-old Brazilian breaks record after 84 years at same company](https://www.guinnessworldrecords.com/news/2022/4/100-year-old-brazilian-breaks-record-after-84-years-at-same-company-701664)
> 概要: 100-year-old Brazilian breaks record after 84 years at same company
### [又是一年17岁！名侦探柯南官方祝工藤新一生日快乐](https://acg.gamersky.com/news/202205/1480170.shtml)
> 概要: 5月4日是江户川柯南&工藤新一的生日，今日，《名侦探柯南》官方发布多张贺图，庆祝工藤新一又一年17岁生日快乐。
### [「LoveLive！虹咲学园学园偶像同好会」公开第二季Blu-ray第三卷封面](http://acg.178.com/202205/445631767892.html)
> 概要: 电视动画「LoveLive！虹咲学园学园偶像同好会」公开了第二季Blu-ray第三卷的封面图，由角色设计·横田拓己绘制。该商品售价为7,000日元（不含税），将于2022年8月26日正式发售。「Lov......
### [铁道益智解谜游戏《Railbound》上线Steam 年内发售](https://www.3dmgame.com/news/202205/3841633.html)
> 概要: 由Afterburn制作并发行的卡通风格铁道益智解谜游戏《Railbound》现已上线Steam，将于2022年内发售，支持中文。游戏中，玩家需要开动脑筋，将铁轨连接起来，让车身与车头连在一起，驶向远......
### [组图：关晓彤《王牌对王牌》红裙造型唱《初恋》 摩登复古感十足](http://slide.ent.sina.com.cn/z/v/slide_4_86512_369243.html)
> 概要: 组图：关晓彤《王牌对王牌》红裙造型唱《初恋》 摩登复古感十足
### [组图：马凡舒亮相央视五四特别节目 明黄色v领长裙温婉优雅](http://slide.ent.sina.com.cn/star/slide_4_86512_369244.html)
> 概要: 组图：马凡舒亮相央视五四特别节目 明黄色v领长裙温婉优雅
### [「夏日重现」ED主题曲无字幕动画MV公开](http://acg.178.com/202205/445634450485.html)
> 概要: 根据田中靖规创作的同名漫画作品改编，OLM负责制作的电视动画「夏日重现」近期公开了ED主题曲「回夏」的无字幕动画MV，该作已于4月14日开始播出。「回夏」无字幕动画MV......
### [Microsoft 3D Movie Maker Source Code](https://github.com/microsoft/Microsoft-3D-Movie-Maker)
> 概要: Microsoft 3D Movie Maker Source Code
### [NFT 市场迅速崩溃，日均销量较峰值锐减 92%](https://www.tuicool.com/articles/6FnEN3U)
> 概要: NFT 市场迅速崩溃，日均销量较峰值锐减 92%
### [龙珠超，当前版本赛亚人4大主流进化方向，官方宣布悟饭将出现新形态](https://new.qq.com/omn/20220504/20220504A04IZ100.html)
> 概要: 赛亚人是宇宙中当之无愧的战斗民族，他们遇强则强，能够一次次地突破极限，从上古到现在，赛亚人给后世留下了数不尽的传说。《龙珠》系列动漫问世至今，剧情中已经出现了形形色色的进化路线，赛亚人也诞生了数不尽的......
### [专家解释建筑晃动原因:SM爱反复舞蹈引起震动](https://ent.sina.com.cn/k/2022-05-04/doc-imcwipii7955830.shtml)
> 概要: 新浪娱乐讯 5月4日，据韩媒，针对今年1月20日在首尔城东区超高层商住两用建筑Acro Seoul Forest发生的建筑晃动现象的原因，专家们经过2个多月的调查，得出了“反复舞蹈动作等引起的震动”的......
### [AMD Zen4锐龙“龙凤胎”来了：游戏本终于满血](https://www.3dmgame.com/news/202205/3841642.html)
> 概要: AMD今天凌晨发布了2022年Q1季度财报，营收创纪录的同时还公布了消费级Zen4的路线图，跟以往只有桌面版+移动版不同，这次实际上是三大消费级产品线了，移动版锐龙7000会多出一个专攻性能的新版本......
### [暴雪计划改善《守望先锋2》辅助的游戏体验 并优化UI](https://www.3dmgame.com/news/202205/3841643.html)
> 概要: 随着《守望先锋2》第一周的技术测试接近尾声，开发者暴雪分享了游戏的发展计划。根据《守望先锋》官方网站上发布的一篇博文，该团队目前的首要任务是让玩家更喜欢辅助职责，这最终将缩短玩家排队时间，以及优化游戏......
### [业内消息称手机零部件供应商面临去库存压力](https://www.ithome.com/0/616/541.htm)
> 概要: 据业内消息人士透露，在中国大陆手机销售依然低迷的情况下，相关手机零部件供应商的库存调整可能会延长至 2022 年底。据 digitimes 报道，半导体供应链对进入二季度的中国大陆手机市场前景持保守态......
### [这届“小镇青年”，真会消费](https://www.huxiu.com/article/545979.html)
> 概要: 本文来自微信公众号：财经国家周刊 （ID：ENNWEEKLY），作者：王亭亭，编辑：张安彤 宋怡青 吴丽华，题图来自：视觉中国周岩反复思量之后，终于在2021年4月结束北漂回到山东淄博老家，在离家不远......
### [小伙伴们五一快乐呀～tienchin 项目准备出视频了！](https://www.tuicool.com/articles/Ir2yUzv)
> 概要: 小伙伴们五一快乐呀～tienchin 项目准备出视频了！
### [谷歌 YouTube Go 变“YouTube Stop”，今年 8 月份关闭](https://www.tuicool.com/articles/A7VfArb)
> 概要: 谷歌 YouTube Go 变“YouTube Stop”，今年 8 月份关闭
### [山东电子健康码再“升级”：核酸检测结果（48 小时、7 天）、行程卡、疫苗接种等同屏显示](https://www.ithome.com/0/616/553.htm)
> 概要: 感谢IT之家网友帆0729的线索投递！IT之家5 月 4 日消息，5 月 4 日一早，不少市民发现山东健康码界面又“升级”了，可同屏显示健康码、核酸检测结果、疫情重点地区核验信息及疫苗接种情况等，比以......
### [动画《风都侦探》新视觉图与主题曲公开！](https://news.dmzj.com/article/74307.html)
> 概要: 根据特摄电视剧《假面骑士W》改编的动画《风都侦探》公开了新视觉图与主题曲，将由吉川晃司作曲、松岗充担任主唱。
### [《面包房模拟器》现于Steam平台发售 支持中文](https://www.3dmgame.com/news/202205/3841655.html)
> 概要: 由Live Motion Games开发的模拟经营游戏《面包房模拟器》现已在Steam平台发售。游戏原价54元，发售首周购买享八五折优惠，只需45.9元，游戏支持中文。在黎明前起床，看看当面包师是什么......
### [新冠小分子口服药销售业绩比拼：默沙东32亿美元，辉瑞15亿美元](https://www.yicai.com/news/101401181.html)
> 概要: 默沙东的molnupiravir销售额暂且领先于辉瑞的Paxlovid。
### [疫情下的中国青年](https://www.yicai.com/image/101401240.html)
> 概要: None
### [央行：坚决落实金融支持稳经济工作措施](https://www.yicai.com/news/101401241.html)
> 概要: 会议认为，要抓紧谋划增量政策工具，支持稳增长、稳就业、稳物价，为统筹疫情防控和经济社会发展营造良好货币金融环境。
### [高喊爱华口号、征战中国互联网，外国网红吸金实录](https://www.tuicool.com/articles/yUZrymq)
> 概要: 高喊爱华口号、征战中国互联网，外国网红吸金实录
### [主板IPO周报：上房服务“临门”撤材料，青蛙泵业上会被否](https://www.yicai.com/news/101401244.html)
> 概要: 四川郎酒终止审核。
### [Clojure/Script mode for CodeMirror 6](https://nextjournal.github.io/clojure-mode/)
> 概要: Clojure/Script mode for CodeMirror 6
### [郭宏超：经济报道也是一种预期管理](https://finance.sina.com.cn/china/2022-05-04/doc-imcwipii8016884.shtml)
> 概要: 原标题：经济报道也是一种预期管理 来源：《中国记者》郭宏超 经济观察报副总编辑、现代广告杂志总编辑 过去一年多时间，在美上市的中概股经历了较大跌幅，一些股价腰斩...
### [门店 98 元起：海澜之家短袖 T 恤 49 元清仓（32 款同价）](https://lapin.ithome.com/html/digi/616576.htm)
> 概要: 【海澜之家官方 outlets 店】断码清仓：海澜之家男士 32 款短袖 T 恤报价 79 元，限时限量 30 元券，实付 49 元包邮，领券并购买。吊牌价 98 元起，至高 5 折优惠。还可凑单叠加......
### [银保监会召开专题会议传达学习贯彻中央政治局会议精神](https://finance.sina.com.cn/china/2022-05-04/doc-imcwipii8006455.shtml)
> 概要: 近日，银保监会召开专题会议，深入传达学习中央政治局会议精神和习近平总书记在中央政治局第三十八次集体学习时的重要讲话精神，研究部署具体贯彻落实措施。
### [又有城市纾困房地产！徐州降低房贷利率和首付比例、放松限售…](https://finance.sina.com.cn/china/2022-05-04/doc-imcwipii8006641.shtml)
> 概要: 又有城市纾困房地产！徐州降低房贷利率和首付比例、放松限售…赣州建行把购房者年龄放宽至70岁！ 原创张达 近日，徐州市住建局等部门召开了房地产行业复工复产及纾困解难工...
### [特斯拉中国第二工厂落地上海，年产能扩大至100万辆？公司尚未回应](https://finance.sina.com.cn/china/2022-05-04/doc-imcwipii8006713.shtml)
> 概要: 传闻不断！特斯拉中国第二工厂落地上海，年产能扩大至100万辆？公司尚未回应 陈霞昌 据路透社5月3日报道，特斯拉计划在上海建造第二座工厂，并把年产能扩大约一倍...
### [半导体企业复工复产进行时：700名员工驻守岗位，4月产销量创新高](https://www.yicai.com/news/101401339.html)
> 概要: 对物流恢复仍有担忧。
### [漫改真人剧《萌系男友是燃燃的橘色》新剧照曝光！](https://news.dmzj.com/article/74308.html)
> 概要: 由玉岛noso原作的同名少女漫改编的真人电视剧《萌系男友是燃燃的橘色》公开了新的剧照画面。
### [曾经刷屏的抖音初代网红都去哪儿了？](https://www.huxiu.com/article/546148.html)
> 概要: 本文来自微信公众号：豹变（ID：baobiannews），作者：杨光，编辑：子睿，原文标题：《刘畊宏霸屏时，那些抖音初代网红都去哪了？》，题图来自：视觉中国连周杰伦都捧不红的刘畊宏在抖音上火了。自从上......
### [Bitwarden: Generate a Username](https://bitwarden.com/help/generator/#generate-a-username)
> 概要: Bitwarden: Generate a Username
### [《赛马娘》系列将推出新作动画！](https://news.dmzj.com/article/74310.html)
> 概要: 《赛马娘》系列将推出新作动画。
### [人大书记校长联名发文：党办的大学让党放心，人民的大学不负人民](https://finance.sina.com.cn/china/2022-05-04/doc-imcwiwst5571751.shtml)
> 概要: 作者：张东刚 刘伟《光明日报》（ 2022年04月29日07版） 五四青年节前夕，习近平总书记来到中国人民大学考察调研并发表重要讲话，充分体现了总书记对中国人民大学建设发展...
### [证监会：稳步推进股票发行注册制改革，推出科创板做市交易](https://www.yicai.com/news/101401381.html)
> 概要: 稳步推进股票发行注册制改革，推出科创板做市交易，丰富期货期权等市场风险管理工具，加快投资端改革步伐，积极引入长期投资者。
### [AMD 新款 RX 6000 系列显卡跑分曝光：RX 6950 XT 超 RTX 3090 Ti](https://www.ithome.com/0/616/583.htm)
> 概要: IT之家5 月 4 日消息，消息称 AMD 新款 RX 6950 XT、RX 6750 XT 和 RX 6650 XT 显卡将在 5 月 10 日推出，但外媒 WccfTech 现在就提前公布了这三款......
### [美欧天然气价格冲高，对我国影响几何？](https://finance.sina.com.cn/china/2022-05-04/doc-imcwiwst5573257.shtml)
> 概要: 北京时间5月3日晚间，美国天然气期货价格再度大涨，一度涨至8.25美元/百万英热，接近4月18日创下的13年高点——8.28美元/百万英热，收盘时报7.9美元/百万英热。
### [《海贼王》烧烧果实狗都不吃？看似是玩梗，实则有一定的道理](https://new.qq.com/omn/20220504/20220504A0ASJ500.html)
> 概要: 海迷们又多了一句口头禅：“”。《海贼王》粉丝们之所以嫌弃烧烧果实，是因为《海贼王》和火焰有关的能力烂大街，路飞觉醒了尼卡果实之后，更是把火焰玩得得心应手，和凯多掀起一场玩火大战。这时候无论是烧烧果实还......
# 小说
### [镇国神殿](http://book.zongheng.com/book/1134789.html)
> 作者：东风乱

> 标签：都市娱乐

> 简介：从军七年杀的敌军不敢再进一步，成就北域之主。一段迟来的录音让他几近癫狂，女儿重病，妻子被处处针对。当他回归都市的那一刻，所有迫害过他家人的，都得为之颤抖！

> 章节末：第673章 其乐融融

> 状态：完本
# 论文
### [SUPA: A Lightweight Diagnostic Simulator for Machine Learning in Particle Physics | Papers With Code](https://paperswithcode.com/paper/supa-a-lightweight-diagnostic-simulator-for)
> 日期：10 Feb 2022

> 标签：None

> 代码：None

> 描述：Deep learning methods have gained popularity in high energy physics for fast modeling of particle showers in detectors. Detailed simulation frameworks such as the gold standard Geant4 are computationally intensive, and current deep generative architectures work on discretized, lower resolution versions of the detailed simulation. The development of models that work at higher spatial resolutions is currently hindered by the complexity of the full simulation data, and by the lack of simpler, more interpretable benchmarks. Our contribution is SUPA, the SUrrogate PArticle propagation simulator, an algorithm and software package for generating data by simulating simplified particle propagation, scattering and shower development in matter. The generation is extremely fast and easy to use compared to Geant4, but still exhibits the key characteristics and challenges of the detailed simulation. We support this claim experimentally by showing that performance of generative models on data from our simulator reflects the performance on a dataset generated with Geant4. The proposed simulator generates thousands of particle showers per second on a desktop machine, a speed up of up to 6 orders of magnitudes over Geant4, and stores detailed geometric information about the shower propagation. SUPA provides much greater flexibility for setting initial conditions and defining multiple benchmarks for the development of models. Moreover, interpreting particle showers as point clouds creates a connection to geometric machine learning and provides challenging and fundamentally new datasets for the field. The code for SUPA is available at https://github.com/itsdaniele/SUPA.
### [A Scaling Law for Synthetic-to-Real Transfer: A Measure of Pre-Training | Papers With Code](https://paperswithcode.com/paper/a-scaling-law-for-synthetic-to-real-transfer)
> 日期：25 Aug 2021

> 标签：None

> 代码：https://github.com/pfnet-research/cg-transfer

> 描述：Synthetic-to-real transfer learning is a framework in which we pre-train models with synthetically generated images and ground-truth annotations for real tasks. Although synthetic images overcome the data scarcity issue, it remains unclear how the fine-tuning performance scales with pre-trained models, especially in terms of pre-training data size.
