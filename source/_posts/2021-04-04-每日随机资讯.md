---
title: 2021-04-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.EggTree_EN-CN1799407063_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-04-04 19:06:43
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.EggTree_EN-CN1799407063_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [组图：高甜名场面！王子文吴永恩上演钢琴吻 相拥对视好似偶像剧](http://slide.ent.sina.com.cn/z/v/slide_4_704_354861.html)
> 概要: 组图：高甜名场面！王子文吴永恩上演钢琴吻 相拥对视好似偶像剧
### [Bertrand Russell Is the Pope (2010)](http://ceadserv1.nku.edu/longa//classes/mat385_resources/docs/russellpope.html)
> 概要: Bertrand Russell Is the Pope (2010)
### [任天堂Switch销量已超过其前辈GBA](https://www.3dmgame.com/news/202104/3811822.html)
> 概要: 据VGChartz估计，任天堂Switch的累计销量已经超过其前辈Game Boy Advance(GBA)。在截至2021年3月20日的一周内，Switch的销量为474488部，使其累计销量达到8......
### [“药神”背后的中国仿制药](https://www.huxiu.com/article/419559.html)
> 概要: 本文来自微信公众号：远川研究所（ID：caijingyanjiu），作者：高翼，原文标题：《歧路：“药神”背后的中国仿制药》作者：高翼 编辑：董指导/李墨天 出品：远川研究所医药组广药集团旗下的白云山......
### [《DayZ》之父新作《伊卡洛斯》首曝演示视频](https://www.3dmgame.com/news/202104/3811824.html)
> 概要: PC Gamer发布了《DayZ》之父Dean Hall新作《伊卡洛斯》的首个演示视频。《伊卡洛斯（Icarus）》是一款开放世界科幻生存游戏，由Dean Hall创建的RocketWerkz工作室开......
### [有种友谊叫“男闺蜜”，看到华晨宇还可以，看到王一博我酸了！](https://new.qq.com/omn/20210331/20210331A0D96A00.html)
> 概要: 有种友谊叫“男闺蜜”，看到华晨宇还可以，看到王一博我酸了！有很多人认为这世界上男女之间没有纯洁的友谊，但也有很多人持相反的态度，比如说“何炅谢娜”。众所周知，谢娜与何炅是娱乐圈里出了名的异性闺蜜，多年......
### [《黑寡妇》电影新中文版预告 7月9日北美上映](https://www.3dmgame.com/news/202104/3811825.html)
> 概要: 漫威影业官微今天凌晨发布了《黑寡妇》预告片，超强回忆杀燃爆来袭。预告片：“多重身份、隐藏任务，黑寡妇成为复仇者联盟成员前从未曝光的经历即将浮出水面迎战最强劲敌，漫威电影宇宙新纪元第一战蓄势待发。电影《......
### [「堀与宫村」漫画作者公开动画完结纪念贺图](http://acg.178.com//202104/411498362317.html)
> 概要: 漫画「堀与宫村」的作者萩原ダイスケ（萩原大辅）公开了其绘制的动画完结纪念贺图。漫画「堀与宫村」讲述了看起来很时尚、却是持家女的堀京子，与看似是宅男、其实满身是刺青和很多耳洞的时尚的宫村伊澄之间的校园恋......
### [enkiTS: A C and C++ Task Scheduler for creating parallel programs](https://github.com/dougbinks/enkiTS)
> 概要: enkiTS: A C and C++ Task Scheduler for creating parallel programs
### [TV动画「入间同学入魔了！」第2季正式PV公开](http://acg.178.com//202104/411501968843.html)
> 概要: TV动画「入间同学入魔了！」第2季公开了正式PV，本作将于4月17日播出。「入间同学入魔了！」第2季正式PV动画「入间同学入魔了！」改编自西修著作的同名漫画作品，讲述了少年铃木入间在成为大恶魔萨利班的......
### [My experience releasing failed SaaS products](https://mmartinfahy.medium.com/my-experience-releasing-3-failed-saas-products-44e61cbde424)
> 概要: My experience releasing failed SaaS products
### [后端 API 接口文档 Swagger 使用指南](https://www.tuicool.com/articles/zQ7ZRfz)
> 概要: 点击上方“匠心零度”，选择“设为星标”做积极的人，而不是积极废人来源： cnblogs.com/wyq178前言一：swagger是什么？二：为什么要使用swaager?2.1:对于后端开发人员来说2......
### [gRPC的平滑关闭和在Kubernetes上的服务摘流方案总结](https://www.tuicool.com/articles/FFFZvym)
> 概要: 平滑关闭和服务摘流是保证部署了多节点的应用能够持续稳定对外提供服务的两个重要手段，平滑关闭保证了应用节点在关闭之前处理完已接收到的请求，以前在文章「学习用Go编写HTTP服务」里给大家介绍过怎么用ne......
### [Apple 2001](https://corecursive.com/063-apple-2001/)
> 概要: Apple 2001
### [假面骑士50周年企划《新·假面骑士》制作确定](https://news.dmzj1.com/article/70533.html)
> 概要: 以女性裤袜描写为特点的原创动画《裤袜视界》推出了新企划《裤袜视界·Another》。
### [怎样惩罚伤了你心的男人最解气？不妨试试这几招](https://new.qq.com/omn/20210401/20210401A0250C00.html)
> 概要: 凡是能够轻易伤害到你的人，都是你非常在乎的人，因为他们最清楚你的软肋在哪里。遇人不淑，是偶然；执迷不悟，则是自找的。男女交往中，难免磕磕绊绊，磨合也时有发生。碰到不解你风情的男人，真是让人七窍生厌。若......
### [《持续狩猎史莱姆三百年，不知不觉就练到LV MAX》第六弹新视觉公开](https://news.dmzj1.com/article/70534.html)
> 概要: 自1971年4月3日开播以来，《假面骑士》迎来了50周年。庵野秀明将担任电影《新·假面骑士》的监督·脚本，2023年3月上映。
### [漫画「为魔女们献上奇迹般的诡术」第三卷封面公开](http://acg.178.com//202104/411506976708.html)
> 概要: 漫画「为魔女们献上奇迹般的诡术」作者渡边静公开了本作第三卷的封面图，该册将于4月16日发售。剧情概述：渴望着掌握真正魔法的男主针井诚，因为一次意外的魔术事故，穿越到了另一个世界，居然邂逅了真正的魔女......
### [TV动画《七骑士》今晚放送PV第1弹](https://news.dmzj1.com/article/70535.html)
> 概要: 游戏改TV动画《七骑士：革命 -英雄的继承者-》PV第1弹公布，今晚放送。
### [在线中文教育公司PPtutor获比特时代数千万元A轮投资](https://www.tuicool.com/articles/NjAZvaY)
> 概要: 阅读时间大约2分钟（542字）1小时前在线中文教育公司PPtutor获比特时代数千万元A轮投资来源：官网截图PPtutor创始人王勃表示：本轮资金将主要用于产品研发、市场拓展、团队建设等方面。【猎云网......
### [成龙曝刘德华为何隐婚](https://new.qq.com/omn/20210401/20210401A053QA00.html)
> 概要: 成龙曾“评价”刘德华隐婚，他说：“抛开艺人的身份，刘德华也只是一个普通人，他结婚的事在圈里是公开的秘密，只是我们不能去讲，所有人都需要守住那个秘密。”            成龙补充说：“刘德华的歌迷......
### [组图：董璇带女儿春游种树 小酒窝认真铲土浇水软萌可爱](http://slide.ent.sina.com.cn/star/w/slide_4_86512_354890.html)
> 概要: 组图：董璇带女儿春游种树 小酒窝认真铲土浇水软萌可爱
### [动画「SSSS.DYNAZENON」角色设计公开感谢绘](http://acg.178.com//202104/411516626891.html)
> 概要: TV动画「SSSS.DYNAZENON」的角色设计兼总作画监督坂本胜公开了播出感谢绘，本作将于每周五进行更新。同时，官方发布了坂本胜所修正的角色原案。TV动画「SSSS.DYNAZENON」是由圆谷P......
### [2021年香港电影大爆发，10部大片投资破亿，古天乐独占6部](https://new.qq.com/omn/20210401/20210401A0965S00.html)
> 概要: 2020年刘德华主演的《拆弹专家2》口碑票房双爆发，给早已式微的香港电影注入了一针强心剂，王晶高呼“港片未死”！现在看来，刘德华掀起的港片小高峰2021年还将持续。今年一大堆港片扎堆上映，成本还都不低......
### [魅族为清明节不当文案道歉，官博暂停运营两天](https://www.ithome.com/0/544/098.htm)
> 概要: IT之家4月4日消息 魅族科技刚刚发布公告，对今日的 “清明节推文”严重用词不当进行道歉，并表示将更加严谨地把控平台内容输出，今日起官方微博暂停运营两天。IT之家了解到，魅族科技今日上午 10:00 ......
### [曾因失误致1500万疫苗报废！美工厂将专注生产强生疫苗](https://finance.sina.com.cn/china/2021-04-04/doc-ikmxzfmk3293999.shtml)
> 概要: 中新网4月4日电 综合外媒报道，在美国一家新冠疫苗制造工厂的员工误将两种新冠疫苗原料混合，导致1500万剂美国强生公司的新冠疫苗被迫销毁后，当地时间3日...
### [时隔60年日本再现猪瘟 已扑杀18万头猪！对我国猪价影响微乎其微](https://finance.sina.com.cn/roll/2021-04-04/doc-ikmyaawa6153538.shtml)
> 概要: 时隔60年日本再现猪瘟，已扑杀18万头猪！对我国猪价影响微乎其微 来源：金十数据 2015年日本被世界动物卫生组织认定为“净化国”，这也成为日本开拓开拓新的猪肉出口市场的一...
### [网红手工耿造了辆电动汽车，网友打趣喊话小米雷军投资](https://www.ithome.com/0/544/107.htm)
> 概要: 昨日晚间，有 “泥石流发明家”之称的网红 “手工耿”在个人微博发布了他的史诗级大作：一辆可以横着走的电动汽车。根据视频，这辆电动汽车是四轮电机独立驱动，轮胎是他在废旧车摊买的摩托车车胎，焊接汽车的骨架......
### [视频：黄翠如挑战配音喉咙痛 比声量不及老公萧正楠](https://video.sina.com.cn/p/ent/2021-04-04/detail-ikmxzfmk3296034.d.html)
> 概要: 视频：黄翠如挑战配音喉咙痛 比声量不及老公萧正楠
### [魅族就清明节“祭奠”文案道歉：用词不当，官博停更两天](https://finance.sina.com.cn/china/2021-04-04/doc-ikmyaawa6155090.shtml)
> 概要: 原标题：魅族就清明节“祭奠”文案道歉：用词不当，官博停更两天@红星新闻 4月4日消息，4月4日是清明节，今日早间，魅族科技发了一条微博文案...
### [3种“新兴行业”崛起，老板大部分是90后、00后，收费高却受欢迎](https://www.tuicool.com/articles/yEvYFbY)
> 概要: 改革开放之后，国内很多行业都迎来了春天，很多人在行业里崭露头角，创办的企业从小做到大，最终成为了标杆，自己也实现了人生的逆袭。经历了几十年的发展，根据社会发展现状，有几大风口行业已经造就了很多富豪，例......
### [视频：邓紫棋带男友和窦骁何超莲聚餐 热聊秀恩爱氛围欢乐](https://video.sina.com.cn/p/ent/2021-04-04/detail-ikmyaawa6155027.d.html)
> 概要: 视频：邓紫棋带男友和窦骁何超莲聚餐 热聊秀恩爱氛围欢乐
### [《破坏领主》发布大型更新 宠物现在可以自动捡钱了](https://www.3dmgame.com/news/202104/3811847.html)
> 概要: ARPG作品《破坏领主》在首发时存在不少bug，不过开发商非常努力的进行了修复，并在本周末再次推出大型更新。《破坏领主》1.1.1.1更新档又名“血风暴”，为通关后的远征部分加入了四个新场景，加入13......
### [视频：闷声干大事！张艺兴黄磊黄渤合开公司 经营范围显野心](https://video.sina.com.cn/p/ent/2021-04-04/detail-ikmxzfmk3298215.d.html)
> 概要: 视频：闷声干大事！张艺兴黄磊黄渤合开公司 经营范围显野心
### [国际空间站又找到三处可能的漏气点，已成功填补](https://www.ithome.com/0/544/110.htm)
> 概要: IT之家4月4日消息 据俄罗斯卫星通讯社消息，俄罗斯火箭航天业人士表示，俄宇航员本月在国际空间站 “星辰”舱段又找到三处可能的空气泄露点，并用密封胶将其填补。2020 年 10 月，宇航员首次在空间站......
### [北京地铁大兴机场全线推出语音购票功能：识别率 98 % 以上，支持普通话、上海话、四川话等](https://www.ithome.com/0/544/111.htm)
> 概要: IT之家4月4日消息 据北京晚报报道，为提高广大乘客的购票效率，2021 年 4 月 1 日起，大兴机场线全线自动售票机均上线语音购票功能，成为北京市首条实现语音购票的轨道交通线路。IT之家获悉，报道......
### [首发预装鸿蒙OS！华为MatePad Pro 2系统界面曝光](https://www.3dmgame.com/news/202104/3811849.html)
> 概要: 前段时间，华为官方已正式宣布将在本月正式推出鸿蒙OS系统正式版，并将率先向旗舰机型推送更新。据相关爆料显示，华为还将在本月发布新一代旗舰平板——华为MatePad Pro 2，该机将成为首款预装鸿蒙O......
### [火爆！景区门口大排“长龙”！酒店满房！连景区文创都卖断货了](https://finance.sina.com.cn/wm/2021-04-04/doc-ikmxzfmk3304338.shtml)
> 概要: 【相关情况】清明旅游太火爆：一亿游客报复性出行！航班量暴增超2倍 人人人从人人人众！重现火爆！景区门口大排“长龙”！酒店满房！
### [沙特上调5月份所有面向亚洲出口的原油价格](https://finance.sina.com.cn/china/2021-04-04/doc-ikmxzfmk3305940.shtml)
> 概要: 沙特阿美的定价信息显示，沙特全面上调5月出口至亚洲的原油价格，轻质原油和超轻质原油价格上调0.4美元/桶，特轻质油价格上调0.2美元/桶，中质原油和重质原油价格上调0...
### [叶檀：炒鞋跟爱国无关  更多的消费股要崛起了](https://finance.sina.com.cn/china/2021-04-04/doc-ikmyaawa6174066.shtml)
> 概要: 【相关阅读】涨疯了！原价1499卖48889！国产鞋遭爆炒，有人几天赚一辆车 文/叶檀☞财经女侠| 毒舌善心 国货潮崛起，国货消费的黄金时代，来了！
### [硬汉男神朱亚文的成名往事，和他爱过的2个重要女人](https://new.qq.com/omn/20210404/20210404A06NPR00.html)
> 概要: 提到这几年，发展最快的男演员，朱亚文算是其中之一。不仅资源好，作品多，而且还成了无数人心中的完美偶像。凭借超强的荷尔蒙气息，朱亚文也成了新一代的硬汉男神代表。            01朱亚文的传统男......
# 小说
### [中国共产党问责工作程序与规范](http://book.zongheng.com/book/705367.html)
> 作者：于建荣，何芹，周翠英

> 标签：评论文集

> 简介：权力就是责任，责任就要担当，忠诚干净担当是党对领导干部提出的政治要求。本书紧扣《中国共产党问责条例》的精神主旨，围绕问责意义、问责要求、问责原则、问责主体、问责对象、问责内容、问责情形、问责方式、问责执行等问题进行了解读，以帮助广大党员及党员领导干部准确把握《问责条例》的要义和要求，更好地掌握问责工作程序与规范。

> 章节末：第七章 问责的执行_六、实行终身问责

> 状态：完本
# 论文
### [Incorporating User's Preference into Attributed Graph Clustering](https://paperswithcode.com/paper/incorporating-user-s-preference-into)
> 日期：24 Mar 2020

> 标签：GRAPH CLUSTERING

> 代码：https://github.com/yeweiysh/LOCLU

> 描述：Graph clustering has been studied extensively on both plain graphs and attributed graphs. However, all these methods need to partition the whole graph to find cluster structures.
### [Predicting Length of Stay in the Intensive Care Unit with Temporal Pointwise Convolutional Networks](https://paperswithcode.com/paper/predicting-length-of-stay-in-the-intensive)
> 日期：29 Jun 2020

> 标签：LENGTH-OF-STAY PREDICTION

> 代码：https://github.com/EmmaRocheteau/eICU-LoS-prediction

> 描述：The pressure of ever-increasing patient demand and budget restrictions make hospital bed management a daily challenge for clinical staff. Most critical is the efficient allocation of resource-heavy Intensive Care Unit (ICU) beds to the patients who need life support.
