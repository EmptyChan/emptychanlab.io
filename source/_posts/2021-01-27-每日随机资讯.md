---
title: 2021-01-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Taormina_EN-CN8042812380_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-27 21:54:08
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Taormina_EN-CN8042812380_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [36氪首发 | 10个月内完成超1.3亿美元融资，AGILE ROBOTS思灵机器人宣布完成B轮融资](https://www.tuicool.com/articles/7n6FRzV)
> 概要: 36氪获悉，近日，人工智能机器人公司思灵机器人（AGILE ROBOTS）宣布完成B轮融资，主要投资方包括全球前三大的手机及3C类产品制造商、交银国际、招银国际招银电信基金、新希望集团，同时还获得原有......
### [组图：艾略特·佩吉与妻子离婚 几月前宣布自己是跨性别者](http://slide.ent.sina.com.cn/star/h/slide_4_704_351776.html)
> 概要: 组图：艾略特·佩吉与妻子离婚 几月前宣布自己是跨性别者
### [组图：徐梦洁带大耳狗毛绒帽全场最萌 裸粉西装显温柔气质](http://slide.ent.sina.com.cn/star/slide_4_704_351782.html)
> 概要: 组图：徐梦洁带大耳狗毛绒帽全场最萌 裸粉西装显温柔气质
### [视频：网友慕了！周润发夫妇相处模式曝光 主动认错接济朋友宠上天](https://video.sina.com.cn/p/ent/2021-01-27/detail-ikftssap1222556.d.html)
> 概要: 视频：网友慕了！周润发夫妇相处模式曝光 主动认错接济朋友宠上天
### [迪士尼《寻龙传说》全新预告公布 高燃冒险一触即发](https://www.3dmgame.com/news/202101/3807192.html)
> 概要: 目前，迪士尼官方公开了动画电影《寻龙传说》全新预告片，此次展示了诸多画面。世界分崩离析，传说的神龙首次现身，史上最飒女战士拉雅发出召唤，集结“萌”友，高燃冒险一触即发。迪士尼动画电影《寻龙传说》全新预......
### [最喜欢的宝可梦电影是？《宝可梦》新作官方进行人气投票](https://news.dmzj1.com/article/69990.html)
> 概要: 剧场版动画《宝可梦 可可》为了纪念火热上映，推出了“大家来决定！宝可梦电影系列No.1决定战！”的活动。
### [CircuitPython: Programming Hardware in Python](https://github.com/adafruit/circuitpython)
> 概要: CircuitPython: Programming Hardware in Python
### [Xbox Series的捕获和分享功能将在未来得到改进](https://www.3dmgame.com/news/202101/3807194.html)
> 概要: 次世代主机Xbox Series X/S已经发售数月，它相比于Xbox One系列有着显著提升。无论是更强大的性能还是读取更快的硬盘，无疑都提升了玩家的游戏体验，而其快速恢复功能则更是锦上添花。不过，......
### [科比逝世一周年 贝克汉姆晒科比和吉安娜合影悼念](https://ent.sina.com.cn/s/u/2021-01-27/doc-ikftpnny2221136.shtml)
> 概要: 新浪娱乐讯 2021年1月26日是美国篮球巨星科比逝世1周年的日子。英国足球运动员贝克汉姆也在当地的1月26日（北京时间1月27日）发文悼念科比，他放上了科比和女儿吉安娜的幸福合照，照片中吉安娜坐在科......
### [视频：《风起霓裳》开播曝主题曲MV 娜扎许魏洲演绎大唐情缘](https://video.sina.com.cn/p/ent/2021-01-27/detail-ikftssap1247929.d.html)
> 概要: 视频：《风起霓裳》开播曝主题曲MV 娜扎许魏洲演绎大唐情缘
### [漫画《咒术回战》因使用“神风”在韩国大炎上！](https://news.dmzj1.com/article/69991.html)
> 概要: 有韩国媒体报道了在漫画《咒术回战》中，有角色使用名为“神风”的技能，而且技能效果是让鸟进行自杀式袭击。这样的设定让很多韩国读者感到了不满。
### [声优·齐藤朱夏首本写真日历即将发售](http://acg.178.com//202101/405725427111.html)
> 概要: 声优·齐藤朱夏的首本写真日历「斉藤朱夏 CALENDAR 2021.04-2022.03」将于2021年3月26日正式发售，售价3000日元（不含税）......
### [「足球小将」大空翼手办登场](http://acg.178.com//202101/405725679386.html)
> 概要: 人气足球漫画「足球小将」中的男主角「大空翼」以小学生篇中的姿态登场，作品由GOOD SMILE COMPANY负责生产，采用ABS、PVC材质，全高约170mm，售价人民币239元，预计将于2021年......
### [高玩羊毛毡打造高达经典机体 亲和度提升别有特色](https://www.3dmgame.com/news/202101/3807216.html)
> 概要: 近日，一位日本动手高玩运用羊毛毡材料打造了《高达》经典机体吉恩号引发网友热赞，秉承羊毛毡的材质特性，毛茸茸的吉恩号明显少了冰冷机械感，亲和度大幅提升。·在如今众多的二次元手工爱好者中，@takebon......
### [虽然看不了但还可以吃！EVA×ViTO在2月推出影院冰激凌](https://news.dmzj1.com/article/69996.html)
> 概要: 人气动画《EVA》系列宣布了将和ViTO日本联动，于2月1日在TJOY系列的日本全国19家影院的饮食区发售“EVA原创ViTO意式冰激凌”。
### [迪士尼英国官网显示《黑寡妇》片长2小时13分钟](https://www.3dmgame.com/news/202101/3807221.html)
> 概要: 《黑寡妇》独立电影目前定于2021年5月7日正式在北美上映，近日，迪士尼（英国）官方网站上显示《黑寡妇》片长2小时13分钟，是MCU影片中第9长的电影。《黑寡妇》一部充满动作戏的间谍惊悚片。娜塔莎·罗......
### [漫画「大罪龙最讨厌了！」第1卷发售纪念PV公开](http://acg.178.com//202101/405733064304.html)
> 概要: 漫画「大罪龙最讨厌了！」公开第1卷发售纪念PV。「大罪龙最讨厌了！」第1卷发售纪念PV「大罪龙最讨厌了！」是兔月あい创作的四格漫画作品，目前正在连载中，作品讲述了犯下滔天大罪的龙少女与被毁灭了家园的少......
### [别轻易把性生活交给互联网](https://www.huxiu.com/article/406880.html)
> 概要: 出品 | 虎嗅年轻组作者 | 渣渣郡题图来自《 Parks and Recreation 》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、......
### [2021 盖茨年信：这一年，全球健康与你我休戚与共](https://www.ithome.com/0/532/455.htm)
> 概要: IT之家 1 月 27 日消息 今日，盖茨基金会公布了署名为 “比尔和梅琳达”的 2021 盖茨年信《这一年，全球健康与你我休戚与共》。▲ 图源：盖茨基金会IT之家了解到，信中指出，在新冠肆虐的当下，......
### [彭博社：字节跳动去年营收翻倍，正寻求部分业务在港上市，包括抖音等，估值 1800 亿美元](https://www.ithome.com/0/532/458.htm)
> 概要: 1 月 27 日下午消息，据彭博社报道，知情人士透露，尽管遭受禁令，抖音母公司字节跳动有限公司（ByteDance ）去年的营收翻了一番多，达到约 350 亿美元。该知情人士称，字节跳动 2020 年......
### [IKEA Buys 11,000 Acres of U.S. Forest to Keep It from Being Developed](https://www.goodnewsnetwork.org/ikea-buys-forest-in-georgia/?)
> 概要: IKEA Buys 11,000 Acres of U.S. Forest to Keep It from Being Developed
### [任正非再次钦点余承东，背后意图是什么？](https://www.huxiu.com/article/406909.html)
> 概要: 出品｜虎嗅科技组作者｜张雪头图｜视觉中国断供余震下，华为内部的调整与变动还在继续。1月27日，华为正式发布任命文件，余承东，现任消费者BG CEO，本次拟增任命Cloud &AI BG总裁（兼）、Cl......
### [落地实践——Service Mesh下的微服务落地实践 | Techo大会精彩回顾第五期](https://www.tuicool.com/articles/2A3emun)
> 概要: 全文共3637字，阅读需要7分钟导读刘冠军《万象伊始——集中式架构为何演进到微服务架构》秦金卫《转型求通——微服务架构的最佳实践和发展趋势》曹国梁《深度剖析——传统架构的云原生改造之路》万俊峰《转型之......
### [3DM轻松一刻第450期 这黑丝腿和猫娘真是好看啊](https://www.3dmgame.com/bagua/4241.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！不太灵活的肥喵哈欠是会传染的真有这么丝滑吗？绝对是猫......
### [刘鹤：力争到年底完成三年国有企业改革任务的70%以上](https://finance.sina.com.cn/china/gncj/2021-01-27/doc-ikftpnny2350441.shtml)
> 概要: 刘鹤主持召开国务院国有企业改革领导小组第五次会议 强调国企改革要把握好“一个抓手、四个切口” 1月27日，中共中央政治局委员、国务院副总理...
### [内蒙古霍林河煤业集团原董事长何江超严重违纪违法被双开](https://finance.sina.com.cn/china/dfjj/2021-01-27/doc-ikftpnny2350388.shtml)
> 概要: 来源：内蒙古纪委监委网站 内蒙古霍林河煤业集团有限责任公司原董事长何江超严重违纪违法被开除党籍和公职 日前，经通辽市委批准通辽市纪委监委指定...
### [她竟然被后辈diss长相了！](https://new.qq.com/omn/20210127/20210127A0FIYX00.html)
> 概要: 大家晚上好，不知不觉这周的工作日进度已过半啦！            疲惫周三来带大家吃吃瓜，今天又发生了什么事儿呢——THE BOYZ贤在被韩国网友扒出，曾经对Girl‘s Day惠利发表过不太好的......
### [昆凌体能挑战秀身材，周杰伦贴心跟随护妻，儿女大喊妈妈加油](https://new.qq.com/omn/20210127/20210127A0FM8R00.html)
> 概要: 1月27日晚昆凌社交账号分享四段小视频，从小看极限体能王的昆凌今天和家人一起体验一下，昆凌信心满满，霸气喊话自己要闯关成功。            视频中昆凌高扎马尾，穿着黑色背心配紧身裤，好身材一览......
### [张亮寇静复婚？同乘一班飞机回家，离婚三年互动如常，被指假离婚](https://new.qq.com/omn/20210127/20210127A0FRA300.html)
> 概要: 1月27号，有八卦媒体拍到张亮和前妻寇静搭乘同一班飞机回到北京，到达机场后两人分开走，寇静带着天天和女儿先出机场乘车回家，随后过了半个小时后张亮也从机场出来。                     ......
### [银保监会：切实做好金融反腐和处置金融风险统筹衔接](https://finance.sina.com.cn/china/bwdt/2021-01-27/doc-ikftssap1363960.shtml)
> 概要: 银保监会党委召开全系统2021年全面从严治党和党风廉政建设工作会议 2021年1月26日，银保监会党委召开全系统2021年全面从严治党和党风廉政建设工作会议...
### [进返京政策有变：持核酸检阴性结果到目的地后不用隔离](https://finance.sina.com.cn/china/dfjj/2021-01-27/doc-ikftpnny2349950.shtml)
> 概要: 进返京政策有变！持核酸检阴性结果到目的地后不用隔离！还有这些重磅政策和福利 来源：21财闻汇 今天（1月27日），北京、陕西、河北等多地召开新闻发布会...
### [39岁王宝强寸头造型曝光，穿紧身衣肚腩凸起，幸福肥太明显了啊](https://new.qq.com/omn/20210127/20210127A0FTWE00.html)
> 概要: 近日，王宝强工作室晒出了一段王宝强健身的视频。视频中，王宝强的头发剃得很短了，就像是光头强似的，看上去十分精干，同时还有点搞笑。并且王宝强看上去还挺高兴的，还露出了笑容，感觉记忆中的王宝强又回来了。 ......
### [应该打破偏见吗？](https://new.qq.com/omn/20210127/20210127A0FVE200.html)
> 概要: 全新解锁更多好玩的版块~~~            先看看这几组照片                                                当他们换一身衣服后，变成了这样   ......
### [民航局：高标准做好新冠病毒疫苗航空运输保障工作](https://finance.sina.com.cn/china/bwdt/2021-01-27/doc-ikftpnny2350195.shtml)
> 概要: 1月27日，民航局召开促进航空物流业发展工作领导小组第五次会议，贯彻落实2021年全国民航工作会议精神，研讨航空物流“十四五”规划编制工作，并部署下一阶段重点任务。
### [刘鹤：国企改革要把握好“一个抓手、四个切口”](https://finance.sina.com.cn/china/2021-01-27/doc-ikftpnny2350980.shtml)
> 概要: 原标题：刘鹤主持召开国务院国有企业改革领导小组会议 1月27日，中共中央政治局委员、国务院副总理、国务院国有企业改革领导小组组长刘鹤主持国务院国有企业改革领导小组第...
# 小说
### [第9层台阶](http://book.zongheng.com/book/833211.html)
> 作者：鱼先生

> 标签：悬疑灵异

> 简介：这个世界上真的有因果循环吗？这个世界上真的有鬼神存在吗？你可以说没有，因为眼见为实你也可以说有，毕竟因果鬼怪之说自古有之那么，下面我来给大家讲一个故事......

> 章节末：第十四章 完结

> 状态：完本
# 论文
### [Cloze Test Helps: Effective Video Anomaly Detection via Learning to Complete Video Events](https://paperswithcode.com/paper/cloze-test-helps-effective-video-anomaly)
> 日期：27 Aug 2020

> 标签：ANOMALY DETECTION

> 代码：https://github.com/yuguangnudt/VEC_VAD

> 描述：As a vital topic in media content interpretation, video anomaly detection (VAD) has made fruitful progress via deep neural network (DNN). However, existing methods usually follow a reconstruction or frame prediction routine.
### [Transforming and Projecting Images into Class-conditional Generative Networks](https://paperswithcode.com/paper/transforming-and-projecting-images-into-class)
> 日期：4 May 2020

> 标签：None

> 代码：https://github.com/minyoungg/GAN-Transform-and-Project

> 描述：We present a method for projecting an input image into the space of a class-conditional generative neural network. We propose a method that optimizes for transformation to counteract the model biases in a generative neural networks.
