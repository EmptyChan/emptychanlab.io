---
title: 2023-03-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BarnOwlWinter_ZH-CN5484796826_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-19 20:28:14
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BarnOwlWinter_ZH-CN5484796826_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [从苦情戏直播到“免费”评书机，为什么骗子专盯老年人？](https://www.woshipm.com/it/5784782.html)
> 概要: 在今年3·15晚会过后，有关银发人群被欺骗消费的问题又进一步摆上了台面，比如“苦情戏直播”就通过赚取银发人群的同情心来实现盈利。那么在生活中，还有哪些针对银发人群的骗局需要被警惕？我们要如何助推减少银......
### [新老商家在淘宝天猫的远虑与近忧](https://www.woshipm.com/it/5784593.html)
> 概要: 对于电商平台而言，消费者和商家都是“水能载舟，亦能覆舟”。淘宝天猫经历了辉煌时期，到现在优势逐渐被磨平，商家和消费者的反馈是怎样的呢？本文作者对此进行了采访分析，一起来看看吧。打江山易守江山难，这句话......
### [从框架来看，积分体系都有什么？](https://www.woshipm.com/operate/5784613.html)
> 概要: 现在不少产品里都会有相应的积分体系，以对用户形成激励的作用，同时结合积分体系，我们也可以更好地了解用户需求与用户行为。那么你知道积分体系如何搭建吗？什么样的产品，更适合搭建积分体系呢？本篇文章里，作者......
### [亲密的朋友](https://www.zcool.com.cn/work/ZNjQ1MDg1NjA=.html)
> 概要: 心相印无畏心世界：《亲密的朋友》以心世界为核心创作，描绘一处不一样的自然秘境。高山雪岭，原始森林，美丽山神与小精灵聚族而居。我可以趴在游翔的飞鱼身上小憩，可以和灌木丛里觅食的麋鹿问好，看闪耀的玫瑰，偶......
### [湘年有味 - 湖南城市文化插画](https://www.zcool.com.cn/work/ZNjQ1MDg3MDg=.html)
> 概要: 湖南自古是热情豪爽的土地。“淳朴”，即敦厚雄浑、未加修饰、不受拘束的生猛活脱之性。“重义”，即强烈的正义感和向群性。二者融贯，构成了独特的湖湘文化。插画“湘年有味”是根据湖南逢年过节时的特色民俗所绘制......
### [索尼在CMA声明中承认微软XGP领先于PS Plus](https://www.3dmgame.com/news/202303/3865115.html)
> 概要: 在给英国竞争和市场管理局的一份声明中，索尼表示，与PlayStation Plus相比，微软的Xbox Game Pass服务领先于索尼。索尼在声明中承认，“毫无疑问，Game Pass的市场份额远远......
### [AMD将锐龙7040HS系列移动CPU推迟到4月上市](https://www.3dmgame.com/news/202303/3865117.html)
> 概要: 年初的CES展会上，AMD推出了针对笔记本平台的锐龙7000系列处理器，总计有4个版本，其中锐龙7045HX系列是面向高端游戏本的，锐龙7040HS系列升级4nm工艺，主打轻薄本，也是主流用户最期待的......
### [躲生活的大厂打工人，挤满了跳海](https://www.huxiu.com/article/841437.html)
> 概要: 出品｜虎嗅商业消费组作者｜昭晰编辑｜苗正卿题图｜《午夜巴黎》剧照春天，北三跳（跳海酒馆北京三店）的自动贩卖机上布满客人留下的涂鸦，还有人在玻璃门上印满唇印，鼎盛得像王尔德在巴黎拉雪兹公墓的墓地。夜晚，......
### [风投又一大窟窿：老虎消失1500亿](https://36kr.com/p/2176690850328840)
> 概要: 风投又一大窟窿：老虎消失1500亿-36氪
### [开出租遇鬼一拉一个准，阴阳眼小哥X失忆女鬼](https://news.idmzj.com/article/77498.html)
> 概要: 2023年3月新韩剧《Delivery Man》中文版翻译《鬼怪出租车》，奇幻悬疑向题材，主打就是治愈+恋爱+陪伴！由尹灿荣和方敏雅主演，共12集，讲述的是开出租车维持生计的徐英民（男主）自从看见黏在车上的失忆灵魂姜智贤（女主 ）后，他的出租车成...
### [《暗黑破坏神4》公测版屠夫随机出现制裁头铁玩家](https://www.3dmgame.com/news/202303/3865121.html)
> 概要: 《暗黑破坏神4》已于本周末开启公测。进入地下城的玩家发现，自己在里面并不是带几瓶药水就可以肆无忌惮的。在探索避难所深处的时候，有玩家会偶然遭遇一个人气很高的地狱仆从——屠夫。这个反派是该系列的主要角色......
### [网友深夜医院偶遇严浩翔 穿黑色外套排队等待](https://ent.sina.com.cn/2023-03-19/doc-imymkiti5566448.shtml)
> 概要: 新浪娱乐讯 19日凌晨，有网友分享出在医院偶遇严浩翔的照片，同时透露严浩翔有发烧症状。照片中的严浩翔身穿黑色外套站在走廊排队等待......
### [网传曹曦月带货3月只卖278元 工作室发文否认](https://ent.sina.com.cn/s/m/2023-03-19/doc-imymkiti5567945.shtml)
> 概要: 新浪娱乐讯 3月18日，曹曦月工作室发文，否认了网传的带货3个月只卖了278元的艺人为曹曦月，并表示不传谣不信谣，拿证据说话......
### [人在职场，渴望双休](https://www.huxiu.com/article/844802.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：王敏、梁丽爽、邹帅、李秋涵、王璐、唐亚华，编辑：邹帅，头图来自：视觉中国“996是福报”之后，“周六是奋斗者的工作日”又一次走上风......
### [被大厂外派出海，会像是「带薪旅游」吗？](https://36kr.com/p/2177817898840325)
> 概要: 被大厂外派出海，会像是「带薪旅游」吗？-36氪
### [中国公司全球化周报｜“杰成新能源”获数亿元B轮融资；湖南出台首个跨境电商专项政策](https://36kr.com/p/2177084698194182)
> 概要: 中国公司全球化周报｜“杰成新能源”获数亿元B轮融资；湖南出台首个跨境电商专项政策-36氪
### [《宁安如梦》制片人发文：没人有时间去河北造谣](https://ent.sina.com.cn/v/m/2023-03-19/doc-imymkpza3762910.shtml)
> 概要: 新浪娱乐讯 近日，疑似《宁安如梦》后期在网上发文引发热议，对此，《宁安如梦》制片人发文：“主创和后期的老师都在苦干，没有时间跑河北去造谣。”......
### [中金公司投行部负责人：中国特色估值体系核心是全面多元看待公司价值](https://www.yicai.com/video/101705598.html)
> 概要: 围绕这些问题，记者在亚布力年会现场对话了中金公司投资银行部执行负责人许佳，他表示，中国特色估值体系是为了更好发挥市场资源配置的作用，核心是对一个公司的看法要更加全面和多元。
### [经典街机游戏中，那些我们一辈子都无法忘记的场景](https://www.ithome.com/0/680/712.htm)
> 概要: 对于大部分资深的玩家来说，随便截取一张游戏中的背景图，都能够直接说出游戏的名字，甚至是第几关。当年的游戏都是非常耐玩的，一款街机游戏我们可以重复玩一百次都不会腻，而且每次都还会抢着投币，抢着选人。不像......
### [海口通报“导游因游客未购物恶劣态度对待游客”](https://www.yicai.com/news/101705616.html)
> 概要: 一则“导游因游客未购物恶劣态度对待游客”视频在相关媒体传播并引发关注，海口市旅游和文化广电体育局联合海口市综合行政执法局进行立案调查。
### [北欧百年高奢家电品牌ASKO正式发布智能炙烤箱新品！](http://www.investorscn.com/2023/03/19/106312/)
> 概要: 随着生活品质的提高,人们对饮食多样性的需求日渐提高,特别对于高净值人群而言,于舒适奢居中享受至美佳肴,不仅是一种味蕾上的满足,更是一种精致生活态度的折射。对于厨房家电的挑选,集创新科技、美学设计和智慧......
### [侯云春：做足功课，迎接中国经济的新机遇](http://www.investorscn.com/2023/03/19/106313/)
> 概要: 2023年3月15日，由投资家网、深圳市国际投融资商会主办的“第十一届中国股权投资年度峰会”在深圳福田香格里拉大酒店隆重召开。本次峰会以“万物资生，福启未来”为主题，深度剖析股权投资全新业态，并广邀1......
### [Haven工作室首款PlayStation多人游戏即将进入制作阶段](https://www.3dmgame.com/news/202303/3865140.html)
> 概要: 根据加拿大时事杂志《麦克莱恩斯》的报道透露，著名女性游戏制作人Jade Raymond的团队Haven Studios正在开发一款PlayStation平台多人游戏，且即将从预制阶段进入正式制作阶段......
### [860亿，2023年最大规模私有化来了](https://36kr.com/p/2177797604782336)
> 概要: 860亿，2023年最大规模私有化来了-36氪
### [极星 4 纯电中型 SUV 将于上海车展首发：溜背造型设计，最快年内交付](https://www.ithome.com/0/680/729.htm)
> 概要: IT之家3 月 19 日消息，近日在极星 3 国内亮相活动中，官方宣布了极星 4 的发布计划，这款全新纯电中型 SUV 将于 4 月 18 日在上海车展正式亮相。极星 4 采用溜背造型设计，预计将与极......
### [传统豪华品牌的电动车，真的是“杂牌”吗？](https://www.huxiu.com/article/847362.html)
> 概要: 出品丨虎嗅汽车组作者丨周到头图丨奥迪曾几何时，在中国作为一个人身份和地位象征的BBA，如今正遭遇其入华以来最大的滑铁卢。原因无他，这便是德系豪华品牌打造的电动车，在中国销量实在太惨。以宝马为例，该品牌......
### [国产版GPT接踵而来，算力平台谁来支撑？](https://www.yicai.com/news/101705727.html)
> 概要: 在国产版GPT接踵而至的背景下，目前国内的算力平台还不足以提供支撑，这也成为制约我国大模型发展的重要瓶颈。
### [《中国睡眠研究报告2023》：去年我国居民睡眠时长8小时以上占比近一半](https://finance.sina.com.cn/china/2023-03-19/doc-imymkyqy0489138.shtml)
> 概要: 2022年，我国居民睡眠水平较上一年有所改善，睡眠时长增加，超过8小时比例达到47.5%，接近半数，远高于2021年的35.53%。
### [TV动画《国王排名 勇气的宝箱》公布正式PV](https://news.idmzj.com/article/77499.html)
> 概要: 根据十日草辅原作改编的TV动画《国王排名 勇气的宝箱》将于4月13日迎来首播，官方公布了正式预告。
### [朱民谈硅谷银行破产：资产错配叠加内部管理不当，金融市场逻辑正发生根本变化](https://finance.sina.com.cn/jjxw/2023-03-19/doc-imymkyqw3714108.shtml)
> 概要: 转自：券中社 证券时报券中社讯，3月18日，中国国际经济交流中心副理事长、国际货币基金组织前副总裁朱民在《2023全球经济信心指数》报告发布现场谈到：“硅谷银行事件是非...
### [《生化危机4：重制版》箱子上的黄色记号合理吗？](https://www.3dmgame.com/news/202303/3865145.html)
> 概要: 《生化危机4：重制版》与原版一样，里面有很多可供玩家破坏的板条箱。但重制版里的板条箱其实是有点不一样的。玩过体验版的玩家就能看出来，开发人员把游戏里可供破坏的板条箱上画了个大大的黄色X号标记，玩家一看......
### [TV动画《物理魔法使马修》公布正式PV](https://news.idmzj.com/article/77500.html)
> 概要: 根据甲本一原作改编的TV动画《物理魔法使马修》将于4月7日首播，官方公布了第2弹视觉图和正式预告。
### [愿原力与你同在，小米将推《星球大战》联名款 TWS 真无线蓝牙耳机](https://www.ithome.com/0/680/736.htm)
> 概要: IT之家3 月 19 日消息，小米宣布将于明日公布一款《星球大战》联名耳机，目前尚不清楚其他信息。愿原力与你同在，明早 10 点，敬请期待。从海报可以看到，这是一款入耳式 TWS 耳机。IT之家查询小......
### [美团还是没披露骑手社保缴纳的事，是有多难？](https://www.huxiu.com/article/823387.html)
> 概要: 出品｜虎嗅ESG组作者｜张小予头图｜视觉中国本文是#ESG进步观察#系列第018篇文章本次观察关键词：劳工权益、企业社会责任最近，美团发布了《2022年美团骑手权益保障社会责任报告》（以下简称《报告》......
### [李迅雷：扩消费仍需突破障碍，可从三方面发力](https://www.yicai.com/news/101705752.html)
> 概要: 李迅雷判断全年消费增速大概在6%到7%。
### [北京市金融监管局局长李文红：持续扩大金融业对外开放，打击各类非法金融活动](https://finance.sina.com.cn/jjxw/2023-03-19/doc-imymmeww0411693.shtml)
> 概要: 北京商报讯（记者廖蒙）3月19日，北京市地方金融监督管理局党组书记、局长李文红在全球财富管理论坛上指出，北京将持续完善资本市场体系，支持北交所不断扩大交易规模...
### [3月19日这些公告有看头](https://www.yicai.com/news/101705763.html)
> 概要: 3月19日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [广州、深圳放出大招！](https://finance.sina.com.cn/wm/2023-03-19/doc-imymmeww0422555.shtml)
> 概要: 转自：中国证券报 记者最新了解到，目前深圳正在制定《深圳市重点产业链供应链保障工作五年行动计划（2023-2027年）》。 此外，日前广州启动首届百家新锐企业培优计划...
### [《银魂》衍生作《3年Z班银八老师》动画化决定](https://news.idmzj.com/article/77501.html)
> 概要: 在今天举办的《银魂》特别活动“銀魂後祭り2023”上，官方宣布了衍生作品《3年Z班银八老师》将制作动画的消息。
### [顺丰前2月业务量双位数增长，大幅领先行业，运营底盘强化](http://www.investorscn.com/2023/03/19/106314/)
> 概要: 3月19日下午,顺丰控股披露《2023年2月快递物流业务经营简报》。顺丰成立30周年之际,顺丰核心主业展现良好增长态势,综合物流服务优势不断巩固。公告显示,顺丰2月速运物流业务收入135.01亿元,同......
### [手机的天线去哪了](https://www.ithome.com/0/680/754.htm)
> 概要: Q1、既然已经有千米（公里）这样的国际单位，为何还要定义英里、海里这样的单位？并且长度还不一样？by 帝阍答：因为英里、海里等单位是在国际单位制之前便被定义使用的，这么多年下来自然会被约定俗成的继续使......
### [海南：从铁路港口离岛也能提免税品了](https://finance.sina.com.cn/jjxw/2023-03-19/doc-imymmewu3676797.shtml)
> 概要: 离岛免税新政实施以来，消费者对离岛免税购物的热情只增不减，为了满足消费者从铁路轮渡海口南港离岛也能提免税品的需求，铁路轮渡海口南港离岛免税提货点即将开业。
# 小说
### [都市之美妙人生](https://book.zongheng.com/book/1089784.html)
> 作者：我家小老弟

> 标签：都市娱乐

> 简介：男主角得到了一枚改变自己命运的戒指，从此走上一条不要脸的人生道路！凭借自己不要脸的优点，从此大杀四方，战无不胜，纵横都市，玩转人生！

> 章节末：第一百九十五章  （大结局）

> 状态：完本
# 论文
### [There is No Big Brother or Small Brother: Knowledge Infusion in Language Models for Link Prediction and Question Answering | Papers With Code](https://paperswithcode.com/paper/there-is-no-big-brother-or-small-brother)
> 日期：10 Jan 2023

> 标签：None

> 代码：https://github.com/ankush9812/knowledge-infusion-in-lm-for-qa

> 描述：The integration of knowledge graphs with deep learning is thriving in improving the performance of various natural language processing (NLP) tasks. In this paper, we focus on knowledge-infused link prediction and question answering using language models, T5, and BLOOM across three domains: Aviation, Movie, and Web. In this context, we infuse knowledge in large and small language models and study their performance, and find the performance to be similar. For the link prediction task on the Aviation Knowledge Graph, we obtain a 0.2 hits@1 score using T5-small, T5-base, T5-large, and BLOOM. Using template-based scripts, we create a set of 1 million synthetic factoid QA pairs in the aviation domain from National Transportation Safety Board (NTSB) reports. On our curated QA pairs, the three models of T5 achieve a 0.7 hits@1 score. We validate out findings with the paired student t-test and Cohen's kappa scores. For link prediction on Aviation Knowledge Graph using T5-small and T5-large, we obtain a Cohen's kappa score of 0.76, showing substantial agreement between the models. Thus, we infer that small language models perform similar to large language models with the infusion of knowledge.
### [Neural Time Series Analysis with Fourier Transform: A Survey | Papers With Code](https://paperswithcode.com/paper/neural-time-series-analysis-with-fourier)
> 日期：4 Feb 2023

> 标签：None

> 代码：https://github.com/bit-yi/time_series_frequency

> 描述：Recently, Fourier transform has been widely introduced into deep neural networks to further advance the state-of-the-art regarding both accuracy and efficiency of time series analysis. The advantages of the Fourier transform for time series analysis, such as efficiency and global view, have been rapidly explored and exploited, exhibiting a promising deep learning paradigm for time series analysis. However, although increasing attention has been attracted and research is flourishing in this emerging area, there lacks a systematic review of the variety of existing studies in the area. To this end, in this paper, we provide a comprehensive review of studies on neural time series analysis with Fourier transform. We aim to systematically investigate and summarize the latest research progress. Accordingly, we propose a novel taxonomy to categorize existing neural time series analysis methods from four perspectives, including characteristics, usage paradigms, network design, and applications. We also share some new research directions in this vibrant area.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
