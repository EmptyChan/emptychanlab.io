---
title: 2020-09-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LaragangaMoth_EN-CN7119930459_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-09-30 23:27:54
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LaragangaMoth_EN-CN7119930459_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：演员韩承羽探班成毅 两年合作四部剧兄弟情谊颇深](http://slide.ent.sina.com.cn/star/slide_4_704_346000.html)
> 概要: 组图：演员韩承羽探班成毅 两年合作四部剧兄弟情谊颇深
### [第57届金马奖公布提名 林家栋桂纶镁等入围影帝后](https://ent.sina.com.cn/m/c/2020-09-30/doc-iivhvpwy9783120.shtml)
> 概要: 新浪娱乐讯 第57届金马奖公布入围名单，陈玉勋《消失的情人节》获11项提名，黄信尧《同学麦娜丝》10项提名，林家栋、桂纶镁等入围最佳男、女主角。奖项将于11月21日揭晓。　　　　《日子》　　《消失的情......
### [张馨予为被丈夫推下悬崖孕妇发声：女孩 祝福你](https://ent.sina.com.cn/s/m/2020-09-30/doc-iivhuipp7286085.shtml)
> 概要: 新浪娱乐讯 9月30日早上，张馨予发文称， “一个女孩被折磨的遍体鳞伤，却还是相信这个世界绝大多数都是好人，还是珍惜这个世界的美好，还是愿意去相信爱情，相信未来，还是愿意等待那个真命天子找到自己”，她......
### [视频：曝黑豹妻子已怀孕 知情人爆料称两人一直想要孩子](https://video.sina.com.cn/p/ent/2020-09-30/detail-iivhuipp7315541.d.html)
> 概要: 视频：曝黑豹妻子已怀孕 知情人爆料称两人一直想要孩子
### [周杰伦接受采访害怕被提问：不会问照片的事情吧?](https://ent.sina.com.cn/y/ygangtai/2020-09-30/doc-iivhvpwy9672470.shtml)
> 概要: 新浪娱乐讯 9月28日，周杰伦出席活动，当主持人提到接下来有三个问题想问他时，周杰伦立马打断他的话，询问道：“不会问照片的事情吧？”　　此前，昆凌在社交平台晒出侯佩岑同款彩虹照片，引发网友热议，随后周......
### [有种“早熟脸”叫热巴，同框29岁杨洋却像他姐姐，演情侣难道合适](https://new.qq.com/omn/20200930/20200930A0EMMP00.html)
> 概要: 自从宣布了迪丽热巴和杨洋将要合作新剧《你是我的荣耀》之后，最近的很多媒体也格外的关注两人的动态，毕竟从两人的颜值上看，这部作品还是很可以期待的。迪丽热巴也算是一个和很多男演员都有CP感的女明星了，前段......
### [肖战工作室1周年快乐，发文暗藏两首歌，未来一起陪肖战走](https://new.qq.com/omn/20200930/20200930A0F0GM00.html)
> 概要: 要知道人的一生并不是都是不顺的，肖战就是如此在他身上发生了很多不可思议的事情，但是他现在正在慢慢恢复中，如今的肖战状态超级好，担心他的粉丝们也逐渐打消了疑虑，同时肖战还获得央视的认可，参演了最美逆行者......
### [《急先锋》猫眼评分8.5，成龙杨洋打戏太燃，网友：全程无尿点](https://new.qq.com/omn/20200930/20200930A0H8YQ00.html)
> 概要: 9月30日下午，由唐季礼执导的成龙新片《急先锋》上映，目前在猫眼评分8.5分。            这部影片也是好评如潮，不少网友都为他留下了影评，有网友表示:太好看了，全程无尿点，成龙电影的打戏真......
### [成龙大哥终会老去，但他的拼搏精神在《急先锋》得以传承](https://new.qq.com/omn/20200930/20200930A0G8IY00.html)
> 概要: 文/芳菲小猪            8090后这代人，基本上都是看着成龙大哥的电影长大的！80后忘不掉成龙大哥的《醉拳》《红番区》《尖峰时刻》《我是谁》《新警察故事》，90后则深受《神话》《十二生肖》......
### [张馨予为孕妇发声后现身机场，挎国货背包接地气，军嫂责任心爆棚](https://new.qq.com/omn/20200930/20200930A0DIZ900.html)
> 概要: 9月30日，有媒体曝光了一组知名演员张馨予的机场照，这是她为被丈夫推下悬崖的孕妇发声后首度现身。33岁张馨予的举止随意，显然心情颇为不错。            照片里张馨予一身黑装打扮，黑色皮质西装......
# 动漫
### [TV动画《五等分的花嫁》公开第二季宣传图与番宣广告](https://news.dmzj.com/article/68750.html)
> 概要: 预计将于2021年1月开始播出的TV动画《五等分的花嫁∬》公开了主宣传图和番宣广告。在公开的宣传图上，可以看到五名女主角在一起的样子。
### [P站美图推荐——背带特辑](https://news.dmzj.com/article/68749.html)
> 概要: 本来只是用于固定裙子和裤子的背带在发展中似乎有了别的作用···？
### [《赌博默示录》舞台剧化！山崎大辉担任主演](https://news.dmzj.com/article/68751.html)
> 概要: 由福本伸行创作的漫画《赌博默示录》宣布了舞台剧化的消息，本作舞台剧预计将于12月4日至6日在京都，12月10日至13日在东京上演。
### [TV动画《影之诗》公开新宣传图和PV](https://news.dmzj.com/article/68745.html)
> 概要: TV动画《影之诗》公开了新的主宣传图和PV。在这段PV与宣传图中，都可以看到在受到灾祸之树的影响而荒废的世界中，主人公们准备进行新的战斗的样子。
### [街拍版《龙珠》 悟空帅气，布欧时尚，布尔玛太正了](https://new.qq.com/omn/20200930/20200930A0ACDP00.html)
> 概要: 龙珠这部动漫，也是漫界中的经典之作，有关于龙珠的各种版本，从开播到现在，也已经几十年了，在这几十年的时间里，陪伴了很多人的童年，也收获了不少粉丝。七龙珠的神话，早就已经在漫迷心中根深蒂固了。对于里面……
### [《灌篮高手》为何英俊的樱木，会失恋50次？三点因素很关键](https://new.qq.com/omn/20200930/20200930A0CWKV00.html)
> 概要: 《灌篮高手》我相信大部分人的童年都看对这部动漫是记忆犹新，世界的尽头这首歌曲曾经响遍校园各个角落，但凡听到这首歌，心中就充满了鸡血，想要立即下场打篮球。灌篮高手虽然没有完美的结局，但这也说明了，人生……
### [粤语配音的动漫，看过这些的都是TVB粉！满满的童年回忆](https://new.qq.com/omn/20200930/20200930A0HK1F00.html)
> 概要: 曾经的香港TVB出品确实很厉害，然而近年来出品的影视剧还是在吃老本。同时不少资深艺人都往内地发展，导致这几年发展大大不如从前。在最辉煌的时候，不仅是真人影视剧红，连动漫作品在tvb播放都能引起热议，……
### [漫威已经为《复联5》铺好了路，故事发生在宇宙，灭霸已死但却是关键角色](https://new.qq.com/omn/20200930/20200930A069FW00.html)
> 概要: 粉丝心心念念的《复联5》看来是有望了，近日有消息称漫威正在筹备将“湮灭战争”作为《复联5》的主题，MCU还会借此进行一次重大突破。原作中，“湮灭战争”是漫威2006年出版的大事件，但这次故事并不是以……
### [COS赏析：蛇喰梦子](https://new.qq.com/omn/20200930/20200930A09NWZ00.html)
> 概要: 原作：狂赌之渊CN：阡子摄影 后期：快门工刚大木
### [铁轨边的jk白丝美少女，网友：初恋的感觉](https://new.qq.com/omn/20200930/20200930A07YU800.html)
> 概要: 铁轨边的jk白丝美少女，网友：初恋的感觉
# 财经
### [外交部：中国经济加快复苏将给世界人民更多信心](https://finance.sina.com.cn/roll/2020-09-30/doc-iivhvpwy9813218.shtml)
> 概要: 原标题：外交部：中国经济加快复苏将给世界人民更多信心 新华社北京9月30日电（记者郑明达）外交部发言人汪文斌30日说，世界银行近期发布的报告表明...
### [被做空企业超五成仍正常交易 沽空这门生意靠谱么？](https://finance.sina.com.cn/china/2020-09-30/doc-iivhuipp7429681.shtml)
> 概要: 被做空企业超五成仍正常交易，沽空这门生意靠谱么？ 与做多相对，在成熟的资本市场当中，做空也是种常见的交易机制，并因此诞生了浑水、香橼、哥谭...
### [李克强：办好群众关切的上学看病养老托幼等方面实事](https://finance.sina.com.cn/china/gncj/2020-09-30/doc-iivhvpwy9803344.shtml)
> 概要: 原标题：李克强在庆祝中华人民共和国成立七十一周年招待会上的致辞 在庆祝中华人民共和国成立七十一周年招待会上的致辞 中华人民共和国国务院总理 李克强 （2020年9月30日...
### [越南公布2名无症状感染者曾途经南宁 当地回应：正组织排查](https://finance.sina.com.cn/china/gncj/2020-09-30/doc-iivhvpwy9813006.shtml)
> 概要: 原标题：越南公布2名无症状感染者曾途经南宁，当地回应：正组织排查 南宁市西乡塘区新冠肺炎疫情防控工作领导小组指挥部9月30日晚发布消息...
### [农业农村部：今年9-12月生猪出栏将比去年同期增长17.3%](https://finance.sina.com.cn/roll/2020-09-30/doc-iivhvpwy9802743.shtml)
> 概要: 原标题：农业农村部：今年9-12月生猪出栏将比去年同期增长17.3% 新京报讯（记者 田杰雄）近日，国务院办公厅印发《关于促进畜牧业高质量发展的意见》（以下简称《意见》）...
### [王毅接受美国驻华大使布兰斯塔德辞行拜会](https://finance.sina.com.cn/china/gncj/2020-09-30/doc-iivhuipp7429689.shtml)
> 概要: 原标题：王毅接受美国驻华大使布兰斯塔德辞行拜会 2020年9月30日，国务委员兼外交部长王毅接受了美国驻华大使布兰斯塔德辞行拜会。
# 科技
### [2020 Java开发者数据分析：中国已成为 Java 第一大国](https://segmentfault.com/a/1190000025175583)
> 概要: 最近知名开发工具供应商Jetbrains在Java 25周年之际，对开发群体做了一次有意思的数据分析。全文内容可见：https://blog.jetbrains.com/id...通过这次的分析，得出......
### [前端实用小工具（URL参数截取、JSON判断、数据类型检测、版本号对比等）](https://segmentfault.com/a/1190000025176721)
> 概要: 背景在日常开发中，我们经常会用一些工具类方法来实现业务逻辑 下面列举几种最常用的URL截取参数//直接调用输入想要截取的参数名称几个export function getParamFromUrl(ke......
### [华硕灵耀 14s 上架： 11 代酷睿，双雷电 4 接口](https://www.ithome.com/0/511/839.htm)
> 概要: IT之家 9 月 30 日消息 华硕现已上架新款灵耀 14s 笔记本，搭载了 11 代酷睿处理器，配备了双雷电 4 接口。i5-1135G7+16GB 内存 + 512GB SSD：6499 元i7-......
### [宏碁发布新款蜂鸟 Fun 14：11 代酷睿 + Xe 核显，3799 元起](https://www.ithome.com/0/511/841.htm)
> 概要: IT之家 9 月 30 日消息 宏碁现已发布新款蜂鸟 Fun 14 笔记本，搭载了 11 代酷睿处理器，还可选配 MX 350 独显，售价 3799 元起。i5-1135G7+16GB+512GB ：......
### [张爱玲：除了天才，一无所有](https://www.huxiu.com/article/385490.html)
> 概要: 虎嗅机动资讯组作品作者 | 竺晶莹题图 | Google都穿列宁装啊，这可受不了的，张爱玲参加上海第一届文代会时喃喃道。1950年7月，当与会者一律穿着蓝布或灰布列宁装的时候，只有张爱玲还是一袭旗袍，......
### [一个被忽视的企服新趋势](https://www.huxiu.com/article/385290.html)
> 概要: 出品| 虎嗅科技组作者| 张雪封面| CFP本篇是“寻鲸”之一。“寻鲸”栏目，致力于挖掘中国企业服务领域里的案例、话题、从业者的经验与困惑，通过图文与线上直播等内容形式呈现。这个栏目隶属于虎嗅的“大鲸......
### [“新丑风”的崛起，丑丑惹人爱，别说还挺好看上头的~ ​](https://www.tuicool.com/articles/f6riEjy)
> 概要: “新丑风”刮起来~文章转载自：三个设计师ID: the3design编辑：darlene7“新丑风”顾名思义就是打破原有的审美把这些像看似好像没有设计的元素组合一起⽤草率变形的字体粗糙简陋的图⽚漫不经......
### [张勇详解阿里巴巴创新战略：为今天工作，为明天投资，为未来孵化](https://www.tuicool.com/articles/iiiu6jQ)
> 概要: 速途网9月30日消息（报道：李楠）“创新是阿里巴巴20余年来不断发展最重要的动力。”阿里巴巴集团董事会主席兼首席执行官张勇在阿里巴巴2020年度全球投资者大会上说，在疫情带来的诸多不确定性中，数字化是......
# 小说
### [西游蛇妖传](http://book.zongheng.com/book/177073.html)
> 作者：2012年的李白

> 标签：武侠仙侠

> 简介：主角穿越到猴子被压的一年前。这是一个蛇妖在西游的世界里拉风的活着的故事。这是一位盖世妖王的崛起之路。新书《驭鬼修罗》发布，，求点击，，求收藏，，求红票。。

> 章节末：第四百零一章 大结局

> 状态：完本
# 游戏
### [双节活动火热上线 《冒险岛》与你嗨翻假期](https://www.3dmgame.com/news/202009/3798673.html)
> 概要: 2020年的中秋恰巧与国庆节撞上，大家有幸能一次享受8天的假期，是不是已经对如何好好利用休假而蠢蠢欲动了呢？在出国游依旧受到限制的当下，小编决定把这段时间奉献给即将上线“双节专题活动”的经典端游《冒险......
### [一周大师级Cos美图欣赏 美女身材火辣前凸后翘](https://www.3dmgame.com/bagua/3778.html)
> 概要: 又到了欣赏每周大师级Cos合集时间，此次包含《FGO》《英雄联盟》《巫师3》《生化危机》等游戏动漫Cos作品。众多Coser完美还原了人物角色，一起来欣赏下美图吧......
### [受疫情影响 美国迪士尼大裁员近3万人](https://www.3dmgame.com/news/202009/3798675.html)
> 概要: 加州的迪士尼乐园自今年3月以来都因为新冠疫情爆发而关闭。虽然佛罗里达州的迪士尼世界主题公园在7月份限时重新开放，但整个加州的迪士尼公园仍然处于关闭状态。现在，迪士尼正在大规模裁减其公园数量、体验和产品......
### [《如龙7》XSX版支持60帧/4K S版尚未明确](https://www.3dmgame.com/news/202009/3798725.html)
> 概要: 《如龙7》作为一款首次登陆Xbox的如龙系游戏而备受关注，目前从微软商店公布的游戏介绍来看，一切进展还算满意。最近微软更新的游戏列表显示，《如龙7》将在XSX上以4K/60FPS+模式运行，这意味着这......
### [3DM速报：P社玩家罄竹难书，CDPR强制加班冲2077](https://www.3dmgame.com/news/202009/3798714.html)
> 概要: 欢迎来到今日的三大妈速报三分钟带你了解游戏业最新资讯穷凶恶极！150余万囚犯被《十字军之王3》玩家活吃；CDPR开启强制加班冲《赛博朋克2077》；Epic表态：收回国区《看门狗2》与育碧无关；《宝可......
# 论文
### [Miss the Point: Targeted Adversarial Attack on Multiple Landmark Detection](https://paperswithcode.com/paper/miss-the-point-targeted-adversarial-attack-on)
> 日期：10 Jul 2020

> 标签：ADVERSARIAL ATTACK

> 代码：https://github.com/qsyao/attack_landmark_detection

> 描述：Recent methods in multiple landmark detection based on deep convolutional neural networks (CNNs) reach high accuracy and improve traditional clinical workflow. However, the vulnerability of CNNs to adversarial-example attacks can be easily exploited to break classification and segmentation tasks.
### [Simulating normalising constants with referenced thermodynamic integration: application to COVID-19 model selection](https://paperswithcode.com/paper/simulating-normalised-constants-with)
> 日期：8 Sep 2020

> 标签：EPIDEMIOLOGY

> 代码：https://github.com/mrc-ide/referenced-TI

> 描述：Model selection is a fundamental part of Bayesian statistical inference; a widely used tool in the field of epidemiology. Simple methods such as Akaike Information Criterion are commonly used but they do not incorporate the uncertainty of the model's parameters, which can give misleading choices when comparing models with similar fit to the data.
