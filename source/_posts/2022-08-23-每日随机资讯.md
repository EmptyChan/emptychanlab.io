---
title: 2022-08-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MentonFrance_ZH-CN5849270429_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-08-23 22:02:02
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MentonFrance_ZH-CN5849270429_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [边涨价边促销，长视频平台不是“赚吆喝”](https://www.woshipm.com/it/5574552.html)
> 概要: 近年来，我们可以看到各大长视频平台会员都在不断涨价，与此同时，他们又在搞着促销，这是为何？本文从视频会员涨价及其营销方式，探讨长视频做出这种动作的用意，一起来看看。从8月9日起，芒果TV正式上调了会员......
### [营销日历 | 九月创意营销攻略来袭，快来找到属于你的营销热点吧！](https://www.woshipm.com/marketing/5573731.html)
> 概要: 金秋九月，迎来了众多营销热点，其中中秋与教师节合二为一，聚集在这一天，是本月最大的营销热点。本文为大家整理了9月重点的一些营销热点，供大家选择参考，提前创意，做好营销！九月是个丰收的好时节，也是营销创......
### [PMM立身之本系列 | GTM的【6+1】必备元素](https://www.woshipm.com/pd/5574491.html)
> 概要: 什么是不变？沉淀并优化自己的坐标系，基本功，就是面对变化的底气。本文就来讲讲PMM的立身之本，GTM的必备元素【6+1】，沉淀好自己的基本功，才能不畏惧变化，大步向前。我学翻译出身，当发现跟不上口译席......
### [怎么防止同事用 Evil.js 的代码投毒](https://segmentfault.com/a/1190000042360645)
> 概要: 视频移步B站https://www.bilibili.com/vide...最近Evil.js被讨论的很多，项目介绍如下项目被发布到npm上后，引起了激烈的讨论，最终因为安全问题被npm官方移除，代码......
### [华为苹果新机发布时间“撞车” 抢占高端市场“贴身肉搏”](https://finance.sina.com.cn/tech/it/2022-08-23/doc-imizirav9268890.shtml)
> 概要: 本报记者 贾 丽......
### [每日优鲜备受关注，经济日报：生鲜电商“烧钱”烧不出美好未来](https://finance.sina.com.cn/tech/internet/2022-08-23/doc-imizmscv7311246.shtml)
> 概要: 来源：经济日报......
### [达达集团“回归”京东：CEO蒯佳祺离职，由京东零售CEO辛利军接任](https://finance.sina.com.cn/tech/internet/2022-08-23/doc-imizmscv7313327.shtml)
> 概要: 新浪科技讯 8月23日早间消息，本地即时零售和即时配送平台达达集团（股票代码：DADA）发布了二季度财报，总营收为23亿元人民币，同比增长55%；调整后净利润率同比优化20个百分点；京东到家直接利润率......
### [Republishing a fork of the sanctioned Tornado Cash repositories](https://twitter.com/matthew_d_green/status/1561813046338748417)
> 概要: Republishing a fork of the sanctioned Tornado Cash repositories
### [茅台拆股，散户狂欢？](https://www.huxiu.com/article/640735.html)
> 概要: 作者｜Eastland头图｜视觉中国由于贵州茅台（600519.SH）股价高高在上，关于通过送转股降低投资者参与门槛的讨论及传闻一再出现。最近，又有消息称：茅台有可能实施“1拆4”。茅台也曾积极送/转......
### [KDD 2022 | 美团技术团队精选论文解读](https://segmentfault.com/a/1190000042363642)
> 概要: 今年，美团技术团队有多篇论文被KDD 2022收录，这些论文涵盖了图谱预训练、选择算法、意图自动发现、效果建模、策略学习、概率预测、奖励框架等多个技术领域。本文精选了7篇论文做简要介绍（附下载链接），......
### [字节推出全新搜索引擎，悟空搜索对标夸克](https://finance.sina.com.cn/tech/internet/2022-08-23/doc-imizirav9286407.shtml)
> 概要: 新言财经（微信ID：tech621）......
### [动画电影《普罗米亚》将于8月27日 在CCTV6播出](https://acg.gamersky.com/news/202208/1511296.shtml)
> 概要: 今石洋之导演的动画电影《普罗米亚》预计将于本周六（8月27日）09:57在央视CCTV6播出。
### [【字演生肖】这是大（dai）王文字编排实验04](https://www.zcool.com.cn/work/ZNjE1NjM4MDg=.html)
> 概要: 文字编排除了要满足自身传递信息的作用之外，还需要通过有效的组合与排列使呆板的文字产生鲜明的记忆点，相较于文字而言，图形化的处理方式使信息内容更加直接明了，而形式与形态的变化、原理与规律的使用、方式与方......
### [「刀剑神域：进击篇·黯淡黄昏的谐谑曲」宣布延期](http://acg.178.com/202208/455219913734.html)
> 概要: 剧场版动画「刀剑神域：进击篇·黯淡黄昏的谐谑曲」宣布因新冠疫情的蔓延，将推迟上映，本作原计划在2022年9月10日上映，目前新的上映时间还未公布。「刀剑神域：进击篇·黯淡黄昏的谐谑曲」讲述了大约1万名......
### [【祥禾饽饽铺】糕点系列包装](https://www.zcool.com.cn/work/ZNjE1NjM5NzY=.html)
> 概要: 一说到天津卫，外地的吃货小伙伴们都会立刻想到【狗不理包子】【十八街麻花】。确实这两个小吃是天津的代表，但是哏儿都的美味可远不止这些...天津作为皇城根儿的北方大城市，对传统小吃的需求必然不在话下。【桂......
### [K8s小白？应用部署太难？看这篇就够了！](https://segmentfault.com/a/1190000042365041)
> 概要: 在云原生趋势下，容器和 Kubernetes 可谓是家喻户晓，许多企业内部的研发团队都在使用 Kubernetes 打造 DevOps 平台。从最早的容器概念到 Kubernetes 再到 DevOp......
### [「无头骑士异闻录」漫画「デュラララ!! RE;DOLLARS編（8）」封面公开](http://acg.178.com/202208/455221177337.html)
> 概要: 近日，「无头骑士异闻录」官方发布了系列漫画最新卷「デュラララ!! RE;DOLLARS編（8）」的封面图，绘制的角色为赛尔提和新罗。漫画「无头骑士异闻录」改编自成田良悟创作的轻小说，讲述了乡下少年龙之......
### [TGA主持人暗示：科隆展将公布新《无主之地传说》](https://www.3dmgame.com/news/202208/3849948.html)
> 概要: TGA主持人Geoff Keighley今日在推特上上传了一张神秘图片，配文“明天见”，并@了《无主之地》官方推特账号，推文标签中还有科隆游戏展和开幕之夜字样。根据之前泄露的消息，这可能是在暗示新《无......
### [ufotable「鬼灭之刃」悲鸣屿行冥生日贺图公开](http://acg.178.com/202208/455221594328.html)
> 概要: 今天（8月23日）是「鬼灭之刃」中人气角色悲鸣屿行冥的生日，动画官方ufotable发布了为他庆生的最新贺图！悲鸣屿行冥是鬼杀队最高级别阶级「柱」的一员，被誉为“最强的柱”，也是鬼杀队的「柱」中最年长......
### [「天籁人偶」角色歌专辑「ON STAGE」封面公开](http://acg.178.com/202208/455221907908.html)
> 概要: 近日，「天籁人偶」官方公开了角色歌专辑「ON STAGE」的封面使用插图，由角色设计·总作画监督矢野茜先生绘制。原创电视动画「天籁人偶」由Bibury Animation Studios制作，是同名混......
### [女子青春群像喜剧，搓麻的小日常~](https://news.dmzj.com/article/75324.html)
> 概要: 这期给大家推荐一部校园漫画《啊、那张我碰了！》，作者けんたうろす，讲述摇曳轻松的麻将（？）女子喜剧！因为我们动漫之家传统就是经常比赛或者直播打麻将（雀魂），那这部漫画不得看一下~漂亮妹妹搓麻~碰。
### [【SpringBoot 实战】数据报表统计并定时推送用户的手把手教程](https://segmentfault.com/a/1190000042365313)
> 概要: 本文节选自《实战演练专题》【实战系列】数据报表统计并定时推送用户的手把手教程通过一个小的业务点出发，搭建一个可以实例使用的项目工程，将各种知识点串联起来; 实战演练专题中，每一个项目都是可以独立运行的......
### [《收获日》开发商仍处于亏损状态 目前正在开发新IP](https://www.3dmgame.com/news/202208/3849962.html)
> 概要: 《收获日》开发商Starbreeze刚刚公布了2022年第二季度的财务业绩，虽然它仍在亏损，但相比之前已有所改善。净销售额同比下降约4.7%，净亏损额为1120万瑞典克朗（约合717万人民币），但低于......
### [励精图治 卓越经营 中植基金荣获年度“最佳职业发展平台雇主”奖](http://www.investorscn.com/2022/08/23/102486/)
> 概要: 8月18日，由中国经营报联合科锐国际共同主办的“2022卓越雇主品牌云端论坛”在线上召开，会上同步隆重揭晓了2022年度卓越雇主品牌榜单。北京中植基金销售有限公司（以下简称：中植基金）凭借丰富的企业文......
### [2022年首届新时代财富峰会中植企业集团总裁颜茂昆：印象中植](http://www.investorscn.com/2022/08/23/102488/)
> 概要: 8月19日，由恒乐汇财富私享俱乐部主办的“2022年首届新时代财富峰会”于云南昆明盛大召开。中植企业集团总裁颜茂昆莅临现场，发表《印象中植》开场致辞，从多元性、协同性、创新性三方面阐述了他对中植的三个......
### [The messages that survived civilisation's collapse](https://www.bbc.com/future/article/20220818-how-to-write-a-message-to-the-future)
> 概要: The messages that survived civilisation's collapse
### [曾沛慈确诊感染新冠 目前在家静养](https://ent.sina.com.cn/y/ygangtai/2022-08-23/doc-imizmscv7370289.shtml)
> 概要: 新浪娱乐讯 23日，曾沛慈公司发布通知，称曾沛慈23日上午感到身体不适，新冠快筛显示阳性，现正在家静养。对于近期取消的工作，以及对各单位造成的困扰，致上最深的歉意......
### [《怪猎崛起》销量破1100万 曙光DLC破400万](https://www.3dmgame.com/news/202208/3849967.html)
> 概要: 今日（8月23日），CAPCOM官方宣布公司发行的动作冒险游戏《怪物猎人：崛起》全球销量现已突破1100万份，其超大型付费DLC“曙光”销量突破400万份。《怪物猎人：崛起》是CAPCOM制作发行的一......
### [受华纳消减成本的策略 《蝙蝠侠》动画电影停止制作](https://acg.gamersky.com/news/202208/1511442.shtml)
> 概要: 《蝙蝠侠：披风战士》将不会在HBO Max上播出。
### [【原创】30顺位又如何！灰熊为何不肯出贝恩？](https://bbs.hupu.com/55253186.html)
> 概要: 本文由西海岸出品@篮球大历史撰写，欢迎大家关注（我们在各大平台id都是@篮球大历史）投稿私信@西海岸篮球喜欢本文的话点赞/评论/转发三连事情大概是这样，昨天深夜，The Athletic名记Shams
### [《爱情而已》发声明回应吻戏片段泄露 剧组已报警](https://ent.sina.com.cn/v/m/2022-08-23/doc-imizmscv7384447.shtml)
> 概要: 新浪娱乐讯 近日，由吴磊、周雨彤主演的《爱情而已》亲密戏视频曝光，23日，《爱情而已》剧组发表声明称相关机密视频及照片为个人或团体使用非法手段获取，剧组已经报警：“对于本次剧集未公开资料泄露事件，剧组......
### [中古风效果图表现](https://www.zcool.com.cn/work/ZNjE1NzA4NDg=.html)
> 概要: 办公与生活的结合，业主有居家办公的需求，将原本的客厅位置作为了主要办公空间，图有点多，慢慢看。希望各位喜欢......
### [锻造韧性，企业到底需要什么样的数字化系统？](http://www.investorscn.com/2022/08/23/102498/)
> 概要: 8月16日晚7点，由CTO李楠领衔，“徙说数字化”云徙产品系列课程正式开讲......
### [出海合规科技平台辰海集团完成C1轮数千万美金融资，引领出海合规科技](http://www.investorscn.com/2022/08/23/102499/)
> 概要: 近日，一站式出海合规科技服务领军者辰海集团宣布完成新一轮数千万美元融资，本轮融资由易达资本（eWTP Arabia Capital）领投，老股东东方富海、金沙江联合资本持续加码。本轮融资将用于持续加大......
### [【直播预告】这周还有抽奖哦~](https://news.dmzj.com/article/75330.html)
> 概要: 本周直播预告
### [Yoovee-带无线充电器的智能语音助手](https://www.zcool.com.cn/work/ZNjE1NzI1Njg=.html)
> 概要: Yoovee- 带无线充电器的智能语音助手2022“YOOVEEE”是一个带无线充电器的语音助手，用户可以将手机放入其中适当的充电。它可以平衡日常生活的节奏，帮助您在社交场合减少手机的使用。目的是让用......
### [三星推出 98 英寸 Neo QLED 电视：带 120W 6.4.4 扬声器，售价高达 23 万](https://www.ithome.com/0/636/638.htm)
> 概要: IT之家8 月 23 日消息，三星在韩国推出了一款新的 98 英寸 Neo QLED 电视。与去年的机型相比，今年的这款 98 英寸电视具有 4K 分辨率，并拥有更高的亮度、更好的图像质量，以及更强大......
### [微软 Edge 浏览器跨设备文件传输功能 Drop 征集中文名：拖拖、无边、闪推等](https://www.ithome.com/0/636/639.htm)
> 概要: IT之家8 月 23 日消息，微软 Edge 浏览器近日测试了一项全新 Drop 功能，该功能可以帮助用户使用浏览器跨设备共享文件和笔记，类似于 Telegram 的已保存消息功能。昨日晚间，微软 E......
### [湖北黄石，刚跑出一个超级独角兽](https://www.tuicool.com/articles/vuU3InY)
> 概要: 湖北黄石，刚跑出一个超级独角兽
### [英国电费价格过高 9%的玩家选择不再玩游戏](https://news.dmzj.com/article/75332.html)
> 概要: ​目前，英国电费由于各种问题不断上调。一些英国的游戏玩家已经开始考虑通过放弃游戏而节省开支了。根据一家国外媒体发布的对2000名英国游戏玩家进行的调查结果，有9%的玩家回答正在考虑完全放弃游戏。
### [组图：虞书欣王鹤棣《苍兰诀》开分7.7分 19万人参与评价](http://slide.ent.sina.com.cn/tv/slide_4_704_374295.html)
> 概要: 组图：虞书欣王鹤棣《苍兰诀》开分7.7分 19万人参与评价
### [P1S推出《鬼泣5》但丁、尼禄1/4雕像 售价8443元](https://www.3dmgame.com/news/202208/3849990.html)
> 概要: Prime 1 Studio为了庆祝公司成立10周年，特别推出了《鬼泣5》但丁、尼禄EX配色限量版1/4雕像，每款限量350个，售价16万9290日元（约合人民币8443元），重16.6公斤，现已开启......
### [重疾险上新啦！人保寿险推出多重利好“尊享嘉倍”](http://www.investorscn.com/2022/08/23/102501/)
> 概要: 盛夏七月，人保寿险携手前海再保险、梧桐树经纪线上推出互联网专属重疾险“爱无忧”，该产品创新了重疾责任，数十种常见疾病的带病人群都有机会承保，引来市场关注。许多人追问：线下产品能不能也像线上一样给力？2......
### [置鲇龙太郎等声优出演！朗读剧《声歌舞》10月上演](https://news.dmzj.com/article/75335.html)
> 概要: 由置鲇龙太郎、细谷佳正、速水奖、逢坂良太出演的原创朗读剧《声歌舞》宣布了将于10月1日和10月2日在日本东京上演的消息。
### [第五届中国—东盟新型智慧城市协同创新大赛动漫分赛启动](https://new.qq.com/omn/ACF20220/ACF2022082300081000.html)
> 概要: 南宁市发展和改革委员会以“漫话绿城”为主题，携手*SCAPE新加坡国家青年发展中心、新加坡新跃社科大学、泰国朱拉隆功大学、马来西亚砂拉越大学、南宁市大数据发展局、南宁市外事办公室、中国电信股份有限责任......
### [【算法实践】| 一步步带你实现寻找最大公约数](https://www.tuicool.com/articles/fiqiMrN)
> 概要: 【算法实践】| 一步步带你实现寻找最大公约数
### [《海贼王》1058话赏金情报：乔巴身价暴增](https://acg.gamersky.com/news/202208/1511616.shtml)
> 概要: 《海贼王》漫画1058话情报公开了一部分，将草帽一伙的新赏金和十字公会成员的赏金公开了。
### [AOC 公布新款 GQ3908VW 大屏显示器：38.5 英寸 QHD 165Hz](https://www.ithome.com/0/636/670.htm)
> 概要: IT之家8 月 23 日消息，AOC 官网公布了新款 GQ3908VW 大屏显示器，38.5 英寸 QHD 165Hz 规格，价格暂未公布。IT之家了解到，这款显示器搭载了 38.5 英寸的曲面 VA......
### [刘强东曾经看好的书店，塌了](https://www.tuicool.com/articles/F7fe2eN)
> 概要: 刘强东曾经看好的书店，塌了
### [安卓端最强游戏手机？ROG 天玑 9000+ 新机跑分超 114 万，硬刚骁龙 8 + 旗舰](https://www.ithome.com/0/636/673.htm)
> 概要: 感谢IT之家网友Jay风耀、蓝色海岸的线索投递！IT之家8 月 23 日消息，此前多方爆料称 ROG 将推出搭载天玑 9000+ 的机型，华硕官网也出现了名为 ROG 游戏手机 6D 的新机。今日，该......
### [国际油价走势转弱，国内成品油价今晚或迎年内“五连降”](https://www.yicai.com/news/101515274.html)
> 概要: 今晚24时，国内成品油将迎来新一轮调价窗口，业内预测将是年内“五连降”。
### [指数全线震荡整理后能否继续上行？](https://www.yicai.com/video/101515289.html)
> 概要: 指数全线震荡整理后能否继续上行？
### [The American Dream](https://www.tomkiefer.com/el-sueno-americano)
> 概要: The American Dream
### [Accessing WebAssembly reference-typed arrays from C++](https://wingolog.org/archives/2022/08/23/accessing-webassembly-reference-typed-arrays-from-c)
> 概要: Accessing WebAssembly reference-typed arrays from C++
### [CVE-2022-2884：GitLab远程代码执行漏洞通告](https://www.tuicool.com/articles/iUj2uqU)
> 概要: CVE-2022-2884：GitLab远程代码执行漏洞通告
### [上海暂停外滩等部分地区景观照明：浦江游览停航，部分酒店打折](https://www.yicai.com/news/101515319.html)
> 概要: 浦江游览“精华游”航线8月22日、8月23日两天全天停航，8月24日起恢复正常。已在网上购票的游客，可通过原购票渠道申请改签或退款。
### [年薪500万，元宇宙画的饼能落地吗？](https://www.huxiu.com/article/643158.html)
> 概要: 本文来自微信公众号：豹变（ID：baobiannews），作者：陈法善，编辑：张子睿，头图来自：视觉中国在不少大厂降本增效、削减福利“过冬”时，处于风口的元宇宙似乎正迎来“春天”。有消息称，元宇宙行业......
### [童年神剧被毁，究竟怪谁？](https://www.huxiu.com/article/643144.html)
> 概要: 本文来自微信公众号：往事叉烧（ID：wschashao），作者：Zack，头图来自：《家有儿女》截图拍《家有儿女》时，高亚麟和宋丹丹两个人都很痛苦，因为三个小演员太折腾，两人受不了。杨紫入戏慢，尤浩然......
### [财经TOP10| 油价五连降！商务部召开发布会，多地房贷利率降至5%以内！格力怎么了？任正非最新讲话曝光...](https://finance.sina.com.cn/china/2022-08-23/doc-imizmscv7430211.shtml)
> 概要: 【政经要闻】 NO.1 油价“五连降”！明起“加油”，又省了→ 根据国家发改委消息，新一轮成品油调价窗口将于今天（8月23日）24时开启。
### [罕见高温“烤”验长江流域供水，水利部调动三峡等水库群为下游补水](https://finance.sina.com.cn/china/gncj/2022-08-23/doc-imizmscv7431175.shtml)
> 概要: 来源：21世纪经济报道 供水受影响的主要是以小型水库或山泉、溪流作为水源的分散供水工程。 21世纪经济报道记者李莎 北京报道“我家现在喝的水能正常供应...
### [微信又在“挤牙膏”了](https://www.huxiu.com/article/643103.html)
> 概要: 本文来自微信公众号：少数派（ID：sspaime），作者：Tp，原文标题：《微信 8.0.27 更新：视频悬窗、隐私保护，还有 1 个新功能》，题图来自：视觉中国距离微信 上一次更新 已经过去 1 个......
### [《PGA TOUR 2K23》Steam开启预购 标准版199元](https://www.3dmgame.com/news/202208/3850005.html)
> 概要: 《PGA TOUR 2K23》Steam国区开启预购，标准版199元，豪华版329元，老虎伍兹版395元，自带简体中文。Steam版采用了D加密。在《PGA TOUR 2K23》中更潇洒自如地尽情挥杆......
### [生态环境部：积极筹备COP15第二阶段会议，推动构建2020年后全球生物多样性框架](https://finance.sina.com.cn/china/gncj/2022-08-23/doc-imizirav9398102.shtml)
> 概要: 21世纪经济报道记者李德尚玉 实习生刘雨青 王彤烨北京报道 8月23日，生态环境部新闻发言人刘友宾在生态环境部举行8月例行新闻发布会上表示...
### [中暑算“意外伤害”还是“疾病”？意外险可以赔吗？法院这样判定](https://www.yicai.com/news/101515383.html)
> 概要: 保险公司该不该赔？
### [今年是未来十年最凉快的？权威部门辟谣！中科院专家：高温热浪将成新常态](https://finance.sina.com.cn/china/2022-08-23/doc-imizmscv7434261.shtml)
> 概要: 高温热浪仍在继续。 8月23日，中央气象台继续发布高温红色预警，#高温红色预警已连发12天#冲上热搜。 为支援重庆抗旱，今日，中国气象局调派一架高性能飞机驰援重庆增雨抗...
### [流言板能扣篮的荷兰人！步行者官方晒图为里克-施密茨庆生](https://bbs.hupu.com/55258201.html)
> 概要: 虎扑08月23日讯 步行者官方更新推特，晒出一张里克-施密茨的照片并为其献上生日祝福。“祝里克-施密茨生日快乐🎉！”步行者官方在推文中写道。里克-施密茨生涯都在步行者效力，里克-施密茨曾在1997-
### [资源股三季度“唱主角”？](https://www.yicai.com/video/101515395.html)
> 概要: 资源股三季度“唱主角”？
### [流言板尼克斯方面认为若有布伦森米切尔，球队能吸引来第三球星](https://bbs.hupu.com/55258320.html)
> 概要: 虎扑08月23日讯 根据《纽约邮报》记者Marc Berman得报道，尼克斯方面认为，如果队内拥有杰伦-布伦森和多诺万-米切尔的话，他们能够吸引另一位球星加盟球队。今年休赛期，尼克斯4年1.04亿美元
### [2019年以来累计清理违法和不良信息200多亿条账号近14亿个](https://finance.sina.com.cn/china/gncj/2022-08-23/doc-imizmscv7435173.shtml)
> 概要: 央视网消息：“从2019年以来，我们累计清理违法和不良信息200多亿条，账号近14亿个，赢得了广大网民的支持肯定！”8月23日，国新办举行2022年中国网络文明大会有关情况新闻发...
### [一图流 张飞关键开团，宫本武藏进场收割，打出1换3](https://bbs.hupu.com/55258375.html)
> 概要: 虎扑08月23日讯 2022年KPL夏季赛，佛山GK对阵上海EDGM的第五局比赛中，张飞关键开团，宫本武藏进场收割，打出1换3   来源： 虎扑
# 小说
### [道仙荒](http://book.zongheng.com/book/898743.html)
> 作者：扶苏石

> 标签：奇幻玄幻

> 简介：山不在高，有仙则名，水不在深，有龙则灵。千百年的轮回，难寻古今的真相。消逝的历史，破灭的世界，损败的大地。历史洪流滚滚，时间长河漫漫。谁能揭秘隐藏起的过去，谁能揭开笼罩在时间中的黑暗？谁又能闻道打破百世的孽业？且看陆天，成道破仙荒！

> 章节末：作者有话说

> 状态：完本
# 论文
### [A Priority Map for Vision-and-Language Navigation with Trajectory Plans and Feature-Location Cues | Papers With Code](https://paperswithcode.com/paper/a-priority-map-for-vision-and-language)
> 日期：24 Jul 2022

> 标签：None

> 代码：https://github.com/jasonarmitage-res/pm-vln

> 描述：In a busy city street, a pedestrian surrounded by distractions can pick out a single sign if it is relevant to their route. Artificial agents in outdoor Vision-and-Language Navigation (VLN) are also confronted with detecting supervisory signal on environment features and location in inputs. To boost the prominence of relevant features in transformer-based architectures without costly preprocessing and pretraining, we take inspiration from priority maps - a mechanism described in neuropsychological studies. We implement a novel priority map module and pretrain on auxiliary tasks using low-sample datasets with high-level representations of routes and environment-related references to urban features. A hierarchical process of trajectory planning - with subsequent parameterised visual boost filtering on visual inputs and prediction of corresponding textual spans - addresses the core challenges of cross-modal alignment and feature-level localisation. The priority map module is integrated into a feature-location framework that doubles the task completion rates of standalone transformers and attains state-of-the-art performance on the Touchdown benchmark for VLN. Code and data are referenced in Appendix C.
### [Heart rate estimation in intense exercise videos | Papers With Code](https://paperswithcode.com/paper/heart-rate-estimation-in-intense-exercise)
> 日期：4 Aug 2022

> 标签：None

> 代码：https://github.com/ynapolean/ibis-cnn

> 描述：Estimating heart rate from video allows non-contact health monitoring with applications in patient care, human interaction, and sports. Existing work can robustly measure heart rate under some degree of motion by face tracking. However, this is not always possible in unconstrained settings, as the face might be occluded or even outside the camera. Here, we present IntensePhysio: a challenging video heart rate estimation dataset with realistic face occlusions, severe subject motion, and ample heart rate variation. To ensure heart rate variation in a realistic setting we record each subject for around 1-2 hours. The subject is exercising (at a moderate to high intensity) on a cycling ergometer with an attached video camera and is given no instructions regarding positioning or movement. We have 11 subjects, and approximately 20 total hours of video. We show that the existing remote photo-plethysmography methods have difficulty in estimating heart rate in this setting. In addition, we present IBIS-CNN, a new baseline using spatio-temporal superpixels, which improves on existing models by eliminating the need for a visible face/face tracking. We will make the code and data publically available soon.
