---
title: 2020-12-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Beethoven250_EN-CN8111797554_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-16 22:12:32
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Beethoven250_EN-CN8111797554_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：朱星杰发文承认恋情 谈过往言论“我说过的话我都认”](https://video.sina.com.cn/p/ent/2020-12-16/detail-iiznctke6822796.d.html)
> 概要: 视频：朱星杰发文承认恋情 谈过往言论“我说过的话我都认”
### [凡而不自知！郑渊洁在线问啥叫凡尔赛道歉](https://ent.sina.com.cn/s/m/2020-12-16/doc-iiznctke6871328.shtml)
> 概要: 近日，有网友给郑渊洁留言：“我郑在背您写的文章，累啊。” 郑渊洁看后回复：“抱歉，你在背我写的文章，我却在玩游戏。真不应该。我现在就退出游戏”，被网友调侃是“凡尔赛式道歉”引发热议。12月16日晚，他......
### [组图：刘亚仁分享最新写真 个性十足气质独特](http://slide.ent.sina.com.cn/star/k/slide_4_704_349833.html)
> 概要: 组图：刘亚仁分享最新写真 个性十足气质独特
### [挖坑过头忘了填？霉霉承认新专乌龙暗示忘记修改](https://ent.sina.com.cn/y/youmei/2020-12-16/doc-iiznctke6764385.shtml)
> 概要: 新浪娱乐讯  据外媒报道，泰勒·斯威夫特（Taylor Swift）的粉丝要失望了，因为没有第三张秘密专辑。　　今年7月，向来喜欢给粉丝制造惊喜的霉霉宣布发行《Folklore》。上个星期，她又推出了......
### [日本票房：《鬼灭之刃》将超《千与千寻》成第一](https://ent.sina.com.cn/m/f/2020-12-16/doc-iiznezxs7185264.shtml)
> 概要: 日本周末票房排行榜2020年12月12日—12月13日　　新浪娱乐讯 12月12日至12月13日日本国内电影票房榜中，《剧场版鬼灭之刃：无限列车篇》周末两天时间动员观众65.5万人次，票房增长9.39......
### [“豪门媳妇”奚梦瑶，原来我们看错你了](https://new.qq.com/omn/20201216/20201216A0HMZN00.html)
> 概要: 这季《幸福三重奏》，最受关注的非何猷君奚梦瑶这对夫妻莫属。最新一期节目中，“西柚夫妇”有个细节格外吸引观众：于谦独自烤鱼，其他人都在一旁聊天，奚梦瑶忽然提出和老公一起唱歌给大家助助兴。        ......
### [陈都灵与陈瑶：遇见多面的自己](https://new.qq.com/omn/20201216/20201216A0HMON00.html)
> 概要: 监制/Fionn策划、统筹、编辑/沈啸Roye导演、制作/张恒Beard造型/闫彦陈都灵妆发/张英嗣陈瑶妆发/木歌摄影/李钱鑫Conor制片统筹/付徜岐Francis视觉设计/Du Xiaobai美术......
### [高圣远庆生不见妻子周迅身影，3年未同框，男方删光合照引猜疑](https://new.qq.com/omn/20201216/20201216A0HLXU00.html)
> 概要: 15日，周迅51岁的老公高圣远在社交平台发布了一则自己的庆生视频，画面中一桌十余人为他庆贺，他看着蛋糕端过来，高兴得手舞足蹈。            只是这份快乐竟然没有和自己的妻子一同分享，还是让人......
### [赵丽颖演14岁少女太违和？看到拍摄花絮，终于理解导演的审美](https://new.qq.com/omn/20201216/20201216A0FT5T00.html)
> 概要: 近日，赵丽颖的新剧《有翡》终于开播了。虽然该剧上线之后的收视情况非常乐观，但赵丽颖所扮演的角色——“周翡”出场时却引发了争议，因为按照剧本设定，她才14岁，所以有部分网友觉得：让32岁的赵丽颖去饰演一......
### [霍启刚又暗戳戳秀恩爱，妻子郭晶晶展现才艺，他则一脸崇拜如迷弟](https://new.qq.com/omn/20201216/20201216A0HMIU00.html)
> 概要: 12月16号霍启刚在社交网上晒出一段有意义且十分温馨的视频，从曝光的视频内容中不难看出，霍启刚与妻子郭晶晶前往的地方是一家位于香港繁华地带的新加坡来港经营的社企餐厅“厨尊”。            作......
# 动漫
### [【咒术回战/All惠】恋与伏黑惠](https://news.dmzj.com/article/69597.html)
> 概要: 看了动画，我确认了一件事——MAPPA，除了《恋与制作人》，真的很神_(:з」∠)_
### [《隔壁的男孩子》书籍化决定！宣传PV公开](https://news.dmzj.com/article/69596.html)
> 概要: 插画家Milliglam绘制的漫画《隔壁的男孩子》书籍化决定，同时公开角色PV。
### [野岛伸司脚本《WONDER EGG PRIORITY》PV](https://news.dmzj.com/article/69603.html)
> 概要: 由野岛伸司负责原案、脚本的原创TV动画《WONDER EGG PRIORITY》公开了主宣图和PV等消息。本作预计将于2021年1月12日开始播出。
### [动画电影《烟囱城的普佩尔》前3分钟内容公开](https://news.dmzj.com/article/69598.html)
> 概要: 动画电影《烟囱城的普佩尔》公开了前三分钟的无剪辑内容。在这段视频中，可以看到主人公的父亲讲故事以及普佩尔诞生的片段。
### [甜丧·低糖·情侣头像：你赠的回忆，我还你铭记](https://new.qq.com/omn/20201216/20201216A03NN100.html)
> 概要: 甜丧·低糖·情侣头像：你赠的回忆，我还你铭记......
### [甜丧·低糖·动漫情侣头像：其实我爱你，只是我说不出口](https://new.qq.com/omn/20201216/20201216A03JWP00.html)
> 概要: 甜丧·低糖·动漫情侣头像：其实我爱你，只是我说不出口......
### [动漫头像：动漫男生头像](https://new.qq.com/omn/20201216/20201216A0607K00.html)
> 概要: 动漫头像：动漫男生头像                                                                                         ......
### [天官赐福剧版人物敲定，是胡一天和侯明昊，网友：这绝对不行](https://new.qq.com/omn/20201216/20201216A0HJDR00.html)
> 概要: 天官赐福的动画正在热播中，这部动画引发了不少粉丝的关注。天官赐福是墨香铜臭的同名小说改编而成的动画作品，而墨香铜臭前段时间也惹了不少麻烦上身，但是这并没有阻拦该动画的播放量暴涨的趋势。        ......
### [唐家三少回应小舞献祭，2年来第二次落泪，上次还是因为亡妻](https://new.qq.com/omn/20201216/20201216A040RB00.html)
> 概要: 唐家三少是一名玄幻小说的作家，所著作的作品有《封印王座》《酒神》《斗罗大陆》等被网友熟知的玄幻作品。            而近期，唐家三少在看到斗罗大陆动画中小舞献祭的场景后，同时在评论的下方，还是......
### [超火“双人”动漫情侣头像：为遇见你，竟花光了我所有的运气](https://new.qq.com/omn/20201216/20201216A03L0I00.html)
> 概要: 超火“双人”动漫情侣头像：为遇见你，竟花光了我所有的运气......
# 财经
### [黄洪:医保目录外的自付医疗费用是商业医疗险未来的发展空间](https://finance.sina.com.cn/china/gncj/2020-12-16/doc-iiznezxs7294273.shtml)
> 概要: 黄洪：医保目录外的自付医疗费用是商业医疗险未来的发展空间 12月16日，银保监会副主席黄洪在国务院政策例行吹风会上谈及城乡居民大病保险时表示...
### [刘尚希：缩小群体性差距是构建新发展格局的关键](https://finance.sina.com.cn/china/gncj/2020-12-16/doc-iiznctke6876133.shtml)
> 概要: 刘尚希：缩小群体性差距是构建新发展格局的关键 近日，论坛中方代表、中国财政科学研究院院长、教授刘尚希就群体性差距发表演讲。
### [财经TOP10|国内新冠疫苗价格首次敲定？这一省份透露采购细节](https://finance.sina.com.cn/china/caijingtop10/2020-12-16/doc-iiznctke6874697.shtml)
> 概要: 【宏观要闻】 NO.1 使用现金支付遭歧视？ 央行：若查实将处罚！ 近期，有关老年人去交医保却被告知“不收现金只能手机支付”、个别商铺挂出“不收现金”标识...
### [澳门回归21年 内地经拱北海关对澳门进出口大增](https://finance.sina.com.cn/china/dfjj/2020-12-16/doc-iiznezxs7288348.shtml)
> 概要: 原标题：澳门回归21年 内地经拱北海关对澳门进出口大增 拱北海关16日发布消息称，澳门回归21年来，内地经拱北海关对澳门进出口总值累计突破2900亿元。
### [国家5A级景区再增21家](https://finance.sina.com.cn/china/gncj/2020-12-16/doc-iiznezxs7292179.shtml)
> 概要: 原标题：国家5A级景区再增21家！ 每经编辑 赵庆 据文旅部网站今日消息，文旅部发布关于拟确定21家旅游景区为国家5A级旅游景区的公示。
### [湖南、浙江多地“拉闸限电” 背后原因却不一样](https://finance.sina.com.cn/china/dfjj/2020-12-16/doc-iiznctke6874021.shtml)
> 概要: 原标题：湖南、浙江多地“拉闸限电”，背后原因却不一样 近日，湖南、浙江等地，陆续发布了“有序用电”通知，多年不见的“拉闸限电”再现。
# 科技
### [开发者大赛报名开启！150W 奖金池，与 HarmonyOS 一起打造全场景智慧生活](https://segmentfault.com/a/1190000038496925)
> 概要: 过去的 10 余年时间里，无论是技术的积累，还是产业链的成熟度，物联网已经完成了原始积累，1.6 万亿美金的产业空间，让我们更加期待万物互联的世界。HarmonyOS 作为一个为万物互联而生的操作系统......
### [含有function的JSON对象转换字符串与反转](https://segmentfault.com/a/1190000038498889)
> 概要: 因为对象转为JSON时JSON.stringify()会把function过滤掉，那么我们就把function转为字符串在去处理就能够达到要求了const page={  version: "8.0......
### [世界最大冰山即将撞击 “企鹅岛”，英国派出潜水艇勘探](https://www.ithome.com/0/525/048.htm)
> 概要: 世界上最大的冰山 A-68a 可能再过几天就会撞上南乔治亚岛的南极野生动物保护区，研究人员已经在为研究由此带来的影响做准备。据追踪冰山数月的英国南极调查局 (BAS)称，两个冰箱大小的机器人潜艇将很快......
### [吊牌价 2728 元：骆驼旗舰店中长款羽绒服 349 元（1.3 折）](https://lapin.ithome.com/html/digi/525090.htm)
> 概要: 防风保暖防钻绒，骆驼旗舰店男女中长款连帽羽绒服报价1349元，叠加App商品价格下方领取满300元减30元品类券，限时限量970元券，实付349元包邮，领券并购买。天猫10年老店，三红旗舰店铺，赠运费......
### [Show HN: Plot your Docker Image’s downloads over time](https://github.com/aeneasr/dockerstats)
> 概要: Show HN: Plot your Docker Image’s downloads over time
### [Facebook accuses people tied to French military of running troll accounts](https://edition.cnn.com/2020/12/15/tech/facebook-france-africa-troll/index.html)
> 概要: Facebook accuses people tied to French military of running troll accounts
### [最多最好的钱，往往流向最不需要它的人那里](https://www.huxiu.com/article/400167.html)
> 概要: 本文来自微信公众号：晚点LatePost（ID：postlate），作者：张潇雨，原标题：《“我最喜欢闪耀的你”》，题图来自：《寄生虫》剧照一2020 年 11 月 24 日上午，几万深圳人在等待一个......
### [从六个方面分析公司的成长性](https://www.huxiu.com/article/400254.html)
> 概要: 本文来自微信公众号：梁孝永康（ID：lxyk523），题图来自：视觉中国公司的成长性是投资重要的一环，能投资一个业绩不断成长的公司，享受由此带来的超高回报率，是每个投资人所追求的目标。同时成长性也是安......
### [萌虎招新如何才能找到高薪工作，守护你的财富](https://www.tuicool.com/articles/32i6bm7)
> 概要: 此稿件为参加萌虎招新的参赛稿件，视频投稿活动正在进行中，详情请戳萌虎招新。本视频作者：认真的天马大家好我是天马，这期我们来说说赚钱。赚钱这件事有点像划船。你可能觉得自己是在一个平静的水面划船，只要划两......
### [印度SaaS创业，千亿的蓝海市场](https://www.tuicool.com/articles/YFjQnqv)
> 概要: 本文作者：Ananya Bhattacharya，译者：艾瑞莉娅，题图来自：视觉中国Flock是由印度连续创业者Bhavin Turakhia成立的一家办公交流软件公司。2020年的新冠疫情似乎没给F......
# 小说
### [妖者为王](http://book.zongheng.com/book/744185.html)
> 作者：妖夜

> 标签：奇幻玄幻

> 简介：少年萧浪，天生觉醒超级神魂，却被误认为废神魂。在家族受尽屈辱，多次被陷害，无奈之下叛出家族，从此一飞冲天，青云直上。为了心爱的姑娘，心中的道义，他不惜举世皆敌，一路屠神。天要压我，我就破了这个天！

> 章节末：第八百三十四章 希望，与未来！（大结局）

> 状态：完本
# 游戏
### [3DM轻松一刻第422期 谁不喜欢渔网袜热裤大长腿](https://www.3dmgame.com/bagua/4057.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！你吃了炫迈？狗：看爷最后反杀你多功能弹射式雨伞不会甩......
### [《赛博朋克2077》旧主机版性能问题罪魁祸首找到了](https://www.3dmgame.com/news/202012/3804336.html)
> 概要: 《赛博朋克2077》在旧主机上有大量的问题，包括游戏崩溃，优化问题，大量bug等等。尽管稳定性和Bug都可以通过后续的补丁修复，但看起来CDPR如果想让游戏在PS4和Xbox One上以可接受的性能运......
### [美国黑人歌唱天后惠特妮·休斯顿传记片主演确定](https://www.3dmgame.com/news/202012/3804289.html)
> 概要: 已故天后歌手惠特妮·休斯顿的传记片《I Wanna Dance With Somebody》（《我愿与之共舞》）选中主演：娜奥米·阿基（《星球大战9》《去他X的世界》）将饰演惠特妮·休斯顿。娜奥米·阿......
### [《超猎都市》2.5更新上线 PC和主机加入跨平台联机](https://www.3dmgame.com/news/202012/3804335.html)
> 概要: 育碧为《超猎都市》发布了全新的更新补丁。这次更新（2.5）为游戏加入了PC和主机平台之间的跨平台支持，允许玩家享受冬季节日活动。详情如下：版本 2.5 中我们引入了跨平台功能，还对之前测评阶段的部分功......
### [《堡垒之夜》黑豹等漫威皮肤泄露 次月或有绿箭皮肤](https://www.3dmgame.com/news/202012/3804292.html)
> 概要: 《堡垒之夜》已经来到了第二章第五赛季了，据国外数据挖掘者ShiinaBR和Guile_GAG的最新爆料，惊奇队长，绿箭，黑豹和模仿大师即将来到《堡垒之夜》。爆料图片：据ShiinaBR称，所有这些漫威......
# 论文
### [Simultaneous Detection and Tracking with Motion Modelling for Multiple Object Tracking](https://paperswithcode.com/paper/simultaneous-detection-and-tracking-with)
> 日期：20 Aug 2020

> 标签：MULTIPLE OBJECT TRACKING

> 代码：https://github.com/shijieS/DMMN

> 描述：Deep learning-based Multiple Object Tracking (MOT) currently relies on off-the-shelf detectors for tracking-by-detection.This results in deep models that are detector biased and evaluations that are detector influenced. To resolve this issue, we introduce Deep Motion Modeling Network (DMM-Net) that can estimate multiple objects' motion parameters to perform joint detection and association in an end-to-end manner.
### [Uncertainty quantification in medical image segmentation with Normalizing Flows](https://paperswithcode.com/paper/uncertainty-quantification-in-medical-image)
> 日期：4 Jun 2020

> 标签：MEDICAL IMAGE SEGMENTATION

> 代码：https://github.com/raghavian/cFlow

> 描述：Medical image segmentation is inherently an ambiguous task due to factors such as partial volumes and variations in anatomical definitions. While in most cases the segmentation uncertainty is around the border of structures of interest, there can also be considerable inter-rater differences.
