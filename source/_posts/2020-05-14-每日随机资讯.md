---
title: 2020-05-14-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BaliRiceHarvest_EN-CN9661763783_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-14 21:08:29
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BaliRiceHarvest_EN-CN9661763783_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：仪式感! EXO金俊勉入伍再现摸头杀 张艺兴隔空表白相爱吧](https://video.sina.com.cn/p/ent/2020-05-14/detail-iirczymk1632656.d.html)
> 概要: 视频：仪式感! EXO金俊勉入伍再现摸头杀 张艺兴隔空表白相爱吧
### [组图：迪丽热巴《长歌行》路透侧颜优越 片场拉伸胳膊俏皮灵动](http://slide.ent.sina.com.cn/tv/slide_4_86512_338653.html)
> 概要: 组图：迪丽热巴《长歌行》路透侧颜优越 片场拉伸胳膊俏皮灵动
### [BLACKPINK粉丝团举行卡车示威 要求组建专门小组](https://ent.sina.com.cn/s/j/2020-05-14/doc-iirczymk1544324.shtml)
> 概要: 新浪娱乐讯 14日，据多家媒体报道，BLACKPINK粉丝团Blink于韩国时间14日上午9时正式进举行了卡车广告，向YG Ent进行了示威。公开的卡车示威中，包含着愤怒的Hash Tag“YG，哪怕......
### [组图：Lisa全新杂志大片曝光 戴粉色手套一头长发性感可爱](http://slide.ent.sina.com.cn/star/slide_4_704_338677.html)
> 概要: 组图：Lisa全新杂志大片曝光 戴粉色手套一头长发性感可爱
### [海莉晒汗蒸照俏皮遮眼 穿性感泳装秀完美身材曲线](https://ent.sina.com.cn/y/youmei/2020-05-14/doc-iirczymk1542755.shtml)
> 概要: 新浪娱乐讯 5月14日，海莉在社交平台上晒出一张穿着大红色性感比基尼的照片，她双手反遮眼睛，显露出凹凸有致的身材，从配文中可以得知她正在汗蒸馆，汗蒸的木桶看起来有质感又舒适。　　网友纷纷评论：“身材简......
### [EXO队长入伍，张艺兴隔空送祝福，金钟大久违现身尽显“团魂”](https://new.qq.com/omn/20200514/20200514A0J4K700.html)
> 概要: 文/阮星竹近日，深受大家喜爱的韩国男团EXO中的队长金俊勉即将入伍，在入伍前，他的EXO队友也前来为他送行，并发出了他和成员们的合照。图片中除了金俊勉还有金钟大、吴世勋、朴灿烈、边伯贤和金钟仁。虽然有......
### [10岁王诗龄近照认不出，瘦了一圈尖下巴抢镜，网友：幸亏没随李湘](https://new.qq.com/omn/20200514/20200514A0GDUD00.html)
> 概要: 虽然已经过去了整整7年，但是当年热播的亲子节目里的宝宝们还是给大家留下了深刻印象。譬如李湘的女儿王诗龄，当年在荧屏里的胖嘟嘟形象就俘获了无数电视观众的心，她和爸爸王岳伦以及跟妈妈李湘的互动，都非常可爱......
### [《清平乐》回归原著，观众却直呼太虐心！不如不回归](https://new.qq.com/omn/20200514/20200514A0L3K700.html)
> 概要: 争议热剧《清平乐》近期随着剧情的发展，逐渐回归原著，虽然口碑两极分化严重，热度却只增不减。            《清平乐》改编自大热网络文学IP《孤城闭》，但剧方为了扩大作品格局，把原著中梁怀吉与徽......
### [张柏芝再晒三胎儿子近照却把脸码住，金色头发被指是混血](https://new.qq.com/omn/20200514/20200514A0C1XP00.html)
> 概要: 张柏芝与谢霆锋离婚之前，曾相继生下儿子Lucas与Quintus，离婚之后两个儿子一直跟随张柏芝生活。前两天，张柏芝发文为二儿子Quintus庆生，已经10岁的Quintus变化不少，与爸爸谢霆锋也是......
### [慈善举报风波后，韩红与朋友聚会热聊，抽雪茄姿势像大佬气势很足](https://new.qq.com/omn/20200514/20200514A05XV700.html)
> 概要: 韩红老师此前一直在积极做公益活动，筹集善款帮助湖北抗击疫情，可谓是殚精竭虑。韩红晒出一张自拍照，显得十分憔悴不堪，让人心疼。但是却有人在网上公开举报韩红老师私吞善款，后来经过调查，证实韩红是无辜的，为......
# 动漫
### [【讣告】漫画家野间美由纪5月2日不幸去世](https://news.dmzj.com/article/67379.html)
> 概要: 漫画家野间美由纪在5月2日正午因缺血性心脏病不幸去世，去世时59岁。葬礼已经于5月4日办理。
### [想死的社畜×美少女吸血鬼！将良创作新单话漫画喜剧](https://news.dmzj.com/article/67383.html)
> 概要: 创作了《思春期 bitter change》等作品的将良，在comic meteor上公开了一篇单话的新作恋爱喜剧《想死的社畜与吸血JK》。
### [漫画《鬼灭之刃》本篇将完结！开始连载短篇外传](https://news.dmzj.com/article/67382.html)
> 概要: 根据偷跑消息，由吾峠呼世晴创作的漫画《鬼灭之刃》即将在Jump24号公开最终回。另外本篇完结后，还将有由平野稜二创作的《鬼灭之刃》外传短篇《炼狱外传》。
### [TV动画《A3!》公开夏组故事的PV](https://news.dmzj.com/article/67374.html)
> 概要: 目前正在播出中的TV动画《A3! SEASON SPRING ＆ SUMMER》，在第6话结束了“SEASON SPRING”的春组故事。在即将于5月15日开始播出的第7话起，本作将播出“SEASON SUMMER”的夏组内容，夏组的宣传视频也一...
### [《鬼灭之刃》正式完结，主角方全员转世，在现代开始了新的生活](https://new.qq.com/omn/20200514/20200514A0HQ2M00.html)
> 概要: 《鬼灭之刃》漫画在205话完结，虽然之前并没有大肆宣传，但在尾页有“おわり”的字样，也说明这一话确实是最终话了。虽然非常的舍不得，但在最后一话作者还是给大家奉献了一个非常用心的结局，几乎所有主角方角……
### [一人之下：八绝技之“拘灵遣将”，王家功法残缺，奥义在于拘灵不是吞灵](https://new.qq.com/omn/20200514/20200514A0L67G00.html)
> 概要: “拘灵遣将”是八绝技中最有争议的一个，继承这门绝技的后辈中，属王家、风家最为强势，传承不同，是两家“勾心斗角”的根源，王家和风家合作过程中也互相试探着，彼此能力的不同之处。文/安小辰拘灵遣将从龙虎山……
### [斗罗105集：小舞八段摔秒杀武魂殿女神，当看清她踢的部位，有够狠](https://new.qq.com/omn/20200514/20200514A0GLRV00.html)
> 概要: 嗨，大家好，我是喜欢《斗罗大陆》的朵朵。经过一场接一场的比赛后，我们终于等来了史莱克和武魂殿的对决。我们都知道这一场对决代表着什么，不仅仅是冠军的头衔，也不仅仅是三块魂骨的奖励，更多的是私人恩怨。不……
### [鬼灭之刃：愈史郎成为当代画家，包括村田在内的所有人都转世成功](https://new.qq.com/omn/20200514/20200514A0NAFA00.html)
> 概要: 动漫《鬼灭之刃》，漫画最新话更新了，酱铺君来分析下出现的每个人设定和职业的特点吧，真是有趣的最终话。善逸家的后人引出人们的转世情况在漫画中，祢豆子与善逸果然被鳄鱼老师撮合成了一对。在现代，整个故事展……
### [海贼王：狂死郎和索大孰强孰弱，一招见分晓，索隆还是太年轻](https://new.qq.com/omn/20200514/20200514A0HK3W00.html)
> 概要: 和之国篇章实在是太过热闹了，出场的人物也非常的多，而有实力的一票人，除了红发基德海贼团，还有不容小觑的和之国本地人，那就是曾经跟随在光月御田身边的赤鞘九侠，但是由于光月御田的死，对赤鞘九侠而言太过悲……
### [《鬼灭之刃》最终话情报，炭治郎的后代登场，蛇恋等人纷纷转世！](https://new.qq.com/omn/20200514/20200514A0BBZ300.html)
> 概要: 在《鬼灭之刃》漫画之前的剧情中，无惨被消灭，炭治郎恢复成人，鬼杀队正式解散，炭治郎等人回到了老家过起了幸福快乐的生活。而在漫画的最新一话情报中，时间来到了现代，炭治郎等角色已经不再登场了，取而代之的……
# 财经
### [今日财经TOP10|央行等部委发布金融支持粤港澳大湾区建设意见](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iirczymk1655139.shtml)
> 概要: 【宏观要闻】 NO.1 中国人民银行等部委发布关于金融支持粤港澳大湾区建设的意见（全文） 为深入贯彻党中央、国务院决策部署，经国务院同意，近日，中国人民银行...
### [中共中央政治局常务委员会召开会议 习近平主持](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iirczymk1642904.shtml)
> 概要: 原标题：中共中央政治局常务委员会召开会议 习近平主持 中共中央政治局常务委员会召开会议 分析国内外新冠肺炎疫情防控形势 研究部署抓好常态化疫情防控措施落地见效 研究...
### [习近平：北京市要加强全国“两会”期间疫情防控工作](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iirczymk1644390.shtml)
> 概要: 原标题：中共中央政治局常务委员会召开会议 习近平主持 中共中央政治局常务委员会召开会议 分析国内外新冠肺炎疫情防控形势 研究部署抓好常态化疫情防控措施落地见效 研究...
### [央行等部委：26条具体举措支持粤港澳大湾区建设](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iirczymk1640840.shtml)
> 概要: 原标题：央行等部委：26条具体举措支持粤港澳大湾区建设 为深入贯彻党中央、国务院决策部署，经国务院同意，近日，中国人民银行、银保监会、证监会...
### [广东明确住宿费退减 不得跨学年或学期预收](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iirczymk1640335.shtml)
> 概要: 广东明确住宿费退减 日前，广东省发布通知，明确学费（保教费）、住宿费均不得跨学年或学期预收，已收取的住宿费，学校在本学期结束前合理退减。
### [习近平：决不能让来之不易的疫情防控成果前功尽弃](https://finance.sina.com.cn/china/gncj/2020-05-14/doc-iircuyvi3115023.shtml)
> 概要: 中共中央政治局常务委员会召开会议分析国内外新冠肺炎疫情防控形势研究部署抓好常态化疫情防控措施落地见效研究提升产业链供应链稳定性和竞争力中共中央总书记习近平主持会...
# 科技
### [使用PyAudio和Numpy在Python中进行实时音频分析](https://www.ctolib.com/tr1pzz-Realtime_PyAudio_FFT.html)
> 概要: 使用PyAudio和Numpy在Python中进行实时音频分析
### [无人驾驶车道/路线检测](https://www.ctolib.com/littlemountainman-modeld.html)
> 概要: 无人驾驶车道/路线检测
### [一个基于DSSM的Lookalike向量化召回pipeline简单实现](https://www.ctolib.com/wangzhegeek-DSSM-Lookalike.html)
> 概要: 一个基于DSSM的Lookalike向量化召回pipeline简单实现
### [用于3D车道检测的综合数据集](https://www.ctolib.com/yuliangguo-3D_Lane_Synthetic_Dataset.html)
> 概要: 用于3D车道检测的综合数据集
### [手把手教你做数据运营与管理](https://www.tuicool.com/articles/yYjQvmf)
> 概要: 本文将为大家介绍什么是数据运营、它对应的层次，以及数据运营需要学会什么、需要用到什么工具。对于企业的 IT 人员来说，最痛苦的事情莫过于面对业务的各种各样的需求，IT 人员要在繁忙的开发任务中抽时间来......
### [Chepy：一款基于CyberChef工具的Python库&命令行实现](https://www.tuicool.com/articles/ryUZjaq)
> 概要: Chepy是一款基于CyberChef工具的Python库&命令行实现，它是一个Python库/命令行，实现了跟CyberChef工具相同的功能。Chepy是一个带有完整命令行接口的Python库，它......
### [为何对快乐的研究，源自人类最深重的创伤？](https://www.tuicool.com/articles/INFbUnz)
> 概要: 本文来自微信公众号：神经现实（ID：neureality），作者：Daniel Horowitz，翻译：NZ各种TED演讲、每日冥想练习、年度世界幸福报告、关于如何保持积极心态的建议……身边的一切都在......
### [2020年了，“汽配佬”赚钱的方式也应该升级了 | 超级观点](https://www.tuicool.com/articles/JRjEr2Y)
> 概要: 带着观点看商业。超级观点，来自新商业践行者的前沿观察。文 | 特约观察员 曾万贵编辑 | 黄臻曜特约观察员 曾万贵行业的先天不足中国是个万国车市场，100多个车品牌，1600多个车系，超过20万个车型......
### [荣耀平板V6真机现身：搭载华为麒麟985](https://www.ithome.com/0/487/157.htm)
> 概要: IT之家5月14日消息 根据长安数码君的消息，荣耀平板V6将搭载麒麟985芯片，还支持手写笔。IT之家曾报道，昨天荣耀智慧生活官方微博公布了荣耀平板V6，号称这是全球首款同时支持5G和Wi-Fi 6联......
### [魅族手机 Meizu Pay 开启大规模免费开卡活动，先到先得](https://www.ithome.com/0/487/162.htm)
> 概要: IT之家5月14日消息 魅族Flyme官方近期宣布，Meizu Pay 多城市交通卡限量免卡费，名额有限，用完为止。打开「 Meizu Pay 」即可看到福利活动，有需要的用户可以尽快领取。此前IT之......
### [Facebook与中国移动建设海底电缆，让更多非洲人上网](https://www.ithome.com/0/487/217.htm)
> 概要: 北京时间5月14日晚间消息，据国外媒体报道，Facebook正在非洲建设一条大型海底电缆，以便让非洲大陆13亿居民中的更多人上网。报道称，Facebook与中国移动、南非最大的电信运营商MTN、法国O......
### [小米生态链/升级4头按摩，脊安适G3护颈仪229元（减270元）](https://lapin.ithome.com/html/digi/487213.htm)
> 概要: 小米生态链/升级4头按摩，脊安适G3护颈仪报价499元，限时限量270元券，实付229元包邮，领券并购买。三红店铺，赠运费险。有品商城G2老款特价359元。天猫旗舰店G3新款报价429元。新品G3系列......
# 小说
### [走过从前](http://book.zongheng.com/book/723198.html)
> 作者：易水川

> 标签：都市娱乐

> 简介：那一年风雪飘零的季节，我在草原上答应过一个女孩子将来哪天我心血来潮了，就用我笨拙的脑壳把散落的文字堆积成句子，再排列起来，那里面流淌着她的爱情，她的故事。她说只有将美好的东西刻写在纸张上，她的爱情才能不朽。她不知道，她的爱情永远不朽，至少在我心里是不朽的。

> 章节末：第二十三章：天国之路

> 状态：完本
### [玩转游戏世界](http://book.zongheng.com/book/791415.html)
> 作者：在你眼眸之外

> 标签：评论文集

> 简介：一款传统的西式网游，四大种族，六大类职业，光明和黑暗两大阵营，每隔千年的信仰之战。故事，从上一个千年之战开始……

> 章节末：第四十九章 尾声

> 状态：完本
# 游戏
### [虚幻5引擎显卡跑得动吗？Epic：RTX 2070S可运行](https://3c.3dmgame.com/show-14-12512-1.html)
> 概要: Epic Games在昨晚公布了虚幻引擎5和实时演示，引起了大家的热议。在接受外媒Digital Foundry的采访时，Epic高管Nick Penwarden表示这段在PS5上运行的虚幻5演示采用......
### [王老菊新作截图曝光：2D卡通风RPG游戏 画风还不错](https://www.3dmgame.com/news/202005/3788543.html)
> 概要: 之前我们报道过B站知名UP主王老菊在去年5月宣布自建团队做游戏，游戏类型偏模拟经营，内容涉及到“未来科技宇宙”概念。之后便没有了任何关于新作的消息。今日(5月14日)王老菊在微博上晒出了新作截图，从图......
### [《绝地求生》大更加入Bot机器人引不满 破坏游戏体验](https://ol.3dmgame.com/news/202005/26229.html)
> 概要: 《绝地求生》大更加入Bot机器人引不满 破坏游戏体验
### [《绣春刀》导演路阳新片使用了虚幻4：期待虚幻5](https://www.3dmgame.com/news/202005/3788533.html)
> 概要: 昨晚，Epic公布虚幻5引擎的首个Demo立马成为了全球最热门的话题之一。当然，大家也知道之前的大热剧《星球大战：曼达洛人》就使用了虚幻4引擎制作特效，这一次国内影视导演，曾执导过两部《绣春刀》的导演......
### [日本女性推理漫画家野间美由纪因病去世 享年59岁](https://www.3dmgame.com/news/202005/3788516.html)
> 概要: 株式会社白泉社5月14日今天宣布，女性推理漫画家野间美由纪因心脏病不治，已经于2020年5月2日去世，享年59岁。野间 美由纪（1960年12月4日－2020年5月2日）是日本漫画家。千叶县千叶市出身......
# 论文
### [Weight Poisoning Attacks on Pre-trained Models](https://paperswithcode.com/paper/weight-poisoning-attacks-on-pre-trained)
> 日期：14 Apr 2020

> 标签：SENTIMENT ANALYSIS

> 代码：https://github.com/neulab/RIPPLe

> 描述：Recently, NLP has seen a surge in the usage of large pre-trained models. Users download weights of models pre-trained on large datasets, then fine-tune the weights on a task of their choice.
### [Context-Transformer: Tackling Object Confusion for Few-Shot Detection](https://paperswithcode.com/paper/context-transformer-tackling-object-confusion)
> 日期：16 Mar 2020

> 标签：FEW-SHOT LEARNING

> 代码：https://github.com/Ze-Yang/Context-Transformer

> 描述：Few-shot object detection is a challenging but realistic scenario, where only a few annotated training images are available for training detectors. A popular approach to handle this problem is transfer learning, i.e., fine-tuning a detector pretrained on a source-domain benchmark.
### [Keyfilter-Aware Real-Time UAV Object Tracking](https://paperswithcode.com/paper/keyfilter-aware-real-time-uav-object-tracking)
> 日期：11 Mar 2020

> 标签：OBJECT TRACKING

> 代码：https://github.com/vision4robotics/KAOT-tracker

> 描述：Correlation filter-based tracking has been widely applied in unmanned aerial vehicle (UAV) with high efficiency. However, it has two imperfections, i.e., boundary effect and filter corruption.
