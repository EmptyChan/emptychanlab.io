---
title: 2023-03-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.NYCClouds_ZH-CN2585785154_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-27 22:22:15
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.NYCClouds_ZH-CN2585785154_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [小产品如何做出大生意，小报童产品体验分析报告](https://www.woshipm.com/evaluating/5786091.html)
> 概要: 小报童是一个付费内容服务，有两种付费模式，订阅制和买断制。本文作者对小报童这个产品的形式、优势等方面进行了分析，想要了解小报童的小伙伴们，一起来看一下吧。作为一名产品经理，进行产品分析是非常有必要的，......
### [互联网变天：ChatGPT长出“操控”的手和脚，但这只是开始](https://www.woshipm.com/it/5791176.html)
> 概要: 近日，有关ChatGPT的一则重磅消息又传出了，即OpenAI将ChatGPT正式接入了互联网，而这意味着未来可能会有更多应用可以集成至OpenAI的生态当中，这于开发者们而言可能是机会，不过对于其他......
### [你没做过我们行业，凭什么给我做营销咨询？](https://www.woshipm.com/marketing/5791731.html)
> 概要: 不知你是否有过以下疑问：自己的业务自己不能做吗，为什么要找咨询公司？没做过这个行业，凭什么给人做咨询？能保证成果吗？本文作者从咨询公司的视角、核心作用两方面出发对这些问题进行了解答，不妨来看看。公司开......
### [CGI BMW M4 G82](https://www.zcool.com.cn/work/ZNjQ2MjcxODA=.html)
> 概要: 原来有很多事情、即便是能力再强、也不是个人能掌控的、于是就微笑着让该发生的发生、该来的来、该去的去，该说的说、该闭的闭，该猜的猜、该想的想，该写的写、该停的停、......
### [汽车市场价格战为何愈演愈烈？当前车市热点剖析](https://finance.sina.com.cn/jjxw/2023-03-27/doc-imynhefm8669898.shtml)
> 概要: 汽车市场价格战，愈演愈烈......
### [马斯克为什么不爽OpenAI？亲自接管被拒 负气甩手走人](https://finance.sina.com.cn/tech/2023-03-27/doc-imynhefp5362669.shtml)
> 概要: 炒股就看金麒麟分析师研报，权威，专业，及时，全面，助您挖掘潜力主题机会......
### [推特称部分源代码在网上泄露，已要求GitHub删除](https://finance.sina.com.cn/stock/usstock/c/2023-03-27/doc-imynhefi2868980.shtml)
> 概要: 新浪科技讯 北京时间3月27日早间消息，据报道，部分推特源代码泄露，数据已经出现在网络上......
### [36氪独家 | 零售咖啡品牌「隅田川咖啡」获数亿元C轮融资](https://36kr.com/p/2188140580438145)
> 概要: 36氪独家 | 零售咖啡品牌「隅田川咖啡」获数亿元C轮融资-36氪
### [36氪首发 ｜通信芯片公司「芯迈微半导体」完成Pre-A+轮融资，5G 芯片Q3流片，4G 芯片Q4量产](https://36kr.com/p/2188416394199429)
> 概要: 36氪首发 ｜通信芯片公司「芯迈微半导体」完成Pre-A+轮融资，5G 芯片Q3流片，4G 芯片Q4量产-36氪
### [头部车企加速收割市场，合资品牌“囚徒困境”加剧](https://www.yicai.com/news/101712678.html)
> 概要: 地方政府对车企价格体系与汽车产业生态的打破，起到的将是加速行业洗牌的催化剂作用。
### [正和岛发布案例战略 关注产业助力乡村振兴的新样本——国晶酒业](http://www.investorscn.com/2023/03/27/106472/)
> 概要: 3月24-26日，正和岛“2023企业家新年大课暨正和岛千企助桂发展行”在广西举办，近千位企业家齐聚南宁，从战略、组织、产业、资本、品牌、管理等众多维度，探讨“高韧性时代”的企业生存与发展之路。大会上......
### [睡前故事](https://www.zcool.com.cn/work/ZNjQ2MzIwNjg=.html)
> 概要: 嫦娥玉兔日常系列《广寒宫冷漫画》31-35话......
### [FITURE沸彻魔镜携手前滩太古里，“不插电运动”助力“地球一小时”](http://www.investorscn.com/2023/03/27/106474/)
> 概要: 3月25日，运动健康生活方式品牌FITURE沸彻魔镜于上海前滩太古里举办“地球一小时之春日健身”主题活动，为本地消费者带来了一场别开生面的运动派对。当天，在前滩太古里顶层户外草坪的活动现场，共有百余名......
### [天然小分子药物大有作为，多靶点全方位治疗慢病，有望从源头提高生命质量](http://www.investorscn.com/2023/03/27/106475/)
> 概要: 一直以来，科学家们对天然药物的研究从未停止；随着人们对健康生活诉求的提升，近些年在此领域的研究热度更是日益高涨。天然药物也在人类医药领域扮演着越来越重要的角色......
### [TRPG改动画电影《狂气山脉》众筹破亿 克苏鲁风](https://www.3dmgame.com/news/202303/3865724.html)
> 概要: 由个人制作者打造，克苏鲁风格TRPG改动画电影《狂气山脉》近日开启众筹，短短5天支援金已经突破1.3亿日元，影片正式版也将稍后确定推出，敬请期待。·动画电影《狂气山脉》于2月28日开启众筹，截至3月2......
### [尚智×空刻意面｜2022年度包装设计合集｜食品包装设计](https://www.zcool.com.cn/work/ZNjQ2MzM4NjA=.html)
> 概要: Play VideoPlayreplay 10 secondsforward 10 secondsCurrent Time0:00/Duration Time0:00Progress: NaN%Pla......
### [《无名之辈2》立项备案 饶晓志此前曾透露讯息](https://ent.sina.com.cn/m/c/2023-03-27/doc-imynhvcf5028576.shtml)
> 概要: 新浪娱乐讯 3月27日消息，据国家电影局关于2023年2月下全国电影剧本（梗概）备案、立项公示的通知，电影《无名之辈2》立项备案，饶晓志、王莉莉编剧。　　剧情梗概：小厂主老闻的货款被南美某国客商卷走潜......
### [《暗黑4》玩家希望有离线模式：不想排队和掉线](https://www.3dmgame.com/news/202303/3865728.html)
> 概要: 许多暗黑粉丝已经厌倦了《暗黑破坏神4》必须全程联网的要求，他们呼吁开发者加入离线模式，让玩家可以离线玩游戏。因为暗黑服务器不给力，让玩家不得不面对漫长排队和频繁断线问题。在国外Reddit论坛暗黑区，......
### [海外new things | 麻省理工创业公司「Pickle」推出卡车卸货机械臂，每小时能进行600次拣选卸货](https://36kr.com/p/2180566714677250)
> 概要: 海外new things | 麻省理工创业公司「Pickle」推出卡车卸货机械臂，每小时能进行600次拣选卸货-36氪
### [加油萝卜4发布](https://www.zcool.com.cn/work/ZNjQ2MzU2ODg=.html)
> 概要: 加油萝卜4发布自主品牌：品牌方向／卡通品牌设计／卡通表情设计创意设计团队：互萌文化丨HOMOE版权归属：此设计最终版权归属为互萌文化所有......
### [国内首款「CDXP」GrowKnows正式亮相，助力品牌向「体验」要增长](http://www.investorscn.com/2023/03/27/106477/)
> 概要: 国内首款「CDXP」GrowKnows正式亮相，助力品牌向「体验」要增长
### [后援会辟谣赵丽颖将出演《那个不为人知的故事》](https://ent.sina.com.cn/v/m/2023-03-27/doc-imynhzkx2410909.shtml)
> 概要: 新浪娱乐讯 27日，......
### [海外New Things | 主攻技能提升与评估的 「Workera」公司获得2350万美元的B轮融资](https://36kr.com/p/2189460868350089)
> 概要: 海外New Things | 主攻技能提升与评估的 「Workera」公司获得2350万美元的B轮融资-36氪
### [《名侦探柯南:贝克街的亡灵》预售总票房突破100万](https://www.3dmgame.com/news/202303/3865741.html)
> 概要: 根据第三方专业版数据显示，《名侦探柯南：贝克街的亡灵》映前8天预售总票房现已突破100万，这打破了全国艺联上映影片预售破百万最快纪录，该片将于4月4日在在全国艺术院线上映，4月11日起在全国影院上映......
### [视频：刘宇宁被爆已婚有女 本人开直播回应称没孩子](https://video.sina.com.cn/p/ent/2023-03-27/detail-imynhzmc4916136.d.html)
> 概要: 视频：刘宇宁被爆已婚有女 本人开直播回应称没孩子
### [机构今日买入这20股，抛售中国化学6.08亿元](https://www.yicai.com/news/101713265.html)
> 概要: 当天机构净买入前三的股票分别是中文在线、软通动力、掌阅科技，净买入金额分别是2.27亿元、1.70亿元、9967万元。
### [腾讯：《全境封锁2》国服终极测试将上线！不删档](https://www.3dmgame.com/news/202303/3865745.html)
> 概要: 今日(3月27日)腾讯发布WeGame游戏之夜阵容揭秘第三弹，预热线索里提到的射击玩家不容错过的3A大作，就是《全境封锁2》！官方原文：特工集结，做好准备！开放世界合作射击端游《全境封锁2》国服“终极......
### [父母教养方式如何影响校园欺凌？](https://www.huxiu.com/article/944240.html)
> 概要: 本文来自微信公众号：中国青年研究 （ID：china-youth-study），作者：朱晓文（西安交通大学社会学系教授）、王凯丽（西安交通大学社会学系博士研究生）、任围（西安交通大学社会学系博士研究生......
### [北向资金今日净卖出6.94亿元，长电科技、宁德时代等获加仓](https://www.yicai.com/news/101713306.html)
> 概要: 前十大成交股中，净买入额居前三的是长电科技、宁德时代、兖矿能源，分别获净买入3.41亿元、3.09亿元、2.71亿元。
### [可盐可甜：多鲜岩烧乳酪吐司 1.04 斤 12.9 元](https://lapin.ithome.com/html/digi/682609.htm)
> 概要: 【多鲜旗舰店】小心奶酪溢出来：多鲜岩烧乳酪吐司面包 520g 整箱 日常售价 29.9 元，下单立减 2 元，现领取 15 元优惠券，实付 12.9 元包邮。天猫多鲜岩烧乳酪吐司面包 520g 整箱下......
### [权威专家详解科技部“人工智能驱动的科学研究”专项部署工作](https://finance.sina.com.cn/china/2023-03-27/doc-imynimyt2185383.shtml)
> 概要: 新华社北京3月27日电题：紧扣基础学科关键问题 紧抓重点领域科研需求——权威专家详解科技部“人工智能驱动的科学研究”专项部署工作
### [美国银行一周流失千亿美元存款，资金寻求更避险去处](https://www.yicai.com/news/101713355.html)
> 概要: 在美国银行存款大量外流之际，美国货币市场基金资金流入规模连续两周突破千亿大关。
### [1-2月中国发行新增地方政府债券10677亿元](https://finance.sina.com.cn/china/2023-03-27/doc-imynimyu1781384.shtml)
> 概要: 中新社北京3月27日电 （记者 赵建华）中国财政部27日公布的数据显示，今年1-2月，全国发行新增地方政府债券10677亿元（人民币，下同），其中一般债券2408亿元...
### [“异托邦”成功举办｜ZIFEI WANG 23AW新品发布会](http://www.investorscn.com/2023/03/27/106487/)
> 概要: 2023年3月26日，ZIFEI WANG 2023秋冬系列新品发布会——“异托邦”于中国国际时装周成功举办，品牌创始人兼创意总监王紫菲出席大秀。本次秀场以人与自然的关系为探讨核心，通过建筑解构的形式......
### [马云回国现身杭州云谷学校，在外云游一年都做了什么？](https://finance.sina.com.cn/stock/roll/2023-03-27/doc-imynimyv8555263.shtml)
> 概要: 海外云游近一年，阿里巴巴集团创始人马云终于回到国内。 3月27日，马云现身杭州，和阿里巴巴高管会面，并参观云谷学校，讨论新一轮技术变革对教育带来的挑战与机遇。
### [报告：武汉数字经济发展水平居全国前列](https://finance.sina.com.cn/china/2023-03-27/doc-imynimyv8550726.shtml)
> 概要: 2023年，武汉数字经济核心产业增加值占比力争提升到11%左右。 多地抢占数字经济新赛道，武汉以“光芯屏端网云智”为特色的数字产业体系已经成型。
### [脱不下长衫的背后，是高等教育的沉没成本困境](https://www.huxiu.com/article/950950.html)
> 概要: 本文来自微信公众号：风声OPINION （ID：ifengopinion），作者：贾拥民（均衡研究所学术顾问、浙江大学跨学科中心特约研究员），编辑：刘军，原文标题：《风声｜孔乙己的长衫与范进的沉没成本......
### [去看《铃芽之旅》前，最好先知道这些](https://www.huxiu.com/article/952227.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。继《你的名字》之后，“你的椅子”又一次引......
### [Counterpoint 研究：苹果 iPhone 13 成为 2022 年中国最畅销智能手机](https://www.ithome.com/0/682/629.htm)
> 概要: IT之家3 月 27 日消息，苹果iPhone 13是 2022 全年中国市场最畅销的智能手机，iPhone13 系列占据了前 10 大销量中的三个机型。中国的智能手机市场对苹果来说是非常重要的市场，......
### [新疆长绒棉，浪莎男士纯棉透气平角裤 9.7 元 / 条](https://lapin.ithome.com/html/digi/682631.htm)
> 概要: 【浪莎官方旗舰店】新疆长绒棉，浪莎男士纯棉透气平角裤 4 条报价 49 元，限时限量 10 元券，实付 39 元包邮。还可凑单叠加天猫新势力周跨店每满 300 减 30 元优惠，相当于 9 折优惠，折......
### [美国模特BrandyGordon福利图赏 凹凸有致曲线火辣](https://www.3dmgame.com/bagua/5886.html)
> 概要: 今天为大家带来的是美国名模Brandy Gordon的福利图。妹子拥有凹凸有致的身材，精致的五官。她经常在ins上分享自己的美照，吸引近80万粉丝关注。Brandy Gordon于1995年出生，纤腰......
### [国资委：坚定不移做强做优做大国有资本和国有企业](https://finance.sina.com.cn/china/2023-03-27/doc-imynishr2058684.shtml)
> 概要: 来源：财联社 财联社3月27日讯，据国资委网站消息，3月27日，国资委党委学习贯彻习近平新时代中国特色社会主义思想和党的二十大精神研讨班开班...
### [马云赶巧回来了](https://www.huxiu.com/article/799683.html)
> 概要: 作者｜周超臣头图｜云谷教育视频截图马云周游列国的日子暂时告一段落。3月27日，周一中午，马云从被传出已经回国到被确认，也就几小时的光景。先是一张显示时间是27日早晨6点多的短视频截图，有网友称今天一大......
### [A股三大指数走势分化 商务部：中国将与世界共享中国大市场机遇](https://www.yicai.com/video/101713614.html)
> 概要: A股三大指数走势分化 商务部：中国将与世界共享中国大市场机遇
### [用 ChatGPT 秒建大模型，OpenAI 全新插件杀疯了，接入代码解释器一键 get](https://www.ithome.com/0/682/647.htm)
> 概要: ChatGPT 可以联网后，OpenAI 还火速介绍了一款代码生成器，在这个插件的加持下，ChatGPT 甚至可以自己生成机器学习模型了。上周五，OpenAI 刚刚宣布了惊爆的消息，ChatGPT 可......
# 小说
### [从亮剑开始崛起](https://m.qidian.com/book/1024784863/catalog)
> 作者：我的头像是猫

> 标签：战争幻想

> 简介：狗作者幼儿园未毕业，被毒死概不负责…………

> 章节总数：共795章

> 状态：完本
# 论文
### [Unsupervised Domain Adaptation for Cross-Modality Retinal Vessel Segmentation via Disentangling Representation Style Transfer and Collaborative Consistency Learning | Papers With Code](https://paperswithcode.com/paper/unsupervised-domain-adaptation-for-cross-2)
> 概要: Various deep learning models have been developed to segment anatomical structures from medical images, but they typically have poor performance when tested on another target domain with different data distribution. Recently, unsupervised domain adaptation methods have been proposed to alleviate this so-called domain shift issue, but most of them are designed for scenarios with relatively small domain shifts and are likely to fail when encountering a large domain gap.
### [Seeing your sleep stage: cross-modal distillation from EEG to infrared video | Papers With Code](https://paperswithcode.com/paper/seeing-your-sleep-stage-cross-modal)
> 日期：11 Aug 2022

> 标签：None

> 代码：https://github.com/spiresearch/sacd

> 描述：It is inevitably crucial to classify sleep stage for the diagnosis of various diseases. However, existing automated diagnosis methods mostly adopt the "gold-standard" lectroencephalogram (EEG) or other uni-modal sensing signal of the PolySomnoGraphy (PSG) machine in hospital, that are expensive, importable and therefore unsuitable for point-of-care monitoring at home. To enable the sleep stage monitoring at home, in this paper, we analyze the relationship between infrared videos and the EEG signal and propose a new task: to classify the sleep stage using infrared videos by distilling useful knowledge from EEG signals to the visual ones. To establish a solid cross-modal benchmark for this application, we develop a new dataset termed as Seeing your Sleep Stage via Infrared Video and EEG ($S^3VE$). $S^3VE$ is a large-scale dataset including synchronized infrared video and EEG signal for sleep stage classification, including 105 subjects and 154,573 video clips that is more than 1100 hours long. Our contributions are not limited to datasets but also about a novel cross-modal distillation baseline model namely the structure-aware contrastive distillation (SACD) to distill the EEG knowledge to infrared video features. The SACD achieved the state-of-the-art performances on both our $S^3VE$ and the existing cross-modal distillation benchmark. Both the benchmark and the baseline methods will be released to the community. We expect to raise more attentions and promote more developments in the sleep stage classification and more importantly the cross-modal distillation from clinical signal/media to the conventional media.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
