---
title: 2020-06-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GorchFock_EN-CN2672694129_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-06-25 18:39:46
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GorchFock_EN-CN2672694129_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [加州迪士尼乐园重开计划推迟 园方：我们别无选择](https://ent.sina.com.cn/m/f/2020-06-25/doc-iirczymk8891879.shtml)
> 概要: 新浪娱乐讯 据外媒报道，前不久宣布将于7月17日重开的加州迪士尼乐园，受加州新冠疫情影响，将推迟重开的时间。　　加州迪士尼乐园在本月初宣布将于7月17日阶段性重开，但受加州疫情持续恶化影响，迪士尼在当......
### [组图：宋慧乔现身刘亚仁新片首映礼 晒黑白照低调表支持](http://slide.ent.sina.com.cn/film/k/slide_4_704_340909.html)
> 概要: 组图：宋慧乔现身刘亚仁新片首映礼 晒黑白照低调表支持
### [组图：《爱我就别想太多》今开播 陈建斌李一桐潘粤明追求真爱](http://slide.ent.sina.com.cn/tv/slide_4_704_340924.html)
> 概要: 组图：《爱我就别想太多》今开播 陈建斌李一桐潘粤明追求真爱
### [杨丞琳追《乘风破浪的姐姐》到凌晨 夸宁静美炸天](https://ent.sina.com.cn/music/zy/2020-06-25/doc-iirczymk8865945.shtml)
> 概要: 新浪娱乐讯 25日凌晨4点多，杨丞琳追完《乘风破浪的姐姐》在微博发文，“追综艺追到此刻，宁静姐姐真的美炸天，我也要跟她一样保持地这么好。”粉丝们纷纷表示，“姐你已经保持得很好了”，也有人笑称原来大家都......
### [IU公司提告恶意留言者 酸民呛：个人意见不能写吗](https://ent.sina.com.cn/s/j/2020-06-25/doc-iirczymk8913385.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，韩星IU（李知恩）所属经纪公司EDAM娱乐24日宣布提告所有毁谤者，并说明了已经针对屡犯多次的酸民提告，相关人士也都完成了传唤调查。怎料，25日传出有被提告的人非但没反省，......
### [腾讯新闻星推榜一周岁，李紫婷送生日祝福](https://new.qq.com/zt/template/?id=ENT2020061901570200)
> 概要: 腾讯新闻星推榜一周岁，李紫婷送生日祝福
### [要被黄圣依这个少奶奶笑死！多么清纯的起点，如今满脸刻薄相](https://new.qq.com/omn/20200625/20200625A00FCV00.html)
> 概要: 要被黄圣依这个少奶奶笑死！多么清纯的起点，如今满脸刻薄相
### [助理尽职尽责为杨幂打伞！最终却让杨幂“买了单”](https://new.qq.com/omn/20200625/20200625A0854A00.html)
> 概要: 助理尽职尽责为杨幂打伞！最终却让杨幂“买了单”
### [刘德华很少演坏人，一演坏人就很吓人](https://new.qq.com/omn/20200625/20200625A03AA500.html)
> 概要: 刘德华很少演坏人，一演坏人就很吓人
# 动漫
### [EVA×猫咪大战争联动6月25日复刻决定！](https://news.dmzj.com/article/67775.html)
> 概要: 日本手游《猫咪大战争》与《EVA》联动将于6月25日重新开始。在过去的合作活动中登场的所有角色、舞台将再次登场。时间为7月2日上午10点59分。
### [剧场版《紫罗兰永恒花园》重新定档，宣布将于9月18日上映](https://news.dmzj.com/article/67773.html)
> 概要: 剧场版《紫罗兰永恒花园》宣布重新定档，将于9月18日上映
### [《名侦探柯南》工藤新一和毛利兰粘土人登场](https://news.dmzj.com/article/67774.html)
> 概要: 青山刚昌老师的人气推理漫画《名侦探柯南》中，工藤新一和毛利兰以粘土人的姿态登场！
### [斗罗：昊天的名号到底多具威名？要知道，这6位强者都曾被吓惨！](https://new.qq.com/omn/20200624/20200624A0UPXS00.html)
> 概要: 在斗罗大陆这片土地之上，不缺乏强者，但像唐昊这样的强者，确实是世间少有。当初的唐昊，无疑就是昊天宗里最靓丽的崽。在他还只有魂斗罗级别的时候，他就拥有宗门的传承魂骨，资源什么的只要需要，完全满足供应。……
### [《日本沉没2020》片头主题曲曝光 7月9日Acfun放送](https://acg.gamersky.com/news/202006/1299581.shtml)
> 概要: Netflix Japan公开了动画《日本沉没2020》的片头主题曲。
### [《紫罗兰永恒花园》新作剧场版再定档 9月18日上映](https://acg.gamersky.com/news/202006/1299532.shtml)
> 概要: 京都动画制作的《紫罗兰永恒花园》新作剧场版宣布延期上映。官方宣布，《紫罗兰永恒花园》新作剧场版重新定档，将于9月18日在日本上映。
# 财经
### [五角大楼认定华为等20家中企“与中国军方有关” 移动电信均在列](https://finance.sina.com.cn/china/gncj/2020-06-25/doc-iircuyvk0408341.shtml)
> 概要: 五角大楼认定华为等20家中企“与中国军方有关”，我学者：这是“乱找理由”的威胁与打压 【环球时报-环球网报道 记者 赵觉珵】为了打压中国企业，美国真是动作不断...
### [1780元卖核酸检测名额 北京警方查获9名涉案人员](https://finance.sina.com.cn/china/gncj/2020-06-25/doc-iirczymk8933469.shtml)
> 概要: 原标题：1780元卖核酸检测名额 北京警方查获9名涉案人员 来源：新京报 北京市公安局副局长、新闻发言人潘绪宏介绍，6月19日我局发布编造传播涉疫谣言典型案例以来...
### [一图看北京14天新增269例确诊情况](https://finance.sina.com.cn/china/gncj/2020-06-25/doc-iircuyvk0409484.shtml)
> 概要: 责任编辑：郭建......
### [山西大同发现特大型石墨矿床 资源量约1亿吨](https://finance.sina.com.cn/china/gncj/2020-06-25/doc-iirczymk8932764.shtml)
> 概要: 原标题：山西大同发现特大型石墨矿床 资源量约1亿吨 新华社太原6月25日电（记者王飞航）记者从山西省地质勘查局获悉，这个省日前在大同市新荣区发现一处特大型石墨矿床...
### [违规填海被罚8000万 三亚新机场为何十年未落成？](https://finance.sina.com.cn/china/gncj/2020-06-25/doc-iircuyvk0402449.shtml)
> 概要: 原标题：违规填海被罚8000万，三亚新机场为何十年未落成？被罚款的违规项目现状，图片来源：三亚新机场海洋环境影响报告 记者 | 唐俊 近日...
### [4人卖核酸检测名额被行拘！北京通报14例病例轨迹](https://finance.sina.com.cn/wm/2020-06-25/doc-iircuyvk0406645.shtml)
> 概要: 6月25日下午，北京市召开疫情防控第132场例行新闻发布会。北京市疾病预防控制中心副主任庞星火介绍6月24日北京新增确诊病例相关情况：6月24日0时至24时...
# 科技
### [A tiny (800 B) utility to simplify web vitals reporting.](https://www.ctolib.com/treosh-web-vitals-reporter.html)
> 概要: A tiny (800 B) utility to simplify web vitals reporting.
### [精简版本的vue-element-admin](https://www.ctolib.com/dailinfeng66-vue-element-admin-silmlify.html)
> 概要: 精简版本的vue-element-admin
### [RDKit中文教程](https://www.ctolib.com/autodataming-RDKitBook_zhCN.html)
> 概要: RDKit中文教程
### [UpToDocs扫描Markdown文件中的PHP代码块，并在一个单独的进程中执行每个代码块](https://www.ctolib.com/mathiasverraes-uptodocs.html)
> 概要: UpToDocs扫描Markdown文件中的PHP代码块，并在一个单独的进程中执行每个代码块
### [如果你到现在还不了解 Go 并发模型](https://www.tuicool.com/articles/AvAzQf3)
> 概要: 今天我们来了解一下 Go 语言的协程并发机制，这也可能是 Go 语言最为吸引人的特性了，了解它的原理和底层机制对于掌握 Go 语言大有裨益，话不多说开始吧！并发和并行并发和并行都是为了充分利用 CPU......
### [阿里洛神云网络这局妥了！集齐“软硬结合、创新NFV、智能管理”三张牌](https://www.tuicool.com/articles/NrMnuqi)
> 概要: 晶少 发自 凹非寺量子位 报道 | 公众号 QbitAI导语：在云网络风靡的今天，洛神云网络作为阿里云飞天的虚拟网络系统核心，目前在软硬一体、NFV平台创新以及智能性方面究竟有何新意？对用户与开发者的......
### [Jetpack App Startup: keep your startup logic organized](https://www.tuicool.com/articles/vmayaem)
> 概要: I doubt that theres an app in production that does not initialize a number of objects.This usually t......
### [迪士尼英语“败走”中国，中途离场真的“冤”？](https://www.tuicool.com/articles/aEJr6vb)
> 概要: 创业者要永远保持居安思危的心态。出品 | 创业最前线作者 | 黄燕华责编 | 蛋总“迪士尼英语关门了，太吃惊，女儿在里面学了好几年，突然没了，不知道去哪里学口语。教育机构艰难啊，迪士尼都熬不住。”6月......
### [微软新 Edge 企业同步服务现可用于 Microsoft 365 商业高级版](https://www.ithome.com/0/494/660.htm)
> 概要: IT之家6月25日消息 微软官方博客近期宣布，Microsoft Edge企业同步服务现在可用于Microsoft 365商业高级版（以前称为Microsoft 365商业版）订阅用户。如果你在202......
### [欣兴电子有望成苹果自研 Mac 处理器主要 ABF 载板供应商](https://www.ithome.com/0/494/672.htm)
> 概要: 6 月 25 日消息，据国外媒体报道，苹果公司在当地时间周一的全球开发者大会上，已公布了基于 ARM 架构自研 Mac 处理器的计划，首款自研芯片 Mac 计划今年年底出货，将在两年的时间里完成过渡，......
### [Facebook 宣布正式停售 Oculus Go VR 产品：搭载骁龙 821](https://www.ithome.com/0/494/673.htm)
> 概要: Oculus Go VR 头戴设备于 2017 年 10 月推出，但直到 2018 年 5 月才开始销售。在发售两年后，Facebook 近日宣布将停售 Oculus Go 产品。Oculus Go ......
### [喜加二！Epic 今晚免费领取游戏《AER：古老的回忆》《怪奇物语 3》](https://www.ithome.com/0/494/644.htm)
> 概要: IT之家6月25日消息 Epic今晚将开启新的「喜加一」活动。本周免费领取游戏包括《AER：古老的回忆》与《怪奇物语3》，免费领取时间为6月25日至7月2日。AER：古老的回忆IT之家了解到，在《AE......
# 小说
### [御剑仙瑶](http://book.zongheng.com/book/404101.html)
> 作者：桥月仍在

> 标签：武侠仙侠

> 简介：一位如蝼蚁般的山野少年，一条漫长而充满艰险的修炼道路，一个充满奇幻却精彩的修真世界。当经过岁月的沉淀，且行且歌的他终于御剑于九天云中，俯视众生。（书群号737044509，欢迎各位大神读者进群。）

> 章节末：第一百五十三章 我与梅花两白头（大结局）

> 状态：完本
# 游戏
### [小高和刚《终结降临》今日正式发售 游戏支持中文](https://www.3dmgame.com/news/202006/3791637.html)
> 概要: 根据官方公开的新消息，由小高和刚出品的真人电影互动游戏《终结降临（Death Come True）》已经在NS日服解锁，游戏支持中文，售价1960日元。目前，官方还公开了一段主题曲视频，一起来了解一下......
### [IGN游戏之夏：《生化变种》10分钟实机视频公开](https://www.3dmgame.com/news/202006/3791636.html)
> 概要: 在IGN游戏之夏活动上，开发商Experiment 101公开了《生化变种》最新版本的10分钟实机视频。在里面创意总监Stefan Ljungqvist告诉IGN，《生化变种》开发工作快要完成，目前正......
### [《堡垒之夜》将免费播放诺兰电影 含《盗梦空间》等](https://ol.3dmgame.com/news/202006/27101.html)
> 概要: 《堡垒之夜》将免费播放诺兰电影 含《盗梦空间》等
### [韩系战术《 Remnants of Gray》公布 2022年发售](https://www.3dmgame.com/news/202006/3791632.html)
> 概要: 由韩国发行商LineGames和开发商StudioReg制作的战术RPG游戏《The War of Genesis: Remnants of Gray》正式公开，并发布了预告，我们一起来看看。《The......
### [网友认为“屌”太粗鄙 周杰伦：老子讲了20年 就是屌](https://www.3dmgame.com/news/202006/3791662.html)
> 概要: 近日篮球名将林书豪在社交平台发表了一段赛后感言，而其好友周杰伦留言一个“屌”字评价。有网友回复认为这是“粗鄙之语”，却获周杰伦再度回复“老子讲了20年 就是屌”，还配了一个笑哭的表情。网友看到纷纷评论......
# 论文
### [Data Mining in Clinical Trial Text: Transformers for Classification and Question Answering Tasks](https://paperswithcode.com/paper/data-mining-in-clinical-trial-text)
> 日期：30 Jan 2020

> 标签：ENTITY EXTRACTION

> 代码：https://github.com/L-ENA/HealthINF2020

> 描述：This research on data extraction methods applies recent advances in natural language processing to evidence synthesis based on medical texts. Texts of interest include abstracts of clinical trials in English and in multilingual contexts.
