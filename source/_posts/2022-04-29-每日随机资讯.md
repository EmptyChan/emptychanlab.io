---
title: 2022-04-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RedwoodSprout_ZH-CN6224667074_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-04-29 18:59:11
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RedwoodSprout_ZH-CN6224667074_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Visual Studio 2022 v17.1.6 发布](https://www.oschina.net/news/193526/visual-studio-2022-17-1-6-released)
> 概要: Visual Studio 2022 v17.1.6 正式发布，更新内容如下：修复了 C++ 编译器（CL.exe）在运行代码分析位值枚举检查时有时会崩溃的问题；修正了 iPhone Sumulato......
### [Brave 浏览器 1.38 发布，引入 De-AMP 隐私保护功能](https://www.oschina.net/news/193532/brave-1-38-released)
> 概要: 基于 Chromium 的 Brave 浏览器已发布 1.38 版本，该版本引入了De-AMP 重定向功能，以及重新设计的 Shields 面板。De-AMP 隐私保护Brave 浏览器认为谷歌的 A......
### [用于写交互式小说的 Inform 7 编程语言正式开源](https://www.oschina.net/news/193544/inform-7-open-source)
> 概要: Inform 7是一种使用自然语言语法创建交互式小说的编程语言，曾多次跻身 TIOBE 指数编程语言前 100 之列。目前，Inform 7 编程语言及其组件已正式宣布在 GitHub 上基于 Art......
### [Spring Tools 4.14.1 发布](https://www.oschina.net/news/193530/spring-tools-4-14-1-released)
> 概要: Spring Tools 4.14.1 已发布。Spring Tools 4 是由 Spring 团队打造的 Spring 开发工具，从零开始构建，融合了现代技术和开发者工具架构。它在单独的进程中运行......
### [深度学习走进死胡同了？](http://www.woshipm.com/it/5419048.html)
> 概要: 编辑导语：近年来，深度强化学习成为一个被业界和学术界追捧的热门技术，社区甚至将它视为圣杯，大多数人都看好它未来发展的巨大潜力。但是，在一片追捧声中，终于有人开始质疑深度强化学习的真实作用。难道深度学习......
### [朴朴超市，困在福州](http://www.woshipm.com/it/5418738.html)
> 概要: 编辑导语：生于福州的朴朴超市，凭着“纯线上运营+前置仓配送”模式，在生鲜赛道成长为“小巨头”。当朴朴走出福州，迈向全国，“福州样本”能成功复制吗？消费类赛道，大多拥有强领地意识，各互联网企业也多以城为......
### [从微信读书到免费网文，聊聊我眼中的「移动阅读」](http://www.woshipm.com/it/5414460.html)
> 概要: 编辑导语：互联网的兴起使手机阅读的方式越来越流行，因此移动阅读行业也渐渐发展起来，而移动阅读本质上是对内容的消费。本篇文章中作者分享了自己作为产品从业者对移动阅读行业的思考，一起来看看。为什么写这篇文......
### [《幻塔》2.0翻红，完美世界仍不完美](https://www.tuicool.com/articles/7NF7naa)
> 概要: 《幻塔》2.0翻红，完美世界仍不完美
### [Snap launches its first drone](https://pixy.com/)
> 概要: Snap launches its first drone
### [为“安利25周年”绘制系列插画主题创作](https://www.zcool.com.cn/work/ZNTk1MjU4Mjg=.html)
> 概要: “安利25周年”插画主题创作：畅游Rich＆Jay的创业之旅!考虑到客户商业属性所以采用了能让客户信息快速表达同时又具有抽象元素的商业化风格合作很愉快绘制完成一稿过～动图处放上了客户文案以供参照，谢谢......
### [The Twelve-Factor App (2011)](https://12factor.net/)
> 概要: The Twelve-Factor App (2011)
### [美团“困”在团购里](https://www.huxiu.com/article/541048.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国“为了活下去，美团正围绕团购业务进行人员精简、业务收缩，过去一年烧了200多亿，这么烧下去难以为继。”4月25日，一位消息人士告诉虎嗅，美团创始人王兴的妻......
### [由红杉中国领投的M20 Genomics完成数千万元pre-A轮融资](https://www.tuicool.com/articles/UZfAziF)
> 概要: 由红杉中国领投的M20 Genomics完成数千万元pre-A轮融资
### [《雷神4：爱与雷霆》新剧照曝光 雷神索尔冥想中](https://www.3dmgame.com/news/202204/3841365.html)
> 概要: 今日(4月29日)《雷神4：爱与雷霆》新剧照曝光，雷神索尔在寻找内心平静的过程中变得更加成熟。他正在冥想中，看起来也瘦了很多。《雷神4》讲述的是雷神踏上了一个他从未面对过的旅程——寻找内心的平静。但他......
### [轻小说《冰剑的魔术师将会统一世界》TV动画化](https://news.dmzj.com/article/74286.html)
> 概要: 轻小说《冰剑的魔术师将会统一世界》宣布了TV动画化决定的消息。本作预计将于2023年1月开始播出。
### [恋 恋 不 减 ｜毛三插画合集9p](https://www.zcool.com.cn/work/ZNTk1MjgyNzY=.html)
> 概要: 本次展示的这部分作品是三月中到四月末的画作，相信很多人在这个时间段都面对着非常严峻的生活状况，我也是。个人经济状况下滑和疫情隔离，让人时常感觉自己是一座孤岛。对话外界的时间变少了，与自己相处的时间更多......
### [《名侦探柯南：万圣节的新娘》将引进国内 今年上映](https://acg.gamersky.com/news/202204/1479487.shtml)
> 概要: 剧场版《名侦探柯南：万圣节的新娘》目前正在日本火热上映中。据华策影视2021年财报显示，剧场版《名侦探柯南：万圣节的新娘》将引进国内，计划在2022年上映。
### [漫画杂志「onBLUE」封面图「春风的异邦人」公开](http://acg.178.com/202204/445200596932.html)
> 概要: 近日，漫画杂志「onBLUE」宣布将在6月24日正式发售vol.59，并公开了本期杂志封面图，绘制的角色来自「春风的异邦人」。「春风的异邦人」是「海边的异邦人」的续作漫画，剧情始于小说家骏离开了冲绳离......
### [股民很伤心！马斯克又出售440万股特斯拉股票 承诺“这次卖完收手”](https://finance.sina.com.cn/tech/2022-04-29/doc-imcwiwst4691560.shtml)
> 概要: 编辑/史正丞......
### [You Should Compile Your Python and Here’s Why](https://glyph.twistedmatrix.com/2022/04/you-should-compile-your-python-and-heres-why.html)
> 概要: You Should Compile Your Python and Here’s Why
### [东西南北中，限购在放松](https://www.huxiu.com/article/542633.html)
> 概要: 本文来自微信公众号：经济观察报 （ID：eeo-com-cn），作者：符小茵 田国宝，题图来自：视觉中国4月27日，继郑州、衢州、秦皇岛、南京、苏州等多城放松楼市限购后，珠三角、京津冀重要城市佛山、廊......
### [美的｜立式空气循环电风扇](https://www.zcool.com.cn/work/ZNTk1MzAyODg=.html)
> 概要: 上市时间｜2022多功能立式空气循环电风扇，立式，台式两用。立地广域送风，畅享清凉舒适。快速搅动气流，平衡屋内温度，让冷气分布更均匀。产品策略｜产品设计｜创新结构研发 | 产品落地跟进场景渲染｜平面设......
### [漫画「炎炎消防队」第34卷封面公开](http://acg.178.com/202204/445203525683.html)
> 概要: 漫画「炎炎消防队」公开了最终卷第34卷的封面图，该卷将于5月17日正式发售。「炎炎消防队」是大久保笃创作的漫画作品，在杂志「周刊少年Magazine」上进行连载，目前已完结。本作曾获得2017年“全国......
### [中国移动回应](https://finance.sina.com.cn/tech/2022-04-29/doc-imcwiwst4690935.shtml)
> 概要: 新浪科技讯 4月29日上午消息，今日，中国移动通过微博发文称，2022年4月28日晚，136号段部分客户收到130开头号码（主叫号码后8位与被叫号码后8位一致）发来的乱码短信，发现问题后，公司立即启动......
### [移动用户收到乱码短信？破案了，联通测试导致！](https://finance.sina.com.cn/tech/2022-04-29/doc-imcwipii7144573.shtml)
> 概要: 相关新闻：中国移动：部分用户收到乱码短信是中国联通在系统升级测试......
### [有意思周报｜约翰尼·德普和前妻艾梅柏的离婚世纪大战进行中；美国一大学开设毛片鉴赏课，价值2学分](https://www.huxiu.com/article/542936.html)
> 概要: 趣事打捞员｜黄瓜汽水、渣渣郡、木子童画板报的｜渣渣郡有意思周报是由几位那個NG编辑，凭借过剩激情和表达欲创造出的产物，其内容大多是充满趣味的边角料新闻。在这趟信息分享之旅中，只要你觉得有意思，我们就美......
### [「回转企鹅罐」十周年纪念剧场版动画后篇主视觉图公开](http://acg.178.com/202204/445208385687.html)
> 概要: 「回转企鹅罐」公开了10周年纪念剧场版「RE:cycle of the PENGUINDRUM」后篇「僕は君を愛してる」的主视觉图，本作将于2022年7月22日上映，前篇「君の列車は生存戦略」已于今日......
### [「回转企鹅罐」10周年纪念·水晶公主手办今日开订](http://acg.178.com/202204/445208939911.html)
> 概要: 「回转企鹅罐」10周年纪念·水晶公主手办1/7比例现已开启预订，作品采用PVC和ABS材质，全高约230mm，日版售价28,380日元（含税），约合人民币1434元，预计将于2022年12月发售......
### [视频：38岁苍井空带双胞胎泡温泉 晒孕期视频分享育儿经验](https://video.sina.com.cn/p/ent/2022-04-29/detail-imcwipii7159723.d.html)
> 概要: 视频：38岁苍井空带双胞胎泡温泉 晒孕期视频分享育儿经验
### [组图：假期快乐！热巴晒出游造型 戴小蓝帽嘟嘴如洋娃娃](http://slide.ent.sina.com.cn/star/slide_4_704_369059.html)
> 概要: 组图：假期快乐！热巴晒出游造型 戴小蓝帽嘟嘴如洋娃娃
### [消息称三星 4 月份向闻泰科技追加超过 1000 万部手机 ODM 订单，为 2023 年新项目](https://www.ithome.com/0/615/917.htm)
> 概要: 4 月 28 日，据中国证券报的消息显示，三星 4 月份向闻泰科技追加超过1000 万部手机ODM（原始设计制造）订单。而据笔者从供应链处获悉，这个追加的超 1000 万部手机订单，是三星 2023 ......
### [动画电影《怪盗皇后喜欢马戏团》预告片](https://news.dmzj.com/article/74292.html)
> 概要: 动画电影《怪盗皇后喜欢马戏团》公开了正式预告片。在这次的预告片中，伴随着诹访部顺一的旁白，可以看到众多本作中登场的角色。
### [二手、废料成抢手货，缺芯潮还未到顶？](https://www.huxiu.com/article/541218.html)
> 概要: 出品| 虎嗅科技组作者| 丸都山头图| IC Photo“ASML的CEO说国外已经有公司开始拆解洗衣机芯片了。”“不错了，至少他们还有的拆。”上午10点05分，王涛的航班从新郑机场准时起飞，如无意外......
### [游戏《格林魔书 OnceMore》公开新PV](https://news.dmzj.com/article/74293.html)
> 概要: 日本一公开了即将于7月28日发售的RTS类游戏《格林魔书 OnceMore》（PS4 / Nintendo Switch）的新PV。在这次的PV中，可以看到在魔法学校学习召唤使魔等的战斗系统的片段。
### [Sturdy (YC W21) Is Hiring (Stockholm, Sweden)](https://getsturdy.com/careers)
> 概要: Sturdy (YC W21) Is Hiring (Stockholm, Sweden)
### [我爸送的名牌包，被室友当场抓包是“假货”！？](https://www.zcool.com.cn/work/ZNTk1MzQyODA=.html)
> 概要: 我爸送的名牌包，被室友当场抓包是“假货”！？
### [假期旅行备一套，洛克数据线转接头套装 + 收纳盒 29 元好价](https://lapin.ithome.com/html/digi/615923.htm)
> 概要: 洛克这款数据线 + 转接头 + 收纳盒套装，在一个小巧的收纳盒中包含一根 C to C PD 数据线 25cm（最高支持 60W 快充），一个 Type-C 转 Mocro 转接头，一个 Type-C......
### [苹果提前拉响警报](https://www.tuicool.com/articles/BRZbqyF)
> 概要: 苹果提前拉响警报
### [组图：演员小李琳素颜现身北京机场 戴渔夫帽穿棒球夹克随性休闲](http://slide.ent.sina.com.cn/star/slide_4_86512_369069.html)
> 概要: 组图：演员小李琳素颜现身北京机场 戴渔夫帽穿棒球夹克随性休闲
### [《春逝百年抄》售前宣传片公布 5月13日上线](https://www.3dmgame.com/news/202204/3841400.html)
> 概要: 今日（4月29日），SE公布真人悬疑推理游戏《春逝百年抄》售前宣传片，该作将于支持简中，计划于5月13日登陆 steam、Switch、PS5|4 平台，，Steam国区原价298元，感兴趣的玩家点击......
### [监管加密货币：一场事关未来权力的战争](https://www.tuicool.com/articles/aUFzYji)
> 概要: 监管加密货币：一场事关未来权力的战争
### [换屏要 5089 元，华为 Mate Xs 2 折叠旗舰维修价格出炉](https://www.ithome.com/0/615/946.htm)
> 概要: IT之家4 月 29 日消息，昨晚，华为正式发布了新款折叠屏 Mate Xs 2 手机，其中 8GB+256GB 售价 9999 元、8GB+512GB 售价 11499 元、12GB+512GB 典......
### [宁德时代：一季度净利润14.93亿元，同比下降23.62%](https://finance.sina.com.cn/tech/2022-04-29/doc-imcwiwst4732478.shtml)
> 概要: 新浪科技 4月29日下午消息，宁德时代公布第一季度业绩。一季度，宁德时代营收486.8亿元，同比增长153.97%；净利润14.93亿元，同比下降23.62%，扣非净利润同比下降41.57%......
### [《长津湖之水门桥》上线首播！看之前不妨先了解这三点](https://new.qq.com/rain/a/20220204V050X700)
> 概要: 《长津湖之水门桥》上线首播！看之前不妨先了解这三点
### [上海及各区社会面清零进展](https://www.yicai.com/news/101398238.html)
> 概要: None
### [梅根福克斯性感写真美图欣赏 逐渐“卡戴珊”化？](https://www.3dmgame.com/bagua/5329.html)
> 概要: 近日35岁的美国女星梅根·福克斯分享了《Glamour》杂志4月刊写真美图。从图中可以看到她身穿不同紧身长裙，凸显完美身材，性感又迷人。网友直呼梅根·福克斯已逐渐“卡戴珊”化。她的造型和前几年《变形金......
### [Epic要求法院禁止谷歌从商店中移除旗下音乐软件](https://www.3dmgame.com/news/202204/3841402.html)
> 概要: Epic Games 日前已提出一项初步禁令申请，以阻止谷歌从安卓 Play 商店中删除独立音乐店面 Bandcamp。谷歌显然威胁要这样做，因为 Bandcamp 使用自己的支付系统，而不是向谷歌支......
### [曝三星显示 CEO 会面库克，要求苹果保证 OLED 面板采购量以应对“iPhone 减产 15%”传闻](https://www.ithome.com/0/615/975.htm)
> 概要: IT之家4 月 29 日消息，TheLec 消息称，三星显示 CEO 崔周善（Joo-Sun Choi）飞赴美国会见苹果 CEO 蒂姆・库克。在得知苹果决定将今年的 iPhone 年产量削减 15% ......
### [疫情扰动难撼稳增长,政治局会议重磅定调经济工作发力点](https://www.yicai.com/news/101398287.html)
> 概要: 政策“稳”的要求再次明确。
### [蛤蟆寨隐藏礼包大放送！快来打卡《开局一座山》X大玩家百城应援](https://new.qq.com/omn/ACF20220/ACF2022042900382900.html)
> 概要: 此路是我开，此树是我栽！充满干劲的少年哟～这部开局人口三、物资一座山，不墨迹不套路不降智的《开局一座山》，你入坑了吗？4月15日，由企鹅影视、腾讯动漫联合出品，启缘映画担任制作的热血爆笑动画《开局一座......
### [乌克兰局势威胁全球粮食安全,MC12前农业谈判步伐缓慢](https://www.yicai.com/news/101398392.html)
> 概要: 乌克兰局势影响了全球粮食、化肥和能源贸易。
### [让《约战》动画党懵圈的问题，十香的姓氏“夜刀神”是怎么来的？](https://new.qq.com/omn/20220429/20220429A09F8B00.html)
> 概要: 然而近期，有人问了星河一个这样的问题，那就是在《约会大作战》这部作品中，五河士道最初见到十香的时候，仅仅只是为她取名为“十香”。那十香的姓氏“夜刀神”是怎么来的？            看到这个问题的......
### [生日月，豆神贺——这七个人也太让人羡慕惹！](https://new.qq.com/omn/20220429/20220429A09NYX00.html)
> 概要: 亲爱的豆神小伙伴            时光 是一圈又一圈的年轮岁月 是一盏又一盏的弓杯芳菲四月 温暖与共7位豆神小伙伴迎来了他们的生日月欢欢喜喜 热热闹闹豆神动漫也送上了暖心的祝福愿你们此生 梦想光......
### [华为折叠旗舰MateXs2维修价格出炉：换屏要5089元](https://www.3dmgame.com/news/202204/3841411.html)
> 概要: 近日华为新一代折叠屏旗舰Mate Xs 2正式发布，提供8GB+256GB、8GB+512GB、12GB+512GB典藏版三款配置，售价分别为9999元、11499元、12999元。华为官网已经有了该......
### [游戏王历史：环境历史断章 加农炮兵被禁的理由](https://news.dmzj.com/article/74295.html)
> 概要: 这篇文章主要对加农炮兵被禁的理由进行分析。
### [为什么自来也不带木叶护额？油字护额代表什么？有什么特殊含义？](https://new.qq.com/omn/20220429/20220429A0A3PI00.html)
> 概要: 动漫火影忍者中，自来也被很多粉丝们亲切地称为“油叔”，最主要的原因就是自来也经常佩戴的那个护额，自来也的护额在整部动漫中是独一无二的，可是，自来也本身就是木叶村的忍者，为什么他没有带木叶的护额，而是选......
### [外交部：经中朝友好协商，临时暂停丹东至新义州口岸铁路货运](https://finance.sina.com.cn/china/2022-04-29/doc-imcwiwst4761408.shtml)
> 概要: 4月29日，外交部发言人赵立坚主持例行记者会。有记者就近期中朝有关合作安排提问。图源：外交部 赵立坚表示，鉴于当前丹东疫情形势，中朝双方经过友好协商...
### [ARM安谋中国430多名员工发布团队公开信：反对软银强势夺取公司控制权](https://finance.sina.com.cn/stock/usstock/c/2022-04-29/doc-imcwipii7213024.shtml)
> 概要: IT之家4 月 29 日消息，今日，安谋科技（ARM 中国）管理层及 430 多名员工联名签署了团队公开信。公开信称，对于近期外媒报道的软银集团正试图夺取安谋科技控制权...
### [承德答“严控中考移民”建议：计划今年取消外市民办高中在承招生](https://finance.sina.com.cn/jjxw/2022-04-29/doc-imcwiwst4763006.shtml)
> 概要: 澎湃新闻记者 吕新文 据河北省承德市政府网站4月29日消息，承德市政府办公室公布了对河北省第十三届人民代表大会第五次会议陈日红代表提出的“高中制跨区域招生政策下严控中...
### [洛阳：首套房贷款最低首付比例20%，公积金最高贷65万元](https://finance.sina.com.cn/money/bank/bank_hydt/2022-04-29/doc-imcwipii7209385.shtml)
> 概要: 澎湃新闻记者 计思敏 4月29日，据“洛阳发布”，《洛阳市促进房地产市场持续平稳健康发展的若干措施》已正式发布，将从5月1日起施行，有效期2年。
### [中共交通运输部党组召开（扩大）会议传达学习中共中央政治局会议精神](https://finance.sina.com.cn/china/gncj/2022-04-29/doc-imcwipii7211021.shtml)
> 概要: 中共交通运输部党组召开（扩大）会议 传达学习中共中央政治局会议精神 部长、部党组副书记李小鹏主持会议 4月29日下午，中共交通运输部党组召开（扩大）会议...
### [证监会：强化退市程序衔接，优化退市板块挂牌流程](https://www.yicai.com/news/101398460.html)
> 概要: 强化退市程序衔接，畅通交易所退出机制，完善主办券商承接安排，简化确权登记程序，优化退市板块挂牌流程，推动退市公司平稳顺畅进入退市板块
### [今年已有超100城发布稳楼市政策，包括多个省会](https://finance.sina.com.cn/china/gncj/2022-04-29/doc-imcwipii7212110.shtml)
> 概要: 4月份，各地不断出台稳楼市的政策。 4月28日晚间发布了《贵阳市人民政府办公厅关于促进房地产业良性循环和健康发展的通知》（下称《通知》），将从企业成本、信贷需求...
### [美国国会参议院计划召马斯克参加听证会，他们想问什么？](https://www.yicai.com/news/101398468.html)
> 概要: 马萨诸塞州民主党参议员马基呼吁推出新的立法限制科技巨头公司。
### [顶级投资人丨瑞士百达财富管理CIO：我希望投资中国股票但需要一些稳定性](https://www.yicai.com/news/101398470.html)
> 概要: 近期中国A股市场大幅波动，裴睿思表示，他目前看好中国债券的价值，而中国股票的仓位比较轻。他希望投资中国股票，但需要一些稳定性。
# 小说
### [猎魂使](http://book.zongheng.com/book/1127861.html)
> 作者：青竹墨雨

> 标签：悬疑灵异

> 简介：猎魂使，巡阴人，阴阳师，鬼见愁他，每隔一段时间都会沉睡，或十年，或百年，或追着阴邪而来，或踏着胜利归去，听，他醒了，一段属于他的故事也开始了。

> 章节末：完结章   从哪里来，回哪里去

> 状态：完本
# 论文
### [Localization Distillation for Object Detection | Papers With Code](https://paperswithcode.com/paper/localization-distillation-for-object-1)
> 日期：12 Apr 2022

> 标签：None

> 代码：None

> 描述：Previous knowledge distillation (KD) methods for object detection mostly focus on feature imitation instead of mimicking the classification logits due to its inefficiency in distilling the localization information. In this paper, we investigate whether logit mimicking always lags behind feature imitation. Towards this goal, we first present a novel localization distillation (LD) method which can efficiently transfer the localization knowledge from the teacher to the student. Second, we introduce the concept of valuable localization region that can aid to selectively distill the classification and localization knowledge for a certain region. Combining these two new components, for the first time, we show that logit mimicking can outperform feature imitation and the absence of localization distillation is a critical reason for why logit mimicking underperforms for years. The thorough studies exhibit the great potential of logit mimicking that can significantly alleviate the localization ambiguity, learn robust feature representation, and ease the training difficulty in the early stage. We also provide the theoretical connection between the proposed LD and the classification KD, that they share the equivalent optimization effect. Our distillation scheme is simple as well as effective and can be easily applied to both dense horizontal object detectors and rotated object detectors. Extensive experiments on the MS COCO, PASCAL VOC, and DOTA benchmarks demonstrate that our method can achieve considerable AP improvement without any sacrifice on the inference speed. Our source code and pretrained models are publicly available at https://github.com/HikariTJU/LD.
### [MFAQ: a Multilingual FAQ Dataset | Papers With Code](https://paperswithcode.com/paper/mfaq-a-multilingual-faq-dataset)
> 日期：27 Sep 2021

> 标签：None

> 代码：None

> 描述：In this paper, we present the first multilingual FAQ dataset publicly available. We collected around 6M FAQ pairs from the web, in 21 different languages.
