---
title: 2023-06-18-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TernFather_ZH-CN1860589914_1920x1080.webp&qlt=50
date: 2023-06-18 23:47:41
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TernFather_ZH-CN1860589914_1920x1080.webp&qlt=50)
# 新闻
### [直面内容焦虑，美团直播转入冲刺期](https://www.woshipm.com/it/5849489.html)
> 概要: 面对如今本地生活赛道“硝烟”四起，美团直面焦虑，在直播赛道上奋勇直追。从年初内测到四月美团官方账号开始直播，美团直播的一系列布局究竟有没有成效？让我们跟着作者的数据一起往下看吧。美团在直播赛道又传出了......
### [GPT-5可能会被GPT-4“背刺”？AI训AI或成「剧毒」，会让模型崩溃](https://www.woshipm.com/ai/5849537.html)
> 概要: 生成式AI技术正在被引入各行各业，甚至在AI行业中，有人已经开始使用AI生成的数据来训练AI。那么，这一动作可能会给语言模型带来什么样的后果？我们是否还需要真实的“人类数据”？不妨一起来看看本文的解读......
### [Vision Pro：VR 行业黎明之前最亮的烛火？](https://www.woshipm.com/it/5849502.html)
> 概要: 前段时间苹果WWDC大会上发布的头显产品Apple Vision Pro，无疑引起了VR界的狂欢与热切讨论。那么综合Vision Pro的各方面来看，Vision Pro的出现意味着什么？这款头显产品......
### [印度人口超过中国无需担忧，专家：印度劳动力规模比我们少2.6亿](https://finance.sina.com.cn/jjxw/2023-06-18/doc-imyxrmuc7958558.shtml)
> 概要: 每经记者 张蕊每经编辑 陈星 卢祥勇 6月16日，中国人口与发展研究中心组织召开“一带一路”人口与发展研讨会暨《“一带一路”人口与发展（第二辑）》新书发布会。
### [经济日报金观平：“东数西算”重在协同发展](https://finance.sina.com.cn/jjxw/2023-06-18/doc-imyxsarq2965840.shtml)
> 概要: “东数西算”工程启动一年多以来，地方政府、运营商、设备厂商等在新型基础设施建设、应用场景拓展及技术研发方面抓紧布局，产业发展取得一定成绩。
### [腾讯入股，这家快递黑马要上市了，三年营收近千亿，你用过吗？](https://36kr.com/p/2306471582903813)
> 概要: 腾讯入股，这家快递黑马要上市了，三年营收近千亿，你用过吗？-36氪
### [Babycare踏入「无人之境」：当一家新消费公司开出第100家门店](https://36kr.com/p/2306462459227649)
> 概要: Babycare踏入「无人之境」：当一家新消费公司开出第100家门店-36氪
### [《星空》为何锁30fps？海量三明治或成主要原因](https://www.3dmgame.com/news/202306/3871585.html)
> 概要: 近日《星空》总监Todd Howard证实，这款开放世界RPG新作在Xbox Series X/S主机上仅以30fps运行。虽然有玩家对此倍感失望，但也有人认为，这款游戏锁30fps的理由很充分，而且......
### [6月LPR报价将公布，这些投资机会最靠谱](https://finance.sina.com.cn/roll/2023-06-18/doc-imyxspfp3627308.shtml)
> 概要: 下周共有逾50家公司限售股陆续解禁，6月20日1年期和5年期以上LPR报价将公布，下周共有5只新股发行。 【重磅新闻】 LPR报价下调概率大 6月20日...
### [袁弘祝贺胡歌拿上影节最佳男演员：恭喜影帝](https://ent.sina.com.cn/m/c/2023-06-18/doc-imyxspfr0405015.shtml)
> 概要: 新浪娱乐讯 6月17日，胡歌凭《不虚此行》获上海电影节金爵奖最佳男演员，好友袁弘转发微博祝贺，表示：“恭喜影帝”。(责编：小万)......
### [《最终幻想16》三位制作人评选心目中的最佳FF游戏](https://www.3dmgame.com/news/202306/3871590.html)
> 概要: 《最终幻想16》三位制作人近日接受IGN采访，透露了自己心目中排名前三的《最终幻想》游戏。让人颇感意外的是，三位制作人的选择里都没有《最终幻想7》，而是选择了一些玩家群体中热度相对较低的《最终幻想》游......
### [父爱如山，父权也如山](https://www.huxiu.com/article/1694293.html)
> 概要: 本文来自微信公众号：界面文化 （ID：BooksAndFun），主持人：林子人，编辑：黄月、尹清露，头图来自：《饮食男女》剧照这个月初我就打算做一个父亲节选题，探讨一下人们对父职的最新看法。但联系了几......
### [中国爸爸饮酒报告：最爱白酒，只求微醺](https://finance.sina.com.cn/china/gncj/2023-06-18/doc-imyxsxvi3418405.shtml)
> 概要: 酒，几乎是渗透在中国人文化血液里的饮料之一。 文人雅士好以茶迎客、以酒送别，民间也常说无酒不成席、无酒不成欢，不管是胸中有块垒，还是春风得意时...
### [中国公司全球化周报｜TikTok宣布将在东南亚投资数十亿美元；2022年我国跨境电商进出口规模首次突破2万亿元](https://36kr.com/p/2306599916612871)
> 概要: 中国公司全球化周报｜TikTok宣布将在东南亚投资数十亿美元；2022年我国跨境电商进出口规模首次突破2万亿元-36氪
### [暂时想不出名字的个人作品](https://www.zcool.com.cn/work/ZNjU2MDQ1NjQ=.html)
> 概要: 之后还尝试了用AE做了个小动画......
### [东实股份营收依赖“东风系”燃油车，如何迎接电动化时代？](https://www.yicai.com/news/101786018.html)
> 概要: 未披露“年度降价”详情以及对电动车企销售比例。
### [巴黎时装周上的中国县城](https://www.huxiu.com/article/1693867.html)
> 概要: 出品｜虎嗅商业消费组作者｜昭晰编辑｜苗正卿题图｜MARRKNULL AW21虎嗅注：今年，中国独立设计师品牌MARRKNULL入围了LVMH Prize 青年设计师大奖赛半决赛。MARRKNULL创始......
### [IPO前瞻：信芯微科创板IPO获受理，海信视像“A拆A”再进一步](https://www.yicai.com/news/101786023.html)
> 概要: 近一周A股IPO新增受理项目16家。
### [AI正在疯狂污染中文互联网](https://www.huxiu.com/article/1694957.html)
> 概要: 本文来自微信公众号：量子位 （ID：QbitAI），作者：金磊、尚恩，题图来自：视觉中国污染中文互联网，AI成了“罪魁祸首”之一。事情是这样的，最近大家不是都热衷于向AI咨询嘛，有位网友就问了Bing......
### [16GB显存直戳RTX 4070？AMD RX 7800 XT显卡定了](https://www.3dmgame.com/news/202306/3871602.html)
> 概要: AMD的RX 7900系列显卡已经将价格杀到5299元起，底部还有2149元起的RX 7600显卡，唯独中高端还是空白，这要等RX 7800系列了，预计秋季发布。RX 7800系列主要跟英伟达的RTX......
### [TV动画《我心里危险的东西》第二季制作决定 2024年1月播出](https://news.dmzj.com/article/78394.html)
> 概要: 由樱井纪雄原作漫画改编的TV动画《我心里危险的东西》（又译为：我心中的野兽）第二季制作决定，将于2024年1月播出。
### [组图：刘浩存休闲穿搭现身机场 白T配格子短裙青春洋溢](http://slide.ent.sina.com.cn/star/slide_4_704_386499.html)
> 概要: 组图：刘浩存休闲穿搭现身机场 白T配格子短裙青春洋溢
### [Netflix真人版《海贼王》中文预告片公开 8月31日播出](https://news.dmzj.com/article/78395.html)
> 概要: 由尾田荣一郎原作漫画改编拍摄制作的真人版《海贼王》预告片公开，将于8月31日Netflix独家播出。
### [中国燃气获“2023年度科技创新‘星’公司”](http://www.investorscn.com/2023/06/18/108282/)
> 概要: 日前,第十八届中国上市公司竞争力公信力调查评选在湖北省老河口市揭晓。本次调查评选主题为“奋进新征程锚定高质量发展”,160家上市公司和众多上市公司掌门人、逾百名上市公司董秘在本届评选中获得了投资者的认......
### [京东618产生“最多数量商品”订单 订单是60万颗焊钉](http://www.investorscn.com/2023/06/18/108283/)
> 概要: 京东618期间,交付了一笔包含了60万件商品的“最多数量商品”订单,订单是60万颗焊钉。这是来自四川省某钢铁企业通过京东工业采购了一批焊钉......
### [618战绩发布！美容仪黑马Jmoon极萌首战即夺冠！](http://www.investorscn.com/2023/06/18/108284/)
> 概要: 根据天猫官方公布的 618 销售成绩单，Jmoon极萌作为新锐美容美体仪品牌，在今年 618 年中大促中取得了令人瞩目的优异成绩......
### [新晋断货王！甜啦啦水果摇摇杯，5天全国销量超100万杯](http://www.investorscn.com/2023/06/18/108286/)
> 概要: 近日,记者于鲜果茶品牌甜啦啦方面确认,继五一小长假,爆品黑武士桑葚销量达到200万杯后,甜啦啦6月推出的水果摇摇杯,凭借新奇的杯型和玩法,上市五天销量超100万杯,接替黑武士桑葚,成为甜啦啦的新晋断货......
### [笃行不怠 接力前行 | 格力地产选派干部驻阳江开展对口帮扶协作](http://www.investorscn.com/2023/06/18/108288/)
> 概要: 笃行不怠 接力前行 | 格力地产选派干部驻阳江开展对口帮扶协作
### [这似曾相识的感觉 《星际迷航：无限》实机预告赏](https://www.3dmgame.com/news/202306/3871607.html)
> 概要: Paradox Entertainment和Nimble Giant Entertainment近日发布大型策略游戏新作《星际迷航：无限》（Star Trek: Infinite）的实机预告片。预告片......
### [组图：胡兵称50万积分被东航清零 航空公司客服回应](http://slide.ent.sina.com.cn/star/slide_4_704_386503.html)
> 概要: 组图：胡兵称50万积分被东航清零 航空公司客服回应
### [消息称奥之心今年将推出两支长焦镜头，其中之一为 50-200mm F2.8 PRO](https://www.ithome.com/0/700/637.htm)
> 概要: IT之家6 月 18 日消息，根据 43rumors 最新消息，奥之心预计将于今年年底推出两支长焦镜头，两支镜头的具体发售日期可能分别在今年年底与明年年初。爆料显示，其中一支镜头为 50-200mm ......
### [TV动画《等级S的冒险者》PV第1弹公开 2023年秋播出](https://news.dmzj.com/article/78396.html)
> 概要: TV动画《想当冒险者的女儿到大都市当了等级S的冒险者》PV第1弹与视觉图公开，将于2023年秋播出。
### [6月18日这些公告有看头](https://www.yicai.com/news/101786103.html)
> 概要: 6月18日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [街机游戏中不科学的格挡能力，身体也能挡子弹](https://www.ithome.com/0/700/651.htm)
> 概要: 二十年后，你是否还记得昔日《功夫》上映时，第一次看到火云邪神硬接子弹是什么感觉呢？说实话当时太震撼。毕竟这种情节往往只出现在科幻片之中。在武打片中，功夫再强也无法抗衡热武器。即使是武林高手，一旦被子弹......
### [任天堂 GBA 经典游戏《火焰纹章：封印之剑》《烈火之剑》将于 6 月 23 日加入 Nintendo Switch Online 会员](https://www.ithome.com/0/700/658.htm)
> 概要: IT之家6 月 18 日消息，任天堂宣布，经典游戏《火焰纹章：封印之剑》与《火焰纹章：烈火之剑》将于 6 月 23 日加入 Nintendo Switch Online 会员 GBA 游戏库。IT之家......
### [拍卖盛况不再，“疯狂的锂”进入博弈期](https://www.yicai.com/news/101786140.html)
> 概要: 截至6月16日，电池级碳酸锂均价已升破31万元/吨，较4月下旬低点反弹幅度达超75%，但相较于去年的疯狂水平相去甚远。
### [社论：创新需自由浇灌，而非政府主导](https://www.yicai.com/news/101786165.html)
> 概要: 中国经济需安装创新这一内驱力，因此政府的创新战略应立足于捍卫市场自由竞争秩序，并不断拓展企业家在市场的行动自由，用高水平改革开放，降低行政和交易成本。
### [国内首款市域 C 型动车组亮相：时速可达 160 公里](https://www.ithome.com/0/700/664.htm)
> 概要: IT之家6 月 18 日消息，国内首列时速 160 公里市域 C 型动车组在中车长客所属上海轨道交通设备发展有限公司正式亮相，未来将投运于上海市域铁路机场联络线。据介绍，这款车型基于复兴号智能动车组技......
### [2023亚布力论坛创新年会在晋启幕](https://finance.sina.com.cn/china/2023-06-18/doc-imyxtqta9885445.shtml)
> 概要: 来源：亚布力企业家论坛CEF 6月17-18日，2023亚布力论坛第九届创新年会在山西太原如期召开。本届创新年会以“开放·创新·转型——高质量发展的方向与路径”为主题...
### [《王国之泪》PC模拟器版支持4K/60帧 需RTX 4090](https://www.3dmgame.com/news/202306/3871617.html)
> 概要: 油管UP主“NoBigDeal La”上传了一段《塞尔达传说：王国之泪》4K/60帧的实际演示视频，所使用的显卡为RTX 4090。注意，此次运行成功的为原生4K，任天堂Switch模拟器Yuzu和R......
# 小说
### [刀逆风云](http://book.zongheng.com/book/1150608.html)
> 作者：狗诞

> 标签：武侠仙侠

> 简介：江湖暗潮起，提刀搅风云！世有百兵，剑为君子，枪为王者。而刀，却无一称。但我的刀不一样，我的刀，是百兵中的霸！霸道的霸，霸主的霸！即世无刀席，便以刀成名！大风来，狂风至！江南有一刀，二十载未出，其名为狂风，搅天下风云！

> 章节末：第七十章相遇

> 状态：完本
# 论文
### [Kformer: Knowledge Injection in Transformer Feed-Forward Layers | Papers With Code](https://paperswithcode.com/paper/kformer-knowledge-injection-in-transformer)
> 概要: Knowledge-Enhanced Model have developed a diverse set of techniques for knowledge integration on different knowledge sources. However, most previous work neglect the language model's own ability and simply concatenate external knowledge at the input. Recent work proposed that Feed Forward Network (FFN) in pre-trained language model can be seen as an memory that stored factual knowledge. In this work, we explore the FFN in Transformer and propose a novel knowledge fusion model, namely Kformer, which incorporates external knowledge through the feed-forward layer in Transformer. We empirically find that simply injecting knowledge into FFN can enhance the pre-trained language model's ability and facilitate current knowledge fusion methods. Our results on two benchmarks in the commonsense reasoning (i.e., SocialIQA) and medical question answering (i.e., MedQA-USMLE) domains demonstrate that Kformer can utilize external knowledge deeply and achieves absolute improvements in these tasks.
### [Synthetic Disinformation Attacks on Automated Fact Verification Systems | Papers With Code](https://paperswithcode.com/paper/synthetic-disinformation-attacks-on-automated)
> 概要: Automated fact-checking is a needed technology to curtail the spread of online misinformation. One current framework for such solutions proposes to verify claims by retrieving supporting or refuting evidence from related textual sources. However, the realistic use cases for fact-checkers will require verifying claims against evidence sources that could be affected by the same misinformation. Furthermore, the development of modern NLP tools that can produce coherent, fabricated content would allow malicious actors to systematically generate adversarial disinformation for fact-checkers. In this work, we explore the sensitivity of automated fact-checkers to synthetic adversarial evidence in two simulated settings: AdversarialAddition, where we fabricate documents and add them to the evidence repository available to the fact-checking system, and AdversarialModification, where existing evidence source documents in the repository are automatically altered. Our study across multiple models on three benchmarks demonstrates that these systems suffer significant performance drops against these attacks. Finally, we discuss the growing threat of modern NLG systems as generators of disinformation in the context of the challenges they pose to automated fact-checkers.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
