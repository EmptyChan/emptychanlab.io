---
title: 2022-02-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WheatonYukon_ZH-CN5573629391_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-25 23:13:42
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WheatonYukon_ZH-CN5573629391_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Spring Boot 2.5.10 发布](https://www.oschina.net/news/184018/spring-boot-2-5-10-released)
> 概要: Spring Boot 2.5.10 已经发布，该版本包括 52 个错误修复、文档改进和依赖性升级。错误修复默认的 JmxAutoConfiguration 改变了多属性@ManagedResourc......
### [Linux 开发人员讨论弃用和删除 ReiserFS](https://www.oschina.net/news/184022/linux-deprecation-reiserfs)
> 概要: 长期 Linux 内核开发人员 Matthew Wilcox 发起了一项“是时候删除 reiserfs 了吗？”的讨论。Wilcox 删除 ReiserFS 的动机是由于，他正在追求的内核基础设施的变......
### [15 年前提交到 Bugzilla 的请求，直到现在才关闭](https://www.oschina.net/news/184025/buzilla-bug-376855)
> 概要: Bugzilla 最初是由开发者 Terry Weissman 于 1998 年为Mozilla.org项目设计开发的，是一个基于 Web 的通用 bug 跟踪系统和测试工具。如今 Bugzilla ......
### [虚幻引擎 5 发布首个预览版](https://www.oschina.net/news/184015/unreal-engine-5-preview1-released)
> 概要: 虚幻引擎 5 (Unreal Engine) 现已推出预览版本。官方强调道，预览版本仍存在不稳定和其他问题，不建议用于生产。他们目前正在制作最终发布的版本，并且已准备好测试将要增加的所有功能。新版本除......
### [《这是我的战争》开发商决定卖游戏救助乌克兰难民](https://www.3dmgame.com/news/202202/3836663.html)
> 概要: 这几天国际上最受关注的事件恐怕就是俄罗斯与乌克兰之间的边境冲突问题，作为《这是我的战争》开发商11 bit studio今天（2月25日）凌晨发布公告，表示愿意将该游戏一周的销售所得捐赠给在本次冲突中......
### [阿里急需一场局部胜利](https://www.huxiu.com/article/501464.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜苗正卿题图｜视觉中国被视为“上市后最受关注的阿里财报”正式出炉。2月24日晚，阿里巴巴发布截至2022财年第三季度财报，财报显示季度内阿里营收2425.8亿元，同比增......
### [《一拳超人》重制版204话：“我的拳法”大功告成](https://acg.gamersky.com/news/202202/1462785.shtml)
> 概要: 《一拳超人》重制版204话公开，这一集就是饿狼单独对战大怪虫蜈蚣仙人，最后直接将大蜈蚣从头到脚完全劈开。并且在战斗中将自己的拳法完成了。
### [漫改真人电影「xxxHOLiC」正式预告公开](http://acg.178.com/202202/439753357790.html)
> 概要: 由CLAMP创作的同名漫画改编的真人电影「xxxHOLiC」公开了正式预告，本作将于2022年4月29日在日本地区上映。真人电影「xxxHOLiC」正式预告主题曲SEKAI NO OWARI「Habi......
### [「攻壳机动队 SAC_2045」第二季先导预告及主视觉图公开](http://acg.178.com/202202/439753589847.html)
> 概要: 动画「攻壳机动队 SAC_2045」第二季公开了先导预告及主视觉图，本作将于2022年5月在Netflix独家播出。「攻壳机动队 SAC_2045」第二季先导预告主视觉图：主题曲OP：「Secret ......
### [「无职转生」洛琪希·米格路迪亚手办正式开订](http://acg.178.com/202202/439755603024.html)
> 概要: 近日，「无职转生」洛琪希·米格路迪亚手办正式开启预订，作品采用ABS、PVC材质，全高约260mm，日版售价26400日元（含税），约合人民币1452元，预计将于2022年4月发售......
### [《怪物猎人:崛起》新活动任务 奖励“盛装”外观装备](https://www.3dmgame.com/news/202202/3836680.html)
> 概要: 卡普空2月25日发布了《怪物猎人：崛起》新活动任务“雷电阴阳”，完成该任务可以获得制作“盛装”外观装备的素材。任务详情：目的：狩猎1头雷狼龙和1头奇怪龙目的地：斗技场参加／承接条件：HR4以上报酬：可......
### [《艾尔登法环》直播观看人数已是《黑魂3》将近三倍](https://www.3dmgame.com/news/202202/3836681.html)
> 概要: Fromsoft 备受期待的开放世界魂系游戏《艾尔登法环》终于在全球范围内陆续解锁，大量玩家迫不及待地涌入这个全新的世界中。同时，也有许多玩家并没有选择自己亲自体验，而是去观看《艾尔登法环》直播。根据......
### [拳头重组亚太地区业务 将在东南亚等地区开设新办事处](https://www.3dmgame.com/news/202202/3836685.html)
> 概要: 据Fami通报道，拳头游戏公司（Riot Games）近日公布了亚太地区(APAC)业务重组的最新消息。此次业务重组将把在东南亚的事业扩大到包括日本在内的亚太地区，将在印度、菲律宾、印度尼西亚、马来西......
### [《xxxHOLiC》真人电影新预告 妖艳女郎蜘蛛登场](https://acg.gamersky.com/news/202202/1462881.shtml)
> 概要: 由CLAMP原作漫画改编的真人电影《xxxHOLiC》公开了全新的预告片，并且公开了新的海报，本作中的反派·女郎蜘蛛登场。
### [剧场版《新EVA》获得日本电影学院奖话题作品奖](https://news.dmzj.com/article/73702.html)
> 概要: 剧场版动画《新EVA》获得了第45届日本电影学院奖的话题作品奖。获奖仪式和对获奖者的采访，将于3月19日深夜的特别节目中播出。
### [视频：范景翔陈君发布《益起向未来》 公益行动致敬冬奥精神](https://video.sina.com.cn/p/ent/2022-02-25/detail-imcwiwss2834280.d.html)
> 概要: 视频：范景翔陈君发布《益起向未来》 公益行动致敬冬奥精神
### [真人电影《xxxHOLiC》公开预告片与海报](https://news.dmzj.com/article/73704.html)
> 概要: 根据CLAMP原作制作的真人电影《xxxHOLiC》公开了正式预告片。在这段预告片中，可以看到四月一日在被妖怪追逐时，与侑子相遇等的场景。
### [漫画「莉莉艾尔与祈祷之国」宣传PV公布](http://acg.178.com/202202/439770499425.html)
> 概要: 漫画「莉莉艾尔与祈祷之国」的宣传PV现已公布，本作是轻小说「魔女之旅」的番外篇。漫画「莉莉艾尔与祈祷之国」宣传PV漫画「莉莉艾尔与祈祷之国」由白石定规担任原作，あずーる担任角色原案，与「魔女之旅」为同......
### [《CS：GO》等多款游戏现已列入Deck支持列表](https://www.3dmgame.com/news/202202/3836708.html)
> 概要: V社于今天公布了最新一批获得 Steam Deck 支持的游戏，包括完全支持以及可玩两种支持形式。其中部分游戏为：· 《见证者（The Witness）》Thekla 2016 年发行的第一人称解谜游......
### [照片里的战争，普通人飘摇的命运](https://www.huxiu.com/article/501558.html)
> 概要: 本文来自微信公众号：GQ报道（ID：GQREPORT），作者：刘楚楚，编辑：李纯，题图来自：受访者今年年初，因为一则视频，Nadine Hwang在网络引起关注。视频的标题叫《寻找Nadine Hwa......
### [满岛光出演木村拓哉新剧 饰演单亲妈妈拳击部顾问](https://ent.sina.com.cn/2022-02-25/doc-imcwipih5319566.shtml)
> 概要: 新浪娱乐讯 据日本媒体Modelpress报道，人气女星满岛光出演木村拓哉主演四月开播朝日台剧集《迈向未来的10 Count》，饰演单亲妈妈和拳击部顾问，这也是她第一次与木村拓哉真正合作，称又开心又紧......
### [日本唯一一部反战电影，35万中国影迷打出8.7高分（上）](https://new.qq.com/rain/a/20210601V0EEEI00)
> 概要: 日本唯一一部反战电影，35万中国影迷打出8.7高分（上）
### [万代南梦宫已完成艾尔登法环商标注册](https://www.ithome.com/0/604/780.htm)
> 概要: IT之家2 月 25 日消息，近日，动作 RPG 游戏《艾尔登法环》游戏解锁上线引发热议。信息显示，“艾尔登法环”商标已于 2021 年 1 月被其发行公司万代南梦宫娱乐注册成功，国际分类包括教育娱乐......
### [当代职场玄学：加薪可以，升职不行](https://www.huxiu.com/article/501594.html)
> 概要: 本文来自微信公众号：塔门（ID：DT-Tamen），作者：张晨阳，编辑：王朝靖，题图来自：视觉中国在职场中，逃离中年危机噩梦的出路似乎只有一条，晋升、晋升，然后进入管理层。在各个公司、尤其是大公司，架......
### [一部揭露二战残酷的电影，父亲用一个谎言，为孩子挡下所有黑暗](https://new.qq.com/rain/a/20210814V08EPT00)
> 概要: 一部揭露二战残酷的电影，父亲用一个谎言，为孩子挡下所有黑暗
### [宫岛礼吏新作《紫云寺家的孩子们》开始连载](https://news.dmzj.com/article/73709.html)
> 概要: 创作了《租借女友》的宫岛礼吏，在本日（25日）发售的《YOUNG ANIMAL》（白泉社）5号上，开始了新作《紫云寺家的孩子们》的连载。
### [前一秒还活蹦乱跳，下一秒就被狙击手干掉，这就是战争的残酷！](https://new.qq.com/rain/a/20191225V0M9YX00)
> 概要: 前一秒还活蹦乱跳，下一秒就被狙击手干掉，这就是战争的残酷！
### [整天幻想打仗的人，应该看看这部电影，它会让你明白战争的残酷！](https://new.qq.com/rain/a/20200810V0OXIV00)
> 概要: 整天幻想打仗的人，应该看看这部电影，它会让你明白战争的残酷！
### [“社交+票务”能为现场演出添一把火吗？](https://www.tuicool.com/articles/fqaQBfu)
> 概要: 2月16日，社交软件Snapchat宣布与全球最大的票务销售平台Ticketmaster 合作，推出了一项发现附近演出活动的新功能。只需要进入Snap Map中的Ticketmaster图层，用户便可......
### [《狼与香辛料》完全新作动画制作决定 萌狼再来](https://acg.gamersky.com/news/202202/1463036.shtml)
> 概要: 《狼与香辛料》将推出完全新作动画，官方公开了动画制作特报PV和视觉图，萌狼赫萝又将开启新的旅程了。
### [金色前哨｜TheGraph将推出子图NFT所有权转移功能](https://www.tuicool.com/articles/y2umee7)
> 概要: 金色财经报道，2月25消息，TheGraph将推出基于NFT的子图所有权转移功能。官方提出GIP-0018治理提案：每当应用程序开发人员发布新的子图时，GNS都会生成NFT，拥有NFT的人控制子图，N......
### [我国网民规模达 10.32 亿，互联网普及率达 73.0％](https://www.tuicool.com/articles/nMbQFbv)
> 概要: IT之家 2 月 25 日消息，中国互联网络信息中心（CNNIC）今日发布了第 49 次《中国互联网络发展状况统计报告》（以下简称：《报告》）。《报告》显示，截至 2021 年 12 月，我国网民规模......
### [视频：《超有趣滑雪大会》林一没有选择虞书欣 找队友太难了](https://video.sina.com.cn/p/ent/2022-02-25/detail-imcwipih5343752.d.html)
> 概要: 视频：《超有趣滑雪大会》林一没有选择虞书欣 找队友太难了
### [组图：乔欣徐正溪扫楼直播笑料不断 做游戏扮鬼脸气氛欢乐](http://slide.ent.sina.com.cn/tv/slide_4_86512_366654.html)
> 概要: 组图：乔欣徐正溪扫楼直播笑料不断 做游戏扮鬼脸气氛欢乐
### [“亚逼”没老过，而你年轻过](https://www.huxiu.com/article/501690.html)
> 概要: 出品 | 虎嗅青年文化组策划｜黄瓜汽水作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。现在，一提起“亚逼”这个词......
### [游戏王历史：从零开始的游戏王环境之旅第⑨期10](https://news.dmzj.com/article/73711.html)
> 概要: 伴随着新势力【机壳】的崛起，当时的OCG环境出现了巨大变化。凭借本家牌组强度，以及“技能抽取”“虚无空间”等【MetaBEAT】牌组常用的贴纸，一口气将【因士】挤了下去。另外，和【机壳】一起出现的新卡令【影依】的地位更加稳如磐石，总的来说这次的卡...
### [AMD 肾上腺素 22.2.3 驱动发布，同步支持满分大作《艾尔登法环》](https://www.ithome.com/0/604/810.htm)
> 概要: 感谢IT之家网友AMD引领未来、肖战割割的线索投递！IT之家2 月 25 日消息，宫崎英高的《艾尔登法环》今日正式解锁，多家媒体给出满分评价，游戏热度也达到同类型新高。与此同时，AMD 为显卡用户发布......
### [视频：30周年感恩会：林志颖谈未来规划 透露Kimi个子和自己一样高了](https://video.sina.com.cn/p/ent/2022-02-25/detail-imcwipih5350039.d.html)
> 概要: 视频：30周年感恩会：林志颖谈未来规划 透露Kimi个子和自己一样高了
### [一个unicode问题](https://www.tuicool.com/articles/bIZnqq7)
> 概要: 最近我在处理从 pdf 转换到 docx 的时候，总会出现丢字问题，让我一度以为是字体的原因，结果方正、思源、文泉驿换了个遍也没搞定，最终发现是一个 unicode 问题。我用 javascript ......
### [三星：Galaxy S22 系列和 Galaxy Tab S8 系列今日全球上市，预购双双破纪录](https://www.ithome.com/0/604/825.htm)
> 概要: IT之家2 月 25 日消息，三星今日在新闻稿中宣布，Galaxy S22 系列和 Galaxy Tab S8 系列已在全球上市，预购打破历史纪录。据介绍，Galaxy S22 系列和 Galaxy ......
### [延庆赛区冬残奥会各项工作准备就绪](https://finance.sina.com.cn/jjxw/2022-02-25/doc-imcwiwss2947787.shtml)
> 概要: 新华社北京2月25日电（记者邰思聪）25日，北京2022年冬奥会和冬残奥会延庆赛区在新闻发布会上介绍，赛区在冬奥会结束后无缝衔接转入冬残奥会状态，高山场馆...
### [厦门市人大常委会原主任陈家东落马，今年前两月10名中管干部被查](https://finance.sina.com.cn/china/gncj/2022-02-25/doc-imcwiwss2949986.shtml)
> 概要: 原标题：厦门市人大常委会原主任陈家东落马，今年前两月10名中管干部被查 澎湃新闻记者 岳怀让 2月25日晚间，中央纪委国家监委网站发布消息：福建省厦门市人大常委会原主任...
### [最热共有产权房！东城永佑嘉园政府产权份额锁定20%](https://finance.sina.com.cn/china/gncj/2022-02-25/doc-imcwipih5390085.shtml)
> 概要: 2月25日，东城区政府发布《关于东城区永佑嘉园项目政府持有产权份额事项的公告》，明确东城区“新型共有产权房”永佑嘉园项目的政府产权份额代持机构为北京佳源投资经营有限...
### [京津冀协同发展迈向更高水平](https://finance.sina.com.cn/china/2022-02-25/doc-imcwiwss2951660.shtml)
> 概要: 03：46 京畿大地，绘就宏图 8年前，在习近平总书记亲自谋划、亲自部署、亲自推动下 京津冀协同发展大幕开启 非首都功能疏解不断取得新突破...
### [贝尔金发布 CONNECT Pro 雷电 4 扩展坞：12 个接口，399.99 美元](https://www.ithome.com/0/604/841.htm)
> 概要: IT之家2 月 25 日消息，据 TechPowerUp 消息，贝尔金发布了新款 CONNECT Pro 雷电 4 扩展坞，拥有 12 个接口，售价 399.99 美元（约 2527.94 元人民币）......
### [刘尚希：谈收入差距不能只看抽象数据，我国依然是以农民为主体的社会](https://finance.sina.com.cn/china/gncj/2022-02-25/doc-imcwiwss2951374.shtml)
> 概要: 中国财政科学研究院院长，第十三届全国政协委员刘尚希近日接受第一财经采访时表示，从社会身份意义上，我国依然是以农民为主体的社会...
### [刘尚希：历史上财政很少支持农村经济的情况早已改变，下一步应积极推进农民市民化](https://finance.sina.com.cn/china/gncj/2022-02-25/doc-imcwiwss2951474.shtml)
> 概要: 中国财政科学研究院院长，第十三届全国政协委员刘尚希近日接受第一财经采访时，回顾了“公共财政”理念的变迁。我国很长时期，财政基本是“城市财政”...
# 小说
### [都市无敌小医仙](http://book.zongheng.com/book/1073999.html)
> 作者：侠梦武魂

> 标签：都市娱乐

> 简介：一代山村小医生得逆天医术传承，依靠着医武两道纵横花都，吊打一切敌！

> 章节末：第七十七章    冰焰门，玄冰凤

> 状态：完本
# 论文
### [Parameter-free Online Linear Optimization with Side Information via Universal Coin Betting | Papers With Code](https://paperswithcode.com/paper/parameter-free-online-linear-optimization)
> 日期：4 Feb 2022

> 标签：None

> 代码：None

> 描述：A class of parameter-free online linear optimization algorithms is proposed that harnesses the structure of an adversarial sequence by adapting to some side information. These algorithms combine the reduction technique of Orabona and P{\'a}l (2016) for adapting coin betting algorithms for online linear optimization with universal compression techniques in information theory for incorporating sequential side information to coin betting. Concrete examples are studied in which the side information has a tree structure and consists of quantized values of the previous symbols of the adversarial sequence, including fixed-order and variable-order Markov cases. By modifying the context-tree weighting technique of Willems, Shtarkov, and Tjalkens (1995), the proposed algorithm is further refined to achieve the best performance over all adaptive algorithms with tree-structured side information of a given maximum order in a computationally efficient manner.
### [ST-MTL: Spatio-Temporal Multitask Learning Model to Predict Scanpath While Tracking Instruments in Robotic Surgery | Papers With Code](https://paperswithcode.com/paper/st-mtl-spatio-temporal-multitask-learning)
> 日期：10 Dec 2021

> 标签：None

> 代码：None

> 描述：Representation learning of the task-oriented attention while tracking instrument holds vast potential in image-guided robotic surgery. Incorporating cognitive ability to automate the camera control enables the surgeon to concentrate more on dealing with surgical instruments.
