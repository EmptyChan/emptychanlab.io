---
title: 2021-03-18-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MtEtna_EN-CN8869032750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-03-18 21:42:22
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MtEtna_EN-CN8869032750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Clio: A functional, distributed programming language that compiles to JavaScript](https://github.com/clio-lang/clio)
> 概要: Clio: A functional, distributed programming language that compiles to JavaScript
### [停课的北京，焦虑的辅导班与家长](https://www.huxiu.com/article/415764.html)
> 概要: 出品｜虎嗅大商业组作者｜格根坦娜题图｜CFP，2020年5月北京家长悬着的心似乎终于可以放下了。3月15日，北京市新闻发言人李奕介绍称，目前，北京市教委已启动学科类校外培训机构有序恢复线下教学的工作，......
### [揭秘拼多多管理架构：黄峥为何敢放手？](https://www.huxiu.com/article/415794.html)
> 概要: 本文来自微信公众号：深网腾讯新闻（ID：qqshenwang），作者：孙宇，编辑：康晓，原文标题：《揭秘拼多多网状管理架构：黄峥为何敢放手拼多多？｜深网》，头图来自：视觉中国40岁的黄峥激流勇退辞任拼......
### [1.9万亿美元强心针，最糟的代价不止是滞胀](https://www.huxiu.com/article/415683.html)
> 概要: 本文来自微信公众号：智本社（ID：zhibenshe0-1），作者：清和社长，原文标题：《通胀，还是滞胀？》，题图来自：视觉中国近期，美国国会通过了拜登政府的1.9万亿美元的经济刺激方案。市场反应不一......
### [Focalboard – a self-hosted alternative to Trello, Notion, and Asana](https://www.focalboard.com/)
> 概要: Focalboard – a self-hosted alternative to Trello, Notion, and Asana
### [不过是个“玩笑”？校园霸凌其实远比我们想象的更残酷！](https://news.dmzj1.com/article/70373.html)
> 概要: 在观看校园霸凌题材作品的时候，我从没想过，现实中的霸凌会比虚拟世界更为可怖和残酷……
### [《寂静岭2：增强版》更新视频 加入高光反射效果](https://www.3dmgame.com/news/202103/3810759.html)
> 概要: 《寂静岭2：增强版》是一款《寂静岭2》Mod合集游戏，增强了游戏功能，让现代PC机器也能顺畅游玩。近日《寂静岭2：增强版》制作团队发布新更新，为游戏加入了动态分辨率的支持，以及高光反射效果。《寂静岭2......
### [安卓版64位Chrome运行至少需8GB内存 网友：用不起](https://www.3dmgame.com/news/202103/3810760.html)
> 概要: 作为全球用户量最大的浏览器之一，谷歌Chrome几乎覆盖了所有平台，包括PC端、iOS、安卓等。近日，谷歌Chrome安卓版终于支持了64位版本，但遗憾的是，这迟迟到来的版本并不适用于大多数用户。据了......
### [凭借新主机和大型游戏 任天堂明年软件销量预期2.5亿](https://www.3dmgame.com/news/202103/3810761.html)
> 概要: 此前彭博社曾爆料，任天堂计划在今年底推出升级版Switch主机，并使用来自三星的OLED屏幕（相关报道）。而据近日任天堂的合作伙伴和供应商向彭博社透露的信息，任天堂正准备在未来一年里创造新的软硬件销售......
### [搞笑恐怖漫画《看的见的女孩》TV动画化 不一样的JK](https://acg.gamersky.com/news/202103/1371327.shtml)
> 概要: 泉朝树原作的搞笑恐怖漫画《看的见的女孩》宣布将推出TV动画，官方公开了视觉图和预告。一位女高中生突然能看到诡异身影，她选择一次又一次地无视那些诡异身影。
### [剧场版《黄金拼图Thank you!!》8月20日上映！CM公开](https://news.dmzj1.com/article/70375.html)
> 概要: 根据原悠衣原作制作的剧场版动画《黄金拼图Thank you!!》宣布了将于8月20日上映的消息，一段CM也一并公开。
### [Nitro+捐赠《刀剑乱舞》部分销售额 获得绀绶褒章](https://news.dmzj1.com/article/70376.html)
> 概要: 株式会社Nitro+发表了获得政府颁发的绀绶褒章的消息。Nitro+称这次是将《刀剑乱舞》的部分销售额支援了刀剑文化获得的，所以希望将这份荣誉分享给氪金的審神者。
### [渡边直美回应东京奥运会导演让其扮成猪表演](https://ent.sina.com.cn/s/j/2021-03-18/doc-ikkntiam4718475.shtml)
> 概要: 新浪娱乐讯 据《文春》昨日报道，东京奥运会开幕式总导演曾提议让渡边直美在开幕式上打扮成猪表演节目，一边在笼子里叫，一边从天而降，取“笼子里的猪”和“奥林匹克”的谐音。对此报道，渡边直美通过所属事务所吉......
### [「秦岭神树」x三星堆博物馆联动活动即将举办](http://acg.178.com//202103/410045396177.html)
> 概要: 近日，国产动画「秦岭神树」官方发布3月24日与三星堆博物馆联动，举行首映礼暨“文物寻宝之旅”启动仪式，并公开联动海报。国产动画「秦岭神树」该作改编自南派三叔同名小说「盗墓笔记 秦岭神树篇」，将于4月4......
### [漫画「7D-O和她的伙伴们」第二卷封面公开](http://acg.178.com//202103/410047417694.html)
> 概要: 漫画「7D-O和她的伙伴们」官方公开了本作第二卷的封面图和四张内页预览，该册将于4月27日发售。剧情概述：性格阴郁的家里蹲富家大小姐玉村小町，一直自视颇高且不善于交朋友，为了使她恢复身为人类的社交机能......
### [中通跑到了第一名，然后呢？](https://www.huxiu.com/article/415870.html)
> 概要: 题图来自视觉中国2020年，中通表现算是中规中矩。北京时间3月18日，中通快递（纽交所代码：ZTO及香港联交所代号：2057）发布2020年第四季度及全年业绩报。财报显示，第四季度中通实现营收82.5......
### [Twitter图片可能被滥用来隐藏ZIP、MP3文件](https://www.tuicool.com/articles/iAJVFfy)
> 概要: 如果你下载了一张图片，却发现大小有好几MB，那你要警惕了，图片可能藏着压缩文件和Mp3文件。3月17日，研究人员披露了一种在Twitter图像中隐藏多达3 MB数据的方法。尽管将非图像数据隐藏在图像中......
### [GSC三款《光能使者》拼装模型上架 每个售价249元](https://www.3dmgame.com/news/202103/3810781.html)
> 概要: 今日（3月18日），GSC《光能使者》光能使者、风暴使者、波涛使者三款拼装模型上架Goodsmile官方旗舰店开启预售，每个售价249元，预计于2021年9月正式出货。轻巧有趣的TV动画《光能使者》中......
### [电影「浪客剑心」x HELLO KITTY联动设计公开](http://acg.178.com//202103/410049226520.html)
> 概要: HELLO KITTY品牌公开与电影「浪客剑心」的联动设计。电影「浪客剑心」根据原著漫画完结篇「人诛篇」改编，故事以明治维新初期为背景，讲述了 “杀人不眨眼”的主人公绯村剑心（佐藤健 饰），通过与薰的......
### [轻小说「我的青春恋爱物语果然有问题。」发售10年回顾视频公开](http://acg.178.com//202103/410049514024.html)
> 概要: 轻小说「我的青春恋爱物语果然有问题。」第1卷2011年3月18日发售至今整10年，今日（3月18日）官方公开回顾视频，结尾处有⑧神绘制的贺图。轻小说「我的青春恋爱物语果然有问题。」发售10年回顾视频......
### [智利英雄铜像损坏 民众提议《进击巨人》兵长接替](https://www.3dmgame.com/news/202103/3810782.html)
> 概要: 岛国动漫在海外很有影响力，至少智利就是其中之一，近日一则趣闻引发热议，据外媒报道，因为不久前的游行示威，原本的民族英雄铜像损坏被暂时撤下待修，民众竟然联名提议由《进击的巨人》兵长接替，理由是，兵长才是......
### [吃辣必囤：英潮魔鬼特辣 / 鲁西牛肉辣酱 210g / 瓶 9.9 元（京东 21.8 元 / 瓶）](https://lapin.ithome.com/html/digi/540770.htm)
> 概要: 吃辣必囤：英潮魔鬼特辣/鲁西牛肉辣酱210g报价19.9元，限时限量10元券，实付9.9元包邮，领券并购买。京东21.8元一瓶使用最会买App下单，预计还能再返 1.63 元，返后 8.27 元包邮，......
### [智利网友提议用利威尔兵长雕像代替同国英雄雕像](https://news.dmzj1.com/article/70379.html)
> 概要: 智利网友在Change.org上发起了一项使用《进击的巨人》中的角色利威尔·阿克曼的雕像，代替原有的同国英雄Manuel Baquedano的雕像的书名活动。
### [中国版《忠犬八公》电影开机定档 冯小刚陈冲主演](https://ent.sina.com.cn/m/c/2021-03-18/doc-ikknscsi7988511.shtml)
> 概要: 新浪娱乐讯 改编自经典IP的中国版《忠犬八公》正式开机拍摄，同时也宣布定档12月31日跨年上映。该片主创阵容也同步曝光：冯小刚、陈冲主演，导演则是徐昂（《十二公民》《法医秦明》）。　　该片讲述一只叫八......
### [组图：又见李玉湖！黄奕着古装嫁衣状态佳 掐腰搞怪引回忆](http://slide.ent.sina.com.cn/z/v/slide_4_704_354142.html)
> 概要: 组图：又见李玉湖！黄奕着古装嫁衣状态佳 掐腰搞怪引回忆
### [春游家族“落后”王牌家族，沈涛：你们就是少了一辆小刀](https://new.qq.com/omn/20210318/20210318A095MJ00.html)
> 概要: 作为热门综艺节目，《王牌对王牌》第六季开播以来，口碑非常不错，每一期播出都会有几个话题会引起热议。比如说这一期综艺王牌大战王牌家族VS春游家族“沈腾把假发笑掉了”就成功登顶，占据到热搜冠军宝座。另外，......
### [FlutterWeb在美团外卖的实践](https://www.tuicool.com/articles/uIRZJne)
> 概要: 一、背景1.1 业务背景美团外卖商家端业务围绕数百万商家，在 PC 和 App 上分别提供了交易履约、运营、广告、营销等一系列功能，且经常有外投 H5 的场景（如外卖学院、商家社区、营销活动等）。在这......
### [爱奇艺回应起诉 B 站：常规维权](https://www.ithome.com/0/540/818.htm)
> 概要: IT之家3月18日消息 据北京法院审判信息网消息显示，近日，北京爱奇艺科技有限公司公开与上海宽娱数码科技有限公司相关侵害作品信息网络传播权纠纷案件的开庭公告，案号为（2021）京 0491 民初 11......
### [欧阳震华接种国产新冠疫苗 发微博分享体验](https://ent.sina.com.cn/s/h/2021-03-18/doc-ikkntiam4884876.shtml)
> 概要: 新浪娱乐讯 3月18日，欧阳震华在微博分享接种国产新冠疫苗的体验，他发文表示：“我今日已经打完国产科兴疫苗，祝大家身体健康。”　　此前除欧阳震华之外，谭咏麟、林青霞等香港艺人都曾先后在微博与大家分享了......
### [豪门媳妇：奚梦瑶生下长孙，郭碧婷被向太把控，郭晶晶备受宠爱](https://new.qq.com/omn/20210318/20210318A0B2MZ00.html)
> 概要: 自古以来，想嫁入豪门的女生不在少数，不少人都想着飞上枝头变凤凰，但一入豪门深似海，不仅规矩繁多，甚至还有着很多勾心斗角，那么那些嫁入豪门的女明星，如今都过得怎样了呢？            首先我们第......
### [组图：汤唯新电影杀青 开心晒出剧组成员照](http://slide.ent.sina.com.cn/film/slide_4_704_354153.html)
> 概要: 组图：汤唯新电影杀青 开心晒出剧组成员照
### [招兼职解难题，雇演员当名师，在线教育闹哪样](https://www.ithome.com/0/540/827.htm)
> 概要: 辅导孩子写作业，已经成为很多家长共同的 “磨难”，同时也衍生出了大量令人啼笑皆非的网络梗：“陪孩子写作业，嗓子痛、手痛、心绞痛。”“我终于知道，我明白和孩子明白是两回事。”“孩子读到高中，我学会了狮吼......
### [“从农田到餐桌”，农产品质量安全追溯是怎么做到的？](https://www.tuicool.com/articles/rUBbiiv)
> 概要: 本文来自微信公众号：中国工程院院刊（ID：CAE-Engineering），作者：杨雅萍、姜侯、胡云锋、孙九林，本文选自中国工程院院刊《中国工程科学》2020年第4期，原文标题：《战略研究丨“互联网+......
### [李冰冰首次公开恋爱，男友却被曝欠债千万：情侣感情再好，也别一起做这件事](https://new.qq.com/omn/20210318/20210318A0BCJE00.html)
> 概要: 前段时间，李冰冰在接受媒体采访时，被问到：“如果要移居另一个星球是否带爱人？”李冰冰坦然自若的回了一句：“我已经没有爱人了，哈哈，我们已经选择分开了。”对于48岁的李冰冰来说，许文楠是她首次承认的恋爱......
### [Unprivileged Chroot()](https://lwn.net/SubscriberLink/849125/c4422a7c318a5a17/)
> 概要: Unprivileged Chroot()
### [司藤大结局：司藤白英合体，重回大自然，只剩下小西竹分身陪伴秦放](https://new.qq.com/omn/20210318/20210318A0C40D00.html)
> 概要: 最近正在热播的《司藤》引起了很多观众们的争议，虽然目前剧情才只播出了一半，但是网上关于这部剧的剧情和一些人物关系的猜测就已经有很多版本了，甚至很多人还开始猜测最后的大结局 。从网上流出的一些路透照和节......
### [《山河令》爆火，龚俊张哲瀚代言不断，能否成为下一个肖战王一博](https://new.qq.com/omn/20210318/20210318A0CK4E00.html)
> 概要: 要说现下最火的电视剧是什么？《山河令》绝对能名列榜首。            继2019年的《陈情令》大热之后，这一部双男主CP的剧再次引爆了耽改话题。潦倒失意的天窗首领周子舒和一心灭世的鬼谷谷主温客......
### [外媒称苹果可为礼貌客户提供额外优惠，国内客服表示没听说过](https://www.ithome.com/0/540/836.htm)
> 概要: IT之家3月18日消息 近日有外媒报道，苹果允许售后人员奖励礼貌的用户，使这些用户不花钱就可以享受正常的维修、更换服务。据蓝鲸财经，苹果国内客服就这一政策表示：零售客服目前没有该政策，具体的要以维修人......
### [上海警方破获制销违禁减肥食品案 郭某某等32人被刑拘](https://finance.sina.com.cn/china/dfjj/2021-03-18/doc-ikkntiam4978257.shtml)
> 概要: 原标题：上海警方破获制销违禁减肥食品案 郭某某等32人被刑拘 记者从上海警方处获悉，2021年3月11日，上海浦东警方成功侦破一起生产...
### [参考智库：部分外媒关注中国科技创新政策前景](https://finance.sina.com.cn/china/gncj/2021-03-18/doc-ikkntiam4977195.shtml)
> 概要: 原标题：部分外媒关注中国科技创新政策前景 参考消息网3月18日报道（记者 李溯）近期，围绕“十四五”规划和2035年远景目标纲要强调科技创新相关方向...
### [简讯：深圳将对公共场所摄像头立法](https://finance.sina.com.cn/china/dfjj/2021-03-18/doc-ikknscsi8148309.shtml)
> 概要: 简讯|深圳将对公共场所摄像头立法 作者：张晓梅 近日，央视315晚会曝光了多个知名品牌商采用含有人脸识别摄像头、非法获取消费者个人信息的行为。
### [全国人大常委会就美国国务院宣布对中国有关官员实施制裁发表谈话](https://finance.sina.com.cn/china/2021-03-18/doc-ikknscsi8156909.shtml)
> 概要: 【相关报道】 美对14名中方官员实施金融制裁 外交部：已采取必要反制措施 全国人大常委会发言人就美国国务院宣布对中国有关官员实施制裁发表谈话 新华社北京3月18日电...
### [财经TOP10|五部门出手！花呗、借呗不得向大学生放贷](https://finance.sina.com.cn/china/caijingtop10/2021-03-18/doc-ikkntiam5012599.shtml)
> 概要: 【政经要闻】 NO.1 全国人大常委会就美国国务院宣布对中国有关官员实施制裁发表谈话 3月18日，针对美国国务院就中国全国人大通过关于完善香港特别行政区选举制度的决定...
### [Norwegian experts say blood clots were caused by the AstraZeneca Covid vaccine](https://sciencenorway.no/covid19/norwegian-experts-say-deadly-blood-clots-were-caused-by-the-astrazeneca-covid-vaccine/1830510)
> 概要: Norwegian experts say blood clots were caused by the AstraZeneca Covid vaccine
### [李佳琦带货翻车！成本60元贴牌暴涨十倍，家用美容仪要交多少“智商税”？](https://www.tuicool.com/articles/fQRBnei)
> 概要: 编者按：本文为创业邦原创报道，作者陈晓 编辑房煜，未经授权禁止转载。图源丨摄图网、产品官网一边不良商家开发三无产品，贴牌摇身一变，价格翻倍，收割“智商税”。另一边新晋国产美容仪品牌，被资本扶持跑步进场......
### [明确了！上海有序放开剧场、上网服务、娱乐等文旅场所人数限制](https://finance.sina.com.cn/china/dfjj/2021-03-18/doc-ikkntiam5016404.shtml)
> 概要: 原标题：明确了！上海有序放开剧场、上网服务、娱乐等文旅场所人数限制 今天（18日），上海市文化和旅游局发布《关于持续加强本市演出、上网服务...
# 小说
### [我要让运气爆棚](http://book.zongheng.com/book/1025563.html)
> 作者：坐吃金山

> 标签：都市娱乐

> 简介：叶礼是个迷信的人。在算过无数次命后，得出的结论都是他命运平平。正当他要放弃挣扎，屈服于命运的时候，转机出现了……

> 章节末：第096章 接受上天的安排（大结局）

> 状态：完本
# 论文
### [CARMI: A Cache-Aware Learned Index with a Cost-based Construction AlgorithmEdit social preview](https://paperswithcode.com/paper/carmi-a-cache-aware-learned-index-with-a-cost)
> 日期：1 Mar 2021

> 标签：None

> 代码：https://github.com/JiaoYiZhang/learned_index

> 描述：Learned indexes, which use machine learning models to replace traditional index structures, have shown promising results in recent studies. However, our understanding of this new type of index structure is still at an early stage with many details that need to be carefully examined and improved.
### [Probing Multimodal Embeddings for Linguistic Properties: the Visual-Semantic Case](https://paperswithcode.com/paper/probing-multimodal-embeddings-for-linguistic)
> 日期：22 Feb 2021

> 标签：None

> 代码：https://github.com/dali-does/vse-probing

> 描述：Semantic embeddings have advanced the state of the art for countless natural language processing tasks, and various extensions to multimodal domains, such as visual-semantic embeddings, have been proposed. While the power of visual-semantic embeddings comes from the distillation and enrichment of information through machine learning, their inner workings are poorly understood and there is a shortage of analysis tools.
