---
title: 2022-05-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BerninaBloodMoon_ZH-CN3349260043_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-05-15 22:13:05
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BerninaBloodMoon_ZH-CN3349260043_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [普通人如何从0到1打造出个人品牌，实现用户增长和变现](http://www.woshipm.com/marketing/5438791.html)
> 概要: 编辑导语：在激烈的职场竞争中，建立个人品牌能将你与其他人区分开来，从中脱颖而出。那么普通人要如何建立个人品牌呢？一起来看一下吧。我们在说个人品牌之前首先要知道什么是个人品牌。个人品牌其实就是别人对你的......
### [困于算法围城，年轻人躲不过的“算计”](http://www.woshipm.com/it/5438749.html)
> 概要: 编辑导语：在互联网时代，我们每个人都不可避免地会受到算法的影响，购物软件上的好物推荐、视频平台上的信息流广告，诸如此类内容频频出现在我们的手机屏幕上。那么，我们是否能真正地逃出这座信息茧房？一起来看看......
### [工作经验｜如何说服他人认可自己的设计方案？](http://www.woshipm.com/zhichang/5437943.html)
> 概要: 编辑导语：在日常工作中，说服同事和领导认可自己的设计方案背后蕴含许多功课，本篇文章作者分享了说服他人认可自己的设计方案的相关方法和思路，一起来学习一下。作为交互设计师，与产品、开发等同事的沟通环节必不......
### [虚幻5xMegascans Quixel展示效果图及视频](https://www.3dmgame.com/news/202205/3842432.html)
> 概要: Megascans背后的团队Quixel日前分享了一段视频，展示了Epic最新引擎虚幻5引擎的潜力。在短短90天内，Quixel的艺术家们创造了40多个令人惊叹的环境内容。这些环境内容视觉效果相当惊人......
### [《鬼玩人：游戏》可通过N卡DLSS获得大幅FPS提升](https://www.3dmgame.com/news/202205/3842433.html)
> 概要: 《鬼玩人：游戏》日前已经通过Epic商店登陆PC平台，该游戏也是支持Nvidia DLSS的典型新作。虽然这款游戏的配置要求还不至于让大部分游戏PC产生帧数压力，但Nvidia团队表示，自家DLSS技......
### [经济日报：炒作虚拟货币终将一场空 远离相关交易炒作活动](https://finance.sina.com.cn/tech/2022-05-15/doc-imcwiwst7464252.shtml)
> 概要: 文 | 李华林......
### [动漫博人传：同样犯错，荒海安慰传马，地印为什么会被迁怒？](https://new.qq.com/omn/20220515/20220515A00WD000.html)
> 概要: 在动漫中，地引铩羽而归，不得不带着败绩去向荒海汇报。文/颚之巨人马赛尔回去的时候，他心里一定很不好受：传马自作主张趁着浓雾偷袭长十郎，把自己搭了进去，原有的作战计划被迫作废，他也被坑得不轻。坑货兄弟偏......
### [够刑！链家程序员“删库”9TB数据 二审被判7年](https://finance.sina.com.cn/tech/2022-05-15/doc-imcwipii9913675.shtml)
> 概要: 新酷产品第一时间免费试玩，还有众多优质达人分享独到生活经验，快来新浪众测，体验各领域最前沿、最有趣、最好玩的产品吧~！下载客户端还能获得专享福利哦......
### [铁血的美国家长，怎么变成了溺爱王中王？](https://www.huxiu.com/article/554421.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。大家小时候，大概都听家......
### [马斯克：推特说我违反保密协议](https://finance.sina.com.cn/tech/2022-05-15/doc-imcwipii9916453.shtml)
> 概要: 新浪科技讯 北京时间5月15日早间消息，特斯拉CEO埃隆·马斯克今日发推文，称推特法律团队指责他违反保密协议，因为他透露了推特对虚假用户的抽查样本数是100......
### [JK：愿心怀热爱，奔赴载满诗意的乐章](https://new.qq.com/omn/20220515/20220515A0197P00.html)
> 概要: 出镜：溢灯灯摄影：@阿喵_帅不过三秒......
### [沈梦辰自曝想生女孩 幽默称长得像海涛就做谐星](https://ent.sina.com.cn/tv/zy/2022-05-15/doc-imcwipii9921582.shtml)
> 概要: 新浪娱乐讯 5月14日《中国婚礼》节目中，沈梦辰在与嘉宾聊天时被问到想生男孩还是女孩，直率表示自己想生女儿，但担心女儿像爸爸。她接着幽默表示：“算了，我的孩子健康快乐就好，生下来就直接做谐星。”　　今......
### [双向暗恋绝配甜宠剧，顶流明星爱上宣传经纪](https://news.dmzj.com/article/74408.html)
> 概要: 韩国2022年4月底tvN金土连续剧《流星》，播出后反响收视颇高，果然现代都市甜宠剧受众人群广泛，豆瓣评分7.9，由颜值般配，拥有超长美腿模特身材的李圣经和金永大主演，是一部以演艺界，经纪公司为题材的剧，讲述了艺人就像是天空中无法独自发光发亮的星...
### [新闻动态｜豆神动漫董事长赵立军参加济宁市品牌建设促进会](https://new.qq.com/omn/20220515/20220515A01UBI00.html)
> 概要: 为进一步提高济宁自主品牌意识，培育发展自主品牌，近日，济宁市品牌建设促进会&5.10中国品牌日交流座谈会成功举办，            新起点、新环境、新竞争，与会者纷纷围绕企业发展的难题及建议进行......
### [动画「租借女友」公开第二期全新视觉图](http://acg.178.com/202205/446577734617.html)
> 概要: 动画「租借女友」官方于近日公开了第二期全新视觉图，本作预计将于2022年7月正式开播。「租借女友」是由宫岛礼吏创作的漫画作品，于2017年7月12日在「周刊少年Magazine」32号上开始连载，其同......
### [动画「群青的号角」新篇「トレセン篇」PV&主视觉图公开](http://acg.178.com/202205/446582784976.html)
> 概要: 电视动画「群青的号角」公开了新篇「トレセン篇」的PV，以及由角色设计·かんざきひろ绘制的主视觉图，本篇将从5月21日播出的第8话开始。「トレセン篇」PV同时，官方还公布了新追加的角色与声优。新追加CA......
### [歌名字體設計｜Typography Design](https://www.zcool.com.cn/work/ZNTk4MTMzNTY=.html)
> 概要: 歌名字體設計｜Typography Design
### [我们这代人的危与机](https://www.huxiu.com/article/555171.html)
> 概要: 本文来自微信公众号：培风客 （ID：peifengke），作者：奥德修斯的凝望，题图来自：视觉中国去年底，贝莱德做了一个2022年的展望，那篇文章是从股债相关性和回报率开始的。文章的意思是，2021年......
### [暴太郎战队12话预告：合体萝卜鬼退神登场，狗哥还是一个人！](https://new.qq.com/omn/20220515/20220515A0307U00.html)
> 概要: 导语：在本周暴太郎战队的故事里我们看到了全新的内幕，那就是美穗很有可能是代表了鹤的兽人。尤其是和狗哥那段剧情是真的令人感到恐惧，如果美穗真是兽人的话，那三人的修罗场可能要比我们想象的还要更加激烈。而与......
### [「女忍者椿的心事」第六话ED主题曲无字幕MV公开](http://acg.178.com/202205/446584923616.html)
> 概要: 根据山本崇一朗创作的同名漫画改编，CloverWorks负责制作的电视动画「女忍者椿的心事」近期公开了第六话ED主题曲「あかね組活動日誌〜寅班〜」的无字幕MV，该作已于4月9日开始播出。「あかね組活動......
### [「我的土妹子未婚妻，在家时却非常可爱」第5卷封面及新插画公开](http://acg.178.com/202205/446585530519.html)
> 概要: 轻小说「我的土妹子未婚妻，在家时却非常可爱」公开了最新卷第5卷的封面图，该卷将于2022年5月20日发售。另外，官方还发布了一张新插画。「我的土妹子未婚妻，在家时却非常可爱」是氷高悠创作、たん旦插画、......
### [英国女子组合Little Mix休团 三位成员将单飞](https://ent.sina.com.cn/y/youmei/2022-05-15/doc-imcwiwst7504567.shtml)
> 概要: 新浪娱乐讯 据国外媒体报道，当地时间5月14日，英国女子组合Little Mix在伦敦结束了巡演最后一站，此后，Little Mix将无限期中止活动。据悉，三位成员将单飞发展，专注个人活动。　　据悉，......
### [创意点缀生活 衣服夹子打造高达机体惟妙惟肖](https://www.3dmgame.com/news/202205/3842450.html)
> 概要: 在高玩的眼中，生活中的不起眼物品都是可以再利用的绝佳素材，近日有日本玩家用衣服夹子打造了《高达》经典机体高战蟹，极高的仿真度引发网友热议点赞。•高战蟹是《机动战士高达》经典机体之一，作为战蟹的改良机型......
### [老挝怎么就成了湖南人的第二故乡](https://www.huxiu.com/article/553639.html)
> 概要: 本文来自微信公众号：不相及研究所 （ID：buuuxiangji），作者：发财金刚，题图来自：微博@大卫独夫DE吴志伟前几年，在昆明的青旅遇到一个湖南背包客，他盘腿坐在十六人间的上铺，嚼着槟郎，像一个......
### [Intel's New Chimera: Alder Lake](https://www.agner.org/forum/viewtopic.php?t=79&p=187#p187)
> 概要: Intel's New Chimera: Alder Lake
### [SigNoz (YC W21) Is Hiring First Product Designer](https://www.ycombinator.com/companies/signoz/jobs/hnlEENe-product-designer)
> 概要: SigNoz (YC W21) Is Hiring First Product Designer
### [盛松成：失去的消费就永远失去了，疫情对经济的影响主要在二季度](https://www.yicai.com/news/101412741.html)
> 概要: 房地产调控可以继续放松。
### [关于 Cloudflare R2 Storage 的使用体验测评和我的观点](https://www.tuicool.com/articles/VvqI7z6)
> 概要: 关于 Cloudflare R2 Storage 的使用体验测评和我的观点
### [一文看懂：14家顶尖科技公司，研发支出都花哪去了？](https://www.huxiu.com/article/555523.html)
> 概要: 本文来自微信公众号：财经十一人 （ID：caijingEleven），作者：吴俊宇，编辑：谢丽容，原文标题：《14家中国科技公司研发支出数据背后的三大信号》，头图来自：视觉中国科技企业核心竞争力是产品......
### [华硕发布新款 RX 6950 XT / 6750 XT / 6650 XT 显卡](https://www.ithome.com/0/618/490.htm)
> 概要: 感谢IT之家网友软媒用户1702727的线索投递！IT之家5 月 15 日消息，日前，AMD 正式发布了 RX 6950 XT / 6750 XT / 6650 XT 三款更新版显卡，更换了更快的显存......
### [家用游戏史上首部女性主角游戏的女性开发者被发现](https://www.3dmgame.com/news/202205/3842460.html)
> 概要: 虽然如今游戏数量已经多入牛毛，不过以女性为主角的游戏是少之又少，日前据外媒报道，经过游戏研究学者的努力，终于发现了首部女性主角游戏《Wabbit》，而这部游戏的开发者也是一位女性。•家庭游戏研究学者凯......
### [“出道即爆火”，宠胖胖如何破译流量密码？](https://www.tuicool.com/articles/YJbM3ei)
> 概要: “出道即爆火”，宠胖胖如何破译流量密码？
### [工作室发文否认任豪恋情传闻：只是同剧组同事](https://ent.sina.com.cn/s/m/2022-05-15/doc-imcwipii9985834.shtml)
> 概要: 新浪娱乐讯 5月15日，有八卦媒体拍到任豪与元气美女相约聚餐，疑似恋情曝光，对此，工作室回应称：“感谢大家对任豪先生的关注和喜爱，纯属同剧组同事们的正常聚餐并有多人同行，请勿信谣传谣！正在剧组认真搬砖......
### [封控下的复工复产是一场“惊心动魄”的生存挑战丨来自四位企业管理者的自述](https://www.yicai.com/news/101412850.html)
> 概要: None
### [TV动画《炎炎消防队》第三季制作决定](https://news.dmzj.com/article/74409.html)
> 概要: TV动画《炎炎消防队》第3季制作决定，开播时间待定。
### [美国4月“恐怖数据”来袭，拜登开启亚洲行丨本周外盘看点](https://www.yicai.com/news/101412894.html)
> 概要: G7集团财长和央行行长会议召开；美国总统拜登则将访问日本和韩国。
### [TV动画《租借女友》第2期7月播出 约会视觉图公开](https://news.dmzj.com/article/74410.html)
> 概要: TV动画《租借女友》第2期2022年7月播出，最新的“约会”视觉图公开。
### [ROG 新款 Flow X16 翻转本外观曝光，搭载 R9 6900HS + RTX 3070 Ti](https://www.ithome.com/0/618/505.htm)
> 概要: IT之家5 月 15 日消息，外媒 technosports 现已曝光 ROG 新款 Flow X16 的外观，这款笔记本国内将被称为幻 16 翻转版。IT之家了解到，ROG 将于北京时间5 月 17......
### [中期修复行情将开启，稳增长主线仍有配置价值丨机构论后市](https://www.yicai.com/news/101412927.html)
> 概要: 机构指出，预计市场开启持续数月的中期修复行情，现代化基建、地产、复工复产和消费修复四大主线轮动慢涨。
### [张晓慧在2022清华五道口首席经济学家论坛上致辞](https://www.tuicool.com/articles/b2YrQ3I)
> 概要: 张晓慧在2022清华五道口首席经济学家论坛上致辞
### [31省2021年人口大数据：16省常住人口负增长](https://www.yicai.com/news/101412928.html)
> 概要: 31个省份中，有16个省份常住人口出现下降，15个省份常住人口增长，其中浙江、广东和湖北增量位居前三。
### [剧场动画《银河英雄传说DNT》第4期《策谋》情报公开](https://news.dmzj.com/article/74411.html)
> 概要: 剧场版动画《银河英雄传说 Die Neue These》第4期《策谋》上映情报和主视觉图公开，剧场版第4期《策谋》全3章，第1章将于2022年9月30日上映，第2章2022年10月28日上映，第3章2022年11月25日上映。
### [11管混采阳性！公交快递工地等发现多例感染者，北京12区域明起3轮核酸筛查](https://finance.sina.com.cn/china/2022-05-15/doc-imcwipik0003204.shtml)
> 概要: 5月15日，在北京市新型冠状病毒肺炎疫情防控工作第335场新闻发布会上，市卫健委党委委员王小娥介绍，5月14日，本市开展了12个区域的核酸筛查，共采样检测2119万人...
### [不明原因儿童急性肝炎最新发现：或与新冠病毒超级抗原相关](https://finance.sina.com.cn/china/2022-05-15/doc-imcwipik0003737.shtml)
> 概要: 澎湃新闻首席记者 贺梨萍 是什么导致了全球二十多个国家和地区的三百多例不明原因儿童急性肝炎？最新研究显示：可能与新冠病毒导致的超级抗原相关。
### [《英雄联盟》MSI 小组赛收官：RNG、T1、G2 均全胜出线](https://www.ithome.com/0/618/514.htm)
> 概要: IT之家5 月 15 日消息，在今日的《英雄联盟》MSI 季中赛小组赛阶段告终，A、B、C 组分别为 T1 和 SGB、RNG 和 PSG、G2 和 EG 六支队伍顺利出线进入 6 强对抗赛。其中，R......
### [上海：5月16日起分阶段推进复商复市](https://finance.sina.com.cn/china/2022-05-15/doc-imcwipik0007527.shtml)
> 概要: 记者从5月15日举行的上海市新冠肺炎疫情防控新闻发布会上了解到，上海正在大力推动网点节点应开尽开、保供人员应返尽返，从5月16日起，分阶段推进复商复市。
### [About iCloud Private Relay](https://support.apple.com/en-au/HT212614)
> 概要: About iCloud Private Relay
### [美联储面临经济软着陆难题，美股反弹或是昙花一现](https://www.yicai.com/news/101412956.html)
> 概要: 上周资金出现无差别抛售，应逢低买入还是静待时机？
### [胡锡进：只要抗疫而不计经济和其余，决不会是中国的风格](https://finance.sina.com.cn/china/2022-05-15/doc-imcwiwst7565842.shtml)
> 概要: 这两天围绕四月份的经济情况出了不少数据，个别有“腰斩”的，很多人看着备感触目惊心。直接受损的企业和个人就不用说了，我们社会的其他成员看了也会很不舒服。
### [《求是》杂志发表习近平总书记重要文章：正确认识和把握我国发展重大理论和实践问题](https://finance.sina.com.cn/china/2022-05-15/doc-imcwiwst7568410.shtml)
> 概要: 正确认识和把握我国发展重大理论和实践问题※ 《求是》2022/10 习近平 2021年12月8日至10日，中央经济工作会议在北京举行。中共中央总书记、国家主席...
### [From AWS Lambda and API Gateway to Knative and Kong API Gateway](https://www.pmbanugo.me/blog/2022-02-13-from-aws-lambda-api-gateway-to-knative-kong-api-gateway/)
> 概要: From AWS Lambda and API Gateway to Knative and Kong API Gateway
### [《木卫四协议》怪物特写图公开 下周公布消息](https://www.3dmgame.com/news/202205/3842470.html)
> 概要: Striking Distance Studios CEO Glen Schofield近日在Twitter分享了他们的生存恐怖游戏《木卫四协议》的新图。Schofield发推说他们将在下周公开一些消......
### [搜狗地图今日下线，官网已变成腾讯地图](https://www.ithome.com/0/618/526.htm)
> 概要: IT之家5 月 15 日消息，搜狗地图宣布2022 年 5 月 15 日 23 点正式下线，届时关闭所有相关服务，并建议用户下载腾讯地图。今日晚间，搜狗地图官网（http://map.sogou.co......
### [首套房贷利率下限下调影响几何？](https://finance.sina.com.cn/jjxw/2022-05-15/doc-imcwiwst7575282.shtml)
> 概要: 新华社北京5月15日电题：首套房贷利率下限下调影响几何？ 新华社记者吴雨 15日，中国人民银行、银保监会发布通知，调整首套住房商业性个人住房贷款利率下限。
### [「两桶油」、老字号、李宁都卖咖啡，插班生们图啥？](https://www.tuicool.com/articles/2aaUnay)
> 概要: 「两桶油」、老字号、李宁都卖咖啡，插班生们图啥？
### [流言板媒体人：亚洲杯场馆再利用难度很大，办世界杯是无奈的解嘲](https://bbs.hupu.com/53682214.html)
> 概要: 虎扑05月15日讯 5月14日，亚洲杯中国组委会官方宣布，2023年中国亚洲杯取消，将易地举办。对此，足球评论员杨天婴发文表示亚洲杯不办了，那些场馆大家说留着办世界杯，基本都是无奈的解嘲，在国内，体育
### [休赛期不忘学习！四川外援哈达迪晒现场观赛视频](https://bbs.hupu.com/53682272.html)
> 概要: 北京时间今日，四川男篮球员哈麦-哈达迪更新Instagram账号，晒现场观赛视频。2021-22赛季CBA联赛，哈达迪为四川出战20场，场均登场34.5分钟，得到15.6分12.8篮板7.4助攻1.7
### [不懂就问，主裁对曼城的这两次判罚是否值得商榷？](https://bbs.hupu.com/53682379.html)
> 概要: 不懂就问，主裁对曼城的这两次判罚是否值得商榷？
### [鲍文天神下凡梅开二度，曼城得势不得分](https://bbs.hupu.com/53682413.html)
> 概要: 鲍文天神下凡梅开二度，曼城得势不得分
# 小说
### [禁区之王](http://book.zongheng.com/book/974179.html)
> 作者：明巧

> 标签：竞技同人

> 简介：这是一个华国球员获得系统附体，从一名替补前锋成长为禁区之王，逐渐征服五大联赛、欧冠、世界杯的故事。

> 章节末：第282章 世界球王（大结局）

> 状态：完本
# 论文
### [An Open-Source Web App for Creating and Scoring Qualtrics-based Implicit Association Test | Papers With Code](https://paperswithcode.com/paper/an-open-source-web-app-for-creating-and)
> 日期：3 Nov 2021

> 标签：None

> 代码：None

> 描述：The Implicit Association Test (IAT) is a common behavioral paradigm to assess implicit attitudes in various research contexts. In recent years, researchers have sought to collect IAT data remotely using online applications.
### [GTNet: A Tree-Based Deep Graph Learning Architecture | Papers With Code](https://paperswithcode.com/paper/gtnet-a-tree-based-deep-graph-learning)
> 日期：27 Apr 2022

> 标签：None

> 代码：None

> 描述：We propose Graph Tree Networks (GTNets), a deep graph learning architecture with a new general message passing scheme that originates from the tree representation of graphs. In the tree representation, messages propagate upward from the leaf nodes to the root node, and each node preserves its initial information prior to receiving information from its child nodes (neighbors). We formulate a general propagation rule following the nature of message passing in the tree to update a node's feature by aggregating its initial feature and its neighbor nodes' updated features. Two graph representation learning models are proposed within this GTNet architecture - Graph Tree Attention Network (GTAN) and Graph Tree Convolution Network (GTCN), with experimentally demonstrated state-of-the-art performance on several popular benchmark datasets. Unlike the vanilla Graph Attention Network (GAT) and Graph Convolution Network (GCN) which have the "over-smoothing" issue, the proposed GTAN and GTCN models can go deep as demonstrated by comprehensive experiments and rigorous theoretical analysis.
