---
title: 2021-04-18-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MontalbanoElicona_EN-CN5189615468_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-04-18 17:10:00
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MontalbanoElicona_EN-CN5189615468_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Regulators urge safety recall of Peloton treadmill after child dies](https://www.washingtonpost.com/business/2021/04/16/peloton-treadmill-injuries-death-cpsc/)
> 概要: Regulators urge safety recall of Peloton treadmill after child dies
### [Codecov breach impacts GoDaddy, Atlassian, P&G etc.](https://www.reuters.com/technology/us-investigators-probing-breach-san-francisco-code-testing-company-firm-2021-04-16/)
> 概要: Codecov breach impacts GoDaddy, Atlassian, P&G etc.
### [和风暴赛跑的培训班](https://www.huxiu.com/article/422450.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：王敏，编辑：向小园，原文标题：《和风暴赛跑的培训班》，封面来自视觉中国。2021年的教培行业，用四个字形容最贴切：风声鹤唳。就在近......
### [组图：那英《浪姐2》C位出道后现身 粉丝送花祝贺人气爆棚](http://slide.ent.sina.com.cn/star/slide_4_704_355486.html)
> 概要: 组图：那英《浪姐2》C位出道后现身 粉丝送花祝贺人气爆棚
### [BI系统里的数据赋能与业务决策：问题诊断篇](https://www.tuicool.com/articles/NZVjmqM)
> 概要: 一、什么是问题诊断？今天我们来谈谈BI系统里数据决策很常见的一个场景应用——问题诊断。所谓问题诊断，就是找到在什么环节出现了问题，即定位问题，他有两层含义：第一，有没有问题？第二，问题出现在什么地方......
### [组图：菲利普亲王葬礼现场曝光 哈里王子返回英国出席](http://slide.ent.sina.com.cn/star/slide_4_704_355489.html)
> 概要: 组图：菲利普亲王葬礼现场曝光 哈里王子返回英国出席
### [Shiro + JWT + Spring Boot Restful 简易教程](https://www.tuicool.com/articles/YJjyYvq)
> 概要: 特性完全使用了 Shiro 的注解配置，保持高度的灵活性。放弃 Cookie ，Session ，使用JWT进行鉴权，完全实现无状态鉴权。JWT 密钥支持过期时间。对跨域提供支持。准备工作在开始本教程......
### [马斯克称将限量生产 Model Y，Cybertruck 年底试产](https://www.ithome.com/0/546/712.htm)
> 概要: 4 月 18 日消息，本周早些时候，电动汽车制造商特斯拉首席执行官埃隆・马斯克 (Elon Musk) 驾驶着全电动皮卡 Cybertruck，参观了其位于美国得克萨斯州的奥斯汀工厂，这款皮卡预计将在......
### [被“围猎”的清华征婚男](https://www.huxiu.com/article/422415.html)
> 概要: 本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。清华姚班毕业、曾供职于摩根大通与谷歌的张昆玮被群嘲了，起因是他的征婚帖。在豆瓣发......
### [《明日传奇》主演“热浪”与工作室不合 宣布退出](https://www.3dmgame.com/news/202104/3812683.html)
> 概要: DC剧集《明日传奇》参演明星多米尼克·珀塞尔今日在Instagram上公布了他离开这一剧集的消息，还透露了他与工作室之间的矛盾。虽然《明日传奇》已经续订了第7季，但珀塞尔今日证实，他并没有签署确保自己......
### [动画「薇薇 -萤石眼之歌-」Blu-ray&DVD第一卷封面公开](http://acg.178.com//202104/412711973880.html)
> 概要: 电视动画「薇薇 -萤石眼之歌-（Vivy -Fluorite Eye's Song-）」公开了Blu-ray&DVD第一卷封面。Blu-ray售价7,700日元（含税），DVD售价6,600日元（含税......
### [老日剧风格律政题材，大呼救命的白痴故事](https://news.dmzj1.com/article/70653.html)
> 概要: 日本2021年本季月九剧《鸦色刑事组》上线，一经播出本身站定最好时间节点，最优良的卡司，就是故事情节稀碎，令观众看得昏昏欲睡。该剧由竹野内丰、黑木华主演，改编自浅见理都的同名漫画，讲述东京地方法院第三分部第一刑事部的刑事法官入间道夫为避免冤案四处...
### [《暗黑破坏神2：重制版》根据原版MOD设计新功能](https://www.3dmgame.com/news/202104/3812684.html)
> 概要: 《暗黑破坏神2：重制版》向玩家的最大承诺就是保证游戏的原汁原味，但暴雪也对这个升级版本进行了一些改动。部分《暗黑破坏神2》的玩家担心这些改动会对游戏造成不良影响，而暴雪承诺只会进行一些感觉正确的改动，......
### [「Fate/Extella Link」尼禄手办开订](http://acg.178.com//202104/412714058985.html)
> 概要: GOODSMILE作品「Fate/Extella Link」尼禄手办开订，高约190mm，主体采用ABS、PVC材料制造。该手办定价为20166日元（含税），约合人民币1209元，预计于2021年10......
### [「不要欺负我，长瀞同学」ED主题曲无字幕MV公开](http://acg.178.com//202104/412714852626.html)
> 概要: 根据NANASHI创作的同名漫画改编，由Telecom Animation Film制作的电视动画「不要欺负我，长瀞同学」于近期公开了ED主题曲的无字幕MV，动画已于4月11日开始播出。「不要欺负我，......
### [罗永浩评 “流氓协议”：成为 “专业人士”甚至没有智力门槛](https://www.ithome.com/0/546/727.htm)
> 概要: 4 月 18 日消息，针对罗永浩曝光投资机构“流氓协议”事件后众多“专业人士”的解读，罗永浩在其微博再次发声，表示“专业人士”的“专业意见”把正常媒体和吃瓜群众都给弄糊涂了，并讽刺“成为‘专业人士’甚......
### [曝宋茜女扮男装造型，片场当众抠鼻孔擦鼻涕，偶像包袱碎了一地](https://new.qq.com/omn/20210418/20210418A02YWJ00.html)
> 概要: 前不久，宋茜、宋轶、王一博、黄轩等主演的古装剧《风起洛阳》已经顺利杀青了。但是在杀青之前，摄影师去剧组实地探班了，宋茜的造型也被摄影师吐槽，直呼这又是一副女扮男装的打扮。            宋茜扎......
### [硬盘也能挖矿：Chia(奇亚 XCH)挖矿教程](https://www.tuicool.com/articles/N3iAJvE)
> 概要: 不知大家发现了没有，最近硬盘也开始缺货了，相信你一定想知道这其中的原因。信不信由你，硬盘也能挖矿了！这几天来咨询的朋友，基本都是关于Chia的。今天我们就来简单聊聊这个Chia，看看它到底如何挖矿，以......
### [爱德华・斯诺登的 NFT 数字肖像以约 500 万美元卖出](https://www.ithome.com/0/546/731.htm)
> 概要: IT之家4 月 18 日消息 数字拍卖网站 Foundation 近日拍出了一件特殊的 NFT 数字艺术品，该艺术品名为“Stay Free”，是一张爱德华・斯诺登的特殊数字肖像。爱德华・斯诺登是美国......
### [TV动画「世界尽头的圣骑士」官方公开动画化贺图](http://acg.178.com//202104/412717226674.html)
> 概要: TV动画「世界尽头的圣骑士」官推公开了由本作角色原案輪くすさが所绘制的动画化贺图。「世界尽头的圣骑士」是柳野かなた自2015年5月1日起在网站“成为小说家吧”连载的轻小说作品，同名动画将于2021年1......
### [吴亦凡银发是yyds，漫画中走出来的美少年！](https://new.qq.com/omn/20210418/20210418A0370P00.html)
> 概要: 吴亦凡银发就是永远的神，漫画中走出来的美少年，这个颜太可了！娱乐圈中有很多艺人的颜值都是特别能打的，但是提到神颜两个字，是不是有不少网友第一个想到的就是吴亦凡，他的这张脸真的可以说是神仙颜值，光是靠脸......
### [中国如何跨过中等收入陷阱？](https://www.huxiu.com/article/422466.html)
> 概要: 本文来自微信公众号：智本社（ID：zhibenshe0-1），作者：清和社长，原文标题：《中国如何跨过中等收入陷阱？》日前，央行研究局陈浩等四位博士在《中国人民银行工作论文》上刊发的《关于我国人口转型......
### [关于用户，张小龙说了这11个核心观点](https://www.tuicool.com/articles/UzqYJfu)
> 概要: 最近在看《微信的产品观》这个本书，这本书汇集了张小龙对于做产品、做微信的诸多思考。开篇中关于用户一些观点，堪称经典。总结一些精要并结合自己做产品的思考，分享给你。用户是什么？回归本质，用户的本质就是人......
### [热巴为证明腿长真拼了！将“白透丝”当靴子穿，效果公开谁会不羡慕](https://new.qq.com/omn/20210416/20210416A0A5IE00.html)
> 概要: 能够在娱乐圈中称得上是拥有“神颜”的女星中，迪丽热巴必然是其中极具代表性的一位，自从迪丽热巴签约杨幂旗下公司出道发展后，她凭借自己身具的美就已经成为了娱乐圈中的大红人。随着迪丽热巴目前咖位的提升，现在......
### [轻改动画《现实主义勇者的王国再建记》公开视觉图及PV！](https://news.dmzj1.com/article/70654.html)
> 概要: 轻改动画《现实主义勇者的王国再建记》公开了视觉图及预告PV，并且追加了新的声优阵容。本作预计今年7月播出！
### [Soviet children’s books became collectors’ items in India](https://www.atlasobscura.com/articles/soviet-childrens-books-in-india)
> 概要: Soviet children’s books became collectors’ items in India
### [组图：洪欣独自拎大包行李现身机场 走路带风气场强](http://slide.ent.sina.com.cn/star/slide_4_704_355498.html)
> 概要: 组图：洪欣独自拎大包行李现身机场 走路带风气场强
### [组图：蒋勤勤独自亮相机场 “黑白配”出行简约时髦](http://slide.ent.sina.com.cn/star/slide_4_704_355502.html)
> 概要: 组图：蒋勤勤独自亮相机场 “黑白配”出行简约时髦
### [博鳌亚洲论坛旗舰报告：今年亚洲经济增速有望达到6.5%以上](https://finance.sina.com.cn/roll/2021-04-18/doc-ikmyaawc0363928.shtml)
> 概要: 原标题：博鳌亚洲论坛旗舰报告：今年亚洲经济增速有望达到6.5%以上 新华社海南博鳌4月18日电（记者周慧敏、陈子薇）在18日举行的博鳌亚洲论坛2021年年会首场新闻发布会上...
### [三星手机在美首次支持 eSIM 卡，Galaxy Note 20 系列率先 OTA 更新](https://www.ithome.com/0/546/743.htm)
> 概要: IT之家4 月 18 日消息 根据外媒 androidpolice 消息，目前在美国地区仅有苹果 iPhone 系列手机和谷歌 Pixel 支持 eSIM 卡，而三星手机尽管在硬件上早已支持该功能，但......
### [42岁邓丽君死因成谜，灵柩覆盖国民党党旗，国军少将：她是国民党女特务](https://new.qq.com/omn/20210416/20210416A0E10800.html)
> 概要: 1995年5月8日，对于全球邓丽君的歌迷来说，这是最为悲伤的一天。当天下午，邓丽君在泰国清迈去世，年仅42岁。邓丽君实在太出名了，她的歌声影响着一个时代，走在大街小巷，依旧能听到她的歌曲。有人说，邓丽......
### [省会城市房价变化：广州杭州南京涨幅居前三 7城低于一年前](https://finance.sina.com.cn/china/2021-04-18/doc-ikmxzfmk7505132.shtml)
> 概要: 原标题：省会城市房价变化：广州杭州南京涨幅居前三，7城低于一年前 3月份，21个省会城市二手房价环比上涨，广州、杭州和南京环比涨幅位居省会城市前三。
### [《骸骨骑士大人异世界冒险中》确定制作TV动画](https://www.3dmgame.com/news/202104/3812705.html)
> 概要: 官方日前宣布，秤猿鬼原作人气轻小说《骸骨骑士大人异世界冒险中》确定制作TV动画，开播日期暂时未定，最新预告先睹为快。·《骸骨骑士大人异世界冒险中》讲述「亚克」玩线上RPG玩到睡著，一醒来发现自己以游戏......
### [轻小说《世界尽头的圣骑士》宣布动画化！PV公开](https://news.dmzj1.com/article/70655.html)
> 概要: 由柳野彼方所著的小说《世界尽头的圣骑士》决定动画化，公开了PV、主要视觉图和声优阵容，预计今年10月播出。
### [轻小说《骸骨骑士大人异世界冒险中》宣布动画化！PV公开](https://news.dmzj1.com/article/70656.html)
> 概要: 由秤猿鬼所著的小说《骸骨骑士大人异世界冒险中》宣布TV动画化并公开了PV、主要视觉图和声优阵容，播出时间待定。
### [The “WAT” talk – a few programming languages quirks](https://www.destroyallsoftware.com/talks/wat)
> 概要: The “WAT” talk – a few programming languages quirks
### [挑战特斯拉：北汽+华为 阿尔法S值40万吗？华为店以后可能会卖车](https://finance.sina.com.cn/china/2021-04-18/doc-ikmyaawc0372582.shtml)
> 概要: 挑战特斯拉！北汽+华为，值40万吗？华为店以后可能会卖车．．．．．． 4月17日晚，北汽新能源旗下高端品牌ARCFOX极狐旗下的第二款新车阿尔法 S正式上市。
### [陕西新增4例境外输入确诊病例 新增8例境外输入无症状感染者](https://finance.sina.com.cn/china/2021-04-18/doc-ikmxzfmk7506623.shtml)
> 概要: 原标题：陕西新增4例境外输入确诊病例 新增8例境外输入无症状感染者 4月17日0—24时，陕西无新增报告本地确诊病例、疑似病例、无症状感染者。
### [微软正逐步放弃在Windows 10中整合Paint 3D应用](https://www.3dmgame.com/news/202104/3812707.html)
> 概要: 微软对旗下最新操作系统Windows 10之前进行了多次修改，包括增加和删除各种功能。在Windows 10秋季创意者更新中，微软引入了3D对象文件夹，并启用了经典的Paint和Paint 3D之间的......
### [阿里被罚182亿后 哪个大型互联网平台还在“赢家通吃”？](https://finance.sina.com.cn/china/2021-04-18/doc-ikmyaawc0372289.shtml)
> 概要: 阿里被罚182亿后，哪个大型互联网平台还在“赢家通吃”？ | 全景读书会 来源：全景财经 阿里巴巴“二选一”垄断案调查四个月后，监管决议终于落地。
### [新华社:QQ音乐、酷狗音乐、网易云音乐诱导粉丝氪金](https://www.3dmgame.com/news/202104/3812708.html)
> 概要: 新华社今日（4月18日）发表文章，点名 QQ 音乐、酷狗音乐、网易云音乐等音乐类 App 诱导粉丝氪金。消费者在花钱购买音乐版权后，音乐可以播放，但付费的界面仍在。同一个用户重复花钱购买同一个作品，重......
### [2021年度票房突破200亿元](https://finance.sina.com.cn/china/2021-04-18/doc-ikmyaawc0374444.shtml)
> 概要: 据灯塔实时数据，截至4月18日下午16时24分，中国电影市场2021年度总票房突破200亿元，总观影人次达 4.75亿，总场次3813.04万。
### [组图：低调撒糖！金宇彬申敏儿外出约会 互为对方拍照超甜](http://slide.ent.sina.com.cn/star/k/slide_4_704_355511.html)
> 概要: 组图：低调撒糖！金宇彬申敏儿外出约会 互为对方拍照超甜
### [张萌道歉有瘾，程莉莎因家庭琐事频上热搜，她们消停一会不行嘛？](https://new.qq.com/omn/20210418/20210418A062F000.html)
> 概要: 【存照第909期】（文/阿拉蕾）凌晨三点给员工布置工作，早上七点问四个小时过去，为什么还不回复？                        就算再有事业心的打工人看到老板说出这种话，拳头都得邦邦硬......
# 小说
### [末世核王](http://book.zongheng.com/book/407825.html)
> 作者：晓岸

> 标签：科幻游戏

> 简介：末世到来，道德、伦理、规矩、法律，一切都在瞬间崩塌。仅存的是残酷，是血腥，是同族相残，是凶兽肆虐还有丧尸横行。没有人能够置身事外，没有人可以逃脱这场浩劫。而那一世，他死了。这一世，他重生。但留给他的时间只剩下一天一夜，二十四小时后，浩劫再临！

> 章节末：第300章  决战黑旗

> 状态：完本
# 论文
### [DropClass and DropAdapt: Dropping classes for deep speaker representation learning](https://paperswithcode.com/paper/dropclass-and-dropadapt-dropping-classes-for)
> 日期：2 Feb 2020

> 标签：REPRESENTATION LEARNING

> 代码：https://github.com/cvqluu/dropclass_speaker

> 描述：Many recent works on deep speaker embeddings train their feature extraction networks on large classification tasks, distinguishing between all speakers in a training set. Empirically, this has been shown to produce speaker-discriminative embeddings, even for unseen speakers.
### [LaProp: a Better Way to Combine Momentum with Adaptive Gradient](https://paperswithcode.com/paper/laprop-a-better-way-to-combine-momentum-with)
> 日期：12 Feb 2020

> 标签：STYLE TRANSFER

> 代码：https://github.com/Z-T-WANG/LaProp-Optimizer

> 描述：Identifying a divergence problem in Adam, we propose a new optimizer, LaProp, which belongs to the family of adaptive gradient descent methods. This method allows for greater flexibility in choosing its hyperparameters, mitigates the effort of fine tuning, and permits straightforward interpolation between the signed gradient methods and the adaptive gradient methods.
