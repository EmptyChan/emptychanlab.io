---
title: 2021-11-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CorkscrewSwamp_ZH-CN2637396790_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-17 21:09:56
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CorkscrewSwamp_ZH-CN2637396790_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Blender 新渲染引擎已支持 AMD GPU](https://www.oschina.net/news/169299/blender-cycles-amd-gpu)
> 概要: Blender团队对 Cycles 渲染引擎的开发已持续了十年，如今也到了要对 Cycles 做出重大改进的时间节点，为此 Blender 启动了Cycles X项目。由于技术和性能方面的原因，Cyc......
### [安全日报 | Chrome 安全更新、一天披露两个索尼 PS5 漏洞](https://www.oschina.net/news/169426)
> 概要: 报告编号：B6-2021-111799报告来源：360CERT报告作者：360CERT更新日期：2021-11-171 Vulnerability | 漏洞Google Chrome 安全更新Fort......
### [Chrome 将代码分区，以加快 Android 上的启动时间](https://www.oschina.net/news/169308/chrome-android-faster-launch-times-less-memory)
> 概要: 11 月 16 日，谷歌宣布通过隔离拆分代码提高 Android 上 Chrome 的速度和内存使用率。通过这些改进，Android 上的 Chrome 现在使用的内存减少了 5-7%，并且启动和加载......
### [DataGear 2.10.0 发布，数据可视化分析平台](https://www.oschina.net/news/169313/datagear-2-10-0-released)
> 概要: DataGear2.10.0发布，新增7种内置图表，扩充看板API，具体更新内容如下：新增：内置图表新增涟漪气泡图、涟漪坐标散点图、涟漪地图散点图；新增：内置图表新增地图路径图、地图飞线图、平行坐标系......
### [氏家ト全漫画《妄想学生会》完结！](https://news.dmzj.com/article/72776.html)
> 概要: 由氏家ト全创作的漫画《妄想学生会》在本日（17日）发售的周刊少年Magazine51号（讲谈社）上迎来了最终回。
### [AniGift彩虹社Vtuber本间向日葵1/7比例手办](https://news.dmzj.com/article/72778.html)
> 概要: AniGift根据彩虹社Vtuber本间向日葵制作的1/7比例手办目前已经开订了。本作附带兽耳和笑脸的可替换表情。向日葵发饰、蝴蝶结等装饰也一并被再现了出来。
### [Weekly Top 10 lists of the most-watched TV and films](https://top10.netflix.com/?)
> 概要: Weekly Top 10 lists of the most-watched TV and films
### [openEuler捐赠，开源盛世开启](https://segmentfault.com/a/1190000040974815?utm_source=sf-homepage)
> 概要: 中国的操作系统，在开源时代迎来了最好的发展契机。如果说三年前，有人谈及中国的国产操作系统，可能真的会让人摸不到头脑。面对Windows、Android、iOS三大系统碾压般的市场优势，无论是终端用户还......
### [P站美图推荐——干花特辑](https://news.dmzj.com/article/72783.html)
> 概要: 永久保存，虽然褪色了但仍然美丽的干花，经常作为一种象征而用于插画中
### [招行的财富密码里，钻进一只蚂蚁](https://www.huxiu.com/article/473250.html)
> 概要: 来源｜妙投APP作者｜刘国辉头图｜电影《英雄本色》截图蛰伏了好久的银行股，终于支棱起来了。在投资机构看来，低估值、业绩恢复的预期，共同助推了银行股估值的修复。银行业也成为四季度确定性比较强的行业之一......
### [大岛司《足球风云》完全原创动画制作决定！](https://news.dmzj.com/article/72786.html)
> 概要: 大岛司创作的漫画《足球风云》，宣布了完全原创动画制作决定的消息。本作动画名称为《足球风云Goal to the Future》预计将于2022年年内播出。
### [国漫《雾山五行2》曝水墨概念海报 犀川幻紫林来袭](https://acg.gamersky.com/news/202111/1438664.shtml)
> 概要: 由六道无鱼动画工作室制作的原创网络动画片《雾山五行》，第二章《犀川幻紫林》即将推出，官方公开了《犀川幻紫林》的水墨概念海报。
### [剧场版动画「刀剑神域：进击篇·无星夜的咏叹调」上映第4周特典公开](http://acg.178.com/202111/431121278129.html)
> 概要: 近日，剧场版动画「刀剑神域：进击篇·无星夜的咏叹调」公开了上映第4周的特典，本次特典附带了广播剧的卡片，描写了西莉卡和莉兹贝特如何走出绝望踏出第一步的「起始之日」的故事。获取时间为11月20日至11月......
### [漫画「化物语」第15卷彩页公开](http://acg.178.com/202111/431121552236.html)
> 概要: 今日（11月17日），漫画「化物语」第15卷正式发售。官方公开了该卷的彩页插图。漫画「化物语」改编自日本轻小说家西尾维新原作的「物语系列」轻小说的第一部「化物语」，由大暮维人负责作画......
### [清华模拟量子计算机 4200万核CPU性能达440亿亿次](https://www.3dmgame.com/news/202111/3828550.html)
> 概要: 量子计算机是未来新型计算系统的重点之一，如何实现量子霸权——性能超过经典计算机是这个领域的重要目标。在日前SC超算大会上，清华大学教授付昊桓使用中国的神威超算模拟了量子计算机，最多可扩展到4200万C......
### [像素风《厨师RPG》公布 登陆Switch和PC平台](https://www.3dmgame.com/news/202111/3828554.html)
> 概要: 开发商 World 2 Studio 公布了《厨师RPG》。在这款像素风游戏中玩家将扮演一名厨师，并肩负重振一个沿海小镇中曾经极负盛名的饭店的任务。游戏将于 2023 年 Q3 同步登陆任天堂 Swi......
### [「SD高达世界：群英集」外传企划名确定 主视觉图公布](http://acg.178.com/202111/431127462446.html)
> 概要: 原创电视动画「SD高达世界：群英集」外传企划的名字现已确定，为“THE LEGEND OF DRAGON KNIGHT”，本作的主视觉图、角色视觉图也已公布。制作监督：池添隆博系列构成：待田堂子SD设......
### [庆祝HP20周年 HBO Max制作“重返霍格沃兹”特辑](https://www.3dmgame.com/news/202111/3828561.html)
> 概要: 今日（11月17日），HBO Max宣布庆祝哈利波特电影系列20周年将举办“重返霍格沃兹”主题活动，于此同时还公布本场主题特辑的预告片，特辑将在2022年1月1日正式开始，特辑里面包含铁三角在内的多名......
### [The OBS project has accused Streamlabs of copying their name and trademark](https://twitter.com/OBSProject/status/1460782968633499651)
> 概要: The OBS project has accused Streamlabs of copying their name and trademark
### [“蔚小理”们的座舱都卷到这种程度了](https://www.huxiu.com/article/473420.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自视觉中国2019年4月，上海车展二层的两个零部件馆里，不少人称在逛了一圈后，被“智能座舱”这个词洗了脑。无论是Tier1、人工智能企业，还是仪表盘、屏幕与内饰制......
### [今泉佑唯生子后复出 首次主演舞台剧《修罗雪姬》](https://ent.sina.com.cn/2021-11-17/doc-iktzscyy6070928.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 原欅坂46今泉佑唯作为女星复出，生子后首次主演舞台剧《修罗雪姬》，该剧将于11月19日上演。今泉佑唯去年十月停止活动，今年六月生下一胎，这是她时隔一年复出出演作品，当天接......
### [韩国YG前练习生韩瑞熙缓刑期再吸毒被判刑一年半](https://ent.sina.com.cn/s/j/2021-11-17/doc-iktzqtyu7835556.shtml)
> 概要: 新浪娱乐讯　韩国YG娱乐公司原训练生韩瑞熙今天因缓刑期间再次吸毒而被一审判处一年半有期徒刑并被当庭拘留。　　韩瑞熙2017年因多次吸食大麻而被判处有期徒刑三年，缓刑四年。去年7月，韩瑞熙被警方抽检出毒......
### [菅田将晖报告与小松菜奈结婚 采访其心情称很幸福](https://ent.sina.com.cn/2021-11-17/doc-iktzqtyu7838463.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 人气歌手、演员菅田将晖主持11月15日深夜播出的日本放送直播节目《菅田将晖的All Night Nippon（ANN）》，向粉丝报告与女星小松菜奈结婚的喜讯，当天他已经通......
### [「奥特银河格斗：命运的冲突」角色视觉图公开](http://acg.178.com/202111/431132174933.html)
> 概要: 近日，「奥特曼」系列特摄剧「奥特银河格斗：命运的冲突」公开了角色视觉图，该作将于2022年上线。利布特奥特曼阿布索留特·迪亚波罗阿布索留特·塔尔塔洛斯该作故事承接上部「奥特银河格斗：巨大的阴谋」和「泽......
### [成田凌新剧演通缉犯天才外科医 本人表示非常期待](https://ent.sina.com.cn/2021-11-17/doc-iktzqtyu7840362.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道，人气男星成田凌接拍日本台2022年1月开播日剧《逃亡医F》，这是他首次主演黄金时段剧集，饰演被人诬告杀了女友遭到全日本通缉的天才外科医，本人表示非常期待挑战这个角色。　　该......
### [王健林67岁依旧四处奔忙 称王思聪没兴趣接班](https://www.3dmgame.com/bagua/4994.html)
> 概要: 王健林已经67岁了，似乎还没把退休甚至退居二线摆上个人日程，万达接班人也没浮出水面。王健林依旧四处奔忙，亲力亲为。据统计，1月以来，王健林分别见了天津、大连、肇庆、珠海、长春、承德，以及丹寨等地的相关......
### [滨崎步右脚踝手术顺利结束出院 称上手术台后吓晕](https://ent.sina.com.cn/2021-11-17/doc-iktzqtyu7842294.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 人气歌手滨崎步11月16日在粉丝俱乐部网站报告本月4日骨折的右脚踝骨折手术顺利结束，已经出院回家，还称上手术台太害怕吓晕过去，已经很久没有如此紧张。　　11月14日滨崎步......
### [感同身受是一种幻觉吗？](https://www.huxiu.com/article/473413.html)
> 概要: 本文来自微信公众号：南京大学出版社（ID：njupress），作者：日广松涉，翻译：邓习议，导语：南大社，头图来源：视觉中国在日常生活中，我们区别四周的存在和我们自身的身体，同时自在地理解这种身体的自......
### [Steam每日特惠：唯美2D平台游戏《花之灵》新史低](https://www.3dmgame.com/news/202111/3828577.html)
> 概要: 今天（11 月 17 日）加入 Steam 免费特惠的游戏是由 Skrollcat Studio 开发，PM Studios 和 CE-Asia 于 2021 年 8 月 24 日发行的温馨 2D 平......
### [网飞误播《鬼灭之刃》草稿镜头致歉 粉丝却表示感谢](https://acg.gamersky.com/news/202111/1438760.shtml)
> 概要: 在动画剧集《鬼灭之刃：无限列车篇》的第5话中，片中使用了还在编辑中的草稿画面。网飞对此迅速的发布致歉声明并且修正画面。不过意外的是有不少粉丝表示感谢网飞。
### [南极人拟花5亿元买吊牌，又想收割谁？](https://www.tuicool.com/articles/jUVVBrY)
> 概要: 是新朋友吗？记得先点蓝字“锌刻度”关注我哦～每日一篇科技财经深度调查走进商业背后的故事成也吊牌，败也吊牌？撰文/黎文婕编辑/炫 岐自家吊牌越来越难卖以后，南极人似乎又走上了买吊牌之路。日前，南极电商股......
### [Amazon to stop accepting UK Visa credit cards](https://www.bloomberg.com/news/articles/2021-11-17/amazon-will-stop-accepting-visa-credit-cards-issued-in-the-u-k)
> 概要: Amazon to stop accepting UK Visa credit cards
### [《派对浪客诸葛孔明》PV公开 诸葛亮重生做经纪人](https://acg.gamersky.com/news/202111/1438783.shtml)
> 概要: 小川亮创作的搞笑漫画《派对浪客诸葛孔明》宣布将推出TV动画，官方公开了《派对浪客诸葛孔明》的PV和视觉图，穿越后的诸葛孔明和月见英子在东京街头相遇了。
### [Q3斗鱼亏损7270万VS虎牙盈利1.8亿，虎鱼之争没有赢家](https://www.tuicool.com/articles/iI3qyqn)
> 概要: 斗鱼的日子过得还是没有虎牙光彩。北京时间11月16日，斗鱼于美股盘前发布了2021年第三季度财报。财报显示，斗鱼在2021年第三季度实现营业收入23.5亿元，同比下滑7.8%；非美国通用会计准则下，录......
### [《迪迦奥特曼》为什么会拥有高额的人气？](https://new.qq.com/omn/20211117/20211117A039RB00.html)
> 概要: 《迪迦奥特曼》是我看的第一部奥特系列，想想曾经放学回家守在电视上的我（自嘲一笑时间真快啊）。感觉很深刻的地方就是，迪迦是人，是光，是保护人类的奥特曼。可是大古就是迪迦，迪迦身体里是没有迪迦的精神体的，......
### [Microsoft Calls Firefox’s Browser Workaround “Improper,” Will Block It](https://www.howtogeek.com/768727/microsoft-calls-firefoxs-browser-workaround-improper-will-block-it/)
> 概要: Microsoft Calls Firefox’s Browser Workaround “Improper,” Will Block It
### [cos：王者荣耀 安琪拉时之奇旅 “给三次元的世界加点惊喜”](https://new.qq.com/omn/20211117/20211117A0ACR200.html)
> 概要: 更多内容，尽在cosplay二次元大全“ 给三次元的世界加点惊喜 ”提供：关玉郎摄后：灰豆出镜：娜娜酱后勤：青木，三岁                                         ......
### [雷蛇迅镖幻彩 RGB 风扇上市：液压轴承，299 元](https://www.ithome.com/0/587/329.htm)
> 概要: IT之家11 月 17 日消息，雷蛇上个月发布的迅镖幻彩风扇现已上市，120mm 版 299 元，140mm 版 329 元。据介绍，雷蛇迅镖风扇有着至高每分钟约 2200 转的转速，它的风扇叶片借鉴......
### [龙珠超：同为神明，神龙为何被比鲁斯吓到颤抖？](https://new.qq.com/omn/20211117/20211117A0AP2P00.html)
> 概要: 在龙珠剧场版神与神当中出现这么有趣的一幕：悟空为了弄清超赛神为何物，选择召唤神龙出来解答。而神龙看到一旁的比鲁斯之后，一改往日的威严，吓得哆哆嗦嗦，连话都说不利索，一口一个小神地称呼自己，滑稽的样子让......
### [马斯克能拿诺贝尔和平奖吗？](https://www.huxiu.com/article/473558.html)
> 概要: 出品｜虎嗅汽车组作者｜梓楠法师是的，机会在眼前，就看马斯克能不能把握了。只需66亿美元，就能拯救43个国家和地区4200万正在挨饿的人们，获得为世人称道歌颂的机会，或者被某个丛林深处的部落奉为神明。这......
### [单月流水过亿，在抖音快手闷声挣钱的人](https://www.tuicool.com/articles/y26JNbz)
> 概要: 开淘宝、做直播没暴富，做“联盟团长”或许可以。“联盟”是一种基于CPS（按成交计费）的商品推广分销系统。以成立最久的淘宝联盟为例，商家通过淘宝联盟，调动百万微信群主、宝妈和羊毛党，在全网各处推广自家产......
### [逆天阵容！巨大彗星将毁地球！科幻喜剧大片《不要抬头》正式预告](https://new.qq.com/rain/a/20211117V0AY5700)
> 概要: 逆天阵容！巨大彗星将毁地球！科幻喜剧大片《不要抬头》正式预告
### [微软 Win10 LTSC 2021 （长期服务频道）正式版发布，附 MSDN 官方 ISO 纯净镜像下载](https://www.ithome.com/0/587/333.htm)
> 概要: IT之家11 月 17 日消息，微软今天正式推出Windows 102021 年 11 月更新（21H2），与此同时，微软 Windows 10 企业版 LTSC 2021 正式版（长期服务频道）发布......
### [何刚：华为智能穿戴设备全球累计发货量超 8000 万台，中国出货第一](https://www.ithome.com/0/587/334.htm)
> 概要: 11 月 17 日晚间消息，在今日的华为智慧生活发布会上，华为推出了智能手表、PC 等新品。今日发布会上，华为将推出 WATCH GT Runner、WATCH GT 3、FreeBuds Lipst......
### [从10月份市场供需积极变化看中国经济韧性](https://finance.sina.com.cn/jjxw/2021-11-17/doc-iktzscyy6135850.shtml)
> 概要: 原标题：从10月份市场供需积极变化看中国经济韧性 新华社北京11月17日电题：从10月份市场供需积极变化看中国经济韧性 新华社记者魏玉坤、丁乐 读懂中国经济...
### [律所短视频普法缘何被罚？](https://www.tuicool.com/articles/yYJnii3)
> 概要: 本文来自微信公众号：法治周末报（ID：fzzmb01），作者：王京仔（法治周末记者），责任编辑：仇飞，题图来自：视觉中国在自媒体盛行的今天，短视频普法已成一种常态。日前，北京某律师事务所却因其自制并发......
### [李克强出席世界经济论坛全球企业家特别对话会](https://finance.sina.com.cn/china/2021-11-17/doc-iktzscyy6136550.shtml)
> 概要: 原标题：李克强出席世界经济论坛全球企业家特别对话会 新华社北京11月17日电 国务院总理李克强11月16日晚在人民大会堂出席世界经济论坛全球企业家特别对话会...
### [杨幂的《斛珠夫人》中，徐开骋才是最亮的仔！](https://new.qq.com/rain/a/20211117A04W7000)
> 概要: “南海之外有鲛人，水居如鱼，不废织绩。”“其眼泣，则能出珠。”——《搜神传》            茫茫荒野上，一抹血红刺痛双眼，凶险鲛海中，鲛人悲哀绝唱，宫廷深处，阴谋如藤蔓缠绕开散……这是《九州·......
### [LV大秀生图来了！热巴暴露发际线，周冬雨瘦过头，刘亦菲状态绝佳](https://new.qq.com/rain/a/20211117A0BAIC00)
> 概要: 又一场时尚圈活动来啦！经历了芭莎慈善夜、GQ年度盛典之后，LV品牌大秀也准时开启，和前两次活动形式不同，这次大秀主办方即品牌，到场嘉宾均需穿着该品牌服装，是一场纯粹的时尚表现力考验，快来看看明星们表现......
### [微信 macOS 版 3.2.2.1 Beta 发布：免打扰群可收进“折叠群聊”，置顶聊天可折叠](https://www.ithome.com/0/587/338.htm)
> 概要: IT之家11 月 17 日消息，今天腾讯微信团队推送发布了微信 macOS 版 3.2.2.1 Beta 应用更新，通过该更新，可以把免打扰的群收进「折叠的群聊」；置顶聊天过多时，可以将其折叠；修复了......
### [四部门：探索开展旅客行李直挂服务](https://finance.sina.com.cn/jjxw/2021-11-17/doc-iktzscyy6137559.shtml)
> 概要: 日前，交通运输部办公厅、公安部办公厅、中国民用航空局综合司、中国国家铁路集团有限公司办公厅联合印发《关于开展空铁（轨）联运旅客换乘流程优化工作的通知》（以下简称...
### [斗罗大陆：唐三表情崩坏，瞪眼状态像二哈](https://new.qq.com/omn/20211117/20211117A0BIU000.html)
> 概要: 在《斗罗大陆》动漫中，唐三经历过多次建模以及微调，到最后却仿佛整容过度，脸型太尖了，宛如锥子脸，最初换回束发造型的时候，面容精致到像个女生。觉醒蓝银皇血脉的时候，建模太壮硕，束发求婚之后，建模又太女气......
### [34部电影冲击贺岁档，哪一部票房能赶上《长津湖》的零头](https://new.qq.com/rain/a/20211117A0BLU800)
> 概要: 随着《误杀2》定档12月24日上映，今年的贺岁档电影数量达到了34部。            根据央媒给出的标准，本次贺岁档统计的范围从11月26日至12月31日，所以像11月19日上映白百何、白客、......
### [曲婉婷之母张明杰一审被判无期，案件重要当事人一月前回国投案](https://finance.sina.com.cn/jjxw/2021-11-17/doc-iktzqtyu7904447.shtml)
> 概要: 原标题：曲婉婷之母张明杰一审被判无期，案件重要当事人一月前回国投案 新京报快讯（记者展圣洁）2021年11月17日上午，黑龙江省哈尔滨市中级人民法院对哈尔滨市城镇化建设...
### [重庆市3个中风险地区调整为低风险地区](https://finance.sina.com.cn/jjxw/2021-11-17/doc-iktzscyy6140770.shtml)
> 概要: 17日，重庆市召开疫情防控新闻发布会，重庆市卫生健康委副主任李畔介绍，从11月13日0时至11月17日18时，重庆市无新增确诊病例和无症状感染者。
### [贺一诚发表施政报告：综合旅游业仍是澳门经济的重要支柱](https://finance.sina.com.cn/china/gncj/2021-11-17/doc-iktzqtyu7905876.shtml)
> 概要: 原标题：贺一诚发表施政报告：综合旅游业仍是澳门经济的重要支柱 推动澳琴旅游及相关产业合作，推出激励项目，对前往横琴举办会议...
# 小说
### [四合院：我崔大可不在偷牛](https://m.qidian.com/book/1030609477/catalog)
> 作者：沦陷宿殇

> 标签：另类幻想

> 简介：十八岁的崔大可，悄悄的融入了四九城。这个世界太复杂，人是铁饭是钢，情满四合院，正阳门下小女人，金婚……梁拉娣:大可的身材真好。秦淮如:大可老是想骑自行车。秦京茹:大可哥，教导我不要有那么多心眼。丁秋楠:大可真棒。……崔大可很感慨，咱也不缺吃不缺穿的，低调!

> 章节总数：共126章

> 状态：完本
# 论文
### [Persona Authentication through Generative Dialogue | Papers With Code](https://paperswithcode.com/paper/persona-authentication-through-generative)
> 日期：25 Oct 2021

> 标签：None

> 代码：None

> 描述：In this paper we define and investigate the problem of \emph{persona authentication}: learning a conversational policy to verify the consistency of persona models. We propose a learning objective and prove (under some mild assumptions) that local density estimators trained under this objective maximize the mutual information between persona information and dialog trajectory.
### [NOD: Taking a Closer Look at Detection under Extreme Low-Light Conditions with Night Object Detection Dataset | Papers With Code](https://paperswithcode.com/paper/nod-taking-a-closer-look-at-detection-under)
> 日期：20 Oct 2021

> 标签：None

> 代码：None

> 描述：Recent work indicates that, besides being a challenge in producing perceptually pleasing images, low light proves more difficult for machine cognition than previously thought. In our work, we take a closer look at object detection in low light.
