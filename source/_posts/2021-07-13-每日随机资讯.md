---
title: 2021-07-13-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MooseVelvet_ZH-CN5891459899_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-07-13 23:31:14
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MooseVelvet_ZH-CN5891459899_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [openEuler 社区成立 OSCourse SIG，在 openEuler 构建开源操作系统课程推广平台](https://www.oschina.net/news/150235)
> 概要: 开源软件供应链点亮计划，等你来！>>>经 openEuler 社区技术委员会讨论批准，openEuler 社区正式成立 OSCourse SIG。OSCourse SIG 致力于发展和推动开源操作系统......
### [ng-zorro-antd 12.0.0 发布，Ant Design 的 Angular 实现](https://www.oschina.net/news/150144/ng-zorro-antd-12-0-0-released)
> 概要: 开源软件供应链点亮计划，等你来！>>>ng-zorro-antd 是 Ant Design 的 Angular 实现，主要用于研发企业级中后台产品。全部代码开源并遵循 MIT 协议，任何企业、组织及个......
### [Audacity 的两个分支争夺成为真正的继任者](https://www.oschina.net/news/150156/two-audacity-forks)
> 概要: 开源软件供应链点亮计划，等你来！>>>知名开源免费跨平台音频编辑器 Audacity 今年早些时候被 Muse Group 收购，它最近更新了隐私政策，表示可能会按执法机构、诉讼和当局要求收集必要的数......
### [Choerodon 猪齿鱼 1.0 先行版已发布](https://www.oschina.net/news/150238/choerodon-1-0-eap-released)
> 概要: 开源软件供应链点亮计划，等你来！>>>自汉得宣布开源以来，Choerodon猪齿鱼已被上千个组织所使用，帮助企业完成软件的生命周期管理，从而更快、更频繁地交付更稳定的软件。经过一千一百多天的奋战，20......
### [小说《怪盗女王喜欢马戏团》剧场OVA化！](https://news.dmzj.com/article/71514.html)
> 概要: 小说《怪盗女王喜欢马戏团》宣布了剧场OVA化的消息。本作将在系列刊行20周年的2022年在影院上映。
### [漫画《不良少年与白手杖女孩》真人电视剧化决定](https://news.dmzj.com/article/71515.html)
> 概要: 漫画《不良少年与白手杖女孩》宣布了真人电视剧化决定的消息。本作将于10月开始每周播出。
### [视频号炮灰简史](https://www.huxiu.com/article/440732.html)
> 概要: 本文来自微信公众号：号榜（ID：haorank123），作者：第二秘书，头图来自：《分手大师》剧照任何一个平台的起来，一定会伴随着一批陪跑者，所谓陪跑者，就是总有那么一批人，看好这个平台，然后在某个阶......
### [ABEMA公开4月番动画总部放量与评论排行榜](https://news.dmzj.com/article/71526.html)
> 概要: ABEMA公开了2021年4月播出的春番动画的播放量排行榜和评论排行榜。其中，排名累计播放量部门第一的是《剃须。然后捡到女高中生。》。排名评论排行榜部门第一的是《佐贺偶像是传奇》。
### [《东方Project》新作游戏制作决定！2022年公开](https://news.dmzj.com/article/71527.html)
> 概要: Cave宣布了已经获得《东方Project》的相关权利，正式开始制作新作游戏的消息。新作游戏预计将于2022年与大家见面。不过有关本作游戏的更多详情，还将于日后公开。
### [视频：倪妮优雅白裙逛表展尽显女神范 自曝即将进组拍戏](https://video.sina.com.cn/p/ent/2021-07-13/detail-ikqciyzk5150627.d.html)
> 概要: 视频：倪妮优雅白裙逛表展尽显女神范 自曝即将进组拍戏
### [视频：28位港姐入围佳丽参加晋级赛 朱晨丽张秀文何依婷靓丽助阵](https://video.sina.com.cn/p/ent/2021-07-13/detail-ikqcfnca6547465.d.html)
> 概要: 视频：28位港姐入围佳丽参加晋级赛 朱晨丽张秀文何依婷靓丽助阵
### [三甲医院集体“搬家”，北京医疗的大城市病好了吗？](https://www.huxiu.com/article/440745.html)
> 概要: 本文来自微信公众号：八点健闻（ID：HealthInsight），作者：方澍晨、李琳，题图来自：视觉中国非首都功能疏解，改写了北京医疗资源扎堆核心区的历史。鲜为人知的是，全国患者的“就诊中心”——北京......
### [RMA: Rapid Motor Adaptation for Legged Robots](https://ashish-kmr.github.io/rma-legged-robots/)
> 概要: RMA: Rapid Motor Adaptation for Legged Robots
### [TV动画「天官赐福」日配版ED公开](http://acg.178.com/202107/420148079919.html)
> 概要: 近日，TV动画「天官赐福」公布了日配版ED主题曲「フリイジア」，该动画已于2021年7月4日在日本播出。TV动画「天官赐福」日配版EDCAST谢怜（シエ・リェン）：神谷浩史三郎（サンラン）：福山润霊文......
### [《炉石传说：佣兵战纪》全球首曝，尽在TapTap游戏发布会](https://shouyou.3dmgame.com/news/56510.html)
> 概要: 对炉石玩家而言，已经过去的“凤凰年”无疑是一场盛大的冒险。我们前往“外域的灰烬”中击退了铁锈军团的进犯，在举世闻名的“通灵学园”中成功毕业，在“疯狂的暗月马戏团”中享受着奇妙的庆典与表演。当这一切尘埃......
### [A妹晒与新婚丈夫蜜月照 坐木鞋看风车甜蜜恩爱](https://ent.sina.com.cn/y/youmei/2021-07-13/doc-ikqcfnca6576075.shtml)
> 概要: 新浪娱乐讯 7月13日，据外媒报道，7月11日星期天，流行歌手爱莉安娜·格兰德（Ariana Grande）在社交网上晒出与新婚丈夫达尔顿·戈麦斯（Dalton Gomez）在荷兰阿姆斯特丹度蜜月的照......
### [进攻与防守：小米的组织能力尚未经过大考](https://www.huxiu.com/article/440818.html)
> 概要: 本文来自微信公众号：晚点LatePost（ID：postlate），作者：贺乾明，编辑：程曼祺，原文标题：《进攻与防守，小米造车后的双重组织架构》，头图来自：视觉中国公司对组织管理的重视程度常取决于对......
### [「Love Live! 虹咲学园学园偶像同好会」联动大江户温泉物语视觉图公开](http://acg.178.com/202107/420155016656.html)
> 概要: 「Love Live! 虹咲学园学园偶像同好会」公开了与东京台场大江户温泉物语联动的视觉图，该活动将于7月30日举办，持续至9月5日。「LoveLive！虹咲学园学园偶像同好会」是一个虚拟偶像团体企划......
### [森下suu「全职猎人」新绘公开](http://acg.178.com/202107/420155154395.html)
> 概要: 「日日蝶蝶」的作者森下suu老师公开了其最新绘制的「全职猎人」绘图，登场人物为西索（ヒソカ）、杰（ゴン）和奇犽（キルア）。「全职猎人」是富坚义博创作的漫画作品，于1998年3月16日起在日本集英社旗下......
### [《EVA剧场版：终》票房100亿日元达成！耗时127天](https://acg.gamersky.com/news/202107/1405629.shtml)
> 概要: 奥特曼中国官方微博再次公布新奥特曼战袍形象，今天来的是最辛苦的反派奥——吕布战袍·贝利亚奥特曼。
### [日本漫画家为东京奥运会绘制的应援插画公开](http://acg.178.com/202107/420157290466.html)
> 概要: 日本漫画家（尾田荣一郎、谏山创、武内直子、安彦良和）公开了为东京奥运会绘制的应援插画。尾田荣一郎（海贼王）谏山创（巨人）武内直子（美少女战士）安彦良和（高达）......
### [视频：谷嘉诚陈小纭恋情疑曝光 两人一起进入酒店过夜](https://video.sina.com.cn/p/ent/2021-07-13/detail-ikqcfnca6606468.d.html)
> 概要: 视频：谷嘉诚陈小纭恋情疑曝光 两人一起进入酒店过夜
### [想要离开互联网，最好是在10年前，其次是现在](https://www.tuicool.com/articles/mmiEz2R)
> 概要: 编者按：本文来源创业邦专栏赤潮AKASHIO，作者河大弯大，图源摄图，创业邦经授权转载。那些逃离网络的人，过得好么？过去十年，我们经历了一场互联网革命。网络从一根根网线连接的计算机里，跳跃到了每个人的......
### [这部以“发刀”闻名的国产动画，是你不能错过的年度黑马](https://acg.gamersky.com/news/202107/1405748.shtml)
> 概要: EVA官推宣布，《新·福音战士剧场版：终》票房于突破了100亿日元大关，电影自公布后多次延期，终于在今年3月8日登陆日本院线，到7月12日，花了127天达成百亿票房。
### [网友自制《血源》传闻生成器  真假难辨小编噩梦](https://www.3dmgame.com/news/202107/3818872.html)
> 概要: 2015年在PS4上独占发售的Fromsoft游戏《血源诅咒》因其结合了洛式世界风格剧情和魂系玩法获得了一致好评。在许多玩家心中，这是一部令人记忆深刻的经典作品。奈何不仅PS4主机年代久远，游戏本体在......
### [《最后的生还者》电视剧或于2022年下半年开播](https://www.3dmgame.com/news/202107/3818873.html)
> 概要: HBO已于7月5日正式开始《最后的生还者》改编真人剧的拍摄工作，本片2022年6月8日完成拍摄，近日确认将执导本剧的康捷米尔·巴拉戈夫在自己的任职经历上提到《最后的生还者》真人电视剧，后面跟着时间20......
### [墓志铭就是我的人间指南](https://www.huxiu.com/article/440889.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡题图 | 《The Simpsons》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。想......
### [刘慈欣《赡养上帝》漫画化决定！日本漫画家创作](https://acg.gamersky.com/news/202107/1405812.shtml)
> 概要: 据微博科幻MCN机构@未来事务管理局官微消息，刘慈欣的小说《赡养上帝》将漫画化，由日本漫画家创作。
### [工信部、国家网信办、公安部印发网络产品安全漏洞管理规定](https://www.tuicool.com/articles/JfeQfeY)
> 概要: DoNews7月13日消息（田小梦）今日，工业和信息化部、国家互联网信息办公室、公安部联合印发《网络产品安全漏洞管理规定》（以下简称《规定》）的通知。《规定》指出，任何组织或者个人不得利用网络产品安全......
### [华为鸿蒙 HarmonyOS 2 智慧识屏最新支持“微信头像和昵称自动涂抹”功能](https://www.ithome.com/0/562/610.htm)
> 概要: IT之家7 月 13 日消息 据网友反馈，近期华为手机上的智慧识屏 App 迎来了 11.1.7.303 更新，本次更新带来了在微信中支持对头像和昵称自动涂抹 (beta) 功能，用户可在微信聊天窗口......
### [天南海北共赴约！《一人之下》动画五周年庆典圆满落幕](https://new.qq.com/omn/ACF20210/ACF2021071300861000.html)
> 概要: 转眼间,距离粉丝们与《一人之下》动画的初识已有整整五年，一路走来，《一人之下》动画收获了不菲的成绩，也与遍布五湖四海的异人粉们结下了不解之缘。            为了纪念这个特殊的日子，7月9日，......
### [最前线 |「罗布乐思」正式上线，但“中国Roblox”的路还很长](https://www.tuicool.com/articles/j6Jzuqf)
> 概要: 7月13日，「罗布乐思」正式登陆App Store、各大手机应用商店及taptap等第三方平台。罗布乐思是一个主打3D沙盒类玩法的游戏平台，在腾讯与美国Roblox合资创办团队完成本地化开发优化后，由......
### [视频：聊天记录曝光！李现和谷嘉诚前女友吐槽他是“装逼侠”](https://video.sina.com.cn/p/ent/2021-07-13/detail-ikqciyzk5244166.d.html)
> 概要: 视频：聊天记录曝光！李现和谷嘉诚前女友吐槽他是“装逼侠”
### [Where in the HypeCycle is GraphQL?](https://wundergraph.com/blog/where_in_the_hypecycle_is_graphql_in_2021_analyzing_public_data_from_google_trends_stackoverflow_github_and_hackernews)
> 概要: Where in the HypeCycle is GraphQL?
### [周杰伦点赞！郎朗分享与娇妻吉娜练琴照，妻子含情脉脉，羡煞旁人](https://new.qq.com/omn/20210713/20210713A0BEXJ00.html)
> 概要: 有关娱乐圈中明星夫妻的感情，一直都是各路粉丝和吃瓜网友关注的热点内容，其实大家不仅仅是想知道明星夫妻今天吵了一架，明天有人“不老实”，也有相当一部分人是非常喜欢看他们秀恩爱的。而在这个领域，国际钢琴大......
### [欧拉樱桃猫现身工信部：旗下首款纯电 SUV](https://www.ithome.com/0/562/621.htm)
> 概要: IT之家7 月 13 日消息 继黑猫、白猫、好猫之后，欧拉又一款新能源车型“樱桃猫”现已通过工信部申报入网，从实拍图来看类似 WEY 玛奇朵。IT之家了解到，这款车型长宽高分别为 4510/1855/......
### [MacKichan Software, maker of Scientific Word, has gone out of business](https://www.mackichan.com/index.html?techtalk/407.htm~mainFrame)
> 概要: MacKichan Software, maker of Scientific Word, has gone out of business
### [Show HN: Fakeflix – Netflix open source clone](https://github.com/Th3Wall/Fakeflix)
> 概要: Show HN: Fakeflix – Netflix open source clone
### [大众 CEO：我们给自己设定的目标是成为电动汽车领域的全球领导者](https://www.ithome.com/0/562/626.htm)
> 概要: IT之家7 月 13 日消息 大众集团今日宣布，他们将在电池电动自动驾驶领域释放价值，并提出到 2030 年的新汽车战略 。大众表示，将于 2026 年开始在新平台上生产纯电动汽车，预计到 2030 ......
### [过气明星不如网红！昔日反派女主直播苦等女网红，姿态摆得太低了](https://new.qq.com/omn/20210713/20210713A0CX3T00.html)
> 概要: 明星身为公众人物最需要维持的就是自己的热度和流量，因为对于那些所谓的过气明星而言，没有了大家的关注和喜欢，也不会有任何话题能够引发大家的围观，其实是一件非常难过的事情。毕竟对于这个圈子里面的明星来说，......
### [太尴尬了！金巧巧素颜在大排档吃饭，全程无人认出，一个签名都没有](https://new.qq.com/omn/20210713/20210713A0CXS200.html)
> 概要: 以前很多人都觉得明星特别不容易，因为这个团体从刚刚进入娱乐圈开始就备受关注，不管是聚光灯还是大众的讨论，始终都没有离开过跟自己相关的话题。虽然这样的事实可能会给明星带来一定的负担，但是如果身为公众人物......
### [专利表明，苹果有望在 2022 年为 iPhone 带来潜望式镜头](https://www.ithome.com/0/562/645.htm)
> 概要: IT之家7 月 13 日消息 韩国业界去年这时有消息称，苹果当时正在为其iPhone找寻潜望式相机技术和专利合作伙伴，但后来不了了之。外媒 Patently Apple 发现，苹果现在已经获得了一项此......
### [火币全球站已暂停BAND和ZIL充提业务](https://www.btc126.com//view/176776.html)
> 概要: 官方消息，由于BAND和ZIL主网升级，火币全球站已暂停BAND（Band Protocol）和ZIL（Zilliqa）的充提业务......
### [比特币Taproot已在测试网上成功激活](https://www.btc126.com//view/176775.html)
> 概要: 据cryptonews消息，包括 Athony Towns 在内的比特币开发者证实，Taproot已在测试网上成功激活。此前消息，比特币已锁定Taproot升级，预计于11月在主网激活......
### [长三角一体化示范区发布“期中”成绩单 18项制度创新成果逐个数](https://finance.sina.com.cn/china/gncj/2021-07-13/doc-ikqcfnca6672601.shtml)
> 概要: 原标题：长三角一体化示范区发布“期中”成绩单 18项制度创新成果逐个数 7月13日，长三角一体化示范区发布最新创新成果。今年以来，已经有18项一体化制度创新成果应用实施。
### [明日看点：巴拉圭议员将提交与比特币监管相关法案](https://www.btc126.com//view/176779.html)
> 概要: 1.巴拉圭议员将于7月14日提交与比特币监管相关法案。2.欧洲央行将于7月14日讨论数字欧元。3.Bitfinex将于7 月 14 日20:00上线FCL。4.烟花艺术家蔡国强首个NFT作品将于7月1......
### [央行：降准是货币政策回归常态后常规流动性操作 PPI有望在今年四季度和明年趋于回落](https://finance.sina.com.cn/roll/2021-07-13/doc-ikqcfnca6672617.shtml)
> 概要: 原标题：央行：降准是货币政策回归常态后常规流动性操作 PPI有望在今年四季度和明年趋于回落 华夏时报记者刘佳北京报道 7月13日，国新办举行上半年金融统计数据情况新闻发...
### [上半年中国纺织品服装出口保持稳健增长](https://finance.sina.com.cn/roll/2021-07-13/doc-ikqciyzk5277458.shtml)
> 概要: 中新社北京7月13日电 （记者 闫晓虹）中国纺织品进出口商会13日披露，上半年中国纺织品服装出口保持稳健增长，按人民币和美元计分别比去年同期增长约3%和12%...
### [连平：目前准备金率依旧偏高，仍有下降空间](https://finance.sina.com.cn/china/gncj/2021-07-13/doc-ikqciyzk5278754.shtml)
> 概要: 原标题：连平：目前准备金率依旧偏高，仍有下降空间 7月9日央行在官网发布消息称，决定于2021年7月15日下调金融机构存款准备金率0...
### [央行最新发声！货币政策已回归常态，全面降准是常规流动性操作，这样回应中美货币政策差异](https://finance.sina.com.cn/roll/2021-07-13/doc-ikqciyzk5279019.shtml)
> 概要: 原标题：央行最新发声！货币政策已回归常态，全面降准是常规流动性操作，这样回应中美货币政策差异 对于本周四即将正式落地的全面降准，央行最新表态称...
### [撸区说英雄联盟区辩论赛第二战!](https://bbs.hupu.com/44198268.html)
> 概要: 【本期辩题】：衡量选手的第一要素是荣誉还是实力？英雄联盟作为一个5个人的团队游戏，有人认为荣誉应该并不能完全反映出选手的个人实力。但是很多人在对比选手实力时，荣誉似乎往往也被作为参考因素。有人说荣誉对
### [监管下的 1 对 1 教育命途](https://www.tuicool.com/articles/URFFzu3)
> 概要: 2000 年至 2010 年间，是 1 对 1 教育野蛮生长的十年，一批专注个性化教育的 1 对 1 辅导机构集体爆发。但因利润过低、营销氛围重、教师地位边缘化等问题，十年间，1 对 1 教育并未有本......
### [《失孤》父子重逢，井柏然为何感同身受，童年经历让人心疼](https://new.qq.com/omn/20210713/20210713A0DTWO00.html)
> 概要: 文/游泳圈儿，纳兰泽自媒体编辑部毫无疑问，电影《失孤》虽然没能达到《我不是药神》那样轰动全国，甚至和政策产生呼应作用，但也因为“打拐”题材在一定程度上引起了热议。            而就在最近，电......
### [IntoTheBlock：BTC每日费用平均低于 1 美元，创 2020 年 12 月以来的新低](https://www.btc126.com//view/176784.html)
> 概要: IntoTheBlock发推表示，自 7 月 8 日以来，BTC每日平均费用已低于 1美元，创 2020 年 12 月以来的新低......
### [流言板独行侠、马刺、热火和森林狼对约翰-科林斯感兴趣](https://bbs.hupu.com/44198528.html)
> 概要: 虎扑07月13日讯 根据The Athletic记者Shams Charania的报道，消息人士透露，联盟中很多人认为老鹰球员约翰-科林斯在帮助球队打进东部决赛的过程中提升了自己作为顶薪水准球员的身价
### [《食人鲨》DLC“真相任务”8月31日登陆各大平台](https://www.3dmgame.com/news/202107/3818895.html)
> 概要: 游戏发行商和开发商Tripwire Interactive宣布，鲨鱼扮演类游戏《食人鲨》的DLC“真相任务”（TruthQuest）将于8月31日在PlayStation 5、Xbox Series、......
### [教育部辟谣：“暑期托管变成第三学期”不实，不会取消教师寒暑假](https://finance.sina.com.cn/wm/2021-07-13/doc-ikqciyzk5281320.shtml)
> 概要: 教育部7月13日召开新闻通气会。会上指出，“要取消教师寒暑假”的说法没有依据，关于“暑期托管变成第三学期”的说法是不符合实际的。
### [《荒野大镖客2》获DLSS升级 在线模式加入新内容](https://www.3dmgame.com/news/202107/3818896.html)
> 概要: 《荒野大镖局2》的PC玩家终于可以享受到全新DLSS支持了，这是今天更新补丁的一部分。Rockstar Games之前就曾承诺将在7月13日为游戏添加NvidiaDLSS支持。该补丁已于今日面向PC及......
### [流言板阿森西奥：东京奥运会，西班牙男足的目标就是金牌](https://bbs.hupu.com/44198697.html)
> 概要: 虎扑07月13日讯 西班牙前锋阿森西奥接受了《阿斯报》的专访首先，你的身体状况现在如何？因为膝盖受伤之后，一直说你距离最佳状态还差一些，现在呢？我之前经过了充分的休息，尤其是在经过一个漫长的赛季之后，
### [转发国足打进世界杯突围路线图，JRs看完觉得合理不？](https://bbs.hupu.com/44198776.html)
> 概要: 国足打进世界杯突围路线图转发自头条，JRs看完觉得合理不？
# 小说
### [世界大教皇](http://book.zongheng.com/book/725013.html)
> 作者：工匠

> 标签：科幻游戏

> 简介：我亲手写的外挂成了神！谁还比本大爷屌！

> 章节末：247就是这么任性

> 状态：完本
# 论文
### [Multiple Sound Sources Localization from Coarse to Fine](https://paperswithcode.com/paper/multiple-sound-sources-localization-from)
> 日期：13 Jul 2020

> 标签：None

> 代码：https://github.com/shvdiwnkozbw/Multi-Source-Sound-Localization

> 描述：How to visually localize multiple sound sources in unconstrained videos is a formidable problem, especially when lack of the pairwise sound-object annotations. To solve this problem, we develop a two-stage audiovisual learning framework that disentangles audio and visual representations of different categories from complex scenes, then performs cross-modal feature alignment in a coarse-to-fine manner.
### [Joint Noise-Tolerant Learning and Meta Camera Shift Adaptation for Unsupervised Person Re-Identification](https://paperswithcode.com/paper/joint-noise-tolerant-learning-and-meta-camera)
> 日期：8 Mar 2021

> 标签：META-LEARNING

> 代码：https://github.com/FlyingRoastDuck/MetaCam_DSCE

> 描述：This paper considers the problem of unsupervised person re-identification (re-ID), which aims to learn discriminative models with unlabeled data. One popular method is to obtain pseudo-label by clustering and use them to optimize the model.
