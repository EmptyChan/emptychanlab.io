---
title: 2020-08-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.OysterFarm_EN-CN8023890103_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-08-05 22:54:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.OysterFarm_EN-CN8023890103_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：韩男团AB6IX等亮相 李大辉粉底选错似画脸谱](http://slide.ent.sina.com.cn/y/k/slide_4_704_343021.html)
> 概要: 组图：韩男团AB6IX等亮相 李大辉粉底选错似画脸谱
### [组图：韩女团SECRET NUMBER出席音乐会 成员穿荧光粉亮相](http://slide.ent.sina.com.cn/y/k/slide_4_704_343025.html)
> 概要: 组图：韩女团SECRET NUMBER出席音乐会 成员穿荧光粉亮相
### [组图：李冰冰穿“夏奇羊”T恤别致可爱 搭紧身裤走清爽运动风](http://slide.ent.sina.com.cn/star/slide_4_704_343035.html)
> 概要: 组图：李冰冰穿“夏奇羊”T恤别致可爱 搭紧身裤走清爽运动风
### [组图：许飞穿白色短袖配暗纹长裤休闲亮相 大步流星潇洒随性](http://slide.ent.sina.com.cn/y/slide_4_704_343042.html)
> 概要: 组图：许飞穿白色短袖配暗纹长裤休闲亮相 大步流星潇洒随性
### [组图：Baby泳池边拍性感美照 穿红色波点连衣裙撩秀发秀白皙身材](http://slide.ent.sina.com.cn/star/slide_4_704_343051.html)
> 概要: 组图：Baby泳池边拍性感美照 穿红色波点连衣裙撩秀发秀白皙身材
# 动漫
### [罗川真里茂漫画《纯白之音》TV动画化决定](https://news.dmzj.com/article/68181.html)
> 概要: 罗川真里茂创造的漫画《纯白之音》宣布了TV动画化决定的消息，一段宣传视频也一并公开。
### [动画《请问您今天要来点兔子吗？》小游戏与OP试听](https://news.dmzj.com/article/68185.html)
> 概要: 即将于10月开始播出的TV动画《请问您今天要来点兔子吗？BLOOM》公开了OP试听视频，本作的OP为《天空自助餐厅》。作品的OP和ED《友好○友好》都将于10月28日发售。
### [P站美图推荐——包臀裙特辑](https://news.dmzj.com/article/68187.html)
> 概要: 强调臀部线条的成熟短裙，似乎相当考验穿着者的身形？
### [制作《妖精的旋律》《一骑当千》等动画的Arms开始特别清算](https://news.dmzj.com/article/68184.html)
> 概要: 动画制作公司Common Sense在7月22日接受了东京地方法院的特别清算决定，开始了特别清算。
### [斗罗票选力压魁拔，本周确认更新两集，杀戮之都结束，唐家小妹上线](https://new.qq.com/omn/20200805/20200805A0NYO400.html)
> 概要: 随着90后的长大，喜欢看动漫的人越来越多，动漫也迎来了最好的发展时机，近几年来越来越多的优质动漫开始涌现。最近腾讯动漫开启了人气动漫的票选，一共有22部优质动漫作品参加了此次投票，不过竞争最激烈的还……
### [爆笑漫画：筋斗云变七彩祥云，哪吒却一脸无辜？](https://new.qq.com/omn/20200805/20200805A0OZVL00.html)
> 概要: 爆笑漫画：筋斗云变七彩祥云，哪吒却一脸无辜？爆笑漫画：筋斗云变七彩祥云，哪吒却一脸无辜？
### [搞笑漫画：见一衣品遭展哥哥嫌弃，但展哥哥新发型竟帅哭隔壁小姐姐！](https://new.qq.com/omn/20200805/20200805A0O4JV00.html)
> 概要: 搞笑漫画：见一衣品遭展哥哥嫌弃，但展哥哥新发型竟帅哭隔壁小姐姐 ！
### [爆笑校园：呆爸和呆头一起“挤奶牛”太辛苦了，于是用挤奶机报复一下牛](https://new.qq.com/omn/20200805/20200805A0NWFG00.html)
> 概要: 爆笑校园：呆爸和呆头一起“挤奶牛”太辛苦了，于是用挤奶机报复一下牛
### [最走心的龙珠COS：牛魔王呆萌，比鲁斯很逼真，最后一位美哭了！](https://new.qq.com/omn/20200805/20200805A0N2UY00.html)
> 概要: 最走心的龙珠COS：牛魔王呆萌，比鲁斯很逼真，最后一位美哭了！要说格斗系动漫，《龙珠》一定算得上是人气最高的，片中的贝吉塔等人一定都是高人气人物，因此在COS圈中，《龙珠》也算得上是人气最高的动漫之……
### [“P站画师”-﹙TID﹚最新插画作品合集](https://new.qq.com/omn/20200805/20200805A0PO3F00.html)
> 概要: 画师资料画师：TIDPixiv ID：418969插画预览预览图经过压缩处理（9P）更多高清原图（128P）来自：E次元二次元资源交流社区
### [艺画开天深耕原创动画 助力动漫行业良性发展](https://acg.gamersky.com/news/202008/1310253.shtml)
> 概要: 艺画开天坚持做原创，旗下末日幻想题材动漫《灵笼》下半季在7月31日晚8点B站开播，上线4小时播放量破百万，占领B站国创榜第一，并登上了微博热搜。
### [日本18米可移动高达完成“上头仪式” 将于年内开放](https://acg.gamersky.com/news/202008/1310418.shtml)
> 概要: 日本横滨1:1 RX-78-2高达在近日完成了整体造型的“上头仪式”，高达的头部终于安装成功。预计将于2020年10月在横滨山下埠头对公众开放。
# 财经
### [深圳用地扩权 土地紧缺城市破局](https://finance.sina.com.cn/china/dfjj/2020-08-05/doc-iivhuipn7042387.shtml)
> 概要: 时值深圳经济特区建立40周年和先行示范区建设一周年，深圳喜获土地政策“大礼包”。 近日，自然资源部印发《关于支持粤港澳大湾区和深圳市深化自然资源领域改革探索意见的函...
### [7月一财首席调研：“六保”“六稳”基调不变 三季度经济稳步复苏](https://finance.sina.com.cn/china/gncj/2020-08-05/doc-iivhvpwx9411898.shtml)
> 概要: 原标题：7月一财首席调研：“六保”“六稳”基调不变，三季度经济稳步复苏 2020年8月“第一财经首席经济学家信心指数”为52.85，连续五个月反弹并创两年来新高...
### [一财社论：有的放矢解决深圳持续发展“痛点”](https://finance.sina.com.cn/roll/2020-08-05/doc-iivhuipn7036091.shtml)
> 概要: 原标题：社论：有的放矢解决深圳持续发展“痛点” 社会主义先行示范区1周年、经济特区成立40周年，这个8月对深圳而言意义重大，同时市场也在期待更多有力度的政策...
### [桂林实施失业保险稳岗计划：相关人员可申领每月35元补助金](https://finance.sina.com.cn/china/2020-08-05/doc-iivhuipn7037314.shtml)
> 概要: 桂林实施失业保险稳岗计划：相关人员可申领每月35元补助金 近日，广西桂林在落实扩大失业保险保障范围和实施失业保险稳岗计划工作中，针对符合条件的失业人员...
### [贵州出手打击囤积、加价、炒作等行为 整肃茅台酒市场！](https://finance.sina.com.cn/china/2020-08-05/doc-iivhvpwx9415101.shtml)
> 概要: 整肃茅台酒市场！贵州出手，打击囤积、加价、炒作等行为 8月5日，贵州省市场监督管理局发布《关于征集茅台酒市场领域违法违规线索的公告》，进一步加强市场监督...
### [重磅：四大行正在大规模内测数字货币App 可凭手机号完成转账](https://finance.sina.com.cn/china/2020-08-05/doc-iivhvpwx9415818.shtml)
> 概要: 重磅！四大行正在大规模内测数字货币App，可凭手机号完成转账 来源：21世纪经济报道 导读：数字货币由央行牵头进行，各家银行此前数月正在就落地场景等进行测试。
# 科技
### [数据库实践丨MySQL多表join分析](https://segmentfault.com/a/1190000023504670)
> 概要: 摘要：在数据库查询中;往往会需要查询多个表的数据;比如查询会员信息同时查询关于这个会员的订单信息;如果分语句查询的话;效率会很低;就需要用到join关键字来连表查询了。Join并行Join并行1. 多......
### [面经手册 · 第2篇《数据结构，HashCode为什么使用31作为乘数？》](https://segmentfault.com/a/1190000023501953)
> 概要: 作者：小傅哥博客：https://bugstack.cn沉淀、分享、成长，让自己和他人都能有所收获！😄一、前言在面经手册的前两篇介绍了《面试官都问我啥》()和《认知自己的技术栈盲区》()，这两篇内容主......
### [山西特产，达其爽旗舰店生榨沙棘汁 280mL×12 瓶 21.9 元](https://lapin.ithome.com/html/digi/501737.htm)
> 概要: 山西特产，达其爽旗舰店生榨沙棘汁280mL×12瓶报价41.9元，限时限量20元券，实付21.9元包邮，领券并购买。三红旗舰店铺。此外可选1.25L×3大瓶，券后仅需29.9元。精选当季原生态种植的优......
### [曝佳能即将推出旗舰 APS-C 画幅相机 EOS M7，3250 万像素](https://www.ithome.com/0/501/754.htm)
> 概要: IT之家 8 月 5 日消息 根据外媒 Canon Watch 的消息，佳能将于今年第四季度发布 EOS M7 和 EOS M50 Mark II 两款 APS-C 画幅相机。IT之家了解到，爆料称佳......
### [Gitlab Support is no longer processing MFA resets for free users](https://about.gitlab.com/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/)
> 概要: Gitlab Support is no longer processing MFA resets for free users
### [The Fundamental Axiom of Floating Point Arithmetic](http://www.johnbcoughlin.com/posts/floating-point-axiom/)
> 概要: The Fundamental Axiom of Floating Point Arithmetic
### [为什么你总觉得自己是对的？](https://www.huxiu.com/article/373630.html)
> 概要: 本文来自微信公众号：MacTalk（ID：MacTalkPro），作者：池建强大多数人都会觉得自己是对的，自己的观点是对的，想法是对的，判断是对的……这好像没什么不妥，不觉得自己是对的，难道还要天天否......
### [八年缠斗、百亿索赔，苹果在中国栽了？](https://www.huxiu.com/article/373653.html)
> 概要: 虎嗅机动资讯组作品作者 | 竺晶莹题图 | 视觉中国历经八年专利纷争，小i机器人重启对苹果的侵权诉讼，要求苹果停止侵权并赔偿100亿元。8月3日，小i机器人（上海智臻智能网络科技股份有限公司）正式向上......
### [Springboard raises $31 million to expand its mentor-guided education platform to more geogr...](https://www.tuicool.com/articles/vUjEfa6)
> 概要: an online education platform that providesupskilling and reskilling training coursesto people lookin......
### [阿里助推小鹏上市，美团力捧理想：巨头为何争抢“上车”？](https://www.tuicool.com/articles/fM7NRbv)
> 概要: 来源 | 连线Insight作者 | 刘喵喵继蔚来、理想之后，小鹏汽车成为新能源汽车下一位IPO的“准选手”。就在6天前，理想汽车刚刚顺利登陆纳斯达克，首个交易日收盘大涨43%。IPO认购过程也异常火......
# 小说
### [楚臣](http://book.zongheng.com/book/747843.html)
> 作者：更俗

> 标签：历史军事

> 简介：唐季既没，诸侯崛起，天佑帝起于草莽之间，于江淮地区创立楚国已经十二年，与占据中原的梁国以及占据河东、幽燕地区的晋国，成为当世最为强大的三大霸主，天下征战不休、民不聊生……【楚臣书迷群，ＱＱ群号：628735469，微信公众号：gengsu1979】

> 章节末：新书《非洲酋长》已发布

> 状态：完本
# 游戏
### [魂类游戏《致命躯壳》发售日公布 Steam版明年发售](https://www.3dmgame.com/news/202008/3794575.html)
> 概要: 发行商Playstack今日公布了动作RPG游戏《致命躯壳》的发售日。本作将于8月18日发售，登陆PC（Epic）、PS4和Xbox One，Epic国区售价17.99美元，支持简体中文，Steam版......
### [《赛博朋克2077》近战很有趣 几发子弹就足以杀敌](https://www.3dmgame.com/news/202008/3794571.html)
> 概要: 几个小时前，一个新的《赛博朋克2077》上手前瞻被发布出来，揭露了一些关于本作近战攻击和其他方面的细节。根据YouTube频道Easy Allies的作者Ben Moore，《赛博朋克2077》的近战......
### [《仁王2》DLC牛若战记IGN 8分：老玩家的回归良机](https://www.3dmgame.com/news/202008/3794516.html)
> 概要: 《仁王2》的首个 DLC“牛若战记”已于7月30日正式上线推出，这款 DLC 单独售价78港元(约合人民币71元)，追加了新剧情、新武器防具、新人物与敌人、新技能等。IGN 近日对这款DLC进行了评测......
### [《装机模拟器》Steam新史低特惠 仅售35元](https://www.3dmgame.com/news/202008/3794538.html)
> 概要: 今日(8月5日)《装机模拟器》在Steam上开启特惠促销，游戏原价70元，现在仅售35元，新史低价。游戏支持中文，在Steam上总评为“特别好评”，93%为好评。此次活动截止至8月8日结束，有兴趣的玩......
### [《十字军之王3》游戏演示介绍：帝国的建立与发展](https://www.3dmgame.com/news/202008/3794539.html)
> 概要: P社官方公开了《十字军之王3》的新一期介绍视频，本次向玩家展现了如何在游戏中建立并发展自己的帝国，讲解了婚姻与盟约、声望、遗产等相关内容。介绍视频：本期视频要点：· 在游戏中的时代，婚姻的结成大都不是......
# 论文
### [Insights from the Future for Continual Learning](https://paperswithcode.com/paper/insights-from-the-future-for-continual)
> 日期：24 Jun 2020

> 标签：CONTINUAL LEARNING

> 代码：https://github.com/arthurdouillard/incremental_learning.pytorch

> 描述：Continual learning aims to learn tasks sequentially, with (often severe) constraints on the storage of old learning samples, without suffering from catastrophic forgetting. In this work, we propose prescient continual learning, a novel experimental setting, to incorporate existing information about the classes, prior to any training data.
