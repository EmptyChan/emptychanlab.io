---
title: 2021-07-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Ortygia_ZH-CN5237934114_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-07-09 23:12:17
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Ortygia_ZH-CN5237934114_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [【漏洞细节公开】Windows Defender 远程代码执行漏洞通告](https://www.oschina.net/news/149726)
> 概要: 开源软件供应链点亮计划，等你来！>>>报告编号：B6-2021-070801报告来源：360CERT报告作者：360CERT更新日期：2021-07-08更新简介1. Google Project Z......
### [坚定开源战略，中兴成为全球开源技术峰会 GOTC 黄金赞助商](https://www.oschina.net/news/149704)
> 概要: 开源软件供应链点亮计划，等你来！>>>2021 年 3 月，在新华社受权全文播发的《中华人民共和国国民经济和社会发展第十四个五年规划和 2035 年远景目标纲要》中，“开源”被首次提及。从纲要提到的“......
### [Kotlin 启用新 Logo](https://www.oschina.net/news/149698/kotlin-new-logo)
> 概要: 开源软件供应链点亮计划，等你来！>>>Kotlin 官方博客公布了启用新 Logo 的消息。公告写道，虽然 Kotlin 已诞生许久，但在视觉风格方面却一直没有体现一致性。团队希望为 Kotlin 创......
### [CNCF 接纳 Piraeus 数据存储项目为沙盒项目](https://www.oschina.net/news/149716)
> 概要: 开源软件供应链点亮计划，等你来！>>>LINBIT, DaoCloud 和 SPDB 于今日宣布，Piraeus 数据存储项目已被 CNCF 的 Sandbox 接纳。01、Piraeus 的「创始方......
### [Vue3 + TypeScript 开发实践总结](https://segmentfault.com/a/1190000040319089?utm_source=sf-homepage)
> 概要: 前言迟来的Vue3文章,其实早在今年3月份时就把Vue3过了一遍。<br/>在去年年末又把TypeScript重新学了一遍，为了上Vue3的车，更好的开车。<br/>在上家公司4月份时，上级领导分配了......
### [游戏《传颂之物斩2》公开新PV“奥修托尔的剑”](https://news.dmzj.com/article/71481.html)
> 概要: 游戏《传颂之物斩2》（PS4/PS5）公开了新的宣传视频。在这次的视频中，公开了一段有关哈克在继承了奥修托尔的身份后，为了在行动上不出现破绽，和猫音一起练习用剑的故事。
### [动画工作室GONZO启动新企划SAMURAI cryptos！](https://news.dmzj.com/article/71482.html)
> 概要: 动画工作室GONZO宣布了新企划SAMURAI cryptos启动的消息。这次的企划以代表暗号和0的Cypher，代表分散的Decentral，和代表团结的Solidarity三个词为中心展开。
### [腾讯越来越不懂游戏了](https://www.huxiu.com/article/439537.html)
> 概要: 出品｜虎嗅机动资讯组作者｜黄青春题图｜视觉中国7月5日，腾讯再一次因为投资“财技”冲上微博热搜——在#腾讯平均7天投资一家游戏公司#话题下，人们津津乐道这家昔日游戏霸主试图通过买买买缓解“焦虑”的可行......
### [实例解析：如何开发 VSCode LSP 服务](https://segmentfault.com/a/1190000040320852?utm_source=sf-homepage)
> 概要: 全文 3000 字，欢迎点赞关注转发从一张动图说起：上图应该大家经常使用的错误诊断功能，它能够在你编写代码的过程中提示，那一块代码存在什么类型的问题。这个看似高大上的功能，从插件开发者的角度看其实特别......
### [P站美图推荐——精灵耳特辑](https://news.dmzj.com/article/71485.html)
> 概要: 金发、碧眼、精灵耳，好，好涩哦
### [《魔法科高校的劣等生》公开10周年纪念PV](https://news.dmzj.com/article/71491.html)
> 概要: 由佐岛勤创作的轻小说《魔法科高校的劣等生》公开了10周年纪念PV。在这次的PV中，伴随着ASCA的主题曲《命之证》，包括达也在内的主人公们都陆续登场。
### [クール教信者「小林家的龙女仆S」动画2期开播感谢绘公开](http://acg.178.com/202107/419795782638.html)
> 概要: 近日，「小林家的龙女仆S」原作者クール教信者公开了由其绘制的动画二期开播感谢绘。「小林家的龙女仆S」是动画「小林家的龙女仆」的第二季，由京都动画负责制作，已于2021年7月7日正式播出......
### [现总经理离职休假 育碧Massive娱乐任命新总经理](https://www.3dmgame.com/news/202107/3818588.html)
> 概要: 育碧Massive娱乐任命Thomas Andrén为新总经理，代替之前的David Polfeldt。Thomas Andrén将在今年10月正式上任，他将取代育碧资深开发者David Polfel......
### [《COD17》第四赛季预告片发布 丧尸模式新地图](https://www.3dmgame.com/news/202107/3818586.html)
> 概要: 今日（7月9日）《使命召唤》官方发布了《使命召唤17》第四赛季的最新预告片，其中预告片中主要描绘《使命召唤17》第四赛季季中丧尸模式新地图“亡者之壁（Mauer der Toten）”。宣传片：从预告......
### [观民俗、赏文化，快和《狐妖小红娘》启程一起畅游山西太原吧！](https://new.qq.com/omn/ACF20210/ACF2021070900441600.html)
> 概要: 动画《狐妖小红娘》跨界牵手山西省文化旅游厅、山西省文物局、太原市人民政府开展的文化探索之旅活动正式发车啦！这一路我们将在“山西文物探索官”涂山苏苏的带领下打卡历史文物，领略山西风土人情。第一站我们在大......
### [《考神当道》5年高考3年中二 从容应对考场中亿点小状况](https://acg.gamersky.com/news/202107/1404636.shtml)
> 概要: 在Netflix上播放的动画《攻壳机动队：SAC_2045》即将推出剧场版动画，确定将在2021年上映，官方还公开了剧场版动画的海报。
### [剧场版动画「龙与雀斑公主」公布30秒CM](http://acg.178.com/202107/419801924743.html)
> 概要: 由细田守执导的新作剧场版动画「龙与雀斑公主」公布了30秒CM「忘れられない夏になる篇」，该作将于7月16日在日本上映。「龙与雀斑公主」30秒CM「忘れられない夏になる篇」「龙与雀斑公主」是由细田守执导......
### [大厂的激励，年轻人们不买账了](https://www.huxiu.com/article/440068.html)
> 概要: 本文来自微信公众号：界面新闻（ID：wowjiemian），作者：佘晓晨，采访：佘晓晨、陆柯言、司林威，编辑：文姝琪，题图来自视觉中国腾讯宣布给特别贡献员工发100股股票的那天，李剑锋的反应颇为平淡......
### [「水果篮子」动画最新版权绘公开](http://acg.178.com/202107/419807708350.html)
> 概要: 电视动画「水果篮子」公开了最新版权绘。TV动画「水果篮子」改编自高屋奈月笔下的同名漫画，共有三季，现已全部播完。原创前传动画「今日子与胜也的故事」于2021年6月宣布制作决定......
### [谁还记得《快乐星球》的冰柠檬？退圈后当网红，官宣结婚超幸福](https://new.qq.com/omn/20210707/20210707A080MD00.html)
> 概要: 《快乐星球》是陪伴着许多90后长大的作品，随着时间的流逝当年参演《快乐星球》的童星们如今长大后也各自有着不同的机遇。看过《快乐星球》的人一定很熟悉当年剧中饰演“冰柠檬”的女孩子，那时候饰演“冰柠檬”的......
### [视频：王彦霖艾佳妮将办婚礼 陈立农或将任伴郎](https://video.sina.com.cn/p/ent/2021-07-09/detail-ikqciyzk4476080.d.html)
> 概要: 视频：王彦霖艾佳妮将办婚礼 陈立农或将任伴郎
### [MegaHouse「死神」井上织姬手办公开](http://acg.178.com/202107/419814688586.html)
> 概要: 近日，MegaHouse公开了「Gals」系列手办的最新企划，本次推出的角色为来自「死神」的井上织姬。本次设计采用了「破面篇」中的特殊装束，将她定格在威风吹拂披风的瞬间。该手办全高约210mm，售价1......
### [孙一宁之后，王思聪又被拍与美女扫货，两人同行十指紧扣超恩爱！](https://new.qq.com/omn/20210709/20210709A06CAE00.html)
> 概要: 孙一宁之后，王思聪又被拍与美女扫货，两人同行十指紧扣超恩爱！            提起王思聪，大家应该对他都很关注了吧，王思聪的父亲是商业大佬王健林，是比较有影响力的公众人物，因为父亲的光环，王思聪......
### [《刺客信条：王朝》中文单行本预售开启 7.12全网上市](https://acg.gamersky.com/news/202107/1404732.shtml)
> 概要: 动画电影《摇摆神探》由其卡通、祥源文化、亨通传媒、其飞祥出品，朱江执导（《快乐东西》联合导演）将于7月17日全国公映。
### [收！攻！略！CCG EXPO 2021终极盘点来了~~~](https://new.qq.com/omn/ACF20210/ACF2021070900665100.html)
> 概要: 逛展最实用最基本的工具没有之一一图在手，自信地走！                                                                           ......
### [万物起源任天堂？但这款经典小游戏却来自科乐美](https://acg.gamersky.com/news/202107/1404748.shtml)
> 概要: 《刺客信条：王朝》中文单行本预售已开启！顶级游戏IP与中国故事碰撞出的国漫精髓。
### [Protect student privacy: Ban Eproctoring](https://www.baneproctoring.com/#letter)
> 概要: Protect student privacy: Ban Eproctoring
### [数据表明，比特币电力消耗降至2020年11月水平](https://www.tuicool.com/articles/YziIBfI)
> 概要: 暴走时评：根据行业数据，比特币的年度总耗电量自5月中旬记录的历史峰值以来已经下降了近60%。翻译：Maya根据剑桥大学比特币电力消耗指数（CBECI）的数据，比特币的年度预计总耗电量已经暴跌了近60%......
### [组图：宋祖儿穿宽松卫衣下衣失踪 秀发慵懒随意挽起俏皮发射Wink](http://slide.ent.sina.com.cn/tv/slide_4_704_359102.html)
> 概要: 组图：宋祖儿穿宽松卫衣下衣失踪 秀发慵懒随意挽起俏皮发射Wink
### [《白蛇2：青蛇劫起》全新预告 妖冶宝青坊主回归](https://acg.gamersky.com/news/202107/1404758.shtml)
> 概要: 国产动画电影《白蛇2：青蛇劫起》公开了全新的“狐说”预告，妖冶的宝青坊主回归，这次她的身份更为神秘，还一语揭开了修罗城存在的真相。
### [“海王”吸金能力=茅台+宁德时代？](https://www.huxiu.com/article/439836.html)
> 概要: 出品 | 虎嗅机动资讯组作者 | 黄青春题图 | 视觉中国一纸业绩预告，彻底点燃了A股股民的热情。7月7日晚间，中远海控披露业绩预告：预计公司2021年上半年实现归属于上市公司股东的净利润约为370......
### [《切尔诺贝利人》新场景视频发布 3D扫描还原现实](https://www.3dmgame.com/news/202107/3818616.html)
> 概要: 昨日（7月8日）ALL iN！GAMES油管频道发布了一段生存恐怖游戏《切尔诺贝利人》新视频，该视频讲述的是取景地“翡翠夏令营（база отдыха Изумрудный）”现实与游戏的对比。对比视......
### [视频平台剧场化排播 “剧场”追剧的魅力在何处？](https://ent.sina.com.cn/v/m/2021-07-09/doc-ikqciyzk4505325.shtml)
> 概要: 原标题：“剧场”追剧的魅力究竟在何处　　视频平台“剧场化”的排播模式，意味着视频平台剧集的类型，在垂直分类上做得愈发细致、极致。　　---------------　　今天，你去“剧场”追剧了吗？　　一......
### [上海迪士尼乐园明年1月上调票价：最高涨至769元](https://www.3dmgame.com/news/202107/3818618.html)
> 概要: 今日(7月9日)上海迪士尼乐园发布公告称，上海迪士尼乐园的票务调整方案将于2022年1月9日起实行。届时，上海迪士尼度假区将在现行四级票价结构下调整上海迪士尼乐园的门票价格。各级定义及对应的票价如下：......
### [暴富的“嘻哈”与失落的音乐人](https://www.tuicool.com/articles/V7ZbMnR)
> 概要: 图片来源@视觉中国文 | 观娱象限，作者 | 琢介 ，编辑 | 缈秒在审美疲劳、负面消息轰炸之后，普普文化在资本市场的崛起与冷静似乎在告诉我们：嘻哈仍有很大的想象空间，尽管并非那么成熟。破发、“瘦身”......
### [杨玏王玉雯“手挽手”被拍，虽甜蜜幸福，但因相差十岁不被看好！](https://new.qq.com/omn/20210709/20210709A09PFA00.html)
> 概要: 近日，有媒体拍到了杨玏王玉雯出入同一小区的视频。其实这也不是第一次了，早前，就有网友晒出了杨玏带着一女子逛超市。但只有背影，并没有看清女孩的脸蛋，所以不确定是谁？            但大家还是把目......
### [相信我，带父母去看一场《中国医生》，你一定会收获很多！](https://new.qq.com/omn/20210709/20210709A0AKK900.html)
> 概要: 夏天一到，每次带父母出去玩都想直接躲进电影远，因为实在太太太热了。可惜的是，暑期档电影虽多，但大部分的目标受众都是小孩子和二三十岁的年轻人，适合带父母去看的真的太少了。不过，             ......
### [晚内参：马斯克称狗狗币比BTC和ETH交易成本低](https://www.btc126.com//view/176210.html)
> 概要: 1.马斯克：Doge比BTC和ETH的交易成本低。2.英国监管机构称将打击有误导性的加密货币广告。3.印度ICICI银行要求客户汇往海外资金不能用于投资加密货币。4.Uniswap在以太坊二层扩容方案......
### [GraalVM at Facebook](https://medium.com/graalvm/graalvm-at-facebook-af09338ac519)
> 概要: GraalVM at Facebook
### [NBA传奇球星奥尼尔对加密货币持怀疑态度](https://www.btc126.com//view/176215.html)
> 概要: 据U.Today消息，NBA传奇球星、名人堂成员沙奎尔·奥尼尔在接受一家体育出版物采访时分享了他对加密货币的个人看法。奥尼尔明确表示了他对加密货币的担忧。他说，他只能在自己坚信的领域投资。奥尼尔还补充......
### [Win11 预览版上手体验：生产力看微软 Surface Pro ，还是苹果 iPad Pro  ？](https://www.ithome.com/0/561/953.htm)
> 概要: 前段时间微软发布了全新的操作系统Windows 11，打脸了Windows 10作为最后一代操作系统的地位。其实在那之前，我们也使用泄露版系统给大家制作了一期评测，但没想到正要发布的时候，真正的 Wi......
### [欧拉电动汽车 2021 上半年累计销售 52547 辆，6 月单月销量破万](https://www.ithome.com/0/561/954.htm)
> 概要: IT之家7 月 9 日消息 今日欧拉汽车公布了销售报告。2021 年 1-6 月，欧拉电动汽车销售 52547 辆，同比增长 456.9%。6 月单月共销售 10791 辆，展现了良好的增长势头。今年......
### [2 亿货款压倒同程生活，员工称账上已没钱，有团长提前换平台](https://www.ithome.com/0/561/957.htm)
> 概要: 曾经的“老三团”（兴盛优选、十荟团、同程生活）之一，同程生活在更名为“蜜橙生活”的第二天“倒下”了。7 月 7 日晚间，苏州鲜橙科技有限公司（原品牌名“同程生活”）发布公告称，因几年来经营不善，决定申......
### [German region to fine Tesla for illegal construction](https://www.reuters.com/business/retail-consumer/german-region-fine-tesla-illegal-construction-2021-07-08/)
> 概要: German region to fine Tesla for illegal construction
### [区块链防火墙提醒：警惕诈骗团伙恶意利用未经证实的虚假信息实行诈骗](https://www.btc126.com//view/176218.html)
> 概要: 近日，区块链防火墙安全团队发现有某不法团伙在社区传播“某平台将停止业务运营”、“某平台即将清退用户”等未经证实的虚假信息，通过恶意制造社区恐慌，将用户引导至骗子搭建的虚假交易平台实施诈骗，截至目前，已......
### [USDT火币OTC价格回升至6.43元](https://www.btc126.com//view/176219.html)
> 概要: 行情显示，美元兑人民币现报6.4798，USDT在火币OTC价格回升至6.43元，较汇率溢价-0.768%......
### [组图：钟南山看《中国医生》首映 与主创合影比赞](http://slide.ent.sina.com.cn/film/slide_4_704_359111.html)
> 概要: 组图：钟南山看《中国医生》首映 与主创合影比赞
### [字节跳动终于双休了](https://www.huxiu.com/article/439294.html)
> 概要: 出品 | 虎嗅机动资讯组作者 | 黄青春题图 | 视觉中国字节跳动终于双休了。7月9日晚间，据新浪科技报道，字节跳动正式宣布 “公司将于2021年8月1日起取消隔周周日工作的安排，请大家做好相应调整，......
### [一文读懂：为什么要建设数字化空域系统？](https://www.tuicool.com/articles/7Fbaeiq)
> 概要: 本文来自微信公众号：中国工程院院刊（ID：CAE-Engineering），作者：朱永文，陈志杰，蒲钒，王家隆，本文选自中国工程院院刊《中国工程科学》2021年第3期，原文标题：《数字化空域系统——空......
### [四川省纪委原副书记、省原监察厅厅长徐波接受审查调查](https://finance.sina.com.cn/china/gncj/2021-07-09/doc-ikqcfnca5942850.shtml)
> 概要: 原标题：四川省纪委原副书记、省原监察厅厅长徐波接受审查调查 据四川省纪委监委网站7月9日消息，四川省纪委原副书记、省原监察厅厅长徐波涉嫌严重违纪违法，主动投案...
### [投资 15.1 万亿韩元，LG 新能源疯狂扩产锂电池](https://www.ithome.com/0/561/978.htm)
> 概要: 7 月 9 日消息 据 TheElec 报道，LG 新能源将在 2030 年之前在韩国投资 15.1 万亿韩元（约合 850 亿人民币），以开发下一代电池技术并巩固其电池产业的领先地位。其中，LG 能......
### [中国这波“组合拳”，漂亮](https://finance.sina.com.cn/review/hgds/2021-07-09/doc-ikqcfnca5946809.shtml)
> 概要: 中国人不惹事，但也不怕事。倘若有人执意挑事，那我们也只能奉陪。 最近，我们打出的一套“组合拳”，结结实实打到了一些“反华”急先锋的痛处。
### [进口冻品又检出阳性！海关总署对境外5家企业采取紧急预防性措施 上半年已叫停144家](https://finance.sina.com.cn/china/2021-07-09/doc-ikqciyzk4545559.shtml)
> 概要: 原标题：进口冻品又检出阳性！海关总署对境外5家企业采取紧急预防性措施，上半年已叫停144家 本报记者徐芸茜 北京报道 进口冷冻食品再次被检出新冠病毒核酸阳性。
### [外交部：鼓吹针对中国的所谓第二阶段溯源是政治操弄](https://finance.sina.com.cn/roll/2021-07-09/doc-ikqcfnca5943611.shtml)
> 概要: 原标题：外交部：鼓吹针对中国的所谓第二阶段溯源是政治操弄 新华社北京7月9日电（记者温馨、许可）外交部发言人汪文斌9日说，鼓吹针对中国的所谓第二阶段溯源...
### [Measuring Algorithmically Infused Societies](https://www.nature.com/articles/s41586-021-03666-1)
> 概要: Measuring Algorithmically Infused Societies
### [人社部发文：未来五年全面强化技能人才建设 今明两年大力发展技工教育](https://finance.sina.com.cn/roll/2021-07-09/doc-ikqciyzk4547801.shtml)
> 概要: 原标题：人社部发文：未来五年全面强化技能人才建设，今明两年大力发展技工教育 经济观察网 记者 李紫宸7月9日，人社部发布消息表示，人社部将制定“十四五”职业技能培训...
### [流言板英国交通部部长劝退意大利球迷：麻烦近日不要再前往伦敦](https://bbs.hupu.com/44133367.html)
> 概要: 虎扑07月09日讯 据队报和法新社联合消息，经英国有关部门和意大利内政部的商议，周日在温布利球场进行的欧洲杯决赛将允许1000名意大利球迷入场观赛。对此，英格兰交通部部长格兰特-夏普斯（Grant S
### [国家网信办通报滴滴企业版等25款App下架](https://finance.sina.com.cn/chanjing/cyxw/2021-07-09/doc-ikqciyzk4550056.shtml)
> 概要: 【#国家网信办通报滴滴企业版等25款App下架#】国家网信办9日发布通报称，根据举报，经检测核实，“滴滴企业版”等25款App存在严重违法违规收集使用个人信息问题。
### [流言板Foot Mercato：西汉姆接近租借+买断签下巴黎门将阿雷奥拉](https://bbs.hupu.com/44133545.html)
> 概要: 虎扑07月09日讯 根据法国媒体Foot Mercato的报道，英超球会西汉姆联有信心与巴黎圣日耳曼达成协议，从而签下法国门将阿方斯-阿雷奥拉。报道称，阿雷奥拉今夏的最重要的想法就是离开巴黎，而他最希
### [集锦亚冠：破门乏术，杰志闷平大阪樱花晋级希望渺茫](https://bbs.hupu.com/44133540.html)
> 概要: 集锦亚冠：破门乏术，杰志闷平大阪樱花晋级希望渺茫
### [东方同人视觉小说《秘封碎片》7月17日登陆Steam](https://www.3dmgame.com/news/202107/3818644.html)
> 概要: 发行商phoenix和开发商ShikakuGames宣布，东方主题同人视觉小说《秘封碎片》（Hifuu Fragment）将于7月17日通过Steam登陆PC平台，售价1000日元。游戏支持日语和简体......
### [流言板曼联官方：2021/22赛季俱乐部超过5万张季票已经售罄](https://bbs.hupu.com/44133823.html)
> 概要: 虎扑07月09日讯 曼联官方宣布，2021/22赛季的所有季票已经售罄。英国政府计划于7月19日开始取消体育运动场所的人数限制，曼联可以在新赛季期待着超过5万名季票持有者回归老塔拉福德球场。曼联俱乐部
### [《革命者》李大钊台词说哭主创 源于张颂文灵感](https://ent.sina.com.cn/m/c/2021-07-09/doc-ikqcfnca5949702.shtml)
> 概要: 由管虎监制，徐展雄执导，张颂文、李易峰、佟丽娅领衔主演的电影《革命者》深受好评，票房已经破亿元。影片描绘了1912年至1927年间，李大钊引领革命志士为救国救民积极奔走、奋起反抗的壮阔革命史诗，以生动......
### [IEnumerable.Count() 和 Length 到底有什么不同？](https://www.tuicool.com/articles/7jiaA3n)
> 概要: 咨询区balalakshmi：请问 Linq 的扩展方法Count()和Length有什么不同 ？回答区JaredPar：Linq 中的Count()一般指的是System.Linq.Enumerab......
# 小说
### [至尊战神](http://book.zongheng.com/book/246512.html)
> 作者：大雪崩

> 标签：奇幻玄幻

> 简介：苏羽堪破生死，沟通天地，却发现自己只不过刚刚踏入修炼的门槛，前方等着他的将是更恢宏的场景！万险千难，荆棘满途，无法阻止他找寻母亲的脚步。涅槃重生，成就天穹之位的他，是否能够成为天地寰宇的王者？拥有生成于天地之前的绝世宝物，苏羽踏上了一条精彩绝伦的战神之路！

> 章节末：第三百九十四章 天地重开（大结局）

> 状态：完本
# 论文
### [Detecting Singleton Spams in Reviews via Learning Deep Anomalous Temporal Aspect-Sentiment Patterns](https://paperswithcode.com/paper/detecting-singleton-spams-in-reviews-via)
> 日期：2 Jan 2021

> 标签：FEATURE ENGINEERING

> 代码：https://github.com/yassienshaalan/DTOPS

> 描述：Customer reviews are an essential source of information to consumers. Meanwhile, opinion spams spread widely and the detection of spam reviews becomes critically important for ensuring the integrity of the echo system of online reviews.
### [FoldIt: Haustral Folds Detection and Segmentation in Colonoscopy Videos](https://paperswithcode.com/paper/foldit-haustral-folds-detection-and)
> 日期：23 Jun 2021

> 标签：None

> 代码：https://github.com/nadeemlab/CEP

> 描述：Haustral folds are colon wall protrusions implicated for high polyp miss rate during optical colonoscopy procedures. If segmented accurately, haustral folds can allow for better estimation of missed surface and can also serve as valuable landmarks for registering pre-treatment virtual (CT) and optical colonoscopies, to guide navigation towards the anomalies found in pre-treatment scans.
