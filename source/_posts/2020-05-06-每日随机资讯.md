---
title: 2020-05-06-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SiegeofCusco_EN-CN9331087857_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-06 21:08:13
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SiegeofCusco_EN-CN9331087857_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [邓紫棋绿洲晒开工照 在亲自安装的桌前办公超骄傲](https://ent.sina.com.cn/y/ygangtai/2020-05-06/doc-iircuyvi1634561.shtml)
> 概要: 新浪娱乐讯 5月6日，邓紫棋在绿洲分享近照，并配文：“假期后第一天开工！ 第一次用自己第一次安装的办公桌！！！！” 照片中的邓紫棋一身休闲装扮，头戴黑色棒球帽，手上戴着一次性塑料手套，她坐在自己组装的......
### [组图：刘诗诗倪妮《流金岁月》路透曝光 一个温柔一个明艳气质绝](http://slide.ent.sina.com.cn/tv/slide_4_704_338171.html)
> 概要: 组图：刘诗诗倪妮《流金岁月》路透曝光 一个温柔一个明艳气质绝
### [视频：海莉谈结婚前短暂分手 没找替补 得知比伯不再渣后才复合](https://video.sina.com.cn/p/ent/2020-05-06/detail-iirczymk0139666.d.html)
> 概要: 视频：海莉谈结婚前短暂分手 没找替补 得知比伯不再渣后才复合
### [视频：易烊千玺喜获第39届金像奖最佳新演员](https://video.sina.com.cn/p/ent/2020-05-06/detail-iirczymk0135464.d.html)
> 概要: 视频：易烊千玺喜获第39届金像奖最佳新演员
### [刘敏涛回应唱歌太投入忘记表情管理：我真的没醉](https://ent.sina.com.cn/music/zy/2020-05-06/doc-iircuyvi1542056.shtml)
> 概要: 新浪娱乐讯 5月5日，刘敏涛在某卫视盛典上演唱《红色高跟鞋》，有网友发现演唱过程中刘敏涛因唱歌太投入忘记表情管理，神情逗趣，网友纷纷调侃她像是“喝醉了”，引发热议，随后登上热搜。5月6日，该盛典官博分......
### [周冬雨夺得影后的3大原因：哭戏是大杀器，还敢为角色豁出去](https://new.qq.com/omn/20200506/20200506A0MHZP00.html)
> 概要: 5月6日下午3时，第39届金像奖获奖名单新鲜出炉，《少年的你》以8项大奖成最大赢家。除了最佳影片、最佳导演、最佳编剧等奖项之外，得到双提名的易烊千玺还获得了最佳新演员奖。易烊千玺创造了历史，成为内地男......
### [放下偏见看《创造营》，国内最强女团，恐怕真的要诞生了](https://new.qq.com/omn/20200506/20200506A0M7TN00.html)
> 概要: 今年的女团选秀，以《青你2》为开端，竞争十分激烈，但是青你的剧本痕迹太重，所以遭到了很大的诟病，不过《创造营》的新赛制带来了很大的惊喜，这一次鹅厂明显是想选出来最强女团。把所有的噱头和话题都放弃了，反......
### [郑秀文早知输给周冬雨？第六次惜败金像影后，发文感叹“人生本难”](https://new.qq.com/omn/20200506/20200506A0NBUH00.html)
> 概要: 周冬雨，金像影后！第39届香港电影金像奖5月6日下午15:00揭晓，在“最佳女主角”环节，周冬雨凭《少年的你》击败了郑秀文、蔡思韵、邓丽欣三位对手，拿下了她人生中的第一个金像影后桂冠！并且成为“金马金......
### [何洁前夫赫子铭现身直播，身形消瘦显憔悴，与何洁幸福近况大不同](https://new.qq.com/omn/20200506/20200506A0JQ4O00.html)
> 概要: 最近这段时间，久未露面的何洁前夫赫子铭，一直忙于在某社交平台上直播。让人颇感意外的是，曾经那个身材健壮的型男赫子铭，如今好像完全变了一个样。从直播截图中，我们可以看出他身形消瘦，脸上的胡茬看起来略显沧......
### [张家辉14岁女儿与妈妈关咏荷逛街，全程不耐烦还甩开妈妈的手](https://new.qq.com/omn/20200506/20200506A03VBQ00.html)
> 概要: 近日《陀枪师姐2020》正在火热拍摄，距离1998年开播已经过去22年了，而有份参与拍摄的旧班底，只剩下滕丽名、朱咪咪及李成昌。但却有不少观众记得关咏荷饰演的娥姐一角，但她并未复出参与新续集拍摄，而是......
# 动漫
### [感染新冠病毒的声优山下麻美痊愈 解除在家隔离](https://news.dmzj.com/article/67294.html)
> 概要: 在之前曾为大家介绍过，声优山下麻美在4月9日被确认感染了新冠病毒的消息。根据其所属的青二事务所的最新消息，经过2次PCR检测为阴性，故在5月4日解除山下麻美的家隔离的命令。
### [争议缠身的《后浪》你是否是“后浪”中的一员？](https://news.dmzj.com/article/67296.html)
> 概要: 今年的五四，B站推出了一部视频《后浪》，然而这部视频却在社交媒体圈引发了激烈争论
### [网友制作刺身艺术 被赞称：感觉吃了都浪费](https://news.dmzj.com/article/67293.html)
> 概要: 近日，有名为Mikyovi00的网友在Instagram晒出了以“刺身艺术”为主题的不少作品。从图片中可以看出，Mikyovi00使用刺身再现了龙、天马以及一些人气作品角色。不少网友都表示看到这样的刺身，感觉吃了浪费。
### [《我们无法一起学习》作者绘制五等分与鬼灭的角色](https://news.dmzj.com/article/67290.html)
> 概要: 趁着日本的黄金周，创作了漫画《我们无法一起学习》的漫画家筒井大志在推特上晒出了练习绘制《鬼灭之刃》中的炭治郎和《五等分的花嫁》中的五胞胎时的画。
### [斗罗大陆：同样是美女，胡列娜碾压小舞，一个侧躺小舞就输了](https://new.qq.com/omn/20200506/20200506A0LHKR00.html)
> 概要: 动漫《斗罗大陆》是根据同名小说改编而成，由于剧情还原度很高，整体画面也不错，所以深受粉丝们的喜爱。小说《斗罗大陆》作为唐家三少的巅峰代表作，当初连载的时候拥有很高的人气，三少也凭借这部小说圈粉无数。……
### [fate：详解卫宫家的伙食费来源 一群后宫养一个家 老虎出力最多](https://new.qq.com/omn/20200506/20200506A0NR3H00.html)
> 概要: fate中最著名和熟知的场景，莫过于是卫宫家的饭了，为此官方还特别制作了相关的番剧来详细描写，不过一个值得思考的地方出现了，那就是卫宫家顿顿吃得如此丰盛和豪华，其伙食费怎么来的呢？fate系列中的正……
### [斗罗大陆真人版武魂曝光，白虎金刚变竟成喵喵拳，斗气化翼预定？](https://new.qq.com/omn/20200506/20200506A0MRH800.html)
> 概要: 大家好，北冥特摄漫评，带你看不一样的动漫资讯。动漫变成真人版一直都是原著党的噩梦，可以说优秀的作品非常的少，当然一旦成功也是会成为非常火爆的作品，就比如魔道祖师，不仅完美还原，也是成为国风的代表了。……
### [终极斗罗18：C8剧透了，唐舞麟谈到神界的回归，绝世美女蓝某登场](https://new.qq.com/omn/20200506/20200506A0LVRC00.html)
> 概要: 喜欢终极斗罗的朋友们，大家好，我是废马刀，大家是不是很想看终极斗罗18册呢？虽然18册还没有更新，不过已经有人可以看到该册的精彩内容了，其中一个就是C8曹德智。C8大家应该很熟悉了，很多靠谱的斗罗剧……
### [物是人非，大家还记得我们年少时，那本《知音漫客》吗？](https://new.qq.com/omn/20200506/20200506A0LLUY00.html)
> 概要: 导读：在那个没有智能手机的时代，男生的浪漫，莫过于课桌里的那本《知音漫客》。那本陪伴我走过全部青春的漫画，虽然已经没落，但它给我带来的欢乐，却永远地留在了大家的心底。现在想想，还是会不由得一笑，心中……
### [动漫界五大丢人魔王，作死抓公主的松冈祯丞即将成为新番中的快乐源泉！](https://new.qq.com/omn/20200506/20200506A0LA5S00.html)
> 概要: 魔王勇者一直以来都是动漫界屡见不鲜的一大题材，正所谓一山不容二虎，除非一公一母！然而不得不说近年来魔王真的是一届不如一届，有跑到异世界快餐店当领班的，也有跑到便利店当店员的，甚至还有变成萝莉去勇者学……
### [《恋与制作人》动画详情公开 共12集年内开播](https://acg.gamersky.com/news/202005/1285912.shtml)
> 概要: 《恋与制作人》的动画将分为四个部分，每部分为3集，标题分别为相遇、初绽、觉醒、羁绊。《恋与制作人》动画将由MAPPA制作，国内的申报单位为“上海叠阿尼动画有限公司”。
### [《鬼灭之刃》漫画最新情报 时间流逝太快](https://acg.gamersky.com/news/202005/1285740.shtml)
> 概要: 5月6日，《鬼灭之刃》漫画204话的图透公开了，一张图的信息量还是不小的，不知道在这一话中又会有什么精彩的展开。
# 财经
### [今日财经TOP10|国务院:允许小微企业等延缓缴纳所得税](https://finance.sina.com.cn/china/gncj/2020-05-06/doc-iirczymk0190880.shtml)
> 概要: 【宏观要闻】 NO.1国务院：允许小微企业和个体工商户延缓缴纳所得税 国务院常务会议今日召开，会议听取支持复工复产和助企纾困政策措施落实情况汇报...
### [专家组解读虎门大桥抖动：为设置水马而产生的桥梁涡振现象](https://finance.sina.com.cn/china/dfjj/2020-05-06/doc-iirczymk0185364.shtml)
> 概要: 原标题：专家组解读虎门大桥抖动原因：为设置水马而产生的桥梁涡振现象 5月5日下午14时许，广东虎门大桥悬索桥桥面发生明显振动，桥面振幅过大影响行车舒适性和交通安全。...
### [澳门将实施新冠肺炎常规核酸检测计划 首次检测免费](https://finance.sina.com.cn/china/dfjj/2020-05-06/doc-iirczymk0186688.shtml)
> 概要: 原标题：澳门将实施新冠肺炎常规核酸检测计划 首次检测免费 澳门新型冠状病毒感染应变协调中心6日下午表示，7日起将实施新冠肺炎常规核酸检测计划，以巩固疫情防控成效...
### [国常会：再提前发1万亿专项债新增限额 力争月底发完](https://finance.sina.com.cn/china/gncj/2020-05-06/doc-iirczymk0188783.shtml)
> 概要: 原标题：国常会：再提前发1万亿专项债新增限额，力争月底发完 新京报讯（记者 程维妙）随着国内新冠肺炎疫情形势的变化，抗疫政策仍在不断补充完善中。
### [海南海口市符合条件托管托餐机构可申请开放经营](https://finance.sina.com.cn/china/dfjj/2020-05-06/doc-iirczymk0180840.shtml)
> 概要: 原标题：海南海口市符合条件托管托餐机构可申请开放经营 海南省海口市相关部门日前发出通知，明确符合开放条件的托管托餐机构向属地指挥部提出复工开放申请...
### [复工复产加速：广东成今年首个用电负荷破亿省份](https://finance.sina.com.cn/china/gncj/2020-05-06/doc-iircuyvi1660355.shtml)
> 概要: 从南方电网公司获悉，受复工复产加速的驱动及高温天气的影响，5月6日10时04分，广东电网公司的电网统调负荷今年首次突破1亿千瓦，较去年首次破亿提前9天。
# 科技
### [AIMET是一个为经过训练的神经网络模型提供高级量化和压缩技术的库](https://www.ctolib.com/quic-aimet.html)
> 概要: AIMET是一个为经过训练的神经网络模型提供高级量化和压缩技术的库
### [一个简单的CLI应用程序，可在Markdown文件上做笔记](https://www.ctolib.com/prdpx7-notes-cli.html)
> 概要: 一个简单的CLI应用程序，可在Markdown文件上做笔记
### [DareBlopy：与框架无关的深度学习数据快速读取Python包](https://www.ctolib.com/podgorskiy-DareBlopy.html)
> 概要: DareBlopy：与框架无关的深度学习数据快速读取Python包
### [使用Python和R实现贝叶斯统计的介绍](https://www.ctolib.com/AllenDowney-BiteSizeBayes.html)
> 概要: 使用Python和R实现贝叶斯统计的介绍
### [以Kubernetes集群管理为例，大牛教你如何设计优秀项目架构](https://www.tuicool.com/articles/yANRFv)
> 概要: 前言架构设计一直是技术人的关注热点，如何设计一个更优的架构对于实际的业务来说至关重要。本文腾讯云专家将从自身从事的一个Kubernetes集群管理项目为例，重点剖析在项目开发过程中的三次架构演进历程，......
### [单点登录认证系统 MaxKey v 1.4.0GA](https://www.tuicool.com/articles/26JVzij)
> 概要: MaxKey介绍MaxKey(马克思的钥匙)，寓意是最大钥匙， 是用户单点登录认证系统（Sigle Sign On System）,支持OAuth 2.0/OpenID Connect、SAML 2......
### [7个技巧，教你做好下拉菜单设计](https://www.tuicool.com/articles/RBNbQrA)
> 概要: 本文将依据下拉菜单的属性，通过举例来讨论下拉菜单的使用场景，希望在设计上对大家有所帮助。正确地设计下拉菜单，可以帮助用户缩小选择范围，减少屏幕其他元素的干扰，以及防止用户输入错误的信息。但是，在某些情......
### [日赚300的抖音快手点赞员，原来是场兼职骗局](https://www.tuicool.com/articles/YzENF3R)
> 概要: 俗话说，人红是非多，对于抖音和快手来说，则是“人红骗局多”，不少人就打着招聘点赞员的幌子招摇撞骗，很多都是针对一些涉世未深的大学生。作者|小谦 来源|小谦笔记（ID:xiaoqianshuo）不知道在......
### [小米米家电动滑板车1S正式开售：30公里续航，1999元](https://www.ithome.com/0/485/837.htm)
> 概要: IT之家5月6日消息 小米米家电动滑板车1S今日正式开售，拥有30km续航、25km/ h最高时速，采用可视化交互仪表盘、双重刹车系统，售价1999元。米家电动滑板车1S支持节能模式(ECO)、正常模......
### [中国联通发布卫星互联网业务：沃星海、沃星陆、沃星空、沃星图](https://www.ithome.com/0/485/858.htm)
> 概要: IT之家5月6日消息 近日，国家发改委首次将卫星互联网列入“新基建”范畴。在全面加速5G“新基建”的同时，中国联通旗下联通航美在卫星互联网领域也有新动作。4月27日-30日，联通航美发布了沃星海、沃星......
### [采用全新引擎！《轩辕剑柒》首个场景实机演示视频公布](https://www.ithome.com/0/485/831.htm)
> 概要: IT之家5月6日消息 今日上午，《轩辕剑柒》官方公布了首个游戏场景展示视频，带来了采用全新引擎后的游戏内场景。神秘古旧的机关仍在运作，熟悉的鲁班船屋再现世间，随时间变化的自然与人文景致……西汉末年的中......
### [Firefox 火狐浏览器 76 正式版发布：画中画，增强密码保护](https://www.ithome.com/0/485/834.htm)
> 概要: Firefox 76 版本正式发布了，此版本一大亮点是增强了对在线帐户登录名和密码的保护：当网站暴露在风险中时，Firefox 会在 Lockwise 密码管理器中显示严重警报如果某个帐户涉及的某个网......
# 小说
### [一起走过那些年](http://book.zongheng.com/book/662534.html)
> 作者：苹果姐姐

> 标签：都市娱乐

> 简介：高中时三人结下了浓厚的友情，在宁泽涛高考落榜，心情跌落谷底时，他们帮助他，把他从人生悲观中拉了出来，为了坚持自己的梦想，他重返高中校园，最终功夫不付有心人，他成功实现了上海F大的梦想。之后在经历亲情，爱情的背叛打击后，友情让他再一次重新站了起来，三个男人一台戏，每个人身上都发生了或多或少不尽人意的事，但不管经历多少大风大浪，三个男人依旧相扶相持走到最后，走向成功。

> 章节末：第六十四章 回国（完结）

> 状态：完本
### [绝妃善类，冷王慢慢爱](http://book.zongheng.com/book/791581.html)
> 作者：陈半夏

> 标签：都市言情

> 简介：他是天宁国最得宠的王爷，杀伐果断，冷酷如冰山。她是曾被他弃为草芥恨不得杀之的王妃。他说她面善心毒，她说他冷酷如冰山。他无情，她无意。可直到后来，苏瑾皓才发现不可一世的他一颗心早就沦陷在她这里。等她要休夫时，他许诺，“弱水三千本王只取你这一瓢。”容绣：“美男三千独不爱你这款。”他，“……”她的王妃真火爆。

> 章节末：第227章 得天眷顾

> 状态：完本
# 游戏
### [只需关联账号 经典《邪恶天才》Steam版官方免费送！](https://www.3dmgame.com/news/202005/3787855.html)
> 概要: 经典游戏续作《邪恶天才2》将于年内正式发售，而英国工作室为了给游戏造势，面向所有Steam玩家限时免费赠送原版游戏《邪恶天才》，玩家需要进入活动专门页面，注册并登录Rebellion账号，和Steam......
### [《刺客信条：英灵殿》编剧：剧本的叙事结构前所未见](https://www.3dmgame.com/news/202005/3787856.html)
> 概要: 根据叙事监督兼剧本的说法，《刺客信条：英灵殿》的故事结构非常独特，是你从未体验过的全新版本。育碧已经展示了《刺客信条》系列新作的CGI预告片，并会在明晚公开实机演示。在GameSpot的采访中，剧本D......
### [小编私藏给力壁纸图集第68期 美女游戏风景速欣赏](https://www.3dmgame.com/bagua/3042.html)
> 概要: 精美的壁纸又来了！小编为大家精心挑选了50张壁纸，美女风景，游戏动漫全都有。相信总有一张会让你忍不住想收藏。不要控制自己，喜欢就赶紧点击大图收藏吧......
### [《彩虹六号：围攻》限时活动“激战风云”已开启](https://www.3dmgame.com/news/202005/3787804.html)
> 概要: 今日(5月6日)育碧宣布《彩虹六号：围攻》新活动“激战风云(Grand Larceny)”限时开启，该活动持续至5月19日。活动预告：“激战风云”活动新增了游戏模式，复古补充包亦同步上线，共31包，售......
### [《光环2周年纪念版》发布新预告 5月12日正式上市](https://www.3dmgame.com/news/202005/3787799.html)
> 概要: 几周前在PC端开启测试的《光环2周年纪念版》终于确定将于5月12日推出正式版，官方团队今天确定发售日的同时发布了一段最新预告片，一起来看看吧。《光环2周年纪念版》预告：《光环2》延续了史诗般的冒险故事......
# 论文
### [Adapted Center and Scale Prediction: More Stable and More Accurate](https://paperswithcode.com/paper/adapted-center-and-scale-prediction-more)
> 日期：20 Feb 2020

> 标签：OBJECT DETECTION

> 代码：https://github.com/WangWenhao0716/Adapted-Center-and-Scale-Prediction

> 描述：Pedestrian detection benefits from deep learning technology and gains rapid development in recent years. Most of detectors follow general object detection frame, i.e. default boxes and two-stage process.
### [MRQy: An Open-Source Tool for Quality Control of MR Imaging Data](https://paperswithcode.com/paper/mrqy-an-open-source-tool-for-quality-control)
> 日期：10 Apr 2020

> 标签：None

> 代码：https://github.com/ccipd/MRQy

> 描述：Even as public data repositories such as The Cancer Imaging Archive (TCIA) have enabled development of new radiomics and machine learning schemes, a key concern remains the generalizability of these methods to unseen datasets. For MRI datasets, model performance could be impacted by (a) site- or scanner-specific variations in image resolution, field-of-view, or image contrast, or (b) presence of imaging artifacts such as noise, motion, inhomogeneity, ringing, or aliasing; which can adversely affect relative image quality between data cohorts.
### [Multi-class Gaussian Process Classification with Noisy Inputs](https://paperswithcode.com/paper/multi-class-gaussian-process-classification-1)
> 日期：28 Jan 2020

> 标签：GAUSSIAN PROCESSES

> 代码：https://github.com/cvillacampa/GPInputNoise

> 描述：It is a common practice in the supervised machine learning community to assume that the observed data are noise-free in the input attributes. Nevertheless, scenarios with input noise are common in real problems, as measurements are never perfectly accurate.
