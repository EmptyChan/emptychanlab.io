---
title: 2020-07-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ColoradoColumbine_EN-CN9773186518_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-07-09 22:05:46
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ColoradoColumbine_EN-CN9773186518_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [疑似粉丝使用去世者微博账号 肖战影音会发文道歉](https://ent.sina.com.cn/s/m/2020-07-09/doc-iirczymm1448106.shtml)
> 概要: 新浪娱乐讯 7月9日，肖战影音会官微就疑似粉丝使用去世者微博账号一事发长文，文中表示某微博账号被意外买卖至他人手中，并且发生了系列删博行为，对部分剑三玩家、该微博用户晨小晨生前的亲友及所有心系晨小晨的......
### [汪诗诗为甄子丹庆生 送电影海报版定制蛋糕显心意](https://ent.sina.com.cn/s/h/2020-07-09/doc-iircuyvk2823186.shtml)
> 概要: 新浪娱乐讯 7月9日凌晨，甄子丹娇妻汪诗诗在微博晒出为老公庆生的合照，照片中甄子丹捧着印有自己演过的电影海报图案的定制蛋糕，蛋糕上还将祝福语写在一个场记板上，十分有意义。汪诗诗依偎在老公身旁，画面温馨......
### [STU48结束船上剧场运营 将进行不固定地点演出](https://ent.sina.com.cn/v/j/2020-07-09/doc-iirczymm1447015.shtml)
> 概要: 新浪娱乐讯 STU48今日发布通告，称将从明年春天开始不再使用船作为活动剧场，以后将以濑户内海为中心，不固定地点进行演出。(责编：maiko)......
### [陈伟霆绿洲晒背影照 穿跨栏背心专注打游戏显童趣](https://ent.sina.com.cn/s/h/2020-07-09/doc-iircuyvk2899602.shtml)
> 概要: 新浪娱乐讯 7月9日，陈伟霆在绿洲分享一张背影照，并发文：“canotwait to（陪你）play嘎嘎嘎嘎嘎嘎嘎”。照片中陈伟霆戴黑色毛线帽，身穿黑色跨栏背心，衣服后背上印着明黄色的canotwai......
### [组图：马伊琍带俩女儿现身机场 姐妹俩一路牵手出落得十分水灵](http://slide.ent.sina.com.cn/star/slide_4_704_341618.html)
> 概要: 组图：马伊琍带俩女儿现身机场 姐妹俩一路牵手出落得十分水灵
### [董璇新恋情获全网好评：真正有本事的二婚女人，都偏爱这种男人](https://new.qq.com/omn/20200709/20200709A0LLCB00.html)
> 概要: 董璇新恋情获全网好评：真正有本事的二婚女人，都偏爱这种男人
### [苗苗挺大肚游泳，素颜仍如少女般清纯！郑恺在旁陪伴，大夸孕妻厉害](https://new.qq.com/omn/20200709/20200709A0RVPN00.html)
> 概要: 算算这日子，怕是不用等多久，郑恺和苗苗的小宝贝就要出生了。自5月21日官宣结婚后，借着特殊时期通告少，苗苗一直在家安心养胎，马上就要当爸爸的郑恺则忙于赚奶粉钱。当然，他也不会冷落了孕妻，得空就陪着。7......
### [董璇无视前夫高云翔，和90后男星街头牵手搂腰，被曝新恋情](https://new.qq.com/omn/20200709/20200709A0E2AR00.html)
> 概要: 董璇无视前夫高云翔，和90后男星街头牵手搂腰，被曝新恋情
### [和赵丽颖开餐厅，和秦昊PK演技，杨超越单飞后的资源太炸了](https://new.qq.com/omn/20200709/20200709A0Q7K900.html)
> 概要: 和赵丽颖开餐厅，和秦昊PK演技，杨超越单飞后的资源太炸了
### [秦昊妈妈出“催命题”拷问老公，秦爸爸求生欲上线秒怂，伊能静当场笑喷](https://new.qq.com/omn/20200709/20200709V0IO3100.html)
> 概要: 秦昊妈妈出“催命题”拷问老公，秦爸爸求生欲上线秒怂，伊能静当场笑喷
# 动漫
### [以《SSSS.GRIDMAN》新条茜为题材的漫画制作终止](https://news.dmzj.com/article/67895.html)
> 概要: 根据漫画家风上旬的推特消息，在之前宣布的《SSSS.GRIDMAN》的新条茜外传漫画，由于作品的方向性的不同，导致了无法进行有效的沟通。
### [《钢铁飞龙之再见奥特曼》侵权案一审圆谷胜诉](https://news.dmzj.com/article/67898.html)
> 概要: 根据圆谷的官方消息，圆谷在2020年6月30日，获得了《钢铁飞龙之再见奥特曼》侵权诉讼的一审胜诉。中国上海浦东新区人民法院一审判决被告立即停止侵权行为、作出公开声明以消除影响、赔偿相关经济损失等在内的民事责任。
### [P站美图推荐——女牛仔特辑](https://news.dmzj.com/article/67901.html)
> 概要: 驰骋吧，在无尽的西部荒野~
### [《骚动时节的少女们啊。》真人电视剧化决定](https://news.dmzj.com/article/67897.html)
> 概要: 冈田磨里原作绘本奈央作画的《骚动时节的少女们啊。》宣布了真人电视剧化决定的消息。
### [鬼灭之刃：和服风，炭子、猪子、善子](https://new.qq.com/omn/20200709/20200709A0LV0V00.html)
> 概要: 鬼灭之刃：和服风，炭子、猪子、善子 （by 43441210）...
### [爆笑校园：呆头想考试作弊，但旺财听不清楚，于是呆头这样发短信](https://new.qq.com/omn/20200709/20200709A0OPGY00.html)
> 概要: 爆笑校园：呆头想考试作弊，但旺财听不清楚，于是呆头这样发短信
### [这就是写作业时的我，连镜子都是同款ヾﾉ≧∀≦）o，没救](https://new.qq.com/omn/20200709/20200709A0QPTB00.html)
> 概要: 这就是写作业时的我，连镜子都是同款ヾﾉ≧∀≦)o，没救
### [黑色放映机 露营](https://new.qq.com/omn/20200709/20200709A0PIDE00.html)
> 概要: ...
### [圣斗士星矢漫画：除了经典海南版，还有这些奇特罕见的版本](https://new.qq.com/omn/20200709/20200709A0NIGK00.html)
> 概要: 圣斗士星矢是日本漫画大师车田正美的经典之作，与龙珠、北斗神拳并列为《少年JUMP》80年代的三大台柱，在日本乃至全世界各地都拥有极高人气。中国大陆地区最早引进圣斗士星矢漫画的，就是鼎鼎大名的海南摄影……
### [爆笑漫画：通天箓到底强在什么地方？一秒成符无限连发，开挂了！](https://new.qq.com/omn/20200709/20200709A0ILQ000.html)
> 概要: ...
# 财经
### [会见这些企业家时，龚正说上海要提供更多“阳光雨露”](https://finance.sina.com.cn/china/dfjj/2020-07-09/doc-iirczymm1483213.shtml)
> 概要: 原标题：会见这些企业家时，龚正说上海要提供更多“阳光雨露”龚正会见中智行（上海）交通科技有限公司董事长兼首席执行官王劲 上海市委副书记...
### [广东全面深化市场主体退出制度改革 深圳试点“僵尸企业”除名制](https://finance.sina.com.cn/china/2020-07-09/doc-iircuyvk2957109.shtml)
> 概要: 广东全面深化市场主体退出制度改革 深圳试点“僵尸企业”除名制 经济观察网 记者 于惠如广东省的“僵尸企业”出清，有了明确的“路线规划图”。
### [今年中央转移支付大增12.8% 超8万亿资金怎么分？](https://finance.sina.com.cn/china/2020-07-09/doc-iircuyvk2958968.shtml)
> 概要: 今年中央转移支付大增12.8%，超8万亿资金怎么分 每年中央财政会掏出大笔资金分配给地方，这笔钱被称为中央对地方一般公共预算转移支付。
### [国务院办公厅发布推进医疗保障基金监管制度体系改革的指导意见](https://finance.sina.com.cn/china/2020-07-09/doc-iirczymm1484093.shtml)
> 概要: 国务院办公厅关于推进医疗保障基金监管制度体系改革的指导意见 国务院办公厅关于推进医疗保障基金监管 制度体系改革的指导意见 国办发〔2020〕20号...
### [今日财经TOP10|央行回应：转账超10万将被严查系误读](https://finance.sina.com.cn/china/caijingtop10/2020-07-09/doc-iirczymm1477354.shtml)
> 概要: 【宏观要闻】 NO.1 央行回应：“转账超10万将被严查”系误读 7月1日，河北省全面开展大额现金管理试点工作。随后，网上便有消息称“转账超10万将被严查”...
### [上海市委书记视频连线思爱普全球CEO柯睿安，会见李彦宏](https://finance.sina.com.cn/china/dfjj/2020-07-09/doc-iirczymm1486461.shtml)
> 概要: 上海市委书记李强今天下午（7月9日）分别视频连线参加2020世界人工智能大会云端峰会的德国思爱普公司全球首席执行官柯睿安，会见来沪出席峰会的百度创始人...
# 科技
### [Webwrap是一个简单的脚本，使用web shell来模拟一个终端](https://www.ctolib.com/mxrch-webwrap.html)
> 概要: Webwrap是一个简单的脚本，使用web shell来模拟一个终端
### [Squzy--是一个用Bazel和Golang编写的高性能开源监控、事件和警报系统。](https://www.ctolib.com/squzy-squzy.html)
> 概要: Squzy--是一个用Bazel和Golang编写的高性能开源监控、事件和警报系统。
### [Vue-Component-Inspector 是一款 Vue 2.0 开发调试工具，它本身也是一个 Vue 组件。](https://www.ctolib.com/zglz-vue-component-inspector.html)
> 概要: Vue-Component-Inspector 是一款 Vue 2.0 开发调试工具，它本身也是一个 Vue 组件。
### [Covid-19 Flutter App](https://www.ctolib.com/shubhamhackz-aarogya_seva.html)
> 概要: Covid-19 Flutter App
### [Technical Tuesdays: Markdig and Error-Free Programming in F#](https://www.tuicool.com/articles/QNvqayA)
> 概要: Nerd Roundup Fridays: Off-by-two errors and the history of F#Off-by-two error:The Second Day of July......
### [所有人都在等待，曹国伟拯救新浪的下一步](https://www.tuicool.com/articles/Ir2EVzU)
> 概要: 编者按：本文来自微信公众号“首席人物观”（ID:sxrenwuguan），作者：王明雅，36氪经授权发布。01“奶酪不见了。”某天，当生活在迷宫里的两只小老鼠和两个小矮人，像往常一样去固定站点食用奶酪......
### [一个程序员老兵的思考](https://www.tuicool.com/articles/uqa6JrZ)
> 概要: 工作走的累了，不妨停下来，思考一下这一路走来的艰辛。算一算，我也是工作时间不短的人了。但是总是感觉工作中思路、方法或多或少有问题。前几日和朋友几杯酒下肚，倒是聊出了一些故事，说说自己的感受，也就成了此......
### [向淘宝“宣战”](https://www.tuicool.com/articles/Avmm2q3)
> 概要: 文丨张娜来源丨毒眸（ID：youhaoxifilm）2020年转眼过半，在这个被称为“一直在见证历史”的半年里，许多行业都在面临着不同程度的困境，反倒是直播电商风口正热，一直不缺少入局者。毒眸在过往的......
### [马斯克身价周三缩水 7 亿美元，个人身价仍高达 538 亿美元](https://www.ithome.com/0/496/957.htm)
> 概要: 7 月 9 日消息，据国外媒体报道，在股价连续大涨的推动下，特斯拉 CEO 埃隆 · 马斯克的身价也大幅上涨，但周三股价下跌，也导致马斯克的身价缩水 7 亿美元。周三美国股市收盘时，特斯拉报 1365......
### [佳能推出全画幅专微 EOS R6 ：2010 万像素，15999 元](https://www.ithome.com/0/496/965.htm)
> 概要: IT之家 7 月 9 日消息 今晚，佳能有限公司宣布正式推出佳能新一代全画幅专微相机 EOS R6，官方表示新机提升了在画质、高速连拍、自动对焦、4K 视频、防抖、网络通讯、可靠性与操作性等多方面表现......
### [欧洲最高法院：YouTube 无需上交发布盗版影片的用户信息](https://www.ithome.com/0/496/961.htm)
> 概要: 北京时间 7 月 9 日晚间消息，据国外媒体报道，欧洲最高法院 “欧盟法院”（Court of Justice of the European Union）今日裁定，谷歌旗下视频网站 YouTube ......
### [特斯拉车机系统已能识别国内交通信号灯等标识，自动驾驶有望](https://www.ithome.com/0/496/948.htm)
> 概要: IT之家7月9日消息  据澎湃新闻报道，特斯拉方面称车机系统最新版本已可识别包括交通信号灯、道路方向标志、停车线、自行车标志、其他物体如垃圾筒等。目前世界范围内能做到识别红绿灯的产品极少，目前技术为其......
# 小说
### [陪衬](http://book.zongheng.com/book/945367.html)
> 作者：气质阿鑫

> 标签：历史军事

> 简介：这是一个陪衬、酱油、绿叶，反抗的故事。

> 章节末：第十五章霸业

> 状态：完本
# 游戏
### [2019届中国本科毕业生平均月收入5440元 你达标没？](https://www.3dmgame.com/news/202007/3792640.html)
> 概要: 近日在北京发布的《就业蓝皮书》显示，2019届本科毕业生平均月收入为5440元，高职毕业生平均月收入为4295元。从近五年大学毕业生半年内自主创业的月收入来看，自主创业人群月收入持续高于同届毕业生平均......
### [网传《Elden Ring》新预告将在Xbox发布会上公开](https://www.3dmgame.com/news/202007/3792608.html)
> 概要: 来自4chan的最新爆料，在即将举办的Xbox发布会上，宫崎英高新作《Elden Ring》将公开全新预告片，在4chan上还有一些关于《Elden Ring》的细节爆料，不过需要注意的是，来自4ch......
### [智能手机处理器收入排名：高通第1 海思苹果紧随其后](https://www.3dmgame.com/news/202007/3792638.html)
> 概要: 日前，知名市场研究机构Strategy Analytics发布了2020年第一季度智能手机应用处理器（AP）营收数据报告。报告显示，尽管发生了COVID-19大流行的影响，但全球智能手机应用处理器市场......
### [《Valorant》Elderflame皮肤7月10日上线 售90美元](https://www.3dmgame.com/news/202007/3792598.html)
> 概要: 《英雄联盟》开发商Riot的免费射击游戏《Valorant》发布后，吸引了大量玩家的关注。到目前为止，Riot并没有为该作放出过多的皮肤。但本周这种情况将会改变了。Riot透露称《Valorant》首......
### [《月光心慌慌：杀戮》公布先导预告 迈尔斯又来了](https://www.3dmgame.com/news/202007/3792641.html)
> 概要: “月光光心慌慌”第12部续集《月光心慌慌：杀戮》（Halloween Kills）首曝先导预告，杰米·李·柯蒂斯再度回归扮演Laurie Strode，她又为何而痛哭？当然，最后迈克尔·麦尔斯回来了......
# 论文
### [HiLLoC: Lossless Image Compression with Hierarchical Latent Variable Models](https://paperswithcode.com/paper/hilloc-lossless-image-compression-with-1)
> 日期：20 Dec 2019

> 标签：IMAGE COMPRESSION

> 代码：https://github.com/hilloc-submission/hilloc

> 描述：We make the following striking observation: fully convolutional VAE models trained on 32x32 ImageNet can generalize well, not just to 64x64 but also to far larger photographs, with no changes to the model. We use this property, applying fully convolutional models to lossless compression, demonstrating a method to scale the VAE-based 'Bits-Back with ANS' algorithm for lossless compression to large color photographs, and achieving state of the art for compression of full size ImageNet images.
