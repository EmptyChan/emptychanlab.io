---
title: 2021-11-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PontRouge_ZH-CN0788212424_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-05 22:14:20
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PontRouge_ZH-CN0788212424_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Apache POI 5.1.0 发布，Office 文档的 Java API](https://www.oschina.net/news/167382/poi-5-1-0-released)
> 概要: Apache POI 是 Apache 软件基金会的开放源码库，POI 提供 API 给 Java 程序对 Microsoft Office 格式文件读和写的功能。它同时支持旧的（OLE2）和新的（O......
### [Tengine 社区双周报 | 2021.10.16~2021.10.31](https://www.oschina.net/news/167456)
> 概要: Tengine 社区 10.16-10.31 双周报更新来啦！感谢以下小伙伴为 Tengine 添砖加瓦：cmdbug；FeiGeChuanShu ；shaoeric（闪电侠的右手）；daquexia......
### [杭州西软加入龙蜥社区，应用软件兼容适配已完成](https://www.oschina.net/news/167497)
> 概要: 近日，杭州西软信息技术有限公司（杭州西软科技有限公司）加入龙蜥社区（OpenAnolis），携手上下游合作伙伴一起共建繁荣生态。杭州西软信息技术有限公司（杭州西软科技有限公司）始创于 1993 年，是......
### [Google 开源 SCENIC，用于计算机视觉研究的 JAX 库](https://www.oschina.net/news/167397/google-open-source-jax-library)
> 概要: 计算机视觉是一个跨学科的科学领域，涉及到计算机如何从数字图像或视频中获得高水平的理解。从工程的角度来看，它试图理解人类视觉系统能够完成的任务并使之自动化。计算机视觉领域如今正迅速发展并具有巨大的潜力，......
### [P站美图推荐——独眼特辑](https://news.dmzj.com/article/72666.html)
> 概要: 用眼罩遮住单边眼睛，像是战损又像是封印，这种恰到好处刺激中二之魂的感觉真让人欲罢不能
### [TV动画《自称贤者弟子的贤者》第三弹PV](https://news.dmzj.com/article/72667.html)
> 概要: TV动画《自称贤者弟子的贤者》公开了第三弹主宣传图和PV。在这次的视频中，可以看到主人公们的冒险片段。另外视频也收录了由亚咲花演唱的OP主题曲《Ready Set Go!!》的片段。
### [37岁男性违法印刷销售人气作品角色鼠标垫被逮捕](https://news.dmzj.com/article/72668.html)
> 概要: 日本警察近日逮捕了一名非法印刷贩卖带有人气作品角色的鼠标垫的37岁公司职员盛将来。这名犯罪嫌疑人从去年9月到12月，在未经授权的情况下，私自印刷贩卖《LoveLive！》的角色商品，涉嫌违反了著作权法。
### [漫画「【我推的孩子】」全新特别插画公布](http://acg.178.com/202111/430075589473.html)
> 概要: 漫画「【我推的孩子】」公布了在「周刊YOUNG JUMP」上的最新特别插画。漫画「【我推的孩子】」由赤坂明原作，横枪萌果作画，于2020年4月23日起在集英社「周刊YOUNG JUMP」上连载，曾获「......
### [游戏王历史：从零开始的游戏王环境之旅第八期19](https://news.dmzj.com/article/72670.html)
> 概要: 2013年3月，【征龙魔导环境】正式开幕，游戏王OCG再次突入了黑暗时期。虽然已经不是第一次经历黑暗时期了，但这次却发生在第八期这个近代，在时间上有些致命。在这两家势力分庭抗礼、并且处于黑暗时期的特殊环境发展到一半的时候，于5~6月份卡池的更新带...
### [《荒野大镖客2》亡灵梦魇Mod演示 大战恐怖丧尸](https://www.3dmgame.com/news/202111/3827556.html)
> 概要: 许多玩家还在期待R星能为《荒野大镖客2》推出新内容，让他们能体验新乐趣。但官方始终没有公布DLC消息，于是有大神自己动手，搞出了全新Mod。Mod作者RichardHertz和KristianD3已发......
### [庆祝20周年 《光环：士官长合集》推出免费福利](https://www.3dmgame.com/news/202111/3827559.html)
> 概要: 为庆祝《光环》系列20周年，《光环》官方发推宣布《光环：士官长合集》将免费推出一系列装备，皮肤和配件等物品。343 Industries和Xbox都希望通过在系列中添加一些非常受欢迎的物品，为Halo......
### [热门韩剧《W两个世界》翻拍美剧 重述漫画爱情](https://ent.sina.com.cn/v/u/2021-11-05/doc-iktzscyy3765164.shtml)
> 概要: 新浪娱乐讯 北京时间11月5日消息，据外国媒体报道，热门韩剧《W-两个世界》（W： Two Worlds/Angel City）将翻拍成美剧。CW正在开发中，来自编剧珍·布莱顿（《爱你，维克托》）与希......
### [我现在看见评分4.8的店，转身就走](https://www.huxiu.com/article/469930.html)
> 概要: 本文来自微信公众号：花儿街参考（ID：zaraghost），作者：林默，头图来自：视觉中国一现在在小X书上，刷到一家阳光正好、线条明朗、风格异域的咖啡店，最大的思虑是什么？不是贵，不是远，是走到跟前的......
### [视频：萧亚轩毁容后再次做手术 拒打麻醉痛不欲生](https://video.sina.com.cn/p/ent/2021-11-05/detail-iktzscyy3776676.d.html)
> 概要: 视频：萧亚轩毁容后再次做手术 拒打麻醉痛不欲生
### [「催眠麦克风」7th LIVE BD&DVD包装封面＆特典公开](http://acg.178.com/202111/430083082992.html)
> 概要: 近日，「催眠麦克风」公开了7th LIVE BD&DVD的包装封面和特典图，该商品将于12月15日正式发售。「催眠麦克风」（ヒプノシスマイク Division Rap Battle）是KING REC......
### [罗永浩：下一个创业项目是“元宇宙”](https://www.3dmgame.com/news/202111/3827570.html)
> 概要: 此前，我们曾报道过锤子手机创始人罗永浩在10月19日一篇微博中表示自己将在明年春季重返科技行业，今日（11月5日）凌晨五点，罗永浩发微博称自己下个创业项目是“元宇宙”。罗永浩在凌晨五点的这篇微博中分享......
### [独家 | 清华学子姚颂29岁再创业：创办商业航天公司，天使轮融资4亿元！预计2023年首飞“引力”1号捆...](https://www.tuicool.com/articles/nqu6RvJ)
> 概要: “吃穿用度已不是我的追求目标，还是想做对人类更有意义的事。”前深鉴科技 CEO、清华校友姚颂，在 25 岁时将自己的 AI 芯片公司深鉴科技卖给赛灵思，并实现财务自由。如今他 29 岁，在三十而立之前......
### [男子买户外广告邀JENNIE共进晚餐 称曾想送她房产](https://ent.sina.com.cn/y/yrihan/2021-11-05/doc-iktzscyy3794892.shtml)
> 概要: 新浪娱乐讯 一名菲律宾企业家近日在首尔发布户外广告，邀请BLACKPINK成员JENNIE共进晚餐而引发了话题。　　这名菲律宾企业家自称JENNIE的粉丝，表示自己因为不知道该怎么联系JENNIE，只......
### [“金克丝发型屋”11月6日开业 anglebaby出场](https://www.3dmgame.com/news/202111/3827573.html)
> 概要: 今日（11月5日），英雄联盟手游官方微博宣布“金克丝发型屋”将于11月6日至14日在上海TX淮海B1层正式开业，其中演员Angelababy将Cos金克丝来到现场担任一日店长。活动预告：金克丝Cos照......
### [「碧蓝航线」应瑞手办即将开订](http://acg.178.com/202111/430090000410.html)
> 概要: 「碧蓝航线」应瑞手办宣布即将开启预订，作品采用ABS、PVC材质，全高约24200mm，日版售价1391日元（含税），约合人民币227元，预计将于2022年11月发售......
### [因缺乏使用 索尼PSV部分商标在欧洲被撤销](https://www.3dmgame.com/news/202111/3827582.html)
> 概要: 由于缺乏使用，索尼已经失去了一些PlayStation Vita的商标权。欧盟法院裁定，该名称现在可以用于“包含程序的数据载体”和“音频和/或图像载体”。据GIBiz报道，索尼将保留Vita这个名字的......
### [组图：景甜晒《司藤》片场花絮照 为导演李木戈庆生](http://slide.ent.sina.com.cn/star/slide_4_704_363383.html)
> 概要: 组图：景甜晒《司藤》片场花絮照 为导演李木戈庆生
### [五年之后变了天，毛戈平怎么和完美日记们竞争市场](https://www.tuicool.com/articles/fUJnqui)
> 概要: 10月21日，据证监会披露，毛戈平化妆品股份有限公司过会成功，不出意外将会登上主板，A股市场也即将迎来“国潮彩妆第一股”。早在2016年12月，毛戈平就有了上市计划，并递交了招股书。但一年之后审查突然......
### [微软验证器 Authenticator 获得新流畅设计 Logo，还支持全新帐户管理](https://www.ithome.com/0/584/966.htm)
> 概要: IT之家11 月 5 日消息，据 onMSFT 报道，微软今天更新了 iOS 和Android版 Authenticator 验证器应用，新版本为用户带来了刷新的流畅设计版图标 Logo 和新的企业帐......
### [2049 元：红米 K40 手机顶配版京东新低 + 6 期免息](https://www.ithome.com/0/584/986.htm)
> 概要: 红米 K40 12GB+256GB（顶配版）今年 3 月上市，售价 2699 元。京东双 11 大促价 2299 元，今日可再领售价下方 100+150 元券，实付 2049 元新低价 + 6 期免息......
### [How to learn compilers: LLVM Edition](https://lowlevelbits.org/how-to-learn-compilers-llvm-edition/)
> 概要: How to learn compilers: LLVM Edition
### [PNY 推出 XLR8 Gaming DDR5 内存：最高 5600 MHz，CL36 时序](https://www.ithome.com/0/584/991.htm)
> 概要: IT之家11 月 5 日消息，根据外媒 techpowerup 报道，存储厂商 PNY 今日宣布推出多款 DDR5 内存条，分为 XLR8 Gaming MAKO 系列，以及 Performance ......
### [组图：《爱的接力棒》召开问候活动 田中圭永野芽郁出席](http://slide.ent.sina.com.cn/star/jp/slide_4_704_363388.html)
> 概要: 组图：《爱的接力棒》召开问候活动 田中圭永野芽郁出席
### [漫威漫画「奇异博士之死：血石」故事梗概及封面公开](http://acg.178.com/202111/430100801479.html)
> 概要: 近日，漫威漫画公开了联动故事「奇异博士之死」全新支线单刊「奇异博士之死：血石」的故事梗概以及正式封面。本刊由编剧Tini Howard女士编写，画师Ig Guara绘制，将于2022年1月发布。「奇异......
### [谁还不是个宝宝：最生活大毛巾 19.8 元闭眼囤（官方 79.9 元）](https://lapin.ithome.com/html/digi/585000.htm)
> 概要: 【京东 最生活拼购旗舰店】最生活 婴儿浴巾（100*50cm）现售 64 元，今日下单 2 件立打 6 折，叠加 37 元大额券，2 件实付 39.6 元包邮：37 元大额券：点此领券京东最生活 婴儿......
### [大牌涨价，务实的年轻人转战二手奢侈品](https://www.tuicool.com/articles/yQ3I7fJ)
> 概要: 随着Z世代成为驱动奢侈品消费市场增长的新动力，“性价比更高”的二手奢侈品迎来春天。疫情之下，国际一线奢侈品牌纷纷逆市涨价，涨幅和调价频率更胜以往。日前，国际奢侈品牌Chanel再次上调手袋价格。其经典......
### [在线协作趋势观察：会聚力的时代 才是最好的时代](https://www.tuicool.com/articles/zyaAJvz)
> 概要: 作者：曹建菊被誉为“硅谷精神之父”、“科技商业预言家”的凯文凯利几年前曾有过一个很著名的关于“未来12个长期趋势”的预言，其中之一是关于共享（Sharing），他当时指出：共享的核心不是分享，而是协作......
### [最高效的领导者，应该都有社交牛逼症](https://www.huxiu.com/article/470091.html)
> 概要: 这段时间，“社交牛逼症”成为热词，它与社交恐惧症相反，形容能在人群交流中具备开朗外向，善于交际的性格特点。而作为一位领导者，是否能真正地达到“高效”，还得依赖团队成员或下属。那么如何引导员工及时且高效......
### [Peeking Through Logs](https://asylum.madhouse-project.org/blog/2021/10/23/peeking-through-logs/)
> 概要: Peeking Through Logs
### [信息泄露别慌，国家帮你打官司](https://www.huxiu.com/article/470133.html)
> 概要: 出品｜虎嗅科技组作者｜张雪封面｜CFP11月1日，《个人信息保护法》开始正式实施，这是我国首部专门针对个人信息保护的系统性、综合性法律，这部法律直面了大众长期关注的个人信息收集、使用等问题，并明确：不......
### [奔着迷雾剧场的招牌追《致命愿望》，差点被弹幕劝退了！](https://new.qq.com/rain/a/20211105A0BTME00)
> 概要: 不知道有多少观众跟我一样，虽然知道迷雾剧场里的每一部新剧都是单独存在的作品，但是还是会因为迷雾剧场这招牌而追剧，特别是像我这样比较喜欢看悬疑片的，迷雾剧场里的新剧就更加不会错过了。          ......
### [From macOS to Arch Linux](https://www.juxt.pro/blog/from-mac-to-arch)
> 概要: From macOS to Arch Linux
### [Show HN: View the patent and innovation history of any company](https://goodip.io/iq/)
> 概要: Show HN: View the patent and innovation history of any company
### [精灵幻想记第二季制作决定，美少女多的动画就是给力](https://new.qq.com/omn/20211105/20211105A0CH1X00.html)
> 概要: 《精灵幻想记》是一部男主穿越异世界题材的作品，原著是轻小说，动画第一季放送期间，也是收获了不少观众的喜爱的。当然了，放在众多异世界题材的作品中，它的剧情并不出彩，甚至可以归为类很多漫迷吐槽的厕纸类。至......
### [长了一张正人君子的脸，演“坏人”却让人恨得牙痒，这样的演员太稀缺](https://new.qq.com/rain/a/20211105A0AHID00)
> 概要: 1990年，小品《主角与配角》中，陈佩斯有一句经典台词：“没想到啊没想到，你浓眉大眼的朱时茂也叛变革命了！ ”这句话说完，台下观众立马哄堂大笑。这句话之所以“笑果”十足，就在于它颠覆了大家的一个认知......
### [斗罗大陆：波塞西亮出魂环，配置比唐三还高，又美又有实力](https://new.qq.com/omn/20211105/20211105A0CT2I00.html)
> 概要: 在《斗罗大陆》动漫中，唐三的实力可以说是一直远超同龄人，包括他的魂环配置也是。然而随着见识越来越多，大家也明白了什么叫人外有人天外有天。来到海神岛之后，唐三不仅增长了实力，还见识到了顶级斗罗波塞西的魂......
### [假面骑士欧兹10周年剧场版公开，800年前的王登场，ankh也回归了](https://new.qq.com/omn/20211105/20211105A0CU8Q00.html)
> 概要: 对于假面骑士来说今年不仅仅是假面骑士的50周年，而且还是假面骑士欧兹的10周年。于是乎在今天晚上东大妈放出了一个令人震撼的消息，那就是假面骑士欧兹迎来了自己10周年的特别篇剧场版，在反派方面更是安排了......
### [这座城市，政府免费给封控区居民发生活物资！每人每天标准→](https://finance.sina.com.cn/wm/2021-11-05/doc-iktzscyy3882600.shtml)
> 概要: 原标题：这座城市，政府免费给封控区居民发生活物资！每人每天标准→江苏省常州市武进区政府副区长张小虎介绍，对封控区6725户居民全部采取政府免费发放生活物资的形式...
### [张桂梅等68人被授予第八届全国道德模范荣誉称号](https://finance.sina.com.cn/china/2021-11-05/doc-iktzscyy3882061.shtml)
> 概要: 原标题：（受权发布）关于表彰第八届全国道德模范的决定 新华社北京11月5日电 关于表彰第八届全国道德模范的决定 各省、自治区、直辖市和新疆生产建设兵团精神文明建设委员...
### [财经TOP10|刚刚！海底捞宣布：关停300家门店！但不裁员](https://finance.sina.com.cn/china/caijingtop10/2021-11-05/doc-iktzscyy3884031.shtml)
> 概要: 【政经要闻】 NO.1 国家发改委：蔬菜供应量充足 居民消费有保障 记者5日从国家发展改革委获悉，当前，蔬菜供应总量充足，居民消费有保障。
### [实现“长制久清”！陕西26条黑臭水体完成整治](https://finance.sina.com.cn/china/dfjj/2021-11-05/doc-iktzscyy3885415.shtml)
> 概要: 原标题：实现“长制久清”！陕西26条黑臭水体完成整治 11月4日，陕西省生态环境厅举办新闻发布会，通报陕西省水生态环境保护工作情况。
### [去年2月被免职的湖北省卫健委原主任刘英姿，履新湖北省政协](https://finance.sina.com.cn/china/dfjj/2021-11-05/doc-iktzscyy3883411.shtml)
> 概要: 澎湃新闻记者 岳怀让 去年2月被免去湖北省卫生健康委员会主任职务的刘英姿，已明确去向。 据湖北省政协官网消息，2021年10月28日政协湖北省第十二届委员会常务委员会第十四...
### [《驾驶我的车》，一直开到敞开心扉](https://new.qq.com/rain/a/20211105A0D6ZB00)
> 概要: 今年，滨口龙介一连出了两部佳作，《偶然与想象》里用三段巧合的故事，把命运齿轮转向时的咔嚓巨响，描摹出了严丝合缝的宿命感。而改编自村上春树同名短篇小说的《驾驶我的车》，在村上春树特有的孤独绅士的内核里，......
### [住建部：北京等21个城市（区）开展第一批城市更新试点工作](https://finance.sina.com.cn/jjxw/2021-11-05/doc-iktzscyy3885681.shtml)
> 概要: 为积极稳妥实施城市更新行动，引领各城市转型发展、高质量发展，11月5日，住房和城乡建设部办公厅发布《关于开展第一批城市更新试点工作的通知》（简称《通知》）...
### [假面骑士欧兹里的王究竟有多强？其欲望是打倒创造人类的神明！](https://new.qq.com/omn/20211105/20211105A0D9AW00.html)
> 概要: 导语：假面骑士欧兹终于迎来了十周年剧场版，不得不说万代亲儿子的称号并非是浪得虚名。而在这次剧场版则是让假面骑士欧兹故事里的王进行登场了，作为800年前的王者，曾经击败了所有的干部，而且吸收了他们的核心......
### [51岁古装美人梁小冰社交网跳热舞，一身少女装扮，却自嘲萝卜腿](https://new.qq.com/rain/a/20211105A0D8SF00)
> 概要: 本文编辑剧透社：彤心晓筑未经授权严禁转载，发现抄袭者将进行全网投诉梁小冰一直是观众眼中的古典美人，提到古装美女，大家总会第一个想到她，这些年逐渐淡出圈的梁小冰虽然鲜少有新作品，可是她却没有淡出观众的视......
# 小说
### [初唐风云](http://book.zongheng.com/book/1142125.html)
> 作者：大大大冰岛

> 标签：历史军事

> 简介：一位孤儿穿越初唐，他会给此界带来怎样的改变呢？

> 章节末：第四十章原是一场梦！

> 状态：完本
# 论文
### [Topology-Imbalance Learning for Semi-Supervised Node Classification | Papers With Code](https://paperswithcode.com/paper/topology-imbalance-learning-for-semi)
> 日期：8 Oct 2021

> 标签：None

> 代码：None

> 描述：The class imbalance problem, as an important issue in learning node representations, has drawn increasing attention from the community. Although the imbalance considered by existing studies roots from the unequal quantity of labeled examples in different classes (quantity imbalance), we argue that graph data expose a unique source of imbalance from the asymmetric topological properties of the labeled nodes, i.e., labeled nodes are not equal in terms of their structural role in the graph (topology imbalance).
### [Effective Use of Graph Convolution Network and Contextual Sub-Tree forCommodity News Event Extraction | Papers With Code](https://paperswithcode.com/paper/effective-use-of-graph-convolution-network)
> 日期：27 Sep 2021

> 标签：None

> 代码：None

> 描述：Event extraction in commodity news is a less researched area as compared to generic event extraction. However, accurate event extraction from commodity news is useful in abroad range of applications such as under-standing event chains and learning event-event relations, which can then be used for commodity price prediction.
