---
title: 2023-04-20-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CrestedButteEclispe_ZH-CN5715446670_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-20 23:17:22
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CrestedButteEclispe_ZH-CN5715446670_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [AI如何帮助服务设计创造：创造力与智能共生](https://www.woshipm.com/ai/5810772.html)
> 概要: 随着互联网技术的飞速发展，设计与 AI 之间的关系变得如何？并且AI该怎样帮助和服务设计创造，实现创造力与智能共生呢？本文不仅探讨了 AI 如何帮助我们设计更好的服务，还探讨了设计如何帮助构建更好的 ......
### [看了这些老外的骚操作，我也觉得应该暂停GPT4](https://www.woshipm.com/ai/5810740.html)
> 概要: 最新的AI技术显示，通过下载公有模型使用云技术，就能打造一个属于自己的模型，为AI添加独一无二的灵魂。AI真的做到如此强大了吗？应该暂停大模型研究吗？一起来看看这篇文章吧。AI比你更懂人情世故。我知道......
### [我对ChatGPT的理解和思考](https://www.woshipm.com/ai/5810087.html)
> 概要: ChatGPT从出世到如今已经快半年了，带来的革新消息有很多，然而也有很多的人在利用它提升自己的工作效率。作者对ChatGPT做了深入的研究，并从三个方面分享自己关于ChatGPT的思考和理解，一起来......
### [特斯拉第一季度营收233.29亿美元 净利润同比下滑24%](https://finance.sina.com.cn/tech/it/2023-04-20/doc-imyqynxx0179755.shtml)
> 概要: 新浪科技讯 北京时间4月20日凌晨消息，特斯拉汽车今天公布了该公司的2023财年第一季度财报。报告显示，特斯拉汽车第一季度总营收为233.29亿美元，与去年同期的187.56亿美元相比增长24%；净利......
### [是什么给了安踏李宁，又一次涨价的勇气？](https://www.huxiu.com/article/1241991.html)
> 概要: 来源｜表外表里（ID：excel-ers）作者｜赫晋一 陈子儒 高赵宇，编辑｜付晓玲 曹宾玲头图｜IC photo放弃尊严、降价“渡劫”的安踏李宁们，又硬气了起来。有消费者反映，李宁把6折券、生日券取......
### [ChatGPT威胁版权？42家德国作者和表演者协会呼吁欧盟对AI加强监管](https://finance.sina.com.cn/tech/internet/2023-04-20/doc-imyqynxv7711738.shtml)
> 概要: ChatGPT威胁版权？42家德国作者和表演者协会呼吁欧盟对AI加强监管
### [订单增长达历史新高，「速卖通」新启三大尝试｜最前线](https://36kr.com/p/2222370208367751)
> 概要: 订单增长达历史新高，「速卖通」新启三大尝试｜最前线-36氪
### [以临床IRT为核心拓展产品，「杉互健康」打通临床数据及药物管理](https://36kr.com/p/2214584061195653)
> 概要: 以临床IRT为核心拓展产品，「杉互健康」打通临床数据及药物管理-36氪
### [邓超发博为鹿晗庆生 送的长寿面加满了辣椒](https://ent.sina.com.cn/s/m/2023-04-20/doc-imyqytfv0057021.shtml)
> 概要: 新浪娱乐讯 4月20日上午，邓超微博发文为鹿晗庆生，“@M鹿M 生日快乐，爸给你准备的长寿面，不能剩啊”，邓超配图里的“长寿面”加满了辣椒。邓超、鹿晗多次一起录制综艺节目，两人私下友情很深，经常在微博......
### [惊喜不断！五月天演唱会泉州站审批通过](https://ent.sina.com.cn/y/ygangtai/2023-04-20/doc-imyqytfv0057617.shtml)
> 概要: 新浪娱乐讯  据福建省文化和旅游厅网站公示，五月天2023“好好好想见到你”世界巡回演唱会泉州站审批通过。此前五月天北京演唱会审批通过，此次归巢即将达成鸟巢7年开唱20场的传奇纪录，五月天2023好好......
### [飞鹤家族IP升级](https://www.zcool.com.cn/work/ZNjQ5NTY5OTI=.html)
> 概要: 为了市场应用更广泛，传达信息更容易，在情感上产生共鸣，在保留飞鹤显著特征同时，进行人形化升级创作。在鹤小飞形象创作完成后，延申到飞鹤家族形象设计中，保证了形象的完整和统一性......
### [钻石杯青少年网球挑战赛高燃回归！Der•1863受邀参与揭幕战发布](http://www.investorscn.com/2023/04/20/106975/)
> 概要: 钻石杯青少年网球挑战赛高燃回归！Der•1863受邀参与揭幕战发布
### [视频：文彬所属社发布公告 确认文彬去世消息](https://video.sina.com.cn/p/ent/2023-04-20/detail-imyqyxpv5092072.d.html)
> 概要: 视频：文彬所属社发布公告 确认文彬去世消息
### [2023年伦敦游戏节客流量达5.8万人 参展商超800家](https://www.3dmgame.com/news/202304/3867510.html)
> 概要: 根据 GI.biz 报道，英国伦敦游戏节组织者日前宣布，今年的活动，包含了 20 个免费和售票游戏活动，例如游戏金融时长、游戏古玩展示Now Play This 和消费者展览 WASD 在 12天内吸......
### [国家级！摇橹船科技荣获中国产学研合作创新奖](http://www.investorscn.com/2023/04/20/106978/)
> 概要: 近日，第十四届中国产学研合作创新大会在北京隆重召开，来自教育部、工信部、科技部、中国科协、中国科学院、中国工程院等相关部门的领导、两院院士以及来自全国产学研界第一线的千余名代表出席大会，摇橹船科技创始......
### [A卡降价后 NVIDIA坐不住了：RTX 4090/4080破发](https://www.3dmgame.com/news/202304/3867526.html)
> 概要: 4月19日消息，RTX 4070发布上市后，压力山大的AMD闪电做出反应，对多款RX 6000系列显卡开刀，一边打着科普旗号强调同价位下选大显存才是王道，另一边RX 6950 XT/RX 6800 X......
### [知乎 「灯塔计划」发布会定格动画](https://www.zcool.com.cn/work/ZNjQ5NTk2MjA=.html)
> 概要: 这是我们WAVE STUDIO定格动画工作室为 知乎「灯塔计划」制作的项目3周，3分钟，13个场景，挑战不可能！众多装置小发明，我们克服了很多困难，做了大量的测试，最后为客户提交了一份满意的答卷！片中......
### [《尼罗河勇士2》「风的勇士」版本现已更新](https://www.3dmgame.com/news/202304/3867533.html)
> 概要: 在今天的Gamera Games五周年特别节目上，由锅炉房工作室开发的肉鸽战棋游戏《尼罗河勇士2》迎来了一段新的宣传视频。在回顾系列的发展历程之余，还公开了最新版本“风的勇士”，宣布《尼罗河勇士》初代......
### [世界唯一超珍《游戏王》卡拍卖 现价已超13万美元](https://www.3dmgame.com/news/202304/3867537.html)
> 概要: 集换式卡牌越来越受收藏界追捧，近日一张世界仅发行一张超珍贵《游戏王》卡牌拍卖，名为“伟大的战士泰勒”，截止本文发稿，现价已超13万美元。·“伟大的战士泰勒（（Tyler the Great Warri......
### [当广告主爱上社区智能屏，中国第一大梯媒的魅力何在？](http://www.investorscn.com/2023/04/20/106985/)
> 概要: 电梯智能屏已成为中国第一大梯媒。4月19日，社区营销研究院发布《2023社区电梯智能屏广告发展报告》（以下简称“报告”）。报告首次披露了社区电梯智能屏广告投放的增速和行业、城市分布情况，多组亮点数据印......
### [漫画《半妖少女绮丽谭》连载再开！](https://news.idmzj.com/article/77811.html)
> 概要: 漫画《半妖少女绮丽谭》连载再开！新插图公开。
### [硅胶儿童吸管杯](https://www.zcool.com.cn/work/ZNjQ5NjMzOTI=.html)
> 概要: 猫系列-儿童硅胶吸管杯Cat Series-Children's Silicone Straw Cup产品设计灵感源于对“猫”的印象。寻找记忆中猫的轮廓曲线，再现了双耳竖立的猫头与软萌可爱的猫爪造型，......
### [海外new things | 网络安全技术初创「Otterize」种子轮融资1150万美元，帮助开发人员自动配置服务器权限](https://36kr.com/p/2215083113362822)
> 概要: 海外new things | 网络安全技术初创「Otterize」种子轮融资1150万美元，帮助开发人员自动配置服务器权限-36氪
### [杂志《花与梦》刊载49周年！纪念企划公开](https://news.idmzj.com/article/77812.html)
> 概要: 《花与梦》49周年!《坠落JK》连载100回，复制原画16张套装礼物等企划公开。
### [网络表情丨小猪猪臭宝 情侣篇  甜蜜上线](https://www.zcool.com.cn/work/ZNjQ5NjQxMDA=.html)
> 概要: Collect......
### [海外new things | 哥伦比亚金融技术初创「Kala」融资400万美元，协助拉丁美洲银行推出信贷产品](https://36kr.com/p/2215720605693318)
> 概要: 海外new things | 哥伦比亚金融技术初创「Kala」融资400万美元，协助拉丁美洲银行推出信贷产品-36氪
### [美国忙“脱钩”，德企为啥要"对着干"？](https://finance.sina.com.cn/china/2023-04-20/doc-imyqzkcm7636818.shtml)
> 概要: 作者：李晓喻 来源：国是直通车 这是一个很有意思的动向：美国对中国各种遏制、围堵、打压，不遗余力推动对华经贸“脱钩”之际，德国企业却纷纷加码对华投资。 到中国去！
### [该戳破网飞“神话”了](https://www.huxiu.com/article/1235761.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜《九品芝麻官》截图这个造神与祛魅同时进行的时代，光环笼罩下的网飞（Netflix）依旧活在浮冰之上——两天前（ 4 月 18 日），网飞递出 2023Q1 财务报......
### [关于通缩、宏观杠杆率、货币政策，央行全面回应](https://finance.sina.com.cn/roll/2023-04-20/doc-imyqzqmi7513629.shtml)
> 概要: 如何看待3月通胀读数偏低与金融数据高增形成背离？发达国家货币政策“大收大放”对我国货币政策有何影响？结构性货币政策工具如何“有进有退”？
### [商务部：今年以来，新签约的外资项目300多个](https://finance.sina.com.cn/roll/2023-04-20/doc-imyqzqmp5043704.shtml)
> 概要: 1~3月，合同外资1亿美元以上的大项目，实到外资2232.8亿元人民币。 今年一季度，我国吸收外资实现“开门红”。 20日，商务部新闻发言人束珏婷在例行新闻发布会上介绍...
### [华为颁发最高荣誉奖：任正非与40名华为员工代表合影](https://www.3dmgame.com/news/202304/3867556.html)
> 概要: 快科技4月19日消息，据心声社区微信公众号，4月18日，40位2022年度金牌个人奖及金牌团队奖的获奖代表集聚于深圳坂田基地A3图书馆，为了表彰他们为华为所做出的突出贡献，华为创始人任正非在此与他们集......
### [皮克斯《疯狂元素城》确认国内引进！](https://news.idmzj.com/article/77816.html)
> 概要: 皮克斯新片《疯狂元素城》发布中文角色海报，宣布确认引进中国内地，档期待定(北美为6.16)。
### [央行召开2023年科技工作电视会议，要求加快金融数字化转型，加强金融标准供给与实施](https://finance.sina.com.cn/china/2023-04-20/doc-imyqzqmk9596660.shtml)
> 概要: 2023年4月20日，人民银行召开2023年科技工作电视会议。会议总结2022年和五年来科技工作，分析当前形势，部署2023年科技工作。人民银行党委委员...
### [美摄科技出席CCBN2023，全方位展示信创国产化产品方案](http://www.investorscn.com/2023/04/20/106990/)
> 概要: 由国家广播电视总局指导、广播电视科学研究院主办的第29届中国国际广播电视信息网络展览会（CCBN2023）于4月18日-21日在北京首钢会展中心举行。本届大会以“大视听 向未来”为主题，通过举办主题报......
### [2023 年“铁拳”行动重点打击“刷单炒信”等 8 类违法行为](https://www.ithome.com/0/687/836.htm)
> 概要: IT之家4 月 20 日消息，据央视新闻，市场监管总局印发了 2023 年“铁拳”行动方案，明确了重点打击的 8 类违法行为，分别是：食品中非法添加降糖降压降脂等物质；假冒伪劣化肥；刷单炒信等虚假宣传......
### [《黑色五叶草 魔法帝之剑》公开新的场景片段](https://news.idmzj.com/article/77819.html)
> 概要: 根据田畠裕基原作改编，将于6月16日上映的动画电影《黑色五叶草 魔法帝之剑》公开了新的场景片段。
### [事关通缩、信贷投放、房地产、利率等 央行最新回应(实录全文)](https://finance.sina.com.cn/china/2023-04-20/doc-imyqzqmk9604386.shtml)
> 概要: 2023年一季度金融统计数据新闻发布会文字实录 人民银行于2023年4月20日（星期四）15：00举行2023年一季度金融统计数据有关情况新闻发布会，调查统计司司长...
### [苹果 Beats Studio Buds+ 无线耳机即将发布，现已通过 IMDA 认证](https://www.ithome.com/0/687/847.htm)
> 概要: IT之家4 月 20 日消息，苹果旗下的 Beats Studio Buds+ 耳机近日通过美国 FCC 以及新加坡 IMDA 认证，认证型号分别为“A2872”和“A2871”。诸多外媒认为，这款耳......
### [连续8年造假、虚增利润就达28亿，*ST奇信这回悬了！](https://www.yicai.com/news/101736739.html)
> 概要: 在曾经漂亮的上市履历背后，却问题重重。
### [【IT之家开箱】vivo X Fold2 折叠屏手机图赏：华夏红养眼，更轻更薄](https://www.ithome.com/0/687/865.htm)
> 概要: 4 月 20 日晚上，vivo 举行了折叠系列旗舰新品发布会，推出了 vivo X Fold2、vivo X Flip 以及 vivo Pad2 三款新品。目前IT之家已经拿到了华夏红版本的 vivo......
### [打造新版图！开云正式成为皇马官方合作伙伴！](http://www.investorscn.com/2023/04/20/106992/)
> 概要: 近日，开云体育宣布与皇家马德里俱乐部达成合作伙伴关系。两家知名品牌将进一步深化体育领域改革，实现线上线下双通道体育娱乐新模式，共同赋能数字体育行业，为全球足球爱好者提供更多精彩的赛事和完美体育娱乐体验......
### [一季度广义财政收支差1.6万亿，二季度收入加快已有迹象](https://www.yicai.com/news/101736787.html)
> 概要: 今年企业销售收入增速逐月上行至3月份同比增长12.8%，4月1~5日同比增长高达21.2%，按一个月时间差，将体现为4月~5月上旬税收高增。
### [航拍进入三摄时代！大疆官宣 4 月 25 日举行新品发布会：Mavic 3 Pro 即将登场](https://www.ithome.com/0/687/873.htm)
> 概要: 感谢IT之家网友焖饭cc、坐和放宽、软媒新友1957189、小爷Jeffery的线索投递！IT之家4 月 20 日消息，大疆今日宣布，将于 4 月 25 日 21:00 举行新品发布会，届时可能会推出......
### [4月LPR“按兵不动” A股三大指数弱势震荡｜财经夜行线](https://www.yicai.com/video/101736832.html)
> 概要: 4月LPR“按兵不动” A股三大指数弱势震荡｜财经夜行线
### [SpaceX星舰发射失败，马斯克祝贺完成发射测试](https://www.huxiu.com/article/1253385.html)
> 概要: 虎嗅注：盼了快3天，星舰终于成功升空，但是这次发射任务最终还是宣告失败了。SpaceX官方表态称：“星舰的试飞似乎还不够令人满意，星舰发生了非计划内的解体。团队将继续审查数据并为下一次飞行测试努力。”......
### [“星舰”点火发射，升空不久后发生爆炸](https://www.yicai.com/news/101736846.html)
> 概要: 马斯克旗下SpaceX重型运载火箭“星舰”点火发射。
### [上海车展繁荣背后，谁将撑起汽车行业的下一个增长点](https://www.yicai.com/news/101736818.html)
> 概要: “汽车厂商正在面临巨大的利润压力，别看车展上场面盛大，接下来的一段日子竞争厮杀会更激烈，特斯拉只要再下调车价，一大批汽车厂商都没法继续生存了。”
# 小说
### [汉王宝藏](http://book.zongheng.com/book/490613.html)
> 作者：只爱小灰灰

> 标签：悬疑灵异

> 简介：某天，一个混蛋盗取了我家祖传的“帝胄”，将我拉入寻找高祖刘邦的六件绝世汉甲的队伍中。危机四伏的探险，各怀鬼胎的同伴，我闹不清所谓宝藏，是人心，还是金银……弱弱建个群：481763506~谢跟书的亲们，求打赏求一切~

> 章节末：第六十七章 善始善终

> 状态：完本
# 论文
### [Dynamic Programming in Rank Space: Scaling Structured Inference with Low-Rank HMMs and PCFGs | Papers With Code](https://paperswithcode.com/paper/dynamic-programming-in-rank-space-scaling-1)
> 概要: Hidden Markov Models (HMMs) and Probabilistic Context-Free Grammars (PCFGs) are widely used structured models, both of which can be represented as factor graph grammars (FGGs), a powerful formalism capable of describing a wide range of models. Recent research found it beneficial to use large state spaces for HMMs and PCFGs. However, inference with large state spaces is computationally demanding, especially for PCFGs. To tackle this challenge, we leverage tensor rank decomposition (aka.\ CPD) to decrease inference computational complexities for a subset of FGGs subsuming HMMs and PCFGs. We apply CPD on the factors of an FGG and then construct a new FGG defined in the rank space. Inference with the new FGG produces the same result but has a lower time complexity when the rank size is smaller than the state size. We conduct experiments on HMM language modeling and unsupervised PCFG parsing, showing better performance than previous work. Our code is publicly available at \url{https://github.com/VPeterV/RankSpace-Models}.
### [Large Selective Kernel Network for Remote Sensing Object Detection | Papers With Code](https://paperswithcode.com/paper/large-selective-kernel-network-for-remote)
> 日期：16 Mar 2023

> 标签：None

> 代码：https://github.com/zcablii/Large-Selective-Kernel-Network

> 描述：Recent research on remote sensing object detection has largely focused on improving the representation of oriented bounding boxes but has overlooked the unique prior knowledge presented in remote sensing scenarios. Such prior knowledge can be useful because tiny remote sensing objects may be mistakenly detected without referencing a sufficiently long-range context, and the long-range context required by different types of objects can vary. In this paper, we take these priors into account and propose the Large Selective Kernel Network (LSKNet). LSKNet can dynamically adjust its large spatial receptive field to better model the ranging context of various objects in remote sensing scenarios. To the best of our knowledge, this is the first time that large and selective kernel mechanisms have been explored in the field of remote sensing object detection. Without bells and whistles, LSKNet sets new state-of-the-art scores on standard benchmarks, i.e., HRSC2016 (98.46\% mAP), DOTA-v1.0 (81.85\% mAP) and FAIR1M-v1.0 (47.87\% mAP). Based on a similar technique, we rank 2nd place in 2022 the Greater Bay Area International Algorithm Competition. Code is available at https://github.com/zcablii/Large-Selective-Kernel-Network.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
