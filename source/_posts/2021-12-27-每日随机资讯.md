---
title: 2021-12-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SnowBuntings_ZH-CN6554424742_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-12-27 20:45:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SnowBuntings_ZH-CN6554424742_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [DBeaver 21.3.2 发布，社区版默认包含 SSHJ 实现](https://www.oschina.net/news/175698/dbeaver-21-3-2-released)
> 概要: DBeaver 是一个免费开源的通用数据库工具，适用于开发人员和数据库管理员。DBeaver 21.3.2 发布，更新内容如下：数据编辑器：过滤器面板中的自动补全功能得到了修复修复了 XML 的自动格......
### [Linus 在 1994 年被丢失的演讲录音公开](https://www.oschina.net/news/175708/lost-talks-from-linus-at-decus94)
> 概要: Linux Professional Institute 董事会主席Jon Maddog Hall在 Archive.org 上公开了Linus Torvalds 在 1994 年发表的主题演讲录音......
### [MyBatis 3.5.9 发布，Java 数据持久层框架](https://www.oschina.net/news/175697/mybatis-3-5-9-released)
> 概要: MyBatis 3.5.9已发布，MyBatis 的前身为 iBatis，是一个数据持久层（ORM）框架，它提供的持久层能力包括 SQL Maps 和 Data Access Objects（DAO）......
### [Swift 扩展已登陆 VS Code，开发者终能摆脱 Xcode](https://www.oschina.net/news/175706/swift-for-visual-studio-code)
> 概要: 日前一款由 SSWG 维护的 Swift 扩展登陆 Visual Studio Code。为 Visual Studio Code 添加了对 Swift 的语言支持。SSWG（Swift Server......
### [使用 Nginx 构建前端日志统计服务](https://segmentfault.com/a/1190000041184489?utm_source=sf-homepage)
> 概要: 背景之前的几篇文章都是关于之前提到的低代码平台的。这个大的项目以 low code 为核心，囊括了编辑器前端、编辑器后端、C 端 H5、组件库、组件平台、后台管理系统前端、后台管理系统后台、统计服务、......
### [TYPE-MOON《魔法使之夜》正式宣布动画电影化](https://news.dmzj.com/article/73173.html)
> 概要: TYPE-MOON作品《魔法使之夜》正式宣布了动画电影化决定的消息。本作的一段PV也一并公开。本作将由ufotable负责制作，有关动画的更多内容，还将于日后公开。
### [P站美图推荐——堆堆袜特辑](https://news.dmzj.com/article/73175.html)
> 概要: 曾经在辣妹中流行一时的堆堆袜最近也被当作是复古风格的象征物之一重新出现在画师的创作中
### [剧场版《咒术回战0》首周末票房26亿日元](https://news.dmzj.com/article/73177.html)
> 概要: 在12月24日上映的剧场版动画《咒术回战0》公开了首周末票房。截止到26日，本作共有190万8053人次观看，累计票房达到了26亿9412万8150日元。其中本作公开首日票房为10亿7225万2950日元。
### [为什么你亏了钱，可2021年还是个牛市？](https://www.huxiu.com/article/485091.html)
> 概要: 今年是A股牛市的第三年。虽然大盘指数没怎么涨，但结构性上涨非常明显，赚钱效应依然存在，收益率接近翻倍的公募基金也有多只。对于2021年的行情，海通证券策略分析师荀玉根团队在年初曾提出，牛市不变，波动难......
### [bell-fine《请问您今天要来点兔子吗》纱路哥特萝莉异色版手办](https://news.dmzj.com/article/73181.html)
> 概要: bell-fine根据《请问您今天要来点兔子吗》中的纱路推出了哥特萝莉异色版1/7比例手办。本作再现了纱路身穿黄色基调的哥特萝莉服装，举着带花边的洋伞时的样子。另外还附属兔子Wild Geese，可以和纱路放在一起。
### [《魔法使之夜》剧场版动画公布！月球人提前过年了](https://acg.gamersky.com/news/202112/1448261.shtml)
> 概要: 型月Type-Moon名作《魔法使之夜》于近日宣布将改编为剧场版动画，并放出了先导预告。
### [Ontario bans non-competes and creates right to disconnect from work](https://gowlingwlg.com/en/insights-resources/articles/2021/ontario-legislation-bill-27-non-competes/)
> 概要: Ontario bans non-competes and creates right to disconnect from work
### [岛内明星领居住证定居大陆，台媒又“震动”了](https://new.qq.com/rain/a/20211227A01OH700)
> 概要: 台湾明星“亮证”都已经快成风了！前段时间，台湾明星千百惠在网上亮出了她的大陆身份证，紧接着又有明星做了同样的事情。据环球时报12月26日消息，台湾综艺一姐、著名演员方芳近日宣布自己在大陆成功办理并领到......
### [《光环》系列开发人员：最初没打算做战役剧情模式](https://www.3dmgame.com/news/202112/3831834.html)
> 概要: 最近，《光环》系列的多人游戏模式的开发人员Stefan Sinclair在接受Retro Gamer的采访中表示，《光环》系列的首部作品《光环：战斗进化（Halo: Combat Evolved）》于......
### [视频：“向剧作家致敬2021”何冀平作品展演启动 李东才王斑等莅临助阵](https://video.sina.com.cn/p/ent/2021-12-27/detail-ikyamrmz1442347.d.html)
> 概要: 视频：“向剧作家致敬2021”何冀平作品展演启动 李东才王斑等莅临助阵
### [HyperNeRF](https://hypernerf.github.io/)
> 概要: HyperNeRF
### [《岸边露伴一动不动》新作今晚开播 3个奇妙故事](https://www.3dmgame.com/news/202112/3831837.html)
> 概要: 日本漫画家荒木飞吕彦原作，《JOJO的奇妙冒险》衍生名作《岸边露伴一动不动》新日剧于去年12月贺岁档播出，备受好评，《岸边露伴一动不动》新作日剧今年同样回归，确定将于12月27日今晚开播开播，原版人马......
### [轻小说「魔王使用的最强支配」第2卷封面插图公开](http://acg.178.com/202112/434574825201.html)
> 概要: 轻小说「魔王使用的最强支配」公开了最新卷第2卷的封面插图，本卷将于2022年2月1日正式发售，定价为715日元（含税）。「魔王使用的最强支配」是空埜一樹创作、コユコム负责插画的轻小说作品，讲述了引发魔......
### [蔡徐坤演唱会主办方转经营权 被没收2万罚款16万](https://ent.sina.com.cn/y/yneidi/2021-12-27/doc-ikyakumx6636431.shtml)
> 概要: 新浪娱乐讯 天眼查App显示，近日，永稻星（北京）文化娱乐有限公司因2021年1月21日至2021年7月17日期间，在演出经营活动中不履行应尽义务、转让演出活动经营权，被北京市文化和旅游局没收违法所得......
### [「租借女友」一番赏新商品插画公开](http://acg.178.com/202112/434574920133.html)
> 概要: 一番赏官方公开了「租借女友」新商品「一番くじ 彼女、お借りします 満足度3」的使用插画，由漫画原作者宫岛礼吏绘制。该系列商品预计将于2022年1月22日开始顺次发售。「租借女友」是宫岛礼吏创作的漫画作......
### [There's an ARM Cortex-M4 with Bluetooth inside a Covid test kit](https://twitter.com/Foone/status/1475231857851527169)
> 概要: There's an ARM Cortex-M4 with Bluetooth inside a Covid test kit
### [永辉超市的三重困境](https://www.huxiu.com/article/485162.html)
> 概要: 作者：安静，编辑：贾乐乐，题图来自：视觉中国去年那场激烈的社区团购之战，炮弹最先落在永辉超市的地盘。在过去十几年中，永辉超市靠低价在生鲜零售领域建立了高壁垒，在疫情中，网上买菜常态化，加上社区团购的价......
### [中芯能卖给华为吗？](https://www.huxiu.com/article/485164.html)
> 概要: 本文来自微信公众号：芯谋研究（ID：icwise），作者：顾文军，题图来自：视觉中国近期，有一“专家”建议将中芯国际卖给华为，又是中芯又是华为、如此“大胆”地乱点鸳鸯谱，着实吸引眼球。因是圈外人士，半......
### [Aimer10周年演唱会'night world”LIVE影像片段公开](http://acg.178.com/202112/434577540958.html)
> 概要: 近期，知名动漫歌手Aimer公开了10周年演唱会"night world”的LIVE影像片段。影像收录于B面精选专辑「星の消えた夜に」之中，专辑将于2022年1月26日发售。「night world」......
### [漫画「マーディスト ―死刑囚・風見多鶴―」第一卷宣传PV公开](http://acg.178.com/202112/434577857709.html)
> 概要: 根据半田畔创作的同名轻小说作品改编，ゆとと负责绘制的漫画作品「マーディスト ―死刑囚・風見多鶴―」公开了该作第一卷的宣传PV。漫画第一卷已于今日（12月27日）发售。漫画「マーディスト ―死刑囚・風見......
### [知名声优坂本真绫宣布怀孕 铃村健一明年升级当爸爸](https://acg.gamersky.com/news/202112/1448337.shtml)
> 概要: 12月27日，日本知名声优坂本真绫宣布了怀孕的消息，同时她的丈夫铃村健一也宣布了这个好消息，两人即将在明年迎来第一个孩子。
### [与变异风险词赛跑，阿里探索AI治理网络风险](https://www.tuicool.com/articles/YJRFr2u)
> 概要: 最近，阿里安全一线风控小二可粒发现，在禁售的风险防控库里，有人试图“上新”新品种，不法份子借助在社交媒体上走红的“魔法改运”等说辞，引人入玄学骗局。尽量提前发现风险问题，提早布防是阿里安全风控部门的日......
### [如何减少精神内耗？](https://www.huxiu.com/article/485217.html)
> 概要: 本文来自微信公众号：简单心理（ID：janelee1231），作者：寒冰，责编：kuma，原文标题：《减少精神内耗的7件小事》，题图来自：《穿普拉达的女王》不知道你会不会也经常陷入“精神内耗”——想得......
### [《新蝙蝠侠》中国台湾定档 3月2日上映](https://www.3dmgame.com/news/202112/3831857.html)
> 概要: 华纳兄弟台湾官方INS宣布由马特·里夫斯执导，罗伯特·帕丁森主演的《新蝙蝠侠》将于2022年3月2日在中国台湾省上映。同时官方还发布了定档动态海报。动态海报本片还将会在明年3月4日登陆北美院线，3月1......
### [小说改编国产动画《雪鹰领主》第三季今日开播](https://www.3dmgame.com/news/202112/3831859.html)
> 概要: 由企鹅影视出品，米粒影业制作的同名小说改编动画《雪鹰领主》于今日（12月27日）开播，第三季共26集，每周一10:00更新1集，VIP抢先看1集。腾讯视频独家播出。《雪鹰领主》第三季预告：动画《雪鹰领......
### [张艺兴加入国家话剧院，学业事业两不误，变身“娱乐圈最卷的人”](https://new.qq.com/rain/a/20211227A0590Y00)
> 概要: 古人云“学无止境”，充分说明无论何时，学习的脚步都不能停止，而身为学生，学习更是头等大事，只有自身坚持不停地前进，才能保证不会被甩出队伍。犹记得哈佛图书馆的训言“即使现在，对手也在不停地翻动书页”， ......
### [《咒术回战0》首周末票房突破26亿日元 日本影史第2](https://acg.gamersky.com/news/202112/1448375.shtml)
> 概要: 剧场版动画《咒术回战0》于12月24日在日本上映，今天官方公开了首周末的票房数据，3天票房为26亿9412万8150日元，190万8053人到场观影。
### [吴宗宪称儿子新专辑发布会照常开 会解释事件经过](https://ent.sina.com.cn/s/h/2021-12-27/doc-ikyakumx6690654.shtml)
> 概要: 新浪娱乐讯 据台湾媒体27日报道，吴宗宪儿子鹿希派（吴睿轩）26日凌晨在台北松高路旁抽烟，遭警员盘查发现，鹿希派手中的烟正是大麻而被逮捕。吴宗宪难忍怒气表示29日鹿希派的发片记者会将取消。事隔一日，吴......
### [快手、美团达成互联互通合作：美团将在快手上线小程序](https://www.tuicool.com/articles/RBrQR3A)
> 概要: IT之家 12 月 27 日消息，据北京商报报道，快手今日下午举行了首届生态开放大会，快手和美团达成互联互通战略合作。美团将在快手开放平台上线美团小程序，为美团商家提供套餐、代金券、预订等商品展示、线......
### [神户大桥圣诞夜气氛太契合《Fate》 来场圣杯战争吧](https://acg.gamersky.com/news/202112/1448430.shtml)
> 概要: 因为天气的原因，12月25日晚间的神户大桥的气氛十分非常契合《Fate》的感觉，太适合魔术师和从者来一场圣杯战争了。
### [理念太超前 任天堂曾想过为GBC开发互联网配件](https://www.3dmgame.com/news/202112/3831875.html)
> 概要: 任天堂GBC发行于1998年，比最早、最简陋的智能手机上市早了几年。那时候，互联网还是一个相当新的事物，携带一个能够给人发邮件、搜索网页、发送照片和传输流媒体视频的设备的想法是很多年以后才出现的。但是......
### [前《英雄联盟》战队 FPX 上单选手 GimGoon 宣布退役，曾获 S9 世界冠军](https://www.ithome.com/0/595/087.htm)
> 概要: IT之家12 月 27 日消息，前《英雄联盟》战队 FPX 上单选手GimGoon（金韩泉）今日宣布退役，他在微博发布了这一消息，表示“我的职业生涯没有遗憾”。大家好我是 GimGoon，在思考了很久......
### [《继母与女儿的蓝调SP》公开剧照 绫濑遥婚纱亮相](https://ent.sina.com.cn/2021-12-27/doc-ikyamrmz1533602.shtml)
> 概要: 新浪娱乐讯 据日本媒体Modelpress报道，绫濑遥主演新春特别剧《继母与女儿的蓝调 2022年谨贺新年SP》公开日剧版未能实现的家族合影剧照，女主宫本亚希子穿婚纱亮相，上白石萌歌饰演宫本美雪抱着爸......
### [小米 Watch S1 预热：商务人士的高端智能手表，头层小牛皮真皮表带](https://www.ithome.com/0/595/100.htm)
> 概要: 感谢IT之家网友肖战割割的线索投递！IT之家12 月 27 日消息，全新的小米手表系列 Xiaomi Watch S1 将与小米 12 系列一同在 12 月 28 日发布。今日，小米官方预热称，该手表......
### [张近东发全员信：苏宁债务问题已取得阶段性企稳，易购的经营工作正有序恢复](https://www.ithome.com/0/595/118.htm)
> 概要: IT之家12 月 27 日消息，据凤凰网科技报道，张近东以苏宁集团董事长名义发布《苏宁 31 周年致全体员工的一封信》，称“目前在政府的支持和帮助下，在我们持续的努力下，集团的债务问题已取得阶段性企稳......
### [自来也用生命换来的情报，最后真的有用吗？自来也的牺牲不值得](https://new.qq.com/omn/20211226/20211226A05BCX00.html)
> 概要: 自来也的牺牲值得吗？他用生命换来的情报，最后真的有用吗？在动漫火影忍者中，你最希望哪个人复活，我想很多人都有一个共同的答案，那就是自来也，自来也作为传说中的三忍之一，他同时也是主人公鸣人的恩师，对于鸣......
### [心上一画｜质感绝了！3D建模强到这种程度，不愧是A站年度霸榜大神！](https://new.qq.com/omn/20211227/20211227A099OS00.html)
> 概要: 2021/12/27/周一致 十二月（Vol.239）希望做你的心上画。                                                                ......
### [火影忍者催泪名场面，你能坚持到第几个？哪一个瞬间让你最感动？](https://new.qq.com/omn/20211227/20211227A094GD00.html)
> 概要: 在动漫火影忍者中，忍者的平均寿命其实是非常低的，整体来说，忍者世界其实是一个悲惨的艺术世界，而在动漫之中也有很多让人流泪的催泪名场面，下面小编就来给大家盘点一下，火影忍者中著名的催泪名场面，看看你的眼......
### [成多数网友年度歌手 李荣浩回应称继续写歌不辜负](https://ent.sina.com.cn/y/yneidi/2021-12-27/doc-ikyakumx6723691.shtml)
> 概要: 新浪娱乐讯 27日，音乐软件年度报告出炉引发了网友们的热议，不少网友的年度歌手都是李荣浩。对此，李荣浩发文称“会继续写更多扎扎实实的真实生活和人生体会，不辜负那么多人的年度歌手”。(责编：Mia)......
### [A stochastic method to generate the Sierpinski triangle](https://github.com/ajnirp/stochastic_sierpinski)
> 概要: A stochastic method to generate the Sierpinski triangle
### [快手发布“聚力计划”：将投入千亿流量、5 亿现金等扶持经营伙伴](https://www.ithome.com/0/595/132.htm)
> 概要: IT之家12 月 27 日消息，今日，在快手生态开放大会上，快手推出“聚力计划”，将投入千亿流量、5 亿现金等扶持经营伙伴。据介绍，在达人层面，快手将在一年内通过佣金翻倍等计划，扶持百万优质达人，鼓励......
### [智能座舱情报局｜产品力怪兽——福特 EVOS](https://www.tuicool.com/articles/Arqqmir)
> 概要: 大家好，这里是 GeekCar 智能座舱情报局第三季的第九期，我是 Mr.Yu。在上期，我们对威马的第三款纯电 SUV，威马 W6 的智能座舱进行了评测。简单实用的应用生态、SOA 自定义场景编程与「......
### [2021年印度电商之战，亚马逊与Flipkart谁赢了？](https://www.tuicool.com/articles/fIJrAvi)
> 概要: Inc42 Plus发布的一份报告显示，截止2026年，印度电商市场将突破2000亿美元，年复合增长率达到19%。仅2021年的节日期间，电商GMV猛增23%，达到92亿美元，相较于前一年的74亿美元......
### [《雪中悍刀行》收视居高不下，8位老戏骨功不可没，说“变脸”就变脸](https://new.qq.com/rain/a/20211227A0AZLT00)
> 概要: 一部古装武侠玄幻剧，让我们一家三口看上了瘾。每天追着看。一到更新时间，就会放下手中活计，齐齐坐到电视机前。这部剧就是由宋晓飞执导，王倦编剧，根据烽火戏诸侯同名小说改编的《雪中悍刀行》。        ......
### [粉色才是最萌的属性？动漫里瞩目的粉毛角色，可爱值加倍](https://new.qq.com/omn/20211227/20211227A0B59U00.html)
> 概要: 粉毛是动漫里人气比较高且应用非常广泛的人物设定，这个设定与相称的外貌属性相结合，可以营造出可爱的萌系角色；与不相称的外貌属性进行结合，也可以营造出让人眼前一亮的反差效果。动漫里人气粉毛角色榜单公布，在......
### [《快本》被彻底停播！何炅一人重开新节目，维嘉杜海涛至今停工待业](https://new.qq.com/rain/a/20211227A093MV00)
> 概要: 原本打算升级整改的《快乐大本营》已经彻底停播了！            此前网上就有消息传出，湖南台将推出全新综艺《你好星期六》，以此代替《快乐大本营》，这档综艺由何炅一人撑起，还邀请了蔡少芬、王鹤棣......
### [北京连续30年江苏连续21年人口出生率低于1%，原因何在？](https://finance.sina.com.cn/china/dfjj/2021-12-27/doc-ikyakumx6742465.shtml)
> 概要: 原标题：北京连续30年江苏连续21年人口出生率低于1%，原因何在？ 在疫情影响下，我国出生人口数量和出生率也大幅走低。近日发布的《中国统计年鉴2021》显示...
### [央行：2022年稳健的货币政策要灵活适度 精准加大重点领域金融支持力度](https://finance.sina.com.cn/china/gncj/2021-12-27/doc-ikyakumx6743205.shtml)
> 概要: 原标题：2022年中国人民银行工作会议召开 2022年中国人民银行工作会议12月27日上午以视频形式召开。会议以习近平新时代中国特色社会主义思想为指导...
### [2022年中国人民银行工作会议召开](https://finance.sina.com.cn/china/gncj/2021-12-27/doc-ikyakumx6743102.shtml)
> 概要: 原标题：2022年中国人民银行工作会议召开 2022年中国人民银行工作会议12月27日上午以视频形式召开。会议以习近平新时代中国特色社会主义思想为指导...
### [央行：稳妥实施好房地产金融审慎管理制度 促进房地产业良性循环和健康发展](https://finance.sina.com.cn/china/gncj/2021-12-27/doc-ikyakumx6743769.shtml)
> 概要: 原标题：2022年中国人民银行工作会议召开 2022年中国人民银行工作会议12月27日上午以视频形式召开。会议以习近平新时代中国特色社会主义思想为指导...
### [一位医生在西安的故事：被隔离后，我在小区卖起了菜](https://finance.sina.com.cn/china/dfjj/2021-12-27/doc-ikyamrmz1571274.shtml)
> 概要: 原标题：一位医生在西安的故事：被隔离后，我在小区卖起了菜 还有1个小时就是12月24日，已被隔离两天的陈医生，开启了她职业转型——从一名中医到菜贩。
### [最新！6人在废弃金矿内遇难，原因曝光](https://finance.sina.com.cn/china/gncj/2021-12-27/doc-ikyakumx6745225.shtml)
> 概要: 原标题：最新！6人在废弃金矿内遇难，原因曝光 山西省绛县里册峪一带发生的6名人员失踪事件引发关注。 据央视新闻报道，今天早晨3点50分许，山西省绛县公安局接到报警称...
# 小说
### [仙武永恒](http://book.zongheng.com/book/844890.html)
> 作者：星空尽头

> 标签：武侠仙侠

> 简介：三千大界，浩瀚罗天。有修士可以焚天煮海，长生不死，与天地同寿；有武修者可以拳破虚空，捉拿日月，以力证道；有神秘莫测的种族天赋异禀，血脉传承，慑服诸天；有体积庞大的妖兽遨游星空，吞噬万物，所向披靡；当走到命运的路口，没有人能够逃脱，这一场审判者与宿命者的较量.....直到时间的尽头才发现这是棋子与棋子之间的博弈，胜利者将改写命运。一个根骨极差的小子；一个被驱逐山门的废柴；一个被未婚妻羞辱的少年；大风起于风萍之末，命运起于抉择的路口，少年从卑微中崛起！

> 章节末：第二百二十一章：封印碎（大结局）

> 状态：完本
# 论文
### [Probabilistic Tensor Decomposition of Neural Population Spiking Activity | Papers With Code](https://paperswithcode.com/paper/probabilistic-tensor-decomposition-of-neural)
> 日期：NeurIPS 2021

> 标签：None

> 代码：None

> 描述：The firing of neural populations is coordinated across cells, in time, and across experimentalconditions or repeated experimental trials; and so a full understanding of the computationalsignificance of neural responses must be based on a separation of these different contributions tostructured activity.Tensor decomposition is an approach to untangling the influence of multiple factors in data that iscommon in many fields. However, despite some recent interest in neuroscience, wider applicabilityof the approach is hampered by the lack of a full probabilistic treatment allowing principledinference of a decomposition from non-Gaussian spike-count data.Here, we extend the Pólya-Gamma (PG) augmentation, previously used in sampling-based Bayesianinference, to implement scalable variational inference in non-conjugate spike-count models.Using this new approach, we develop techniques related to automatic relevance determination to inferthe most appropriate tensor rank, as well as to incorporate priors based on known brain anatomy suchas the segregation of cell response properties by brain area.We apply the model to neural recordings taken under conditions of visual-vestibular sensoryintegration, revealing how the encoding of self- and visual-motion signals is modulated by thesensory information available to the animal.
### [Bridging the Gap between Label- and Reference-based Synthesis in Multi-attribute Image-to-Image Translation | Papers With Code](https://paperswithcode.com/paper/bridging-the-gap-between-label-and-reference-1)
> 日期：ICCV 2021

> 标签：None

> 代码：None

> 描述：The image-to-image translation (I2IT) model takes a target label or a reference image as the input, and changes a source into the specified target domain. The two types of synthesis, either label- or reference-based, have substantial differences.
