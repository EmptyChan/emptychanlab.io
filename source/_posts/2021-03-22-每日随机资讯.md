---
title: 2021-03-22-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.JouxFog_EN-CN2462005370_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-03-22 20:45:07
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.JouxFog_EN-CN2462005370_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [The Infinite Drum Machine](https://experiments.withgoogle.com/ai/drum-machine/view)
> 概要: The Infinite Drum Machine
### [撬开贝壳“藏着”的秘密](https://www.huxiu.com/article/416432.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年3月15日（美东时间），贝壳（NYSE：BEKE）发布2020年Q4及全年未经审计业绩。财报显示，2020年营收、总成交额（GTV）分别达到704.8亿、......
### [P站美图推荐——人造人特辑](https://news.dmzj1.com/article/70402.html)
> 概要: 机械，男人的浪漫。美女，男人的浪漫。人造人，浪漫max！
### [Updating a 50 terabyte PostgreSQL database (2018)](https://medium.com/adyen/updating-a-50-terabyte-postgresql-database-f64384b799e7)
> 概要: Updating a 50 terabyte PostgreSQL database (2018)
### [北京环球影城停车楼竣工 提供近7000个车位](https://ent.sina.com.cn/m/c/2021-03-22/doc-ikkntiam6056325.shtml)
> 概要: 本报讯（记者  李泽伟）北京青年报记者日前从通州区了解到，北京环球影城的停车楼目前已经竣工，可为游客提供近7000个停车位。停车楼旁还修建了一处可以容纳2200余个车位的地面停车场。环球影城开园后，将......
### [从宏观经济大势，看未来投资方向](https://www.huxiu.com/article/416482.html)
> 概要: 本文来自微信公众号：巴伦周刊（ID：barronschina），作者：张明（中国社科院金融研究所副所长、国家金融与发展实验室副主任），编辑：彭韧，本文为张明的新著《宏观中国：经济增长、周期波动与资产配......
### [《大宋宫词》开播 前朝与后宫的嫁接戏两头不讨好](https://ent.sina.com.cn/v/m/2021-03-22/doc-ikkntiam6064237.shtml)
> 概要: 刘雨涵　　蛰伏了两年时间，20日得以上线的《大宋宫词》首播8集，却出师不利。从剧情、剪辑、台词到演员、CP感等等，被网友们吐槽了一个遍。该剧豆瓣开分时仅有6.1分，经过一夜时间，便直坠至5.2分。《大......
### [动画「无职转生」第一季度完结绘公开](http://acg.178.com//202103/410377711370.html)
> 概要: 电视动画「无职转生~到了异世界就拿出真本事~」第一季现已完结，官方公开了动画第一季度完结绘图。电视动画「无职转生～到了异世界就拿出真本事～」改编自不讲理不求人著作、白鹰负责插画的同名轻小说作品，讲述3......
### [田中将贺绘制的「未闻花名」十周年官方纪念贺图公开](http://acg.178.com//202103/410378075108.html)
> 概要: 动画「未闻花名（我们仍未知道那天所看见的花的名字。）」即将迎来十周年纪念，动画人设兼作画监督田中将贺公开了自己绘制的十周年纪念贺图。「我们仍未知道那天所看见的花的名字。」由日本动画公司A-1 Pict......
### [「摇曳露营△」特别活动4月11日举办](http://acg.178.com//202103/410378300779.html)
> 概要: 动画「摇曳露营△」的特别活动「STAY△TENT」确定将于4月11日在线上举办，目前已经开始售票。据了解，将会在此次活动中出席的声优包括：花守ゆみり、東山奈央、原紗友里、豊崎愛生、高橋李依、黒沢ともよ......
### [全员危机？《灵笼》终章终极大刀即将落下？](https://acg.gamersky.com/news/202103/1372324.shtml)
> 概要: 动画《灵笼》自2019年播出后，因其颇具特色的世界观、悬念层出的剧情、成群结对的噬极兽、炫酷高燃的打斗收获一众好评。
### [《星期一的丰满》：送好身材妹子回家 被误会大了](https://acg.gamersky.com/news/202103/1372348.shtml)
> 概要: 《星期一的丰满》又带来了连续剧情，之前那个派发巧克力的妹子，终于和男同学有了好结果。然而在她和男生一起回家的时候被奶奶看到，好像产生了严重误会。
### [三星堆文明为何如此重要？](https://www.huxiu.com/article/416529.html)
> 概要: 本文来自微信公众号：国家人文历史（ID：gjrwls），作者：李思达，原文标题：《三星堆“上新了”：有个性的三星堆文明到底从何而来》，头图来源：IC photo四川广汉有一个地方，一经出世便引起世界瞩......
### [鸿蒙手机更新计划曝光 4月支持Mate X2/40/P40等](https://www.3dmgame.com/news/202103/3811001.html)
> 概要: 华为在去年 12 月 16 日举行 HarmonyOS 2.0 手机开发者 Beta 活动。现场正式发布了 HarmonyOS 2.0 手机开发者 Beta 版本。同时，HarmonyOS 2.0 手......
### [2021年春番动画番表！3月第4周新闻汇总](https://news.dmzj1.com/article/70404.html)
> 概要: 这次准备的是2021年春番动画番表，漫画大奖2021结果，日本消防司令補因强行要求同事玩手游被停职，Bushiroad购入FrontWing的部分股份，以及惯例的新作相关消息。
### [Counterfeit Mitutoyo digital calipers (2019)](https://canadianhobbymetalworkers.com/threads/counterfeit-mitutoyo-digital-calipers.1182/)
> 概要: Counterfeit Mitutoyo digital calipers (2019)
### [段奥娟欲与缔壹娱乐解除合约 已诉讼至法院审理](https://ent.sina.com.cn/y/yneidi/2021-03-22/doc-ikkntiam6156347.shtml)
> 概要: 新浪娱乐讯 3月22日早，北京缔壹娱乐经纪有限公司发表声明表示自己是段奥娟全球范围内独家全权经纪公司，拥有段奥娟姓名、肖像、影响、姓名、形象及声音等专有使用权，任何单位和个人不可以擅自与段奥娟及公司以......
### [「摇曳露营△」各务原抚子1/7手办登场](http://acg.178.com//202103/410394273098.html)
> 概要: 电视动画「摇曳露营△」中的主角之一各务原抚子1/7标准比例手办登场。该手办以动画第1季第1集「富士山与咖喱面」中的场景为基础制作，附带篝火及专用底座。商品全高约200mm，售价1097元人民币，将于2......
### [看韩剧时，主角都喜欢吃泡面是为什么？因为好吃？其实另有隐情](https://new.qq.com/omn/20210322/20210322A06KEV00.html)
> 概要: 在韩国，泡面被称为“拉面”，经常看韩剧或者韩综的朋友，一定对这样的场景很熟悉，那就是一堆艺人对着一盆泡面垂涎三尺，大呼“吗西索”（音译：好吃）！            相信有许多的小伙伴都跟我一样喜欢......
### [古天乐发文纪念并致敬《寻秦记》作者黄易](https://ent.sina.com.cn/m/c/2021-03-22/doc-ikknscsi9373843.shtml)
> 概要: 新浪娱乐讯 3月22日，古天乐在微博发文纪念并致敬《寻秦记》作者黄易，并解释了选择将电影《寻秦记》预告片在三月份正式公布的原因之一，就是要向三月十五日生辰的黄易老师致敬。　　古天乐表示：“作为《寻秦记......
### [1、2代的故事！生化危机新真人电影定名：欢迎来到浣熊市](https://news.dmzj1.com/article/70409.html)
> 概要: 《生化危机》的新真人电影宣布了正式名称为《生化危机：欢迎来到浣熊市（Resident Evil: Welcome to Raccoon City）》。
### [官方旗舰店爆料：“轻薄大作”小米笔记本 Pro 15 确认亮相 3 月 29 日发布会](https://www.ithome.com/0/541/462.htm)
> 概要: IT之家 3 月 22 日消息 小米官方今天宣布，小米春季新品发布会将于 3 月 29 日 19 点 30 分举行，主题是 “生生不息”，外界普遍猜测这次小米有望发布小米 11 Pro、小米 11 P......
### [5 种基本的产品框架，总有一款适合你](https://www.tuicool.com/articles/YfquEvM)
> 概要: 神译局是36氪旗下编译团队，关注科技、商业、职场、生活等领域，重点介绍国外的新技术、新观点、新风向。编者按：框架是很有用的工具，它可以帮助加快思考、弥补偷懒、激发创意，甚至可以避免心理偏见。Waze的......
### [能链完成3亿美元战略融资，由贝恩资本（Bain Capital）领投](https://www.tuicool.com/articles/mQ7VFfa)
> 概要: 【猎云网（微信：）北京】3月22日报道3月22日，国内最大的一站式能源服务平台能链集团宣布完成新一轮2亿美元战略融资。本轮融资由贝恩资本（Bain Capital）领投，老股东愉悦资本继续跟投。综合今......
### [小米 “K9”新机入网照公布：侧边指纹识别，直屏设计](https://www.ithome.com/0/541/475.htm)
> 概要: IT之家3月22日消息 根据IT之家此前报道，小米的一款代号为 “M2101K9C”手机近日通过了 3C 认证。近日这款手机通过工信部入网认证，实拍照片得到曝光。从图片可以看出，手机背面采用与小米 1......
### [赵丽颖晒近照！罕见穿少数民族服装越长越嫩，笑容甜美还撞脸谢娜](https://new.qq.com/omn/20210322/20210322A08QUV00.html)
> 概要: 赵丽颖绝对可以说是娱乐圈中公认的劳模，去年连拍了好几部电视剧，刚跟老公冯绍峰度假归来，又迅速进组《理想照耀中国》，真的是太拼了，3月22号，她还特地在自己的社交平台上跟粉丝们分享了一张最新的剧拍花絮......
### [“许知远吐槽薇娅”？从经济学角度而言，你真的理解“直播带货”吗？](https://www.tuicool.com/articles/m6Nfii6)
> 概要: 他被“吐槽”最多的槽点是：“人字拖卖全网最高价”“我不理解，为什么他的拖鞋要卖298，但我理解，为什么卖不出去。”这一集，主咖是薇娅。直播带货一姐，一年卖货300多亿。许知远高价卖拖鞋被吐槽的一塌糊涂......
### [《活了100天的鳄鱼》公布宣传PV 画风可爱轻松治愈](https://acg.gamersky.com/news/202103/1372519.shtml)
> 概要: 动画电影《活了100天的鳄鱼》公布宣传PV，5月28日在日本地区上映。
### [又想白要腊肠，又想赊账，杨子在菜场遭围堵，一点不怕难堪](https://new.qq.com/omn/20210322/20210322A08VFK00.html)
> 概要: 杨子和黄圣依这对夫妻，遭受过太多的网络暴力，杨子之前有老婆有孩子，黄圣依的介入俨然成了“小三”，也因此被骂很多年，当时吃瓜群众都觉得圣依跟着他完全是为了钱和资源。直到这几年屡次上综艺，黄圣依才算打了一......
### [日票房跌至35万，沈腾来救场都没用！2021年评分最低的电影](https://new.qq.com/omn/20210322/20210322A098BG00.html)
> 概要: 近段时间的内地电影市场，正处于一个有点尴尬的时间段，具有竞争力的新片基本上没有，依旧是春节档的老片在撑门面，唯一一部票房说得过去的，还是重映的《阿凡达》。            在经历了春节档的激烈争......
### [中国电信 2 月 5G 用户数净增 620 万户，累计破亿](https://www.ithome.com/0/541/493.htm)
> 概要: 今日，中国电信公布了 2021 年 2 月运营数据。当月，中国电信移动用户数净增 25 万户，移动用户数累计 3.5280 亿户。当月 5G 套餐用户净增 620 万户，5G 套餐用户数累计 1.03......
### [推特漫画：两女高中生进入厕所单间调查校园怪谈之后](https://news.dmzj1.com/article/70412.html)
> 概要: 在日本校园怪谈中，有很多都和厕所有一定关系，以这个主题创作的故事也有很多。近日，推特网友“kakinuma31”也根据这个主题投稿了短篇漫画《调查幽灵的声音的故事》。
### [《EVA新剧场版：终》上映2周票房突破49亿日元](https://acg.gamersky.com/news/202103/1372502.shtml)
> 概要: 《新世纪福音战士新剧场版：终》目前已经上映两周时间，官方公开了本作到目前的票房成绩，票房为49亿3499万6800日元，一共有322万2873人到场观看。
### [视频：杨幂为参加漫画腰挑战道歉 无专业指导可能对身体有害](https://video.sina.com.cn/p/ent/2021-03-22/detail-ikkntiam6298413.d.html)
> 概要: 视频：杨幂为参加漫画腰挑战道歉 无专业指导可能对身体有害
### [云集2020年Q4财报：单季GMV达117亿元，商城业务同比增长超90%](https://www.tuicool.com/articles/zQVJriy)
> 概要: 【猎云网（微信：）北京】3月22日报道3月22日，会员电商平台云集发布了截至2020年12月31日的第四季度及2020财年未经审计的财务业绩。财报数据显示，2020年第四季度，云集总营收环比增长超过2......
### [“I’ve made a Cursed USB-C 2.0 device”](https://twitter.com/mifune/status/1373564866443759617)
> 概要: “I’ve made a Cursed USB-C 2.0 device”
### [西双版纳：决定在线上组织举办“2021云过泼水节”](https://finance.sina.com.cn/china/dfjj/2021-03-22/doc-ikkntiam6347786.shtml)
> 概要: “西双版纳发布”微信公众号3月22日发布《关于举办西双版纳“2021云过泼水节”系列活动的公告》。 公告称，为持续全面落实新冠肺炎疫情防控“外防输入、内防反弹”的要求...
### [部委高官齐聚“钓鱼台”释放五大重磅信号！](https://finance.sina.com.cn/china/2021-03-22/doc-ikknscsi9510251.shtml)
> 概要: 来源：国是直通车 “十四五”是中国开启全面建设现代化新征程、向第二个百年奋斗目标迈进的关键阶段，这一时期能否开好局对我国未来社会经济发展至关重要。
### [英伟达将推新的矿卡 迄今为止英伟达最强大的GPU](https://www.3dmgame.com/news/202103/3811021.html)
> 概要: 英伟达可能会推出一款新的CMP HX系列矿卡，将采用Ampere GA100 GPU。这是迄今为止英伟达最强大的GPU，专门用于AI和HPC方面的工作，采用台积电7nm工艺制造。这则消息来源于推特用户......
### [三星发布多款显示器：24 英寸到 32 英寸，全部支持 HDR10](https://www.ithome.com/0/541/511.htm)
> 概要: IT之家3月22日消息 据外媒 sammobile 报道，三星今日发布了S6、S7、S8 三个系列共十几款显示器，屏幕尺寸从 24 英寸到 32 英寸不等，所有显示器都可显示十亿色，且都支持 HDR1......
### [陕西一男子称年会被公司高管烟头烫脸 高管：疼才长记性](https://finance.sina.com.cn/china/2021-03-22/doc-ikkntiam6355766.shtml)
> 概要: 原标题：陕西一男子称年会被公司高管烟头烫脸！高管：疼才长记性… 来源：华商报 “公司年会聚餐时被高管用烟头故意烫脸，这是一起极其恶劣的职场暴力...
### [《生化危机 Re:Verse》BETA公测4月8日开始](https://www.3dmgame.com/news/202103/3811022.html)
> 概要: 《生化危机 Re：Verse》将于北京时间4月8日下午2点开启BETA公测，北京时间4月11日下午2点结束，PS4，Xbox One和PC Steam玩家都可以参与。玩家可以于4月6日早上8点下载BE......
### [LPR连续按兵不动、易纲表态利率水平适中 政策利率如何走](https://finance.sina.com.cn/jjxw/2021-03-22/doc-ikknscsi9516182.shtml)
> 概要: 最新出炉的LPR利率以及上周末央行行长易纲“用好正常货币政策空间”、 “利率水平适中”的表述，给市场带来了今年整体利率水平走向的新信号。
### [十三届全国人大常委会第二十七次会议3月29日至30日在京举行](https://finance.sina.com.cn/china/gncj/2021-03-22/doc-ikknscsi9519457.shtml)
> 概要: 栗战书主持召开十三届全国人大常委会第八十八次委员长会议决定十三届全国人大常委会第二十七次会议3月29日至30日在京举行
### [内蒙古粮食高质量发展指数发布 将探索农产品电商化](https://finance.sina.com.cn/china/dfjj/2021-03-22/doc-ikknscsi9517975.shtml)
> 概要: 新京报讯（记者 欧阳晓娟）3月22日，内蒙古粮食高质量发展指数在京发布，内蒙古粮食高质量发展2019年指数达到122.97点，较基期2015年上升22.97%。
### [3DM轻松一刻第485期 你可知道从天而降的棍法么](https://www.3dmgame.com/bagua/4427.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！右腿也是这么伤的今晚煲仔饭手慢无慢手无在崩溃的边缘，......
### [宅男女神伊织萌美图 谁不喜欢爱打游戏的妹子？](https://www.3dmgame.com/bagua/4428.html)
> 概要: 日本知名Coser伊织萌小姐姐最近沉迷于游戏直播，特别喜欢玩《Apex英雄》。这么一个爱玩游戏的妹子谁不喜欢呢？一起来看看她近期的美图吧......
### [周迅、徐静蕾搞不定的黄觉，娶小13岁网友，从不婚浪子变好男人](https://new.qq.com/omn/20210322/20210322A0CVGL00.html)
> 概要: 黄觉私底下这么没有明星包袱？他和马伯骞在录制一档综艺《万里走单骑》。            两人录完节目不仅一起健身，还玩起了互踩鞋子的游戏。            评论区网友纷纷评论，没想到黄觉居然......
# 小说
### [射雕之狂风快剑](http://book.zongheng.com/book/298567.html)
> 作者：猫眼镜

> 标签：竞技同人

> 简介：穿越到了射雕的世界，成了黄河帮的喽啰。我可不想做黄河第五鬼不如，一起颠覆了这射雕的世界吧！

> 章节末：终章

> 状态：完本
# 论文
### [Precise Multi-Neuron Abstractions for Neural Network CertificationEdit social preview](https://paperswithcode.com/paper/precise-multi-neuron-abstractions-for-neural)
> 日期：5 Mar 2021

> 标签：None

> 代码：https://github.com/eth-sri/eran

> 描述：Formal verification of neural networks is critical for their safe adoption in real-world applications. However, designing a verifier which can handle realistic networks in a precise manner remains an open and difficult challenge.
### [LayoutLMv2: Multi-modal Pre-training for Visually-Rich Document Understanding](https://paperswithcode.com/paper/layoutlmv2-multi-modal-pre-training-for)
> 日期：29 Dec 2020

> 标签：DOCUMENT IMAGE CLASSIFICATION

> 代码：https://github.com/microsoft/unilm

> 描述：Pre-training of text and layout has proved effective in a variety of visually-rich document understanding tasks due to its effective model architecture and the advantage of large-scale unlabeled scanned/digital-born documents. In this paper, we present \textbf{LayoutLMv2} by pre-training text, layout and image in a multi-modal framework, where new model architectures and pre-training tasks are leveraged.
