---
title: 2023-04-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.QingMing2023_ZH-CN6951199028_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-05 21:58:46
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.QingMing2023_ZH-CN6951199028_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [从供需角度，洞察智能学习产品的策略和机会点](https://www.woshipm.com/pd/5797871.html)
> 概要: “双减”政策的影响和居家学习模式的兴起，使得在家能独立完成辅助学习的产品受到家长和学生的青睐。本文作者对教育领域的智能学习产品的供给和需求进行了分析，洞察智能学习产品可能得产品策略和机会点，一起带来看......
### [从订单本质谈谈订单的设计要点](https://www.woshipm.com/pd/5798286.html)
> 概要: 互联网出现之前“订单”就已经出现，它承载着线下交易的整套采购信息。交易离不开“人货场”，如今互联网作为一个重要的交易“场”所，自然将“订单”的概念带到了线上。那么线上订单的实现要点有哪些？一起来看一下......
### [产品体验洞察：AI时代，产品经理的剩余价值](https://www.woshipm.com/user-research/5797015.html)
> 概要: AI 时代，产品经理可以利用 AI 的能力进行处理、分析和优化数据，然而AI无法取代的是产品经理对体验洞察的能力。所以，体验洞察能力对产品经理是非常重要的。那么，怎么获得产品体验洞察呢？一起来看一下吧......
### [《星球大战绝地：幸存者》主线或轻松超20小时](https://www.3dmgame.com/news/202304/3866380.html)
> 概要: 近日《星球大战绝地：幸存者》设计总监Jeff Magers接受了外媒WCCF的采访，当被问到这款游戏通关时长时，Magers不太愿意讲出具体的数字，但他表示《幸存者》将比2019年的《星球大战绝地：陨......
### [王琳凯方回应被抄袭 鹭卓道歉并晒曲谱否认抄袭](https://ent.sina.com.cn/music/zy/2023-04-05/doc-imyphqhq9502255.shtml)
> 概要: 新浪娱乐讯 近日，《种地吧》嘉宾鹭卓的歌曲《不放手》疑似抄袭小鬼王琳凯单曲《最后的外卖》引发争议。4日晚，小鬼经纪公司果然娱乐发布声明回应此事，表示“小鬼-王琳凯单曲《最后的外卖》demo 最早于 2......
### [美国就业市场显现降温迹象，交易员押注美联储今夏降息](https://www.yicai.com/news/101722016.html)
> 概要: 美国2月份职位空缺降至2021年5月以来最低水平。
### [36氪专访 | 云鲸张峻彬：「死磕创新」是赞誉也是重负](https://36kr.com/p/2201801876253575)
> 概要: 36氪专访 | 云鲸张峻彬：「死磕创新」是赞誉也是重负-36氪
### [韩国女艺人金赛纶酒驾被判处2000万韩元罚款](https://ent.sina.com.cn/s/j/2023-04-05/doc-imyphuqp8075535.shtml)
> 概要: 新浪娱乐讯 4月5日上午，首尔中央地方法院开庭对金赛纶酒驾案做出判决，判处了金赛纶2000万韩元罚款。　　金赛纶去年5月18在首尔江南一带驾车接连与护栏，树木以及配电箱相撞，导致街区停电，红绿灯停摆......
### [成立近500家数科类公司，央企为何纷纷入局这个赛道？](https://www.yicai.com/news/101722101.html)
> 概要: 央企布局数字经济，有资金和资源的优势。但数字化不仅是技术更新，而且是经营理念、战略、组织、运营等全方位的变革，这需要进一步破除体制机制的阻碍。
### [求购Open AI老股；转让持有Space X、Neuralink、Shein的专项基金份额｜资情留言板第88期](https://36kr.com/p/2201946256796549)
> 概要: 求购Open AI老股；转让持有Space X、Neuralink、Shein的专项基金份额｜资情留言板第88期-36氪
### [周鸿祎离婚，360的董秘大人为啥如此激动？](https://www.huxiu.com/article/1060123.html)
> 概要: 本文来自微信公众号：花儿街参考（ID：zaraghost），原标题《360的董秘大人，仿佛睡在周鸿祎枕畔》，作者：林默，题图来自：《唐伯虎点秋香》1在周鸿祎与胡欢宣布离婚的那一天，谁是无法控制他寄几的......
### [独家｜虎头局上海门店大量停业：总部办公点人去楼空](https://www.yicai.com/news/101722168.html)
> 概要: 虎头局倒闭的消息或许并不是空穴来风。
### [5年万店、70亿+收入，预制菜大鳄「锅圈」长成记丨IPO观察](https://36kr.com/p/2202021241367685)
> 概要: 5年万店、70亿+收入，预制菜大鳄「锅圈」长成记丨IPO观察-36氪
### [五粮液亮相博鳌亚洲论坛，助力各方凝聚和美共识](http://www.investorscn.com/2023/04/05/106676/)
> 概要: 南海之滨，春暖花开，随着“万泉河水清又清”的歌声再次唱响，一年一度的“博鳌之约”如期而至。3月28日至31日，全球政界领袖、商界翘楚、学界精英齐聚博鳌亚洲论坛，激荡博鳌智慧，提供博鳌方案，为国际团结合......
### [极萌Jmoon惊喜连连！极速变美科技不断升级](http://www.investorscn.com/2023/04/05/106677/)
> 概要: 近日，从有关人士处获悉，新锐美容美体仪器品牌极萌Jmoon计划将于近期惊喜献礼给大家。据悉，极萌Jmoon深研极速变美科技，此消息一出，引发了业界的诸多猜测......
### [爱游戏体育携手罗马俱乐部并肩向冠军发起冲击](http://www.investorscn.com/2023/04/05/106678/)
> 概要: 欧联杯八强即将拉开帷幕，作为上届欧协杯冠军的罗马俱乐部备受关注，这支球队在爱游戏体育成为合作伙伴以来，成绩一路攀升，如今的罗马俱乐部正朝着欧联杯冠军发起有力冲击......
### [王琳凯经纪公司发律师声明 将对鹭卓提起民事诉讼](https://ent.sina.com.cn/y/yneidi/2023-04-05/doc-imypifei7831776.shtml)
> 概要: 新浪娱乐讯 4月5日，小鬼王琳凯经纪公司果然天空娱乐发布律师声明，就鹭卓《不放手》涉嫌抄袭王琳凯《最后的外卖》将正式提起民事诉讼。声明还要求所有涉侵权主体立即停止对涉侵权作品的传播、扩散和转载。(责编......
### [玩法元素大融合 回合制战斗新作《太空监狱》公布](https://www.3dmgame.com/news/202304/3866394.html)
> 概要: 独立游戏开发商Wooden Alien近日宣布，旗下首款作品《太空监狱》（Space Prison）即将登陆PC、PS5和Xbox Series X/S平台。本作是一款回合制生存战斗游戏，厂商还公布了......
### [热搜第一！央企员工怒怼领导！多方回应，省总工会介入…](https://finance.sina.com.cn/china/gncj/2023-04-05/doc-imypifef2625958.shtml)
> 概要: 中国基金报 晨曦 就在放假前夕，“中国电科员工因清明节加班怒怼领导”一事火爆全网。因被指定清明节加班，中国电科成都区软件开发事业部的一名员工怒喷安排工作者...
### [“拒绝加班集体辞职”，中国电科回应](https://finance.sina.com.cn/wm/2023-04-05/doc-imypimne9323229.shtml)
> 概要: 转自：长安街知事 4月4日，疑似“中国电科员工怒怼领导清明节强制安排加班”的相关聊天记录在网络上传播。5日，中国电子科技集团有限公司（以下简称“中国电科”）针对此事回应...
### [北京市文化投资发展集团有限责任公司原党委书记、董事长周茂非严重违纪违法被开除党籍和公职](https://finance.sina.com.cn/jjxw/2023-04-05/doc-imypimnc2543233.shtml)
> 概要: 经北京市委批准，北京市纪委监委决定给予北京市文化投资发展集团有限责任公司原党委书记、董事长周茂非（正局级）开除党籍、开除公职处分。
### [“跪地求水”的孙国友背后：水权应该怎么管？](https://www.huxiu.com/article/1009176.html)
> 概要: 本文来自微信公众号：底线思维 （ID：dixiansiwei），作者：李昂（中国科学院植物研究所副研究员），原文标题：《万亩林场主这么种树治沙，科学吗？》，题图来自：视觉中国近日，一则“宁夏万亩林场主......
### [机票燃油附加费又降了：单程最低30元](https://finance.sina.com.cn/wm/2023-04-05/doc-imypimne9334850.shtml)
> 概要: 3月底开始，中国民航正式执行2023年夏秋航季航班计划，国际航班票价呈下降趋势。另外，多家国内航空公司表示，从4月5日开始，下调燃油附加费，目前机票价格如何？
### [澳大利亚官员考虑起诉 ChatGPT，后者称其因行贿而入狱](https://www.ithome.com/0/684/584.htm)
> 概要: 北京时间 4 月 5 日消息，澳大利亚一位地方郡长周三表示，如果 OpenAI 不纠正 ChatGPT 关于他曾因行贿入狱的错误说法，他可能会起诉 OpenAI。如果他真的采取法律行动，这将是 Cha......
### [扎克伯格反思裁员：人少了，事情反而更快](https://www.huxiu.com/article/1062700.html)
> 概要: 本文来自微信公众号：MacTalk（ID：MacTalkPro），作者：小盖，题图来自：视觉中国去年 11 月，Facebook 的母公司 Meta 宣布裁员 1.1 万人，约占员工总数的 13%。也......
### [张继科门背后的民间借贷史](https://finance.sina.com.cn/china/gncj/2023-04-05/doc-imypimni0556341.shtml)
> 概要: 来源丨德林社 文 | 李德林 编辑 | 德小强 张继科被推向风口浪尖，皆因一段恋情与一纸合同。作为奥运冠军的张继科曾经是体育骄子，跟女明星的一段红尘往事莫名其妙地卷入一...
### [R星起诉GTA逆向工程MOD组作者一案达成庭外和解](https://www.3dmgame.com/news/202304/3866399.html)
> 概要: Rockstar Games已经和一个玩家自制项目的创作者达成和解。该项目是之前我们曾经报道过的《侠盗猎车：罪恶都市》和《侠盗猎车3》的逆向工程。2021年，Take-Two Interactive和......
### [任天堂 Switch 迎来新对手，消息称索尼将推出新一代 PlayStation 掌机](https://www.ithome.com/0/684/589.htm)
> 概要: IT之家4 月 5 日消息，爆料人 Tom Henderson 透露，索尼将推出新款 PlayStation 手持设备，代号为 Q Lite，目标是成为另一款需要 PlayStation 5 的硬件产......
### [重磅！中国在WTO要求美日荷澄清是否存在芯片出口限制](https://www.yicai.com/news/101722394.html)
> 概要: 中国询问这三个WTO成员，该协议是否存在？如果存在的话，是否应该通知WTO成员并由WTO成员审查？
### [模拟游戏《中国边疆》新截图公开 支持简中](https://www.3dmgame.com/news/202304/3866402.html)
> 概要: 由SolidGames开发的模拟游戏《中国边疆（Chinese Frontiers）》上架Steam，新截图公开，本作将于年内发售，支持简体中文。探索在长城修建期间建立的华人定居点的生活。获得新技能，......
### [首届“中日韩展望对话会”举行，呼吁尽快重启中日韩领导人会议](https://www.yicai.com/news/101722453.html)
> 概要: 与会各方一致认为有必要尽快重启自2019年底以来中断的中日韩领导人会议。
### [AOKZOE 预热新款掌机：《卧龙：苍天陨落》最高画质 40-50fps](https://www.ithome.com/0/684/609.htm)
> 概要: IT之家4 月 5 日消息，AOKZOE 即将推出新款 A1 Pro 掌机，预计将搭载 R7 7840U 处理器。现在，官方放出了一段视频，信息显示这款掌机运行《卧龙：苍天陨落》最高画质 40-50f......
### [重磅：传索尼开发新掌机 仍然需要PS5](https://www.3dmgame.com/news/202304/3866403.html)
> 概要: 外媒Insider Gaming报道索尼正在开发一个新的PlayStation掌机，代号为Q Lite，需要拥有PS5主机。据了解，Q Lite并不是一个云串流设备（最近业内记者Grub称索尼的新掌机......
### [升级石墨烯：南孚传应纽扣电池 1.5 元再降新低（商超 4.9 元）](https://lapin.ithome.com/html/digi/684612.htm)
> 概要: 【升级超导石墨烯】南孚 传应纽扣电池5 粒大促价 11.9 元，今日可领 4 元加码券，实付 7.9 元包邮。如果淘金币足够，抵扣后仅需 7.54 元。共有 CR2032 / CR2025 / CR2......
# 小说
### [超能国宝](https://book.zongheng.com/book/1161016.html)
> 作者：潇潇雨歧

> 标签：都市娱乐

> 简介：奇葩外星人，奇葩穿越成国宝文物，灭世的启示，他们决定组团拯救，对抗、矛盾、背叛、陷害、撕裂、忠诚……从恶里滋生善，还是善被恶吞噬？

> 章节末：第426章  南无宗

> 状态：完本
# 论文
### [Prepended Domain Transformer: Heterogeneous Face Recognition without Bells and Whistles | Papers With Code](https://paperswithcode.com/paper/prepended-domain-transformer-heterogeneous)
> 日期：12 Oct 2022

> 标签：None

> 代码：https://gitlab.idiap.ch/bob/bob.paper.tifs2022_hfr_prepended_domain_transformer

> 描述：Heterogeneous Face Recognition (HFR) refers to matching face images captured in different domains, such as thermal to visible images (VIS), sketches to visible images, near-infrared to visible, and so on. This is particularly useful in matching visible spectrum images to images captured from other modalities. Though highly useful, HFR is challenging because of the domain gap between the source and target domain. Often, large-scale paired heterogeneous face image datasets are absent, preventing training models specifically for the heterogeneous task. In this work, we propose a surprisingly simple, yet, very effective method for matching face images across different sensing modalities. The core idea of the proposed approach is to add a novel neural network block called Prepended Domain Transformer (PDT) in front of a pre-trained face recognition (FR) model to address the domain gap. Retraining this new block with few paired samples in a contrastive learning setup was enough to achieve state-of-the-art performance in many HFR benchmarks. The PDT blocks can be retrained for several source-target combinations using the proposed general framework. The proposed approach is architecture agnostic, meaning they can be added to any pre-trained FR models. Further, the approach is modular and the new block can be trained with a minimal set of paired samples, making it much easier for practical deployment. The source code and protocols will be made available publicly.
### [Predicting Density of States via Multi-modal Transformer | Papers With Code](https://paperswithcode.com/paper/predicting-density-of-states-via-multi-modal)
> 日期：13 Mar 2023

> 标签：None

> 代码：https://github.com/heewoongnoh/dostransformer

> 描述：The density of states (DOS) is a spectral property of materials, which provides fundamental insights on various characteristics of materials. In this paper, we propose a model to predict the DOS by reflecting the nature of DOS: DOS determines the general distribution of states as a function of energy. Specifically, we integrate the heterogeneous information obtained from the crystal structure and the energies via multi-modal transformer, thereby modeling the complex relationships between the atoms in the crystal structure, and various energy levels. Extensive experiments on two types of DOS, i.e., Phonon DOS and Electron DOS, with various real-world scenarios demonstrate the superiority of DOSTransformer. The source code for DOSTransformer is available at https://github.com/HeewoongNoh/DOSTransformer.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
