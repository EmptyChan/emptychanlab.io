---
title: 2022-05-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MountFryatt_ZH-CN0611142036_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-05-30 22:07:11
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MountFryatt_ZH-CN0611142036_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [拆解100+个博主，我们总结一套小红书万能涨粉公式！](http://www.woshipm.com/operate/5464286.html)
> 概要: 编辑导语：小红书作为一个种草平台，吸引了很多博主入驻，那么如何才能快速涨粉呢？本篇文章作者总结了一套小红书万能涨粉公式，并列举具体案例进行分析，总结了涨粉的方法，感兴趣的小伙伴一起来学习一下吧，希望对......
### [抖音快手，走向两极](http://www.woshipm.com/operate/5463802.html)
> 概要: 编辑导语：抖音和快手可以说是短视频平台的两大巨头，可它们之间的生态属性却大不相同。本篇文章中作者从平台内容和延伸服务两方面对抖音和快手展开了一系列详细的分析，感兴趣的小伙伴们快来一起看看吧。电影《流浪......
### [职场秘籍：设计师如何牵头做产品改版并推动项目](http://www.woshipm.com/zhichang/5463796.html)
> 概要: 编辑导语：“改版”这件事对于产品研发团队来说，可不是一件容易的事情。本篇文章中作者从项目和职场的角度与大家分享了由设计师主导的改版过程中，需要注意的点。一起来看看吧，希望对你有所帮助。听到改版，想必大......
### [黑色四叶草：泽农的实力被严重低估，空间魔法本应该排名第一](https://new.qq.com/omn/20220528/20220528A0AMBF00.html)
> 概要: 在黑色四叶草漫画当中，说起漆黑三极性，公认最强者非但丁莫属，毕竟作为漆黑三极性的老大，除了备份让人尊敬之外，其重力魔法，也是来自冥界之王路西法罗的传承，作为冥界三大柱之一的路西法罗，也是冥界公认的最强......
### [专访导演陈剑莹谈短片获奖：戛纳评审感动落泪，姚安娜很真诚很努力](https://new.qq.com/rain/a/ENT2022053000003300)
> 概要: 文/耿飏北京时间5月29日凌晨，第75届戛纳电影节公布了主竞赛单元获奖名单。由陈剑莹导演，姚安娜主演的《海边升起一座悬崖》斩获了主竞赛单元短片金棕榈奖。颁奖典礼上，直到听到颁奖嘉宾在一片掌声中念出自己......
### [300万人的第一双“虚拟鞋”](https://www.huxiu.com/article/567960.html)
> 概要: 出品｜虎嗅科技组作者｜周舟头图｜视觉中国虚拟鞋卖疯了。今年上半年，一家叫做StepN的Web3公司，在短短几个月里卖了几十甚至上百万双“虚拟鞋”。这些鞋子，以NFT的形式展现，大部分价格在5000元～......
### [被卷入币圈死亡螺旋，我的千万资产3天归零](https://www.huxiu.com/article/567465.html)
> 概要: 本文来自微信公众号：凤凰WEEKLY财经 （ID：fhzkzk），作者：刘思洁，编辑：雪梨王，原文标题：《我，35岁程序员，卷入币圈死亡螺旋，千万资产3天归零》，头图来自：《大空头》剧照账户里最多时近......
### [Q版水浒传108将系列人物全传](https://www.zcool.com.cn/work/ZNjAwODg0MzI=.html)
> 概要: 历时一年半，终于把这套水浒画完了......
### [「排球少年！！」影山飞雄手办现已开订](http://acg.178.com/202205/447873753585.html)
> 概要: Good Smile Company作品「排球少年！！」影山飞雄手办现已开订，手办高约180mm，主体采用ABS与PVC材料制造，原型师为Design COCO。该手办售价4,080日元（含税），约合......
### [TV动画「Love Live! Superstar!!」公开第2期全新视觉图](http://acg.178.com/202205/447874029771.html)
> 概要: TV动画「Love Live! Superstar!!」于近日公开了第2期全新视觉图，本作预计将于2022年7月正式开播。「Love Live! Superstar!!」是京极尚彦执导、花田十辉负责总......
### [女子青春音乐企划「swing, sing」公布全新视觉图](http://acg.178.com/202205/447874186846.html)
> 概要: 近日，女子青春音乐企划「swing, sing」公布了全新的视觉图，本作漫画版现已开始连载。STAFF原案・原作：伊澄アキ角色设计：東山エイト音乐：早川博隆漫画：トリヤロウプロデュース：ISARIBI......
### [元宇宙的“性侵”事件，是对现实世界的提醒](https://finance.sina.com.cn/tech/2022-05-30/doc-imizirau5562096.shtml)
> 概要: 人们应当得出一些共识，去划定女性应当享有的生活空间。这种空间，既包括人身安全的保障，也包括精神、心灵免受骚扰的安全感......
### [《石纪元》新舞台剧公开 主角定妆海报极度还原](https://www.3dmgame.com/news/202205/3843536.html)
> 概要: 人气漫画《石纪元》已经于3月6日发售的《周刊少年JUMP》第14期正式完结，作为纪念活动之一，全新舞台剧《『Dr.STONE』THE STAGE ～SCIENCE WORLD～》公开，官方COS出品素......
### [PS4/PS5模拟器Kyty发布 已能在PC上运行一些游戏](https://www.3dmgame.com/news/202205/3843537.html)
> 概要: PS3模拟器RPCS3已渐入佳境，最近的新更新大幅改进了游戏性能。而PS4/PS5模拟器项目也在加速推进，其中就包括Kyty、Spine和GPCS4。近日开发商InoriRus发布Kyty的V0.10......
### [测不准时钟](https://www.zcool.com.cn/work/ZNjAwOTA3Mjg=.html)
> 概要: 时隔差不多一年没有发作品了。这次给大家带来一款今年设计的桌面多功能小时钟。熟悉阿吉作品的小伙伴应该能看得出来。这个产品其实是6年前EYE BALL 创意时钟与5年前CUBIC便携式迷你音箱的精神续作......
### [出售假冒“华为”耳机，华为起诉网店擅用其商标获赔100万](https://finance.sina.com.cn/tech/2022-05-30/doc-imizmscu4142928.shtml)
> 概要: IT之家5月30日消息，信息显示，近日，浙江省杭州市余杭区人民法院公布一起侵权华为商标及不正当竞争纠纷的案件......
### [漫画「地球之子」第一卷封面公开](http://acg.178.com/202205/447879511195.html)
> 概要: 「周刊少年JUMP」新连载「地球之子」（地球の子）公开了单行本第一卷的封面图，该卷将于2022年6月3日发售。「地球之子」是神海英雄创作的少年漫画作品，在知名漫画杂志「周刊少年JUMP」上进行连载。本......
### [男子沉迷游戏受痛责离家12年 找到时父母已去世](https://www.3dmgame.com/news/202205/3843542.html)
> 概要: 有些东西一生只有一次，失去了真的再也找不回来了，日前据媒体报道，5月26日晚，广东东莞高埗，12年不见的姐弟俩抱头痛哭的画面，让在场的寻亲志愿者纷纷落泪。29日，上游新闻记者从当地警方了解到，男子李某......
### [形外之意](https://www.zcool.com.cn/work/ZNjAwOTIwMTI=.html)
> 概要: 正在参与：2022大设计奖·校园赛道......
### [《午后两点半》](https://www.zcool.com.cn/work/ZNjAwOTIxNjQ=.html)
> 概要: Two Thirty in the Afternoon午后两点半………………作者author：孟小书  郭埙开本format：130mm x185mm页码Page：240设计design：Mlimt出......
### [“两高两部”：不得授权网络平台直接查询未成年人犯罪信息](https://finance.sina.com.cn/tech/2022-05-30/doc-imizirau5599730.shtml)
> 概要: 作者：王峰 编辑：钟映佳......
### [舞台剧《Dr.STONE》公开PV](https://news.dmzj.com/article/74549.html)
> 概要: 根据稻垣理一郎、Boichi原作创作的舞台剧《『Dr.STONE』THE STAGE ～SCIENCE WORLD～》公开了宣传图与PV。
### [全球化手册·中东篇｜去中东“掘金”，我们还有哪些机会？](https://www.tuicool.com/articles/R3QJb22)
> 概要: 全球化手册·中东篇｜去中东“掘金”，我们还有哪些机会？
### [《石纪元》舞台剧定妆海报公开 各位角色很还原](https://acg.gamersky.com/news/202205/1486778.shtml)
> 概要: 稻垣理一郎原作，Boichi作画的漫画《石纪元》宣布将推出舞台剧，官方公开了角色定妆海报，不得不说这个定妆照的效果真是很棒，各位角色都是相当还原。
### [I disabled WiFi on the new Samsung fridge](https://eattherich.club/@swaggboi/108382897807037127)
> 概要: I disabled WiFi on the new Samsung fridge
### [《新奥特曼》推出联动剃须刀 只要116你也能这么光溜](https://acg.gamersky.com/news/202205/1486795.shtml)
> 概要: 《新·奥特曼》 x “axia”联动款剃须刀今日正式发售。
### [新消费急转直下，怎么办？](https://www.huxiu.com/article/568532.html)
> 概要: 本文来自微信公众号：真探AlphaSeeker（ID：deep_insights），作者：真探，题图来源：视觉中国新消费赛道冰火两重天。去年各种面馆、中式烘培高估值融资的场面历历在目，千店万店的扩张口......
### [喜羊羊之父来了！预约看直播：喜羊羊24岁了，但国漫至今仍未成年？](https://new.qq.com/rain/a/ENT2022053000049200)
> 概要: 喜羊羊之父来了！预约看直播：喜羊羊24岁了，但国漫至今仍未成年？
### [国家广电总局：经纪机构不得以应援集资等方式诱导粉丝消费](https://finance.sina.com.cn/tech/2022-05-30/doc-imizirau5597593.shtml)
> 概要: 来源：国家广播电视总局......
### [人气漫画《间谍过家家》累计突破2100万部！](https://news.dmzj.com/article/74552.html)
> 概要: 人气漫画《间谍过家家》在本日（30日）宣布了累计突破2100万部的消息。
### [《间谍过家家》漫画总销量突破2100万 动画增效明显](https://acg.gamersky.com/news/202205/1486824.shtml)
> 概要: 《间谍过家家》漫画总销量宣布突破2100万部，这个成绩也是相当喜人。TV动画开播时漫画销量为1250万部，也就是说动画播出的两个月内原作销量就增加了850万部。
### [ORNL’s Frontier breaks the exaflop ceiling](https://top500.org/news/ornls-frontier-first-to-break-the-exaflop-ceiling/)
> 概要: ORNL’s Frontier breaks the exaflop ceiling
### [强强联合 日本新动画制作公司JOEN宣布成立](https://acg.gamersky.com/news/202205/1486834.shtml)
> 概要: 日本动画制作公司CloverWorks、 WIT STUDIO、Aniplex和集英社联合宣布成立专注于艺术和动画制作的新公司——JOEN。
### [北美票房：《壮志凌云2》创阿汤哥最佳开画纪录](https://ent.sina.com.cn/m/f/2022-05-30/doc-imizirau5616357.shtml)
> 概要: 新浪娱乐讯 北京时间5月30日消息，据外国媒体报道，《壮志凌云2》北美首周3天票房粗报收1.24亿美元，表现出色，是汤姆·克鲁斯40年演员生涯的最佳开画成绩，也是首次破亿（原纪录为2005年《世界大战......
### [CloverWorks、WIT STUDIO、Aniplex、集英社合作创立新公司](https://news.dmzj.com/article/74553.html)
> 概要: CloverWorks、WIT STUDIO、Aniplex、集英社四家公司共同出资，在本日（30日）合作创立了新公司JOEN。JOEN的CEO将由武哲也和福岛祐一担任。
### [95号汽油每升“直逼10元”，你还要“上车”吗？](https://www.huxiu.com/article/568822.html)
> 概要: 本文来自微信公众号：时代财经APP （ID：tf-app），作者：余思毅，原文标题：《直逼10元！油价或迎第9涨，加满一箱油比年初多花近百元》，题图来自：《头文字D》5月30日24时，成品油调价窗口将......
### [存款利率下行，该不该提前还贷？](https://www.yicai.com/news/101428049.html)
> 概要: 2021年以来，同业存单利率进入下行区间，银行继续高息揽存的动力下降。现阶段看，其应对策略有二：一是在同等收益水平下追求更高的流动性，现金管理类产品逐步受到追捧；二是减少借款并提前偿还贷款，尤其是近几年新发放的利率相对较高的住房贷款。
### [Show HN: Top Links from Hacker News, Reddit, Techmeme, PH on a Single Page](https://alltoplinks.com/)
> 概要: Show HN: Top Links from Hacker News, Reddit, Techmeme, PH on a Single Page
### [动画《咒术回战》与TAKARA TOMY联动推出人生游戏](https://news.dmzj.com/article/74554.html)
> 概要: TAKARA TOMY和TV动画《咒术回战》联动，推出了《咒术回战 人生游戏》和《咒术回战 虎杖危机一发》两款实体游戏。其中《咒术回战 人生游戏》售价4950日元，《咒术回战 虎杖危机一发》售价2530日元。
### [消息称三星将退出印度功能手机市场，聚焦智能手机业务](https://www.ithome.com/0/621/326.htm)
> 概要: 知情人士称，三星正计划逐步退出印度功能手机市场，以聚焦智能手机业务。据《印度时报》报道，三星已经通知渠道合作伙伴，将在未来几个月或今年年底退出印度功能手机市场。据悉，三星是印度生产关联激励（PLI）计......
### [丁真天天向上，直男速通理塘](https://www.tuicool.com/articles/ZRFJneA)
> 概要: 丁真天天向上，直男速通理塘
### [哔哩哔哩桌游棋牌狂欢节 将于6月1日正式开启](https://www.3dmgame.com/news/202205/3843564.html)
> 概要: 哔哩哔哩桌游棋牌狂欢节将于6月1日至6月30日举办，万代、威世智、BUSHIROAD、栢龙玩具、艾赐魔袋、游卡桌游等20余家厂商&发行商将会参展，6月2日19点桌游棋牌狂欢节发布会上将为大家带来超过3......
### [直降 1100 元：魅族 18s Pro 手机 24 期免息](https://www.ithome.com/0/621/338.htm)
> 概要: 魅族 18s Pro 发布于 2021 年 9 月 22 日，搭载高通最新的骁龙 888 Plus 芯片，还有 LPDDR5+UFS 3.1+WiFi-6 增强版三件套，6.7 英寸 120Hz 屏幕......
### [《龙之家族》发新剧照 《权力的游戏》故事再续](https://ent.sina.com.cn/v/u/2022-05-30/doc-imizirau5635380.shtml)
> 概要: 新浪娱乐讯 北京时间5月30日消息，据外国媒体报道，《权力的游戏》衍生剧《龙之家族》发布新剧照，将于8月21日开播。　　该剧聚焦坦格利安家族的故事，会涉及其著名的血腥内战“血龙狂舞”，时间回到200多......
### [《小丑回魂》前传剧集运作 奥兰多布鲁姆定新片](https://ent.sina.com.cn/v/u/2022-05-30/doc-imizmscu4208352.shtml)
> 概要: 新浪娱乐讯 北京时间5月30日消息，据外国媒体报道，好莱坞多个新项目曝光：HBO Max将打造2017年电影版《小丑回魂》的前传剧集《欢迎来到德里镇》（Welcome to Derry），设定在196......
### [三星新款 G8 Mini LED 显示器开始上市：4K 240Hz HDR2000，9999 元](https://www.ithome.com/0/621/343.htm)
> 概要: IT之家5 月 30 日消息，在今年台北电脑展上，三星推出了新款 Odyssey Neo G8 4K Mini LED 显示器，建议零售价为 1500 美元（约 9990 元人民币）。现在，这款显示器......
### [中兴通讯：独家中标中国广电全国 5G 核心网工程位置服务平台](https://www.ithome.com/0/621/344.htm)
> 概要: IT之家5 月 30 日消息，中兴通讯宣布，近日，中国广电 5G 核心网工程位置类招标项目结果公示，中兴通讯独家中标，承建中国广电全国位置服务平台北京、南京两个站点（主备），该项目建成后将为中国广电全......
### [安踏女鞋海报被指有打擦边球嫌疑：官方已下架](https://www.3dmgame.com/news/202205/3843569.html)
> 概要: 近日宝洁、洁婷、妇炎洁等多个女性品牌的广告营销出现涉嫌侮辱女性的行为，引发网友声讨，官方也都公开致歉。但是看起来，教训并没有被充分吸取。5月29日不少网友发帖称，安踏当季女鞋新品“喵喵鞋2.0”的一则......
### [5月30日这些公告有看头](https://www.yicai.com/news/101428632.html)
> 概要: 5月30日晚间，多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [赵老哥加仓次新股阳光乳业，卖出钱江水利](https://www.yicai.com/vip/news/101428634.html)
> 概要: 东方路跌停割肉新华制药，亏700多万元；溧阳路卖出中设股份4000万元；二纬路卖出农发种业1.1亿元。5月还剩最后一天，6月看点>>>
### [圣斗士：不算童虎，各级别最能打圣斗士是谁？有一辉黄金不是沙加](https://new.qq.com/omn/20220520/20220520A05AG600.html)
> 概要: 在动漫《圣斗士》的世界观里，圣斗士从低到高依次分为青铜、白银、黄金这3个级别。若按正常人设来看的话，12名黄金圣斗士才是圣域最强的战士，白银圣斗士则其次，青铜级别的人垫底。可车田正美偏要走励志路线，硬......
### [市场小幅攀升 反弹还能走多远？](https://www.yicai.com/news/101428644.html)
> 概要: None
### [现代 Monorepo 工程技术选型，聊聊我的思考](https://www.tuicool.com/articles/VbMN7zf)
> 概要: 现代 Monorepo 工程技术选型，聊聊我的思考
### [无所不能的杰拉德！英超官推盘点他的高光时刻祝贺他生日快乐](https://bbs.hupu.com/53978918.html)
> 概要: 来源：https://twitter.com/i/status/1531237076796166144
### [“空投”不断、人才涌现，上海游戏公司在疫情下“卷起来”？](https://www.tuicool.com/articles/If26Nzj)
> 概要: “空投”不断、人才涌现，上海游戏公司在疫情下“卷起来”？
### [再次明确！2022年医保改革重点有这些](https://www.yicai.com/news/101428769.html)
> 概要: 国家医保局等部门将指导地方科学设置医疗服务价格调整的启动条件、触发标准及约束条件，年内开展1次调价评估，符合条件的及时调价。
### [理财产品净值V型反弹，投资经理如何调仓布局？](https://www.yicai.com/news/101428703.html)
> 概要: 理财产品净值V型反转能持续多久？
### [Hacking Detergent DRM for 98% Cost Saving](https://github.com/dekuNukem/bob_cassette_rewinder/blob/master/README.md)
> 概要: Hacking Detergent DRM for 98% Cost Saving
### [赵刚新当选陕西省委副书记，蒿慧杰、李春临、王琳新当选省委常委](https://finance.sina.com.cn/jjxw/2022-05-30/doc-imizmscu4235414.shtml)
> 概要: 据新华社报道，中共陕西省第十四届委员会第一次全体会议5月30日选举刘国中为省委书记，赵一德、赵刚为省委副书记，当选为省委常委的还有王晓、王兴宁、方红卫、程福波...
### [云南省政府第156次常务会议强调：放下身段服务企业 竭尽全力发展企业 精准落实政策 有力提振信心](https://finance.sina.com.cn/china/2022-05-30/doc-imizirau5665905.shtml)
> 概要: 原标题：省政府第156次常务会议强调：放下身段服务企业 竭尽全力发展企业 精准落实政策 有力提振信心 来源：云南发布 5月30日，省委副书记...
### [一家“黄码”医院的抗疫之战](https://finance.sina.com.cn/china/2022-05-30/doc-imizirau5666352.shtml)
> 概要: 原标题：一家“黄码”医院的抗疫之战 来源： 重案组37号 感染者减少，生活复位，医院的一切即将回归正轨。 医护人员在发热门诊接收封管控区病人就诊。
### [西安电子科技大学通报学生毕设代做事件：两名学生留校察看一年](https://finance.sina.com.cn/china/gncj/2022-05-30/doc-imizmscu4236034.shtml)
> 概要: 新京报讯 据@西安电子科技大学微博5月30日消息，近日，该校收到有关计算机科学与技术学院本科生雷某某、卢某某涉嫌学术不端问题的反映，学校高度重视...
### [流言板巴特勒东决G6和G7合计砍下82分，2001年艾弗森后历史第二人](https://bbs.hupu.com/53979966.html)
> 概要: 虎扑05月30日讯 热火主场96-100不敌凯尔特人，东部决赛大比分3-4不敌对手被淘汰出局。全场比赛，热火前锋吉米-巴特勒首发出战48分钟，24投13中，三分球4投1中，罚球11罚8中得到35分9篮
### [卧谈会你认为哪种食材是吃火锅时的GOAT？](https://bbs.hupu.com/53980004.html)
> 概要: 兄弟们，你觉得吃火锅时，哪种食材下进去最好吃呢？我心中的GOAT是毛肚，太绝了，不知道兄弟们想把这一票投给谁。
### [苏州：明起陆续恢复市际、县际客运班线](https://finance.sina.com.cn/jjxw/2022-05-30/doc-imizmscu4237830.shtml)
> 概要: “苏州交通运输”微信公众号30日消息，随着近期全省疫情形势向好，为满足广大人民群众出行需求，苏州市将陆续恢复市际、县际客运班线。
### [刘鹤出席中国工程院第十六次院士大会](https://finance.sina.com.cn/china/2022-05-30/doc-imizmscu4238274.shtml)
> 概要: 新华社北京5月30日电 中共中央政治局委员、国务院副总理刘鹤5月30日上午出席中国工程院第十六次院士大会开幕式并讲话。
### [流言板斯波：两支球队都在应对伤病，洛瑞他们本该多休息](https://bbs.hupu.com/53980126.html)
> 概要: 虎扑05月30日讯 今天热火抢七大战96-100憾负凯尔特人，无缘总决赛。赛后热火主帅埃里克-斯波尔斯特拉接受了采访。谈到球员们的伤病，斯波说：“我认为，如果我们能在某些比赛中间多休息一天，也许球员们
# 小说
### [九登鬼宴](http://book.zongheng.com/book/801227.html)
> 作者：四六妖

> 标签：奇幻玄幻

> 简介：偶然成为猎捕亡魂的魂师，本来只想混口饭吃，结果发现这个诡异的第二世界并不简单，人心叵测，鬼计多端，误入鬼宴结果搭档惨死恶鬼之手，招募自己的猎魂公司又带着别样的目的，在这魂鬼的世界中又该何去何从，张嫌登宴杀鬼，下宴灭魂，追寻着魂师至高之境……

> 章节末：第一千二百八十四节：终局一战

> 状态：完本
# 论文
### [UniSAr: A Unified Structure-Aware Autoregressive Language Model for Text-to-SQL | Papers With Code](https://paperswithcode.com/paper/unisar-a-unified-structure-aware-1)
> 日期：15 Mar 2022

> 标签：None

> 代码：None

> 描述：Existing text-to-SQL semantic parsers are typically designed for particular settings such as handling queries that span multiple tables, domains or turns which makes them ineffective when applied to different settings. We present UniSAr (Unified Structure-Aware Autoregressive Language Model), which benefits from directly using an off-the-shelf language model architecture and demonstrates consistently high performance under different settings. Specifically, UniSAr extends existing autoregressive language models to incorporate three non-invasive extensions to make them structure-aware: (1) adding structure mark to encode database schema, conversation context, and their relationships; (2) constrained decoding to decode well structured SQL for a given database schema; and (3) SQL completion to complete potential missing JOIN relationships in SQL based on database schema. On seven well-known text-to-SQL datasets covering multi-domain, multi-table and multi-turn, UniSAr demonstrates highly comparable or better performance to the most advanced specifically-designed text-to-SQL models. Importantly, our UniSAr is non-invasive, such that other core model advances in text-to-SQL can also adopt our extensions to further enhance performance.
### [A Strong Baseline for Query Efficient Attacks in a Black Box Setting | Papers With Code](https://paperswithcode.com/paper/a-strong-baseline-for-query-efficient-attacks)
> 日期：10 Sep 2021

> 标签：None

> 代码：None

> 描述：Existing black box search methods have achieved high success rate in generating adversarial attacks against NLP models. However, such search methods are inefficient as they do not consider the amount of queries required to generate adversarial attacks.
