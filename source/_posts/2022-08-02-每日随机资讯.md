---
title: 2022-08-02-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HickmanBridge_ZH-CN0976106691_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-08-02 22:10:40
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HickmanBridge_ZH-CN0976106691_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [Apple music用户体验分析，原来苹果也没有把这些做好](https://www.woshipm.com/ucd/5550065.html)
> 概要: 编辑导语：Apple Music是苹果推出的一款具有付费订阅流媒体服务功能的新音乐产品，但是用户体验似乎并没有想象中优秀，本文作者整理了一些Apple music存在的体验问题，感兴趣的朋友一起来看看......
### [「24小时直播商超」取代「四大天王」？](https://www.woshipm.com/it/5550562.html)
> 概要: 编辑导语：如今的直播时间越来越长，当你打开直播频道时，大概率有一个带货直播间像24小时便利商超一样稳定在线。而随着越来越多24小时直播间的出现，“24小时直播商超”会取代“四大天王”吗？本文就此问题做......
### [每日优鲜倒了，叮咚买菜的春天在哪？](https://www.woshipm.com/it/5549636.html)
> 概要: 编辑导语：社区团购一家接着一家倒下，继每日生鲜倒下以后，叮咚买菜的存在意味着什么，又将何去何从？作者从叮咚买菜的用户体验及其盈利两方面展开分析，一起看看其未来发展方向。每日优鲜的前置仓模式已经扑街，叮......
### [模仿北京健康宝动画](https://segmentfault.com/a/1190000042256198)
> 概要: 模仿北京健康宝动画北京健康宝有一个动画，一圈小球，沿着正方形顺时针滚动，并且颜色是渐变的，像下图的这样提取出以下关键信息：小球沿着矩形顺时针连续滚动，小球的间距是均匀的小球在矩形直角的时候会沿着直角滚......
### [8月1日起完全自动驾驶汽车在深圳可合法上路](https://finance.sina.com.cn/tech/it/2022-08-02/doc-imizirav6351258.shtml)
> 概要: 从8月1日开始，《深圳经济特区智能网联汽车管理条例》正式实施，这也意味着，深圳作为先行示范区，完全自动驾驶汽车可以合法上路。目前在深圳市福田区，自动驾驶出租车已经有超过200个站点，每天在外运营和测试......
### [Mall电商实战项目微服务版本全面升级！支持最新版SpringCloud，权限解决方案升级...](https://segmentfault.com/a/1190000042256434)
> 概要: 前阵子我把mall项目全面升级了，支持了SpringBoot 2.7.0，相信很多小伙伴已经知道了。最近抽空把它的微服务版本mall-swarm也升级了，已支持最新版SpringCloud&Aliba......
### [中原银行实时风控体系建设实践](https://segmentfault.com/a/1190000042257318)
> 概要: 摘要：本文整理自中原银行数据平台中心开发工程师陈玉强在 Flink Forward Asia 2021 行业实践专场的演讲。主要内容包括：建设体系选型 & 架构应用场景建设成效点击查看直播回放 & 演......
### [包装营销推广涉嫌违法](https://finance.sina.com.cn/tech/internet/2022-08-02/doc-imizmscv4463817.shtml)
> 概要: 来源：法治日报......
### [警惕网络平台成“知假售假”主渠道](https://finance.sina.com.cn/tech/internet/2022-08-02/doc-imizmscv4458916.shtml)
> 概要: 微信朋友圈转发，就能卖货、带货，再根据销售量获得提成——这样的“好事”近年来成为一些年轻人“兼职赚钱”的新门路。但轻松赚钱的“好事”背后隐患重重......
### [经济日报：美“芯片和科学法案”损人不利己](https://finance.sina.com.cn/tech/it/2022-08-02/doc-imizmscv4459228.shtml)
> 概要: 美国会近日通过的“芯片和科学法案”部分条款限制有关企业在华正常经贸与投资活动，反映出美方一些人根深蒂固的冷战及零和博弈思维，这势必限制阻碍中美正常科技合作，不利于双方共同利益和人类共同进步......
### [完美世界：雨王在上界还有后台，在石昊面前实力再强也将其抹杀](https://new.qq.com/omn/20220717/20220717A007X700.html)
> 概要: 完美世界石昊清算石都中投靠三教的势力，雨族作为王族首当其冲，败于十五爷之手的雨王再度现身与人皇石昊大战，不出意料地惨败，在这危急时刻，雨神的石像动了，让雨王有了一丝喘息之机，那么凭一座雕像就能让人有反......
### [爱优腾的自救路，写在北美电视台奋斗史里](https://www.huxiu.com/article/624335.html)
> 概要: 来源：表外表里（ID：excel-ers）作者：陈成 胡汀琅题图：视觉中国长视频行业，正在失去“尊严”。近期爱奇艺被抖音“拐”入同一个战壕，就影视二创达成合作，向曾经唾弃的“猪食论”妥协。之所以退到如......
### [《童梦嬉戏》](https://www.zcool.com.cn/work/ZNjEyMjAxOTI=.html)
> 概要: 每一代人都有自己的童年故事，那么多快乐的童年游戏都消失在时间长河且长久的留在了我们回忆中，经过6年，我的童年故事在记忆中闪着显眼星光的部分基本已经画完了。虽有不舍，《童年回忆》这个系列也正式做个告别......
### [《赛博朋克2077》衍生动画公布新预告 下月即将开播](https://acg.gamersky.com/news/202208/1505024.shtml)
> 概要: 今日，扳机社公布了《赛博朋克2077》衍生动画《赛博朋克：边缘行者》的新预告，本片将于9月上线Netflix。
### [「鸭乃桥论的禁忌推理」最新8月月历壁纸公开](http://acg.178.com/202208/453404055514.html)
> 概要: 近日，漫画家天野明公开了其绘制的「鸭乃桥论的禁忌推理」8月月历壁纸，这次带来的角色是穿着浴衣的雨宫前辈。漫画「鸭乃桥论的禁忌推理」于2020年10月11日开始在杂志「少年JUMP+」上进行连载，讲述了......
### [京东“满天星计划”落地浙江 助力企业走“专精特新”发展之路](http://www.investorscn.com/2022/08/02/102139/)
> 概要: 近日，由中国中小企业发展促进中心、浙江省经济和信息化厅、浙江省丽水市政府三方共建的全国“专精特新”企业培育基地（杭州）（以下简称“基地”）正式揭牌。基地将围绕“提升产业基础高级化、产业链水平现代化”，......
### [波场Poloniex发布全新交易系统 更快速、更稳定、更易用](http://www.investorscn.com/2022/08/02/102140/)
> 概要: 2022年8月1日，波场Poloniex正式发布了全面升级的新交易系统，此次升级主打“更快速、更稳定、更易用”，从性能、稳定性和用户体验上都做了大幅优化，将波场Poloniex交易系统提升到一线水平，......
### [Understanding Jane Street](https://www.thediff.co/p/jane-street)
> 概要: Understanding Jane Street
### [「消失的初恋」公布漫画完结纪念迷你画册封面](http://acg.178.com/202208/453408676553.html)
> 概要: 近日，ひねくれ渡原作、アルコ作画的恋爱喜剧漫画「消失的初恋」公布了漫画完结纪念迷你画册封面。「消失的初恋」讲述了主角青木喜欢隔壁桌的女生桥下，不过有一天却看到桥下同学的橡皮擦上面写着同班的男生井田名字......
### [Parallel Programming for FPGAs](https://github.com/KastnerRG/pp4fpgas)
> 概要: Parallel Programming for FPGAs
### [组图：鹿晗夜晚醉酒被拍 走路摇摇晃晃被好友搀扶离开](http://slide.ent.sina.com.cn/star/slide_4_86512_373358.html)
> 概要: 组图：鹿晗夜晚醉酒被拍 走路摇摇晃晃被好友搀扶离开
### [周口市委书记张建慧率队考察深圳招商，与国厚资产商谈深化合作项目](http://www.investorscn.com/2022/08/02/102145/)
> 概要: 围绕“两个确保”,坚定不移实施“十大战略”三年行动计划,切实推动河南省委“十大战略”在周口落地落细落实。周口市委书记张建慧率队在深圳开展考察招商专题活动,走访重点企业和商会组织,参观学习、畅叙友谊、商......
### [组图：白敬亭宋轶带工作人员看沈腾新电影 两人打扮低调先后离开](http://slide.ent.sina.com.cn/star/slide_4_86512_373359.html)
> 概要: 组图：白敬亭宋轶带工作人员看沈腾新电影 两人打扮低调先后离开
### [「Fate/kaleid liner 魔法少女伊莉雅」婚纱伊莉雅手办开订](http://acg.178.com/202208/453411876898.html)
> 概要: KADOKAWA作品「Fate/kaleid liner 魔法少女☆伊莉雅 Licht 无名少女」婚纱伊莉雅手办现已开订，手办高206mm，主体采用ABS和PVC材料制造。该手办售价25000日元，预......
### [Claris第24张专辑「ALIVE」全曲试听片段公开](http://acg.178.com/202208/453413127389.html)
> 概要: 近日，知名动漫歌手Claris公开了第24张专辑「ALIVE」的全曲试听片段。通常版定价为1430日元，将于8月3日发售。「ALIVE」全曲试听片段专辑全曲目收录如下：01.「ALIVE」02.「ルー......
### [百年汽车进化史图鉴｜The Evolution of Automobile](https://www.zcool.com.cn/work/ZNjEyMjUwODQ=.html)
> 概要: 从19世纪1885年第一辆内燃机载具的诞生，到今天电动汽车的普及。137年的时间跨度里，一台装置了内燃机的三轮车，是如何演变成今天我们所熟知的形态，并成为地球上最通行的交通工具之一的？我在历史的节点上......
### [寿屋《Prima Doll》鸦羽1/7比例手办开订](https://news.dmzj.com/article/75118.html)
> 概要: 寿屋根据《Prima Doll》中的鸦羽制作的1/7比例手办目前已经开订了。本作再现了鸦羽在黑猫亭中身穿制服，拿着菜单接待顾客时的样子，可以和之前发售的灰樱放在一起装饰。
### [Netflix《赛博朋克 Edgerunners》公开新预告](https://news.dmzj.com/article/75119.html)
> 概要: Netflix原创动画《赛博朋克 Edgerunners》公开了新预告与宣传图。日语版配音也一并公开，KENN、悠木碧、东地宏树、鸡冠井美智子等声优将参与配音。
### [4个技巧让设计立马变潮](https://www.zcool.com.cn/article/ZMTQzNTY0OA==.html)
> 概要: 4个技巧让设计立马变潮
### [Wi-Fine](https://wifine.gitlab.io/)
> 概要: Wi-Fine
### [《勇者斗恶龙X Online》开服十周年 官方发文庆祝](https://www.3dmgame.com/news/202208/3848365.html)
> 概要: SE旗下大型多人在线角色扮演游戏《勇者斗恶龙X Online》在今日（8月2日）迎来了开服十周年纪念，游戏官方发文感谢一路相伴的玩家表示感谢，并希望今后也可以一起度过美好的旅行！SE也在近日宣布，《勇......
### [任天堂发布NSO会员新头像素材 马车8与动森主题](https://www.3dmgame.com/news/202208/3848373.html)
> 概要: 任天堂官方8月2日今天宣布，再次发布NSO会员新头像素材，依然是深受玩家喜爱的《马车8》与《动森》主题，今天至9月6日期间，设定为8月诞生的《动森》岛民的新头像素材 可以领取。•在Switch主机上访......
### [楼工团·石匠译站马拉基-布兰纳姆将成为马刺队选秀的又一次神偷](https://bbs.hupu.com/54991841.html)
> 概要: 【想要了解更多关于马刺的优质内容，请关注公众号：石匠译站】三分，中距离，篮下进攻，这个19岁的天才少年是一个全能得分手，他还可以做到更多。他每天早早起床投入训练，脸上总是挂着大大的笑容。2003年5月
### [蔚小理的好日子，过去了？](https://www.huxiu.com/article/622897.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 视觉中国轮流上热搜的“蔚小理”，意外跌落销量神坛。8月1日，造车新势力相继公布7月交付量情况：哪吒汽车首夺新势力月销第一名，零跑汽车位列第二......
### [JRs之声姚明向所有人证明，尊重是靠自己的表现赢来的](https://bbs.hupu.com/54992337.html)
> 概要: 欢迎阅读今日的JRs之声！JRs之声是从外国各大论坛的NBA板块中，选取今日热门讨论话题，并翻译外国网友们的神评论，整理成文分享给国内球迷们。Yao Ming blocks Shaquille O'N
### [华为鸿蒙 HarmonyOS 3 多设备通信共享支持名单公布，9 月开启公测](https://www.ithome.com/0/632/842.htm)
> 概要: 感谢IT之家网友普莱是袋熊的线索投递！IT之家8 月 2 日消息，华为官网今日公布了支持多设备通信共享的设备名单，包括15 款手机和 2 款平板将获支持，最早 2022 年 9 月开启公测。2022 ......
### [像素冒险游戏《小鸡大探险》上线steam 今年Q4推出](https://www.3dmgame.com/news/202208/3848377.html)
> 概要: 像素平台冒险游戏《小鸡大探险（Chicken Journey）》现已上线steam，预计于2022年第四季度发售，支持中文。在这个放松的平台里做一只快乐可爱的小鸡。通过跳跃，攀爬以及解谜，去寻找每一只......
### [TSF微服务治理实战系列（一）——治理蓝图](https://www.tuicool.com/articles/NbqERj2)
> 概要: TSF微服务治理实战系列（一）——治理蓝图
### [P站美图推荐——向日葵特辑（二）](https://news.dmzj.com/article/75124.html)
> 概要: 色调明快，画风清新，向日葵花田里手捧鲜花、身穿白裙的长发少女可以说是ACG作品中最常见的夏日景象之一，不过盛开的花田里到底有什么呢……
### [播放量暴涨2000w+，单日狂揽24w粉，内卷的搞笑赛道](https://www.tuicool.com/articles/uUZfA33)
> 概要: 播放量暴涨2000w+，单日狂揽24w粉，内卷的搞笑赛道
### [【直播预告】清凉一夏增加新内容啦！](https://news.dmzj.com/article/75125.html)
> 概要: 本周直播预告
### [微众银行马智涛：打造区块链开源平台，助力粤港澳大湾区一体化融合发展](http://www.investorscn.com/2022/08/02/102154/)
> 概要: 近日，中国(深圳)综合开发研究院举办了《数“链”大湾区——区块链助力粤港澳大湾区一体化发展报告(2022)》(下称《报告》)发布会。《报告》提出，以区块链为代表的数字技术在破解粤港澳大湾区制度差异坚冰......
### [互联网前员工，回到现实世界](https://www.huxiu.com/article/624720.html)
> 概要: 本文来自微信公众号：晚点LatePost（ID：postlate），作者：祝颖丽、朱丽琨、姚胤米，题图来源：《楚门的世界》剧照一、离开大厂体系创业，第一步就困难前蚂蚁金服、前字节电商工程师陈熙（化名）......
### [海清主演《隐入尘烟》密钥延期 上映至9月12日](https://ent.sina.com.cn/m/c/2022-08-02/doc-imizirav6459037.shtml)
> 概要: 新浪娱乐讯 由李睿珺执导，武仁林、海清主演的《隐入尘烟》宣布密钥延期，延长上映至9月12日。该片于7月8日上映，目前累计票房1684万。　　该片曾入围今年柏林电影节主竞赛单元，讲述两个被各自家庭抛弃掉......
### [V社在印尼完成注册 Steam已在当地解封](https://www.3dmgame.com/news/202208/3848390.html)
> 概要: 此前报道，印尼政府对Steam，PayPal，战网，Epic等游戏平台进行了封禁，原因是印尼的新法律要求在线平台和服务机构必须申请许可证才能继续在该国运营。分析师ZhugeEX今日在个人社交账号中透露......
### [3DM速报：动视暴雪财报，月活收入齐下滑](https://www.3dmgame.com/news/202208/3848392.html)
> 概要: 来，哥几个把锅分分吧。1、动视暴雪月活收入齐下滑，《暗黑不朽》获高度评价动视暴雪公布了他们2022财年第二季度的公司财报，这一片两位数下跌略有不妙啊，16.4亿美元的收入，跟去年同期的23亿相比，整整......
### [devops-1：代码仓库git的使用](https://www.tuicool.com/articles/A73iuqq)
> 概要: devops-1：代码仓库git的使用
### [小米展示 MiGu 小米头箍，可通过脑电波控制智能家居及进行疲劳驾驶监测](https://www.ithome.com/0/632/884.htm)
> 概要: IT之家8 月 2 日消息，小米公司官微今日宣布，小米集团第三届线上黑客马拉松圆满落幕。其中，“MiGu 小米头箍”项目获得活动一等奖。据小米介绍，MiGu 小米头箍最大的亮点就是实现了通过脑电波控制......
### [王毅：美方在台湾问题上背信弃义，只能使其国家信誉进一步破产](https://www.yicai.com/news/101493376.html)
> 概要: 美国一些政客只顾一己之私，公然在台湾问题上玩火，与14亿中国人民为敌，决不会有好下场。美方的霸凌嘴脸暴露于世，只能让各国人民愈发看清，美国才是当今和平的最大破坏者。
### [亿流量大考（1）：日增上亿数据，把MySQL直接搞宕机了](https://www.tuicool.com/articles/U73MjyM)
> 概要: 亿流量大考（1）：日增上亿数据，把MySQL直接搞宕机了
### [哪些板块品种短期显现防御性优势？](https://www.yicai.com/video/101493358.html)
> 概要: 哪些板块品种短期显现防御性优势？
### [五仁才叫月饼嘛：桃李提浆月饼 19.8 元 1.6 斤（京东 39.8 元）](https://lapin.ithome.com/html/digi/632892.htm)
> 概要: 天猫【桃李食品旗舰店】桃李多口味京式提浆月饼 100g*8 块日常售价为 39.8 元，下单领取 20 元优惠券，到手价为 19.8 元 1.6 斤。天猫桃李京式提浆月饼 8 枚散装共 800g券后 ......
### [腾讯还能等来“准生证”吗？](https://www.huxiu.com/article/624578.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国8 月，腾讯、网易依旧没有等来“准生证”。8 月 1 日，国家新闻出版署公示年内第四批游戏版号，共计 69 款游戏拿到“准生证”。除一款页游（《三国之志 ......
### [理想ONE高速起火烧成车架,比亚迪宣布进德国、瑞典市场](https://www.yicai.com/news/101493406.html)
> 概要: 新能源汽车起火事件频发，引发广大关注。
### [美国经济离衰退有多远](https://www.yicai.com/news/101493439.html)
> 概要: 未来1~2年美国经济出现衰退的风险高，但不大可能在下半年就出现。
### [腾讯创三年新低，中概股怎么投？基金经理态度出现分化](https://www.yicai.com/news/101493469.html)
> 概要: 香港市场的优势愈发明显
### [上虞基地生物酶制剂项目正式投产，雅本化学迎来三个转变](http://www.investorscn.com/2022/08/02/102156/)
> 概要: 8月2日，雅本化学上虞基地暨雅本（绍兴）药业有限公司的生物酶制剂项目正式投产。该项目生产的生物酶制剂，主要用于小分子化合物的酶催化反应，制备各类手性化合物中间体和活性成分，如非天然氨基酸系列、抗病毒药......
### [Pornhub: Judge rules Visa can be sued in abuse claim](https://www.bbc.co.uk/news/technology-62372964)
> 概要: Pornhub: Judge rules Visa can be sued in abuse claim
### [万亿级增值税留抵退税收尾 下一步怎么走？](https://finance.sina.com.cn/china/2022-08-02/doc-imizirav6484037.shtml)
> 概要: 今年留抵退税规模超过2万亿元，7月底新政基本收尾，下一步留抵退税政策如何实施？维持现状适度扩围或将是大趋势。 今年稳经济关键一招，是实施大规模增值税留抵退税新政...
### [房贷提前还款摸底：多数商业银行不收补偿金](https://finance.sina.com.cn/jjxw/2022-08-02/doc-imizmscv4581086.shtml)
> 概要: 来源：证券日报 本报记者 苏向杲 杨洁 近日有银行发布公告称，将于11月1日起，对个人按揭类贷款、个人线上抵押贷（消费）提前还款补偿金收费标准进行调整。
### [绿媒炒作:解放军军机今晚两度进入台西南空域，“空中情势紧张”](https://finance.sina.com.cn/china/2022-08-02/doc-imizmscv4579763.shtml)
> 概要: 【环球网报道】岛内亲绿媒体《自由时报》报道称，美国联邦众议院议长佩洛西将于今（2日）晚抵台。该媒体声称，佩洛西专机目前已经进入菲律宾空域...
### [绿媒炒作：解放军军机今晚两度进入台西南空域，“空中情势紧张”](https://finance.sina.com.cn/money/forex/forexroll/2022-08-02/doc-imizmscv4578639.shtml)
> 概要: 来源：环球网 【环球网报道】岛内亲绿媒体《自由时报》报道称，美国联邦众议院议长佩洛西将于今（2日）晚抵台。该媒体声称，佩洛西专机目前已经进入菲律宾空域...
### [13城二手房指导价政策松绑，什么信号？多地二手房成交复苏，这个城市成交量创近10年新高](https://finance.sina.com.cn/wm/2022-08-02/doc-imizmscv4579099.shtml)
> 概要: 自去年2月深圳率先推出二手房指导价以来，全国共有15个城市发布二手房指导价，但目前，多地松绑了这项政策。据克而瑞研究中心统计...
### [《熊出没：怪兽计划2》定档8月12日，共52集，赵琳肥波还会出场吗？](https://new.qq.com/omn/20220802/20220802A0AH4Y00.html)
> 概要: l 《熊出没：怪兽计划2》定档8月12日，日更四集大家期待已久的《熊出没：怪兽计划2》终于要上映了！            8月2日，《熊出没》官博宣布了《熊出没：怪兽计划2》的定档时间——8月12日......
### [流言板黄喜灿社媒回应种族歧视：感谢大家支持，要向种族歧视说不](https://bbs.hupu.com/54996204.html)
> 概要: 虎扑08月02日讯 狼队的韩国前锋黄喜灿今天在Instagram上发文，感谢人们对他的支持，并呼吁大家一起向种族主义说不。在狼队1-1战平葡萄牙球队法鲁人的友谊赛中，黄喜灿在进球后遭到对手球迷种族歧视
### [流言板有球踢了？德尔加多更新社交媒体：已经准备好了！](https://bbs.hupu.com/54996329.html)
> 概要: 虎扑08月02日讯 今天山东泰山队球员德尔加多更新社交媒体，他发布了自己的训练照片，值得一提的是他在中超第一阶段并未报名泰山一线队。在中超第一阶段暂未报名一线队的德尔加多今天更新社交媒体，他晒出自己的
### [谷歌 Chrome 浏览器 104 正式版发布：加快网页加载，蓝牙 API 改进](https://www.ithome.com/0/632/904.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家8 月 2 日消息，谷歌 Chrome 浏览器104 正式版今日发布，带来了网页加载、蓝牙方面的多项改进。尝试加快页面加载早期的浏览器采用一次加载整个页面......
# 小说
### [我的体内住着恶灵](http://book.zongheng.com/book/877843.html)
> 作者：末尾的ZJ

> 标签：都市娱乐

> 简介：随着科技的发展，世界也悄无声息地发生了变化……一具从天而降的龙骨架，让人类完成了超级基因这项伟大的科研成果，却有些自私的人企图从中谋利，带着不完整的科研技术进行了秘密实验。他的实验，是将人类推向灭亡的起源…“你说，我是不是那种被洗了记忆的秘密特工啊？”“……”“你说，我是不是有着我自己都无法察觉到的超能力啊？”“……”“你说，我是不是会成为拯救世界的关键啊？”“……你充其量算个炮灰。”附身于叶陵身上的恶灵赋予了他强大的治愈能力，却没有给他提升一丁点的战斗力……书友群：822426910

> 章节末：第253章：友谊关系

> 状态：完本
# 论文
### [Adapting Self-Supervised Vision Transformers by Probing Attention-Conditioned Masking Consistency | Papers With Code](https://paperswithcode.com/paper/adapting-self-supervised-vision-transformers)
> 概要: Visual domain adaptation (DA) seeks to transfer trained models to unseen, unlabeled domains across distribution shift, but approaches typically focus on adapting convolutional neural network architectures initialized with supervised ImageNet representations. In this work, we shift focus to adapting modern architectures for object recognition -- the increasingly popular Vision Transformer (ViT) -- and modern pretraining based on self-supervised learning (SSL). Inspired by the design of recent SSL approaches based on learning from partial image inputs generated via masking or cropping -- either by learning to predict the missing pixels, or learning representational invariances to such augmentations -- we propose PACMAC, a simple two-stage adaptation algorithm for self-supervised ViTs. PACMAC first performs in-domain SSL on pooled source and target data to learn task-discriminative features, and then probes the model's predictive consistency across a set of partial target inputs generated via a novel attention-conditioned masking strategy, to identify reliable candidates for self-training. Our simple approach leads to consistent performance gains over competing methods that use ViTs and self-supervised initializations on standard object recognition benchmarks. Code available at https://github.com/virajprabhu/PACMAC
### [DetarNet: Decoupling Translation and Rotation by Siamese Network for Point Cloud Registration | Papers With Code](https://paperswithcode.com/paper/detarnet-decoupling-translation-and-rotation)
> 概要: Point cloud registration is a fundamental step for many tasks. In this paper, we propose a neural network named DetarNet to decouple the translation $t$ and rotation $R$, so as to overcome the performance degradation due to their mutual interference in point cloud registration.
