---
title: 2022-06-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BBMomCub_ZH-CN7715738841_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-06-25 23:04:52
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BBMomCub_ZH-CN7715738841_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [外卖还能卷出新故事？](http://www.woshipm.com/it/5501451.html)
> 概要: 编辑导语：近期，京东也开展了外卖的业务。外卖这个有巨头、经过好几轮充分竞争的红海，也有“新人入局”了。本篇文章据此展开了一系列的讲述，感兴趣的小伙伴们快来一起看看吧。作者：林小白；公众号：鞭牛士原文链......
### [为什么说小程序的商业价值被低估了？](http://www.woshipm.com/it/5501428.html)
> 概要: 编辑导语：小程序伴随着微信，已经悄无声息地融入了我们的日常生活。但即便如此，许多人还是低估了其背后的商业价值。本文阐释了小程序在如今移动互联网的存量博弈背景下存在的意义，并给出了最大化挖掘小程序商业价......
### [短期看流量，中期看供给，长期看品牌](http://www.woshipm.com/it/5501032.html)
> 概要: 编辑导语：所有的机会，都藏在变化里，周期、波动、趋势都是变化，而这些变化会带来失衡，也就是红利。谁先发现这种失衡，谁就能先抓住因为失衡涌现的大量需求和机会。那么，在失衡脉络出现的短期、中期、长期中，我......
### [Pig heart transplant failed as its heart muscle cells died](https://arstechnica.com/science/2022/06/pig-heart-transplant-failed-as-its-heart-muscle-cells-died/)
> 概要: Pig heart transplant failed as its heart muscle cells died
### [经济日报：直播带货有规矩才能走得远](https://finance.sina.com.cn/tech/2022-06-25/doc-imizirav0420684.shtml)
> 概要: 来源：经济日报......
### [“厅局风穿搭”背后的体制内审美哲学](https://www.huxiu.com/article/591026.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水题图、编辑 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。人类对所有物种的......
### [The Composition over Inheritance Principle (2020)](https://python-patterns.guide/gang-of-four/composition-over-inheritance/)
> 概要: The Composition over Inheritance Principle (2020)
### [组图：张曼玉林嘉欣郭富城谢霆锋合影曝光 众人笑对镜头状态好](http://slide.ent.sina.com.cn/star/slide_4_86448_371706.html)
> 概要: 组图：张曼玉林嘉欣郭富城谢霆锋合影曝光 众人笑对镜头状态好
### [好拼！娜塔莉·波特曼为《雷神4》健身10个月](https://ent.sina.com.cn/hlw/2022-06-25/doc-imizmscu8629300.shtml)
> 概要: 新浪娱乐讯 据外媒报道，娜塔莉·波特曼（Natalie Portman）在《雷神4》中扮演女雷神的肌肉令人惊叹，她的健身教练Naomi Pendergast近日在采访中透露了一些细节，波特曼一共训练了......
### [组图：欧阳娜娜《仙剑》赵灵儿扮相好仙 镜头前嘟嘴卖萌俏皮可爱](http://slide.ent.sina.com.cn/star/slide_4_86448_371710.html)
> 概要: 组图：欧阳娜娜《仙剑》赵灵儿扮相好仙 镜头前嘟嘴卖萌俏皮可爱
### [《JOJO的奇妙冒险：群星之战R》预告 齐贝林归来](https://www.3dmgame.com/news/202206/3845492.html)
> 概要: 《JOJO的奇妙冒险：群星之战 重制版》近日公布最新角色预告片。本次我们看到的是系列第一部的主要人物：威廉·A·齐贝林。由于威廉·A·齐贝林是《JOJO的奇妙冒险》的早期人物，意味着他并不使用替身作战......
### [美最高法院推翻堕胎权 科技大佬纷纷反对](https://finance.sina.com.cn/tech/2022-06-25/doc-imizmscu8637144.shtml)
> 概要: 新浪科技讯 北京时间6月25日上午消息，据报道，美国最高法院周五推翻了“罗伊诉韦德案”的裁决，结束了美国宪法自1973年以来赋予女性的堕胎权。在此之后，多位科技大佬相继公开反对......
### [谷歌回应](https://finance.sina.com.cn/tech/2022-06-25/doc-imizirav0445736.shtml)
> 概要: 新浪科技讯 北京时间6月25日上午消息，据报道，当地时间周五，谷歌向公司全体员工发送电子邮件。邮件称，由于美国联邦最高法院推翻了约50年前在罗伊诉韦德(Roe v. Wade)案中确立的基本堕胎权，未......
### [官宣了！《辉夜大小姐》官方发推：最新一季制作决定](https://acg.gamersky.com/news/202206/1494073.shtml)
> 概要: 今日，《辉夜大小姐想让我告白》官方推特宣布了动画剧集新作制作决定。
### [一图读懂！上海低风险区人员去这些城市可自由流动！](https://www.yicai.com/news/101455083.html)
> 概要: 部分省市如海南、山东，甘肃兰州、山西的太原和晋中平遥等，对上海的低风险地区出行人士彻底取消了隔离措施。
### [40城防控政策大调查！这些城市仍在“一刀切”](https://www.yicai.com/news/101455086.html)
> 概要: “不准随意将限制出行的范围由中、高风险地区扩大到其他地区”，“不准对来自低风险地区人员采取强制劝返、隔离等限制措施”。
### [轻小说「魔法使黎明期」第6卷彩页插图公开](http://acg.178.com/202206/450123216512.html)
> 概要: 轻小说「魔法使黎明期」公开了第6卷（最终卷）「深潭の魔術師と杖の魔女」的彩页插图，该卷预计将于2022年7月1日发售。轻小说「魔法使黎明期」由虎走翔创作、岩崎隆插画、静间良纪担任部分角色原案，所属讲谈......
### [卡普空公布《怪物猎人：崛起》原版试玩下架时间](https://www.3dmgame.com/news/202206/3845498.html)
> 概要: 卡普空近日公布《怪物猎人：崛起》试玩版的下架时限。这款试玩版将于北京时间2022年7月1日中午12时下架。NS版和PC版试玩都将按照这一时间执行。不过，此次下架仅限于原版试玩，而6月14日上架的《怪物......
### [漫画「催眠麦克风- side F.P & M＋」第三卷封面公开](http://acg.178.com/202206/450124011406.html)
> 概要: 「催眠麦克风」官方发布了漫画「hypnosismic -Division Rap Battle- side F.P & M＋」第三卷封面图，该单行本第三卷通常版和限定版都将于2022年8月25日发售......
### [「咒术回战」钉崎野蔷薇手办发售](http://acg.178.com/202206/450125202544.html)
> 概要: 良笑社作品「咒术回战」钉崎野蔷薇手办现已正式开订，全长约145mm，主体采用PVC材料制造。该手办定价为9400日元（含税），约合人民币466元，预计于2023年4月发售......
### [国家公园特许经营亟待立规 业内直言地方执行五花八门](https://www.yicai.com/news/101455212.html)
> 概要: “各地特许经营管理办法大多匆忙出台，无法落地实施。特许经营主体、客体、范围、方式等五花八门。”
### [超华科技：全力推进玉林铜箔项目建设，加速高端产能布局谋发展](http://www.investorscn.com/2022/06/25/101475/)
> 概要: 6月23日，据玉林广播电视台报道，玉林市创新资本招商合作模式助力超华科技（002288.SZ）旗下超华玉林基地建设提速，今年有望部分建成投产。超华玉林基地项目共分两期建设，全部建成后可实现年产10万吨......
### [杂志「spoon.2Di」vol.87封面公开](http://acg.178.com/202206/450126989245.html)
> 概要: 近日，动画杂志「spoon.2Di」公开了第87期的封面图，本次封面绘制的角色来自「Paradox Live」和「ヒロインたるもの！」。「Paradox Live」和「ヒロインたるもの！」（告白实行委......
### [乌克兰入盟？一张地图看懂欧盟发展史](https://www.yicai.com/news/101455229.html)
> 概要: 6月23日，欧洲理事会主席米歇尔宣布欧盟峰会同意批准乌克兰和摩尔多瓦为欧盟候选国。米歇尔称，这标志着乌克兰向欧盟迈出了关键一步，但俄罗斯方面并不看好。克里姆林宫称，不希望该进程进一步恶化俄欧关系。根据欧盟相关法律，获得欧盟候选国地位只是加入欧盟的第一步，最复杂且最耗时的是入盟谈判。而同样是欧盟候选国的土耳其，23年还没“毕业”。一图看懂欧盟发展史>>
### [《辉夜大小姐想让我告白》新作动画制定决定！](https://news.dmzj.com/article/74768.html)
> 概要: 《辉夜大小姐想让我告白》新作动画制定决定！
### [拼多多推出电脑网页版](https://www.tuicool.com/articles/uAvmA3z)
> 概要: 拼多多推出电脑网页版
### [Win11 笔记本华为 MateBook D 16/16s 即将登陆欧洲市场：搭载英特尔第 12 代处理器](https://www.ithome.com/0/626/326.htm)
> 概要: IT之家6 月 25 日消息，上个月，华为发布了配备第 12 代英特尔处理器的 MateBook D 16 和 MateBook 16s。这仅适用于中国市场，但华为也计划在欧洲出售这两款产品。这两款笔......
### [邓布利多“输”给了霸王龙](https://www.huxiu.com/article/591476.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国“恐龙”在暑期档票房大战中一骑绝尘。截至6月25日，上映16天的《侏罗纪世界3》票房达到7.09亿元，在暑期档所有已公映影片中，它的票房比2~4名票房总和......
### [漫画《和帅哥一起吃饭》电视剧化决定！](https://news.dmzj.com/article/74769.html)
> 概要: 漫画《和帅哥一起吃饭》电视剧化决定！视觉图与演员信息一并公开。
### [软银孙正义：ARM 最有可能在纳斯达克上市](https://www.ithome.com/0/626/344.htm)
> 概要: IT之家6 月 25 日消息，英国《金融时报》此前曾报道称，政府官员们正在考虑迫使 ARM 在伦敦上市。周三，当被问及“政府是否会利用新的国家安全权力，试图迫使软银旗下的 ARM 在伦敦上市”时，约翰......
### [首个疫苗应对奥密克戎对照疗效公布 预防再感染有效性93.2%](https://www.yicai.com/news/101455313.html)
> 概要: 该疫苗对于已经感染过新冠的人预防再次感染的有效性更强，对整体症状性新冠再感染的有效性为75.1%，对确认感染过奥密克戎的人预防症状性再感染的有效性为93.2%。
### [I Finally Found a Solid Debian Tablet: The Surface Go 2](https://changelog.complete.org/archives/10396-i-finally-found-a-solid-debian-tablet-the-surface-go-2)
> 概要: I Finally Found a Solid Debian Tablet: The Surface Go 2
### [《星露谷物语》还有后续更新 1.6主要为模组支持](https://www.3dmgame.com/news/202206/3845513.html)
> 概要: 近日，《星露谷物语》开发者Eric Barone集中回复了一批粉丝问题，玩家“Claudiu101”询问Eric：“我喜欢《星露谷物语》同时我也等不及要玩《恐怖巧克力工厂》，我能问你两个关于《星露谷物......
### [交通管理游戏《都市车流》登陆Xbox 上市宣传片赏](https://www.3dmgame.com/news/202206/3845514.html)
> 概要: Baltoro Games曾宣布交通管理解谜游戏《都市车流》（Urban Flow）将登陆多平台。如今，这款游戏已经在Xbox平台发售。在《都市车流》中，玩家需要管理繁忙都市十字路口中的交通信号灯。每......
### [一岗难求和两人坠亡，试车员职业的红与黑](https://www.huxiu.com/article/591566.html)
> 概要: 本文来自微信公众号：盒饭财经 （ID：daxiongfan），作者：刘星志，编辑：赵晋杰，题图来自：视觉中国试车员这一冷门职业，以一种谁都不愿见到的方式被大众熟知。2022年6月22日17时22分许，......
### [俄罗斯航天集团：美国载人“龙”飞船计划 9 月 1 日发射，俄女宇航员或被列入乘组](https://www.ithome.com/0/626/353.htm)
> 概要: IT之家6 月 25 日消息，据俄罗斯卫星通讯社报道，俄罗斯国家航天集团公司通过 Telegram 频道发布消息称，安娜∙基基娜或被列入美国载人“龙”飞船乘组，该飞船计划于 9 月 1 日发射。IT之......
### [民航局：到 2025 年我国将初步建成安全、智慧、高效和绿色的航空物流体系](https://www.ithome.com/0/626/356.htm)
> 概要: IT之家6 月 25 日消息，据央视新闻报道，从民航局了解到，到 2025 年，我们将初步建成安全、智慧、高效和绿色的航空物流体系。IT之家了解到，民航局运输司司长梁楠表示，这具体表现在四个方面：航空......
### [12小强喜欢什么食物？鸣人最爱一乐拉面，只有他不挑食？](https://new.qq.com/omn/20220625/20220625A06S8700.html)
> 概要: 在动漫火影忍者中，木叶村十二小强是粉丝们命名的组合，作为木叶村新生代最有人气的忍者，他们也有自己的喜好，那么对于木叶十二小强而言，他们最喜欢吃的食物是什么呢？他们最讨厌的食物又分别是什么呢？     ......
### [历代火影之中，为什么只有两个人会火遁？火影之名与火遁无关](https://new.qq.com/omn/20220625/20220625A06S1Q00.html)
> 概要: 动漫火影忍者对于很多人来说都可以算是青春的回忆，而在这部动漫之中，每个村子里最强的忍者会被选为村子的影，而这个影也可以说是村长一样的存在，而在木叶村中，最强的也就是村子里的火影了。          ......
### [Papers on GitHub Copilot, copyright law, and ownership for AI-generated code](https://www.fsf.org/licensing/copilot)
> 概要: Papers on GitHub Copilot, copyright law, and ownership for AI-generated code
### [踏上E时代新征程，北汽瑞翔新能源战略发布](http://www.investorscn.com/2022/06/25/101476/)
> 概要: 6月25日，北汽瑞翔在2022(第二十四届)重庆国际汽车展览会正式发布了以“聚势启新，拥抱未来”为主题的新能源战略。该战略的发布和品牌首款新能源产品博腾V2 EV的产品细节与价格让现场观众充满惊喜，同......
### [斗罗214集：胡列娜邪月合体真容曝光，T部线条太绝，妥妥秀身材](https://new.qq.com/omn/20220625/20220625A07P4M00.html)
> 概要: 斗罗大陆214集今天已经更新了，本集算是小舞彻底复活的一集，除此之外，在本集中还有一些额外的关注点，那就是武魂殿黄金一代的画面，胡列娜和邪月两人的合体建模也在本集中曝光了出来，算得上是小舞复活之外的另......
### [广电5G又一大动作,6月27日或将迎来192商用历史性时刻?](http://www.investorscn.com/2022/06/25/101477/)
> 概要: 继6月6日品牌正式亮相后,广电5G又有大事要宣布。有消息称,6月27日,广电5G将举行网络服务启动仪式,全新192号段或将迎来首批投用......
### [视觉设计师求职指南 | 面试技巧篇](https://www.tuicool.com/articles/6Vfy2mi)
> 概要: 视觉设计师求职指南 | 面试技巧篇
### [羽毛球动画《Love All Play》后半季新PV公开！](https://news.dmzj.com/article/74770.html)
> 概要: 羽毛球动画《Love All Play》最新后半季主视觉图公开！
### [《约会大作战 第五季》、《辉夜大小姐》新作制作决定，最近一些动漫资讯](https://new.qq.com/omn/20220625/20220625A08ILZ00.html)
> 概要: 四月新番已经陆续完结了，来看看最近一周时间又有哪些新作动画化的消息吧~《秋叶原冥途战争》制作决定又是一部联动企划的原创作品，由游戏公司Cygames和动画制作公司P.A.WORKS共同企划。从宣传PV......
### [是否布局“造手机”？上汽相关人士：手机和车机融合有深度思考](https://finance.sina.com.cn/jjxw/2022-06-25/doc-imizirav0530306.shtml)
> 概要: 上汽集团（600104）正深度聚合乘用车研发能力，未来有望大幅提高整个集团研发投入转化效率。 6月24日，上汽集团创新研究开发总院（下称“研发总院”）常务副院长芦勇在媒体沟...
### [谷歌灵异算法事件，荒诞却又可悲](https://www.huxiu.com/article/591691.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自视觉中国如果把这看做是一场精彩的营销事件，那么两周来，谷歌人工智能大模型LaMDA引发的滔天舆论巨浪，已经完胜三年来苦心向大众普及大模型的 OpenAI 以及微......
### [《银翼杀手：增强版》Steam多半差评](https://www.3dmgame.com/news/202206/3845522.html)
> 概要: 《银翼杀手：增强版》现已登陆了Steam等平台发售，截止到目前，该作在Steam上获得了“多半差评”，好评率只有27%。而外媒PC Gamer也表示，《银翼杀手：增强版》是一次灾难，而非复刻。PC G......
### [陈文健与石家庄市委书记张超超、市长马宇骏举行会谈](https://finance.sina.com.cn/china/gncj/2022-06-25/doc-imizirav0533403.shtml)
> 概要: 证券时报e公司讯，6月24日上午，中国中铁总裁、党委副书记陈文健与石家庄市委书记张超超、市长马宇骏举行会谈。双方就进一步深化务实合作...
### [海南推出首届国际离岛免税购物节 促离岛免税销售恢复增长](https://finance.sina.com.cn/jjxw/2022-06-25/doc-imizirav0533349.shtml)
> 概要: 中新网海口6月25日电 （符宇群）海南官方25日召开新闻发布会介绍，为加快推动海南离岛免税销售快速恢复增长，政府和各免税经营主体...
### [台湾花莲县发生4.9级地震 震源深度7千米](https://finance.sina.com.cn/jjxw/2022-06-25/doc-imizmscu8724403.shtml)
> 概要: 中国地震台网正式测定：6月25日21时34分在台湾花莲县（北纬23.63度，东经121.46度）发生4.9级地震，震源深度7千米。 （总台央视记者 张腾飞）
### [摩根大通：矿工抛售BTC或将持续到2022年第三季度](https://finance.sina.com.cn/roll/2022-06-25/doc-imizirav0534545.shtml)
> 概要: 财联社6月25日电，由 Nikolaos Panigirtzoglou 领导的摩根大通策略团队发布报告称，如果比特币矿工盈利能力未能提高，为了满足持续成本或去杠杆化将会选择持续抛售比特币...
### [流言板罗马诺：J罗至今未接受巴甲球队报价，仍梦想重回欧洲](https://bbs.hupu.com/54442392.html)
> 概要: 虎扑06月25日讯 据知名记者罗马诺报道，J罗仍然梦想再次回到欧洲踢球，因此他至今仍没有接受巴甲球队博塔弗戈的邀请。此前，有消息称巴甲的博塔弗戈有意引进哥伦比亚球员哈梅斯-罗德里格斯，但罗马诺表示，J
### [一图流三进三出直捣黄龙！盲僧R闪扭转劣势团战](https://bbs.hupu.com/54442408.html)
> 概要: 一图流三进三出直捣黄龙！盲僧R闪扭转劣势团战
### [流言板罗林斯：准备好向库里普尔学习了，勇士曾想换30号签选我](https://bbs.hupu.com/54442498.html)
> 概要: 虎扑06月25日讯 勇士44号签选择的新秀莱恩-罗林斯接受了采访。罗林斯表示，他是麦科勒姆和布克的忠实粉丝，并且已经准备好向库里和普尔去学习了。罗林斯还表示，选秀大会时，勇士一度想通过交易得到30号签
### [“钢琴媛”再翻红：穿成这样，真让人把持不住](https://www.tuicool.com/articles/QvUZraU)
> 概要: “钢琴媛”再翻红：穿成这样，真让人把持不住
### [LPL群访Tian：最近休息得不是很好，下一场我们要展现出更好的状态](https://bbs.hupu.com/54442678.html)
> 概要: 虎扑06月25日讯 2022LPL夏季赛常规赛，TES以2-1战胜FPX后，TES全体成员接受媒体群访，内容如下：Q：提问教练。觉得第二局失利的原因是？以及面对第三局做出了怎样的调整？A：第二局前期我
### [折戟618，手机为什么卖不动了？](https://www.tuicool.com/articles/VnABN32)
> 概要: 折戟618，手机为什么卖不动了？
# 小说
### [十王座](http://book.zongheng.com/book/868939.html)
> 作者：光痕

> 标签：奇幻玄幻

> 简介：命运之星，十位王者。力量、智慧、速度、命运、元素、毁灭、精神、生命、天使、恶魔。王者野心，猩红獠牙，力量陨落，智慧、命运不知所踪，穹宇之下，暗潮涌动。王之姓氏，扑朔迷离，异魔灾祸，双生双灭，谁能打开命运之门？踏破命运彼岸，歌颂王之乐章！

> 章节末：第七百零四章.不需要王者的星球

> 状态：完本
# 论文
### [Study of positional encoding approaches for Audio Spectrogram Transformers | Papers With Code](https://paperswithcode.com/paper/study-of-positional-encoding-approaches-for)
> 概要: Transformers have revolutionized the world of deep learning, specially in the field of natural language processing. Recently, the Audio Spectrogram Transformer (AST) was proposed for audio classification, leading to state of the art results in several datasets.

..

However, in order for ASTs to outperform CNNs, pretraining with ImageNet is needed. In this paper, we study one component of the AST, the positional encoding, and propose several variants to improve the performance of ASTs trained from scratch, without ImageNet pretraining. Our best model, which incorporates conditional positional encodings, significantly improves performance on Audioset and ESC-50 compared to the original AST.

read more
### [Markedness in Visual Semantic AI | Papers With Code](https://paperswithcode.com/paper/markedness-in-visual-semantic-ai)
> 概要: We evaluate the state-of-the-art multimodal "visual semantic" model CLIP ("Contrastive Language Image Pretraining") for biases related to the marking of age, gender, and race or ethnicity. Given the option to label an image as "a photo of a person" or to select a label denoting race or ethnicity, CLIP chooses the "person" label 47.9% of the time for White individuals, compared with 5.0% or less for individuals who are Black, East Asian, Southeast Asian, Indian, or Latino or Hispanic. The model is more likely to rank the unmarked "person" label higher than labels denoting gender for Male individuals (26.7% of the time) vs. Female individuals (15.2% of the time). Age affects whether an individual is marked by the model: Female individuals under the age of 20 are more likely than Male individuals to be marked with a gender label, but less likely to be marked with an age label, while Female individuals over the age of 40 are more likely to be marked based on age than Male individuals. We also examine the self-similarity (mean pairwise cosine similarity) for each social group, where higher self-similarity denotes greater attention directed by CLIP to the shared characteristics (age, race, or gender) of the social group. As age increases, the self-similarity of representations of Female individuals increases at a higher rate than for Male individuals, with the disparity most pronounced at the "more than 70" age range. All ten of the most self-similar social groups are individuals under the age of 10 or over the age of 70, and six of the ten are Female individuals. Existing biases of self-similarity and markedness between Male and Female gender groups are further exacerbated when the groups compared are individuals who are White and Male and individuals who are Black and Female. Results indicate that CLIP reflects the biases of the language and society which produced its training data.
