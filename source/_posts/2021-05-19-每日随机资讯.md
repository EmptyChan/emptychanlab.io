---
title: 2021-05-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RoanRhododendron_EN-CN5959825452_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-19 22:41:06
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RoanRhododendron_EN-CN5959825452_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Android 12 Beta 1 发布，改善 UI 和隐私，引入设备高性能标准](https://www.oschina.net/news/142098/android-12-beta-1-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>在今天的 Google I/O 大会上，Google 发布了 Android 12 的第一个测试版。Android 12 Beta 对 ......
### [Android 版 Chrome 将可以一键修改外泄密码](https://www.oschina.net/news/142190/chrome-automated-password-changes)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Google 计划在 Android 版 Chrome 浏览器中启动一项新功能，以帮助用户一键轻松修改因数据泄露而外泄的密码。根据官方所......
### [WebRTC 的现状和未来：专访 W3C WebRTC Chair Bernard Aboba](https://www.oschina.net/news/142177/webrtc-today-tomorrow-bernard-aboba-qa)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>WebRTC 无疑推动和改变了互联网视频，而这仅仅是刚刚开始，除了大家熟悉的 WebRTC-PC、Simulcast 和 SVC，有太多......
### [Android Studio Arctic Fox (2020.3.1) Beta 发布，启用新的版本号命名方案](https://www.oschina.net/news/142143/android-studio-arctic-fox-beta)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>谷歌发布了 Android Studio Arctic Fox (2020.3.1) 首个 Beta 版本，如果按照旧的版本号方案来命名......
### [Project Starline: Feel like you're there, together](https://blog.google/technology/research/project-starline/)
> 概要: Project Starline: Feel like you're there, together
### [近3万字游戏报告：谁能挑战腾讯？](https://www.huxiu.com/article/428830.html)
> 概要: 来源｜互联网怪盗团（ID：TMTphantom）作者｜裴培头图｜IC photo在2021年4月5日发布的深度研究报告《诸神之黄昏》当中，互联网怪盗团深入讨论了互联网行业的未来——移动流量红利的耗尽，......
### [鸿蒙系统UI标准：HarmonyOS中的字体标准](https://www.tuicool.com/articles/q6Zzueb)
> 概要: 想了解更多内容，请访问：51CTO和华为官方战略合作共建的鸿蒙技术社区https://harmonyos.51cto.com通过研究用户在不同场景下对多终端设备的阅读反馈，HarmonyOS 形成了独......
### [视频：电影《音为有梦》兼具两大潮爆元素 金志文透露与影片缘分](https://video.sina.com.cn/p/ent/2021-05-19/detail-ikmxzfmm3306127.d.html)
> 概要: 视频：电影《音为有梦》兼具两大潮爆元素 金志文透露与影片缘分
### [心灵治愈之旅完成！《水豚汤馆》动画温暖收官](https://new.qq.com/omn/ACF20210/ACF2021051900346000.html)
> 概要: 由腾讯动漫出品，启缘映画制作的清甜系动画《水豚汤馆》动画迎来了完美收官，开创了让人眼前一亮的国漫新风格。《水豚汤馆》动画是一部主打激萌治愈的短篇泡面番，画风清甜软糯，人物设定蠢萌可爱。动画开播当天就登......
### [《哆啦A梦：伴我同行2》新预告公开 奶奶的心愿](https://acg.gamersky.com/news/202105/1389395.shtml)
> 概要: 《哆啦A梦》50周年纪念电影《哆啦A梦：伴我同行2》，在今天（5月19日）公开了“奶奶的心愿”版预告和新海报。奶奶的心愿一直都是：希望大雄能获得幸福。
### [Steam《巫师》系列特惠：《王权的陨落》新史低29元](https://www.3dmgame.com/news/202105/3814737.html)
> 概要: 目前，Steam商城已经开启了“庆祝Playism10周年”特卖活动、“巫师系列”开发商特卖活动，此外，《赛博朋克2077》也有优惠，一起来了解一下此次公开的打折促销细节。“巫师系列”开发商特卖活动：......
### [年轻人开始“反算法”](https://www.huxiu.com/article/428842.html)
> 概要: 本文作者：王敏，编辑：向小园，头图来自：视觉中国“算法不讲武德！”越来越多年轻人发觉，生活正在被算法控制。从事互联网运营的卢锋，想要搜索某个科技产品的特性及用户使用体验，但打开知乎，还没来得及输入关键......
### [漫画「赛马娘：芦毛灰姑娘」累计销量破50万部](http://acg.178.com/202105/415396565506.html)
> 概要: 漫画「赛马娘：芦毛灰姑娘」的第三卷于今日（5月19日）发售，本作的累计销量（包括电子版）现已突破50万部。「赛马娘：芦毛灰姑娘」是Cygames手机游戏「赛马娘」的衍生漫画作品，相关创作信息如下：作者......
### [广东总决赛23记3分集锦:赵睿领衔 徐杰每场都进 有机会就要大胆投！](https://bbs.hupu.com/42988065.html)
> 概要: 广东总决赛23记3分集锦:赵睿领衔 徐杰每场都进 有机会就要大胆投！
### [迪士尼真人电影《黑白魔女库伊拉》官宣引进内地！](https://news.dmzj1.com/article/70930.html)
> 概要: 迪士尼真人电影《黑白魔女库伊拉》确认引进内地，将于近期上映！
### [「有谁规定了在现实中不能有恋爱喜剧？」6月25日开始连载](http://acg.178.com/202105/415404053643.html)
> 概要: 漫画「有谁规定了在现实中不能有恋爱喜剧？」将于6月25日在「月刊BIG GANGAN」开始连载，由カタケイ老师进行绘制。漫画「有谁规定了在现实中不能有恋爱喜剧？」改编自初鹿野創所著同名轻小说，轻小说插......
### [【直播预告】期待已久的恐怖游戏终于可以开始了！](https://news.dmzj1.com/article/70931.html)
> 概要: 本周直播预告
### [组图：佟丽娅深夜与三位帅哥出门遛弯 长发披肩穿着风衣气场强大](http://slide.ent.sina.com.cn/star/slide_4_86512_356798.html)
> 概要: 组图：佟丽娅深夜与三位帅哥出门遛弯 长发披肩穿着风衣气场强大
### [漫画「摇曳百合」作者なもり画业15周年纪念展海报公开](http://acg.178.com/202105/415405680051.html)
> 概要: 漫画「摇曳百合」、「大室家」作者なもり画业15周年纪念作品展将于6月5日起正式开办，官方公开了该活动的宣传海报。「摇曳百合」是なもり所作，一迅社出版，先后连载于「Comic百合姬S」和「Comic百合......
### [《狐妖小红娘》担任“山西文化探索官” 开启山西文物探索之旅](https://new.qq.com/omn/ACF20210/ACF2021051900607600.html)
> 概要: 近日，由企鹅影视、腾讯动漫联合出品的动画《狐妖小红娘》与山西省文化旅游厅、山西省文物局、太原市人民政府共同开展“100件文物读中华文明史”跨界合作，并正式宣布《狐妖小红娘》动画女主角涂山苏苏担任“山西......
### [组图：金莎戴蕾丝边贝雷帽复古摩登 黑色纱裙配丝袜穿搭个性十足](http://slide.ent.sina.com.cn/tv/slide_4_704_356807.html)
> 概要: 组图：金莎戴蕾丝边贝雷帽复古摩登 黑色纱裙配丝袜穿搭个性十足
### [岛国《拳皇98》手游5年前虚标扭蛋概率 如今才遭罚款](https://www.3dmgame.com/news/202105/3814760.html)
> 概要: 扭蛋扭出人生暴死，在游戏圈这种必不可少的桥段屡屡上演，近日据日媒报道，揭开了一段扭蛋界不太久远的轶事，2016年时岛国《拳皇98》手游在限时扭蛋活动中因为虚标扭蛋概率，5年后的近日才遭到日本政府609......
### [爱奇艺CEO称会费涨价是行业必然趋势 网友:不用也罢](https://www.3dmgame.com/news/202105/3814762.html)
> 概要: 去年11月，爱奇艺率先宣布VIP会员价格整体上调计划并开始实施，随后腾讯视频和芒果TV等国内平台跟风涨价。每次会费上涨也意味着视频网站又要被网友们吐槽。在新浪科技4月份做的一份“你认为视频网站会员费涨......
### [组图：黄圣依清凉上阵玩转oversize风 下衣失踪秀美腿](http://slide.ent.sina.com.cn/tv/slide_4_704_356812.html)
> 概要: 组图：黄圣依清凉上阵玩转oversize风 下衣失踪秀美腿
### [面试官：从 MySQL 数据库里读取 500w 数据行进行处理，应该怎么做更效益化？](https://www.tuicool.com/articles/Zvi2yyi)
> 概要: 推荐学习错过“金三银四”的Java程序员面试有多苦逼！机会只留给有准备的人！Mybatis面试题（2021最新版）MySQL优化面试题（2021最新版）前言由于现在 ORM 框架的成熟运用，很多小伙伴......
### [杨紫：国民闺女人设翻车？用纸巾匆忙盖住私人物品，形状引发争议](https://new.qq.com/omn/20210519/20210519A0777B00.html)
> 概要: 不得不承认，吃瓜是一件令人非常开心的事情，特别是吃别人的瓜那就更乐呵了。史上最为壮观的吃瓜日，估计是被称为“内娱辟谣日”的1月22日，这一天我们见证了二十多位明星发声辟谣。赵丽颖、冯绍峰、杨幂、王俊凯......
### [漫威双倍特长单刊「星球级X战警」封面及无字预览公开](http://acg.178.com/202105/415410249942.html)
> 概要: 近日，漫威漫画放出了大型联动故事“地狱火晚宴”其中的双倍特长单刊「星球级X战警」的封面及无字预览。本刊的编剧工作由Gerry Duggan担任，由Pepe Larraz负责绘画，本期将于6月16日正式......
### [众筹17天破千万 国产动画《灵笼》引爆5月！](https://acg.gamersky.com/news/202105/1389646.shtml)
> 概要: 由武汉艺画开天打造的国内首部末日幻想动画《灵笼》终章和特别篇自五月上线以来，就在社交媒体引发了火热讨论。
### [耗时半年！网友用剪纸和磁铁再现《我的世界》场景](https://news.dmzj1.com/article/70934.html)
> 概要: 《我的世界》是一款使用3D方块建造自己的世界的游戏。近日，有着7年此游戏经验的推特用户Kami使用剪纸和磁铁，在现实世界中再现了游戏中的场景。
### [重新出发的B站：后浪如风起，二次元割舍“11年”](https://www.tuicool.com/articles/3MzMzun)
> 概要: 点击蓝字 ·关注我们重要的并非二次元标签，而在于二次元“社群”。作者| 包蕴涵编辑| 马志学 大成3月29日，是B站回港上市的日子。在庆典上，B站董事长兼CEO陈睿带给观众的感觉，就像一团冷静的火焰......
### [Steam出现疑似冒充官方的《辉夜大小姐想让我告白》游戏](https://news.dmzj1.com/article/70935.html)
> 概要: 近日，有网友发现在Steam上有一款《辉夜大小姐想让我告白》的文字冒险游戏将于6月1日发售。不过在看了这部作品的介绍后，有不少人质疑这可能是一部冒充官方招摇撞骗的作品。
### [组图：林志玲夫妇东京被偶遇 搂腰依偎丈夫怀中露幸福笑容](http://slide.ent.sina.com.cn/star/slide_4_86512_356823.html)
> 概要: 组图：林志玲夫妇东京被偶遇 搂腰依偎丈夫怀中露幸福笑容
### [微信又来一波改版，拍一拍能“扔炸弹”了？](https://www.huxiu.com/article/428959.html)
> 概要: 本文来自微信公众号：运营研究社（ID：U_quan），作者：丽言、仵静文，设计：瓜瓜，题图来自：视觉中国今天，我在工作群里“拍了拍”新来的编辑小姐姐，谁知道场面一度“爆炸”。诺，就是这样：好家伙，拍一......
### [调查：超过半数澳大利亚人认为马斯克发明了比特币](https://www.ithome.com/0/552/373.htm)
> 概要: 5 月 19 日消息，据国外媒体报道，根据澳大利亚一家网站 Finder 最新调查显示，澳洲人对加密货币的了解还远知之甚少，竟有一半的人认为是埃隆・马斯克创造了比特币。据报道，Finder 对 100......
### [Automatically Make Unit Tests](http://wiki.call-cc.org/eggref/5/make-tests)
> 概要: Automatically Make Unit Tests
### [祝融号传回火星照片](https://www.ithome.com/0/552/378.htm)
> 概要: IT之家5 月 19 日消息 国家航天局发布我国首次火星探测天问一号任务探测器着陆过程两器分离和落火影像。图像中，着陆平台驶离坡道以及祝融号火星车太阳翼、天线等机构展开正常到位。目前，火星车正在开展驶......
### [新车牌设计前瞻：公安部申请多款机动车牌照外观专利](https://www.ithome.com/0/552/379.htm)
> 概要: IT之家5 月 19 日消息 企查查 App 显示，公安部交通管理科学研究所近日公开了两项“小型汽车号牌”外观设计专利，申请日期为 2020 年 11 月 9 日。在两项专利中，公安部交通管理科学研究......
### [Poor in Tech](http://megelison.com/poor-in-tech)
> 概要: Poor in Tech
### [Android 又更新了，但谁又在乎呢？](https://www.tuicool.com/articles/Vj6ZJbY)
> 概要: 一年一度的 Google I/O 如期而至。在疫情影响，所有大型活动都变成线上的情况下，开发者大会—这一本就不是面向所有消费者的活动，就更不可能有太大的声音了。按照以往惯例，Google 会在会上发布......
### [明星背后的造型师，真能化腐朽为神奇？](https://new.qq.com/omn/20210519/20210519A0D6ZS00.html)
> 概要: 本文来自微信公众号“灵魂有香气的女子”经授权转载大家有没有发现，从结婚生子到离婚，曾经被说“土”的圆脸小姑娘赵丽颖，整个人气质和以前大不一样，越来越美了？离婚后的她在微博许愿“愿未来更好”，最近还真是......
### [3DM轻松一刻第524期 短裙少女的残影拳真厉害](https://www.3dmgame.com/bagua/4576.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！这要怎么清洗呢awsl终于可以解放双手了护驾！护驾......
### [新 iPad Pro 首发体验：距离完美，只差…](https://www.huxiu.com/article/429011.html)
> 概要: Pro DisPlay XDR，苹果产品线中，最贵，显示素质最好的屏幕，售价 39999 元起。支架还得单收 7999 。现在，你只需要 8499 元，就可以在新款 12.9 寸 iPad Pro 上......
### [日跌 25%，比特币跌破 35000 美元 / 枚关口](https://www.ithome.com/0/552/401.htm)
> 概要: IT之家5 月 19 日消息 在上周多次大跳水之后，比特币的价格近日持续走低，今日连续跌破九次大关，目前已跌破 32000 美元关口，市值也缩水到了 6100 亿美元，离最高时的 1 万亿美元之上已翻......
### [Crypto Crash Deepens](https://www.reuters.com/business/global-markets-wrapup-4-2021-05-19/)
> 概要: Crypto Crash Deepens
### [最新求职参考来了：2020年平均工资出炉  IT业年平均工资最高超17万](https://finance.sina.com.cn/china/2021-05-19/doc-ikmyaawc6324088.shtml)
> 概要: 原标题：最新求职参考来了！2020年平均工资出炉，这个行业“最香”：177544元！ 钱袋子是老百姓最关注的话题。去年疫情冲击下，全国的平均工资是涨了还是跌了？
### [广州“十四五”规划纲要发布：2025年冲刺3.5万亿，推动战略性新兴产业“首要工程”](https://finance.sina.com.cn/china/dfjj/2021-05-19/doc-ikmxzfmm3463016.shtml)
> 概要: 原标题：广州“十四五”规划纲要发布：2025年冲刺3.5万亿，推动战略性新兴产业“首要工程” 作者：李振，陈洁 5月19日，《广州市国民经济和社会发展第十四个五年规划和2035年远...
### [《尚气》男主刘思慕登国外杂志封面 写真美图欣赏](https://www.3dmgame.com/news/202105/3814786.html)
> 概要: 《尚气与十戒传奇》男主刘思慕登上《男士健康》杂志6月刊封面，成为该杂志史上第一个登上封面的东亚演员和模特。《尚气与十戒传奇》为漫威电影宇宙第25部电影作品，属于漫威电影宇宙第四阶段，由德斯汀·克里顿执......
### [监管互联网贷款“没有最严、只有更严” ：江西推首个地方细则 其他省或跟进](https://finance.sina.com.cn/china/dfjj/2021-05-19/doc-ikmyaawc6327867.shtml)
> 概要: 监管互联网贷款“没有最严、只有更严” ：江西推首个地方细则 其他省或跟进 郭子硕 来源： 时代周报地方加码监管互联网贷款业务 地方加码监管互联网贷款业务。
### [BTC反弹至35000 USDT](https://www.btc126.com//view/166496.html)
> 概要: 火币行情显示，BTC反弹至35000USDT，现报35274.96 USDT，24H跌幅18.16%......
### [流言板拜仁官方：戈雷茨卡下赛季将改穿8号球衣](https://bbs.hupu.com/42997165.html)
> 概要: 虎扑05月19日讯 据拜仁慕尼黑官方今天的最新消息，球队中场大将戈雷茨卡将会在下赛季改穿8号球衣，不再身披此前的18号战袍。8号球衣的主人目前是哈维-马丁内斯，而他将会在效力9年之后于今年夏天正式离开
### [关于求婚，林峯意想不到陈建斌直接，秦海璐取消婚礼感动王新军](https://new.qq.com/omn/20210519/20210519A0E80T00.html)
> 概要: 绝大多数女孩子，都看重仪式感，尤其是在男朋友求婚的时候。毕竟，谁都期盼那个特别的日子充满浪漫和温馨。《妻子的浪漫旅行》最新一期，妻子团们聊到了她们当年的的求婚仪式和婚礼。陈建斌妥妥的直男简单直接，林峯......
### [新晋“宇宙中心”曹县真的牛：棺材大县，汉服基地还是淘宝村集群](https://finance.sina.com.cn/china/gncj/2021-05-19/doc-ikmxzfmm3466271.shtml)
> 概要: 原标题：新晋“宇宙中心”曹县真的牛：棺材大县，汉服基地还是淘宝村集群 记者 | 昝慧昉 编辑 | 牙韩翔 猝不及防地，曹县火了。 “宇宙中心曹县”“宁睡曹县一张床...
### [流言板弗格森对话内维尔：始终尊重利物浦；只有冒险才能成功](https://bbs.hupu.com/42997204.html)
> 概要: 虎扑05月19日讯 近日，曼联传奇主教练弗格森爵士接受天空体育专访，与昔日弟子加里-内维尔进行对话。在采访中，弗格森提到自己对利物浦始终保持尊重，同时他认为自己成功的秘诀是愿意不断冒险。为什么你曾经多
### [接货现场堆着上亿现金 深喉告诉你庄股接盘有多暴利！](https://finance.sina.com.cn/roll/2021-05-19/doc-ikmyaawc6328565.shtml)
> 概要: 原标题：接货现场堆着上亿现金，深喉告诉你庄股接盘有多暴利！ 掩护庄股出货时，接盘方向庄家收取的费用，达到交易额的6%至8%。为了打消接盘方顾虑...
### [浙江卫视一哥豪宅谁最牛？沈涛豪宅堪比宫殿，华少让人叹为观止](https://new.qq.com/omn/20210519/20210519A0A70300.html)
> 概要: 作为浙江卫视的当家主持，华少一直在主持界享有盛誉。早年曾与朱丹成为最佳拍档，在当时可谓是非常养眼的一对俊男美女。大家除了对他在我爱记歌词中留有印象之外，真正让华少一炮而红的。就是在中国好声音中，凭借4......
### [伊布晒恢复训练视频：我没说结束，那就不算结束](https://bbs.hupu.com/42997337.html)
> 概要: 虎扑05月19日讯 伊布在个人社交媒体上晒出自己进行恢复训练的视频，并表示：“我没说结束，那就不算结束。”伊布此前扭伤了左膝，瑞典主帅随后确认伊布将伤缺约6周时间，因此错过今夏的欧洲杯。今年39岁的伊
### [黑色、有色齐回调 疯狂的商品牛市熄火了吗？](https://finance.sina.com.cn/roll/2021-05-19/doc-ikmyaawc6329327.shtml)
> 概要: 原标题：黑色、有色齐回调，疯狂的商品牛市熄火了吗？ 在连续猛涨了一个多月后，大宗商品价格上周出现了大幅回撤，本周黑色、有色品种持续震荡。
### [Coinbase：正在解决余额查询、交易的问题，资金安全](https://www.btc126.com//view/166505.html)
> 概要: Coinbase Support发推称：我们知道用户在登录、查询余额和交易时遇到一些问题。我们的团队正在努力尽快解决此问题，Coinbase账户的资金是安全的......
### [O3Labs VP Product Tim：针对性解决主流区块链流动性割裂问题，O3Swap让用户跨链资产交易不再困难](https://www.btc126.com//view/166507.html)
> 概要: 2021年05月19日晚，由Gate.io主办的直播专访节目《酒局币赴》邀请到O3Labs VP Product Tim直播分享近期最新发展。直播期间Tim与Gate.io立春就O3Labs及其相关事......
### [DeFi 总锁仓量距离最高点已下跌 37%](https://www.btc126.com//view/166508.html)
> 概要: 据 DeBank 数据显示，全网DeFi总锁仓量距离 5 月 11 日的最高点 1319.8 亿美元已跌去近 500 亿美元，下跌幅度为 37%，当前全网 DeFi 总锁仓量为 830.6 亿美元......
# 小说
### [混乱世界当根葱](http://book.zongheng.com/book/960588.html)
> 作者：醉水公子

> 标签：科幻游戏

> 简介：一个游戏角色为一根普通葱的男人的故事，而且游戏不是打打杀杀而是人情世故。

> 章节末：第四百九十三章  真相与新未来（全书完）

> 状态：完本
# 论文
### [Generating a Doppelganger Graph: Resembling but Distinct](https://paperswithcode.com/paper/generating-a-doppelganger-graph-resembling)
> 日期：23 Jan 2021

> 标签：GRAPH REPRESENTATION LEARNING

> 代码：https://github.com/yizhidamiaomiao/DoppelgangerGraph

> 描述：Deep generative models, since their inception, have become increasingly more capable of generating novel and perceptually realistic signals (e.g., images and sound waves). With the emergence of deep models for graph structured data, natural interests seek extensions of these generative models for graphs.
### [Bridging Adversarial and Statistical Domain Transfer via Spectral Adaptation Networks](https://paperswithcode.com/paper/bridging-adversarial-and-statistical-domain)
> 日期：25 Feb 2021

> 标签：DOMAIN ADAPTATION

> 代码：https://github.com/ChristophRaab/ASAN

> 描述：Statistical and adversarial adaptation are currently two extensive categories of neural network architectures in unsupervised deep domain adaptation. The latter has become the new standard due to its good theoretical foundation and empirical performance.
