---
title: 2022-09-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.JohnstonWater_ZH-CN3121890365_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-09-29 22:39:03
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.JohnstonWater_ZH-CN3121890365_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [日活超7.8亿的小程序，网购“刺客”潜伏](https://www.woshipm.com/it/5628240.html)
> 概要: 日活超7.8亿的小程序，居然也存在卷款跑路的风险。微信小程序“易联购线上商城”低价预售苹果手机，却在收到大量订单的钱款后跑路，许多人被骗。本篇文章分析了相关问题，感兴趣的话一起来看看吧。同样是买iPh......
### [小红书上的AAA天团，消灭了多少爹味？](https://www.woshipm.com/it/5628217.html)
> 概要: 不知道大家是否刷到过“AAA组织”？谁能想到曾经一直被我们吐槽的爹味发言，竟然有这么一天，也能成为年轻人们之间心照不宣的流行了。小红书上的AAA天团，消灭了许多爹味，可谓魔法打败魔法，感兴趣的话一起来......
### [小红点篇 | 用好这招，让用户的触达率大幅度提升！](https://www.woshipm.com/pd/5627503.html)
> 概要: 对于APP上的小红点，强迫症患者很不舒适，一定要点掉消除不可。但对于产品设计者而言，小红点是他们转化的一种设计手段。本文将就如何降低或者打消小红点为用户带来的支配恐惧，以及是否能够带来转化这两个方面展......
### [争议摩尔定律：英特尔反驳 英伟达“结束论”](https://finance.sina.com.cn/tech/it/2022-09-29/doc-imqqsmrp0868756.shtml)
> 概要: 作者： 李娜 樊雪寒......
### [撸了一款 Vue 生态缺失的 CMD+K 类库](https://segmentfault.com/a/1190000042559544)
> 概要: 9月底，新轮子又来了，Vue Command Palette是一个为 Vue 而生的快速、无样式、可组合的 Command Palette（CMDK）组件库。灵感来源这个组件的诞生的灵感来自上个月观察......
### [造车新势力第二梯队掀上市潮](https://finance.sina.com.cn/tech/2022-09-29/doc-imqqsmrp0884488.shtml)
> 概要: □从融资角度看，“蔚小理”多重上市后手握丰沛现金，但仍面对巨大资金压力，更何况“造血”能力远低于“蔚小理”的造车新势力第二梯队......
### [美国登月火箭“下台”避飓风 年内三次推迟发射](https://finance.sina.com.cn/tech/2022-09-29/doc-imqmmtha9156989.shtml)
> 概要: 为了避免即将到来的飓风“伊恩”造成破坏，当地时间9月26日深夜，美国宇航局将登月火箭“太空发射系统”从发射台上撤下。这是今年以来，美国宇航局第三次推迟登月火箭的发射......
### [B站将转为双重主要上市，10月3日正式生效](https://finance.sina.com.cn/tech/internet/2022-09-29/doc-imqmmtha9159688.shtml)
> 概要: 新浪科技讯 9月29日早间消息，港交所官网显示，B站在香港联交所由第二上市转为主要上市，将于10月3日正式生效。届时，B站将成为中国TMT（科技、媒体、通信）企业中首家完成自愿由在港二次上市转换为双重......
### [左突右杀！见识下巅峰林书豪的变向进攻集锦](https://bbs.hupu.com/55705595.html)
> 概要: 变向，是指运球行进间的状态下，两只手交换控球，可以为突破的准备动作也可以接上后撤步投篮的一种过渡性动作，是运球队员利用突然改变运球方向来突破防守的一种运球方法。15-16赛季，是我心目中的巅峰赛季，这
### [5 种瀑布流场景的实现原理解析](https://segmentfault.com/a/1190000042562451)
> 概要: 一、背景本文介绍 5 种瀑布流场景的实现，大家可以根据自身的需求场景进行选择。5 种场景分别是：瀑布流特点纵向+高度排序纯 CSS 多列实现，是最简单的瀑布流写法纵向+高度排序+根据宽度自适应列数通过......
### [《春喜》](https://www.zcool.com.cn/work/ZNjIxNDM1OTI=.html)
> 概要: 《聚仙庵》......
### [新海诚《铃芽户缔》中文预告公开 奇幻的关门之旅](https://acg.gamersky.com/news/202209/1522899.shtml)
> 概要: 新海诚新作动画电影《铃芽户缔》，公开了中文预告，本次的PV公开了大量的剧情画面。电影预定于11月11日在日本上映。
### [Monaco Editor 中的 Keybinding 机制](https://segmentfault.com/a/1190000042562942)
> 概要: 一、前言前段时间碰到了一个 Keybinding 相关的问题，于是探究了一番，首先大家可能会有两个问题：Monaco Editor 是啥？Keybinding 又是啥？Monaco Editor：微软......
### [高清重制不再麻烦 玩家利用AI提高辐射2 NPC画质](https://www.3dmgame.com/news/202209/3852718.html)
> 概要: 近日，一位昵称为“Misha_Vozduh”Reddit用户分享了自己使用AI文本生成图像的方式提高《辐射2》中NPC立绘的工作，该技术代表是DALL-E，DALL-E通过120亿参数版本的GPT-3......
### [《勇者斗恶龙宝藏》新预告公开 12月9日登陆NS](https://www.3dmgame.com/news/202209/3852719.html)
> 概要: SE公布《勇者斗恶龙 寻宝探险团：蓝色眼眸与天空罗盘》新预告，本次将介绍介绍“冒险据点”与“寻宝团”。游戏将于12月9日在Switch平台发售，支持中文。该作是一款以《勇者斗恶龙11》中角色，卡缪玛雅......
### [国产末日机械朋克肉鸽《湮灭线》正式公布](https://www.3dmgame.com/news/202209/3852724.html)
> 概要: 今日一款国产机械风格肉鸽游戏《湮灭线》公开了游戏宣传片，游戏采用了独特的机美术风格，呈现废土下的机械纪元，你将扮演这个机械纪元下的“病毒”，踏上找寻“湮灭线”破解之法的冒险旅程;本作采用了爽快奔放的横......
### [【vue3源码】十四、认识vnode中的shapeFlag和patchFlag属性](https://segmentfault.com/a/1190000042563157)
> 概要: shapeFLagvnode的shapeFLag属性使用二进制的方式描述了组件的类型。shapeFLag的值的类型是个枚举：export const enum ShapeFlags {  ELEMEN......
### [《宝可梦:大集结》巨钳螳螂新演示 现已正式上线](https://www.3dmgame.com/news/202209/3852737.html)
> 概要: 《宝可梦：大集结》新宝可梦“巨钳螳螂”现已正式上线，这将作为《宝可梦：大集结》一周年纪念活动的一部分，官方还公布了一段演示视频，展示了“巨钳螳螂”的技能等动作。《宝可梦：大集结》现已登陆Switch、......
### [谷歌微软：“软硬通吃”之迷惑与野望](https://www.huxiu.com/article/674587.html)
> 概要: 出品 | 虎嗅科技组作者 | 丸都山头图 | Google I/O如无意外，微软和谷歌将在未来两周先后召开新品发布会，更新旗下的终端产品。早在今年5月的谷歌开发者大会上，谷歌就对Pixel 7系列以及......
### [江西，特重度干旱](https://www.huxiu.com/article/674291.html)
> 概要: 本文来自微信公众号：地球知识局 （ID：diqiuzhishiju），作者：小哲， 校稿：辜汉膺，编辑：板栗、果果，头图来自：视觉中国（图为赣江大面积河床裸露开裂）9月23日，秋分降临，这一天阴阳相半......
### [牛信云入围2022年度中国MarTech行业「智能客服最佳服务商榜单」](http://www.investorscn.com/2022/09/29/103286/)
> 概要: 今年5月，MarTech概念创始人Scott Brinker团队发布2022年全球「Martech Map」。疫情间接推动了Martech行业的发展，入选的全球MarTech服务商数量达到9932家，......
### [洪恩二季度佳绩观察：推出全新国际品牌Bekids，通过STEAM产品赋能出海](http://www.investorscn.com/2022/09/29/103288/)
> 概要: 科技益智产品企业洪恩（纽约证券交易所股票代码：IH）日前公布了其未经审计的截至2022 年 6 月 30 日的第二季度业绩报告，营业收入为2.31 亿元人民币，同比增长 4.2%; 净利润 2180 ......
### [《福星小子》新动画PV2公开 10月13日正式开播](https://acg.gamersky.com/news/202209/1523170.shtml)
> 概要: 日本漫画家高桥留美子的经典作品《福星小子》完全新作动画，在今天公布了第二弹预告视频。动画将于2022年10月13日正式开播。
### [在元宇宙，流量不再是成为“周杰伦”的唯一密码](https://www.huxiu.com/article/673554.html)
> 概要: 出品｜虎嗅科技组作者｜周舟头图｜视觉中国几乎每一代都有能引发集体回忆的歌手，90后有周杰伦，95后有许嵩、汪苏泷，徐良三巨头，不过00后、05后似乎没有独属于他们这一代的新歌手。原因有很多，其中智能手......
### [联想夺笋 App 今日上线：支持安卓 / iOS，帮助用户解决各类电脑问题](https://www.ithome.com/0/644/094.htm)
> 概要: IT之家9 月 29 日消息，今年 2 月份，联想公布了一个“夺笋阿”的数码社区，提供各种数码产品信息和相关讨论专区，例如“C 盘满了？设置虚拟内存？软件卡住了？”该怎么办。现在，联想宣布夺笋 App......
### [组图：马思纯李玉断桥故事感大片 展成熟魅力氛围感满满](http://slide.ent.sina.com.cn/star/w/slide_4_704_375859.html)
> 概要: 组图：马思纯李玉断桥故事感大片 展成熟魅力氛围感满满
### [地球霸权人类神经！B站引进《pop子和pipi美》第二季](https://acg.gamersky.com/news/202209/1523196.shtml)
> 概要: 哔哩哔哩番剧官方微博发文宣布，B站正式引进《pop子和pipi美的日常 第二季》。
### [《原神》2年收入37亿美元 排全球手游第三](https://www.3dmgame.com/news/202209/3852765.html)
> 概要: 根据 Sensor Tower 的最新数据，自2020年 9 月推出以来，米哈游热门游戏《原神》已经产生了 37 亿美元玩家支出收入，称为了全球收入排名第 3 的手机游戏。腾讯手游位居榜首，《王者荣耀......
### [Stable Diffusion 还能压缩图：比 JPEG 更小，肉眼看更清晰，但千万别试人脸](https://www.ithome.com/0/644/116.htm)
> 概要: 免费开源的 Stable Diffusion 又被玩儿出了新花样：这次是被拿来压缩图片。Stable Diffusion 不仅能把同一张原图缩到更小，而且表现还肉眼可见地优于 JPEG 和 WebP......
### [组图：宋仲基丁海寅等出席apan颁奖典礼 穿黑色西装帅气养眼](http://slide.ent.sina.com.cn/tv/k/slide_4_704_375863.html)
> 概要: 组图：宋仲基丁海寅等出席apan颁奖典礼 穿黑色西装帅气养眼
### [推动“保交楼”专项借款加快落地，货币政策委员会例会有哪些新提法](https://www.yicai.com/news/101550903.html)
> 概要: 住房金融环境将逐步回暖，房贷利率仍有小幅下调的空间。
### [组图：宋祖儿隧道写真故事感十足 披肩长发尽显港风韵味](http://slide.ent.sina.com.cn/star/w/slide_4_704_375864.html)
> 概要: 组图：宋祖儿隧道写真故事感十足 披肩长发尽显港风韵味
### [这8个县十年人口增量超30万，义乌新郑昆山位居前三](https://www.yicai.com/news/101550930.html)
> 概要: 十年来，人口增长最快的区域，绝大多数是省会城市、中心城市下辖的县域，或者大城市周边的部分县域。这也反映了2010年以来我国人口流动的趋势，即人口正向中心城市、大都市圈加快集聚。
### [联合利华不仅需要新CEO，还需要新故事](https://www.huxiu.com/article/675169.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国联合利华全球CEO乔安路的“突然退休”，让人们开始想象：这家百年巨头大刀阔斧的改革时代或许即将结束。据9月26日联合利华通告，在公司工作35年、任职全球C......
### [幻海映月赛季剧情动画上线，和云中沙之盟一起共赴决战！](https://bbs.hupu.com/55714276.html)
> 概要: 斗篷下的少女，借用蕴含上古力量的云中蝶，掀起新的波澜；当她的灵魂与云中蝶共振，幻月之力生成照见人心的幻境，笼罩着广袤的云中沙漠……月神台上，云中沙之盟的英雄们正在经历前所未有的挑战：琉璃幻月，破念成蝶
### [中国全球创新指数升至11位，拥有21个科技集群](https://www.yicai.com/news/101550991.html)
> 概要: 在今年的全球“最佳科技集群”这一指标中， 中国首次拥有与美国一样多的顶级科技集群，各为21个。
### [中国全球创新指数升至11位，拥有21个科技集群](https://finance.sina.com.cn/china/2022-09-29/doc-imqmmtha9265046.shtml)
> 概要: 作为评价各经济体创新能力的一个指标，全球创新指数是衡量各国创新的风向标，并成为各国政府制定经济决策的参考。 当地时间9月29日...
### [刚刚，人民币暴拉1000点！央行再次重磅发声，外汇局：继续保持人民币汇率弹性](https://finance.sina.com.cn/china/gncj/2022-09-29/doc-imqmmtha9265941.shtml)
> 概要: 国家外汇管理局9月29日发布的《2022年上半年中国国际收支报告》表示，2022年上半年，面对复杂严峻的国际环境，我国有效统筹疫情防控和经济社会发展...
### [招商信诺人寿：险企如何逐浪大财富管理时代？](http://www.investorscn.com/2022/09/29/103293/)
> 概要: 在居民财富总量持续增长、无风险利率下行的背景下，民众的风险偏好显著降低，对财富管理的需求日益增加，大财富管理市场迎来繁荣发展期。而作为家庭资产配置的保障基石，保险已然成为大众财富管理中不可或缺的一环，......
### [名创优品推出1亿美元股份回购计划 兴趣消费布局增强发展信心](http://www.investorscn.com/2022/09/29/103295/)
> 概要: 9月29日消息，名创优品集团(NYSE:MNSO；HKEX:9896)发布公告，称董事会已批准于未来12个月期间在公开市场购回最多价值1亿美元的股份购回计划，董事会亦可能决定随市况变化而进一步动用股份......
### [孙宇晨出席米尔肯研究院亚洲峰会，首提波场全球普惠金融愿景](http://www.investorscn.com/2022/09/29/103298/)
> 概要: 9月29日，波场TRON创始人孙宇晨受邀出席2022米尔肯研究院亚洲峰会（2022 Milken Institute Asia Summit），与米尔肯研究院亚洲中心主席陈天宗（Curtis S. C......
### [10月1日至5日，故宫开馆时间提前！前4天门票已售罄](https://finance.sina.com.cn/china/2022-09-29/doc-imqqsmrp0996175.shtml)
> 概要: 今天，故宫博物院发布《关于2022年国庆开放时间的公告》。 公告称，10月1日至5日，故宫博物院开馆时间为7：30，停止入院时间为16：10，闭馆时间为17：00。
### [身价 84 亿的董事长夫妇工资百万，万润新能上市跌近 30% 后承销商亏成股东](https://www.ithome.com/0/644/148.htm)
> 概要: 双高发行股民弃购，承销商亏成股东曾被视为稳赚不赔的新股“打新”神话失效。9 月 29 日，动力电池上游企业湖北万润新能源科技股份有限公司（SH:688275，下称“万润新能”）在科创板挂牌上市，发行价......
### [欧元、英镑接连闪崩，会有转机吗？](https://www.yicai.com/news/101551039.html)
> 概要: 随着欧元此前对美元跌破1：1平价后，英镑看似将步其后尘。
### [为市场主体纾困发展提供有力支撑——国家税务总局介绍退减缓免税费政策落实进展](https://finance.sina.com.cn/roll/2022-09-29/doc-imqmmtha9269173.shtml)
> 概要: 新华社北京9月29日电 题：为市场主体纾困发展提供有力支撑——国家税务总局介绍退减缓免税费政策落实进展 新华社记者王雨萧、申铖 今年以来，新的组合式税费支持政策...
### [王江华被双开：靠“教育”吃“教育”](https://finance.sina.com.cn/china/2022-09-29/doc-imqqsmrp0998117.shtml)
> 概要: “廉洁江西”微信公号 日前，经中共江西省委批准，江西省纪委省监委对省委教育工委原委员、省教育厅原副厅长王江华严重违纪违法问题进行了立案审查调查。
### [流言板林书豪：我一直在适应郭指导的体系，我们都要做好自己](https://bbs.hupu.com/55714970.html)
> 概要: 虎扑09月29日讯 林书豪在球队媒体日上接受采访谈到适应情况。林书豪说：“我一直在适应球队跟郭指导的体系，然后就是我感觉今年是比较好，因为我有这一段时间可以学战术，就是跟球员配合，就是训练然后找一点默
### [苹果 CEO 蒂姆・库克在意大利获得创新和国际管理硕士学位](https://www.ithome.com/0/644/152.htm)
> 概要: IT之家9 月 29 日消息，人们到意大利旅行时，通常会尝试当地的意大利面、冰淇淋或参观博物馆。苹果 CEO Tim Cook （蒂姆库克）在欧洲访问中获得了 Università Degli Stu......
### [新一轮稳外贸外资政策加力，制造业引资专项政策将出台](https://www.yicai.com/news/101551064.html)
> 概要: 将于近期发布以制造业为重点促进外资扩增量稳存量提质量的政策措施。
### [流言板中国篮协：姑娘们好样的！属于我们的征途尚未结束](https://bbs.hupu.com/55715209.html)
> 概要: 虎扑09月29日讯 2022年女篮世界杯展开四分之一决赛的争夺，中国女篮以85-71战胜法国队，晋级四强。赛后，中国篮球协会发文祝贺。原文：“姑娘们好样的！中国女篮时隔28年再次闯入女篮世界杯四强！属
# 小说
### [洪主](https://m.qidian.com/book/1016872810/catalog)
> 作者：烽仙

> 标签：修真文明

> 简介：大江东去，洗不尽人族英雄血。自六千年前成阳大帝起兵，这天下，便是我人族天下。

> 章节总数：共1767章

> 状态：完本
# 论文
### [HOPE: A Task-Oriented and Human-Centric Evaluation Framework Using Professional Post-Editing Towards More Effective MT Evaluation | Papers With Code](https://paperswithcode.com/paper/hope-a-task-oriented-and-human-centric)
> 概要: Traditional automatic evaluation metrics for machine translation have been widely criticized by linguists due to their low accuracy, lack of transparency, focus on language mechanics rather than semantics, and low agreement with human quality evaluation. Human evaluations in the form of MQM-like scorecards have always been carried out in real industry setting by both clients and translation service providers (TSPs).
### [Background Activation Suppression for Weakly Supervised Object Localization | Papers With Code](https://paperswithcode.com/paper/background-activation-suppression-for-weakly)
> 概要: Weakly supervised object localization (WSOL) aims to localize the object region using only image-level labels as supervision. Recently a new paradigm has emerged by generating a foreground prediction map (FPM) to achieve the localization task.