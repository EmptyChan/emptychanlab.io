---
title: 2022-07-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.AmericanGoldfinch_ZH-CN2996912015_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-16 19:40:19
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.AmericanGoldfinch_ZH-CN2996912015_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [9种“占便宜”的营销策略方案，助你轻松精准引流、成交客户！](https://www.woshipm.com/marketing/5526955.html)
> 概要: 编辑导语：在日常消费时，有很多消费者都有一种占便宜的心理。本文作者分享了9种“占便宜”的营销策略方案，通过营销策略使顾客产生购买力，感兴趣的一起来看看吧。经商必须要讲消费心理学，还要讲消费行为学，重点......
### [微信视频号带货，放出了“视频号小店”这个大招](https://www.woshipm.com/it/5528111.html)
> 概要: 编辑导语：就在前两天，微信视频号发出一个重要通知，称在2022年7月21日，将上线“视频号小店”，且未来使用视频号橱窗功能，要先注册“视频号小店”。互联网上对此议论纷纷，猜想不断。视频号也有自己的“小......
### [网红品牌信任原罪，在定位？](https://www.woshipm.com/marketing/5527919.html)
> 概要: 编辑导语：随着新消费方式的不断变化，越来越多的网红品牌不断更新迭代，当品牌传输的认知与消费者的体验无法匹配时便会产生所谓的信任危机。本文作者分享了有关网红品牌带来的信任危机等相关内容，讲述了网红品牌的......
### [践行可持续理念，渣打中国助力亚投行成功发行其首笔可持续发展熊猫债](http://www.investorscn.com/2022/07/16/101857/)
> 概要: 当前,应对全球气候变化、保护地球环境并倡导可持续发展正成为全社会关注的焦点。金融行业作为全球经济和社会的重要组成部分,一直在其中发挥积极作用——通过创新、定制的可持续金融产品,助力迈向低碳经济和社会的......
### [Pursuing the Reunification of Home and Work](https://americancompass.org/essays/pursuing-the-reuinification-of-home-and-work/)
> 概要: Pursuing the Reunification of Home and Work
### [记者调查：汽车“以租代购”陷阱多风险大](https://finance.sina.com.cn/tech/it/2022-07-16/doc-imizirav3655149.shtml)
> 概要: 来源：法治日报......
### [张小泉致歉：常规刀具可拍蒜 一些硬度较高的有断刀风险](https://finance.sina.com.cn/tech/it/2022-07-16/doc-imizirav3655266.shtml)
> 概要: 中新网7月15日电 “张小泉品牌官方”微信公众号15日晚就“广州客诉事件”发布情况说明......
### [505 Games母公司Digital Bros成立新发行厂牌HOOK](https://www.3dmgame.com/news/202207/3847089.html)
> 概要: 505 Games的母公司DigitalBros宣布成立新的发行厂牌HOOK。HOOK总部位于DigitalBros的米兰总部，拥有一支来自不同国家的国际团队，由拥有超过20年经验的行业资深人士组成，......
### [343首次展示《光环：无限》战役合作模式内容](https://www.3dmgame.com/news/202207/3847090.html)
> 概要: 343 Industries日前首次展示了《光环：无限》的战役合作模式。工作室成员展示了这个期待已久的特性，以及游戏新的任务重玩系统。343计划在本周进行战役合作和任务重玩的测试，不过并不确保一定会实......
### [特斯拉版“擎天柱”呼之欲出  人形机器人距离现实有多远？](https://finance.sina.com.cn/tech/roll/2022-07-16/doc-imizmscv1792470.shtml)
> 概要: 本报记者 曲忠芳 李正豪 北京报道......
### [网约车行业加码竞争下半场](https://finance.sina.com.cn/tech/roll/2022-07-16/doc-imizmscv1794006.shtml)
> 概要: 本报记者 于典 张家振 上海报道......
### [2.5%，中国经济企稳之战](https://www.huxiu.com/article/609605.html)
> 概要: 本文来自微信公众号：经济观察报 （ID：eeo-com-cn），作者：田进、郑淯心，题图来自：视觉中国2022年上半年，中国经济在画出了一条漫长的N型增长曲线后，交出了上半年GDP同比增长2.5%、二......
### [组图：赵露思庆工作室成立两周年 穿紫色抹胸扎双马尾元气可爱](http://slide.ent.sina.com.cn/star/w/slide_4_704_372653.html)
> 概要: 组图：赵露思庆工作室成立两周年 穿紫色抹胸扎双马尾元气可爱
### [Android removes much of Fuchsia-related code as Starnix project progresses](https://9to5google.com/2022/07/15/android-removes-fuchsia-code-starnix/)
> 概要: Android removes much of Fuchsia-related code as Starnix project progresses
### [Windows 12最早于2024年推出 每3年更新换代](https://www.3dmgame.com/news/202207/3847096.html)
> 概要: 微软大幅度修改了路线图，其Windows升级计划改成了三年。新的计划意味着Windows 12有望最早在2024年推出。根据Windows Central报道，微软的新路线图意味着Windows系统每......
### [A股有些地方也挺“热”的丨股市演义](https://www.yicai.com/news/101476244.html)
> 概要: 小编看一眼自己的股票账户，觉得可以“清凉一夏”了。
### [Mojang表示《我的世界：传奇》的故事没有对错之分](https://www.3dmgame.com/news/202207/3847098.html)
> 概要: 《我的世界：传奇》讲述了一段设置在《我的世界》背景中的英雄故事，而根据开发者的说法，这个故事里并没有严格的对错之分。在《我的世界：传奇》开发日志中，Mojang工作室探讨了《我的世界：传奇》创意来源......
### [ElasticSearch 学习笔记(一) 基本概念与基本使用](https://segmentfault.com/a/1190000042180624)
> 概要: 一般我介绍某个框架、MQ、中间件，一般都是讲是啥，能帮助我们干啥，然后用起来，高级特性。这次打算换一种风格，穿插一些小故事。写到这篇的时候，我想起我刚入行的第一个项目，有一个页面查询，主表两百七十万条......
### [前网球运动员莎拉波娃产子 五届大满贯冠军当妈](https://ent.sina.com.cn/s/u/2022-07-16/doc-imizirav3683944.shtml)
> 概要: 35岁的莎拉波娃通过社交媒体宣布了一则喜讯，自己已经产下一子—西奥多。　　五座大满贯冠军、前世界第一的莎拉波娃，周五在社交媒体上分享了她儿子西奥多的照片，她和未婚夫、英国商人吉尔克斯，以及儿子西奥多全......
### [良笑社公开犬夜叉「新月之日Ver.」手办](http://acg.178.com/202207/451936419471.html)
> 概要: 近日，良笑社公开了「POP UP PARADE」系列手办的最新企划，本次推出的角色是犬夜叉，造型取自黑发形态的「新月之日Ver.」。该手办全高约170mm，售价3,900日元，现已开订，预计将在7月末......
### [组图：张天爱分享《浪姐3》四公练习实录 两天速成钢管舞](http://slide.ent.sina.com.cn/z/v/w/slide_4_704_372657.html)
> 概要: 组图：张天爱分享《浪姐3》四公练习实录 两天速成钢管舞
### [TV动画「傲娇恶役大小姐莉泽洛特与实况转播远藤君和解说员小林」第一弹PV和视觉图公布](http://acg.178.com/202207/451937763853.html)
> 概要: TV动画「傲娇恶役大小姐莉泽洛特与实况转播远藤君和解说员小林」公布了第一弹PV和视觉图，本作将于2023年1月播出。TV动画「傲娇恶役大小姐莉泽洛特与实况转播远藤君和解说员小林」第一弹PVSTAFF原......
### [「进击的巨人」最终季第4卷BD封面公开](http://acg.178.com/202207/451938617446.html)
> 概要: 「进击的巨人」最终季发布了第4卷BD封面，该商品将于8月17日发售。电视动画「进击的巨人」改编自谏山创创作的同名漫画作品，最终季为动画第4期，共分为三部分播出，前两部分已播出完毕，第三部分将于2023......
### [动画「蓝色监狱」剑城斩铁角色PV、角色视觉图公开](http://acg.178.com/202207/451938825605.html)
> 概要: 足球题材TV动画「蓝色监狱」（Blue Lock）发布了剑城斩铁的角色PV和角色视觉图，本作将于2022年10月开播。「蓝色监狱」剑城斩铁角色PV角色视觉图：CAST潔世一：浦和希蜂楽廻：海渡翼國神錬......
### [井上敏树开新连载漫画《机动絶记高达SEQUEL》！](https://news.dmzj.com/article/74965.html)
> 概要: 井上敏树·脚本 「高达」新作漫画《機動絶記高达SEQUEL》将于2022年7月29日中午开始连载！
### [TV动画《间谍教室》公开最新视觉图与先导PV！](https://news.dmzj.com/article/74966.html)
> 概要: TV动画《间谍教室》公开最新视觉图与先导PV！
### [爱立信第二季度净销售额 625 亿瑞典克朗同比增长 14%，毛利率达 42.1%](https://www.ithome.com/0/629/913.htm)
> 概要: 7 月 16 日消息，日前，爱立信发布的 2022 年第二季度业绩报告数据显示，2022 年第二季度，爱立信实现净销售额 625 亿瑞典克朗（约 401.6 亿元人民币），同比增长 14%；按可比单位......
### [步长制药荣登2021年中国中药企业排行榜民营企业第一位](http://www.investorscn.com/2022/07/16/101858/)
> 概要: 7月12-15日,以“锚定稳增长”为主题的2022米思会(中国医药健康产业共生大会)在湖州举行。会上,备受关注的2021年中国医药工业百强系列榜单揭晓,步长制药(603858)凭借扎实的研发生产实力,......
### [Rachel Jin出席2022国际消费者大会，分享中国绿色消费新趋势](http://www.investorscn.com/2022/07/16/101859/)
> 概要: 本文源自:南早网......
### [最近就没点儿好消息吗？](https://www.huxiu.com/article/609633.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童题图｜《纽约客》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。这世界是怎么了？疫情、战......
### [为什么 99% 的人做短视频，思路全错？](https://www.tuicool.com/articles/q2eiuau)
> 概要: 为什么 99% 的人做短视频，思路全错？
### [日本与澳大利亚加强 5G 领域合作，拟讨论 Open RAN 新通信网](https://www.ithome.com/0/629/920.htm)
> 概要: 据日媒报道，日本总务省 15 日发布消息称，将与澳大利亚政府设立局长级会议，就第五代（5G）移动通信系统的建设等交换意见。今年内将召开首次会议。会议名称为“日澳电信强化政策对话”，拟讨论可以连接多个厂......
### [哪家社区，最爱明星？](https://www.tuicool.com/articles/6NbiYnu)
> 概要: 哪家社区，最爱明星？
### [下一个倒下的，会不会是“华为顾问”？](https://www.tuicool.com/articles/zABnqma)
> 概要: 下一个倒下的，会不会是“华为顾问”？
### [《冲出地球》：守护“星游”之心（我的评分：6.8）](https://new.qq.com/omn/20220716/20220716A0577O00.html)
> 概要: 《冲出地球》是从十年前一部很另类、很前卫、但却不那么知名的国产电视动画《星游记》衍生出来的一部电影。当时很另类的原因在于，在“喜灰”等为数众多的低幼动画的夹缝中，它是主打青年至少是青少群体的一部作品......
### [辅助驾驶存在风险 德国法院判给顾客一辆特斯拉](https://www.yicai.com/news/101476365.html)
> 概要: Model X车辆无法可靠地识别道路施工狭窄等障碍物，并且有时会不必要地启动刹车。慕尼黑法院认为，使用AutoPilot可能会在市中心造成“巨大危险”并导致碰撞。
### [特斯拉前员工创办 Lightship Energy 公司，计划推出电动旅行拖车](https://www.ithome.com/0/629/936.htm)
> 概要: IT之家7 月 16 日消息，据 Electrek 报道，由特斯拉前员工创办的 Lightship Energy 公司创立之初是为了造电动版流动餐车，但由于外界对电动旅行拖车很感兴趣，因此他们决定推出......
### [On Bill Watterson’s Refusal to License Calvin and Hobbes (2016)](https://www.thelegalartist.com/blog/on-bill-wattersons-refusal-to-license-calvin-and-hobbes)
> 概要: On Bill Watterson’s Refusal to License Calvin and Hobbes (2016)
### [传言称《刺客信条：无限》部分背景将设定在日本](https://www.3dmgame.com/news/202207/3847118.html)
> 概要: 游戏记者Jeff Grubb在个人油管节目中声称，《刺客信条》接下来将选取玩家呼声一直非常高的日本背景设定。Grubb透露，日本背景将会是《刺客信条：无限》的背景之一，不过也有可能是在这之后的另一款游......
### [How the Colosseum Was Built and Why It Was an Architectural Marvel](https://www.history.com/news/how-roman-colosseum-built)
> 概要: How the Colosseum Was Built and Why It Was an Architectural Marvel
### [主力资金一周流向速览，这些个股最受青睐](https://www.yicai.com/news/101476405.html)
> 概要: 沪指本周跌3.81%，深成指本周跌3.47%，创业板指本周跌2.03%。主力资金本周呈现大幅净流出态势，这些个股获逆势加仓。
### [中暑会致命！张文宏团队提示户外普通人群采样无需穿“大白”](https://www.yicai.com/news/101476416.html)
> 概要: 在炎热的环境中，继续穿着“大白”会阻止人体热量的散发，大大增加中暑发生的概率。被汗水浸透的“大白”防护作用也已经大打折扣。
### [以宇智波斑的实力，黑绝为何能偷袭成功？](https://new.qq.com/omn/20220716/20220716A06W9Z00.html)
> 概要: 动漫火影忍者中，忍界大战这部分的剧情应该是整部动漫之中最高潮的部分，而这部分的剧情也可以说是一波三折，在忍界大战之中，反派的boss也一直在更换，从带土到宇智波斑，再从斑到黑绝，最后到大筒木辉夜，bo......
### [如果鸣人最强的是嘴遁，小樱和佐助应该是什么遁？](https://new.qq.com/omn/20220716/20220716A06WBW00.html)
> 概要: 在动漫的发展过程中，有三部动漫曾经被称为是“三大民工动漫”，这三部动漫分别是《海贼王》，《死神》还有《火影忍者》，其中《火影忍者》在当时更是这三部动漫之中的领头羊，而这部动漫中的角色也都已经深入人心......
### [忍界大战之后，佐助为何不回木叶村？](https://new.qq.com/omn/20220716/20220716A06W8800.html)
> 概要: 动漫火影忍者中，宇智波佐助和鸣人一样同样都是这部动漫的一位主角，在忍界大战结束之后，鸣人成为了木叶村的火影，但是宇智波佐助却选择了自我流放，可是此时佐助之前犯下的罪行已经得到了原谅，为什么在忍界大战结......
### [李鬼还是李逵，海关查获侵权物品](https://finance.sina.com.cn/china/2022-07-16/doc-imizmscv1919815.shtml)
> 概要: 深圳宝安机场海关查获侵权汽车零配件400件 近日，深圳宝安机场海关关员在对一票出口货物实施查验时，查获一批标有“BMW”文字及圆形双色标识汽车车标，共计400件...
### [思必驰冲刺科创板：年营收3亿亏3.4亿 阿里与联想之星是股东](https://www.tuicool.com/articles/3Erquu2)
> 概要: 思必驰冲刺科创板：年营收3亿亏3.4亿 阿里与联想之星是股东
### [世行等五大国际机构联合呼吁：采取紧急行动应对全球粮食安全危机](https://www.yicai.com/news/101476354.html)
> 概要: 伊维拉表示，降低贸易壁垒，能更有希望抑制通胀。
### [最新发布！这15批次雪糕，不合格！市场监管总局发话：严厉查处](https://finance.sina.com.cn/wm/2022-07-16/doc-imizmscv1919051.shtml)
> 概要: 市场监管总局今天（16日）发布消息，入夏以来，市场监管总局要求各地市场监管部门加强对雪糕等冷冻饮品的监管，进一步加大对雪糕生产经营企业的监督检查和雪糕产品的监督抽...
### [9家首批科创板公司大股东“惜售”：最长延长锁定期12个月](https://finance.sina.com.cn/roll/2022-07-16/doc-imizmscv1918699.shtml)
> 概要: 随着科创板开市三周年临近，首批25家公司即将迎来解禁，不过，有9家公司已明确表达“惜售”之意，自愿延长锁定期。 据梳理，7月15日晚间，首批公司齐发公告...
### [赛后BLG 1-1 RA，抽刀断丝锋芒不可挡，BLG运筹帷幄扳平比分](https://bbs.hupu.com/54764960.html)
> 概要: Game2：蓝色方：RA，红色方：BLGRA禁用：船长，阿狸，刀妹，岩雀，破败之王RA选用：Cube酒桶、Leyan赵信、Strive飞机、iBoy滑板鞋、yuyanjia烈娜塔BLG禁用：泽丽，武器
### [“团购”刺激楼市无异于缘木求鱼！](https://finance.sina.com.cn/china/gncj/2022-07-16/doc-imizmscv1919480.shtml)
> 概要: 原标题：记者观察 | “团购”刺激楼市无异于缘木求鱼 “团购买房”作为一种激活楼市购买端的辅助手段，并非每个地方都适用，其其效果如何尚进一步观察。
### [流言板贝尼特斯：库利巴利在意甲是顶级后卫，他仍需提高专注力](https://bbs.hupu.com/54765094.html)
> 概要: 虎扑07月16日讯 在接受《The Athletic》的采访时，前切尔西主帅贝尼特斯谈到了刚刚加盟蓝军的后卫库利巴利。他表示，库利巴利在意甲是顶级后卫，但来到英超之后，这一切还需要时间来检验。贝尼特斯
### [北京加强预付费消费治理 多措并举保护消费者合法权益](https://finance.sina.com.cn/jjxw/2022-07-16/doc-imizirav3789128.shtml)
> 概要: 中新网北京7月16日电 （记者 杜燕）今年6月1日起，被称为北京市预付卡消费“紧箍咒”的《北京市单用途预付卡管理条例》正式实施。今天，记者从北京市市场监管局了解到...
### [牧星智能获数千万元A2轮融资，领建资本担任财务顾问](http://www.investorscn.com/2022/07/16/101860/)
> 概要: 牧星智能获数千万元A2轮融资，领建资本担任财务顾问
### [流言板卡恩：巴萨拜仁只达成口头协议；大牌离队后拜仁往往更辉煌](https://bbs.hupu.com/54765245.html)
> 概要: 虎扑07月16日讯 在莱万多夫斯基敲定以4500万欧元+500万欧元附加条款加盟巴萨之后，德国媒体图片报主编Christian Falk独家采访了拜仁主席奥利弗-卡恩，他阐述了自己的想法。图片报：卡恩
### [流言板萨利哈米季奇：我非常尊重C罗，但签下他不是拜仁现在讨论的](https://bbs.hupu.com/54765306.html)
> 概要: 虎扑07月16日讯 哈桑-萨利哈米季奇再次明确表示拜仁慕尼黑今年夏天不会签下C罗。萨利哈米季奇告知体育一台：“我非常尊重克里斯蒂亚诺-罗纳尔多、他的成功和他的职业生涯我都很尊重。但再说一遍：这曾经是我
### [FCC 提议：将美国宽带最低网速标准提高到 100Mbps](https://www.ithome.com/0/629/976.htm)
> 概要: 感谢IT之家网友OC_Formula的线索投递！IT之家7 月 16 日消息，据 Engadget 报道，近日，FCC 主席 Jessica Rosenworcel 提议将宽带的最低定义提高到下载速度......
# 小说
### [我是禁忌生命](https://m.qidian.com/book/1027976362/catalog)
> 作者：男人不潇洒

> 标签：异世大陆

> 简介：有一种异能，不存在于肉身和灵魂中，只存在于自身的概念中，无法被检测到，无法被剥夺走，表现形式，就是在脑海中出现文字、数字、图像、模型等等，这种异能，被称为“概念系异能”。觉醒了概念系异能的姜临，发现自己只要不停地肝资源，就能快速提升。他的异能只要使用，就会升级，只要经验足够，就不会有瓶颈。于是他化身挖矿狂魔，开局一把石镐，挖遍原始洪荒世界。本书原名《我的概念系异能》。本书为挖矿生存自由建造类，算是种田文，但主线是禁忌生命，求支持！

> 章节总数：共215章

> 状态：完本
# 论文
### [Braille Letter Reading: A Benchmark for Spatio-Temporal Pattern Recognition on Neuromorphic Hardware | Papers With Code](https://paperswithcode.com/paper/braille-letter-reading-a-benchmark-for-spatio)
> 概要: Spatio-temporal pattern recognition is a fundamental ability of the brain which is required for numerous real-world applications. Recent deep learning approaches have reached outstanding accuracy in such tasks, but their implementation on conventional embedded solutions is still very computationally and energy expensive. Tactile sensing in robotic applications is a representative example where real-time processing and energy-efficiency are required. Following a brain-inspired computing approach, we propose a new benchmark for spatio-temporal tactile pattern recognition at the edge through braille letters reading. We recorded a new braille letters dataset based on the capacitive tactile sensors/fingertip of the iCub robot, then we investigated the importance of temporal information and the impact of event-based encoding for spike-based/event-based computation. Afterwards, we trained and compared feed-forward and recurrent spiking neural networks (SNNs) offline using back-propagation through time with surrogate gradients, then we deployed them on the Intel Loihi neuromorphic chip for fast and efficient inference. We confronted our approach to standard classifiers, in particular to a Long Short-Term Memory (LSTM) deployed on the embedded Nvidia Jetson GPU in terms of classification accuracy, power/energy consumption and computational delay. Our results show that the LSTM outperforms the recurrent SNN in terms of accuracy by 14%. However, the recurrent SNN on Loihi is 237 times more energy-efficient than the LSTM on Jetson, requiring an average power of only 31mW. This work proposes a new benchmark for tactile sensing and highlights the challenges and opportunities of event-based encoding, neuromorphic hardware and spike-based computing for spatio-temporal pattern recognition at the edge.
### [Center-Embedding and Constituency in the Brain and a New Characterization of Context-Free Languages | Papers With Code](https://paperswithcode.com/paper/center-embedding-and-constituency-in-the)
> 日期：27 Jun 2022

> 标签：None

> 代码：https://github.com/dmitropolsky/assemblies

> 描述：A computational system implemented exclusively through the spiking of neurons was recently shown capable of syntax, that is, of carrying out the dependency parsing of simple English sentences. We address two of the most important questions left open by that work: constituency (the identification of key parts of the sentence such as the verb phrase) and the processing of dependent sentences, especially center-embedded ones. We show that these two aspects of language can also be implemented by neurons and synapses in a way that is compatible with what is known, or widely believed, about the structure and function of the language organ. Surprisingly, the way we implement center embedding points to a new characterization of context-free languages.
