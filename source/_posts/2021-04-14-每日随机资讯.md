---
title: 2021-04-14-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CarrizoPlain_EN-CN0676049798_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-04-14 21:25:12
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CarrizoPlain_EN-CN0676049798_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [The most important statistical ideas of the past 50 years](https://fermatslibrary.com/s/what-are-the-most-important-statistical-ideas-of-the-past-50-years)
> 概要: The most important statistical ideas of the past 50 years
### [组图：刘嘉玲回应删baby好友后现身 全黑look项链叠带十分优雅](http://slide.ent.sina.com.cn/star/slide_4_704_355302.html)
> 概要: 组图：刘嘉玲回应删baby好友后现身 全黑look项链叠带十分优雅
### [日本到底行不行？](https://www.huxiu.com/article/421565.html)
> 概要: 来源｜智本社（ID：zhibenshe0-1）作者｜清和头图｜视觉中国2021年4月13日，日本政府正式决定两年后将福岛第一核电站的核废水排放入海。消息一出，舆论哗然，反对者众。该放射性核废水源主要是......
### [Effort to disrupt exploitation of Microsoft Exchange Server vulnerabilities](https://www.justice.gov/usao-sdtx/pr/justice-department-announces-court-authorized-effort-disrupt-exploitation-microsoft)
> 概要: Effort to disrupt exploitation of Microsoft Exchange Server vulnerabilities
### [关于创业的七个“反常识”建议](https://www.huxiu.com/article/421524.html)
> 概要: 本文来自微信公众号：人神共奋（ID：tongyipaocha），作者：人神共奋，头图来自：视觉中国一、安稳上班，还是冒险创业？前几天，收到一个读者的留言，他因为看了我的一篇文章，在人生的路口选择了不同......
### [当乔布斯从牛皮纸袋里抽出Macbook，新时代开始了](https://www.tuicool.com/articles/rEZBN3I)
> 概要: 造物的过程，有新的物品、新的理念出现的过程，都是设计的一部分。这些过程集合起来，创造出了一个与昨日截然不同的明日世界，我们可以说：这是设计创世纪。本文来自微信公众号：腾云（ID：tenyun700），......
### [微软泄露《尼尔伪装者》免费DLC 与机械纪元有关](https://www.3dmgame.com/news/202104/3812433.html)
> 概要: 根据微软商城页面透露的消息，该商城目前已经泄露了《尼尔：伪装者1.22474487139…》免费DLC“4 YoRHa”，该DLC将于4月23日发布，随游戏本体一同推出。据悉，此次《尼尔：伪装者1.2......
### [组图：持续撒糖！张雨绮与绯闻男友李柄熹手牵手现身街头](http://slide.ent.sina.com.cn/star/slide_4_704_355310.html)
> 概要: 组图：持续撒糖！张雨绮与绯闻男友李柄熹手牵手现身街头
### [新剧场版《高达G之复国运动3》定档7月22日上映](https://www.3dmgame.com/news/202104/3812435.html)
> 概要: 已经确认会有五部曲的高达系列剧场动画《高达Reconguista in G（高达G之复国运动）》4月13日晚间官方宣布，其第3部剧场电影《来自宇宙的遗产》将于7月22日上映，同时新海报以及预告公开。·......
### [P站美图推荐——腰间外套特辑](https://news.dmzj1.com/article/70620.html)
> 概要: 本周直播预告
### [「BEASTARS」路易手办开订](http://acg.178.com//202104/412367562711.html)
> 概要: MEGASHOUSE作品「BEASTARS」路易手办开订，高约190mm，主体采用ABS、PVC材料制造。该手办定价为10450日元（含税），约合人民币628元，预计于2021年10月发售......
### [Netflix动画《弥助》公开日语配音版PV](https://news.dmzj1.com/article/70622.html)
> 概要: Epic Games在13日晚宣布了完成了10亿美元融资的消息。在这之中，包括了索尼进行的2亿美元的战略投资。
### [《柯南：绯色的子弹》终极预告 磁悬浮列车上的决战](https://acg.gamersky.com/news/202104/1379127.shtml)
> 概要: 剧场版《名侦探柯南：绯色的子弹》公开中字版终极预告，柯南、赤井一家全员集结，大家在飞驰的磁悬浮列车上与罪犯展开终极对决。
### [少女时代徐贤回应金正贤事件:愿所有人都只有好事](https://ent.sina.com.cn/s/j/2021-04-14/doc-ikmxzfmk6731528.shtml)
> 概要: 新浪娱乐讯 4月14日，少女时代成员徐贤分享近照并发文“愿所有人都只有好事发生。Have a wonderful day”，疑似回应此前因徐睿知不让金正贤和女演员身体接触，而在拍剧时被对方黑脸对待的事......
### [nocras个人制作 动作冒险游戏《沉眠于黄昏之街》发售](https://news.dmzj1.com/article/70624.html)
> 概要: 朝日电视台在4月13日晚播出了由业内人士评选的改变日本动画历史的14部很厉害的动画。这次的14部动画是由76名制作过人气动画的职业制作人选出的。
### [动画电影《你好世界》内地即将上映 穿越记忆世界](https://acg.gamersky.com/news/202104/1379199.shtml)
> 概要: 由伊藤智彦导演的动画电影《你好世界》（HELLO WORLD）即将在国内上映，新预告公开。
### [漫画「古见同学有交流障碍症。」第21卷封面公开](http://acg.178.com//202104/412377779398.html)
> 概要: 漫画「古见同学有交流障碍症。」官方公开了本作第21卷的封面图，该册将于5月18日发售。「古见同学有交流障碍症。」是小田智仁创作的漫画，在小学馆旗下漫画杂志「周刊少年Sunday」2016年第25期上正......
### [PS4/Switch《真流行之神3》发售决定！](https://news.dmzj1.com/article/70625.html)
> 概要: 根据偷跑消息，日本一的PS4/NS平台新作《真流行之神3》决定将于7月29日发售。游戏售价7678日元，约合人民币461元。
### [日本曝出新一波艺人感染新冠 艺人工作日程将取消](https://ent.sina.com.cn/s/j/2021-04-14/doc-ikmxzfmk6750229.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 日本演艺圈最近再次接连出现艺人感染新冠病毒的报道，今天4月14日一天就有原早安少女后藤真希、女星贯地谷诗穗梨和演员山本耕史三位艺人通过事务所宣布感染新冠，工作也受到不同程......
### [墓地问题只有拼坟坟才能解决](https://www.huxiu.com/article/421648.html)
> 概要: 本文来自微信公众号：仙人JUMP（ID：xrtiaotiao），作者：半佛仙人，题图来自：视觉中国1众所周知，万物起源于房地产。毕竟房地产是经济周期之母，是平凡少年在钢铁森林大冒险寻觅一生的宝藏。年轻......
### [TV动画「转生史莱姆日记」OP和ED影像公布](http://acg.178.com//202104/412380609203.html)
> 概要: TV动画「转生史莱姆日记」公布了OP和ED的影像，本作现正在热播中。「转生史莱姆日记」OP及EDTV动画「转生史莱姆日记」改编自伏濑原著、柴负责作画的同名漫画作品，是知名动漫「关于我转生变成史莱姆这档......
### [金色观察 | 2021年 全球加密货币纳税进行时](https://www.tuicool.com/articles/ZjiQ7n7)
> 概要: 随着投资机构对加密货币兴趣的增加，以及加密货币用户的激增，加密货币也成为世界各国政府监管的重要目标，越来越多的政府加入推出加密货币税法的行列。西班牙财政部（Ministerio de Hacienda......
### [画师绘制日漫风格漫威英雄变体封面图公开](http://acg.178.com//202104/412382590359.html)
> 概要: 近日，漫威「风暴战士」的画师Peach Momoko为漫威漫画绘制了一整套的人物变体封面图，本次绘制采用日漫风格全新演绎了诸多经典漫威角色。登场的人物分别为：绿魔、「幽灵蜘蛛」格温·斯泰西、惊奇队长、......
### [千值练 天元突破 超银河红莲螺岩 拼装模型 售价540元](https://www.3dmgame.com/news/202104/3812460.html)
> 概要: 今日（4月14日），著名模型厂商千值练推出了新品《天元突破》超银河红莲螺岩拼装模型，售价9000日元（不含税，约合人民币540元），预计于2021年8月发售。《天元突破》超银河红莲螺岩塑料拼装模型尺寸......
### [《高达独角兽》特别视频 泽野弘之组曲MV超震撼](https://www.3dmgame.com/news/202104/3812461.html)
> 概要: 有粉丝说《机动战士高达：独角兽》是系列最杰出的作品，原因归结于泽野弘之亲自谱曲，没错，泽野大神的风格就是那么的震慑心灵，近日，为纪念5月7日《机动战士高达 闪光的哈萨维》上映，官方公布了在18米高高达......
### [井柏然晒自拍力证没秃头 自我安慰“很稳”](https://ent.sina.com.cn/s/m/2021-04-14/doc-ikmyaawa9633975.shtml)
> 概要: (责编：小5)......
### [智能汽车新时代开启，为什么是上汽？](https://www.ithome.com/0/545/983.htm)
> 概要: 4 月 9 日，上汽集团于上海正式召开了预热已久的零束 SOA 平台开发者大会。这是一场关于汽车行业未来的盛会，包括上汽集团董事长陈虹，上汽副总裁、总工程师祖似杰以及零束软件 CEO 李君在内的上汽众......
### [2万字解读2020中国餐饮，洞见4大趋势](https://www.huxiu.com/article/421607.html)
> 概要: 本文来自微信公众号：笔记侠（ID：Notesman），作者：卿永（番茄资本创始人，本文为作者在窄门学社、番茄资本主办的第四届餐饮品牌与供应链投融资大会暨窄门年会中的主题分享），头图来自：视觉中国费斯汀......
### [注册、登录、找回密码模块的设计构思](https://www.tuicool.com/articles/FZrIjiF)
> 概要: 编辑导语：我们在使用很多产品时，首先都会经历的就是注册登录这一步，从以前的手机号、邮箱等方式，到现在的第三方直接授权登录，也是用户登录系统的一种转变；本文作者分享了关于注册登录找回密码模块的设计构思，......
### [人们祛不掉量子力学的“魅”](https://www.tuicool.com/articles/jA3eya2)
> 概要: 文 | 李北辰追溯人类科技史，几乎所有最前沿的技术领域，都存在一个普遍宿命，就是当新的研究范式基本确立，但相关细节仍在累积，有些议题尚未有全景性理解时，学术界与社会公众之间，会存在一道明显的信息鸿沟，......
### [是袒露心声、还是故意炒作，反正快手主播辛巴不会 “退”](https://www.ithome.com/0/546/052.htm)
> 概要: “GIAO 哥”直播间里经常出现的 “退网”梗，也被快手一哥 “辛巴”用了几次。当然，二者的目的不同：GIAO 哥是想让观众们给自己多刷点礼物，而辛巴含泪喊出的 “臣退了”，则被辛选官方回应为 “退网......
### [ATP 发布支持 AES-256 硬件加密的 microSD 卡，MLC 闪存最大 16GB](https://www.ithome.com/0/546/063.htm)
> 概要: IT之家4月14日消息 据外媒 techpowerup 消息，存储设备厂商 ATP 近日发布了SecurStor系列 microSD 卡。这款产品支持硬件级别的加密，具有高寿命、高可靠性，提供 4GB......
### [The Muse (YC W12) Is Hiring a Designer Come Join Our Team](https://www.themuse.com/jobs/themuse/product-designer-employer-squad)
> 概要: The Muse (YC W12) Is Hiring a Designer Come Join Our Team
### [深圳集中供地来了 起拍价超百亿门槛也更高](https://finance.sina.com.cn/china/2021-04-14/doc-ikmxzfmk6824195.shtml)
> 概要: 原标题：深圳集中供地来了，起拍价超百亿门槛也更高 记者 | 张子怡 自决定实施集中供地后，深圳的土地市场备受关注。 4月14日，深圳挂牌6宗居住用地，起始总价约105...
### [Once on the Brink of Eradication, Syphilis Is Raging Again](https://www.npr.org/sections/health-shots/2021/04/14/986997576/once-on-the-brink-of-eradication-syphilis-is-raging-again)
> 概要: Once on the Brink of Eradication, Syphilis Is Raging Again
### [日本美少女星内真美福利图 黑丝美腿根本抵挡不住](https://www.3dmgame.com/bagua/4487.html)
> 概要: 今天为大家带来的是日本美少女偶像星内真美的福利图，妹子的写真风格充满了青春气息，黑丝美腿根本抵挡不住！一起来欣赏下她的美图......
### [国家能源局：五年内全国地热能供暖面积增加五成](https://finance.sina.com.cn/china/2021-04-14/doc-ikmxzfmk6825292.shtml)
> 概要: 原标题：国家能源局：五年内全国地热能供暖面积增加五成 4月14日，国家能源局综合司发布《关于促进地热能开发利用的若干意见（征求意见稿）》公告称，到2025年...
### [国家能源局：2025年地热能供暖面积增加50% 推进发电示范项目建设](https://finance.sina.com.cn/china/bwdt/2021-04-14/doc-ikmxzfmk6825637.shtml)
> 概要: 原标题：国家能源局：2025年地热能供暖面积增加50%，推进地热能发电示范项目建设 作者：见习记者，王晨 4月14日，国家能源局综合司发布《关于促进地热能开发利用的若干意见...
### [比亚迪汉 EV 获 2021 德国 iF 设计奖：中国品牌首款获奖轿车](https://www.ithome.com/0/546/066.htm)
> 概要: IT之家 4 月 14 日消息 今天，比亚迪宣布汉 EV 斩获 2021 德国 iF 设计奖，成为中国品牌首款获奖轿车车型。官方表示，iF 设计奖评审团高度评价 :“比亚迪汉 EV 为中国用户提供了健......
### [33岁男子一年交四个女友骗上百万元 他有两任妻子四个孩子](https://finance.sina.com.cn/china/2021-04-14/doc-ikmyaawa9685894.shtml)
> 概要: 澎湃新闻资深记者 李菁 通讯员 童画 2019年，33岁的郎军很忙。 他这一年来到上海，和之前打网游认识的女友伍娜同居，又先后开始和三名女子交往...
### [那英：农民才会听他的歌，杨坤：他那是音乐吗，刀郎为何被群攻？](https://new.qq.com/omn/20210414/20210414A0DFCU00.html)
> 概要: 那英：农民才会听他的歌，杨坤：他那是音乐吗，刀郎为何被群攻？那英参加浪姐2热度一直居高不下，常做评委席的她这次转换了身份成为了参赛者，被人点评。当年那英当评委不得到她赏识的人，如今都通过实力征服了观众......
### [“分配生”名额增加 杭州学区房酝酿再调整](https://finance.sina.com.cn/china/dfjj/2021-04-14/doc-ikmyaawa9686857.shtml)
> 概要: 原标题：“分配生”名额增加 杭州学区房酝酿再调整 作者：唐韶葵 大城市居不易，学也不易。由于杭州学区房有落户时间限制，每年1-4月份是学区房的交易高峰期。
### [长三角征信链调查：串起八城市，破除银行“戒心”是关键](https://finance.sina.com.cn/roll/2021-04-14/doc-ikmyaawa9688133.shtml)
> 概要: 原标题：长三角征信链调查：串起八城市，破除银行“戒心”是关键 企业征信问题一直是困扰很多银行的问题，尤其是异地数据调取的权限问题难以解决。
### [顶刘-刘宇宁的草根逆袭之路](https://new.qq.com/omn/20210414/20210414A0DL6A00.html)
> 概要: 没想到《长歌行》播出后，出圈的并不是男女主，而是男二女二刘宇宁和赵露思这对。            剧中，刘宇宁扮演冷酷侍卫皓都，而赵露思则扮演单纯可爱的公主李乐嫣，冷面狼侍卫恋上软萌兔公主让人磕得飞......
### [赵本山女儿球球素颜直播，卸妆后黑眼圈明显，透露自己有整容！](https://new.qq.com/omn/20210414/20210414A0DOYJ00.html)
> 概要: 提到本山大叔可以说是家喻户晓，他与宋丹丹的作品曾陪伴全国人民度过多少个大年夜晚，即使现在因为各种原因不再参演小品，但在大家心目中的地位依旧很高，现在回看他的作品依然笑点十足，相比现在的很多作品高了一个......
### [南韩顶级神颜元彬：23岁火遍亚洲，年赚千万婚礼只花6000元](https://new.qq.com/omn/20210414/20210414A0DUPS00.html)
> 概要: 提起韩国的男明星，大家也许会想到李敏镐，李钟硕，玄彬这些比较活跃的艺人。            但有这么一位艺人虽然已经退圈11年，但在韩国娱乐圈里仍然有着他的传说。韩国女神全智贤和S.H.E中的EL......
### [66岁刘晓庆回老家阵仗大，身边围数个保镖气势足，街道拥挤全是人](https://new.qq.com/omn/20210414/20210414A0DVG300.html)
> 概要: 近日，刘晓庆出现重庆街头，身边围绕数个保镖，派头十足。画面中刘晓庆穿着一身休闲装，头戴红色帽子，脚踩白色运动鞋，戴着黑超墨镜很显气质。皮肤白皙紧致，步履矫健，丝毫看不出是66岁的女性，状态很显年轻。 ......
# 小说
### [御厄有道](http://book.zongheng.com/book/898740.html)
> 作者：冬月黑兔

> 标签：奇幻玄幻

> 简介：什么？一觉醒来睡到了一千年之后？！什么？现在连鬼都进化了？！什么？灵体居然没办法处理？！让我鬼门第三十七代弟子来解决吧！来来来，大鬼小鬼都给我滚过来！什么？给钱，哎呀，不用那么多，一万两万就可以了。什么？给房，算了算了，你还是给我钱吧。什么？以身相许，哎呀，那我就啥也不要了！媳妇你别生气啊！听我解释啊！

> 章节末：第三百章 最后的蛋

> 状态：完本
# 论文
### [Supervised Domain Adaptation: Were we doing Graph Embedding all along?](https://paperswithcode.com/paper/supervised-domain-adaptation-were-we-doing-1)
> 日期：arXiv 2020

> 标签：DOMAIN ADAPTATION

> 代码：https://github.com/lukashedegaard/dage

> 描述：The performance of machine learning models tends to suffer when the distributions of the training and test data differ. Domain Adaptation is the process of closing the distribution gap between datasets.
### [Wavelet Adaptive Proper Orthogonal Decomposition for Large Scale Flow Data](https://paperswithcode.com/paper/wavelet-adaptive-proper-orthogonal)
> 日期：10 Nov 2020

> 标签：None

> 代码：https://github.com/adaptive-cfd/WABBIT

> 描述：The proper orthogonal decomposition (POD) is a powerful classical tool in fluid mechanics used, for instance, for model reduction and extraction of coherent flow features. However, its applicability to high-resolution data, as produced by three-dimensional direct numerical simulations, is limited owing to its computational complexity.
