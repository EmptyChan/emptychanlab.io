---
title: 2020-05-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LastJedi_EN-CN3271212586_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-04 18:53:01
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LastJedi_EN-CN3271212586_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：王丽坤穿拼色针织衫靓丽清新 蹲地抬行李车美腿吸睛](http://slide.ent.sina.com.cn/star/slide_4_704_338067.html)
> 概要: 组图：王丽坤穿拼色针织衫靓丽清新 蹲地抬行李车美腿吸睛
### [视频：EXO第三位！金俊勉5月14日入伍 晒手写信表白粉丝](https://video.sina.com.cn/p/ent/2020-05-04/detail-iirczymi9813878.d.html)
> 概要: 视频：EXO第三位！金俊勉5月14日入伍 晒手写信表白粉丝
### [组图：不见“星爵”陪伴 娇妻凯瑟琳挺孕肚独自遛狗状态佳](http://slide.ent.sina.com.cn/star/slide_4_704_338080.html)
> 概要: 组图：不见“星爵”陪伴 娇妻凯瑟琳挺孕肚独自遛狗状态佳
### [李易峰穿家居服直播刷牙 看粉丝弹幕惊叹"真有人"](https://ent.sina.com.cn/s/m/2020-05-04/doc-iircuyvi1278572.shtml)
> 概要: 新浪娱乐讯 5月4日是李易峰的生日。午后，峰峰突然上线，惊喜现身微博的直播间，然而就在粉丝感叹福利来得如此猝不及防之际，峰峰竟然只是在短短的三分钟内直播刷了个牙！身穿着舒适家居服的峰峰戴着眼镜，在家里......
### [没钱发工资有钱买楼？小贝夫妇被爆借款购豪宅](https://ent.sina.com.cn/s/u/2020-05-04/doc-iirczymi9802746.shtml)
> 概要: 新浪娱乐讯 据香港媒体报道 辣妹维多利亚早前被爆因疫情打击时装店的生意而逼30名员工放无薪假两个月，还要向政府申请企业纾困补助支付员工的薪酬。因而被外界批评她那么有钱还要政府帮！有网民表示：“有钱人躲......
### [怼遍整个娱乐圈，那英都怕她，却当众对张国荣撒娇太飘了](https://new.qq.com/omn/20200504/20200504A0IEMT00.html)
> 概要: 那英的性子有多直，想必大家都有所耳闻。提起她，你脑子里冒出来的第一句话是什么？“MD，最烦装逼的人。”            要问娱乐圈谁能与之一战？那必然是那英的好朋友——。            ......
### [周扬青与罗志祥分手后首晒视频，与友人度假容光焕发心情超好](https://new.qq.com/omn/20200504/20200504A0HSFZ00.html)
> 概要: 5月4日，周扬青晒出了视频，并且配文：“off to海南”，看来是要去海南度假散心，放松一下最近的心情啊。            视频中，周扬青的状态相当不错，身穿白色的连衣裙，特别适合度假，头戴米妮......
### [青你2：新一期选手粉丝活跃榜出炉，许佳琪第3，第1却不是虞书欣](https://new.qq.com/omn/20200504/20200504A0I6VB00.html)
> 概要: 最近有关《青春有你2》的热度可谓十分高，作为一档大型选秀类的综艺节目，自开播之初便受到了广泛关注，在播出的16期中出现了许多“实力强，颜值高”的练习生。众所周知，既然是比赛那就不可避免的存在排名，在《......
### [白鹿遭偷拍裙底图曝光，于正为其发声，当事人答应夏天不穿裙子](https://new.qq.com/omn/20200504/20200504A0I86M00.html)
> 概要: 5月4日中午，著名编剧于正愤怒发文谴责娱乐圈代拍和直播乱象，贴文称“他（她）们已经布满了各个宾馆的通道区域，一种前所未有的恐惧感笼罩着所有正在拍摄中的演员们”。身为某些艺人的“家长”，于正言辞中透露着......
### [《乘风破浪的姐姐》官宣，黄晓明担任发起人，选手平均年龄35岁](https://new.qq.com/omn/20200504/20200504A0IWJJ00.html)
> 概要: 今年的女团选秀简直五花八门，网友在经过了“黑料云集”的《青春有你2》洗礼后，还没回过神来《创3》就接踵而至，一时间关于新一轮的女团成为了热门话题。而鹅厂和爱奇艺都已经下场，又怎么少得了芒果台？果不其然......
# 动漫
### [《间谍过家家》累计发行数量即将突破300万！](https://news.dmzj.com/article/67283.html)
> 概要: 据日媒4日消息报道，间谍家庭喜剧漫画《间谍过家家》的漫画累计发行数，将伴随着13日发售的最新第4卷突破300万册（包含电子版）。
### [日本轻小说家笹本祐一斥责向中国索赔的愚蠢行径](https://news.dmzj.com/article/67282.html)
> 概要: 此前各界媒体曾报道了欧美国家借疫情向中国索赔的愚蠢行径，最近日本知名轻小说家笹本祐一也表达了他对这起事件的看法
### [《Muv-Luv Alternative》动画先行影像解禁](https://news.dmzj.com/article/67284.html)
> 概要: 于19年10月宣布动画化的《Muv-Luv Alternative》终于公布了首个先行影像
### [假面骑士01：大神自制零一最终形态，恐龙联组和獠牙的完美结合](https://new.qq.com/omn/20200504/20200504A0IDF000.html)
> 概要: 零一的故事已经来到了中后期的阶段，灭亡迅雷的登场标志着亚克零号即将降临。但是关于今年零一的最终形态至今没有任何相关消息，因此善于动手的大神就自制假面骑士零一的各种最终形态，其中更为夸张的是就连升华钥……
### [当叶罗丽仙子化身侠女，大家都惦记着冰公主，我却只想要毒娘娘](https://new.qq.com/omn/20200504/20200504A0GY2H00.html)
> 概要: 《精灵梦叶罗丽》是一部高人气的3D国产动漫，它不仅剧情精彩，画风也很漂亮，确实是值得一看的一部国漫。从2013年至今，叶罗丽已经更新了7年多的时间，积累了大量的粉丝。在这部动漫中有不少美丽可爱的叶罗……
### [鬼灭之刃：继亲儿子一脱成名之后，鬼杀队活得最潇洒的角色](https://new.qq.com/omn/20200504/20200504A0IF8R00.html)
> 概要: 在《鬼灭之刃》中最初让大家认定的鬼舞辻无惨堪比是鳄鱼老师笔下的亲儿子，作恶多端从无受到任何处罚，也没有什么让人虐心的遭遇，除了天生差点夭折以外在机缘巧合的情况下还被仁医变成了鬼王，从此过上了肆无忌惮……
### [适合萌新的“治愈番”有哪些？日在校园上榜，未来日记让你难忘](https://new.qq.com/omn/20200504/20200504A0CA1H00.html)
> 概要: 导读：在我们的身边，总是有一些新入圈的朋友，让自己推荐几部适合新人的动漫。每当朋友这样问我时，我都会在第一时间，向他们推荐几部温馨佳作，保证能让他们终身难忘。日在校园要说新人入圈，那肯定要是先来瞻仰……
### [《魔道祖师》漫画：莫玄羽还有一个姐姐，可惜姐弟从未相认](https://new.qq.com/omn/20200504/20200504A0G7WA00.html)
> 概要: 莫玄羽本是兰陵金氏宗主金光善的儿子，只不过身份不太光彩，是个私生子。在金光善将金光瑶接回金家之后，莫玄羽也得到了认祖归宗的机会。只不过他在兰陵金氏没住多久，就被赶回了莫家庄。后来，莫玄羽在莫家庄被人……
### [盘点《假面骑士》系列中初次登场强无敌，却疯狂吃瘪的骑士](https://new.qq.com/omn/20200504/20200504A0HKTA00.html)
> 概要: 《假面骑士》相信许多朋友都比较熟悉，似乎自从吃瘪这个说法出现以来，不管再厉害的皮套，不在地上滚两圈东映就觉得就没内味儿，有时候甚至连最终形态都跑不掉。1. 假面骑士巴隆出自《假面骑士铠武》最早开创了……
### [2020年7月新番一览表 《大春物》最终季不在其中](https://acg.gamersky.com/news/202005/1285408.shtml)
> 概要: 现在2020年7月新番最新一览表公开了最新的版本，目前一共有32部动画将于7月开播，其中有5部是之前原定4月开播的动画。
### [《星期一的丰满》：不能搞事！好身材女警战力很强](https://acg.gamersky.com/news/202005/1285410.shtml)
> 概要: 本周《星期一的丰满》为大家带来一个新的职业妹子，战斗力很强，身材也很棒的女巡警登场。这个妹子上来就制止了一场斗殴事件，之后的说教大家也得乖乖的听着。
### [杭州一小区现“假面骑士” 无法出示健康码被拦下](https://acg.gamersky.com/news/202005/1285393.shtml)
> 概要: 5月2日晚间，杭州三墩派出所接到辖区一小区保安报警，称有一“奥特曼”要进小区，但是无法核实身份。民警立即出警到现场，发现这不是奥特曼而是一个假面骑士coser。
# 财经
### [明日将迎返京高峰 你最关心的五个问题都在这](https://finance.sina.com.cn/china/gncj/2020-05-04/doc-iirczymi9828761.shtml)
> 概要: 原标题：明日将迎返京高峰，你最关心的五个问题都在这 明天（5月5日）就是“五一”假期的最后一天，各条高速公路也即将迎来返程车流最高峰。
### [梁建章：后浪没了怎么办？](https://finance.sina.com.cn/china/gncj/2020-05-04/doc-iircuyvi1294709.shtml)
> 概要: 作者：梁建章、黄文政 今年的“五四青年节”，无数人刚起床就被那个名为《后浪》的视频刷屏。对此心甚感慨者有之，对此心存质疑者亦有之...
### [湖北省连续30天无新增确诊病例](https://finance.sina.com.cn/china/gncj/2020-05-04/doc-iirczymi9817025.shtml)
> 概要: 原标题：湖北省连续30天无新增确诊病例 新京报快讯（记者 徐美慧）5月4日下午四时，湖北省新型冠状病毒肺炎疫情防控工作指挥部召开第91场新闻发布会...
### [长三角铁路出行热起来 连续4天单日旅客发送量破百万](https://finance.sina.com.cn/china/gncj/2020-05-04/doc-iirczymi9813923.shtml)
> 概要: 原标题：长三角铁路出行“热”起来 连续4天单日旅客发送量破百万 新华社上海5月4日电（记者贾远琨）五一假期，长三角铁路出行如逐渐升高的气温一般“热”起来。
### [专家学者建言湖北企业疫后“危中寻机”](https://finance.sina.com.cn/roll/2020-05-04/doc-iircuyvi1300707.shtml)
> 概要: 原标题：专家学者建言湖北企业疫后“危中寻机” （抗击新冠肺炎）专家学者建言湖北企业疫后“危中寻机” 中新社武汉5月4日电 （记者 梁婷）由武汉大学经济与管理学院主办的振兴...
### [国家卫健委：做好从境外到国门再到家门的全链条管理](https://finance.sina.com.cn/china/gncj/2020-05-04/doc-iirczymi9811800.shtml)
> 概要: 原标题：国家卫健委：做好从“境外”到“国门”再到“家门”的全链条管理 国家卫健委新闻发言人米锋4日在国务院联防联控机制新闻发布会上说，5月3日...
# 科技
### [基于Spark的点击率预测模型（LR，FM，XGBoost，XGBoostLR，XGBoostFM）](https://www.ctolib.com/JeemyJohn-spark-ctr-models.html)
> 概要: 基于Spark的点击率预测模型（LR，FM，XGBoost，XGBoostLR，XGBoostFM）
### [使用React，Material UI和Firebase构建的现代URL缩短服务](https://www.ctolib.com/xprilion-fireshort.html)
> 概要: 使用React，Material UI和Firebase构建的现代URL缩短服务
### [Python库可下载YouTube内容并检索元数据](https://www.ctolib.com/mps-youtube-pafy.html)
> 概要: Python库可下载YouTube内容并检索元数据
### [supervenn: 精确易读的多套Python可视化](https://www.ctolib.com/gecko984-supervenn.html)
> 概要: supervenn: 精确易读的多套Python可视化
### [五一直播带货：虚拟主播PK真人主播，洛天依坑位费90万超老罗](https://www.tuicool.com/articles/rERjAfj)
> 概要: 洛天依、乐正绫淘宝直播五一假期直播带货更加火爆了。各大平台纷纷拉上主播们，推出特别直播活动。淘宝直播这次瞄准了Z世代年轻人们，4月30日-5月5日推出「云端动漫嘉年华」，还打造虚拟偶像天团直播间，玩起......
### [Jenkins kubernetes插件的原理](https://www.tuicool.com/articles/jiiYbuz)
> 概要: 技术干货，可收藏，点“在看”后，再看。如何使用使用Kubernetes插件时，我们需要做三件事情：根据官方文档，在Jenkins上加入kubernetes配置。在Jenkinsfile中加入kuber......
### [五一线下娱乐寒冰未解，但有玩家已经准备抄底扩张](https://www.tuicool.com/articles/neQzIz6)
> 概要: 编者按：本文来自微信公众号“连线Insight”（ID:lxinsight），作者：关渡，36氪经授权发布。文/关渡 编辑/水笙沉寂已久的线下娱乐，在今年五一有了一波局部复苏。苏芸在西湖边上的酒吧工作......
### [UWP推荐一款很Fluent Design的bilibili UWP客户端 ： 哔哩](https://www.tuicool.com/articles/mI3yqq3)
> 概要: UWP已经有好几个Bilibili的客户端，最近有多了一个：哔哩 - Microsoft Store作者云之幻是一位很擅长设计的UWP开发者，我也从他那里学到了很多设计方面的技巧。它还是一位Bilib......
### [三星 S20 Ultra DXOMARK 自拍评分 100，排名第二](https://www.ithome.com/0/485/655.htm)
> 概要: IT之家5月4日消息 三星Galaxy S20 Ultra是该公司最新的高端智能手机，配备有最新的Exynos 990芯片组，6.9英寸AMOLED显示屏，大容量5000毫安电池，先进的后置四摄像头配......
### [微软 Win10 仍存在删除个人配置文件数据 Bug](https://www.ithome.com/0/485/606.htm)
> 概要: IT之家5月4日消息 外媒Windows Latest报道，一些安装了Windows 10最新更新（包括KB4549951）的用户报告称，他们的用户配置文件以及所有数据，自定义选项和文档在登录时均未加......
### [单打孔屏+后置单摄：谷歌Pixel 4a最新渲染图曝光](https://www.ithome.com/0/485/630.htm)
> 概要: IT之家5月4日消息 据Exbulletin报道，有关Google Pixel 4a逐渐增多，近日xleaks7与PIGTOU合作打造了一组Pixel 4a的最新渲染图。IT之家了解到，Google ......
### [AMD R7 4700G 首曝：8核16线程，主频3.6GHz](https://www.ithome.com/0/485/633.htm)
> 概要: IT之家5月4日消息 AMD 新款桌面APU R7 4700G首次曝光，8核16线程，主频3.6GHz，核显规格未知。从曝光的信息来看，R7 4700G的CPU规格与R7 3700X规格相似，都是8核......
# 小说
### [永恒杀神](http://book.zongheng.com/book/603287.html)
> 作者：跳子琪

> 标签：奇幻玄幻

> 简介：落魄少年，因祸得福，收封天神印，修太古功法，集无敌神通。   新书《都市狂仙》已上传  书友群486237168

> 章节末：第三百八十九章　永恒杀神

> 状态：完本
### [明风九州行](http://book.zongheng.com/book/643626.html)
> 作者：东方青玄

> 标签：武侠仙侠

> 简介：龙起大明，有船西行。握一柄“残”剑，挂一葫清酒，牵一颗醉心，公子淡淡而笑之。多歧路，今犹在，纵使仙魔魑魅为拦阻，此生亦无悔。待从头，望青山，只留美名不世谈。  而江湖又有歌曰，天若有情天亦老，公子怜花冷声笑：“十年漂泊过伶仃，一柄苦剑，醉点凄凉。浮生若梦恍隔世，热枕心，不自已。我道苍天怜世人，可笑天意弄意何。叹人生，不过三千世界一尘埃，繁华归处，尽是虚无。”

> 章节末：第四十章  繁尘似水滚滚流 （2）大结局

> 状态：完本
# 游戏
### [《使命召唤6：现代战争2 重制版》PC版性能表现分析](https://www.3dmgame.com/news/202005/3787724.html)
> 概要: 经过一个月的等待，动视终于发布了《使命召唤6：现代战争2 重制版》的PC版。本次复刻提升了贴图材质、动作动画、物理渲染、HDR光照等。外媒DSOGaming对本作PC版的性能表现进行了分析，以下是文章......
### [全球PC浏览器大战：谷歌Chrome独占69.18％份额](https://www.3dmgame.com/news/202005/3787717.html)
> 概要: 统计调查机构Netmarketshare发布了2020年4月的最新的市场份额。数据显示，Windows 10的全球份额与2020年3月的数据相比出现下降，降到了56%，而Linux和macOS系统份额......
### [《最后的生还者2》多人模式泄露 物品制作系统曝光](https://www.3dmgame.com/news/202005/3787720.html)
> 概要: 《最后的生还者2》前段时间遭到泄露和剧透，但除了剧透以外，我们还提前看到了这款游戏的多人模式内容。本文将只讨论多人模式，不会剧透单机剧情内容。在这几张截图的UI界面里，我们可以观察到很多细节。首先是“......
### [吉恩军魂不灭 最新《高达》主题夏亚专用眼镜公开](https://www.3dmgame.com/news/202005/3787730.html)
> 概要: 经典IP《机动战士高达》正在四处联动各行业发挥着余热，近日来自OWNDAYS品牌的最新《高达》主题夏亚专用眼镜公开，戴上它即使米诺夫粒子中也能看见敌人了！·绰号“赤色彗星”的夏亚已经代言过多种商品，这......
### [尽管有《半条命》拉动 但Steam VR用户还不到2%](https://www.3dmgame.com/news/202005/3787710.html)
> 概要: 根据V社，《半条命：Alyx》激发了大量用户购买VR头盔，但最新的一份调查指出，拥有VR头盔的Steam用户还不到2%，仅仅1.9%。自《半条命：Alyx》今年3月底发售后，V社公布了VR硬件调查，V......
# 论文
### [An interpretable probabilistic machine learning method for heterogeneous longitudinal studies](https://paperswithcode.com/paper/an-interpretable-probabilistic-machine)
> 日期：7 Dec 2019

> 标签：

> 代码：https://github.com/jtimonen/lgpr

> 描述：Identifying risk factors from longitudinal data requires statistical tools that are not restricted to linear models, yet provide interpretable associations between different types of covariates and a response variable. Here, we present a widely applicable and interpretable probabilistic machine learning method for nonparametric longitudinal data analysis using additive Gaussian process regression.
### [AutoML: Exploration v.s. Exploitation](https://paperswithcode.com/paper/automl-exploration-vs-exploitation)
> 日期：23 Dec 2019

> 标签：AUTOML

> 代码：https://github.com/DataSystemsGroupUT/automl_exploration_vs_exploitation

> 描述：Building a machine learning (ML) pipeline in an automated way is a crucial and complex task as it is constrained with the available time budget and resources. This encouraged the research community to introduce several solutions to utilize the available time and resources.
### [Towards a General Theory of Infinite-Width Limits of Neural Classifiers](https://paperswithcode.com/paper/towards-a-general-theory-of-infinite-width)
> 日期：12 Mar 2020

> 标签：None

> 代码：https://github.com/deepmipt/infinite-width_nets

> 描述：Obtaining theoretical guarantees for neural networks training appears to be a hard problem in a general case. Recent research has been focused on studying this problem in the limit of infinite width and two different theories have been developed: mean-field (MF) and kernel limit theories.
