---
title: 2020-08-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeaFireflies_EN-CN1245095359_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-08-11 21:23:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeaFireflies_EN-CN1245095359_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [李宇春加盟《好声音》 谢霆锋调侃用好吃的抢学员](https://ent.sina.com.cn/music/zy/2020-08-11/doc-iivhvpwy0398416.shtml)
> 概要: 新浪娱乐讯 8月10号，《2020中国好声音》发布会顺利举行。发布会上，导师李宇春、李荣浩、谢霆锋、李健悉数亮相现场，大谈选人标准以及对原创音乐的各自理解。　　在李宇春之前，好声音有两位女导师都带出了......
### [组图：海清与儿子同框现身机场 14岁“蛋妞”身高已超过妈妈](http://slide.ent.sina.com.cn/star/slide_4_704_343366.html)
> 概要: 组图：海清与儿子同框现身机场 14岁“蛋妞”身高已超过妈妈
### [组图：腿精！蓝盈莹黑超遮面大方任拍 顶利落短发穿超短裤秀美腿](http://slide.ent.sina.com.cn/z/v/slide_4_704_343337.html)
> 概要: 组图：腿精！蓝盈莹黑超遮面大方任拍 顶利落短发穿超短裤秀美腿
### [视频：张萌写超长小作文回应淘汰 情真意切被赞文笔好](https://video.sina.com.cn/p/ent/2020-08-11/detail-iivhvpwy0383817.d.html)
> 概要: 视频：张萌写超长小作文回应淘汰 情真意切被赞文笔好
### [组图：甜中带尴尬！谭松韵宋威龙树咚吻被百人围观](http://slide.ent.sina.com.cn/tv/slide_4_86512_343350.html)
> 概要: 组图：甜中带尴尬！谭松韵宋威龙树咚吻被百人围观
### [谢杏芳和闺蜜吐槽林丹太臭美，没事就照镜子，自己还被他嫌弃胖](https://new.qq.com/omn/20200811/20200811V0EJ5I00.html)
> 概要: 谢杏芳和闺蜜吐槽林丹太臭美，没事就照镜子，自己还被他嫌弃胖
### [李现双男主新作正式官宣！携手当红人气偶像，首度饰演“狐妖”](https://new.qq.com/omn/20200811/20200811A0H3LW00.html)
> 概要: 李现双男主新作正式官宣！携手当红人气偶像，首度饰演“狐妖”
### [肖战粉丝又闯祸了！汪海林提议禁止中小学生应援明星，获网友认可](https://new.qq.com/omn/20200811/20200811A0IVGH00.html)
> 概要: 如今流量明星四处泛滥，但要论能够称得上顶流巨星的，却是寥寥无几，而肖战就是其中的一个。他自出道以来，一直备受外界的关注与热议，主演过多部优秀的影视作品，俘获了无数影迷粉丝的心，名副其实的颜值与实力并存......
### [太离谱！钟镇涛女儿结婚冲上热搜，原视频曝光却是7年前的MV画面](https://new.qq.com/omn/20200811/20200811A0EXH200.html)
> 概要: 太离谱！钟镇涛女儿结婚冲上热搜，原视频曝光却是7年前的MV画面
### [肖战：求求你停止散发魅力吧！](https://new.qq.com/omn/20200811/20200811A0IR9G00.html)
> 概要: 果然，今天（8月11日）又是被肖战霸屏的一天源于肖战工作室在8月10日晚发布了一条肖战的小日常视频，视频中肖战还是依旧帅气迷人，不管是逗小猫还是喝咖啡，都散发着独特的魅力，而这段视频在发布后也获得了四......
# 动漫
### [游戏王历史：从零开始的游戏王环境之旅第五期11](https://news.dmzj.com/article/68245.html)
> 概要: 在这大量过于强大的卡牌充斥整个环境的时候出现了一颗新星，那就是一张叫做“命运抽卡”的魔法卡。
### [漫画《平稳世代的韦驮天们》TV动画化决定](https://news.dmzj.com/article/68239.html)
> 概要: 天原原作、COOL教信者作画的漫画《平稳世代的韦驮天们》正式宣布了TV动画化决定的消息。
### [为抢《宝可梦GO》道馆 男性出手打伤熟人被逮捕](https://news.dmzj.com/article/68242.html)
> 概要: 日本北海道警方宣布在10日现场逮捕了涉嫌暴行和损坏财产的犯罪嫌疑人佐藤兼寿（56岁）。
### [论如何在暧昧的情况下还是只单身狗](https://news.dmzj.com/article/68236.html)
> 概要: 这期给大家推荐一部职场爱情漫画《不坦率的大姐姐》，作者したらなな，碳酸NG汉化汉化，讲述不坦率的前辈与后辈之间的爱情。
### [关于漫展的一些事，尊重coser是本分，送零食并不是特别的暗示](https://new.qq.com/omn/20200811/20200811A0JTRK00.html)
> 概要: 大家好，我是热爱二次元的小林，漫展可以说是大多数二次元爱好者的节日，每场漫展都有许多来自全国各地的小伙伴参加，漫展应该是三次元集结地，在这个盛宴上，你能够看到漂亮的coser小姐姐，也能够看到自己喜……
### [爆笑校园：呆头送给茵茵送驻颜用品！](https://new.qq.com/omn/20200811/20200811A0G4IF00.html)
> 概要: ...
### [悲报，继北美之后，澳洲也开始封杀日本萝莉动漫了](https://new.qq.com/omn/20200811/20200811A0HQM700.html)
> 概要: 这次真是出乎意料了。美亚那边陆续以“不符合世俗基准”为由下架包括《俺妹》、《游戏人生》、《埃罗芒阿老师》等作品还没过一个月，澳洲这边也迅速做出反应开始对日本萝莉题材动漫下手了。之前美亚宣布下架消息时……
### [非人哉动漫：大士与猴哥的“恩怨”，看样子一时半会还解决不了！](https://new.qq.com/omn/20200811/20200811A0HD6W00.html)
> 概要: 非人哉动漫：大士与猴哥的“恩怨”，看样子一时半会还解决不了！
### [《雾山五行》短短的三集就达到国产动漫的巅峰了吗？](https://new.qq.com/omn/20200811/20200811A0IT4Z00.html)
> 概要: 大家好啊，你们的失踪人口总算回归了，最近一直隐身，除了家里有些必要事需要处理以外，还有目前素材实在匮乏，因此我特意去补充一番营养，像《死神》、《进击的巨人》以及《鬼灭之刃》的漫画都翻了看一遍，所以接……
### [当“山寨雕像”出现在主题公园，《海贼王》被毁，艾莎女神变大妈](https://new.qq.com/omn/20200811/20200811A0HS9A00.html)
> 概要: 大家好，我是你们的速递君。导语：现在二次元文化发展迅猛，每年都有着许多优秀的作品登场，可以说现在二次元文化在融入了主流文化之后，成为了我们日常中不可缺少的元素 。文/速递君随着许多优秀动漫作品出现后……
### [《银魂》完全新作剧场版详情公开 2021年1月8日上映](https://acg.gamersky.com/news/202008/1311957.shtml)
> 概要: 之前《银魂》官方宣布将制作全新的剧场版动画，现在剧场版的详情公开，本作的标题为《银魂 THE FINAL》，将于2021年1月8日在日本上映。
### [衍生动画《派大星秀》即将推出 派大星主持脱口秀](https://acg.gamersky.com/news/202008/1311826.shtml)
> 概要: 根据外媒deadline报道，《海绵宝宝》的衍生动画《派大星秀》(The Patrick Star Show)正在开发中，本作的主角是派大星，内容是派大星主持自己的深夜脱口秀。
# 财经
### [北京排水集团按特级防汛响应备勤 应对明日入汛最强降雨](https://finance.sina.com.cn/china/2020-08-11/doc-iivhvpwy0477983.shtml)
> 概要: 北京排水集团按特级防汛响应备勤 应对明日入汛最强降雨 新京报快讯（记者 黄哲程）明日（8月12日）北京将遭遇入汛以来最强降雨。
### [微软断供传言流行背后：中国软件业底色如何？](https://finance.sina.com.cn/china/2020-08-11/doc-iivhvpwy0468379.shtml)
> 概要: 原标题：微软断供传言流行背后：中美科技冷战恐升级，中国软件业底色如何 《财经》记者王凤  一个跨国公司对于自身政治风险的预防性条款被过度解读的背后...
### [2019年中国人均国民总收入10410美元 你被平均了吗？](https://finance.sina.com.cn/roll/2020-08-11/doc-iivhvpwy0471837.shtml)
> 概要: 原标题：2019年，中国人均国民总收入10410美元！你又被平均了？ 突破1万美元大关 8月7日，国家统计局公布的最新数据显示，2019年...
### [八达岭长城、圆明园等景区12日临时关闭 因北京将有大到暴雨](https://finance.sina.com.cn/china/gncj/2020-08-11/doc-iivhuipn8100364.shtml)
> 概要: 原标题：八达岭长城、圆明园遗址公园等景区12日关闭 来源：八达岭长城、圆明园遗址公园 新京报快讯 11日晚，八达岭长城、圆明园遗址公园在其官博发布消息称：12日早晨至夜...
### [为稳外贸跑出“海关速度”](https://finance.sina.com.cn/china/gncj/2020-08-11/doc-iivhuipn8093474.shtml)
> 概要: 原标题：为稳外贸跑出“海关速度” 新华社天津8月11日电 题：为稳外贸跑出“海关速度” 新华社记者张宇琪 10日晚，地处渤海湾西侧的中国北方第一大港天津港，灯火通明...
### [央行：7月住户存款减少7195亿](https://finance.sina.com.cn/china/2020-08-11/doc-iivhvpwy0465290.shtml)
> 概要: 11日，央行公布2020年7月金融统计数据报告。数据显示，7月份人民币存款增加803亿元，同比少增5617亿元。其中，住户存款减少7195亿元，非金融企业存款减少1.55万亿元...
# 科技
### [Typescript 设计模式之工厂方法](https://segmentfault.com/a/1190000023576290)
> 概要: 在现实生活中，工厂是负责生产产品的，比如牛奶、面包或礼物等，这些产品满足了我们日常的生理需求。此外，在日常生活中，我们也离不开大大小小的系统，这些系统是由不同的组件对象构成。而作为一名 Web 软件开......
### [持续输出面试题系列之ZooKeeper篇](https://segmentfault.com/a/1190000023573430)
> 概要: 开篇介绍大家好，我是Java最全面试题库的提裤姐，今天这篇是JavaEE面试题系列的第十二篇，主要总结了ZooKeeper相关的面试题；在后续，会沿着第一篇开篇的知识线路一直总结下去，做到日更！如果我......
### [看好 iPhone 12 超级更新周期，分析师再次上调苹果目标股价](https://www.ithome.com/0/502/756.htm)
> 概要: 据国外媒体报道，果粉们期待已久的苹果首批支持 5G 网络连接的 iPhone 12，已经确定将会延期几周发售，但这并未影响外界对苹果这一系列 5G 新品的期待，也未影响分析师对 iPhone 12 的......
### [雷军：小米 MIUI 系统先在别人家手机上做，启动页面列出 100 位粉丝名字](https://www.ithome.com/0/502/775.htm)
> 概要: IT之家8月11日消息 今天晚上，小米CEO雷军开启十周年演讲。2010年4月6日，小米成立。小米公司虽然从没做过手机，但我们有一个脑洞大开的梦想：做全球最好的手机，只卖一半的价钱，让每个人都买得起......
### [The day I accidentally built a nudity/porn platform](https://elazzabi.com/2020/08/11/the-day-i-accidentally-built-a-nudity-porn-platform/)
> 概要: The day I accidentally built a nudity/porn platform
### [Emacs 27.1](https://lists.gnu.org/archive/html/emacs-devel/2020-08/msg00237.html)
> 概要: Emacs 27.1
### [为什么多数成功创业者都有“浑球儿思维”？](https://www.huxiu.com/article/374680.html)
> 概要: 人生的一大难题是情面。我们很多时候都会顾及他人的情面，在应该行动的时候犹豫不决。可有一类人，他们丝毫不受情面影响，朝着自己的目标坚定行动，看起来甚至有些浑球儿。浑球儿清晰地定义了自己要讨好谁，并且非常......
### [6个人撑足中国面子：不把他们拍下来就是失职](https://www.huxiu.com/article/374710.html)
> 概要: 本文来自微信公众号：一条（ID：yitiaotv），自述：张同道，编辑：倪楚娇，原文标题：《央视神仙纪录片，6个人撑足中国面子：不把他们拍下来就是失职》，头图来自：一条今年7月，央视纪录片《文学的故乡......
### [TensorFlow最出色的30个机器学习数据集](https://www.tuicool.com/articles/m2iYNjN)
> 概要: 字幕组双语原文：TensorFlow最出色的30个机器学习数据集英语原文：30 Largest TensorFlow Datasets for Machine Learning翻译：雷锋字幕组（che......
### [先融合再填充，上海交大提出少样本图像生成新方法F2GAN](https://www.tuicool.com/articles/jiAJnqI)
> 概要: 少样本图像生成（few-shot image generation）任务是指用已知类别（seen category）的大量图片训练出一个生成模型，然后给定某个未知类别（unseen category）......
# 小说
### [长风一梦入轮回](http://book.zongheng.com/book/843230.html)
> 作者：悟道心

> 标签：武侠仙侠

> 简介：浩劫来临之际，正道江河日下之时，看神秘书生如何与暗势力展开一场场的斗智斗法……

> 章节末：第一百七十二章  尾声（大结局）

> 状态：完本
# 游戏
### [电影《八佰》终极预告发布 将于8月21日全国上映](https://www.3dmgame.com/news/202008/3794919.html)
> 概要: 近日电影《八佰》终极预告发布，描绘被战争裹挟的底层士兵群像，他们有挣扎有牵挂，但大敌当前，却舍弃小家挺身而出。影片片长147分钟，以国语2D版本发行，密钥期限是8月21日至9月21日每天9点至23点......
### [经典名作《妖怪人贝姆》真人电影预告 9月11日上映](https://www.3dmgame.com/news/202008/3794941.html)
> 概要: 经典名作《妖怪人贝姆》的衍生真人电影版《妖怪人贝拉》预定9月11日上映，8月11日今天官方公开了最新预告，一起来先睹为快。·《妖怪人贝姆》讲述了在港湾都市“里布拉市”，作为政治经济文化中心将城市中的“......
### [PS4版《漫威复仇者联盟》测试文件发现三名新角色](https://www.3dmgame.com/news/202008/3794910.html)
> 概要: 前几天PS4版《漫威复仇者联盟》开始向预购用户开启限时测试，在这次测试中，有数据挖掘发现本作的开发商水晶动力在游戏代码中包含三名尚未公布的角色，他们分别是：女绿巨人、凯特·毕肖普和战争机器。根据国外游......
### [《火焰纹章英雄》将开启声优座谈会 一同回顾30年！](https://shouyou.3dmgame.com/news/51322.html)
> 概要: 根据《火焰纹章：英雄》官推公开的新消息，官方将于北京时间8月14下午5点举办“直播座谈会”，届时会有一些知名声优嘉宾参与，一同回顾《火焰纹章》30年，此次活动不会公开与游戏相关的新情报。官推截图：据悉......
### [《海贼无双4》DLC角色基拉截图 杀戮武士高速攻击](https://www.3dmgame.com/news/202008/3794939.html)
> 概要: 近日万代南梦宫公布了《海贼无双4》第五名DLC角色基拉的新截图以及介绍。杀戮武士基拉将作为可用角色参战，收录进第二弹DLC角色包内。基拉是基德海贼团的战斗员，被人称为“杀戮武士”，也是“最可怕世代”的......
# 论文
### [Gaussian Embedding of Large-scale Attributed Graphs](https://paperswithcode.com/paper/gaussian-embedding-of-large-scale-attributed)
> 日期：2 Dec 2019

> 标签：GRAPH EMBEDDING

> 代码：https://github.com/bhagya-hettige/GLACE

> 描述：Graph embedding methods transform high-dimensional and complex graph contents into low-dimensional representations. They are useful for a wide range of graph analysis tasks including link prediction, node classification, recommendation and visualization.
