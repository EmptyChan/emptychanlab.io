---
title: 2020-05-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WildflowerWeek_EN-CN0198560289_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-07 21:28:41
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WildflowerWeek_EN-CN0198560289_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [德尼罗宣布翠贝卡电影节将在汽车影院举行放映](https://ent.sina.com.cn/m/f/2020-05-07/doc-iirczymk0328000.shtml)
> 概要: 新京报讯 （记者 李妍）5月7日，据外媒消息，由于受到疫情影响，全球电影业务几乎无法运营，翠贝卡电影节举办方近日正与IMAX公司和美国电话电报AT&T合作推出“翠贝卡汽车放映”计划，并将于今年夏天在美......
### [马浚伟方就得罪黎明等谣言发律师声明 将依法维权](https://ent.sina.com.cn/s/h/2020-05-07/doc-iircuyvi1730604.shtml)
> 概要: 新浪娱乐讯 5月7日，马浚伟方就网络不实言论委托律师发布律师声明，声明中提到，近日有部分网络用户在互联网平台发布马浚伟“得罪黎明、刘德华被泼粪”等失实言论，严重损害了马浚伟的名誉和公众形象，构成对其名......
### [组图：秦牛正威淘汰后再现身 大眼睛水灵获赠“牛油果玩偶”](http://slide.ent.sina.com.cn/z/v/slide_4_704_338272.html)
> 概要: 组图：秦牛正威淘汰后再现身 大眼睛水灵获赠“牛油果玩偶”
### [王晶辟谣迪丽热巴饰演新《倚天》小昭：绝对不是](https://ent.sina.com.cn/m/c/2020-05-07/doc-iirczymk0345712.shtml)
> 概要: 新浪娱乐讯 5月7日，王晶在微博发文辟谣网络盛传的《新倚天屠龙记》中小昭是迪丽热巴一事，他表示：“今天撤下了微博是因为想换上高清照片，过两天会重新发。另外想辟个谣。小昭…………………绝对不是迪丽热巴......
### [视频：中信银行就泄露信息向池子道歉 支行行长已撤职](https://video.sina.com.cn/p/ent/2020-05-07/detail-iircuyvi1722093.d.html)
> 概要: 视频：中信银行就泄露信息向池子道歉 支行行长已撤职
### [笑果文化调取池子个人银行流水 上海虹口法院回应：没有签发过调查令](https://new.qq.com/omn/20200507/20200507A0J6EP00.html)
> 概要: 5月6日，脱口秀演员王越池（艺名“池子”）发布声明指责中信银行股份有限公司泄露其个人账户交易信息。王越池在该声明中指出，在他向此前供职的笑果文化提出和平解约未果的情况下，双方均提出了仲裁，笑果文化让其......
### [张大奕开心自拍神采奕奕，怒斥网友：你们是什么人？不爱看走开](https://new.qq.com/omn/20200507/20200507A0NJHI00.html)
> 概要: 此前总裁夫人发文手撕网红张大奕，随后处理结果被公开，总裁蒋凡被除名阿里合伙人，被降级记过，奖金也被取消，辉煌的事业被糟蹋了。同时有消息指出，总裁蒋凡和夫人婚姻感情并不是很好，产生了很大的隔阂，似乎确实......
### [田馥甄挺罗志祥代价有多大？知情人曝又丢一代言，被认为三观与品牌理念不合](https://new.qq.com/omn/20200507/20200507A0MIGE00.html)
> 概要: 罗志祥被前女友周扬青曝出私生活混乱，导致现在名誉扫地，在演艺圈打拼多年的事业也险些毁于一旦。网友纷纷唾骂，粉丝脱粉回踩，昔日仇人发文暗讽。在这种情况下，好友们也只能选择明哲保身，几乎没有人敢站出来为罗......
### [TVB最成功的续集，名副其实“降魔2.0”！](https://new.qq.com/omn/20200507/20200507A0CYJK00.html)
> 概要: 2017年曾有一部港剧黑马，它成了那年港媒和网友在讨论区“洗屏式”讨论的神迹，那就是由马国明主演的《降魔的》。            众所周知，《降魔的》是作为TVB在《僵》大张旗鼓试水后却遭遇滑铁卢......
### [《妻子4》路透来袭，张歆艺蔡少芬产后瘦身差距大](https://new.qq.com/omn/20200507/20200507V0N11S00.html)
> 概要: 《妻子4》路透来袭，张歆艺蔡少芬产后瘦身差距大
# 动漫
### [动画《五等分的花嫁》第二季延期播出](https://news.dmzj.com/article/67303.html)
> 概要: 根据春场葱原作制作的TV动画《五等分的花嫁》第二季宣布了延期播出的消息，播出日期从原定的2020年10月变更为2021年1月。
### [漫画《鬼灭之刃》系列单行本累计突破6000万部](https://news.dmzj.com/article/67305.html)
> 概要: 由吾峠呼世晴创作的《鬼灭之刃》系列算上将在5月13日发售的单行本20卷，以及电子版，累计发行部数突破了6000万部。与今年2月宣布了突破4000万部相比，累计发行部数上升了2000万部。
### [写手变枪手？这份阅文霸王合约，810万网文作者该跪着签下吗](https://news.dmzj.com/article/67304.html)
> 概要: 今年的五一可谓非常特殊，这一年的劳动节小长假，不少网文作者们史无前例地没有在家疯狂赶稿，而是以各种借口停止更新一天，这一天也因此被称作“五五断更节”……
### [GSC《鬼灭之刃》我妻善逸粘土人8日开订](https://news.dmzj.com/article/67311.html)
> 概要: GSC根据《鬼灭之刃》中的我妻善逸制作的粘土人将于明天（8日）开始预订。本作包括了通常颜、睡眠颜、害羞颜和惊愕颜，另外配有日轮刀和再现壹之型霹雳一闪的配件。
### [网友自制《魔道祖师》手办，一元一个，谁买谁吃亏](https://new.qq.com/omn/20200507/20200507A0I18T00.html)
> 概要: 喜欢动漫的人，都会买一些手办。现在的手办行业，可以说不太正规，价格太高了。一个普通的手办，都得卖一千多，一般人很难消费得起。无奈只能自己动手做手办了，这不，《魔道祖师》的粉丝，就自制了一组手办，还出……
### [小猪佩奇居然也有山寨版？这下终于轮到00后被毁童年了](https://new.qq.com/omn/20200507/20200507A09IZM00.html)
> 概要: 动漫我想很多人都会看，但是有一些人因为制作不出精品的动漫作品就会选择抄袭或者山寨一些动漫。在近期我看到了一部动漫，这部动漫抄袭的是《小猪佩奇》，就让我带大家看看吧。这部山寨版的小猪佩奇是一部名为《小……
### [辉夜大小姐：早坂爱的三种形态，你更喜欢哪一种？](https://new.qq.com/omn/20200507/20200507A0HZVQ00.html)
> 概要: 《辉夜大小姐》动画已经播出近半，动画中的很多角色都给人留下了深刻印象，尤其是学生会的两位女主，四宫辉夜和藤原千花依然保持着上一季的高人气，这一季她们的表情包依然有不少。不过在动画里配角也一样有他们的……
### [海贼王979话情报：罗宾称甚平很成熟，路飞又要引起“大骚动”](https://new.qq.com/omn/20200507/20200507A0IVRX00.html)
> 概要: 不出意外的话，明天将会出现英文版的海贼王979话，而官方漫画则要等到下周一。因此，会有一些粉丝进行翻译，看看自然是好的，但更喜欢能支持正版哈，支持作者尾田哩。虽然漫画尚未更新，但大量情报内容已经在一……
### [非人哉：观音家里来新室友，白泽帮忙腾地方，谁知观音太无情！](https://new.qq.com/omn/20200507/20200507A0NKHR00.html)
> 概要: 又是阳光明媚的一天，各位亲爱的小伙伴们大家好！今天小编为大家带来非常搞笑的非人哉漫画。虽然目前是放长假的节日，不过劳动最光荣，小编还有为大家带来开心。希望大家能够喜欢本次的动漫，喜欢的朋友也可以关注……
### [混血赛亚人天赋更高？真的是这样吗？](https://new.qq.com/omn/20200507/20200507A0MLUY00.html)
> 概要: 《龙珠》系列动漫是日本漫画家鸟山明老师的代表作品，而如今《龙珠》系列动漫也已经陪伴很多人走过了整个青春，在这部动漫之中，悟空作为一名生长在地球上的赛亚人，一直在为了保护地球的和平而战，最终成为了全宇……
### [《五等分的花嫁》第二季宣布延期 2021年1月开播](https://acg.gamersky.com/news/202005/1286090.shtml)
> 概要: 由于受到疫情影响，原定于2020年10月播出的第二季《五等分的花嫁∬》将延期至2021年1月播出。
### [《大春物》第三季动画7月开播 完结篇终于要来](https://acg.gamersky.com/news/202005/1286105.shtml)
> 概要: 《我的青春恋爱物语果然有问题》第三季动画，在之前因为疫情宣布推迟播出，现在官方终于宣布本作动画将于7月开播，大家期待的完结篇终于要来了。
# 财经
### [今日财经TOP10|商务部：将全面落实外商投资法及实施条例](https://finance.sina.com.cn/china/gncj/2020-05-07/doc-iircuyvi1862134.shtml)
> 概要: 【宏观要闻】 NO.1 商务部：将全面落实外商投资法及其实施条例 商务部新闻发言人高峰表示，坚持内外资一致是我们在制定外商投资法过程中坚持的一项重要原则。
### [东莞：商办房改建租赁住房后用地调整为居住 改建房不得销售](https://finance.sina.com.cn/china/dfjj/2020-05-07/doc-iircuyvi1859775.shtml)
> 概要: 原标题：东莞：商办房改建租赁住房后用地调整为居住，改建房不得销售 5月7日，广东省东莞市下发《关于印发&lt;东莞市关于加快培育和发展住房租赁市场的实施意见&gt;的通知...
### [大湾区一季度就业观察：9市出台逾百项政策稳就业促转型](https://finance.sina.com.cn/china/dfjj/2020-05-07/doc-iircuyvi1849524.shtml)
> 概要: 原标题：大湾区一季度就业观察：广深港澳失业率略超2%，湾区9市出台逾百项政策稳就业促转型 来源：21世纪经济报道 据统计，今年一季度全国城镇新增就业229万人...
### [4月中国出口数据 实在太太太意外了](https://finance.sina.com.cn/china/gncj/2020-05-07/doc-iirczymk0380287.shtml)
> 概要: 来源：国是直通车 外贸企业需要专属“红包” △5月7日，江苏连云港港码头，一批风电叶片正在装船出口海外。中新社发 耿玉和 摄 中国出口爆了个“冷门”。
### [甘肃省长震撼发言：保中小微企业 十万火急](https://finance.sina.com.cn/china/gncj/2020-05-07/doc-iirczymk0387432.shtml)
> 概要: 原标题：省长震撼发言：保中小微企业，十万火急 作者：唐仁健 甘肃省委副书记 省长 来源：甘肃日报 五一节前，甘肃省政府召开“全省中小微企业发展推进会”，省委副书记...
### [近3亿农民工分布变迁 跨省流动持续下降](https://finance.sina.com.cn/roll/2020-05-07/doc-iircuyvi1858234.shtml)
> 概要: 原标题：近3亿农民工分布变迁，跨省流动持续下降 随着产业的转型升级，近年来农民工的区域分布正在发生改变。 中西部省内就业占比增加...
# 科技
### [Luneta 命令行模糊查找器](https://www.ctolib.com/fbeline-luneta.html)
> 概要: Luneta 命令行模糊查找器
### [PHNotepad是用Java编写的简单Java文本/代码编辑器（记事本）](https://www.ctolib.com/pH-7-Simple-Java-Text-Editor.html)
> 概要: PHNotepad是用Java编写的简单Java文本/代码编辑器（记事本）
### [ndindex 一个用于操作ndarray索引的Python库](https://www.ctolib.com/Quansight-ndindex.html)
> 概要: ndindex 一个用于操作ndarray索引的Python库
### [dSock 一全是分布式WebSocket broker（在Go中，使用Redis）](https://www.ctolib.com/Cretezy-dSock.html)
> 概要: dSock 一全是分布式WebSocket broker（在Go中，使用Redis）
### [刚刚，GitHub重磅发布四大新功能！](https://www.tuicool.com/articles/eQfuymJ)
> 概要: 作者 | 万佳、小智今天，GitHub 在线上举办了 Satellite 2020。每年的 Satellite 是 GitHub 年度最大型的国际产品和社区活动，由于受新型冠状病毒肺炎疫情的影响，Gi......
### [家居零售的消费路径，是时候被颠覆了](https://www.tuicool.com/articles/NJ3UN3M)
> 概要: 编者按：本文来自微信公众号“Alter聊科技”（ID:spnews），作者：Alter，36氪经授权发布。整整五天的五一假期，一半时间贡献给了周边的家居市场，一半时间浪费在了去家居市场的路上。在个性化......
### [你累死累活做业务，绩效还不怎么样，我只能帮你到这了……](https://www.tuicool.com/articles/QJfeaqM)
> 概要: 作者|犽羽出品|阿里巴巴新零售淘系技术部前言作为一个业务前端，完成业务需求的同时，还要处理各种线上问题，加班辛苦忙碌了一年，还要被老板说“思考是不够的”、“没有业务 sence”，出去面试，被问项目，......
### [每千人拥有近1.5家，东莞奶茶店密度为何全国最大？](https://www.tuicool.com/articles/Ef6vqu7)
> 概要: 题图来自视觉中国，本文来自微信公众号：界面新闻（ID：wowjiemian），记者：吴容复工之后，哪个城市的人最先回到了奶茶店？来自维智科技对一线、新一线城市的统计，和去年12月对比，3月16日-3月......
### [微软谷歌三星等巨头成立的 5G Open RAN 政策联盟到底是什么](https://www.ithome.com/0/486/027.htm)
> 概要: 据外媒报道，近日，由移动生态系统多家企业成立的Open RAN政策联盟（Open RAN Policy Coalition）正式宣布启动，其目的在于倡导有助于推动Open RAN技术发展的政府政策。前......
### [全球首家：石头手持无线吸尘器H6通过德国莱茵TÜV吸力无损耗认证](https://www.ithome.com/0/486/049.htm)
> 概要: IT之家5月7日消息 据石头科技官微消息，石头手持无线吸尘器H6通过德国莱茵TÜV吸力无损耗认证，也是全球首个获此认证的吸尘器。石头科技称，基于TÜV莱茵开发的吸力标准2PfGQ 2807/04.20......
### [领克05正式上市：搭载全新领克05智能网联系统，17.58万元起](https://www.ithome.com/0/486/032.htm)
> 概要: IT之家5月7日消息 领克汽车旗下领克05于已正式上市，共推出五款配置车型，售价区间17.58——21.28万元。为感恩用户对领克05的支持与信赖，领克汽车特为前10000名领克05车主提供以下领享权......
### [外媒：阿里巴巴平头哥半导体有望成台积电主要客户](https://www.ithome.com/0/486/059.htm)
> 概要: 5月7日消息，据国外媒体报道，产业链方面的人士表示，阿里巴巴全资的平头哥半导体有限公司，有望成为台积电主要的客户。平头哥半导体有限公司是阿里巴巴全资的半导体芯片业务主体，成立于2018年9月，主要针对......
# 小说
### [三生三世枕上书](http://book.zongheng.com/book/923139.html)
> 作者：唐七

> 标签：奇幻玄幻

> 简介：有一句话是情深缘浅，情深是她，缘浅是她和东华。有一个词是福薄，她福薄，所以遇到他，他福薄，所以错过她。“你说我只是个宠物，我走的时候，你也没有挽留我。”“为什么我尤其需要你的时候，你都恰好不在呢？我希望遇到一个我有危险就会来救我的人，救了我不会把我随手抛下的人，我痛的时候会安慰我的人。”我用了很多时间去赌那个缘分，结果没有赌来。或许终有一日，我与他能在天庭相见，我是青丘的凤九，而在他眼中，我也不过是个初见的小帝姬，我同他的前缘，不过就是我曾经那样喜欢过他，而他从不知道罢了。”

> 章节末：尾声 父子初见

> 状态：完本
### [我的皇后（全）](http://book.zongheng.com/book/791687.html)
> 作者：谢楼南

> 标签：评论文集

> 简介：她是当朝权臣之女，入宫封后，尊荣无二。她睥睨后宫，欺才人、压贵妃、与太后抗衡。皇帝对她百般包容、真心相护，她却对他虚与委蛇，不假辞色。只因他们曾相识相爱，他却错杀她的至亲，这一次，她为复仇而来。风云突起，强敌犯边，京城、皇宫暗流涌动。阴差阳错间，她陷落敌营，他孤身前往解救。返身时，宫廷已然政变，他背弃天下，只为守她无恙。他藏身江湖，她随他出生入死、杀伐决断；战火再起，她随他仗剑沙场、相依相伴。关于复仇与爱情，家国与天下，最深情的守护，最绝情的决裂，比不过执子之手，与子偕老。

> 章节末：第五十一章 海棠

> 状态：完本
# 游戏
### [《泰拉瑞亚》1.4版本演示：旅途模式带来全新体验！](https://www.3dmgame.com/news/202005/3787877.html)
> 概要: 今日（5月7日），《泰拉瑞亚》公开了1.4版本大型更新“旅途的终点”的一些新情报，此次游戏中的“旅途模式”将为玩家们带来更多乐趣和全新体验。目前，官方还公开了一段12分钟的《泰拉瑞亚》1.4版本演示视......
### [新番《无能的奈奈》制作人员公开 妖尾导演加盟](https://www.3dmgame.com/news/202005/3787907.html)
> 概要: 正在史艾旗下漫画志《月刊少年GANGAN》火热连载的《无能的奈奈》之前宣布制作TV动画引发粉丝期待，5月7日今天官方公布了重要制作人员，曾执导《妖精的尾巴》的导演等加盟，一起来了解下。·《无能的奈奈》......
### [《惩戒魅魔》Steam抢先体验版发售 性感女主战力高](https://www.3dmgame.com/news/202005/3787879.html)
> 概要: 魅魔地牢游戏《惩戒：魅魔》Steam抢先体验版已于4月30日发售。游戏售价54元，现在打折仅售32元，优惠活动截止至5月7日。游戏支持中文，感兴趣的玩家抓紧时间入正吧！Steam商店地址：点击进入游戏......
### [《死亡终局轮回试炼2》新截图公开：主角团登场！](https://www.3dmgame.com/news/202005/3787928.html)
> 概要: 近日，知名绅士游戏厂商Compile Heart地雷社公开了《死亡终局：轮回试炼2》的一些新截图，主要展示了游戏中的部分场景，以下为具体细节。新截图欣赏：据悉，《死亡终局：轮回试炼2》故事的背景发生了......
### [《刺客信条：英灵殿》并非AC系列最宏大的作品](https://www.3dmgame.com/news/202005/3787925.html)
> 概要: 育碧已经正式公布了AC系列的最新作品《刺客信条：英灵殿》，之前育碧育碧也确认了《刺客信条：英灵殿》中将包含“巨石阵”，而且会是大家从未体验过的全新剧本。那么看来《刺客信条：英灵殿》应该是一部宏大的作品......
# 论文
### [Quantization Networks](https://paperswithcode.com/paper/quantization-networks-1)
> 日期：CVPR 2019

> 标签：IMAGE CLASSIFICATION

> 代码：https://github.com/aliyun/alibabacloud-quantization-networks

> 描述：Although deep neural networks are highly effective, their high computational and memory costs severely challenge their applications on portable devices. As a consequence, low-bit quantization, which converts a full-precision neural network into a low-bitwidth integer version, has been an active and promising research topic.
### [ALET (Automated Labeling of Equipment and Tools): A Dataset, a Baseline and a Usecase for Tool Detection in the Wild](https://paperswithcode.com/paper/alet-automated-labeling-of-equipment-and)
> 日期：25 Oct 2019

> 标签：OBJECT DETECTION

> 代码：https://github.com/metu-kovan/METU-ALET

> 描述：Robots collaborating with humans in realistic environments will need to be able to detect the tools that can be used and manipulated. However, there is no available dataset or study that addresses this challenge in real settings.
### [Deep Graph Translation](https://paperswithcode.com/paper/deep-graph-translation-1)
> 日期：ICLR 2020

> 标签：GRAPH GENERATION

> 代码：https://github.com/anonymous1025/Deep-Graph-Translation-

> 描述：Deep graph generation models have achieved great successes recently, among which, however, are typically unconditioned generative models that have no control over the target graphs are given an input graph. In this paper, we propose a novel Graph-Translation-Generative-Adversarial-Networks (GT-GAN) that transforms the input graphs into their target output graphs.
