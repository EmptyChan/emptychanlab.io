---
title: 2023-03-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.NuzzleManatee_ZH-CN3263788190_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-29 18:52:30
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.NuzzleManatee_ZH-CN3263788190_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [你的 SaaS，给个价吧！](https://www.woshipm.com/marketing/5792108.html)
> 概要: 在SaaS产品准备面向市场时，如何定价这个问题，就被摆上了台面，某种程度上，SaaS的定价也可以等于SaaS的定位，因为它圈选了不同的客户群体，也潜在地影响着SaaS产品的市场竞争力。那么SaaS定价......
### [藏在支付宝里的黑灰产生意](https://www.woshipm.com/it/5793184.html)
> 概要: 作为一个在线支付APP，支付宝除了的芝麻信用、会员积分等业务往往受到用户的关注。这些业务衍生出许多关于支付宝的灰色产业——花钱买积分、会员交易……它们会不会威胁到普通用户的正常权益？一起来看看这篇文章......
### [数据中台：指标管理中台演进路径及价值](https://www.woshipm.com/pd/5793203.html)
> 概要: 指标管理平台是可以帮助企业进行指标规范化管理的有效工具。针对不同企业的需求，怎样覆盖产品需要适应的业务场景，完善系统功能呢？本文介绍了指标管理平台的演进路径，一起起来看看这篇文章，希望能为你提供帮助......
### [“币圈巨骗”权道亨在黑山被捕，韩国、美国、新加坡展开引渡之争](https://finance.sina.com.cn/jjxw/2023-03-29/doc-imynnsmq8331600.shtml)
> 概要: 【环球时报驻韩国特约记者 张静】虚拟货币Terra和Luna崩盘致全球投资者损失惨重，该案核心人物、Terraform Labs创始人权道亨（32岁）在黑山被捕，韩国、美国、新加坡对其展开引渡之争，在......
### [阿里脱胎重生](https://www.huxiu.com/article/965195.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国一年一度的阿里变阵大戏，又到拉开帷幕的时刻，只是这一次变阵力度空前。3月28日阿里巴巴集团（下称“阿里”）董事会主席兼首席执行官逍遥子（张勇）发布全员信《......
### [阿里变革后开启上市“竞速赛”：菜鸟、阿里云，钉钉、盒马，谁先冲线？](https://finance.sina.com.cn/tech/internet/2023-03-29/doc-imynnsms5129473.shtml)
> 概要: 文|新浪科技 原祎鸣......
### [美光科技公布史上最大季度亏损，称内存市场已触底](https://finance.sina.com.cn/stock/usstock/c/2023-03-29/doc-imynnsmm2876502.shtml)
> 概要: 美光科技公布史上最大季度亏损，称内存市场已触底
### [已拿下三类证和数千万元融资，「医智影」聚焦肿瘤精准治疗赛道](https://36kr.com/p/2190858276258176)
> 概要: 已拿下三类证和数千万元融资，「医智影」聚焦肿瘤精准治疗赛道-36氪
### [Oculus Rift DK1发货十周年这天，我们想约VR行业的朋友们来聊聊自己的这十年 | 36氪活动](https://36kr.com/p/2190502177849478)
> 概要: Oculus Rift DK1发货十周年这天，我们想约VR行业的朋友们来聊聊自己的这十年 | 36氪活动-36氪
### [朝云集团战略收购广东中科研，孵化多品类爆品抢市场先机](http://www.investorscn.com/2023/03/29/106521/)
> 概要: 来源：独角兽早知道......
### [追觅科技发布会宣言：未来每卖10台强清洁力洗地机，7台是追觅](http://www.investorscn.com/2023/03/29/106522/)
> 概要: 3月28日，追觅科技在上海举办了以“更强清洁力，洗地新标准”为主题的洗地机产品发布会......
### [ChatGPT出来后，办公室摸鱼这件事要凉凉](https://www.huxiu.com/article/968592.html)
> 概要: 本文来自微信公众号：一口老炮（ID：yikoulaopao），作者：炮哥被人注册了，题图来自：视觉中国实话实说，我认识很多职场摸鱼大师，他们一般喜欢周五中午来找我吃饭。因为小酌两杯，下午就可以直接回家......
### [风和日丽的相遇](https://www.zcool.com.cn/work/ZNjQ2NjE0Njg=.html)
> 概要: 第一次画小漫画————之前看的《蒙古草原，天气晴》有感，画的纪录片里的冒险家和小女孩普洁的故事～......
### [《街头霸王6》新开发者实机演示：卡密vs曼侬](https://www.3dmgame.com/news/202303/3865880.html)
> 概要: 今日（3月29日），卡普空发布了《街头霸王6》的新开发者实机演示视频，嘉米·怀特对阵曼侬。《街头霸王6》将于2023年6月2日发售，登陆PS5、PS4、Xbox Swries X|S和PC。宣传片：视......
### [《女神异闻录：夜幕魅影》首测开启  手游体验超前曝光](https://shouyou.3dmgame.com/news/66134.html)
> 概要: 天鹅绒房间的旋律响起，异世界的大门再次打开，殿堂级JRPG-《女神异闻录5》(以下简称为P5)IP系列首款手游正式来袭！《女神异闻录：夜幕魅影》(以下简称为P5X)官方手游已于今日10点正式开启「潜入......
### [《魔发精灵3》首支预告 11月17日北美上映](https://www.3dmgame.com/news/202303/3865889.html)
> 概要: 今日（3月29日），梦工场动画官方微博公布《魔发精灵3》发布海报以及预告，魔发村再度回归，拯救你的所有不开心！11月17日北美上映。预告片：配音阵容：贾斯汀·汀布莱克、安娜·肯德里克回归，卡米拉·卡贝......
### [《暗黑4》玩家发现攻速“漏洞”：DPS轻松提升30%](https://www.3dmgame.com/news/202303/3865894.html)
> 概要: 最近一段时间的抢先测试和公开测试后，《暗黑破坏神4》玩家已经发现了游戏中存在的许多“漏洞”。其中就包括一种可以让玩家的总输出伤害轻松提升的方法。最近体验游戏的一位玩家 RaizQT 发现，通过这个方法......
### [AIGC杀入社交！生产力解放后，人类情感至上——GGV投资笔记第144期](https://36kr.com/p/2191948800770946)
> 概要: AIGC杀入社交！生产力解放后，人类情感至上——GGV投资笔记第144期-36氪
### [硬科技捕手湖畔宏盛的“三角进攻”](http://www.investorscn.com/2023/03/29/106525/)
> 概要: “智慧农业第一股”要来了......
### [嘉里物流联网2022年财报出炉：收入和核心纯利维持增长](http://www.investorscn.com/2023/03/29/106527/)
> 概要: 3月28日，嘉里物流联网有限公司（“嘉里物流联网”，“公司”或与其子公司统称“集团”或“嘉里物流联网集团”；股份代号0636.HK）公布其2022年全年业绩......
### [一口春天溢出来：五芳斋青团 8.9 元 4 枚再发车（4 月中旬到期）](https://lapin.ithome.com/html/digi/683091.htm)
> 概要: 天猫【五芳斋官方旗舰店】五芳斋豆沙青团 50g*4 枚日常售价为 19.9 元，下单折后价 15.92 元，领取 7 元优惠券，到手价为 8.92 元：天猫五芳斋 豆沙青团50g*4 枚券后 8.92......
### [药明康德回应药明生基临港工厂关闭，国内基因与细胞治疗CDMO内卷 | 最前线](https://36kr.com/p/2192217654493575)
> 概要: 药明康德回应药明生基临港工厂关闭，国内基因与细胞治疗CDMO内卷 | 最前线-36氪
### [一加 Nord CE 3 Lite 手机将搭载 5000mAh 电池，支持 67W 快充](https://www.ithome.com/0/683/101.htm)
> 概要: IT之家3 月 29 日消息，一加宣布将于 4 月 4 日发布一加 Nord CE 3 Lite 手机，官方宣布将配备 5000 mAh 电池和 67W SuperVOOC 充电，带来更长续航，更快充......
### [《我的世界：龙与地下城》联动主线长达10小时](https://www.3dmgame.com/news/202303/3865931.html)
> 概要: 昨天（3 月 28 日），开发商 Mojang 宣布了《我的世界》与《龙与地下城》的联动内容。这次联动并非独立游戏，而是《我的世界》游戏本体的新增内容。目前Mojang 分享了有关该内容的更多细节。开......
### [ZDNS完成C轮融资：发展下一代DNS，筑牢数字经济重要网络根基](http://www.investorscn.com/2023/03/29/106534/)
> 概要: 3月28日,互联网域名系统国家地方联合工程研究中心(英文简称:ZDNS)在成立十周年之际,宣布完成亿元C轮融资,由中科院资本、新鼎资本领投。本轮资金将主要用于推动下一代DNS(域名系统)技术研发和行业......
### [为啥有那么多“二极管人士”？](https://www.huxiu.com/article/970444.html)
> 概要: 本文来自微信公众号：那些原本是废话的常识（ID：feihuayuchangshi），作者：叶克飞，编辑：二蛋，原文标题：《为啥有那么多二极管？一靠悠久传统，二靠不教逻辑》，题图来源：视觉中国大概十年前......
### [微星 PCIe 5.0 SSD M570 HS 开始上市，速度可达 10000MB/s](https://www.ithome.com/0/683/108.htm)
> 概要: IT之家3 月 29 日消息，微星日前发布的 SPATIUM M570 HS PCIe 5.0 SSD 现已在淘宝第三方店铺发售，速度可达 10000MB/s，2TB 售价 2683 元。据介绍，微星......
### [视频：秦岚承认与魏大勋恋情？被问感情时称就是大家看见的状态](https://video.sina.com.cn/p/ent/2023-03-29/detail-imynpprc7025554.d.html)
> 概要: 视频：秦岚承认与魏大勋恋情？被问感情时称就是大家看见的状态
### [视频：刘些宁工作室否认与何与恋情 发文还原事情经过](https://video.sina.com.cn/p/ent/2023-03-29/detail-imynpprh5067780.d.html)
> 概要: 视频：刘些宁工作室否认与何与恋情 发文还原事情经过
### [踏空行情主线、收益表现不佳，资金“围剿”百亿基金经理](https://www.yicai.com/news/101715673.html)
> 概要: 百亿基金经理今年在涨幅居前的个股、板块配置很少
### [降了又降，房贷利率还高吗？](https://www.huxiu.com/article/950979.html)
> 概要: 本文来自微信公众号：财经十一人 （ID：caijingEleven），作者：陈汐、刘建中，原文标题：《中国房贷利率降了又降，为什么还是高》，头图来自：视觉中国房子是大部分中国人一生中最大的一笔支出。因......
### [格温妮丝滑雪被撞 称以为撞人者是想性侵犯](https://ent.sina.com.cn/s/u/2023-03-29/doc-imynppre8316313.shtml)
> 概要: 新浪娱乐讯 据媒体报道，格温妮丝·帕特洛（Gwyneth Paltrow）在滑雪受伤审判中作证说，她最初以为从她身后走过来并与她相撞的男子是想对她进行性侵犯。　　格温妮丝在证人席上向陪审团讲述了她在鹿......
### [美国地区银行倒闭余波：楼市抢房大战再次重演](https://www.yicai.com/news/101715710.html)
> 概要: 安萨里称，现在美国房地产市场对美联储政策越来越敏感。
### [吉利共享出行定制车品牌“曹操汽车”首款车型曹操 60 正式公布，售价 11.98-13.98 万元](https://www.ithome.com/0/683/118.htm)
> 概要: IT之家3 月 29 日消息，吉利集团上周推出了旗下首个共享出行定制车品牌，并命名为 —— 曹操汽车。今日，曹操出行召开发布会，现场推出了该品牌首款车型 —— 曹操 60 以及 60Pro，分别为 1......
### [中国长城闪崩跌停，四机构合计卖出超4亿元](https://www.yicai.com/news/101715782.html)
> 概要: 龙虎榜数据显示，深股通买入中国长城7718万元并卖出4637万元，四机构合计卖出4.02亿元，长江证券上海分公司卖出2.26亿元。
### [机构今日买入这12股，抛售中国长城超4亿元](https://www.yicai.com/news/101715810.html)
> 概要: 当天机构净买入前三的股票分别是安徽合力、创业黑马、三变科技，净买入金额分别是1.07亿元、5038万元、3896万元。
### [瑞银宣布新任CEO 负责指导瑞信并购案](https://www.yicai.com/image/101715887.html)
> 概要: 瑞银宣布新任CEO 负责指导瑞信并购案
### [博鳌直击丨中国国际经济交流中心副理事长朱民：中国的经济非常稳定，大家都看好中国市场](https://finance.sina.com.cn/china/2023-03-29/doc-imynptwz6907453.shtml)
> 概要: 博鳌亚洲论坛2023年会于3月28-31日在海南博鳌召开，今年主题为“不确定的世界：团结合作迎挑战，开放包容促发展”。中国国际经济交流中心副理事长...
### [博鳌直击丨中国国际经济交流中心副理事长朱民：硅谷银行危机是美国金融系统错位的结果](https://finance.sina.com.cn/china/2023-03-29/doc-imynptxf9402637.shtml)
> 概要: 博鳌亚洲论坛2023年会于3月28-31日在海南博鳌召开，今年主题为“不确定的世界：团结合作迎挑战，开放包容促发展”。中国国际经济交流中心副理事长...
### [白春礼：促进科技进步的关键是维护开放合作的创新生态](https://finance.sina.com.cn/china/2023-03-29/doc-imynptwz6909385.shtml)
> 概要: 来源：博鳌亚洲论坛 3月29日上午，博鳌亚洲论坛2023年年会举行“科技竞争与合作”分论坛，中国科学院院士、中国科学院前院长白春礼指出...
### [曾培炎：构建和平安全、繁荣开放、合作共赢的亚洲](https://finance.sina.com.cn/china/2023-03-29/doc-imynptxf9403656.shtml)
> 概要: 来源：博鳌亚洲论坛 团结合作迎挑战，开放包容促发展。3月29日， 博鳌亚洲论坛咨询委员会副主席、中国国务院原副总理曾培炎先生在博鳌亚洲论坛2023年年会“亚洲区域合作：新...
### [福田康夫：团结合作对亚洲区域发展至关重要](https://finance.sina.com.cn/china/2023-03-29/doc-imynptxf9403695.shtml)
> 概要: 来源：博鳌亚洲论坛 团结合作迎挑战，开放包容促发展。3月29日，博鳌亚洲论坛咨询委员会主席、日本前首相福田康夫在博鳌亚洲论坛2023年年会“亚洲区域合作：新机遇...
# 小说
### [蒸汽大明：别再叫我监国了](https://m.qidian.com/book/1033838684/catalog)
> 作者：周星河不会开车

> 标签：两宋元明

> 简介：新书《蒸汽大汉：家兄霍去病》汽车维修工穿越成为大明仁宗第五子朱瞻墡，三次监国三过皇位而不坐。瓦剌也先看着浩浩荡荡的大明军队哀嚎：“为何明军有坦克。”哥伦布望着停在北美洲的驱逐舰陷入了沉思。百年战争中的英法看到手持步枪的明军一声不吭。某社畜汽车修理工哀嚎：“别再叫我监国了，我只想做个快活王爷”回到鼎盛大明发动工业革命，在蒸汽升腾中大明扬帆远航。

> 章节总数：共365章

> 状态：完本
# 论文
### [Magnitude-Corrected and Time-Aligned Interpolation of Head-Related Transfer Functions | Papers With Code](https://paperswithcode.com/paper/magnitude-corrected-and-time-aligned)
> 日期：17 Mar 2023

> 标签：None

> 代码：https://github.com/audiogroupcologne/supdeq

> 描述：Head-related transfer functions (HRTFs) are essential for virtual acoustic realities, as they contain all cues for localizing sound sources in three-dimensional space. Acoustic measurements are one way to obtain high-quality HRTFs. To reduce measurement time, cost, and complexity of measurement systems, a promising approach is to capture only a few HRTFs on a sparse sampling grid and then upsample them to a dense HRTF set by interpolation. However, HRTF interpolation is challenging because small changes in source position can result in significant changes in the HRTF phase and magnitude response. Previous studies greatly improved the interpolation by time-aligning the HRTFs in preprocessing, but magnitude interpolation errors, especially in contralateral regions, remain a problem. Building upon the time-alignment approaches, we propose an additional post-interpolation magnitude correction derived from a frequency-smoothed HRTF representation. Employing all 96 individual simulated HRTF sets of the HUTUBS database, we show that the magnitude correction significantly reduces interpolation errors compared to state-of-the-art interpolation methods applying only time alignment. Our analysis shows that when upsampling very sparse HRTF sets, the subject-averaged magnitude error in the critical higher frequency range is up to 1.5 dB lower when averaged over all directions and even up to 4 dB lower in the contralateral region. As a result, the interaural level differences in the upsampled HRTFs are considerably improved. The proposed algorithm thus has the potential to further reduce the minimum number of HRTFs required for perceptually transparent interpolation.
### [Open Problems in Applied Deep Learning | Papers With Code](https://paperswithcode.com/paper/open-problems-in-applied-deep-learning)
> 日期：26 Jan 2023

> 标签：None

> 代码：https://github.com/maziarraissi/Applied-Deep-Learning

> 描述：This work formulates the machine learning mechanism as a bi-level optimization problem. The inner level optimization loop entails minimizing a properly chosen loss function evaluated on the training data. This is nothing but the well-studied training process in pursuit of optimal model parameters. The outer level optimization loop is less well-studied and involves maximizing a properly chosen performance metric evaluated on the validation data. This is what we call the "iteration process", pursuing optimal model hyper-parameters. Among many other degrees of freedom, this process entails model engineering (e.g., neural network architecture design) and management, experiment tracking, dataset versioning and augmentation. The iteration process could be automated via Automatic Machine Learning (AutoML) or left to the intuitions of machine learning students, engineers, and researchers. Regardless of the route we take, there is a need to reduce the computational cost of the iteration step and as a direct consequence reduce the carbon footprint of developing artificial intelligence algorithms. Despite the clean and unified mathematical formulation of the iteration step as a bi-level optimization problem, its solutions are case specific and complex. This work will consider such cases while increasing the level of complexity from supervised learning to semi-supervised, self-supervised, unsupervised, few-shot, federated, reinforcement, and physics-informed learning. As a consequence of this exercise, this proposal surfaces a plethora of open problems in the field, many of which can be addressed in parallel.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
