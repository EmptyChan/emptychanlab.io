---
title: 2023-04-02-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.JavaBromo_ZH-CN2744043733_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-02 20:38:42
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.JavaBromo_ZH-CN2744043733_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [内容营销全面解析：想让内容营销事半功倍？绝不能错过这三招！​](https://www.woshipm.com/marketing/5795734.html)
> 概要: 在这个信息爆炸的时代，想要抓住用户的注意力，让他们对你的品牌产生兴趣和信任，就要做好内容营销。那么，如何制定有效的内容营销呢？本文作者对此进行了分析，一起来看一下吧。一、内容营销策略有哪些在当下这个信......
### [不想上班的年轻人，都去摆摊了？](https://www.woshipm.com/it/5794986.html)
> 概要: 不想上班的年轻人们，最近终于找到了新的赚钱方式——摆摊。年轻人摆摊，都在卖些什么呢？能赚回本吗？本文作者分享了两个摆摊的年轻人的故事，一起来看看吧。每周有五天都嚷着不想上班的年轻人们，终于在上班和创业......
### [力推美团企业版，美团究竟意欲何为？](https://www.woshipm.com/it/5795699.html)
> 概要: 据消息称，美团将于近期正式上线面向To B市场的业务“美团企业版”，定位企业消费赛道。美团为什么要在这时加速迈向B端市场呢？本文作者对此进行了分析，一起来看一下吧。已经拥有930万活跃商家的美团公司，......
### [不能让“社区万能章”盖住了社会治理漏洞](https://finance.sina.com.cn/jjxw/2023-04-02/doc-imynwzxs7047894.shtml)
> 概要: 李英锋 到村委会、居委会开证明，是很多人在日常生活中都会遇到的事情，这给人的印象也似乎是“万事皆可证明”。然而，有人“钻空子”开具虚假证明，骗过司法机关...
### [ChatGPT向癌症开了一枪](https://www.huxiu.com/article/1001353.html)
> 概要: 出品 | 虎嗅医疗组作者 | 陈广晶编辑 | 陈伊凡头图 |《绝命毒师》剧照经典靶点的奠基人到中国创业，会带来哪些改变？前不久，一家名为赛得康的生物技术公司获得种子轮投资的消息，在医药行业媒体来了一轮......
### [aespa将于今年8月在日本东京巨蛋举行演唱会](https://ent.sina.com.cn/s/j/2023-04-02/doc-imynxxcm4820302.shtml)
> 概要: 新浪娱乐讯 2日，韩国4人女子组合aespa1日表示，将在东京代代木第一体育馆举行首场日本巡演东京公演，并于8月5、6日举行首场东京巨蛋公演。　　虽然是在日本出道之前，但自2020年11月在本土国出道......
### [《五等分的新娘∽》新动画确定制作 系列外传作品](https://www.3dmgame.com/news/202304/3866184.html)
> 概要: 人气恋爱喜剧番《五等分的新娘》全新动画《五等分的新娘∽》官方宣布确定制作，作为系列外传作品推出，开播日期未定，敬请期待。·《五等分的新娘》改编自春场葱创作的同名恋爱喜剧漫画，故事讲述了一直过着贫困生活......
### [骗子利用AI声音冒充亲人诈骗：老人成为主要受害者](https://www.3dmgame.com/news/202304/3866191.html)
> 概要: 央视网报道，美国和加拿大各地使用AI合成语音进行电信诈骗的案例多发，有不少上当的都是老年人。人工智能语音一直受到业界关注，如今该技术已经发展到需要几秒钟的对话就足以准确模仿某人声音的程度。加拿大警方称......
### [光荣有意让旗下更多游戏加入XGP等会员订阅服务](https://www.3dmgame.com/news/202304/3866192.html)
> 概要: 光荣特库摩近日表示，有意让旗下更多游戏加入Xbox Game Pass和PS Plus Extra业务。在针对其热门新作《卧龙：苍天陨落》的调查中，光荣特库摩询问受访者希望在此类高级游戏订阅服务中看到......
### [日本加强尖端芯片出口管制，中日高科技经贸合作怎么走？](https://www.yicai.com/news/101719308.html)
> 概要: 中日高科技领域的贸易合作或许会出现逐渐下滑的趋势。
### [罗晋为粉丝庆13周年生日 粉丝：继续陪帅哥走花路](https://ent.sina.com.cn/s/m/2023-04-02/doc-imynycme6772726.shtml)
> 概要: 新浪娱乐讯 4月2日，罗晋发微博为粉丝庆十三周年生日：“13年，感谢一起走过，生日快乐” 。粉丝热情回应：“一起走下个13年”，“......
### [中国公司全球化周报｜OPPO缩减英国、德国业务；中国与巴西达成协议，使用本币而非美元开展大规模贸易](https://36kr.com/p/2196388584490887)
> 概要: 中国公司全球化周报｜OPPO缩减英国、德国业务；中国与巴西达成协议，使用本币而非美元开展大规模贸易-36氪
### [上下五千年，熊猫第一次有了站姐](https://www.huxiu.com/article/1000748.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。熊猫很可爱，但没有人能......
### [这届年轻人，干啥都要找“搭子”](https://www.huxiu.com/article/1020759.html)
> 概要: 作者：马志刚、陶淘、吕敬之、张琳、马舒叶，编辑：曹杨，头图来自：视觉中国近日，#搭子成为一种新型社交关系#的话题，登上微博热搜。“饭搭子”“旅游搭子”“演唱会搭子”“游戏搭子”“运动搭子”“摄影搭子”......
### [上交所服务周：科技赋能，会员投行热议内控信息化建设](https://www.yicai.com/news/101719340.html)
> 概要: 数字化智能化升级提升投行业务管理效能
### [汪小菲飞台湾看儿女 被S妈劝与大S停吵回应：再说](https://ent.sina.com.cn/s/m/2023-04-02/doc-imynypza3395749.shtml)
> 概要: 新浪娱乐讯 4月2日，消息称汪小菲昨天飞台北，昨天S妈带着他的一儿一女到S Hotel和他碰面，大S没去，汪小菲和小孩相处一个下午。子女和他互动很愉快，他和S妈也聊的很开心，S妈劝汪小菲：“你和熙媛两......
### [200 年后，人们终于知道了贝多芬的神秘死因（和家族八卦）](https://www.ithome.com/0/683/930.htm)
> 概要: 撰文 | Skin审校 | Ziv“乐圣”贝多芬去世的时候，是 1827 年 3 月 26 日，距今天已经过去了 195 年。据说当时下午 5 点，“天空中忽然出现了一道闪电和一声雷鸣，贝多芬庄严地举......
### [《拳皇13：全球大赛》公布 登陆PS4和NS平台](https://www.3dmgame.com/news/202304/3866203.html)
> 概要: 在EVO Japan期间，SNK Playmore宣布将于PS4和NS上发布《拳皇13：全球大赛》（King of Fighters XIII Global Match），并将于2023年夏季开放公测......
### [美国全力救助硅谷银行的真正目的是什么？](https://www.yicai.com/news/101719359.html)
> 概要: 以超常规手段救助硅谷银行，是否仅仅为了稳定银行业和金融市场？
### [六大行中仅一家净息差上升，稳息差银行如何发力？](https://www.yicai.com/news/101719360.html)
> 概要: 下行压力依然存在。
### [北汽蓝谷：3 月汽车销量 10302 辆，1~3 月累计 15114 辆同比增长 65.72%](https://www.ithome.com/0/683/944.htm)
> 概要: IT之家4 月 2 日消息，北汽蓝谷今日发布公告，子公司北京新能源汽车股份有限公司 2023 年 3 月份销量 10302 辆；1 至 3 月累计销量 15114 辆，同比增长 65.72%。北汽新能......
### [海底捞的第二曲线不是外卖丨焦点分析](https://36kr.com/p/2193574454822786)
> 概要: 海底捞的第二曲线不是外卖丨焦点分析-36氪
### [《方舟：生存进化》手游将重制 动画版进入后期制作](https://www.3dmgame.com/news/202304/3866209.html)
> 概要: 昨天Studio Wildcard公布了《方舟2》跳票到2024年的消息。随后厂商还公布了《方舟：生存进化》手游和动画版的消息。厂商介绍，《方舟：生存进化》手游将于年底重新启动，并成为一款完全的“重制......
### [桌宠游戏《Capoo Pals》上线 Steam，支持中文](https://www.ithome.com/0/683/959.htm)
> 概要: IT之家4 月 2 日消息，桌宠游戏《 Capoo Pals 螢幕貓蟲咖波》上线 Steam，售价 42 元，支持英文与繁体中文，感兴趣的玩家可以点此购买。IT之家了解到，猫猫虫咖波是由台湾漫画家亚拉......
### [雨天不知道拍什么，学会这 2 招，超易出片](https://www.ithome.com/0/683/963.htm)
> 概要: 春暖花开，在这期间我们也分享了许多在公园拍花🌸小技巧~感兴趣的朋友可以点击以下标题进行详细学习！《春日拍花如何规避人海？实用攻略来了！》《公园的花太矮不会拍？教你 2 个创意技巧！》《逛公园看见不同的......
### [4月2日这些公告有看头](https://www.yicai.com/news/101719480.html)
> 概要: 4月2日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
# 小说
### [浊世任逍遥](https://book.zongheng.com/book/1018739.html)
> 作者：沧海无量

> 标签：奇幻玄幻

> 简介：天地初开，有九窍玄石吸收混沌本源，人族善念，化为九天玄女孕育无上道法，扶助人族自强自立，屹立于苍茫浊世。

> 章节末：完美新世界《大结局》

> 状态：完本
# 论文
### [Fixing Model Bugs with Natural Language Patches | Papers With Code](https://paperswithcode.com/paper/fixing-model-bugs-with-natural-language)
> 日期：7 Nov 2022

> 标签：None

> 代码：https://github.com/murtyshikhar/languagepatching

> 描述：Current approaches for fixing systematic problems in NLP models (e.g. regex patches, finetuning on more data) are either brittle, or labor-intensive and liable to shortcuts. In contrast, humans often provide corrections to each other through natural language. Taking inspiration from this, we explore natural language patches -- declarative statements that allow developers to provide corrective feedback at the right level of abstraction, either overriding the model (``if a review gives 2 stars, the sentiment is negative'') or providing additional information the model may lack (``if something is described as the bomb, then it is good''). We model the task of determining if a patch applies separately from the task of integrating patch information, and show that with a small amount of synthetic data, we can teach models to effectively use real patches on real data -- 1 to 7 patches improve accuracy by ~1-4 accuracy points on different slices of a sentiment analysis dataset, and F1 by 7 points on a relation extraction dataset. Finally, we show that finetuning on as many as 100 labeled examples may be needed to match the performance of a small set of language patches.
### [DeepOrder: Deep Learning for Test Case Prioritization in Continuous Integration Testing | Papers With Code](https://paperswithcode.com/paper/deeporder-deep-learning-for-test-case)
> 概要: Continuous integration testing is an important step in the modern software engineering life cycle. Test prioritization is a method that can improve the efficiency of continuous integration testing by selecting test cases that can detect faults in the early stage of each cycle.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
