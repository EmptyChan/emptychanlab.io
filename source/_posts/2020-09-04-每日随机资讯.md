---
title: 2020-09-04-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PicoIsland_EN-CN0349311381_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-09-04 22:16:24
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PicoIsland_EN-CN0349311381_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：陈赫发文疑回应助阵《姐姐》决赛夜质疑“笑吧”](https://video.sina.com.cn/p/ent/2020-09-04/detail-iivhvpwy4840360.d.html)
> 概要: 视频：陈赫发文疑回应助阵《姐姐》决赛夜质疑“笑吧”
### [林心如送3岁女儿上学掉泪 谈离婚传言称一笑置之](https://ent.sina.com.cn/s/h/2020-09-04/doc-iivhuipp2507005.shtml)
> 概要: 新浪娱乐讯 9月3日，据台湾媒体报道，林心如送3岁多爱女小海豚上幼儿园，原本以为爸爸霍建华会哭，结果没想到落泪的是她，她不好意思表示：“她进去前突然紧抱我一下，我就掉了2滴泪。”她也叮咛女儿被人欺负一......
### [郎朗谈胡宇桐质疑乐评人：每个人都有自己的想法](https://ent.sina.com.cn/2020-09-04/doc-iivhuipp2535831.shtml)
> 概要: 新浪娱乐讯 今日，郎朗在采访中被问到“如何看待选手胡宇桐质疑乐评人”这件事，郎朗表示：我觉得每个人都不一样，每个人都有自己的想法，我也不好说什么。我们跟每个学员交流都很顺畅，没有隔阂。我们私下就是谈音......
### [52岁伊能静拼二胎遭反对 秦昊曝原因反被指戏精](https://ent.sina.com.cn/s/h/2020-09-04/doc-iivhvpwy4813602.shtml)
> 概要: 新浪娱乐讯 9月4日，据台湾媒体报道，伊能静2016年和秦昊结婚后，两人育有一女小米粒，即便已经52岁了，仍有想拼第二胎的打算，不过老公秦昊在仔细思考一天后，决定反对伊能静，主要是生下第二胎之后“小米......
### [组图：不愿只当家庭主妇？港媒曝方媛奔波网红事业没时间带女儿](http://slide.ent.sina.com.cn/star/hr/slide_4_704_344592.html)
> 概要: 组图：不愿只当家庭主妇？港媒曝方媛奔波网红事业没时间带女儿
### [郑爽又接新综艺，放弃演员身份的她，这回能靠综艺翻身吗](https://new.qq.com/omn/20200904/20200904A0JOJF00.html)
> 概要: 提到郑爽，想必很多人的第一印象都会想到在《一起来看流星雨》中清纯可爱的楚雨荨一角吧！这部十年前的校园偶像剧，也成为了郑爽的出道之作，即使在今天依然被观众津津乐道。郑爽清纯的颜值为她加分不少，出道后所饰......
### [红了就“忘恩负义”了？被送上热搜的她太冤！](https://new.qq.com/omn/20200904/20200904A0L3DN00.html)
> 概要: 红了就“忘恩负义”了？被送上热搜的她太冤！
### [再看周星驰电影，才发现星爷所有角色名字深意！当初理解太表面了](https://new.qq.com/omn/20200904/20200904A0JMXH00.html)
> 概要: （相对电影社/原创发布：敬请关注）提起周星驰电影，相信没有人会不喜欢！实话说，本人就是从小看周星驰电影长大的，而且直到现在，依然还是会时常找来重温。            最近，由于再看周星驰电影，笔......
### [有种“整容”叫郑爽，保持了3年锥子脸，30岁时却发福似包子脸](https://new.qq.com/omn/20200904/20200904A0JNWD00.html)
> 概要: 在早期出道的时候，郑爽也算是一个包子脸的美女了，毕竟在拍摄《一起来看流星雨》的时候郑爽也只是刚刚在读大学而已，和张翰恋爱的时候郑爽也才刚刚满18岁。这也是为什么郑爽一路红了10年的原因，郑爽年少成名不......
### [恋情才曝光就怀孕？董璇穿紧身裙拍广告，小腹隆起明显还用手护住](https://new.qq.com/omn/20200904/20200904A0HPHV00.html)
> 概要: 近日，有媒体曝光了一组董璇在摄影棚拍广告的图片，其状态引发热议。            当天，董璇身穿一袭白色抹胸紧身连衣裙，长发披肩，尽显温柔优雅的气质。她摆着或是唯美或是性感的pose，动作十分娴......
# 动漫
### [日本棒球队福冈软银鹰宣布开始VTuber计划](https://news.dmzj.com/article/68472.html)
> 概要: 日本棒球队福冈软银鹰队宣布了开始VTuber计划的消息，从本日（9月4日）起至9月22日的海选活动也一并开始。
### [游戏王历史：从零开始的游戏王环境之旅第五期17](https://news.dmzj.com/article/68473.html)
> 概要: 2006年宣告结束，游戏王OCG迎来了第八个新年。06年是新世代开始的一年，途中也有一些强卡参战导致环境的战斗力明显膨胀，但总体来说它作为一款卡牌游戏还是收敛在了一个健全的范围，并不断地推移着Metagame环境。在这快速且稳定变迁的第五期中期环...
### [男性VTuber计划《拜托了支持者！》启动！10月播出动画](https://news.dmzj.com/article/68462.html)
> 概要: 男性VTuber计划《拜托了支持者！》正式启动了，在10月将在YouTube的官方频道播出动画。
### [青叶真司开始进行站立的复健！主治医师发表治疗报告](https://news.dmzj.com/article/68467.html)
> 概要: 京都动画纵火杀人案的犯罪嫌疑人青叶真司的主治医师，在3日举行的医疗学会上发表了历时4个月的对青叶真司的治疗方法的报告。
### [小姐姐，月里嫦娥，风姿卓越](https://new.qq.com/omn/20200903/20200903A0BHQ300.html)
> 概要: 小姐姐，月里嫦娥，风姿卓越
### [路飞饿久了就会衰老成老头，尾田：路飞仅饿3天就能变成木乃伊](https://new.qq.com/omn/20200904/20200904A0GH7A00.html)
> 概要: 路飞很能吃，我们常常看到他狼吞虎咽，而且食量是别人10倍的同时消化能力也极强。曾有人在SBS中问道："路飞是三天不吃饭就会饿死的男人吗？"尾田给了肯定回答，他说路飞经常要运动，所以肚子消化的速度非同……
### [COS赏析：枝垂萤](https://new.qq.com/omn/20200904/20200904A0BQJW00.html)
> 概要: 原作：粗点心战争Cn: Yuli暮暮
### [名侦探柯南：画师笔下的情侣，平次看起来很腼腆，基德很慌张](https://new.qq.com/omn/20200904/20200904A0L2NP00.html)
> 概要: 在画师czech_gana笔下，名侦探柯南中的情侣们看起来各有特色。下面，我们就一起来看看吧。首先，当然是主角工藤新一和毛利兰了。工藤新一在名侦探柯南中一直都以江户川柯南的身份“潜伏”在小兰的身边，……
### [《龙珠》中有5种融合方式，只知道舞步融合和耳环？这3种了解下](https://new.qq.com/omn/20200904/20200904A0ET7200.html)
> 概要: 巴特拉耳环是界王神才拥有的东西，老界王神把巴特拉可以合体的秘密告诉了悟空，于是悟空和贝吉塔的合体贝吉特就诞生了，这一招在龙珠超系列中卡莉芙拉和开尔也用过，合体成为开芙拉。舞步融合也不知道是谁交给悟空……
### [这些动漫现在看不懂，以后也未必看得，伊藤润二的格局究竟有多大](https://new.qq.com/omn/20200831/20200831A01PSE00.html)
> 概要: 伊藤润二的画风放在现在还是曾经都不是最好的，无论是在过去还是在现在，比他好的漫画家都大有人在，而为什么他能作为恐怖一哥呢？伊藤润二是一个把恐怖做到了极致的人，游戏里的恐怖是寂静岭，而动漫里的恐怖则是……
# 财经
### [习近平在2020年中国国际服贸会全球服务贸易峰会上致辞金句](https://finance.sina.com.cn/china/gncj/2020-09-04/doc-iivhuipp2562240.shtml)
> 概要: 国家主席习近平在2020年中国国际服务贸易交易会全球服务贸易峰会上的视频致辞金句 2020年中国国际服务贸易交易会全球服务贸易峰会9月4日晚在北京举行...
### [习近平在2020年中国国际服务贸易交易会全球服务贸易峰会上致辞](https://finance.sina.com.cn/china/gncj/2020-09-04/doc-iivhvpwy4937023.shtml)
> 概要: 尊敬的各国领导人，尊敬的各位国际组织负责人，尊敬的各代表团团长，各位来宾，女士们，先生们，朋友们： 大家好！值此2020年中国国际服务贸易交易会举办之际...
### [数字银行卡来袭 8家银行可网上申请的虚拟卡难办吗](https://finance.sina.com.cn/chanjing/cyxw/2020-09-04/doc-iivhvpwy4939171.shtml)
> 概要: 别out了！数字银行卡来袭，8家银行可网上申请的虚拟卡难办吗  “无卡时代”的变与不变。 在这个时不时就会吐槽“卡太多”的时代，数字银行卡终于来了。
### [习近平就服务业开放合作提出3点倡议：营造开放包容的合作环境](https://finance.sina.com.cn/china/2020-09-04/doc-iivhuipp2557647.shtml)
> 概要: 国家主席习近平在2020年中国国际服务贸易交易会全球服务贸易峰会上发表视频致辞时就服务业开放合作提出3点倡议：共同营造开放包容的合作环境；共同激活创新引领的合作动能...
### [习近平：中国将坚定不移扩大对外开放](https://finance.sina.com.cn/china/gncj/2020-09-04/doc-iivhuipp2558134.shtml)
> 概要: 原标题：习近平：中国将坚定不移扩大对外开放 国家主席习近平4日在2020年中国国际服务贸易交易会全球服务贸易峰会上致辞。 国家主席习近平说...
### [习近平在2020年中国国际服务贸易交易会全球服务贸易峰会上致辞](https://finance.sina.com.cn/china/2020-09-04/doc-iivhvpwy4939242.shtml)
> 概要: 习近平在2020年中国国际服务贸易交易会全球服务贸易峰会上致辞 国家主席习近平4日在2020年中国国际服务贸易交易会全球服务贸易峰会上致辞。
# 科技
### [Facebook prohibits music or music listening experience on Live](https://www.facebook.com/legal/music_guidelines)
> 概要: Facebook prohibits music or music listening experience on Live
### [Learning@home hivemind – train large neural networks across the internet](https://learning-at-home.github.io/)
> 概要: Learning@home hivemind – train large neural networks across the internet
### [广深没有CP](https://www.huxiu.com/article/380222.html)
> 概要: 本文来自微信公众号：体面主义（ID：ziyutongxie2019），作者：子宇爸爸，题图来自：视觉中国整个8月，深圳和广州各有心事。深圳人的朋友圈刷屏深圳40周年。可能如果不是疫情，这个40周年会搞......
### [卫生巾为什么那么贵？](https://www.huxiu.com/article/380043.html)
> 概要: 本文来自公众号：远川商业评论（ID：ycsypl），作者：于可心、姚书恒，题图来自：视觉中国这几天，一条有关“散装卫生巾”的微博，在互联网上掀起了巨大的风浪。事情的起源是一位博主在淘宝上偶然发现了散装......
### [助初创企业乘风破浪，AWS创业者之日2020来了](https://www.tuicool.com/articles/uay6ZfE)
> 概要: 2020年，创业更难了？创新创业是这些年一个蔚然成风的词条，但从0到1从来都不是一件容易的事。本·霍洛维茨在《创业维艰》中有这样一句话：在担任CEO的8年多时间里，只有3天是顺境，剩下的8年几乎全是举......
### [美国封禁又如何？8月全球非游戏类最吸金App：TikTok无人能敌](https://www.tuicool.com/articles/BryIBrY)
> 概要: 距离9月20日美国政府封禁的日期临近，目前TikTok收购也是进入到了最后阶段，到底其在美业务会滑落谁家呢？根据Sensor Tower商店情报数据公布的8月数据显示，2020年8月，全球收入最高的非......
# 小说
### [卡厄斯的棋局](http://book.zongheng.com/book/128012.html)
> 作者：壹壹年十月

> 标签：奇幻玄幻

> 简介：天地为局，众生为棋，可身为神者，你又陷在谁的局中？人，就是杂草！人生，就应该像杂草一样艰苦求生！无论是干旱的土地，还是岩石缝隙；无论是潮湿雨林，还是城市街道。只要想活下去，杂草都要痛苦的生存，吸收每一滴水，吃下每一块土，扛住每一次践踏！ps：慢热作品,敬请耐心品读。

> 章节末：060 虚空飞渡一头颅

> 状态：完本
# 游戏
### [《魔兽世界》暗影国度动画短片彼岸之地：玛卓克萨斯](https://ol.3dmgame.com/news/202009/28325.html)
> 概要: 《魔兽世界》暗影国度动画短片彼岸之地：玛卓克萨斯
### [周五福利囧图云飞系列 让人妻难接受的泳装是啥样](https://www.3dmgame.com/bagua/3666.html)
> 概要: 转眼到周五了，一起来看看小编准备的福利囧图。托车头灯的工作还缺人吗？让人把持不住！这不是在查酒驾，姑娘请注意你的动作！多么娴熟的舌吻，多么轻柔的抚摸，街边美景经常见。让人妻都难以接受的泳装，会是什么样......
### [罗伯特帕丁森确诊感染新冠 新《蝙蝠侠》电影停拍](https://www.3dmgame.com/news/202009/3796710.html)
> 概要: 据外媒《名利场》(Vanity Fair)报道，新《蝙蝠侠》电影主演罗伯特·帕丁森(Robert Pattinson)确诊新冠阳性，而新《蝙蝠侠》的拍摄工作已暂停。华纳也发布相关声明，确认有一名《蝙蝠......
### [《NBA 2K21》今日全平台正式解锁](https://www.3dmgame.com/news/202009/3796704.html)
> 概要: 《NBA 2K21》已经于今日凌晨正式解锁，登陆PC、PS4、Xbox One、Switch和Stadia平台。Steam国区售价199元，曼巴永恒版售价489元，支持简体中文。游戏还将推出PS5/X......
### [《全面战争：三国》南蛮DLC登陆Steam：多半好评](https://www.3dmgame.com/news/202009/3796714.html)
> 概要: 目前，《全面战争：三国》南蛮DLC已经在Steam平台正式推出，国区售价84元，目前Steam总体评价为“多半好评”，112篇评测中有75%为好评。Steam商城页面截图：此次《全面战争：三国》南蛮D......
# 论文
### [Cluster-level Feature Alignment for Person Re-identification](https://paperswithcode.com/paper/cluster-level-feature-alignment-for-person-re)
> 日期：15 Aug 2020

> 标签：PERSON RE-IDENTIFICATION

> 代码：https://github.com/qychen13/ClusterAlignReID

> 描述：Instance-level alignment is widely exploited for person re-identification, e.g. spatial alignment, latent semantic alignment and triplet alignment. This paper probes another feature alignment modality, namely cluster-level feature alignment across whole dataset, where the model can see not only the sampled images in local mini-batch but the global feature distribution of the whole dataset from distilled anchors.
### [Cyclic Differentiable Architecture Search](https://paperswithcode.com/paper/cyclic-differentiable-architecture-search)
> 日期：18 Jun 2020

> 标签：None

> 代码：https://github.com/researchmm/CDARTS

> 描述：Recently, differentiable architecture search has draw great attention due to its high efficiency and competitive performance. It searches the optimal architecture in a shallow network, and then measures its performance in a deep evaluation network.
