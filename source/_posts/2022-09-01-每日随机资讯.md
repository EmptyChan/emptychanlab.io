---
title: 2022-09-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WildlifeCrossing_ZH-CN1493053695_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-09-01 21:56:10
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WildlifeCrossing_ZH-CN1493053695_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [东方甄选，意欲何为？](https://www.woshipm.com/it/5585944.html)
> 概要: 如今的东方甄选，已经成为了新东方的一块「金字招牌」。而在东方甄选推出APP之后，也让我们感受到，东方甄选并不仅仅是想做直播带货，而是想要「做一件大事」。东方甄选上线APP，意欲何为呢？很显然，现在的东......
### [我在大众点评，实现了“白吃白喝”自由](https://www.woshipm.com/it/5586142.html)
> 概要: 你相信有人能在点评类网站实现白吃白喝自由吗？本篇文章就采访了几位在大众点评中实现了吃喝自由的大V，揭开其背后的秘密，感兴趣的朋友快来看看吧。曾有无数的网民炒股返贫，很多韭菜戏称，吃饭都成问题了。但有一......
### [鬼畜告别时代](https://www.woshipm.com/it/5586124.html)
> 概要: 在鬼畜最繁荣时期，曾有“B站只有两个区，鬼畜区与鬼畜素材区”的说法，可见鬼畜在破圈方面曾经走得挺远，然而不知从何时开始，恶搞不再是鬼畜区的专属，鬼畜区则显露出疲态。本篇文章梳理了鬼畜这种题材的发展历程......
### [微服务网关Gateway实践总结](https://segmentfault.com/a/1190000042415252)
> 概要: 有多少请求，被网关截胡；一、Gateway简介微服务架构中，网关服务通常提供动态路由，以及流量控制与请求识别等核心能力，在之前的篇幅中有说过Zuul组件的使用流程，但是当下Gateway组件是更常规的......
### [电商龙头集体转向：降速换盈利 即时零售补位还是互搏？](https://finance.sina.com.cn/tech/internet/2022-09-01/doc-imizmscv8585443.shtml)
> 概要: 高补贴烧钱的时代已经过去......
### [JavaScript中的可变性与不可变性](https://segmentfault.com/a/1190000042416343)
> 概要: 不可变性(Immutability)是函数式编程的核心原则，在面向对象编程里也有大量应用。在这篇文章里，我会给大家秀一下到底什么是不可变性(Immutability)、她为什么还这么屌、以及在Java......
### [年轻人不换手机了？国产手机如何“活下来”](https://finance.sina.com.cn/tech/tele/2022-09-01/doc-imizmscv8606009.shtml)
> 概要: 文/陈惟杉......
### [SpaceX再获NASA新合同：将新增5项载人发射任务](https://finance.sina.com.cn/tech/it/2022-09-01/doc-imizmscv8605121.shtml)
> 概要: 新浪科技讯 北京时间9月1日早间消息，据报道，美国航空航天局（NASA）在8月31日表示，根据一份新的价值14亿美元的合同订单，SpaceX公司将在2030年之前为NASA再执行5次载有宇航员的国际空......
### [东方甄选、顺丰、京东坐上了同一张“牌桌”](https://finance.sina.com.cn/tech/internet/2022-09-01/doc-imizmscv8608266.shtml)
> 概要: 每经记者 王郁彪  杨昕怡    每经编辑 程鹏 刘雪梅......
### [HTML5 地理定位+地图 API：计算用户到商家的距离](https://segmentfault.com/a/1190000042419042)
> 概要: 最近在做一个类似支付宝口碑商家的功能模块，其中有个功能就是计算出用户与商家的距离，如下图：支付宝口碑商家页面截图思路分析1、商家选取店铺地址，将坐标经纬度存入数据库；2、移动端定位当前用户坐标经纬度；......
### [性能优化必备——火焰图](https://segmentfault.com/a/1190000042419139)
> 概要: 引言本文主要介绍火焰图及使用技巧，学习如何使用火焰图快速定位软件的性能卡点。结合最佳实践实战案例，帮助读者加深刻的理解火焰图构造及原理，理解 CPU 耗时，定位性能瓶颈。背景当前现状假设没有火焰图，你......
### [JOIN US DESIGN 2](https://www.zcool.com.cn/work/ZNjE3MTA4NDA=.html)
> 概要: JOIN US 为公司自研的B端上线项目，是一款职位招聘管理的应用，目前已服务于数万员工。项目涵盖Pc端和移动端，本提案主要展示和设计方面相关的内容，内容主要包括设计规范、交互设计和视觉设计，提案如有......
### [《JOJO群星之战R》发售预告 Steam定价248元](https://www.3dmgame.com/news/202209/3850650.html)
> 概要: 万达南梦宫发布了《JOJO的奇妙冒险 群星之战 重制版》的发售预告片，该作现已正式登陆PS5、PS4、Xbox Series X|S、Xbox One、Switch和Steam（9月2日解锁），支持中......
### [视频：杨幂和尔冬升导演深夜聚餐 网友：是不是有新合作了？](https://video.sina.com.cn/p/ent/2022-09-01/detail-imizmscv8628642.d.html)
> 概要: 视频：杨幂和尔冬升导演深夜聚餐 网友：是不是有新合作了？
### [下一部漫画大奖2022公开获奖结果](https://news.dmzj.com/article/75425.html)
> 概要: 由KADOKAWA和niconico共同运营的“下一部漫画大奖2021”公开了获奖结果。其中，实体书漫画部门第一位由《舞冰的祈愿（原名直译：奖牌获得者）》获得。
### [《拳愿阿修罗》第二季新视觉图 2023年上线Netflix](https://acg.gamersky.com/news/202209/1514378.shtml)
> 概要: Netflix根据人气热血漫画《拳愿阿修罗》改编的同名动画《拳愿阿修罗》将推出第二季，官方公开了第二季动画的视觉图。
### [优必选科技推出智慧康养全体系解决方案，与多家康养巨头达成合作](http://www.investorscn.com/2022/09/01/102726/)
> 概要: 8月31日，由中国老龄产业协会、上海市养老服务行业协会、广东省养老服务业协会、深圳市人工智能产业协会指导的《有AI就是幸福——科技与健康养老融合发展论坛暨优必选智慧康养全球战略发布会》在深圳举行，来自......
### [“疯狂星期四”，一种虚假繁荣？](https://www.huxiu.com/article/650342.html)
> 概要: 本文来自微信公众号：锐见Neweekly （ID：app-neweekly），作者：恺哥，原文标题：《这个夏天，没人能逃得过“疯四”文学》，题图来自：视觉中国大声告诉我，今天是什么日子？这个夏天，没人......
### [插画师组团入驻30层地下商城 | 集体创意](https://www.zcool.com.cn/work/ZNjE3MTQ4NzY=.html)
> 概要: 这是一次ROOM114社群邀请活动，灵感就来自今年这炎热的夏天……那会儿正在思索新活动的我们，出门就会感觉进入了一个大焖罐，“阴凉”不复存在！也正是这一点启发到我们——不如让新活动的场景安置在地下吧......
### [《2022投资入籍项目排名》公布：移投界“新秀”圣卢西亚位列第三](http://www.investorscn.com/2022/09/01/102730/)
> 概要: 8月31日，《投资入籍项目排名》公布，圣卢西亚以总分78分的成绩排名在第三位，较去年的第四名再进一步......
### [声优平野绫离开原所属事务所 今后以演出为中心活动](https://news.dmzj.com/article/75429.html)
> 概要: 声优平野绫宣布了因合同期满，于9月1日离开原所属的事务所Grick的消息。在今后平野绫将以舞台为中心，作为演员活动。
### [手写集 | 手写与彩墨碰撞试验](https://www.zcool.com.cn/work/ZNjE3MTUzODQ=.html)
> 概要: -会有一个习惯，不定时的回看以前的作品，即使当时觉得是很不错的作品，过段时间再看也会觉得有进步和调整的空间，会去思考如何才能变得更好。-找了一些字形很有张力很美的字来做个实验，于是就有了今天这一组字，......
### [云鲸发布新品J3小鲸灵：怎样清洁才干净，它都替你想好了](http://www.investorscn.com/2022/09/01/102731/)
> 概要: 云鲸发布了最新一代产品的宣传片，正式推出第三代扫拖一体机器人——J3小鲸灵。云鲸J3小鲸灵于8月31日20：00在天猫、京东、苏宁、抖音等各大平台同步开始预售......
### [优衣库和MUJI都不是对手，这件白T火遍日本](https://www.tuicool.com/articles/zArqeaz)
> 概要: 优衣库和MUJI都不是对手，这件白T火遍日本
### [“我在B站分享坐牢经验，1个月涨粉近200万”](https://www.tuicool.com/articles/rYzYFfJ)
> 概要: “我在B站分享坐牢经验，1个月涨粉近200万”
### [有妖气漫画将于12月31日关停 加入B站漫画大家族](https://acg.gamersky.com/news/202209/1514504.shtml)
> 概要: 原创漫画平台有妖气官博发布公告称，该平台将于12月31日正式关停。
### [GSC《初音未来》15周年纪念版1/7比例手办开订](https://news.dmzj.com/article/75433.html)
> 概要: GSC根据在2022年8月31日迎来15周年纪念的《初音未来》制作的初音未来15周年纪念版1/7比例手办目前正在预订中。本作以“草莓”为灵感设计，再现了森仓圆为本手办绘制的插图。
### [对 JavaBean 的特点写法与实战心得详解](https://www.tuicool.com/articles/22MRraM)
> 概要: 对 JavaBean 的特点写法与实战心得详解
### [[连载】中投也很实用！詹姆斯哈登中投集锦第一部](https://bbs.hupu.com/55368669.html)
> 概要: B站和微信公众号ID：(76人控卫登登)，欢迎喜欢哈登的兄弟来关注呦！！
### [人间烟火盛处，柴火大院五常大米成为中秋送礼新选择！](http://www.investorscn.com/2022/09/01/102739/)
> 概要: 四方食事，不过一碗人间烟火。烟火最盛处，是心安吾乡，对国人而言，这碗人间烟火是与亲朋好友在熙熙攘攘中相互交融出的“人气”......
### [波场Poloniex启用全球通用顶级域名P.xyz，向国际顶级交易所迈出重要一步](http://www.investorscn.com/2022/09/01/102740/)
> 概要: 9月1日，波场Poloniex官方推特宣布，正式启用新顶级域名“P.xyz”，助力平台为全球更多用户提供便捷高效的加密货币交易服务。据了解，“.xyz”是全球通用顶级域名，拥有超480万注册量，位列新......
### [《JOJO石之海》第二章B站正式开播！13-18集全放送](https://acg.gamersky.com/news/202209/1514583.shtml)
> 概要: 《JOJO的奇妙冒险：石之海》第二章（13-18集上线）现已正式B站独家上线！
### [在美国能放手的辅助驾驶，在中国好用吗？](https://www.huxiu.com/article/650055.html)
> 概要: 出品丨虎嗅汽车组作者丨周到头图丨虎嗅似乎，全天下已经没有人不知道特斯拉、蔚来、小鹏和理想等新势力的“辅助驾驶厉害”这件事了。而伴随着一次次事故，大家又不禁有些怀疑，这些辅助驾驶，是否真的足够安全。相比......
### [【字幕】盘点NBA球员特殊训练方法:波神把篮筐缩小练投篮,威少KD一起在沙丘训练](https://bbs.hupu.com/55369014.html)
> 概要: 【三叶屋出品】大家喜欢的话，点赞转发关注～
### [小米手机开启 8 折换电池活动：支持 9 大机型，79 元起](https://www.ithome.com/0/638/446.htm)
> 概要: 感谢IT之家网友www434566、Gakkilove、软媒新友ubivpj、新心思、软媒用户1007347、情系半生nh的线索投递！IT之家9 月 1 日消息，小米服务今天公布了服务周手机电池换新活......
### [视频：关晓彤自曝也爱在横店找明星 喊话王鹤棣去梁爽直播间买水](https://video.sina.com.cn/p/ent/2022-09-01/detail-imiziraw0680040.d.html)
> 概要: 视频：关晓彤自曝也爱在横店找明星 喊话王鹤棣去梁爽直播间买水
### [小米钱包上线兰州公交一卡通（交通联合）：息屏刷卡，“嘀一声”就通行](https://www.ithome.com/0/638/451.htm)
> 概要: 感谢IT之家网友草莓可鲤饼的线索投递！IT之家9 月 1 日消息，小米手机钱包最新上线兰州公交一卡通，手机变身公交卡“嘀一声”就通行。支持息屏刷卡，不用解锁手机，贴一下过闸机；有据可查，乘车 / 余额......
### [联想推出第二代 Tab P11 / P11 Pro 平板：分别搭载联发科 Helio G99 / 迅鲲 1300T 芯片，起售价约 2000 元](https://www.ithome.com/0/638/454.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家9 月 1 日消息，今日，联想推出了第二代 Tab P11 / P11 Pro 平板。其中第二代 Tab P11 标准版起售价 299 美元（约 2063......
### [英伟达停货，伤了谁](https://www.huxiu.com/article/650916.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自视觉中国如果能给人工智能与科学计算产业设一个热搜榜，那么从昨晚到今天，榜单只有两个热词：英伟达停货，AMD停货。作为全球顶级计算芯片企业，两家的公告几乎一夜之间......
### [TV动画《作为恶役大小姐就该养魔王》公开新PV](https://news.dmzj.com/article/75436.html)
> 概要: TV动画《作为恶役大小姐就该养魔王》宣布了将于10月1日开始播出的消息。本作的第二弹PV也一并公开。在这次的PV中，收录了由高桥李依演唱的主题曲的片段。
### [Cityboy是怎么烂大街的？](https://www.huxiu.com/article/650981.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。说不清是因为疫情三年，大家搁家变胖了，还......
### [国产山寨iPhone 14 Pro Max出炉：叹号屏不服不行](https://www.3dmgame.com/news/202209/3850703.html)
> 概要: 赶在苹果9月8日凌晨的发布会前，山寨版iPhone 14 Pro Max已经在国内抢先出炉。无论是机器本体还是外包装，显然是铁了心的山寨克隆正版的既视感。仔细看正面的话，桌面采用的是高仿iOS主题，叹......
### [男孩暑假送外卖挣17350元交学费 暴晒下每天送50多单](https://www.3dmgame.com/news/202209/3850706.html)
> 概要: 8月31日，山东齐河一位妈妈发视频记录自己19岁儿子2个月暑假生活。视频中，她透露儿子暑期送外卖挣了17350元，并用来交了学费，剩下的生活费也够了。视频欣赏：据悉，儿子七月份开始送外卖，妈妈起初认为......
### [资金成本低至4.1%，招商蛇口“双百战略”正逐步落地](https://www.yicai.com/news/101524897.html)
> 概要: 资金成本低至4.1%，招商蛇口“双百战略”正逐步落地
### [印度二季度经济同比增长13.5%，高速增长能持续多久？](https://www.yicai.com/news/101524895.html)
> 概要: 印度央行预计印度经济全年增长7.2%。
### [郭明錤：印度制造版苹果 iPhone 14 量产依旧落后中国 6 周](https://www.ithome.com/0/638/486.htm)
> 概要: 北京时间 9 月 1 日消息，天风国际证券知名苹果分析师郭明錤今天发布推文称，iPhone 14 今年在印度的量产进度仍落后中国大约 6 周时间。▲ 印度制造版 iPhone 14 仍落后于中国今年 ......
### [5000亿的生意，为什么这群男人搅黄了？](https://www.tuicool.com/articles/jeINNrM)
> 概要: 5000亿的生意，为什么这群男人搅黄了？
### [四川、重庆银保监局齐唱“双城记”，共建“经济圈”](https://www.yicai.com/news/101524936.html)
> 概要: 截至今年上半年，川渝两地共有银行业金融机构338家，资产规模占全国的5.63%，贷款同比增速高于全国平均2.16个百分点。
### [中国恒大：于8月30日接获联交所的额外复牌指引](https://www.yicai.com/news/101524976.html)
> 概要: 公告称，本公司的股份由2022年3月21日上午9时正起于联交所停止买卖。本公司 股份将继续暂停买卖，直至另行通知。
### [俄妹Cos《艾尔登法环》女武神二阶段 气场十足](https://www.3dmgame.com/bagua/5540.html)
> 概要: 近日俄罗斯美女Coser Alin Ma分享了Cos《艾尔登法环》女武神二阶段美照，虽然只有一张，但特效拉满，气场十足。Alin Ma(ins：xenon_ne)身材火辣，性感无比。其种族天赋颜值展现......
### [财经TOP10|理想汽车销量断崖式下滑，海底捞、呷哺呷哺关店求生，爱尔眼科被罚70万](https://finance.sina.com.cn/china/caijingtop10/2022-09-01/doc-imqmmtha5548511.shtml)
> 概要: 【政经要闻】 NO.1 服贸会开幕！想去打卡的你，先了解这9个细节 2022年中国国际服务贸易交易会（下称“服贸会”）31日正式拉开大幕！
### [韩正：大力发展新能源和清洁能源，全面提升可再生能源利用水平](https://finance.sina.com.cn/china/gncj/2022-09-01/doc-imizmscv8717075.shtml)
> 概要: 韩正强调，中国将加快构建清洁低碳、安全高效的能源体系，不断增强能源供应稳定性、安全性、可持续性。 中共中央政治局常委、国务院副总理韩正1日以视频形式出席2022年太原...
### [广东农业农村厅：强化对非法擅自放生外来物种行为的打击处罚力度](https://finance.sina.com.cn/china/gncj/2022-09-01/doc-imizmscv8716655.shtml)
> 概要: 澎湃新闻记者 钟煜豪 外来入侵物种的防治工作近日广受关注，而擅自放生外来物种的行为引发颇多讨论。 澎湃新闻注意到，广东省农业农村厅日前在答复政协委员《关于防治外来...
### [JR热议思路清晰，Tian这把巨魔的针对打出了效果？](https://bbs.hupu.com/55373207.html)
> 概要: ﻿给Tian本局表现打分感觉tian目前几盘打下来思路真的很清晰。巨魔的发挥也很好限制住了kanavi这边选出来的大招潘森，功不可没啊
### [超限爆闪闪耀全场！你怎么评价这把JackeyLove泽丽的发挥？](https://bbs.hupu.com/55373234.html)
> 概要: 在刚刚结束的JDG对阵TES的第四局比赛里，JackeyLove再次选到了AD泽丽，在比赛里JackeyLove在团战中打出爆炸的伤害完成了收割，虽然对线期略有瑕疵，但瑕不掩瑜。超限爆闪闪耀全场！你怎
### [组图：王菊30岁生日办新娘舞会主题趴 穿婚纱梦回《爱很美味》](http://slide.ent.sina.com.cn/star/w/slide_4_704_374640.html)
> 概要: 组图：王菊30岁生日办新娘舞会主题趴 穿婚纱梦回《爱很美味》
### [湖北发布“稳增长18条”：支持企业开首店办首秀，再发5000万专项消费券](https://finance.sina.com.cn/roll/2022-09-01/doc-imizmscv8719670.shtml)
> 概要: 21世纪经济报道记者刘茜 武汉报道 为了打好三季度经济发展攻坚战，8月31日，湖北公布《关于进一步激发市场活力稳住经济增长的若干措施》（以下简称《措施》）。
### [微软收购动视暴雪受阻 英国当局认定会影响竞争](https://www.3dmgame.com/news/202209/3850710.html)
> 概要: 微软对动视暴雪的收购看起来在英国遇到了阻力。英国竞争与市场管理局已经决定，微软创纪录地收购动视暴雪可能会影响英国国内的竞争。9月1日发布的第一阶段裁决称，合并“可能会导致一个市场或英国市场内的竞争大幅......
### [进击“中国氢都”：最高补贴2千万，武汉出台支持氢能发展16条政策](https://finance.sina.com.cn/roll/2022-09-01/doc-imqmmtha5551767.shtml)
> 概要: 21世纪经济报道记者刘茜 武汉报道 在政策的支持下，我国氢能产业进入发展快车道，也成为多地在能源新风口下拥抱发展机遇的关键赛道。
### [国务院督导组赴经济大省联合办公，激发微观主体活力](https://www.yicai.com/news/101525033.html)
> 概要: 督导重点涉及稳投资、减税降费、政策性开发性金融工具落地、房地产保交楼等
# 小说
### [绝影神尊](https://book.zongheng.com/book/838251.html)
> 作者：瞬影定格

> 标签：奇幻玄幻

> 简介：追求武道，始于初心，历尽生死，心坚志定。偶获至宝，自创神技，影之所到，血洒纷飞。少年一手执剑，影灭八方，杀出通天之路，踏血歌泣，独傲群雄。

> 章节末：第四百二十四章  登顶万界之首

> 状态：完本
# 论文
### [Out-of-Distribution Detection in Time-Series Domain: A Novel Seasonal Ratio Scoring Approach | Papers With Code](https://paperswithcode.com/paper/out-of-distribution-detection-in-time-series)
> 日期：9 Jul 2022

> 标签：None

> 代码：https://github.com/tahabelkhouja/srs

> 描述：Safe deployment of time-series classifiers for real-world applications relies on the ability to detect the data which is not generated from the same distribution as training data. This task is referred to as out-of-distribution (OOD) detection. We consider the novel problem of OOD detection for the time-series domain. We discuss the unique challenges posed by time-series data and explain why prior methods from the image domain will perform poorly. Motivated by these challenges, this paper proposes a novel {\em Seasonal Ratio Scoring (SRS)} approach. SRS consists of three key algorithmic steps. First, each input is decomposed into class-wise semantic component and remainder. Second, this decomposition is employed to estimate the class-wise conditional likelihoods of the input and remainder using deep generative models. The seasonal ratio score is computed from these estimates. Third, a threshold interval is identified from the in-distribution data to detect OOD examples. Experiments on diverse real-world benchmarks demonstrate that the SRS method is well-suited for time-series OOD detection when compared to baseline methods. Open-source code for SRS method is provided at https://github.com/tahabelkhouja/SRS
### [FairLex: A Multilingual Benchmark for Evaluating Fairness in Legal Text Processing | Papers With Code](https://paperswithcode.com/paper/fairlex-a-multilingual-benchmark-for-1)
> 概要: We present a benchmark suite of four datasets for evaluating the fairness of pre-trained language models and the techniques used to fine-tune them for downstream tasks. Our benchmarks cover four jurisdictions (European Council, USA, Switzerland, and China), five languages (English, German, French, Italian and Chinese) and fairness across five attributes (gender, age, region, language, and legal area). In our experiments, we evaluate pre-trained language models using several group-robust fine-tuning techniques and show that performance group disparities are vibrant in many cases, while none of these techniques guarantee fairness, nor consistently mitigate group disparities. Furthermore, we provide a quantitative and qualitative analysis of our results, highlighting open challenges in the development of robustness methods in legal NLP.
