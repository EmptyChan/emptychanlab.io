---
title: 2020-07-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HappyBalloon_EN-CN5158200824_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-07-17 21:45:56
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HappyBalloon_EN-CN5158200824_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：杨烁夫妇罕见同框走机场 穿情侣鞋紧搂老婆大秀恩爱](http://slide.ent.sina.com.cn/star/slide_4_704_342100.html)
> 概要: 组图：杨烁夫妇罕见同框走机场 穿情侣鞋紧搂老婆大秀恩爱
### [组图：周笔畅全黑look戴三层项链个性十足 黑色短发造型炫酷](http://slide.ent.sina.com.cn/star/slide_4_704_342098.html)
> 概要: 组图：周笔畅全黑look戴三层项链个性十足 黑色短发造型炫酷
### [应采儿晒陈小春抱娃照 小儿子乖巧伏肩画面温馨](https://ent.sina.com.cn/s/2020-07-17/doc-iivhuipn3619667.shtml)
> 概要: 新浪娱乐讯 7月17日，应采儿在社交平台晒出一张陈小春抱着小儿子Hoho的照片。陈小春温柔地托举着Hoho，Hoho则乖巧地靠在爸爸的肩膀上，背影十分可爱，画面温馨。　　网友纷纷评论：“好温馨呀！”“......
### [视频：许飞感谢粉丝投复活票 霸气表示不恨黑骂自己的人](https://video.sina.com.cn/p/ent/2020-07-17/detail-iivhvpwx5859024.d.html)
> 概要: 视频：许飞感谢粉丝投复活票 霸气表示不恨黑骂自己的人
### [邓超别样庆影迷会成立15周年 向机舱外比心说唇语](https://ent.sina.com.cn/s/m/2020-07-17/doc-iivhuipn3577547.shtml)
> 概要: 新浪娱乐讯 7月17日是邓超影迷会无泪之城成立15周年的纪念日，邓超在微博分享一段视频别样庆祝，并at了自己的影迷会和邓超吧官微。视频中邓超坐在即将起飞的机舱内，他向机舱外记录着这一切的视角说了出“7......
### [《浪姐》再惹争议，排名迷惑难懂，张雨绮天山童姥造型是认真的？](https://new.qq.com/omn/20200717/20200717A0S1F500.html)
> 概要: 告别精彩的二公舞台，《乘风破浪的姐姐》迎来了三公分组！前两次组队都“宫心计”，这次姐姐也都是带着自己的小心思来的。            上一次当过队长的姐姐们，这次都不想再当。，已经表明不想当队长的......
### [第13届金鹰节主持阵容：汪涵占C位，朱广权王一博为“副咖”](https://new.qq.com/omn/20200717/20200717A0IN9U00.html)
> 概要: 金鹰电视艺术节，作为电视剧圈子来说，也算是个盛典了。每两年举办一次，也是令人十分期待。            这不第13届金鹰节很快就要来临了，目前不少观众也在积极地为自己喜欢的电视剧、演员投票。7月......
### [数据虾事业狂潮的背后，怎样定义明星的影响力，肖战这步棋下对了](https://new.qq.com/omn/20200717/20200717A0LQPJ00.html)
> 概要: 肖战的名字现在可以说是家喻户晓的，当然有人喜欢也会有人抵制。当时，他在《陈情令》的表演让他的人气飙升，同时他也拥有了大量的粉丝。然而，当时只能说他在饭圈或者说娱乐圈里面刚刚初出茅庐，而没有达到跨圈吸粉......
### [郭晶晶搬入新豪宅是为了拼四胎？听听公公婆婆是怎么说的](https://new.qq.com/omn/20200717/20200717A0N1SY00.html)
> 概要: 近日，好久没在公开场合露面的郭晶晶传来一个好消息，她和老公霍启刚搬入了总价1.6亿元的新豪宅当中。            当然至于为什么搬家，港媒也是给出了答案，那就是因为郭晶晶生够了3个宝宝成功达标......
### [51岁蔡国庆休闲穿搭显身材，搞怪比耶活力十足，还是当年模样](https://new.qq.com/omn/20200717/20200717A0SIH700.html)
> 概要: 51岁蔡国庆休闲穿搭显身材，搞怪比耶活力十足，还是当年模样
# 动漫
### [阔诺新连载哒！7月新连载漫画不完全指北第二期](https://news.dmzj.com/article/67977.html)
> 概要: 新来的转校生兄妹并不是很凶只是面瘫而已？那个眼睛很漂亮的OL究竟能不能成为艺人？！DD的我（们）也能成为爱抖露吗————？！请看本周新连载不完全指北！
### [纵火烧毁京都动画工作室的青叶真司烧伤基本痊愈](https://news.dmzj.com/article/67976.html)
> 概要: 根据日本媒体报道，涉嫌纵火烧毁京都动画工作室，造成36人死亡的犯罪嫌疑人青叶真司，目前的烧伤已经基本痊愈，回复到了无需继续接受追加手术的状态。
### [游戏王历史：从零开始的游戏王环境之旅第五期04](https://news.dmzj.com/article/67980.html)
> 概要: 以“连锁爆击”为首的Chain卡牌的加入使得【Chain Burn】牌组成立，并逐渐作为极具威胁的速攻烧血牌组渗透进Meta环境中。虽然它弱点很多、没能进军顶级Meta的位置，但作为地雷来说很强，只要存在于环境中就能给Metagame带来影响。...
### [【欢乐向编辑部】“热情”7月番，你喜欢哪部？~](https://news.dmzj.com/article/67981.html)
> 概要: “热情”7月番，你喜欢哪部？~
### [斗罗：唐昊到达杀戮之都，为何唐晨不敢与他相见？因为唐晨打不过](https://new.qq.com/omn/20200717/20200717A0NBM500.html)
> 概要: 《斗罗大陆》是当红网络小说作家唐家三少的巅峰代表作，经过近十年的连载，如今已经更新到第四部《终极斗罗》，不过在粉丝们心目中最经典的还是第一部。正是因为斗罗大陆1有如此高的人气，所以相继被改编成漫画和……
### [最终战场确认！斩龙传说再现！尾田的完美计划](https://new.qq.com/omn/20200717/20200717A0MCJV00.html)
> 概要: 你们好，本期将讲述官方的最新剧情发展。若害怕被剧透的请在5秒之内点出去。那么我们继续本篇内容海贼王第985话的情报已经更新的很大部分，虽然还无法完全了解本话的完整内容，但是具体的内容也大致了解了，这……
### [20年前火爆全中国，80后在电视上都见过它，却因为太坑钱而凉凉](https://new.qq.com/omn/20200717/20200717A0MRHS00.html)
> 概要: 对于80后来说点播台一定是童年时期最重要的回忆之一。不知道有多少的无知少年因为点播台让家里的当月电话费飙升，遭到了爸妈的混合双打，或许这也是他们人生当中交的第一笔智商税。后来因为点播台昂贵的收费规则……
### [动漫情头强势来袭，快来送给你的TA！](https://new.qq.com/omn/20200717/20200717A0LY2600.html)
> 概要: ...
### [iQOOZ1航海王限量版预售进行时，2498元加量不加价](https://new.qq.com/omn/20200717/20200717A0L3Q600.html)
> 概要: 提到日漫里的经典之作，《航海王》绝对是最经典、最热门的漫画之一，不仅被吉尼斯世界纪录认证为“世界上发行量最高的单一作者创作的系列漫画”，连载20多年至今当之无愧的成为漫画界中最热门的漫画之一。可以说……
### [狐妖小红娘：黑狐娘娘计谋得逞，苏苏被抓，蓉蓉一句话道破原因](https://new.qq.com/omn/20200717/20200717A0TA8J00.html)
> 概要: 本文由“咸鱼漫评”编辑发布，未经准许不得搬运，违者必究！写独家漫评，做良心推荐，hello大家好，我是咸鱼——一个有态度的创作者。《狐妖小红娘 金晨曦篇》终于再次更新了，在最近的剧情中，黑狐娘娘的分……
# 财经
### [工信部：上半年清偿拖欠民营和中小企业款项956亿元](https://finance.sina.com.cn/roll/2020-07-17/doc-iivhuipn3622030.shtml)
> 概要: 原标题：工信部：上半年清偿拖欠民营和中小企业款项956亿元 中新社北京7月17日电 （记者 刘育英）中国工业和信息化部副部长王江平17日介绍，2020年上半年...
### [发改委：我国经济保持企稳回升态势有条件、有基础](https://finance.sina.com.cn/roll/2020-07-17/doc-iivhvpwx5996162.shtml)
> 概要: 原标题：“我国经济保持企稳回升态势有条件、有基础”——国家发展改革委谈经济走势 新华社北京7月17日电 题：“我国经济保持企稳回升态势有条件...
### [江苏南通扩容：撤销海门市设立海门区 有助南通统筹发展](https://finance.sina.com.cn/china/2020-07-17/doc-iivhvpwx5988089.shtml)
> 概要: 江苏南通扩容：撤销海门市设立海门区 有助南通统筹发展 城市扩容再增一例。 7月17日，据江苏省人民政府公布，国务院日前批复同意江苏省调整南通市部分行政区划...
### [站稳城市竞争新赛道：杭州南京等多个新一线城市发力科创](https://finance.sina.com.cn/china/2020-07-17/doc-iivhvpwx5986790.shtml)
> 概要: 站稳城市竞争新赛道：多个新一线城市发力科创 7月16日上午，中芯国际在上海证券交易所鸣锣上市，成为自去年科创板开市以来的一个重量级、标杆性的“压舱石”企业。
### [一个公开的秘密：高校就业率造假调查](https://finance.sina.com.cn/china/gncj/2020-07-17/doc-iivhuipn3619807.shtml)
> 概要: 一个公开的秘密：高校就业率造假调查|深度报道 来源：北青深一度 采写/郭玉洁 蒋宇楼 袁思檬 编辑/计巍 找工作的毕业生（图/新华社） 在毕业生群体中...
### [今日财经TOP10|中方制裁美军火商：在华边捞钱边资助反华势力](https://finance.sina.com.cn/china/caijingtop10/2020-07-17/doc-iivhuipn3616039.shtml)
> 概要: 【宏观要闻】 NO.1 一场权威发布会透露重磅消息 外资大举买入中国资产 “受疫情影响，今年以来全球的直接投资总体低迷，上半年中国的利用外资达到了4722亿元人民币...
# 科技
### [DrissionPage - 以POM模式整合selenium和requests的web自动化工具](https://www.ctolib.com/g1879-DrissionPage.html)
> 概要: DrissionPage - 以POM模式整合selenium和requests的web自动化工具
### [Linux 系统监控与优化工具：Stacer](https://www.ctolib.com/oguzhaninan-Stacer-.html)
> 概要: Linux 系统监控与优化工具：Stacer
### [fairscale：PyTorch扩展用于高性能和大规模的训练](https://www.ctolib.com/facebookresearch-fairscale.html)
> 概要: fairscale：PyTorch扩展用于高性能和大规模的训练
### [✨ 一个 LeetCode 答题看板的生成插件](https://www.ctolib.com/lryong-hugo-leetcode-dashboard.html)
> 概要: ✨ 一个 LeetCode 答题看板的生成插件
### [估值140亿，元気森林遇上最凶猛对手：喜茶正式卖气泡水](https://www.tuicool.com/articles/nUFbQnb)
> 概要: 这一次，喜茶杠上了新晋独角兽元気森林。投资界（ID:pedaily2012）获悉，日前，通过官方公众号宣布，继子品牌“喜小茶”首条果汁产线面世后，又推出汽水产线，主打0糖0脂+膳食纤维。开卖首日，喜茶......
### [Why companies struggle with recalcitrant IT](https://www.tuicool.com/articles/R3myE37)
> 概要: IT IS SUPPOSEDto be the “Tesla killer”. Volkswagen’s newID.3 is the firm’s first mass-produced all-e......
### [一年内连获五轮融资，印度在线教育公司「Vedantu」完成 1 亿美元 D 轮融资](https://www.tuicool.com/articles/If6r2eV)
> 概要: 据外媒VCCircle报道，印度Vedantu完成1亿美元D轮融资，由 Coatue Management 领投。根据Mosaic Digital 数据研究部门 VCCEdge 的报道，Vedantu......
### [从虎扑女神大赛看，直男审美到底是什么样](https://www.tuicool.com/articles/7RR3QfZ)
> 概要: 本文来自微信公众号：网易数读（ID：datablog163），作者：严钰，数据：涂维标，头图来自：《速度与激情5》剧照第五届“虎扑女神大赛”结束了，连续四届屈居亚军的“无冕之王”高圆圆终于夺冠，成为新......
### [看好苹果在 5G 市场的表现，加拿大投行上调苹果目标价至 444 美元](https://www.ithome.com/0/498/355.htm)
> 概要: 7 月 17 日消息，据国外媒体报道，由于看好苹果在 5G 市场的表现，加拿大投行加通贝祥（Canaccord Genuity）将苹果的目标价从 310 美元上调至 444 美元，并重申了对苹果股票的......
### [ROG 游戏手机 3 肩键升级：4 键联动](https://www.ithome.com/0/498/410.htm)
> 概要: IT之家 7 月 17 日消息 ROG 游戏手机 3 将于 7 月 23 日正式发布，在今天的预热中，官方表示新机的 AirTrggers 肩键重磅升级，可 4 键联动。IT之家了解到， ROG 游戏......
### [三星 870 QVO  SSD 上架：单盘 4TB，4099 元](https://www.ithome.com/0/498/411.htm)
> 概要: IT之家 7 月 17 日消息 本月初，三星发布了 870 QVO SSD，现已在京东上架，1TB 849 元，4TB 4099 元。IT之家了解到，870 QVO 采用了三星第二代 QLC 闪存，可......
### [宁德时代公布非公开发行股票发行结果：高瓴资本认购 100 亿元](https://www.ithome.com/0/498/422.htm)
> 概要: 7 月 17 日消息，宁德市代发布公告，公布非公开发行股票发行结果，其中高瓴资本认购 100 亿元，本田认购 37 亿元。根据公告，宁德市代本次发行股份数量 122,360,248 股，发行价格为 1......
# 小说
### [网游之生命祭祀](http://book.zongheng.com/book/188179.html)
> 作者：初雪寒江

> 标签：科幻游戏

> 简介：来日方长，写点文字！虽然很烂，可以不看。

> 章节末：第十章

> 状态：完本
# 游戏
### [《孤岛危机：复刻版》Switch新预告 真实的720P30帧](https://www.3dmgame.com/news/202007/3793239.html)
> 概要: 今天（7月17日）《孤岛危机》官方，公开了《孤岛危机：复刻版》Switch 版的全新技术宣传片（Tech Trailer），展示了本作在 720P，30帧模式下运行的实机画面。《孤岛危机：复刻版》Sw......
### [《侍魂 晓》联动《王者荣耀》 两游戏各加入对方角色](https://www.3dmgame.com/news/202007/3793207.html)
> 概要: 之前《侍魂：晓》推出了联动《荣耀战魂》的角色“看守者(The Warden)”。今日(7月17日)SNK与《王者荣耀》官方联合宣布，《侍魂 晓》将与《王者荣耀》联动，两款游戏都将新加入对方的一名角色......
### [马来西亚钢琴女神李元玲美图欣赏 真人版海贼王娜美](https://www.3dmgame.com/bagua/3445.html)
> 概要: 今天为大家带来的是马来西亚钢琴女神李元玲的美图，她身材姣好，气质端庄，颜值出众，很有清纯女大学生的感觉。但你别被她纯情长相给欺骗了，这位钢琴女神可是健身专家，健美而不壮硕，没有“金刚芭比”的感觉。可又......
### [亨利·卡维尔在线性感组装电脑主机 游戏宅的自我修养](https://www.3dmgame.com/news/202007/3793194.html)
> 概要: 国外疫情严重，《巫师》第二季的拍摄工作被迫中止。杰洛特的扮演者、游戏爱好者亨利·卡维尔闲在家里无聊，就购买了电脑配件，自己动手组装游戏PC机。他还在线分享了组装PC的详细示范，技术很稳，BGM超赞，一......
### [网传任天堂将于直面会推出《超级马力欧纪念合集》](https://www.3dmgame.com/news/202007/3793211.html)
> 概要: 曾准确爆料几次任天堂直面会的知名舅舅党“Zippo”，近日爆料称任天堂将于7月20日举办直面会，并发布《超级马力欧纪念合集》和《超级马里欧3D世界 豪华版》。据 Zippo 的说法，《超级马力欧纪念合......
# 论文
### [Defense of Word-level Adversarial Attacks via Random Substitution Encoding](https://paperswithcode.com/paper/defense-of-word-level-adversarial-attacks-via)
> 日期：1 May 2020

> 标签：SENTIMENT ANALYSIS

> 代码：https://github.com/Raibows/RSE-Adversarial-Defense

> 描述：The adversarial attacks against deep neural networks on computer version tasks has spawned many new technologies that help protect models avoiding false prediction. Recently, word-level adversarial attacks on deep models of Natural Language Processing (NLP) tasks have also demonstrated strong power, e.g., fooling a sentiment classification neural network to make wrong decision.
