---
title: 2022-09-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LastDollarRoad_ZH-CN1462265798_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-09-23 22:44:32
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LastDollarRoad_ZH-CN1462265798_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [初创公司要在新兴行业打拼，经营型公司要在落日行业接盘](https://www.woshipm.com/chuangye/5615054.html)
> 概要: 赛道的选择对创业有多重要不言而喻，选对了赛道就有可能迎风而上，选错了赛道很可能前功尽弃。本文作者作为一名创业者，对赛道的选择有自己的理解，一起来看看吧。初创公司一定要在新兴行业发展里当矛，而不是在传统......
### [如何建立提炼并创作品牌文本？](https://www.woshipm.com/copy/5616159.html)
> 概要: 品牌需要建立完善的品牌体系以此规划和布局，消费者需要通过品牌文本来理解品牌，企业的各种营销推广同样需要通过文本的指引展开。在品牌体系中，品牌文本起着至关重要的作用。本文将分别阐述如何建立提炼并创作品牌......
### [钟薛高招嘴替，到底是赚口碑还是去库存](https://www.woshipm.com/operate/5616326.html)
> 概要: 钟薛高前几个月因为“雪糕刺客”事件被推上风口浪尖，不久前，做了次招嘴替活动，引起了热议。这是一场既拉高了品牌口碑，又清掉库存的好营销活动吗，来看看作者的观点，欢迎感兴趣的伙伴阅读。作者：潘哥 编辑：三......
### [易联购预售iPhone 14两个月后携款跑路，警方已立案，问题出在哪里？](https://finance.sina.com.cn/tech/internet/2022-09-23/doc-imqqsmrp0154046.shtml)
> 概要: 被各种商家营销套路侵害权益？买到的商品出故障投诉无门？ 黑猫投诉平台全天候帮您解决消费难题【消费遇纠纷，就上黑猫投诉】......
### [一文解读阿里云短信网关的云原生技术](https://segmentfault.com/a/1190000042535683)
> 概要: 随通信行业不断的业务迭代，新的赛道为业务带来了新变化，生态合作和渠道的规模上量给系统带来了模式创新的同时，也带来了更大的压力。同时，国际站的地域环境和当地政策法规的因素，给全球化的建设也带来了全新的机......
### [腾讯云，DevOps 领导者！](https://segmentfault.com/a/1190000042536595)
> 概要: 刚刚，《IDC MarketScape: 中国DevOps平台市场厂商评估，2022》正式发布。腾讯云CODING成功入选领导者位置在战略和能力两大维度国内领先📃IDC报告指出：腾讯云CODING在一......
### [蜜雪冰城冲刺A股：七成营收来自食材，一季度净利3.9亿元](https://finance.sina.com.cn/tech/internet/2022-09-23/doc-imqqsmrp0153436.shtml)
> 概要: 记者/汪琦雯......
### [Meta涉嫌规避苹果隐私规定遭集体诉讼](https://finance.sina.com.cn/tech/internet/2022-09-23/doc-imqmmtha8405613.shtml)
> 概要: 新浪科技讯 北京时间9月23日早间消息，据报道，在Meta旗下的Facebook和Instagram应用被曝对用户进行跨网站追踪后，已经有两起集体诉讼提交给法院，指控该公司违反苹果的《应用追踪透明度》......
### [疫情后的上海，人口真的流失了么](https://www.huxiu.com/article/669318.html)
> 概要: 来源｜真叫卢俊（ID：zhenjiaolujun0426）作者｜真叫卢俊团队头图｜视觉中国回想上海封控，似乎已经是有点远的事。但是解封后，这几个月一个话题依然被讨论得喋喋不休，没有答案。上海到底因为这......
### [法官批准马斯克：放弃收购推特理由中可增加“吹哨人780万美元补偿”](https://finance.sina.com.cn/tech/internet/2022-09-23/doc-imqmmtha8407777.shtml)
> 概要: 新浪科技讯 北京时间9月23日早间消息，据报道，特斯拉掌门人埃隆·马斯克和推特之间的“诉讼大战”出现了新的转折。之前，推特公司曾经向一名“吹哨人”支付780万美元离职补偿金，法官周四表示，马斯克可以用......
### [MySQL Binlog 组提交实现](https://www.tuicool.com/articles/BnIjEnA)
> 概要: MySQL Binlog 组提交实现
### [上交所换帅](https://www.huxiu.com/article/669160.html)
> 概要: 黄红元推动了科创板的落地和发展，如今深化改革的接力棒，落在证监体系老兵邱勇手中。本文来自微信公众号：读数一帜 （ID：dushuyizhi007），作者：张欣培、郭楠、成梓琢，编辑：杨秀红、陆玲，原文......
### [外媒曝约翰尼·德普新恋情 新女友为其代理律师](https://ent.sina.com.cn/s/u/2022-09-23/doc-imqmmtha8425789.shtml)
> 概要: 新浪娱乐讯 据多家媒体报道，曝出约翰尼·德普新恋情：他正与一名叫做Joelle Rich的英国律师交往。德普与《太阳报》打诽谤官司时，Rich是他的代理律师，在德普与前妻艾梅伯·希尔德的诽谤官司中，R......
### [TV动画《我与机器子》宣传图公开 机器女仆强力股四](https://acg.gamersky.com/news/202209/1521231.shtml)
> 概要: 近日，由日本漫画作家宫崎周平创作的搞笑漫画《我与机器子》的TV动画版特别宣传图公开
### [微博12年创作者「新春限定礼盒」](https://www.zcool.com.cn/work/ZNjIwNDkwMTY=.html)
> 概要: Collect......
### [八仙儿](https://www.zcool.com.cn/work/ZNjIwNTAwOTY=.html)
> 概要: 八仙：铁拐李（李玄）、汉钟离（钟离权）、张果老（张果）、吕洞宾（吕岩）、何仙姑（何琼）、蓝采和（许坚）、韩湘子、曹国舅（曹景休）。八仙也分别代表了男、女、老、幼、富、贵、贫、贱。铁拐李：精专于药理，并......
### [《大富翁11》即将上线PC与NS，Steam预约现已开启](https://www.3dmgame.com/news/202209/3852334.html)
> 概要: 《大富翁11》是由大宇资讯发行的强手棋类休闲游戏，《大富翁》系列游戏历时30余载陪伴了几代人的成长，在全球华人群体中说强手棋，必然想到《大富翁》，现今第十一部作品即将上线，本作将同步上线PC与NS平台......
### [P站美图推荐——红叶特辑（三）](https://news.dmzj.com/article/75657.html)
> 概要: 秋分到了，秋意渐浓，天冷注意添衣哦。
### [紫色幻想](https://www.zcool.com.cn/work/ZNjIwNTE5ODg=.html)
> 概要: Collect......
### [恒大造车官宣量产，许家印貌似领先贾跃亭](https://www.tuicool.com/articles/ueMFZrE)
> 概要: 恒大造车官宣量产，许家印貌似领先贾跃亭
### [动画《在地下城寻求邂逅是否搞错了什么4》后半1月播出](https://news.dmzj.com/article/75660.html)
> 概要: 根据偷跑消息，TV动画《在地下城寻求邂逅是否搞错了什么4 深章 灾厄篇》宣布了将于2023年1月开始播出的消息。
### [《守望先锋：归来》新地图埃斯佩兰萨 10月5日上线](https://www.3dmgame.com/news/202209/3852344.html)
> 概要: 《守望先锋：归来》发布了新机动推进地图“埃斯佩兰萨”的宣传影片，该地图将于10月5日跟随《守望先锋：归来》一同上线，免费游玩。机动推进是《守望先锋：归来》的全新地图类型，地图为对称设计，战斗节奏非常快......
### [财商教育是“财务自由教育”？微淼：理性学习理性投资](http://www.investorscn.com/2022/09/23/103122/)
> 概要: 财务自由是指无需为生活开销而努力赚钱工作的状态。简单来说，当一个人的被动收入等于或者超过其日常开支时，就可以称其已经进入财务自由的状态。这里的被动收入是指不需要花费太多时间和精力就可以自动获得的收入，......
### [投资黄金的年轻人都在用皇御贵金属官方APP](http://www.investorscn.com/2022/09/23/103123/)
> 概要: 投资市场里,谁掌握了第一手资讯,谁就能在投资道路上快人一步。而快人一步就意味着,你较之他人可以提前获取投资先机,有更多布局时间,当别人还在为如何投资没有头绪时,你已经规划清楚,果断出击。尤其是在现货黄......
### [深圳数字人民币应用场景超百万，四方精创积极参与推广](http://www.investorscn.com/2022/09/23/103124/)
> 概要: 近日，中国人民银行深圳市中心支行（以下简称“深圳人民银行”）对外透露，截至8月末，深圳已正式落地数字人民币应用场景105万个，开立数字人民币钱包2671万个，累计交易金额达184亿元......
### [用于 VS Code 的 Edge 开发工具扩展太棒了，因此我放弃了 Chrome](https://www.tuicool.com/articles/N7RvmiB)
> 概要: 用于 VS Code 的 Edge 开发工具扩展太棒了，因此我放弃了 Chrome
### [PE开始抄底餐饮](https://www.tuicool.com/articles/NZjeUj7)
> 概要: PE开始抄底餐饮
### [飞书｜《拒绝低效办公方式》完整版](https://www.zcool.com.cn/work/ZNjIwNTUxMzY=.html)
> 概要: 拒绝低效办公方式，2022去先进团队工作......
### [丰田推出美少女虚拟形象 开朗有气场还是九零后](https://acg.gamersky.com/news/202209/1521410.shtml)
> 概要: Yota酱是丰田B站账号的代言人，设定是开朗有气场，喜欢汽车喜欢二次元，而且还是九零后，是个知性稳重的御姐。
### [《密特罗德 生存恐惧》萨姆斯手办 下周开启预定](https://www.3dmgame.com/news/202209/3852353.html)
> 概要: 《密特罗德 生存恐惧》萨姆斯figma手办将于9月27日开启预购，届时将公布商品详细信息。萨姆斯·阿兰是游戏《密特罗》系列中的主角，电子游戏史上最早的女性英雄形象，宇宙最强的赏金猎人。由于在游戏中萨姆......
### [动画《苍穹之法芙娜BEHIND THE LINE》1月先行上映](https://news.dmzj.com/article/75661.html)
> 概要: 动画《苍穹之法芙娜》系列最新作《苍穹之法芙娜BEHIND THE LINE》宣布了将于2023年1月20日先行上映的消息。
### [星环科技入选2022中国信科独角兽榜单，综合实力备受认可](http://www.investorscn.com/2022/09/23/103133/)
> 概要: 9月22日，由安徽省经济和信息化厅、合肥市人民政府主办的“2022世界制造业大会”平行分论坛“百家独角兽进安徽高峰论坛暨2022中国信科独角兽及新物种榜单颁奖盛典”在合肥召开。论坛发布了“中国信科独角......
### [视频：陈乔恩晒照庆祝出道21周年 同庆恋爱三周年感情事业皆有成](https://video.sina.com.cn/p/ent/2022-09-23/detail-imqmmtha8474015.d.html)
> 概要: 视频：陈乔恩晒照庆祝出道21周年 同庆恋爱三周年感情事业皆有成
### [日本动画占领票房排行 你觉得海贼能进票房前五吗？](https://news.dmzj.com/article/75663.html)
> 概要: 《航海王》系列最新剧场版《红发歌姬》，从今年8月6日开始在日本上映。虽然这部动画在粉丝间的评价有些微妙，但其票房增速令众人无不震惊。
### [多个特大城市全面放开落户](https://www.yicai.com/news/101545264.html)
> 概要: 在济南、昆明、大连之后，又有一个特大城市全面放开落户。
### [再补 1000 元大额券：骆驼户外徒步鞋 179 元起官旗狂促](https://lapin.ithome.com/html/digi/642859.htm)
> 概要: 天猫【骆驼官方旗舰店】骆驼户外徒步鞋 23 款日常定价 1179~1199 元（很有骆驼的风格），今日再补 1000 元大额券，实付 179~199 元包邮：天猫骆驼 户外登山鞋 24 款可选线上线下......
### [9月23日这些公告有看头](https://www.yicai.com/news/101545281.html)
> 概要: 9月23日晚间，多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [华兴资本集团全面落地江阴,助力经济高质量发展](http://www.investorscn.com/2022/09/23/103137/)
> 概要: 9月21日,江苏省政府投资基金园区行——江阴站暨江阴市2022年度投资峰会在江阴黄嘉喜来登酒店黄嘉宴会厅举行。华兴资本集团董事长、华兴新经济基金创始合伙人兼首席投资官包凡受邀参加此次峰会,同时带领华兴......
### [通用接大额订单：将向租车巨头赫兹出售17.5万辆电动汽车](https://www.yicai.com/video/101545350.html)
> 概要: 通用接大额订单：将向租车巨头赫兹出售17.5万辆电动汽车
### [组图：余景天图书馆自习被网友偶遇 认真学习讨论好自律](http://slide.ent.sina.com.cn/star/slide_4_704_375609.html)
> 概要: 组图：余景天图书馆自习被网友偶遇 认真学习讨论好自律
### [上海第三轮土拍开启，首日揽金超680亿](https://www.yicai.com/news/101545337.html)
> 概要: “老面孔”保利、华发大手笔拿地，“新面孔”积极试探布局。
### [高能电玩节：卡通风格合作《同舟共济》预告](https://www.3dmgame.com/news/202209/3852382.html)
> 概要: 于今天（9 月 23 日）举办的 Bilibili高能电玩节上，由 Fika Productions 开发 Team 17 发行的卡通风格合作肉鸽类游戏《同舟共济》发布了全新预告。游戏将于 11 月 ......
### [若 iPhone 14 Pro / Max 一直销售强劲，苹果将牢牢占据高端智能手机 60% 的份额](https://www.ithome.com/0/642/880.htm)
> 概要: IT之家9 月 23 日消息，如果消费者的热情反应持续到今年底，iPhone 14 Pro 机型的强劲销售可能会使苹果将其高端手机的市场份额稳定占据 60%。据 DigiTimes 报道，今年到目前为......
### [槟榔这事，是要砸掉数百万人的饭碗，还是送更多人的命？](https://www.huxiu.com/article/669823.html)
> 概要: 出品｜虎嗅医疗组作者｜陈广晶题图｜视觉中国槟榔又在舆论场掀起了新一轮交锋。9月10日，36岁的歌手傅松因口腔癌离世，生前曾多次表示，患病是因为嚼槟榔，并告诫网友“远离槟榔”。此事一度冲上微博热搜。9月......
### [你喝的水里，这些有害物质永远不会消失](https://www.ithome.com/0/642/881.htm)
> 概要: 细菌吃不了，火烧不掉，埋进地里只会污染土壤…… 这类物质到底要怎么处理？1938 年，一位名叫罗伊・布朗克（Roy J. Plunkett）的化学家，在新泽西州的一间实验室研究用于冰箱的制冷剂，却意外......
### [高能电玩节：独立游戏《觉醒异刃》发布中文预告](https://www.3dmgame.com/news/202209/3852364.html)
> 概要: 在今天（9 月 23 日）举办的 Bilibili高能电玩节上，由黑鸽游戏独立开发的横板动作游戏《觉醒异刃》发布了中文预告。游戏预告：游戏暂未公布发售时间，Steam页面已经上线。Steam商店页面：......
### [脱口秀大会5：思文程璐离婚梗，谁更好？](https://new.qq.com/rain/a/20220923V04YRQ00)
> 概要: 脱口秀大会5：思文程璐离婚梗，谁更好？
### [银行板块逆势上扬，城农商行股更受青睐](https://www.yicai.com/news/101545458.html)
> 概要: 城农商行在平均总资产收益率上表现的更为稳定
### [苹果 Apple Watch Ultra / AirPods Pro 2 已支持 Apple Store Pickup 到店取货](https://www.ithome.com/0/642/896.htm)
> 概要: IT之家9 月 23 日消息，苹果 Apple Watch Ultra 订单开始送达世界各地的客户手中。对于忘记预订 Apple Watch Ultra 或只是决定等待的客户，现在估计许多配置将在 3......
### [​ 攻略心得来吧！湮灭在美梦里！海月入门级攻略教学](https://bbs.hupu.com/55645309.html)
> 概要: 前言：大家好，这里是立志陪你练会每一个英雄的秋白~欢迎来到《顶尖教学》第七期—海月教学。前排提醒，本期教学是由攻略视频的文案改编，视频版指路【https://www.bilibili.com/vide
### [流言板骑士记者：奥科罗应该纳入首发讨论范畴，因为努力和强硬](https://bbs.hupu.com/55645840.html)
> 概要: 虎扑09月23日讯 骑士记者Kelsey Russo近日撰文谈到了球队后卫艾萨克-奥科罗。“奥科罗出任首发侧翼，这在讨论范畴内，因为他为球队带来的努力和强硬。”Kelsey Russo写道。2021-
### [央行报告：人民币国际化各项指标总体向好](https://finance.sina.com.cn/jjxw/2022-09-23/doc-imqmmtha8519082.shtml)
> 概要: 原标题：中国央行报告：人民币国际化各项指标总体向好 中新社北京9月23日电 （记者 夏宾）中国央行23日发布的《2022年人民币国际化报告》称...
### [流言板三镇梯队日籍教练：会教队员别假摔卧草，青训不可急功近利](https://bbs.hupu.com/55645893.html)
> 概要: 虎扑09月23日讯 近日，武汉三镇U17梯队日本籍主帅前田浩二接受《体坛周报》专访时表示他会要求小球员们不要去假摔和“卧草”，因为这既不尊重裁判和对手，也是对观众的不负责任。“立刻站起来，不要躺！”一
### [长三角三省一市将建知识产权大数据中心 实现数据全链条共享](https://finance.sina.com.cn/jjxw/2022-09-23/doc-imqmmtha8519091.shtml)
> 概要: 中新网上海9月23日电 （记者 陈静）长三角地区知识产权更高质量一体化发展论坛23日在上海举行。 记者从论坛上了解到， 三省一市知识产权部门将探索深化新领域知识产权合作...
### [阳澄湖大闸蟹今日开捕，蟹农：上市虽迟但不影响品质](https://finance.sina.com.cn/china/dfjj/2022-09-23/doc-imqqsmrp0266088.shtml)
> 概要: 原标题：阳澄湖大闸蟹今日开捕，蟹农：上市虽迟但不影响品质 蟹农徐皓刚从水里捞出第一篓蟹，就看到数十只大小不一的蟹顺着网口呼呼啦啦往外跑开。 9月23日，时至秋分。
### [海南自贸港-非洲国际远洋集装箱干线正式开通](https://finance.sina.com.cn/china/gncj/2022-09-23/doc-imqmmtha8519662.shtml)
> 概要: 9月23日上午，海南自由贸易港至非洲国际远洋集装箱干线在洋浦国际集装箱码头正式开通运营，这是自2020年首条远洋洲际航线开通以来...
### [流言板英足总官方：将正式对C罗摔碎球迷手机及暴力行为提起指控](https://bbs.hupu.com/55646074.html)
> 概要: 虎扑09月23日讯 英足总发布公告，将正式对此前C罗在代表曼联对阵埃弗顿比赛后摔打小球迷的手机以及后续暴力行为提起指控。具体公告如下：早在今年4月9日曼联对阵埃弗顿的比赛后，C罗在离场时摔坏了一名小球
### [合肥第三批供地：21宗地收金204.5亿，万科、龙湖落子](https://finance.sina.com.cn/jjxw/2022-09-23/doc-imqmmtha8521414.shtml)
> 概要: 原标题：合肥第三批供地：21宗地收金204.5亿，万科、龙湖落子 新京报讯 （记者徐倩）9月23日，合肥第三批集中供地落幕。据中指研究院统计，此次集中供地中...
# 小说
### [苍莽仙途](https://book.zongheng.com/book/1074299.html)
> 作者：虚如风

> 标签：武侠仙侠

> 简介：小小少年背负期望拜入仙门，岂知一入山门深似海。天赋一般，资质一般，如何在这漫漫仙路前行？

> 章节末：第三二一章   终极（三）

> 状态：完本
# 论文
### [Induced Natural Language Rationales and Interleaved Markup Tokens Enable Extrapolation in Large Language Models | Papers With Code](https://paperswithcode.com/paper/induced-natural-language-rationales-and)
> 日期：24 Aug 2022

> 标签：None

> 代码：https://github.com/mirelleb/induced-rationales-markup-tokens

> 描述：The ability to extrapolate, i.e., to make predictions on sequences that are longer than those presented as training examples, is a challenging problem for current deep learning models. Recent work shows that this limitation persists in state-of-the-art Transformer-based models. Most solutions to this problem use specific architectures or training methods that do not generalize to other tasks. We demonstrate that large language models can succeed in extrapolation without modifying their architecture or training procedure. Experimental results show that generating step-by-step rationales and introducing marker tokens are both required for effective extrapolation. First, we induce it to produce step-by-step rationales before outputting the answer to effectively communicate the task to the model. However, as sequences become longer, we find that current models struggle to keep track of token positions. To address this issue, we interleave output tokens with markup tokens that act as explicit positional and counting symbols. Our findings show how these two complementary approaches enable remarkable sequence extrapolation and highlight a limitation of current architectures to effectively generalize without explicit surface form guidance. Code available at https://github.com/MirelleB/induced-rationales-markup-tokens
### [Safe Model-Based Reinforcement Learning Using Robust Control Barrier Functions | Papers With Code](https://paperswithcode.com/paper/safe-model-based-reinforcement-learning-using)
> 概要: Reinforcement Learning (RL) is effective in many scenarios. However, it typically requires the exploration of a sufficiently large number of state-action pairs, some of which may be unsafe.