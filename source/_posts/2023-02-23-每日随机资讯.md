---
title: 2023-02-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BabblingBrook_ZH-CN9371346787_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-23 21:49:01
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BabblingBrook_ZH-CN9371346787_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [VR设备成吃灰神器？塞满二手平台，有人仅用两次就闲置](https://www.woshipm.com/it/5762776.html)
> 概要: 在元宇宙这个概念火起来了之后，与之相关的VR设备销量也一定程度上被带动了起来。不过不少用户却表示VR设备“容易吃灰”，有些人甚至已经在二手平台上甩卖先前购入的VR头显。那么，VR设备为何会成为“吃灰”......
### [Midjourney：AIGC现象级应用，一年实现1000万用户和1亿美元营收](https://www.woshipm.com/it/5761936.html)
> 概要: 去年9月23日，美国红杉在官网上发布了一篇报告《生成式AI：一个创造性的新世界》，文章插图全部在Midjourney上生成。同月，在美国科罗拉多州博览会艺术比赛上，一名没有任何绘画基础的参赛者通过Mi......
### [运用AIGC人工智能生产内容](https://www.woshipm.com/pd/5763008.html)
> 概要: 随着互联网行业发展和技术的进步，设计师也有越来越丰富的手段来应对多种类型的产品需求，最近火热的AI绘图，也时常出现在设计师的讨论话题中。那么，如何运用AI来协助商业设计呢？一起来学习一下吧。一、AI绘......
### [你买的国产车，可能都是外国人设计的](https://www.huxiu.com/article/800685.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博编辑｜周到头图｜《中国合伙人》提到中国汽车设计，你的第一反应是什么？是“皮尺式测量”+“像素级抄袭”，最后攒成一个“几何式丢人”的四不像超级缝合怪？是明面上打着“致敬”的幌......
### [智己汽车难圆上汽高端梦：大量盲订被转让，靠注水数据挽救销量？](https://finance.sina.com.cn/tech/it/2023-02-23/doc-imyhruta8566535.shtml)
> 概要: 文 | 新浪科技 张俊......
### [日均30笔，单笔2.7亿，种子融资减少了2072笔｜2022年中国投融资年报](https://36kr.com/p/2114274335164547)
> 概要: 日均30笔，单笔2.7亿，种子融资减少了2072笔｜2022年中国投融资年报-36氪
### [马斯克再解雇数十名Twitter员工：此前要求一周时间扭转广告业务](https://finance.sina.com.cn/tech/internet/2023-02-23/doc-imyhruta8604552.shtml)
> 概要: 新浪科技讯 北京时间2月23日早间消息，据报道，埃隆·马斯克上周解雇了数十名Twitter员工，其中包括那些从事广告产品工作的人，此前马斯克给他们一周的最后期限来扭转公司的广告业务......
### [36氪首发 | “新势力”电动自行车品牌「VELOTRIC」完成A轮融资，推出高性价比产品](https://36kr.com/p/2142982091997702)
> 概要: 36氪首发 | “新势力”电动自行车品牌「VELOTRIC」完成A轮融资，推出高性价比产品-36氪
### [36氪独家 | 中国电商净增超70%后，沃尔玛电商彻底取消GMV门槛](https://36kr.com/p/2143178004761088)
> 概要: 36氪独家 | 中国电商净增超70%后，沃尔玛电商彻底取消GMV门槛-36氪
### [雅诗兰黛等大牌集结 京东新百货美妆2.28超级品类日预售开启](http://www.investorscn.com/2023/02/23/105873/)
> 概要: 2月21日，京东新百货“美妆2.28超级品类日”预售正式开启。作为2023年的首个美妆大促活动，京东新百货携手雅诗兰黛、赫莲娜、海蓝之谜、珀莱雅等国内外品牌及商家一起，带来全品类、超万款美妆商品，涵盖......
### [《五星物语》帝骑黄金骑士 天照最强座驾酷炫极致](https://www.3dmgame.com/news/202302/3863328.html)
> 概要: 机械美学大师、传说级漫画大师永野护的代表作、神漫《五星物语》中的机甲各个造型酷炫美形，日前一款帝骑黄金骑士高端模型公开，贵为主角天照的最强座驾酷炫霸气极致，价格不菲。·原本天照的座驾·初代黄金骑士就在......
### [高端电器发展新十年，红星美凯龙携手行业领袖奏响高增长最强音](http://www.investorscn.com/2023/02/23/105876/)
> 概要: “中国经济的魅力来自于不断创新升级。中国电器行业的未来十年，是潮向高端化发展的十年，是以场景创新、模式创新、生态创新创造新红利，潮向高增长的新十年。”红星美凯龙家居集团执行总裁兼大营运中心总经理朱家桂......
### [TV动画《地下忍者》宣布秋季开播](https://news.dmzj.com/article/77239.html)
> 概要: 根据花泽健吾原作制作的TV动画《地下忍者》宣布了将于10月开始播出的消息。本周的宣传图也一并公开。
### [蚁人3：如同AI的剧本，依旧炫目的特效，勉强及格的电影](https://news.dmzj.com/article/77241.html)
> 概要: 漫威可能真的完了……
### [BBI&雷报发布《2023中国动漫出海前瞻报告》](https://acg.gamersky.com/news/202302/1570167.shtml)
> 概要: 由中国传媒大学经济与管理学院商务品牌战略研究所与雷报牵头，联合OneSight一网互通和广大大SocialPeta，共同发布《2023中国动漫出海前瞻报告》。
### [权威数据公布：TCL电视全球销量跃升至全球第二，居中国第一](http://www.investorscn.com/2023/02/23/105879/)
> 概要: 2月22日，权威市场调查机构Omdia公布了2022年全球电视销量数据，TCL电视2022年全年销量市占率提升0.2个百分点至11.7%，排名超越LG，跃升至全球第二。TCL电视的市占率自2019年至......
### [《原神》迪希雅角色PV——「沙际晨光」](https://www.3dmgame.com/news/202302/3863343.html)
> 概要: 今日（2月23日），原神官方公布《原神》迪希雅角色PV——「沙际晨光」，「墙壁的本意不是阻挡，而是保护。」烁金的赤阳高高升起，穿过漠漠沙原与层层峭壁，将落下希冀的晨光。宣传片：CV：迪希雅——陈雨视频......
### [顺成集团携手广域铭岛，共同推动煤化工行业数字化节能降碳](http://www.investorscn.com/2023/02/23/105882/)
> 概要: 2月21日，由吉利控股集团和河南省顺成集团共同投资的全球首个十万吨级绿色低碳甲醇工厂在安阳正式投产，这是我国首套、全球规模最大的二氧化碳加氢制绿色低碳甲醇工厂，为中国能源多样化战略点燃了一座新的灯塔，......
### [2023年，咖啡都跌到5元以内了](https://www.huxiu.com/article/802594.html)
> 概要: 燃次元（ID:chaintruth）原创，作者：张琳，编辑：曹杨，头图来自：视觉中国很难想象，2023年咖啡降价的“第一枪”，是新式茶饮品牌打响的。今年春节刚过，CoCo都可（以下简称CoCo）宣布全......
### [NCSOFT新MMO《王权与自由》将由亚马逊负责发行](https://www.3dmgame.com/news/202302/3863357.html)
> 概要: 亚马逊游戏部门 Amazon Games 日前宣布，已与韩国游戏开发商 NCSOFT 达成协议，将在北美、南美、欧洲和日本发行 MMORPG 新作《王权与自由（Throneand Liberty）》，......
### [美剧主角穿鞋上沙发，急坏了多少中国爹妈](https://www.huxiu.com/article/802580.html)
> 概要: 本文来自微信公众号：跳海大院 （ID：meerjump），作者：野汉，责编：大彪子，头图来自：《老友记》（温馨提示：本文配图易引起洁癖人士不适）出于对爸妈精神生活的关心和对抗日神剧的嫌弃，每次放假回家......
### [庄家最怕散户知道的2个“逃顶”秘笈](https://www.yicai.com/news/101683344.html)
> 概要: 一般来说，当快线自下向上交叉慢线时为金叉，视为买入信号，尤其是快线由0轴以下上穿过慢线时，买入信号更加强烈；当快线自上向下交叉慢线时为死叉，视为卖出信号，尤其是快线由0轴以上向下穿过慢线时，更应该考虑卖出。
### [英诺10亿新募资首轮关账，2023的早期投资越来越卷](https://36kr.com/p/2144152715774465)
> 概要: 英诺10亿新募资首轮关账，2023的早期投资越来越卷-36氪
### [三星 Galaxy S22 系列、Z Fold4 / Flip4 折叠屏国行开始推送安卓 13 / One UI 5.1 正式版](https://www.ithome.com/0/675/356.htm)
> 概要: 感谢IT之家网友末6_、李云龙师长、Vandy、ywok、许哥Geg的线索投递！IT之家2 月 23 日消息，据多名IT之家网友反馈，三星 Galaxy S22 系列，包括 S22、S22+、S22 ......
### [电锯人动画将有重大发表 JUMP两部漫画接连腰斩](https://news.dmzj.com/article/77247.html)
> 概要: 电锯人动画将有重大发表、JUMP两部漫画接连腰斩、约稿平台Skeb将引入AI审核
### [《和平精英》即将推出开放世界玩法“绿洲世界”，可赛车、钓鱼、旅游……](https://www.ithome.com/0/675/358.htm)
> 概要: IT之家2 月 23 日消息，据和平精英官方消息，全新的共建式开放世界玩法“绿洲世界”即将上线，拥有 24 小时昼夜变化，还能赛车、钓鱼、旅游……预告视频：官方介绍：这是一个全新世界，也是一次全新冒险......
### [视频：好养眼！许光汉宋雨琦程潇同框看秀状态佳](https://video.sina.com.cn/p/ent/2023-02-23/detail-imyhsrwu9881122.d.html)
> 概要: 视频：好养眼！许光汉宋雨琦程潇同框看秀状态佳
### [视频：肖战米兰看秀造型大片释出 露迷人笑眼帅气又温柔](https://video.sina.com.cn/p/ent/2023-02-23/detail-imyhsrwp3261213.d.html)
> 概要: 视频：肖战米兰看秀造型大片释出 露迷人笑眼帅气又温柔
### [视频：李钟硕表白女友IU让自己更成熟：她是依靠和鼓励](https://video.sina.com.cn/p/ent/2023-02-23/detail-imyhsrwr1810467.d.html)
> 概要: 视频：李钟硕表白女友IU让自己更成熟：她是依靠和鼓励
### [网易庆《明日之后》累计玩家达 2 亿，将抽奖送出一套房](https://www.ithome.com/0/675/363.htm)
> 概要: IT之家2 月 23 日消息，网易今日举行《明日之后》累计玩家达 2 亿的庆祝活动，除了在游戏中送出福利之外，官方还宣布送出现实中的一套房，地点位于有着“塞外江南”之称的牡丹江，可折现 5 万元。网易......
### [宝可梦温情短片《同梦之旅》公开 2月28日放送](https://acg.gamersky.com/news/202302/1570360.shtml)
> 概要: 今日（2月23日），Pokemon宝可梦官方宣布将于2月28日公开宝可梦温情短片《同梦之旅》，敬请期待。
### [Phat Company《赛马娘》双涡轮手办即将开始预订](https://news.dmzj.com/article/77248.html)
> 概要: Phat Company根据《赛马娘》中的双涡轮制作的1/7比例手办手办即将于2月28日开始预订。
### [华体会（HTH）正式签约沃尔夫斯堡俱乐部，迈向合作新进程！](http://www.investorscn.com/2023/02/23/105889/)
> 概要: 近日，数字体育巨头华体会（HTH）官宣了一个重大消息，华体会（HTH）与德甲劲旅沃尔夫斯堡达成官方合作协议，成为球队的赞助商。同时双方将在数字体育领域以及赛事资讯方面展开全方位的深度合作，形成合作共赢......
### [游戏复苏 黄仁勋：玩家热切拥抱RTX40显卡](https://www.3dmgame.com/news/202302/3863379.html)
> 概要: 今天NVIDIA发布了截至1月29日的2023财年Q4财报，营收60.5亿美元，同比下滑21%，但好于预期的60.2亿美元，净利润14.1亿美元，同比下滑53%。不过财报发布之后，NVIDIA股价大涨......
### [商务部：有数十家跨国公司总部与商务部对接来华商务考察](https://www.yicai.com/news/101683730.html)
> 概要: 2023年1月，全国实际使用外资金额同比增长14.5%。
### [中办 国办印发《关于进一步深化改革促进乡村医疗卫生体系健康发展的意见》(全文)](https://finance.sina.com.cn/jjxw/2023-02-23/doc-imyhsweq8572735.shtml)
> 概要: 中共中央办公厅 国务院办公厅印发《关于进一步深化改革促进乡村医疗卫生体系健康发展的意见》 新华社北京2月23日电 近日，中共中央办公厅...
### [全球第三例治愈者出现，人类能杀死艾滋“巨兽”吗？](https://www.huxiu.com/article/802971.html)
> 概要: 出品丨虎嗅科技组作者丨苏北佛楼蜜编辑丨陈伊凡题图丨电影《达拉斯买家俱乐部》艾滋病无法治愈的阴影像个蛰伏于暗处准备随时出击的史前巨兽，令人恐惧。如今，德国一名53岁的男子似乎成功杀死了它，他成为了第三个......
### [配给制重现！西红柿黄瓜都很难买到，英国超市这样应对短缺](https://www.yicai.com/news/101683732.html)
> 概要: 英国最大超市乐购已暂时将每位顾客能购买的西红柿、辣椒和黄瓜包装数量限制为三包。
### [热榜！企业月薪4万招人去非洲养鸡，公司回应：工资真实但环境艰苦，不招20岁以下年轻人](https://finance.sina.com.cn/jjxw/2023-02-23/doc-imyhswep1821128.shtml)
> 概要: 来源：每日经济新闻 2月23日，山东青岛一企业称月薪3-4万招聘养鸡场场长，工作地点在非洲坦桑尼亚。据其招聘需求显示，任职需要具备丰富的蛋种或肉种鸡饲养管理经验...
### [调查显示：OpenAI ChatGPT 将导致 26% 的欧洲软件和科技公司计划裁员](https://www.ithome.com/0/675/410.htm)
> 概要: 2 月 23 日消息，据外媒报道，人工智能可能真会抢走人类的饭碗！Sortlist Data Hub 的一项新调查显示，ChatGPT 将直接导致 26% 的欧洲软件和科技公司计划裁员，其次是金融公司......
### [“毁灭台湾计划”风波未平，美台官员密会7小时……](https://finance.sina.com.cn/china/2023-02-23/doc-imyhsweq8657532.shtml)
> 概要: 作者：谢开华 马晗雁 来源：参考消息 美台勾连又有新的“小动作”！往年一般都遮遮掩掩搞的所谓“美台高层国安对话”，今年居然“不避媒体镜头”...
### [市场震荡整理 多空博弈将持续多久？](https://www.yicai.com/video/101683834.html)
> 概要: 市场震荡整理 多空博弈将持续多久？
### [接近4万！《原子之心》Steam在线峰值38469人](https://www.3dmgame.com/news/202302/3863383.html)
> 概要: 据SteamDB统计，《原子之心》Steam在线峰值为38469人，接近4万，当前在线玩家为28178人。对于一个2A单机游戏来说，这个在线还算能接受。另外，截止到目前，SteamDB还显示《原子之心......
### [多场景首单落地！数字人民币试点成绩单出炉](https://finance.sina.com.cn/wm/2023-02-23/doc-imyhswep1882896.shtml)
> 概要: 来源：中国证券报 近日，全国首笔数字人民币跨境消费在深圳福田口岸落地，全国首个实现试点银行全渠道数字人民币免密支付的公交场景在义乌落地。
### [千人挤爆现场！这场春季投资策略会火了](https://finance.sina.com.cn/wm/2023-02-23/doc-imyhswep1882861.shtml)
> 概要: 来源：中国证券报 “加快建设现代化产业体系为资本市场投资指明方向”“我国经济增长回归常态具有扎实基础”“春季行情中还有价值和周期占优的阶段”“下半年机会少于上半年”“全年...
### [国资委部署今年改革发展新任务，加大集成电路等关键领域科技投入](https://www.yicai.com/news/101683869.html)
> 概要: 加大对传统制造业改造、战略性新兴产业，也包括对集成电路、工业母机等关键领域的科技投入，提升基础研究和应用基础研究的能力。
### [詹姆斯训练师晒全明星赛前治疗花絮：期待回归工作](https://bbs.hupu.com/58188200.html)
> 概要: 来源：  虎扑
### [情报站LEC官推：前FNC打野选手Broxah将做客LEC总决赛周末的分析台](https://bbs.hupu.com/58188338.html)
> 概要: 虎扑02月23日讯 LEC官推发布LEC冬季赛决赛分析台嘉宾，以下是推特译文（大意）：前FNC打野选手Broxah将做客LEC总决赛周末的分析台！LEC解说Medic也在评论区发布了Broxah的表情
### [流言板朱俊龙全场投篮3中1，罚球2中2，得到4分3篮板](https://bbs.hupu.com/58188377.html)
> 概要: 虎扑02月23日讯 男篮世界杯预选赛亚大区第六阶段比赛，中国男篮全场71-59战胜哈萨克斯坦男篮。朱俊龙全场出场10分钟，投篮3中1，罚球2中2，得到4分3篮板。   来源： 虎扑
### [流言板郭艾伦投篮8中2，全场得到9分2篮板3抢断](https://bbs.hupu.com/58188386.html)
> 概要: 虎扑02月23日讯 世预赛第6窗口期，中国男篮71-59战胜哈萨克斯坦男篮。此役中国男篮队员郭艾伦出场18分钟，投篮8中2，其中三分1中0，罚球7中5，得到9分2篮板3抢断。   来源： 虎扑
# 小说
### [佛缘江湖](http://book.zongheng.com/book/1189255.html)
> 作者：龙城枫

> 标签：武侠仙侠

> 简介：有道是大树底下好乘凉。本以为靠着罗汉堂首座师兄了空，作为师弟的了能可以在少室寺里吃喝等死一辈子。岂料世事无常，慧定方丈一死，了空争夺未来方丈之位失败，作为师弟的了能也遭受到了波及，成为少室寺的弃徒，只能离开空门，进入尘世。了能变廖能，心却没变，誓救师兄，再续佛缘。可惜，往事已矣，他却只剩佛缘。

> 章节末：298，最终一击

> 状态：完本
# 论文
### [DOMINO: Domain-aware Loss for Deep Learning Calibration | Papers With Code](https://paperswithcode.com/paper/domino-domain-aware-loss-for-deep-learning)
> 日期：10 Feb 2023

> 标签：None

> 代码：https://github.com/lab-smile/domino

> 描述：Deep learning has achieved the state-of-the-art performance across medical imaging tasks; however, model calibration is often not considered. Uncalibrated models are potentially dangerous in high-risk applications since the user does not know when they will fail. Therefore, this paper proposes a novel domain-aware loss function to calibrate deep learning models. The proposed loss function applies a class-wise penalty based on the similarity between classes within a given target domain. Thus, the approach improves the calibration while also ensuring that the model makes less risky errors even when incorrect. The code for this software is available at https://github.com/lab-smile/DOMINO.
### [Attending to Graph Transformers | Papers With Code](https://paperswithcode.com/paper/attending-to-graph-transformers)
> 日期：8 Feb 2023

> 标签：None

> 代码：https://github.com/luis-mueller/probing-graph-transformers

> 描述：Recently, transformer architectures for graphs emerged as an alternative to established techniques for machine learning with graphs, such as graph neural networks. So far, they have shown promising empirical results, e.g., on molecular prediction datasets, often attributed to their ability to circumvent graph neural networks' shortcomings, such as over-smoothing and over-squashing. Here, we derive a taxonomy of graph transformer architectures, bringing some order to this emerging field. We overview their theoretical properties, survey structural and positional encodings, and discuss extensions for important graph classes, e.g., 3D molecular graphs. Empirically, we probe how well graph transformers can recover various graph properties, how well they can deal with heterophilic graphs, and to what extent they prevent over-squashing. Further, we outline open challenges and research direction to stimulate future work. Our code is available at https://github.com/luis-mueller/probing-graph-transformers.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
