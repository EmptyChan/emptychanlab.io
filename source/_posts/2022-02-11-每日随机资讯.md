---
title: 2022-02-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TeaGardensMunnar_ZH-CN9587720369_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-11 22:44:48
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TeaGardensMunnar_ZH-CN9587720369_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [LLVM 开始支持 LoongArch CPU](https://www.oschina.net/news/181946/loongarch-llvm-landing)
> 概要: LLVM 15.0 开发树已出现支持 LoongArch CPU 架构的初始补丁。LoongArch是龙芯打造的自主指令系统架构，LoongArch包括基础架构部分和向量指令、虚拟化、二进制翻译等扩展......
### [OpenArkCompiler 1.0 正式发布，方舟编译器的开源项目](https://www.oschina.net/news/181936/openarkcompiler-1-0-0-released)
> 概要: OpenArkCompiler 1.0 版本已正式发布。OpenArkCompiler 是来自华为方舟编译器的开源项目，它具备的四个技术特点能够将不同语言代码编译成一套可执行文件，在运行环境中高效执行......
### [COBOL 代码行数超 8000 亿，应用现代化是首选发展道路](https://www.oschina.net/news/181943/microfocus-cobol-market-research-2022)
> 概要: Micro Focus 最新进行的一项针对 COBOL 编程语言的研究表明，全球 COBOL 应用程序的足迹继续增长，大多数受访者打算在 2022 年年底前实现应用现代化改造并支持云。92% 的受访者......
### [Google 发布 Android 13 首个开发者预览版](https://www.oschina.net/news/181941/android-13-dp-1)
> 概要: 虽然 Google 在去年 10 月才推出Android 12的正式版，但他们已经开始为下一个版本迭代做好了准备，并于今天发布了 Android 13 的第一个开发者预览版。这是一个尚处于非常早期的版......
### [这个万亿新赛道，会是陆正耀的救命稻草么？](https://www.huxiu.com/article/497025.html)
> 概要: 出品 | 妙投APP作者 | 李昱佳编辑 | 关雪菁头图 | 东方IC陆正耀回来了。作为瑞幸创始人的他，带着预制菜又打回来了。去年8月，趁着小面的风口，陆正耀打造的趣小面开张，计划在全国开出500家门......
### [《生化危机4：重制版》更恐怖 艾达王出场时间更多](https://www.3dmgame.com/news/202202/3835493.html)
> 概要: 最近外媒Fanbyte透露了《生化危机4：重制版》新细节，这款重制版将更加恐怖诡异，而艾达王出场时间更多了。Fanbyte编辑Imran Khan表示，卡普空并不打算将《生化危机4》进行“严格的逐个场......
### [《一拳超人》重制版203话：共鸣与增幅 敌人也不错](https://acg.gamersky.com/news/202202/1459183.shtml)
> 概要: 《一拳超人》重制版203话公开了，本集中饿狼和金属球棒联手对付大怪虫蜈蚣仙人，暂时化敌为友，全篇都是精彩的战斗。
### [最强幕后解密！104s带你回顾水门桥背后的X小时](https://new.qq.com/rain/a/20220211V0037500)
> 概要: 最强幕后解密！104s带你回顾水门桥背后的X小时
### [远藤达哉「间谍过家家」新绘公开](http://acg.178.com/202202/438548389406.html)
> 概要: 近日，「间谍过家家」原作者远藤达哉公开了其绘制的新绘图，绘制的人物为阿尼亚·福杰，本作预计将于2022年正式播出。「间谍过家家」是远藤达哉创作的漫画作品，讲述了一名男性特工与女性暗杀者结婚，领养了一个......
### [P站美图推荐——自助洗衣房特辑](https://news.dmzj.com/article/73563.html)
> 概要: 自助洗衣店总是会有一种异样的时尚都市感，因此常常被用于休闲帅气风格的插画背景之中
### [本届冬奥官方语言是东北话！香港选手开口也是一股大碴子味](https://new.qq.com/rain/a/20220211V03GQ900)
> 概要: 本届冬奥官方语言是东北话！香港选手开口也是一股大碴子味
### [Team17员工：取消NFT仅因公众抵制 员工反对无作用](https://www.3dmgame.com/news/202202/3835514.html)
> 概要: 游戏发行商 Team17 在今年早些时候试图加入 NFT 的潮流，但遭到了玩家和合作开发工作室在内所有人的抵制，并在第二天决定取消该计划。但故事并没有到此为止。Team17 与 NFT 合作的后果仍在......
### [轻小说「姫騎士様のヒモ」发售贺图公开](http://acg.178.com/202202/438550132026.html)
> 概要: 昨日（2月10日），轻小说「姫騎士様のヒモ」正式发售，该作插画师マシマサキ公开了其绘制的发售贺图。「姫騎士様のヒモ」是由白金透创作，マシマ サキ负责插画的轻小说作品......
### [本乡奏多演晨间剧获好评 演得太坏被赞演技太妙](https://ent.sina.com.cn/2022-02-11/doc-ikyamrna0190021.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 人气男星本乡奏多出演2月11日播出的NHK晨间剧《Come Come Everybody》第72集，和前一天播出的剧情一样，他继续对川荣李奈饰演的女主日向态度恶劣，让观众......
### [《大王饶命》高燃收官！原声大碟今日乘风发行！](https://new.qq.com/omn/ACF20220/ACF2022021100513500.html)
> 概要: 大魔王「吕怼怼」一期课程今天结课啦！动画《大王饶命》第一季今日完结，同时原声大碟也于今日正式发行！本张OST共收录16首动画BGM，来QQ音乐、酷狗音乐、酷我音乐三大音乐平台，搜索专辑名《浪费您宝贵的......
### [SpaceX星舰新宣传片 画面超震撼！将建造火星城](https://www.3dmgame.com/news/202202/3835527.html)
> 概要: 今日(2月11日)SpaceX太空探索公司召开发布会，并发布了星舰新宣传片，画面非常震撼。场景包括在地球轨道加注燃料，降落抵达火星等。新宣传片：SpaceX将实现地球轨道加注燃料，并抵达火星降落，在火......
### [「卡特队长」第一期正式封面及刊物信息公开](http://acg.178.com/202202/438559831924.html)
> 概要: 近日，漫威漫画官方公开了全新5期迷你系列「卡特队长」的刊物信息，本刊由2012年「惊奇队长」形象的创作者Jamie McKelvie负责剧情编写及人物形象设计，画师Marika Cresta负责绘制......
### [微软在监管之前采纳新的应用商店原则](https://www.3dmgame.com/news/202202/3835530.html)
> 概要: 作者：微软总裁兼副董事长 Brad Smith玩家通过 Xbox Cloud Gaming(beta 版本)在 Surface Go 3 上玩《盗贼之海》本周，我们宣布了一套新的“开放应用商店原则”，......
### [轻小说《龙与虎》公开15周年纪念PV](https://news.dmzj.com/article/73566.html)
> 概要: 轻小说《龙与虎》公开了15周年纪念PV。在这段PV中，可以听到当年给TV动画配音的钉宫理惠等声优，新录制的角色台词。
### [预约看2022中央广播电视总台元宵晚会](https://new.qq.com/rain/a/ENT2022021100585800)
> 概要: 预约看2022中央广播电视总台元宵晚会
### [预约看2022江苏/安徽/山东卫视元宵晚会](https://new.qq.com/rain/a/ENT2022021100593700)
> 概要: 预约看2022江苏/安徽/山东卫视元宵晚会
### [《RM》HAHA公司否认其确诊新冠：还在等最终结果](https://ent.sina.com.cn/s/j/2022-02-11/doc-ikyakumy5404847.shtml)
> 概要: 新浪娱乐讯 11日，韩国艺人HAHA（河东勋）所属公司相关人士表示：“HAHA的PCR检查结果还没有出来，新冠病毒阳性的报道是误报。”　　今日上午有韩国媒体报道《Running Man》主持人HAHA......
### [索尼公布VR游戏《莫斯2》详细信息 2022年春季发售](https://www.3dmgame.com/news/202202/3835538.html)
> 概要: 开发商Polyarc宣布旗下VR动作冒险游戏《莫斯2》（Moss:Book II）将于2022年春季登陆PS VR平台，索尼在昨日的独立游戏发布会上，分享了游戏的详细信息。你陪伴Quill的历险，在《......
### [组图：多米尼克·菲克晒出和亨特·莎弗接吻照 两人曾合作《亢奋》](http://slide.ent.sina.com.cn/h/slide_4_86512_366276.html)
> 概要: 组图：多米尼克·菲克晒出和亨特·莎弗接吻照 两人曾合作《亢奋》
### [这个空间，“造梦盗钱”](https://www.huxiu.com/article/497331.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者 | 竺晶莹题图 | CFP新消费品牌正在空间美学上发力。有些品牌遵循“一店一设计”的路线，让每家门店都能带给消费者新意；而有的品牌则试图通过打造标准形象破圈以后再大量......
### [知乎否认视频部门几乎裁掉一半员工：没有裁员计划](https://www.ithome.com/0/602/600.htm)
> 概要: IT之家2 月 11 日消息，今日 Tech 星球报道，近日有网友爆料称，知乎目前正在低调裁员，主要是视频相关的部门，几乎裁掉一半员工。对此，知乎方面回应称：视频是知乎内容生态不可或缺的部分。在 20......
### [画师绘制「猫女」动画版本人物图公开](http://acg.178.com/202202/438567704832.html)
> 概要: 近日，画师Mike Maihack在社交平台上公开了自己绘制的「猫女」赛琳娜·凯尔动画版本的人物图。「猫女」美国DC漫画公司旗下的超级反派，本名塞琳娜·凯尔，是一名大盗，同时也是蝙蝠侠的暧昧对象之一......
### [英特尔 i9-13900K 现身《奇点灰烬》基准测试，24 核 32 线程](https://www.ithome.com/0/602/604.htm)
> 概要: IT之家2 月 11 日消息，据 WccfTech 报道，英特尔 13 代酷睿 i9-13900K 的测试样品已经出现在《奇点灰烬》游戏的基准测试中。如上图所示，该基准测试未能正确识别该处理器，显示为......
### [高速运转的数字时代，我们如何对抗“效率成瘾”？](https://www.tuicool.com/articles/qUFvqqQ)
> 概要: 编辑导语：随着科技的进步，我们的生活节奏变得越来越快，视频开倍速、网上的速成秘诀等等，我们或多或少都成为了追求效率的一员。这篇文章作者引入了传播学的技术决定理论，讨论我们为什么会陷入“效率成瘾”，以及......
### [网友票选《JOJO》最受欢迎反派角色 第一名毫无争议](https://acg.gamersky.com/news/202202/1459360.shtml)
> 概要: 《JOJO的奇妙冒险》中有各种个性鲜明的反派角色，也正是有了这些反派让剧情更加跌宕起伏，近日，就有网友投票选出了《JOJO的奇妙冒险》那些受欢迎的反派排名。
### [杨国福冲上市，麻辣烫会受资本宠爱吗？](https://www.tuicool.com/articles/Y77r63u)
> 概要: 文/张霏编辑/李信火锅市场曾孕育多个上市公司，如今麻辣烫市场的领头羊杨国福，也在冲刺上市。20年前，杨国福在哈尔滨永和街52号开第一家杨国福麻辣烫（当时名为“杨记麻辣烫”）时，或许他也没想到这一看似其......
### [组图：娜扎晒杀青照告别余靖秋 蓝衣又仙又纯笑容甜美](http://slide.ent.sina.com.cn/tv/slide_4_704_366277.html)
> 概要: 组图：娜扎晒杀青照告别余靖秋 蓝衣又仙又纯笑容甜美
### [这款年轻人的新玩具，史上最强还是史上最大？](https://www.huxiu.com/article/497384.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜空心三角题图｜Steam官网再有半个月（2月25日），V社（Valve，维尔福集团）旗下PC游戏掌机Steam Deck即将发售。坐拥全世界最大的PC游戏平台Stea......
### [组图：龚俊现身央视元宵晚会录制现场 身穿灰色羽绒服发型帅气](http://slide.ent.sina.com.cn/z/v/slide_4_704_366278.html)
> 概要: 组图：龚俊现身央视元宵晚会录制现场 身穿灰色羽绒服发型帅气
### [《全力兔子》令和版动画化企划启动！](https://news.dmzj.com/article/73571.html)
> 概要: 《全力兔子》宣布了令和版动画化企划启动的消息。本作将由浊川敦担任导演，森江实咲负责剧本，浦岛美纪担任角色设计，Rising Force负责动画制作。
### [美国动漫平台Crunchyroll年度动画大奖结果公布！](https://news.dmzj.com/article/73572.html)
> 概要: 近日，美国视频动漫平台Crunchyroll举办的2021年度动画大奖终于发表了结果。
### [长城汽车将发布全新高端汽车品牌：内部代号 BC，主打新能源车型](https://www.ithome.com/0/602/617.htm)
> 概要: 2 月 10 日，长城汽车在长城控股电子招标平台上发布的《BC 品牌 2022 年区域活动执行公司招标项目》显示，该公司将发布一个全新轿车品牌，内部代号为 BC。界面新闻记者援引一位接近长城汽车内部人......
### [情人节用点高端货：得宝抽纸 8 包 18.9 元（商超 26.9 元）](https://lapin.ithome.com/html/digi/602623.htm)
> 概要: 【拼多多 得宝旗舰店】得宝抽纸 Mini 系列 8 包 * 4 层 80 抽日常售价 26.9 元，今日拼多多旗舰店券后直降至 18.9 元。折合 2.36 元 / 包近期好价，情人节用点高端货：拼多......
### [Migrating from Netlify to Cloudflare Workers Sites for 2x Performance (2020)](https://brianli.com/migrating-from-netlify-to-cloudflare-workers-sites-for-2x-performance/)
> 概要: Migrating from Netlify to Cloudflare Workers Sites for 2x Performance (2020)
### [商标侵权如何适用于 NFT](https://www.tuicool.com/articles/267F7j2)
> 概要: Nike、Quentin Tarantino 和 Birkin 包包有一些共同点——它们都卷入与 NFT、不可替代代币相关的单独诉讼中。人们在 NFT 上花费了数十亿美元。它们被视为购买、交易和销售商......
### [YSFlight – A free flight simulator where anything is possible](https://ysflight.org/)
> 概要: YSFlight – A free flight simulator where anything is possible
### [腾讯兴亡，“皮肤”有责](https://www.tuicool.com/articles/2yEZ7jJ)
> 概要: 春节，有且只有一个赢家——腾讯。2月8日，微信发布2022年虎年春节数据报告，数据显示，春节期间（1月31日至2月5日）带有封面的微信红包收发总数超50亿个。根据微信开放平台要求，要想制作红包封面必须......
### [MirageOS 3.0 (2017)](https://mirage.io/blog/announcing-mirage-30-release)
> 概要: MirageOS 3.0 (2017)
### [冰墩墩，从查无此人到全民宠儿](https://www.huxiu.com/article/497452.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 | 渣渣郡题图 | 视觉中国本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。距离......
### [一拳重制：饿狼人设割裂，本应是绝对之恶，现在却越发人性](https://new.qq.com/omn/20220211/20220211A0BKUD00.html)
> 概要: 一拳超人重制版，从原先的追捧，到如今的挖苦，剧情上的割裂和冗长，的确是存在的。近期更新的重制版第202话，饿狼的表现，引发很多网友的不满。相比于原作，重制版饿狼的人设显得很割裂。           ......
### [让大妈只是退场不是被打败是有原因的，还得留给路飞再打一场](https://new.qq.com/omn/20220211/20220211A0C1YX00.html)
> 概要: 在海贼王漫画1040话中，基德和罗宣布了胜利。但他们并不是直接打败大妈的，而是让大妈被火前坊和炸弹炸了一下，只能算暂时退场。这堪比能炸掉整个鬼岛的威力，，不愧是天生怪物，最强女人。          ......
### [《斗罗大陆》官微关闭评论区，舞粉和雪粉互撕，你怎么看？](https://new.qq.com/omn/20220211/20220211A0CASU00.html)
> 概要: 《斗罗大陆》官微做出了一个“史无前例”的举动：关闭了评论区。要知道以前《斗罗大陆》动画节奏再大，官微都没有关过评论，为什么这次官微会主动认怂呢？主要是因为这个星期将要播出的《斗罗大陆》最新一集，关乎千......
### [An Optimization Story](https://tinkering.xyz/fmo-optimization-story/)
> 概要: An Optimization Story
### [外媒称“冬奥运动员冷到快昏倒”？本报记者和专业人士实地感受戳穿炒作](https://finance.sina.com.cn/jjxw/2022-02-11/doc-ikyakumy5466079.shtml)
> 概要: 【环球时报-环球网报道 赴张家口特派记者崔萌 记者 范凌志 刘欣】“运动员在参赛时被看见冷得颤抖，在比赛末尾几乎昏倒。”一些外媒近日关于北京冬奥的报道注重渲染赛场的“冷...
### [周江勇被决定逮捕，一图看懂周氏兄弟一政一商的“买卖”](https://finance.sina.com.cn/china/gncj/2022-02-11/doc-ikyamrna0292023.shtml)
> 概要: 责任编辑：李墨轩......
### [广西百色新增本土确诊病例35例 防控防疫物资储备充足](https://finance.sina.com.cn/china/2022-02-11/doc-ikyakumy5467714.shtml)
> 概要: （抗击新冠肺炎）广西百色新增本土确诊病例35例 防控防疫物资储备充足 中新网百色2月11日电 （陈秋霞 杨强 王伟臣）广西百色市官方11日通报...
### [深圳医保局曝光案例：骗取生育津贴719万，4人被执行逮捕](https://finance.sina.com.cn/jjxw/2022-02-11/doc-ikyakumy5468213.shtml)
> 概要: 来源：深圳医保微信公号 为充分发挥打击欺诈骗取医疗保障基金违法行为的震慑与警示作用，深圳市医疗保障局专门设立“曝光台”，定期曝光典型案件。现发布第一批案件。
### [外卖员给别墅送菜先被狗咬再被气晕，狗主人被判赔3800元](https://finance.sina.com.cn/china/2022-02-11/doc-ikyamrna0294480.shtml)
> 概要: 澎湃新闻资深记者 李菁 通讯员 陈卫锋 外卖骑手小张送菜时，被客户家门口未拴绳的狗咬伤；而犬主人赵女士解决问题的态度，让患有高血压的小张气得晕倒。
### [粤式办税再提速！广东将推十项智慧税务成果，打造跨平台“纳税缴费服务圈”](https://finance.sina.com.cn/china/gncj/2022-02-11/doc-ikyakumy5468757.shtml)
> 概要: 南方财经全媒体记者郑玮 广州报道 近日，国家税务总局广东省税务局发布2022年“我为纳税人缴费人办实事暨便民办税春风行动”工作方案（下称“春风行动”）。
# 小说
### [逆流时光](http://book.zongheng.com/book/978563.html)
> 作者：北落南风

> 标签：都市娱乐

> 简介：时光是一场旅行，每个人都是过客。再活一次，顾前不要活的像前世那样。曾经年少的最爱，这一次，他不要放手，要陪她度过余生。曾经受过的伤，他要一一的讨回来。曾经欺负过他的人，他要一一把账算回来。......这一辈子，他不要平凡，他要伟大。

> 章节末：抱歉，写不下去了

> 状态：完本
# 论文
### [DANets: Deep Abstract Networks for Tabular Data Classification and Regression | Papers With Code](https://paperswithcode.com/paper/danets-deep-abstract-networks-for-tabular)
> 日期：6 Dec 2021

> 标签：None

> 代码：None

> 描述：Tabular data are ubiquitous in real world applications. Although many commonly-used neural components (e.g., convolution) and extensible neural networks (e.g., ResNet) have been developed by the machine learning community, few of them were effective for tabular data and few designs were adequately tailored for tabular data structures.
### [Multi-Task Pre-Training for Plug-and-Play Task-Oriented Dialogue System | Papers With Code](https://paperswithcode.com/paper/multi-task-pre-training-for-plug-and-play)
> 日期：29 Sep 2021

> 标签：None

> 代码：None

> 描述：Pre-trained language models have been recently shown to benefit task-oriented dialogue (TOD) systems. Despite their success, existing methods often formulate this task as a cascaded generation problem which can lead to error accumulation across different sub-tasks and greater data annotation overhead.
