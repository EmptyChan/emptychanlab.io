---
title: 2022-02-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CypressTunnel_ZH-CN1174542149_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-23 22:52:37
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CypressTunnel_ZH-CN1174542149_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Vim 核心维护者 Sven Guckes 去世，Vim 之父：9.0 版本将献给他](https://www.oschina.net/news/183696/vim-main-promoter-programmer-sven-guckes-passed-away)
> 概要: Vim 之父 Bram Moolenaar 在邮件列表宣布了核心维护者 Sven Guckes 去世的消息。邮件正文显示，Sven Guckes 于 2022 年 2 月 20 日在柏林去世。他在 2......
### [GNOME 宣布 Clutter 库正式退役](https://www.oschina.net/news/183697/clutter-gnome-retired)
> 概要: GNOME Project 近日宣布Clutter 库正式退役，这个用于 GNOME Shell 并且是 GTK3 的核心库曾在 2006 年为 Linux 带来了基于 OpenGL 的硬件渲染。Cl......
### [FreeDOS 1.3 发布，时隔 6 年再次更新](https://www.oschina.net/news/183688/freedos-1-3-released)
> 概要: FreeDOS 是一个开源的 DOS 兼容操作系统，也是 MS-DOS 的开源替代。近日 FreeDOS 的作者 Jim Hall 正式发布了 FreeDOS 1.3 版本。该版本包含了自 1.2 版......
### [GNOME 42 Beta 发布](https://www.oschina.net/news/183689/gnome-42-beta-released)
> 概要: GNOME 团队在今天推出了 GNOME 42 的 Beta 版本，该版本比最初计划的发布日期延迟了十天。在 GNOME 42 Beta 中，变化包括：GNOME Calls 现在允许 SIP 供应商......
### [vscode语音注释, 让信息更丰富(下)](https://segmentfault.com/a/1190000041445495?utm_source=sf-homepage)
> 概要: vscode语音注释;让信息更丰富 (下)前言这个系列的最后一篇;主要讲述录制音频&音频文件存储相关知识;当时因为录音有bug搞得我一周没心情吃饭(voice-annotation)。一、MP3文件储......
### [Redis 忽然变慢了如何排查并解决？](https://segmentfault.com/a/1190000041446607?utm_source=sf-homepage)
> 概要: Redis 通常是我们业务系统中一个重要的组件，比如：缓存、账号登录信息、排行榜等。一旦 Redis 请求延迟增加，可能就会导致业务系统“雪崩”。我在单身红娘婚恋类型互联网公司工作，在双十一推出下单就......
### [Vim 核心开发者 Sven Guckes 去世，Vim 之父：我要把 9.0 版本献给他](https://segmentfault.com/a/1190000041447407?utm_source=sf-homepage)
> 概要: 2 月 21 日，Vim 之父 Bram Moolenaar 在一份公开邮件中宣布了 Vim 核心维护者 Sven Guckes 在柏林因病去世的消息。邮件中，Bram Moolenaar 对 Sve......
### [乌克兰是如何走到今天这一步的？](https://www.huxiu.com/article/500721.html)
> 概要: 本文来自微信公众号：地球知识局（ID：diqiuzhishiju），作者：无梦、樊学长，校稿：辜汉膺，编辑：格瓦斯，头图来自：视觉中国当地时间2022年2月21日俄罗斯总统普京向全国公民发表关于乌克兰......
### [《康斯坦丁:神秘之所》发行日期公布 今年5月3日上映](https://acg.gamersky.com/news/202202/1462130.shtml)
> 概要: 今日，华纳兄弟动画公司宣布，《康斯坦丁:神秘之所》的数字版和蓝光版将于今年5月3日上映。
### [谁在向拼多多“搬运视频”？](https://www.huxiu.com/article/500694.html)
> 概要: 本文来自微信公众号：剁椒TMT（ID：ylwanjia），作者：蓝莲花，原文标题：《“视频搬运者”掘金拼多多：冒充大V搬运视频，日赚3000》，头图来自：视觉中国拼多多上正在上演一场短视频的乾坤大挪移......
### [网友票选最有趣的杀手漫画 《城市猎人》果然经典](https://acg.gamersky.com/news/202202/1462061.shtml)
> 概要: 说到杀手就给人一种专业、行事作风干脆的印象。今天跟大家分享的“觉得有趣的杀手类型漫画”排行榜，突然发现杀手类的漫画还真是不少。
### [TROYCA×DMM pictures！原创动画《忍的一时》2022年播出](https://news.dmzj.com/article/73680.html)
> 概要: 动画制作公司TROYCA和DMM.com的DMM pictures合作制作的原创动画《忍的一时》宣布了将于2022年年内播出的消息。
### [奥斯卡又出争议操作：将有八个奖项不直播颁奖](https://ent.sina.com.cn/m/f/2022-02-23/doc-imcwiwss2437339.shtml)
> 概要: 新浪娱乐讯 北京时间2月23日消息，据外国媒体报道，此前奥斯卡一项引起了强烈反对的操作回归了：部分奖项不在颁奖典礼时直播颁发。本届奥斯卡颁奖典礼上，23个奖中的8个的颁奖将不是现场直播，而是在典礼直播......
### [TV动画「天才王子的赤字国家振兴术」无字ED影像公布](http://acg.178.com/202202/439582521827.html)
> 概要: TV动画「天才王子的赤字国家振兴术」的无字ED影像现已公布，本作已于2022年1月11日起播出。TV动画「天才王子的赤字国家振兴术」无字ED影像本作改编自鸟羽彻创作的轻小说作品「天才王子的赤字国家振兴......
### [动画「忍者乱太郎」第30季主视觉图公开](http://acg.178.com/202202/439582708838.html)
> 概要: 近日，经典动画「忍者乱太郎」公开了本作第30季的主视觉图，该作为系列30周年纪念的特别企划，将于2022年春季播出。动画「忍者乱太郎」改编自尼子骚兵卫原作的漫画「落第忍者乱太郎」，讲述了出身平凡的忍者......
### [漫画「炎炎消防队」作者大久保笃公开完结贺图](http://acg.178.com/202202/439582930736.html)
> 概要: 本周，热血漫画「炎炎消防队」正式完结，作者大久保笃于个人社交媒体公开了其绘制的完结贺图，并表示：“「炎炎消防队」约7年的连载任务，终于顺利完成了。”「炎炎消防队」是大久保笃在「周刊少年Magazine......
### [脑洞大开的动漫角色融合术 比鲁斯+弗利萨=超梦](https://acg.gamersky.com/news/202202/1462175.shtml)
> 概要: 说到“融合”，估计大家最先想到的就是来自《七龙珠》的那套神奇的合体术吧。一组让各种不同毫不相干作品的角色一同施展融合术，结果居然融合出完全令人没想到的角色。
### [谁在用“你的性格”赚钱？](https://www.huxiu.com/article/500749.html)
> 概要: 深燃（shenrancaijing）原创，作者：邹帅，编辑：唐亚华，题图来自：《催眠大师》测星座、塔罗占卜、测基因……年轻人对自己的好奇从未减少，现在大家开始玩“性格测试”了。不仅是年轻人自己喜欢测着......
### [P站美图推荐——骑士姬特辑](https://news.dmzj.com/article/73683.html)
> 概要: 坚强又美丽，骑士姬身上闪耀着独立女性的光辉
### [「LoveLive!Sunshine!!」国木田花丸第二张个人音乐会专辑试听公开](http://acg.178.com/202202/439586938152.html)
> 概要: 由日升动画制作的原创电视动画「LoveLive! Sunshine!!」近日公开了国木田花丸第二张个人音乐会专辑「THE STORY OF FEATHER」的全曲试听片段，系列专辑「LoveLive......
### [英如镝直播谈及哥哥巴图：希望他能联系我](https://ent.sina.com.cn/s/m/2022-02-23/doc-imcwiwss2457315.shtml)
> 概要: 新浪娱乐讯 近日，在此次北京冬奥会为国出战的冰球运动员英如镝在直播时谈及了自己同父异母的哥哥巴图。在直播中有网友问他和哥哥巴图的关系好不好，他说：“我们家一直欢迎他回来，我希望他也能联系我。”而对于为......
### [视频：电影《安魂》宣布撤档 巍子陈瑾等主演](https://video.sina.com.cn/p/ent/2022-02-23/detail-imcwiwss2458426.d.html)
> 概要: 视频：电影《安魂》宣布撤档 巍子陈瑾等主演
### [日本网友票选《Jump》中最佳对手 成长的道路不孤单](https://acg.gamersky.com/news/202202/1462177.shtml)
> 概要: 日本评选网站Goo让网友们再度选出《Jump》中的最佳敌手，来看看有哪些充满闪光点的敌手吧，基本都是大家熟悉的老对手了。
### [动画《银河英雄传说NTD》手游化决定！](https://news.dmzj.com/article/73684.html)
> 概要: 动画《银河英雄传说NTD Die Neue These》宣布了手机游戏化决定的消息。手机游戏名为《银河英雄传说 Die Neue Saga》，Aiming负责制作。
### [Refashioning the Lavoisiers (2021)](https://www.metmuseum.org/perspectives/articles/2021/9/david-lavoisier-conservation)
> 概要: Refashioning the Lavoisiers (2021)
### [《火焰纹章无双：风花雪月》在澳通过评级 含在线互动](https://www.3dmgame.com/news/202202/3836494.html)
> 概要: 继《异度神剑3》上周通过评级后，《火焰纹章无双：风花雪月》现已在澳大利亚通过评级。该作获得“M”级（成熟）评级，根据评级信息，它将包含幻想主题和暴力内容以及在线互动，这与前作《火焰纹章无双》基本一致......
### [On Bloom's two sigma problem](https://nintil.com/bloom-sigma)
> 概要: On Bloom's two sigma problem
### [The cats sitting on a fence in early builds of Windows 8](https://devblogs.microsoft.com/oldnewthing/20220208-00/?p=106232)
> 概要: The cats sitting on a fence in early builds of Windows 8
### [游戏公司Zordix Games 让其子公司创始人担任CEO](https://www.3dmgame.com/news/202202/3836502.html)
> 概要: 瑞典游戏公司Zordix Games近日宣布，Maximum Games创始人兼CEOChristina Seelyee将成为Zordix的新任CEO。Zordix Games的创始人兼前CEO Ma......
### [《黎明杀机》注册“约会模拟”商标 或将推出衍生游戏](https://www.3dmgame.com/news/202202/3836504.html)
> 概要: 《黎明杀机》在美国注册了一个“约会模拟”标题的新游戏商标，可能暗示了其将推出一款约会模拟衍生游戏。根据Reddit论坛用户LongJonSiIver的发现，在美国专利商标局官方数据库中，有一个名为“H......
### [本周Fami通游戏评分出炉 《消逝的光芒2》获黄金殿堂](https://www.3dmgame.com/news/202202/3836515.html)
> 概要: 本周Fami通杂志游戏评分出炉（2022年2月23日），这次共测评了4款游戏，其中《消逝的光芒2》获得了33分黄金殿堂评价。评分详情：《消逝的光芒2》 9/8/8/8【33/40】《遗忘之城》8/8/......
### [香港电影导演会编剧家协会发公告悼念楚原](https://ent.sina.com.cn/m/c/2022-02-23/doc-imcwiwss2510016.shtml)
> 概要: 新浪娱乐讯 2月23日，香港电影工作者总会的属会成员“香港电影导演会”及“香港电影编剧家协会”官方发布公告悼念于21日离世的导演兼演员楚原。　　香港电影导演会在悼文中写道：不论是粤语片抑或港产片年代，......
### [PS日本官方发推 祝贺《尼尔：机械纪元》发售5周年](https://www.3dmgame.com/news/202202/3836521.html)
> 概要: 由SE与白金工作室联合开发的动作冒险游戏《尼尔：机械纪元》在今日（2月23日）迎来了发售5周年纪念日。PlayStation日本也在官方推特上发文庆祝这一时刻。SE此前宣布将于北京时间2月23日5点举......
### [京东 218 元：西班牙贝蒂斯特级初榨橄榄油 2 瓶礼盒 99 元新低](https://lapin.ithome.com/html/digi/604422.htm)
> 概要: 西班牙进口，贝蒂斯特级初榨橄榄油 750mL×2 瓶礼盒报价 209 元，限时限量 110 元券，实付 99 元包邮，领券并购买。吊牌价 298 元，相当于 3.3 折优惠。使用最会买 App下单，预......
### [组图：柳岩打扮休闲现身机场 白蓝拼接棉服搭配牛仔裤清爽有活力](http://slide.ent.sina.com.cn/star/slide_4_86512_366604.html)
> 概要: 组图：柳岩打扮休闲现身机场 白蓝拼接棉服搭配牛仔裤清爽有活力
### [别人家的公司！Pocket Pair为员工放“Elden Ring假”](https://news.dmzj.com/article/73687.html)
> 概要: 游戏公司Pocket Pair日本为了让员工专心工作，为全体公司员工放了“Elden Ring假”。各位“Elden王”可以在游戏发售的25日和28日休息，在家体验游戏。
### [中科院用二氧化碳合成淀粉，合成生物学到底有多神？](https://www.tuicool.com/articles/aAjYVvn)
> 概要: 本文来自微信公众号：观察者网，作者：万三，原文标题：《万三| 二氧化碳人工合成淀粉背后：合成生物学——构建未来的制造方式》，头图来自：视觉中国“细胞治疗做了这么多年，感觉要做不下去了，但有了合成生物学......
### [你是否同意这些设计师的观点？](https://www.tuicool.com/articles/67niyqf)
> 概要: 编辑导语：按钮应该如何设计才更有效？页面应该如何布局才更合理？也许关于这些问题，前人已有相应的普适观点，但是在具体场景下，每个设计细节都有可能被改变。因此，产品设计不能落入惯性思维里。不妨看看本文的设......
### [车企造手机，复仇之外的一盘大棋](https://www.huxiu.com/article/501002.html)
> 概要: 出品 | 虎嗅汽车组作者 | 张博文头图 | IC Photo2021 年 9 月 28 日，李书福创办的湖北星纪时代科技有限公司与武汉经济技术开发区签署战略合作协议，正式宣布要投入100亿元，专攻高......
### [从Nacos到完全自研｜得物的注册中心演进之路](https://www.tuicool.com/articles/3yumEvB)
> 概要: 近几年，随着得物业务的快速发展，应用数目与实例规模在快速地增加。得物的注册中心也在不断的演进优化，以支撑业务的发展。从最初的 Nacos 到 Nacos Proxy 引进再到完全自研的 Sylas 替......
### [深夜剧上新 |高分悬疑神反转，50次还不止，简直了](https://new.qq.com/rain/a/20220221V03HNK00)
> 概要: 深夜剧上新 |高分悬疑神反转，50次还不止，简直了
### [如果贺尔荷斯代替了阿布德尔，瓦尼拉艾斯一战给人的震撼更大！](https://new.qq.com/omn/20220223/20220223A0BMGP00.html)
> 概要: 今天咱们来说一下JOJO的那些事，众说周知，就连荒木也曾经说过，的确有过要将贺尔荷斯这个小老帝塞入JOJO的主角团中，甚至有一期的JO3封面出现了小老帝的身影，不过最后，小老帝还是选择站在了DIO阵营......
### [火影：自叔疯狂追求纲手多年，为什么会被平平无奇地断给截胡](https://new.qq.com/omn/20220223/20220223A0BN2F00.html)
> 概要: 火影三人组的基调一般都是这样的，一个憨憨，一个装酷，一个妹子，妹子要么喜欢装酷的，要么就反感憨憨，自来也人设一登场，一看就知道铁憨憨，然而三忍最早登场的是大蛇丸，雌雄难辨，人妖不分，纲手这钦点要当火影......
### [The Meme Leak Theory](https://www.drorpoleg.com/chinas-war-on-memes/)
> 概要: The Meme Leak Theory
### [海贼王1041话：四档路飞不足以打败凯多，大和上船稳了](https://new.qq.com/omn/20220223/20220223A0BRB500.html)
> 概要: 海贼王1041话详细情报出炉，本以为这集又是个过渡篇，没想到尾田又给观众奉上一场全程高能。之前漫迷们一直猜测，大妈败北之后，凯老师很快就会领到属于自己的盒饭，然而事实却恰恰相反，凯多不仅没落幕，反而越......
### [全国首条电动重卡干线投运，宁德时代与三一重工合作的福建换电重卡示范项目落地](https://www.ithome.com/0/604/468.htm)
> 概要: 宁德时代联合三一重工昨日在福建宁德举办福建省换电重卡应用示范投运仪式。全国首条电动重卡干线 —— 福宁干线正式投入运营，福建省首批三一电动渣土车正式交付。同时，宁德市交通投资集团有限公司与宁德时代参股......
### [法国原瓶进口，卡思黛乐珍酿干红 39 元 / 支（京东 119 元）](https://lapin.ithome.com/html/digi/604471.htm)
> 概要: 【德龙宝真酒类旗舰店】法国原瓶进口，卡思黛乐 ×卡柏莱珍酿干红葡萄酒 750mL×2 支报价 299 元，下单立减 20 元，限时限量 200 元券，实付 79 元包邮，领券并购买。折合 39.5 元......
### [云韵在原著里不温不火，动画化之后却能大火，归功于戏份和建模](https://new.qq.com/omn/20220223/20220223A0C6K600.html)
> 概要: 云韵现在是《斗破苍穹》人气最高的几个角色之一，人气和美杜莎、萧薰儿三雄争霸。但当年小说连载的时候，云韵的人气比不过两个女主。2020年的漫画《知音漫客》举行最具人气女角色评选的时候，云韵的人气连前五都......
### [2021年城市50强：24城GDP超万亿，23个普通地级市在列](https://finance.sina.com.cn/china/dfjj/2022-02-23/doc-imcwiwss2553426.shtml)
> 概要: 原标题：2021年城市50强：24城GDP超万亿 23个普通地级市在列 第一财经记者统计梳理了2021年各大城市GDP数据，在全国337个地级及地级以上城市中...
### [中央农办等解读中央一号文件热点话题：如何保障粮食安全？耕地保护怎么落实？怎样巩固脱贫攻坚成果？](https://finance.sina.com.cn/jjxw/2022-02-23/doc-imcwiwss2553589.shtml)
> 概要: 新华社北京2月23日电 题：如何保障粮食安全？耕地保护怎么落实？怎样巩固脱贫攻坚成果？——有关部门负责人解读中央一号文件热点话题
### [全国政协主席会议确定郭卫民为全国政协十三届五次会议新闻发言人](https://finance.sina.com.cn/china/gncj/2022-02-23/doc-imcwipih4996220.shtml)
> 概要: 政协第十三届全国委员会第六十六次主席会议23日在京召开。会议确定，全国政协委员、外事委员会委员郭卫民为全国政协十三届五次会议新闻发言人。
### [基于 Kafka 的实时数仓在搜索的实践应用](https://www.tuicool.com/articles/3QfUzaU)
> 概要: 作者：vivo互联网服务器团队-Deng jie一、概述Apache Kafka 发展至今，已经是一个很成熟的消息队列组件了，也是大数据生态圈中不可或缺的一员。Apache Kafka 社区非常的活跃......
### [谷歌 Pixel 7 早期 CAD 图曝光，比 Pixel 6 更小更薄](https://www.ithome.com/0/604/474.htm)
> 概要: IT之家2 月 23 日消息，外媒 choosebesttech 与 @xleaks7 David 合作绘制了 Google Pixel 7 的 CAD 3D 模型图，我们一起来看一下吧。从上图可以看......
### [关于苏州、呼和浩特、北京疫情，最新情况！](https://finance.sina.com.cn/china/2022-02-23/doc-imcwipih4998077.shtml)
> 概要: 苏州市 记者23日从江苏省苏州市新冠肺炎疫情联防联控指挥部获悉，23日0时至15时，苏州新增新冠肺炎确诊病例2例，新增无症状感染者3例，均在管控范围内发现。
### [吉林省委书记景俊海到吉林省工业技术研究院调研](https://finance.sina.com.cn/china/dfjj/2022-02-23/doc-imcwiwss2557409.shtml)
> 概要: 原标题：景俊海到吉林省工业技术研究院调研 2月23日，省委书记景俊海到省工业技术研究院，就工业技术成果转化、中小企业培育等工作进行调研。
### [吉林省委书记景俊海会见格鲁吉亚迈威航空、西安旅游集团等组成的企业家合作考察团一行](https://finance.sina.com.cn/china/dfjj/2022-02-23/doc-imcwipih4998440.shtml)
> 概要: 原标题：景俊海会见企业家合作考察团 2月23日，省委书记景俊海在长春会见由格鲁吉亚迈威航空亚洲区总经理林钧、西安旅游集团文化旅游产业公司总经理徐亮...
# 小说
### [我儿快拼爹](https://m.qidian.com/book/1026085339/catalog)
> 作者：东土大茄

> 标签：异世大陆

> 简介：“叮！您的儿子惹上了纯阳境强者，本着父爱如山、父亲必须胜的原则，您的修为将提升到纯阳境。”……秦川发现，只要儿子惹事，他就能变强。于是，他在坑儿子的路上一去不复返…………儿子，你爹我天下无敌，随便造作吧！（QQ群：684418104）

> 章节总数：共443章

> 状态：完本
# 论文
### [Semi-Supervised Medical Image Segmentation via Cross Teaching between CNN and Transformer | Papers With Code](https://paperswithcode.com/paper/semi-supervised-medical-image-segmentation-2)
> 日期：9 Dec 2021

> 标签：None

> 代码：None

> 描述：Recently, deep learning with Convolutional Neural Networks (CNNs) and Transformers has shown encouraging results in fully supervised medical image segmentation. However, it is still challenging for them to achieve good performance with limited annotations for training.
### [ArchABM: an agent-based simulator of human interaction with the built environment. $CO_2$ and viral load analysis for indoor air quality | Papers With Code](https://paperswithcode.com/paper/archabm-an-agent-based-simulator-of-human)
> 日期：2 Nov 2021

> 标签：None

> 代码：None

> 描述：Recent evidence suggests that SARS-CoV-2, which is the virus causing a global pandemic in 2020, is predominantly transmitted via airborne aerosols in indoor environments. This calls for novel strategies when assessing and controlling a building's indoor air quality (IAQ).
