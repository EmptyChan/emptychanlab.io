---
title: 2023-03-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SouthDownsSheep_ZH-CN8986424729_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-12 19:26:35
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SouthDownsSheep_ZH-CN8986424729_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [消费者的需求终点：实现超我价值](https://www.woshipm.com/it/5778715.html)
> 概要: 德鲁克认为，企业的唯一目的是创造顾客。而定位恰是企业创造顾客所要遵从的生理机制。即：用一个品牌占据消费者心智中的一个品类。当消费者在思考用什么品类解决需求时，心智会将对应品类的品牌名表达出来。所以，如......
### [CCD-TOB运营法详解，打造资产型品牌帮助企业复利增长](https://www.woshipm.com/operate/5778714.html)
> 概要: 本篇文章中，作者介绍了一套TOB品牌运营的方法——“CCD-TOB运营法”，这套方法的雏形是作者在2016年总结并创立的运营方法。本篇是一份“CCD运营法”操作手册，希望能够以更高效、便捷地帮助到同行......
### [探秘IOT领域中神奇的“状态机”](https://www.woshipm.com/pd/3469280.html)
> 概要: 状态机模型在IOT领域中广泛应用于智能设备的状态控制和事件处理。如何在IOT设备中应用状态机模型，提高智能设备的灵活性和智能化程度，是一个具有挑战性的问题。作者为我们展示了IOT设备中状态机模型的设计......
### [荡然无存 谷歌Stadia云服务也被关闭](https://www.3dmgame.com/news/202303/3864597.html)
> 概要: 谷歌Stadia的云串流服务，最近也被关闭，这是该平台原有技术的最后一块“残片”。当谷歌Stadia在今年早些时候被关闭时，据称该项目的部分云基础设施将在谷歌cloud中使用，并将提供给对该技术感兴趣......
### [国家春笋地理](https://www.huxiu.com/article/817865.html)
> 概要: 本文来自微信公众号：食味艺文志（ID：foodoor），作者：魏水华，首发于《深圳航空》杂志，有删节，题图：视觉中国春，草长莺飞、万象更新的节令。在四季分明的东亚大陆，“春”字很早就被赋予了超出其字面......
### [承认吧，人人心里都演过《黑暗荣耀》](https://www.huxiu.com/article/818091.html)
> 概要: 本文来自微信公众号：硬核读书会 （ID：hardcorereadingclub），作者：张文曦，编辑：钟毅，校对：杨潮，头图来自：《黑暗荣耀》第二季剧照万众期待中，《黑暗荣耀》第二季如期播出了。开播之......
### [文科毕业生深陷就业颓势，出路只有转码？](https://www.huxiu.com/article/818198.html)
> 概要: 本文来自微信公众号：新周刊 （ID：new-weekly），作者：昂仔， 编辑：西西，校对：向阳，原文标题：《月薪5000的文科生，出路只有转码？》，头图来自：视觉中国文科毕业生，你找到工作了吗？近日......
### [虚拟偶像AZKi游戏天赋 速通超难地理游戏GeoGuessr](https://www.3dmgame.com/news/202303/3864608.html)
> 概要: 在世界地图上随机给你一个地方的街景，你能猜出那是在哪里吗？这就是地理游戏《GeoGuessr》的乐趣，单单想一想就知道有多难，然而近日一位虚拟偶像AZKi展示了特殊游戏天赋，不到20分钟就速通了这款超......
### [钓鱼番《放学后海堤日记》日剧版确定6月13日发布](https://www.3dmgame.com/news/202303/3864610.html)
> 概要: 由小坂泰之原作、少见的钓鱼题材漫画《放学后海堤日记》之前宣布制作真人日剧，日前官方宣布日剧版将于6月13日发布，网络平台为Lemino，敬请期待。《放学后海堤日记》讲述了鹤木阳渚对于加入海堤部本来是心......
### [第一人称生存恐怖游戏《永无止境》发布实机预告片](https://www.3dmgame.com/news/202303/3864614.html)
> 概要: Nacon近日为第一人称生存恐怖游戏《永无止境》（Ad Infinitum）发布最新实机预告片。厂商还宣布，本作将于2023年9月发售。在《永无止境》中，玩家将扮演一位深受世界大战噩梦困扰的德军士兵......
### [玩家对本地化游戏诉求：从“有中文”变成“有用心的中文”](https://www.ithome.com/0/679/028.htm)
> 概要: 原文标题：《优秀的本地化游戏，一定要用 CNM“奖励”玩家？》文丨狗鸽 审核丨千里排版丨鹿九说到本地化这个领域，近期有关的乐子可谓是层出不穷。先有烧机冰箱力压双子拿下苏联魅魔之首。张琦老师大胆又专业的......
### [新疆长绒棉 + 石墨烯，安比斯男士平角裤 3 条 29.9 元](https://lapin.ithome.com/html/digi/679029.htm)
> 概要: 【安比斯旗舰店】新疆长绒棉 + 石墨烯，安比斯男士平角裤 3 条报价 59.9 元，限时限量 30 元券，实付 29.9 元包邮。天猫新疆长绒棉 + 石墨烯内衬，安比斯男士平角裤 3 条券后 29.9......
### [中国公司全球化周报｜SHEIN在美起诉Temu；辛巴达完成数千万美元融资，服装柔性供应链服务走向全球](https://36kr.com/p/2167332018073865)
> 概要: 中国公司全球化周报｜SHEIN在美起诉Temu；辛巴达完成数千万美元融资，服装柔性供应链服务走向全球-36氪
### [“老央行人”易纲留任央行行长](https://finance.sina.com.cn/roll/2023-03-12/doc-imykqscw8932691.shtml)
> 概要: 3月12日上午举行的十四届全国人大一次会议第五次全体会议上，经投票表决，现任央行行长易纲再次成为中国人民银行行长。
### [康义：今年将建立中国式现代化统计监测体系，实施碳排放核算](https://finance.sina.com.cn/jjxw/2023-03-12/doc-imykqscv2159929.shtml)
> 概要: 3月12日上午，第十四届全国人民代表大会第一次会议在人民大会堂举行第五次全体会议。会议结束后举行第三场“部长通道”采访活动，邀请部分列席会议的国务院有关部委负责人接...
### [漫改真人电影《放学后失眠的你》6月23日公开](https://news.dmzj.com/article/77425.html)
> 概要: 根据オジロマコト原作改编的真人电影《放学后失眠的你》将于6月23日上映，发表了追加出演者。
### [美少女战士CosmosxMaison de FLEUR合作款小物共39款发售](https://news.dmzj.com/article/77426.html)
> 概要: 武内直子原作的动画剧场版《美少女战士Cosmos》和杂货小物品牌Maison de FLEUR的合作款将于3月17日起在premium万代和Maison de FREUR的电商网站STRIPE CLUB开始预售。
### [《间谍过家家》约儿掌上尺寸手办发售](https://news.dmzj.com/article/77427.html)
> 概要: 根据远藤达哉原作改编的TV动画《间谍过家家》中约儿的手办“g.e.m.系列间谍过家家约儿”将于9月下旬发售，价格为含税8030日元。
### [两会现场 | 易纲、唐仁健竖大拇指](https://finance.sina.com.cn/china/2023-03-12/doc-imykqwmx8889298.shtml)
> 概要: 来源：政事儿 3月12日上午，十四届全国人大一次会议在人民大会堂举行第五次全体会议。 大会经投票表决，决定国务院秘书长、各部部长、各委员会主任、中国人民银行行长...
### [万亿美元资管巨头，盯上人民币基金](https://36kr.com/p/2166496272413191)
> 概要: 万亿美元资管巨头，盯上人民币基金-36氪
### [我国首台 F 级 50 兆瓦重型燃气轮机成功商用](https://www.ithome.com/0/679/042.htm)
> 概要: 感谢IT之家网友肖战割割、grass罗雨滋的线索投递！IT之家3 月 12 日消息，据央视新闻报道，我国首台全国产化 F 级 50 兆瓦重型燃气轮机商业示范机组在广东清远顺利通过 72+24 小时试运......
### [经济观察|硅谷银行破产 银行如何“引以为戒”？](https://finance.sina.com.cn/china/2023-03-12/doc-imykqwmt2082747.shtml)
> 概要: （经济观察）硅谷银行破产 银行如何“引以为戒”？ 中新社上海3月12日电 （高志苗）当地时间3月10日，美国Silicon Valley Bank（硅谷银行）因资不抵债遭金融监管部门关闭。
### [两家中国生物医药公司回应在硅谷银行有存款，其他公司情况如何](https://www.yicai.com/news/101699114.html)
> 概要: 云顶新耀表示，该公司已对硅谷银行(SVB)事件的风险敞口进行了全面分析，在此宣布公司只有非常少量现金存在该银行（远低于公司现金总量的1%）。
### [为解决机器学习碎片化问题，阿里、苹果、谷歌等 12 家巨头推出 OpenXLA](https://www.ithome.com/0/679/051.htm)
> 概要: 感谢IT之家网友Coje_He的线索投递！IT之家3 月 12 日消息，据谷歌官博消息，机器学习 ML 开发和部署如今受到了分散和孤立的基础设施的影响，这些基础设施可能因框架、硬件和用例而异。这种碎片......
### [危险的健身房：中产避坑必修课](https://www.huxiu.com/article/817884.html)
> 概要: 本文来自微信公众号：凤凰WEEKLY （ID：phoenixweekly），作者：米利暗，编辑：闫如意，题图来自：视觉中国没想到，供人强身健体的健身房，竟然这么“要命”。前几天，一篇维权文章在朋友圈刷......
### [动作冒险《 虚构世界2》发售 PS4/NS版稍后登场](https://www.3dmgame.com/news/202303/3864621.html)
> 概要: Bedtime Digital Games制作发行的《虚构世界2:坚信之谷》近日正式发售，登陆了Steam、Epic Games Store）およびPS5、Xbox Series X|S／Xbox O......
### [二孩三孩中考可加10分，产假最长有218天！山西一地大招“催生”，数万元补助刚刚兑现](https://finance.sina.com.cn/china/dfjj/2023-03-12/doc-imykratx9157318.shtml)
> 概要: 来源：每日经济新闻 来自山西省晋城市泽州县政府办的消息，该县于日前举行“促进人口均衡发展九项措施”首批受益家庭补助发放仪式，为来自全县16个镇的首批10户家庭发放了一...
### [医疗健康行业周报 |「永仁心医疗」完成近亿美元A轮融资；「Bigfoot」首创糖尿病数字化MDI疗法](https://36kr.com/p/2168166978908424)
> 概要: 医疗健康行业周报 |「永仁心医疗」完成近亿美元A轮融资；「Bigfoot」首创糖尿病数字化MDI疗法-36氪
### [涉及多只中国美元基金！硅谷银行的破产处置，你需要知道这些](https://www.yicai.com/news/101699225.html)
> 概要: 众多中国背景的美元VC基金牵涉其中，它们大多选择在SVB开户。
### [陪伴了创投40年的硅谷银行，却不敌市场情绪的40个小时](https://36kr.com/p/2168252921032964)
> 概要: 陪伴了创投40年的硅谷银行，却不敌市场情绪的40个小时-36氪
### [视频：徐璐工作室晒live生图 徐璐穿浅绿色长裙清新露腰上衣性感](https://video.sina.com.cn/p/ent/2023-03-12/detail-imykratu9833930.d.html)
> 概要: 视频：徐璐工作室晒live生图 徐璐穿浅绿色长裙清新露腰上衣性感
### [视频：敬佩又心疼！68岁成龙带伤拍戏摔下马 郭麒麟看了头皮发麻](https://video.sina.com.cn/p/ent/2023-03-12/detail-imykratx9197376.d.html)
> 概要: 视频：敬佩又心疼！68岁成龙带伤拍戏摔下马 郭麒麟看了头皮发麻
### [视频：吴尊一家四口露营真实接地气 女儿NeiNei越长越女神](https://video.sina.com.cn/p/ent/2023-03-12/detail-imykratv8774117.d.html)
> 概要: 视频：吴尊一家四口露营真实接地气 女儿NeiNei越长越女神
### [丁腈手套需求降温，A股医疗器械业绩如何填补？](https://www.yicai.com/news/101699258.html)
> 概要: 一次性健康防护手套市场逐步恢复到正常状态，价格也将逐步恢复到理性区间。
### [美欧同意启动电动汽车关键矿物谈判，能谈成吗](https://www.yicai.com/news/101699267.html)
> 概要: 所谓关键矿物就是在制造电动汽车电池时所需的锂、镍和钴等矿物。
### [多部委表态预期改善，经济增长有望向潜在增长水平回归](https://www.yicai.com/news/101699268.html)
> 概要: 对中国经济潜在增长率的测算基本上都在5%~6%之间
### [《蛋仔派对》：小七老师的蛋仔之旅，欢声笑语未曾间断](http://www.investorscn.com/2023/03/12/106194/)
> 概要: 两人在直播过程中欢声笑语，仿若两姐妹般亲密无间，阿呆更是将自己毕生所学的《蛋仔派对》技巧倾囊相授，与小七老师共同轻松过关......
### [甜啦啦圣代家族，直击消费新生代](http://www.investorscn.com/2023/03/12/106195/)
> 概要: 数据显示,2022年冰淇淋市场规模达到1672亿元。其中酒味冰淇淋、泡芙冰淇淋、烤红薯冰淇淋在市场中颇受欢迎。随着年轻消费者占据市场主流,冰淇淋行业也迎来了“百花齐放”的时代。在打破“淡季魔咒”上,各......
### [华为力挺“赛力斯汽车模式” 跨界合作为行业最佳范本](http://www.investorscn.com/2023/03/12/106196/)
> 概要: 近日,有细心的网友发现,问界系列在官方宣传物料中新增了华为标识,再一次引发外界关于华为下场造车以及赛力斯“代工厂”的种种猜测。事实上,华为余承东已经多次声明,华为没必要亲自下场造车,同时,华为和赛力斯......
# 小说
### [逆乱星辰](https://book.zongheng.com/book/407734.html)
> 作者：醉朱颜

> 标签：奇幻玄幻

> 简介：背负血海深仇的少年从小生活在一个千年的阴谋之中，当寓言成真的那一刻他毅然决定踏上揭开谜底的道路。就这样一个又一个的惊天谜团不断被揭开，而主角却发现自己从始至中只是一枚棋子，所有的一切都只是一场棋局。

> 章节末：终章 过客的你还能爱谁？

> 状态：完本
# 论文
### [VisCUIT: Visual Auditor for Bias in CNN Image Classifier | Papers With Code](https://paperswithcode.com/paper/viscuit-visual-auditor-for-bias-in-cnn-image)
> 概要: CNN image classifiers are widely used, thanks to their efficiency and accuracy. However, they can suffer from biases that impede their practical applications. Most existing bias investigation techniques are either inapplicable to general image classification tasks or require significant user efforts in perusing all data subgroups to manually specify which data attributes to inspect. We present VisCUIT, an interactive visualization system that reveals how and why a CNN classifier is biased. VisCUIT visually summarizes the subgroups on which the classifier underperforms and helps users discover and characterize the cause of the underperformances by revealing image concepts responsible for activating neurons that contribute to misclassifications. VisCUIT runs in modern browsers and is open-source, allowing people to easily access and extend the tool to other model architectures and datasets. VisCUIT is available at the following public demo link: https://poloclub.github.io/VisCUIT. A video demo is available at https://youtu.be/eNDbSyM4R_4.
### [Accurate and Efficient Stereo Matching via Attention Concatenation Volume | Papers With Code](https://paperswithcode.com/paper/accurate-and-efficient-stereo-matching-via)
> 日期：23 Sep 2022

> 标签：None

> 代码：https://github.com/gangweix/acvnet

> 描述：Stereo matching is a fundamental building block for many vision and robotics applications. An informative and concise cost volume representation is vital for stereo matching of high accuracy and efficiency. In this paper, we present a novel cost volume construction method, named attention concatenation volume (ACV), which generates attention weights from correlation clues to suppress redundant information and enhance matching-related information in the concatenation volume. The ACV can be seamlessly embedded into most stereo matching networks, the resulting networks can use a more lightweight aggregation network and meanwhile achieve higher accuracy. We further design a fast version of ACV to enable real-time performance, named Fast-ACV, which generates high likelihood disparity hypotheses and the corresponding attention weights from low-resolution correlation clues to significantly reduce computational and memory cost and meanwhile maintain a satisfactory accuracy. The core idea of our Fast-ACV is volume attention propagation (VAP) which can automatically select accurate correlation values from an upsampled correlation volume and propagate these accurate values to the surroundings pixels with ambiguous correlation clues. Furthermore, we design a highly accurate network ACVNet and a real-time network Fast-ACVNet based on our ACV and Fast-ACV respectively, which achieve the state-of-the-art performance on several benchmarks (i.e., our ACVNet ranks the 2nd on KITTI 2015 and Scene Flow, and the 3rd on KITTI 2012 and ETH3D among all the published methods; our Fast-ACVNet outperforms almost all state-of-the-art real-time methods on Scene Flow, KITTI 2012 and 2015 and meanwhile has better generalization ability)

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
