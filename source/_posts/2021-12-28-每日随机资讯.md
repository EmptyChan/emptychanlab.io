---
title: 2021-12-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.KjellHenriksen_ZH-CN6626275076_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-12-28 21:42:00
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.KjellHenriksen_ZH-CN6626275076_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [2021 年最受欢迎的 15 个 VS Code 主题排行榜](https://www.oschina.net/news/175861/most-popular-vscode-themes)
> 概要: 微软的 VS Code 凭其丰富的扩展受到众多开发者的青睐，俺总结了 2021 年最受欢迎的15 个 VS Code 主题（按VS Code 扩展商店的安装次数排名），仅供大家参考。1、One Dar......
### [LinkWechat 2021 年度报告](https://www.oschina.net/news/175878)
> 概要: LinkWechat 2021 年度报告
### [stronghop 开源跨境商城 v1.4.4 发布，修复一些问题](https://www.oschina.net/news/175961/strongshop-1-4-4-released)
> 概要: stongshop v1.4.4 更新内容如下：修复添加角色500错误修复注册成功302StrongShop 简介StrongShop 是一款免费开源的跨境电商商城网站。StrongShop 是基于 ......
### [社区持续升级，openGauss 联合产业创新，推动数据库跨越式发展](https://www.oschina.net/news/175923)
> 概要: 今天，以“汇聚数据库创新力量 逐梦数字时代星辰大海”为主题的 openGauss summit 2021 在北京线上线下同步举办。大会现场，openGauss开源社区理事会和技术委员会升级，openG......
### [ONE OK ROCK吉他手Toru与模特大政绚宣布结婚](https://news.dmzj.com/article/73194.html)
> 概要: ONE OK ROCK吉他手Toru与模特大政绚宣布结婚
### [【直播预告】2021年的最后一周直播](https://news.dmzj.com/article/73195.html)
> 概要: 本周直播预告
### [长城汽车前途光明、道路模糊](https://www.huxiu.com/article/485064.html)
> 概要: 作者｜Eastland头图｜IC photo2021年6月发布的《长城汽车2025年战略》将目标销量定在400万台，其中80%为新能源车，预期营收6000亿。也是2021年6月，长城汽车旗下子公司蜂巢......
### [4月新番动画《身为女主角》新PV公开！](https://news.dmzj.com/article/73196.html)
> 概要: 根据HoneyWorks歌曲改编的动画《身为女主角！被厌恶的女主角的秘密工作》今日公开新视觉图、PV，并追加了新的角色声优，本作将于2022年4月播出。
### [音乐剧《薄樱鬼 真改》斋藤一篇视觉图及全演员阵容公布](https://news.dmzj.com/article/73199.html)
> 概要: 音乐剧《薄樱鬼 真改》斋藤一篇全部演员阵容公布，视觉图也一并公开。
### [“替中国男导演道歉”，它做到了](https://www.huxiu.com/article/485508.html)
> 概要: 本文来自微信公众号：锐见Neweekly（ID：app-neweekly），内容转载自新周刊APP，作者：马路天使，头图来自：《爱情神话》剧照也许你也和我一样，被《爱情神话》这个名字骗了，不走进电影院......
### [苏北和鲁南：两大强省的边缘竞赛](https://www.huxiu.com/article/485515.html)
> 概要: 本文来自微信公众号：江南智造总局（ID：SouthReviews_csj），作者：海下（南风窗长三角研究院特约研究员），编辑：陈莹，原文标题：《两大强省的边缘竞赛》，头图来自：视觉中国苏北和鲁南，可能......
### [《汪汪队立大功》大电影内地定档预告 明年1.14上映](https://acg.gamersky.com/news/202112/1448557.shtml)
> 概要: 热门儿童动画《汪汪队立大功》的大电影发布定档预告，中国内地正式定档2022年1月14日上映。
### [视频：《新蝙蝠侠》正式预告大量新画面 明年3月4日北美上映](https://video.sina.com.cn/p/ent/2021-12-28/detail-ikyakumx6832795.d.html)
> 概要: 视频：《新蝙蝠侠》正式预告大量新画面 明年3月4日北美上映
### [Japan's Paper Culture](https://www.jetpens.com/blog/Japan-s-Paper-Culture/pt/998)
> 概要: Japan's Paper Culture
### [组图：陕西籍演员白宇为西安加油 发视频称“愿长安常安”](http://slide.ent.sina.com.cn/star/slide_4_86512_364981.html)
> 概要: 组图：陕西籍演员白宇为西安加油 发视频称“愿长安常安”
### [漫画「玫瑰之王的葬礼」宣布完结](http://acg.178.com/202112/434658980432.html)
> 概要: 由漫画家·菅野文创作的漫画作品「玫瑰之王的葬礼」迎来了最终话，宣布完结。本作的最新卷第16卷目前正在好评发售中。「玫瑰之王的葬礼」（蔷薇王的葬列）是菅野文创作的漫画作品，故事以十五世纪的玫瑰战争为原型......
### [视频：姚焯菲称赚到的钱由父母保管 直言有钱会买巧克力吃](https://video.sina.com.cn/p/ent/2021-12-28/detail-ikyamrmz1667247.d.html)
> 概要: 视频：姚焯菲称赚到的钱由父母保管 直言有钱会买巧克力吃
### [突破次元壁的动漫角色剪影美景 搭配现实画面超唯美](https://acg.gamersky.com/news/202112/1448534.shtml)
> 概要: Ins上的一位用户“aduniiss”，将动漫角色的剪影和现实的场景进行搭配，呈现出一种非常唯美的画面，而且丝毫没有突兀感或是不协调，效果真是非常棒。
### [到底谁在买NFT 美国顶流网红主播今年花了264万刀](https://www.3dmgame.com/news/202112/3831944.html)
> 概要: 如今NFT（数字原创不可替代艺术）、元宇宙等超前新概念突然爆火，引发各种经营买卖，来自美国的一位顶流网红主播logan-paul日前透露了自己的特殊收集，2021年在NFT上花费了264万5000美元......
### [大政绚宣布结婚 男方为OneOkRock吉他手Toru](https://ent.sina.com.cn/2021-12-28/doc-ikyamrmz1680962.shtml)
> 概要: 新浪娱乐讯 女演员大政绚今日宣布与ONE OK ROCK的吉他手Toru结婚。大政绚表示：“我从14岁来到东京开始工作，一直以来都以事业为重心。从今以后，有他一直用宽容的心陪伴我，让我感到很安心。”　......
### [胡杏儿和老公庆祝结婚六周年：愿我们慢慢走下去](https://ent.sina.com.cn/s/h/2021-12-28/doc-ikyamrmz1687620.shtml)
> 概要: 新浪娱乐讯 12月28日，胡杏儿老公李乘德在微博发文庆祝自己和胡杏儿的六周年结婚纪念日：“六年前的我们两，六年后的我们五，Happy Anniversary 老婆 xxx！”并晒出了当年两人的结婚照以......
### [漫画「鸭乃桥论的禁忌推理」第46话封面公开](http://acg.178.com/202112/434669471745.html)
> 概要: 漫画「鸭乃桥论的禁忌推理」公开了第46话的封面插图，本话为占卜王子杀人事件篇的第一集，现已更新。「鸭乃桥论的禁忌推理」是由「家庭教师」的作者、日本漫画家天野明创作的最新作品，于2020年10月11日在......
### [世嘉推世界真最速游戏PC 带轮子物理时速100公里](https://www.3dmgame.com/news/202112/3831951.html)
> 概要: 现在游戏PC厂家宣传旗下产品中总喜欢称产品速度快，世嘉近日也推出了一台，特别纪念版世界真最速游戏PC ，不过这回可是真的“最速”，因为这款PC附带轮子加电动驱动装置，在赛道上能达到100公里的惊人时速......
### [灰度投资者报告：比特币投资范式的深刻转移](https://www.tuicool.com/articles/ZJ7JzeF)
> 概要: 一直以来，比特币都是数字货币中的龙头老大，是投资者、金融机构、监管机构之间广泛提起的话题。随着更多其他优秀币种的出现，比特币在数字货币中的主导地位开始被一些投资者所不看好，认为其竞争对手已经出现。但事......
### [视频审查员看大量违规视频导致抑郁 要求TikTok赔偿](https://www.3dmgame.com/news/202112/3831958.html)
> 概要: TikTok深受全球网友喜爱，许多人上网分享了有趣短片，但其中也不乏违规内容，导致公司视频审查员每天上班要观看大量色情、血腥暴力画面，严重影响身心健康，部分员工甚至患上精神疾病。据外媒nypost报道......
### [任天堂法务部再次出手 五个盗版Switch ROM网站被停](https://www.3dmgame.com/news/202112/3831961.html)
> 概要: 据游戏媒体NintendoLife消息，近日，任天堂公司获得英国高级法院针对六家互联网服务提供商的禁令，任天堂公司可以此使六家网站停止其针对任天堂旗下Switch产品的盗版侵权行为。此道禁令迫使BT、......
### [艾斯纳奖最佳画师桃桃子绘制「不朽X战警」变体封面公开](http://acg.178.com/202112/434674922822.html)
> 概要: 近日，漫威漫画公开了全新故事刊「不朽X战警」第一期的变体封面。此封面由美国艾斯纳漫画奖最佳封面画师Peach Momoko桃桃子女士独家绘制，登场人物是「白皇后」艾玛·弗斯特。本期将于2022年3月正......
### [JUMP放出《咒术回战》狱门疆BGM 被封印效果太魔性](https://acg.gamersky.com/news/202112/1448662.shtml)
> 概要: JUMP的官方YouTube频道，公开了一部由作品中特级咒物“狱门疆”为意象的作业用BGM，可以感受被狱门疆封印的感觉。
### [动画电影「小虎墩大英雄」发布宣传海报](http://acg.178.com/202112/434677089263.html)
> 概要: 国产动画电影「小虎墩大英雄」发布了“成语小课堂”宣传海报，影片将于2022年2月1日全国上映。影片的主人公是一个立志成为镖师的“虎孩子”虎墩，一次误打误撞下，虎墩接到一份绝不简单的护镖任务，随后踏上了......
### [Show HN: Distributed Tracing Using OpenTelemetry and ClickHouse](https://github.com/uptrace/uptrace)
> 概要: Show HN: Distributed Tracing Using OpenTelemetry and ClickHouse
### [微软 Azure Database for MySQL Flexible Server 在中国大陆地区正式上线：完全托管的数据库服务](https://www.ithome.com/0/595/304.htm)
> 概要: IT之家12 月 28 日消息，据微软发布，Azure Database for MySQL Flexible Server 在中国大陆地区正式上线了！即刻起，可登录由二十一世纪互联运营的 Micro......
### [华为商城 299 元：惠齿华为 HiLink 水牙线 99 元跨年大促](https://lapin.ithome.com/html/digi/595311.htm)
> 概要: 【惠齿美容工具旗舰店】惠齿 h2ofloss 智能水牙线 日常售价 299 元，以往节日大促价都是 199 元（包括双 11 期间）。今日可领 200 元大促券，实付 99 元近期探底价：天猫惠齿 h......
### [清溢光电：预计明年上半年半透膜掩膜版 (HTM) 产品能实现量产](https://www.ithome.com/0/595/312.htm)
> 概要: 近日，清溢光电在接受投资机构调研时表示，目前合肥清溢的新产能正进入爬坡阶段，产线的设备匹配比较均衡，有利于后续产能开出。合肥清溢的生产制作能力主要针对 AMOLED / LTPS 等中高端产品。随着涂......
### [深入理解Go Json.Unmarshal精度丢失之谜](https://www.tuicool.com/articles/I32uEbe)
> 概要: 缘起前几天写了个小需求，本来以为很简单，但是上线之后却发现出了bug。需求大概是这样的：上游调用我的服务来获取全量信息，上游的数据包虽然是json但是结构不确定我的服务使用Go语言开发，所以就使用了原......
### [Please don't use Discord for FOSS projects](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html)
> 概要: Please don't use Discord for FOSS projects
### [陈婷晒照为女儿庆生，张艺谋出镜秀恩爱，画面温馨幸福](https://new.qq.com/rain/a/20211228A093CN00)
> 概要: 12月28日，陈婷在个人社交账号上晒出了一张照片，庆祝女儿的15岁生日。照片中女儿站在中间，捧着一个可爱的生日蛋糕，张艺谋和陈婷则站在两边，贴心的搂着孩子。一家三口对着镜头，露出幸福的微笑。     ......
### [一月会有哪些狗粮番？每部都很甜，不可错过](https://new.qq.com/omn/20211228/20211228A09NRK00.html)
> 概要: 不知不觉当中，新一轮的动漫新番即将到来，对于这些将会陪伴我们一段时光的作品，相信很多观众都从心期待吧！那么，新一轮的一月新番当中，有哪些狗粮番呢？接下来，就让我们来了解一下，一月甜蜜的狗粮番吧！   ......
### [蚂蚁增资220亿，消费金融两极分化成趋势？](https://www.tuicool.com/articles/7V3aymi)
> 概要: 本文来自微信公众号：时代周报（ID：timeweekly），作者：吴斯悠，原文标题：《消金公司花式补血：蚂蚁增资220亿，中原消金等6家募资超50亿》，头图来自：视觉中国年末，消金机构补血潮仍在继续，......
### [微星新款 Stealth GS77 游戏本曝光：i9-12900H + 17 英寸 4K 120Hz 屏](https://www.ithome.com/0/595/367.htm)
> 概要: IT之家12 月 28 日消息，据 VideoCardz 消息，微星 Stealth GS76 游戏本的继任款 GS77 现已曝光，搭载英特尔即将发布的 i9-12900H 和 4K 120Hz 屏......
### [谁在让电动车越来越贵](https://www.huxiu.com/article/485745.html)
> 概要: 出品｜虎嗅汽车组作者｜梓楠法师最近一个月，特斯拉、小鹏等品牌都直接涨价或通过缩减购车权益变相涨价。此前，特斯拉中国官网上线了Model 3后轮驱动版，该车售价上调了1.5万元至25.09万元。小鹏汽车......
### [Golang 并发编程指南](https://www.tuicool.com/articles/67nUneI)
> 概要: 作者：dcguo分享 Golang 并发基础库，扩展以及三方库的一些常见问题、使用介绍和技巧，以及对一些并发库的选择和优化探讨。go 原生/扩展库提倡的原则不要通过共享内存进行通信;相反，通过通信来共......
### [奥特曼：“闪耀特利迦”战力成谜，最无语的一次“吃瘪”](https://new.qq.com/omn/20211228/20211228A0AJ0S00.html)
> 概要: 大家好，我是小蜘蛛。特利迦最新的一集剧情更新了，这一集剧情的主线可以说是十分精彩的，希特拉姆和伊格尼斯的战斗绝对是大结局之前的预热，而且最后希特拉姆被卡姐给吞噬更是给剧情增加了不一样的色彩。这里不得不......
### [全球发展倡议和“一带一路”倡议是什么关系？外交部回应](https://finance.sina.com.cn/china/gncj/2021-12-28/doc-ikyamrmz1772400.shtml)
> 概要: 12月28日，外交部发言人赵立坚主持例行记者会。有记者提问，习近平主席提出“一带一路”倡议8年来，高质量共建“一带一路”，取得丰硕成果，为全球发展注入了强大动能。
### [《新斗罗大陆》朱竹清成神造型曝光，身材比例佳，宁荣荣放弃白袜](https://new.qq.com/omn/20211228/20211228A0AR0L00.html)
> 概要: 在《斗罗大陆》动漫的衍生IP中，《其中朱竹清的形象格外迷人。在《斗罗大陆》中，朱竹清的武魂是幽冥灵猫，走的是轻盈敏捷的路线，她的身材可以说是史莱克三美中最好的一个。            幽冥灵猫变成......
### [内地香港探路高质量发展内生动力：互利互补互联，找准“国家所需”“香港所长”交汇点](https://finance.sina.com.cn/roll/2021-12-28/doc-ikyakumx6949056.shtml)
> 概要: 原标题：内地香港探路高质量发展内生动力：互利互补互联，找准“国家所需”“香港所长”交汇点 南方财经全媒体记者柳宁馨 广州报道 12月28日...
### [云南体育运动职业技术学院女生骂外卖员“送外卖的狗”，学校成立调查组](https://finance.sina.com.cn/china/gncj/2021-12-28/doc-ikyakumx6949752.shtml)
> 概要: 12月26日，云南体育运动职业技术学院一女生辱骂外卖员小虎“就是送外卖的狗，死在你爹怀里”等，小虎听到后心情变得很糟，选择取消订单。学校已和相关部门成立调查组。
### [Mezli (YC W21) – hiring a full-stack SWE to build robot restaurant software](https://www.mezli.com/careers)
> 概要: Mezli (YC W21) – hiring a full-stack SWE to build robot restaurant software
### [第四期中国妇女社会地位调查：近四成被访者愿意孩子随母姓](https://finance.sina.com.cn/jjxw/2021-12-28/doc-ikyamrmz1775604.shtml)
> 概要: 原标题：第四期中国妇女社会地位调查：近四成被访者愿意孩子随母姓 “关于子女姓氏，近四成被访者愿意孩子随母姓，男女比例分别比2010年提高7.2和2.2个百分点。
### [凭什么都说他是下届影帝？](https://new.qq.com/rain/a/20211228A0B6WR00)
> 概要: 安德鲁加菲最近有点忙。《蜘蛛侠3：英雄无档》在国外热火朝天狂澜票房，作为前任蜘蛛侠扮演者，加菲无数次被问及三代同框的场景，粉丝对于三代相遇的想象乐此不疲。            《塔米·菲的眼睛》和《......
### [摩托罗拉正研发第三代Razr可折叠智能手机](https://www.3dmgame.com/news/202112/3831989.html)
> 概要: 日前负责摩托罗拉智能手机业务的联想公司高管表示，摩托罗拉正在研发第三代Razr可折叠智能手机。联想中国区手机业务部总经理陈劲透露，公司一直在悄然研发可折叠手机产品。摩托罗拉推出的前两款可折叠智能手机乏......
### [63岁陈秀珠罕露面，与22岁儿子爬山精神好，拒绝告知其生父身份](https://new.qq.com/rain/a/20211228A0B9LC00)
> 概要: 12月28日，老戏骨陈秀珠更新了个人社交账号的动态，曝光了自己的近况。            罕见露面的陈秀珠，穿着一身黑色运动装出镜，头戴棒球帽，打扮休闲但难掩气质。已经63岁的她，无论皮肤还是身材......
### [赵本山准女婿送球球礼物，奢侈品价值2万多，却被嘲土到掉渣](https://new.qq.com/rain/a/20211228A0B98Q00)
> 概要: 赵本山女儿球球在今年4月份，官宣恋爱的消息，6月份，二人高调订婚，他们二人也早已同居到了一起。马上就过年了，球球要回东北老家陪父母过年，赵本山这个准女婿晒出了送球球的礼物，说：“有我在的冬天不会冷，都......
### [香港海关侦破洗黑钱案件 涉案金额超3.8亿港元](https://finance.sina.com.cn/jjxw/2021-12-28/doc-ikyamrmz1779583.shtml)
> 概要: 12月28日，香港海关发布消息称，海关今日（28日）采取执法行动，拘捕2名涉嫌进行洗黑钱活动人士，涉案金额约3.84亿港元。
### [灰原哀：别想要解药](https://new.qq.com/omn/20211228/20211228A0BLPY00.html)
> 概要: 灰原哀......
### [解码工业百强区：占全国GDP15%，向中高端制造业挺进](https://finance.sina.com.cn/roll/2021-12-28/doc-ikyakumx6957835.shtml)
> 概要: 原标题：解码工业百强区：占全国GDP15%，向中高端制造业挺进 近日，中国信息通信研究院布了《中国工业百强县（市）、百强区发展报告（2021）》。
# 小说
### [都市战尊奶爸](http://book.zongheng.com/book/1083290.html)
> 作者：珊妹子

> 标签：都市娱乐

> 简介：无敌战尊回归都市，竟然成了一个新手奶爸？

> 章节末：第281章药膳

> 状态：完本
# 论文
### [Locally Interpretable Model Agnostic Explanations using Gaussian Processes | Papers With Code](https://paperswithcode.com/paper/locally-interpretable-model-agnostic)
> 日期：16 Aug 2021

> 标签：None

> 代码：https://github.com/adityasaini70/bo-lime

> 描述：Owing to tremendous performance improvements in data-intensive domains, machine learning (ML) has garnered immense interest in the research community. However, these ML models turn out to be black boxes, which are tough to interpret, resulting in a direct decrease in productivity.
### [Machine Learning-Based Estimation and Goodness-of-Fit for Large-Scale Confirmatory Item Factor Analysis | Papers With Code](https://paperswithcode.com/paper/machine-learning-based-estimation-and)
> 日期：20 Sep 2021

> 标签：None

> 代码：None

> 描述：We investigate novel parameter estimation and goodness-of-fit (GOF) assessment methods for large-scale confirmatory item factor analysis (IFA) with many respondents, items, and latent factors. For parameter estimation, we extend Urban and Bauer's (2021) deep learning algorithm for exploratory IFA to the confirmatory setting by showing how to handle user-defined constraints on loadings and factor correlations.
