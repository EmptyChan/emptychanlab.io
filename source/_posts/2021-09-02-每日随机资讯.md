---
title: 2021-09-02-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PortoFlavia_ZH-CN0573894597_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-09-02 22:27:44
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PortoFlavia_ZH-CN0573894597_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Apache ECharts 5.2.0 发布，新增全局过渡动画、极坐标柱状图标签等功能](https://www.oschina.net/news/158358/apache-echarts-5-2-released)
> 概要: Apache ECharts 5.2.0 于 2021.9.1 正式发布，在这个版本中，我们引入了非常酷炫的全局过渡动画，并且对调色盘取色策略、极坐标柱状图标签等做了很多优化和增强，本文将带大家一睹为......
### [TypeScript 官网启用新的主页](https://www.oschina.net/news/158323/typescript-use-new-homepage)
> 概要: TypeScript 官方宣布，其网站采用了一个新的主页，以更好地向用户介绍 TypeScript。文中，官方表示以前的主页存在诸多问题，比如网站顶部没有明显的号召性用语、使用了太多解释概念的文字、网......
### [Linux Lite 5.6 正式发布，从 Windows 过渡到 Linux 的友好方案](https://www.oschina.net/news/158314/linux-lite-5-6-released)
> 概要: Linux Lite 5.6 已正式发布。Linux Lite 是一个对新手友好的 Linux 发行版，它基于 Ubuntu LTS，并以 Xfce 桌面为特色。Linux Lite 主要针对 Win......
### [微软低调发布 Visual Studio Code for the Web 预览](https://www.oschina.net/news/158325/ms-previews-free-vs-code-for-web)
> 概要: 根据外媒 The Register 的报道，微软在一篇已删除（估计是发布早了）的文章中宣布了 Visual Studio Code for the Web 的公共预览版本。Visual Studio ......
### [P站美图推荐——画师青十紅 ao+beni特辑](https://news.dmzj.com/article/72071.html)
> 概要: 青红老师对于笔下的女孩子表情刻画的细致入微，在他的皮肤、口红质感特长加持下，一个个美少女跃然纸上，只是用看的仿佛就能感觉到女孩子们的皮肤触感
### [三位肉食钙蜜的恋爱协奏曲](https://news.dmzj.com/article/72072.html)
> 概要: 这期给大家推荐一部美食耽美漫画《肉食组曲》，作者ダヨオ，讲述职业与个性各不相同的三人——三柴、千之助和平良，时常在烤肉店一边大口吃着肉一边互相为彼此的恋情加油打气的故事。
### [TCG动画《build divide》公开PV与宣传图](https://news.dmzj.com/article/72073.html)
> 概要: TCG动画《build divide-#000000》公开了第一弹宣传图与PV。在这次PV中，可以看到主人公们的打牌等的片段。
### [Redis 新特性篇：100% 掌握多线程模型](https://segmentfault.com/a/1190000040617969?utm_source=sf-homepage)
> 概要: Redis 官方在 2020 年 5 月正式推出 6.0 版本，提供很多振奋人心的新特性，所以备受关注。码老湿，提供了啥特性呀？知道了我能加薪么？主要特性如下：多线程处理网络 IO；客户端缓存；细粒度......
### [新漫周刊第101期 一周新漫推荐(20210902期)](https://news.dmzj.com/article/72078.html)
> 概要: 大学狗回老家接收奶奶的咖啡店以及里面的五个女房客&服务员（赤松健：勿cue谢谢）、有画风激似道满晴明剧情特别五十岚蓝的邻座美少女、给AR游戏debug结果困在里面出不来的程序员和NPC一起大冒险、被霸总狗大少求婚的深夜变身狗少女，少年JU...
### [Vue 更新到 3.2 版本了，你还学得动吗？](https://segmentfault.com/a/1190000040619654?utm_source=sf-homepage)
> 概要: 不久前，前端框架 Vue 发布 3.2 版本，对单文件组件功能、网络组件、服务端渲染以及性能等做出改进。而当时距离 Vue 3.0 Beta 版本的发布才不到一年半，Vue 的更新速度不可谓不快。有此......
### [人才是互联网公司最不值钱的东西](https://www.huxiu.com/article/453057.html)
> 概要: 本文来自微信公众号：底层观察家（ID：societybottom），作者：程春晓，头图来自：视觉中国把互联网公司当象牙塔，是本世纪最大的谎言。互联网的前身是阿帕网，上世纪60年代由美国国防部高级研究计......
### [视频：高圆圆赵又廷被偶遇挽手逛街 隔着屏幕也感受到恩爱甜蜜](https://video.sina.com.cn/p/ent/2021-09-02/detail-iktzscyx1817065.d.html)
> 概要: 视频：高圆圆赵又廷被偶遇挽手逛街 隔着屏幕也感受到恩爱甜蜜
### [视频：范冰冰街边等朋友被偶遇 肤白貌美生图颜值依旧在线](https://video.sina.com.cn/p/ent/2021-09-02/detail-iktzscyx1817796.d.html)
> 概要: 视频：范冰冰街边等朋友被偶遇 肤白貌美生图颜值依旧在线
### [Apple Watch production delayed as engineers wrestle with quality issues](https://asia.nikkei.com/Business/Technology/Apple-Watch-production-delayed-as-engineers-wrestle-with-quality-issues)
> 概要: Apple Watch production delayed as engineers wrestle with quality issues
### [网飞《范马刃牙》中文预告公开 奥利巴垫脚生涯开始](https://acg.gamersky.com/news/202109/1420451.shtml)
> 概要: Netflix动画剧集《范马刃牙》（第三季）中文正式预告片公开，定于9月30日开播。
### [「东京卍复仇者」×ナムコキャンペーン视觉图公开](http://acg.178.com/202109/424553593609.html)
> 概要: 「东京卍复仇者」公开了与ナムコキャンペーン合作的活动视觉图与周边商品图，商品包括立牌、徽章、挂件、包装袋等，活动将从2021年9月10日开始，持续至10月31日......
### [「LoveLive！虹咲学园学园偶像同好会」首专「MONSTER GIRLS」试听公开](http://acg.178.com/202109/424553724915.html)
> 概要: 由Sunrise制作的电视动画「LoveLive！虹咲学园学园偶像同好会」于近期公开了R3BIRTH组合的首张专辑「MONSTER GIRLS」的全曲试听片段，该专辑定价为2000日元（去税），确定于......
### [「Muv-Luv Alternative」开播前CM公开](http://acg.178.com/202109/424553920691.html)
> 概要: 由日本游戏开发商âge制作的同名文字冒险游戏改编，FLAGSHIP LINE和ゆめ太カンパニー×グラフィニカ共同制作的电视动画「Muv-Luv Alternative」于近期公开了开播前CM，动画将于......
### [未成年玩家真难 暂无法登陆守望魔兽等暴雪游戏](https://www.3dmgame.com/news/202109/3822662.html)
> 概要: 近日国家新闻出版署发布《关于进一步严格管理切实防止未成年人沉迷网络游戏的通知》，坚决防止未成年人沉迷网络游戏，切实保护未成年人身心健康。为了响应新规，守望先锋官微宣布，自9月1日起战网所有产品将进行游......
### [「烟囱城的普佩尔」再上映特别CM公开](http://acg.178.com/202109/424554615294.html)
> 概要: 由西野亮广的绘本改编，STUDIO4℃负责制作的动画电影「烟囱城的普佩尔」近日公开了再上映的特别CM。本作将于10月22日再上映，截止至10月31日。「烟囱城的普佩尔」再上映特别CM......
### [《Valorant》新地图预热 返视镜第二章即将推出](https://www.3dmgame.com/news/202109/3822663.html)
> 概要: 拳头的射击游戏《Valorant》公布了新地图“天漠之峡”的预热宣传片，“天漠之峡”地图将在返视镜第二章中推出。预热视频：官方介绍：太厉害了，这么小的东西却能产生这么大的机会，他们虽然努力不懈，但总要......
### [团伙在闲鱼低价卖山寨二手手机诈骗获利141万元](https://www.3dmgame.com/news/202109/3822664.html)
> 概要: 据@闲鱼官方微博公布的消息，在江苏南通，一团伙通过闲鱼销售廉价二手苹果手机，共计诈骗141万元。新闻视频：据调查，该团伙用低价和其它正品苹果手机的外观视频和图片做诱导，添加被害人微信后私下交易，随后邮......
### [饭制版《空洞骑士：丝之歌》预告 制作精良](https://www.3dmgame.com/news/202109/3822665.html)
> 概要: 近日，一位名叫“3D Print Guy”油管博主在自己的油管平台发布自制版《空洞骑士：丝之歌》预告片，据悉，该博主曾是皮克斯工作室的一位动画师，曾参与过《飞屋环球历险记》等动画的制作。原视频：<br......
### [从《仙剑》到《荣耀》 看游戏改编剧十六年演变](https://ent.sina.com.cn/v/m/2021-09-02/doc-iktzqtyt3610701.shtml)
> 概要: 随着年轻一代步入社会主流，流行文化越来越多地被赋予属于他们的色彩。就像他们所喜好的电子游戏及其衍生的电子竞技，曾经被上一辈不理解，现在却已成为流行文化的一个标签，以不同的面貌出现在影视剧里，丰富了影视......
### [Pong Circuit-Level Simulation](https://www.falstad.com/pong/vonly.html)
> 概要: Pong Circuit-Level Simulation
### [Show HN: A simple recording program with the ability to record the screen](https://github.com/akon47/ScreenRecorder)
> 概要: Show HN: A simple recording program with the ability to record the screen
### [奢侈品中古店，是一门好生意吗？](https://www.huxiu.com/article/453074.html)
> 概要: 本文来自微信公众号：新眸（ID：xinmouls），作者：李瑷蔚，题图来自：视觉中国奢侈品中古店，正在大众化。“中古”一词，源自于日语ちゅうこ，翻译过来就是有年代感的旧商品，通俗地说，就是二手商品的意......
### [组图：50岁李英爱登封面风格大突破 展酷帅时尚型格](http://slide.ent.sina.com.cn/star/k/slide_4_704_361046.html)
> 概要: 组图：50岁李英爱登封面风格大突破 展酷帅时尚型格
### [UK ISP Sky feeds realtime customer bandwidth data to litigous anti-piracy firm](https://torrentfreak.com/sky-subscribers-piracy-habits-directly-help-premier-league-block-illegal-streams-210828/)
> 概要: UK ISP Sky feeds realtime customer bandwidth data to litigous anti-piracy firm
### [携程TrainPal 首次亮相2021服贸会，旅行服务技术引领出海数字贸易](https://www.tuicool.com/articles/viaa2yJ)
> 概要: 阅读时间大约3分钟（814字）27分钟前携程TrainPal 首次亮相2021服贸会，旅行服务技术引领出海数字贸易来源：携程黑板报公众号英国火车票在线预定市场行业第二，携程TrainPal首次亮相20......
### [《扫黑风暴》片头制作方发表声明回应抄袭争议](https://ent.sina.com.cn/v/m/2021-09-02/doc-iktzqtyt3662362.shtml)
> 概要: 新浪娱乐讯 近日，电视剧《扫黑风暴》片头被指涉嫌抄袭，针对此事，《扫黑风暴》片头制作方于2日发表声明回应，表示公司已于2021年7月21日在成都伦索科技有限公司运营的VJshi网站购买了该素材，并在素......
### [天猫公布今年双11节奏 双阶段时间提前无需再熬夜](https://www.3dmgame.com/news/202109/3822695.html)
> 概要: 众多小伙伴捂了许久的钱包又快要开放了，9月2日今天，阿里巴巴副总裁、天猫副总裁吹雪在阿里妈妈m峰会上正式公布了今年双11节奏。据吹雪介绍，今年双11和去年一样，依旧保持双节棍”的节奏设计，分两个阶段售......
### [全球唯一一所美国官方认证的魔法学校，在教些什么？](https://www.huxiu.com/article/453240.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童题图 | 《哈利·波特与魔法石》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。小时候你......
### [市场要闻 | 央视曝光钱大妈加盟商亏损内幕，有人年亏四十万](https://www.tuicool.com/articles/NRNjeuZ)
> 概要: 凭着“不卖隔夜肉”和阶梯式打折在社区生鲜赛道博出位的钱大妈，正在为自己的模式买单。昨日晚间，央视财经曝光钱大妈加盟商的亏损内幕，在钱大妈大举扩张的同时，大批加盟商正因难承重压而陆续关店。报道称，为了吸......
### [天津大学研发出可溶解智能手表，实现电子产品无污染快速回收](https://www.ithome.com/0/573/075.htm)
> 概要: IT之家9 月 2 日消息 自“碳中和”出现、流行，并成为国家口号，我们生活中常见的电子垃圾处理问题也成了一直不得不直面的挑战。IT之家了解到，中国向世界承诺到 2060 年将达到碳中和，也就是二氧化......
### [电视剧全网热度榜，《云南虫谷》排第二，第一热度高达79.82](https://new.qq.com/omn/20210902/20210902A0A73Z00.html)
> 概要: 9月2日，从“骨朵热度指数排行榜”数据显示可知，当前热播电视剧全网热度榜前五分别如下：第一部《扫黑风暴》79.82先说优点吧，题材敢拍，很好，启用老戏骨，以及认真演戏的流量，呈现出来的整体观感不错。再......
### [悦刻发布首个电子烟空弹回收计划：用于水泥生产](https://www.ithome.com/0/573/079.htm)
> 概要: IT之家9 月 2 日消息 今日，RELX 悦刻在发布会上推出烟弹空弹回收计划，这也是国内电子烟品牌发布的首个空弹回收计划。悦刻介绍，该计划将联合中华思源工程扶贫基金会，回收空弹用于水泥生产。空弹中的......
### [《海贼王》1024话情报：雷鸣八卦VS雷鸣八卦](https://acg.gamersky.com/news/202109/1420464.shtml)
> 概要: 《海贼王》1024话情报公开，本集大和还在和凯多对战，期间大和回忆起小时候的事情。
### [又酷又闪！卡米然晒休赛期训练场集锦](https://bbs.hupu.com/45057522.html)
> 概要: 又酷又闪！卡米然晒休赛期训练场集锦
### [幸存的教育公司，到底谁留着“PLAN B”?](https://www.tuicool.com/articles/jiAzmau)
> 概要: 无论教育行业如何改变，“教育科技”永不过时。7月24日，“双减”政策正式落地，在线教育企业股价暴跌。截至7月30日，这股恐慌情绪达到顶峰，主营业务为职业教育、教育信息化的企业悉数下跌。根据图一，图一统......
### [索尼 Xperia 1 III 系统软件 61.0.A.11.92 发布，优化温度控制算法，官方分享减少不必要发热小技巧](https://www.ithome.com/0/573/106.htm)
> 概要: IT之家9 月 2 日消息 今年 5 月份，索尼中国发布了 Xperia 1 III 国行手机，12GB + 256GB 售价 8499 元，12GB + 512GB 售价 9499 元。该机在今年 ......
### [大小周都取消了，什么时候轮到周报？](https://www.huxiu.com/article/453267.html)
> 概要: 本文来自微信公众号：字母榜（ID：wujicaijing），作者：薛亚萍，原文标题：《取消大小周易，取消周报难》，题图来自：视觉中国又一家公司宣布取消大小周。9月1日，小鹏汽车向全员发布了相关通知。自......
### [以太坊侧链xDai：已将Arbitrum开源实例部署至其网络中](https://www.btc126.com//view/185525.html)
> 概要: 9月2日消息，以太坊侧链xDai在推特上表示，已经将以太坊扩容方案Arbitrum的开源实例部署至xDai网络中，该项目被称为AoX。xDai表示，该版本不是由Arbitrum官方部署的，是为了研究和......
### [Fido币安智能链上线5000T](https://www.btc126.com//view/185526.html)
> 概要: 据官方消息，Fido DApp将于9月5日14点开始首期算力上线，数量为5000T......
### [波卡生态概念板块今日平均涨幅为4.00%](https://www.btc126.com//view/185527.html)
> 概要: 财经行情显示，波卡生态概念板块今日平均涨幅为4.00%。26个币种中20个上涨，6个下跌，其中领涨币种为：MATH(+26.01%)、XOR(+10.42%)、PCX(+8.56%)。领跌币种为：CR......
### [曾365天排队，如今被嫌弃！火了13年的网红鼻祖，要过气了吗？](https://www.tuicool.com/articles/fMfmeuy)
> 概要: 文/ 金错刀频道有哪个餐厅值得你反复打卡？在知乎的这个提问帖下，有个回答引起了大家争论。值得反复打卡的，除了海底捞、呷哺外还有绿茶。关于绿茶的回答，有人说是年轻时的最爱，很多年不吃了；有年轻人却表示没......
### [周伟任昆山市委书记、昆山经济技术开发区党工委书记](https://finance.sina.com.cn/china/dfjj/2021-09-02/doc-iktzqtyt3714997.shtml)
> 概要: 原标题：权威发布：周伟任昆山市委书记、昆山经济技术开发区党工委书记 来源：昆山发布 9月2日下午，我市召开全市领导干部会议，宣布江苏省委...
### [全国股转公司：全力保障北京证券交易所顺利开市、平稳运行](https://finance.sina.com.cn/roll/2021-09-02/doc-iktzqtyt3712677.shtml)
> 概要: 原标题：全国股转公司：全力保障北京证券交易所顺利开市、平稳运行 e公司讯，9月2日晚，全国股转公司党委召开专题会议。下一步，全国股转公司党委将牢牢坚守服务创新型中小...
### [压哨引援！拜仁新援萨比策技术分析](https://bbs.hupu.com/45060223.html)
> 概要: 压哨引援！拜仁新援萨比策技术分析
### [罚2.99亿！如果你知道郑爽全家以前有多能搞钱，肯定不会同情他们](https://new.qq.com/omn/20210902/20210902A0EGYY00.html)
> 概要: 今年4月26日，张恒在微博曝光与郑爽妈妈的聊天记录，其中提到郑爽对《倩女幽魂》1.5亿的片酬不满，与片方交涉要求涨到1.8亿，经讨价还价最终以成交。            而且更重要的是，为了逃避限薪......
### [《机动部队：绝路》：港版“树先生”，一部只能看一遍的压抑电影](https://new.qq.com/omn/20210902/20210902A0EH0C00.html)
> 概要: 2003年杜琪峰拉着任达华、林雪新开了一个IP系列那就是《机动部队》，因为第一部《机动部队》的成功。后来杜琪峰所在的公司，又接力拍了好几部《机动部队》，包括《机动部队：同胞》、《机动部队：绝路》等等......
### [国内第三家证券交易所来了！北交所上市公司将来自创新层，试点发行注册制](https://finance.sina.com.cn/china/gncj/2021-09-02/doc-iktzscyx1947034.shtml)
> 概要: 原标题：国内第三家证券交易所来了！北交所上市公司将来自创新层，试点发行注册制 记者 | 王鑫 靴子落地，此前被传多次的“北京证券交易所”真的来了！
### [美股开盘，区块链板块普涨](https://www.btc126.com//view/185532.html)
> 概要: 美股小幅高开，道指涨0.36%，纳指涨0.32%，标普500指数涨0.33%，区块链普涨......
### [45岁张震近照罕曝光，身材健硕状态好，6万豪表十分抢眼](https://new.qq.com/omn/20210902/20210902A0EJUA00.html)
> 概要: 9月2日，一组张震的近照罕见地在社交平台上曝光。            照片中，张震身穿渐变红色西装搭配墨绿色长裤，在普遍是黑白色男星的造型中，张震的此次造型挑战系数极高。但即使如此，并且张震经典的偏......
### [王毅出席菌草援外20周年暨助力可持续发展国际合作论坛](https://finance.sina.com.cn/china/2021-09-02/doc-iktzqtyt3714133.shtml)
> 概要: 原标题：王毅出席菌草援外20周年暨助力可持续发展国际合作论坛 新华社北京9月2日电（记者伍岳）国务委员兼外长王毅2日出席菌草援外20周年暨助力可持续发展国际合作论坛...
### [两条主线：一条是中小企业创新发展，一条是新三板改革，设立北京证券交易所是这两条主线的交会和结合](https://finance.sina.com.cn/china/2021-09-02/doc-iktzqtyt3714840.shtml)
> 概要: 作者：北京上市公司协会秘书长余兴喜 两条主线：一条是中小企业创新发展，一条是新三板改革，设立北京证券交易所是这两条主线的交会和结合。
### [DigiTimes：苹果 Apple Watch S7 将在 9 月底扩产，新款 MacBook Pro 将在 10/11 月推出](https://www.ithome.com/0/573/129.htm)
> 概要: IT之家9 月 2 日消息 供应链媒体 DigiTimes 今日报道称：苹果 Apple Watch Series 7 的产量将在 9 月底爬坡，这一消息表明苹果新品生产确实有短暂的延期，而此前日经亚......
### [杨德龙：设立北京证券交易所有利于促进交易的活跃，支持科技创新，支持新三板企业发展](https://finance.sina.com.cn/china/2021-09-02/doc-iktzqtyt3714941.shtml)
> 概要: 前海开源基金首席经济学家杨德龙表示，深化新三板改革，设立北京证券交易所，这是一个重要的里程碑事件，在沪深交易所之外设立北京证券交易所，将新三板交易所升级...
### [流言板日本足协会长：这只是10场比赛里的1场，要用剩下9场来弥补](https://bbs.hupu.com/45060770.html)
> 概要: 虎扑09月02日讯 北京时间今天晚上结束的卡塔尔世界杯亚洲区预选赛12强赛第一回合比赛中，日本0-1不敌阿曼。赛后，日本足球协会会长田岛幸三对这场比赛发表了自己的看法。“十分感谢那些今天能够冒雨来到体
### [流言板独行侠总经理谈球队阵容：我肯定满意，这是毫无疑问的](https://bbs.hupu.com/45060748.html)
> 概要: 虎扑09月02日讯 今天，独行侠总经理尼克-哈里森接受了记者的采访。谈到球队的阵容，哈里森说：“我肯定满意，这是毫无疑问的。”“但是在我们开始比赛之前，你真的不知道情况如何，但是我肯定满意，很可能我就
# 小说
### [霸天雷神](http://book.zongheng.com/book/357799.html)
> 作者：萧潜

> 标签：奇幻玄幻

> 简介：我是萧潜，我为霸天雷神代言——这绝对是一本好书。兄弟们可以来加我新建的Q群122075557，好些年没见大家了，可以一起聊聊

> 章节末：完本感言

> 状态：完本
# 论文
### [Learning Transferable Parameters for Unsupervised Domain Adaptation | Papers With Code](https://paperswithcode.com/paper/learning-transferable-parameters-for)
> 日期：13 Aug 2021

> 标签：None

> 代码：https://github.com/zhyhan/transpar

> 描述：Unsupervised domain adaptation (UDA) enables a learning machine to adapt from a labeled source domain to an unlabeled domain under the distribution shift. Thanks to the strong representation ability of deep neural networks, recent remarkable achievements in UDA resort to learning domain-invariant features.
### [Tree Decomposed Graph Neural Network | Papers With Code](https://paperswithcode.com/paper/tree-decomposed-graph-neural-network)
> 日期：25 Aug 2021

> 标签：None

> 代码：https://github.com/YuWVandy/TDGNN

> 描述：Graph Neural Networks (GNNs) have achieved significant success in learning better representations by performing feature propagation and transformation iteratively to leverage neighborhood information. Nevertheless, iterative propagation restricts the information of higher-layer neighborhoods to be transported through and fused with the lower-layer neighborhoods', which unavoidably results in feature smoothing between neighborhoods in different layers and can thus compromise the performance, especially on heterophily networks.
