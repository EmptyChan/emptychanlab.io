---
title: 2023-03-08-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WhitehorseAurora_ZH-CN0978404088_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-08 22:12:40
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WhitehorseAurora_ZH-CN0978404088_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [硬核养生，男颜经济，反向旅行…打工人的钱都花哪儿了？| 2023消费趋势报告](https://www.woshipm.com/it/5775510.html)
> 概要: 消费市场永远在动态变化，而变化中也隐藏机遇。本文梳理了2023年的11个消费趋势，为相关从业者提供参考。伴随着市场环境的提升、平台政策的完善，以及消费者心智的愈发成熟，相信2023年的消费市场，将迎来......
### [抖音商业采访赛道，不是一个好生意](https://www.woshipm.com/operate/5775302.html)
> 概要: 在短视频发展浪潮下，每位入局者都想来分一杯羹。短视频赛道有很多的行业，电商志、程前朋友圈这两个账号的商业采访做得很火，那么商业采访类好做吗？本文对此进行探讨。许多人应该都有刷到过电商志、程前朋友圈这两......
### [Lazada——东南亚电商市场的核心竞争者](https://www.woshipm.com/evaluating/5774249.html)
> 概要: Lazada 成立于 2012 年，是东南亚本土领先的电子商务平台。作为国内代表性的跨境电商平台，Lazada 是如何在众多竞争者中保持核心竞争力的呢？整个行业现状又是如何？本文作者对Lazada这款......
### [马斯克谈特斯拉下一代小型汽车：成本仅为Model 3一半 以自动驾驶模式为主](https://finance.sina.com.cn/tech/it/2023-03-08/doc-imykcfei0305884.shtml)
> 概要: 新浪科技讯 北京时间3月8日早间消息，据报道，当地时间周二，特斯拉CEO埃隆·马斯克（Elon Musk）表示，该公司正在研发的下一代小型汽车，且该汽车将主要以自动驾驶模式运行。这与他在2020年做出......
### [美FTC加强对Twitter隐私保护调查 计划要求马斯克出席听证](https://finance.sina.com.cn/tech/internet/2023-03-08/doc-imykcfec7109752.shtml)
> 概要: 新浪科技讯 北京时间3月8日早间消息，据报道，消息人士透露，在近期对Twitter的调查中，美国联邦贸易委员会（FTC）计划要求埃隆·马斯克（Elon Musk）出席听证。马斯克去年以440亿美元的价......
### [百亿补贴终燃“战火”：京东补贴遭拼多多狙击，阿里承压？](https://finance.sina.com.cn/tech/internet/2023-03-08/doc-imykcfec7110614.shtml)
> 概要: 文 | 新浪科技 张俊......
### [36氪首发｜细胞培养肉公司「极麋生物」获千万元天使+轮融资，培养基成本降为市面3%](https://36kr.com/p/2161590370889990)
> 概要: 36氪首发｜细胞培养肉公司「极麋生物」获千万元天使+轮融资，培养基成本降为市面3%-36氪
### [36氪专访 | NEXBEAT创始人Yoko：女性视角中的品牌出海“少数派”](https://36kr.com/p/2161367886094857)
> 概要: 36氪专访 | NEXBEAT创始人Yoko：女性视角中的品牌出海“少数派”-36氪
### [项目报道｜关注中小微企业产研协作需求，「明象科技」打造轻量级PLM SaaS产品](https://36kr.com/p/2160988690096388)
> 概要: 项目报道｜关注中小微企业产研协作需求，「明象科技」打造轻量级PLM SaaS产品-36氪
### [《东京复仇者2》最新名场面剧照公开！](https://news.dmzj.com/article/77383.html)
> 概要: 《东京复仇者2》最新名场面剧照公开！电影的两个篇章将分别于4月21日和6月30日上映。
### [《春日苦短，少年恋爱吧！》电视剧化决定！](https://news.dmzj.com/article/77385.html)
> 概要: 椎叶奈奈原作漫画《春日苦短,少年恋爱吧!》电视剧化决定!
### [求购Neuralink、Space X老股；转让持有OpenAI、某自动驾驶公司的基金份额｜资情留言板第84期](https://36kr.com/p/2162341785694473)
> 概要: 求购Neuralink、Space X老股；转让持有OpenAI、某自动驾驶公司的基金份额｜资情留言板第84期-36氪
### [舞台剧《蓝色监狱》主要角色及所有角色定妆照公开](https://news.dmzj.com/article/77388.html)
> 概要: 根据金城宗幸·野村优介原作改编的舞台剧《蓝色监狱》的主视觉图，以及所有演员的角色视觉图均已公开。
### [手写字与色彩的邂逅⑤ 红+黑](https://www.zcool.com.cn/work/ZNjQzNDg1ODA=.html)
> 概要: 𝗪𝗲𝗹𝗰𝗼𝗺𝗲 𝘁𝗼 𝗺𝘆 𝗰𝗼𝗹𝗼𝘂𝗿 𝗽𝗹𝗮𝗻𝗲𝘁.鲜艳喜庆红+低调奢华黑玫瑰无原则    心动至上嘿  你知道嘛  有趣的灵魂终会遇见背景图 cr Pinterest / Unsplash 侵删......
### [科创中心“核”动力｜叮当健康：“数字化”到家的健康守护者](http://www.investorscn.com/2023/03/08/106121/)
> 概要: 网络时代,人们无时无刻不体验着互联网科技发展所带来的便捷,购物、买菜、点餐都可以通过一部手机来实现。同样,当病人或者老年人,需要一些常规药物时他们只需打开“叮当快药”APP下单就可以很快收到自己所需的......
### [小身板撬动新体验，小米激光打印机K100再秀精准卡位](http://www.investorscn.com/2023/03/08/106124/)
> 概要: 近几年,消费场景迭代带动打印机市场发生了很大改变。过去以办公场景、打印店等B端用户为主要消费力量的打印机行业,在居家办公及学习场景的带动下,迎来了C端客户的爆炸性增长......
### [《海贼王》1077话情报：草帽团VS炽天使 陷入苦战](https://acg.gamersky.com/news/202303/1574332.shtml)
> 概要: 《海贼王》1077话情报公开，路飞他们还在和炽天使们战斗，但是战况不太乐观。释迦离开了小组去寻找本体，当他找到本体的时候，跟在他后面的神秘人突然开枪。
### [王兴将以个人身份参与王慧文创业公司A轮投资](https://finance.sina.com.cn/china/2023-03-08/doc-imykcvzu7829455.shtml)
> 概要: 来源：钛媒体App3月8日消息，3月8日下午，美团创始人王兴在朋友圈中透露，将以个人身份参与王慧文创业公司“光年之外”的A轮投资，并出任董事。
### [王一博包场大鹏《保你平安》 网友赞梦幻联动](https://ent.sina.com.cn/2023-03-08/doc-imykcvzv6767594.shtml)
> 概要: 新浪娱乐讯 3月8日，有网友晒出王一博包场大鹏新电影《保你平安》的截图。电影《保你平安》于3月8日大规模点映，3月10日上映，由大鹏执导编剧主演。而大鹏和王一博合作的电影《热烈》则将于暑期上映。不少网......
### [《模拟城市》开发者：很高兴看到《天际线》取得成功](https://www.3dmgame.com/news/202303/3864324.html)
> 概要: 近日，《模拟城市》的一位首席开发人员在与PC Gamer的对话中表示：“我很高兴看到《城市：天际线》取得了成功，因为它终于弥补了EA在《模拟城市》系列中留下的空白”。他补充说，“如果不是《城市：天际线......
### [央企核心资产估值有望优先修复，招商港口布局完善迎来全面估值提升](http://www.investorscn.com/2023/03/08/106130/)
> 概要: 继去年易主席提出中国特色估值体系之后，2023年2月2日，中国证监会召开2023年系统工作会议，会议提出了建设中国特色现代资本市场这一系统工程的实施路线，提出要推动提升估值定价科学性有效性，逐步完善适......
### [买房，首付来自哪里？](https://www.huxiu.com/article/810507.html)
> 概要: 作者｜虎嗅用户编辑｜虎嗅内容运营团队头图｜《心居》剧照中国人历来有房子情结，所谓“居者有其屋”“金窝银窝不如自己的狗窝”，房子是安全感和归属感的象征，自古以来人们就把房子当成安身立命之所。到如今，买房......
### [广东河源一个月内两次4级以上地震正常吗？专家释疑](https://finance.sina.com.cn/jjxw/2023-03-08/doc-imykeaix0199378.shtml)
> 概要: 3月8日，河源市东源县发生4.5级地震，随后又发生3.4级余震。此前的2月11日，河源市源城区也发生了4.3级地震。两次地震间隔不久，彼此之间是否有关系？
### [手机快充，到底伤不伤电池啊](https://www.huxiu.com/article/814589.html)
> 概要: 本文来自微信公众号：网易数读 （ID：datablog163），作者：苏晚水，数据：乐乐，设计：起司司、敏穗，头图来自：unsplash当下年轻人有众多强迫症，让手机保持满电恐怕是其中之一。屏幕右上角......
### [视频：乐视两部7年未播电视剧流拍 由张翰朴敏英等主演](https://video.sina.com.cn/p/ent/2023-03-08/detail-imykeait6643536.d.html)
> 概要: 视频：乐视两部7年未播电视剧流拍 由张翰朴敏英等主演
### [飞利浦 34M2C8600 显示器上架：34 英寸 OLED 带鱼屏，首发 7999 元](https://www.ithome.com/0/678/274.htm)
> 概要: IT之家3 月 8 日消息，飞利浦新款 EVNIA 系列 34M2C8600 显示器现已上架，首发 7999 元。据介绍，这款显示器采用 34 英寸 QD OLED 面板，原装三星 OLED 技术，1......
### [英伟达：现在将近400款游戏和软件支持RTX技术！](https://www.3dmgame.com/news/202303/3864338.html)
> 概要: 近日据英伟达消息，目前已有398款游戏和软件支持英伟达RTX技术，包括最畅销的游戏、最流行的软件和最常用的游戏引擎。英伟达介绍称，RTX光追技术能为游戏带来逼真且身临其境的视觉效果。在应用中，RT C......
### [淘宝推出0保证金开店 11项激励助新商家从0到百万](http://www.investorscn.com/2023/03/08/106133/)
> 概要: 3月8日讯，淘宝在近日上线新一期的“百万新商计划”，新商家开店不仅免缴保证金，且同时提供从“实现首单”到“年成交实现100万元”的成长激励权益，最高可得4000元发展基金......
### [辍学or读书？《我16岁辍学做游戏！》3月上线Steam](https://www.3dmgame.com/news/202303/3864342.html)
> 概要: 由开发商“好好回去读书”制作的独立游戏《我16岁辍学做游戏！》将于2023年3月上线Steam。《我16岁辍学做游戏！》是一款双结局的游戏开发模拟游戏。讲述一位16岁学生在辍学游戏开发和正常上学导致的......
### [未获版号就擅自出版网络游戏 重庆一公司被罚45万元](https://www.3dmgame.com/news/202303/3864343.html)
> 概要: 快科技消息，近日，重庆市万州区文化和旅游发展委员会依法对重庆某网络科技有限公司擅自出版网络游戏的违法行为作出没收违法所得人民币4.9万元、罚款人民币39.2万元的行政处罚。经查，2018年5月至案发期......
### [李沁肖像权维权胜诉 被告被判赔偿经济损失6000元](https://ent.sina.com.cn/s/m/2023-03-08/doc-imykeaiv3490453.shtml)
> 概要: 新浪娱乐讯 近日，李沁与某百货商行网络侵权责任纠纷案判决书曝光，李沁告百货商行侵犯肖像权胜诉。　　法院认为，某百货商行未经许可在其商品配图中使用李沁的肖像图片，侵犯了李沁的肖像权，应当承担相应的侵权责......
### [MONSTERS CHRONICLE 游戏王DM 炎之剑士手办开始预约](https://news.dmzj.com/article/77391.html)
> 概要: “MONSTERS CHRONICLE 游戏王DM 炎之剑士”手办开始接受预约了，该手办售价为7040日元含税。
### [消息称魅族将在年底推出折叠屏手机产品](https://www.ithome.com/0/678/286.htm)
> 概要: IT之家3 月 8 日消息，据钛媒体报道，星纪魅族集团执行副总裁、魅族品牌 CEO 黄质潘透露，魅族将会在年底推出折叠屏产品。目前，官方暂未透露更多有关于魅族折叠屏产品的相关信息。不过，随着魅族折叠屏......
### [澳洲珀斯铸币厂被曝“掺杂”交割 上金所称有关报道“背离真相”](https://www.yicai.com/news/101695863.html)
> 概要: 并非掺假
### [报告称今年女性平均月薪8689元 比男性少1253元](https://www.3dmgame.com/news/202303/3864348.html)
> 概要: 三八妇女节到来之际，智联招聘发布《2023中国女性职场现状调查报告》，从职场发展、婚育家庭、职场性别平等维度，洞察职场女性生存发展状况。从薪酬来看，2023年，女性的平均薪酬为8689元/月，与男性的......
### [《王者荣耀》狂铁英雄品质升级，即将上线体验服](https://www.ithome.com/0/678/288.htm)
> 概要: IT之家3 月 8 日消息，王者荣耀宣布，狂铁英雄品质升级，包括叙事、美学、玩法三个部分的更新，并即将上线体验服。狂铁故事新篇：在海都，人们知道有一位最强佣兵，外号“狂铁”。没有人记得他的本名，只记得......
### [报告：全球中端市场女性高管占比增长缓慢](https://finance.sina.com.cn/china/2023-03-08/doc-imykehrt3459156.shtml)
> 概要: 中新网3月8日电 题：报告：全球中端市场女性高管占比增长缓慢 中新财经记者 庞无忌 会计师事务所致同8日发布的《2023商业女性调查报告》显示，在全球中端市场企业中，有32...
### [数实深度融合之下，如何进一步释放汽车消费潜力？](http://www.investorscn.com/2023/03/08/106136/)
> 概要: 汽车产业作为国民经济重要的战略性、支柱性产业，是稳增长、促消费的重要领域。如何促进数字经济和汽车产业深度融合？如何满足消费者线上购车的需求，进一步释放汽车消费潜力？近日，在人民政协网主办的“加快数实融......
### [两会话题：粮食安全党政同责，这些突出问题值得重视](https://finance.sina.com.cn/china/2023-03-08/doc-imykehrt3424642.shtml)
> 概要: 代表、委员建言：加强高标准农田建设；大力发展油菜产业；促进草食畜牧业发展 今年的政府工作报告提出，全面落实粮食安全党政同责，强化粮食和重要农产品稳产保供...
### [“她经济”崛起拉动旅游、餐饮、美妆产业，女性消费主打“悦己”体验感](https://finance.sina.com.cn/china/2023-03-08/doc-imykehrv0214835.shtml)
> 概要: “悦己”体验感是如今女性消费的趋势。 在西双版纳，身着各民族服饰的女性游客“百花齐放”；在广州，一碗糖水就能成为“她”此行最美好的回忆；在泰山...
### [小镇贵妇：赢在起跑线，不去北上广](https://www.huxiu.com/article/814854.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 、题图 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。当我们去羡慕一......
### [遭遇“砍一刀”入侵，澳洲被Temu盯上了](https://www.huxiu.com/article/814784.html)
> 概要: 出品｜虎嗅商业消费组作者｜戚露丹编辑｜苗正卿题图｜视觉中国澳大利亚和新西兰已经成为Temu瞄准镜中的新猎物。据拼多多相关人士透露，拼多多跨境电商Temu计划于3月13日正式上线澳大利亚、新西兰站点。这......
### [在外卖、网约车、快递行业，她们都在实现自我价值](https://www.yicai.com/news/101696190.html)
> 概要: 在快递、外卖员、网约车司机这类过去以男性员工为主的职业中，女性工作者的身影日益增多，她们也逐渐成为行业里的中坚力量。
### [江南美食汇：知味观 20 种糕点任选 3 件 19.9 元](https://lapin.ithome.com/html/digi/678314.htm)
> 概要: 【知味观官方旗舰店】江南美食汇：知味观 20 种糕点 报价 19.9 元，叠加满 3 件 5 折优惠，限时限量 10 元券，拍 3 件共发 3 盒实付 19.85 元包邮。天猫江南美食汇，知味观 20......
### [西安印发流感应急预案，称必要时应停工停业停市停课](https://www.yicai.com/news/101696292.html)
> 概要: 在应急响应阶段，Ⅱ级响应中提出，适时采取“临时社会面管控”措施，必要时，在疫情暴发和流行地区依法采取停工、停业、停市、停课，限制或者停止使用有关公共场所，限制或禁止开展人群聚集活动及可能导致危害扩大的生产经营活动等措施。
### [社论：廓清市场噪声，做好美联储超预期紧缩的应对准备](https://www.yicai.com/news/101696320.html)
> 概要: 只要秉持开放包容的心态，不断加强中国在国际竞争中的比较优势，顺应国际产业生态链的变迁而动，中国经济就能行稳致远。
### [“鹰派”美联储引发全球市场巨震 A股三大指数缩量调整](https://www.yicai.com/video/101696346.html)
> 概要: “鹰派”美联储引发全球市场巨震 A股三大指数缩量调整
# 小说
### [凶手的痛恨之泪](http://book.zongheng.com/book/1230786.html)
> 作者：牙狼刃爪

> 标签：悬疑灵异

> 简介：某村里，生活着许多以务农为主的村民；一天早晨，两位中年农民怀念他们许久未见的老伙计；经过土地的边缘之时，看到这里生长旺盛的植物。俩人便认为该地下藏有珍宝，便开始挖地，却不知，两人挖出了两个头盖骨，使得原本落后宁静的的村庄变得恐怖无比；很快，公安厅接到了来自村长家的电话，便赶往现场；警方通过村庄的失踪人口和找到的案发地点，从而确认了两个头骨的身份是；其中，受害者家的媳妇却下落不明。所有警察从该现场所收集到的各种关于罪犯和受害者的物件和口头信息整理归纳，回到公安厅后，联合6名从中国人民公安大学来的实习警察一同破案，找到该凶手。从她口中，明白了凶手的作案手法和遭遇。并通过在大学校区附近的全程监控摄像头，找到了凶手，将其抓获带去警局审问。在审问犯人的过程中，因审讯人员的口误从而使犯人流下“痛恨”的眼泪。引发出了猛烈的过激话语深深地刺痛所有审讯人员和警察们的心，将原本判死刑的犯人直送“精神病院”。

> 章节末：

> 状态：完本
# 论文
### [Few-Shot Transfer Learning to improve Chest X-Ray pathology detection using limited triplets | Papers With Code](https://paperswithcode.com/paper/few-shot-transfer-learning-to-improve-chest-x)
> 概要: Deep learning approaches applied to medical imaging have reached near-human or better-than-human performance on many diagnostic tasks. For instance, the CheXpert competition on detecting pathologies in chest x-rays has shown excellent multi-class classification performance. However, training and validating deep learning models require extensive collections of images and still produce false inferences, as identified by a human-in-the-loop. In this paper, we introduce a practical approach to improve the predictions of a pre-trained model through Few-Shot Learning (FSL). After training and validating a model, a small number of false inference images are collected to retrain the model using \textbf{\textit{Image Triplets}} - a false positive or false negative, a true positive, and a true negative. The retrained FSL model produces considerable gains in performance with only a few epochs and few images. In addition, FSL opens rapid retraining opportunities for human-in-the-loop systems, where a radiologist can relabel false inferences, and the model can be quickly retrained. We compare our retrained model performance with existing FSL approaches in medical imaging that train and evaluate models at once.
### [Graph-Guided Network for Irregularly Sampled Multivariate Time Series | Papers With Code](https://paperswithcode.com/paper/graph-guided-network-for-irregularly-sampled-1)
> 概要: In many domains, including healthcare, biology, and climate science, time series are irregularly sampled with variable time between successive observations and different subsets of variables (sensors) are observed at different time points, even after alignment to start events. These data create multiple challenges for prevailing models that assume fully observed and fixed-length feature representations.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
