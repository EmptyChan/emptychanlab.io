---
title: 2022-02-02-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GHDMarmot_ZH-CN5983212280_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-02 22:05:05
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GHDMarmot_ZH-CN5983212280_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Electron 17.0.0 正式发布](https://www.oschina.net/news/181034/electron-17-0-0-released)
> 概要: Electron 17.0.0已正式发布。更新内容包括将 Chromium 升级至 98、将 Node.js 升级至 v16.13.0，以及将 V8 引擎升级至 v9.8 等。发布节奏变化从 Elec......
### [2022 技术趋势：C++、Go、Rust 大放异彩，网络安全值得关注](https://www.oschina.net/news/181041/oreilly-2022-technology-trends)
> 概要: 在线学习平台 O'Reilly 最新发布了一份《Technology Trends for 2022》报告。该报告基于该平台 2021 年 1 至 9 月产生的数据，并与 2020 年同期进行了比较；......
### [程序员们，你们的编程语言是不是选错了？](https://www.tuicool.com/articles/uIRFvqy)
> 概要: 培训机构和所谓同行专家的忽悠，让很多程序员感到迷茫。以下我认为是你现在和将来都不应该考虑学习的六种编程语言(老手可以转型，新手就不用学了)。为什么呢：1.JavaJava 目前是排名第三的语言。各类语......
### [组图：虎年春节档新片总票房破20亿 《水门桥》超8亿领跑](http://slide.ent.sina.com.cn/film/slide_4_704_366040.html)
> 概要: 组图：虎年春节档新片总票房破20亿 《水门桥》超8亿领跑
### [韩寒导演的烂片](https://www.huxiu.com/article/495329.html)
> 概要: 本文来自微信公众号：神奇人物在哪里（ID：inlens），作者：几何小姐姐，题图来自：《四海》大年初一，韩寒导演电影《四海》上映。起初知道这部影片，是看了它的一个碎片预告片。刘昊然演绎的小镇青年，几十......
### [剧场版「咒术回战 0」杂志3月号封面彩图公开](http://acg.178.com/202202/437760986345.html)
> 概要: 近日，杂志「Newtype」正式公开了剧场版「咒术回战 0」的杂志3月号封面彩图，本次封面上登场的人物是「乙骨忧太」。「咒术回战 0」是动画「咒术回战」的剧场版，改编自芥见下下创作的同名漫画作品，由M......
### [「初音未来」高山流水手办开订](http://acg.178.com/202202/437761091955.html)
> 概要: 近日，「初音未来」高山流水手办正式开启预订，作品采用ABS、PVC材质，全高约260mm，售价31440日元（含税），折合人民币1807元，预计将于2023年10月发售......
### [奥特曼：同样是闪耀形态，泰迦B级，特利迦A级，而他SSS级](https://new.qq.com/omn/20220110/20220110A05UZB00.html)
> 概要: 在《奥特曼》系列作品里，这些不同的正义战士都有各自的特点，其中部分人还有属于自己的一些形态，而闪耀形态就是1种非常罕见的样子。虽然闪耀形态十分稀有，但仍有一大堆的人存在闪耀形态。这里来看下5个具有代表......
### [隐私币：一场注定要失败的战争](https://www.tuicool.com/articles/qayEfiU)
> 概要: 遇到熊时，想要幸存下来，你不需要跑过熊，你只需要跑过你身后的人，隐私币或许正在称为那个“跑在最后的人”。隐私币是一类独特的加密货币，允许用户在进行区块链交易时完全匿名，即无法溯源。用户的身份和他们的交......
### [网络漫画改编动画「Chiikawa」将于四月播出](http://acg.178.com/202202/437766053375.html)
> 概要: 画师长野（nagano）创作的网络漫画改编的动画「Chiikawa」确认将于今年4月4日播出，并公开由动画工房制作。“Chiikawa”是在推特上连载的可爱短漫，由于内容基本是小动物的可爱日常（なんか......
### [剧场版动画「咒术回战 0」第三弹入场特典插图公开](http://acg.178.com/202202/437766772075.html)
> 概要: 近日，剧场版动画「咒术回战 0」公开了由MAPPA绘制的第三弹入场特典插图，插图上的人物为五条悟与夏油杰。「咒术回战 0」是动画「咒术回战」的剧场版，改编自芥见下下创作的同名漫画作品，讲述了以乙骨忧太......
### [TGA创始人：几个大手笔收购案已经进入最后阶段](https://www.3dmgame.com/news/202202/3834796.html)
> 概要: 据TGA创始人Geoff Keighley爆料，他从多个消息人士那里获知，目前有几个大手笔游戏公司收购案在谈判的最后阶段，2022年将是一个非常有意思的一年。看起来这些收购案在未来几个月内有望公开。2......
### [组图：古巨基携两岁爱子拜年 小Kuson叼奶嘴说祝福语太萌了](http://slide.ent.sina.com.cn/star/slide_4_704_366046.html)
> 概要: 组图：古巨基携两岁爱子拜年 小Kuson叼奶嘴说祝福语太萌了
### [事实证明，并不是所有人都想要元宇宙](https://www.tuicool.com/articles/qYv6b2y)
> 概要: 由于解释最新科技进展，报道硅谷大事小情点击上方“硅星人”关注我们~元宇宙还没到来，抵制元宇宙的力量就已经开始集结了。——文｜Juny   编辑｜VickyXiao自从Facebook改名Meta宣布正......
### [吴京主演影片累计票房破250亿：中国影史第一位](https://www.3dmgame.com/news/202202/3834801.html)
> 概要: 据猫眼专业版实时数据，2022年2月1日12时58分，吴京主演票房破250亿，是中国影史第一位。吴京该成绩的取得与其主演的《长津湖之水门桥》密不可分。据灯塔专业版实时数据，截至2月2日07时13分，《......
### [日本人气声优伊藤美来确诊新冠](https://news.dmzj.com/article/73498.html)
> 概要: 2月1日，日本声优伊藤美来所属的事务所发表了她感染新冠的消息。
### [TOPTEN2021 设计癖年度盘点：十大原创设计](https://www.tuicool.com/articles/2MnmmqM)
> 概要: 爱原创文章来源：设计癖ID：shejipi作者：设计癖媒体部编辑：妮妮有的设计大刀阔斧地改变形态，它们虽然暂时无法落地（或者无需落地），但演示了关于未来的丰富遐想；有的设计精准击中一个小痛点，虽然变化......
### [新春音乐会丨全网琴友弹唱合集拜年视频，前方高能！](https://new.qq.com/rain/a/20220130V06EZB00)
> 概要: 新春音乐会丨全网琴友弹唱合集拜年视频，前方高能！
### [我为什么不骂中国足球？](https://www.huxiu.com/article/495355.html)
> 概要: 本文来自微信公众号：那些原本是废话的常识（ID：feihuayuchangshi），作者：叶克飞，题图来自：视觉中国在我听过的蠢话里，“中国十几亿人难道挑不出11个踢球的”绝对排得上出镜率前十。在我年......
### [山本直树漫画《Believers》真人电影化决定](https://news.dmzj.com/article/73499.html)
> 概要: 山本直树的漫画《Believers》决定改编为真人电影了，导演由城定秀夫担任，预计于今年夏天上映。
### [漫改电影《噬谎者》“赌郎”影像公开](https://news.dmzj.com/article/73500.html)
> 概要: 根据迫稔雄原作改编的真人电影《噬谎者》，公开了迫近黑暗赌博俱乐部“赌郎”的全新影像。
### [苹果MacBook卖爆：中国区大受欢迎 吸引60%新用户](https://www.3dmgame.com/news/202202/3834813.html)
> 概要: 苹果前不久发布了2022财年Q1财报，当季净营收为1239.45亿美元，与去年同期的1114.39亿美元相比增长11%，创下纪录新高；净利润为346.30亿美元，与去年同期的287.55亿美元相比增长......
### [净利润同比大涨 36%，谷歌母公司 Alphabet 高管解读 2021 财年 Q4 财报](https://www.ithome.com/0/601/510.htm)
> 概要: 北京时间 2 月 2 日早间消息，Alphabet（谷歌母公司）今天发布了该公司截至 12 月 31 日的 2021 财年第四季度财报。报告显示，Alphabet 第四季度总营收为 753.25 亿美......
### [索尼 2021 财年第三季度销售收入同比增长 13%，PS5 总计卖出 1720 万台](https://www.ithome.com/0/601/513.htm)
> 概要: IT之家2 月 2 日消息，索尼发布 2021 财年第三季度财报，得益于影视、音乐、半导体及金融业务的强劲表现，销售收入达到 30313 亿日元（约 1679.34 亿元人民币），同比增长 13%；该......
### [虎年，新造车还虎得起来吗？](https://www.huxiu.com/article/495323.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔虎年第一天，恰逢造车新势力们每月初公布销量的日子。2022年2月1日，新势力们迎来开门红。1月有三家销量破万，分别是小鹏汽车的12,922辆，理想汽车的12,268......
### [《四海》7大彩蛋解析，韩寒的那些梗，你都看出来了吗？](https://new.qq.com/rain/a/20220202A065SL00)
> 概要: 继《飞驰人生》阔别大银幕三年后，韩寒终于带着新片《四海》重返春节档。            自从《乘风破浪》开始，韩寒似乎就对自己的电影定档春节这件事怀有十足信心（或执念），尽管他的电影所讲述的故事经......
### [组图：吴尊一家首次在中国过春节 夫妻庆祝相爱26周年秀恩爱](http://slide.ent.sina.com.cn/star/slide_4_704_366049.html)
> 概要: 组图：吴尊一家首次在中国过春节 夫妻庆祝相爱26周年秀恩爱
### [育碧：首部中国原创漫画 《刺客信条：王朝》数字版全网阅读量突破 10 亿](https://www.ithome.com/0/601/520.htm)
> 概要: IT之家2 月 2 日消息，据育碧官方消息，育碧宣布《刺客信条：王朝》漫画数字版在中国地区的所有线上平台的总阅读量已突破 10 亿。自发布以来，《刺客信条：王朝》在腾讯动漫平台“武侠”类漫画持续稳坐前......
### [DC元老《蝙蝠侠：煤气灯下的哥谭市》编剧去世](https://www.3dmgame.com/news/202202/3834833.html)
> 概要: 根据其长期合作者和朋友作家马克·韦德在 Facebook 上的一篇帖子，作家/编辑布莱恩·奥古斯丁在中风后去世。韦德在他的 Facebook 页面上披露了奥古斯丁去世的消息，奥古斯丁的家人没有要求他宣......
### [吴京担任北京冬奥会火炬手 晒火炬接力出发照](https://ent.sina.com.cn/s/m/2022-02-02/doc-ikyakumy3927661.shtml)
> 概要: 新浪娱乐讯 2月2日下午，吴京在微博晒出一张冬奥火炬接力的出发照，微笑比耶。他配文道：“2022年2月2号下午2点22号车上22个冬奥火炬手出发……缘份啊！”据悉，2日北京冬奥会火炬接力正式启动。(责......
### [《人世间》花絮：雷佳音说这部剧是献给自己爸妈的礼物](https://new.qq.com/rain/a/20220130V00YRF00)
> 概要: 《人世间》花絮：雷佳音说这部剧是献给自己爸妈的礼物
### [视频：林志玲父亲感谢外界关心 期待外孙成为有贡献的人](https://video.sina.com.cn/p/ent/2022-02-02/detail-ikyakumy3930908.d.html)
> 概要: 视频：林志玲父亲感谢外界关心 期待外孙成为有贡献的人
### [虎年春节近郊短途游客比重超七成，返程航线直飞机票开始涨价](https://finance.sina.com.cn/jjxw/2022-02-02/doc-ikyamrmz8766139.shtml)
> 概要: 虎年春节大年初二，北京冬奥会火炬接力正式启动，冰雪游成为虎年春节最热门的“打开”方式。来自去哪儿平台的数据显示，大年三十、初一两天...
### [万亿巨头盘前疯涨10%，外围全线爆发！这一国指数2年新高，A股要稳了？](https://finance.sina.com.cn/china/gncj/2022-02-02/doc-ikyamrmz8766759.shtml)
> 概要: 原标题：万亿巨头盘前疯涨10%，涨了半个阿里！近3000元股价直接1拆20，外围全线爆发！这一国指数2年新高，A股要稳了？ 来源：证券时报·e公司
### [网传河南有人售卖虎肉？警方通报](https://finance.sina.com.cn/wm/2022-02-02/doc-ikyakumy3940889.shtml)
> 概要: 近日，网传河南有人售卖虎肉、虎骨、虎皮及金钱豹肉、皮、麝香等制品引发网络热议。河南沁阳市委宣传部2月2日下午发布消息称，沁阳公安调查发现...
### [叙利亚官方：因为没能打进世界杯决赛圈，宣布解散国家队行政机关和所有技术人员](https://finance.sina.com.cn/jjxw/2022-02-02/doc-ikyakumy3942393.shtml)
> 概要: 原标题：叙利亚官方：因为没能打进世界杯决赛圈，宣布解散国家队行政机关和所有技术人员 2日，世预赛亚洲12强赛后，叙利亚足协官方宣布，0-3阿联酋...
### [“千元饮料”涉事公司，被罚50万](https://finance.sina.com.cn/china/gncj/2022-02-02/doc-ikyakumy3946332.shtml)
> 概要: 来源：南方都市报、N视频报道 1月30日，南都从深圳市市场监管局网站了解到，新茶饮品牌“野萃山”所属公司深圳市豪麟餐饮有限公司被深圳市市场监管局福田监管局罚款50万元...
### [你收到了一条来自周深的新年祝福，请查收](https://new.qq.com/rain/a/20220202V08PEX00)
> 概要: 你收到了一条来自周深的新年祝福，请查收
### [《战神4》《地平线黎明曙光》已完美支持Steam Deck](https://www.3dmgame.com/news/202202/3834845.html)
> 概要: 《战神4》《地平线：黎明曙光》现在是Steam Deck验证游戏，这意味着这两个游戏已经为Steam Deck做出完整优化，可以在Steam Deck上完美运行。今年1月20日时，V社确认了首批38款......
### [美国出售1100亿美元的3年期、10年期和30年期国债以进行再融资](https://finance.sina.com.cn/money/future/fmnews/2022-02-02/doc-ikyamrmz8785143.shtml)
> 概要: 美国出售1100亿美元的3年期、10年期和30年期国债以进行再融资。
### [消息称日本将于 2022-2024 年上线大量新晶圆厂，台积电和索尼都有](https://www.ithome.com/0/601/548.htm)
> 概要: 随着日本政府加大对本地 IC 制造业的支持力度，包括台积电和索尼的合资企业在内的几家新晶圆厂将在日本建设。据业内人士透露，这些晶圆厂预计将在 2022 年至 2024 年之间上线。《电子时报》援引消息......
### [男性失去求偶动力，《终末的后宫》第二部描绘出了真实的日本国情](https://new.qq.com/omn/20220202/20220202A09BSR00.html)
> 概要: 提到日本你会第一时间想到什么？是那些脑洞大开能够从中获取到各种知识的影视作品？还是通过包装宣传后的工匠精神？我想每个人心中都有不同的答案，但如果问我，我会第一时间想到：动漫。因为日本动漫的确做到了在全......
# 小说
### [我来自史前](http://book.zongheng.com/book/1119663.html)
> 作者：柏小锤

> 标签：都市娱乐

> 简介：我来自史前，那是一个神奇的时代现在的我本是冥府一小差，却不得不混在人类中

> 章节末：688章：大结局

> 状态：完本
# 论文
### [Text Detoxification using Large Pre-trained Neural Models | Papers With Code](https://paperswithcode.com/paper/text-detoxification-using-large-pre-trained)
> 日期：18 Sep 2021

> 标签：None

> 代码：None

> 描述：We present two novel unsupervised methods for eliminating toxicity in text. Our first method combines two recent ideas: (1) guidance of the generation process with small style-conditional language models and (2) use of paraphrasing models to perform style transfer.
### [Indian Licence Plate Dataset in the wild | Papers With Code](https://paperswithcode.com/paper/indian-licence-plate-dataset-in-the-wild)
> 日期：11 Nov 2021

> 标签：None

> 代码：None

> 描述：Indian Licence Plate Detection is a problem that has not been explored much at an open-source level.There are proprietary solutions available for it, but there is no big open-source dataset that can be used to perform experiments and test different approaches.Most of the large datasets available are for countries like China, Brazil, but the model trained on these datasets does not perform well on Indian plates because the font styles and plate designs used vary significantly from country to country.This paper introduces an Indian license plate dataset with 16192 images and 21683 plate plates annotated with 4 points for each plate and each character in the corresponding plate.We present a benchmark model that uses semantic segmentation to solve number plate detection. We propose a two-stage approach in which the first stage is for localizing the plate, and the second stage is to read the text in cropped plate image.We tested benchmark object detection and semantic segmentation model, for the second stage, we used lprnet based OCR.
