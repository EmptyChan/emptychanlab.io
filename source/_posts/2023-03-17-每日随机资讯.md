---
title: 2023-03-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BallyvooneyCove_ZH-CN0284564457_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-17 21:59:47
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BallyvooneyCove_ZH-CN0284564457_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [别煽动焦虑了，GPT-4还取代不了你](https://www.woshipm.com/ai/5783845.html)
> 概要: 近日OpenAI发布的GPT-4让人们对AI模型的能力有了新的认知，那么在发展迅猛的AI人工智能面前，人类最终是否会被其取代？GPT-4真的可以“无所不能”吗？本篇文章里，作者便针对GPT-4与其对人......
### [“潮退”之后，秀场直播路向何方？](https://www.woshipm.com/it/5784508.html)
> 概要: 流水的平台、铁打的主播。秀场直播是什么？相比传统直播有什么区别？这篇文章作者从主播行业的发展史一步步讲起，详细讲述了直播风口过后，秀场直播的新篇章故事，最后分析了秀场直播的新变局。推荐对电商直播、秀场......
### [B站关闭前台播放量显示，意在何处？](https://www.woshipm.com/it/5783066.html)
> 概要: 播放量是视频平台中衡量视频效果的重要参考之一。B站考虑全面取消播放量显示，这一举措的用意是什么？这对平台和用户会有什么影响？天天问小伙伴们对此给出了他们的看法，一起来看看吧。近日有消息传出，B站正在考......
### [《上古卷轴6》在B社被收购前并非Xbox独占游戏](https://www.3dmgame.com/news/202303/3864989.html)
> 概要: 近日英国CMA公布了六家公司的调查结果，这些公司都赞成微软收购动视暴雪，或者根本不认为这次收购会有负面影响，但是CMA公布了索尼的回应，当然是反对意见，并且索尼还宣称在微软收购ZeniMax之前，《星......
### [黑暗荣耀登顶全球收视榜：Netflix井喷大热韩剧](https://finance.sina.com.cn/tech/internet/2023-03-17/doc-imymcrff1082132.shtml)
> 概要: 新浪科技 郑峻发自美国硅谷......
### [文心一言“大战”ChatGPT：谁更能忽悠？](https://finance.sina.com.cn/tech/internet/2023-03-17/doc-imymcrfc4266386.shtml)
> 概要: 文 丨 新浪财经 周文猛......
### [匹配创作者和客户，「WAVE CLOUD」想让视频制作更高效 | 早期项目](https://36kr.com/p/2169505073050120)
> 概要: 匹配创作者和客户，「WAVE CLOUD」想让视频制作更高效 | 早期项目-36氪
### [Office全家桶引入GPT-4，微软：辅助用户而非取代用户](https://finance.sina.com.cn/stock/usstock/c/2023-03-17/doc-imymcrfc4293936.shtml)
> 概要: 新浪科技讯 北京时间3月17日早间消息，当地时间周四，微软宣布，将通过生成式人工智能（AI）技术来增强Office办公套装。在AI技术商业化的过程中，科技巨头正在展开激烈竞争......
### [《饥荒联机版》威尔逊人物重做上线 新皮肤DLC发售](https://www.3dmgame.com/news/202303/3865000.html)
> 概要: 《饥荒：联机版》威尔逊人物重做现已正式上线，与重做一同上线的还有三个全新皮肤DLC，威尔逊豪华衣柜(19元)，包含皮肤：威尔逊的全套贵宾、玫瑰、冒险家、凯旋，和理发师皮肤物品；理发师套装（8元），包含......
### [改变世界的超级AI](https://news.idmzj.com/article/77484.html)
> 概要: 大家好，我是文森。作为“ChatGPT学家“，上次刚刚和大家介绍完什么是ChatGPT，它都拥有什么能力。作为OpenAI发布的一款产品，ChatGPT刚刚面世的时候就惊艳到了我们所有人，并且我们预测......
### [《阿松》新作剧场动画第2弹将于7月21日上映！](https://news.idmzj.com/article/77485.html)
> 概要: 作为TV动画《阿松》6周年纪念作品的新作动画第2弹，《阿松~灵魂的章鱼烧派对和传说的留宿会~》的公开日期决定了。该片将于7月21日起在日本全国影院期间限定上映。
### [网友“晗晗qvq”向王一博道歉：我是粉丝 是口嗨](https://ent.sina.com.cn/s/m/2023-03-17/doc-imymczux4206331.shtml)
> 概要: 新浪娱乐讯 3月17日，网友“晗晗qvq”发微博向王一博道歉。“我是粉丝，是口嗨，我没有想到因为我的一个评论起这么大的风波，我本以为这件事一会就可以过去，没想到这件事已经到了我无法控制的局面，我真诚的......
### [金融“人上人”的时代，会走向终结吗？](https://www.huxiu.com/article/823304.html)
> 概要: 本文来自微信公众号：新潮沉思录（ID：xinchaochensi），作者：潮思，头图来自：《大时代》剧照前段时间，中央纪委国家监察委网站发布文章，其中十六次点名金融，还有那引起高度关注的表述，如破除“......
### [尖货爆品享专属低价、以旧换新立减10%，京东家电家居春季家装节即将上线](http://www.investorscn.com/2023/03/17/106291/)
> 概要: 随着“扩大消费”、“提振市场”、“创新消费”成为开年热词，消费市场也出现了明显的回暖势头。特别是在阳春3月，众多有家装改造想法的消费者都开始行动起来，家电家居市场呈现出一派欣欣向荣的景象。在这样的大背......
### [《Toads of the Bayou》Steam页面 明年发售](https://www.3dmgame.com/news/202303/3865020.html)
> 概要: 今日（3月17日），Roguelike牌组构筑游戏《Toads of the Bayou》，游戏暂不支持简体中文，预计于2024年发售，感兴趣的玩家可以点击此处进入商店页面。游戏介绍：在回合制战术和R......
### [坐上GPT-4的副驾，微软又行了](https://www.huxiu.com/article/823517.html)
> 概要: 出品｜虎嗅科技组作者｜齐健编辑｜陈伊凡头图｜电影《速度与激情5》2022年底，ChatGPT刚刚问世时，只是震动了AI业界。到2023年1月，微软、谷歌开始了关于对话大模型的消息大战。到了2月，不仅是......
### [不要给客户选择，要替客户做选择](https://www.huxiu.com/article/823555.html)
> 概要: 本文来自微信公众号：达叔天演论（ID：Uncle-Da8），作者：达叔，头图来自：《赌神（1989）》剧照一昨天出门，见一个医疗潜在合作者（为方便起见，下文皆简称为医疗），年轻人，出来创业的。医疗说：......
### [应对甲流，香雪抗病毒口服液有依据](http://www.investorscn.com/2023/03/17/106296/)
> 概要: 近期甲流来势汹汹,抗病毒类药物再次被关注,药店里,相关品类产品被放在最醒目的位置,店员介绍,中成药类抗病毒产品比较受消费者欢迎。据香雪制药介绍,或是受到甲流等多种上呼吸道疾病多发因素影响,自2月中下旬......
### [潮袜品牌视觉升级](https://www.zcool.com.cn/work/ZNjQ0ODk5NDQ=.html)
> 概要: 版权声明：2021年在公司做的潮袜项目2个飞机稿分享纪念，禁止搬运禁止转发。版权归属“松鼠跃动传媒集团”。———————————————————————————————————————————————......
### [海外new things | 资金陷入困境的电动汽车公司「Arrival」获得3亿美元以维持运营，仍在寻求更多资金](https://36kr.com/p/2170141766283529)
> 概要: 海外new things | 资金陷入困境的电动汽车公司「Arrival」获得3亿美元以维持运营，仍在寻求更多资金-36氪
### [Chat AI｜AI大牛颜水成加入「智源研究院」，任访问首席科学家](https://36kr.com/p/2175261642125829)
> 概要: Chat AI｜AI大牛颜水成加入「智源研究院」，任访问首席科学家-36氪
### [视频：42岁宋智孝现身机场出国 中长发造型回春状态佳](https://video.sina.com.cn/p/ent/2023-03-17/detail-imymehax0747243.d.html)
> 概要: 视频：42岁宋智孝现身机场出国 中长发造型回春状态佳
### [为生育存一丸“后悔药”，女性冻卵自由成本几何？| 焦点分析](https://36kr.com/p/2175268328943875)
> 概要: 为生育存一丸“后悔药”，女性冻卵自由成本几何？| 焦点分析-36氪
### [构筑消费环境 提振消费信心 —渤海人寿北京朝阳支公司开展3.15宣教活动](http://www.investorscn.com/2023/03/17/106301/)
> 概要: 3.15消费者权益保护教育宣传周期间，渤海人寿北京朝阳支公司围绕“构筑诚信消费环境 提振金融消费信心”的活动主题，多措并举开展系列消费者权益保护宣传教育活动，以开设总经理接待日、进办公楼、进银行网点等......
### [轻改TV动画《狂怒的暴食》公布预告 年内播出](https://www.3dmgame.com/news/202303/3865040.html)
> 概要: 一色一凛所著轻小说改编的同名 TV 动画《狂怒的暴食~只有我突破了等级这概念~》的官方网站公开了动画的主要配音演员、预告视觉图、首个先导预告视频以及将于 2023年首播的消息。动画预告：动画视觉图：配......
### [吼浪工作室发布通告 永久终止与姜广涛等的合作](https://ent.sina.com.cn/s/m/2023-03-17/doc-imymenkv0622509.shtml)
> 概要: 新浪娱乐讯 17日，吼浪工作室发布通告及联合声明，永久终止与姜广涛、宋明、王雪、宋扬在所有层面的合作关系，并配合司法机关追究其全部责任。　　公司表示：自2021年起遭遇了由公司人员宋明，王雪及外部人员......
### [伞柄灯光 + 伞面 105cm 大直径：大嘴猴自动雨伞 29.9 元新低（减 60 元）](https://lapin.ithome.com/html/digi/680457.htm)
> 概要: 大嘴猴自动雨伞日常售价为 89.9 元，下单领取 60 元优惠券，到手价为 29.9 元：天猫大嘴猴 晴雨伞伞柄灯光 + 伞面 105cm 大直径券后 29.9 元领 60 元券伞柄支持灯光 + 伞面......
### [新海诚晒出北京之旅 游戏人生2遥遥无期](https://news.idmzj.com/article/77488.html)
> 概要: 新海诚晒出北京之旅、Animate池袋翻新开业、游戏人生何时能出第二季、每周游戏喜加N
### [京东京造这五年：超100个品类年均销售额翻三倍以上](http://www.investorscn.com/2023/03/17/106304/)
> 概要: 2022年销售额同比增长60%、100个以上品类年均销售额增长超过300%、新品开发成功率超过90%、帮助工厂平均降低30天库存周转……在3月17日举办的“向新而生 造未来——京东京造五周年暨京东自有......
### [IPO迎更强监管，证监会现场检查强化“申报即担责”](https://www.yicai.com/news/101704848.html)
> 概要: 现场检查到底是怎么查的？查什么？听听检查组组长怎么说
### [阔诺新连载哒！3月新连载漫画不完全指北第二期](https://news.idmzj.com/article/77491.html)
> 概要: 隐藏身份的吸血鬼女高中生，偷偷的爱上了同校的不良少年；目标是向魔族复仇的勇者，转生成了魔王的儿子？！精灵与矮人相爱，跨越种族的爱情将何去何从？请看本周指北~~
### [如何研判欧盟《关键原材料法案》减少稀土和锂对外依赖意图？](https://www.yicai.com/news/101704857.html)
> 概要: “我认为这些目标中的很多都不会发生，那种想法太过有雄心。”
### [复旦再推AI文本检测工具“谛听” 识别ChatGPT成功率达80%](https://www.yicai.com/news/101704889.html)
> 概要: 基于黑盒假设，无需大规模监督训练，就能检测AI生成的文本。据该软件的英文页面介绍，对于ChatGPT生成文本的检测率达到80%左右。
### [《流浪地球 2》密钥再次延期，将延长上映至 4 月 15 日](https://www.ithome.com/0/680/494.htm)
> 概要: IT之家3 月 17 日消息，电影《流浪地球 2》今日宣布密钥再次延期至 4 月 15 日。此前，电影《流浪地球 2》已宣布密钥延期至 3 月 21 日。电影《流浪地球》和《流浪地球 2》已分别在 2......
### [车站和线路不断关停，公路客运如何“优化”出路](https://www.yicai.com/news/101704926.html)
> 概要: 从业者们都知道公路客运曾经的辉煌不可再现，但也认为像疫情三年那样最艰难的时期已经过去。
### [黄金阵容！刘德华将导演成龙吴京主演的新片](https://www.3dmgame.com/news/202303/3865056.html)
> 概要: 日前，在电影《流浪地球2》香港幕后交流会上，吴京透露由刘德华导演，成龙、吴京合作主演的新片可能已经在路上。在交流会上，吴京表示自己喜欢做没有做过的事情，“我个人不太喜欢重复，下一部可能是文艺片，不知道......
### [又一国取消对中国旅客入境限制，还有哪些国要查核酸](https://www.yicai.com/news/101704998.html)
> 概要: 目前从美国和加拿大回国的核酸报告提供尚未取消。
### [OPPO Enco Free3 真无线耳机官宣：首创竹纤维振膜，49dB 超深度降噪](https://www.ithome.com/0/680/509.htm)
> 概要: 感谢IT之家网友grass罗雨滋、偏科骚黄4100只眼、肖战割割的线索投递！IT之家3 月 17 日消息，OPPO 将于 3 月 21 日 14:00 的新品发布会上，除了 OPPO Find X6 ......
### [华大北斗芯片亮相纽伦堡国际嵌入式展EW2023](http://www.investorscn.com/2023/03/17/106308/)
> 概要: 2023年3月14日至16日,华大北斗携公司自主研发的系列芯片和模块产品亮相全球最大的嵌入式行业展会——德国纽伦堡国际嵌入式展览会(embedded world2023)......
### [诺安基金谈降准：下调幅度25BP则符合预期](https://finance.sina.com.cn/stock/roll/2023-03-17/doc-imymesst0515083.shtml)
> 概要: 为推动经济实现“质”的有效提升和“量”的合理增长，打好宏观政策组合拳，提高服务实体经济水平，保持银行体系流动性合理充裕，中国人民银行决定于2023年3月27日降低金融机构...
### [“东数西算”工程进入全面建设阶段：8 个国家算力枢纽节点建设已全部开工](https://www.ithome.com/0/680/515.htm)
> 概要: IT之家3 月 17 日消息，据央视新闻报道，截至目前，“东数西算”工程的 8 个国家算力枢纽节点建设已全部开工，“东数西算”工程从系统布局进入全面建设阶段。在甘肃，庆阳国家数据中心集群开工建设，建成......
### [农银汇理基金：央行全面降准，提高服务实体经济水平](https://finance.sina.com.cn/china/2023-03-17/doc-imymesst0516818.shtml)
> 概要: 今日事件 3月17日消息，中国人民银行决定于2023年3月27日下调金融机构存款准备金率0.25个百分点（不含已执行5%存款准备金率的金融机构）。
### [降准落地：意料之外，情理之中！预计释放超5000亿元中长期资金](https://finance.sina.com.cn/china/2023-03-17/doc-imymewyw1252142.shtml)
> 概要: 来源：上海证券报 “意料之外，情理之中。”多位市场分析人士这样形容本次降准的到来。 3月17日，人民银行宣布于3月27日降低金融机构存款准备金率0...
### [万亿巨头的对决：微软、谷歌战事升级，从搜索到办公，谁能赢得AI？](https://finance.sina.com.cn/jjxw/2023-03-17/doc-imymewyw1266888.shtml)
> 概要: 每经记者 文巧    每经编辑 谭玉涵     微软和谷歌围绕AI的“战役”再次升级，在新版必应开启了搜索之战后，这两家科技巨头的战场又转向了办公领域。
### [刚刚，中金所发布！利好来了](https://finance.sina.com.cn/china/2023-03-17/doc-imymewyw1275462.shtml)
> 概要: 来源：证券时报 经中国证监会同意，中国金融期货交易所进一步调整股指期货交易安排，自2023年3月20日起，将股指期货平今仓交易手续费标准调整为成交金额的万分之二点三。
# 小说
### [帝国回忆](https://book.zongheng.com/book/1054893.html)
> 作者：岛风雅

> 标签：科幻游戏

> 简介：100年风雨兼程，筚路蓝缕，玉汝于成，留下了美好的回忆，一起回忆，记录历史

> 章节末：关于作者感想的有关问题

> 状态：完本
# 论文
### [CoRTX: Contrastive Framework for Real-time Explanation | Papers With Code](https://paperswithcode.com/paper/cortx-contrastive-framework-for-real-time)
> 日期：5 Mar 2023

> 标签：None

> 代码：https://github.com/ynchuang/cortx-720

> 描述：Recent advancements in explainable machine learning provide effective and faithful solutions for interpreting model behaviors. However, many explanation methods encounter efficiency issues, which largely limit their deployments in practical scenarios. Real-time explainer (RTX) frameworks have thus been proposed to accelerate the model explanation process by learning a one-feed-forward explainer. Existing RTX frameworks typically build the explainer under the supervised learning paradigm, which requires large amounts of explanation labels as the ground truth. Considering that accurate explanation labels are usually hard to obtain due to constrained computational resources and limited human efforts, effective explainer training is still challenging in practice. In this work, we propose a COntrastive Real-Time eXplanation (CoRTX) framework to learn the explanation-oriented representation and relieve the intensive dependence of explainer training on explanation labels. Specifically, we design a synthetic strategy to select positive and negative instances for the learning of explanation. Theoretical analysis show that our selection strategy can benefit the contrastive learning process on explanation tasks. Experimental results on three real-world datasets further demonstrate the efficiency and efficacy of our proposed CoRTX framework.
### [DPText-DETR: Towards Better Scene Text Detection with Dynamic Points in Transformer | Papers With Code](https://paperswithcode.com/paper/dptext-detr-towards-better-scene-text)
> 日期：10 Jul 2022

> 标签：None

> 代码：https://github.com/ymy-k/dptext-detr

> 描述：Recently, Transformer-based methods, which predict polygon points or Bezier curve control points to localize texts, are quite popular in scene text detection. However, the used point label form implies the reading order of humans, which affects the robustness of Transformer model. As for the model architecture, the formulation of queries used in decoder has not been fully explored by previous methods. In this paper, we propose a concise dynamic point scene text detection Transformer network termed DPText-DETR, which directly uses point coordinates as queries and dynamically updates them between decoder layers. We point out a simple yet effective positional point label form to tackle the side effect of the original one. Moreover, an Enhanced Factorized Self-Attention module is designed to explicitly model the circular shape of polygon point sequences beyond non-local attention. Extensive experiments prove the training efficiency, robustness, and state-of-the-art performance on various arbitrary shape scene text benchmarks. Beyond detector, we observe that existing end-to-end spotters struggle to recognize inverse-like texts. To evaluate their performance objectively and facilitate future research, we propose an Inverse-Text test set containing 500 manually labeled images. The code and Inverse-Text test set will be available at https://github.com/ymy-k/DPText-DETR.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
