---
title: 2021-02-18-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.VerzascaValley_EN-CN0835346354_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-02-18 20:16:35
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.VerzascaValley_EN-CN0835346354_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Visualization of 40M Cell Towers](https://alpercinar.com/open-cell-id/)
> 概要: Visualization of 40M Cell Towers
### [张芷溪深夜曝男友金瀚“出轨” 怒骂其渣男 后发文否认：喝多误会了](https://new.qq.com/omn/ENT20210/ENT2021021800239700.html)
> 概要: 张芷溪深夜曝男友金瀚“出轨” 怒骂其渣男 后发文否认：喝多误会了
### [组图：全智贤拍户外运动写真 线条紧致活力十足](http://slide.ent.sina.com.cn/star/k/slide_4_704_352747.html)
> 概要: 组图：全智贤拍户外运动写真 线条紧致活力十足
### [视频：女团本人！伊能静18岁儿子哈利与表姐跳女团舞 妩媚却不违和](https://video.sina.com.cn/p/ent/2021-02-18/detail-ikftssap6283558.d.html)
> 概要: 视频：女团本人！伊能静18岁儿子哈利与表姐跳女团舞 妩媚却不违和
### [欧洲消费者组织投诉TikTok：未过滤不当内容保护儿童](https://www.3dmgame.com/news/202102/3808768.html)
> 概要: 近日据外媒彭博社报道，欧洲消费者组织BEUC向欧盟委员会和欧洲消费者保护机构网络投诉TikTok，称其涉嫌违反欧盟消费者法律，并且未能保护儿童免受隐藏广告和不当内容的侵害。BEUC在投诉中指出了数项问......
### [我们分析了2020年全球500家独角兽企业，发现这些趋势](https://www.huxiu.com/article/409944.html)
> 概要: 本文来自微信公众号：中欧商业评论（ID：ceibs-cbr），作者：何涧石，出品：中欧商业评论产业研究院，原文标题：《我们分析了2020年全球的500家独角兽企业，发现了......》，头图来自：un......
### [细田守《龙与雀斑公主》全新PV 少女与龙的奇幻之旅](https://acg.gamersky.com/news/202102/1363379.shtml)
> 概要: 细田守新作动画电影《龙与雀斑公主》公开了全新的特报视频和视觉图，在其中细田守为大家展现了一个充满了极具他风格的奇幻世界，少女与龙的故事在其中徐徐展开。
### [视频：李现欧阳娜娜偶遇晒合照 网友“1分钟内我要知道哪条街”](https://video.sina.com.cn/p/ent/2021-02-18/detail-ikftpnny7279936.d.html)
> 概要: 视频：李现欧阳娜娜偶遇晒合照 网友“1分钟内我要知道哪条街”
### [「电锯人」官网头像合集公开](http://acg.178.com//202102/407617071061.html)
> 概要: 近日，漫画「电锯人」官方发布了官网头像合集。漫画「电锯人」（链锯人）是漫画家藤本树创作的漫画作品。本作讲述了主角电次与链锯恶魔波奇塔相依为命，在残酷的世界中活下去的故事，被称为“震荡世界的黑暗英雄物语......
### [下一个被收购的半导体巨头会是谁？](https://www.huxiu.com/article/409970.html)
> 概要: 本文来自微信公众号：半导体行业观察（ID：icbank），作者：邱丽婷，头图来自：视觉中国并购行为这几年来似乎有愈演愈烈的趋势。近日，据businessKorea报道，三星电子正积极计划进行并购交易，......
### [北川景子主演四月新剧 搭档瑛太饰演新婚夫妻](https://ent.sina.com.cn/s/j/2021-02-18/doc-ikftssap6311229.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道，人气女星北川景子接拍四月开播TBS日剧《离婚活动》，这是她继2011年1月《LADY~最后的犯罪心理分析官~》时隔十年再次主演金曜档日剧，讲述一对因命运安排相遇，交往0天闪......
### [OPPO 官宣将带来全新交互的充电技术，2 月 23 日上海见](https://www.ithome.com/0/535/612.htm)
> 概要: IT之家2月18日消息 OPPO 方面今日宣布要在 上海 MWC 公布全新交互的充电技术，不排除展示一些趣味性新技术的可能，例如近期多有热点的隔空充电。此外，OPPO 也表示将在闪充生态上公布更多内容......
### [D3P《MAGLAM LORD》角色新演示：机械人偶登场](https://www.3dmgame.com/news/202102/3808786.html)
> 概要: 今日（2月18日），D3P官方公开了铸剑物语精神续作”《MAGLAM LORD》「被遗忘的机械人偶」莫布演示影像（CV：石川界人），展示了与该角色有关的一些细节，该作将于2021年3月18日正式发售......
### [动画「通灵王」追加声优：泽海阳子](http://acg.178.com//202102/407625230505.html)
> 概要: TV动画「通灵王」公开了最新追加的角色リゼルグ・ダイゼル及其声优泽海阳子。该动画将于今年4月播出。STAFF监督：古田丈司系列构成：米村正二角色设计：佐野聡彦道具设定：柴田ユウジ美术监督：木村仁哉美术......
### [《尼尔Reincarnation》已上线 2B/A2新服装亮相](https://shouyou.3dmgame.com/news/53975.html)
> 概要: 今日（2月18日），《尼尔》手游《尼尔Reincarnation》现已在iOS和Android平台开始运营，与《尼尔：自动人形》的联动活动也已经开启，2B、9S、A2在手游中的新服装也正式公布。2B、......
### [微软收购B社是谷歌关闭stadia内部工作室的一个因素](https://www.3dmgame.com/news/202102/3808790.html)
> 概要: 据报道，微软对贝塞斯达的收购是谷歌决定关闭其内部Stadia游戏工作室的因素之一。据外媒Kotaku的消息，谷歌在2月初关闭位于洛杉矶和蒙特利尔的两家内部游戏工作室后，在员工问答环节，stadia总经......
### [视频：孙俪发文透露邓超练舞日常“全家人都要记得离他远一点”](https://video.sina.com.cn/p/ent/2021-02-18/detail-ikftssap6337911.d.html)
> 概要: 视频：孙俪发文透露邓超练舞日常“全家人都要记得离他远一点”
### [漫画「蓝色时期」作者发布第九卷重版贺图](http://acg.178.com//202102/407632164895.html)
> 概要: 漫画「蓝色时期」作者山口飞翔发布了第九卷的重版贺图。本作漫画1-8卷重版已于2月12日发行，第9卷重版将于3月12日发售。漫画「蓝色时期」于2017年12月开始在「月刊afternoon」上连载，同名......
### [宇髄天元登场！《鬼灭之刃》新作动画花街篇制作决定](https://news.dmzj1.com/article/70140.html)
> 概要: 根据吾峠呼世晴原作漫画改编的TV动画《鬼灭之刃》确定将要推出新作动画——花街篇，动画预计将于2021年开播。
### [贾玲谈《李焕英》创作初衷，提母亲几度哽咽，曾说自己的快乐永远缺了一角](https://new.qq.com/omn/20210218/20210218A07CG500.html)
> 概要: 2月18日，由贾玲导演及主演的电影《你好，李焕英》票房突破30亿，她也成为中国票房最高的女导演，下午她和主演张小斐直播，和大家讲述了电影背后的许多故事。            贾玲今日身着红白色拼接卫......
### [游戏如何长效且优雅地蹭热点？](https://www.tuicool.com/articles/UNnaMjM)
> 概要: 编者按：本文来自微信公众号“Morketing”（ID:Morketing），作者：April，36氪经授权发布。「曾经火爆一时，纯因“偶然事件”成为爆款的休闲游戏，结局基本殊途同归，前段时间引爆社交......
### [Baby亲吻神秘男被疑和黄晓明离婚，男方身份被扒，早年曾发文澄清](https://new.qq.com/omn/20210218/20210218A07ZA100.html)
> 概要: 2月18日，网络上突然曝出Baby出轨有新欢的消息，网友爆料晒出了Baby亲吻神秘男的照片，表示“不想（和黄晓明）过了就离婚，好老婆好妈妈”。言语之中暗示Baby有出轨的可能性。           ......
### [美少女游戏《蓝色反射》TV动画化决定](https://news.dmzj1.com/article/70141.html)
> 概要: TV动画《蓝色反射：RAY/澪》将于2021年4月开始播出
### [杨幂：《刺杀小说家》让我“刚”了一回](https://new.qq.com/omn/20210218/20210218A094V500.html)
> 概要: 对杨幂来说，刚结束的2020年是极为丰富的一年，拍摄完成了影视剧《斛珠夫人》，在独白剧《听见她说完美女孩》中饰演了机器人小爱，还挑战了脱口秀，在奇葩说中输出观点，最新电影作品《刺杀小说家》也将在今年春......
### [《索尼克》真人电影续篇公布 2022年4月8日上映](https://news.dmzj1.com/article/70143.html)
> 概要: 日本人气游戏《索尼克》系列的好莱坞真人电影续篇的标题和logo已经公开了
### [Lithuania plans to hold drills in case of accident at the Belarus nuclear plant](https://www.lrt.lt/en/news-in-english/19/1346034/lithuania-plans-to-hold-evacuation-drills-after-belarus-launches-nuclear-plant)
> 概要: Lithuania plans to hold drills in case of accident at the Belarus nuclear plant
### [沈腾为什么在《王牌》狂cue鹿晗？得知背后真正原因，王牌家族有心了](https://new.qq.com/omn/20210218/20210218A09UZ400.html)
> 概要: 《王牌对王牌》是一档深受网友喜欢的综艺节目，众所周知王牌家族的成员有沈腾，贾玲，华晨宇，关晓彤，而在第6季节目中则加入了一位新的成员，他就是时代少年团的宋亚轩。其实除了这5位外，还有一位也是王牌家族的......
### [李克强听取2020年全国两会建议提案办理情况汇报](https://finance.sina.com.cn/china/2021-02-18/doc-ikftssap6472874.shtml)
> 概要: 原标题：李克强主持召开国务院常务会议 听取2020年全国两会建议提案办理情况汇报 证券时报网讯，据央视新闻联播18日消息，李克强主持召开国务院常务会议...
### [我国1月CPI再次转负 政策要转的“弯”应该起伏不大](https://finance.sina.com.cn/roll/2021-02-18/doc-ikftpnny7460065.shtml)
> 概要: 原标题：CPI再次转负 政策要转的“弯”应该起伏不大 冉学东 1月份的CPI数据让许多预计通胀形势严峻的分析人士大跌眼镜，不是他们预计的消费价格上涨，而是同比下降0.3%。
### [第132届广交会将启用新展馆 展览面积达62万平方米](https://finance.sina.com.cn/china/2021-02-18/doc-ikftpnny7461337.shtml)
> 概要: 中新社广州2月18日电 （记者 程景伟）广交会（全称“中国进出口商品交易会”）新闻中心18日通报称，广交会展馆四期项目将于第132届广交会投入使用。
### [牛年春运返乡迁徙指数降50% 热门迁入城市有哪些](https://finance.sina.com.cn/china/2021-02-18/doc-ikftssap6481528.shtml)
> 概要: 原标题：牛年春运返乡迁徙指数降50%，热门迁入城市有哪些 百度地图迁徙大数据平台显示，北京、上海、广州、深圳、天津、杭州、成都、重庆等地城内出行强度高于过去两年...
### [曝光 中国志愿者协会等10个社会组织上黑名单](https://finance.sina.com.cn/wm/2021-02-18/doc-ikftpnny7465860.shtml)
> 概要: 原标题：曝光！这10个社会组织，上黑名单 据民政部社会组织管理局消息，民政部18日公布了2021年第一批涉嫌非法社会组织名单，包括中国志愿者协会、中国美学研究会...
### [身材 + 营养管理，韩国别样妙奶昔 / 果冻 6 瓶 29 元（减 50 元）](https://lapin.ithome.com/html/digi/535663.htm)
> 概要: 控卡饱腹补营养，韩国别样妙摇摇奶昔6瓶报价79元，限时限量50元券，实付29元包邮，领券并购买。天猫控卡饱腹补营养，韩国别样妙摇摇奶昔6瓶券后29元领50元券天猫0卡0脂0蔗糖，韩国别样妙魔芋果冻15......
### [海关总署：支持中欧班列发展畅通陆上国际贸易大通道](https://finance.sina.com.cn/china/2021-02-18/doc-ikftssap6485089.shtml)
> 概要: 海关总署：全力以赴促进外贸稳增长，支持中欧班列发展畅通陆上国际贸易大通道 据海关总署官网消息，2月18日，海关总署署长倪岳峰主持召开总署第三十六次形势分析及工作督查...
### [AMD Radeon RX 6700系列将于3月18日发布](https://www.3dmgame.com/news/202102/3808818.html)
> 概要: 之前一直传言AMD Radeon RX 6700系列显卡将在三月份推出，很多渠道的消息都指AMD的发布会将在3月中旬举行。目前具体日期已经得到Cowcotland的确认，AMD将于3月18日发布其下一......
### [登陆火星，美国毅力号的“死亡7分钟”](https://www.tuicool.com/articles/VJj6Rff)
> 概要: 本文来自微信公众号：果壳（ID：Guokr42），作者：太空精酿，原文标题：《死亡七分钟！毅力号明天凌晨登陆火星》，题图来自：视觉中国天问一号发回的首张近距离火星照片 | CNSA火星，太阳系里的那颗......
# 小说
### [六道共主](http://book.zongheng.com/book/431208.html)
> 作者：香烟的世界

> 标签：奇幻玄幻

> 简介：轮回盘碎裂，六道失衡，为弥补过错，他踏上了灵魂自赎的征程。历尽千辛万苦，九死一生，随着散落到各处的轮回盘碎片陆续被寻回，一个惊天阴谋也随之露出了冰山一角......新书《不休神座》已发

> 章节末：1019章  身化轮回（大结局）

> 状态：完本
# 论文
### [Efficient Intent Detection with Dual Sentence Encoders](https://paperswithcode.com/paper/efficient-intent-detection-with-dual-sentence)
> 日期：10 Mar 2020

> 标签：INTENT DETECTION

> 代码：https://github.com/PolyAI-LDN/polyai-models

> 描述：Building conversational systems in new domains and with added functionality requires resource-efficient models that work under low-data regimes (i.e., in few-shot setups). Motivated by these requirements, we introduce intent detection methods backed by pretrained dual sentence encoders such as USE and ConveRT.
### [Multi-Time Attention Networks for Irregularly Sampled Time Series](https://paperswithcode.com/paper/multi-time-attention-networks-for-irregularly-1)
> 日期：25 Jan 2021

> 标签：TIME SERIES

> 代码：https://github.com/satyanshukla/mTANs

> 描述：Irregular sampling occurs in many time series modeling applications where it presents a significant challenge to standard deep learning models. This work is motivated by the analysis of physiological time series data in electronic health records, which are sparse, irregularly sampled, and multivariate.
