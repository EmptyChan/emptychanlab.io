---
title: 2021-05-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ICanHearIt_EN-CN3252355519_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-27 22:58:49
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ICanHearIt_EN-CN3252355519_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Mozilla Thunderbird 以明文存储密钥，目前问题已被修复](https://www.oschina.net/news/143297/thunderbird-stores-keys-in-plaintext)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Thunderbird 是 Mozilla 旗下的开源电子邮件客户端，在过去几个月里，经过代码重构之后的版本一直以纯文本形式保存一些用户......
### [Google Chrome 现在可以通过地址栏运行更多命令](https://www.oschina.net/news/143284/chrome-publish-more-actions)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Google 在最新发布的 Chrome 浏览器中增加了一批新的 Chrome Actions，以便对该功能进行早期测试。Chrome ......
### [微软将 GPT-3 应用于 PowerFx 语言以简化编码](https://www.oschina.net/news/143298/microsoft-gpt-3-power-fx-build-2021)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>微软在 Build 2021 大会上公布了由 GPT-3 驱动的客户产品的首批功能。GPT-3 是由微软投资的 OpenAI 开发的强大......
### [Fedora Cloud 35 将默认使用 Btrfs 文件系统](https://www.oschina.net/news/143289/fedora-cloud-35-will-use-btrfs)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>根据最新的变更提案显示，Fedora Cloud 35 将默认使用 Btrfs 文件系统。自 F33 以来，Fedora Worksta......
### [孙正义“卷土重来” ？](https://www.huxiu.com/article/430704.html)
> 概要: 本文来自微信公众号：巴伦周刊（ID：barronschina），作者：埃里克·J·萨维茨，翻译：喇珺宜，编辑：郭力群，题图来自：视觉中国过去18个月，软银集团（SFTBY）首席执行官孙正义（Masay......
### [瓶装水的旧王和新王](https://www.huxiu.com/article/430789.html)
> 概要: 来源｜远川研究所（ID：caijingyanjiu）作者｜李墨天头图｜视觉中国在中美之外的绝大多数国家，消费品一直是“首富密度”最高的行业。优衣库创始人柳井正是日本首富，LVHM集团老板阿诺特是法国首......
### [A.I. Is Solving the Wrong Problem](https://onezero.medium.com/a-i-is-solving-the-wrong-problem-253b636770cd)
> 概要: A.I. Is Solving the Wrong Problem
### [从颅顶“精致”到私处，你也有这样的容貌焦虑吗？](https://news.dmzj1.com/article/71012.html)
> 概要: 护肤化妆、医美整形对于大多数人来说早已算不得什么新鲜词。但是，人类对“美”的追求永无止境，越来越多匪夷所思的整容整形项目出现在了大众的面前……
### [剧场版「美少女战士Eternal」插入曲动画MV公开](http://acg.178.com/202105/416082149725.html)
> 概要: 近日，剧场版动画「美少女战士Eternal」公开了其插入曲「Moon Effect」的完整版动画MV，该MV将收录于在6月30日发售的本作BD/DVD中。「美少女战士 Eternal」插入曲动画MV「......
### [P站美图推荐——画师【yae】狼ト生キル特辑](https://news.dmzj1.com/article/71013.html)
> 概要: 相信大家肯定都在不少地方见过这位画师（特别是某些绘画网站的广告上），但是画师本人的水平可远远超过了那些绘画网站的水平哦~（最后有这位神画师用鼻子画的画，只能说不愧是神画师的作品！）
### [「赛马娘Pretty Derby」第1卷BD发售祝贺PV公开](http://acg.178.com/202105/416084684789.html)
> 概要: 由CyGames的同名手游改编，P.A.Works担任制作的电视动画「赛马娘PrettyDerby」第二季于近日公开了第1卷BD的发售祝贺PV，BD已于5月26日发售。「赛马娘PrettyDerby」......
### [日本消费电子巨头利润多创新高 躲疫情在家玩游戏](https://www.3dmgame.com/news/202105/3815317.html)
> 概要: 近日据日媒报道，虽然疫情形势不见好转，不过可以看见的是新冠疫情确实改变了人们生活方式，促进了日本消费电子巨头利润增长。据日本共同社报道，除东芝公司外的日本7家消费电子巨头2020财年（2020年4月至......
### [这届年轻人，为什么不爱吃周黑鸭了？](https://www.huxiu.com/article/430802.html)
> 概要: 本文来自微信公众号：新眸（ID：xinmouls），作者：亚婷，头图来自：视觉中国绝味、周黑鸭、煌上煌“卤味三巨头”之间的竞争一直是行业内的关注焦点。成立于2006年的周黑鸭，是一家鸭类、鹅类产品和素......
### [轻小说「Re：从零开始的异世界生活」第27卷封面公开](http://acg.178.com/202105/416087528392.html)
> 概要: 近日，轻小说「Re：从零开始的异世界生活」公开了第27卷封面图，该卷将于6月25日发售。「Re:从零开始的异世界生活」是轻小说家长月达平著作，插画家大冢真一郎负责插画，MF文库J所属的轻小说......
### [网易又又又刷屏了！彩虹屁背后藏着什么秘密？](https://www.tuicool.com/articles/bue6viM)
> 概要: 哈喽大家好，我是无色透明的碳基生物关关。论H5，还是网易牛，昨天又又又又又刷屏了。上到我妈，下到我16岁的00后表妹，集体化身欢天喜地七仙女+金刚葫芦娃，聊天第一句就是——你什么色。古早味的配对地摊在......
### [迈克·迈尼昂，快来看看米兰新门将的成长轨迹](https://bbs.hupu.com/43187958.html)
> 概要: 迈克·迈尼昂(Mike·Maignan)​1995.7.3 190cm 80kg 门将 25岁        迈尼昂​出生于法属圭亚那的首都卡宴，是法国的一个海外地区和省，位于南美洲，这座城市坐落在大
### [杂志「MEN’S NON-NO」最新封面公开](http://acg.178.com/202105/416097618262.html)
> 概要: 杂志「MEN’S NON-NO」公开了最新封面，本次的封面为「鬼灭之刃 无限列车篇」。剧场版动画「鬼灭之刃：无限列车篇」改编自日本漫画家吾峠呼世晴创作的人气漫画「鬼灭之刃」，讲述了主角组灶门炭治郎等人......
### [你床底下的落灰宠物小精灵卡，现在可能值套房](https://www.huxiu.com/article/430917.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡题图 | pinterest本文首发于虎嗅青年文化公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。直到最近，我......
### [组图：蔡徐坤身穿白衬衫现身机场 搭配格纹西裤优雅帅气](http://slide.ent.sina.com.cn/y/slide_4_704_357155.html)
> 概要: 组图：蔡徐坤身穿白衬衫现身机场 搭配格纹西裤优雅帅气
### [村上春树名作《驾驶我的车》电影定档 8.20日上映](https://www.3dmgame.com/news/202105/3815343.html)
> 概要: 根据著名作家村上春树短篇名作《驾驶我的车》改编的同名电影官方5月27日今天宣布定档，预定8月20日正式上映，敬请期待。·《驾驶我的车》是由滨口龙介执导并编剧，西岛秀俊主演的剧情片，讲述了家福悠介（西岛......
### [《海贼王》1014话情报：人生的蹩脚演员](https://acg.gamersky.com/news/202105/1392136.shtml)
> 概要: 《海贼王》漫画1014话情报来啦，保皇宣布了凯多的宣言，她说路飞和赤鞘九侠都被凯多打败。锦卫门十分愤怒的砍倒了再次变成御田的勘十郎。
### [长泽雅美发行20周纪念写真集 展现私密生活面孔](https://ent.sina.com.cn/s/j/2021-05-27/doc-ikmxzfmm4990951.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 人气女星长泽雅美为纪念出道20周年时隔九年发行写真集《长泽雅美20th Anniversary PHOTO BOOK（暂定名）》，这是她个人第十部写真集，也是她三十岁后第......
### [‘Bomb Threat' That Justified Belarus Hijacking Came 24 Minutes After](https://www.thedailybeast.com/bomb-threat-cited-in-belarus-hijacking-came-24-minutes-after)
> 概要: ‘Bomb Threat' That Justified Belarus Hijacking Came 24 Minutes After
### [视频：《天官赐福》剧方回应选角“请勿轻信网络传言”](https://video.sina.com.cn/p/ent/2021-05-27/detail-ikmxzfmm4996636.d.html)
> 概要: 视频：《天官赐福》剧方回应选角“请勿轻信网络传言”
### [英伟达：RTX 30系列显卡的短缺可能会持续到下半年](https://www.3dmgame.com/news/202105/3815346.html)
> 概要: 由于全球芯片短缺等多种原因，英伟达的RTX 30系列显卡也成为难买电子产品之一。所以玩家们有可能要等到今年年末前，才能用上RTX 30系列显卡。在2021年第一季度财报电话会议上，英伟达官方宣布，预计......
### [B2B行业主题推广的闭环思维](https://www.tuicool.com/articles/QVjAJ3a)
> 概要: 编辑导语：PDCA分别指Plan、Do、Check、Action，是指任务行为在这四个环节的反复循环中解决问题、检查总结，最终处理结果进行反馈修正的闭环过程。而主题推广作为企业营销宣传的手段之一，也可......
### [OPPO Enco Free2 耳机参数公布：最大 42dB 降噪，续航 30 小时](https://www.ithome.com/0/553/953.htm)
> 概要: IT之家5 月 27 日消息 OPPO 将于今晚 6 点举行 OPPO Reno6 新品发布会，此前官方也宣布将发布OPPO Enco Free2 无线降噪耳机。刚刚，OPPO 在媒体沟通会上提前公布......
### [高校里的电竞梦！《我是大神仙》探访中国传媒大学](https://new.qq.com/omn/ACF20210/ACF2021052700906800.html)
> 概要: 每周四10点，腾讯视频独家热播的《我是大神仙》动画第二季已收获近11亿播放量，游戏玩家时江穿越仙界的游戏修仙之旅，也得到了越来越多仙友的喜爱与支持。在这个电竞方方面面地融入我们生活的时代，《我是大神仙......
### [Protasevich Street? Bucharest mulls changing address of Belarusian embassy](https://www.reuters.com/world/europe/protasevich-street-bucharest-mulls-changing-address-belarusian-embassy-2021-05-27/)
> 概要: Protasevich Street? Bucharest mulls changing address of Belarusian embassy
### [漫画《青之芦苇》动画化！2022年夏开播](https://news.dmzj1.com/article/71023.html)
> 概要: 根据偷跑消息，由小林有吾创作的漫画《青之芦苇》宣布了TV动画化的消息。有关本作动画的更多消息，还将于日后公开。
### [国家市场监管总局：对十荟团不正当价格行为案作出行政处罚，合并罚款 150 万元](https://www.ithome.com/0/553/967.htm)
> 概要: IT之家5 月 27 日消息 国家市场监管总局对北京十荟科技有限公司不正当价格行为案作出行政处罚决定，合并罚款 150 万元。IT之家获悉，2021 年 5 月 27 日，市场监管总局对北京十荟科技有......
### [《勇者斗恶龙》发布包括系列新作在内的多部作品](https://news.dmzj1.com/article/71024.html)
> 概要: SE在《勇者斗恶龙》35周年纪念特别节目中，宣布了包括系列新作《勇者斗恶龙12》在内的多条系列相关消息。
### [Klarna users are being signed in to random accounts](https://twitter.com/esraefe/status/1397842160607711232)
> 概要: Klarna users are being signed in to random accounts
### [快手“失手”，追不上海外老铁？](https://www.tuicool.com/articles/EBRrEb7)
> 概要: 本文首发于36氪出海网站，欢迎移步letschuhai.com查看更多全球化商业相关资讯。快手，重拾了出海赛道的目光。本周，快手发布了 2021 年第一季度财报。据最新财报数据，其海外市场第一季度月活......
### [杨元庆：联想肯定不造车，比华为还坚决](https://www.ithome.com/0/553/980.htm)
> 概要: IT之家5 月 27 日消息 5 月 27 日晚间消息，联想集团董事长兼 CEO 杨元庆今日在回答提问时表示，联想坚决不造车，比华为还坚决。“我们不会赶热闹”，杨元庆称，联想在 PC 等领域的赛道足够......
### [骊歌行：傅柔奉子成婚，与公主同一天结婚，身份让盛楚慕高攀不起](https://new.qq.com/omn/20210527/20210527A0C8ZO00.html)
> 概要: 傅柔跟盛楚慕两人感情线相当曲折，比起其他古装剧中的感情，这两人经历了很多，有许多次误会差点把两人彻底分开，好在误会终究是误会。只是一向谨慎的傅柔，在遇到盛楚慕之后，很多规矩都被打破，就连男女授受不亲这......
### [12 期免息：造梦者 100 档智能体感风扇 299 元再发车](https://lapin.ithome.com/html/digi/553983.htm)
> 概要: 【造梦者旗舰店】智能直流变频体感风扇 售价 699 元，限时限量 400 元券，实付 299 元包邮。店铺新客可领大约 45 元首单礼金（实付 250 元左右），支持 12 期免息，赠运费险。使用最会......
### [电视剧神犬小七泄露市民手机号：被判赔3.5万元](https://ent.sina.com.cn/v/m/2021-05-27/doc-ikmxzfmm5040528.shtml)
> 概要: 5月25日，裁判文书网公布了陶某与《神犬小七》出品方完美世界（北京）互动娱乐有限公司（以下简称“完美影视”）等网络侵权责任纠纷一审民事判决书，案号为（2020）鲁0113民初2504号。据悉，由于电视......
### [被扒出有14个女友的郝富申终于道歉，态度敷衍，难怪胡先煦王俊凯远离他](https://new.qq.com/omn/20210527/20210527A0D9K700.html)
> 概要: 要说今年塌房最彻底的，目前来说只有郝富申了，小小年纪在两天之内被曝光了非常多的女友，网友总结的是14个，还做出了郝富申与女朋友们的思维导图，很是细致。            事情发酵了两天之后，郝富申......
### [NVIDIA Reflex现已加入虚幻5 DLSS正在路上](https://www.3dmgame.com/news/202105/3815367.html)
> 概要: 就在AMD宣布在虚幻5上和Epic进行合作后，NVIDIA也发表了一个类似的博客，在里面NVIDIA也宣布在虚幻5的开发过程中他们和Epic深度合作的事情。Epic Games工程副总裁Nick Pe......
### [组图：张柏芝穿红色蛋糕裙明艳优雅 刘令姿曾可妮等女爱豆齐现身](http://slide.ent.sina.com.cn/star/slide_4_704_357189.html)
> 概要: 组图：张柏芝穿红色蛋糕裙明艳优雅 刘令姿曾可妮等女爱豆齐现身
### [《战国无双5》10名“固有武将”公开 可操作角色达37名](https://www.3dmgame.com/news/202105/3815369.html)
> 概要: 《战国无双5》10名拥有专属人设的“固有武将”公开，固有武将是指他们和之前已经公布过的27名无双武将相比，没有专用的蓄力技，在施展无双奥义时的一部分表现也有差异。在加入固有武将之后，本作的可操作角色达......
### [戈壁没有狼性，但真有狼](https://www.tuicool.com/articles/IVBNJrA)
> 概要: 大佬们长点心吧。全文共 5807 字，阅读大约需要 15 分钟作者 | 仉泽翔编辑 | 王晓玲2007年5月，热爱运动的“地产一哥”王石组织了一场穿越罗布泊的徒步活动。罗布泊在若羌县境东北部，诞生于第......
### [Balancer Labs再获2425万美元融资 Pantera参投](https://www.btc126.com//view/168100.html)
> 概要: 据官方公布，Balancer Labs 与加密货币领域投资者进一步合作，他们向 Balancer Labs 提供了 2425 万美元的资金，以支持 Balancer Protocol 进一步的发展。这......
### [流言板威少：喷垃圾话可以但不能越界，否则我必须要守护我的自尊](https://bbs.hupu.com/43195734.html)
> 概要: 虎扑05月27日讯 今天奇才客场95-120不敌76人，系列赛大比分0-2落后。比赛第四节奇才后卫拉塞尔-威斯布鲁克扭伤脚踝离场时，现场有76人球迷向他扔爆米花并大喊垃圾话，威斯布鲁克十分愤怒并试图返
### [明日看点：以太坊扩容方案 Arbitrum上线主网](https://www.btc126.com//view/168102.html)
> 概要: 1.以太坊扩容方案Arbitrum预计于5月28日上线主网。2.价值18亿美元的比特币期权将于5月28日（周五）在Deribit上到期......
### [美国OFAC希望使用Chainalysis区块链分析工具用于跟踪虚拟货币交易](https://www.btc126.com//view/168103.html)
> 概要: 据The Block消息，美国财政部外国资产控制办公室（OFAC）希望使用Chainalysis的区块链分析工具用于“关键任务研究”。OFAC希望使用Chainalysis，“让其全球目标办公室(OG......
### [流言板比尔：今天在客场我听到了很多球迷疯狂的语言](https://bbs.hupu.com/43195916.html)
> 概要: 虎扑05月27日讯 今天奇才客场95-120不敌76人，系列赛大比分0-2落后，赛后奇才后卫布拉德利-比尔接受采访谈起了比赛中76人主场有球迷向拉塞尔-威斯布鲁克扔爆米花的事情。“这太恶心了，作为球员
### [赛后武汉ES 2-1 佛山GK，头龙加持梓墨封神，e星碾压之势再领先](https://bbs.hupu.com/43195910.html)
> 概要: GAME 3：蓝色方武汉eStarPro，红色方佛山GK【Ban/Pick】【高光时刻】开局2分钟，阿改蔡文姬技能全空被留，花海阿古朵扔球一砸获得一血开局3分钟，尽管鹏鹏收下小主宰没能秀全，无心周瑜
### [员工签了自愿放弃社保声明 工作中突发疾病死亡公司赔了60万](https://finance.sina.com.cn/china/2021-05-27/doc-ikmyaawc7917730.shtml)
> 概要: 原标题：员工签了自愿放弃社保声明，工作中突发疾病死亡公司赔了60万，法院：这钱该赔，原因是…… 每经编辑 毕陆名 员工若自愿放弃社保，事后又反悔状告单位...
### [北京集中土拍学问大，谁是大赢家，谁颗粒无收？](https://finance.sina.com.cn/china/dfjj/2021-05-27/doc-ikmxzfmm5055754.shtml)
> 概要: 北京集中土拍学问大，谁是大赢家，谁颗粒无收？ 来源：地产锐观察 5月27日，随着北京10宗竞高标准商品住宅建设方案地块的竞得人全部出炉，2021年北京首次集中供地收官。
### [最新通报：病毒可以通过吃一顿饭，短暂的非直接接触传播！](https://finance.sina.com.cn/china/gncj/2021-05-27/doc-ikmxzfmm5055479.shtml)
> 概要: 原标题：病毒可以通过吃一顿饭，短暂的非直接接触传播！ 在27日举行的广州市政府新闻发布会上，广州市卫生健康委员会副主任陈斌通报...
### [订单火爆遭遇错峰用电 广东制造业企业有点难](https://finance.sina.com.cn/jjxw/2021-05-27/doc-ikmxzfmm5055112.shtml)
> 概要: 原标题：订单火爆遭遇错峰用电，广东制造业企业有点难 从“开六停一”到“开五停二”再到“开四停三”，广东一些地方的错峰限电正在“升级”。
### [为什么国产青春爱情片，总是过不了7分？](https://new.qq.com/omn/20210527/20210527A0EKP500.html)
> 概要: 君伟 | 作者     周矗 | 编辑(注: 本文含有剧透)从情人节档的《我为你喝彩》，五一档，到520档，国产青春爱情片“沉渣泛起”。            《我为你喝彩》豆瓣评分都未开出，票房只有......
### [赵丽颖新剧无往日灵气，眼神空洞磨皮重，结婚生子对女星影响很大](https://new.qq.com/omn/20210527/20210527A0EL8600.html)
> 概要: 近日，《理想照耀中国》之《希望的田野》篇播出，赵丽颖在其中饰演“雷金玉”一角。赵丽颖作为时下热门的女演员，在不久前刚刚官宣了离婚，所以她的新剧上线，引起了不少关注。            在剧中，赵丽......
### [波卡Polkadot节点5月24日出现错误的问题已解决](https://www.btc126.com//view/168106.html)
> 概要: 据官方消息，5月24日，Polkadot节点因区块5202216的内存不足（OOM）错误而失败。该区块包含验证者选举的链上解决方案，该解决方案通常在链外进行计算，并且仅在未提交链下解决方案的情况下才在......
### [栗战书在乡村振兴促进法实施座谈会上强调 推动乡村振兴促进法贯彻实施和宣传普及](https://finance.sina.com.cn/china/gncj/2021-05-27/doc-ikmyaawc7919843.shtml)
> 概要: 原标题：栗战书在乡村振兴促进法实施座谈会上强调 深入学习贯彻习近平总书记关于“三农”工作的重要论述 推动乡村振兴促进法贯彻实施和宣传普及
### [金融部门七天三次发声 没有任何人可以准确预测汇率走势](https://finance.sina.com.cn/china/2021-05-27/doc-ikmxzfmm5056707.shtml)
> 概要: 近期，有消息称，人民币国际化条件下，中国人民银行最终要放弃汇率目标。而由于今年以来，国际大宗商品价格出现较大涨幅，也有观点认为，大宗商品价格中长期升势或已成型...
# 小说
### [重生之我是大魔法师](http://book.zongheng.com/book/1015198.html)
> 作者：许楠

> 标签：奇幻玄幻

> 简介：心中刻下的那颗蔚蓝色的星球，经常在梦中出现，可是在这幽寂的宇宙中，怎么才能找到我回家的路……

> 章节末：结尾

> 状态：完本
# 论文
### [High resolution weakly supervised localization architectures for medical images](https://paperswithcode.com/paper/high-resolution-weakly-supervised)
> 日期：22 Oct 2020

> 标签：None

> 代码：https://github.com/cmb-chula/pylon

> 描述：In medical imaging, Class-Activation Map (CAM) serves as the main explainability tool by pointing to the region of interest. Since the localization accuracy from CAM is constrained by the resolution of the model's feature map, one may expect that segmentation models, which generally have large feature maps, would produce more accurate CAMs.
### [Asymptotically Exact and Fast Gaussian Copula Models for Imputation of Mixed Data Types](https://paperswithcode.com/paper/asymptotically-exact-and-fast-gaussian-copula)
> 日期：4 Feb 2021

> 标签：IMPUTATION

> 代码：https://github.com/boennecd/mdgc

> 描述：Missing values with mixed data types is a common problem in a large number of machine learning applications such as processing of surveys and in different medical applications. Recently, Gaussian copula models have been suggested as a means of performing imputation of missing values using a probabilistic framework.
