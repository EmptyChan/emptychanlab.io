---
title: 2023-03-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ChavarocheWinter_ZH-CN1842519491_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-23 22:34:38
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ChavarocheWinter_ZH-CN1842519491_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [创业180天，我发现了工具型SaaS的最佳实践](https://www.woshipm.com/chuangye/5788368.html)
> 概要: 产品的切入点不同，所带来的结果也会有所不同。三句话介绍产品，听得下去的人成交率80%，听不下去的多难以继续合作。本文以该创业公司的故事为例，聊聊工具型SaaS PMF时会遇到的问题以及启示，希望对你有......
### [旅游业的AI狂想：干掉谁，成就谁，超度谁？](https://www.woshipm.com/ai/5788472.html)
> 概要: AI人工智能表现出来的能力正在震惊越来越多人，那么当AI人工智能被应用到实际领域时，又会产生什么样的神奇碰撞？比如作为偏向线下的旅游业，AI的加入，会给个人的旅行规划、会给宏观的旅游产业、旅游组织，带......
### [数仓建模——维度表详细讲解](https://www.woshipm.com/data-analysis/5788960.html)
> 概要: 维度表是一种数据建模技术，用于存储与数据中心的各个业务领域相关的维度信息，通常于构建数据仓库、数据集市等决策支持系统，以便进行多维数据分析和报告。本文作者对维度表进行了分析讲解，一起来看一下吧。一、维......
### [《反恐精英2》官宣！可免费从CSGO升级](https://www.3dmgame.com/news/202303/3865435.html)
> 概要: 今日V社毫无预兆地公布了一个爆炸性消息——《反恐精英2（Counter-Strike 2）》正式官宣，采用起源2引擎开发，V社称之为是“反恐精英的下个时代”。今年夏季《CSGO》将会平稳过渡，免费升级......
### [30万以下，谁还买油车？](https://www.huxiu.com/article/820983.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博编辑｜周到头图｜《九品芝麻官》“完全不理解，怎么现在还有人买油车？”当蔚来创始人、董事长、CEO李斌在2021 NIO Day后说出这句话时，引来了铺天盖地的诘责。人们“批......
### [关于“产业互联网”的一些思考](https://www.huxiu.com/article/892531.html)
> 概要: 今年春节前后，听了一个行业母基金出身，参与了200多家公司IPO的资深投资人关于产业互联网的分享，非常深刻，也激起了我的一些相关思考欲望。于是也专门写了一些我对于产业互联网，产业数字化升级的一些相关思......
### [美国SEC向加密货币交易所Coinbase发出警告通知 或将提起诉讼](https://finance.sina.com.cn/tech/internet/2023-03-23/doc-imymvcvn6545802.shtml)
> 概要: 新浪科技讯 北京时间3月23日早间消息，美国证券交易委员会（SEC）周三向加密货币交易所Coinbase发出通知，警告称该公司可能违反美国证券法。受此消息影响，Coinbase股价在周三美国股市盘后交......
### [ChatGPT真让搜索市场变天了？微软必应下载量翻了8倍 谷歌下跌2%](https://finance.sina.com.cn/tech/internet/2023-03-23/doc-imymvkcc9544220.shtml)
> 概要: 新浪科技讯 北京时间3月23日早间消息，据报道，根据第三方数据分析机构Similarweb的报告，微软旗下的必应搜索在植入OpenAI公司的聊天机器人ChatGPT后，已经改变了在用户中几乎没有存在感......
### [《生化危机4 重制版》动画PV3 3月24日发售](https://www.3dmgame.com/news/202303/3865454.html)
> 概要: 今日（3月23日），卡普空官方公布《生化危机4：重制版》X Nippon Animation动画PV3（《RE名作剧场 不思议村子里的里昂》第3话 “救救我吧！老爷爷！”）现已公开由以“世界名作剧场”......
### [韩星郑东元违规驾驶摩托车道歉:第一天上路不熟悉](https://ent.sina.com.cn/y/yrihan/2023-03-23/doc-imymvkch9756278.shtml)
> 概要: 新浪娱乐讯 韩国歌手郑东元今天对其违反交通规则一事进行了公开道歉。　　郑东元今天凌晨在首尔东部干线道路上驾驶摩托车时驾车驶入汽车专用道而被交警拦下，郑东元在被交警拦住后承认自己违反交通规则并受到了交警......
### [36氪首发｜「宇称电子」获长城资本战略融资，希望以集成芯片方式降低激光雷达成本](https://36kr.com/p/2183393017265666)
> 概要: 36氪首发｜「宇称电子」获长城资本战略融资，希望以集成芯片方式降低激光雷达成本-36氪
### [奈雪的茶旗下机构投了家云南咖啡品牌，「嗨罐咖啡」获初芽投资天使轮战略融资](https://36kr.com/p/2172433217155592)
> 概要: 奈雪的茶旗下机构投了家云南咖啡品牌，「嗨罐咖啡」获初芽投资天使轮战略融资-36氪
### [年度最佳日剧候选！我愿称之为治愈版《魔法少女小圆》](https://news.idmzj.com/article/77545.html)
> 概要: 好好看！希望每个人都去看！
### [青崎有吾《不死少女》TV动画化！PV公开](https://news.idmzj.com/article/77546.html)
> 概要: 青崎有吾原作小说《Undead Girl·Murder Farce》TV动画化决定！视觉图与PV一并公开，本作将于7月开播。
### [舍得品牌上市会葡萄牙站于里斯本举行，让东方生活美学走向世界](http://www.investorscn.com/2023/03/23/106408/)
> 概要: 当地时间3月19日，“舍得酒与世界分享舍得智慧暨舍得全球品牌上市会”葡萄牙站于里斯本举行。这是近两年来，葡萄牙规模最大的华人企业活动......
### [友邦香港与Humansa合作首创一站式服务高净值客户养生中心](http://www.investorscn.com/2023/03/23/106409/)
> 概要: (2023年3月22日，深圳) — 友邦香港22日宣布与新世界集团旗下领先业界的医疗健康管理品牌Humansa仁山优社签署合作备忘录，携手于香港开设保险业界首家一站式养生中心，独家为友邦香港本地及内地......
### [营收大增、中高端酒贡献突出、机构大举买入，舍得酒业年报解读](http://www.investorscn.com/2023/03/23/106410/)
> 概要: 3月21日盘后晚间，舍得酒业股份有限公司（600702.SH，以下简称“舍得酒业”）发布2022年年度报告。报告显示，2022年公司实现营业收入60.56亿元，同比增长21.86%。净利润16.85亿......
### [美国零售商GameStop 2022财年亏损3.13亿美元](https://www.3dmgame.com/news/202303/3865475.html)
> 概要: 美国游戏零售商 GameStop日前公布了公司 2022 财年财务业绩。在截至 2023 年 1 月 28 日的12 个月内，公司净销售额下降了 1.4%，为 59.3 亿美元。而 2022 财年第四......
### [海外new things | 新加坡锂电池回收技术初创「Green Li-ion」Pre-B轮融资2050万美元，计划扩大回收设备的生产规模](https://36kr.com/p/2177770253742601)
> 概要: 海外new things | 新加坡锂电池回收技术初创「Green Li-ion」Pre-B轮融资2050万美元，计划扩大回收设备的生产规模-36氪
### [《足球小将》TV动画第二季10月播出！](https://news.idmzj.com/article/77547.html)
> 概要: 高桥阳一《足球小将》新作动画放送决定。以《足球小将第二季 世少篇》为标题，东京电视台系6局网络从10月开始放送。
### [BL漫画《口罩男子明明不想恋爱》动画化!](https://news.idmzj.com/article/77548.html)
> 概要: 参号ミツル《口罩男子明明不想恋爱》动画化决定。收录在10月20日发售的单行本4卷特装版的Blu-ray中。
### [一场或将改变全球人睡眠的技术革新，正在中国发生](http://www.investorscn.com/2023/03/23/106414/)
> 概要: 人类追求美好生活的渴望，与生俱来......
### [视频：《宁安如梦》团综全员聚餐 张凌赫贴心帮白鹿拧瓶盖](https://video.sina.com.cn/p/ent/2023-03-23/detail-imymvyyv0278808.d.html)
> 概要: 视频：《宁安如梦》团综全员聚餐 张凌赫贴心帮白鹿拧瓶盖
### [视频：丁太升辣评《声生不息》首期节目 杨宗纬最佳张杰最差](https://video.sina.com.cn/p/ent/2023-03-23/detail-imymvyyv9218679.d.html)
> 概要: 视频：丁太升辣评《声生不息》首期节目 杨宗纬最佳张杰最差
### [换新机认准京东手机焕新季 华为P50、荣耀80 GT等限时享超值低价](http://www.investorscn.com/2023/03/23/106422/)
> 概要: 每年3-4月都是消费者换机的高峰期，等待半年的旗舰手机终于迎来优惠好价，非常值得入手。3月23日至4月10日期间，京东手机焕新季活动火热上线，iPhone 14 Pro Max、华为P50、荣耀80 ......
### [商务部答一财：美方应尽快取消全部对华加征关税](https://www.yicai.com/news/101710196.html)
> 概要: 美方应倾听国内各界呼声，尊重世贸组织专家组裁决，尽快取消全部对华加征关税。
### [经典游戏《拳皇》SNK 早年删除的官方剧情，却被港漫发扬光大了](https://www.ithome.com/0/681/867.htm)
> 概要: 在 SNK 最初的设计中，三神器的剧情远远没有那么简单。当时的《拳皇 94》《拳皇 95》和《拳皇 96》影响力已经非常大了，但人气却无法和《饿狼传说》前面几作相提并论，这从移植平台的数量就可以看出来......
### [1400亿，恒大刚解决了债务重组难题的1/5](https://www.huxiu.com/article/901628.html)
> 概要: 本文来自微信公众号：风财讯 （ID：fengcaixun），作者：黄小妹，原文标题：《140000000000元，恒大刚解决了债务重组难题的1/5》，头图来自：视觉中国一拖再拖的恒大境外债务重组方案终......
### [iQOO Z7 手机上线 4 款“Jovi 语音”全新音色，有御姐、大学生](https://www.ithome.com/0/681/865.htm)
> 概要: IT之家3 月 23 日消息，iQOO Z7 手机在 3 月 20 日发布，首发价 1599 元起。OriginOS 今日官方宣布，iQOO Z7 上线 4 款“Jovi 语音”全新音色，分别为婉清（......
### [腾讯总算缓过来了](https://www.huxiu.com/article/892817.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国增长抢回财报 C 位时，腾讯总算缓过来了。3 月 22 日，腾讯发布 2022 年四季度及全年财报：2022 全年腾讯营收 5545.52 亿元，同比下滑......
### [泰国驻华大使馆回应泰国旅游风险传闻](https://www.yicai.com/news/101710346.html)
> 概要: 此前，泰国总理兼国防部长巴育也已下令泰国旅游与体育部部长就此事作出澄清。
### [小米新专利获授权，可提高智能穿戴设备的定位精度和通讯效率](https://www.ithome.com/0/681/883.htm)
> 概要: IT之家3 月 23 日消息，北京小米移动软件有限公司申请的“天线模组及可穿戴设备”专利近日公布。IT之家查询国家知识产权局得知，本公开是关于一种天线模组及可穿戴设备。天线模组的辐射体包括相互隔离的第......
### [*ST板块两成个股年内涨幅超11%，这些个股确定退市](https://www.yicai.com/news/101710380.html)
> 概要: “财务退”仍是主流
### [金士顿宣布名列2022年全渠道SSD出货量榜首](https://www.3dmgame.com/news/202303/3865510.html)
> 概要: 金士顿是许多玩家熟悉的存储品牌，其内存产品一直有着很高的市场占有率。其实金士顿在SSD市场的出货量也很高，只不过很少被注意到，或者被玩家谈及。近日，金士顿宣布其名列2022年全渠道SSD出货量排行榜的......
### [商务部回应事关中美经贸关系的三个关键问题](https://finance.sina.com.cn/china/2023-03-23/doc-imymwfie0074269.shtml)
> 概要: 红星资本局原创 记者|王田 编辑|余冬梅 肖子琦 3月23日，商务部例行新闻发布会上，商务部新闻发言人束珏婷就开展“投资中国年”招商引资系列活动...
### [千亿光伏组件回收蛋糕，只是看上去很美？](https://36kr.com/p/2183666012470792)
> 概要: 千亿光伏组件回收蛋糕，只是看上去很美？-36氪
### [金山办公：未来会接入 GPT-4](https://www.ithome.com/0/681/896.htm)
> 概要: IT之家3 月 23 日消息，金山办公近期接受机构调研时被问及“未来是否会接入 GPT-4？”公司表示，会的，出海的公司基本都在和 OpenAI 合作，这是目前最好的技术。3 月 21 日晚间，金山办......
### [“中国菠萝之乡”广东徐闻今年菠萝出口大幅增长](https://finance.sina.com.cn/china/2023-03-23/doc-imymwmqx7161117.shtml)
> 概要: 中新社湛江3月23日电 （梁盛 陈成桥）三月，“中国菠萝之乡”广东湛江市徐闻县一望无际的菠萝田里，随处可见果农们忙碌的身影，一颗颗金色的菠萝在被采摘、清洗、包装后...
### [商务部：美方应尽早取消全部对华加征关税](https://finance.sina.com.cn/china/2023-03-23/doc-imymwmqz9995924.shtml)
> 概要: 新华社北京3月23日电（谢希瑶、索骄）商务部新闻发言人束珏婷23日说，美方应倾听国内各界呼声，尊重世贸组织专家组裁决，尽快取消全部对华加征关税。
### [商务部：美方应尽早取消对华加征的关税 为减少贸易逆差创造条件](https://finance.sina.com.cn/china/2023-03-23/doc-imymwmqr8990845.shtml)
> 概要: 记者：赵博 商务部新闻发言人束珏婷3月23日称，美方应尽早取消对中国加征的“301”关税，取消对中国企业的贸易限制，为减少对华贸易逆差创造条件。
### [信托首现全行业季度亏损，资金“大搬家”后房地产余额再降30%](https://www.yicai.com/news/101710465.html)
> 概要: 向标准化转型，信托公司自身也难再“保本”。
### [索尼成2022年MTC评分最高的游戏发行商](https://www.3dmgame.com/news/202303/3865514.html)
> 概要: Metacritic公布了第13届（2022年）年度游戏发行商排名结果，索尼排名第一。排名基于2022年所发售游戏的品质。索尼2022年推出了多个高分大作，包括《战神5》《最后的生还者1：重制版》《地......
### [刘国中在全国春季农业生产工作会议上强调 抓紧抓好粮食和重要农产品生产 确保加快建设农业强国良好开局](https://finance.sina.com.cn/china/2023-03-23/doc-imymwmqy3244927.shtml)
> 概要: 新华社成都3月23日电 全国春季农业生产工作会议3月23日在四川省德阳市召开。中共中央政治局委员、国务院副总理刘国中出席会议并讲话。
### [瑞银：不应抛售优质银行AT1债，但信心修复仍具不确定性](https://www.yicai.com/news/101710481.html)
> 概要: 对欧洲银行股票持中立态度。
# 小说
### [星际：生存从冷湖镇开始](https://book.zongheng.com/book/1239814.html)
> 作者：下传深蓝

> 标签：科幻游戏

> 简介：冷湖天文基地观测员秦朗，因一次巡视，意外与拥有神秘背景的女博士洛莹莹相遇，交集。拥有上万人的石油小镇，为何会莫名荒废？俄博梁雅丹魔鬼城，究竟隐藏着什么？冷湖天文基地又为何因此建立？总总的一切，都指向一个不为人知的秘密。从这一刻开始，秦朗意外踏入一个未知的世界，一个背负人类未来命运的世界！

> 章节末：第一百七十一章 探索星际！（大结局）

> 状态：完本
# 论文
### [Reasoning with Language Model Prompting: A Survey | Papers With Code](https://paperswithcode.com/paper/reasoning-with-language-model-prompting-a)
> 日期：19 Dec 2022

> 标签：None

> 代码：https://github.com/zjunlp/Prompt4ReasoningPapers

> 描述：Reasoning, as an essential ability for complex problem-solving, can provide back-end support for various real-world applications, such as medical diagnosis, negotiation, etc. This paper provides a comprehensive survey of cutting-edge research on reasoning with language model prompting. We introduce research works with comparisons and summaries and provide systematic resources to help beginners. We also discuss the potential reasons for emerging such reasoning abilities and highlight future research directions.
### [Molecular convolutional neural networks with DNA regulatory circuits](https://pattern.swarma.org/paper?id=019820cc-fbee-11ec-a044-0242ac17000f)
> 概要: Complex biomolecular circuits enabled cells with intelligent behaviour to survive before neural brains evolved. Since DNA computing was first demonstrated in the mid-1990s, synthetic DNA circuits in liquid phase have been developed as computational hardware to perform neural network-like

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
