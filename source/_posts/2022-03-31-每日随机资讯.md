---
title: 2022-03-31-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.AnniEiffel_ZH-CN1786932808_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-03-31 22:51:16
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.AnniEiffel_ZH-CN1786932808_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Visual Studio Code 1.66 发布，新增 JavaScript 调试](https://www.oschina.net/news/189038/visual-studio-code-1-66-released)
> 概要: FinClip小程序编程挑战赛｜十万元悬赏小程序开发爱好者>>>>>Visual Studio Code 1.66现已发布，该版本更新内容很多，下面摘录部分新特性作介绍：本地历史记录可以在时间轴视图中......
### [Grafana Labs 发布高性能开源时序数据库：Grafana Mimir](https://www.oschina.net/news/189046/grafana-mimir)
> 概要: FinClip小程序编程挑战赛｜十万元悬赏小程序开发爱好者>>>>>Grafana Labs宣布推出Grafana Mimir，号称是世界上可扩展性最强、性能最高的开源时序数据库。Mimir 支持将指......
### [Redmine 5.0.0 发布，项目管理与缺陷跟踪管理系统](https://www.oschina.net/news/189037/redmine-5-0-0-released)
> 概要: FinClip小程序编程挑战赛｜十万元悬赏小程序开发爱好者>>>>>Redmine 是一个网页界面的项目管理与缺陷跟踪管理系统的自由及开放源代码软件工具。它集成了项目管理所需的各项功能：日历、燃尽图和......
### [微软妥协，允许用户一键更改默认浏览器](https://www.oschina.net/news/189045/windows-11-default-browser)
> 概要: FinClip小程序编程挑战赛｜十万元悬赏小程序开发爱好者>>>>>微软已于本周开始推送一个新的 Windows 11 更新（KB5011563），此次更新中最大的变化就是允许 Windows 11 ......
### [虚拟人火了，然后呢？](http://www.woshipm.com/it/5374239.html)
> 概要: 编辑导语：技术的发展为我们创造了更多可能性，虚拟人、元宇宙、NFT等新兴概念的出现便是证明之一。那么，这类概念的发展是否值得期待？就拿虚拟人来说，虚拟人可以为品牌带来商业价值，但整体产业的盈利变现前景......
### [“圈层化”的大学生群体，品牌如何精准渗透？](http://www.woshipm.com/marketing/5374777.html)
> 概要: 编辑导语：你是否想要了解美妆护肤品牌该如何精准渗透，靠大学生群体来获取品牌增量，实现品牌建设呢？本篇文章的作者将会告诉你答案！推荐想要了解如何把握大学生群体，做品牌精准渗透的群体阅读。说到大学生，想必......
### [简单聊聊电商系统的订单号生成规则](http://www.woshipm.com/pd/5375714.html)
> 概要: 编辑导语：订单号、支付流水号、售后订单号、快递取货号、电子券核销码等，这些都是我们日常在生活中进行会遇见和使用的一些单号，那么为什么有些单号那么长，有些只有几位数？有些单号一看就知道年月日的信息，有些......
### [林之校和林老师一起吃冰激凌谈心](https://new.qq.com/rain/a/20220331V0CEG100)
> 概要: 林之校和林老师一起吃冰激凌谈心
### [《余生》花絮：余生夫妇与邵江的三角修罗场](https://new.qq.com/rain/a/20220331V04I8000)
> 概要: 《余生》花絮：余生夫妇与邵江的三角修罗场
### [日画1886~1916 三月的无声国度](https://www.zcool.com.cn/work/ZNTg5MTQ1MDQ=.html)
> 概要: 2022年3月的每日一画合集。勉强完成的第二个短篇故事，《无声国》。百感交集的一个月，祝四月顺利......
### [她右滑了1024个“真爱”](https://www.huxiu.com/article/518641.html)
> 概要: 作者｜竺晶莹题图｜《非诚勿扰》剧照29岁挪威女孩Cecilie在伦敦工作，她使用Tinder7年，配对数高达1024对，这意味着她右滑的这1024名男子也右滑了她（在交友软件Tinder上右滑表示喜欢......
### [外媒爆料称网易将收购《底特律：变人》开发商](https://www.3dmgame.com/news/202203/3839316.html)
> 概要: Quantic Dream工作室开发了《底特律：变人》《暴雨》《超凡双生》等知名游戏。近日据外媒Exputer报道称，网易将完全收购Quantic Dream。近年来微软和索尼等公司挥舞钞票，大肆收购......
### [《戴卡奥特曼》变身预告公开 7月9日开播](https://acg.gamersky.com/news/202203/1471587.shtml)
> 概要: 《奥特曼》新系列《戴卡奥特曼》，公开了全新预告，展示了新奥特曼们的变身情形。同时也确定了《戴卡奥特曼》将于7月9日开播。
### [国产动画「时空之隙」发布最新PV 定档4月15日](http://acg.178.com/202203/442693010385.html)
> 概要: 昨晚（3月30日），根据「梦幻西游」手游改编的国产动画「时空之隙」公开第四弹宣传PV，其中展现了剑侠客、骨精灵、龙太子等主要角色的全新战斗画面。该动画将于4月15日播出，每周五更新。国产动画「时空之隙......
### [TV动画「RPG不动产」番宣CM公布](http://acg.178.com/202203/442693477542.html)
> 概要: TV动画「RPG不动产」的番宣CM现已公布，本作将于4月6日播出。TV动画「RPG不动产」番宣CMTV动画「RPG不动产」改编自险持智代创作的同名四格漫画作品，于2021年2月宣布电视动画化，由越田知......
### [P站美图推荐——画师碧風羽特辑](https://news.dmzj.com/article/74020.html)
> 概要: 碧風羽老师非常极其特别十分擅长将花朵元素用于绘画之中，同时独特的光影也是特色之一
### [B站确认引进4月新番《间谍过家家》 开播时间待定](https://acg.gamersky.com/news/202203/1471634.shtml)
> 概要: B站宣布将会引进《间谍过家家》，并且公开了中文字幕预告。《间谍过家家》在B站的具体开播时间还没有宣布，一起来期待吧。
### [《地平线：西之绝境》1.09版补丁 改善画质修复Bug](https://www.3dmgame.com/news/202203/3839341.html)
> 概要: 《地平线：西之绝境》于2月18日推出后，取得非常好的成绩。Guerrilla也不断为该作推出新更新，解决游戏里出现的各种问题。近日官方又发布《地平线：西之绝境》1.09版补丁，调整和改善了游戏内容，修......
### [百度等中企被美“预摘牌”？](https://finance.sina.com.cn/tech/2022-03-31/doc-imcwipii1595364.shtml)
> 概要: 3月31日，中国证监会国际部负责人在盘前发布答记者问，通报了中美审计监管合作的进展，对一些中国企业被美国证监会列入退市风险清单进行了回应......
### [DMM游戏《灰色 战场的船歌》4月5日开服！序章视频公开](https://news.dmzj.com/article/74021.html)
> 概要: EXNOA运营的DMM游戏《灰色 战场的船歌》宣布了将于4月5日开服的消息。本作的一段序章视频也一并公开。在这次的视频中，可以看到原作本篇和这次的游戏故事之间的联系。
### [孙二娘模拟器？惊悚经营游戏《贪婪的魔鬼》上线](https://www.3dmgame.com/news/202203/3839344.html)
> 概要: 由Bad Vices Games开发的惊悚模拟经营游戏《贪婪的魔鬼（Ravenous Devils）》现已上线Steam平台，游戏将于4月30日正式发售，支持中文。在游戏中玩家将扮演一对夫妇，经营一家......
### [【站酷x丁几】联名共创表情包设计](https://www.zcool.com.cn/work/ZNTg5MjAzODQ=.html)
> 概要: 有幸和站酷联名，推出2022全新的站酷小Z表情包～小Z做为Zcool深入人心的形象担当，一直以来给人的感觉都是酷酷的，甚至有些神秘感，我不禁会想象：在电脑前疯狂肝图的小z是什么样子？和甲方battle......
### [TV动画「半妖的夜叉姬」完结应援绘公开](http://acg.178.com/202203/442697640020.html)
> 概要: TV动画「半妖的夜叉姬」现已正式完结，官方公开了完结应援绘图。动画「半妖的夜叉姬」是「犬夜叉」的续篇，讲述了犬夜叉的女儿与杀生丸的女儿们之间的故事。动画第一季已于2020年10月开始播出，动画第二季「......
### [TV动画「杀爱」完结应援绘公开](http://acg.178.com/202203/442698006757.html)
> 概要: TV动画「杀爱」现已正式完结，官方公开了完结应援绘图。「杀爱」原作由网络漫画家Fe创作，最开始连载于pixiv。后来本作由于人气火爆，成功刊登在「月刊COMIC GENE」上。该漫画讲述的是互相以对方......
### [《海贼王》1045话情报：5档路飞VS凯多 全程爆笑](https://acg.gamersky.com/news/202203/1471567.shtml)
> 概要: 《海贼王》1045话图文情报公开，本集就是路飞和凯多的战斗，感觉尾田已经彻底放飞自我，各种场面都是非常有喜感。
### [集英社设立100%出资的“集英社游戏”！6部作品制作中](https://news.dmzj.com/article/74023.html)
> 概要: 集英社宣布了设立100%出资的关联公司“集英社游戏”的消息。集英社在去年启动了游戏创作者支援企划“集英社游戏装创作者CAMP”。这次设立新公司，也是为了扩大支援事业。
### [How does Firefox's Reader View work? (2020)](https://videoinu.com/blog/firefox-reader-view-heuristics/)
> 概要: How does Firefox's Reader View work? (2020)
### [手游开发商Nexters2021年收入4.34 亿美元 创新纪录](https://www.3dmgame.com/news/202203/3839362.html)
> 概要: 手机游戏公司Nexters公布了截至2021年12月31日止期间的第四季度和全年财务业绩。在其财政年度的最后三个月，Nexters报告收入为1.23亿美元，同比增长65%，创下公司历史新高。该公司还报......
### [Turn your phone into a space monitoring tool](https://www.esa.int/Applications/Navigation/Turn_your_phone_into_a_space_monitoring_tool)
> 概要: Turn your phone into a space monitoring tool
### [荣耀赵明：欢迎车企造手机 手机复杂度并不比车小](https://finance.sina.com.cn/tech/2022-03-31/doc-imcwiwss9185058.shtml)
> 概要: 相关新闻：李斌回应蔚来造手机：正处于调研阶段......
### [NS游戏《雪人兄弟 特别版》日版 延期至5月19日发售](https://www.3dmgame.com/news/202203/3839370.html)
> 概要: DAEWON MEDIA GAME LAB官方宣布Switch游戏《雪人兄弟特别版》日版将从原本的4月21日延期至5月19日，与昨日公布的欧美版同一天发售。游戏将从4月22日开始预购。游戏普通版售价3......
### [茅台称用区块链技术保证摇号公平](https://finance.sina.com.cn/tech/2022-03-31/doc-imcwipii1632192.shtml)
> 概要: 记者 薛晨  编辑 李严 校对 王心......
### [组图：赵今麦最新封面大片吸睛 穿深色西装白色纱裙反差大](http://slide.ent.sina.com.cn/star/slide_4_86512_367623.html)
> 概要: 组图：赵今麦最新封面大片吸睛 穿深色西装白色纱裙反差大
### [“i茅台”试运行首日太火爆](https://finance.sina.com.cn/tech/2022-03-31/doc-imcwipii1632629.shtml)
> 概要: 上证报中国证券网讯（记者 朱文彬）3月31日上午9时，茅台官方新电商平台“i茅台”正式上线开启试运行......
### [林之校爸妈陪林之校试婚纱](https://new.qq.com/rain/a/20220331V0C12I00)
> 概要: 林之校爸妈陪林之校试婚纱
### [《冰川时代》衍生动画短片预告：小松鼠奶爸故事](https://acg.gamersky.com/news/202203/1471759.shtml)
> 概要: 《冰川时代》系列人气角色小松鼠斯科特的衍生剧《冰川时代：斯克特的传说》发布预告，将于4月13日上线Disney+！
### [网络电影靠小屏幕逆袭，“优爱腾”找到新财富密码？](https://www.tuicool.com/articles/MRnmAnY)
> 概要: 网络电影靠小屏幕逆袭，“优爱腾”找到新财富密码？
### [非人哉782-784](https://www.zcool.com.cn/work/ZNTg5MjcxNTY=.html)
> 概要: 非人哉782-784
### [《Basil人物场景》MG动画——安戈力文化](https://www.zcool.com.cn/work/ZNTg5Mjc1NTY=.html)
> 概要: 《Basil人物场景》MG动画——安戈力文化
### [中国潮玩“征服”秋叶原](https://www.huxiu.com/article/519334.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜苗正卿题图｜视觉中国中国潮玩正在“征服”秋叶原和梨泰院。3月21日，23岁的韩国女孩朴慧在首尔三成洞coex商城买到了Dimmo盲盒。这是过去半年，她买到的第9个中国......
### [没出过国的留学生，还能叫留学生吗？](https://www.huxiu.com/article/519337.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。“青春才几年，疫情占三......
### [爱奇艺：进入暂时性名单不意味近期被强制摘牌和退市，将积极谋求解决方案](https://www.ithome.com/0/610/710.htm)
> 概要: IT之家3 月 31 日消息，美国当地时间周三，美国证券交易委员会（SEC）将百度、富途控股、爱奇艺、凯信远达医药和从事渔业养殖的 Nocera 加入“预摘牌名单”，这也是三月以来第三批被纳入名单的中......
### [韩女星金香奇剧组遇车祸 造成一名工作人员死亡](https://ent.sina.com.cn/v/j/2022-03-31/doc-imcwiwss9205499.shtml)
> 概要: 新浪娱乐讯 31日，据韩国媒体报道，演员金香奇所在剧组乘坐的大巴发生交通事故，目前拍摄停止。　　金香奇、金旻载、金相庆出演的tvN新剧《精神科医生刘世风》拍摄组乘坐的大巴于今日上午7点50分左右在京畿......
### [互联网还会变好吗？](https://www.huxiu.com/article/519258.html)
> 概要: 本文来自微信公众号：晚点LatePost （ID：postlate），作者：王汉洋，原文标题：《互联网还会变好吗？极客精神与 Web 3》，头图来自：视觉中国什么才是更好的互联网？很多人会回答：Web......
### [JavaScript 数组，看这篇就 ok 了！](https://www.tuicool.com/articles/JV3ENbZ)
> 概要: JavaScript 数组，看这篇就 ok 了！
### [直播打赏遇阻，虎牙斗鱼添堵](https://www.tuicool.com/articles/IfMnmiV)
> 概要: 直播打赏遇阻，虎牙斗鱼添堵
### [TV动画《相合之物》公开新PV](https://news.dmzj.com/article/74026.html)
> 概要: TV动画《相合之物》公开了一段新PV。在这次的PV中，介绍了将在本作中登场的主要角色以及动画的主要制作阵容。
### [Pointer Tagging for x86 Systems](https://lwn.net/SubscriberLink/888914/e81588082fa3b858/)
> 概要: Pointer Tagging for x86 Systems
### [外贸企业受困物流：半数单子不敢接，后面情况看不准](https://www.yicai.com/news/101367311.html)
> 概要: 由于物流不畅，一些外贸企业不得不放弃那些需要在5月紧急出货的“短平快”外贸订单。
### [天然新鲜的甜蜜，荟芙园百花蜂蜜 7.9 元 / 斤白菜价](https://lapin.ithome.com/html/digi/610733.htm)
> 概要: 荟芙园百花蜂蜜 500g 装报价 39.9 元，限时限量 32 元券，实付 7.9 元包邮，领券并购买。百花蜂蜜此价，还有洋槐蜂蜜、枸杞蜂蜜、枣花蜂蜜、紫云英蜜、椴树雪蜜等可选，稍贵 20-30 元......
### [上门试驾、线上下定、闭环生产，疫情下车企积极“自救”](https://www.yicai.com/news/101367345.html)
> 概要: 疫情下，汽车销售转线上，车企也开启闭环生产。
### [具俊晔抵台后将首次公开露面 出席夜店表演活动](https://ent.sina.com.cn/s/j/2022-03-31/doc-imcwipii1670591.shtml)
> 概要: 新浪娱乐讯 据台媒，具俊晔2月与大S登记结婚，他为爱飞来台湾，两人的爱情故事轰动两地，一举一动都备受关注。他31日惊喜宣布将在台举办表演活动，这也是其来台后首次公开露面。　　具俊晔31日宣布将以DJ身......
### [Ted Cruz在美参议院推介法案支持众议员Emmer，禁止美联储向个人发行CBDC](https://www.tuicool.com/articles/22uIJv3)
> 概要: Ted Cruz在美参议院推介法案支持众议员Emmer，禁止美联储向个人发行CBDC
### [券商业绩榜出炉：“三中一华”净利排位生变，投行、财富管理看点多](https://www.yicai.com/news/101367424.html)
> 概要: 中信建投是百亿净利队伍中的“新面孔”。
### [汽车厂商犯难了：继续购买零部件，还是效仿特斯拉自主研发？](https://www.ithome.com/0/610/751.htm)
> 概要: 北京时间 3 月 31 日晚间消息，据报道，对于那些想与特斯拉竞争的汽车厂商，如今又面临一个新的挑战：哪些技术应该自主研发，哪些应该从供应商那里购买？对于全球大多数汽车制造商来说，通过进行更多的内部制......
### [The Unintended Consequences of China Leapfrogging to Mobile Internet](https://yiqinfu.github.io/posts/walled-gardens-china/)
> 概要: The Unintended Consequences of China Leapfrogging to Mobile Internet
### [美国上周20.2万人首次申领失业救济金](https://finance.sina.com.cn/jjxw/2022-03-31/doc-imcwipii1687739.shtml)
> 概要: 据美国劳工部当地时间3月31日发布的数据，截至3月26日的一周，美国首次申请失业救济金的人数为202000人，较前一周首次申请失业救济金的188000人增加了14000人。
### [香港本财政年度前11月获得723亿港元盈余](https://finance.sina.com.cn/jjxw/2022-03-31/doc-imcwiwss9246066.shtml)
> 概要: 香港特区政府3月31日公布本财政年度前11个月（即截至2022年2月28日）的财务状况显示，计入在政府绿色债券计划下发行的绿色债券所得净收入291亿元（港币，下同）后...
### [《中国（北京）自由贸易试验区条例》今年5月1日起实施](https://finance.sina.com.cn/jjxw/2022-03-31/doc-imcwipii1688853.shtml)
> 概要: 新华社北京3月31日电（记者邰思聪）3月31日，北京市第十五届人大常委会第三十八次会议表决通过了《中国（北京）自由贸易试验区条例》，条例将于今年5月1日起实施。
### [财经TOP10|卫龙上市前的焦虑？致歉难解危机，柔宇科技深陷缺钱困境，“十荟团”全面关停背后](https://finance.sina.com.cn/china/caijingtop10/2022-03-31/doc-imcwipii1692702.shtml)
> 概要: 【政经要闻】 NO.1 中共中央政治局常务委员会召开会议听取“3·21”东航MU5735航空器飞行事故应急处置情况汇报 中共中央政治局常务委员会3月31日召开会议...
### [汽油、柴油价格再上调](https://finance.sina.com.cn/jjxw/2022-03-31/doc-imcwipii1691135.shtml)
> 概要: 新华社北京3月31日电（记者安蓓、王悦阳）国家发展改革委31日称，根据近期国际市场油价变化情况，按照现行成品油价格形成机制，自2022年3月31日24时起，国内汽油...
### [澳门连续15年派钱：现金分享计划明日起发放，永久居民每人获派1万澳门元](https://finance.sina.com.cn/china/gncj/2022-03-31/doc-imcwipii1692264.shtml)
> 概要: 南方财经全媒体记者田静发自澳门 澳门特区政府于4月1日至5月13日推行《2022年度现金分享计划》，通过银行转账或邮寄划线支票两种方式...
### [小米 49 元换电池活动明天上午 10 点开启](https://www.ithome.com/0/610/766.htm)
> 概要: 感谢IT之家网友小李飞刀1号的线索投递！IT之家3 月 31 日消息，小米米粉节 49 元换电池活动明天上午 10 点开启。据活动介绍，2022 年 4 月 1 日 10:00:00-2022 年 4......
### [20家派现金额超百亿！这些抗跌慢牛股值得关注](https://www.yicai.com/news/101367547.html)
> 概要: 136家2021年股息率超过3%，58家连续3年股息率均超过3%。持续高股息率个股在弱势中抗跌性显著，这类个股往往成为慢牛股集中营。
### [《余生》大结局：林之校顾魏婚礼互说感人誓词，快进来给新人随礼](https://new.qq.com/rain/a/20220331V0CKE500)
> 概要: 《余生》大结局：林之校顾魏婚礼互说感人誓词，快进来给新人随礼
### [3月511股获券商首次关注！这只股距目标价还有58%上涨空间](https://www.yicai.com/news/101367555.html)
> 概要: 数据显示，有5只个股收盘价距离相应券商给出的目标价仍有超过20%的上涨空间。
### [A股三大指数均累计下跌，北向、主力最青睐这些个股](https://www.yicai.com/news/101367567.html)
> 概要: 一图纵览3月A股资金、个股最全数据。
# 小说
### [天下英雄策](http://book.zongheng.com/book/1055792.html)
> 作者：流年书柬

> 标签：历史军事

> 简介：九州烽火再起，又是人间浩劫。仗义每多屠狗辈，王侯将相宁有种?英雄只为红颜怒，连陌铁骑，纵横侠气!长风一剑，天下无敌。弱水八千，江山万里。待我泼血为墨，龙飞凤舞，写完这天下最后一笔!

> 章节末：第二百一十六章 恩怨刀头消

> 状态：完本
# 论文
### [Generative Flow Networks for Discrete Probabilistic Modeling | Papers With Code](https://paperswithcode.com/paper/generative-flow-networks-for-discrete)
> 日期：3 Feb 2022

> 标签：None

> 代码：None

> 描述：We present energy-based generative flow networks (EB-GFN), a novel probabilistic modeling algorithm for high-dimensional discrete data. Building upon the theory of generative flow networks (GFlowNets), we model the generation process by a stochastic data construction policy and thus amortize expensive MCMC exploration into a fixed number of actions sampled from a GFlowNet. We show how GFlowNets can approximately perform large-block Gibbs sampling to mix between modes. We propose a framework to jointly train a GFlowNet with an energy function, so that the GFlowNet learns to sample from the energy distribution, while the energy learns with an approximate MLE objective with negative samples from the GFlowNet. We demonstrate EB-GFN's effectiveness on various probabilistic modeling tasks.
### [Ex-Model: Continual Learning from a Stream of Trained Models | Papers With Code](https://paperswithcode.com/paper/ex-model-continual-learning-from-a-stream-of)
> 日期：13 Dec 2021

> 标签：None

> 代码：None

> 描述：Learning continually from non-stationary data streams is a challenging research topic of growing popularity in the last few years. Being able to learn, adapt, and generalize continually in an efficient, effective, and scalable way is fundamental for a sustainable development of Artificial Intelligent systems.
