---
title: 2021-01-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Calakmul_EN-CN9963471796_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-19 21:40:52
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Calakmul_EN-CN9963471796_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [SJ李晟敏将与妻子上夫妻综艺 曾舞台热吻拉票失败](https://ent.sina.com.cn/tv/zy/2021-01-19/doc-ikftssan8329218.shtml)
> 概要: 新浪娱乐讯 Superjunior晟敏将与妻子金思垠一起出演TV CHOSUN电视台《妻子的味道》节目。　　《妻子的味道》节目组工作人员今天透露，晟敏与妻子金思垠已确定出演《妻子的味道》节目。晟敏与金......
### [王大陆官宣新综艺晒嘉宾合照 手动P上倪萍超贴心](https://ent.sina.com.cn/tv/zy/2021-01-19/doc-ikftssan8222450.shtml)
> 概要: 新浪娱乐讯 1月19日，湖南卫视官宣综艺《怦然再心动》嘉宾阵容为：王子文、王琳、黄奕、白冰、蔡卓宜。作为观察嘉宾的王大陆晒出与王子文、王琳、黄奕、白冰、蔡卓宜、倪萍的合照，并艾特她们写道：“一辈子家人......
### [法国电影人让·皮埃尔·巴克里去世 享年69岁](https://ent.sina.com.cn/m/f/2021-01-19/doc-ikftpnnx9009038.shtml)
> 概要: 新浪娱乐讯 据外媒报道，法国电影人让·皮埃尔·巴克里去世，享年69岁。(责编：小5)......
### [组图：行走的荷尔蒙！吴亦凡路透怼脸拍颜值能打 秀优越下颌线](http://slide.ent.sina.com.cn/y/slide_4_86512_351420.html)
> 概要: 组图：行走的荷尔蒙！吴亦凡路透怼脸拍颜值能打 秀优越下颌线
### [杨超越沉迷微博“福牛”小游戏：摸一下又没罪](https://ent.sina.com.cn/s/m/2021-01-19/doc-ikftssan8286948.shtml)
> 概要: 新浪娱乐讯 近日，杨超越沉迷微博“福牛新春旅行记”小游戏，1月19日，杨超越发文说道：“微博真的……我都关了的……奸诈。无所谓我就摸了怎么了？我承认！摸一下又没罪的”，又在评论区补充问道：“谁有羊和狮......
### [中央政法委评郑爽代孕弃养风波：钻法律空子，这绝不是无辜](https://new.qq.com/omn/20210119/20210119A0CZNW00.html)
> 概要: 一出代孕弃养的魔幻大剧，各种瓜让人们应接不暇，沉默一天后，当事人终于出面回应。19日，郑爽回应称，这是自己非常伤心和私密的事情，本不愿意在公众面前多说，“身为艺人我深知我国疫情的防控与重视。在中国国土......
### [星女郎林允白嫩左脸满是针眼，密密麻麻好吓人，为了变美也太拼了](https://new.qq.com/omn/20210119/20210119A0BLWM00.html)
> 概要: 1月18日，林允在其社交平台晒出一张照片，分享了自己手打水光针后的状态，还大呼脸有点疼。            在照片中，林允以素颜亮相，但是原本白白嫩嫩的脸上分布着密密麻麻的针孔，像是斑点一样，有些......
### [三圣母扮演者又出事！被曝大白天醉酒驾车，曾在怀孕时涉毒被判刑](https://new.qq.com/omn/20210119/20210119A0ESOS00.html)
> 概要: 1月19日，据韩媒报道，著名演员朴时妍大白天醉酒驾车，引发热议。            朴时妍这个名字大家可能不是很熟悉，但是她曾经在《宝莲灯》中扮演的三圣母一角，漂亮灵动的表演给观众留下了深刻印象......
### [郑爽爸爸发文回应：录音内容系被激怒引导，从未放弃负责两个孩子](https://new.qq.com/omn/20210119/20210119A0ESD600.html)
> 概要: 在郑爽和张恒事件发酵一天有余后，爽子再次回应，而她的爸爸郑成华也发文控诉大爆粗口。            在长文中，郑成华直言“断章取义”（没有主语），并为张恒一家贴上“阴险狡猾”等标签，称他“为了金......
### [郑爽出事徐冬冬发文力挺？蹭热度还是真爱粉？本人回应来了](https://new.qq.com/omn/20210119/20210119A093OJ00.html)
> 概要: 近两天，郑爽和前男友张恒的事情闹得沸沸扬扬的，娱乐圈中郑爽的小伙伴们也均未出面为期发生。            然而徐冬冬在1月18日多次发文，被人猜疑是在力挺郑爽。徐冬冬先是发文表示：不会脱粉。  ......
# 动漫
### [TV动画《不要欺负我、长瀞同学》第一弹PV](https://news.dmzj.com/article/69924.html)
> 概要: 根据774原作制作的TV动画《不要欺负我、长瀞同学》公开了第一弹PV。本作预计将于4月开始播出。
### [TV动画《新干线变形机器人 SHINKALION Z》4月开播](https://news.dmzj.com/article/69920.html)
> 概要: TV动画《新干线变形机器人 SHINKALION Z》宣布了将于今春开始播出的消息，宣传图与PV也一并公开。
### [颜狗的狂喜，谁看谁变尖叫鸡](https://news.dmzj.com/article/69915.html)
> 概要: 这期给大家推荐一部爱情少女漫《只靠脸的话才不会喜欢上你呢》，作者安斋かりん，才不汉化组汉化，讲述长得好帅但又狡猾的要死的先辈和呆萌痴女甜甜的腻死人的恋爱故事。
### [TV动画「新干线变形机器人 SHINKALION Z」宣传图公开](http://acg.178.com//202101/405035232086.html)
> 概要: TV动画「新干线变形机器人 SHINKALION Z」公开了最新的宣传图，本作将于2021年春起在东京电视台播出......
### [漫画「蓝色时期」宣布TV动画化](http://acg.178.com//202101/405035637669.html)
> 概要: 由山口飞翔绘制的漫画「蓝色时期（Blue Period.）」宣布TV动画化，将于今年（2021年）播出。本作曾获得“漫画大赏2020”第1名的成绩。该作讲述了矢口八虎一个像不良少年，但在学校却是个成绩......
### [TV动画「碧蓝航线 微速前行！」Blu-ray全2卷发售CM公开](http://acg.178.com//202101/405035791873.html)
> 概要: 近日，TV动画「碧蓝航线 微速前行！」发布了30秒Blu-ray全2卷发售CM。TV动画「碧蓝航线 微速前行！」Blu-ray全2卷发售CM......
# 财经
### [哈尔滨4例无症状感染者轨迹公布：1例曾到正大食品加工厂上夜班](https://finance.sina.com.cn/roll/2021-01-19/doc-ikftpnnx9388255.shtml)
> 概要: 原标题：哈尔滨4例无症状感染者轨迹公布！1例曾到正大食品加工厂上夜班 1月19日，哈尔滨市新增4例新冠肺炎无症状感染者，主要情况如下： 无症状感染者1：高某，女，48岁...
### [2021年 中国出口是否依旧可期？](https://finance.sina.com.cn/china/gncj/2021-01-19/doc-ikftssan8415436.shtml)
> 概要: 原标题：2021年，中国出口是否依旧可期？ 来源：中国金融四十人论坛 2020年中国出口超预期增长，为国内经济回归常态做出显著贡献，2021年中国出口是否依旧可期？
### [财经TOP10|郑爽被曝美国代孕弃养 央视:法律道德皆难容](https://finance.sina.com.cn/china/caijingtop10/2021-01-19/doc-ikftssan8413699.shtml)
> 概要: 【宏观要闻】 NO.1发改委：城区常住人口300万以下城市基本取消落户限制 国家发展改革委秘书长赵辰昕19日在介绍新型城镇化主要进展和成效时表示...
### [因管网维护 黑龙江哈尔滨冰雪大世界临时关园](https://finance.sina.com.cn/china/dfjj/2021-01-19/doc-ikftpnnx9396347.shtml)
> 概要: 原标题：因管网维护 黑龙江哈尔滨冰雪大世界临时关园 1月19日，黑龙江哈尔滨冰雪大世界股份有限公司发布信息，因管网维护，经哈尔滨冰雪大世界股份有限公司研究决定...
### [哈尔滨4例无症状感染者轨迹公布：1例曾到正大食品加工厂上夜班](https://finance.sina.com.cn/china/2021-01-19/doc-ikftssan8407579.shtml)
> 概要: 原标题：哈尔滨4例无症状感染者轨迹公布！1例曾到正大食品加工厂上夜班 1月19日，哈尔滨市新增4例新冠肺炎无症状感染者，主要情况如下： 无症状感染者1：高某，女，48岁...
### [如何用好RCEP红利？商务部专门搞了一场线上培训……](https://finance.sina.com.cn/china/gncj/2021-01-19/doc-ikftpnnx9388623.shtml)
> 概要: 原标题：如何用好RCEP红利？商务部专门搞了一场线上培训…… 新华社北京1月19日电 题：如何用好RCEP红利？商务部专门搞了一场线上培训…… 新华社记者于佳欣 区域全面经济伙伴...
# 科技
### [哔哩哔哩面试官：你可以手写Vue2的响应式原理吗？](https://segmentfault.com/a/1190000039021688)
> 概要: 写在前面这道题目是面试中相当高频的一道题目了，但凡你简历上有写：“熟练使用Vue并阅读过其部分源码”，那么这道题目十有八九面试官都会去问你。什么？你简历上不写阅读过源码，那面试官也很有可能会问你是否阅......
### [深入剖析 RSA 密钥原理及实践](https://segmentfault.com/a/1190000039021671)
> 概要: 一、前言在经历了人生的很多至暗时刻后，你读到了这篇文章，你会后悔甚至愤怒：为什么你没有早点写出这篇文章？！你的至暗时刻包括：1.你所在的项目需要对接银行，对方需要你提供一个加密证书。你手上只有一个六级......
### [微信张小龙：可能有两亿以上的用户朋友圈设置三天可见](https://www.ithome.com/0/530/981.htm)
> 概要: 1 月 19 日晚间消息，在今天的 2021 微信公开课 Pro 版的微信之夜上，腾讯高级副总裁，微信事业群总裁张小龙透露，越来越多的人在朋友圈设置三天可见，可能的数字是在 2 亿以上。他谈到，视频号......
### [长城汽车第 1000 万辆整车下线：全新第三代哈弗 H6](https://www.ithome.com/0/530/938.htm)
> 概要: IT之家1月19日消息 从长城汽车获悉，今日，“长城汽车第 1000 万辆整车下线仪式”在长城汽车徐水智慧工厂举行，一辆全新的第三代哈弗 H6 的下线。IT之家了解到，长城汽车旗下拥有哈弗、WEY、欧......
### [A History of Primary Colours (2020)](https://publicdomainreview.org/essay/primary-sources)
> 概要: A History of Primary Colours (2020)
### [I wasted $40k on a fantastic startup idea](https://tjcx.me/p/i-wasted-40k-on-a-fantastic-startup-idea)
> 概要: I wasted $40k on a fantastic startup idea
### [悟空问答关了，多闪和飞聊还会远吗？](https://www.huxiu.com/article/405287.html)
> 概要: 本文作者：蒋晓婷，编辑：王靖，题图来自：视觉中国张一鸣“大力出奇迹”的方法论，付诸社交和社区，好像总是差着一口气。日前悟空问答宣告关服停运：1月20日起从应用商店下线，2月3日停止运营。悟空问答之外，......
### [2020年，一个创业者在朋友圈的403条反思](https://www.huxiu.com/article/401758.html)
> 概要: 本文来自微信公众号：彭萦（ID：thatyingpeng），作者：彭萦，题图来自：视觉中国2020 年，一个（还没成功的）创业者在朋友圈的 403 条反思。关于品牌的反思关于细节的反思关于设计的反思关......
### [交互思考：无处不在的用户习惯](https://www.tuicool.com/articles/6VFZbem)
> 概要: 编辑导语：产品经理在设计一个新产品的时候，需要注重对用户习惯的培养，这有利于产品被用户更好地接受，也有助于减少使用上的障碍。本文作者从交互体验出发，对用户习惯展开了深入讨论，与大家分享。我们在互联网产......
### [NGS跑出的迷你独角兽，贝康医疗能否撑起"基因检测第一股"？](https://www.tuicool.com/articles/VnMnMzi)
> 概要: 图片来源@视觉中国文 | 港股研究社近年来随着社会发展给人们带来的压力不断增加，越来越多的人选择以辅助生殖来孕育下一代，导致我国试管婴儿服务市场需求庞大，而三代试管婴儿技术的出现，成为了在选择辅助生殖......
# 小说
### [灵气逼人](https://m.qidian.com/book/1013070398/catalog)
> 作者：卧牛真人

> 标签：未来世界

> 简介：浩瀚星辰，万族争锋，灵潮来袭，血战重启。灵山市和平街道花园社区幸福新村的治安形势格外严峻。当街道大妈都不跳广场舞，改练魔法和斗气，楚歌的传说，才刚刚开始。

> 章节总数：共1036章

> 状态：完本
# 游戏
### [《巫师》第二季恢复拍摄 大超已经回归片场](https://www.3dmgame.com/news/202101/3806643.html)
> 概要: 《巫师》第二季因为“大超”亨利·卡维尔受伤，拍摄进度已经耽误。近日，有传闻称《巫师》第二季已经恢复拍摄，亨利·卡维尔已经回归片场。据外媒Redanian Intelligence报道，Netflix《......
### [Netflix第四季度收入预期66亿美元 分析师普遍看好](https://www.3dmgame.com/news/202101/3806661.html)
> 概要: 据报道，Netflix即将发布最新的季度财报。随着行业逐渐进入疫情后经济时代，Netflix的财报结果将受到市场的密切关注。在第一季度和第二季度，Netflix的订阅用户增长强劲。彼时，很多美国人几乎......
### [再创佳绩 《集合啦！动物森友会》德国销量突破100万](https://www.3dmgame.com/news/202101/3806644.html)
> 概要: 尽管疫情形势依旧危急，但任天堂在2020年的销售业绩尤为惊人。这家游戏厂商不仅在日本和美国，还在世界各地取得了销量佳绩，尤其是在《集合啦！动物森友会》这部轰动一时的作品上。Game是一家德国电子游戏行......
### [3DM轻松一刻第444期 妹子大白腿这么折腾可惜了](https://www.3dmgame.com/bagua/4201.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！吃惊？我刚才真的胡了我先看完都给你分好了......
### [最强手机、芯片性能排名出炉！骁龙888力压麒麟9000](https://www.3dmgame.com/news/202101/3806619.html)
> 概要: 近日，鲁大师发布2020年度手机报告。在手机综合性能跑分排行榜中，华为Mate 40 Pro+以871220分的成绩获得年度机皇称号，ROG游戏手机3经典版（830135）、Redmi K30S至尊纪......
# 论文
### [Coupled Oscillatory Recurrent Neural Network (coRNN): An accurate and (gradient) stable architecture for learning long time dependencies](https://paperswithcode.com/paper/coupled-oscillatory-recurrent-neural-network)
> 日期：ICLR 2021

> 标签：SENTIMENT ANALYSIS

> 代码：https://github.com/tk-rusch/coRNN

> 描述：Circuits of biological neurons, such as in the functional parts of the brain can be modeled as networks of coupled oscillators. Inspired by the ability of these systems to express a rich set of outputs while keeping (gradients of) state variables bounded, we propose a novel architecture for recurrent neural networks.
### [Hyperspectral Image Clustering with Spatially-Regularized Ultrametrics](https://paperswithcode.com/paper/hyperspectral-image-clustering-with-spatially)
> 日期：10 Apr 2020

> 标签：IMAGE CLUSTERING

> 代码：https://github.com/ShukunZhang/Spatially-Regularized-Ultrametrics

> 描述：We propose a method for the unsupervised clustering of hyperspectral images based on spatially regularized spectral clustering with ultrametric path distances. The proposed method efficiently combines data density and geometry to distinguish between material classes in the data, without the need for training labels.
