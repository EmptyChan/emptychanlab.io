---
title: 2022-02-08-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SpeloncatoSnow_ZH-CN8115437163_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-08 21:58:58
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SpeloncatoSnow_ZH-CN8115437163_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Apache Wicket 9.8.0 发布，开源 Java Web 框架](https://www.oschina.net/news/181476/apache-wicket-9-8-0-released)
> 概要: Apache Wicket 是一个开源的面向 Java 组件的 Web 应用框架，为政府、商店、大学、城市、银行、电子邮件提供商等成千上万的 Web 应用和网站提供支持。Wicket 的开发中使用了语......
### [DBeaver 21.3.4 发布](https://www.oschina.net/news/181478/dbeaver-21-3-4-released)
> 概要: DBeaver 是一个免费开源的通用数据库工具，适用于开发人员和数据库管理员。DBeaver 21.3.4 发布，更新内容如下：Data editor:行的 fetch 大小现在可以设置为一个较小的值......
### [墨干编辑器 v1.0.0 发布](https://www.oschina.net/news/181477/mogan-1-0-0-released)
> 概要: 墨干编辑器是一个基于 GNU TeXmacs 的结构化编辑器。墨干编辑器衍生自 GNU TeXmacs（作者是 Joris van der Hoeven）。与 TeXmacs 的主要区别是：界面：从 ......
### [Swift 与 C++ 的互操作性工作组成立](https://www.oschina.net/news/181488/swift-and-c-interoperability-workgroup)
> 概要: 近日 Swift 社区发布公告，为了进一步提供 Swift 和 C++ 之间的互操作性支持，他们成立了 Swift 和 C++ 互操作性工作组，以作为 Swift 项目的一部分。这个新的工作组的职责就......
### [还在用HttpUtil？试试这款优雅的HTTP客户端工具吧，跟SpringBoot绝配！](https://segmentfault.com/a/1190000041371806?utm_source=sf-homepage)
> 概要: 我们平时开发项目时，就算是单体应用，也免不了要调用一下其他服务提供的接口。此时就会用到HTTP客户端工具，之前一直使用的是Hutool中的HttpUtil，虽然容易上手，但用起来颇为麻烦！最近发现一款......
### [You can change your number](https://signal.org/blog/change-number/)
> 概要: You can change your number
### [Suspense对React的意义在哪里？](https://segmentfault.com/a/1190000041372960?utm_source=sf-homepage)
> 概要: 大家好，我卡颂。可能很多朋友在项目中还没用过Suspense，但是Suspense是React未来发展非常重要的一环。本文会讲解Suspense对于React的意义。欢迎加入人类高质量前端框架群，带飞......
### [IRS to ditch biometric requirement for online access](https://krebsonsecurity.com/2022/02/irs-to-ditch-biometric-requirement-for-online-access/)
> 概要: IRS to ditch biometric requirement for online access
### [“买妻”背后的法律与正义](https://www.huxiu.com/article/496172.html)
> 概要: 来源：秦朔朋友圈（ID：qspyq2015）作者：悟00000空题图：视觉中国快过年的时候，在微信群里看到一个微博截图，一个妇女脖子上套着铁链被拴在一个破屋里，看完文字，整个人都不好了。为了比较喜乐地......
### [P站美图推荐——透明水彩特辑（二）](https://news.dmzj.com/article/73524.html)
> 概要: 经典画材透明水彩与二次元的融合风味特别浓而且好看透明感十足
### [组图：李晨现身三亚被偶遇 穿白色T恤戴虎头帽状态佳](http://slide.ent.sina.com.cn/star/slide_4_86512_366173.html)
> 概要: 组图：李晨现身三亚被偶遇 穿白色T恤戴虎头帽状态佳
### [「咒术回战」七海建人粘土人开订](http://acg.178.com/202202/438284184086.html)
> 概要: Good Smile Company作品「咒术回战」七海建人粘土人现已开订，手办高约100mm，主体采用塑料制造，原型师为Shuraken。该手办定价为4,620日元（含税），约合人民币254元，预计......
### [《极主夫道》真人电影首曝预告 原班人马爆笑来袭](https://acg.gamersky.com/news/202202/1458286.shtml)
> 概要: 真人电影《极主夫道》公开了首个特报预告，依旧由玉木宏担任主演，川口春奈、志尊淳、泷藤贤一等原班人马出演，此外还有打码的新角色登场。
### [令人放松愉快的连载，让大脑兴奋的停不下来](https://news.dmzj.com/article/73526.html)
> 概要: 这期给大家推荐一部推理谜题的校园漫画《比解谜还刺激》，作者远藤准，讲述在放学后的教师，解谜学弟x出题学姐一起开始无止境解谜日常的青春恋爱喜剧。
### [那些小县城楼市里的山寨品牌](https://www.huxiu.com/article/496215.html)
> 概要: 本文来自微信公众号：真叫卢俊（ID：zhenjiaolujun0426），作者：乔不丝，头图来自：作者拍摄这个话题，是我今年春节回家所见有感而发；那天我正在从一个小县城返回上海的高速路上，在我的老家路......
### [【直播预告】开工大吉！新年新直播来啦！](https://news.dmzj.com/article/73527.html)
> 概要: 本周直播预告
### [「LoveLive! Sunshine!!」BD第2弹发售告知CM公开](http://acg.178.com/202202/438291057521.html)
> 概要: 由日升动画制作的原创电视动画「LoveLive! Sunshine!!」近日公开了BD BOX的第二弹发售宣传CM，BD BOX除动画第二季BD光盘之外还包含了多张特典CD。该BD BOX定价4180......
### [童年阴影回归！Disney+将推出《鸡皮疙瘩》剧集](https://ent.sina.com.cn/v/u/2022-02-08/doc-ikyakumy4784984.shtml)
> 概要: 新浪娱乐讯 北京时间2月8日消息，据外国媒体报道，许多人的“童年噩梦”回归了。Disney+将推出著名小说系列《鸡皮疙瘩》的一部新真人剧集，该流媒体平台最新在TCA上宣布拿下此项目。　　本剧2020年......
### [轻小说「佐佐木与宫野 2年生」封面公开](http://acg.178.com/202202/438291382117.html)
> 概要: 近日，由春園ショウ创作的同名漫画改编，八条ことこ创作的番外篇轻小说「佐佐木与宫野 2年生」公开了最新封面图，该作正在好评发售中......
### [「鬼灭之刃」&ufotable官方公开富冈义勇生日贺图](http://acg.178.com/202202/438291776157.html)
> 概要: 今日（2月8日）是「鬼灭之刃」中富冈义勇的生日，动画制作公司ufotable和「鬼灭之刃」官方公开了为其绘制的生日贺图，祝富冈义勇生日快乐~富冈义勇是吾峠呼世晴创作的漫画「鬼灭之刃」及其衍生作品中的角......
### [《师父》今日发售 开发人员分享游戏生存提示](https://www.3dmgame.com/news/202202/3835211.html)
> 概要: 武打动作游戏《师父》于今日（2月8日）推出，Sloclap工作室的营销经理Félix今日也在PS博客上分享游戏中的生存提示以助各位玩家在复仇的路上一路顺风。《师父》是个具有挑战性的游戏。当然，根据玩家......
### [郭德纲发文为郭麒麟庆生 生日礼物是一件蟒](https://ent.sina.com.cn/s/m/2022-02-08/doc-ikyakumy4800054.shtml)
> 概要: 新浪娱乐讯 2月8日，郭德纲晒照为儿子郭麒麟庆生，他还晒出了给郭麒麟买的一件蟒。不少微博网友转发并评论表示：“卑微老父亲”“来自父亲沉甸甸的爱”。新(责编：小5)......
### [NV下一代Hopper架构：5nm工艺 晶体管超1400亿](https://www.3dmgame.com/news/202202/3835223.html)
> 概要: 近日据外媒Videocardz报道，英伟达下一代面向高性能计算、人工智能的Hopper架构将采用5nm工艺制程，晶体管超1400亿个，面积核心达900平方毫米，是有史以来最大的GPU。但这一说法遭到许......
### [声优下野纮第二次感染新冠后恢复健康](https://news.dmzj.com/article/73534.html)
> 概要: 因第二次感染新冠而休养的声优下野纮，在本日（8日）宣布活动再开的消息。下野纮的事务所称，正在休养的下野纮，已经达到了日本厚生劳动省认定的解除休养的标准，在经过相关部门确认后，从8日开始继续展开活动。
### [日升将于4月1日更名 变为“万代南梦宫film works”](https://acg.gamersky.com/news/202202/1458416.shtml)
> 概要: 以TV动画《机动战士高达》闻名的日升于今日宣布，公司名称将于4月1日更名为“万代南梦宫Film Works”，整合Sunrise和万代南梦宫Arts的视频部门万代南梦宫Rights Marketing。
### [NFT是纽约社交俱乐部的入场券](https://www.tuicool.com/articles/ANBb2eA)
> 概要: 当 Maxwell Tribeca 于 7 月开业时，它将拥有定义某种社交俱乐部的所有元素：享有盛誉的地址、时尚的装饰、会员的专属福利以及富有且人脉广泛的创始团队。但这对科技旅游平台 Mozio 的创......
### [新版逃离“北上广”？当“折叠的他乡”遇上“重叠的家乡”](https://www.tuicool.com/articles/JrYVvun)
> 概要: 图片：杨勇高  提供《沙丘》中，作家法兰克·赫伯特描绘了一个被沙漠覆盖的星球——厄拉科斯，对于主角保罗而言，这里是异乡，对于世世代代生活在厄拉科斯的弗雷曼人而言，这里是故乡。在尚未被电影呈现的《沙丘》......
### [400亿美元的收购英伟达收购ARM交易现已终止](https://www.3dmgame.com/news/202202/3835243.html)
> 概要: 2月8日下午，NVIDIA和软银双双确认ARM交易案终止，这笔400亿美元的收购本将成为芯片业最大并购案。NVIDIA在公告中将原因总结为监管阻碍，并称其与软银都做出了真诚的努力。公司CEO黄仁勋发声......
### [砸3000多亿 欧盟将推出芯片法案：8年内掌握2nm工艺](https://www.3dmgame.com/news/202202/3835250.html)
> 概要: 先进半导体制造可谓是高科技皇冠上的明珠，各个大国都想掌握这一优势，美国前不久推出了520亿美元的半导体促进方案，现在欧盟也要推出类似的计划，至少投资3000多亿元推动半导体产业发展。英国等媒体报道指出......
### [视频：冠军谷爱凌的另一面：每年都回海淀黄庄补课学奥数](https://video.sina.com.cn/p/ent/2022-02-08/detail-ikyamrmz9681171.d.html)
> 概要: 视频：冠军谷爱凌的另一面：每年都回海淀黄庄补课学奥数
### [比亚迪：K6 纯电动大巴在毛里求斯投入运营，该国总理出席发布仪式](https://www.ithome.com/0/602/123.htm)
> 概要: IT之家2 月 8 日消息，据比亚迪官方消息，近日，比亚迪在毛里求斯首都路易港（Port Louis）向当地最大的国营公共交通运营商 National Transport Cooperation 交付......
### [超市里的年货，静悄悄](https://www.tuicool.com/articles/3ymeAbZ)
> 概要: 也许再过几年，超市里的“年货”将会被人们彻底遗忘。今年春节，家在武汉的沈浩依旧没有前往超市采购年货。这是刚刚毕业的他所经历的连续第三个“简化版”春节。当然，除了没有像小时候一样跟随父母去超市挑选心仪的......
### [Tesla cut a steering component from some cars, didn’t tell customers](https://www.cnbc.com/2022/02/07/tesla-cut-a-steering-component-to-deal-with-chip-shortage.html)
> 概要: Tesla cut a steering component from some cars, didn’t tell customers
### [消息称一加 Pad 安卓平板将搭载 Android 12L 系统](https://www.ithome.com/0/602/135.htm)
> 概要: IT之家2 月 8 日消息，据印度媒体 91Mobile 的独家消息，一加即将推出的 OnePlus Pad 将配备专为平板电脑和可折叠手机设计的Android12L 系统。IT之家了解到，去年 7 ......
### [RankScience (YC W17) Is Hiring a Head of Content Marketing (Remote)](https://www.rankscience.com/jobs)
> 概要: RankScience (YC W17) Is Hiring a Head of Content Marketing (Remote)
### [排片太少，但它仍然是口碑之王！](https://new.qq.com/rain/a/20220208A09NNA00)
> 概要: 今年的春节档，本以为会竞争激烈。但上映后，除了《长津湖之水门桥》，其他几部票房表现均不如预期。票房与影片质量不完全挂钩。张艺谋导演的《狙击手》，豆瓣得分7.7，在几部影片中口碑位列第一。遗憾的是该片排......
### [如何写出让人抓狂的代码？](https://www.tuicool.com/articles/2QfYRbR)
> 概要: 大家好，我是苏三，又跟大家见面了。前言今天跟大家聊一个有趣的话题：如何写出让人抓狂的代码？大家看到这个标题，第一印象觉得这篇文章可能是一篇水文。但我很负责的告诉你，它是一篇有很多干货的技术文。曾几何时......
### [组图：虞书欣穿蓝色丝绒吊带裙性感甜辣 扎低丸子头撩发慵懒迷人](http://slide.ent.sina.com.cn/star/slide_4_86512_366193.html)
> 概要: 组图：虞书欣穿蓝色丝绒吊带裙性感甜辣 扎低丸子头撩发慵懒迷人
### [超强 AI 数字员工来了：金融、保险样样精通，不拿工资还不摸鱼](https://www.ithome.com/0/602/137.htm)
> 概要: 据报道，在当今劳动力短缺的情况下，许多行业的公司开始依赖数字员工自动完成工作，实现工作流程自动化，其中保险和金融领域的公司对其需求较大。RPA（robotic process automation，机......
### [俄罗斯美女Axilirator福利图赏 性感撩人欲罢不能](https://www.3dmgame.com/bagua/5161.html)
> 概要: 今天为大家带来的是俄罗斯美女Axilirator的福利图。妹子是俄罗斯圣彼得堡COSER，也是一名模特，还擅长自己制作COS服装和拍摄，可以说是多才多艺了。Axilirator经常在IG和推特上晒出自......
### [“墩”出了个涨停](https://www.huxiu.com/article/496350.html)
> 概要: 出品 | 虎嗅金融组作者 | 周月明题图 | IC photo一墩难求！冬奥会愈加火热，冬奥吉祥物冰墩墩也迅速走红，圆滚滚的大熊猫，身穿一层冰雪外壳，憨态可掬，已经火速晋升为新一代“顶流”。微博上、抖......
### [赴华冬奥记者发现“中国真清零了”，《纽约时报》播客节目主持人震惊](https://finance.sina.com.cn/china/2022-02-08/doc-ikyakumy4882404.shtml)
> 概要: 【文/观察者网 熊超然】为抹黑中国抗疫成就，以《纽约时报》为代表的部分美媒此前曾多次炮制攻击中国“清零政策”的报道文章。如今正值北京冬奥会举行之际...
### [北京警方：3人高价倒卖“冰墩墩”被处罚](https://finance.sina.com.cn/jjxw/2022-02-08/doc-ikyamrmz9709807.shtml)
> 概要: 据“平安北京”公众号2月8日通报，随着北京冬奥会的进行，冬奥会吉祥物“冰墩墩”受到热捧，体现了广大市民群众关心冬奥、支持冬奥的热情。
### [90后银行柜员骗取单位近300万贷款，投向酒吧、会所等，领导竟然还给他升了职！](https://finance.sina.com.cn/jjxw/2022-02-08/doc-ikyamrmz9709819.shtml)
> 概要: 日前，银保监会益阳监管分局一日之内对益阳某银行开出13张罚单，主要披露了该行4家支行的行长、客户经理等人在任职期间存在的违法违规行为。
### [LG 推出 UltraGear 桌面游戏音箱：20W 功率，Hi-Res 认证](https://www.ithome.com/0/602/147.htm)
> 概要: IT之家2 月 8 日消息，在今年 CES 上，LG 发布了 UltraGear 游戏显示器 32GQ950，同时也发布了一款 UltraGear 桌面游戏音箱。现在，这款 UltraGear GP9......
### [2月9日起 郑州网吧、KTV等娱乐场所有序恢复开放](https://finance.sina.com.cn/china/gncj/2022-02-08/doc-ikyakumy4882981.shtml)
> 概要: 记者从郑州市新冠肺炎疫情防控指挥部办公室了解到：为统筹推进疫情防控和社会经济发展，经郑州市新冠肺炎疫情防控指挥部研究决定，恢复部分行业生产生活秩序...
### [全国人大外事委员会就美国国会众议院通过“2022年美国竞争法案”发表声明](https://finance.sina.com.cn/china/2022-02-08/doc-ikyakumy4882911.shtml)
> 概要: 原标题：全国人大外事委员会就美国国会众议院通过“2022年美国竞争法案”发表声明 新华社北京2月8日电 2月8日，针对美国国会众议院通过“2022年美国竞争法案”...
### [美国会众议院通过“2022年美国竞争法案”，全国人大外事委回应](https://finance.sina.com.cn/china/2022-02-08/doc-ikyamrmz9712710.shtml)
> 概要: 2月8日，针对美国国会众议院通过“2022年美国竞争法案”，全国人大外事委员会发表声明。全文如下： 美国国会众议院近日通过的“2022年美国竞争法案”涉华内容充斥冷战思维和意...
# 小说
### [无敌赘婿魔尊](http://book.zongheng.com/book/850870.html)
> 作者：拾梦者

> 标签：奇幻玄幻

> 简介：我有一剑，可斩群仙！我有一身，可碎诸天！我叫楚天，无法无天……

> 章节末：第四百五十二章 一万年

> 状态：完本
# 论文
### [Benchmarking human visual search computational models in natural scenes: models comparison and reference datasets | Papers With Code](https://paperswithcode.com/paper/benchmarking-human-visual-search-1)
> 日期：10 Dec 2021

> 标签：None

> 代码：None

> 描述：Visual search is an essential part of almost any everyday human goal-directed interaction with the environment. Nowadays, several algorithms are able to predict gaze positions during simple observation, but few models attempt to simulate human behavior during visual search in natural scenes.
### [PixMix: Dreamlike Pictures Comprehensively Improve Safety Measures | Papers With Code](https://paperswithcode.com/paper/pixmix-dreamlike-pictures-comprehensively)
> 日期：9 Dec 2021

> 标签：None

> 代码：None

> 描述：In real-world applications of machine learning, reliable and safe systems must consider measures of performance beyond standard test set accuracy. These other goals include out-of-distribution (OOD) robustness, prediction consistency, resilience to adversaries, calibrated uncertainty estimates, and the ability to detect anomalous inputs.
