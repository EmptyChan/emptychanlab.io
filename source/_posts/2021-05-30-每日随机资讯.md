---
title: 2021-05-30-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeaDog_EN-CN1435830380_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-30 22:31:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeaDog_EN-CN1435830380_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [阿里云开源 PolarDB for PostgreSQL 数据库](https://www.oschina.net/news/143703/aliyun-opensource-polardb-for-postgresql)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>在5月29日举办的2021阿里云开发者大会上，阿里云宣布了“阿里云数据库开源计划”，并率先开源了 PolarDB for Postgre......
### [CoSky(Govern-Service) 1.0.0 发布，基于 Redis 的服务治理平台](https://www.oschina.net/news/143733/cosky-1-0-0-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>CoSky基于 Redis 的服务治理平台（服务注册/发现 & 配置中心）Consul + Sky=CoSkyCoSky是一个轻量级、低......
### [Apache Pulsar 2.7.2 版本发布](https://www.oschina.net/news/143702/pulsar-2-7-2-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>关于 Apache PulsarApache Pulsar 是 Apache 软件基金会顶级项目，是下一代云原生分布式消息流平台，集消息......
### [SphereEx 加入 openGauss 社区](https://www.oschina.net/news/143704)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>SphereEx 签署 CLA ( Contribution License Agreement，贡献许可协议），正式加入 openGa......
### [为什么我们会一次又一次地进行性格测试？](https://www.huxiu.com/article/431473.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡题图 | 视觉中国本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。这两天，性格颜色测试又......
### [Trials begin on lozenge that rebuilds tooth enamel](https://dental.washington.edu/trials-begin-on-lozenge-that-rebuilds-tooth-enamel/)
> 概要: Trials begin on lozenge that rebuilds tooth enamel
### [这届年轻人，把裸辞当充电？](https://www.huxiu.com/article/431469.html)
> 概要: 本文来自微信公众号：字母榜（ID：wujicaijing），作者：薛亚萍，编辑：蒋晓婷，原文标题：《这届年轻人，把裸辞当充电？》，题图：视觉中国工作不舒心，要不要裸辞？大多数职场前辈都会告诫：千万不要......
### [What is the Fourth Dimension? (1884)](https://en.wikisource.org/wiki/What_is_the_Fourth_Dimension%3F)
> 概要: What is the Fourth Dimension? (1884)
### [GPG-Tui, a Terminal User Interface for GnuPG](https://orhun.dev/blog/introducing-gpg-tui/)
> 概要: GPG-Tui, a Terminal User Interface for GnuPG
### [徐新驰工作室发声明否认恋情：专注演戏且单身](https://ent.sina.com.cn/s/m/2021-05-30/doc-ikmyaawc8335986.shtml)
> 概要: 新浪娱乐讯 5月29日，有疑似徐新驰站姐脱粉回踩，并曝出徐新驰私下恋爱且私联粉丝。随后徐新驰工作室发声明否认，表示徐新驰目前一直在专注学业及演艺事业，并且目前是单身状态。网络上的传言均为不实，将保留法......
### [伪高颜值cp的超甜恋爱，俗套的互相治愈的故事](https://news.dmzj1.com/article/71050.html)
> 概要: 日本4月春季恋爱小甜饼剧《盛装打扮的恋爱也有理由》，由川口春奈、横滨流星主演，该剧讲述真柴久留美在室内装饰公司工作必须盛装打扮才安心，与价值观不同的合租人们相处，她逐渐做回真正的自己，也收获藤野骏的爱情的故事。
### [「SSSS.DYNAZENON」第9话部分原画摄公开](http://acg.178.com/202105/416341426875.html)
> 概要: 由TRIGGER制作的原创电视动画「SSSS.DYNAZENON」于近期公开了第9话的部分原画摄，动画已于2021年4月2日开始播出。「SSSS.DYNAZENON」第9话部分原画摄......
### [「Vivy -Fluorite Eye's Song-」第三话插曲MV公开](http://acg.178.com/202105/416341901464.html)
> 概要: 由Wit Studio制作的原创电视动画作品「Vivy -Fluorite Eye's Song-」于近期公开了插曲「A Tender Moon Tempo」的动画MV，动画已于4月3日开始播出。「A......
### [矢吹健太朗「出包王女」最新插画公开](http://acg.178.com/202105/416342135568.html)
> 概要: 为纪念个人推特粉丝突破30万，日本漫画家矢吹健太朗公开了其绘制的「出包王女」三姐妹插画。「出包王女」是动漫编剧长谷见沙贵负责脚本、矢吹健太朗负责作画的漫画作品，于「周刊少年Jump」上连载，讲述了男主......
### [“医美只花几杯奶茶钱”](https://www.huxiu.com/article/431484.html)
> 概要: 本文来自微信公众号：Vista看天下（ID：vistaweek），作者：冯美丽，原文标题：《“医美只花几杯奶茶钱”，今年听过最大的笑话》，题图来自：《谈判官》剧照不知道你们会不会时常有这样一种感觉：有......
### [与李子璇传恋情 周翊然工作室否认:一直是单身](https://ent.sina.com.cn/s/m/2021-05-30/doc-ikmxzfmm5499007.shtml)
> 概要: 新浪娱乐讯 5月30日，周翊然工作室声明表示周翊然是单身状态。声明中承认周翊然即是李子璇恋情传闻中的男生，“当天和同小区朋友吃饭，送完朋友就离开了”。周翊然工作室还针对周翊然被拍到的抽烟行道歉。　　今......
### [Purpose以太坊ETF持仓量达5.16万枚ETH](https://www.btc126.com//view/168586.html)
> 概要: OKLink数据显示，目前Purpose以太坊ETF持仓量达5.16万枚ETH，总值约1.18亿美元......
### [传三星/LG开始为iPhone 13生产OLED屏：高配120Hz](https://www.3dmgame.com/news/202105/3815501.html)
> 概要: 本周，有消息称，用于iPhone 13系列机型的A15处理器已经在台积电规模投产，且预计产能超去年的A14。作为iPhone 13另一大关键元器件，最新报道称，三星、LG已经开始生产前者所需的OLED......
### [苏富比拍卖NFT“Two Degrees”，将在全球气温升高2°C后自行销毁](https://www.btc126.com//view/168589.html)
> 概要: Decrypt消息，苏富比拍卖行正在拍卖一件NFT，如果全球平均气温比工业化前的水平上升2摄氏度（35.6华氏度），此NFT就可以被销毁。该NFT名为“Two Degrees”，是一个3D扫描德国南部......
### [午行情：BTC于34800 USDT附近震荡](https://www.btc126.com//view/168590.html)
> 概要: BTC上午突破35101USDT后坠至33379 USDT，目前回升至34800 USDT附近。BTC在欧易OKEx现报34880.7 USDT，24H跌幅-4.06%。1. 全球数字货币市场总价值为......
### [漫画「朋友的妹妹只喜欢烦我」第3卷封面公开](http://acg.178.com/202105/416347943780.html)
> 概要: 漫画「朋友的妹妹只喜欢烦我」第3卷封面图正式公开，该册将于6月7日发售。漫画「朋友的妹妹只喜欢烦我」改编自三河ごーすと创作的同名轻小说作品，由平冈平负责作画。此外，本作于2021年1月31日宣布了TV......
### [IPO留言板丨京东物流登陆港交所，「东鹏饮料」连续涨停，网易云音乐分拆上市，「爱玛电动车」9年上市梦？](https://www.tuicool.com/articles/JnQFFjm)
> 概要: 全新栏目《IPO留言板》——带你紧追本周IPO热点。文|李欣封面来源|视觉中国新股上市京东物流5月28日，京东物流（02618.HK）在港交所挂牌上市，上市首日开盘涨14%，报46.05港元。当日收盘......
### [Autofarm：受Belt Finance闪电贷攻击事件影响，已暂停4Belt vault存款](https://www.btc126.com//view/168594.html)
> 概要: DeFi跨链收益聚合器Autofarm发推称，Belt Finance暂停了4Belt池存取款，以便做一个完整的分析并进行合约升级。Autofarm也暂时禁止了4Belt vault存款。注：所有其他......
### [李斌：蔚来汽车平均售价 43.47 万元，已初步建成高端品牌](https://www.ithome.com/0/554/384.htm)
> 概要: IT之家5 月 30 日消息 第四届全国青年企业家峰会今日在南京举行。会上，蔚来汽车创始人、董事长、CEO 李斌透露，截止 4 月底，在过去将近三年的时间里，蔚来已经交付了 102803 台车，平均售......
### [《鬼灭之刃》富冈义勇全新手办 特效配件形神兼备](https://www.3dmgame.com/news/202105/3815502.html)
> 概要: 来自超人气动漫《鬼灭之刃》的人气角色富冈义勇的全新手办日前公开，附赠特技配件形神兼备，一起来了解下。·富冈义勇是漫画《鬼灭之刃》及其衍生作品中的角色。鬼杀队中的水柱。外表高冷的剑士，不善于表达言辞。不......
### [《原子之心》“已经就绪” 将于E3公布最新预告片](https://www.3dmgame.com/news/202105/3815506.html)
> 概要: 《原子之心》开发商Mundfish近日透露称本作开发工作已经接近完成，团队正在对这款开放世界FPS游戏进行后期优化，并同时制作主机版本。开发团队一位网名为Zace的成员在游戏官方Discord频道介绍......
### [知名声优石川由依宣布结婚 对象为一般男性](https://acg.gamersky.com/news/202105/1392857.shtml)
> 概要: 据雅虎日本发布的报道，知名声优石川由依今天（30日）更新了自己的推特。在推文中她宣布了自己结婚的消息，对象是一位普通男性。
### [交出 Q1 成绩单，蔚来、理想、小鹏造车三兄弟背后的喜与忧](https://www.ithome.com/0/554/395.htm)
> 概要: 随着理想汽车近日发布 2021 年第一季度未经审计财务报告，至此，包括蔚来汽车、小鹏汽车在内的造车新势力 Q1 财报已全部出炉。在国内新能源市场蓬勃发展之下，并且特斯拉在国内增长势头放缓情况下，这三家......
### [FIRST公布竞赛评委会及主视觉，期待“思辨性影像语言”](https://new.qq.com/omn/20210530/20210530A058JZ00.html)
> 概要: 第十五届FIRST电影展年度发布会海报第十五届FIRST青年电影展“电影的，未来的”年度发布会日前在北京举办，FIRST影展CEO李子为提出“前瞻性，探索性”定位，以影展串联中国电影产业中各版块人才，......
### [理想汽车：" 理想同学”即将升级全车自由对话功能](https://www.ithome.com/0/554/398.htm)
> 概要: IT之家5 月 30 日消息 今日，理想汽车宣布，" 理想同学”即将升级全车自由对话功能。理想汽车表示，" 理想同学”不只会答应车主听歌、导航、开天窗的需求，还可以帮车主查天气、讲笑话、看股市。此外，......
### [组图：张紫宁条纹衫哈伦裤时尚感爆棚  秀小猫咪屏保可可爱爱](http://slide.ent.sina.com.cn/y/slide_4_704_357303.html)
> 概要: 组图：张紫宁条纹衫哈伦裤时尚感爆棚  秀小猫咪屏保可可爱爱
### [港式甜品老字号“许留山”被曝遭清盘，会员退费无门成难题](https://www.tuicool.com/articles/zYZR3qU)
> 概要: 很多人关于港式甜品的记忆里，都有一款甜品，是许留山的芒果西米露。多家香港媒体5月26日报道称，香港高等法院聆案官考虑许留山食品制造有限公司无力偿还债务，对该公司颁发了清盘令。据了解，清盘是一种法律程序......
### [汤臣倍健旗下：胶原蛋白早餐奶 2 瓶 9.9 元（立减 20 元）](https://lapin.ithome.com/html/digi/554413.htm)
> 概要: 【食代说旗舰店】汤臣倍健旗下：食代说 胶原蛋白早餐奶 250ml*2 瓶 售价 29.9 元，今日可领 20 元清仓券，实付 9.9 元包邮。2021 年 9 月到期，介意慎拍～天猫汤臣倍健 胶原蛋白......
### [RTX 3080 Ti挖矿性能曝光：老黄一刀砍废](https://www.tuicool.com/articles/fQvAjum)
> 概要: 看起来，NVIDIA RTX 3070 Ti和3080 Ti显卡有望在台北电脑展期间发布。日前，RTX 3080 Ti以太坊的挖矿算力曝光，只有58MH/s，而且功耗高达299瓦。不得不说老黄上马LH......
### [The Muse (YC W12) Is Hiring a Senior Platform Engineer](https://jobs.lever.co/themuse/f7bfe873-0ff0-487b-8544-1c57c898c996?lever-origin=applied&lever-source%5B%5D=YC)
> 概要: The Muse (YC W12) Is Hiring a Senior Platform Engineer
### [视频：恋爱了？李子璇被拍与白衬衫小鲜肉吃饭小区散步](https://video.sina.com.cn/p/ent/2021-05-30/detail-ikmyaawc8422962.d.html)
> 概要: 视频：恋爱了？李子璇被拍与白衬衫小鲜肉吃饭小区散步
### [视频：霍启刚夫妇挽手甜蜜 郭晶晶腹部平坦破四胎传言](https://video.sina.com.cn/p/ent/2021-05-30/detail-ikmyaawc8423094.d.html)
> 概要: 视频：霍启刚夫妇挽手甜蜜 郭晶晶腹部平坦破四胎传言
### [紧急通知！广州高考时间不变，高二学生暂不参加学考](https://finance.sina.com.cn/china/2021-05-30/doc-ikmxzfmm5564563.shtml)
> 概要: 5月30日，广州市教育局发布《关于全力保障广州市平安高考的温馨提示》，全文如下： 今年是广东新高考的第一年。广州市有54900名考生报名参加高考，社会高度关注。
### [深圳市教育局：暂停所有校园聚集性活动](https://finance.sina.com.cn/china/2021-05-30/doc-ikmxzfmm5566466.shtml)
> 概要: 原标题：深圳市教育局：暂停所有校园聚集性活动 来源：央视新闻客户端 5月30日20时，深圳市举行疫情防控新闻发布会，通报疫情防控最新情况。
### [除了优化资源配置 产业联盟对长三角一体化还有哪些作用](https://finance.sina.com.cn/jjxw/2021-05-30/doc-ikmyaawc8431927.shtml)
> 概要: 5月26日，2021年度长三角地区主要领导座谈会在江苏无锡举行；5月27日，长三角一体化发展高层论坛上，举行了长三角科技创新共同体建设办公室，长三角集成电路、生物医药...
### [从金字招牌到科技新品 上海品牌100+聚焦哪些领域](https://finance.sina.com.cn/jjxw/2021-05-30/doc-ikmxzfmm5570011.shtml)
> 概要: 从国民经典大白兔奶糖、百雀羚面霜，再到最新的消毒机器人、新能源汽车，上海制造从传统金字招牌扩展至时尚消费和科技智造。
### [董明珠、俞敏洪谈“躺平”、“内卷”：年轻人应该讲奋斗精神](https://finance.sina.com.cn/stock/hyyj/2021-05-30/doc-ikmxzfmm5569847.shtml)
> 概要: 董明珠、俞敏洪谈“躺平”、“内卷”：年轻人应该讲奋斗精神 近日，一篇《躺平即是正义》的文章火了。作者在文中大谈“躺平即正义”，描述了自己是如何在两年没有稳定工作的情况...
### [骗贷"成瘾"：银行员工一年骗贷34次 离婚协议、户口本、房产证全是伪造](https://finance.sina.com.cn/china/2021-05-30/doc-ikmyaawc8432716.shtml)
> 概要: 原标题：骗贷“成瘾”！银行员工一年骗贷34次，离婚协议、户口本、房产证、信用报告全是伪造！ 又见一起银行员工骗贷案。 裁判文书网日前公布的一则刑事判决书显示...
### [露营模拟器《模拟露营：小队》现已登陆SteamEA](https://www.3dmgame.com/news/202105/3815528.html)
> 概要: 露营模拟器游戏《模拟露营：小队（Camping Simulator: The Squad）》现已登陆了Steam抢先体验，支持简体中文。截止到目前，游戏获得玩家“好评”评价。关于这款游戏：营地模拟器可......
### [时隔两年之后，流浪大师沈巍重回观众视野，眼神不再坚定充满忧郁](https://new.qq.com/omn/20210530/20210530A09UFU00.html)
> 概要: 日前，自从2020年上半年宣布无限期退网之后，曾经火爆全网的流浪大师沈巍就很少出现在观众视野。而这两年时间，也有很多像沈巍这样现象级的网红脱颖而出，像大家熟悉的理塘丁真，山东拉面哥和曹县大硕等，纵观这......
### [流言板穆勒：我能理解勒夫当初的决定，只不过沟通方式还有待讨论](https://bbs.hupu.com/43257501.html)
> 概要: 虎扑05月30日讯 德国前锋穆勒出席了新闻发布会，谈到了自己回归国家队之后的一些感受，以及关于部分年轻的德国队友的评价。【关于自己的回归】“我离开这里的时间有一点长，不过不太清楚确切是多久，团队非常欢
### [流言板本赛季欧冠抢断榜：阿斯皮利奎塔和若日尼奥26次并列第一](https://bbs.hupu.com/43257597.html)
> 概要: 虎扑05月30日讯 随着今天凌晨切尔西在决赛当中击败曼城，本赛季欧冠联赛也落下了帷幕。著名数据统计网站Squawka也统计了本赛季欧冠抢断榜，本赛季冠军球队切尔西的正副队长阿斯皮利奎塔和若日尼奥以26
### [假燕窝、山寨机横行，带货主播到底真不懂产品，还是故意坑网友？](https://new.qq.com/omn/20210530/20210530A09XXK00.html)
> 概要: 众所周知，直播带货是一个非常普遍的行业，随着互联网不断发展，人们已经从最开始不信任网红，到如今普遍在网红直播间购物，这是一种意识的转变，也标志着时代正在进步。            不过由于直播带货行......
### [柯震东大谈前任后再次直播睡觉，吸引近7000人观看，人气飙高](https://new.qq.com/omn/20210530/20210530A09Y8300.html)
> 概要: 5月30日下午，柯震东在大谈前任后再次直播，吸引来近7000多人的观看，人气直线飙高！在整个直播的过程中，摄像头都是全黑的，粉丝们只能听到柯震东轻微的呼吸声以及偶尔的打呼声，但哪怕如此，依然有那么多人......
### [流言板李铁：感谢球迷营造了良好氛围，最满意队员的精神状态](https://bbs.hupu.com/43257715.html)
> 概要: 虎扑05月30日讯 世预赛今晚继续进行，国足7-0大胜关岛。赛后，主教练李铁出席新闻发布会。总结本场比赛首先我还是为球迷们说一声谢谢，今天营造了非常好的主场氛围，球员很兴奋，对我来说，球队取得胜利，球
### [流言板感觉怎么样？皇马新赛季主场球衣宣传照曝光](https://bbs.hupu.com/43257706.html)
> 概要: 虎扑05月30日讯 知名足球装备网站FootyHeadline在今天独家披露了皇马21-22赛季主场球衣的宣传照。新赛季的主场球衣为白色，搭配蓝色和橙色，而上面的漩涡状图案则是收到了丰收女神广场的喷泉
### [2021年LOGO设计趋势报告](https://www.tuicool.com/articles/Y7fM7fM)
> 概要: 在GrayDesign去年6月发布的文章《2020年LOGO设计趋势报告》中，分享了由LOGO设计资讯网站LogoLounge发布的标志设计趋势报告。近日，我们关注到LogoLounge正式发布了20......
### [《小小梦魇》免费后在线玩家暴涨 Steam超7.7万人](https://www.3dmgame.com/news/202105/3815529.html)
> 概要: 恐怖解迷游戏《小小梦魇》近期在Steam开启了限时免费领取，看起来很多玩家并不仅仅是喜加一，而且还真的体验了这款经典作品。根据游戏的SteamCharts统计，《小小梦魇》5月28日在线玩家还是483......
# 小说
### [战锤40K征途](http://book.zongheng.com/book/630503.html)
> 作者：山岳之王

> 标签：二次元

> 简介：多诺凡星球上的混乱一刻也没有停息过，这里到处充满着战火，邪恶的力量称霸一方，人民生活在水深火热之中，就在这千钧一发的时刻，铁血上尉泰图斯降临了，他将带领着一群新兵经历血与火的考验，给这个濒死的星球带来正义与希望，邪恶将被净化，审判终将降临！！

> 章节末：（一百六十六）大结局

> 状态：完本
# 论文
### [Non-Autoregressive Dialog State Tracking](https://paperswithcode.com/paper/non-autoregressive-dialog-state-tracking-1)
> 日期：19 Feb 2020

> 标签：DIALOGUE STATE TRACKING

> 代码：https://github.com/henryhungle/NADST

> 描述：Recent efforts in Dialogue State Tracking (DST) for task-oriented dialogues have progressed toward open-vocabulary or generation-based approaches where the models can generate slot value candidates from the dialogue history itself. These approaches have shown good performance gain, especially in complicated dialogue domains with dynamic slot values.
### [Adversarial Meta Sampling for Multilingual Low-Resource Speech Recognition](https://paperswithcode.com/paper/adversarial-meta-sampling-for-multilingual)
> 日期：22 Dec 2020

> 标签：META-LEARNING

> 代码：https://github.com/iamxiaoyubei/AMS

> 描述：Low-resource automatic speech recognition (ASR) is challenging, as the low-resource target language data cannot well train an ASR model. To solve this issue, meta-learning formulates ASR for each source language into many small ASR tasks and meta-learns a model initialization on all tasks from different source languages to access fast adaptation on unseen target languages.
