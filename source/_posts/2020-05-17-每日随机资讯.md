---
title: 2020-05-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LofotenIslands_EN-CN2859197450_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-17 21:16:28
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LofotenIslands_EN-CN2859197450_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：指明星足球队成员无情 罗家英组“兄弟班”助粤剧界](https://video.sina.com.cn/p/ent/2020-05-17/detail-iircuyvi3571480.d.html)
> 概要: 视频：指明星足球队成员无情 罗家英组“兄弟班”助粤剧界
### [视频：张韶涵洛天依合唱《芒种》 破次元壁热舞battle](https://video.sina.com.cn/p/ent/2020-05-17/detail-iirczymk2097467.d.html)
> 概要: 视频：张韶涵洛天依合唱《芒种》 破次元壁热舞battle
### [组图：蒋劲夫现身北京机场 独自拉行李箱低头玩手机](http://slide.ent.sina.com.cn/star/slide_4_704_338832.html)
> 概要: 组图：蒋劲夫现身北京机场 独自拉行李箱低头玩手机
### [发存货了！刘亦菲清早晒自拍 泡花茶生活精致悠然](https://ent.sina.com.cn/s/m/2020-05-17/doc-iircuyvi3511903.shtml)
> 概要: 新浪娱乐讯 5月17日，刘亦菲在微博晒自拍照，并配文：“发存货”。在晒出的照片中，刘亦菲身穿紫色上衣，耳朵上戴着可爱的小熊耳钉，长发半掩面，面色红润。她还晒出了自己泡的花茶的照片，看起来十分惬意......
### [“网课推荐肖战”老师被停课 家长称有孩子哭了](https://ent.sina.com.cn/s/m/2020-05-17/doc-iirczymk2088266.shtml)
> 概要: 5月15日中午，一段指称“厦门某中学老师网课宣传肖战”的音视频在网络热传，引发争议，当地教育部门称，接到大量群众举报并第一时间核实。　　15日晚，厦门市思明区教育局在“思明教育网”发布通报称，经查，网......
### [罗云熙被指“照骗”？生图曝光被批干瘪苍老，与男神形象差别太大](https://new.qq.com/omn/20200517/20200517A0F32S00.html)
> 概要: 比起活在聚光灯、精修图中的神仙颜值，罕见曝光的未修图才是观众热议的话题，毕竟少数能抗住生图的男神女神才是真正的强者！            5月16日晚，当红小生罗云熙现身某平台直播，因为是残酷的怼脸......
### [知名男星突然逝世年仅16岁，死因曝光因吸毒过量](https://new.qq.com/omn/20200517/20200517A0HKL400.html)
> 概要: 5月17日，外媒曝光好莱坞童星罗根威廉斯的死因。今年4月，他突然在家中去世，年仅16岁。罗根威廉斯成名于好莱坞，他曾在电影《闪电侠》饰演了少年闪电侠，后来还有很多其他角色。因为长相比较好，所以罗根威廉......
### [票房190万，华语首部经典乐队传记电影，《兄弟班》做错了什么？](https://new.qq.com/omn/20200517/20200517A0DWQD00.html)
> 概要: 原来你我不相见传来问候更暖心中至真至诚绵绵千载此心不变《千载不变》【票房190万，华语首部经典乐队传记电影《兄弟班》，做错了什么？】是香港乐坛最资深的殿堂级乐队，，至今已经47年；比另一支最著名的摇滚......
### [直播与照片判若两人！周扬青回应太直接：P图是网红的职业素养](https://new.qq.com/omn/20200517/20200517A0FUOL00.html)
> 概要: 罗志祥与周扬青分手事件已经告一段落，但有关罗志祥的各种传闻依然是众人追逐的热点。而这件事对罗志祥的负面影响仍在继续，目前，罗志祥内地和台湾双向的节目通告都已暂停，作为受伤害最深的当事人周扬青，却显然并......
### [杨超越打脸吴宣仪？火箭少女解散在即，塑料姐妹花又出幺蛾子](https://new.qq.com/omn/20200517/20200517A0HJGM00.html)
> 概要: 凭借着选秀节目《创造101》成功出道的本土女团，火箭少女101组合可以说是这两年内国内最火的女团了，成员杨超越、孟美岐、吴宣仪等每一个人单独拎出来都是能够独当一面的圈内流量担当，可想而知火箭少女101......
# 动漫
### [拍摄备案公示 《旅行青蛙》动画电影制作决定](https://news.dmzj.com/article/67404.html)
> 概要: 《旅行青蛙》动画电影制作决定！据国家电影局动画电影拍摄制作备案公示表显示，《旅行青蛙》将由阿里巴巴影业拍摄制作国产动画电影。
### [TV动画《恶棍DRIVE》延期至2020年10月播出](https://news.dmzj.com/article/67403.html)
> 概要: 原定7月播出的TV动画《恶棍DRIVE》近日官网通知将延期至2020年10月播出。
### [弟弟带男朋友回家了！？](https://news.dmzj.com/article/67401.html)
> 概要: 本周要介绍的是BL漫画，来自愛葉もーこ的单行本《弟弟带来的恋人是男高中生》，讲述了热爱BL的女主·惠美发现自己的弟弟居然就是个GAY的故事。
### [新漫周刊第85期 一周新漫推荐(20200517期)](https://news.dmzj.com/article/67400.html)
> 概要: 大家在家隔离这俩月可谓是闲出花来了，本期作品数量跟上期一样，把往期回顾给挤没了……笨女孩作者和初恋僵尸的作者分别开了新的本色连载，此外不知道哪位同学给上传了跟精灵之蛋一时瑜亮的修萝剑圣，还有欢乐向的IE娘和小美人鱼等着大家去疼爱（溜了溜了.jpg
### [如果要评选日本四大名著，龙珠海贼入选，剩余两个名额谁能入围？](https://new.qq.com/omn/20200517/20200517A0HFXH00.html)
> 概要: 如果要说评选日本漫画中的四大名著，很多人内心都有自己的想法，毕竟日漫太多经典的作品了，能进入名著，必然是有一定的优势，以及无人能及的影响力，否者是不可能入选的。目前来说，日漫的作品虽然是很多，但是能……
### [忍界大战中被手鞠一直吐槽的两位可爱妹子，后来怎么样了？](https://new.qq.com/omn/20200517/20200517A0FGQ600.html)
> 概要: 第四次忍界的中，手鞠与鹿丸和丁次被分配到第四部队-远距离战斗联合军部队，在和敌人战斗的时候手鞠一直鼓励鹿丸，希望他能够拿出首领应有的姿态，之后由于战略调整，鹿丸离队，手鞠便代替了鹿丸成为了第四部队代……
### [B站看动漫最常见的弹幕“梗”，“你脸红个泡泡茶壶”很多人不懂](https://new.qq.com/omn/20200517/20200517A0FF8900.html)
> 概要: 在B站看番，相信很多漫迷在享受番剧的同时也在享受弹幕中的乐趣，尤其是一些比较“搞笑”的弹幕经常能逗得我们开怀大笑。弹幕在看动漫的过程中有很大的“情绪调节”作用，所以很多漫迷都非常愿意开着弹幕看动漫。……
### [星辰变：秦羽为父助阵，先秒伍德又杀项央，海底世界副本即将开始](https://new.qq.com/omn/20200517/20200517A0FLBC00.html)
> 概要: 导读：阅文旗下的《星辰变》动画已经更新至第十六集，秦羽与小黑从蛮荒回到潜龙大陆，来了一段温情的父子兄弟团聚时刻，立马就开启了战斗模式。秦羽为父助阵，先秒伍德又杀项央，海底世界副本即将开始。文/阿岩《……
### [《鬼灭之刃》大结局分析，表面看起来大团圆，实际上烂尾很严重！](https://new.qq.com/omn/20200517/20200517A0C1EB00.html)
> 概要: 导语：近期，日本人气动漫《鬼灭之刃》正式完结，在它的最后一话中，主角们的后代纷纷登场，另外，鬼杀队的柱们也以转世的方式出现在了剧情中。那么问题来了，作为近两年来最火的动漫，《鬼灭之刃》的这个大结局究……
### [名侦探柯南最常见的四种弹幕，扯cp不算啥，这个弹幕让人哭笑不得](https://new.qq.com/omn/20200517/20200517A0G1RE00.html)
> 概要: 作为一部连载了二十多年的作品，《名侦探柯南》对于漫迷的影响力真不小，毕竟现如今是唯一一部可以和《海贼王》抗衡的动漫。长时间的连载吸引了一批又一批的漫迷入坑，因此有些“掐架”的弹幕至今都停不下来。土豆……
# 财经
### [中共中央 国务院：加快川藏铁路、沿江高铁等重大工程规划建设](https://finance.sina.com.cn/roll/2020-05-17/doc-iirczymk2116185.shtml)
> 概要: 新华社受权发布《中共中央、国务院关于新时代推进西部大开发形成新格局的指导意见》。意见指出，强化基础设施规划建设。提高基础设施通达度、通畅性和均等化水平...
### [2019年申请的344个贫困县全部脱贫摘帽](https://finance.sina.com.cn/china/2020-05-17/doc-iirczymk2121989.shtml)
> 概要: 5月16日，云南省人民政府批准鲁甸县等31个县市区退出贫困县行列，至此，我国2019年申请脱贫摘帽的344个贫困县实现全部脱贫摘帽。
### [中共中央 国务院：新时代推进西部大开发形成新格局](https://finance.sina.com.cn/china/2020-05-17/doc-iircuyvi3588792.shtml)
> 概要: 新华社北京5月17日电 强化举措推进西部大开发形成新格局，是党中央、国务院从全局出发，顺应中国特色社会主义进入新时代、区域协调发展进入新阶段的新要求...
### [780个贫困县脱贫摘帽 中央对最后52个贫困县挂牌督战](https://finance.sina.com.cn/china/2020-05-17/doc-iircuyvi3593639.shtml)
> 概要: @人民日报5月17日消息，5月16日，云南省人民政府批准鲁甸县等31个县市区退出贫困县行列，至此，我国2019年申请脱贫摘帽的344个贫困县实现全部脱贫摘帽。
### [中共中央 国务院：西部贫困地区企业IPO适用绿色通道政策](https://finance.sina.com.cn/roll/2020-05-17/doc-iirczymk2116359.shtml)
> 概要: 中共中央、国务院《关于新时代推进西部大开发形成新格局的指导意见》指出，深化要素市场化配置改革。提高西部地区直接融资比例，支持符合条件的企业在境内外发行上市融资...
### [西部大开发重磅文件：贫困企业适用IPO绿色通道](https://finance.sina.com.cn/china/2020-05-17/doc-iircuyvi3596228.shtml)
> 概要: 原标题：西部大开发重磅文件发布！提高直接融资比例、贫困企业适用IPO绿色通道…… 新华社今日受权发布中共中央、国务院关于新时代推进西部大开发形成新格局的指导意见。
# 科技
### [NVlabs的StyleGAN的一个Web移植版](https://www.ctolib.com/k-l-lambda-stylegan-web.html)
> 概要: NVlabs的StyleGAN的一个Web移植版
### [Coward 是一个Deno模块，可轻松与Discord API进行交互](https://www.ctolib.com/fox-cat-coward.html)
> 概要: Coward 是一个Deno模块，可轻松与Discord API进行交互
### [ZipFly是基于zipfile.py的zip存档生成器](https://www.ctolib.com/BuzonIO-zipfly.html)
> 概要: ZipFly是基于zipfile.py的zip存档生成器
### [Markov(阿里妈妈功能测试智能化平台)](https://www.ctolib.com/alibaba-intelligent-test-platform.html)
> 概要: Markov(阿里妈妈功能测试智能化平台)
### [盒马侯毅：上海成在线新经济高地，将占领未来经济制高点](https://www.tuicool.com/articles/aIJBfui)
> 概要: 【猎云网（微信：）北京】5月17日报道盒马总裁侯毅在5月17日举行的上海信息消费节开幕式上发表了题为《立足上海“在线新经济”高地，让产业数字化继续领跑全球》的主题演讲，开场就提出了针对每个市民生活的“......
### [Flink原理｜Flink 1.10 细粒度资源管理解析](https://www.tuicool.com/articles/ruuIFje)
> 概要: 相信不少读者在开发 Flink 应用时或多或少会遇到在内存调优方面的问题，比如在我们生产环境中遇到最多的 TaskManager 在容器化环境下占用超出容器限制的内存而被 YARN/Mesos kil......
### [谷歌提出任务无关的轻量级预训练模型 MobileBERT：比 BERT 小 4 倍、速度快 5 倍](https://www.tuicool.com/articles/M3mMVjA)
> 概要: 预训练模型 BERT 在自然语言处理的各项任务中都展现了不错的效果。但在移动手机普及的今天，如何在移动端或者资源受限的设备上使用 BERT 模型，一直是个挑战。最近，Google Brain 团队提出......
### [Dubbo 授渔：微内核架构在 Dubbo 的应用](https://www.tuicool.com/articles/qUfAFv2)
> 概要: 授人以鱼，不如授之以渔，其实这句话并不只是说如何教人。从另一个角度看这句话，我们在学一样东西的时候，要找到这样东西的”渔“是什么。对于一项技术来说，它背后的设计思想，就是学习它的”渔“，对于 Dubb......
### [土味情话专用，汉王忆卡新品上市：98 元，无需充电，仅支持安卓 NFC 手机](https://www.ithome.com/0/487/628.htm)
> 概要: IT之家5月17日消息 汉王电纸书近期推出了汉王忆卡新产品，采用了电子墨水屏，重量12.5克，存储容量4GB，无需充电，售价 98元，主打辅助学习、电子书签、电子备忘录、提醒标签、支持自定义内容等。汉......
### [技嘉 B550 “超级雕”主板曝光，搭载3个M.2 SSD插槽](https://www.ithome.com/0/487/613.htm)
> 概要: IT之家5月17日消息 根据外媒TechPowerUp的消息，技嘉B550 AORUS Master的外观现已曝光，搭载了AMD B550芯片组，采用了3个M.2 SSD插槽。据介绍，主板上所有三个M......
### [纯可可：巧乐思麦丽素520g桶装38元（立减30元）](https://lapin.ithome.com/html/digi/487602.htm)
> 概要: 纯可可：巧乐思麦丽素520g桶装报价68元，限时限量30元券，实付38元包邮，领券并购买。巧乐思纯可可脂麦丽素520g，精选可可脂原料，巧克力外衣入口即化，麦芽夹心，香甜酥脆，可可香气弥漫齿间，从此爱......
### [手慢无：空调挡风板2.5元（立减5元）](https://lapin.ithome.com/html/digi/487632.htm)
> 概要: 手慢无：空调挡风板报价7.5元，限时限量5元券，实付2.5元包邮，领券并购买。买个玩玩试试，还是很划算的。7.5元款（券后2.5元）颜色随机，不接受指定颜色。• 点此享受手慢无：空调挡风板2.5元：领......
# 小说
### [快乐丧尸](http://book.zongheng.com/book/75079.html)
> 作者：烟灰在飞

> 标签：科幻游戏

> 简介：怪物多了，人类就成了怪物。换个思维方式，尝试从丧尸的角度看世界，末世只是新时代的开始，或许，您也会很期待的让可爱的，新时代的主宰者，丧尸，轻轻的、温柔的咬上一小口。末世不是终点而是起点；死亡不是结束而是开始快乐不是树上的苹果而是天上的星辰女人不是生活必需品而是生命一部分

> 章节末：16、 流氓本色

> 状态：完本
### [奇门相士](http://book.zongheng.com/book/598553.html)
> 作者：夏天的爱晴

> 标签：都市娱乐

> 简介：正一道传人王浩，自小跟随三爷爷学习奇门之术，占卜、看相、堪舆。一张铁嘴断前程，一卦千金问生死。摸、听、套、吓四真诀，混迹上海富豪界。临、兵、斗、者、皆、阵、列、前、行九子真言，驱鬼伏魔荡江湖。

> 章节末：035 隐世生活（大结局）

> 状态：完本
# 游戏
### [任天堂秘史 90年代描写任天堂与世嘉主机战争纪实剧](https://www.3dmgame.com/news/202005/3788720.html)
> 概要: 任天堂除了游戏做的好，在知识产权界的威严也是人尽皆知，“任天堂法务部”的威名一向令众多想打擦边球的玩家和团体胆战心惊，近日日本玩家社区又扒出了一段任天堂秘史， 90年代描写任天堂与世嘉主机战争纪实剧《......
### [李彦宏直播首秀 百度市值猛涨120亿](https://www.3dmgame.com/news/202005/3788706.html)
> 概要: 近日，百度CEO李彦宏也首次试水直播，与樊登创始人樊登进行了一场跨界对谈，吸引了近千万网友围观。这场直播持续近70分钟，与市面上流行的“种草”直播不同，李彦宏的直播首秀却不为“带货”。李彦宏与樊登以“......
### [飞机模拟《Balsa Model Flight Simulator》开放注册](https://www.3dmgame.com/news/202005/3788734.html)
> 概要: 模型飞机模拟游戏《Balsa Model Flight Simulator》今日开放了注册页面，注册的玩家有机会获得今年晚些时候举行的BETA封闭测试，玩家越早申请，获得资格的可能性就越高。本作自带简......
### [岸本齐史自曝《武士8》灵感来自《阿基拉》 无奈完结](https://www.3dmgame.com/news/202005/3788704.html)
> 概要: 自从火影完结后，岸本齐史老师就陷入了长期的“修养”中，他曾亲口表示“比火影还有趣”的酝酿多时的原作新漫《武士8：八丸传》于2019年5月开启连载，一直反响平平，今年3月23日已经草草完结，近日岸本自述......
### [王思聪出售保时捷918：里程仅1千公里 要价超千万](https://www.3dmgame.com/bagua/3110.html)
> 概要: 近日有网友在微博上曝出，王思聪卖出了个人手中两台超跑，分别是保时捷918 Spyder和兰博基尼Aventador LP 700-4。目前这两台车子已经流向了二手市场。根据二手车商提供的消息显示，王思......
# 论文
### [EmbedMask: Embedding Coupling for One-stage Instance Segmentation](https://paperswithcode.com/paper/embedmask-embedding-coupling-for-one-stage)
> 日期：4 Dec 2019

> 标签：INSTANCE SEGMENTATION

> 代码：https://github.com/yinghdb/EmbedMask

> 描述：Current instance segmentation methods can be categorized into segmentation-based methods that segment first then do clustering, and proposal-based methods that detect first then predict masks for each instance proposal using repooling. In this work, we propose a one-stage method, named EmbedMask, that unifies both methods by taking advantages of them.
### [PaStaNet: Toward Human Activity Knowledge Engine](https://paperswithcode.com/paper/pastanet-toward-human-activity-knowledge)
> 日期：2 Apr 2020

> 标签：HUMAN-OBJECT INTERACTION DETECTION

> 代码：https://github.com/DirtyHarryLYL/HAKE

> 描述：Existing image-based activity understanding methods mainly adopt direct mapping, i.e. from image to activity concepts, which may encounter performance bottleneck since the huge gap. In light of this, we propose a new path: infer human part states first and then reason out the activities based on part-level semantics.
