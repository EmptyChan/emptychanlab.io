---
title: 2021-08-13-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Southpaw_ZH-CN0080320297_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-08-13 18:02:05
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Southpaw_ZH-CN0080320297_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [OpenYurt 联手 eKuiper，解决 IoT 场景下边缘流数据处理难题](https://www.oschina.net/news/155247)
> 概要: 云计算的出现促使物联网实现爆炸式增长。在设备规模和业务复杂度不断攀升的趋势之下，边缘计算因其能够将计算能力更靠近网络边缘和设备，从而带来云性能成本的降低，也在这波浪潮之下得到快速发展。诚然，物联网边缘......
### [Google 开源 Allstar ，为 GitHub 项目持续实施最佳安全实践](https://www.oschina.net/news/155179/google-open-source-allstar)
> 概要: 如今，安全漏洞日益困扰着大型开源项目。根据 RiskSense 的统计数据，与 2018 年相比，2019 年开源软件漏洞的数量增加了一倍多。考虑到近 91% 的商业应用程序包含过时或废弃的开源组件，......
### [Linux Kernel 为支持 AV1 解码做准备](https://www.oschina.net/news/155177/linux-media-rfc-av1-uapi)
> 概要: 目前市场上提供加速 AV1 编码的硬件平台数量仍然非常有限，但随着更多支持这种免版税视频编解码器编码/解码的硬件上市，Linux 内核的媒体子系统也正在准备就绪。本周二，来自 Collabora 公司......
### [Shutter 0.98 发布，Linux 屏幕截图工具](https://www.oschina.net/news/155166/shutter-0-98-released)
> 概要: Shutter 是一个功能丰富的屏幕截图程序，适用于基于 Linux 的操作系统。用户可以对一个特定的区域、窗口、整个屏幕，甚至是一个网站进行截图。并且可以在截图后对图片进行适当编辑，然后上传到图片托......
### [《网球王子》公开新PV！收录龙马和樱乃的歌舞场景](https://news.dmzj.com/article/71870.html)
> 概要: 根据许斐刚原作制作的剧场版动画《龙马！The Prince of Tennis 新生剧场版网球王子》公开了新PV。在这次的PV中，收录了龙马和樱乃的歌舞场景等内容。
### [Geometric Deep Learning Course: Grids, Groups, Graphs, Geodesics, and Gauges](https://geometricdeeplearning.com/lectures/)
> 概要: Geometric Deep Learning Course: Grids, Groups, Graphs, Geodesics, and Gauges
### [The deceptive PR behind Apple’s “expanded protections for children”](https://piotr.is/2021/08/12/apple-csam/)
> 概要: The deceptive PR behind Apple’s “expanded protections for children”
### [足球漫画《BLUE LOCK》动画化！2022年播出](https://news.dmzj.com/article/71872.html)
> 概要: 由金城宗幸、野村优介创作的漫画《BLUE LOCK》宣布了动画化决定的消息，本作预计将于2022年开始播出。在公开的PV中，可以看到主人公们盘带与射门的场景。
### [C++ Exceptions: Under the Hood (2013)](https://monkeywritescode.blogspot.com/p/c-exceptions-under-hood.html)
> 概要: C++ Exceptions: Under the Hood (2013)
### [动画《看得见的女孩》推出声音朗读怪谈企划](https://news.dmzj.com/article/71873.html)
> 概要: 即将于10月开始播出的TV动画《看得见的女孩》公开了新的宣传图。与前两次相同，这次同样分为“看得见”的版本和“看不见”的版本。
### [P站美图推荐——服务员特辑](https://news.dmzj.com/article/71875.html)
> 概要: 全心全意为客人服务的服务员，轻飘飘的裙摆和杀死童贞的围裙贼拉好
### [主播的本质就是新品牌收割机](https://www.huxiu.com/article/448075.html)
> 概要: 来源：远川商业评论（ID：ycsypl）作者：张榕潇题图：IC photo不安分的王思聪最近又开始频繁上头条。先是看似舔狗，实则骚扰的一系列“追不到就毁掉她”的操作让人大跌眼镜，后又花了100万在家里......
### [组图：RedVelvet新专辑预告公开 Irene耍大牌风波后仍站C位](http://slide.ent.sina.com.cn/y/k/slide_4_704_360322.html)
> 概要: 组图：RedVelvet新专辑预告公开 Irene耍大牌风波后仍站C位
### [《彩虹小马》动画新预告：当独角兽闯入马儿的生活](https://acg.gamersky.com/news/202108/1415067.shtml)
> 概要: 动画电影《小马宝莉：新世代》今日公布了全新预告，本片将于今年9月24日上线网飞平台。
### [韩影票房：《摩加迪沙》蝉联冠军战胜《X特遣队》](https://ent.sina.com.cn/m/f/2021-08-13/doc-ikqcfncc2567968.shtml)
> 概要: 名次 片名 上映时间 占有率 周末人次 累计人次　　11 2021/07/28 53.8% 480，626 1，712，396　　2新 2021/08/04 20.9% 177，469 281，384......
### [漫画「五星物语」第16卷封面公开](http://acg.178.com/202108/422818290297.html)
> 概要: 漫画「五星物语」公开了第16卷封面，该卷将于10月8日发售。「五星物语」是日本漫画家永野护的科幻漫画，自1986年起在角川书店的月刊杂志「Newtype」上连载，至今仍在连载中......
### [揭秘七夕鲜花乱象：你收到的花，多半是C级货](https://www.huxiu.com/article/448089.html)
> 概要: 本文来自微信公众号：新浪科技（ID：techsina），作者：刘亚丹，头图来自：视觉中国七夕节悄然来到。这种S级节日，无疑将给鲜花市场带来日常20倍以上的销量。盒马、叮咚买菜、抖音纷纷盯上了鲜花市场，......
### [视频：声梦学员毕业演唱会 TVB高层曾志伟到场打气](https://video.sina.com.cn/p/ent/2021-08-13/detail-ikqcfncc2580170.d.html)
> 概要: 视频：声梦学员毕业演唱会 TVB高层曾志伟到场打气
### [Show HN: Lisp implementation in modern Java, Go, C#, TypeScript, Python](https://github.com/eatonphil/lisp-rosetta-stone)
> 概要: Show HN: Lisp implementation in modern Java, Go, C#, TypeScript, Python
### [漫画「纯情罗曼史」最新杂志封面公开](http://acg.178.com/202108/422820176858.html)
> 概要: 漫画「纯情罗曼史」公开了最新的杂志封面图，该杂志将于8月30日发售。「纯情罗曼史」是由中村春菊创作的BL漫画作品，讲述的是非常有精神的大学生高桥美咲与年轻英俊却兼写BL小说名作家小兔老师（宇佐见秋彦）......
### [被EA收购之后 Glu Mobile的CEO和COO双双辞职](https://www.3dmgame.com/news/202108/3821148.html)
> 概要: 今年2月，EA宣布以21亿美元收购手机游戏开发商Glu mobile。该开发商带来了超过15款直播游戏的产品，共产生了13.2亿美元的净预订量。最终的收购是在4月份完成的，最终价格为24亿美元。当一家......
### [年度游戏《黑帝斯》今日正式登陆Xbox/PS平台](https://www.3dmgame.com/news/202108/3821150.html)
> 概要: Supergiant Games和Private Division共同宣布获得超过50个年度游戏大奖的游戏《黑帝斯》现已登陆PlayStation 5、PlayStation 4，以及Xbox X|S......
### [组图：王彦霖清凉休闲装扮现身 与老婆十指紧扣帮背包超贴心](http://slide.ent.sina.com.cn/star/slide_4_86512_360333.html)
> 概要: 组图：王彦霖清凉休闲装扮现身 与老婆十指紧扣帮背包超贴心
### [腾讯天美或将用虚幻引擎打造《战地》PC/手机游戏](https://www.3dmgame.com/news/202108/3821149.html)
> 概要: 近日腾讯天美工作室发布招聘信息，或将采用虚幻引擎打造一款《战地》新作，登陆PC和手机平台。昨日(8月12日)腾讯互娱招聘微信公众号发招聘图，称天美J3工作室知名IP开放大战场FPS诚邀各位加入，游戏美......
### [日本上映15部国外动画电影 《罗小黑战记》等在列](https://ent.sina.com.cn/s/j/2021-08-13/doc-ikqcfncc2605995.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 东京都写真美术馆将于8月31日至9月17日举行名为“全球杰作动画2021秋季篇”的特辑上映，学龄前儿童免费观看，最多有两位陪同家长也可免费入场。　　该特辑选择了近年来上映......
### [诚心发问：辅助玩家如何在双c拉跨的情况下赢下游戏？](https://bbs.hupu.com/44746409.html)
> 概要: 第一把，QQ区1700巅峰赛，双c一个选后羿一个选墨子后羿太乙其实挺好的，关键这人带的净化？？？而且明显不会玩，大招全场基本没射中几次，倒是中了三次鱼，平白无故开净化，给人突脸没闪现拉都拉不开，抬起来
### [10年代码经验程序员UP主复刻“阴间”超级马里奥，获赞27万，马里奥：我头呢？](https://www.tuicool.com/articles/meUjeuv)
> 概要: 大数据文摘出品作者：王烨《超级马里奥兄弟》是很多人童年的回忆，对B站up主“M木糖M”来说也不例外。为了纪念自己的童年，我们这位代码经验丰富的up主决定自己动手复刻一个最经典的超级马里奥兄弟第一关！来......
### [阿里在东南亚没做成的，在土耳其成功了](https://www.tuicool.com/articles/rYZrA3Q)
> 概要: 本文来自微信公众号“墨腾创投”（ID:MomentumWorks），作者：Yusuf，36氪经授权发布。阿里巴巴在7月底发布了2021年的财报，萎靡不振的股价的背后则是一系列数字指标增长。在财报中也阿......
### [如何获得更高质量的反馈？这其实并不难](https://www.tuicool.com/articles/bqYvm2z)
> 概要: 神译局是36氪旗下编译团队，关注科技、商业、职场、生活等领域，重点介绍国外的新技术、新观点、新风向。编者按：工作中的沟通是不可避免的，在同事的协作之中，如何获得更质量的反馈，减少无效沟通是一个很重要的......
### [轻小说「处刑少女的生存之道」第6卷封面＆插图公开](http://acg.178.com/202108/422834225345.html)
> 概要: 轻小说「处刑少女的生存之道」公开了第6卷的封面图和彩页插图，发售日期尚未公布。「处刑少女的生存之道」是由佐藤真登著作，ニリツ负责插画的轻小说。本作TV动画将于2022年开始播出......
### [剧场版动画「Free!–the Final Stroke–」前篇新预告将于8月19日解禁](http://acg.178.com/202108/422834805279.html)
> 概要: 近日，「Free!」官方确认将在8月19日晚正式公开剧场版动画「Free!–the Final Stroke–」前篇的最新预告PV。「Free!–the Final Stroke–」分为前后两篇，前篇......
### [张哲瀚不得不道歉？代言多达36个，已有8个品牌开始有动作](https://new.qq.com/omn/20210813/20210813A07HT500.html)
> 概要: 8月13日下午，张哲瀚通过社交平台发表致歉信，为他此前参加朋友婚礼引发的争议道歉。            道歉信的第一段是“今天我要为曾经无知的自己而羞愧，更要对之前不当行为深刻地道歉。”接着他针对争......
### [赚不到钱的流量扶持，服装厂老板逃离淘抖快](https://www.tuicool.com/articles/n6j6JrM)
> 概要: 新老两代人出现分野，但依旧是同样的诉求：无论是线下渠道、线上电商、直播电商，商家们需要的是自己能够盈利，而不是空有数字实际却因高退货率而亏钱的「成交额」和看似用户很多却无法盈利的「流量扶持」。正如新老......
### [小米开启全新 MIUI 小部件内测招募：新网格体系、小部件商店、系统应用小部件，支持自由拖拽](https://www.ithome.com/0/569/144.htm)
> 概要: IT之家8 月 13 日消息 据IT之家网友反馈，小米现已开启全新 MIUI 小部件内测招募，新增系统应用小部件，支持自由拖拽、自选样式等。此次 MIUI 小部件内测带来了全新网格体系、小部件商店、系......
### [大差价：雀巢 NIDO 荷兰奶粉 24.9 元/罐（商超 59 元）](https://lapin.ithome.com/html/digi/569146.htm)
> 概要: 【雀巢官方海外旗舰店】雀巢 NIDO 高钙高蛋白奶粉 400g*2 罐清仓大促，直降至 106 元。今日力度加码：下单立打 8 折 + 可领 35 元大额券，实付 49.8 元含税包邮：天猫雀巢 NI......
### [小米常程：“铁蛋”四足机器人处于工程探索阶段，内置小爱同学](https://www.ithome.com/0/569/147.htm)
> 概要: IT之家8 月 13 日消息 在小米 8 月 10 晚举办的发布会上，雷军带来了小米自研的CyberDog 四足机器人，外号“铁蛋”。这款机器人搭载高精度环境感知系统，全身 11 个高精度传感器向 A......
### [美国新法案将针对苹果和谷歌商店的微交易系统](https://www.3dmgame.com/news/202108/3821176.html)
> 概要: 美国参议院星期三提出一项新法案，该法案将寻求保护应用程序开发者的权利，以挑战苹果和谷歌应用商店的政策。该法案被称为“Open App Markets Act”（开放应用市场法案），由田纳西州共和党参议......
### [CELEB首席营销官Fabio：将推出区块链广告平台CELEBPLUS](https://www.btc126.com//view/181652.html)
> 概要: 据官方消息，2021年8月13日CELEB首席营销官Fabio做客XT直播间，在谈及发展计划时Fabio表示：“我们正打算在8月左右推出我们的区块链广告平台CELEBPLUS。首先，我们计划在其平台上......
### [realme Book 笔记本预热：搭载第 11 代英特尔酷睿 i5 处理器](https://www.ithome.com/0/569/172.htm)
> 概要: IT之家8 月 13 日消息 realme 旗下首款笔记本电脑 realme Book 将于 8 月 18 日 15 点发布，近期官方不断对新品进行预热，公布新品的一些配置。今天官方再次对新品进行预热......
### [BitMEX推出可以保护用户隐私的交易所资产与负债证明工具](https://www.btc126.com//view/181658.html)
> 概要: 加密衍生品交易所BitMEX推出可以保护用户隐私的交易所资产与负债证明工具，该工具实现了可以验证BitMEX可支配代币余额，以及让用户验证其账户余额是否被包含在交易所可支配余额中，或交易所能否偿付其负......
### [目前供应首都生产生活的车辆进京绿色通道已经打开](https://finance.sina.com.cn/china/gncj/2021-08-13/doc-ikqcfncc2668593.shtml)
> 概要: 原标题：目前供应首都生产生活的车辆进京绿色通道已经打开 来源：央视新闻客户端 8月13日，在北京市新型冠状病毒肺炎疫情防控工作第239场新闻发布会上...
### [商业大佬冯仑的暮年：涉嫌挪用资金罪被三亚警方立案](https://finance.sina.com.cn/chanjing/gsnews/2021-08-13/doc-ikqcfncc2675487.shtml)
> 概要: 撰文|巫英蛟 刘虎 一宗“12.08挪用资金案”，正由海南三亚警方立案侦查，矛头直指商业大佬冯仑。 2020年12月8日起，多位自然人及公司向三亚市公安局报案称：三亚万通健康开发...
### [名侦探柯南：某UP主爆肝1071集，只为算出毛利小五郎被扎了多少针](https://new.qq.com/omn/20210813/20210813A079WD00.html)
> 概要: 看《名侦探柯南》的人都知道，一旦有案子毛利小五郎基本上就要被柯南扎一次针，但是具体扎了多少针没有一个具体的数字。最近就有一个UP主：“星曜Hoxi”，为了弄清楚这个疑惑直接看了1071集柯南，对里面毛......
### [小说作者笔名有多搞笑，拉棉花的兔子，广播剧报幕怎么忍住不笑场](https://new.qq.com/omn/20210813/20210813A0ALJR00.html)
> 概要: 对于资深书粉来说，也是博览群书了，见过各式各样的书名，当然认识的作者也非常的多，不过比起书名的奇奇怪怪，作者笔名也是一大亮点。有些作者非常的有意境，当然也有很多作者的笔名带着迷惑性，甚至还带着搞笑的元......
### [3年能领1.8万元！攀枝花生育补贴远超山西](https://finance.sina.com.cn/china/gncj/2021-08-13/doc-ikqciyzm1271575.shtml)
> 概要: 《中国经济周刊》记者 杨琳丨四川报道 因为在全国首推生育补贴，四川小城攀枝花瞬间“惊艳”了一票人。 近期，攀枝花市宣布对按政策生育第二个及以上孩子的攀枝花户籍家庭...
### [张哲瀚被多家官媒点名批评，人民日报最犀利：就得付出沉重代价](https://new.qq.com/omn/20210813/20210813A0AOHQ00.html)
> 概要: 8月13日，张哲瀚风波进一步发酵，尽管他本人已经发文道歉，但是网友的怒气依然无法平息。随后，多家官媒转发或评论，其中《人民日报》更是点名批评。                        人民日报指......
### [lolita私影～粘人的小猫猫](https://new.qq.com/omn/20210813/20210813A0AWU800.html)
> 概要: 出镜：芸菱L（微博同名）摄影：AJ阿姐后勤：曦妈妈......
### [香港特区政府上调经济增长预测 预计全年增长5.5%至6.5%](https://finance.sina.com.cn/china/gncj/2021-08-13/doc-ikqcfncc2671380.shtml)
> 概要: 原标题：香港特区政府上调经济增长预测 预计全年增长5.5%至6.5% 8月13日，香港特区政府表示，除非全球经济状况因疫情发展而急剧恶化...
### [陈露以省略号回应霍尊，网友纷纷为其鸣不平，霍尊渣男实锤](https://new.qq.com/omn/20210813/20210813A0AWIQ00.html)
> 概要: 霍尊回应个寂寞，父亲朋友齐上阵站队，渣男本性已暴露霍尊和陈露之间的瓜葛想必已经成为了大家饭后的闲谈。之前陈露发文回应霍尊的事情，一下子让霍尊陷入了风波里面，但是霍尊一直没有回应，反而他的爸爸和好友还出......
### [下周一起 北京这些地铁站早高峰适时采取限流措施](https://finance.sina.com.cn/china/2021-08-13/doc-ikqcfncc2671781.shtml)
> 概要: 原标题：下周一起，北京这些地铁站早高峰适时采取限流措施 北京地铁官微发布消息：为进一步做好疫情防控工作，控制列车满载率，地铁公司将于2021年8月16日（周一）起对部分...
### [HIVE Blockchain从比特大陆订购1800台加密矿机](https://www.btc126.com//view/181665.html)
> 概要: HIVE Blockchain Technologies宣布，已从Bitmain Technologies订购了1,800台Antminer S19j Pro矿机，以提高其加密挖矿能力。新订购的加密矿......
### [国产纪录片《棒！少年》｜2020年度“国产片之王”](https://new.qq.com/omn/20210813/20210813A09HTL00.html)
> 概要: 一直听说，。就拿体育项目来说，在一些人眼里，像跆拳道、游泳、足球那些，似乎只配待在最低端来娱乐大众。而眼睛盯着国外精英教育的中产们，更钟情于棒球、橄榄球、冰球、马术这些小众项目。在他们眼中，这些项目代......
### [漫画家宫尾岳吐槽现在的阿宅眼光太高，明明以前都没有这么高要求](https://new.qq.com/omn/20210813/20210813A0B2US00.html)
> 概要: 近年来，动画业界反怼阿宅的案例实在有点多，继上次声优绪方恵美发推请求阿宅们不要再嫌弃动画作画崩坏之后，最近漫画家宫尾岳又在推特上拿阿宅眼光太高来说话。                        根......
### [彭博社报道：亚马逊游戏将废除部分霸王条款](https://www.3dmgame.com/news/202108/3821179.html)
> 概要: 近日，彭博社记者Jason Schreier在于亚马逊员工通过电子邮件交流时得知亚马逊将废除部分不合理的规定，其中包括在雇佣合同中严格要求员工在闲暇时间也要开发游戏的条例。除此之外，在废除清单中还有以......
### [土地供应端调控逐渐深入 上海将第二批集中供地推迟到9月](https://finance.sina.com.cn/china/gncj/2021-08-13/doc-ikqcfncc2673091.shtml)
> 概要: 原标题：土地供应端调控逐渐深入，这座一线城市也将第二批集中供地推迟到9月 唐韶葵 土拍规则酿变。 21世纪经济报道记者 唐韶葵 上海报道 8月13日...
### [以太坊基金会资助筹建zkEVM团队](https://www.btc126.com//view/181666.html)
> 概要: 以太坊基金会在定期公布其支持的团队研究与开发进度的文章中表示，过去几个月的时间已经组建了一个名为zkEVM的团队，希望能将EVM （以太坊虚拟机）的所有操作码直接通过ZK （零知识证明）电路实现，以实......
### [流言板CBA官方更新自由球员名单：张涵钧、郭一飞、张兆旭在列](https://bbs.hupu.com/44751481.html)
> 概要: 虎扑08月13日讯 CBA官网更新自由球员名单，新增郭一飞、张涵钧、张兆旭3名球员。郭一飞上赛季代表同曦出场8次，场均出场20.3分钟，能得到6分3.4篮板。张涵钧上赛季代表山西出场44次，场均出场2
### [WE vs OMG精彩回顾：开局四人包下，WE打出0换3](https://bbs.hupu.com/44751542.html)
> 概要: 来源：  虎扑
### [流言板由于全美直播场次少，森林狼球衣赞助金额预计不超过七位数](https://bbs.hupu.com/44751624.html)
> 概要: 虎扑08月13日讯 根据CNBC记者Jabari Young的报道，森林狼与Fitbit的球衣广告赞助合同于2020年6月到期。森林狼目前在物色新的球衣广告赞助商，考虑到森林狼参加全美直播的场次不多，
# 小说
### [带着仓库到大宋](http://book.zongheng.com/book/837957.html)
> 作者：路人家

> 标签：历史军事

> 简介：现代青年孙途因故带仓库系统穿越到北宋末年，从而开始了一段精彩曲折，前途难料的生存与救亡之旅。在这里，不但有昏君权奸，还有忠臣名将，而更出乎意料的是，这还是一个有着水浒群雄的世界。孙途将与他们如何相处？一切答案，尽在带着仓库到大宋！

> 章节末：新书发啦！！！

> 状态：完本
# 论文
### [A Novel Approach to Discover Switch Behaviours in Process Mining | Papers With Code](https://paperswithcode.com/paper/a-novel-approach-to-discover-switch)
> 日期：24 Jun 2021

> 标签：None

> 代码：https://github.com/bearlu1996/switch

> 描述：Process mining is a relatively new subject which builds a bridge between process modelling and data mining. An exclusive choice in a process model usually splits the process into different branches.
### [TempoRL: Learning When to Act](https://paperswithcode.com/paper/temporl-learning-when-to-act)
> 日期：9 Jun 2021

> 标签：None

> 代码：https://github.com/automl/TempoRL

> 描述：Reinforcement learning is a powerful approach to learn behaviour through interactions with an environment. However, behaviours are usually learned in a purely reactive fashion, where an appropriate action is selected based on an observation.
