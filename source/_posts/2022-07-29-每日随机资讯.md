---
title: 2022-07-29-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FourTigresses_ZH-CN4095017352_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-29 22:16:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FourTigresses_ZH-CN4095017352_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [说不说黑话都要好好说话！](https://www.woshipm.com/zhichang/5545266.html)
> 概要: 编辑导语：互联网黑话发展已久，但还是有很多人对黑话的了解甚微。本篇文章中作者围绕“互联网黑话”展开了一系列详细的讲述，感兴趣的小伙伴们快来看看吧，希望对你有所帮助。最近看综艺节目，看到了模拟互联网职场......
### [小成本营销，和它背后的增长陷阱](https://www.woshipm.com/marketing/5543865.html)
> 概要: 编辑导语：为了抢占用户心智，品牌们开始寻觅各式各样的打法，试图找到最合适的自身的营销套路。而像之前的蜜雪冰城“黑化”等小成本营销方式，虽然引起了热烈反响，但其背后仍然存在着一定的增长陷阱。具体存在着哪......
### [我在阿里干外包的177天（上）](https://www.woshipm.com/it/5546011.html)
> 概要: 编辑导语：在大厂工作是很多互联网人的梦想，而大厂外包成为了他们追逐梦想的一大备选，但成为外包，大厂梦就能实现吗？本篇文章中，作者讲述了在大厂干外包的177天的所见所闻，感兴趣的话，一起来看看吧。一、写......
### [可线性渐变的环形进度条的实现探究](https://segmentfault.com/a/1190000042238962)
> 概要: 本文主要介绍基于 NutUI Vue3 的circleProgress组件的设计与实现原理，是一个圆环形的进度条组件，用来展示操作的当前进度，支持修改进度以及渐变色。环形进度条是很常用的一个组件，特别......
### [英特尔第二季度同比转盈为亏](https://finance.sina.com.cn/tech/it/2022-07-29/doc-imizirav5848391.shtml)
> 概要: 新浪科技讯 北京时间7月29日凌晨消息，英特尔今天公布了该公司的2022财年第二季度财报。报告显示，英特尔第二季度营收为153.21亿美元，与去年同期的196.31亿美元相比下降22%；净亏损为4.5......
### [每日优鲜“死”于短视](https://www.huxiu.com/article/621101.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国“非常失望，期权没有变现、6月工资没有发、7月工资也没有发、还突然被裁退了。”7月28日晚，每日优鲜员工吴萍（化名）告诉虎嗅。让吴萍们最为气愤的是，7月2......
### [盘后大涨！但连续第二季度亏损](https://finance.sina.com.cn/tech/internet/2022-07-29/doc-imizirav5875491.shtml)
> 概要: 见习记者/杨阳......
### [上市两个月资金链就已开始紧张](https://finance.sina.com.cn/tech/internet/2022-07-29/doc-imizmscv3967311.shtml)
> 概要: 文 | 新浪科技 杨雪梅......
### [马斯克：美国通胀可能下降 更多特斯拉商品价格在回落](https://finance.sina.com.cn/tech/it/2022-07-29/doc-imizirav5866130.shtml)
> 概要: 新浪科技讯 北京时间7月29日早间消息，特斯拉CEO埃隆·马斯克今日在Twitter上表示，美国通货膨胀可能呈下降趋势，更多特斯拉商品价格在回落......
### [历史转折中的中美互联网](https://www.huxiu.com/article/621238.html)
> 概要: 本文来自微信公众号：卫夕指北（ID：weixizhibei），作者：卫夕，原文标题：《消失的增长与难觅的第二曲线——历史转折中的中美互联网》，题图来自：视觉中国一直高歌猛进的互联网似乎走到了一个十字路......
### [云快充完成C轮融资，投资方为ABB](https://www.tuicool.com/articles/umEV73Z)
> 概要: 云快充完成C轮融资，投资方为ABB
### [「作为恶役大小姐就该养魔王」第一弹PV和视觉图公布](http://acg.178.com/202207/453060236722.html)
> 概要: TV动画「作为恶役大小姐就该养魔王」的第一弹PV和视觉图现已公布，同时宣布追加声优：福山润、小野友树、花泽香菜、杉田智和，本作将于2022年10月播出。「作为恶役大小姐就该养魔王」第一弹PVSTAFF......
### [致智慧互通科技股份有限公司的道歉信](http://www.investorscn.com/2022/07/29/102099/)
> 概要: 致：智慧互通科技股份有限公司......
### [冒险ADV《沧海天记》开场影片公布 游戏年底发售](https://www.3dmgame.com/news/202207/3848100.html)
> 概要: Idea FactoryI旗下游戏品牌「ALTERGEAR」推出的主题为男孩间的友情的视觉小说游戏《沧海天记》开场影片现已公开，游戏将于12月8日在日本发售，登陆Switch平台。游戏售价为7150日......
### [「平凡职业造就世界最强」OVA动画公开第二弹PV](http://acg.178.com/202207/453062517795.html)
> 概要: 近日，「平凡职业造就世界最强」官方公开了OVA动画「幻之冒险与奇迹的邂逅」的第二弹宣传PV。该OVA收录于原作小说的第13卷「BD特装版」单行本商品中，商品将在2022年9月25日发售，售价7,535......
### [漫画《咒术回战》累计突破7000万部](https://news.dmzj.com/article/75089.html)
> 概要: 由芥见下下创作的漫画《咒术回战》宣布了算上将于8月4日发售的第20卷，系列累计出版突破7000万部的消息。
### [爱斯特获近2亿元B轮融资，申银万国领投](https://www.tuicool.com/articles/7jui6vI)
> 概要: 爱斯特获近2亿元B轮融资，申银万国领投
### [MIRROR演唱会荧幕断裂砸舞者头部 有成员受惊入院](https://ent.sina.com.cn/s/h/2022-07-29/doc-imizmscv3997508.shtml)
> 概要: 新浪娱乐讯 据台媒29日报道，2018年选秀节目《Good Night Show 全民造星》结束后挑出12名男成员组成“MIRROR”正式出道，过去创下香港团体出道后最快举行演唱会的纪录，并在出道第一......
### [潮玩盲盒 包包兄妹BREAD BAO&BAO面包工坊系列创意摆件](https://www.zcool.com.cn/work/ZNjExNTkyMjg=.html)
> 概要: 包包兄妹，英文名字是BREAD BAO&BAO。哥哥大包，蘑菇头是他的标志发型。他是一个外向开朗、对面包颇有研究、稳重又负责的小男孩。妹妹小包，是一个迷迷糊糊却又充满好奇心的小女孩。她现在还只是个烘焙......
### [轻小说「精灵幻想记」第22卷彩图公开](http://acg.178.com/202207/453071411350.html)
> 概要: HJ文库公开了第22卷「精灵幻想记22·纯白方程式」的彩页插图，该卷将于8月1日正式发售。「精灵幻想记」是北山结莉创作、Riv负责插画的轻小说作品，于2015年10月1日由Hobby JAPAN出版......
### [日服eShop 开启夏季优惠活动 12款游戏参与折扣](https://www.3dmgame.com/news/202207/3848113.html)
> 概要: 任天堂今日宣布日服eshop将于8月1日至8月16日期间，开启夏日夏季优惠活动。第一方与第三方游戏共12款进行打折。活动页面：点击此出具体游戏打折情况《马里奥派对:超级巨星》  -20%  折后520......
### [「弦音 风舞高中弓道部」最新剧场版鸣宫凑&竹早静弥角色PV公开](http://acg.178.com/202207/453073063527.html)
> 概要: 「弦音 风舞高中弓道部」最新剧场版「ツルネ －はじまりの一射－」发布了鸣宫凑&竹早静弥的角色PV，本作将于8月19日开始在日本上映。「弦音 风舞高中弓道部」最新剧场版鸣宫凑&竹早静弥角色PVMUSIC......
### [夏日躲猫猫-BUG BUNNY](https://www.zcool.com.cn/work/ZNjExNjExNjA=.html)
> 概要: ☀️高温的夏日企划了这次逃离炎热的行动，大暑天让本来可爱的邦尼情绪如同高温一般高涨不降，变成了凶凶的邦尼。☀️邦尼决定进行避暑行动。在非常适合度过炎热时光的电玩之境，暴躁的邦尼终于愿意娱乐一番，温度的......
### [【混剪】历史第一后场组合——水花兄弟（上）](https://bbs.hupu.com/54943304.html)
> 概要: 【混剪】历史第一后场组合——水花兄弟（上）
### [虚拟主播举办生前葬引发争议 你觉得这种做法合适吗 ？](https://news.dmzj.com/article/75095.html)
> 概要: 7月28日，彩虹社的虚拟主播黛灰宣布毕业，在毕业前一天的27日他为自己举办了一场生前葬的活动。
### [KADOKAWA《魔法少女伊莉雅》伊莉雅婚纱版手办](https://news.dmzj.com/article/75096.html)
> 概要: KADOKAWA根据《Fate/kaleid liner 魔法少女☆伊莉雅 Licht 无名少女》中的伊莉雅制作的1/7比例婚纱版手办目前已经开始预订了。本作再现了原作者广山弘绘制的插图中，伊莉雅身穿白色婚纱礼服时的样子。
### [海外多家权威媒体热议波场TRON：为互联网去中心化奠定基础](http://www.investorscn.com/2022/07/29/102108/)
> 概要: 2022年，波场TRON迎来了发展第5年。经过了5年的发展，它成长为全球三大公链之一，拥有超过1.05亿用户，和36亿笔交易次数，并在2021年12月成立世界上最大的去中心化自治组织之一——TRON ......
### [时隔3年 任天堂再次参展独立游戏展BitSummit](https://www.3dmgame.com/news/202207/3848122.html)
> 概要: 时隔三年，任天堂宣布再次参展日本最大的独立游戏展「BitSummit X-Roads」。本次展会将于8月6日- 8月7日举办，现场将会提供之前在Indie World中宣发的7款游戏可的试玩，试玩的玩......
### [三星 Galaxy Z Fold 4 / Flip 4 所有颜色版本渲染图流出：前者铰链相比上代更小](https://www.ithome.com/0/632/270.htm)
> 概要: IT之家7 月 29 日消息，三星此前宣布 Samsung Galaxy Unpacked 2022 将于北京时间 8 月 10 日晚 21:00 举行，距离发布会还有不到两周的时间，91mobile......
### [第五弹_爆蛋也要打工呀](https://www.zcool.com.cn/work/ZNjExNjQ1MTI=.html)
> 概要: 正在参与：别管我，表情包是我唯一的交流方式......
### [艺画开天将为武汉影视文化产业发展掀开新篇章](https://acg.gamersky.com/news/202207/1504272.shtml)
> 概要: 7月28日下午，第36届大众电影百花奖产业论坛在武汉举办。电影业界代表、知名电影企业家、产业专家出席活动。
### [3家华尔街投行给予嘉楠科技买入评级](http://www.investorscn.com/2022/07/29/102109/)
> 概要: 近期，包括H.C. Wainwright和Benchmark等在内3家华尔街投行和一家中国金融机构华兴资本给予嘉楠科技买入评级。据外媒Forkast报道，嘉楠科技表示，公司将很快在美国开展自营或联合挖......
### [2022年公募基金保有量规模百强发布中植基金再度上榜](http://www.investorscn.com/2022/07/29/102110/)
> 概要: 7月27日，中国证券投资基金业协会公布最新公募基金保有量规模100强榜单。2022年二季度股市巨震中，北京中植基金销售有限公司（简称：中植基金）凭借权益类基金（股票+混合）保有规模135亿元、非货币市......
### [夏日限定放映厅开张啦！大剧专场带你重温《开端》等高分好剧](https://new.qq.com/rain/a/ENT2022072000068400)
> 概要: 夏日限定放映厅开张啦！大剧专场带你重温《开端》等高分好剧
### [做产业孵化背后的“使能者”——专访盛景网联董事长彭志强](https://www.tuicool.com/articles/BNVFnu7)
> 概要: 做产业孵化背后的“使能者”——专访盛景网联董事长彭志强
### [《电锯人》TV动画新视觉图公开 8月5日公开重要情报](https://acg.gamersky.com/news/202207/1504298.shtml)
> 概要: 官方在今天（7月29日），公开了TV动画《电锯人》的全新视觉图，官方还宣布，8月5日将会有网络现场直播节目，届时将有《链锯人》电视动画的重要情报公布。
### [视频：尔冬升《海的尽头是草原》发剧照 丁程鑫新电影角色造型曝光](https://video.sina.com.cn/p/ent/2022-07-29/detail-imizmscv4045003.d.html)
> 概要: 视频：尔冬升《海的尽头是草原》发剧照 丁程鑫新电影角色造型曝光
### [蔚来充电桩破万根，官方称超 80％电量服务其他品牌车主](https://www.ithome.com/0/632/296.htm)
> 概要: IT之家7 月 29 日消息，今日，蔚来官方宣布蔚来充电桩已经突破 1 万根大关，达到了 10071 根，其中仅今年一年就增加了 6689 根。至此，蔚来充电桩已经覆盖了全国 269 座城市。蔚来表示......
### [【Bobo搞怪日常】/表情包ExpressionPackage](https://www.zcool.com.cn/work/ZNjExNjY2NTI=.html)
> 概要: 正在参与：别管我，表情包是我唯一的交流方式......
### [OPPO / 一加 ColorOS 13 系统升级亮点曝光：全新 UI 设计和部分图标重绘、优化性能电池内存、延迟降低等](https://www.ithome.com/0/632/306.htm)
> 概要: IT之家7 月 29 日消息，此前多款一加、OPPO 手机开启了 Android 13×ColorOS 13 升级内测。爆料图显示，ColorOS 13 支持创建九宫格大文件夹，可以直接点击打开 Ap......
### [阔诺新连载哒！7月新连载漫画不完全指北第四期](https://news.dmzj.com/article/75100.html)
> 概要: 家里蹲男生和学校有名的问题儿童成了同班同学，喧闹的高中生活这就开始了！最强时之魔术士回到过去，力图拯救自己的青梅竹马！不幸且穷的男子，要入住超便宜房子的条件是和狂拽酷炫美男子同居？！请看本周指北！
### [为什么我们越来越迷恋成为蝼蚁？](https://www.huxiu.com/article/621820.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童题图丨《降临》本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。巨物爱好者过年了。几天前，......
### [传大基金总裁丁文武被调查，行业人士：很突然 ，刚出席公开活动](https://www.yicai.com/news/101490125.html)
> 概要: 近日，多位大基金相关人士被调查的消息引发业内高度关注，尤其是对于未来中国芯片行业的投资，可能产生一石激起千层浪的效应。
### [越南：资本热土，遍地都是商机吗？](https://www.tuicool.com/articles/nq6F3mR)
> 概要: 越南：资本热土，遍地都是商机吗？
### [市场情绪大幅走低 短期是否需要减仓？](https://www.yicai.com/news/101490143.html)
> 概要: 市场情绪大幅走低 短期是否需要减仓？
### [组图：马思纯分享夏日好心情自拍 长发蓬松面露甜笑嘟嘴搞怪](http://slide.ent.sina.com.cn/star/slide_4_86512_373222.html)
> 概要: 组图：马思纯分享夏日好心情自拍 长发蓬松面露甜笑嘟嘴搞怪
### [证监会最新表态！做好全面注册制准备](https://www.yicai.com/news/101490251.html)
> 概要: 做实做细全面实行股票发行注册制的各项准备
### [被育碧解雇的前《刺客信条》创意总监 现已加盟腾讯](https://www.3dmgame.com/news/202207/3848143.html)
> 概要: 此前被育碧解雇的《刺客信条》创意总监Ashraf Ismail现已加盟了腾讯，开发一个新的游戏。Ashraf Ismail曾是《刺客信条：英灵殿》《刺客信条：起源》的创意总监，由于滥用职权和婚外情指控......
### [首发价 8299/8999 元，联想新款拯救者 R9000X 正式发布：R7 6800H 处理器，最高 RX 6800S 显卡](https://www.ithome.com/0/632/335.htm)
> 概要: IT之家7 月 29 日消息，联想在今日晚间的拯救者 A+A R 系列新品发布会上推出了 R9000X 笔记本，首发价 8299 元起。拯救者 R9000X 标配 R7 6800H 处理器，显卡可选R......
### [F5进取游戏发布会复盘，第三年精彩不断](https://www.3dmgame.com/news/202207/3848139.html)
> 概要: F5进取游戏联合发布会，一份送给所有玩家的惊喜在今年也如约而至。7月29日晚19:30;已成功举办第三年的F5进取游戏联合发布会在全平台进行直播。在这一个半小时的直播中，F5为玩家们上演了一场福利满满......
### [《喜羊羊与灰太狼》奇妙大营救结局篇，灰太狼居然被孤心狼拯救了](https://new.qq.com/omn/20220729/20220729A0AUHB00.html)
> 概要: 喜羊羊与灰太狼之奇妙大营救终于迎来结局了，这一季的作品，可以堪称是神作，剧情的起伏实在是太强了，而且官方在结尾的时候，还引出了下一季的作品，所以羊守系列第六部很快就来临了。在结局篇中，喜羊羊和灰太狼两......
### [韩网翻译Lehends：很期待我们和T1谁会赢，希维尔是清兵机器](https://bbs.hupu.com/54948254.html)
> 概要: 虎扑7月29日讯 韩媒fomos发布“看到Gumayusi垃圾话的Lehends：不知道是不是因为我年纪大了，只是觉得很有趣，没有什么其他想法”一文，原文翻译如下：（未经允许禁止转载，搬运截图）“很期
### [流言板记者分析：对詹姆斯而言，合同灵活度比薪资保障更为重要](https://bbs.hupu.com/54948284.html)
> 概要: 虎扑07月29日讯 近日，雅虎体育记者Ben Rohrbach撰文分析湖人前锋勒布朗-詹姆斯的续约前景。节选如下：勒布朗-詹姆斯在自由球员市场的悠久历史每一次，詹姆斯在进入他在骑士、热火和第二段骑士生
### [中金公司一季度职工薪酬超112亿；雷克萨斯回应“遇车祸车门疑未解锁”](https://www.yicai.com/news/101490368.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [高志丹已任国家体育总局局长、党组书记](https://finance.sina.com.cn/china/gncj/2022-07-29/doc-imizirav5971919.shtml)
> 概要: 7月29日，国家体育总局官网“总局领导”栏目信息显示，此前担任国家体育总局副局长、党组成员的高志丹现已担任国家体育总局局长、党组书记。
### [机构：1-7月全国百强房企拿地规模同比下降55.6%](https://finance.sina.com.cn/china/2022-07-29/doc-imizirav5971403.shtml)
> 概要: 原标题：机构：1-7月全国百强房企拿地规模同比下降55.6% 北京商报讯（记者 王寅浩 孙永志）7月29日，中指研究院发布了2022年1-7月全国房企拿地TOP100排行榜报告。
### [今年4月以来广东合计退税1434亿元 制造业约占28%](https://finance.sina.com.cn/jjxw/2022-07-29/doc-imizmscv4076237.shtml)
> 概要: 中新社广州7月29日电 （孙秋霞）记者29日从中国国家税务总局广东省税务局获悉，截至7月20日，广东地区（不含深圳）共为12.6万户纳税人办理退税1520亿元（人民币，下同）...
### [国常会部署进一步扩内需，延续免征新能源汽车购置税](https://finance.sina.com.cn/china/gncj/2022-07-29/doc-imizirav5971932.shtml)
> 概要: 据央视新闻消息，国务院总理李克强29日主持召开国务院常务会议，部署进一步扩需求举措，推动有效投资和增加消费。 会议指出，按照疫情要防住、经济要稳住...
### [国内航线燃油附加费迎今年首降，8月5日起买一张票最高降60元](https://www.yicai.com/news/101490376.html)
> 概要: 五连涨后的国内航线燃油附加费终于下跌，主要是由于国际航油价格的持续回落。
### [暗黑不朽手游7.25国服震撼开启，移动端复现酣畅割草体验！](http://www.investorscn.com/2022/07/29/102117/)
> 概要: 国内暗黑玩家期待已久的《暗黑破坏神：不朽》终于在7月25日正式开启。而国服上线不久后，这款新作就延续了当初国际服在国外的火爆程度，迅速登顶国内免费榜榜首，随后又占据畅销榜第二的位置，让人惊叹这款经典I......
### [中国成功发射遥感三十五号03组卫星](https://finance.sina.com.cn/china/2022-07-29/doc-imizmscv4077505.shtml)
> 概要: 中新网北京7月29日电 （马帅莎 胡煦劼 王茄欢）北京时间7月29日21时28分，中国在西昌卫星发射中心使用长征二号丁运载火箭，成功将遥感三十五号03组卫星发射升空。
### [流言板CUBA全国赛：王岚嵚10分，清华战胜重庆文理学院晋级四强](https://bbs.hupu.com/54948733.html)
> 概要: 虎扑07月29日讯 CUBA全国赛今日拉开战幕，清华大学对阵重庆文理学院，全场比赛结束，清华大学84-46战胜重庆文理学院晋级全国四强。具体数据：清华大学：邹阳19分7篮板6助攻，王岚嵚10分2篮板3
# 小说
### [剑葬三千界](http://book.zongheng.com/book/884582.html)
> 作者：沐南封

> 标签：奇幻玄幻

> 简介：老天爷开不开那道门，是老天爷的事。我要做的，只是守在这里，每一个来的人，请谨记一条:兽走留皮，雁过拔毛！想硬闯？看看你的头，有没有我的剑硬！

> 章节末：第四百七十章  梦碎无数（完结）

> 状态：完本
# 论文
### [On the Equivalence between Neural Network and Support Vector Machine | Papers With Code](https://paperswithcode.com/paper/on-the-equivalence-between-neural-network-and)
> 概要: Recent research shows that the dynamics of an infinitely wide neural network (NN) trained by gradient descent can be characterized by Neural Tangent Kernel (NTK) \citep{jacot2018neural}. Under the squared loss, the infinite-width NN trained by gradient descent with an infinitely small learning rate is equivalent to kernel regression with NTK \citep{arora2019exact}. However, the equivalence is only known for ridge regression currently \citep{arora2019harnessing}, while the equivalence between NN and other kernel machines (KMs), e.g. support vector machine (SVM), remains unknown. Therefore, in this work, we propose to establish the equivalence between NN and SVM, and specifically, the infinitely wide NN trained by soft margin loss and the standard soft margin SVM with NTK trained by subgradient descent. Our main theoretical results include establishing the equivalence between NN and a broad family of $\ell_2$ regularized KMs with finite-width bounds, which cannot be handled by prior work, and showing that every finite-width NN trained by such regularized loss functions is approximately a KM. Furthermore, we demonstrate our theory can enable three practical applications, including (i) \textit{non-vacuous} generalization bound of NN via the corresponding KM; (ii) \textit{non-trivial} robustness certificate for the infinite-width NN (while existing robustness verification methods would provide vacuous bounds); (iii) intrinsically more robust infinite-width NNs than those from previous kernel regression. Our code for the experiments are available at \url{https://github.com/leslie-CH/equiv-nn-svm}.

read more
### [Fine-Grained Neural Network Explanation by Identifying Input Features with Predictive Information | Papers With Code](https://paperswithcode.com/paper/fine-grained-neural-network-explanation-by)
> 概要: One principal approach for illuminating a black-box neural network is feature attribution, i.e. identifying the importance of input features for the network's prediction. The predictive information of features is recently proposed as a proxy for the measure of their importance.