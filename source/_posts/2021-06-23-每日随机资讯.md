---
title: 2021-06-23-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Nichinan_ZH-CN9549208263_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-06-23 22:56:02
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Nichinan_ZH-CN9549208263_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [华为 Linux 内核贡献者被批评刷 KPI 后续](https://www.oschina.net/news/147304/huawei-linux-kernel-kpi-jerk-ending)
> 概要: 开源软件供应链点亮计划，等你来！>>>华为 Linux 内核贡献者 Leizhen 近日在邮件列表回复了此前被批评刷 API一事。他在邮件中提到自己过去对内核的主要贡献是优化 ARM64 SMMU 驱......
### [国人主导的开源高性能 RISC-V 处理器“香山”来了](https://www.oschina.net/news/147332/risc-v-processor-xiangshan)
> 概要: 开源软件供应链点亮计划，等你来！>>>昨日在上海举办的首届 RISC-V 中国峰会上，中科院计算所宣布了一款开源的高性能 RISC-V 处理器 —— 香山。香山是一款开源的高性能 RISC-V 处理器......
### [数据库软件公司 Couchbase 提交 IPO 申请](https://www.oschina.net/news/147292/couchbase-ipo)
> 概要: 开源软件供应链点亮计划，等你来！>>>2021 年的 IPO 热潮还在继续。数据库软件公司 Couchbase 于近日发布了首次公开募股的文件，证实了外界有关其即将上市的报道。Couchbase 计划......
### [Qt 发布 Android 自动驾驶技术预览](https://www.oschina.net/news/147303/qt-publish-qaa-preview)
> 概要: 开源软件供应链点亮计划，等你来！>>>Qt 官方发文宣布，其Qt Android Automotive OS技术预览 (QAA) 现已发布，并可供所有商业 Qt Device Creation 被许可......
### [开源项目的 5 年长跑，runc v1.0 终于正式发布！](https://segmentfault.com/a/1190000040223946?utm_source=sf-homepage)
> 概要: 本文我来分享下与我们（搞容器化/K8S 从业者）息息相关的一个基础项目runc是如何自 2016 年发布了 v1.0.0-rc1 到现在历经 5 年长跑，从 rc1 一直到 rc95 ，如今终于正式发......
### [电影咖“下凡”救不了国产剧](https://www.huxiu.com/article/436486.html)
> 概要: 来源 |刺猬公社（ID：ciweigongshe）作者 | 星晖 六一题图 | 视觉中国多年前章子怡声名鹊起之际，诱人的邀约找上门来。导演张艺谋曾郑重提醒她爱惜羽毛，永远不要去演电视剧。二十多年过去，......
### [“垄断者”滴滴，如何丢掉了支配地位？](https://www.huxiu.com/article/436474.html)
> 概要: 来源 | 表外表里（ID：excel-ers）作者 | 陈子儒 周霄题图 | 视觉中国“开过滴滴吗？”“没有！”“来，我告诉你，司机上任，得贷款买车、服务上乘、跪舔乘客、增加订单，乘客舒服了给好评，才......
### [视频：王子文吴永恩贴脸合照秀恩爱 亲密搂肩笑容灿烂好甜蜜](https://video.sina.com.cn/p/ent/2021-06-23/detail-ikqciyzk1275229.d.html)
> 概要: 视频：王子文吴永恩贴脸合照秀恩爱 亲密搂肩笑容灿烂好甜蜜
### [视频：宣萱透露电影《寻秦记》上映时间 可能要补拍](https://video.sina.com.cn/p/ent/2021-06-23/detail-ikqcfnca2676062.d.html)
> 概要: 视频：宣萱透露电影《寻秦记》上映时间 可能要补拍
### [组图：网友外滩偶遇王思聪 贴心为美女开门坐红色豪车超拉风](http://slide.ent.sina.com.cn/star/slide_4_86512_358405.html)
> 概要: 组图：网友外滩偶遇王思聪 贴心为美女开门坐红色豪车超拉风
### [主流手游均已适配鸿蒙 流畅性比安卓有所提升](https://www.3dmgame.com/news/202106/3817356.html)
> 概要: 华为鸿蒙系统发布后，获得国内用户的强烈关注，尤其是华为手机群体，正在犹豫是否升级系统，不过一大担心就是目前的安卓游戏能否在鸿蒙系统上稳定运行。现在这个顾虑可以放下了，日前，鸿蒙系统迎来了公开发布后的首......
### [《Apex英雄》最新剧情动画 探路者身世谜团明了](https://www.3dmgame.com/news/202106/3817360.html)
> 概要: 《Apex英雄》开发商Respawn于今日在各大社交网站上发布了最新的游戏剧情动画。官方推特给动画的命名为“真相”。游戏剧情动画：该动画由游戏中漫画剧情改编，讲述了英雄——探路者的生平故事。在之前“F......
### [小米电视6挑战万元画质天花板 范志毅都为其代言](https://www.3dmgame.com/news/202106/3817362.html)
> 概要: 今日(6月23日)中国首位亚洲足球先生、中国职业足球运动员、原中国国家足球队队长范志毅发布小米电视6预告片。视频中，范志毅分享了小米电视6系列的关键信息——“高端画质百级分区起”。视频欣赏：据悉，小米......
### [手越祐也独立一年计划七夕发新歌 实现音乐梦想](https://ent.sina.com.cn/s/j/2021-06-23/doc-ikqciyzk1315343.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 原NEWS成员手越祐也去年6月19日离开杰尼斯事务所独立，随后在6月23日举行记者见面会宣布此事，至今刚好一年，他决定单独出道，从七夕开始发音乐作品，终于实现了他音乐活动......
### [【火箭社·翻译】选秀大会前20位球员分析：实力强劲但差距不大（第1位）](https://bbs.hupu.com/43811579.html)
> 概要: 内容较多，将分段发布ps：成文时间6月22日，分析顺序不代表预测顺位，以下所有观点仅为约翰-霍林格的个人观点，跟不少主流媒体存在一定差异。等等，我们不是才刚进行完选秀吗？在度过了一个压缩的NBA赛季和
### [为什么美国人总爱欺负贝索斯？](https://www.huxiu.com/article/436579.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。作为世界首富的贝索斯，在互联网上又被欺负......
### [组图：利路修着果绿色水洗牛仔套装潮范足 大步流星气场强](http://slide.ent.sina.com.cn/z/v/slide_4_704_358421.html)
> 概要: 组图：利路修着果绿色水洗牛仔套装潮范足 大步流星气场强
### [I made 56874 calls to explore the telephone network. Here’s what I found](https://shufflingbytes.com/posts/wardialing-finnish-freephones/)
> 概要: I made 56874 calls to explore the telephone network. Here’s what I found
### [Details of Yesterday's Bunny CDN Outage](https://bunny.net/blog/the-stack-overflow-of-death-dns-collapse/)
> 概要: Details of Yesterday's Bunny CDN Outage
### [《龙姬混～日子 LOVE+PLUS》登陆Steam 支持繁中](https://www.3dmgame.com/news/202106/3817393.html)
> 概要: 由Whirlpool开发并由Sekai Project发行的《龙姬混～日子LOVE+PLUS》已经登陆Steam平台，国区开启优惠活动，折后价19元，以下为相关介绍内容。Steam商城页面截图：游戏介......
### [New network of European sleeper trains planned](https://www.theguardian.com/travel/2021/jun/22/new-network-of-european-sleeper-overnight-trains-planned)
> 概要: New network of European sleeper trains planned
### [一文深入理解 Kubernetes](https://www.tuicool.com/articles/EjEJVfU)
> 概要: 作者：xixie，腾讯 IEG 后台开发工程师这篇文章，你要翻很久，建议收藏。Kubernetes，简称 K8s，是用 8 代替 8 个字符“ubernete”而成的缩写。是一个开源的，用于管理云平台......
### [松下 S1 / S5 全画幅无反固件更新：支持 HDMI 输出 RAW 视频](https://www.ithome.com/0/558/958.htm)
> 概要: IT之家6 月 23 日消息 今日松下影像宣布，为 S1 / S1R / S5 三款全画幅无反相机推出新固件，新增多项新功能，S1 / S5 两款相机支持 HDMI 输出 RAW 视频。松下 LUMI......
### [两大“奇葩设计”生死决战！深圳新地标方案出炉，大碗对决宽面！](https://www.tuicool.com/articles/ZVR3AjY)
> 概要: 你们不要再打了！组合一下！文章来源：日本设计小站ID：japandesign作者：日站君编辑：卝生“地标设计又翻车了啊？！”——这是日站君在看到深圳“前海城市新中心地标方案设计国际竞赛”其中的参赛作品......
### [以前挺火的五部武侠剧，每一部都是经典，如今无人翻拍！](https://new.qq.com/omn/20210623/20210623A0B4LC00.html)
> 概要: 第一部《少年黄飞鸿》老剧好看，没话说，每个人物都很可爱，何中华的黄麒英和庞统，在我这可以演技封神了，吕丽萍的吴娴，郝蕾的十三姨，姚芊羽的艳红，还有我们的男主角释小龙的黄飞鸿，真的好会演，真的是天生的演......
### [TeXmacs 2.1](https://www.texmacs.org/tmweb/home/videos.en.html)
> 概要: TeXmacs 2.1
### [一个D2C精简护肤品牌，如何赢得中美Z世代消费者？](https://www.tuicool.com/articles/jaaQVra)
> 概要: 图片来源@视觉中国文 | 峰瑞资本FREESFUND把桉树、甘蔗等天然原料作为制鞋材料的Allbirds、耗水少且碳排放低的豌豆奶Ripple Foods、可十分钟内徒手组装的沙发Burrow……这些......
### [口齿“榴”香，佰味葫芦猫山王榴莲饼 16 枚 14.88 元](https://lapin.ithome.com/html/digi/558972.htm)
> 概要: 口齿“榴”香，佰味葫芦猫山王榴莲饼 8 枚报价 24.9 元，叠加满 2 件立享 6 折优惠，限时限量 15 元券，拍 2 件共发 16 枚实付 14.88 元包邮，领券并购买。使用最会买 App下单......
### [陈好中戏教授职称被质疑，谁之过？](https://new.qq.com/omn/20210623/20210623A0BNDL00.html)
> 概要: 近日，有网友质疑陈好作为中央戏剧学院表演系教授，却未发表任何论文，引发热议。            在网友提出质疑后，中戏官网将陈好的名字默默从教授一栏中移出，并改到了国家一级演员当中。陈好的工作室也......
### [绿联发布 4K 无线投屏器：60GHz 毫米波  30 米距离，1699 元](https://www.ithome.com/0/558/982.htm)
> 概要: IT之家6 月 23 日消息 绿联今日发布了“星云瞭望者”4K 无线投屏器，包含一个发射器以及一个接收器。该产品可以实现0-30m 远距离投屏，提供高质量的 4K 分辨率画面，兼容多种输入方式。这款产......
### [《狙击手：幽灵战士契约2》PS5版8月24日发售](https://www.3dmgame.com/news/202106/3817413.html)
> 概要: CI Games宣布，《狙击手：幽灵战士契约2》PS5版将于8月24日发售，售价39.99美元。本作最初于6月4日发售，登陆了Xbox Series，PS4，Xbox One和PC Steam。Ste......
### [去中心化杠杆交易协议Lever集成Chainlink预言机](https://www.btc126.com//view/173284.html)
> 概要: 官方消息，去中心化杠杆交易协议 Lever.Network 宣布集成 Chainlink 预言机，从 DOGE/USD 和 EPS/USD 开始，Lever 将参考 Chainlink 喂价发放贷款，......
### [北京冬奥组委发布《北京2022年冬奥会和冬残奥会遗产报告（2020）》](https://finance.sina.com.cn/china/2021-06-23/doc-ikqcfnca2816030.shtml)
> 概要: 原标题：北京冬奥组委发布《北京2022年冬奥会和冬残奥会遗产报告（2020）》 参考消息网6月23日报道 2021年6月23日上午，北京冬奥组委会同国家体育总局、中国残联...
### [第五批国家组织药品集采结果“出炉” 拟中选药品平均降价56%](https://finance.sina.com.cn/china/2021-06-23/doc-ikqciyzk1418032.shtml)
> 概要: 原标题：新华全媒+|第五批国家组织药品集采结果“出炉” 拟中选药品平均降价56% 新华社上海6月23日电（记者彭韵佳、龚雯）第五批国家组织药品集中带量采购23日在上海开标...
### [BTC五分钟内下跌1.3%，现报$34,123.76](https://www.btc126.com//view/173290.html)
> 概要: BTC五分钟内下跌1.3%，下跌金额为449.31美元，其中欧易OKEx上现价为$34,123.76，请密切关注行情走向，注意控制风险。更多实时行情异动提醒，快在APP内添加自选，开启智能盯盘，快人一......
### [流言板穆里尼奥：萨卡天赋异禀并且非常自信，我完全相信他](https://bbs.hupu.com/43821807.html)
> 概要: 虎扑06月23日讯 在今天凌晨进行的英格兰1-0击败捷克的欧洲杯第三轮比赛当中，为三狮军团首发出战的阿森纳小将萨卡发挥出色，赛后他也被欧足联官方评选为本场比赛最佳球员。而早在比赛开始之前，罗马主帅穆里
### [李佳琦薇娅公司否认上市传闻 蛋壳公寓创始人被限制高消费](https://finance.sina.com.cn/china/2021-06-23/doc-ikqciyzk1421355.shtml)
> 概要: 原标题：李佳琦、薇娅公司否认上市传闻；蛋壳公寓创始人被限制高消费… 文/锌财经责编组 整理 李佳琦、薇娅公司否认上市传闻 近日有消息称，李佳琦、薇娅公司拟赴美IPO...
### [建党百年纪念币认购火爆，投资与收藏价值如何判断？一文尽览](https://finance.sina.com.cn/roll/2021-06-23/doc-ikqcfnca2821095.shtml)
> 概要: 原标题：建党百年纪念币认购火爆，投资与收藏价值如何判断？一文尽览 6月21日起，央行陆续发行中国共产党成立100周年纪念币，23日下午银行渠道预约已结束。
### [视频丨澳门举行庆祝中国共产党成立100周年大型主题图片展](https://finance.sina.com.cn/china/2021-06-23/doc-ikqciyzk1420954.shtml)
> 概要: 原标题：视频丨澳门举行庆祝中国共产党成立100周年大型主题图片展 由国务院新闻办公室、澳门特区政府、澳门中联办主办的“中国共产党的100年——庆祝中国共产党成立100周年大...
### [华为笔记本产品一直缺货，消息称 8 月份会缓解，还有新款 PC 在路上](https://www.ithome.com/0/558/992.htm)
> 概要: IT之家6 月 23 日消息 华为今年 5 月推出了两款 MateView 无线原色显示器以及华为 MateBook 16 等产品。微博博主 @长安数码君 透露，华为今年还有很多新品，例如将于八月前后......
### [深入解读 Flink SQL 1.13](https://www.tuicool.com/articles/quUZrqn)
> 概要: 摘要：本文由社区志愿者陈政羽整理，Apache Flink 社区在 5 月份发布了 1.13 版本，带来了很多新的变化。文章整理自徐榜江(雪尽)5 月 22 日在北京的 Flink Meetup 分享......
### [陈乔恩的“甜爱蜜恋”，曾收割古天乐和霍建华，还与吴磊传绯闻](https://new.qq.com/omn/20210623/20210623A0DM0A00.html)
> 概要: 文丨朱小北编辑丨大麦2016年，网上传出某30多岁的台湾女星，和内地小鲜肉的瓜，分外惹人注意。            网友对号入座，很快锁定了陈乔恩和吴磊。            很多人不信，但流言不......
### [流言板塞尔电台：皇马询问了皇家社会前锋伊萨克，得到积极反馈](https://bbs.hupu.com/43822172.html)
> 概要: 虎扑06月23日讯 据塞尔电台的皇家社会跟队记者Robeo Ramajo消息，皇马已经向伊萨克的经纪公司询问了他今夏转会离队的可能。皇马对于伊萨克的现状很感兴趣，他们收到的报告显示伊萨克获得了长足的进
### [流言板多方消息：梅西已准备宣布留队视频，最快明天官宣](https://bbs.hupu.com/43822191.html)
> 概要: 虎扑06月23日讯 根据RAC1电台知名记者Romero，西班牙六台驻巴塞罗那记者Jose Alvarez Haya等人透露称，梅西宣布续约视频已经准备就绪。根据JAH的说法，巴萨准备在6月30日之前
### [加密分析师：比特币mempool被清算表明BTC需求已达到最低水平](https://www.btc126.com//view/173293.html)
> 概要: U.today消息，加密分析师、投资经理Timothy Peterson称，比特币mempool（积压交易）被清算可能表明市场崩溃。他表示，比特币mempool是待处理交易在被添加到区块链之前等待处理......
### [DeFi项目Yield宣布完成1000万美元融资，Paradigm领投](https://www.btc126.com//view/173294.html)
> 概要: Decrypt消息，DeFi项目Yield今日宣布完成1000万美元融资，Paradigm领投，Framework Ventures、Symbolic Capital Partners、CMS Hol......
### [连淮伟签约留白：2次参加《青你》均意难平，新的花路已开启](https://new.qq.com/omn/20210623/20210623A0DT4200.html)
> 概要: 6月23日，留白娱乐正式官宣连淮伟的加入，这个消息让无数粉丝感到非常惊喜。            连淮伟所加入的这个留白娱乐曾打造过《长安十二时辰》《天醒之路》，还有待播中的《风起洛阳》《良言写意》等......
### [《苹果日报》今日午夜起停止运作 明日出版最后一份报纸](https://finance.sina.com.cn/china/2021-06-23/doc-ikqcfnca2823730.shtml)
> 概要: 原标题：《苹果日报》今日午夜起停止运作 明日出版最后一份报纸 香港壹传媒有限公司董事会23日下午宣布，决定今天午夜后即时停止运作...
# 小说
### [大剑域](http://book.zongheng.com/book/936732.html)
> 作者：挂挡模式

> 标签：奇幻玄幻

> 简介：金戈铁马，一剑封喉！神剑现世，万魔跪拜！人族王者，神通广大！……哪怕是，万丈深渊，深陷魔窟，为了寻到你，杀尽天下阻碍者，有何不可！这，是一代剑神，对天下人，天下魔的宣战！

> 章节末：第一百四十九章  一场便是空（大结局！）

> 状态：完本
# 论文
### [Residuals-based distributionally robust optimization with covariate information](https://paperswithcode.com/paper/residuals-based-distributionally-robust)
> 日期：2 Dec 2020

> 标签：None

> 代码：https://github.com/rohitkannan/DD-DRO

> 描述：We consider data-driven approaches that integrate a machine learning prediction model within distributionally robust optimization (DRO) given limited joint observations of uncertain parameters and covariates. Our framework is flexible in the sense that it can accommodate a variety of learning setups and DRO ambiguity sets.
### [Attentional Graph Neural Network for Parking-slot Detection](https://paperswithcode.com/paper/attentional-graph-neural-network-for-parking)
> 日期：6 Apr 2021

> 标签：None

> 代码：https://github.com/Jiaolong/gcn-parking-slot

> 描述：Deep learning has recently demonstrated its promising performance for vision-based parking-slot detection. However, very few existing methods explicitly take into account learning the link information of the marking-points, resulting in complex post-processing and erroneous detection.
