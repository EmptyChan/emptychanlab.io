---
title: 2023-03-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.EdaleValley_ZH-CN8464524952_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-10 22:33:20
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.EdaleValley_ZH-CN8464524952_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [退休后，你能拿多少养老金？](https://www.huxiu.com/article/815353.html)
> 概要: 本文来自微信公众号：财经十一人（ID：caijingEleven），作者：郑慧，编辑：刘建中，头图来自：视觉中国永远有人正在年轻，但没有人会永远年轻。与已经步入老年的上一辈人不同，还未老去的这届打工人......
### [抖音泛商城“起势”，正在测试商业化](https://www.woshipm.com/it/5777405.html)
> 概要: 抖音的知情人士称：泛商城是抖音电商今年最重要的业务。今年抖音电商整体GMV目标不低于2万亿，其中泛商城的目标大概要占整体目标的30%左右。本篇文章提到了抖音具体将如何实现这一目标，一起来看看吧。抖音电......
### [“百亿补贴”点燃38大促了吗？](https://www.woshipm.com/it/5777335.html)
> 概要: 京东近日上线的“百亿补贴”项目，是否让这个38大促掀起了更多波澜？可能各家电商平台在这个38大促里都有着自己的动作和考量，平台们关于此次京东“百亿补贴”背后的价格之争，似乎也不太热衷。一起来看看作者的......
### [电商场景下，多种优惠叠加时的商品平摊和退款规则](https://www.woshipm.com/pd/5777215.html)
> 概要: 每到大促时，各种优惠叠加算到我们头疼。那么对于产品经理而言，电商场景下存在一个订单中使用了多种优惠的场景，会涉及到退款和优惠平摊的规则，这时候该怎么设计才合适？作者结合具体案例，谈谈其中的逻辑。在电商......
### [乘胜追击！OpenAI将为企业和个人用户提供更多个性化服务](https://finance.sina.com.cn/tech/it/2023-03-10/doc-imykitke9752959.shtml)
> 概要: 新浪科技讯 北京时间3月10日早间消息，据报道，作为ChatGPT的开发商，OpenAI CEO山姆·阿尔特曼（Sam Altman）表示，该公司将推出一系列工具，让用户可以更好地控制这套生成式人工智......
### [苹果将推出独立古典音乐应用Apple Music Classical](https://finance.sina.com.cn/tech/it/2023-03-10/doc-imykitki2816122.shtml)
> 概要: 新浪科技讯 北京时间3月10日早间消息，据报道，当地时间周四，苹果宣布，将在3月28日推出一名为Apple Music Classical的古典音乐独立App应用，它将为现有的Apple Music付......
### [通过RPA+ AI自动生成尽调报告及合同，「案牍AutoDocs」希望改变尽调及合规的工作方式｜项目报道](https://36kr.com/p/2163944086417668)
> 概要: 通过RPA+ AI自动生成尽调报告及合同，「案牍AutoDocs」希望改变尽调及合规的工作方式｜项目报道-36氪
### [海外new things｜「Immi」完成1000万美元A轮融资，生产更多口味的健康方便面](https://36kr.com/p/2158520649326338)
> 概要: 海外new things｜「Immi」完成1000万美元A轮融资，生产更多口味的健康方便面-36氪
### [《木卫四协议》新DLC“传染捆绑包” 3月14日上线](https://www.3dmgame.com/news/202303/3864473.html)
> 概要: 开发商Striking Distance Studios在推特上宣布《木卫四协议》的新DLC“传染捆绑包”将于3月14日上线，该DLC是《木卫四协议》季票的第二部分，《木卫四协议》季票将分为四个部分发......
### [度小满将赴港上市？回应：为不实消息](https://finance.sina.com.cn/tech/2023-03-10/doc-imykixsh1641248.shtml)
> 概要: 新浪科技讯 3月10日上午消息，有消息称度小满（原百度金融）最快将于今年上半年正式向港交所递交招股书，拟主板挂牌上市。成为百度AI生态下又一家独立上市的公司......
### [悦肤达丨安安金纯丨品牌详情页](https://www.zcool.com.cn/work/ZNjQzNzg0OTI=.html)
> 概要: *平台展示仅做设计交流，不负任何法律责任，实际以上线为准。品牌：悦肤达项目产品：悦肤达次抛精华液项目监制：Black shadow_阿文文案策划：悦肤达品牌方项目摄影：芃睿品牌诠释者页面设计：Blac......
### [《层层恐惧》开发商希望能保持独立 腾讯是最大股东](https://www.3dmgame.com/news/202303/3864488.html)
> 概要: 《灵媒》、《寂静岭2：重制版》、《层层恐惧》系列的开发商Bloober Team近日在与IGN交谈时表示，他们希望能在保持工作室的独立自主的同时，抵制其他公司的收购计划。当被IGN问及Bloober ......
### [“花都开咯！”——小兔仙官](https://www.zcool.com.cn/work/ZNjQzNzk2OTI=.html)
> 概要: 之前画的两张兔年生肖贺图，大家春天快乐！～......
### [TV动画《水星的魔女》第二季预告PV公开](https://news.dmzj.com/article/77411.html)
> 概要: TV动画《机动战士高达 水星的魔女》公开了第二季的预告PV。
### [36氪专访丨星纪魅族CEO沈子瑜：三年之内重回中高端手机市场前五，新魅族不造车](https://36kr.com/p/2163798070112521)
> 概要: 36氪专访丨星纪魅族CEO沈子瑜：三年之内重回中高端手机市场前五，新魅族不造车-36氪
### [雅迪的AAA，存在多少泡沫？](https://www.huxiu.com/article/815495.html)
> 概要: 出品 | 虎嗅ESG组作者 | 一夜航船头图 | 视觉中国本文是#ESG进步观察#系列第016篇文章本次观察关键词：碳足迹、产品质量与安全、清洁技术机遇近日，由北京市场监督管理局的抽查报告直指雅迪电动......
### [2023年东盟碳中和峰会|ASEAN Carbon Neutral Summit 2023](http://www.investorscn.com/2023/03/10/106173/)
> 概要: 2023年东盟碳中和峰会|ASEAN Carbon Neutral Summit 2023
### [施耐德电气举办绿色智能制造创赢计划结营仪式 以创新加速数实融合](http://www.investorscn.com/2023/03/10/106174/)
> 概要: 近日，由工业和信息化部国际经济技术合作中心、施耐德电气联合主办的第三季绿色智能制造创赢计划结营仪式在上海成功举办。本次活动汇聚3年来的重要成果，亮点不断：5家加速营创新中小企业围绕5大场景，展示联合创......
### [UP10TION李欢喜因健康原因辞演《BOYS PLANET》](https://ent.sina.com.cn/y/yrihan/2023-03-10/doc-imykkkha1421633.shtml)
> 概要: 新浪娱乐讯 韩国男团UP10TION成员李欢喜因健康原因而辞演了Mnet《BOYS PLANET》节目。　　李欢喜的经纪公司今天宣布，李欢喜因为健康原因而辞演了Mnet《BOYS PLANET》，虽然......
### [动画《七魔剑支配天下》将于7月开播，第一弹PV公开](https://news.dmzj.com/article/77412.html)
> 概要: TV动画《七魔剑支配天下》将于7月开始放送，第1弹关键视觉图、第1弹PV、声优阵容及工作人员公开。
### [元宇宙征途前哨：STT链Statter生态钱包正式发布](http://www.investorscn.com/2023/03/10/106179/)
> 概要: 作为全球第一个兼顾安全、公平和高效率的元宇宙公链平台,Statter Network在经过长时间的建设和实践后,官方频频暗示项目已经无限接近于可上线的状态。前不久刚宣布获得Holo Foundatio......
### [奥斯汀·沃尔夫透露过去曾和数千人发生过关系](https://ent.sina.com.cn/s/u/2023-03-10/doc-imykkqpx2363324.shtml)
> 概要: 新浪娱乐讯 据媒体3月8日报道，知名演员奥斯汀·沃尔夫（Austin Wolf）在最近的采访中透露他过去总共和数千人发生过关系。　　“可能在 7000 到 9000 之间，”奥斯汀·沃尔夫透露道，并补......
### [年轻人不买账，阿迪很受伤](https://www.huxiu.com/article/815302.html)
> 概要: 出品 | 虎嗅商业消费组作者 | 齐敏倩编辑 | 苗正卿题图 | 视觉中国3月8日下午，北京三里屯太古里临街的广场上人来人往。隔壁的阿迪达斯旗舰店里，一楼女鞋区坐着不少正在试穿的顾客，两名男顾客径直走......
### [传《星空》新发售日期由B社自行决定 微软完全放权](https://www.3dmgame.com/news/202303/3864516.html)
> 概要: 本周早些时候，B社和微软宣布了《星空》延期到9月6日发售的消息，而现在据VGC报道称，这个新日期是由B社自己决定的，微软愿意给予B社更多时间来开发《星空》，以防止它变成下一个《赛博朋克2077》。VG......
### [组建国家数据局，能改变哪些产业？](https://www.huxiu.com/article/817030.html)
> 概要: 出品｜虎嗅科技组作者｜齐健编辑 | 陈伊凡头图｜视觉中国数据要素，正在变得越来越重要。2023年3月7日，据新华社报道，根据国务院关于提请审议国务院机构改革方案的议案，组建国家数据局。国家数据局将负责......
### [TV动画《机甲爱丽丝》关键视觉图、新PV公开](https://news.dmzj.com/article/77413.html)
> 概要: TV动画《机甲爱丽丝Expansion》将于4月3日开始放送。同时公开了关键视觉图和新PV。
### [市场监管总局：2022 年新能源汽车召回数量创历史新高，将推进事故调查体系建设](https://www.ithome.com/0/678/797.htm)
> 概要: IT之家3 月 10 日消息，国家市场监督管理总局于今日发布了 2022 年全国汽车和消费品召回情况的通告。2022 年，我国共实施汽车召回 204 次，涉及车辆 448.8 万辆，分别比上年降低 1......
### [横版射击《劲爆51飞行队》中文版3月16日登陆NS](https://www.3dmgame.com/news/202303/3864523.html)
> 概要: 由巴西电影公司Fehorama Filmes和独立游戏制作人Loomiarts联袂打造的横版射击游戏《劲爆51飞行队》将于2023年3月16日登陆Nintendo Switch平台，支持繁体中文。预告......
### [TP-LINK 春季发布会官宣，有望发布 Wi-Fi 7 路由器](https://www.ithome.com/0/678/805.htm)
> 概要: 感谢IT之家网友华南吴彦祖、评论圈主任的线索投递！IT之家3 月 10 日消息，TP-LINK 春季新品发布会今日官宣，将在 3 月 25 日 19:30 举行。目前，TP-LINK 官方暂未公布发布......
### [AI绘制性感唯美的美少女图集3 蒂法爱丽丝等逼真](https://www.3dmgame.com/bagua/5857.html)
> 概要: 今天又为大家带来一些AI绘制的美少女图集。AI绘制的二次元妹子纤细唯美，三次元妹子则让人难辨真假。最近国外网友开始制作了“动起来的AI美女”，虽然只是简单的眨眨眼，但也代表了从平面到动态的进步。以AI......
### [江波龙发布 AQUILA 系列 RDIMM 企业级专用内存条，DDR4 3200 2R×4 规格](https://www.ithome.com/0/678/821.htm)
> 概要: IT之家3 月 10 日消息，江波龙近日发布了 Longsys AQUILA 系列32GB 2R×4 规格 DDR4 RDIMM 企业级专用内存条，速率可达 3200MT / s。这是江波龙成立近 2......
### [国内首款 1200 平方米 8K 裸眼 3D 户外大屏启动，位于广州北京路](https://www.ithome.com/0/678/822.htm)
> 概要: IT之家3 月 10 日消息，据 4K 花园消息，由 4K 花园独家运营的国内首块 1200㎡ 8K 裸眼 3D 户外大屏 ——“8K 春天”正式启动，位于广州北京路新大新大厦。图源 4K 花园据介绍......
### [TV动画《我家的英雄》公布第2弹PV](https://news.dmzj.com/article/77419.html)
> 概要: 根据山川直辉、朝基胜士原作改编的TV动画《我家的英雄》将于4月2日首播，官方公开了第2弹PV。
### [3月10日这些公告有看头](https://www.yicai.com/news/101698375.html)
> 概要: 3月10日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [共40国！第二批恢复出境团队旅游国家名单来了](https://finance.sina.com.cn/jjxw/2023-03-10/doc-imykkuvs9696582.shtml)
> 概要: 为进一步贯彻落实党中央、国务院决策部署，按照国务院联防联控机制外事组要求，2023年3月15日起，文旅部试点恢复全国旅行社及在线旅游企业经营中国公民赴有关国家（第二批...
### [沪指低开低走，两市超4200只个股下跌丨复盘论](https://www.yicai.com/video/101698323.html)
> 概要: 沪指低开低走，两市超4200只个股下跌丨复盘论
### [两会声音|长三角如何助力双循环？代表建议增设中欧班列集结中心](https://finance.sina.com.cn/china/2023-03-10/doc-imykkuvv2256109.shtml)
> 概要: 中新网北京3月10日电 （记者 张斌 董易鑫）北京时间3月9日17时许，西班牙马德里时间3月9日10时许，以“中西建交纪念号”特别命名的“义新欧”中欧班列从浙江金华义乌和马德里同...
### [12.9%！M2增速创7年新高](https://www.yicai.com/news/101698387.html)
> 概要: "这表明，货币政策更加精准有力，市场流动性较为充裕，金融对实体经济支持力度较大。"
### [文旅部发布第二批恢复出境团队旅游国家名单](https://www.yicai.com/news/101698403.html)
> 概要: 通知要求有序组织实施。各地要指导旅游企业安全有序组织实施试点工作。
### [外墙算建筑面积会增加公摊？酒精和免冲洗洗手液能杀灭诺如病毒？事实是这样的](https://www.yicai.com/news/101698423.html)
> 概要: 洞悉事件原委，多方一手求证，还原事实真相
### [全国人大代表曾从钦：坚守初心使命，认真履职尽责](http://www.investorscn.com/2023/03/10/106181/)
> 概要: 十四届全国人大一次会议于3月5日在北京隆重召开。全国人大代表，五粮液集团（股份）公司党委书记、董事长曾从钦不负民之重托，坚守初心使命，认真履职尽责，以高度的政治责任和饱满的精神状态参与大会各项议程......
### [信贷“开门红”态势延续，居民中长贷、短贷双双改善](https://finance.sina.com.cn/roll/2023-03-10/doc-imykkuvr2958734.shtml)
> 概要: 最大的亮点就是信贷同比高增的主要贡献项是居民端，居民贷款结束了15个月的同比少增。 继1月信贷投放“天量”增长后，2月信贷投放依然保持了较快节奏...
### [3999元起！荣耀Magic5系列正式开售，京东“先人一步”购机可享24期免息](http://www.investorscn.com/2023/03/10/106182/)
> 概要: 3月10日，荣耀Magic5系列正式开售，售价3999元起。荣耀Magic5系列搭载第二代骁龙8、荣耀自研射频增强芯片、荣耀鹰眼相机系统、荣耀青海湖电池、超动态臻彩显示技术，在信号、影像、续航上、显示......
### [刘慈欣《超新星纪元》将影视化 制作中英文版本](https://ent.sina.com.cn/m/f/2023-03-10/doc-imykkuvs9741408.shtml)
> 概要: 新浪娱乐讯 据Deadline报道，影视公司Conqueror Entertainment已将刘慈欣科幻作品《超新星纪元》影视化作为首批开展项目，除中文改编外，该公司还计划同时制作《超新星纪元》的英文......
### [深交所：发布3条数字经济细分主题指数](https://finance.sina.com.cn/roll/2023-03-10/doc-imykkuvr2974227.shtml)
> 概要: 来源：上海证券报 上证报中国证券网讯（记者 时娜）“深证XR” “数字文化”“新型显示”主题指数来了！ 3月10日，深交所全资子公司深圳证券信息有限公司（以下简称“深证信息”）...
### [专家称可再生能源补贴支出低于预期，受补贴核查等因素影响](https://finance.sina.com.cn/roll/2023-03-10/doc-imykkzcq9671484.shtml)
> 概要: 可再生能源电价附加收入安排的支出低于预期。 2022年是实质性解决可再生能源拖欠困局的重要一年。成效如何，企业及投资界一直保持高度关注。
### [海外new things｜「HR Signal」完成160万美元种子前轮融资，用算法帮助公司留住员工](https://36kr.com/p/2159487133966339)
> 概要: 海外new things｜「HR Signal」完成160万美元种子前轮融资，用算法帮助公司留住员工-36氪
# 小说
### [大唐开局圈地为王](http://book.zongheng.com/book/1141599.html)
> 作者：七耳

> 标签：历史军事

> 简介：贞观三年，一场罕见大雪侵袭大唐，百姓民不聊生，饿殍遍野李昊作为重生者，此时却接到系统发布的任务，让他在大唐圈地为王他只能派人跟李世民谈条件“陛下若答应我们自建主权，整个大唐现在需要多少粮食，我们全包了！”“瘟疫问题，我们也一并解决，保证大唐不再受瘟疫之苦！”“此外，我们还能让粮食产量提高数十倍，保证天下百姓丰衣足食！”“大唐的农业，工业，我们也能让它进步几十年！”……朝廷上，众人皆惊。完成第一个任务，李昊有收到系统发布的第二个，第三个任务“打造一个最强王国！”“建立自己的殖民地！”……

> 章节末：第七百四十三章：功成身退

> 状态：完本
# 论文
### [Domain Randomization-Enhanced Depth Simulation and Restoration for Perceiving and Grasping Specular and Transparent Objects | Papers With Code](https://paperswithcode.com/paper/domain-randomization-enhanced-depth)
> 日期：7 Aug 2022

> 标签：None

> 代码：https://github.com/pku-epic/dreds

> 描述：Commercial depth sensors usually generate noisy and missing depths, especially on specular and transparent objects, which poses critical issues to downstream depth or point cloud-based tasks. To mitigate this problem, we propose a powerful RGBD fusion network, SwinDRNet, for depth restoration. We further propose Domain Randomization-Enhanced Depth Simulation (DREDS) approach to simulate an active stereo depth system using physically based rendering and generate a large-scale synthetic dataset that contains 130K photorealistic RGB images along with their simulated depths carrying realistic sensor noises. To evaluate depth restoration methods, we also curate a real-world dataset, namely STD, that captures 30 cluttered scenes composed of 50 objects with different materials from specular, transparent, to diffuse. Experiments demonstrate that the proposed DREDS dataset bridges the sim-to-real domain gap such that, trained on DREDS, our SwinDRNet can seamlessly generalize to other real depth datasets, e.g. ClearGrasp, and outperform the competing methods on depth restoration with a real-time speed. We further show that our depth restoration effectively boosts the performance of downstream tasks, including category-level pose estimation and grasping tasks. Our data and code are available at https://github.com/PKU-EPIC/DREDS
### [Investigating Deep Learning Benchmarks for Electrocardiography Signal Processing | Papers With Code](https://paperswithcode.com/paper/investigating-deep-learning-benchmarks-for)
> 概要: In recent years, deep learning has witnessed its blossom in the field of Electrocardiography (ECG) processing, outperforming traditional signal processing methods in various tasks, for example, classification, QRS detection, wave delineation. Although many neural architectures have been proposed in the literature, there is a lack of systematic studies and open-source libraries for ECG deep learning. In this paper, we propose a deep learning framework, named \texttt{torch\_ecg}, which gathers a large number of neural networks, both from literature and novel, for various ECG processing tasks. It establishes a convenient and modular way for automatic building and flexible scaling of the networks, as well as a neat and uniform way of organizing the preprocessing procedures and augmentation techniques for preparing the input data for the models. Besides, \texttt{torch\_ecg} provides benchmark studies using the latest databases, illustrating the principles and pipelines for solving ECG processing tasks and reproducing results from the literature. \texttt{torch\_ecg} offers the ECG research community a powerful tool meeting the growing demand for the application of deep learning techniques.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
