---
title: 2021-10-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FanalMadeira_ZH-CN5337723033_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-10-19 22:41:35
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FanalMadeira_ZH-CN5337723033_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [CakePHP 4.3.0-RC4 已发布，PHP 快速开发框架](https://www.oschina.net/news/164813/cakephp-4-3-0-rc4-released)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>CakePHP 是一个运用了诸如 ActiveRecord、Association Data Mapping、Fro......
### [J 语言联合创始人 Roger Hui 因癌症去世](https://www.oschina.net/news/164823/roger-hui-has-died)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>J 语言邮件列表昨日发布了 J 语言发明者之一 Roger Hui 去世的讣告。我们从讣告得知，Roger Hui ......
### [微软推送 Windows 11 更新，修复 AMD CPU 性能问题](https://www.oschina.net/news/164826/windows-11-amd-cpu-performance-fix)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>微软已于近日在Beta和 Release Preview Channels 中面向 Windows Insiders......
### [阿里发布自研 CPU 芯片倚天 710](https://www.oschina.net/news/164854/alibaba-cpu-yitian-710)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>10 月19 日，2021 云栖大会现场，阿里巴巴旗下半导体公司平头哥发布自研云芯片倚天 710。该芯片是业界性能最......
### [京都动画纵火杀人案被告人将再度接受精神鉴定！](https://news.dmzj.com/article/72484.html)
> 概要: 根据日本媒体报道，日本京都地方法院为了调查在京都动画工作室进行纵火杀人，造成了36人死亡的被告人青叶真司（43岁）是否有刑事能力，同意再次对其进行精神鉴定。
### [《与变成了异世界美少女的大叔一起冒险》新PV](https://news.dmzj.com/article/72487.html)
> 概要: 根据池泽真、津留崎优原作制作的TV动画《与变成了异世界美少女的大叔一起冒险》宣布了将于2022年1月开播，第一弹PV也一并公开。
### [【直播预告】这周要和四郎愉快地拼积木啦](https://news.dmzj.com/article/72492.html)
> 概要: 本周直播预告
### [昨晚，苹果解释了什么叫“吓人”](https://www.huxiu.com/article/464882.html)
> 概要: 前两天，Intel CEO 接受美国媒体 Axios 采访，他说：苹果认为，他们自己可以制造出比我们更好的芯片。而且，你知道，他们做得很好。所以我要做的就是做一个比他们自己做的更好的芯片。我希望随着时......
### [半价打车，薅了谁的羊毛？](https://www.huxiu.com/article/464771.html)
> 概要: 本文来自微信公众号：财经十一人（ID：caijingEleven），作者：刘以秦、赵嫣雨、乔雨萌，编辑：谢丽容，题图来自视觉中国网约车平台刚刚出现时，通过补贴吸引用户。当平台逐渐扩大，红包、优惠等补贴......
### [Phat Company《无职转生》洛琪希1/7比例手办](https://news.dmzj.com/article/72494.html)
> 概要: Phat Company根据TV动画《无职转生-到了异世界就拿出真本事-》中的洛琪希·米格路迪亚制作的1/7比例手办目前已经开订了。本作采用了洛琪希在旅行途中准备战斗的造型。
### [漫画「生命短暂 行善吧少女」第2卷封面公开](http://acg.178.com/202110/428608832849.html)
> 概要: 漫画「生命短暂 行善吧少女」公开了第2卷的封面图，该卷将于11月26日发售。「生命短暂 行善吧少女」是土管创作的四格漫画作品，讲述了主角风上乙女在不幸去世后获得了复活的机会，但需要依靠行善来续命的故事......
### [视频：王俊凯发文告别平遥影展“跟山西面食一样厚实耐寻味”](https://video.sina.com.cn/p/ent/2021-10-19/detail-iktzqtyu2257334.d.html)
> 概要: 视频：王俊凯发文告别平遥影展“跟山西面食一样厚实耐寻味”
### [动漫头像 “我知我喜乐，纵情跋涉”。](https://new.qq.com/omn/20211014/20211014A0CAEG00.html)
> 概要: 动漫头像“我知我喜乐，纵情跋涉”。cr  wxgzh(二次元头像库)......
### [曝金宣虎骗女友堕胎抛弃对方 公司取消其新剧采访](https://ent.sina.com.cn/v/j/2021-10-19/doc-iktzqtyu2266068.shtml)
> 概要: 新浪娱乐讯 韩国艺人金宣虎近日被爆料哄骗女友堕胎后抛弃对方。19日，金宣虎的经纪公司宣布取消tvN电视剧《海岸村恰恰恰》的终映采访。此前，剧中女主角申敏儿已宣布取消采访。　　一名自称是金宣虎前女友的网......
### [动画「与变成了异世界美少女的大叔一起冒险」视觉图公开](http://acg.178.com/202110/428611879500.html)
> 概要: 由池泽真·津留崎优创作的同名漫画改编的电视动画「与变成了异世界美少女的大叔一起冒险」公开了最新视觉图，本作将于2022年1月开始播出。「与变成了异世界美少女的大叔一起冒险」是一部魔法冒险类漫画作品，讲......
### [David Zucker reflects on "Airplane!" in 2021](https://www.commentary.org/articles/david-zucker/wokeness_destroys-comedy/)
> 概要: David Zucker reflects on "Airplane!" in 2021
### [组图：Amber新浪扫楼飚东北话 挑战抠糖饼失败变身太阳花](http://slide.ent.sina.com.cn/y/k/slide_4_704_362818.html)
> 概要: 组图：Amber新浪扫楼飚东北话 挑战抠糖饼失败变身太阳花
### [「宝可梦：超世代」小瑶/火稚鸡手办开订](http://acg.178.com/202110/428615703956.html)
> 概要: 寿屋作品「宝可梦：超世代」小瑶/火稚鸡手办开订，全高约195mm（含底座），主体采用ABS和PVC材料制造。该手办定价为10780日元（含税），约合人民币606元，预计于2022年4月发售......
### [中影协青委会吸纳新会员 发布入会条件及审批程序](https://ent.sina.com.cn/m/c/2021-10-19/doc-iktzscyy0537296.shtml)
> 概要: 新浪娱乐讯 10月19日，中国电影家协会青年和新文艺群体工作委员会发布公告，广泛吸纳新会员，同时也正式发布会员入会条件及审批程序。　　中国电影家协会青年和新文艺群体工作委员会，简称“中国影协青委会”，......
### [谁说小机场不重要？](https://www.huxiu.com/article/465004.html)
> 概要: 本文来自微信公众号：读城记工作室（ID：DUCHENGJIPLUS），作者：戴睿敏，编辑：马妮，头图来自：视觉中国玉林福绵机场旁山坡上的人群，伴随着飞机引擎轰鸣声开始躁动起来。随着飞机缓缓向前滑行，大......
### [「斗破苍穹三年之约」公开概念海报及定档PV](http://acg.178.com/202110/428624216015.html)
> 概要: 国产动画「斗破苍穹三年之约」（特别篇3）公开了概念海报以及定档PV。该作共13集，将于10月31日开始播出。「斗破苍穹三年之约」定档PV概念海报：该作是阅文集团、企鹅影视、万达影业出品，幻维数码制作的......
### [梦幻联动！寿屋1/7初音未来×小马宝莉雕像开订](https://acg.gamersky.com/news/202110/1431252.shtml)
> 概要: 知名玩具制造商寿屋于近日公布了1/7初音未来×小马宝莉雕像，该款雕像于今日开启预订
### [Spork: Peer-to-peer socket magic in the air](https://spork.sh/)
> 概要: Spork: Peer-to-peer socket magic in the air
### [年轻人租个房怎么那么难](https://www.huxiu.com/article/464841.html)
> 概要: 作者｜周超臣头图｜电视剧《我在他乡挺好的》剧照在电视剧《我在他乡挺好的》第一集里，乔夕辰租的房子的房东上门砸门，带着一帮人未经许可闯入开始收拾东西，逼乔夕辰立马搬走，乔夕辰问凭什么。房东说：“就凭你们......
### [Nim 1.6](https://nim-lang.org/blog/2021/10/19/version-160-released.html)
> 概要: Nim 1.6
### [载人飞行器公司「小鹏汇天」完成超 5 亿美元融资，创亚洲低空载人飞行领域最大单笔融资](https://www.ithome.com/0/581/462.htm)
> 概要: IT之家10 月 19 日消息，据小鹏汽车官方公众号，今天，小鹏汽车生态企业小鹏汇天宣布完成超过 5 亿美元 A 轮融资，投前估值超 10 亿美元。此轮融资由 IDG 资本、小鹏汽车等领投。小鹏汇天的......
### [组图：好美！方媛扮美人鱼在水下拍写真 皮肤白皙纤纤细腰身材辣](http://slide.ent.sina.com.cn/star/w/slide_4_704_362828.html)
> 概要: 组图：好美！方媛扮美人鱼在水下拍写真 皮肤白皙纤纤细腰身材辣
### [Arm 推出物联网全面解决方案，平均减少 2 年产品开发时间](https://www.ithome.com/0/581/472.htm)
> 概要: 10 月 19 日报道，今天，Arm 推出了Arm 物联网全面解决方案（Arm Total Solutions for IoT）。该方案可以加快物联网产品开发速度，使物联网产品的开发时间从 5 年最多......
### [国内首个聋哑人快递组织：一个公益项目的破产](https://www.tuicool.com/articles/Fj2mUve)
> 概要: 作者 | 赵维鹏编辑 | 诗婕曾有科技公司找到吾声快递，推荐使用他们的文字转语音或者手语转汉字功能。「但是没什么用，很多聋哑人的语法、语序都是错的。翻译过来别人也看不懂。」在大量聋哑人使用的手语语言中......
### [淘宝或将推出退货秒退款功能！剁手党网购退货方便了](https://www.tuicool.com/articles/A7ZnyqM)
> 概要: 一年一度的“双十一”即将到来，热衷网购的同学做好准备了没？今日，据媒体报道，淘宝将为88VIP会员用户推出商品极速退款服务，有望双十一期间上线。据介绍，会员发起未发货订单退款申请，无需商家审核，平台将......
### [Reflect (YC S20) is hiring a software engineer to modernize web testing](https://www.workatastartup.com/jobs/46431)
> 概要: Reflect (YC S20) is hiring a software engineer to modernize web testing
### [从中心化到去中心化，稳定币将如何发展？](https://www.tuicool.com/articles/Ifeauy2)
> 概要: 稳定币已经成为加密市场中最为常见和最为实用的价值媒介之一。在加密市场发展初期，投资者眼中的基础资产是 BTC，其他加密货币都需要使用 BTC 来兑换。不过，比特币早期巨大的波动性导致了加密投资者的风险......
### [昆明一女童幼儿园受伤下体缝7针：园方称意外摔伤，警方介入](https://finance.sina.com.cn/jjxw/2021-10-19/doc-iktzqtyu2384265.shtml)
> 概要: 原标题：昆明一女童幼儿园受伤下体缝7针：园方称意外摔伤，警方介入涉事幼儿园澎湃新闻记者王万春图 刚入读昆明扬帆贝贝滇池卫城幼儿园两天...
### [中国信通院：今年 9 月国内市场手机出货量 2140 万部，同比下降 8.1%](https://www.ithome.com/0/581/494.htm)
> 概要: IT之家10 月 19 日消息，今日，中国信通院发布《2021 年 9 月国内手机市场运行分析报告》。数据显示，2021 年 9 月，国内市场手机出货量为 2144 万部，同比下降 8.1%。其中，5......
### [国家发改委：进一步释放煤炭产能，严厉查处资本恶意炒作动力煤期货](https://finance.sina.com.cn/roll/2021-10-19/doc-iktzqtyu2386214.shtml)
> 概要: 原标题：国家发改委：进一步释放煤炭产能，严厉查处资本恶意炒作动力煤期货 据国家发展改革委消息，10月19日下午，国家发展改革委负责同志主持召开煤电油气运重点企业保供...
### [突出北京特色，打造冬奥新地标，北京大力建设国际消费中心城市](https://finance.sina.com.cn/china/dfjj/2021-10-19/doc-iktzqtyu2385496.shtml)
> 概要: 原标题：突出北京特色，打造冬奥新地标，北京大力建设国际消费中心城市 来源：北京商报 环球主题公园逐步凸显溢出效应、首钢园将展示出全新面貌...
### [这个男人凭什么让漫威惦记了十多年？看完你就明白了！](https://new.qq.com/rain/a/20211019A0DD5500)
> 概要: 这不是我第一次聊“基努·里维斯”这个男人，当然，也不会是最后一次！            之所以多次提及到基努·里维斯，除了这个男人身上所汇聚了魅力以外，更多的还是因为基努·里维斯的为人处世的态度，让......
### [14岁女孩靠呼吸机维持生命，刚拿到手的救命钱却被骗光...](https://finance.sina.com.cn/wm/2021-10-19/doc-iktzscyy0621400.shtml)
> 概要: 原标题：14岁女孩靠呼吸机维持生命，刚拿到手的救命钱却被骗光．．． 近日，黑龙江哈尔滨 一维修小哥李海玉上门疏通下水道 看到卧室里有一位生病女孩 还插着呼吸机...
### [迷雾剧场《八角亭迷雾》强势来袭，剧情环环相扣，越往后看越烧脑](https://new.qq.com/rain/a/20211016A089UV00)
> 概要: 最近段奕宏的热度直线上升，悬疑剧《双探》才刚刚落下帷幕，完美收官，又一部新剧就无缝衔接，它就是广受好评的迷雾剧场，强袭来袭的《八角亭迷雾》引起了不少观众的注意力，身边有不少朋友都在第一时间点开了这部作......
### [复出成谜！黄心颖神隐近一年电话号码也已换掉，与TVB仍有8年长约](https://new.qq.com/rain/a/20211019A0DDE600)
> 概要: 本文编辑剧透社：小彤未经授权严禁转载，发现抄袭者将进行全网投诉如今说起在2019年4月，香港艺人黄心颖与许志安发生的那件“安心偷食”事件，相信大家如今还印象非常深刻。不知不觉间，这件事情也已经过去了有......
### [依法依规守正创新 营造更好的金融环境——访中国人民银行党委书记、银保监会主席郭树清](https://finance.sina.com.cn/roll/2021-10-19/doc-iktzqtyu2386814.shtml)
> 概要: 新华社北京10月19日电 题：依法依规守正创新 营造更好的金融环境——访中国人民银行党委书记、银保监会主席郭树清 新华社记者吴雨 金融领域的反垄断和防止资本无序扩张...
### [8位台湾过气艺人，破产、失业、当服务员，走到今日个个都有因果](https://new.qq.com/rain/a/20211019A0DDHC00)
> 概要: 近年来，随着港圈的没落，有不少香港艺人的境况不太好，比如昔日的亚姐韩君婷破产，TVB配角去工地当搬运工等等。其实台湾演艺圈里也有不少名气大不如前的艺人，有人破产，有人失业，有人晚年没有亲人陪伴，不过也......
### [郭树清：金融业竞争程度很高，部分领域垄断问题突出](https://finance.sina.com.cn/jjxw/2021-10-19/doc-iktzqtyu2386496.shtml)
> 概要: 原标题：郭树清：金融业竞争程度很高，部分领域垄断问题突出 来源：新华社 金融领域的反垄断和防止资本无序扩张，是实现金融治理体系和治理能力现代化的必由之路...
### [西班牙皇室供应商，贝蒂斯特级初榨橄榄油 2.5L 装 88 元（减 180 元）](https://lapin.ithome.com/html/digi/581496.htm)
> 概要: 西班牙皇室供应商，贝蒂斯特级初榨橄榄油 2.5L 装报价 268 元，下单立减 50 元，限时限量 130 元券，实付 88 元包邮，领券并购买。使用最会买 App下单，预计还能再返 14.52 元，......
### [看腻了滤镜，被他们不够完美但真实的样子种草了](https://www.tuicool.com/articles/In2MBfU)
> 概要: 如果在最近打开淘宝App，会发现首页下方菜单栏中第二个按钮有点不一样了。进入这一栏目后，点击“去种草”，就能一键获取你最感兴趣的内容和产品，堪称双11前夕最贴心的种草。这个能够迅速获知用户心意的功能，......
# 小说
### [通界驭灵者](http://book.zongheng.com/book/906035.html)
> 作者：堕落邪浪

> 标签：奇幻玄幻

> 简介：一个人类少年穿越到了……没有穿越，严格意义上来说是去到了另一个世界，为什么说去？因为随时能再回来……是生活在现代都市，还是成长在共存空间异世，贯穿于现世与异世的驭灵使，维护着均衡之道的人类少年，是人各有命，还是我命由我？掀开了一页生灵的救赎，执行了一次命数的转轮。

> 章节末：第八十五章 小结

> 状态：完本
# 论文
### [You Only Hear Once: A YOLO-like Algorithm for Audio Segmentation and Sound Event Detection | Papers With Code](https://paperswithcode.com/paper/you-only-hear-once-a-yolo-like-algorithm-for)
> 日期：1 Sep 2021

> 标签：None

> 代码：https://github.com/satvik-venkatesh/you-only-hear-once

> 描述：Audio segmentation and sound event detection are crucial topics in machine listening that aim to detect acoustic classes and their respective boundaries. It is useful for audio-content analysis, speech recognition, audio-indexing, and music information retrieval.
### [Many-body thermodynamics on quantum computers via partition function zeros](https://pattern.swarma.org/paper?id=be27bb20-0145-11ec-a093-0242ac17000a)
> 概要: Partition functions are ubiquitous in physics: They are important in determining the thermodynamic properties of many-body systems and in understanding their phase transitions. As shown by Lee and Yang, analytically continuing the partition function to the complex plane allows us to obtain its
