---
title: 2023-02-13-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MoonValley_ZH-CN1906470869_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-02-13 21:46:01
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MoonValley_ZH-CN1906470869_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [如何迈向年薪百万？](https://www.woshipm.com/zhichang/5751687.html)
> 概要: 年薪百万，看起来是一件非常遥不可及的事情，然而，只要选好方向，坚持走一条又直又远的路，还是有可能到达的。本文作者结合自己十几年的产品生涯，分享了他从月薪5.5K到年薪百万的历程，希望能给你带来一些启发......
### [从用户满意度层面聊需求筛选](https://www.woshipm.com/pmd/5751373.html)
> 概要: 在产品工作中，我们能够遇到各种各样的问题，产品需求池里累积了各类需求，不知道从何做起。这时候，就需要对需求进行分类管理，进行需求筛选。那么，当我们以用户满意度为主要考量标准时，该如何对现有需求做出合理......
### [九转大肠火了，老综艺流下时代的眼泪](https://www.woshipm.com/it/5752703.html)
> 概要: 春节期间，一道山东的名菜九转大肠出现在大众视野，打开了新年的流量密码，却也意外地让综艺节目《顶级厨师》走红。其实这并不是一个孤例，还有很多这样的例子，下面跟随本文一起来看看吧。春节期间，一段让评委品尝......
### [字节迟早啃下美团这块“肉”](https://www.huxiu.com/article/789201.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国“字节迟早要啃下美团这块‘肉’。”当“抖音外卖即将 3 月上线”的消息席卷互联网时，一位 NKA（全国跨区域连锁品牌）华北市场负责人认为，抖音外卖上线传了......
### [假跑致富、代跑盛行……这乱象，如Keep所愿](https://finance.sina.com.cn/tech/internet/2023-02-13/doc-imyfnzmc2584592.shtml)
> 概要: 被各种商家营销套路侵害权益？买到的商品出故障投诉无门？ 黑猫投诉平台全天候帮您解决消费难题【消费遇纠纷，就上黑猫投诉】......
### [ChatGPT爆红让谷歌抓狂：“退休”创始人布林罕见要求查看代码](https://finance.sina.com.cn/stock/usstock/c/2023-02-13/doc-imyfnzme9367567.shtml)
> 概要: 新浪科技讯 北京时间2月13日早间消息，据报道，近日，早已经退出日常管理的谷歌联合创始人谢尔盖·布林几年来第一次要求访问谷歌内部的软件代码。人工智能领域的白热化竞争已经惊动了这位联合创始人......
### [36氪首发 | 连锁咖啡品牌「AoTiger虎闻咖啡」完成天使轮融资，由「鹿角巷」原班人马打造](https://36kr.com/p/2125775436524552)
> 概要: 36氪首发 | 连锁咖啡品牌「AoTiger虎闻咖啡」完成天使轮融资，由「鹿角巷」原班人马打造-36氪
### [海外new things｜「Digs」完成700万美元种子轮融资，推出建筑蓝图管理系统](https://36kr.com/p/2125955895487488)
> 概要: 海外new things｜「Digs」完成700万美元种子轮融资，推出建筑蓝图管理系统-36氪
### [新能源材料研发商「寒暑科技」获A轮千万元融资，此前已获两轮融资](https://36kr.com/p/2126199891422217)
> 概要: 新能源材料研发商「寒暑科技」获A轮千万元融资，此前已获两轮融资-36氪
### [迪士尼流媒体CTO离职 CEO暗示或出售Hulu股权](https://finance.sina.com.cn/stock/usstock/c/2023-02-13/doc-imyfpfsv8261061.shtml)
> 概要: 新浪科技讯 北京时间2月13日早间消息，迪士尼流媒体首席技术官Jeremy Doig将会离开公司，首席执行官Bob Iger正在重组迪士尼......
### [BPO讨论观众对《机动战士高达》的投诉 2月第2周新闻汇总](https://news.dmzj.com/article/77119.html)
> 概要: 这次准备的是BPO讨论观众对《机动战士高达》新作的投诉，迪士尼宣布三大动画推出续篇，东京动画奖2023公开年度动画作品奖，动画《生而为狗，我很幸福》英文版翻译引争议，最后还是惯例的新作相关消息。
### [四天工作制是延迟退休的镇痛良药吗？](https://www.huxiu.com/article/793029.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。因为法定退休时间要推迟，法国人民罢工了......
### [《君子盟》大结局 井柏然晒兰珏定妆照告别](https://ent.sina.com.cn/v/m/2023-02-13/doc-imyfpmyt8161820.shtml)
> 概要: 新浪娱乐讯 13日，《君子盟》今日大结局，井柏然晒兰珏定妆照告别，他还分享自己对兰珏这一角色的理解，称兰珏是破碎的，但“一路有老师如父亲般的守护，老朋友坚定的信任，新朋友的灵魂救赎”，他“正在小爱到大......
### [警方通报刘亚仁涉毒案进展 暂未对其进行人身处理](https://ent.sina.com.cn/s/j/2023-02-13/doc-imyfpmyy9368505.shtml)
> 概要: 新浪娱乐讯 13日，："以目前的嫌疑，我们还不打算对刘亚仁进行人身处理，等国立科学搜查研究院的鉴定结果再处理。”还称“刘亚仁因违反《毒品管理法》正在毒品调查队接受调查， 第1次嫌疑人调查是在执行扣押搜......
### [《JOJO的奇妙冒险》第9部新海报公开 历代JOJO齐聚](https://acg.gamersky.com/news/202302/1565645.shtml)
> 概要: 荒木飞吕彦原作的《JOJO的奇妙冒险》第9部漫画《The JOJOLands》最新涉谷站宣传长海报公开，除了本作登场的主角和众角色，还有历代JOJO齐聚一堂。
### [恐怖游戏《恐怖录像带》上线Steam 将于3月18日发售](https://www.3dmgame.com/news/202302/3862570.html)
> 概要: 恐怖游戏《恐怖录像带》现已上线Steam页面，预计于3月18日正式推出，游戏提供试玩demo，支持中文。为寻找失踪的兄弟，你独自展开了调查，通过探索那些令人极度不安的地方，一点点的拼凑出真相，并惊讶的......
### [TV动画《Liar·Liar》定档2023年夏](https://news.dmzj.com/article/77124.html)
> 概要: TV动画《Liar·Liar》宣布了将于2023年夏开始播出的消息。另外本作的官方YouTube频道将从本月起至6月，每月公开一段广播剧。第一次公开日为2月14日，内容和情人节有关。
### [SM娱乐为土耳其、叙利亚地震抗震救灾捐款2亿韩元](https://ent.sina.com.cn/s/j/2023-02-13/doc-imyfpshv2541487.shtml)
> 概要: 新浪娱乐讯 13日，SM娱乐公司（以下简称SM）捐赠了2亿韩元帮助土耳其、叙利亚地震受灾的捐款。SM的李成洙、共同代表理事卓英俊（音）表示:"向瞬间失去家人和生活来源的所有人表示慰问"，"在克服灾害的......
### [动力电池厂积极扩产，电芯价格有望回归理性 | 媒体专访](https://36kr.com/p/2129843391201030)
> 概要: 动力电池厂积极扩产，电芯价格有望回归理性 | 媒体专访-36氪
### [环电摄影 | 智米空气净化器海外 ✖ foodography](https://www.zcool.com.cn/work/ZNjM5Njg0MjA=.html)
> 概要: Foodography Visual Lab / Since 2016我们是一家以数字化创意为驱动的创新型品牌视觉服务公司 以电商产品视觉升级服务为核心，致力于为客户提供多样化的数字化视觉和视觉内容解......
### [TV动画《魔王学院的不适合者》第二季延期播出](https://news.dmzj.com/article/77125.html)
> 概要: 目前正在播出中的TV动画《魔王学院的不适合者》第二季宣布了延期播出的消息。这次延期是受到了新冠疫情的影响。从2月18日起，将开始重播第1话至第6话的内容。
### [中国版ChatGPT，你听听就好](https://www.huxiu.com/article/793004.html)
> 概要: 本文来自微信公众号：不宜高调（ID：buyigaodiao），作者：肖芳，编辑：文姝琪，原文标题：《我怎么相信中国版ChatGPT会来呢？》，头图来自：视觉中国中国互联网公司，如果不高调宣布即将推出类......
### [PS VR2新游《家园 VR》演示 中世纪背景城市建设](https://www.3dmgame.com/news/202302/3862577.html)
> 概要: 经典模拟名作即将推出PSVR2版，新游《家园 VR》预定2月22日登陆PSVR平台，日前官方公布了新演示，一起来先睹为快。·《Townsmen VR》利用虚拟现实的令人兴奋的可能性,以全新的游戏体验和......
### [【字幕】奎克利：哈特在防守和进攻方面都带来了额外的帮助，和他一起打球很棒](https://bbs.hupu.com/57939327.html)
> 概要: 【字幕】奎克利：哈特在防守和进攻方面都带来了额外的帮助，和他一起打球很棒
### [李斌、李想、何小鹏的性格与宿命](https://www.huxiu.com/article/793134.html)
> 概要: 本文来自微信公众号：胡说成理（ID：hushuochengli），作者：胡喆，题图来自：视觉中国“南派务实者”何小鹏由于最近虎嗅和36Kr都发布了关于小鹏汽车组织架构和公司治理问题的非常深入的文章，所......
### [《星战绝地:幸存者》不登陆旧主机 总监：契合愿景](https://www.3dmgame.com/news/202302/3862586.html)
> 概要: 虽然《星球大战绝地士：陨落武士团》在PS4和Xbox One上发布，但其续集《星球大战绝地：幸存者》并非如此，它不会登陆PS4和Xbox One，在2023年3月版的PLAY杂志中，游戏总监Stig ......
### [移动流量时代“撞墙” 中国 5G 迎来新关口](https://www.ithome.com/0/673/051.htm)
> 概要: 流量业务收入增长创新低 中国 5G 迎来新关口作者／  IT 时报记者 钱立富编辑／  钱立富 挨踢妹近日，工信部公布了《2022 年通信业统计公报》，整体来看，2022 年中国通信行业增长态势向好，......
### [缺点明显 飞向月球续作《火星孤征》好评率不及前作](https://www.3dmgame.com/news/202302/3862587.html)
> 概要: KeokeN Interactive在2019年推出的科幻冒险游戏《飞向月球》取得了不错的口碑，目前在Steam上的好评率为88%。今年2月该工作室推出了续作《火星孤征》，但目前本作Steam的好评率......
### [三星 Galaxy S22 / Ultra 和 Z Fold 4 在欧洲开始推送安卓 13 / One UI 5.1](https://www.ithome.com/0/673/067.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家2 月 13 日消息，三星在本月初发布的 Galaxy S23 系列中首次推出 One UI 5.1 系统。现在少数人开始收到 Galaxy S23 系列......
### [睡多是浪费！张朝阳四小时睡眠法被炮轰：误导国人](https://www.3dmgame.com/news/202302/3862589.html)
> 概要: 近日，搜狐创始人张朝阳在直播节目中再度谈及自己的“四小时睡眠法”，引发热议。“从医学健康的角度，这是不太科学的。”北京大学第六医院睡眠医学科副主任范滕滕说道。对于张朝阳叫醒后2个小时再睡的做法，范滕滕......
### [中国选手夺得星际2冠军！这能唤醒你的电竞梦吗？](https://news.dmzj.com/article/77130.html)
> 概要: 今天一早，中国选手Oliveira（李培楠）夺得《星际争霸2》2023IEM卡托维兹站总决赛冠军的消息传遍了大街小巷。
### [“充 29.9 元返 100 元话费”投诉激增，18 家经营者被约谈](https://www.ithome.com/0/673/079.htm)
> 概要: IT之家2 月 13 日消息，据“自贡市市场监督管理局”微信公众号消息，2 月 9 日，四川省自贡市市场监管局组织召开自贡市“互联网充值返券”消费投诉信息公示行政约谈会，约谈 18 家涉及“互联网充值......
### [深海城堡的秘密：人类，你从哪里来？](https://www.ithome.com/0/673/081.htm)
> 概要: 本文来自微信公众号：返朴 （ID：fanpu2019），作者：Amanda Heidt撰文 | Amanda Heidt编译 | 黄炎“我从哪里来？”“我从哪里来”，这个问题和“我是谁”“我到哪里去”......
### [报告称美国1月集装箱量回升7.2%，旺季何时到来？](https://www.yicai.com/news/101673423.html)
> 概要: 2023年1月美国集装箱进口量迎来“开门红”，止跌反弹。
### [北京：支持头部企业打造对标ChatGPT的大模型](https://www.yicai.com/news/101673490.html)
> 概要: 据介绍，北京持续保持人工智能领先优势。截至2022年10月，北京拥有人工智能核心企业1048家，占我国人工智能企业总量的29%，位列全国第一。
### [中共中央 国务院关于做好2023年全面推进乡村振兴重点工作的意见(全文)](https://finance.sina.com.cn/jjxw/2023-02-13/doc-imyfqieq9278129.shtml)
> 概要: 新华社北京2月13日电中共中央 国务院关于做好2023年全面推进乡村振兴重点工作的意见 （2023年1月2日） 党的二十大擘画了以中国式现代化全面推进中华民族伟大复兴的宏伟蓝图...
### [“一拖多”遭遇业绩翻车后，多位基金经理选择“减负”](https://www.yicai.com/news/101673491.html)
> 概要: “瘦身”同时还“老带新”
### [2023年中央一号文件：加快先进农机研发推广](https://finance.sina.com.cn/china/2023-02-13/doc-imyfqieq9284904.shtml)
> 概要: 来源：财联社 【2023年中央一号文件：加快先进农机研发推广】财联社2月13日电，中共中央、国务院发布关于做好2023年全面推进乡村振兴重点工作的意见，意见提出...
### [2023年中央一号文件：落实生猪稳产保供省负总责 强化以能繁母猪为主的生猪产能调控](https://finance.sina.com.cn/china/2023-02-13/doc-imyfqiep2507849.shtml)
> 概要: 来源：财联社 【2023年中央一号文件：落实生猪稳产保供省负总责 强化以能繁母猪为主的生猪产能调控】财联社2月13日电，《中共中央...
### [孙春兰出席世界数字教育大会开幕式并致辞](https://finance.sina.com.cn/jjxw/2023-02-13/doc-imyfqiep2514445.shtml)
> 概要: 新华社北京2月13日电世界数字教育大会2月13日在北京开幕，国务院副总理孙春兰出席会议并致辞。 孙春兰指出，现代信息技术对教育发展具有革命性影响。
### [指数震荡上行 板块机会如何捕捉？](https://www.yicai.com/video/101673514.html)
> 概要: 指数震荡上行 板块机会如何捕捉？
### [2023年中央一号文件：确保全国粮食产量保持在1.3万亿斤以上](https://www.yicai.com/news/101673506.html)
> 概要: 实施新一轮千亿斤粮食产能提升行动。开展吨粮田创建。推动南方省份发展多熟制粮食生产，鼓励有条件的地方发展再生稻。
### [帅气十足！陆文博更新社媒分享生活碎片](https://bbs.hupu.com/57943565.html)
> 概要: 浙江队球员陆文博更新个人抖音，分享生活碎片。陆文博分享视频并配文：“很久很久以前。”本赛季至今，陆文博场均出场27.1分钟，得到10分4.6篮板1.3助攻1.6抢断。 来源：  虎扑
### [这些平平无奇的电器，“偷走”了你的电费](https://finance.sina.com.cn/china/2023-02-13/doc-imyfqieq9348082.shtml)
> 概要: 刚刚过去的这个冬天，很多人被电费“背刺”了。 此前有位上海女士晒出自家2022年12月的电费，高达3481元，迅速登上热搜。 其实，不只去年12月，也不只上海...
### [流言板21岁生日快乐！NBA官方晒图为戴申-尼克斯庆生](https://bbs.hupu.com/57943681.html)
> 概要: 虎扑02月13日讯 NBA官方更新推特，晒出一张火箭球员戴申-尼克斯的照片并为其献上生日祝福。“让我们一起祝戴申-尼克斯21岁生日快乐！”NBA官方在推文中写道。本赛季至今，戴申-尼克斯场均可以得到3
### [流言板北控展望第三阶段：国家奥林匹克体育中心体育馆，不见不散](https://bbs.hupu.com/57943825.html)
> 概要: 虎扑02月13日讯 北控队官方更新微博，发文展望本赛季常规赛第三阶段。微博原文：“3月2日，国家奥林匹克体育中心体育馆！不见不散！”本赛季至今，北控男篮11胜17负，暂列联赛第16位。    来源：
# 小说
### [我有一个识海世界](https://book.zongheng.com/book/965899.html)
> 作者：仙道引路人

> 标签：武侠仙侠

> 简介：五彩缤纷的修仙世界，神奇莫测的术法，移山填海的神通，流传恒久的传奇故事，强者传承的争夺，有死无生的必杀墓葬，五光十色的秘境。一点点探寻仙之尽头……

> 章节末：第七百二十二章大结局

> 状态：完本
# 论文
### [Learning Graph Neural Networks for Multivariate Time Series Anomaly Detection | Papers With Code](https://paperswithcode.com/paper/learning-graph-neural-networks-for)
> 概要: In this work, we propose GLUE (Graph Deviation Network with Local Uncertainty Estimation), building on the recently proposed Graph Deviation Network (GDN). GLUE not only automatically learns complex dependencies between variables and uses them to better identify anomalous behavior, but also quantifies its predictive uncertainty, allowing us to account for the variation in the data as well to have more interpretable anomaly detection thresholds.

### [In-BoXBART: Get Instructions into Biomedical Multi-Task Learning | Papers With Code](https://paperswithcode.com/paper/in-boxbart-get-instructions-into-biomedical-1)
> 概要: Single-task models have proven pivotal in solving specific tasks; however, they have limitations in real-world applications where multi-tasking is necessary and domain shifts are exhibited. Recently, instructional prompts have shown significant improvement towards multi-task generalization; however, the effect of instructional prompts and Multi-Task Learning (MTL) has not been systematically studied in the biomedical domain. Motivated by this, this paper explores the impact of instructional prompts for biomedical MTL. We introduce the BoX, a collection of 32 instruction tasks for Biomedical NLP across (X) various categories. Using this meta-dataset, we propose a unified model termed In-BoXBART, that can jointly learn all tasks of the BoX without any task-specific modules. To the best of our knowledge, this is the first attempt to propose a unified model in the biomedical domain and use instructions to achieve generalization across several biomedical tasks. Experimental results indicate that the proposed model: 1) outperforms the single-task baseline by ~3% and multi-task (without instruction) baseline by ~18% on an average, and 2) shows ~23% improvement compared to the single-task baseline in few-shot learning (i.e., 32 instances per task) on an average. Our analysis indicates that there is significant room for improvement across tasks in the BoX, implying the scope for future research direction.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
