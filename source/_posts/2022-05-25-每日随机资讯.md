---
title: 2022-05-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Alhambra_ZH-CN9040625762_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-05-25 22:44:35
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Alhambra_ZH-CN9040625762_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [我的客群观，互联网业务的流量之争](http://www.woshipm.com/operate/5455552.html)
> 概要: 编辑导语：关于互联网业务如何做客群管理，你了解多少呢？其实只要做好分客群就能事半功倍，谁分得更细，谁效率更高谁就是赢家。一起来看看作者的分享。先吐槽几句现在的互联网业务。消费金融：不管需不需要，让人人......
### [大厂产品专家教你：四招开好需求评审会](http://www.woshipm.com/zhichang/5456744.html)
> 概要: 编辑导语：不管是对于刚入行做产品的小白，还是有几年经验的老司机，都可能会在需求评审会遇到一些挑战。本文作者总结了开好需求评审会的四个“招数”，希望能给你带来帮助。提起需求评审会，很多产品的童鞋就一脸抗......
### [互联网平台的「创造」，仍然植根于古老建筑的空间喻体](http://www.woshipm.com/it/5456768.html)
> 概要: 编辑导语：现如今，互联网中新建社交媒体的结构越来越丰富。本篇文章中作者结合多个实际事例来阐述互联网平台的创造与古老建筑的空间之中的密切联系，感兴趣的小伙伴们快来一起看看吧。近两年，世界范围内大量对于互......
### [微信何以成为字节的心病？](https://www.huxiu.com/article/562484.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国上周，抖音内测“兴趣匹配”（根据短视频内容匹配同时观看的网友进行聊天等交互行为）的新闻在互联网不胫而走，预示着字节跳动又悄咪咪在抖音一级入口内给社交挤出来......
### [「妖精的尾巴」作者公布最新绘制的插图](http://acg.178.com/202205/447441736114.html)
> 概要: 近日，「妖精的尾巴」的作者真岛浩公布了最新绘制的插图，插图上的人物为露西与纳兹。「妖精的尾巴」是日本漫画家真岛浩创作的少年漫画。该作品讲述了魔导士公会“妖精的尾巴”的成员露西、纳兹、哈比、格雷、艾露莎......
### [《全职猎人》作者富坚义博推特再更新：神秘大树](https://acg.gamersky.com/news/202205/1485445.shtml)
> 概要: 5月25日，人气漫画《全职猎人》作者富坚义博的推特再次更新了一张图，为森林中的一颗大树。
### [「东京卍复仇者」河田兄弟官方生日贺图公开](http://acg.178.com/202205/447444607299.html)
> 概要: 今天（5月25日）是「东京卍复仇者」中人气角色河田兄弟“河田奈保也&河田飒也”的生日，官方公开了为兄弟俩庆生的生日贺图。河田兄弟是「东京卍会」中第四番队的成员，哥哥河田奈保也是番队队长，弟弟河田飒也是......
### [声优三上枝织担任青森市观光大使](https://news.dmzj.com/article/74506.html)
> 概要: 和分体水手服不一样，连衣裙水手服可以把身体线条完美地衬托出来
### [椎名高志公开「～异传·绘本草子～ 半妖的夜叉姬」新绘图](http://acg.178.com/202205/447446828092.html)
> 概要: 漫画「～异传·绘本草子～ 半妖的夜叉姬」作画椎名高志公开了新绘图，绘制的人物是刹那、永远和诸叶。「～异传·绘本草子～ 半妖的夜叉姬」是高桥留美子担任角色设计、隅沢克之负责脚本协力、椎名高志作画的漫画作......
### [万代「艾尔登法环」白狼战鬼手办开订](http://acg.178.com/202205/447447069839.html)
> 概要: 近日，万代SPIRITS公开了「Figuarts mini」系列可动手办的最新企划，本次推出的是「艾尔登法环」中NPC白狼战鬼的Q版形象，同企划的角色还有梅琳娜。该手办全高约90mm，附赠可装备配件「......
### [《雷神4》全新剧照：男女雷神同框 深情对视](https://www.3dmgame.com/news/202205/3843196.html)
> 概要: 之前《雷神4：爱与雷霆》正式预告发布，展示了大量新画面。今天(5月25日)《雷神4》全新剧照公布，男女雷神同框深情对视，似乎有旧情复燃的迹象。《雷神4》导演塔伊加·维迪提表示该影片会紧跟漫威漫画，贴近......
### [Naur on Programming as Theory Building (1985) pdf](https://pages.cs.wisc.edu/~remzi/Naur.pdf)
> 概要: Naur on Programming as Theory Building (1985) pdf
### [昨日电视购物，今日露营滑雪…回回都上当，当当不一样](https://finance.sina.com.cn/tech/2022-05-25/doc-imizirau4678794.shtml)
> 概要: 作者|肖芳......
### [时隔约1年半 漫画《宝石之国》连载再开](https://news.dmzj.com/article/74508.html)
> 概要: 这期给大家推荐一部恋爱漫《塔子小姐无法成为像样的大人》，作者たかせうみ，讲述没有表情的塔子小姐通过交友软件结识了隐瞒年龄的高中学生，二人结识后发生了一些暧昧有趣的故事。
### [小小梦魇工作室暗示新作开发中 或于6月公布](https://www.3dmgame.com/news/202205/3843202.html)
> 概要: 近日，开发小小梦魇系列的Tarsier Studios工作室发推暗示新作正在开发中，工作室在推文中表示：“我们一直很忙”并配上疑似包含新作概念设计图的短片，传闻新作或将在6月公布。Tarsier St......
### [别“回T”退订，是挡住诈骗短信的第一步](https://www.huxiu.com/article/564245.html)
> 概要: 本文来自微信公众号：每经头条 （ID：nbdtoutiao），作者：潘婷，编辑：廖丹，原文标题：《这种短信，千万别“退订回T”》，题图来自：视觉中国“‘××银行’我们已经为你准备了40万贷款的额度，请......
### [动画《点满农民相关技能后，不知为何就变强了。》PV](https://news.dmzj.com/article/74510.html)
> 概要: 由市川春子创作的漫画《宝石之国》宣布了将于6月24日在《月刊Afternoon》8月号（讲谈社）上连载再开。
### [再次警告人口崩溃：越有钱越不想生](https://finance.sina.com.cn/tech/2022-05-25/doc-imizirau4715951.shtml)
> 概要: 特斯拉CEO埃隆·马斯克（Elon Musk）周二再次对“人口崩溃”发出警告，他过去曾表示，这可能是人类文明未来面临的最大风险......
### [解决邮箱安全问题到底难在哪儿？](https://finance.sina.com.cn/tech/2022-05-25/doc-imizmscu3302619.shtml)
> 概要: 记者/姜菁玲......
### [漫画《名侦探柯南 零的日常》第一部完结](https://news.dmzj.com/article/74511.html)
> 概要: TV动画《点满农民相关技能后，不知为何就变强了。》宣布了将于10月开始播出的消息，本作的第一弹PV也一并公开。在这次的PV中，可以看到主人公被卷入冒险时的片段。
### [《王国之心》20周年纪念版Walkman和耳机 现已开售](https://www.3dmgame.com/news/202205/3843214.html)
> 概要: 为了纪念“王国之心”系列20周年，索尼推出限定主题的NW-A105/KH20 Walkman，售价为税后36000日元（约合人民币1890元），此外还有限定版WF-1000XM4/KH20无线蓝牙耳机......
### [为什么网络诈骗越来越多？周鸿祎：因为拦不住“看似正常”的邮件](https://finance.sina.com.cn/tech/2022-05-25/doc-imizirau4712116.shtml)
> 概要: 来源：@周鸿祎......
### [一场10亿补偿的赌局，对环保业有何启示？](https://www.huxiu.com/article/564213.html)
> 概要: 本文来自微信公众号：青山产业评论（ID：Qingshan-Research），作者：山少爷，原文标题：《从创业板传奇到对赌失利——碧水源20年浮沉启示录｜青山》，头图来自：视觉中国不得不说，近两年从大......
### [从亏损2.7亿到盈利五千万，露营捧红了这家公司](https://www.tuicool.com/articles/ruANZfJ)
> 概要: 从亏损2.7亿到盈利五千万，露营捧红了这家公司
### [被雀巢收购后，失去“第一”的惠氏经历了什么？](https://www.tuicool.com/articles/zIVbaej)
> 概要: 被雀巢收购后，失去“第一”的惠氏经历了什么？
### [图拉斯 X inDare | 半导体挂脖风扇产品创意设计](https://www.zcool.com.cn/work/ZNjAwMDg4NTY=.html)
> 概要: 『 inDare X 图拉斯 』项目时间： 2022 年项目属性：家居生活项目地区：全网服务内容：人体工学设计 | 用户研究 | 产品设计 | 产品视觉符号构建 | 产品落地跟进inDare 携手「 ......
### [贰婶手写--奇妙的中国汉字【拙字趣玩】](https://www.zcool.com.cn/work/ZNjAwMDkxMDQ=.html)
> 概要: 贰婶手写--奇妙的中国汉字【拙字趣玩】......
### [广末凉子兼顾事业与家庭 “格差婚”受关注](https://ent.sina.com.cn/jp/2022-05-25/doc-imizirau4719885.shtml)
> 概要: （文/枣）　　最近这几天，“姐姐”这个词又在国内掀起了热潮。　　说起日本娱乐圈的“姐姐”们，不知道大家会想起谁呢？特别巧的一件事，有几个都叫做“凉子”的姐姐在中日两国的人气、好感度都很不错，　　米仓凉......
### [日本电装开发新长方形二维码 功能一致应用多样化](https://www.3dmgame.com/news/202205/3843230.html)
> 概要: 小小的二维码如今已经遍布我们生活的各个角落，5月25日今天，其最早的发明者所在的日本电装公司宣布研发了全新的长方形二维码rMQR Code，性能与我们目前常用的QR Code保持一致的同时，适用于更多......
### [组图：韩女星金度妍拍摄夏日大片 腮红妆显气质养眼](http://slide.ent.sina.com.cn/y/k/slide_4_704_370220.html)
> 概要: 组图：韩女星金度妍拍摄夏日大片 腮红妆显气质养眼
### [《间谍过家家》即将推出主题咖啡店 带阿尼亚回家](https://acg.gamersky.com/news/202205/1485668.shtml)
> 概要: 《间谍过家家》将推出主题咖啡店，届时将推出各种和《间谍过家家》相关的美食和周边，还能带可爱的一家人回家~
### [减免房租、降宽带费，国资委出台27条举措助力中小企业](https://www.yicai.com/news/101423726.html)
> 概要: 在确保资金安全、对方书面申请、严格履行内部决策程序的前提下，央企可提前支付或预付部分账款。
### [停摆中的VC从业者：投什么可能都是错的](https://www.huxiu.com/article/559617.html)
> 概要: 本文来自微信公众号：家办新智点 （ID：foinsight），作者：foinsight，原文标题：《停摆中的上海VC从业者：保持“钝感”，不想躺平》，头图来自：视觉中国上海按下“暂停键”之后，扎根于此......
### [《海贼王》新剧场版公开新情报 海军、五老星登场](https://acg.gamersky.com/news/202205/1485681.shtml)
> 概要: 《海贼王》新剧场版《ONE PIECE FILM RED》，在今天（5月25日）公开了新情报，曝光了将在片中登场的海军及世界政府阵营的角色，五老星也登场了。
### [优质IP接连爆火，潮玩赛道未来可期](https://www.tuicool.com/articles/nMNzuyv)
> 概要: 优质IP接连爆火，潮玩赛道未来可期
### [36氪出海首发｜亚太版“得物”，球鞋潮玩交易平台Novelship完成近千万美元A轮融资](https://www.tuicool.com/articles/yAfmUjZ)
> 概要: 36氪出海首发｜亚太版“得物”，球鞋潮玩交易平台Novelship完成近千万美元A轮融资
### [R9T改装](https://www.zcool.com.cn/work/ZNjAwMTExMjg=.html)
> 概要: UE5练习~拿铁改装，研究了一下角色和毛发......
### [消息称韩国厂商 12 英寸改 8 英寸晶圆设备正赢得多家日本厂商订单](https://www.ithome.com/0/620/494.htm)
> 概要: 集微网消息，消息人士称，韩国设备厂商 Auros Technology 已经从多家韩国和日本芯片制造商获得了 8 英寸晶圆 overlay 测量设备的订单，其 8 英寸 OL-100n 正在由多达 5......
### [The benefits of “low tech” user interfaces](https://uxdesign.cc/the-forgotten-benefits-of-low-tech-user-interfaces-57fdbb6ac83)
> 概要: The benefits of “low tech” user interfaces
### [国际奢侈品又双叒叕涨价 还有消费者买单吗](https://www.yicai.com/news/101423879.html)
> 概要: 侈品保值增值不过是一个营销噱头，只有少数品牌的少数产品具有一定的保值或者增值属性，而这部分产品中的大部分在市场上处于有限流通和无法流通的状态。绝大部分奢侈品作为消费品和消耗品，即使没有使用，也会大幅折价。
### [大量网友反映QQ被盗！疯狂向好友/群聊发送低俗广告](https://www.3dmgame.com/news/202205/3843240.html)
> 概要: 根据大量用户在微博反映投诉，近日很多网友QQ号都集体被盗，而且会疯狂向好友和QQ群发送低俗x色广告。从这些用户反馈的图片来看，此次的大规模盗号似乎是同一个团队所为，因为发送的广告虽然背景图片各不相同，......
### [组图:古力娜扎摇滚感大片个性十足 斜切刘海搭配烟熏妆好酷飒](http://slide.ent.sina.com.cn/star/slide_4_86512_370231.html)
> 概要: 组图:古力娜扎摇滚感大片个性十足 斜切刘海搭配烟熏妆好酷飒
### [399 元，小米米家智能空气炸锅 Pro 4L 开启众筹](https://www.ithome.com/0/620/520.htm)
> 概要: IT之家5 月 25 日消息，小米米家智能空气炸锅 Pro 4L 今日开启众筹，众筹价 399 元，原价 449 元。IT之家了解到，这款空气炸锅配备了一个“烹饪可视窗”以及 OLED 交互屏，可实时......
### [实控人违规占用逾2亿资金，生物谷成首家被立案调查的北交所公司](https://www.yicai.com/news/101423904.html)
> 概要: 控股股东承诺一个月归还资金，但大部分资产处于质押状态。
### [为何去年全球碳排放量反弹？克里：减排技术缺乏规模效应](https://www.yicai.com/news/101423905.html)
> 概要: 在全球碳中和政策得到推进的2021年，全球碳排放量和煤炭使用量均反弹并达到新高。
### [苹果 iOS 15 地图开始在日本东京提供 AR 步行导航](https://www.ithome.com/0/620/528.htm)
> 概要: IT之家5 月 25 日消息，据 MacRumors 报道，在 iOS 15 中，苹果地图应用程序包括在一些大城市使用增强现实（AR）步行路线的功能，而今天我们了解到日本东京已成为用户可以使用它的最新......
### [稳增长主题看好哪些细分方向？](https://www.yicai.com/news/101423687.html)
> 概要: None
### [2.6 毛洗一桶：好爸爸浓缩洗衣凝珠 260 颗 68 元狂促](https://lapin.ithome.com/html/digi/620539.htm)
> 概要: 【立白集团官方旗舰店】除菌除螨除味，好爸爸浓缩亲肤洗衣凝珠 52 颗 ×5 盒报价 155 元，限时限量 87 元券，实付 68 元包邮，领券并购买。买 4 盒送 1 盒，到手共 5 盒 260 颗......
### [Are You Sure You Want to Use MMAP in Your Database Management System? pdf](http://www.cidrdb.org/cidr2022/papers/p13-crotty.pdf)
> 概要: Are You Sure You Want to Use MMAP in Your Database Management System? pdf
### [SwiftUI in 2022](https://mjtsai.com/blog/2022/05/24/swiftui-in-2022/)
> 概要: SwiftUI in 2022
### [经济日报署名文章：全面辩证看待当前经济形势](https://finance.sina.com.cn/china/gncj/2022-05-25/doc-imizmscu3367926.shtml)
> 概要: 新华社北京5月25日电 经济日报5月26日署名文章：全面辩证看待当前经济形势 经济日报编辑部 科学判断形势、利用趋势、发挥优势、引领态势...
### [国管公积金中心助职工纾困 公积金租房提取限额提高](https://finance.sina.com.cn/jjxw/2022-05-25/doc-imizirau4769408.shtml)
> 概要: 中新财经5月25日电 25日，中央国家机关住房资金管理中心发布《关于落实阶段性支持政策加强中央国家机关住房公积金服务保障工作的通知》...
### [苹果要求App必须内置删除账号功能;马斯克又跨界了!这次是餐饮业](https://www.yicai.com/news/101424115.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [流言板12.10云顶之弈版本更新，S7赛季前的最后一个大版本更新](https://bbs.hupu.com/53884806.html)
> 概要: 虎扑05月25日讯 LOL将在5月26日凌晨1点开始全区停机维护（请注意：5月26日0点将关闭排位赛入口，不会影响正在进行中的排位赛）发布12.10版本，预计停机时间为1:00-12：00 。对于在停
### [海口教育系统面向全国招聘393名事业编工作人员](https://finance.sina.com.cn/china/gncj/2022-05-25/doc-imizirau4770526.shtml)
> 概要: 就业服务平台|海口教育系统面向全国招聘393名事业编工作人员 澎湃新闻记者 杨喆 5月24日，海南省海口市教育局通过其官网公开发布公告称...
### [流言板字母哥连续四年入选一阵，现役球员仅次于詹姆斯杜兰特](https://bbs.hupu.com/53884819.html)
> 概要: 虎扑05月25日讯 今日，NBA官方公布本赛季最佳阵容评选结果，雄鹿前锋扬尼斯-阿德托昆博全票入选最佳阵容一阵。据统计，这是阿德托昆博连续第4个赛季入选一阵，追平詹姆斯-哈登（2017-2020年），
### [流言板芬尼-史密斯：尼利基纳能防多个位置，也能命中三分球](https://bbs.hupu.com/53884833.html)
> 概要: 虎扑05月25日讯 独行侠主场119-109击败勇士，系列赛大比分1-3。赛后，独行侠球员多里安-芬尼-史密斯接受了媒体采访。谈到队友弗朗克-尼利基纳的表现，史密斯说：“弗朗克，他是又一位出色的防守者
### [流言板邮报：穆里尼奥将加大力度引入维拉中场道格拉斯-路易斯](https://bbs.hupu.com/53884836.html)
> 概要: 虎扑05月25日讯 据《每日邮报》的报道，罗马主教练穆里尼奥希望在欧协联决赛后重组阵容，从而为下赛季做准备；而他已经把目光聚集在阿斯顿维拉中场道格拉斯-路易斯。在周三的欧协联决赛之后，穆里尼奥将加大力
### [又一特大城市“零门槛”抢人，晚了吗？](https://finance.sina.com.cn/china/gncj/2022-05-25/doc-imizmscu3370858.shtml)
> 概要: 来源：城市进化论 人口增长已进入下滑区间 大连全面放开落户条件 近日，大连市政府发布《关于全面放开落户条件的通知》，6月1日起，全面放宽个人落户学历和年龄限制。
### [保通保畅月余，货运之路有什么变化？](https://finance.sina.com.cn/china/gncj/2022-05-25/doc-imizirau4771314.shtml)
> 概要: 21世纪经济报道记者高江虹 北京报道 “总理关心的，都是我们司机最重要的事。”电话那头，长途货车司机周吉华的声音难抑喜悦与感激。
### [上海：疫情防控期间减免部分公共停车费用](https://finance.sina.com.cn/jjxw/2022-05-25/doc-imizmscu3370278.shtml)
> 概要: 关于新冠疫情防控期间减免部分公共停车费用的通知 各有关单位： 针对本轮疫情防控以及后续常态化防控期间，因车主被临时封控造成已停放在道路停车场和公共停车场（库）的社...
### [海贼王1050话：四皇时代终结，凯多生死不明，桃之助成为新任将军](https://new.qq.com/omn/20220525/20220525A0D41A00.html)
> 概要: 海贼王1050话情报已经更新，这一话的内容还是很精彩的，和之国篇章很快就要完结了。四皇大妈和凯多双双战败，他们两人是和之国篇章最大的boss，其实凯多才是最大的boss，大妈完全是来凑数的。和之国战争......
### [「时空履行者」百丽时尚鞋履美学设计大赛](https://www.zcool.com.cn/event/bellefashion/)
> 概要: 「时空履行者」百丽时尚鞋履美学设计大赛
# 小说
### [悲欢咒](http://book.zongheng.com/book/1161970.html)
> 作者：一念秦子

> 标签：武侠仙侠

> 简介：悲欢’通常指的是悲，就如同‘恩怨’大多指的是怨。郭万和无意中卷入一场天网追杀吕凤天的大祸乱，经历过各种离奇危险之后，却发现一切的真相居然都完全不同。而他的一生也都在别人的安排之下，一切的事都是身不由己，不甘，却也只能任由命运的摆布。以悲欢说江湖，以江湖说人生。正如你我。

> 章节末：迟到但不会缺席

> 状态：完本
# 论文
### [A Conditional Point Diffusion-Refinement Paradigm for 3D Point Cloud Completion | Papers With Code](https://paperswithcode.com/paper/a-conditional-point-diffusion-refinement-1)
> 日期：7 Dec 2021

> 标签：None

> 代码：None

> 描述：3D point cloud is an important 3D representation for capturing real world 3D objects. However, real-scanned 3D point clouds are often incomplete, and it is important to recover complete point clouds for downstream applications.
### [Extracting Space Situational Awareness Events from News Text | Papers With Code](https://paperswithcode.com/paper/extracting-space-situational-awareness-events)
> 日期：15 Jan 2022

> 标签：None

> 代码：None

> 描述：Space situational awareness typically makes use of physical measurements from radar, telescopes, and other assets to monitor satellites and other spacecraft for operational, navigational, and defense purposes. In this work we explore using textual input for the space situational awareness task. We construct a corpus of 48.5k news articles spanning all known active satellites between 2009 and 2020. Using a dependency-rule-based extraction system designed to target three high-impact events -- spacecraft launches, failures, and decommissionings, we identify 1,787 space-event sentences that are then annotated by humans with 15.9k labels for event slots. We empirically demonstrate a state-of-the-art neural extraction system achieves an overall F1 between 53 and 91 per slot for event extraction in this low-resource, high-impact domain.
