---
title: 2020-12-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HocesDuraton_EN-CN3405878858_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-12-01 22:19:38
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HocesDuraton_EN-CN3405878858_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [王菲女儿李嫣修改ins简介 留两个英文单词引猜疑](https://ent.sina.com.cn/s/m/2020-12-01/doc-iiznezxs4557756.shtml)
> 概要: 新浪娱乐讯 近日，继李嫣清空ins内容后，有网友发现李嫣又悄悄修改了简介。据悉，“Wrong Generation”字面意思为“垮掉的一代”，不知道李嫣是在指代他人还是认为自己也属于这一行列。(责编：......
### [陆毅为母校上戏庆生 晒与鲍蕾大学时自拍甜蜜青涩](https://ent.sina.com.cn/s/m/2020-12-01/doc-iiznezxs4617972.shtml)
> 概要: 新浪娱乐讯 12月1日下午，陆毅发文祝贺母校上海戏剧学院七十五周年快乐，并配文：“75岁，生日快乐，我的母校！ ”除了一张以上海戏剧学院为背景的手写“早上好上戏，七十五岁快乐”的图片，还附赠了一张学生......
### [刘仁娜身体状况突然不佳缺席活动 已接受新冠检测](https://ent.sina.com.cn/s/j/2020-12-01/doc-iiznctke4176859.shtml)
> 概要: 新浪娱乐讯 演员刘仁娜因身体健康突然不佳缺席了电影《新年前夜》的线上制作发布会。　　1日，YG娱乐透露刘仁娜缺席电影《新年前夜》线上制作发布会是因身体状况突然不佳。同样由于健康问题，刘仁娜也没能参加韩......
### [视频：曹格12岁儿子近照曝光已有胡茬 颜值高像爸爸](https://video.sina.com.cn/p/ent/2020-12-01/detail-iiznezxs4563351.d.html)
> 概要: 视频：曹格12岁儿子近照曝光已有胡茬 颜值高像爸爸
### [《原始人2》主创：家庭能引起全世界观众的共鸣](https://ent.sina.com.cn/m/f/2020-12-01/doc-iiznezxs4538818.shtml)
> 概要: 11月27日，好莱坞梦工厂出品的动画电影《疯狂原始人2》在中国内地上映。该系列第一部《疯狂原始人》在上映后获得了非常高的口碑成绩，在豆瓣电影TOP250中排名第185。此时，距离该系列第一部，已经整整......
### [郭敬明四年前立的人设在《演员请就位》崩了](https://new.qq.com/omn/20201201/20201201A0080400.html)
> 概要: 《演员请就位》第二季的节目马上就要接近尾声了，说实话，最近几期的节目由于没有了李诚儒老师的各种毒舌批评反倒是觉得没意思了，每个星期看得导演之间的互相吹捧，点评的全都是一些无关痛痒的毛病，真的很无趣。 ......
### [李湘放权给丈夫！王岳伦掌管她直播事业，成公司最大股东](https://new.qq.com/omn/20201130/20201130A0FE7I00.html)
> 概要: 11月30日据网上资料显示，王岳伦近日成为了李湘关联公司的最大股东，这家名为海南友欢互娱文化传媒有限公司，王岳伦原本持股49%，但如今却变成了99.9%，资料中表明这家公司是在2020年6月成立，注册......
### [张柏芝前男友陈晓东做直播被指脸僵，看起来比谢霆锋还年轻](https://new.qq.com/omn/20201130/20201130A0FVFW00.html)
> 概要: 张柏芝感情方面虽然历尽波折，个性也大大咧咧，但实际上，她在与谢霆锋相恋之前，唯一承认过的男友只有一位，便是陈晓东。            陈晓东出生于1975年，比张柏芝大5岁。他1995年出道便加盟......
### [13岁天天晒照支持张亮话剧，身高超170，背影和爸爸分不清](https://new.qq.com/omn/20201130/20201130A0DXQ800.html)
> 概要: 11月30日，张亮儿子张悦轩（天天）罕见更新社交平台，久违晒照的他，附文写道：“分享图片”，简单的文案不禁让人感慨曾经那个俏皮可爱的小男孩真的是长大了。            照片中，站在舞台上的天天......
### [祸不单行！周星驰与旧爱官司未完，遭上海新文化为对赌协议入禀](https://new.qq.com/omn/20201201/20201201A00G2300.html)
> 概要: 分手十年，周星驰和于文凤还是为钱闹上法庭。            周星驰的前女友于文凤，是香港建设公司名誉主席于镜波的小女儿，她的爸爸为人低调，是香港隐形富豪之一，身家有数十亿，于文凤可以说是标准的白......
# 动漫
### [剧场版《Healin' Good♥光之美少女...大变身！》公开最新PV！](https://news.dmzj.com/article/69410.html)
> 概要: 《光之美少女》系列最新剧场版《Healin' Good ♥光之美少女 在梦之城 心动！GoGo！大变身！！》特报视频公布，将于2021年3月20日上映。
### [《摇曳露营△》系列宣布今冬推出VR游戏](https://news.dmzj.com/article/69412.html)
> 概要: 《摇曳露营△》系列宣布了将于今冬（2021年初）推出VR游戏《摇曳露营△ VIRTUAL CAMP》。在本作中，用户可以在VR世界中，和凛一起享受露营的乐趣。
### [P站美图推荐——辣妹特辑](https://news.dmzj.com/article/69411.html)
> 概要: 淋雨的水珠是「Sexy！」 全身心都是「G.A.L」早熟女子的最强形态
### [游戏王历史：从零开始的游戏王环境之旅第六期02](https://news.dmzj.com/article/69418.html)
> 概要: 随大师规则一同被导入的【同调召唤】引领游戏王OCG这卡牌游戏踏入了新的境地。这次更新将第五期以前的风格完全改写，并且还给整体游戏人口带来了些许影响。时代激变的气息让玩家感到期待的同时又有一些不安，在这种情绪逐渐膨胀的3月中旬，卡池以同调为主进行了...
### [肖战粉丝又出手了，疾冲真人VS画像谁更可？](https://new.qq.com/omn/20201201/20201201A08KR400.html)
> 概要: 有人说《狼殿下》剧情狗血，枯燥乏味，如果不是因为对肖战的一腔执念，很难坚持下来。但也有不少粉丝没有因《陈情令》入坑，却为了疾冲迷上肖战。不管如何，《狼殿下》热度颇高，疾冲一角也广受好评这次粉圈大神再……
### [海贼王：索隆已经超越“超新星”，当之无愧成为“皇副级”](https://new.qq.com/omn/20201201/20201201A07CZ800.html)
> 概要: 导读：海贼王最新997话的汉化内容已经更新了，相信很多海迷也已经看过了。这一话最亮眼的表现无疑就是索隆，秒杀阿普拿到了“冰鬼”的抗体，成为了这一话中当之无愧的MVP。这一场战斗意味着什么？索隆已经完……
### [“仙鹤罗裙，樱草遍地，梦中之人何时来？”——LOLITA国风洛丽塔](https://new.qq.com/omn/20201201/20201201A069I100.html)
> 概要: 出镜：sc_名无_今天喝奶茶了吗
### [模玩前线：全比例高达开展！乐高元旦新品！海贼王新手办！](https://new.qq.com/omn/20201201/20201201A0ADWE00.html)
> 概要: 一、“横滨高达工厂”1:1全比例可动高达将于12月19日开放！等了那么久，全比例高达终于来了！昨天，位于横滨市山下码头的“横滨高达工厂”，制作了好几年的18米高全比例可动高达公开向媒体亮相。在开放日……
### [《火影忍者》为什么有人认为奇拉比打不过鬼鲛？](https://new.qq.com/omn/20201201/20201201A06AUA00.html)
> 概要: 《火影忍者》中很多战斗都不是公平的，这动漫里情报很重要，如果对方知道你底细，那么他打你就占据巨大的优势，奇拉比与佐助交手过，佐助回晓组织后必然交代了经过，那么鬼鲛对上奇拉比就对奇拉比有所了解了。鬼鲛……
### [斗罗大陆：里面的他们杀青了？余生不会再出现，独孤博也在所难免](https://new.qq.com/omn/20201201/20201201A09PZH00.html)
> 概要: 斗罗大陆终于迎来了小舞献祭，而唐三也即将踏上孤独的历练之旅。之前很多出现在他人生中的角色也将一一下线，就连老怪物独孤博也没有登场的机会了。相信熟悉斗罗大陆的朋友都知道，独孤博和唐三是非常要好的忘年之……
### [《崖上的波妞》内地定档12月31日 宫崎骏送上亲笔信](https://acg.gamersky.com/news/202012/1342254.shtml)
> 概要: 宫崎骏经典动画电影《崖上的波妞》(又译《悬崖上的金鱼姬》)中国内地定档12月31日。
# 财经
### [“真假中管院”：一个国家级事业单位的8年停顿与乱战](https://finance.sina.com.cn/china/gncj/2020-12-01/doc-iiznezxs4687764.shtml)
> 概要: 中国经营报《等深线》记者 张锦 北京报道 67岁的李树林，已经当选本单位的“临时负责人”3年了，但他还没见过自家单位的公章。 作为“中字头”自收自支的事业单位...
### [上海松江2.27万平米集体土地挂牌 经所属农民同意后出让](https://finance.sina.com.cn/roll/2020-12-01/doc-iiznctke4270093.shtml)
> 概要: 原标题：上海松江2.27万平米集体土地挂牌，经所属农民同意后出让 据上海土地市场消息，12月1日，上海市挂牌一宗位于松江区的集体土地...
### [跟着王兴去炒股：三季度美团炒股赚了58个亿](https://finance.sina.com.cn/chanjing/gsnews/2020-12-01/doc-iiznctke4269674.shtml)
> 概要: 来源：远川商业评论 作者：姚书恒 出品：远川研究所消费组 昨天下午，美团发布了第三季度财报。 在这个季度里，美团收入达到354亿，同比增长28.8%；其中外卖贡献了206...
### [王石：我的改变，我们这群人的改变](https://finance.sina.com.cn/china/gncj/2020-12-01/doc-iiznezxs4686594.shtml)
> 概要: 来源：回归未来工作室 Introduction： “我为对抗疫情已经准备了十几年，因为我能未卜先知？当然不是。” “12年后，再说说‘让灵魂跟上脚步’。
### [雪地策马走红 新疆伊犁昭苏县女副县长发文“冷静”](https://finance.sina.com.cn/china/dfjj/2020-12-01/doc-iiznctke4265123.shtml)
> 概要: 澎湃新闻首席记者 岳怀让 近日，新疆伊犁哈萨克自治州昭苏县副县长贺娇龙策马雪原，为昭苏县旅游做宣传的短视频走红网络平台、圈粉无数。
### [网信办就App必要个人信息范围公开征求意见](https://finance.sina.com.cn/china/bwdt/2020-12-01/doc-iiznctke4269229.shtml)
> 概要: 近年来，App超范围收集、强制收集用户个人信息普遍存在，用户拒绝同意就无法安装使用。为保障公民个人信息安全，国家互联网信息办公室研究起草了《常见类型移动互联网应用...
# 科技
### [本地存储，三兄弟的异同（localStorage、sessionStorage、cookie）](https://segmentfault.com/a/1190000038337113)
> 概要: 答应我，十二月不准垂头丧气哦，显矮共同点localStorage和sessionStorage和cookie共同点同域（同源策略）限制：同源策略：请求与响应的 协议、域名、端口都相同 则时同源，否则为......
### [Vue.js 桌面端虚拟滚动条|vue美化滚动条VScroll](https://segmentfault.com/a/1190000038336471)
> 概要: 介绍VScroll一款基于vue2.x构建的桌面PC端自定义模拟滚动条组件。支持自定义是否原生滚动条、自动隐藏、滚动条大小、层级及颜色等功能。拥有丝滑般的原生滚动条体验！除了垂直滚动条，同样的也支持水......
### [国产无人机鸿雁（HY100）获得全国首个大型民用无人机 “准生证” ：最大航程 1560 公里](https://www.ithome.com/0/522/412.htm)
> 概要: IT之家12月1日消息 据工信部网站发布，11 月 27 日，四川省天域航通科技有限公司生产的鸿雁（HY100）无人机获得中国民用航空局颁发的无人机系统设计生产批准函，是我国首个通过适航审定的大型无人......
### [联咏科技将从明年下半年开始为苹果 iPad 设备提供 LCD 驱动芯片](https://www.ithome.com/0/522/387.htm)
> 概要: 12 月 1 日消息，据国外媒体报道，业内消息称，芯片设计厂商、华为供应商联咏科技已经与苹果公司签署了一项协议，将允许该公司从 2021 年下半年开始为 iPad 设备提供 LCD 驱动芯片。联咏科技......
### [Show HN: Wisdom-of-crowds survey tool that finds hidden consensus at scale](http://opinionx.co)
> 概要: Show HN: Wisdom-of-crowds survey tool that finds hidden consensus at scale
### [Write your own arbitrary-precision JavaScript math library](https://jrsinclair.com/articles/2020/sick-of-the-jokes-write-your-own-arbitrary-precision-javascript-math-library/)
> 概要: Write your own arbitrary-precision JavaScript math library
### [他不在江湖，但江湖处处都有他](https://www.huxiu.com/article/396745.html)
> 概要: 出品｜虎嗅科技组作者｜宇多田头图｜视觉中国在与陆奇博士交流的这段极短时间内，他极力避免把话题引向自己。在北京海淀的一个Wework开放办公空间里，他穿着印有微软logo的旧T恤，针对我们提出的每一个问......
### [阿里充值十荟团，社区团购论持久战](https://www.huxiu.com/article/397172.html)
> 概要: 出品｜虎嗅大商业组作者｜房煜  虎嗅主笔题图｜IC photo在当下火热的社区团购市场有个说法，无论新三团（美团、滴滴、拼多多）和兴盛优选谁胜出，最大的赢家都是腾讯。一方面是腾讯直接投资了兴盛优选和拼......
### [三季度亏损8640万元，嘉楠科技为何把自己"挖"出了血？](https://www.tuicool.com/articles/6R3ANfz)
> 概要: 图片来源@视觉中国文 | 美股研究社美东时间11月30日美股盘前，嘉楠科技发布了截至2020年9月30日的2020财年第三季度的财务报告。营收同比大跳水，净利润实现扭盈为亏，导致财报发布后，盘后股价下......
### [辛巴赔款6189万后 燕窝从业者一句话道出幕后真相](https://www.tuicool.com/articles/mY3QnaR)
> 概要: 文丨铅笔道记者 希言一场直播彻底撕碎了燕窝行业的遮羞布。这次，燕窝真的成功走进普通消费者了，只不过靠的是恶名。近日，主播辛巴回应双十一直播中的“燕窝成分每碗不足2克”事件：直播销售的所有燕窝退一赔三，......
# 小说
### [山上有道](http://book.zongheng.com/book/1002025.html)
> 作者：是狗子啊

> 标签：武侠仙侠

> 简介：魔族的号角从天外发起！为求人间的生机，拳仙于乱世中苏醒！剑仙的血在九重天洒落，大能纷纷以命殉道！老祖亦在天外喋血！人间需要一把剑！（本书前期有点慢热）

> 章节末：第二百九十五章：新的征途

> 状态：完本
# 游戏
### [志村健主演《电影之神》最新预告 21年4月16日上映](https://www.3dmgame.com/news/202012/3803147.html)
> 概要: 松竹映画公司成立100周年献礼电影《电影之神》官方宣布将于2021年4月16日上映，12月1日今天官方公开了首次预告，影片实际影像首次公开，一起来先睹为快。·《电影之神》改编自原田マハ的小说，是松竹映......
### [一周大师级Cos美图欣赏 小姐姐们为何如此性感呢](https://www.3dmgame.com/bagua/3999.html)
> 概要: 又到了欣赏每周大师级Cos合集时间，此次包含《英雄联盟》《最终幻想》《尼尔：机械纪元》《赛博朋克2077》等游戏动漫Cos作品，甚至还有娘化PS5。众多Coser完美还原了人物角色，一起来欣赏下美图吧......
### [《桃太郎地铁》的乐趣 玩家热议通过《桃铁》学地理](https://www.3dmgame.com/news/202012/3803182.html)
> 概要: 也许是近期游戏大作不多，科乐美的经典续作《桃太郎地铁》意外热销，连续数周霸占日本游戏销量榜，近日玩家讨论关于《桃太郎地铁》乐趣之一，可通过《桃铁》学习地理。·《桃太郎地铁》是日本一款国民级的大富翁游戏......
### [3DM速报：部分PS5机身出现黑斑，NS扫码传图](https://www.3dmgame.com/news/202012/3803196.html)
> 概要: 欢迎来到今日的三大妈速报三分钟带你了解游戏业最新资讯大家好，我是米瑟部分玩家的PS5机身出现黑斑，有的擦都擦不掉；《渡神纪：芬尼斯崛起》媒体评测解禁，IGN、GameSpot打出7分；Switch系统......
### [大闹人间！奇葩新游《滑板猪模拟器》上架Steam](https://www.3dmgame.com/news/202012/3803170.html)
> 概要: 由Freaky Games开发的沙雕模拟游戏《滑板猪模拟器》近日已经上架Steam商城页面，玩家将在游戏中操控一头猪跳上滑板来制造混乱，发售时间未定，目前游戏暂不支持中文。官方宣传片：Steam商城页......
# 论文
### [Knowledge Distillation for Brain Tumor Segmentation](https://paperswithcode.com/paper/knowledge-distillation-for-brain-tumor)
> 日期：10 Feb 2020

> 标签：BRAIN TUMOR SEGMENTATION

> 代码：https://github.com/lachinov/brats2019

> 描述：The segmentation of brain tumors in multimodal MRIs is one of the most challenging tasks in medical image analysis. The recent state of the art algorithms solving this task is based on machine learning approaches and deep learning in particular.
### [CardioLearn: A Cloud Deep Learning Service for Cardiac Disease Detection from Electrocardiogram](https://paperswithcode.com/paper/cardiolearn-a-cloud-deep-learning-service-for)
> 日期：4 Jul 2020

> 标签：None

> 代码：https://github.com/hsd1503/CardioLearn

> 描述：Electrocardiogram (ECG) is one of the most convenient and non-invasive tools for monitoring peoples' heart condition, which can use for diagnosing a wide range of heart diseases, including Cardiac Arrhythmia, Acute Coronary Syndrome, et al. However, traditional ECG disease detection models show substantial rates of misdiagnosis due to the limitations of the abilities of extracted features.
