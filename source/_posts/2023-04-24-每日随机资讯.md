---
title: 2023-04-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FranconianWineCellar_ZH-CN8234719750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-04-24 22:39:25
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FranconianWineCellar_ZH-CN8234719750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [抖音食品账号销量10万+，掌握这8个技巧](https://www.woshipm.com/operate/5813467.html)
> 概要: 本文主要简述了抖音视频类直播销售的成功技巧，从选品到账号无一不体现着作者深谙与直播带货行业的火爆手法。站在直播带货的风口，我们是否能屹立不倒？快来看看吧。在抖音卖食品，特别容易爆单。很多人眼红想做但是......
### [研究真实的年轻人，而不是想象中的“年轻人”](https://www.woshipm.com/user-research/5812760.html)
> 概要: 前段时间，《灌篮高手》大电影在内地上映。之前有很多说法说这个电影已经没有了市场，票房不会很高。但这几天的数据狠狠打了这些人一脸。其实分析都是对的，但论据站不住脚——毕竟都没有调研，怎么保证自己所相信的......
### [十年经验谈：为外国品牌起中文名那些事](https://www.woshipm.com/marketing/5812649.html)
> 概要: 中国文化和消费趋势剧烈变迁，本土品牌从弱到强，品牌营销花样层出不穷。本土品牌意识到品牌引流问题，利用品牌名引流，与此同时，外国品牌的名称也越发注重个性，喊出自己最独特的宣言。本文旨在发现外国品牌中文命......
### [古惑狼多人游戏《Crash Team Rumble》介绍视频](https://www.3dmgame.com/news/202304/3867766.html)
> 概要: 发行商动视和开发商Toys for Bob发布了四对四团队在线多人游戏《Crash Team Rumble》的介绍视频。虽然没有特别说明，但“Toys for Bob”的一条推特暗示，这部动画是由《蜘......
### [粉丝创作《塞尔达传说：王国之泪》8比特主题曲](https://www.3dmgame.com/news/202304/3867768.html)
> 概要: 《塞尔达传说：王国之泪》是任天堂旗下的经典游戏系列《塞尔达传说》的最新作品。该作品的主题曲已经在网络上引起了广泛的关注和讨论，不少粉丝对其赞不绝口。近日，有一位粉丝以8比特风格重新编排了这首主题曲，并......
### [飞鹤家族ip设计-ip品牌升级-飞鹤送子【FEIHE FAMILY】](https://www.zcool.com.cn/work/ZNjUwMDQwMjg=.html)
> 概要: 品牌故事：在“鹤送子”的古典故事中，这个故事中，鹤代表了一个值得信任的信使，送来了夫妻所期盼的宝贝儿女。它也成为人们对于孕育新生命的美好愿望和祝福的象征。在飞鹤牧场中，飞鹤家族是负责对于孕育新生命的爱......
### [苹果MR头显或采用外接电池：磁吸充电，续航2小时](https://finance.sina.com.cn/stock/usstock/c/2023-04-24/doc-imyrmtrv4793235.shtml)
> 概要: 新浪科技讯 北京时间4月24日早间消息，据报道，长期跟踪苹果的专栏作家马克·古尔曼（Mark Guarman）在最新的文章中披露了关于苹果混合现实（MR）头显的最新细节......
### [降价促销也难挡下滑，特斯拉在“大本营”加州份额创2017年来最低值](https://finance.sina.com.cn/stock/usstock/c/2023-04-24/doc-imyrmtrv4807645.shtml)
> 概要: 降价促销也难挡下滑，特斯拉在“大本营”加州份额创2017年来最低值
### [《蜘蛛侠：纵横宇宙》四大宇宙设计图公布](https://www.3dmgame.com/news/202304/3867776.html)
> 概要: 《蜘蛛侠：纵横宇宙》四大宇宙设计图公布，4位蜘蛛侠所在的平行宇宙来了。1610迈尔斯的终极宇宙格温虫的65号宇宙印度蜘蛛侠的50101号宇宙蜘蛛侠2099的928号宇宙剧情简介：讲述了新生代蜘蛛侠迈尔......
### [90所大学撤销这个专业，为什么？](https://www.huxiu.com/article/1292098.html)
> 概要: 本文来自微信公众号：财经杂志 （ID：i～caijing），作者：金贻龙，编辑：朱弢，责编：要琢，题图来源：视觉中国高校学科专业设置将迎来重大改革。教育部等五部委近日发布的《普通高等教育学科专业设置调......
### [《花野》插画合集](https://www.zcool.com.cn/work/ZNjUwMDQ0OTI=.html)
> 概要: Collect......
### [FEIHE FAMILY | 飞鹤家族](https://www.zcool.com.cn/work/ZNjUwMDUyODQ=.html)
> 概要: IP设计 & 插画 ：周卉清 | 周若婷三维建模 ：周若婷 | 李小娟三维动画 ：李文敬......
### [卷卷薯条-餐饮策略视觉设计](https://www.zcool.com.cn/work/ZNjUwMDYyNDg=.html)
> 概要: 出品：言合YeeHoo品牌项目：卷卷薯条-餐饮策略视觉设计地址：昆明-------------------------------------------------------------让普通的品......
### [漫改爱情背德恋，无性婚姻是为何？](https://news.dmzj.com/article/77853.html)
> 概要: 2023年4月13日上线一部漫改电视剧《我们之间没有的》，由奈绪主演，岩田刚典、田中美奈实、永山瑛太共演，根据春乃晴的同名漫画改编，讲述32岁的OL吉野美智与丈夫阳一结婚5年，却两年没有性生活。另一对被大家称赞是理想绝配夫妻的新名诚和新名枫，也有...
### [《银河护卫队3》采用特殊摄影手法 5月5日上映](https://www.3dmgame.com/news/202304/3867790.html)
> 概要: 漫威超级英雄电影《银河护卫队3》即将于5月5日同步北美在全国公映，日前导演詹姆斯·古恩在采访中透露，《银河护卫队3》采用特殊摄影手法，让影片更加充满活力。·作为该系列的终局，《银河护卫队3》将系列的精......
### [省呗用户故事：又一批省斗士正满怀信心从星夜出发](http://www.investorscn.com/2023/04/24/107053/)
> 概要: “走，一起去淄博吃烤串儿”最近人均几十块淄博烧烤刷爆各大社交平台。为“保命”停业三天的烧烤店老板，“像是被炮轰过”的烧炭小哥一次次引发网友关注，随着这股“烧烤热”的持续发酵，#淄博烧烤后厨有多忙#、#......
### [【直播预告】不知道啥时候才能通关空轨](https://news.dmzj.com/article/77854.html)
> 概要: 本周直播预告
### [像素人狼《暗锅人狼》上架Steam 年内登陆Switch](https://www.3dmgame.com/news/202304/3867793.html)
> 概要: 发行商Waku Waku Games日前宣布，旗下一款像素人狼游戏《暗锅人狼》上架Steam，他预定年内登陆Switch/PC平台，本作暂不支持中文。·《暗锅人狼》：Steam地址《暗锅人狼》是一款大......
### [霉霉演唱会上手部受伤 简单包扎后仍坚持表演](https://ent.sina.com.cn/y/youmei/2023-04-24/doc-imyrnkpt2478827.shtml)
> 概要: 新浪娱乐讯 4月23日，The Eras Tour巡演休斯顿第二场，霉霉Taylor Swift在<REPUTATION ERA>表演期间被发现受伤，左手手掌接近腕部磕掉一大块皮，用绷带紧急包扎后仍坚......
### [“二阳”来了？不必过度恐慌，但应做足准备](https://finance.sina.com.cn/jjxw/2023-04-24/doc-imyrnkpp4723739.shtml)
> 概要: 近日，不少网友晒出显示阳性的抗原图片，称自己或家人“二阳”了。“二次感染”的波峰真的来了？又是否会影响“五一”出行？一时间，相关话题再度成为舆论焦点。
### [红牛深耕中国市场30年，创始公司天丝集团持续加码在华投](http://www.investorscn.com/2023/04/24/107054/)
> 概要: 红牛从何而来？上个世纪70年代，天丝集团创始人许书标先生成功研发出红牛饮料，随后，在全球合作伙伴的营销网络下，如今红牛产品已畅销全球170多个国家和地区，成为标志性的能量饮料。1993年，许书标先生在......
### [海外new things | 德国XaaS平台「Equipme」种子轮融资380万美元，由La Famiglia VC领投](https://36kr.com/p/2220862057915394)
> 概要: 海外new things | 德国XaaS平台「Equipme」种子轮融资380万美元，由La Famiglia VC领投-36氪
### [经济到底怎么样？](https://www.yicai.com/news/101739479.html)
> 概要: 二季度经济修复的斜率或有放缓，即经济的景气程度或有一定回落，尤其是地产积累需求集中释放后，会有边际回落，出口仍然面临压力，消费缓慢改善。不过去年基数偏低，预计公布的二季度经济同比数据都会较高，在这样的情况下，大规模政策出台的概率也较低，需要耐心等待。
### [六家公司以华谊兄弟名义招练习生 因侵权被起诉](https://ent.sina.com.cn/2023-04-24/doc-imyrnkpp4734862.shtml)
> 概要: 来源：北京青年报　　新浪娱乐讯 23日，海淀法院公开开庭审理涉“华谊兄弟”商标权及不正当竞争纠纷案。六家公司因在公开宣传中未经许可使用涉案商标，并以“华谊兄弟”的名义招募练习生，被北京华谊兄弟聚星文化......
### [海外new things | 功能性电解质饮料品牌「Cure」A轮融资560万美元，将加快拓展线下销售渠道](https://36kr.com/p/2220862387266817)
> 概要: 海外new things | 功能性电解质饮料品牌「Cure」A轮融资560万美元，将加快拓展线下销售渠道-36氪
### [赵建：淄博烧烤、服务型政府与消费型社会](https://www.yicai.com/news/101739525.html)
> 概要: 从这个意义上，淄博烧烤消费的不仅仅是廉价的平民食物，更多的是消费一种回归自然、自由、自在的氛围。
### [成都失去性价比？](https://www.huxiu.com/article/1299511.html)
> 概要: 本文来自微信公众号：吹火车咯（ID：blowthetrain），作者：喻折，编辑：无荒，图片：文中截图来源均为小红书，原文标题：《西南柬埔寨：在“Boss直拒”里接近一种城市真相》，头图来自：视觉中国......
### [海外new things | 供应链技术初创「Odeko」D轮融资5300万美元，帮助小型咖啡店节约成本、扩大规模](https://36kr.com/p/2220862645118214)
> 概要: 海外new things | 供应链技术初创「Odeko」D轮融资5300万美元，帮助小型咖啡店节约成本、扩大规模-36氪
### [“农管”执法事项达251项，基层人员：执法不能简单粗暴](https://finance.sina.com.cn/china/2023-04-24/doc-imyrnkpp4765340.shtml)
> 概要: “虽然我们是农业农村执法部门，但执法权力有明确范围，有什么授权，干什么事情。” 近期一些有关“农管砍树扒菜”的视频在网络上引发热议，“农管”到底是什么组织...
### [习近平向第四届联合国世界数据论坛致贺信](https://finance.sina.com.cn/china/2023-04-24/doc-imyrnkpt2490622.shtml)
> 概要: 来源：央视新闻客户端 习近平向第四届联合国世界数据论坛致贺信。
### [日剧《恋恋洗衣店》第二季制作决定](https://news.dmzj.com/article/77856.html)
> 概要: 根据椿ゆず、缶爪さわ原作改编的日剧《恋恋洗衣店》宣布将要制作第二季，7月开播。
### [外汇天眼首届模拟大赛获奖名单公布，获奖者公开交易秘诀！](http://www.investorscn.com/2023/04/24/107063/)
> 概要: 自2022年以来,复杂多变的国际政治经济局势,带动了国际金融市场的发展,同样提升了外汇市场的交易热度。为了促进行业发展,降低交易门槛,外汇天眼秉承着“让交易更容易”的理念,于2023年在外汇天眼APP......
### [上万AI涌入互联网“鬼城”，在这里“人类已死”](https://www.huxiu.com/article/1302626.html)
> 概要: 本文来自微信公众号：Founder Park（ID：Founder-Park），作者：创业者帕克，原文标题：《互联网惊现 AI 鬼城，上万 AI 发帖聊天，人类禁止入内，这一天终于来了》，头图来自：电......
### [工信部：一季度电信业务收入累计完成 4252 亿元，5G 用户 6.2 亿户](https://www.ithome.com/0/688/606.htm)
> 概要: IT之家4 月 24 日消息，工信部公布 2023 年第一季度通信业经济运行情况，电信业务收入累计完成 4252 亿元，同比增长 7.7%。按照上年不变价计算的电信业务总量同比增长 18%，增速较 1......
### [今天是中国航天日，让我们以格物致知的精神叩问浩瀚苍穹](https://www.ithome.com/0/688/607.htm)
> 概要: 感谢IT之家网友雨雪载途、软媒新友2037839、刺客的线索投递！IT之家4 月 24 日消息，1970 年 4 月 24 日，中国第一颗人造地球卫星在酒泉卫星发射中心成功发射。中国航天，数不清的日夜......
### [平台经济春风将至，凹凸出行持续释放新动能](http://www.investorscn.com/2023/04/24/107064/)
> 概要: 今年三月，国务院公布的政府工作报告中提出，要“大力发展数字经济，提升常态化监管水平，支持平台经济发展”。多位代表委员表示，数字经济是新兴技术和先进生产力的代表，大力发展数字经济，支持平台经济发展，也是......
### [货物跨境结算上涨近四成，资源贸易扩大人民币朋友圈](https://finance.sina.com.cn/china/2023-04-24/doc-imyrnqvr2366740.shtml)
> 概要: 来源：财经五月花 摘 要 跨境人民币在贸易结算方面取得了一定进展，但在投资、储备货币方面，人民币国际化尚有较长的路要走 文|康恺 编辑|张威袁满...
### [海外new things｜「Elevate」完成2800万美元融资，打造人工智能的福利管理平台](https://36kr.com/p/2223285802468483)
> 概要: 海外new things｜「Elevate」完成2800万美元融资，打造人工智能的福利管理平台-36氪
### [Fairphone 全新 Fairbuds XL 头戴式无线耳机曝光：拥有特别绿色款](https://www.ithome.com/0/688/634.htm)
> 概要: IT之家4 月 24 日消息，荷兰企业 Fairphone 以制造可维修手机而闻名，这些设备得到长期技术支持，并且至少部分由符合道德标准的材料制成。几年前，Fairphone 推出了一款售价 100 ......
### [谷歌 Google Fi 更名升级“Wireless”：支持 Pixel Watch / 三星 Galaxy Watch5 系列手表连接](https://www.ithome.com/0/688/641.htm)
> 概要: IT之家4 月 24 日消息，在 MVNO（IT之家注：mobile virtual network operator，移动虚拟网络运营商）服务八年历史中，Google Fi 迎来了又一次更名。为庆祝......
### [造车“失败者”联盟：4年近30家新势力“消失”，下个十年谁还在场？](https://finance.sina.com.cn/nextauto/2023-04-24/doc-imyrnqvm4757721.shtml)
> 概要: 眼见他高楼起，眼见他宴宾客，眼见他楼塌了。 上海国际车展熙熙攘攘，但喧嚣的背后也有无奈的落寂。历经疫情波折、原材料涨价、国补退潮、价格战，有人依旧站在舞台中央...
### [丁薛祥：强化数字创新应用 加快推进数字化转型](https://www.yicai.com/news/101739782.html)
> 概要: 强化数字创新应用，加快推进数字化转型，加强新一代数字技术协同创新，推动数字技术融入经济社会发展各领域和全过程。
### [工信部：一季度我国软件业务收入2.44万亿元 同比增长13.5%](https://www.yicai.com/news/101739788.html)
> 概要: 一季度，我国软件和信息技术服务业运行态势持续向好，软件业务收入增长加快，利润总额保持两位数增长，软件业务出口增速小幅回落。
### [荣耀深圳研发实验室首次开放：高端旗舰背后 不为人知的基础研究](https://finance.sina.com.cn/tech/mobile/n/n/2023-04-24/doc-imyrnvcp2248063.shtml)
> 概要: 多年前，有人说手机厂商所谓黑科技“都是供应商供的”。本以为除了华为，手机圈里不可能还有那么大规模的实验室，直到我们摸进了这几栋楼，看到高端旗舰背后不为人知的基础研究......
### [杉田智和原作的有声剧企划《真乡街》PV公开](https://news.dmzj.com/article/77859.html)
> 概要: 杉田智和原作·脚本的有声剧企划《真乡街》锐意制作中，设计视觉图和PV公开。
### [福耀集团：董事长曹德旺从未发表过关于特斯拉薪酬的任何言论](https://www.yicai.com/news/101739821.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [沈腾评价乔杉马丽演夫妻：搭得好但是没有那么顺](https://ent.sina.com.cn/m/c/2023-04-24/doc-imyrnzmr4054509.shtml)
> 概要: 新浪娱乐讯 24日，电影《人生路不熟》首映，沈腾观影后分享感受，表示看片中途好几次笑出声了，“尤其是乔杉和我搭档马丽的表演，确实啊，你俩在一起搭都挺好，就感觉没有那么顺。”......
### [蓝思科技一季度营收同比增5.41%，营业成本较上年同期微降](http://www.investorscn.com/2023/04/24/107067/)
> 概要: 蓝思科技4月24日晚间发布一季度业绩公告。数据显示，2023年第一季度营收约98.39亿元，较上年同期增5.41%；归属于上市公司股东的净利润约6454.86万元，较上年同期增115.69%；营业成本......
# 小说
### [我的精灵室友](https://book.zongheng.com/book/1105314.html)
> 作者：神秘熊猫人

> 标签：都市娱乐

> 简介：长耳精灵为拯救族人,竟屈身来到男生寝室?!小西,都是大老爷们儿,你怎么还穿睡衣啊?小西,今天快没热水了,你不来跟我们一起洗吗?阿西,你这腿,不去当妹子简直可惜了...........背负着全族的命运,长耳精灵与她的室友们究竟会摩擦出什么样的火花?

> 章节末：第163章 混沌帝（终章）

> 状态：完本
# 论文
### [Attribute Surrogates Learning and Spectral Tokens Pooling in Transformers for Few-shot Learning | Papers With Code](https://paperswithcode.com/paper/attribute-surrogates-learning-and-spectral)
> 概要: This paper presents new hierarchically cascaded transformers that can improve data efficiency through attribute surrogates learning and spectral tokens pooling. Vision transformers have recently been thought of as a promising alternative to convolutional neural networks for visual recognition. But when there is no sufficient data, it gets stuck in overfitting and shows inferior performance. To improve data efficiency, we propose hierarchically cascaded transformers that exploit intrinsic image structures through spectral tokens pooling and optimize the learnable parameters through latent attribute surrogates. The intrinsic image structure is utilized to reduce the ambiguity between foreground content and background noise by spectral tokens pooling. And the attribute surrogate learning scheme is designed to benefit from the rich visual information in image-label pairs instead of simple visual concepts assigned by their labels. Our Hierarchically Cascaded Transformers, called HCTransformers, is built upon a self-supervised learning framework DINO and is tested on several popular few-shot learning benchmarks. In the inductive setting, HCTransformers surpass the DINO baseline by a large margin of 9.7% 5-way 1-shot accuracy and 9.17% 5-way 5-shot accuracy on miniImageNet, which demonstrates HCTransformers are efficient to extract discriminative features. Also, HCTransformers show clear advantages over SOTA few-shot classification methods in both 5-way 1-shot and 5-way 5-shot settings on four popular benchmark datasets, including miniImageNet, tieredImageNet, FC100, and CIFAR-FS. The trained weights and codes are available at https://github.com/StomachCold/HCTransformers.
### [Learning idempotent representation for subspace clustering | Papers With Code](https://paperswithcode.com/paper/learning-idempotent-representation-for)
> 日期：29 Jul 2022

> 标签：None

> 代码：https://github.com/weilyshmtu/learning-idempotent-representation-for-subspace-segmentation

> 描述：The critical point for the successes of spectral-type subspace clustering algorithms is to seek reconstruction coefficient matrices which can faithfully reveal the subspace structures of data sets. An ideal reconstruction coefficient matrix should have two properties: 1) it is block diagonal with each block indicating a subspace; 2) each block is fully connected. Though there are various spectral-type subspace clustering algorithms have been proposed, some defects still exist in the reconstruction coefficient matrices constructed by these algorithms. We find that a normalized membership matrix naturally satisfies the above two conditions. Therefore, in this paper, we devise an idempotent representation (IDR) algorithm to pursue reconstruction coefficient matrices approximating normalized membership matrices. IDR designs a new idempotent constraint for reconstruction coefficient matrices. And by combining the doubly stochastic constraints, the coefficient matrices which are closed to normalized membership matrices could be directly achieved. We present the optimization algorithm for solving IDR problem and analyze its computation burden as well as convergence. The comparisons between IDR and related algorithms show the superiority of IDR. Plentiful experiments conducted on both synthetic and real world datasets prove that IDR is an effective and efficient subspace clustering algorithm.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
