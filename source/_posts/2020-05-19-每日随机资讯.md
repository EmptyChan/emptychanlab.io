---
title: 2020-05-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RoaringFork_EN-CN3660327852_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-19 21:15:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RoaringFork_EN-CN3660327852_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：有心！陈志朋49岁生日好友P图《夫妻的世界》主演送祝福](http://slide.ent.sina.com.cn/star/w/slide_4_704_338963.html)
> 概要: 组图：有心！陈志朋49岁生日好友P图《夫妻的世界》主演送祝福
### [大写的搞笑！吴青峰为粉丝取名善心人士画风跑偏](https://ent.sina.com.cn/y/ygangtai/2020-05-19/doc-iircuyvi3852272.shtml)
> 概要: 新浪娱乐讯 5月18日，吴青峰更新动态与粉丝互动，发文：“来喔~每周一聊~” 随后有粉丝在微博下留言询求青峰给粉丝统一一下称呼，并列举了如“吹峰机、啵啵鱼、啵啵鲨、倾听者、聆听者、听众”等诸多称呼供他......
### [视频：助理晒王雨馨自杀诊断书 此前曾指黄景瑜劈腿家暴](https://video.sina.com.cn/p/ent/2020-05-19/detail-iirczymk2330981.d.html)
> 概要: 视频：助理晒王雨馨自杀诊断书 此前曾指黄景瑜劈腿家暴
### [拳王泰森重回微博问候网友 透露或将再次站上擂台](https://ent.sina.com.cn/s/u/2020-05-19/doc-iircuyvi3901845.shtml)
> 概要: 新浪娱乐讯 5月19日，拳王泰森重回微博，他分享了一段问候视频并发文：“Weibo， I‘m back！” 视频中泰森赤半身站在自家院子里，透露自己有可能复出重回擂台，他还向大家展示了一套帅气的拳法，......
### [视频：《密室大逃脱2》定档阵容曝光 杨幂邓伦黄明昊再集结](https://video.sina.com.cn/p/ent/2020-05-19/detail-iirczymk2400535.d.html)
> 概要: 视频：《密室大逃脱2》定档阵容曝光 杨幂邓伦黄明昊再集结
### [哈林当街扮猴逗得小14岁妻子花枝乱颤，二人两年抱俩仍似热恋小情侣](https://new.qq.com/omn/20200519/20200519A03T0000.html)
> 概要: 58岁哈林四年前再婚，很快晋升人生胜利组，老婆贤惠漂亮，两年内生下一子一女凑成“好”字。            为了与小14岁太太张嘉欣匹配，哈林每日坚持锻炼，体力还是跟年轻一样出色，加之有又一双年幼......
### [孙俪19岁参加选秀，青涩颜值令人惊叹，刘德华作为评委很看好她](https://new.qq.com/omn/20200519/20200519V038MV00.html)
> 概要: 孙俪19岁参加选秀，青涩颜值令人惊叹，刘德华作为评委很看好她
### [吴佩慈替男友纪晓波澄清欠租百万传闻，因员工忘存支票导致误会](https://new.qq.com/omn/20200519/20200519A0NOMP00.html)
> 概要: 去年7月为富商纪晓波产下三个孩子仍然未能嫁入豪门的女星吴佩慈，再度宣布怀上四胎。让许多网友感到非常震惊和不理解，当时吴佩慈对于外界质疑，吴回应称：“生孩子的痛和养孩子的累比起来完全可以忽略不计，但再比......
### [娱乐图鉴丨娱乐圈神秘的职业：开挖掘机、钻狗洞，一个视频赚上万](https://new.qq.com/omn/ENT20200/ENT2020051900367500.html)
> 概要: 娱乐图鉴丨娱乐圈神秘的职业：开挖掘机、钻狗洞，一个视频赚上万
### [《极限挑战》张艺兴退出，“男人帮”惨遭改名，网友：没内味了](https://new.qq.com/omn/20200519/20200519A071JK00.html)
> 概要: 《极限挑战》张艺兴退出，“男人帮”惨遭改名，网友：没内味了
# 动漫
### [漫画《黄金拼图》特别篇“Best wishes.”开始连载](https://news.dmzj.com/article/67418.html)
> 概要: 由原悠衣创作的新漫画《黄金拼图Best wishes.》在本日（19日）发售的Manga Time Kirara MAX 7月号（芳文社）上开始连载。
### [《海贼王》联动钓具品牌！今后将推出更多户外周边](https://news.dmzj.com/article/67420.html)
> 概要: 根据尾田荣一郎原作制作的动画《海贼王》宣布了将与钓具品牌DRESS联动，从6月中旬开始，在日本钓具店、DRESS官网和各再现网站发售。作为第一弹联动，这次推出的是收纳钓具和鱼饵等物品的钓鱼箱和钓鱼箱的配套商品。
### [甜到在柠檬山上劈叉的爱情喜剧](https://news.dmzj.com/article/67415.html)
> 概要: 这期给大家推荐爆甜少女爱情漫画《一条花恋的诱惑》，作者藤谷阳子，Freedom汉化组汉化，讲述喜欢强硬而又粗鲁的青梅竹马，俩人17岁要一起踏上大人的阶梯的约定，可两个人独处的话，还是有点紧张，虽然在大家面前是好胜的女王，但在喜欢的人面前就是个可爱...
### [文春：室屋与多名女性有恋爱关系 近期又约茅原去泡温泉](https://news.dmzj.com/article/67416.html)
> 概要: 在之前曾为大家介绍过声优茅原实里和已婚男性室屋光一郎交往的消息。就在昨晚，日本八卦媒体文春在线也对此事进行了更进一步的报道。
### [日媒投票，JUMP史上最强的好女人排行榜，博雅汉库克成功登顶](https://new.qq.com/omn/20200518/20200518A0CY1S00.html)
> 概要: 在人气漫画《周刊少年JUMP》中，出现了不少非常有人气的女性角色，从可爱到漂亮到帅气，类型各种各样。为此，近日日媒gooranking针对jump史上最强的“好女人”到底是谁进行了问卷调查，并进行了……
### [精灵宝可梦：小智再次回到卡洛斯地区，甲贺忍蛙回归团队](https://new.qq.com/omn/20200519/20200519A05WFW00.html)
> 概要: 精灵宝可梦是一部非常有趣的动漫，给我们构造了一个人和宝可梦和平共处的世界，其中小智已经旅行了20多年的时间，碰到了无数的精灵，并且也放生了很多精灵，人称精灵放生大师。在小智放生的精灵中，最可惜的不是……
### [海贼王：难怪海军不敢动凯多团，他们之间的体系相差一个大将级别](https://new.qq.com/omn/20200519/20200519A0LPQ600.html)
> 概要: 在海贼世界中，凯多是绝对的霸主，实力如同怪物般恐怖强悍，拥有超乎想象的防御力和生命力，他曾被海军抓捕过，海军对他动刑，他都毫发无损，最后还逃跑了。后来他成立了百兽团，并且霸占了和之国的鬼岛，和将军狼……
### [一人之下：老孟为何请求放了陈朵？蛊身圣童来由曝光，身世太可怜](https://new.qq.com/omn/20200519/20200519A04YKS00.html)
> 概要: 导读：陈朵篇一直都是《一人之下》中，非常沉重的一个篇章。每个人看完，都会有不同的感受。可你们真的了解陈朵的过去吗？临时工集结的目的，便是干掉陈朵，可为何老孟要求大家放了陈朵呢？一起来看看吧～在异人……
### [龙珠：我们应该如何评价雅木茶这个人物，难道他真的不配作为武道家吗](https://new.qq.com/omn/20200519/20200519A04RB200.html)
> 概要: 如果你是老龙珠粉，那么对于雅木茶这个角色一定是印象深刻的，虽然现在关于雅木茶的戏份是不多了，但是雅木茶绝对是一位元老级的角色，并且这个人物，可以说是从高峰跌落到低谷的人物。为什么要这么说呢？只因他初……
### [全世界喜欢初音的人很多，这3位堪称佼佼者，一位美女一位和尚](https://new.qq.com/omn/20200519/20200519A05UAV00.html)
> 概要: 作为一位世界性的虚拟歌姬，初音未来在全世界都有着极高的人气，粉丝也是相当多的，无论大家的贫穷贵贱，大家都有着同一个爱好，都喜欢着初音未来，可以说我们每一个粉丝，都是公主殿下的宝贵财富，也是公司的钱包……
# 财经
### [中央再推西部大开发 财税、金融、用地多项政策支持基建](https://finance.sina.com.cn/china/gncj/2020-05-19/doc-iircuyvi3956367.shtml)
> 概要: 原标题：中央再推西部大开发，财税、金融、用地多项政策支持基建 近日，新华社受权发布《中共中央 国务院关于新时代推进西部大开发形成新格局的指导意见》。
### [海南前4月水产品出口逆势增长10.2% 出口水产品4.8万吨](https://finance.sina.com.cn/china/gncj/2020-05-19/doc-iircuyvi3956882.shtml)
> 概要: 原标题：海南前4月水产品出口逆势增长10.2% 出口水产品4.8万吨 记者从海口海关了解到，今年1至4月，海南省出口水产品4.8万吨、货值9.9亿元，同比增长10.2%和5%。
### [工信部副部长陈肇雄任中国电子科技集团董事长、党组书记](https://finance.sina.com.cn/china/gncj/2020-05-19/doc-iircuyvi3957558.shtml)
> 概要: 原标题：工信部副部长陈肇雄任中国电子科技集团董事长、党组书记 5月19日下午，中国电子科技集团有限公司召开中层以上管理人员大会。
### [山西：住宿服务单位不得接待来自中、高风险地区人员](https://finance.sina.com.cn/china/gncj/2020-05-19/doc-iircuyvi3960373.shtml)
> 概要: 原标题：山西：住宿服务单位不得接待来自中、高风险地区人员 新京报快讯据山西省卫生健康委员会网站消息，5月19日，山西省新冠肺炎疫情防控工作领导小组办公室发布关于调整...
### [2020，世界期待来自两会的“中国答案”](https://finance.sina.com.cn/china/gncj/2020-05-19/doc-iirczymk2489681.shtml)
> 概要: 原标题：特稿：2020，世界期待来自两会的“中国答案” 新华社北京5月19日电 特稿：2020，世界期待来自两会的“中国答案” 新华社记者商婧 中国两会即将开幕，世界瞩目。
### [香港失业率急升至逾10年新高 386万打工仔生计告急](https://finance.sina.com.cn/roll/2020-05-19/doc-iirczymk2486958.shtml)
> 概要: 原标题：386万打工仔生计告急！香港失业率急升至逾10年新高 来源：21世纪经济报道 受疫情影响，香港失业率连升7个月，升至逾10年高位。
# 科技
### [Flask简单的和可扩展的管理界面框架](https://www.ctolib.com/flask-admin.html)
> 概要: Flask简单的和可扩展的管理界面框架
### [AR Copy Paste - Text Proto](https://www.ctolib.com/cyrildiagne-ar-cptext.html)
> 概要: AR Copy Paste - Text Proto
### [基于SSM的健身房管理系统](https://www.ctolib.com/CPG123456-GymMS.html)
> 概要: 基于SSM的健身房管理系统
### [从XD生成资产以在现有Flutter项目中使用](https://www.ctolib.com/AdobeXD-xd-to-flutter-plugin.html)
> 概要: 从XD生成资产以在现有Flutter项目中使用
### [周鸿祎全国两会提四份提案 聚焦新基建网络安全](https://www.tuicool.com/articles/iYfIjqi)
> 概要: 2020年全国两会在即。今年，全国政协委员、360集团董事长兼CEaO周鸿祎将向两会提交四份提案，聚焦新基建的网络安全。“网络安全是我的‘老话题’，但是随着国家新基建战略的提出和实施，今年又有了不少‘......
### [Creating files in JavaScript in your browser](https://www.tuicool.com/articles/A7JFzi6)
> 概要: Did you know you can create files using JavaScript right inside your browser and have users download......
### [高效学习方法—费曼学习技巧](https://www.tuicool.com/articles/RVf2Q32)
> 概要: 编者按：本文来自微信公众号“咏舍”（ID:yongshe01），作者：咏舍，36氪经授权发布。什么是费曼学习？最近看到知乎上分享40个思维方式，其中学习力模块中就有费曼学习。简单的粗看了一下，我理解为......
### [百度的“反击”：一季度营收 255 亿元，净利润增长 219 %，AI 新业务“钱景”向好](https://www.tuicool.com/articles/rA3q6rb)
> 概要: 对于百度而言，2020 年并不是轻松的一年。受疫情影响，互联网公司都在艰难渡劫，百度也不例外。4 月初，百度 App 因多个频道存在严重违规问题，被网信办约谈。4 月 8 日，百度宣布停止更新客户端部......
### [宏碁推出 C250i 便携投影仪：配备自动屏幕旋转技术](https://www.ithome.com/0/488/064.htm)
> 概要: IT之家5月19日消息 根据网友投递线索，宏碁 C250i 便携投影仪已在京东平台上架，售价2999元，6月1日京东首发。IT之家了解到，C250i投影机最早于2019年9月4号在柏林的Acer产品发......
### [三星连续14年称霸全球电视市场，今年一季度份额再创历史新高](https://www.ithome.com/0/488/019.htm)
> 概要: IT之家5月19日消息 三星不仅在智能手机行业连续占据高市场份额，其在电视市场的份额也连续14年称霸。根据统计机构Omdia的最新报告，2020年第一季度，三星在全球电视市场份额再创新高，占比32.4......
### [【IT之家开箱】，iQOO Z1 5G开箱图赏：北斗天玑，银色星河](https://www.ithome.com/0/488/018.htm)
> 概要: IT之家5月19日消息 今日下午，iQOO Z1 5G手机正式发布，首发搭载天玑1000 Plus旗舰芯片，采用6.57英寸144Hz刷新率FHD+显示屏，拥有太空蓝、星河银与幻彩流星三种配色。IT之......
### [传音控股：非洲人口保持较快增长，功能机正向智能手机切换](https://www.ithome.com/0/488/037.htm)
> 概要: 5月19日，深圳传音控股股份有限公司举行2019年度业绩说明会，会上传音控股董事长、总经理 竺兆江表示，非洲手机市场人口红利仍在，市场需求增长广阔。他表示，非洲手机市场具有三方面特点：第一、非洲拥有庞......
# 小说
### [还珠格格第二部之风云再起](http://book.zongheng.com/book/696850.html)
> 作者：琼瑶

> 标签：历史军事

> 简介：乾隆二十五年，新疆王将掌上明珠含香献给乾隆皇帝。小燕子、紫薇、永琪、尔康得知含香和心上人蒙丹的相恋故事，竟路见不平，决定要助有情人终成眷属，甚至把蒙丹偷带入宫……

> 章节末：21

> 状态：完本
### [全能召唤师系统](http://book.zongheng.com/book/879620.html)
> 作者：高山苦竹林

> 标签：奇幻玄幻

> 简介：看获得金手指的秦一，如何一步步在异界大陆建立起属于自己的顶级势力！

> 章节末：第520章 结束也是开始大结局

> 状态：完本
# 游戏
### [开发商：PS5手柄触觉反馈能让你感受游戏中的天气](https://www.3dmgame.com/news/202005/3788876.html)
> 概要: 近年来，游戏手柄的触觉反馈变得越来越重要，显然PS5的DualSense手柄也不例外。而独立益智游戏《托马斯孤独一人》的开发者Mike Bithel就表示，DualSense触觉反馈技术甚至可以让你在......
### [纸壳高玩打造纸壳版回转手里剑手枪 合法持枪乐趣多](https://www.3dmgame.com/news/202005/3788893.html)
> 概要: 手工高玩们的世界总是很精彩，而纸壳版手工在日本也已经有了自己的玩家群体，近日又有高玩展示了自己制作的纸壳版回转手里剑手枪，精致做工加上实用射击引发网友惊叹，一起来开开眼。·这名叫@ponz_cr的网友......
### [WiiU模拟器CEMU新版演示 完美运行《荒野之息》](https://www.3dmgame.com/news/202005/3788850.html)
> 概要: CEMU是目前PC平台上最棒的Wii U模拟器，近日CEMU团队公布了CEMU新版本1.19，该版本将于本周发布。1.19版加入了Vulkan下异步着色器和管线编译的选项，提升了游戏运行表现，修复了V......
### [漫威游戏副总：想做一款《超胆侠》游戏 但不是现在](https://www.3dmgame.com/news/202005/3788902.html)
> 概要: 之前，我们曾经报道过《战神4》制作人Cory Barlog回答了“你最想做的超级英雄游戏是哪一款？”，他表示很想做“鹰眼”的游戏。而作为漫威游戏副总监的Bill Rosemann也回答了这个问题，他表......
### [CDPR庆贺《巫师3》发售五周年！还赠送纪念壁纸](https://www.3dmgame.com/news/202005/3788897.html)
> 概要: 今日（5月19日），CDPR官方在微博上和推特上为《巫师3》发售五周年庆贺，官方还为玩家们准备了手机、电脑周年纪念壁纸，以下为此次官方公开的相关细节。官博截图：官推截图：CDPR官方在推特上表示：“今......
# 论文
### [Cross-Representation Transferability of Adversarial Perturbations: From Spectrograms to Audio Waveforms](https://paperswithcode.com/paper/cross-representation-transferability-of)
> 日期：22 Oct 2019

> 标签：

> 代码：https://github.com/karlmiko/icassp2020

> 描述：This paper shows the susceptibility of spectrogram-based audio classifiers to adversarial attacks and the transferability of such attacks to audio waveforms. Some commonly adversarial attacks to images have been applied to Mel-frequency and short-time Fourier transform spectrograms and such perturbed spectrograms are able to fool a 2D convolutional neural network (CNN) for music genre classification with a high fooling rate and high confidence.
### [Would Mega-scale Datasets Further Enhance Spatiotemporal 3D CNNs?](https://paperswithcode.com/paper/would-mega-scale-datasets-further-enhance)
> 日期：10 Apr 2020

> 标签：CLASSIFICATION

> 代码：https://github.com/kenshohara/3D-ResNets-PyTorch

> 描述：How can we collect and use a video dataset to further improve spatiotemporal 3D Convolutional Neural Networks (3D CNNs)? In order to positively answer this open question in video recognition, we have conducted an exploration study using a couple of large-scale video datasets and 3D CNNs.
### [Breast Cancer Diagnosis with Transfer Learning and Global Pooling](https://paperswithcode.com/paper/breast-cancer-diagnosis-with-transfer)
> 日期：26 Sep 2019

> 标签：DATA AUGMENTATION

> 代码：https://github.com/sara-kassani/ICIAR_Transfer_Learning_Global_Average_Pooling

> 描述：Breast cancer is one of the most common causes of cancer-related death in women worldwide. Early and accurate diagnosis of breast cancer may significantly increase the survival rate of patients.
