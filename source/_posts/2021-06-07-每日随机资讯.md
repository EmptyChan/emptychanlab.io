---
title: 2021-06-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BuntingBird_EN-CN7390631181_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp&w=1920&h=1080&rs=1&c=4
date: 2021-06-07 21:31:51
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BuntingBird_EN-CN7390631181_UHD.jpg&rf=LaDigue_UHD.jpg&pid=hp&w=1920&h=1080&rs=1&c=4)
# 新闻
### [Chrome OS 成全球第二大桌面系统，国内市场能否复制成功？](https://www.oschina.net/news/144888/gotc-2021-fyde-os)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>提到桌面操作系统，Windows 与 macOS 自然是家喻户晓，Linux 在开发者群体中的地位也无需赘言。而在刚刚过去的 2020 ......
### [Apache Doris 社区喜迎两位 PPMC 成员](https://www.oschina.net/news/145010)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>关于 Apache Doris(Incubating)Apache Doris 是一款基于MPP的分析型数据库系统。其简洁的架构，能够帮......
### [安全机构调查显示，2021 年 Android 银行类恶意软件数量增长 159%](https://www.oschina.net/news/144878/eset-threat-android)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>当企业在持续的勒索软件攻击下步履维艰时，Android 用户也正日益成为银行恶意软件的目标，安全机构 ESET 调查显示，在过去几个月中......
### [Facebook 开源 Flores-101 数据集以实现更准确的 AI 翻译](https://www.oschina.net/news/144879/facebook-open-sources-flores-101-dataset)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Facebook 宣布开源了一个名为 Flores-101 的数据集。该公司表示，这是一个首创的多对多评估数据集，涵盖世界各地的101种......
### [咱们从头到尾说一次 Spring 事务管理（器）](https://segmentfault.com/a/1190000040130617?utm_source=sf-homepage)
> 概要: 先点赞再看，养成好习惯事务管理，一个被说烂的也被看烂的话题，还是八股文中的基础股之一。但除了八股文中需要熟读并背诵的那些个传播行为之外，背后的“为什么”和核心原理更为重要。​写这篇文章之前，我也翻过一......
### [视频：李振宁用心筹备粉丝见面会 徐艺洋新歌影视剧两手抓](https://video.sina.com.cn/p/ent/2021-06-07/detail-ikqciyzi8144453.d.html)
> 概要: 视频：李振宁用心筹备粉丝见面会 徐艺洋新歌影视剧两手抓
### [剧场版「BanG Dream! ぽっぴん'どりーむ！」先导PV公开](http://acg.178.com/202106/417030461176.html)
> 概要: 「BanG Dream!」新作剧场版动画「BanG Dream! ぽっぴん'どりーむ！」公开了先导PV，该动画将于2022年1月1日在日本上映。「BanG Dream! ぽっぴん'どりーむ！」先导PV......
### [他们的高考记忆](https://www.huxiu.com/article/433173.html)
> 概要: 本文来自微信公众号：新周刊（ID：new-weekly），作者：新周刊，原文标题：《我们怀念的高考，从来都不是为了“拱白菜”》，头图来自：视觉中国6月7日的太阳照常升起，当人们又一次热烈谈论高考的时候......
### [P站美图推荐——透明雨衣特辑](https://news.dmzj1.com/article/71138.html)
> 概要: 不止具有实用性，在梅雨季的时候也是一件十分好的时尚单品。在透明雨衣下面穿上喜欢的衣服然后给灰蒙蒙的城市增加一些色彩吧！
### [「BanG Dream! FILM LIVE 2nd Stage」60秒宣传PV公开](http://acg.178.com/202106/417035777716.html)
> 概要: 由日本游戏厂商Bushiroad企划，三次元制作的剧场版动画「BanG Dream! FILM LIVE 2nd Stage」于近日公开了60秒宣传PV，剧场版动画将于8月20日上映。「BanG Dr......
### [新企划「東京カラーソニック!!」PV公开](http://acg.178.com/202106/417036186835.html)
> 概要: 由冨士原良担任人设的音乐新企划「東京カラーソニック!!」（东京Color Sonic!!）公开了PV，该企划以2080年为舞台，讲述了以音乐节的顶点为目标的学生们的青春故事。「東京カラーソニック!!」......
### [组图：《热带往事》首映主创送高考祝福 彭于晏隔空喊张艾嘉妈](http://slide.ent.sina.com.cn/film/slide_4_704_357620.html)
> 概要: 组图：《热带往事》首映主创送高考祝福 彭于晏隔空喊张艾嘉妈
### [漫画「新手姐妹的双人餐桌」第9卷封面公开](http://acg.178.com/202106/417043510180.html)
> 概要: 漫画「新手姐妹的双人餐桌」官方公开了本作第9卷的封面图，该册将于6月25日发售。「新手姐妹的双人餐桌」是柊ゆたか创作的漫画作品，同名漫改电视剧由山田杏奈和大友花恋主演......
### [真人版《孤独的美食家》第9季7月开播！](https://news.dmzj1.com/article/71145.html)
> 概要: 根据同名漫画改编的真人电视剧《孤独的美食家 Season9》宣布了将于7月开始播出的消息。
### [这个世界怎么能缺少BL呢！](https://news.dmzj1.com/article/71147.html)
> 概要: 本周要介绍的新漫画是GUNP新单行本《请叫我恶役腐女大小姐！》。
### [《邪恶国王和高尚勇者》角色新情报 6月24日发售](https://www.3dmgame.com/news/202106/3816089.html)
> 概要: 日本一旗下新作RPG《邪恶国王和高尚勇者》即将于6月24日发售，登陆PS4/Switch平台，6月7日今天官方公开了角色系统新情报，一起来先睹为快。·《邪恶国王和高尚勇者》讲述了人类女孩与龙之魔王之间......
### [iPhone售后泄露女学生私密果照 苹果要赔百万美元](https://www.3dmgame.com/news/202106/3816091.html)
> 概要: 近日据外媒telegraph报道，早先有苹果iPhone维修技术人员将一名俄勒冈州女学生手机上的果照和视频发布到脸书上，这名女生愤怒起诉苹果。最近苹果和她达成和解，并将支付数百万美元赔偿。苹果同意与这......
### [兵临城下：互联网公司杭州人才争夺战](https://www.huxiu.com/article/433176.html)
> 概要: 本文来自微信公众号：朱思码记（ID：zhusimaji88），作者：狐狸安，头图来自：视觉中国在IPO叫停并接受约谈、出台整改方案、CEO宣布辞职的200多天是非曲折后，靴子中的另一只，即将落地。据朱......
### [韩国惊悚片《第八天之夜》首发预告片 韩式驱魔](https://www.3dmgame.com/news/202106/3816094.html)
> 概要: Netflix韩国驱魔恐怖惊悚片《第八天之夜》发布中文预告片，影片讲述一群人与从封印中被释放的恶魔展开殊死搏斗的故事，影片将于7月2日上线Netflix。影片主演为金裕贞、李星民、朴海俊、南多凛。金裕......
### [《刺客信条英灵殿》1/4艾沃尔可换头发光雕像 7699元](https://www.3dmgame.com/news/202106/3816095.html)
> 概要: 近日，育碧天猫旗舰店上架了《刺客信条：英灵殿》1/4发光版Animus艾沃尔可换头雕像，分男/女两款，高64cm，重大9.8公斤，售价7699元，全球限量2000个。艾沃尔是游戏《刺客信条：英灵殿》中......
### [给1078万考生批卷的，可能不是个人](https://www.huxiu.com/article/433278.html)
> 概要: 本文来自微信公众号：放大灯（ID：guokr233），作者：杨景诒，头图来自：视觉中国又一年高考开始了，4000余万份高考试卷成绩，还有背后1078万考生的命运，都在各位阅卷组老师手中。自1977年恢......
### [《摇曳百合》作者举办画业15周年纪念作品展现场照片](https://news.dmzj1.com/article/71148.html)
> 概要: 创作过漫画《摇曳百合》的なもり，在日本东京都千代田举办了“なもり画业15周年纪念作品展”。在这次的展会中，展示了《摇曳百合》、《大室家》等过去作品的插图。另外会场还有なもり老师Q&A，Web公开图等展出。
### [视频：闪闪发光的女明星们 杨幂热巴刘亦菲造型又美出新高度](https://video.sina.com.cn/p/ent/2021-06-07/detail-ikqcfnaz9646546.d.html)
> 概要: 视频：闪闪发光的女明星们 杨幂热巴刘亦菲造型又美出新高度
### [6亿日活的抖音进军OTA，能做好吗？](https://www.tuicool.com/articles/J7RFfqj)
> 概要: 编辑导读：抖音作为用户量巨大的短视频产品，近年来频频在本地生活方向试水。最近，抖音在向OTA进军，内测的山竹旅行上线了。这个旅游服务平台有什么特别之处呢？本文作者对此展开分析，与你分享。山竹旅行内测上......
### [杨超越工作到凌晨在民宿包饺子，手上的指甲抢镜，网友：太精致了](https://new.qq.com/omn/20210607/20210607A09CQA00.html)
> 概要: 杨超越离开《创造101》之后就变了很多，那段时间她才刚刚踏进娱乐圈，位置没有坐稳，她没有仗着热度高就哗众取宠，反而低调了下来，网友总批评她毫无才艺，她也不敢吭声，只在背后默默的努力。         ......
### [湖北能源局：新建小区停车位应全部配建充电桩或预留安装条件](https://www.ithome.com/0/555/987.htm)
> 概要: IT之家6 月 7 日消息 根据微博 @湖北发布 消息，近日，湖北省能源局发文要求，今后居住区停车场所、单位内部停车场所、公共停车场所、高速公路和普通国省干道服务区等地，都要按比例配置新能源汽车充电基......
### [一边是在线教育先“抢”后裁，一边是兼课教师先走一步](https://www.tuicool.com/articles/3e2Qbmm)
> 概要: 今年四月底，北京市场监管局发布通报，针对跟谁学、学而思、新东方在线、高思四家校外教育培训机构出现的部分违规行为，分别给予警告及 50 万元顶格罚款的行政处罚。处罚锤音刚落，近日市场监管部门再对 15 ......
### [组图:丁程鑫出考场被私生围堵 多次劝阻无效后喊话"你听不懂吗"](http://slide.ent.sina.com.cn/star/slide_4_704_357656.html)
> 概要: 组图:丁程鑫出考场被私生围堵 多次劝阻无效后喊话"你听不懂吗"
### [大s和汪小菲离婚是因为一条微博？台湾小公主和北京糙汉子的爱情](https://new.qq.com/omn/20210607/20210607A0AKO000.html)
> 概要: 睡一觉起来，大s跟汪小菲离婚的消息全网飞，吓得我马上去汪小菲和大s的微博看官方消息，翻看汪小菲和大s的微博，都没有说离婚，还好，现实言情版《台湾傲娇小公主和北京糙汉》的小说还没有结束。        ......
### [起底国内追踪器江湖：AirTag 吐槽满天飞，华强北永相随](https://www.ithome.com/0/556/005.htm)
> 概要: 苹果“鸽王”AirTag 追踪器正式发售刚过一个月，就已经被“玩坏”了。仅仅在 B 站上搜索关键词“AirTag”，就有 800 多个视频映入我们眼帘。只是在商场、车库或某个犄角旮旯体验测评的内容就占......
### [TypeScript 声明文件全解析](https://www.tuicool.com/articles/Nv6ri2u)
> 概要: 导语：全面拥抱 TS 的时代，TS 已经成为日常开发中的重要部分。本文主要介绍 TS 声明文件的写法。声明文件的定义通俗地来讲，在 TypeScript 中以.d.ts为后缀的文件，我们称之为 Typ......
### [组图：《梦华录》首曝预告 刘亦菲古装好美陈晓眼神温柔](https://video.sina.com.cn/p/ent/2021-06-07/detail-ikqciyzi8270084.d.html)
> 概要: 组图：《梦华录》首曝预告 刘亦菲古装好美陈晓眼神温柔
### [沃尔玛旗下电商 Flipkart 推迟 IPO，现寻求 30 亿美元融资](https://www.ithome.com/0/556/008.htm)
> 概要: 6 月 7 日晚间消息，据报道，多位知情人士今日称，沃尔玛控股的印度电商巨头 Flipkart，正与软银集团和几家主权财富基金等投资者谈判，拟至少融资 30 亿美元。这些知情人士称，Flipkart ......
### [“倍速”不是个好东西，这里有 7个值得看的视频](https://www.tuicool.com/articles/VVjMNju)
> 概要: 上周中国网络视听大会召开后，“猪食”这两个字就频繁地出现在虎虎子的信息流里，短视频被痛斥低俗反智，拉低了一代人的审美品位。这番言论乍一听觉得如鲠在喉如芒在背，好像自己莫名其妙挨了一顿骂。但仔细想想又似......
### [火币全球站已恢复VET充提业务](https://www.btc126.com//view/170043.html)
> 概要: 官方消息，VET主网升级已完成，火币全球站现已恢复VET(Vechain)的充币和提币业务......
### [Ternoa入选币安NFT市场首批100位创作者名单](https://www.btc126.com//view/170046.html)
> 概要: 官方消息，基于NFT的去中心化数据传输区块链Ternoa近期在推特上宣布，Ternoa入选币安NFT市场100位创作者名单。据悉，Ternoa是基于NFT的去中心化数据传输区块链，能够长期存储，加密和......
### [《庆余年》《赘婿》火爆背后，2020 年网络文学市场规模超 288 亿元](https://www.ithome.com/0/556/014.htm)
> 概要: IT之家6 月 7 日消息 昨日，第 27 届上海电视节在上海展览中心开幕。据央视财经报道，本届电视节期间，部分网络文学 IP 改编的电视剧也受到关注。IT之家了解到，以 2019 年 11 月上映的......
### [恶搞安切洛蒂回归皇马执教](https://bbs.hupu.com/43442898.html)
> 概要: 恶搞安切洛蒂回归皇马执教
### [视频徐阳猜国足4-0菲律宾，保险起步赢3个球](https://bbs.hupu.com/43442971.html)
> 概要: 视频徐阳猜国足4-0菲律宾，保险起步赢3个球
### [习近平赴青海考察调研](https://finance.sina.com.cn/china/2021-06-07/doc-ikqcfnaz9683198.shtml)
> 概要: 习近平总书记7日赴青海考察调研。当天下午，他来到位于西宁市的青海圣源地毯集团有限公司，了解企业依托当地原材料资源优势，创新设计理念，提升产品竞争力...
### [第二届中国—中东欧国家博览会8日在宁波举行 塞尔维亚和捷克将担任主宾国](https://finance.sina.com.cn/jjxw/2021-06-07/doc-ikqcfnaz9684166.shtml)
> 概要: 原标题：第二届中国—中东欧国家博览会8日在宁波举行，塞尔维亚和捷克将担任主宾国 6月8日至11日，第二届中国—中东欧国家博览会暨国际消费品博览会（以下简称“中东欧博览会”...
### [《尚气与十戒传奇》新剧照 灵感来自中国武侠](https://www.3dmgame.com/news/202106/3816113.html)
> 概要: 漫威电影《尚气与十戒传奇》新剧照发布，主演刘思慕身穿便装在中式建筑里亮相。导演德斯汀·克里顿在接受《帝国》杂志时提到，影片灵感来自于成龙电影和中国武侠，如《卧虎藏龙》，还请来了中国内地的动作编排，设计......
### [流言板雷吉：我们是联盟中三分最好的球队，今天在主场投进了球](https://bbs.hupu.com/43443193.html)
> 概要: 虎扑06月07日讯 今天结束的一场季后赛，快船126-111战胜独行侠，大比分4-3晋级西部半决赛。赛后，快船球员雷吉-杰克逊接受了采访。“我知道我们是联盟中最好的三分球球队，但我们在这个系列赛中的投
### [MicroStrategy宣布发行4亿美元的优先票据以购买比特币](https://www.btc126.com//view/170056.html)
> 概要: MicroStrategy 宣布将按照《证券法》的规定，且在市场和其他因素允许的条件下，向合格的机构买家提供价值 4 亿美元的优先票据。MicroStrategy 计划将出售票据所获得的净收益购买更多......
### [交通运输部：中国铁路、高速公路20万以上人口城市覆盖率均超95%](https://finance.sina.com.cn/china/bwdt/2021-06-07/doc-ikqciyzi8285419.shtml)
> 概要: 原标题：交通运输部：中国铁路、高速公路20万以上人口城市覆盖率均超95% 中新社北京6月7日电 （李京泽 梁晓辉）中国交通运输部部长李小鹏7日受中国国务院委托...
### [人民币问道：汇率利率跟谁走](https://finance.sina.com.cn/roll/2021-06-07/doc-ikqcfnaz9684618.shtml)
> 概要: 原标题：【首席观察】人民币问道：汇率利率跟谁走 经济观察网首席记者欧阳晓红 一 6.4072､ 2.1741%！人民币的这两种价格怎么了？
### [流言板瓜迪奥拉：巴萨签约阿圭罗和埃里克是绝对正确的选择](https://bbs.hupu.com/43443272.html)
> 概要: 虎扑06月07日讯 曼城主帅瓜迪奥拉在今天到马略卡参加了Legends Trophy高尔夫球赛，包括巴萨主席拉波尔塔，前巴萨后卫托尼-纳达尔，前毕尔巴鄂竞技前锋阿杜里斯等人也参加了这次的高尔夫球赛。关
### [曝李晟李佳航离婚，去年11月结束婚姻关系，麦迪娜姜潮则被曝撒谎](https://new.qq.com/omn/20210607/20210607A0C9I900.html)
> 概要: 6月7号下午，某百万大V博主爆料某85花女星正式离婚，提示大家对方签过柴智屏，大IP女主，1985年出生，一时间引起网友关注。            到了晚上，另一博主公开爆料对象，称签过柴智屏的85......
### [50岁钟丽缇晒日常，意外暴露800万豪车！连帽子都价值3900元](https://new.qq.com/omn/20210607/20210607A0CB8D00.html)
> 概要: 6月7日下午，50岁的香港影星钟丽缇在社交平台晒出一则视频，配文称:“Just be yourself。”视频中，钟丽缇皮肤白皙，画着大浓妆，随着音乐摇摆，活力十足显年轻。            钟丽......
### [国企改革三年行动“期中考” 央企科技创新将迎新政策利好](https://finance.sina.com.cn/china/2021-06-07/doc-ikqcfnaz9685277.shtml)
> 概要: 原标题：国企改革三年行动“期中考”，央企科技创新将迎新政策利好 国务院国资委党委书记、主任郝鹏透露，《“十四五”中央企业发展规划纲要》即将发布。
### [BC Tech Group联合创始人：看好比特币和以太坊等数字资产](https://www.btc126.com//view/170058.html)
> 概要: 据u.today消息，OSL和BC Technology Group的联合创始人兼董事总经理Dave Chapman表示，他的公司非常看好央行数字货币和稳定币，根据查普曼的说法，CBDC将在未来十年内......
### [GDP千亿县达38个：6县超2000亿 江苏占16席](https://finance.sina.com.cn/china/2021-06-07/doc-ikqcfnaz9686988.shtml)
> 概要: 原标题：GDP千亿县达38个：6县超2000亿，江苏占16席 38个GDP千亿县分布在江苏、浙江、福建、湖南、山东、河北、江西、贵州、陕西共9个省份，其中江苏达到了16个。
# 小说
### [惧寻龙](http://book.zongheng.com/book/883106.html)
> 作者：孤星袭月

> 标签：悬疑灵异

> 简介：这是地行者的历险，这是地行将军的劫难，这是地藏王的证道之路！

> 章节末：第四十章 第一卷大结局

> 状态：完本
# 论文
### [Dual Attention GANs for Semantic Image Synthesis](https://paperswithcode.com/paper/dual-attention-gans-for-semantic-image)
> 日期：29 Aug 2020

> 标签：IMAGE GENERATION

> 代码：https://github.com/Ha0Tang/DAGAN

> 描述：In this paper, we focus on the semantic image synthesis task that aims at transferring semantic label maps to photo-realistic images. Existing methods lack effective semantic constraints to preserve the semantic information and ignore the structural correlations in both spatial and channel dimensions, leading to unsatisfactory blurry and artifact-prone results.
### [Deep coastal sea elements forecasting using U-Net based models](https://paperswithcode.com/paper/deep-coastal-sea-elements-forecasting-using-u)
> 日期：6 Nov 2020

> 标签：WEATHER FORECASTING

> 代码：https://github.com/jesusgf96/Sea-Elements-Prediction-UNet-Based-Models

> 描述：Due to the development of deep learning techniques applied to satellite imagery, weather forecasting that uses remote sensing data has also been the subject of major progress. The present paper investigates multiple steps ahead frame prediction for coastal sea elements in the Netherlands using U-Net based architectures.
