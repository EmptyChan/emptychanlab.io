---
title: 2021-10-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Whakarewarewa_ZH-CN4957778498_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-10-17 22:16:22
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Whakarewarewa_ZH-CN4957778498_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [Go 语言之父：不要在 Go 1.18 标准库中使用泛型](https://www.oschina.net/news/164554/do-not-change-the-libraries-in-1-18)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>Go 语言之父 Rob Pike 近日在 Go 代码仓库提交了一个 issue (#48918)，建议不要改动 Go......
### [Netcraft 10 月 Web 服务器调查报告发布](https://www.oschina.net/news/164551/netcraft-web-server-report-in-2021-10)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>Netcraft 2021 年 10 月份的全球 Web 服务器调查报告已经发布，该报告可以帮助人们了解全球网站数量......
### [KaOS 2021.10 发布，KDE 桌面 Linux 发行版](https://www.oschina.net/news/164545/kaos-2021-10-released)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>KDE 成立 25 周年，为了纪念这一日子，KaOS 发布了 2021.10 的 ISO，其中包括最新的 Plasm......
### [Devuan 4.0 发布，不依赖 systemd 的 Debian 分支](https://www.oschina.net/news/164550/devuan-4-0-released)
> 概要: Linux基金会开源软件学园人才激励计划来了，免费培训+考试机会等你报名！>>>>>Devuan 4.0 稳定版已正式发布，代号 "Chimaera"。主要变化：基于 Debian Bullseye ......
### [人形导盲犬关爱视障少女，不良少年的温柔以待](https://news.dmzj.com/article/72464.html)
> 概要: 今年日本第四季度10月新剧《这是恋爱！不良少年与白手杖女孩》已上线，被称之为一脚踢出的爱情，由NTV出品的恋爱喜剧，杉咲花和杉野遥亮主演，改编自同名漫画，讲述几乎只能分辨光与颜色，在盲人学校高中三年级就读的弱视女孩赤座雪子和不良少年黑川森生相遇，...
### [Amazon Corretto JDK](https://docs.aws.amazon.com/corretto/)
> 概要: Amazon Corretto JDK
### [贝壳裁员真相](https://www.huxiu.com/article/464379.html)
> 概要: 本文来自微信公众号：财经十一人（ID：caijingEleven），作者：张光裕 陈嘉瑶 桂澜珊，编辑：王博。原文标题：《贝壳裁员真相》此时裁员对贝壳来说，是无奈但明智之举。10月11日至今，贝壳找房......
### [机会平等是公平的吗？](https://www.huxiu.com/article/464332.html)
> 概要: 编者按：本文是华东师范大学教授刘擎为桑德尔的新书《精英的傲慢》所写的推荐序。2019年，美国曝出了有史以来最严重的一起高校招生舞弊丑闻。联邦检察官在3月对50人提出指控，知名演员、商业领袖及其他富有的......
### [Hacking around on the telephone, the internet before the internet](https://madned.substack.com/p/wardialing-and-other-phoney-stuff)
> 概要: Hacking around on the telephone, the internet before the internet
### [《佐贺偶像是传奇 Revenge》fanbook明年2月发售](https://news.dmzj.com/article/72465.html)
> 概要: TV动画《佐贺偶像是传奇 Revenge》的fanbook发售日确定为2022年2月14日。
### [动漫情侣头像｜小众情头](https://new.qq.com/omn/20211010/20211010A02NME00.html)
> 概要: 图源网络侵删  原截或原创看到可评论注明一键分享投币评论转发欢迎白嫖加关注......
### [动画《风都侦探》主人公翔太郎和菲利普海报公开](https://news.dmzj.com/article/72466.html)
> 概要: 石森章太郎、佐藤まさき、三条陆原作改编的动画《风都侦探》的主人公海报公开。
### [TV动画《SELECTION PROJECT》铃音&玲那穿着suzu☆rena衣装的手办开订](https://news.dmzj.com/article/72467.html)
> 概要: TV动画《SELECTION PROJECT》中登场的美山铃音和花野井玲那的1/7比例手办将于2022年10月发售。
### [「天使降临到我身边！」公开新作剧场版动画最新视觉图](http://acg.178.com/202110/428435700314.html)
> 概要: 「天使降临到我身边！」于近日公开了新作剧场版动画的最新视觉图，预计将于2022年播出。「天使降临到我身边！」是四格漫画作品，于2017年1月在「Comic百合姬」上开始连载，单行本由一迅社出版，同名电......
### [「蓝色监狱」公开「ブルーロック渋谷ジャック」人物海报](http://acg.178.com/202110/428438333049.html)
> 概要: 漫画「蓝色监狱」（ブルーロック）官方公开了「ブルーロック渋谷ジャック」人物海报，包括蜂楽廻、千切豹馬、國神錬介和潔世一。「蓝色监狱」是ノ村优介创作的漫画作品，根据漫画改编的同名电视动画由8 Bit负责......
### [「哥斯拉：奇异点」佩罗2粘土人开订](http://acg.178.com/202110/428441412306.html)
> 概要: GOODSMILE作品「哥斯拉：奇异点」佩罗2粘土人开订，高约100mm，主体采用ABS、PVC材料制造。该手办定价为6200日元（含税），约合人民币349元，预计于2022年7月发售......
### [「刀剑神域：进击篇·无星夜的咏叹调」上映直前PV「世界篇」公开](http://acg.178.com/202110/428441633677.html)
> 概要: 剧场版动画「刀剑神域：进击篇·无星夜的咏叹调」公开了最新的上映直前PV「世界篇」，该动画将于2021年10月30日上映。「刀剑神域：进击篇·无星夜的咏叹调」上映直前PV「世界篇」STAFF原作：川原 ......
### [《长津湖》观影人次破1亿 票房破48亿](https://ent.sina.com.cn/m/c/2021-10-17/doc-iktzqtyu1909882.shtml)
> 概要: 新浪娱乐讯 据猫眼专业版，10月17日11时7分，观影人次破亿，成为有公开数及统计以来，中国电影市场第五部观影人次破亿影片。　　观影人次排名前四部：　　 1.6亿人次　　 1.41亿人次　　 1.21......
### [《长津湖》票房破48亿 超《流浪地球》成中国影史第4](https://www.3dmgame.com/news/202110/3825939.html)
> 概要: 今日（10月17日），据猫眼专业版公布的数据，《长津湖》上映第18天，总票房突破48亿，超过《流浪地球》，目前位列中国影史票房榜第四，全球年度票房榜第二名。（全球年度票房冠军为《你好，李焕英》54.1......
### [秦时明月：编剧到底给天明开了多少挂？至少6个](https://new.qq.com/omn/20211014/20211014A0D3J200.html)
> 概要: 天明作为动漫最惨主角，自从第五部中期开始就各种掉线，除了片头片尾之外几乎都见不到人。作为武侠剧，成长速度最快的方法有两种，一种是拿到武功秘籍，另一种就是有高手传功。而天明刚进墨家就被燕丹传功，墨家第一......
### [小米又出爆款！米家电水壶系列销量破1500万台](https://www.3dmgame.com/news/202110/3825940.html)
> 概要: 继宣布米家电饭煲系列全平台累计销量突破1000万台后，小米官方再公布最新战报。今日，小米智能生态官微宣布，截至2021年9月30日，米家电水壶系列销量破1500万台。从小米商城了解到，目前上架在售的米......
### [樊振东长文回应球迷提问:反对代拍跟机等不良风气](https://ent.sina.com.cn/s/m/2021-10-17/doc-iktzscyy0150287.shtml)
> 概要: 新浪娱乐讯 10月17日，樊振东通过球迷会账号发长文回应部分球迷感兴趣的和有争议的问题，表示“希望跟大家沟通沟通，彼此进一步互相理解互相尊重。”文中写道：　　1.暂时没有注册个人账号的打算　　2.与球......
### [工信部：继续深入推进互联网行业专项整治行动，开屏弹窗关不掉基本清零，乱跳转由 90% 下降至 1%](https://www.ithome.com/0/580/980.htm)
> 概要: IT之家10 月 17 日消息，自去年下半年以来，国家有关部门对于多个互联网企业进行了约谈和整治，包括阿里巴巴、腾讯、美团在内的数个龙头企业均有涉及。反垄断和反不正当竞争，事关市场公平竞争秩序维护，事......
### [瑞萨：以 40 纳米为界，分划自产和外包代工](https://www.ithome.com/0/580/981.htm)
> 概要: 10 月 17 日消息，日经亚洲近日专访了日本半导体供应商瑞萨的高管，谈及了公司的未来发展的总体规划。据日经亚洲评论 10 月 13 日报道，瑞萨高管 Sailesh Chittipeddi 向记者透......
### [《王国之心》系列暂时没有打算推出NS原生移植版](https://www.3dmgame.com/news/202110/3825941.html)
> 概要: 任天堂和《王国之心》这几周走得很近。《任天堂明星大乱斗特别版》的最终DLC角色是《王国之心》主角索拉。而在当天的活动中还公布了《王国之心》全系列将登陆NS主机的消息，包括《王国之心1.5+2.5 HD......
### [MIT 副教授赵宇飞团队“等角线”研究登数学四大顶刊之一：作者中两位是本科生，最小的是 00 后](https://www.ithome.com/0/580/995.htm)
> 概要: 你能想象，一个等角线问题，竟然困扰了数学家们 70 余年？等角线的定义很简单，穿过一个点的一组直线，任 2 条之间夹角都相等就是等角线。比如在二维平面相互垂直的两条直线或，或相互成 60 度角的 3 ......
### [如何为高管设计合理的薪酬激励？](https://www.huxiu.com/article/464434.html)
> 概要: 众所周知，上市公司高管的绩效表现直接影响公司整体长期的绩效表现。如何为高管设计合理的薪酬激励，确保其科学性，是公司激励体系中的重中之重。本文来自微信公众号：中欧商业评论（ID：ceibs-cbr），作......
### [焦点分析｜被150万网络黑产盯上之后，科技巨头要建起新型防火墙](https://www.tuicool.com/articles/IviEfmN)
> 概要: 文 | 张婧怡编辑 | 苏建勋“高仿明星”、“刷单返利”的骗局还未消减，“外汇男友”剧情又持续上演。上饶女孩小张在社交软件上认识了一个男孩，双方聊得投机，按照这位“甜蜜网友”的要求，小张下载了某炒外汇......
### [张杰受伤后去医院缝针，对谢娜说的第一句话是“女儿有没有吓到”](https://new.qq.com/omn/20211017/20211017A050AZ00.html)
> 概要: 序言：2021年10月16日晚，张杰苏州站演唱会举行，因为最近2年有疫情，所以这是张杰2年之后首次开演唱会，粉丝和他都显得很激动，很珍惜这个难得的时刻。            张杰为了回馈粉丝，在舞美......
### [张杰演唱会主办方发声明致歉 并解释事故原因](https://ent.sina.com.cn/y/yneidi/2021-10-17/doc-iktzscyy0175272.shtml)
> 概要: 新浪娱乐讯 10月17日，张杰演唱会主办方罗盘文化通过官方微博发表致歉声明，对张杰在演唱会中意外受伤表示道歉。声明中，罗盘文化回应事故原因，称是由于舞台电梯在下降中碰到外圈固定平台造成电梯骤降。罗盘文......
### [《风都侦探》主角动画形象公布 假面骑士W归来](https://acg.gamersky.com/news/202110/1430735.shtml)
> 概要: 漫画《风都侦探》官方于近日公布该作两位主角：左翔太郎与菲利普的动画形象。
### [我想再来谈谈Immer —— 用法篇](https://www.tuicool.com/articles/n2Qrimb)
> 概要: 起因之前已经写过一篇关于Immer的介绍，这次还想再详细谈谈Immer，主要的原因有以下两点Web业务越来越复杂，原来React工程里推荐的范式化的数据结果不适合于像低代码平台这类复杂业务了，并且同时......
### [DC动画《猫女：追捕》公布预告 二次元美少女版猫女](https://acg.gamersky.com/news/202110/1430741.shtml)
> 概要: DC动画电影《猫女：追捕》公布预告，二次元美少女版猫女偷窃绝世珠宝，被黑白两道猎捕。
### [Git Subtree 的使用](https://www.tuicool.com/articles/nYVb2qv)
> 概要: git 的subtree是一种复用源代码的方式，可以让多个仓库引用某个仓库的代码，也可以将仓库中的某个目录拆分成一个子仓库以供其他仓库使用。相较于 git 的submodule，目前社区中多更推崇 s......
### [丁磊：中国原创音乐的粗放时期已过去，所有从业者要真正下定决心解除独家版权](https://www.ithome.com/0/581/018.htm)
> 概要: IT之家10 月 17 日消息，第八届中国国际版权博览会昨日在杭州开幕，今日，网易 CEO 丁磊在会上发表了以《探索数字音乐版权高质量发展 推动音乐产业共同富裕》为主题的演讲，畅想了音乐产业的“共同富......
### [视频：张杰回应受伤称三个月会康复 相关舞蹈表演将暂停](https://video.sina.com.cn/p/ent/2021-10-17/detail-iktzscyy0193132.d.html)
> 概要: 视频：张杰回应受伤称三个月会康复 相关舞蹈表演将暂停
### [圆明园遗址公园明日对公众免费开放](https://finance.sina.com.cn/china/2021-10-17/doc-iktzscyy0193967.shtml)
> 概要: 原标题：圆明园遗址公园明日对公众免费开放 据@圆明园遗址公园 微博消息，2021年10月18日，圆明园将迎来罹难161周年纪念日，为铭记历史，让更多的人走进圆明园...
### [神舟13号见面会记者用小米旗舰！画面极具辨识度](https://www.3dmgame.com/news/202110/3825960.html)
> 概要: 近日，在神舟十三号载人飞行任务航天员见面会现场，博主@叽歪数码VVV发现，一位记者使用的手机是小米11 Ultra，非常有辨识度。小米创办人、小米集团董事长兼CEO雷军表示，小米11 Ultra的辨识......
### [直播占领朋友圈](https://www.tuicool.com/articles/IJ32air)
> 概要: 深燃（shenrancaijing）原创作者 | 苏琦编辑 | 金玙璠9月10日，格十三在“八点一刻”微信视频号直播活动上，和观众聊起了“无性婚姻”，直播间在线人数从30万瞬间涨到50万。她没想到，从......
### [历代五影之中，哪些人曾经遭受过暗杀？为何砂隐村总是成为目标？](https://new.qq.com/omn/20211016/20211016A046P200.html)
> 概要: 历代五影之中，哪些人曾经遭受过暗杀？为何砂隐村总是成为目标？
### [沈阳市社保大厦保安与提前到达的办事群众发生冲突，官方通报处理结果](https://finance.sina.com.cn/jjxw/2021-10-17/doc-iktzscyy0198884.shtml)
> 概要: 原标题：沈阳市社保大厦保安与提前到达的办事群众发生冲突，官方通报处理结果 来源：上观新闻 关于驻厦保安推搡呵斥市民提前进门的处理情况 10月16日下午...
### [央企共谋助力构建乡村振兴新格局](https://finance.sina.com.cn/jjxw/2021-10-17/doc-iktzqtyu1964629.shtml)
> 概要: 原标题：央企共谋助力构建乡村振兴新格局 新华社成都10月17日电（记者袁秋岳、唐文豪）今年以来，中央企业在246个定点帮扶县直接投入帮扶资金75.95亿元...
### [外省通报2名离沪旅游人员核酸检测阳性，上海对其在沪相关人员和场所落实防控措施](https://finance.sina.com.cn/china/2021-10-17/doc-iktzqtyu1966539.shtml)
> 概要: 上海发布微信公号10月17日消息，上海市疫情防控工作领导小组办公室发布信息： 陕西今通报10月9日离沪赴甘肃、内蒙古、陕西等地旅游的2名人员...
### [新冠阳性夫妻曾在三省区旅游，轨迹一图读懂](https://finance.sina.com.cn/jjxw/2021-10-17/doc-iktzqtyu1968258.shtml)
> 概要: 10月17日，陕西省卫健委通报2名外地游客（夫妻）核酸检测阳性。通报轨迹显示，2人曾在甘肃、内蒙古自驾旅游，10月15日在甘肃嘉峪关核酸检测结果异常...
### [Apparently, it's the next big thing. What is the metaverse?](https://www.bbc.co.uk/news/technology-58749529)
> 概要: Apparently, it's the next big thing. What is the metaverse?
### [组图：网友大理偶遇洪欣一家三口 张丹峰搂老婆合照再破婚变传闻](http://slide.ent.sina.com.cn/star/slide_4_704_362759.html)
> 概要: 组图：网友大理偶遇洪欣一家三口 张丹峰搂老婆合照再破婚变传闻
### [群星荟萃恋庐山：第二届庐山国际爱情电影周盛大启幕！](https://new.qq.com/omn/20211017/20211017A07OJJ00.html)
> 概要: 10月16日，为期7天的第二届庐山国际爱情电影周在江西庐山开幕。本届电影周主题是“庐山天下恋，缘在此山中”，共分为爱情大道、主题盛典、国际爱情电影庐山论坛、电影《邓小平小道》全国点映启动仪式、经典爱情......
### [太火热！国考报名仅3天，这个岗位的报录比竟高达1700：1……](https://finance.sina.com.cn/china/2021-10-17/doc-iktzqtyu1972923.shtml)
> 概要: 原标题：太火热！国考报名仅3天，这个岗位的报录比竟高达1700：1…… 每经实习编辑 李泽东 中国新闻网报道，10月17日，国考报名进入第三天。
### [潮汐正在变化！《海王2》电影首曝中字特辑](https://www.3dmgame.com/news/202110/3825962.html)
> 概要: DC新片《海王2》首曝中字特辑，导演温子仁及杰森·莫玛等主演现身介绍本片，还有大量幕后花絮及概念图率先出炉！艾梅柏·希尔德、帕特里克·威尔森、叶海亚·阿卜杜勒-迈丁、杜夫·龙格尔、兰道尔·朴等第一部的......
### [印度，瞧不上中国造的特斯拉？](https://www.huxiu.com/article/464461.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔题图 | 网友PS“明年一定”，这是特斯拉CEO埃隆·马斯克，去年10月2日，回答印度网友“特斯拉何时进入印度”时给出的许诺。然而一整年过去了，印度官方也坐不住了......
### [分手章子怡后，撒贝宁转身娶洋媳妇“李白”，结婚5年儿女双全](https://new.qq.com/omn/20211017/20211017A08G7500.html)
> 概要: 流光容易把人抛，红了樱桃，绿了芭蕉。——蒋捷《一剪梅》英雄迟暮，美人色衰，亘古不变的定律。飞逝而过的时光催着小孩长大；催着少年变老；催着美人老去。年年岁岁樱桃红了又谢，芭蕉绿了又枯，时光的力量过于强大......
### [Facebook is researching AI systems that see, hear, remember everything you do](https://www.theverge.com/2021/10/14/22725894/facebook-augmented-reality-ar-glasses-ai-systems-ego4d-research)
> 概要: Facebook is researching AI systems that see, hear, remember everything you do
### [《兰心大剧院》娄烨再创佳作，历史车流永远滚动](https://new.qq.com/omn/20211017/20211017A08L0O00.html)
> 概要: 一个女人的命运被时代的洪流所牵引，这就是电影《兰心大剧院》。导演娄烨，编剧马英力，电影根据电影《上海之死》以及横光利一《上海》改编，自威尼斯电影节首映后就备受好评，那么这一次娄烨又将给我们带来怎样的惊......
### [博人传221集：新中忍考试开始！神秘大蛇丸现身，蛇叔盯上博人](https://new.qq.com/omn/20211017/20211017A090C000.html)
> 概要: 《博人传》主线动画的剧情估计要告一段落，接下来应该又是动画原创剧情了。没办法，随着大筒木一式被击败，接下来就是各种改造人出场。不仅如此，这些家伙的实力也强得可怕，甚至可以说个个都是“强无敌”。另一方面......
# 小说
### [狼啸游龙](http://book.zongheng.com/book/805987.html)
> 作者：寄居龙城

> 标签：奇幻玄幻

> 简介：落魄江湖身如萍，一生坎坷一生勤。匹马单刀万里尘，浩气长存纵皇城。红尘滚滚，坎坷重重，一刀在手，快意恩仇。道义压肩挺起头，富贵在前莫弯腰。这一生，活他个坦坦荡荡，这一生，拼他个太平盛世！

> 章节末：新书《落榜生》已发表

> 状态：完本
# 论文
### [CrossAug: A Contrastive Data Augmentation Method for Debiasing Fact Verification Models | Papers With Code](https://paperswithcode.com/paper/crossaug-a-contrastive-data-augmentation)
> 日期：30 Sep 2021

> 标签：None

> 代码：None

> 描述：Fact verification datasets are typically constructed using crowdsourcing techniques due to the lack of text sources with veracity labels. However, the crowdsourcing process often produces undesired biases in data that cause models to learn spurious patterns.
### [Black-Box Attacks on Sequential Recommenders via Data-Free Model Extraction | Papers With Code](https://paperswithcode.com/paper/black-box-attacks-on-sequential-recommenders)
> 日期：1 Sep 2021

> 标签：None

> 代码：https://github.com/yueeeeeeee/recsys-extraction-attack

> 描述：We investigate whether model extraction can be used to "steal" the weights of sequential recommender systems, and the potential threats posed to victims of such attacks. This type of risk has attracted attention in image and text classification, but to our knowledge not in recommender systems.
