---
title: 2022-02-05-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MexicoMonarchs_ZH-CN7526758236_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-05 21:37:42
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MexicoMonarchs_ZH-CN7526758236_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [RT-Thread v4.1.0 Beta 发布](https://www.oschina.net/news/181223/rt-thread-4-1-0-beta-released)
> 概要: 前言虽然距离上次发布v4.0.5的更新才刚刚过去一个月的时间，但是经过我们紧锣密鼓的准备，我们终于在农历新年第一天为大家带来了全新的 v4.1.0 Beta 版本。这是一个体验尝鲜版并非4.1.0正式......
### [Slackware Linux 15.0 正式发布](https://www.oschina.net/news/181226/slackware-linux-15-0-released)
> 概要: Slackware Linux 15.0 正式版本已发布。Slackware Linux 于 1993 年推出，是目前历史最悠久且仍在维护的 Linux 发行版。Slackware Linux 上一个......
### [Qt 简化商业授权方案](https://www.oschina.net/news/181231/qt-simplify-commercial)
> 概要: Qt 公司正在积极探索改进其产品多样性和提升财务业绩的方法，除了最近的Qt Digital Advertising 1.0（Qt 数字广告 1.0），近日它还宣布简化其商业授权方案。Qt 现在已将其面......
### [Apache Linkis(incubating) 1.0.3 版本发布](https://www.oschina.net/news/181224/linkis-1-0-3-released)
> 概要: Linkis 1.0.3 版本发布Apache Linkis(incubating) 1.0.3 包含所有 Project Linkis-1.0.3。该版本是Linkis进入Apache孵化的第一个版......
### [《英雄连3》全新准预览实机游戏片段分享](https://www.3dmgame.com/news/202202/3834965.html)
> 概要: SEGA和Relic日前《英雄连3》发布了新的开发日志视频，其中包含了一些之前未公开的实机游戏片段。在这段视频中，Relic的开发者谈论了新的任务类型，改进的AI，自定义玩家部队，以及任务设计的整体处......
### [​是谁在幕后搭建2022北京冬奥会？](https://www.huxiu.com/article/495663.html)
> 概要: 本文来自“凤凰网体育”，作者：特约记者梁中明，原文名称为《是谁在幕后搭建2022北京冬奥会？》，封面来自视觉中国。北京冬奥临近，除了赛场上奋勇拼搏的中国运动健儿外，冬奥场馆的技术运维人员也将经历一场为......
### [陈坤46岁生日晒与母亲合照：谢谢妈妈给予我生命](https://ent.sina.com.cn/s/m/2022-02-05/doc-ikyakumy4277025.shtml)
> 概要: 新浪娱乐讯 2月4日，陈坤46岁生日，在绿洲发文晒与母亲合照，配文表示：“谢谢妈妈给予我生命。”照片中，陈坤嘟嘴与母亲自拍，画风十分有爱......
### [「PASH!」2022年3月号封面公开](http://acg.178.com/202202/438020355855.html)
> 概要: 「PASH!」官方公开了月刊动画杂志「PASH!」2022年3月号的封面图，本期封面为动画「时光代理人」和「佐佐木与宫野」。「时光代理人」是bilibili与哔梦联合出品、澜映画制作的原创网络动画作品......
### [「刀剑神域」Anime Japan 2022年视觉图公开](http://acg.178.com/202202/438020531814.html)
> 概要: 近日，「刀剑神域」官方公开了Anime Japan 2022年的最新视觉图，本次视觉图由角色设计山本由美子绘制。电视动画「刀剑神域」改编自川原砾著作、abec插画的同名轻小说，动画由伊藤智彦担任导演，......
### [福原爱微博发文晒观看冬奥开幕式照片](https://ent.sina.com.cn/jp/2022-02-05/doc-ikyakumy4280214.shtml)
> 概要: 新浪娱乐讯 2月4日，北京冬奥会正式开幕，日本运动员福原爱也在微博发文晒出自己观看冬奥开幕式的照片，配文：“北京冬奥开幕了！！！”......
### [贵圈｜他被称作内地吴孟达，曾与沈腾合租，演20年配角终于红了](https://new.qq.com/rain/a/20220204A091YW00)
> 概要: * 版权声明：腾讯新闻出品内容，未经授权，不得复制和转载，否则将追究法律责任从大年初二起，《这个杀手不太冷静》就从声量平平，迅速成为春节档票房和排片第二名。在明星、大导和大制作扎堆的春节档，鲜为人知的......
### [HoneyWorks单曲「Dear LAYLA」动画MV公开](http://acg.178.com/202202/438026563087.html)
> 概要: NICONICO动画网站知名创作团队HoneyWorks已于近期公开了单曲「Dear LAYLA」的动画MV，该曲收录于专辑「FT4」中，由声优齐藤壮马和内田雄马担任演唱，专辑将于2月23日发售。「D......
### [（中午速看）海贼王1039话](https://new.qq.com/omn/20220205/20220205A03CX100.html)
> 概要: 本公众号发布的所有内容仅用于学习交流，请勿盗用谋取私利。                                                                        ......
### [微软突然暂停Win11更新：直言准备更多重磅新功能](https://www.3dmgame.com/news/202202/3834982.html)
> 概要: 在过去6个月时间里，Windows 11预览版一直很有规律的发布。不过遗憾的是，由于新预览版不符合微软设定的“质量标准”，微软本周决定暂停发布新版本。至于为何突然宣布不更新，具体的细节官方并没有公布......
### [漫画「陛下，您的心声泄露了!」宣传PV公布](http://acg.178.com/202202/438031009528.html)
> 概要: 漫画「陛下，您的心声泄露了!」的最新宣传PV现已公布，为本次PV配音的声优是细谷佳正和早见沙织。本作的第一、二卷正在好评发售中，第三卷将于2022年4月15日发售。漫画「陛下，您的心声泄露了!」宣传P......
### [猪器官移植人体，基因编辑公司启函生物转向细胞疗法](https://www.ithome.com/0/601/734.htm)
> 概要: 在过去的几个月里，异种器官移植领域发生的一系列的“第一”激发了人们新的希望，即用经过基因编辑的猪器官来代替人体器官，解决全球器官严重短缺的问题 —— 移植领域的一个全新时代正在到来。和目前正在开始临床......
### [Rockstar宣布《GTA》系列新作制作中！](https://news.dmzj.com/article/73507.html)
> 概要: Rockstar Games宣布了正在制作《GTA》系列的下一代新作。有关新作的更多消息，将在日后公开。
### [《长津湖之水门桥》票房破20亿 观影人次3650万+](https://www.3dmgame.com/news/202202/3834987.html)
> 概要: 今日（2月5日），上映第5日，电影《长津湖之水门桥》累计票房突破20亿，观影人次3650万+。该片淘票票9.6分，猫眼电影9.6分，微博9.2分，豆瓣7.2分，B站9.2分。作为中国影史票房冠军《长津......
### [回顾氢原子、介绍谐振子，《张朝阳的物理课》进入新阶段](https://www.ithome.com/0/601/736.htm)
> 概要: 2 月 4 日 12 时，《张朝阳的物理课》第二十五期如期开播。搜狐创始人、董事局主席兼 CEO 张朝阳坐镇搜狐视频直播间，开讲谐振子问题。经历了前几节课对薛定谔方程的详细求解，这节课先是回顾了最初的......
### [NS游戏《探灵直播》漫画版开始连载](https://news.dmzj.com/article/73508.html)
> 概要: 株式会社一二三书房开始了NS游戏《探灵直播》的漫画版的连载。在这次公开的第一话中，可以看到主人公们来到废弃的旅馆，初遇玩偶服生物的片段。
### [FuRyu《自称贤者弟子的贤者》米菈1/7比例手办](https://news.dmzj.com/article/73509.html)
> 概要: FuRyu根据《自称贤者弟子的贤者》中的主人公米菈制作的1/7比例手办正在预订中。本作还原了人气画师藤原的插图，以黑色和白色为主色调的服装和随风飘扬的长发都进行了细致的再现。
### [AMD RX 6500显卡首次曝光 1000块钱就能拿下？](https://www.3dmgame.com/news/202202/3834989.html)
> 概要: AMD日前发布了两款入门级新卡，其中RX 6500 XT价格1599元起，不过显存、编解码方面的过度阉割使之“恶评如潮”，而另一个RX 6400仅供OEM领域，并不参与零售。据悉，除了高端升级RX 6......
### [能和“老消费”学什么 | 畅销100年的饼干巨头卡夫](https://www.tuicool.com/articles/JnY36f3)
> 概要: 「能和“老消费”学什么？」系列由进击波财经旗下湃动研究院倾力推出。我们希望在这里，重新回顾“老消费”崛起之路，探寻它们的变与不变，旨在为大家抽丝剥茧，梳理清楚那些穿越牛熊的消费品企业都做对了什么。我们......
### [热闹年夜饭，没落餐饮人](https://www.tuicool.com/articles/3iAfq2F)
> 概要: 春节期间，阖家团圆，伴随着铺天盖地的炮竹声，一年一度的节日再次席卷全国各地，每一个家庭也都上演着各自的热闹与欢喜。这个时候，也通常是餐饮行业举足轻重的时节，包括年夜饭在内的春节档，几乎是所有餐厅全年盈......
### [羽生结弦去哪儿了？](https://www.huxiu.com/article/495675.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。世上就是有这么一种天生的主角，人还没出场......
### [赫兰德表示希望看到加菲尔德再演《超凡蜘蛛侠3》](https://www.3dmgame.com/news/202202/3834992.html)
> 概要: 在最近的一次媒体采访时，汤姆·赫兰德表示自己非常期待安德鲁·加菲尔德能回归《蜘蛛侠》系列出演《超凡蜘蛛侠3》。这次采访主要是为宣传赫兰德的新片《神秘海域》，而在交谈过程中，话题来到了《蜘蛛侠：英雄无归......
### [传统鞭炮消失，电子鞭炮能取代么？](https://www.huxiu.com/article/495702.html)
> 概要: 本文来自微信公众号：数据线SJX（ID：shujuxian_kuaikan），作者：李童 孟令稀，头图来自：视觉中国没有鞭炮声的春节该怎么过？随着多地发布“禁燃令”，“爆竹声中一岁除”的传统习俗被慢慢......
### [视频：台媒曝阿娇前夫赖弘国新任未婚妻已怀孕 女方是空姐](https://video.sina.com.cn/p/ent/2022-02-05/detail-ikyakumy4334150.d.html)
> 概要: 视频：台媒曝阿娇前夫赖弘国新任未婚妻已怀孕 女方是空姐
### [视频：成龙双手竖大拇指为张艺谋点赞 冬奥开幕式播出后众星自豪发文](https://video.sina.com.cn/p/ent/2022-02-05/detail-ikyamrmz9162828.d.html)
> 概要: 视频：成龙双手竖大拇指为张艺谋点赞 冬奥开幕式播出后众星自豪发文
### [国家发改委：加快推进体育公园建设 促进家政服务业提质扩容](https://finance.sina.com.cn/china/bwdt/2022-02-05/doc-ikyakumy4339648.shtml)
> 概要: 原标题：国家发改委：加快推进体育公园建设 促进家政服务业提质扩容 证券时报e公司讯，国家发改委社会司5日发文指出，新的一年，推动生活性服务业补短板上水平...
### [英伟达 MX 550/570、RTX 2050 显卡获 Linux 驱动支持](https://www.ithome.com/0/601/759.htm)
> 概要: IT之家2 月 5 日消息，去年 12 月，英伟达发布了 RTX 2050、MX 570 和 MX 550 三款笔记本独显，其中 RTX 2050、MX 570 采用 RTX 3050 同款的 GA ......
### [“灯厂”黯然退市，玩家的信仰或许也救不了雷蛇](https://www.tuicool.com/articles/mYBfiyB)
> 概要: 说起雷蛇，相信绝大多数玩家都听过这个名字，而这家诨号“灯厂”的游戏设备品牌，也是过去二十年里游戏行业中的一个重要角色。然而在千禧一代和E世代先后成为社会中坚力量，游戏行业特别是电竞蓬勃发展时，雷蛇却要......
### [视频：开幕式出乎意料 张艺谋凌晨四点想到点火方案兴奋到睡不着](https://video.sina.com.cn/p/ent/2022-02-05/detail-ikyakumy4353544.d.html)
> 概要: 视频：开幕式出乎意料 张艺谋凌晨四点想到点火方案兴奋到睡不着
### [张艺谋罕见打出满分，图像算法、裸眼3D拉开冬奥会科技美学盛宴](https://www.tuicool.com/articles/ZRnuQrR)
> 概要: 2月4日晚间，国家体育场“虚拟鸟巢”首次呈现在北京2022年冬奥会上，给全球亿万观众带来极致享受的视觉盛宴。这也是全球第一次规模化应用8K技术来进行开幕式直播和重点赛事报道。在谈及开幕式时，冬奥会开闭......
### [阿里自营，淘宝心选 × 菠萝君迷你热敷筋膜枪 189 元（3.8 折）](https://lapin.ithome.com/html/digi/601765.htm)
> 概要: 阿里官方自营，淘宝心选 × 菠萝君迷你热敷筋膜枪报价 399 元，限时限量 210 元券，实付 189 元包邮，领券并购买。吊牌价 499 元，相当于 3.8 折优惠。淘宝心选定制机型，官方旗舰店类似......
### [国家发改委：适度超前开展基础设施投资](https://finance.sina.com.cn/china/bwdt/2022-02-05/doc-ikyakumy4363719.shtml)
> 概要: 财联社2月5日电，国家发展改革委有关负责人日前表示，适度超前开展基础设施投资，扎实推动“十四五”规划102项重大工程项目实施。推进新型基础设施建设...
### [国家发改委：加快发展长租房市场 推进保障性住房建设](https://finance.sina.com.cn/china/bwdt/2022-02-05/doc-ikyamrmz9191090.shtml)
> 概要: 财联社2月5日电，国家发展改革委有关负责人日前表示，扎实推进共同富裕，扩大中等收入群体，提高居民收入水平。保障新就业形态劳动者获得合理劳动报酬...
### [国家发改委：当前宏观调控要突出稳字当头](https://finance.sina.com.cn/china/bwdt/2022-02-05/doc-ikyakumy4364742.shtml)
> 概要: 财联社2月5日电，国家发展改革委有关负责人日前表示，当前宏观调控要突出稳字当头，宏观政策要稳健有效。保持宏观政策连续性、稳定性、可持续性，提高前瞻性、针对性...
### [大招要来了？浙江卫健委重磅调查：每个月补贴1000块钱，愿意生二胎/三胎吗？多地已出手，动心了？](https://finance.sina.com.cn/china/dfjj/2022-02-05/doc-ikyamrmz9192130.shtml)
> 概要: 来源：中国基金报 每个月补贴1000块钱，你愿意生二胎/三胎吗？ 这个春节，浙江省卫生健康委在做一份调查——《浙江省3岁以下婴幼儿养育成本调查问卷》，问卷显示...
### [小县城里的劳务中介：经营近十年，就数去年收益最好](https://finance.sina.com.cn/china/dfjj/2022-02-05/doc-ikyakumy4366160.shtml)
> 概要: 原标题：小县城里的劳务中介：经营近十年，就数去年收益最好 2021年阿鑫的招工人数比前几年增加了3倍左右，收入增加了2倍左右。 在江西省抚州市南丰县...
### [影味习俗丨初五如何正确迎财神，想招财莫要错过此良机](https://new.qq.com/rain/a/ENT2022013100218100)
> 概要: 影味习俗丨初五如何正确迎财神，想招财莫要错过此良机
### [那年春节，志愿军们也在冰天雪地的前线吃上了热饺子…](https://new.qq.com/rain/a/20220204V05J3100)
> 概要: 那年春节，志愿军们也在冰天雪地的前线吃上了热饺子…
### [海贼王1039话，大妈被击败是合理的，因为他的对手是基德和罗两人](https://new.qq.com/omn/20220205/20220205A095C500.html)
> 概要: 我们知道这周更新了海贼王最新一期的漫画1039话，            四皇大妈被基德和罗打败很正常不过对于基德和罗两人打败了四皇大妈，比如之前路飞被四皇凯多一棒子撂倒直接昏迷了过去，还有就是赤霄九......
# 小说
### [能力学院](http://book.zongheng.com/book/884295.html)
> 作者：破幻

> 标签：奇幻玄幻

> 简介：三千年后的地球，经历了许许多多的灾难，拥有着先进的科技，已经在两千年前改变人类基因的荒，让人类拥有着特殊的属性以及能力分布，而作为最难掌控的时间能力和空间能力出现在一位少年身上，这会发生什么呢？

> 章节末：完结篇

> 状态：完本
# 论文
### [An Empirical Survey of the Effectiveness of Debiasing Techniques for Pre-Trained Language Models | Papers With Code](https://paperswithcode.com/paper/an-empirical-survey-of-the-effectiveness-of)
> 日期：16 Oct 2021

> 标签：None

> 代码：None

> 描述：Recent work has shown that pre-trained language models capture social biases from the text corpora they are trained on. This has attracted attention to developing techniques that mitigate such biases.
### [Robust Pose Estimation in Crowded Scenes with Direct Pose-Level Inference | Papers With Code](https://paperswithcode.com/paper/robust-pose-estimation-in-crowded-scenes-with)
> 日期：NeurIPS 2021

> 标签：None

> 代码：None

> 描述：Multi-person pose estimation in crowded scenes is challenging because overlapping and occlusions make it difficult to detect person bounding boxes and infer pose cues from individual keypoints. To address those issues, this paper proposes a direct pose-level inference strategy that is free of bounding box detection and keypoint grouping.
