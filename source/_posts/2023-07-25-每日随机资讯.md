---
title: 2023-07-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LasLagunas_ZH-CN9917702340_1920x1080.webp&qlt=50
date: 2023-07-25 22:26:13
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LasLagunas_ZH-CN9917702340_1920x1080.webp&qlt=50)
# 新闻
### [The Health Benefits of Bitter Melon](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1504950116361&cate_id=-1)
> 概要: 从管理血糖到降低胆固醇，这种水果是一个万能的......
### [如何避免“长寿却不健康”？老年朋友这样锻炼更科学](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2335304867861&cate_id=-1)
> 概要: 国家卫生健康委、国家体育总局、国家中医药局三部门24日起，联合开展为期一周的全国老年健康宣传周活动......
### [国际Castleman病日丨关注“铃铛病”患者 提升血液罕见病诊疗水平](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2672627572740&cate_id=-1)
> 概要: 有一种非常罕见的血液疾病，叫Castleman病（CD），这是一种淋巴结过度增生，炎症反应相关的血液系统的罕见病，以单个或者多个部位的淋巴结肿大为特点，部分患者可伴有全身症状和（或）多系统受累。201......
### [Exposure to Extreme Temperatures, Pollution Linked to MI Death](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx3790560583703&cate_id=-1)
> 概要: 2023年7月24日，星期一-根据7月25日出版的《循环》杂志上发表的一项研究，暴露于极端温度事件（ETEs）和细颗粒物（PM2.5）与心肌梗死（MI）死亡率相关......
### [重磅！中南湘雅医院发现肝癌治疗新靶点](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx5041469808657&cate_id=-1)
> 概要: <strong>导读：</strong>表观遗传调控因子和蛋白质翻译后修饰在多种癌症细胞死亡中发挥重要作用，包括铁死亡(一种非凋亡形式的细胞死亡)。然而，染色质修饰因子和去泛素化酶(DUB)在铁死亡中......
### [企业微信的春天来了吗？](https://www.woshipm.com/it/5872793.html)
> 概要: 现在，商家和平台都逐渐重视起了私域，并围绕私域进行布局，比如抖音推出了面向抖客的“超级红包”和分佣政策，私域电商平台快团团也借私域取得了增长。那么坐拥微信这一流量池的企业微信，又可能面临怎样的机遇或挑......
### [北上广Citywalk火爆背后：旅游“特种兵”付费百元为遛弯，小而美生意难成持续业务](https://www.woshipm.com/it/5873035.html)
> 概要: 当下最火的旅游形式从特种兵旅行变成了City walk。本篇文章详细介绍了怎么抓住City walk爆火的机会和能火多久，推荐想了解营销的同学阅读。暑期游仍在火热进行时，当下最火的旅游形式，则从特种兵......
### [谁能挑战拼多多？](https://www.woshipm.com/evaluating/5872413.html)
> 概要: 拼多多是目前电商领域发展最为火热的app之一，并且深受消费者喜欢。这篇文章的作者就用三个步骤详细地分析了拼多多的发展战略，我们一起来看看吧。拼多多位列头部电商平台阵营之后，几年时间里，增长势头依旧不减......
### [海外科技巨头“AI成色”迎业绩检验 中国BAT能否受益](https://finance.sina.com.cn/china/2023-07-25/doc-imzcvmfh1773908.shtml)
> 概要: 作者： 周艾琳......
### [苹果给开发者尝鲜：上海等地现在可借用Vision Pro，明年归还](https://finance.sina.com.cn/stock/usstock/c/2023-07-25/doc-imzcvvva1559576.shtml)
> 概要: 来源：华尔街见闻......
### [谷歌去年消耗212亿升水，人工智能导致用水量激增20%](https://finance.sina.com.cn/stock/usstock/c/2023-07-25/doc-imzcwaay1463340.shtml)
> 概要: 谷歌刚刚发布了2023年环境报告，有一件事是肯定的：该公司的用水量正在飙升......
### [组图：21岁电竞选手叮当猫去世 疑因酒后下海捞手机不幸溺亡](http://slide.ent.sina.com.cn/star/w/slide_4_704_387392.html)
> 概要: 组图：21岁电竞选手叮当猫去世 疑因酒后下海捞手机不幸溺亡
### [育碧澄清：不会删除购买过游戏的不活跃账号](https://www.3dmgame.com/news/202307/3874056.html)
> 概要: 之前育碧曾表示会删除长时间未登录的账号，该政策引发许多玩家不满。现在据外媒PCGamer报道，育碧已做出更改，不会删除购买过游戏的不活跃账号。最近一位反DRM的玩家在推特上称，育碧会以账户删除通知为证......
### [TV动画《王者天下 第五季》公开最新情报！](https://news.dmzj.com/article/78749.html)
> 概要: TV动画《王者天下 第五季》公开最新视觉图和角色形象，本作预计于2024年1月开播。
### [《动物派对》将举行多次游戏测试 参加科隆游戏展](https://www.3dmgame.com/news/202307/3874067.html)
> 概要: 国产多人派对游戏《猛兽派对》（动物派对）发布了游戏开发日志07，这次的内容主要涉及服务器测试、皮肤换色、游戏成就、教学系统、隐私保护、Xbox审核和国内外展会，要点如下：·8月还将进行两次压力测试，9......
### [P站美图推荐——阳伞特辑（二）](https://news.dmzj.com/article/78750.html)
> 概要: 好——晒——啊——
### [忽悠变成现实 曾经拔签救人欺诈广告真实版游戏上线](https://www.3dmgame.com/news/202307/3874070.html)
> 概要: 曾经有一阵几乎各大小平台都出现了一种欺诈广告，虽然包装各有不同，但核心都是拔掉签子拯救什么，但当用户点进去一探究竟你会发现被恶心到了，现在有开发者推出了现实版游戏《尝试将曾经常见的那种游戏用火柴棒小人......
### [组图：Angelababy称《尘缘》拍了两版结局 和马天宇都想看he](http://slide.ent.sina.com.cn/tv/slide_4_704_387402.html)
> 概要: 组图：Angelababy称《尘缘》拍了两版结局 和马天宇都想看he
### [上诉人吴亦凡强奸聚众淫乱案二审开庭 将择期宣判](https://ent.sina.com.cn/s/m/2023-07-25/doc-imzcwnsv8078799.shtml)
> 概要: 新浪娱乐讯 2023年7月25日，北京市第三中级人民法院依法开庭审理了上诉人吴亦凡强奸、聚众淫乱一案。因涉及被害人隐私，案件依法采取不公开开庭审理方式。　　2022年11月25日，北京市朝阳区人民法院......
### [KKR为什么反复强调“保持简单”？](https://36kr.com/p/2359212461901444)
> 概要: KKR为什么反复强调“保持简单”？-36氪
### [对话德邦证券首席经济学家芦哲：经济是否会很快触底反弹？未来有哪些投资机会？](https://36kr.com/p/2359046966360065)
> 概要: 对话德邦证券首席经济学家芦哲：经济是否会很快触底反弹？未来有哪些投资机会？-36氪
### [雷石投资荣获「融中2022-2023年度中国最佳政府引导基金TOP50」](http://www.investorscn.com/2023/07/25/109126/)
> 概要: 2023年7月18日-20日，融中传媒举办的融中2023（第六届）有限合伙人峰会顺利召开，会上发布了“2022-2023年度有限合伙人榜”等多份极具重量的奖项。凭借优秀的业绩，雷石投资荣获“中国最佳政......
### [用碎片化时间抹去“身材焦虑”，AEKE化身居家运动小管家](http://www.investorscn.com/2023/07/25/109128/)
> 概要: “身材焦虑”影响生活,影响健康......
### [海外new things | 韩国时尚电商「Musinsa」C轮融资1.9亿美元，估值超过3万亿韩元](https://36kr.com/p/2355581387537156)
> 概要: 海外new things | 韩国时尚电商「Musinsa」C轮融资1.9亿美元，估值超过3万亿韩元-36氪
### [德华安顾人寿智能双录系统：创新驱动 客户至上](http://www.investorscn.com/2023/07/25/109131/)
> 概要: 在保险行业各级监管规定下,保险销售过程关键环节要通过录音录像方式,对业务人员的销售行为进行可回溯记录,以确保合规性和保护消费者权益。为了满足监管要求的同时助力业务员展业,德华安顾人寿以创新的科技手段为......
### [剧场版《美少女战士Cosmos》公布特别映像](https://news.dmzj.com/article/78751.html)
> 概要: 根据武内直子原作改编的剧场版动画《美少女战士Cosmos》公布了特别映像《Happy Marriage Song PV》。
### [【西海岸】A-史密斯：批评利拉德的可以闭嘴了，他坚守了11年](https://bbs.hupu.com/61375230.html)
> 概要: 【西海岸】A-史密斯：批评利拉德的可以闭嘴了，他坚守了11年
### [比亚迪在沈阳成立方程豹销售公司](http://www.investorscn.com/2023/07/25/109138/)
> 概要: 【#比亚迪在沈阳成立方程豹销售公司#】......
### [Steam最新一周销量榜 《遗迹2》成功登顶](https://www.3dmgame.com/news/202307/3874097.html)
> 概要: Steam最新一周销量榜出炉（2023年7月18日-2023年7月25日），即将于7月26日发售的《遗迹2》成功登顶，上一期的榜一Steam Deck掉到了第五，另外，那个游戏5本周再次上榜（第六）......
### [Sensor Tower：2023 上半年中国手游海外吸金超 75 亿美元，占海外手游总收入 24%](https://www.ithome.com/0/708/073.htm)
> 概要: 感谢IT之家网友某咸鱼的小号的线索投递！IT之家7 月 25 日消息，移动应用数据分析机构 Sensor Tower 最新发布的报告显示，今年上半年，中国手游海外吸金超过 75 亿美元（IT之家备注：......
### [极空间 NAS 私有云硬件虚拟机功能上线：支持安装 Win / Linux、可远程连接](https://www.ithome.com/0/708/071.htm)
> 概要: IT之家7 月 25 日消息，据极空间私有云官方公众号表示，目前极空间私有云虚拟机功能已经正式上线，官方称“这一虚拟机支持多种操作系统，包括各种版本 Windows、爱快、iStore、多种版本 Li......
### [克鲁赛德战记或将停运 漫画连载在即莫名被砍](https://news.dmzj.com/article/78756.html)
> 概要: 天命影响克鲁赛德战记或将停运、讲谈社漫画连载在即莫名被砍
### [菜鸟驿站数智商业总经理苏道：在不确定性的世界里抓住确定性，让品牌迎风而上丨WISE 2023未来品牌大会](https://36kr.com/p/2359255339531779)
> 概要: 菜鸟驿站数智商业总经理苏道：在不确定性的世界里抓住确定性，让品牌迎风而上丨WISE 2023未来品牌大会-36氪
### [雅达利研究者分享奇遇 捡到几乎全新立体飞盘街机](https://www.3dmgame.com/news/202307/3874104.html)
> 概要: Tim Lapetino是一位资深游戏业界研究者，特别是曾著有雅达利研究书籍《Art of Atari》，没想到近日在部落格上分享了奇遇，竟然捡到了经典街机游戏《立体飞盘（Discs of Tron）......
### [金融、地产定调，A股大涨，持续性如何？](https://www.yicai.com/video/101818178.html)
> 概要: 金融、地产定调，A股大涨，持续性如何？
### [从荣获五项ADMEN国际大奖，透视舍得酒业2023年上半年立体式品牌营销路径](http://www.investorscn.com/2023/07/25/109143/)
> 概要: 7月18日，Y2Y品牌年轻节暨2023 ADMEN国际大奖颁奖盛典在北京举行。舍得酒业凭借《舍得智慧人物》自有品牌IP、《美味几食友》节目合作、2023年舍得酒春节新媒体社会化传播、舍得酒业重庆地区媒......
### [程实：数字经济时代，资本市场与金融监管体系变革](https://www.yicai.com/news/101818194.html)
> 概要: 中国以资本市场与金融监管体系变革为钥，为中国经济高质量发展提供更加稳定高效的金融支持。
### [长城汽车起诉六个涉嫌严重侵权账号，最高索赔 500 万元](https://www.ithome.com/0/708/124.htm)
> 概要: 感谢IT之家网友西窗旧事、月桂使者、航空先生、张子枫我老婆、霜风神影的线索投递！IT之家7 月 25 日消息，IT之家从长城汽车公众号获悉，近日，长城汽车已正式启动针对“向北不断电”、“向东北”、“大......
### [6月末深圳跨境电商融资余额同比增长197%](https://www.yicai.com/news/101818264.html)
> 概要: 6月末，深圳辖内中资银行外贸企业融资余额1.2万亿元，同比增长34%，中小外贸企业融资余额同比增长53%。
### [等等党大胜：匹克路威篮球鞋 75 元官方新低（上市价 469 元）](https://lapin.ithome.com/html/digi/708127.htm)
> 概要: 天猫【匹克官方旗舰店】匹克 闪电系列 路威篮球鞋 门店售价 469 元，今日直降至 239 元。叠加 140 元大额券，实付 99 元清仓好价。88VIP 可再减 24 元，抵扣后仅需 74 元破冰新......
### [【招商策略】哪些行业景气好转支撑了经济企稳？——行业比较与景气跟踪系列（2023年7月）](https://finance.sina.com.cn/stock/stockzmt/2023-07-25/doc-imzcxiwm7727596.shtml)
> 概要: 二季度整体经济复苏力度与市场预期相比具有一定的差异，但是6月单月经济数据显示出筑底特征，部分领域景气企稳甚至回暖迹象明显。
### [流言板老鹰社媒与球迷互动：谁是队史上最被低估的球员？](https://bbs.hupu.com/61380339.html)
> 概要: 虎扑07月25日讯 今日老鹰官方更新社媒动态，与球迷互动。“谁是老鹰队史上最被低估的球员？”老鹰官方写道。   来源： Twitter
### [【直播】西卡：四强就是常规赛前四吗？那说明常规赛很有含金量](https://bbs.hupu.com/61380359.html)
> 概要: 【直播】西卡：四强就是常规赛前四吗？那说明常规赛很有含金量
### [流言板23世界赛LPL赛区积分：WBG确定60分，掉入冒泡赛败者组](https://bbs.hupu.com/61380377.html)
> 概要: 虎扑07月25日讯 【2023全球总决赛积分——LPL赛区即时积分】JDG：90 + 60 = 150【进入资格赛】BLG：70 + 60 = 130【进入资格赛】EDG：50 + 40 = 90【进
### [“来了就是江门人”！这份“见面礼”，真贴心！](https://finance.sina.com.cn/jjxw/2023-07-25/doc-imzcxiwk0948892.shtml)
> 概要: 转自：江门发布白水带下，麻园片区。江翠路霓虹闪烁，车水马龙；路旁的安定里闹中有静，曲径通幽，陈泽佳租住的房子就在这里。小屋30平方米不到，一房一厅一厨卫...
### [京东再战社区团购 京喜拼拼改名京东拼拼](https://finance.sina.com.cn/jjxw/2023-07-25/doc-imzcxiwm7727062.shtml)
> 概要: 转自：北京商报北京商报讯（记者 何倩）7月25日，北京商报记者在京喜拼拼小程序获悉，自7月27日起，京喜拼拼将正式更名为京东拼拼。
### [流言板勇士官方祝福名宿内特-瑟蒙德82岁诞辰快乐](https://bbs.hupu.com/61380567.html)
> 概要: 虎扑07月25日讯 今日勇士官方更新社媒动态，祝福名宿内特-瑟蒙德82岁诞辰快乐。瑟蒙德在1963年选秀大会上首轮第3顺位被选中，生涯期间共7次入选全明星和5次最佳防守阵容，于1985年入选名人堂。
### [每人收费10800元进北大校园？校方通报违规预约入校行为](https://finance.sina.com.cn/wm/2023-07-25/doc-imzcxiwp9454996.shtml)
> 概要: 全文2391字，阅读约需4分钟 新京报记者 杨菲菲 刘洋 编辑 缪晨霞 校对 翟永军7月24日，北京大学发布《校友预约入校违规情况通报》称...
### [夏夜，四合院里传出美妙乐声，北京国安等和市民参加慈善义捐义演义拍](https://finance.sina.com.cn/jjxw/2023-07-25/doc-imzcxiwm7728470.shtml)
> 概要: 转自：上观新闻7月24日晚，北京市东城区美术馆后街的一座修葺一新的四合院中传出阵阵美妙的乐声，引来附近居民纷纷前来观看。原来...
# 小说
### [我的次元群友](http://book.zongheng.com/book/1027347.html)
> 作者：佛鱼梦想

> 标签：科幻游戏

> 简介：一时失意，终生遗憾。偶然获得聊天群系统，林崇一步步走向救赎之路。

> 章节末：第74章 诡异生物

> 状态：完本
# 论文
### [Stress-Testing LiDAR Registration | Papers With Code](https://paperswithcode.com/paper/stress-testing-lidar-registration)
> 概要: Point cloud registration (PCR) is an important task in many fields including autonomous driving with LiDAR sensors. PCR algorithms have improved significantly in recent years, by combining deep-learned features with robust estimation methods. These algorithms succeed in scenarios such as indoor scenes and object models registration. However, testing in the automotive LiDAR setting, which presents its own challenges, has been limited. The standard benchmark for this setting, KITTI-10m, has essentially been saturated by recent algorithms: many of them achieve near-perfect recall. In this work, we stress-test recent PCR techniques with LiDAR data. We propose a method for selecting balanced registration sets, which are challenging sets of frame-pairs from LiDAR datasets. They contain a balanced representation of the different relative motions that appear in a dataset, i.e. small and large rotations, small and large offsets in space and time, and various combinations of these. We perform a thorough comparison of accuracy and run-time on these benchmarks. Perhaps unexpectedly, we find that the fastest and simultaneously most accurate approach is a version of advanced RANSAC. We further improve results with a novel pre-filtering method.
### [Delving Globally into Texture and Structure for Image Inpainting | Papers With Code](https://paperswithcode.com/paper/delving-globally-into-texture-and-structure)
> 日期：17 Sep 2022

> 标签：None

> 代码：https://github.com/htyjers/dgts-inpainting

> 描述：Image inpainting has achieved remarkable progress and inspired abundant methods, where the critical bottleneck is identified as how to fulfill the high-frequency structure and low-frequency texture information on the masked regions with semantics. To this end, deep models exhibit powerful superiority to capture them, yet constrained on the local spatial regions. In this paper, we delve globally into texture and structure information to well capture the semantics for image inpainting. As opposed to the existing arts trapped on the independent local patches, the texture information of each patch is reconstructed from all other patches across the whole image, to match the coarsely filled information, specially the structure information over the masked regions. Unlike the current decoder-only transformer within the pixel level for image inpainting, our model adopts the transformer pipeline paired with both encoder and decoder. On one hand, the encoder captures the texture semantic correlations of all patches across image via self-attention module. On the other hand, an adaptive patch vocabulary is dynamically established in the decoder for the filled patches over the masked regions. Building on this, a structure-texture matching attention module anchored on the known regions comes up to marry the best of these two worlds for progressive inpainting via a probabilistic diffusion process. Our model is orthogonal to the fashionable arts, such as Convolutional Neural Networks (CNNs), Attention and Transformer model, from the perspective of texture and structure information for image inpainting. The extensive experiments over the benchmarks validate its superiority. Our code is available at https://github.com/htyjers/DGTS-Inpainting.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
