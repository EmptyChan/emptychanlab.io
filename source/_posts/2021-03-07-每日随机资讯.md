---
title: 2021-03-07-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Wakodahatchee_EN-CN9780170147_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-03-07 20:48:05
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Wakodahatchee_EN-CN9780170147_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Dolt is Git for Data: a SQL database that you can fork, clone, branch, merge](https://github.com/dolthub/dolt#dolt)
> 概要: Dolt is Git for Data: a SQL database that you can fork, clone, branch, merge
### [妙投书单：做投资不懂周期，就没法赢](https://www.huxiu.com/article/413400.html)
> 概要: 太阳底下没有新鲜事，投资领域尤其如此。每当我们说“这次不一样”时，现实总会跳出来打脸——这次还一样，白天之后还是黑夜，大涨之后必有大跌。历史虽然不会重演，但总是押着同样的韵脚。在经济活动、企业活动以及......
### [好评如潮！《循环勇者》上市一天销量突破15万](https://www.3dmgame.com/news/202103/3809955.html)
> 概要: 俄罗斯独立游戏工作室Four Quarters昨日在Steam平台正式发售了他们的新游戏《循环勇者》（Loop Hero）。本作由Devolver Digital发行，是一款卡牌roguelike游戏......
### [「数码宝贝大冒险：」新ED「オーバーシーズ・ハイウェイ」MV公开](http://acg.178.com//202103/409084667160.html)
> 概要: 由富士电视台、读卖广告社、东映动画企划并制作的电视动画「数码宝贝大冒险：」公开了第39话ED「オーバーシーズ・ハイウェイ」的MV 动画，该作品已于2020年4月5日播出。「オーバーシーズ・ハイウェイ」......
### [「约会大作战“DATE A BULLET MUSIC”」专辑封面及特典绘图公开](http://acg.178.com//202103/409084951184.html)
> 概要: 动画「约会大作战」的音乐专辑「约会大作战“DATE A BULLET MUSIC”」公开了封面图及购买者特典绘图，该专辑将于3月24日正式发售，售价为2273日元（不含税）......
### [一反差评 港媒曝罗志祥或借"为医护加油"成功复出](https://ent.sina.com.cn/s/h/2021-03-07/doc-ikknscsh8906476.shtml)
> 概要: 新浪娱乐讯 据港媒报道，歌手罗志祥被曝出丑闻后人气跌落谷底，事发至今差差不多一年，此前他化身清洁工做公益，却引发网友怒骂，收到众多差评。　　近日，罗志祥再次为复出铺路，6日他化身“粉红猪”，身上挂满“......
### [「冰冻荒原」版Linux内核有bug，不要使用！Linux之父紧急警告并发布5.12-rc2版本](https://www.tuicool.com/articles/3amaQ3B)
> 概要: 2 月份，受恶劣天气影响，美国多个地区出现大规模断电。Linux 之父 Linus Torvalds 所在的俄勒冈州波特兰地区也没有幸免。但比较励志的是，即使经历了六天的断电生活，Linus Torv......
### [陈薇：提议建设特需疫苗国家技术创新中心](https://www.ithome.com/0/538/582.htm)
> 概要: IT之家3月7日消息 据央视新闻报道，全国政协十三届四次会议第二次全体会议3 月 7 日上午 9 时在人民大会堂举行，委员们进行了发言。大会上，全国政协委员、中国工程院院士陈薇今天在政协大会发言中提出......
### [动画「侦探已经死了。」公开特报PV](http://acg.178.com//202103/409096017752.html)
> 概要: 动画「侦探已经死了。」官方公开了特报PV，宣布本作将于今年7月份开始播出。TV动画「侦探已经死了。」特报PV【CAST】君冢君彦：长井新シエスタ：宫下早纪夏凪渚：竹达彩奈斎川唯：高尾奏音シャーロット・......
### [NCSOFT官方庆祝《剑灵2》预约注册玩家达400万](https://www.3dmgame.com/news/202103/3809976.html)
> 概要: NCSOFT于近日宣布，旗下新作MMORPG《剑灵2》（PC / iOS / Android）在韩国预约注册人数23日已突破400万，官方表示，这是韩国MMORPG中有史以来最快的纪录。而NCSOFT......
### [TV动画《不是真正同伴的我》新预告 确定7月开播](https://www.3dmgame.com/news/202103/3809977.html)
> 概要: 根据著名长名轻小说改编的TV动画《不是真正同伴的我被逐出了勇者队伍,因此决定在边境过上慢生活》官方3月7日今天宣布将于7月正式开播，同时公开了首弹预告，一起来先睹为快。·《不是真正同伴的我被逐出了勇者......
### [全球面临沙子资源枯竭 CPU晶圆也会涨价？不至于](https://www.3dmgame.com/news/202103/3809978.html)
> 概要: 最近“全球面临沙子资源枯竭”问题的报道又被不少媒体拿到台面上来翻炒。根据报道显示，在过去的20年间，全球范围沙子的使用量已经增加两倍。相关数据显示，每生产一吨水泥需要10吨沙子，单是建筑工程，全球每年......
### [TV动画《我们的重置人生》PV第1弹放出](https://news.dmzj1.com/article/70279.html)
> 概要: 轻改TV动画《我们的重置人生》PV第1弹放出，动画将于2021年7月开始放送。
### [《里亚德录大地》TV动画制作阵容 穿越游戏世界](https://www.3dmgame.com/news/202103/3809980.html)
> 概要: Ceez原作幻想穿越系轻小说名作《里亚德录大地》不久官方宣布将制作TV动画，具体开播日期未定，近日官方公开了制作阵容，一起来了解下。·《里亚德录大地》讲述了由于遭逢意外，身体只能依靠维生系统活著的少女......
### [TV动画《侦探已死。》特报PV公开](https://news.dmzj1.com/article/70280.html)
> 概要: 轻改TV动画《侦探已死。》特报PV公开，动画将于2021年7月开播。
### [《因为不是真正的伙伴而被逐出勇者队伍，流落到边境展开慢活人生》PV放出](https://news.dmzj1.com/article/70281.html)
> 概要: 轻改TV动画《因为不是真正的伙伴而被逐出勇者队伍，流落到边境展开慢活人生》PV第一弹放出
### [Spark 实践 | Spark SQL 查询 Parquet 文件性能提升 30%，字节跳动是如何做到的？](https://www.tuicool.com/articles/Rji2qeU)
> 概要: 本文来自11月举办的Data + AI Summit 2020（原 Spark+AI Summit），主题为《Improving Spark SQL Performance by 30%: How W......
### [组图：李振宁现身机场收获一大束鲜花 双手抱住可爱十足](http://slide.ent.sina.com.cn/star/slide_4_86448_353603.html)
> 概要: 组图：李振宁现身机场收获一大束鲜花 双手抱住可爱十足
### [央视热评：“奋力一跳” 把全身力气恰到好处地使出来](https://finance.sina.com.cn/china/2021-03-07/doc-ikkntiak5837941.shtml)
> 概要: 原标题：热评·解读“十四五”丨“奋力一跳”：把全身力气恰到好处地使出来 观两会，话蓝图。两会内外，正热议“十四五”规划和2035年远景目标纲要草案。
### [周星驰吊唁吴孟达，被影迷威胁，20年恩怨已经释然？](https://new.qq.com/omn/20210307/20210307A0757U00.html)
> 概要: 知名演员吴孟达先生的葬礼今日在红磡世界殡仪馆举办，作为圈内人也是来了不少，更受到关注的便是周星驰了，周星驰此次前来吊唁，也说明两人长达20年的恩怨就此画上了一个句号，人都不在了，想必周星驰内心也是有很......
### [组图：刘亦菲春日紫樱蛋糕裙浪漫文艺 直播镜头下甜美十足状态佳](http://slide.ent.sina.com.cn/star/slide_4_704_353607.html)
> 概要: 组图：刘亦菲春日紫樱蛋糕裙浪漫文艺 直播镜头下甜美十足状态佳
### [楚天科技董事长唐岳：科技创新不能吃快餐、图省事、加杠杆](https://finance.sina.com.cn/china/2021-03-07/doc-ikkntiak5840747.shtml)
> 概要: 聚焦春天的盛会。7日，据两会日程，人代会审查计划报告和草案、预算报告和草案；政协举行第二次全体会议，委员进行大会发言。（记者熊争艳，海报设计：张晨光）
### [王金南院士：“十四五”时期要瞄准两个“基本消除”](https://finance.sina.com.cn/china/2021-03-07/doc-ikkntiak5840933.shtml)
> 概要: 聚焦春天的盛会。7日，据两会日程，人代会审查计划报告和草案、预算报告和草案；政协举行第二次全体会议，委员进行大会发言。（记者高敬，海报设计：张晨光）
### [视频：少年可期！时代少年团成员马嘉祺艺考总分为186分](https://video.sina.com.cn/p/ent/2021-03-07/detail-ikkntiak5844618.d.html)
> 概要: 视频：少年可期！时代少年团成员马嘉祺艺考总分为186分
### [从扶贫、修路到半导体](https://www.huxiu.com/article/413455.html)
> 概要: 本文来自微信公众号：宁南山（ID：ningnanshan2017），作者：宁南山，题图来自视觉中国这是从一个节目发散的一点想法。看了央视之前的一期节目，讲重庆的一个国家级贫困县酉阳县脱贫，2017年的......
### [全国人大代表、五粮液集团李曙光：推动中国白酒成为世界白酒](https://finance.sina.com.cn/stock/s/2021-03-07/doc-ikkntiak5855471.shtml)
> 概要: 全国人大代表、五粮液集团董事长李曙光：推动中国白酒成为世界白酒 红星资本局原创 3月6日上午，十三届全国人大四次会议四川代表团在驻地举行全体会议...
### [Simple and privacy-friendly alternative to Google Analytic](https://github.com/plausible/analytics)
> 概要: Simple and privacy-friendly alternative to Google Analytic
### [“强大引力场”是什么？王一鸣详解“十四五”新提法](https://finance.sina.com.cn/china/2021-03-07/doc-ikknscsh9009839.shtml)
> 概要: 原标题：专注两会|“强大引力场”是什么？王一鸣详解“十四五”新提法 新华网北京3月7日电 “十四五”规划和2035年远景目标纲要草案提出...
### [罗云熙生图曝光干瘦认不出，一双筷子腿没手臂粗，与精修差距太大](https://new.qq.com/omn/20210307/20210307A07ZQA00.html)
> 概要: 这段时间《山河令》热度居高不下，同为耽改剧的《皓衣行》也备受关注，主演陈飞宇和罗云熙的一举一动都被放大。            3月7号，网上有人发帖感叹“救命，这也太瘦了吧”，放出一张罗云熙的偶遇生......
### [防范化解金融风险三年攻坚战收官 未来防风险有4个重点](https://finance.sina.com.cn/china/2021-03-07/doc-ikknscsh9012867.shtml)
> 概要: 2021年3月5日，《政府工作报告》中提及，“去年坚决打好三大攻坚战，主要目标任务如期完成”。 我国的三大攻坚战是“防范化解重大风险、精准脱贫、污染防治”。
### [吴孟达设灵日感人细节：星爷提前到达，大屏幕播放作品，花牌送不停](https://new.qq.com/omn/20210307/20210307A082ZH00.html)
> 概要: 3月7日，吴孟达亲友为其在殡仪馆地下礼堂设置灵堂，普通民众可前去公祭，明星好友们也纷纷送上花牌，家属也会在固定时间接受了媒体采访。                        在接受媒体采访之前，家......
### [郑恺独自现身酒吧年会，被美女搭话成焦点，满脸不悦被指不自在](https://new.qq.com/omn/20210307/20210307A06TAO00.html)
> 概要: 3月6日，上海某知名酒吧晒出一则视频，并透露：“一年一度的年会，老板郑恺亲临现场。”            众所周知，郑恺除了演艺事业风生水起，火锅店、酒吧等副业也发展得红红火火。          ......
### [魅族 18 系列明日开售：重 162g 的骁龙 888 小屏旗舰 4399 元起](https://www.ithome.com/0/538/624.htm)
> 概要: IT之家3月7日消息 魅族在3月3日发布了魅族 18Pro 以及魅族 18 两款机型，定位“年度极致旗舰”和“小屏满血旗舰”，这两款机型将于明日正式开售，已经预定的用户可在 0 点之后付尾款，Pro ......
### [才播2集就冲上8.4分，美剧又出惊艳之作](https://new.qq.com/omn/20210307/20210307A08O6M00.html)
> 概要: 《超人和露易斯》是由泰勒·霍奇林、比茜·图诺克领衔主演的现代背景独立超级英雄剧集。该剧由《闪电侠》剧集的制作人托德·赫尔宾负责剧本创作，主要探讨超人与露易斯如何应对当今社会以及身为在职父母所带来的生活......
### [视频：吴孟达胞弟接受采访“哥哥和周星驰一直很要好”](https://video.sina.com.cn/p/ent/2021-03-07/detail-ikkntiak5867188.d.html)
> 概要: 视频：吴孟达胞弟接受采访“哥哥和周星驰一直很要好”
# 小说
### [燕云怅恨录](http://book.zongheng.com/book/955914.html)
> 作者：枭笑生

> 标签：武侠仙侠

> 简介：有人资材超凡入圣，修得超人武功，便妄图以力压人，驱使天下英雄。有人连遭惨变，却始终认为人活世上，自由自在才是第一要义，若不能自家做自家的主，则生不如死。此两人如水与火难于共处，必至全力 拚斗，至死方休。

> 章节末：159........ 山中一夕话 03

> 状态：完本
# 论文
### [An Enhanced Adversarial Network with Combined Latent Features for Spatio-Temporal Facial Affect Estimation in the Wild](https://paperswithcode.com/paper/an-enhanced-adversarial-network-with-combined)
> 日期：18 Feb 2021

> 标签：CURRICULUM LEARNING

> 代码：https://github.com/deckyal/Seq-Att-Affect

> 描述：Affective Computing has recently attracted the attention of the research community, due to its numerous applications in diverse areas. In this context, the emergence of video-based data allows to enrich the widely used spatial features with the inclusion of temporal information.
### [SeqMix: Augmenting Active Sequence Labeling via Sequence Mixup](https://paperswithcode.com/paper/seqmix-augmenting-active-sequence-labeling)
> 日期：5 Oct 2020

> 标签：ACTIVE LEARNING

> 代码：https://github.com/rz-zhang/SeqMix

> 描述：Active learning is an important technique for low-resource sequence labeling tasks. However, current active sequence labeling methods use the queried samples alone in each iteration, which is an inefficient way of leveraging human annotations.
