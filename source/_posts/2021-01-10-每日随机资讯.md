---
title: 2021-01-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GoldenDragon_EN-CN8915120816_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-10 18:54:21
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GoldenDragon_EN-CN8915120816_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：宋茜疑似腿受伤步履蹒跚 获工作人员搀扶](http://slide.ent.sina.com.cn/star/slide_4_704_351001.html)
> 概要: 组图：宋茜疑似腿受伤步履蹒跚 获工作人员搀扶
### [组图：黄晓明宣布退出《浪姐2》 节目组声明称随时欢迎回来](http://slide.ent.sina.com.cn/z/v/slide_4_704_350993.html)
> 概要: 组图：黄晓明宣布退出《浪姐2》 节目组声明称随时欢迎回来
### [组图：吴尊带儿女玩水 Neinei穿泳装秀长腿Max高难度倒立](http://slide.ent.sina.com.cn/star/slide_4_704_351002.html)
> 概要: 组图：吴尊带儿女玩水 Neinei穿泳装秀长腿Max高难度倒立
### [组图：林允大花棉袄穿出潮流感 长发披肩上演“歪头杀”](http://slide.ent.sina.com.cn/star/slide_4_704_351000.html)
> 概要: 组图：林允大花棉袄穿出潮流感 长发披肩上演“歪头杀”
### [视频：为李菲儿发声？苏醒称不参加节目你养我啊](https://video.sina.com.cn/p/ent/2021-01-10/detail-iiznezxt1560230.d.html)
> 概要: 视频：为李菲儿发声？苏醒称不参加节目你养我啊
### [宋茜拍戏腿部受伤，走路一瘸一拐现身机场上车要助理搀扶](https://new.qq.com/omn/20210110/20210110A03RBX00.html)
> 概要: 1月10日有粉丝晒出了在机场接机宋茜的画面，只见落地杭州的宋茜走路步伐沉重，还有些一瘸一瘸的显然是受了伤。有粉丝问姐姐有没有好一些的时候，只见宋茜点了点头并没有说话，粉丝们纷纷跟随喊着“姐姐注意安全”......
### [吴尊带儿女玩水，Neinei穿泳装秀长腿，max挑战高难度倒立](https://new.qq.com/omn/20210110/20210110A0583L00.html)
> 概要: 1月10日，吴尊在个人社交平台晒出陪伴一双儿女在水上乐园玩耍的系列照片，并附文写道：“孩子快乐就是我们的幸福，大家周末愉快”。            老婆林丽吟镜头下，吴尊背着10岁女儿Neinei出......
### [恭喜！向佐前女友黄婉佩怀二胎晒孕照，不介意做富商丈夫孩子后妈](https://new.qq.com/omn/20210110/20210110A06J7K00.html)
> 概要: 本文编辑剧透社：issac未经授权严禁转载，发现抄袭者将进行全网投诉现年38岁的香港女演员、歌手，前组合2R成员黄婉佩（Race）早年自出道以来，就凭借甜美的形象深受粉丝的喜爱。在2016年黄婉佩宣布......
### [蔡少芬儿子正脸罕曝光！萌态十足已会走路，张晋带娃两处明显失误引热议](https://new.qq.com/omn/20210110/20210110A0833800.html)
> 概要: 知名女星蔡少芬与老公张晋结婚10多年，一直是娱乐圈里的模范夫妻。1月9日下午，张晋罕见在社交平台上发视频晒出1岁的小儿子，引来网友围观。            正值周末，张晋也带着自己的小儿子来到户外......
### [那英和老公孟桐组红绿CP默契足 “老夫老妻”一路亲密挽臂撒狗粮](https://new.qq.com/omn/20210110/20210110A083VA00.html)
> 概要: 那英和老公孟桐组红绿CP默契足 “老夫老妻”一路亲密挽臂撒狗粮
# 动漫
### [TV动画《因为太怕痛就全点防御力了》第二季2022年开播！](https://news.dmzj.com/article/69845.html)
> 概要: TV动画《因为太怕痛就全点防御力了》确定将于2022年开始播出第二季，同时，官方也公开了新视觉图。
### [剧场动画《ARGONAVIS from BanG Dream!》制作决定！](https://news.dmzj.com/article/69847.html)
> 概要: 在1月9日举办的直播活动中，官方公布了将会制作剧场动画《ARGONAVIS from BanG Dream!》的消息。
### [剧场版《银魂 THE FINAL》，空知英秋作为声优亲自出演！](https://news.dmzj.com/article/69846.html)
> 概要: 今天，剧场版《银魂 THE FINAL》公布了一段关于空知英秋本人作为声优出演的神秘视频，至于他所配音的角色，官方并没有进行说明……
### [《海绵宝宝》3D衍生动画预告 海绵宝宝欢闹夏令营](https://acg.gamersky.com/news/202101/1353470.shtml)
> 概要: 《海绵宝宝》官方推特今天公开了衍生动画《珊瑚营地》先导预告。本段预告展现了10岁的海绵宝宝和朋友们一起欢闹的场景，一起来欣赏一下。
# 财经
### [4.4万亿新高后：城投偿债、发行压力隐现 坚持“城投信仰”？](https://finance.sina.com.cn/china/2021-01-10/doc-iiznezxt1712054.shtml)
> 概要: 原标题：4.4万亿新高后，城投偿债、发行压力隐现！坚持“城投信仰”？机构持续热捧，这类风险加大 信用风险加固“城投信仰”？ 刚刚过去的2020 年，受益于疫情下的流动性呵护...
### [北京发布会要点汇总：新增“1+1”系父子 明起乘网约车需扫健康码](https://finance.sina.com.cn/china/2021-01-10/doc-iiznctkf1287535.shtml)
> 概要: 原标题：新增“1+1”系父子，明起乘网约车需扫健康码！北京发布会要点汇总 今日（1月10日）16时，北京召开第206场疫情防控新闻发布会，通报有关情况。
### [河北疫情传来三个好消息](https://finance.sina.com.cn/china/2021-01-10/doc-iiznctkf1290336.shtml)
> 概要: 来源：国是直通车 稳住 🔺1月8日晚，河北石家庄，工作人员在“火眼”实验室建设现场忙碌。中新社记者 翟羽佳 摄 河北省新冠确诊病例还在增加。
### [北京：暂停居住于顺义的驾驶员（含巡游车、网约车）上路运营](https://finance.sina.com.cn/china/2021-01-10/doc-iiznctkf1280280.shtml)
> 概要: 原标题：北京：暂停居住于顺义的驾驶员（含巡游车、网约车）上路运营 1月10日，北京市第206场新冠肺炎疫情防控工作新闻发布会召开。
### [今天再增40例本土确诊 河北疾控：“疫情没有看到明显的拐点”](https://finance.sina.com.cn/china/2021-01-10/doc-iiznctkf1290273.shtml)
> 概要: 原标题：今天再增40例本土确诊！河北疾控：“疫情没有看到明显的拐点”，张文宏也发声 一文速览河北第三场疫情防控发布会要点： 1月10日15时...
### [10小时又增40+6：河北疫情仍无明显拐点 初步推断病毒来自境外](https://finance.sina.com.cn/stock/zqgd/2021-01-10/doc-iiznctkf1290384.shtml)
> 概要: 原标题：最新！10小时又增40+6，河北疫情仍无明显拐点！注意，扩散风险依然存在！初步推断：病毒来自境外 券商中国王璐璐/整理 河北10小时新增40例确诊 1月10日下午...
# 科技
### [6cmx6cm：大牌振德酒精棉片 0.08 元 / 片探底](https://lapin.ithome.com/html/digi/529242.htm)
> 概要: 【振德旗舰店】75°酒精棉片 100片 报价17.9元，下单第二件0元，叠加限量2元券，2件实付15.9元包邮，领券并购买。振德是上市大牌，春节马上就要到了，这个囤点备用还是很不错的。6cm*6cm尺......
### [荣耀 V40 线下海报曝光：10 亿色视网膜级超感屏，后置矩阵相机模组](https://www.ithome.com/0/529/234.htm)
> 概要: IT之家1月8日消息 2021 年荣耀新品发布会将于 1 月 18 日召开。届时，荣耀 V40 5G 手机将正式发布。去年 12 月，型号为 YOK-AN10 的荣耀手机通过莱茵安全快充认证。这款手机......
### [The Platform Is the Enemy](https://danielbmarkham.com/the-platform-is-the-enemy/)
> 概要: The Platform Is the Enemy
### [Top Ranked Tweets on Hacker News 2020](https://harishgarg.com/writing/hacker-news-front-page-tweets-2020/)
> 概要: Top Ranked Tweets on Hacker News 2020
### [软技能：使用四象限法分析一切问题](https://www.tuicool.com/articles/YRBZbiv)
> 概要: 摄影：产品经理酱蟹拌饭很好吃对时间管理有了解的同学，肯定知道四象限法——用一个坐标轴来区分任务的重要性和紧急性，如下图所示：通过这个坐标轴，可以把任务分为重要且紧急、重要不紧急、紧急且重要、紧急不重要......
### [Mysql性能监控可视化](https://www.tuicool.com/articles/NJVbMrE)
> 概要: 前言​		操作系统以及Mysql数据库的实时性能状态数据尤为重要，特别是在有性能抖动的时候，这些实时的性能数据可以快速帮助你定位系统或Mysql数据库的性能瓶颈，镜像你在Linux系统上使用top、i......
# 小说
### [剑客行](http://book.zongheng.com/book/792818.html)
> 作者：古龙

> 标签：武侠仙侠

> 简介：少年展白初出江湖，却背负一代江湖传说——“霹雳剑”展云天的弑父之仇。展白资质平平，却不得不面对接二连三的江湖强敌。在一次次几殒性命的拼杀中，少年死里逃生，忍受难以忍受的痛苦和折磨，终于学成惊世奇招，为父报仇。然而，中原面临着史无前例的巨大威胁，而少年一人，将要肩负起整个江湖的生死大任……

> 章节末：第六十二章 情根深种

> 状态：完本
# 游戏
### [94年以来首次！PS4和PS5去年在日本销量不到100万](https://www.3dmgame.com/news/202101/3805972.html)
> 概要: 2020年，索尼PlayStation在日本的硬件销量是近年来表现表现最差的一次，PS4和PS5销量之和还不到100万台。根据统计，这也是自1994年以来首次出现这种情况。PS5缺货是原因的一方面，另......
### [借助硬件优势 《瑞奇与叮当：分离》战斗更上一层楼](https://www.3dmgame.com/news/202101/3805981.html)
> 概要: Insomniac Games去年公布了《瑞奇与叮当：分离》之后，就表示本作将会全面利用PS5的次世代硬件优势。Insomniac Games之前就曾分享过《瑞奇与叮当：分离》将会应用PS5的次世代技......
### [雷军什么时候退休？本人回应：小米不需要我的时候](https://www.3dmgame.com/news/202101/3806002.html)
> 概要: 近日，小米创办人、小米集团董事长兼CEO雷军在CCTV一档节目《鲁健访谈》中谈到了退休问题。雷军表示，我刚创办小米两三年的时候，当时企业家非常流行退休，就有记者问我什么时候退休。我考虑退不退休只有一个......
### [《复仇者联盟5》上映时间曝光 乔斯韦登回归指导](https://www.3dmgame.com/news/202101/3805997.html)
> 概要: 在《复仇者联盟4》中，初代复仇者纷纷退出舞台，如今新联盟成员未定，也让《复仇者联盟5》显得遥遥无期。实际上，可能包含《复联5》的漫威第四阶段宇宙自2019年7月至今，已经多次变更了计划。日前，4cha......
### [亨利·卡维尔晒慢跑照：《巫师》第2季片场伤势已好转](https://www.3dmgame.com/news/202101/3805979.html)
> 概要: 近日，“大超”亨利·卡维尔伤势好转中，他发ins分享自拍并报告近况：此前在《巫师》第二季片场遭受腘绳肌受伤的他，如今恢复到可以开始伤后第一次慢跑了，“跑得不快，很远，但是我恢复过程的重大一步”。亨利·......
# 论文
### [Scalable Gaussian Process Variational Autoencoders](https://paperswithcode.com/paper/scalable-gaussian-process-variational)
> 日期：26 Oct 2020

> 标签：None

> 代码：https://github.com/ratschlab/SVGP-VAE

> 描述：Conventional variational autoencoders fail in modeling correlations between data points due to their use of factorized priors. Amortized Gaussian process inference through GP-VAEs has led to significant improvements in this regard, but is still inhibited by the intrinsic complexity of exact GP inference.
### [Tidying Deep Saliency Prediction Architectures](https://paperswithcode.com/paper/tidying-deep-saliency-prediction)
> 日期：10 Mar 2020

> 标签：SALIENCY PREDICTION

> 代码：https://github.com/samyak0210/saliency

> 描述：Learning computational models for visual attention (saliency estimation) is an effort to inch machines/robots closer to human visual cognitive abilities. Data-driven efforts have dominated the landscape since the introduction of deep neural network architectures.
