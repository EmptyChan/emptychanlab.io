---
title: 2022-05-31-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ParrotDay_ZH-CN0775936218_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-05-31 22:18:35
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ParrotDay_ZH-CN0775936218_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [许家印连一根羽毛都不愿失去](https://www.huxiu.com/article/569021.html)
> 概要: 作者：任雪芸，编辑：王芳洁，头图来自：IC photo一位持有1000余张恒大债券的人士对问题的解决不抱任何希望，他告诉《最话》：“明年也够呛。”一位购买了恒大理财的客户，在社交平台上对恒大的声讨周期......
### [SaaS创业的最大陷阱，很多公司深陷坑底却不自知](http://www.woshipm.com/chuangye/5464942.html)
> 概要: 编辑导语：SaaS创业是否成功，应该看什么标准？不少人可能会从SaaS企业所处的赛道本身等其他因素进行衡量，但其实，SaaS企业如果想摆脱风险，就应该注重获客效率的提升速度。本篇文章里，作者就这个话题......
### [小红书运营套路：分析4500篇爆文，发现流量密码竟是这些【家居篇】](http://www.woshipm.com/operate/5465867.html)
> 概要: 编辑导语：随着小红书内容的不断发展，各种品类的种草推荐都能在平台上搜到笔记，家居类也不例外，本篇文章作者通过列举十个具体案例分析了小红书家居品类的爆文流量密码，感兴趣的一起来看一下，希望对你有帮助。小......
### [用户体验，一地鸡毛](http://www.woshipm.com/user-research/5465411.html)
> 概要: 编辑导语：用户体验对于一款产品来说十分重要，而不是简单的产品界面视觉设计，本篇文章作者从影响用户体验的五个层级进行分析，讲述了做好用户体验的注意点，一起来学习一下吧。不知从什么时候起，用户体验已经成了......
### [新显卡GTX 1630上市时间曝光 性能低于GTX 1050 Ti](https://www.3dmgame.com/news/202205/3843580.html)
> 概要: NVIDIA一面嚷着自己库存压力大，显卡供应有点多，一方面居然在悄悄准备“小马甲”产品GTX 1630。来自VCZ的爆料称，GTX 1630将于6月15日上架开卖，比之前传言的5月31日推迟两周。新显......
### [快手春节预热主会场设计 - Kuaishou 2022 CNY](https://www.zcool.com.cn/work/ZNjAxMDgzODg=.html)
> 概要: 一年一更系列......
### [「Free！」监督公布最新绘制的插图](http://acg.178.com/202205/447959757531.html)
> 概要: 近日，「Free！」的监督河浪荣作公布最新绘制的插图，插图上的人物为松冈凛。电视动画「Free！」改编自おおじこうじ创作的轻小说作品「High☆Speed!」，目前共三季。最新的剧场版动画「Free......
### [县城买房，十年只住十五天](https://www.huxiu.com/article/569405.html)
> 概要: 本文来自微信公众号：行业研习 （ID：hangyeyanxi），作者：刘超（四川大学公共管理学院讲师），编辑：Susu，原文标题：《刘超丨县城买房，十年只住十五天，直到成为“穷人的全职太太”》，头图来......
### [web components-LitElement实践](https://www.tuicool.com/articles/mqaMfuQ)
> 概要: web components-LitElement实践
### [《索尼克：起源》NS版开启预购 游戏大小公开](https://www.3dmgame.com/news/202205/3843609.html)
> 概要: 世嘉旗下新游《索尼克 起源》将在索尼克的生日6月23日当天发售，该作是《刺猬索尼克》、《索尼克CD》、《刺猬索尼克2》和《索尼克3与纳克鲁斯》四款经典游戏的合集。游戏的Switch版现已开启预购，标准......
### [《地下城与勇士：决斗》在线模式预告 支持录像回放](https://www.3dmgame.com/news/202205/3843613.html)
> 概要: 《地下城与勇士：决斗》今日（5月31日）发布了在线模式的预告片，这包括排位赛，排行榜，用户设置和录像回放等功能。《地下城与勇士：决斗》将于6月28日发售，登陆PS4、PS5和Steam。《地下城与勇士......
### [声优钉宫理惠宣布开设推特账号](https://news.dmzj.com/article/74561.html)
> 概要: 声优钉宫理惠开设了推特账号（@rie_k_0530），关注者已经达到了24.7万。根据推特上的介绍，目前她正在好友的帮助下学习使用推特，也在事务所学习着相关课程。
### [Recent Layout | 文字排印の版式审美](https://www.zcool.com.cn/work/ZNjAxMTIyNjQ=.html)
> 概要: ---许久没更新作品了甚是惭愧，对文字编排在版式中新的呈现方式的一些理解。●～●继续探索文字编排与构图的更多可能性，取了下这次的主题：文字排印の版式审美。●～●期间尝试Riso印刷，印了一些咖啡海报分......
### [探寻成人的世界，把手伸向15禁](https://news.dmzj.com/article/74562.html)
> 概要: 这期给大家推荐一部青春校园漫《R15+又怎样？》，作者岸谷轰，讲述已经是高中生的女主与身为电影迷的冬峰相识、并开始一起观看15禁电影感悟大人世界的故事。
### [《天竺鼠车车》宣布制作第2季 2022年秋季开播](https://acg.gamersky.com/news/202205/1487038.shtml)
> 概要: 定格动画《天竺鼠车车》宣布制作第2季，将在今年秋季开播。动画《天竺鼠车车》此前播出平台为YouTube，而新系列将在电视台播出。
### [《猫爪牛6.1特辑》](https://www.zcool.com.cn/work/ZNjAxMTQyMjg=.html)
> 概要: 又迎来了一年一度的6.1儿童节,不仅孩子开心，也让我回忆起了甜甜的童年，这次就以“甜甜”为切入点，走一个甜甜的儿童节专辑~第一章：<甜甜的糖梦> 希望大家回忆起童年的色彩，那时候是不是颜色都特别鲜艳呢......
### [userscript.zone: search for userscripts by URL, domain or search term](https://www.userscript.zone/)
> 概要: userscript.zone: search for userscripts by URL, domain or search term
### [手机门店，无人问津 618能救手机厂商吗？](https://finance.sina.com.cn/tech/2022-05-31/doc-imizmscu4358904.shtml)
> 概要: 撰文/吴迪  编辑/董雨晴......
### [东映申请注册《假面骑士》相关的新商标](https://news.dmzj.com/article/74564.html)
> 概要: 根据商标速报bot的消息，东映株式会社申请了《假面骑士》相关的新商标“假面骑士GEATS”。申请编号为2022-58078〜2022-58099。
### [「街角魔族」动画官方10万粉丝贺图公布](http://acg.178.com/202205/447974644263.html)
> 概要: 动画「街角魔族」社交平台账号粉丝数量现已经突破了10万，官方发布了10万粉丝贺图，并对支持他们的观众表达了感谢。电视动画「街角魔族」改编自伊藤いづも创作的同名漫画作品，由J.C.STAFF负责动画制作......
### [【红蓝村-女足】拉波尔塔出席巴萨女足庆功晚宴](https://bbs.hupu.com/53988137.html)
> 概要: 当地时间5月29日，巴萨女足举行了赛季末的庆功晚宴，俱乐部主席拉波尔塔参加。本赛季的巴萨女足先后赢得了西超杯冠军、联赛冠军和王后杯冠军，实现了国内赛事的三冠王。唯一的遗憾是在欧冠决赛中输给强大的里昂，
### [荣耀2022年员工将增长30-40%](https://finance.sina.com.cn/tech/2022-05-31/doc-imizmscu4360238.shtml)
> 概要: 5月31日，荣耀赵明在接受界面新闻记者采访时表示，即便手机市场下滑，但今年荣耀员工数量将增长30-40%......
### [特斯拉预计6月恢复正常水平](https://finance.sina.com.cn/tech/2022-05-31/doc-imizmscu4363154.shtml)
> 概要: 文/戚七......
### [《游戏王》三幻神金属浮雕卡 910元的法老王我当定了](https://acg.gamersky.com/news/202205/1487095.shtml)
> 概要: Movic《游戏王》“三幻神金属浮雕卡” 套装今日开订。
### [赵明：未来3-5年不存在增长天花板](https://finance.sina.com.cn/tech/2022-05-31/doc-imizmscu4365488.shtml)
> 概要: 新浪科技讯 5月31日下午消息，荣耀近日发布了荣耀70系列，荣耀CEO赵明在接受媒体采访时表示，目前荣耀线下销售占比已经超过70%，今年线下体验店将从2千多家增长到3千家左右......
### [优酷上线4K修复版《黑猫警长》 童年经典再来](https://acg.gamersky.com/news/202205/1487104.shtml)
> 概要: 优酷在今天（5月31日）宣布，为了庆祝六一儿童节，4K修复版《黑猫警长》动画正式上线。
### [「只想看着你」决定制作真人日剧 公开预告PV](http://acg.178.com/202205/447980204281.html)
> 概要: 漫改真人电影「萍水相逢腐缘下」宣布将制作剧中剧「只想看着你」（君のことだけ見ていたい）的真人日剧，并公开了预告PV。「只想看着你」预告PV出演：仓悠贵、水沢林太郎、石川瑠华、樱井健人、片山友希、シソン......
### [现代暗黑系效果图表现](https://www.zcool.com.cn/work/ZNjAxMTc5NTY=.html)
> 概要: 持续更新，希望各位喜欢......
### [动画「天籁人偶」新追加声优公开](http://acg.178.com/202205/447981273690.html)
> 概要: 电视动画「天籁人偶」公布了三位新追加的声优：村濑步、久野美咲、诹访彩花，本作将于2022年7月开始播出。CAST灰桜：和氣あず未鴉羽：楠木ともり月下：富田美憂箒星：中島由貴レーツェル：鬼頭明里遠間ナギ......
### [Show HN: Todool, a full blown environment for managing large trees of task lists](https://todool.handmade.network/)
> 概要: Show HN: Todool, a full blown environment for managing large trees of task lists
### [《AI 梦境档案 涅槃计划》发布新情报 游戏6月底发售](https://www.3dmgame.com/news/202205/3843643.html)
> 概要: Spike Chunsoft近日更新了悬疑冒险视觉小说新作《AI梦境档案 涅槃计划》的相关情报，游戏将于2022年6月23日登陆PS4、Xbox One、Switch、PC平台（欧洲地区延期至7月8日......
### [视频：张艺兴尼克杨《热爱》上线 以热爱致热爱以音乐致敬NBA](https://video.sina.com.cn/p/ent/2022-05-31/detail-imizmscu4377817.d.html)
> 概要: 视频：张艺兴尼克杨《热爱》上线 以热爱致热爱以音乐致敬NBA
### [时隔12年，凯尔特人拿下抢7进入总决赛](https://bbs.hupu.com/53991126.html)
> 概要: 北京时间5月30日，波士顿凯尔特人队在进行的NBA季后赛东部决赛“抢七”战中以100:96击败劲敌迈阿密热火队，从而以4:3的总比分时隔12年再度闯入总决赛。此前，凯尔特人曾经在2017年、2018年
### [银格第三季：白贝和蓝托成为最大赢家，格斗仪和面具都回来了！](https://new.qq.com/omn/20220531/20220531A08I4P00.html)
> 概要: 银格第三季：白贝和蓝托成为最大赢家，格斗仪和面具都回来了！
### [七彩虹显卡 618 大促：RTX 3060 2799 元起，RTX 3070 3999 元起](https://www.ithome.com/0/621/556.htm)
> 概要: IT之家5 月 31 日消息，今天，七彩虹公布了本次 618 开门红显卡大促活动，50 余款显卡将参与促销，其中 iGame 显卡多达三十余款，包括入门级显卡 GeForce RTX 2060 / 3......
### [组图：陈晓发博为陈妍希庆生 甜蜜表白“新的一岁一切顺利顺心”](http://slide.ent.sina.com.cn/star/w/slide_4_704_370526.html)
> 概要: 组图：陈晓发博为陈妍希庆生 甜蜜表白“新的一岁一切顺利顺心”
### [半导体吞噬软件的600亿铁证](https://www.huxiu.com/article/569926.html)
> 概要: 作者| 宇多田出品| 虎嗅科技组封面来自《爱、死亡与机器人》第三季《经济学人》曾把半导体产业形容为非洲塞伦盖蒂大草原——这里充斥着大型捕食者贪婪吞食巨型猎物的残酷游戏。作为一个同样复杂的“食物链”，不......
### [欣旺达拟 23 亿元投建高性能圆柱锂离子电池项目，年产 3.1 亿只](https://www.ithome.com/0/621/557.htm)
> 概要: IT之家5 月 31 日消息，欣旺达 5 月 31 日晚间公告，公司拟与浙江省兰溪市人民政府签署《欣旺达高性能圆柱锂离子电池项目投资协议书》。该项目主要产品为高性能圆柱锂离子电池。该项目计划总投入 2......
### [游戏王历史：环境历史断章 连接十字被禁的理由](https://news.dmzj.com/article/74570.html)
> 概要: 有这么一张卡，叫做“连接十字”。
### [OPPO 耳机夏季更新时间公布：6 月 8 日支持所有安卓手机弹窗，Enco X2 支持 LDAC + LHDC 4.0 协议](https://www.ithome.com/0/621/566.htm)
> 概要: 感谢IT之家网友菱形窗、夕颜陌城的线索投递！IT之家5 月 31 日消息，OPPO 官宣将为 Enco X2 和 Enco X 无线蓝牙耳机带来夏季更新，还为旗下所有耳机带来安卓手机弹窗。更新时间点如......
### [阿布索留特的王也是神四级别的，将在第四季登场，白贝又要搞事情](https://new.qq.com/omn/20220531/20220531A093LU00.html)
> 概要: 阿布索留特的王也是神四级别的，将在第四季登场，白贝又要搞事情
### [疫情受控后，A股下一个阶段关注点是什么？](https://www.yicai.com/news/101428919.html)
> 概要: 当前部分地区疫情大幅好转，市场持续反弹将近一个月。当前，自3月份以来部分地区爆发的疫情已经大幅好转，全国新冠肺炎确诊+无症状感染者病例从4月中旬将近30000降低至目前的200左右。
### [Show HN: Shale – a Ruby object mapper and serializer for JSON, YAML and XML](https://www.shalerb.org/)
> 概要: Show HN: Shale – a Ruby object mapper and serializer for JSON, YAML and XML
### [许松松：时代的沙粒压下，个人求职如何翻越大山？](https://www.tuicool.com/articles/Uja2UvQ)
> 概要: 许松松：时代的沙粒压下，个人求职如何翻越大山？
### [35岁程序员炒虚拟货币 千万资产3天归零！](https://www.3dmgame.com/news/202205/3843656.html)
> 概要: 今年35岁的程序员郭瑞度过了自己人生中最灰暗的一周。他向公司请了一周假，每天无法入睡，只有在妻子的逼迫下才能吃上一点食物。他时刻盯着手机里的消息，妄图在纷繁的消息中看到一丝他所购入的Luna代币可能涨......
### [力挺小米，苹果等科技巨头指责印度执法机构“不懂行”](https://www.ithome.com/0/621/574.htm)
> 概要: 感谢IT之家网友航空先生、MissBook、蓝海岸Nibiru的线索投递！北京时间 5 月 31 日消息，在印度当地官员因专利费与小米公司发生争执后，一个由苹果公司和其他在印度运营的科技巨头组成的游说......
### [体验服装备商城上新](https://bbs.hupu.com/53993547.html)
> 概要: ﻿各位共创策划好，我们对2件装备的方案进行了完善，补齐了相关信息，两件装备也将于今天上线体验服（如无意外），这里向大家做下同步~
### [中国软件，挣的钱都去哪儿了？](https://www.tuicool.com/articles/U3EJFjQ)
> 概要: 中国软件，挣的钱都去哪儿了？
### [上海汽车经销商将开市，盯上新增的四万个车牌](https://www.yicai.com/news/101430015.html)
> 概要: 购置税补贴政策会给今年乘用车市场带来约200万辆的零售增量。
### [Killer Asteroids are hiding in plain sight; a new tool finds them](https://www.nytimes.com/2022/05/31/science/asteroids-algorithm-planetary-defense.html)
> 概要: Killer Asteroids are hiding in plain sight; a new tool finds them
### [“网标”落地，网络剧片纳入正规军](https://www.tuicool.com/articles/u2iQnqE)
> 概要: “网标”落地，网络剧片纳入正规军
### [S妈因汪小菲言论哭一天 接受道歉称会继续疼爱他](https://ent.sina.com.cn/s/m/2022-05-31/doc-imizirau5856597.shtml)
> 概要: 新浪娱乐讯 5月31日，汪小菲在微博发文公开向大S及S妈道歉。随后，有台媒采访到S妈，她哭着说：“这样很好呀！道歉是勇敢的态度，说错话、做错事都道歉，我会原谅他的。”　　台媒还称，S妈因汪小菲昨日的言......
### [北京二批集中供地首日：底价成交7宗地块，总金额218亿元](https://finance.sina.com.cn/china/gncj/2022-05-31/doc-imizmscu4422452.shtml)
> 概要: 5月31日，北京市第二轮集中供地首日最终有7宗地块以底价成交，总成交金额218.1亿元，另外有3宗地块流拍，剩余7宗地块将进入6月1日的现场竞拍环节。
### [吉林四平市再发通报 删除连续两次未参加核酸可被行政拘留10天](https://finance.sina.com.cn/china/2022-05-31/doc-imizmscu4422693.shtml)
> 概要: 吉林四平市再发通报 删除连续两次未参加核酸可被行政拘留10天 吉林四平市疫情防控指挥部2022年5月31日通报称，为巩固疫情防控成果...
### [杭州第二批宅地集中出让45宗地，起拍总价529亿元](https://finance.sina.com.cn/china/gncj/2022-05-31/doc-imizmscu4423012.shtml)
> 概要: 澎湃新闻记者 刘畅 5月31日，浙江省杭州市规划和自然资源局发布《关于公布2022年杭州市区第二批住宅用地集中出让的公告》。杭州市2022年第二批宅地集中出让共计45宗...
### [喜报！发布两则中国动漫作品在国际顶级节展获奖或入围的新闻](https://new.qq.com/omn/20220531/20220531A0CCFV00.html)
> 概要: 喜报！发布两则中国动漫作品在国际顶级节展获奖或入围的新闻
### [一波回忆杀来袭，快男超女主题曲大放送，十年选秀里有你的青春吗](https://new.qq.com/rain/a/20220527V04YWF00)
> 概要: 一波回忆杀来袭，快男超女主题曲大放送，十年选秀里有你的青春吗
### [成都：二孩及以上家庭可在现有限购套数基础上新购买1套住房](https://finance.sina.com.cn/china/gncj/2022-05-31/doc-imizmscu4425178.shtml)
> 概要: 原标题：成都进一步优化完善房地产政策！支持刚性和改善性住房需求 来源：成都发布 5月31日，记者从成都市住建局获悉，为认真贯彻落实党中央、国务院决策部署...
### [时隔多年陈冰再出发，复出曲被网友痛批难听，网友：选歌的问题](https://new.qq.com/rain/a/20220527V04D9V00)
> 概要: 时隔多年陈冰再出发，复出曲被网友痛批难听，网友：选歌的问题
### [解读扎实稳住经济一揽子政策措施 国家发改委：合理扩大有效需求，推动重点领域加快复工达产](https://finance.sina.com.cn/roll/2022-05-31/doc-imizirau5861009.shtml)
> 概要: 每经记者 李可愚北京报道  5月31日，《国务院关于印发扎实稳住经济一揽子政策措施的通知》正式对外发布，包括进一步加大增值税留抵退税政策力度...
### [哪几区居家？哪些区正常上班？北京各区最新防控措施一图汇总](https://finance.sina.com.cn/china/gncj/2022-05-31/doc-imizmscu4424632.shtml)
> 概要: 责任编辑：王翔......
### [土耳其斡旋，普京点头，黑海粮食安全大动脉能打通了？](https://www.yicai.com/news/101430131.html)
> 概要: 目前尚不清楚俄方所说的是哪个乌克兰港口。
### [喻言MV被曝抄袭泰妍，多处相似几乎石锤，魏晨：原来我不是最惨的](https://new.qq.com/rain/a/20220530V03CMF00)
> 概要: 喻言MV被曝抄袭泰妍，多处相似几乎石锤，魏晨：原来我不是最惨的
### [5月A股红盘收官，6月怎么走？投资策略提前看](https://www.yicai.com/news/101430142.html)
> 概要: 后市怎么走？有机构认为，6月A股将延续5月震荡上行的行情。
### [真实事件改编而来的歌曲有多触目惊心？听到最后已经哭麻了](https://new.qq.com/rain/a/20220530V0461S00)
> 概要: 真实事件改编而来的歌曲有多触目惊心？听到最后已经哭麻了
### [流言板吉马良斯：孙兴慜很出色，我们会在比赛中尽可能地防守好他](https://bbs.hupu.com/53995381.html)
> 概要: 虎扑05月31日讯 北京时间6月2号19：00，巴西与韩国将在首尔进行一场友谊赛。赛前，巴西中场吉马良斯在接受采访时谈到了孙兴慜，对于这位刚刚在本赛季夺得英超金靴的韩国前锋，吉马良斯表示孙兴慜给自己留
### [不得隐匿违规关联交易！监管规范上市公司与财务公司业务往来](https://www.yicai.com/news/101430129.html)
> 概要: 双方应当签订金融服务协议。
### [机构舍互联网转投网络安全，但头部企业相较海外仍需成长](https://www.yicai.com/news/101430101.html)
> 概要: 从投资端来看，互联网行业发展速度下降，很多基金转投网络安全行业，“过去‘安全’是大家看不上的，现在反而优先级上升了。”
# 小说
### [网游之刺灵](http://book.zongheng.com/book/974533.html)
> 作者：轻狂酥风

> 标签：科幻游戏

> 简介：“在这里，你也拥有触碰梦想的机会。”一句简单的广告词，触动谢朗的心灵，是啊！没有人甘愿一世平凡。哪怕他的愿望是那么的低微。一步一脚印，一名菜鸟刺客，如何在虚拟世界翻江倒海。小风QQ：981673741，欢迎前来骚扰！

> 章节末：请假条

> 状态：完本
# 论文
### [GisPy: A Tool for Measuring Gist Inference Score in Text | Papers With Code](https://paperswithcode.com/paper/gispy-a-tool-for-measuring-gist-inference)
> 日期：25 May 2022

> 标签：None

> 代码：None

> 描述：Decision making theories such as Fuzzy-Trace Theory (FTT) suggest that individuals tend to rely on gist, or bottom-line meaning, in the text when making decisions. In this work, we delineate the process of developing GisPy, an open-source tool in Python for measuring the Gist Inference Score (GIS) in text. Evaluation of GisPy on documents in three benchmarks from the news and scientific text domains demonstrates that scores generated by our tool significantly distinguish low vs. high gist documents. Our tool is publicly available to use at: https://github.com/phosseini/GisPy.
### [Graph Anisotropic Diffusion | Papers With Code](https://paperswithcode.com/paper/graph-anisotropic-diffusion)
> 日期：30 Apr 2022

> 标签：None

> 代码：None

> 描述：Traditional Graph Neural Networks (GNNs) rely on message passing, which amounts to permutation-invariant local aggregation of neighbour features. Such a process is isotropic and there is no notion of `direction' on the graph. We present a new GNN architecture called Graph Anisotropic Diffusion. Our model alternates between linear diffusion, for which a closed-form solution is available, and local anisotropic filters to obtain efficient multi-hop anisotropic kernels. We test our model on two common molecular property prediction benchmarks (ZINC and QM9) and show its competitive performance.
