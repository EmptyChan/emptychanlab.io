---
title: 2023-09-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MarathonMedoc_ZH-CN6649798028_1920x1080.webp&qlt=50
date: 2023-09-11 22:27:02
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MarathonMedoc_ZH-CN6649798028_1920x1080.webp&qlt=50)
# 新闻
### [Given together, COVID and flu vaccines appear safe, immune-boosting](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx6058806636656&cate_id=-1)
> 概要: 以色列希巴医学中心研究人员领导的一项研究表明，COVID-19和流感疫苗可以安全地一起接种，抗体反应不会显著下降......
### [专家呼吁：重视肺癌术后辅助治疗 提升药品可支付性](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx7609373716569&cate_id=-1)
> 概要: 新华网北京9月8日电（肖寒）根据国家癌症中心《2016年中国癌症发病率和死亡率》发布的数据显示，在中国，每分钟有超过8个人被诊断为癌症，每分钟有超过5个人因癌症逝世。无论你有多么不愿面对，癌症确实潜伏......
### [More cases of breast cancer detected with the help of AI](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2757771948119&cate_id=-1)
> 概要: 卡罗林斯卡医学院的ScreenTrustCAD研究报告称，一名由AI支持的放射科医生在筛查乳房X光检查中检测到的乳腺癌病例比两名放射科医生一起工作的病例更多，论文标题为“筛查乳房X光检查中乳腺癌检测的......
### [温州医科大学发现肝内胆管癌的抑制新机制 为新的治疗方法提供理论依据](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx3722092769393&cate_id=-1)
> 概要: <strong>导读：</strong>肝内胆管细胞癌(ICC)是一种原发性肝脏恶性肿瘤，具有高度侵袭性和恶性生物学行为。目前，有效的治疗策略有限。仑伐替尼对ICC的作用尚不清楚......
### [HPV疫苗上半年国内批签发量增约80％，男性适应症也要来了？](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx7311846568035&cate_id=-1)
> 概要: 本周，默沙东旗下九价HPV疫苗新适应上市申请获受理的消息受到关注，有媒体报道，此次新适应症为用于男性接种。如果消息属实，这将成为国内首个针对男性的HPV疫苗上市申请......
### [非熟人圈，为什么也搞“熟人”推荐？](https://www.woshipm.com/it/5900868.html)
> 概要: 社交平台上的推荐功能——“可能认识的人”，你是否曾经遇到过？它为什么如此受平台欢迎？这篇文章将探讨这个话题。如果你对此感兴趣，那么不要错过这篇文章。在当下，我们在陌生人社交平台上，能够更加真实吐露自己......
### [周2.8亿播放2千万点赞，网红短剧《逃出大英博物馆》出圈背后](https://www.woshipm.com/operate/5902528.html)
> 概要: 近期，《逃出大英博物馆》突然爆火，周2.8亿播放2千万点赞，该短剧是如何出圈的？本文在对近段时间出圈的短剧《逃出大英博物馆》背后的逻辑进行分析讲解，一起来看看吧。我是说比如，作为一个拥有百万粉丝颜值网......
### [生成式AI已成为企业新兴风险，但我们不应该因噎废食](https://www.woshipm.com/ai/5901102.html)
> 概要: 2023年，生成式AI破茧成蝶，掀起了一股数字浪潮。生成式AI能够为企业解决问题，提高效率，但同时它也容易在大模型训练过程中，泄露隐私安全。该如何应对生成式AI的安全问题？一起来看看本文吧。2023年......
### [这可能是最全面的Python入门手册了！](https://segmentfault.com/a/1190000044202606)
> 概要: 无论是学习任何一门语言，基础知识一定要扎实，基础功非常的重要，找到一个合适的学习方法和资料会让你少走很多弯路， 你的进步速度也会快很多，无论我们学习的目的是什么，不得不说Python真的是一门值得付出......
### [五部门联合发布《元宇宙产业创新发展三年行动计划》：打造一批典型应用](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmharv9729342.shtml)
> 概要: 五部门联合发布《元宇宙产业创新发展三年行动计划（2023-2025年）》......
### [探索 Java 线程的创建](https://segmentfault.com/a/1190000044203236)
> 概要: by emanjusaka fromhttps://www.emanjusaka.top/archives/7彼岸花开可奈何本文欢迎分享与聚合，全文转载请留下原文地址。前言在并发编程中我们为啥一般选用......
### [这么多人用codesandbox，他服务器扛得住么？](https://segmentfault.com/a/1190000044203484)
> 概要: 大家好，我卡颂。codesandbox是前端工程师经常使用的代码在线运行环境，页面如下：他的应用场景很广，比如：有代码逻辑要分享，分享个codesandbox链接有新想法需要验证，又不想本地起个项目，......
### [裁剪的3种方式，CSS 如何隐藏移动端的滚动条？](https://segmentfault.com/a/1190000044204165)
> 概要: 在移动端开发中，经常会碰到需要横向滚动的场景，例如这样的但很多时候是不需要展示这个滚动条的，也就是这样的效果，如下你可能想到直接设置滚动条样式就可以了，就像这样::-webkit-scrollbar ......
### [消息称漫威美国队长扮演者已经结婚](https://www.3dmgame.com/news/202309/3877281.html)
> 概要: 根据外媒Page Six的报道，漫威复仇者联盟等系列电影中美国队长的扮演者，演员Chris Evans和Alba Baptista据说已经结婚。这对夫妇在9月9日在他们位于波士顿地区的家中举行了一场亲......
### [组图：吴磊宣传新片谈及生活状态 称可以和爱人家人在一起就好](http://slide.ent.sina.com.cn/star/slide_4_704_388753.html)
> 概要: 组图：吴磊宣传新片谈及生活状态 称可以和爱人家人在一起就好
### [医疗健康行业周报 | 「迈德斯特」获近亿元首轮融资；「雷奥顶峰」完成数千万元Pre-A轮融资](https://36kr.com/p/2426273169892358)
> 概要: 医疗健康行业周报 | 「迈德斯特」获近亿元首轮融资；「雷奥顶峰」完成数千万元Pre-A轮融资-36氪
### [36氪首发 | 元宇宙服务商「烧糖文化」获首轮500万元融资，创新推出虚拟影片制作解决方案](https://36kr.com/p/2410185456657159)
> 概要: 36氪首发 | 元宇宙服务商「烧糖文化」获首轮500万元融资，创新推出虚拟影片制作解决方案-36氪
### [36氪首发｜盘古新能源获数千万元融资，钠电电芯中试线年底投产](https://36kr.com/p/2426145522312196)
> 概要: 36氪首发｜盘古新能源获数千万元融资，钠电电芯中试线年底投产-36氪
### [马斯克：如果不迅速转向自动驾驶电动汽车，大众迟早会完蛋](https://finance.sina.com.cn/stock/usstock/c/2023-09-11/doc-imzmhxvp0826737.shtml)
> 概要: 特斯拉CEO埃隆·马斯克周末在回应一位X用户的帖子时表示，包括大众在内的传统汽车制造商必须迅速转向自动驾驶电动汽车，否则迟早会完蛋......
### [吸血鬼原创动画《Delicos Nursery》公开最新PV！](https://news.dmzj.com/article/79157.html)
> 概要: 吸血鬼原创动画《Delicos Nursery》公开最新PV，2024年开播。
### [《伤物语》总集篇《伤物语-历吸血鬼-》制作决定，PV公开](https://news.dmzj.com/article/79160.html)
> 概要: 《伤物语》总集篇《伤物语-历吸血鬼-》制作决定，PV公开，2024年上映。
### [五行之IP形象设计](https://www.zcool.com.cn/work/ZNjY0NzE2NjQ=.html)
> 概要: 好久没有画画了，手上的东西不能落下，这次作品穿插了绘画部分。五行ip形象展示，新的一套作品，希望大家喜欢~五行主题或多或少会觉得比较传统，土味国潮，我想做点稍微不一样的，作品也有不足，感恩指正！每个形......
### [可爱物语.2 -by丁几](https://www.zcool.com.cn/work/ZNjY0NzIyNTY=.html)
> 概要: 可爱的元素永远能带给人们治愈的能量，希望天真和无拘无束的生活态度也能一直伴随我们～整理了七月的一部分作品，还将持续更新。原创绘画 by 丁几huangding—————————————————————......
### [投资人张勇值得期待吗？](https://36kr.com/p/2427005352535043)
> 概要: 投资人张勇值得期待吗？-36氪
### [TV动画《英雄教室》公布第2弹PV](https://news.dmzj.com/article/79163.html)
> 概要: TV动画《英雄教室》目前正在热播中，官方公布了追加声优、第3弹主视觉图、第2弹PV等信息。
### [组图：杰克逊女儿帕里斯家中被盗后 由保安“护驾”出门遛狗](http://slide.ent.sina.com.cn/slide_4_704_388773.html)
> 概要: 组图：杰克逊女儿帕里斯家中被盗后 由保安“护驾”出门遛狗
### [舞台剧《咒术回战》公布公演PV](https://news.dmzj.com/article/79165.html)
> 概要: 根据芥见下下原作改编的舞台剧《咒术回战-京都姐妹校交流会·起首雷同篇》将于今年12月~2024年1月在东京和兵库公演，公演PV、第2弹视觉图及追加演员等相关信息也一并公布。
### [《碧蓝航线：微速前行》第二季宣布制作 新PV发布](https://www.3dmgame.com/news/202309/3877313.html)
> 概要: 动画《碧蓝航线：微速前行》第二季发布了制作决定PV，具体播出时间待定，该作改编自同名漫画，讲述了舰娘们在母港日常生活的故事。预告视频：视频截图：......
### [“新生代、新力量、新创投”中国创投创新峰会暨蓉城论坛成功举办！](http://www.investorscn.com/2023/09/11/110373/)
> 概要: “新生代、新力量、新创投”中国创投创新峰会暨蓉城论坛成功举办！
### [2023祝融奖收官，看雷士照明如何构筑行业良性循环](http://www.investorscn.com/2023/09/11/110374/)
> 概要: 9月9日，由中国建筑装饰协会主办、雷士照明共同协办的2023“祝融奖”第十五届CBDA照明应用设计大赛全国总决赛于苏州正式落下帷幕。据悉，今年已经是雷士照明第11年冠名祝融奖，本届大赛也迎来了全面革新......
### [菜鸟供应链在杭州成立企业管理公司](http://www.investorscn.com/2023/09/11/110380/)
> 概要: 【#菜鸟供应链在杭州成立企业管理公司# 注册资本1000万】......
### [aibo 寻找新家：索尼推出新计划让机器狗继续发挥作用](https://www.ithome.com/0/718/379.htm)
> 概要: IT之家9 月 11 日消息，aibo 是索尼公司 5 年前推出的一款智能机器狗，可以与人类建立深厚的感情。但是，有些 aibo 因为各种原因而不得不结束与主人的生活，它们的未来将如何呢？为了让这些 ......
### [捷佳伟创：国内首台完全自主研发的电子级硅芯清洗设备即将交付客户](https://www.ithome.com/0/718/384.htm)
> 概要: 感谢IT之家网友航空先生的线索投递！IT之家9 月 11 日消息，据捷佳伟创官方公众号报道，常州捷佳创精密机械有限公司国内首台完全自主研发的电子级硅芯清洗设备目前已经成功下线，将于近日部署至某合作客户......
### [克里姆林宫：金正恩将应普京邀请于近日访问俄罗斯](https://www.yicai.com/news/101856069.html)
> 概要: 朝鲜最高领导人金正恩将于近日访问俄罗斯。
### [比亚迪仰望 U8 官宣 9 月 20 日上市：定位百万级硬派越野车，预售价 109.8 万元](https://www.ithome.com/0/718/395.htm)
> 概要: IT之家9 月 11 日消息，比亚迪旗下高端新能源品牌仰望汽车今日官宣，百万级硬派越野车仰望 U8 将于 9 月 20 日正式上市。新车已于上海车展开启预售，提供豪华版、越野玩家版两种车型，预售价为 ......
### [英国一周实体游戏销量榜：《星空》无意外夺冠](https://www.3dmgame.com/news/202309/3877323.html)
> 概要: 尽管《星空》首发就加入了Game Pass，但它在英国的实体游戏销售首秀可以说非常强劲。当地时间周日，外媒GamesIndustry.biz公开了英国实体游戏销量榜，显示这款Xbox和PC独占游戏在发......
### [小米汽车通过蓝牙 SIG 认证，支持蓝牙 5.2 和苹果 CarPlay](https://www.ithome.com/0/718/398.htm)
> 概要: 感谢IT之家网友华南吴彦祖的线索投递！IT之家9 月 11 日消息，这次，小米汽车传出了外观和内饰之外的新进展。据蓝牙技术联盟 SIG 数据库显示，小米汽车（代号 MS11）已于今日通过了 SIG 认......
### [大学校友圈带起千亿储能产业，长沙如何穿越“锂周期”](https://www.yicai.com/news/101856092.html)
> 概要: 众多锚定“储能之都”的城市中，长沙尤为亮眼。2022年，长沙先进储能材料产业产值突破1000亿元，拥有链上企业150家。
### [丰巢受邀参加中国国际投资贸易洽谈会，持续拓展海外市场](http://www.investorscn.com/2023/09/11/110385/)
> 概要: 9月8日-11日，第二十三届中国国际投资贸易洽谈会期间在厦门盛大启幕。丰巢作为末端智慧物流领域的代表企业，获邀参会......
### [意大利美女Cos《塞尔达》公主美图 性感美艳惊人](https://www.3dmgame.com/bagua/6190.html)
> 概要: 近日意大利美女Coser himee.lily分享了自己Cos《塞尔达》公主的美照。从图中可以看到，她在海边身穿清凉服装，秀出纤腰美腿，真是性感美艳。一起来欣赏下她的美图吧......
### [殡仪馆：袁仁国遗体暂不能探视](https://www.yicai.com/news/101856103.html)
> 概要: 贵州茅台原董事长袁仁国已去世的消息传出。
### [Arm认为AI浪潮将有巨大商机 或实现20%营收增长](https://www.3dmgame.com/news/202309/3877322.html)
> 概要: 此前Arm已经为其首次公开募股（IPO）向美国纳斯达克提交了上市申请文件，希望以“ARM”为股票代码进行交易，其最终估值大概在500亿至550亿美元之间，预计会在9月14日正式上市。据相关媒体报道，在......
### [《奥本海默》全球票房破9亿美元，有望刷新传记片纪录](https://www.yicai.com/news/101856112.html)
> 概要: 不出意外，《奥本海默》本周将有望超越《波西米亚狂想曲》创造的9.13亿美元全球票房，登顶传记类影片全球票房冠军。
### [白象：从一碗好面到亿碗好面的“冠军之志”](http://www.investorscn.com/2023/09/11/110386/)
> 概要: 近日，全国瞩目的杭州亚运会火炬传递在浙江省内如火如荼地进行。9月9日，浙江湖州，本次亚运火炬手、白象食品董事长兼总裁姚忠良接受媒体采访时表示：“赛场上，运动健儿以冠军为目标拼搏，令人振奋。其实，冠军不......
### [流言板Shams：詹杜库计划联合征战奥运与美国队世界杯失利关系不大](https://bbs.hupu.com/62072896.html)
> 概要: 虎扑09月11日讯 根据Shams Charania报道，消息人士透露，勒布朗-詹姆斯计划代表美国男篮出战2024年巴黎奥运会。随后，包括KD，库里，布克，欧文，AD等多位球员均有意出战。据Shams
### [组图：宋茜《另一种蓝》景德镇戏份杀青 晒机舱内自拍可爱元气](http://slide.ent.sina.com.cn/tv/w/slide_4_704_388786.html)
> 概要: 组图：宋茜《另一种蓝》景德镇戏份杀青 晒机舱内自拍可爱元气
### [融资保证金比例首次下调落地，将给A股带来多少“活水”](https://www.yicai.com/news/101856156.html)
> 概要: “政策落地后理论上可释放约4000亿元可融资空间。”
### [流言板鹿盆大口，小鹿发文展示整齐牙齿：请夸夸我的牙齿](https://bbs.hupu.com/62072983.html)
> 概要: 虎扑09月11日讯 鹿盆大口，小鹿发文展示整齐牙齿：请夸夸我的牙原文如下：第一步：选一张炸裂的配图吸引你们注意第二步：真诚地向各位表达我的心声：很多姐妹私信我关于牙套的问题，想录一个视频统一回复第三步
### [流言板Shams：詹杜库都将明年奥运会视作自己在国家队的最后一舞](https://bbs.hupu.com/62072986.html)
> 概要: 虎扑09月11日讯 根据之前的报道，勒布朗-詹姆斯计划出战24年奥运会，并打算联合杜兰特、库里等球星一起出战。根据Shams Charania跟进报道，消息人士透露，詹姆斯、杜兰特和库里都将明年奥运会
### [老挝青年领导人：借力老中铁路 让更多青年搭上中国发展快车](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmizhz0353370.shtml)
> 概要: “老中铁路极大便利了沿线民众出行，推动沿线产业升级，对老挝经济发展的促进作用显著。希望能够借力老中铁路，让更多老挝青年从中受益，搭上中国发展快车。
### [泸州签约一批“网红”项目 促特色农产品出圈](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmizih5971615.shtml)
> 概要: 四川在线记者 范芮菱为加快发展数字经济，打造具有国际竞争力的数字产业集群，9月11日，泸州市举行网红经济推介会暨 “系列十佳” 评选活动启动仪式。
### [泸州签约一批“网红”项目 促特色农产品出圈](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmizie9194499.shtml)
> 概要: 转自：四川在线四川在线记者 范芮菱为加快发展数字经济，打造具有国际竞争力的数字产业集群，9月11日，泸州市举行网红经济推介会暨 “系列十佳” 评选活动启动仪式。
### [流言板Shams：若不是休赛期进行脚部手术，贝恩本可入选世界杯阵容](https://bbs.hupu.com/62073077.html)
> 概要: 虎扑09月11日讯 根据Shams Charania报道，消息人士透露，勒布朗-詹姆斯计划代表美国男篮出战2024年巴黎奥运会。随后，包括KD，库里，布克，欧文，AD等多位球员均有意出战。据Shams
### [【广西】柳州市市场监管局公布2023民生领域案件查办“铁拳·桂在真打”行动第二批典型案件](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmizhz0353788.shtml)
> 概要: 转自：柳州市场监管2023年以来，柳州市市场监管系统持续深入开展民生领域案件查办“铁拳·桂在真打”行动，进一步加强重点领域市场监管执法，集中力量，重拳出击...
### [南宁开展违法广告专项整治行动，为期1个月](https://finance.sina.com.cn/jjxw/2023-09-11/doc-imzmizih5973511.shtml)
> 概要: 9月11日，记者从南宁市市场监管局了解到，从9月起，该局在全市范围内组织开展为期一个月的违法广告专项整治行动，对户外广告内容、传统媒体和互联网广告等方面进行监管...
### [流言板拉特克利夫：如果竞购曼联失败，这种结局太打人脸和痛苦了](https://bbs.hupu.com/62073204.html)
> 概要: 虎扑09月11日讯 拉特克利夫在接受英力士的YouTube频道采访时表示，他无法想象在竞购曼联这样的俱乐部时失败，他说这样做将是“痛苦和难以忍受的”。他说：“如果不是我们的一些过往经历——其中不少是在
# 小说
### [当武林盟主的日子](https://book.zongheng.com/book/952386.html)
> 作者：写出来的世界

> 标签：武侠仙侠

> 简介：吴笑天有三大新年愿望：成为武林大神，撩尽武林靓妹，创造武林传奇。但此刻的他却是扑街一个，还是武林扑街群的盟主。据说新年可以心想事成，这吴笑天能够如愿以偿吗？你看，他春风满脸，显然机会来了！

> 章节末：第七百二十七章 大结局

> 状态：完本
# 论文
### [Fast and explainable clustering based on sorting | Papers With Code](https://paperswithcode.com/paper/fast-and-explainable-clustering-based-on)
> 概要: We introduce a fast and explainable clustering method called CLASSIX. It consists of two phases, namely a greedy aggregation phase of the sorted data into groups of nearby data points, followed by the merging of groups into clusters. The algorithm is controlled by two scalar parameters, namely a distance parameter for the aggregation and another parameter controlling the minimal cluster size. Extensive experiments are conducted to give a comprehensive evaluation of the clustering performance on synthetic and real-world datasets, with various cluster shapes and low to high feature dimensionality. Our experiments demonstrate that CLASSIX competes with state-of-the-art clustering algorithms. The algorithm has linear space complexity and achieves near linear time complexity on a wide range of problems. Its inherent simplicity allows for the generation of intuitive explanations of the computed clusters.
### [AvatarCap: Animatable Avatar Conditioned Monocular Human Volumetric Capture | Papers With Code](https://paperswithcode.com/paper/avatarcap-animatable-avatar-conditioned)
> 日期：5 Jul 2022

> 标签：None

> 代码：https://github.com/lizhe00/avatarcap

> 描述：To address the ill-posed problem caused by partial observations in monocular human volumetric capture, we present AvatarCap, a novel framework that introduces animatable avatars into the capture pipeline for high-fidelity reconstruction in both visible and invisible regions. Our method firstly creates an animatable avatar for the subject from a small number (~20) of 3D scans as a prior. Then given a monocular RGB video of this subject, our method integrates information from both the image observation and the avatar prior, and accordingly recon-structs high-fidelity 3D textured models with dynamic details regardless of the visibility. To learn an effective avatar for volumetric capture from only few samples, we propose GeoTexAvatar, which leverages both geometry and texture supervisions to constrain the pose-dependent dynamics in a decomposed implicit manner. An avatar-conditioned volumetric capture method that involves a canonical normal fusion and a reconstruction network is further proposed to integrate both image observations and avatar dynamics for high-fidelity reconstruction in both observed and invisible regions. Overall, our method enables monocular human volumetric capture with detailed and pose-dependent dynamics, and the experiments show that our method outperforms state of the art. Code is available at https://github.com/lizhe00/AvatarCap.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
