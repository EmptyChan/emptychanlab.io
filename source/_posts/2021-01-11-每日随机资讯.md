---
title: 2021-01-11-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Yunkai_EN-CN9301719323_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-11 21:29:58
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Yunkai_EN-CN9301719323_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：创4学员上岛打包vlog曝光 展示行李与奇葩见面礼花样百出](http://slide.ent.sina.com.cn/z/v/slide_4_704_351054.html)
> 概要: 组图：创4学员上岛打包vlog曝光 展示行李与奇葩见面礼花样百出
### [《上阳赋》原著作者发文力挺章子怡](https://ent.sina.com.cn/v/m/2021-01-11/doc-iiznezxt1812583.shtml)
> 概要: 新浪娱乐讯 近日播出的电视剧《上阳赋》中，41岁的章子怡饰演15岁的少女阿妩，引发网友热议。1月10日晚，《上阳赋》原著《帝王业》作者“寐语者”发文力挺章子怡。　　“寐语者”文中表示，所有人都知道38......
### [视频：热巴扎双马尾青春俏皮 王源美颜滤镜下忽胖忽瘦](https://video.sina.com.cn/p/ent/2021-01-11/detail-iiznezxt1998729.d.html)
> 概要: 视频：热巴扎双马尾青春俏皮 王源美颜滤镜下忽胖忽瘦
### [视频：陈飞宇妈妈陈红关注欧阳娜娜 列表中只有三个明星](https://video.sina.com.cn/p/ent/2021-01-11/detail-iiznctkf1407726.d.html)
> 概要: 视频：陈飞宇妈妈陈红关注欧阳娜娜 列表中只有三个明星
### [视频：向太曝向佐郭碧婷已领证 时隔40年自己也领证](https://video.sina.com.cn/p/ent/2021-01-11/detail-iiznezxt1943881.d.html)
> 概要: 视频：向太曝向佐郭碧婷已领证 时隔40年自己也领证
### [张艺谋一生最对不起的两女人：一个成终生残疾，一个是自己亲女儿](https://new.qq.com/omn/20210111/20210111A0EYU100.html)
> 概要: 刘岩，让张艺谋愧疚一生的女人。本来她可以成为享誉世界的舞蹈家 ，却因为两个男人断送了她的前程，在2008年，奥运会开幕式彩排时摔成高位截瘫的独舞。            这两个男人，一个是郎昆，一个是......
### [黄圣依现身商演被围观，零下九度光腿穿旗袍，寒风凛冽中表演超卖力](https://new.qq.com/omn/20210111/20210111A09ZKS00.html)
> 概要: 黄圣依现身商演被围观，零下九度光腿穿旗袍，寒风凛冽中表演超卖力
### [从杨笠到小鹿，从男性群体到肖战粉丝，脱口秀演员的底线在哪？](https://new.qq.com/omn/20210111/20210111A0EZ1W00.html)
> 概要: 当看到《奇葩说》中“疯狂追星”这个辩题时，就知道必上热搜无疑，只不过没想到热搜的内容竟然跑了偏。            “为什么不能有灯牌里的妈妈？”辩手小鹿的这句话，赢得了现场雷鸣般的掌声，节目播出......
### [《追光吧！哥哥》官博与郑爽互动，疑似官宣郑爽将继续参与录制](https://new.qq.com/omn/20210111/20210111A0F0X600.html)
> 概要: 郑爽出道以来，关于她的一些话题，一直很高，并且很容易引起热搜，外界称郑爽有热搜体质。上个月，郑爽因私聊被播出，发文宣布将退出《追光吧！哥哥》，对此，官方也一直没有一个明确的态度。可就在今天（1月11日......
### [苏醒廖语辰快官宣吧，不用暗搓搓秀恩爱](https://new.qq.com/omn/20210111/20210111A0F03U00.html)
> 概要: 苏醒这段时间可谓是疯狂营业了，作为老一代选秀歌手，再次参加《追光吧哥哥》，也难道粉丝吐槽。虽然一开始就惨遭垫底，但他也不气馁，还把自己垫底这件事当作梗来开玩笑。            1月11日，他在......
# 动漫
### [今天，地球升级了！](https://news.dmzj.com/article/69855.html)
> 概要: 本周要给大家介绍的新连载是改编自生咲日月所著小说的同名漫画《地球先生升级了！@COMIC》。
### [都什么构造？日本网友投票不受现实世界制约的角色发型](https://news.dmzj.com/article/69857.html)
> 概要: 在动画作品中，有不少角色平时有着在现实中极难再现的发型。日本网站goo排行就请网友对最想吐槽的角色发型进行了投票。这次投票共有1767票。
### [《最游记》Even a worm篇动画化！宣传PV公开](https://news.dmzj.com/article/69851.html)
> 概要: 《最游记》系列宣布了Even a worm篇TV动画化决定的消息，本作动画的正式名称为《最游记RELOAD -ZEROIN-》。
### [P站美图推荐——翘腿特辑](https://news.dmzj.com/article/69849.html)
> 概要: 请！朝！这！里！踩！
### [《水豚汤馆》营业中！打造今冬最软萌娃娃机店！](https://new.qq.com/omn/ACF20210/ACF2021011100707100.html)
> 概要: 每周三10:00在腾讯视频独家热播中的《水豚汤馆》搞事情啦！联合北京LAMOLEMO娃娃机合作开启，蠢萌的水豚妹妹摇身一变成了可爱的玩偶，已经可以预定是今年最抢手的娃娃机产品了！这么萌的水豚妹妹不带一......
### [冬季国漫口碑之选！《水豚汤馆》喊你来泡汤！](https://new.qq.com/omn/ACF20210/ACF2021011100710200.html)
> 概要: 吊了大家好久胃口的《水豚汤馆》终于在1月6日开播啦！不知道在座各位有多少人被这一部治愈力满满的国漫正中红心了呢？反正我已经被萌到昏古七啦~软萌可爱的小动物，搞笑又治愈的剧情，这！才是猛男（划掉）该看的......
### [定格动画大师篠𠩤健太新作：EVA2号机体操表演](https://acg.gamersky.com/news/202101/1353791.shtml)
> 概要: 日本定格动画大师篠𠩤健太发布了新作品：EVA2号机体的操表演。
### [《星期一的丰满》：好身材妹子紧急事态 休假太安逸](https://acg.gamersky.com/news/202101/1353647.shtml)
> 概要: 本周《星期一的丰满》由上班族迷糊妹子带来。目前国外的新年假期基本已经结束了，如果新年假期过得太过安逸的话，就有可能会出现迷糊妹子的超囧状况，衣服突然都瘦了。
# 财经
### [石家庄藁城区通告:藁城籍和常驻藁城的人和车一律不得离藁](https://finance.sina.com.cn/china/2021-01-11/doc-iiznctkf1627802.shtml)
> 概要: 原标题：石家庄藁城区通告：藁城籍和常驻藁城的人和车一律不得离藁 石家庄藁城区应对新冠肺炎疫情工作领导小组办公室发布《藁城区关于疫情防控期间应急和民生车辆通行有关...
### [《经济学家时刻》：公共政策制定者不应该做什么](https://finance.sina.com.cn/jjxw/2021-01-11/doc-iiznctkf1629941.shtml)
> 概要: 《经济学家时刻》：公共政策制定者不应该做什么苏京春 公共政策制定者往往容易成为吐槽对象。有的人在抱怨他们已经做的事，有的人在抱怨他们正在做的事...
### [进京提速 北京生活必需品供应逐步回暖](https://finance.sina.com.cn/china/dfjj/2021-01-11/doc-iiznezxt2059191.shtml)
> 概要: 进京提速 北京生活必需品供应逐步回暖 来源：北京商报 北京商报讯（记者 赵述评 赵驰）1月11日，北京市召开第207场新冠肺炎疫情防控工作新闻发布会...
### [国家集采冠脉支架落地青海](https://finance.sina.com.cn/china/dfjj/2021-01-11/doc-iiznezxt2058596.shtml)
> 概要: 原标题：国家集采冠脉支架落地青海 据新华视点消息，1月1日起，国家集采冠脉支架正式落地北京、湖南、青海等18个省区市，原本动辄几千元甚至上万元的冠脉支架如今下降至700...
### [石家庄公布疫情防控期间11起价格违法案例：多家果蔬店、超市被罚](https://finance.sina.com.cn/jjxw/2021-01-11/doc-iiznctkf1634963.shtml)
> 概要: 原标题：石家庄公布疫情防控期间11起价格违法案例：有菜店药店被罚 据石家庄市场市场监督管理局官网近日消息，疫情防控期间，全市各级市场监管部门持续加大市场监管领域的...
### [10部门：2025年全国地级及以上缺水城市再生水利用率达到25%以上](https://finance.sina.com.cn/roll/2021-01-11/doc-iiznezxt2048479.shtml)
> 概要: 原标题：10部门：2025年全国地级及以上缺水城市再生水利用率达到25%以上 新华社北京1月11日电（记者安蓓）记者11日从国家发展改革委了解到...
# 科技
### [飞书回应 “遭遇腾讯微信不合理限制”：多次联系微信人工客服未果](https://www.ithome.com/0/529/448.htm)
> 概要: IT之家1月11日消息 今日，飞书发文表示，截至目前，腾讯微信一直未回应为何无理由封禁飞书，其曾尝试多次联系微信人工客服，发现根本无法联系上。此前，字节跳动副总裁谢欣1 月 7 日在微头条称，由于微信......
### [大牌牛头牌：灯影牛肉丝 4 两 19.99 元新低（京东 35 元）](https://lapin.ithome.com/html/digi/529437.htm)
> 概要: 牛头牌：灯影牛肉丝108g*2袋 日常售价34.99元，限时限量15元券，实付19.99元包邮：天猫牛头牌 灯影牛肉丝 108g*2袋1984年老牌券后19.99元领15元券有【麻辣】与【五香】2种口......
### [Setting goals for 2021 – A brief guide about personal goal setting](https://blog.doit.io/goal-setting/)
> 概要: Setting goals for 2021 – A brief guide about personal goal setting
### [Governments spurred the rise of solar power](https://www.economist.com/technology-quarterly/2021/01/07/how-governments-spurred-the-rise-of-solar-power)
> 概要: Governments spurred the rise of solar power
### [Opinionated Product 是未来产品趋势](https://www.tuicool.com/articles/jIv2A3m)
> 概要: 编辑导语：Opinionated Product，是指有自己明确的愿景和价值观，把价值观施加于用户，让用户按照自己对未来的愿景去做事情的产品。这样的产品往往会挑战用户传统习惯，提出激进的，创新的功能和......
### [Prolog软件包管理器](https://www.tuicool.com/articles/yUn6Vra)
> 概要: Prolog语言知识推理系列文章，不同编程语言有着不同编程视角，JAVA是面向对象，Nodejs是异步回调，R语言是统计算法，Prolog就是知识推理。每种语言都是独特的，如果想把一门语言学好用好，关......
# 小说
### [重启人生](http://book.zongheng.com/book/849972.html)
> 作者：沙漠

> 标签：都市娱乐

> 简介：天行健，君子以自强不息。人生低谷期，迎难而上者，总能打开一片新的世界，开启新的人生。一个创业者打造品牌的故事！

> 章节末：大结局

> 状态：完本
# 游戏
### [《英雄联盟》重做英雄投票开启 截止到1月20日](https://ol.3dmgame.com/esports/13384.html)
> 概要: 《英雄联盟》重做英雄投票开启 截止到1月20日
### [原创动画《奇蛋优先》播前PV公开 1月12日正式开播](https://www.3dmgame.com/news/202101/3806065.html)
> 概要: 著名剧作家・野岛伸司参与创作的完全原创新作TV动画《奇蛋优先》即将于1月12日明天正式开播，今天官方放出了最新播前PV，一起来先睹为快。原创动画《奇蛋优先（WONDER EGG PRIORITY）》是......
### [Xbox/Win10《如龙7》国际版2月25日发售 有中文](https://www.3dmgame.com/news/202101/3806043.html)
> 概要: 据微软商城预购页面的新消息，《人中之龙7光与暗的去向》国际版将于2021年2月25日正式发售，登陆Xbox Series X|S/XboxOne/Win10平台，包含了繁体与简体中文，附带早期购入特典......
### [《怪物猎人：崛起》新角色介绍：可爱接待员等登场](https://www.3dmgame.com/news/202101/3806022.html)
> 概要: 目前，卡普空官方公开了《怪物猎人：崛起》新一批角色介绍内容，此次介绍的人物有：公会经纪人、集会所接待员、教官、随从雇用窗口办事员、艾露猫首领和交易窗口办事员。公会经纪人岁丰稔开朗快活的村庄长老声优：绪......
### [百度也要造车了！吉利为官宣合作伙伴](https://www.3dmgame.com/news/202101/3806024.html)
> 概要: 今日（1月11日），百度作为全球领先的人工智能平台型公司，宣布正式组建一家智能汽车公司，以整车制造商的身份进军汽车行业。吉利控股集团将成为新公司的战略合作伙伴。据悉，新组建的百度汽车公司将面向乘用车市......
# 论文
### [Bridging Imagination and Reality for Model-Based Deep Reinforcement Learning](https://paperswithcode.com/paper/bridging-imagination-and-reality-for-model)
> 日期：23 Oct 2020

> 标签：None

> 代码：https://github.com/Mehooz/BIRD_code

> 描述：Sample efficiency has been one of the major challenges for deep reinforcement learning. Recently, model-based reinforcement learning has been proposed to address this challenge by performing planning on imaginary trajectories with a learned world model.
### [FeatureNMS: Non-Maximum Suppression by Learning Feature Embeddings](https://paperswithcode.com/paper/featurenms-non-maximum-suppression-by)
> 日期：18 Feb 2020

> 标签：None

> 代码：https://github.com/fzi-forschungszentrum-informatik/NNAD

> 描述：Most state of the art object detectors output multiple detections per object. The duplicates are removed in a post-processing step called Non-Maximum Suppression.
