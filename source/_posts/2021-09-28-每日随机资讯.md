---
title: 2021-09-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.TheBroads_ZH-CN0485661191_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-09-28 21:27:29
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.TheBroads_ZH-CN0485661191_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [东北拉闸限电的三个真实原因](https://www.huxiu.com/article/459822.html)
> 概要: 本文来自微信公众号：财经十一人（ID：caijingEleven），作者：韩舒淋、江帆，编辑：马克，题图来自视觉中国马路上红绿灯不亮，居民楼电梯突然停电，东北三省的限电问题正在引发全国关注。自9月中旬......
### [New work sheds light on the Etruscans, who may have founded Rome](https://www.science.org/content/article/they-may-have-founded-rome-then-vanished-work-sheds-light-mysterious-etruscans)
> 概要: New work sheds light on the Etruscans, who may have founded Rome
### [恐怖游戏《恐鬼症》更新 新增单人离线模式](https://www.3dmgame.com/news/202109/3824639.html)
> 概要: 近日多人合作恐怖游戏《恐鬼症》(Phasmophobia)迎来周年纪念更新，此次更新添加了一个单人离线模式，不用连接服务器也可以游玩。在单人离线模式里，没有AI操控的队友，玩家只能独自行动。这也让恐怖......
### [吴姗儒首公开未婚夫照片 粉丝称赞男帅女美好登对](https://ent.sina.com.cn/s/m/2021-09-28/doc-iktzscyx6764112.shtml)
> 概要: 新浪娱乐讯 近日，据台湾媒体报道，艺人吴姗儒（Sandy）27日向媒体证实将于明年1月29日结婚，晚间发长文，并晒出与未婚夫的合照，也是发表婚讯后首度放出照片，未来老公超高颜值被粉丝称赞“从韩剧走出来......
### [《鬼谷八荒》宗门版本新前瞻 10月16日上线](https://www.3dmgame.com/news/202109/3824641.html)
> 概要: 近日《鬼谷八荒》官方在微博上公布了《鬼谷八荒》宗门版本新前瞻，本次前瞻主要包括器灵配音、宗门大比积分赛、灭宗战等内容。官方还公布了前瞻视频，一起来看看吧！前瞻视频：官方表示内测已经在紧锣密鼓地进行了，......
### [「刀剑神域 爱丽丝篇 异界战争」亚丝娜手办开订](http://acg.178.com/202109/426793668383.html)
> 概要: 近日，「刀剑神域 爱丽丝篇 异界战争」亚丝娜手办正式开启预订，作品采用ABS、PVC材质，全高约350mm，日版售价26400日元（含税），约合人民币1557元，预计将于2022年1月发售......
### [《宝可梦钻石·珍珠》15周年 玩家回忆任天堂售后精神](https://www.3dmgame.com/news/202109/3824643.html)
> 概要: 虽然任天堂法务部威名赫赫，不过其在对待玩家们的售后服务方面确实极具人性化，9月28日今天是《宝可梦钻石·珍珠》正式发售15周年，一名日本老玩家媒体人就回顾了当年的经历，表示任天堂的善意售后拯救了当时小......
### [视频：吴京回应拍“抗日神剧” 余皑磊不要片酬理由爆笑全场](https://video.sina.com.cn/p/ent/2021-09-28/detail-iktzqtyt8541621.d.html)
> 概要: 视频：吴京回应拍“抗日神剧” 余皑磊不要片酬理由爆笑全场
### [「鬼灭之刃」柱合会议粘土人套装公开](http://acg.178.com/202109/426798270934.html)
> 概要: 近日，BANDAI SPIRITS公开了「Figuarts mini」系列粘土人的最新企划，本次推出的角色为动画中实力强大的象征“柱”的成员套装「柱合会议」，包含了9名全部队员。该套装粘土人均为关节可......
### [轻小说「欢迎来到实力至上主义的教室 二年级生篇」第5卷封面公开](http://acg.178.com/202109/426799057837.html)
> 概要: 轻小说「欢迎来到实力至上主义的教室 二年级生篇」公开了第5卷的封面插图，该卷将于10月25日发售。「欢迎来到实力至上主义的教室 二年级生篇」是由衣笠彰梧创作、トモセ シュンサク负责插画的轻小说作品，是......
### [视频：范景翔任艺术之星大赛评委 加盟《快乐天堂》诠释公益精神](https://video.sina.com.cn/p/ent/2021-09-28/detail-iktzqtyt8559053.d.html)
> 概要: 视频：范景翔任艺术之星大赛评委 加盟《快乐天堂》诠释公益精神
### [萩原ダイスケ新绘「鬼灭之刃」炼狱杏寿郎公开](http://acg.178.com/202109/426800233880.html)
> 概要: 近日，萩原ダイスケ（萩原大辅）公开了最新绘图，本次绘制的是「鬼灭之刃」炼狱杏寿郎。「鬼灭之刃」是日本漫画家吾峠呼世晴所著的少年漫画，自2016年2月15日—2020年5月11日在集英社「周刊少年Jum......
### [I don't think Elasticsearch is a good logging system](https://blog.sinkingpoint.com/posts/elasticsearch-logging/)
> 概要: I don't think Elasticsearch is a good logging system
### [45天套现20亿，周鸿祎也缺钱？](https://www.huxiu.com/article/459910.html)
> 概要: 本文作者：何玥阳，编辑：嘉辛，头图来自：视觉中国那个一向以直言著称的老周，最近又回来了。在2021年世界互联网大会前后，周鸿祎说了很多心里话。谈互联网相互屏蔽时，周鸿祎强调：过去几年，中国有些互联网巨......
### [百度智能云学生首购优惠：1 核 2G 云服务器 9 元/月](https://www.ithome.com/0/578/098.htm)
> 概要: IT之家9 月 28 日消息 百度智能云现已开启校园启航计划。完成学生认证（24 岁及以下免学生认证）的新用户可以 9 元/1 个月（或 18 元/3 个月、74 元/6 个月）的价格抢购 1 核 2......
### [山下智久首部写真集人气超高 豪华限定版确定再版](https://ent.sina.com.cn/jp/2021-09-28/doc-iktzscyx6839354.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 人气男星山下智久首部写真集《Circle（豪华限定版）》将于11月25日发行，因为预约人数太多成为话题，这部写真集分为通常版和豪华限定版，其中豪华限定版定价为11000日......
### [一位外国小哥把整个 CNN 都给可视化了，卷积、池化清清楚楚！网友：美得不真实...](https://www.tuicool.com/articles/NFjy2iu)
> 概要: 红色石头的个人网站：萧箫 鱼羊 发自 凹非寺做计算机视觉，离不开CNN。可是，卷积、池化、Softmax……究竟长啥样，是怎样相互连接在一起的？对着代码凭空想象，多少让人有点头皮微凉。于是，有人干脆用......
### [寺库，春天再没来过](https://www.tuicool.com/articles/R7feQfJ)
> 概要: 头图来源丨创客贴2020年冬袭来的疫情带着寒风凛冽，奢侈品行业陷入低迷，寺库裹挟其中。可哪怕经济渐渐回暖，寺库的春天，似乎也没再来过。文丨游璃来源：BT财经（ID：btcjv1）《说文解字》中，“奢”......
### [2014年的今天，内马尔第一次在西甲赛场完成帽子戏法！](https://bbs.hupu.com/45387514.html)
> 概要: 🔙2014年的今天🎩内马尔第一次在西甲赛场完成帽子戏法！😉
### [一打四，四杀，守家成功](https://bbs.hupu.com/45387599.html)
> 概要: 兄弟们觉得我这个一打四，四杀，守家成功有几分。元歌去带线不回来守，我想的是拖住这四个人，让他带掉，结果没守约单杀。
### [剑指阿里，张一鸣想要做成这件“苦活”](https://www.huxiu.com/article/460114.html)
> 概要: 出品｜虎嗅科技组作者｜张雪封面｜IC photo近日，字节跳动在云计算领域的新动作又开始浮出水面，比如发力汽车云，比如加大在数据中心的投入，二季度数据中心投入超过腾讯，位居世界第七。从这接连的布局中，......
### [视频：未见偶像高圆圆先见其老公 秦霄贤赵又廷再现初见尴尬场景](https://video.sina.com.cn/p/ent/2021-09-28/detail-iktzqtyt8636901.d.html)
> 概要: 视频：未见偶像高圆圆先见其老公 秦霄贤赵又廷再现初见尴尬场景
### [一人之下X小红书丨安利本命国漫，赢万元好礼！](https://new.qq.com/omn/ACF20210/ACF2021092800847100.html)
> 概要: 随着中国动漫行业的发展，国漫发展的速度也日新月异，出现了很多非常优质的动画内容，越来越多的人伴随和支持着国漫一同成长。            国庆佳节即将到来，在此举国同庆的佳节之际，一人之下正式入驻......
### [字节跳动想做音乐，可以好好学学YouTube](https://www.tuicool.com/articles/RR3EFfa)
> 概要: 字节跳动对于娱乐产业的野心依然没有消失，在抖音接连捧红神曲之后，字节跳动的下一个目标是决心要做成一个独立的音乐流媒体平台，尽管其在海外相应的音乐实验并未收获太大声量。但得益于国内监管机构对于音乐版权垄......
### [李诚儒曝《西游记》珍贵照片，总投资不足600万，拍出顶级水平](https://new.qq.com/omn/20210928/20210928A0BY8G00.html)
> 概要: 9月28日，李诚儒老师的《戏儒人生》第三期上线。在这一期节目当中，李诚儒老师以奇幻影视剧内容为题，聊了不少干货。尤其是李老师曝出了自己曾是杨洁导演的左膀右臂，作为重要的工作人员，参与制作了电视剧《西游......
### [Google’s dominance in ad tech supply chain harms businesses and consumers](https://www.accc.gov.au/media-release/google%E2%80%99s-dominance-in-ad-tech-supply-chain-harms-businesses-and-consumers)
> 概要: Google’s dominance in ad tech supply chain harms businesses and consumers
### [微软 CEO 纳德拉：TikTok 并购案是我见过的最奇怪的事情](https://www.ithome.com/0/578/181.htm)
> 概要: 北京时间 9 月 28 日消息，去年 8 月，微软公司曾提议收购短视频应用 TikTok 的部分业务，但最终失败。微软 CEO 萨提亚・纳德拉 (Satya Nadella) 今天在 Code 大会上......
### [限电，为什么还要搞电动车？](https://www.huxiu.com/article/459989.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔东北人，又多了一条放弃纯电动车的理由。近日，“限电潮”席卷全国多地省市。9月27日，辽宁、吉林、黑龙江有关部门先后就当前供电形势作出回应，称将全力保障基本民生用电需......
### [动漫美图丨优菈（傲娇大小姐）](https://new.qq.com/omn/20210928/20210928A0D5FO00.html)
> 概要: 该起床了，日出前两个小时，值岗一夜的哨兵最为疲惫，是我们活动的绝佳时间！“愿柔风伴你，度过安详的一夜。”……旧贵族的问候语里，难得有这么中听的一句......
### [画师大佬绘制的表情包拟人太有爱了，感觉可以做成动画啊](https://new.qq.com/omn/20210928/20210928A0DB4O00.html)
> 概要: 现在拟人化什么的真的很火，我个人就特别喜欢看猫咪的拟人，各种品种的可爱猫咪被拟人之后，变成了各种萌萌哒的猫耳娘，看起来特别可爱。除此之外，像是狗狗啊、狐狸啊各种动物的拟人也挺多的，现在更有诸如汽车、家......
### [奥特曼：我数了下，能让我落泪的作品，可能就只有这些](https://new.qq.com/omn/20210928/20210928A0DBIZ00.html)
> 概要: 《奥特曼》系列是日本最具代表性的特摄剧了。1966—2021年，圆谷推出了好多个相关之作，这些作品经历了昭和、平成、新生代等不同时期。由于各个时代不一样，加上那会儿技术、演员、导演以及编剧等各方面的差......
### [Dover (YC S19) just raised $20m and is hiring eng leadership](https://www.dover.com/open-roles/engineering-manager)
> 概要: Dover (YC S19) just raised $20m and is hiring eng leadership
### [阿里巴巴国际站将禁售虚拟货币矿机类产品](https://www.ithome.com/0/578/185.htm)
> 概要: IT之家9 月 28 日消息 阿里巴巴国际站发布了关于禁售虚拟货币矿机类产品的公告。阿里巴巴国际站表示，根据相关法律法规的规定，以及国际各市场对虚拟货币及虚拟货币相关产品的法律法规的不稳定性，经平台评......
### [ADAMoracle预言机第一轮公测结束](https://www.btc126.com//view/188991.html)
> 概要: 据官方消息，ADAMoracle实验室已于9月27日24:00宣布ADAMoracle广域节点网络第一轮公测结束。ADAMoracle实验室在为期两周的公开测试中完成了各类服务器适配测试，进行了产品完......
### [Bacon Protocol推出去中心化抵押平台](https://www.btc126.com//view/188992.html)
> 概要: 财经报道，去中心化抵押贷款机构Bacon Protocol于周二正式启动，允许加密货币投资者通过由美元硬币(USDC)和住房贷款支持的新稳定币参与住房市场。该平台使加密货币持有者可以直接进入目前由银行......
### [谷歌否认对安卓厂商“胡萝卜加大棒”：促进竞争而非扼杀对手](https://www.ithome.com/0/578/186.htm)
> 概要: 北京时间 9 月 28 日消息，谷歌公司周二在法院表示，公司与Android手机制造商达成的协议促进了竞争，而非欧盟所指控的这是扼杀对手的“胡萝卜加大棒”策略。谷歌正在就欧盟 43 亿欧元 (50 亿......
### [央视网评：拉闸限电里没那么多“大棋”](https://finance.sina.com.cn/china/gncj/2021-09-28/doc-iktzqtyt8665873.shtml)
> 概要: 原标题：拉闸限电里没那么多“大棋” 连日来，“限电停工潮”席卷全国十余个省份，尤其是东北多地，“拉闸限电”相关消息几度登上热搜。
### [“90后”职场大数据：超7成常加班，超2成资产过30万](https://finance.sina.com.cn/china/2021-09-28/doc-iktzscyx6898234.shtml)
> 概要: 超7成“90后”职场人名下资产不超过30万。其中，“10万-30万”占比最高，为27.29%；其次是“5万-10万”，占比26.12%；再者是“5万以下”占比21.65%。
### [山东省委书记、省长已任省推进济南起步区建设领导小组组长](https://finance.sina.com.cn/jjxw/2021-09-28/doc-iktzscyx6898497.shtml)
> 概要: 原标题：山东省委书记、省长已任省推进济南起步区建设领导小组组长 9月28日，第十届山东青年创新创业大赛颁奖暨泉城青年创新大会在济南新旧动能转换起步区举行。
### [全球金融观察｜如何打造国际金融中心？伦敦金融城治理模式对前海的启示](https://finance.sina.com.cn/china/gncj/2021-09-28/doc-iktzqtyt8666877.shtml)
> 概要: 原标题：全球金融观察|如何打造国际金融中心？伦敦金融城治理模式对前海的启示 来源：21世纪经济报道 最近，国家密集出台了国际金融中心建设政策与规划。
### [《新世界》Steam在线超47万 当前评价褒贬不一](https://www.3dmgame.com/news/202109/3824695.html)
> 概要: 亚马逊网游《新世界》全球开服才过了仅仅几个小时，但根据SteamDB数据显示，这款游戏已经获得了相当高的人气。截止到发稿时，该作同时在线玩家最高来到了47万，而且继续有上涨的趋势。Twitch观看人数......
### [流言板瑞士记者：扎卡右膝内侧韧带断裂，预计伤缺至少6-8周](https://bbs.hupu.com/45389935.html)
> 概要: 虎扑09月28日讯 根据瑞士记者Andreas Böni的报道，首次诊断扎卡为右膝内侧韧带断裂，而这种伤势至少要6-8周的恢复时间。随后扎卡还将进一步的检查，不过十字韧带的伤势可以被排除。在上周末北伦
### [流言板萨迪克-贝：入选选拔队非常酷，增加三分球能力对我们有利](https://bbs.hupu.com/45390040.html)
> 概要: 虎扑09月28日讯 活塞队媒体日今日举行，活塞球员萨迪克-贝接受媒体采访，谈到了自己入选美国选拔队的经历。“我对能拥有这次机会而充满感激。在那里我可以对抗那种级别的球员，这太酷了。”萨迪克-贝在采访中
### [国家防办、应急管理部：当前防汛工作重点是四川、陕西、河南、湖北等地较强降雨过程防范](https://finance.sina.com.cn/china/2021-09-28/doc-iktzqtyt8667980.shtml)
> 概要: 原标题：国家防办、应急管理部：当前防汛工作重点是四川、陕西、河南、湖北等地较强降雨过程防范 应急管理部网站消息，9月28日，国家防办...
### [北京国际戏剧中心让北京人艺“提速”](https://new.qq.com/omn/20210928/20210928A0EGYJ00.html)
> 概要: 距离晚上的演出开始还有几个小时，北京人艺的大院外就已经有观众等待排队入场……晚上18时30分左右，越来越多的观众出示了“健康码”“行程卡”走进大院，首都剧场前的广场上可以听到剧场工作人员在引导观众，“......
### [虚幻5 技术Demo现已免费登陆Steam 最低GTX 1070](https://www.3dmgame.com/news/202109/3824696.html)
> 概要: 免费虚幻5引擎技术Demo“光之集市（The Market of Light）”现已免费登陆了Steam，玩家可以下载体验。该技术Demo由日本工作室Historia开发，他们曾开发了RPG游戏《Ca......
### [波卡生态概念板块今日平均跌幅为5.91%](https://www.btc126.com//view/188998.html)
> 概要: 财经行情显示，波卡生态概念板块今日平均跌幅为5.91%。26个币种中8个上涨，18个下跌，其中领涨币种为：KTON(+8.66%)、PCX(+4.35%)、MXC(+4.11%)。领跌币种为：RING......
### [美元指数DXY突破前高93.745，创去年11月以来新高](https://www.btc126.com//view/189000.html)
> 概要: 行情显示，美元指数DXY突破前高93.745，创去年11月以来新高......
### [骂完陈坤了，那之后呢？行业现状不改了？](https://new.qq.com/omn/20210928/20210928A0EHDY00.html)
> 概要: 最近两天，第十一届北京国际电影节开幕了。嘉宾这边请到了巩俐、陈坤、陈正道、乌尔善、张颂文作为“天坛奖”的评委。别的不说，光这几个金光闪闪的名字，就足够引起讨论了。于是这两天，阿杠经常能看到围绕北影节展......
### [《快本》收视创今年新低！最近一期的节目中，暴露出多少问题？](https://new.qq.com/omn/20210928/20210928A0EHPF00.html)
> 概要: 现在活跃在各大视频软件的综艺节目不少，有很多新型的综艺节目，也有很多老牌综艺，所以各种综艺节目的竞争也是必不可少的，大家都在争谁的收视率高，谁的关注度高，随着物质水平的提高，大家开始更多的关心精神世界......
### [阿尔斯通前高管：我没孟晚舟那么幸运 我的祖国也没提供强大支持](https://finance.sina.com.cn/china/2021-09-28/doc-iktzqtyt8669315.shtml)
> 概要: 外交部发言人华春莹9月28日在例行记者会上回答提问时表示，众所周知，美国是胁迫外交的开山鼻祖，这项专利的发源地和指挥部就在华盛顿。
### [“医美贷”广告遭封杀，马上、捷信消金等已“全身而退”](https://www.tuicool.com/articles/eYjYVnZ)
> 概要: 新言财经（微信ID：tech621）作者|南惜封面来源|视觉中国“医美贷”再迎强监管。9月27日晚，国家广播电视总局办公厅发布《关于停止播出“美容贷”及类似广告的通知》称，“近期发现，一些“美容贷”广......
# 小说
### [重生无尽世界](http://book.zongheng.com/book/1017744.html)
> 作者：太上l忘情

> 标签：奇幻玄幻

> 简介：茫茫宇宙，无边无垠。大千世界，无奇不有。前路漫漫，无穷无尽。生命不息，轮回不止。一对苦命鸳鸯，历经磨难后终于走到一起，但天不遂人愿，生生将之分离。失落之中，意外再生，他被雷击中，从楼顶掉了下去。这是早已注定的结局吗？不，这只是刚刚开始。且看忘情神帝为求真相，踏遍无尽世界，走向主宰之路。

> 章节末：第三百七十三章  离开，落幕

> 状态：完本
# 论文
### [COCO Denoiser: Using Co-Coercivity for Variance Reduction in Stochastic Convex Optimization | Papers With Code](https://paperswithcode.com/paper/coco-denoiser-using-co-coercivity-for)
> 日期：7 Sep 2021

> 标签：None

> 代码：https://github.com/manuelmlmadeira/coco-denoiser

> 描述：First-order methods for stochastic optimization have undeniable relevance, in part due to their pivotal role in machine learning. Variance reduction for these algorithms has become an important research topic.
### [Predicting emergent linguistic compositions through time: Syntactic frame extension via multimodal chaining | Papers With Code](https://paperswithcode.com/paper/predicting-emergent-linguistic-compositions)
> 日期：10 Sep 2021

> 标签：None

> 代码：None

> 描述：Natural language relies on a finite lexicon to express an unbounded set of emerging ideas. One result of this tension is the formation of new compositions, such that existing linguistic units can be combined with emerging items into novel expressions.
