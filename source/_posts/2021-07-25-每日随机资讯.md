---
title: 2021-07-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CityPalaceUdaipur_ZH-CN2773121437_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-07-25 22:41:50
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CityPalaceUdaipur_ZH-CN2773121437_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [FreeBSD 2021 Q2 报告](https://www.oschina.net/news/152051/freebsd-q2-2021-report)
> 概要: FreeBSD 公布了今年的Q2 报告，介绍了 4 月到 6 月的主要工作内容和项目进展。下面简单介绍 FreeBSD 在今年第二季度的主要工作。COVID-19 对 FreeBSD 基金会的影响与大......
### [Fedora Workstation 35 将默认启用 Power Profiles Daemon](https://www.oschina.net/news/152050/fedora-workstation-default-use-power-profiles-daemon)
> 概要: 根据提案显示，Fedora Workstation 35 将安装 power-profiles-daemon 并默认启用，以帮助用户在优化系统性能或电池寿命之间进行选择。power-profiles-......
### [CVE-2021-32761: 32 位 Redis 远程代码执行漏洞](https://www.oschina.net/news/152046)
> 概要: 报告编号：B6-2021-072201报告来源：360CERT报告作者：360CERT更新日期：2021-07-221 漏洞简述2021年07月22日，360CERT监测发现Redis官方发布了Red......
### [日剧翻拍人气韩剧，人设中二又搞笑](https://news.dmzj.com/article/71637.html)
> 概要: 《她很漂亮》这部剧最初版是韩国超人气水木迷你连续剧，由黄正音、朴叙俊主演，讲述了儿时财貌双全、长大后却一无所有的女主人公与儿时默默无闻、长大后却变身为完美男子的男主人公之间搞笑的爱情故事。此次日剧也选择了该剧翻拍，由中岛健人、小芝风花主演，现7月...
### [视频：首日勇夺三金！张雨绮发文祝贺“中国姑娘了不起”](https://video.sina.com.cn/p/ent/2021-07-25/detail-ikqciyzk7447766.d.html)
> 概要: 视频：首日勇夺三金！张雨绮发文祝贺“中国姑娘了不起”
### [《Blacktail》开发者表示 XSS是“一款矛盾的主机”](https://www.3dmgame.com/news/202107/3819697.html)
> 概要: 《Blacktail》开发商THEPARASIGHT的首席执行官兼创意总监在接受Gamingbolt采访时对微软的次世代硬件提供了一些很棒的见解，Kapron表示Xbox Series S是一款自相矛......
### [《古墓丽影：暗影》PS5获更新 增加4K/60FPS支持](https://www.3dmgame.com/news/202107/3819698.html)
> 概要: 《古墓丽影:暗影》在PlayStation5上获得惊人更新，增加了4K分辨率/60fps支持。《古墓丽影:暗影》本周更新了2.01版本。其中提到：“在PlayStation 5的高帧率模式下支持4K分......
### [《侏罗纪世界：进化2》开发者日志 介绍增强特性](https://www.3dmgame.com/news/202107/3819700.html)
> 概要: 如果你关注Frontier Developments即将推出的恐龙管理模拟游戏《侏罗纪世界：进化2》，这里有一些信息可以分享。官方日前公布了这款游戏的第一期开发者日志视频，其中详细描述了这款期待已久的......
### [日本民众怎么评价东奥开幕式？](https://www.huxiu.com/article/443413.html)
> 概要: 本文来自微信公众号：东西文娱（ID：EW-Entertainment），撰文：东西文娱 北京办公室 日本市场组 负责人 杨晶。原文标题：《日本民众怎么评价东奥开幕式》；头图来自Time Magazin......
### [组图：恋情再添实锤？王大陆蔡卓宜低调同游雍和宫被偶遇](http://slide.ent.sina.com.cn/star/slide_4_704_359612.html)
> 概要: 组图：恋情再添实锤？王大陆蔡卓宜低调同游雍和宫被偶遇
### [漫画「鸭乃桥论的禁忌推理」第3卷封面公开](http://acg.178.com/202107/421177790387.html)
> 概要: 漫画「鸭乃桥论的禁忌推理」公开了最新的第3卷封面和宣传图，该卷将于8月4日发售。侦探题材的奇幻漫画「鸭乃桥论的禁忌推理」是人气漫画「家庭教师」的作者·天野明老师的最新作，讲述了一个天才侦探和一个耿直警......
### [神山健治×荒牧伸志《银翼杀手：黑莲花》预告片 今秋开播](https://news.dmzj.com/article/71638.html)
> 概要: 日美联手打造《银翼杀手》系列新动画《银翼杀手：黑莲花》（原名：Blade Runner: Black Lotus）公开了预告片。动画第一季预定2021年秋开播，全13集，由曾执导《攻壳机动队》的荒牧伸志和神山健治担任监督，《星际牛仔》的渡边信一郎...
### [伊藤润二拿下美国漫画大奖 《地狱星》获最佳亚洲作品](https://acg.gamersky.com/news/202107/1409170.shtml)
> 概要: 据日本媒体Liverdoor报道，漫画家伊藤润二的作品近日在美国埃斯纳奖竞奖单元获颁“最佳亚洲作品”等奖项，一起来了解一下。
### [《白蛇2：青蛇劫起》票房破亿 《白蛇：缘起》7月30日日本上映](https://news.dmzj.com/article/71639.html)
> 概要: 国产动画电影《白蛇2：青蛇劫起》上映两天票房正式过亿，截止目前（7月25日18点）猫眼票房显示即时票房1.8亿。同时前作动画 《白蛇：缘起》7月30日将在日本上映。
### [伊藤润二同时获美国埃斯纳漫画奖2项大奖 系日本作者首次](https://news.dmzj.com/article/71640.html)
> 概要: 日本漫画家伊藤润二的漫画作品《地狱星》和《伊藤润二短篇集BEST OF BEST》分别获得美国最具代表性的漫画奖"埃斯纳奖"，这是日本作者首次同时有2部作品获得此奖两个部门的奖项。
### [《海贼王女》新预告公布 白发王女落发为寇](https://acg.gamersky.com/news/202107/1409179.shtml)
> 概要: I.G原创动画《海贼王女》发布了一段新PV。
### [洪灾中的“救援神器”：无人机、救生机器人、动力舟桥……](https://www.ithome.com/0/564/997.htm)
> 概要: 在台风肆虐、暴雨侵袭的 7 月，河南防汛救灾仍在进行时。一方有难，八方支援。“83 军”、“平凡英雄”犹如天降神兵，企业慷慨捐赠、民众奉献爱心，共同铸成救援后盾。而在防汛救灾体系中，人力之外，无人机、......
### [组图：《千古玦尘》收官曝单人海报 周冬雨许凯玦恋相守共白头](http://slide.ent.sina.com.cn/tv/slide_4_704_359616.html)
> 概要: 组图：《千古玦尘》收官曝单人海报 周冬雨许凯玦恋相守共白头
### [一个提升英文单词拼写检测性能 1000 倍的算法？](https://www.tuicool.com/articles/zqIneyF)
> 概要: 序言小明同学上一次在产品经理的忽悠下，写好了一个中英文拼写纠正工具：https://github.com/houbb/word-checker。本来以为可以一劳永逸了，直到昨天闲来无事，发现了另一个开......
### [「邻家的吸血鬼小妹」かぴばらくじ商品图公开](http://acg.178.com/202107/421189294841.html)
> 概要: 「邻家的吸血鬼小妹」（邻家索菲）公开了最新的かぴばらくじ商品图。「邻家的吸血鬼小妹」是甘党创作的漫画作品，讲述了天野灯在被吸血鬼女孩“索菲”救下后对她产生了感情，并搬到了索菲家里与她共同生活的故事。根......
### [Reimagined toilets at a South Korea university transform human waste into biogas](https://www.atlasobscura.com/articles/korea-toilet-biogas)
> 概要: Reimagined toilets at a South Korea university transform human waste into biogas
### [将芯片节点缩小至 1nm 以下：彭博社对话 IMEC CEO](https://www.ithome.com/0/565/008.htm)
> 概要: 近日，比利时微电子研究中心（IMEC）发布了1nm 工艺下金属互连的新方法，解决了 1nm 节点互连发热问题。事实上，IMEC 已经成为了全球半导体生态中的重要一环，在先进制程工艺的开发中有着举足轻重......
### [「哔哩哔哩」2233娘手办开订](http://acg.178.com/202107/421192830029.html)
> 概要: F:NEX作品「哔哩哔哩」2233娘手办开订，高约260mm，主体采用ABS、PVC材料制造。该手办定价为41580日元（含税），约合人民币2438元，预计于2022年6月发售......
### [被困30多个小时后，她逃出了K31](https://www.huxiu.com/article/443447.html)
> 概要: 本文来自微信公众号：新记者（ID：njuxjz），作者：雒少龙、王欣睿，头图来自：视觉中国“我没有拍照，那场面太恐怖了，我都想忘掉，不想记得。”电话那头的李女士回忆起被困在列车上的场面仍然心有余悸。“......
### [亚马逊下重手！超过5万中国卖家被封，损失超千亿，商务部回应来了](https://www.tuicool.com/articles/Nbuu2aq)
> 概要: 世界电商看中国，中国电商看深圳。在亚马逊上，70%的卖家来自于中国；而中国的跨境电商中，广东的卖家能占到70%，这当中50%的份额都来自于深圳。因中国在抗疫防疫的优异表现，国内跨境电商行业迎来了一大波......
### [「推理之绊」漫画作画最新绘制的插图公开](http://acg.178.com/202107/421195529283.html)
> 概要: 「推理之绊」漫画作画水野英多公开了自己最新绘制的鸣海步&结崎雏乃插图。「推理之绊」是城平京所作的日本漫画，后来推出电视动画以及小说，drama CD，画集等衍生作品。该漫画讲述了月臣学院一年级学生鸣海......
### [组图：ibg的天堂！杨洋迪丽热巴海报拍摄花絮高甜互动又苏又撩](http://slide.ent.sina.com.cn/tv/slide_4_704_359618.html)
> 概要: 组图：ibg的天堂！杨洋迪丽热巴海报拍摄花絮高甜互动又苏又撩
### [可兑 12 斤：奥妙浓缩洗衣原浆 3 瓶 29.9 元（立减 60 元）](https://lapin.ithome.com/html/digi/565024.htm)
> 概要: 【天猫超市次日达】奥妙 桉树艾草浓缩洗衣原浆 400g*3 瓶售价 89.9 元，今日下单立减 30 元，可领 15 元单品券 + 5 元超市券，实付 39.9 元。下单再返 10 元超市卡（结算可见......
### [《真人快打传奇》新动画电影预告公布 雷电破树](https://acg.gamersky.com/news/202107/1409228.shtml)
> 概要: 《真人快打传奇：王国之战》新预告公布。
### [明天中国板仔首登奥运，咱该关注点啥？](https://www.huxiu.com/article/443474.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。当王一迪小时候在东直门来福士门口体验到滑......
### [河南慈善总会票据显示：华为向河南捐款3000万元](https://www.3dmgame.com/news/202107/3819739.html)
> 概要: 河南遭遇空前特大暴雨灾害，社会各界都在伸出援手，以各种方式帮助河南共渡难关。来自河南省慈善总会的最新消息显示，7月25日，华为技术有限公司向河南省慈善总会捐赠3000万元，用于支援河南省抗洪救灾。不过......
### [视频：北野武吐槽东奥会开幕式称看睡着了 还纳税人钱！](https://video.sina.com.cn/p/ent/2021-07-25/detail-ikqciyzk7576829.d.html)
> 概要: 视频：北野武吐槽东奥会开幕式称看睡着了 还纳税人钱！
### [Amazon Web Services In Plain English (2019)](https://www.web3us.com/how-guides/amazon-web-services-plain-english)
> 概要: Amazon Web Services In Plain English (2019)
### [金山捐赠 1000 万元支持河南灾后重建，雷军等人点赞](https://www.ithome.com/0/565/042.htm)
> 概要: IT之家7 月 25 日消息 金山软件今日宣布，携西山居等捐赠 1000 万元支持河南灾后重建。小米创办人，董事长兼 CEO；金山软件董事长雷军对此表示支持，金山集团高级副总裁、西山居 CEO 郭炜炜......
### [黄金赛道覆灭记](https://www.tuicool.com/articles/EV3Mruq)
> 概要: 在我们经常使用的“PEST”分析框架中，P（Politics）永远排在第一位。多少赛道就是因为一纸政令而直接血流成河。P2P，幼儿园，电子烟等等。而今天，政策的车轮碾过了教培机构，所过之处，愁云遍地......
### [Pachyderm (YC W15) Is Hiring a React/TypeScript Engineer and DevOps](https://www.pachyderm.com/careers/#positions)
> 概要: Pachyderm (YC W15) Is Hiring a React/TypeScript Engineer and DevOps
### [最新通报：珠海金海大桥垮塌事故已发现2名遇难者 施工单位为中铁建大桥工程局集团](https://finance.sina.com.cn/china/2021-07-25/doc-ikqcfnca8983395.shtml)
> 概要: 原标题：最新通报！珠海金海大桥垮塌事故已发现2名遇难者，施工单位为中铁建大桥工程局集团 每经编辑 彭水萍 据“珠海发布”微信号最新消息，7月25日9时25分...
### [哥伦比亚金融监管机构批准一项为期12 月的加密货币试点计划](https://www.btc126.com//view/178660.html)
> 概要: 哥伦比亚金融监管机构 Superfinanciera 最近批准一项为期 12 个月的试点计划，该计划将允许哥伦比亚人通过 Movvi 和 Bitpoint 进行提现和提现。该平台的用户将能够使用哥伦比......
### [河南银保监局组织召开金融支持灾后重建和 恢复生产生活秩序专题座谈会](https://finance.sina.com.cn/china/2021-07-25/doc-ikqciyzk7589014.shtml)
> 概要: 7月25日下午，河南银保监局组织召开融支持灾后重建和恢复生产生活秩序专题座谈会，听取辖内主要银行机构工作情况汇报，并对金融支持灾后重建和恢复生产生活作出安排部署。
### [“野性消费”致鸿星尔克库存告急 总裁吴荣照再发声：不要神化鸿星尔克](https://finance.sina.com.cn/china/2021-07-25/doc-ikqcfnca8988080.shtml)
> 概要: 原标题：“野性消费”致鸿星尔克库存告急！总裁吴荣照再发声：不要神化鸿星尔克 每经编辑：王鑫 7月25日下午，鸿星尔克在抖音官方账号发布发货以及库存告急公告称...
### [社论：非营利性是规范校外培训的关键](https://finance.sina.com.cn/china/gncj/2021-07-25/doc-ikqciyzk7591217.shtml)
> 概要: 要回归教育正常生态，首先是要让教育回归公益性本质，而不能让资本通过校外培训裹挟教育行业偏离正常的发展轨道。 教育的本质在于教书育人...
### ["绿色股票"试水欧洲](https://finance.sina.com.cn/china/gncj/2021-07-25/doc-ikqcfnca8987792.shtml)
> 概要: 原标题：“绿色股票”试水欧洲丨明言ESG 绿色股权作为一种新型可持续金融产品，为全球投资者提供了新的可供选择的资产类别。 自2009年世界首笔标准化绿色债券发行以来...
### [波卡生态概念板块今日平均跌幅为0.25%](https://www.btc126.com//view/178663.html)
> 概要: 财经行情显示，波卡生态概念板块今日平均跌幅为0.25%。26个币种中13个上涨，13个下跌，其中领涨币种为：KTON(+14.39%)、AKRO(+9.90%)、CELR(+5.51%)。领跌币种为：......
### [Tomato fruits send electrical warnings to the rest of the plant when attacked](https://blog.frontiersin.org/2021/07/20/tomato-fruits-send-electrical-warnings-to-the-rest-of-the-plant-when-attacked-by-insects/)
> 概要: Tomato fruits send electrical warnings to the rest of the plant when attacked
### [RTX 40系显卡爆料：台积电5nm工艺 研发已经完成](https://www.3dmgame.com/news/202107/3819744.html)
> 概要: 据外媒 VideoCardz 消息，英伟达下一代 RTX 40 系列游戏显卡代号为“Ada Lovelac”，近日有名为 @Greymon55 的爆料者在推特透露了新显卡的一些信息。他表示，英伟达 R......
### [广东21个人口超级镇：第一名近百万人 东莞占十席](https://finance.sina.com.cn/china/2021-07-25/doc-ikqciyzk7593935.shtml)
> 概要: 相比内陆地区一些袖珍小县只有几万人，东南沿海的一些超级镇则动辄几十万人。 第一财经记者梳理了广东各地发布的第七次全国人口普查数据...
### [晚报 | 7月25日晚间重要动态一览](https://www.btc126.com//view/178665.html)
> 概要: 12:00-21:00关键词：马斯克、互金协会、FTX、哥伦比亚......
### [王宝强连线救灾士兵全程笑嘻嘻遭吐槽，透露儿子日常习惯引网友担忧](https://new.qq.com/omn/20210725/20210725A09USG00.html)
> 概要: 今天，王宝强连线在河南救灾抗洪的人民子弟兵，在视频中不断的感谢他们，感谢他们一直在争分夺秒抢救人们的生命和财产，随后还敬军礼表达谢意。            不过在视频中王宝强开始并没有很严肃，而是一......
### [奥运新闻美三崩？美国第三节仅得到11分，法国得到25分](https://bbs.hupu.com/44389710.html)
> 概要: 虎扑07月25日讯 今日，东京奥运会男篮小组赛A组焦点战美国vs法国的比赛正在进行中，目前比赛进行到第四节。第三节比赛结束，美国以56-62被法国反超。本场比赛，美国第三节仅得到11分，法国得到25分
### [张柏芝3岁儿子正脸曝光！五官帅气头发浓密，打扮潮流小腿肉嘟嘟](https://new.qq.com/omn/20210725/20210725A0A0EF00.html)
> 概要: 近日，有网友偶遇张柏芝带着几个孩子独自现身上海浦东机场，身边没有其他助理的她化身超级妈妈，儿子们也很是乖巧紧跟在身旁。            网友发布的照片中，张柏芝随意披散着头发，虽然戴着口罩距离颇......
### [站在巨人肩膀，学习新技术！](https://www.tuicool.com/articles/eqIbiin)
> 概要: 技术的海洋，没有终点，只有杨帆，“书山有路勤为径，学海无涯苦作舟”我们扬帆起航、乘风破浪，当然技术的知识海洋少不了的公众号的陪伴。轻扫一下二维码就行了，你可以试试，肯定会有意外收获。今天介绍几个优质的......
### [上映3天，票房近2亿，网友：特效有多好，故事剧情就有多差](https://new.qq.com/omn/20210725/20210725A0A29900.html)
> 概要: 文|令狐伯光如果从国产动画电影产业上发展来讲，2015年的《大圣归来》是国产动画电影首次开始赚钱，后面一直延续到《哪吒之魔童降世》。我们可以视为国产动画这一阶段的结束，或者是国产动画第二阶段的开始。 ......
### [曼德拉孙子之一Dumani Mandela将于8月中旬在OpenSea拍卖其小说NFT](https://www.btc126.com//view/178668.html)
> 概要: 纳尔逊·曼德拉（Nelson Mandela）的17个孙子之一Dumani Mandela将从今年8月开始出售《I Dream of Kemet》和《Young and on the run from......
### [奥运新闻关键时刻！巴图姆底角毫不手软命中三分](https://bbs.hupu.com/44389961.html)
> 概要: 虎扑07月25日讯 今日，东京奥运会男篮小组赛A组焦点战美国vs法国的比赛正在进行中，目前比赛进行到第四节。第四节关键时刻，巴图姆底角毫不手软命中三分。 来源：  虎扑
### [影后咏梅：父母相继去世，栾树相伴熬过悲伤，相爱近30年至今无子](https://new.qq.com/omn/20210725/20210725A0A0B100.html)
> 概要: 双料影后咏梅：打败歌后王菲，成为栾树相守一生的真命天女——引言。7月中旬，咏梅在个人社交平台上分享了她平淡生活中的一角，翻看她之前的动态，生活气息满满。            这么一位低调、气质独特的......
### [奥运新闻致命失误！美国队开球，利拉德接球滑倒](https://bbs.hupu.com/44390258.html)
> 概要: 虎扑07月25日讯 今日，东京奥运会男篮小组赛A组焦点战美国vs法国的比赛正在进行中，目前比赛进行到第四节。第四节比赛，美国美74-78落后法国，随后美国队开球，达米安-利拉德接球滑倒。裁判看过回放后
### [奥运新闻关键时刻连续三分不中，美国不敌法国苦吞奥运会小组赛首败](https://bbs.hupu.com/44390331.html)
> 概要: 虎扑07月25日讯 今日，东京奥运会男篮小组赛A组焦点战美国vs法国。两队鏖战到关键时刻，法国队埃文-富尼耶三分果断命中，76-74法国领先，随后美国连续三分出手不中，最终球权来到法国这边。开球后，美
# 小说
### [记落](http://book.zongheng.com/book/1008552.html)
> 作者：颜词旧殇

> 标签：评论文集

> 简介：凡尘琐事，世俗眼光，悲喜从何而来，月下酒杯，爱恨愁。

> 章节末：悲喜（全本完）

> 状态：完本
# 论文
### [DMSANet: Dual Multi Scale Attention Network](https://paperswithcode.com/paper/dmsanet-dual-multi-scale-attention-network)
> 日期：13 Jun 2021

> 标签：None

> 代码：https://github.com/abhinavsagar/DMSANet

> 描述：Attention mechanism of late has been quite popular in the computer vision community. A lot of work has been done to improve the performance of the network, although almost always it results in increased computational complexity.
### [Hierarchical Multi-head Attentive Network for Evidence-aware Fake News Detection](https://paperswithcode.com/paper/hierarchical-multi-head-attentive-network-for)
> 日期：4 Feb 2021

> 标签：FAKE NEWS DETECTION

> 代码：https://github.com/nguyenvo09/EACL2021

> 描述：The widespread of fake news and misinformation in various domains ranging from politics, economics to public health has posed an urgent need to automatically fact-check information. A recent trend in fake news detection is to utilize evidence from external sources.
