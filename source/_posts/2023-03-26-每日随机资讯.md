---
title: 2023-03-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WildAnza_ZH-CN2384861750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-26 20:42:20
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WildAnza_ZH-CN2384861750_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [在线视频如何搭建防盗链？](https://www.woshipm.com/pd/5790254.html)
> 概要: 盗链是指不法分子通过一些手段，将别人的资源（例如图片、视频等）嵌入到自己的网站或APP中，从而达到谋取利益的目的。作为视频平台，盗链会让其损失惨重。那么，有没有一种行之有效的防盗链技术呢？本文作者对此......
### [年轻人逃离大厂，如同80后逃离北上广](https://www.woshipm.com/user-research/5788020.html)
> 概要: #本文为人人都是产品经理《原创激励计划》出品。今年的招聘市场从开年以来就颇为不乐观，岗位减少、薪酬降低、裁员，曾经的金三银四变成铜墙铁壁。而在这样的环境下，大厂的生存状况也不乐观，一些年轻人甚至主动离......
### [关于GPT-4的产品化狂想](https://www.woshipm.com/ai/5790742.html)
> 概要: GPT-4的出现，成为了科技界的一个惊雷。但是，任何技术想要最终发挥效应，都要经历一个“技术-产品-经济效益”的三阶段发展。这其中，产品化是承上启下不可或缺的一步。本文作者对GPT-4的产品化提出了一......
### [“211毕业后，我靠体力活谋生”](https://www.huxiu.com/article/902379.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 、题图 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。几乎所有中国孩......
### [言承旭受访自曝单身 衷心祝福林志玲希望她过得好](https://ent.sina.com.cn/s/h/2023-03-26/doc-imynczhi4066173.shtml)
> 概要: 新浪娱乐讯 3月24日，言承旭接受台媒访问，在访问中谈到了过往经历，以及当下的感情状态。他只表示目前单身，希望可以遇到让他想不顾一切交往的人，如果对方同意，会公开让全世界知道他有多幸福。言承旭之前与林......
### [《无职转生》TV动画第二季新预告 确定7月开播](https://www.3dmgame.com/news/202303/3865660.html)
> 概要: 根据同名人气轻小说改编的新番TV动画《无职转生：到了异世界就拿出真本事》第二季官方宣布，将于7月开播，同时公布了最新主艺图与预告，一起来先睹为快。•《无职转生 ~到了异世界就拿出真本事~》是不讲理不求......
### [虚拟人是坐不了牢，但企业可能替它坐牢](https://www.huxiu.com/article/929564.html)
> 概要: 出品｜虎嗅ESG作者｜袁加息头图｜《底特律：变人》游戏截图本文是#ESG进步观察#系列第021篇文章本次观察关键词：负责任AI、可信虚拟人AIGC大模型放飞了市场对虚拟人的想象。给ChatGPT穿一个......
### [乔纳森-梅杰斯被捕 曾出演《蚁人3》《奎迪3》](https://ent.sina.com.cn/m/f/2023-03-26/doc-imynefqf3948392.shtml)
> 概要: 新浪娱乐讯 乔纳森·梅杰斯（《蚁人3》《奎迪3》）在当地时间3月25日于纽约被捕。警方透露原因为“家庭纠纷”，未曝光身份的受害者是“一个30岁女性……告知警方她被袭击。……她的头、颈部有轻伤，状况稳定......
### [36氪首发｜主打新鲜现制，连锁酸奶品牌「王子森林」完成千万元战略融资](https://36kr.com/p/2184138311778439)
> 概要: 36氪首发｜主打新鲜现制，连锁酸奶品牌「王子森林」完成千万元战略融资-36氪
### [成龙哽咽谈动作演员处境：市场只要好看就行](https://ent.sina.com.cn/m/c/2023-03-26/doc-imynefqm6448919.shtml)
> 概要: 新浪娱乐讯 3月24日，成龙哽咽谈动作演员处境：“我也曾想找第二个Jackie Chan，但非常难，市场不需要。多好的功夫没用，现在一定要长得好看，不用你会做戏，站在那里就行”。成龙表示，希望有一天又......
### [第一批用AI画漫画的人，已经赚到钱了](https://www.huxiu.com/article/902029.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童编辑、制图丨渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。生逢AI的寒武纪大爆发......
### [中国公司全球化周报｜中国电商霸榜西班牙购物App前三；元仓海外仓完成数千万元Pre-A轮融资](https://36kr.com/p/2187078053904775)
> 概要: 中国公司全球化周报｜中国电商霸榜西班牙购物App前三；元仓海外仓完成数千万元Pre-A轮融资-36氪
### [《海贼王：红发歌姬》4K蓝光内容公开 6月14日发售](https://www.3dmgame.com/news/202303/3865670.html)
> 概要: 经典漫画《海贼王》新动画电影《海贼王：红发歌姬》票房口碑双丰收，4KUHD蓝光大碟即将于6月14日发售，日前内容物首次曝光，多种限量版组合内容与赠品丰富。·《海贼王：红发歌姬》是该系列的第15部动画电......
### [这世界大概只有中年人错了](https://36kr.com/p/2186601369518209)
> 概要: 这世界大概只有中年人错了-36氪
### [启明创投邝子平：国际交流、监管和全球共识，新一代人工智能将引发深度思考与探讨](https://36kr.com/p/2187691631902849)
> 概要: 启明创投邝子平：国际交流、监管和全球共识，新一代人工智能将引发深度思考与探讨-36氪
### [张颖讲了半句很严重的话](https://www.huxiu.com/article/932190.html)
> 概要: 本文来自微信公众号：投中网 （ID：China-Venture），作者：刘燕秋，题图来自：视觉中国2023年开年以来，张颖已经系统性地发声两三回了。前阵子，他在微博上分享《融资环境变化下可能要更新的一......
### [ぽむ原作漫画《前辈是伪娘》TV动画化决定](https://news.idmzj.com/article/77577.html)
> 概要: ぽむ原作漫画《前辈是伪娘》TV动画化决定，近日于“Animejapan 2023 ANIPLEX STAGE”宣布了这一消息。
### [《七大罪：默示录四骑士》新预告 确定10月开播](https://www.3dmgame.com/news/202303/3865674.html)
> 概要: 经典动漫《七大罪》官方宣布，正统续篇《默示录四骑士》TV动画官方宣布10月开播。同时公布了新预告，一起来先睹为快。《七大罪》是日本漫画家铃木央所著的少年漫画。2011年在讲谈社《周刊少年magazin......
### [BBA等是终端客户，三联锻造毛利率连年下滑](https://www.yicai.com/news/101712223.html)
> 概要: 2023年汽车价格战或对三联锻造继续形成压力。
### [《间谍过家家》剧场版2023年12月22日上映 动画2期10月播出](https://news.idmzj.com/article/77578.html)
> 概要: 《间谍过家家》剧场版动画《剧场版 间谍过家家 CODE：White》将于2023年12月22日上映，TV动画第2期将于2023年10月播出。
### [休闲百搭，真维斯弹力束脚牛仔裤 49.9 元（减 110 元）](https://lapin.ithome.com/html/digi/682361.htm)
> 概要: 【jeanswestz 旗舰店】休闲百搭，真维斯 Z + 男士弹力束脚牛仔裤 报价 159.9 元，限时限量 110 元券，到手价仅需 49.9 元：天猫休闲百搭，真维斯 Z + 男士弹力束脚牛仔裤券......
### [TV动画《无职转生》第二季7月播出 主视觉图与预告公开](https://news.idmzj.com/article/77579.html)
> 概要: 由不讲理不求人创作、白鹰担任插画的轻小说改编的TV动画《无职转生～到了异世界就拿出真本事～》第二季将于2023年7月播出，官方还一同公布了最新PV第2弹以及最新视觉图。
### [Netflix动画《阴阳师》主角设定图 年内独占发布](https://www.3dmgame.com/news/202303/3865677.html)
> 概要: Netflix于3月25日AnimeJapan 2023活动中宣布，梦枕獏原作，经典奇幻小说《阴阳师》将首次制作动画，预定2023年内独占发布，同时公布了制作阵容以及两位主角设定图。·《阴阳师》讲述了......
### [G树林公益基金会 亚洲植树造林公益事业启动](http://www.investorscn.com/2023/03/26/106460/)
> 概要: 世界气象组织指出，2022年发生的极端气候和天气事件再次表明，必须采取更多措施来减少温室气体排放，并对此进行更好的监测；同时，还需通过早期预警全覆盖等手段加强对气候变化的适应......
### [正和岛案例战略重磅发布 助力企业高韧性发展](http://www.investorscn.com/2023/03/26/106461/)
> 概要: 中国商界最具权威性和前瞻性的年度盛会——2023企业家新年大课暨正和岛千企助桂发展行,于2023年3月24-26日在南宁举行。3天时间里,来自全国各地的近千位企业家将围绕“高韧性时代”的主题进行深度研......
### [腰缠万贯：男子携带 239 个 CPU 入境被查](https://www.ithome.com/0/682/370.htm)
> 概要: 感谢IT之家网友肖战割割的线索投递！IT之家3 月 26 日消息，据海关发布公众号消息，3 月 16 日，拱北海关所属闸口海关在拱北口岸查获一宗旅客人身绑藏中央处理器（CPU）239 个进境案。当天 ......
### [小米集团2022财报解读：利空出尽，夯实基本功迎接新周期](https://finance.sina.com.cn/stock/observe/2023-03-26/doc-imynewmy2128025.shtml)
> 概要: 疾风知劲草，烈火见真金。在宏观经济下行，全球通胀加剧，消费需求受到极大冲击的大环境下，众多企业面临风雨飘摇，在生存与发展的重重挑战下艰难前行......
### [EA将于6月关闭《暴力辛迪加》2012重启版服务器](https://www.3dmgame.com/news/202303/3865680.html)
> 概要: EA近日再次宣布将关闭多款老游戏的服务器。其中2012年重启的《暴力辛迪加》服务器将被关闭。这款作品在重启之后一直备受争议，但曾经喜欢过这款游戏的玩家可以抢在服务器关闭之前再次缅怀一下。另外，Xbox......
### [零度解读3月23日美联储利率决议发布会](https://www.yicai.com/news/101712350.html)
> 概要: 这张试卷在曾经的低通胀、零利率和流动性过剩情况下，央行答卷子能场场过关。这一切在最近两个星期宣告彻底结束了。
### [上汽大通称不会参与价格战，向海外市场突围](https://www.yicai.com/news/101712345.html)
> 概要: 价格战不是唯一出路。
### [微软必应聊天每轮对话次数限制提高到 20 次，每天 200 次](https://www.ithome.com/0/682/378.htm)
> 概要: 感谢IT之家网友蓝色大眼猫、丙肝的线索投递！IT之家3 月 26 日消息，微软必应聊天再次提高了聊天次数的限制，这个聊天机器人现在每次会话可以进行 20 次聊天，每天可以进行 200 次聊天。之前，每......
### [年报高峰已至，ChatGPT4再燃市场热情](https://www.yicai.com/vip/news/101712367.html)
> 概要: 年报高峰已至，ChatGPT4再燃市场热情
### [【1分钟快问大咖】德国荣根财团总裁：对外资企业和投资者来说中国充满了机遇](https://finance.sina.com.cn/china/gncj/2023-03-26/doc-imynfatx8862733.shtml)
> 概要: 国际在线消息（记者 谢诗佳 刘朱鹮）：3月25至27日，由国务院发展研究中心主办的中国发展高层论坛2023年年会在北京举行，主要国际经济组织代表、世界500强企业负责人...
### [从山脚堵到山顶！泰山凌晨紧急提示 原因竟是这个](https://finance.sina.com.cn/china/gncj/2023-03-26/doc-imynfatw2091473.shtml)
> 概要: 来源 中国基金报  泰山又火了！ 3月25日一早，“泰山门票”的话题就冲上微博热搜榜第二。有网友表示，“根本预约不上，太火爆了！” 下午，现场视频传出，泰山景区游客爆满...
### [7-Eleven 将在北美布局充电网络，7×24 小时有人看守](https://www.ithome.com/0/682/386.htm)
> 概要: IT之家3 月 26 日消息，零售业品牌 7-Eleven 除了提供各种食品和饮料外，现在还推出了自己的快速充电网络，为电动车主提供更多选择。7-Eleven 近日宣布，将在北美的部分门店安装直流快速......
### [多城市机票预订量已恢复至疫情前水平 国际客运航班计划共涉及境外123个城市](https://finance.sina.com.cn/china/gncj/2023-03-26/doc-imynfatw2106866.shtml)
> 概要: 新航季，新期待，据国内某旅游平台数据显示，2023年3月以来，国内多个城市机票预订量已恢复至疫情前水平，且部分城市已超过2019年同期，预订量前25名的城市中，三亚...
### [远超北京、上海！鄂尔多斯去年人均GDP突破25万元，接近全国平均水平3倍](https://finance.sina.com.cn/jjxw/2023-03-26/doc-imynfatx8886615.shtml)
> 概要: 据“鄂尔多斯统计微讯”3月23日消息，2022年，鄂尔多斯全市地区生产总值5613.44亿元，扣除价格因素同比增长5.4%；年末常住人口220.07万人，同比增加3.23万人。
### [杉杉股份：郑驹与郑永刚遗孀周婷已建立正常沟通渠道 确保经营稳定](https://finance.sina.com.cn/jjxw/2023-03-26/doc-imynfatw2103875.shtml)
> 概要: 每经记者 黄鑫磊  今日（3月26日）傍晚，杉杉股份（SH600884，股价17.5元，市值396.2亿元）相关人士回复《每日经济新闻》记者采访时称，目前...
### [极致炒作后估值“恐高”，TMT板块后市如何看](https://www.yicai.com/news/101712424.html)
> 概要: 22只概念股翻倍，AI概念炒出“遍地股神”感，而主力资金正从芯片等电子板块大幅流出。
# 小说
### [重生2000](https://book.zongheng.com/book/1230867.html)
> 作者：半樽清酒

> 标签：都市娱乐

> 简介：重生一世，杨洛决定不再蹉跎岁月，徒耗青春。前世错过的，遗憾的，可望不可求的，他统统都要。时势造英雄，风云出我辈！

> 章节末：186 终结亦是开端

> 状态：完本
# 论文
### [Metagenome assembly of high-fidelity long reads with hifiasm-meta | Papers With Code](https://paperswithcode.com/paper/metagenome-assembly-of-high-fidelity-long)
> 概要: Current metagenome assemblers developed for short sequence reads or noisy long readswere not optimized for accurate long reads. Here we describe hifiasm-meta, a new metagenome assembler that exploits the high accuracy of recent data.
### [Open-Vocabulary Multi-Label Classification via Multi-modal Knowledge Transfer | Papers With Code](https://paperswithcode.com/paper/open-vocabulary-multi-label-classification)
> 日期：5 Jul 2022

> 标签：None

> 代码：https://github.com/seanhe97/mkt

> 描述：Real-world recognition system often encounters a plenty of unseen labels in practice. To identify such unseen labels, multi-label zero-shot learning (ML-ZSL) focuses on transferring knowledge by a pre-trained textual label embedding (e.g., GloVe). However, such methods only exploit singlemodal knowledge from a language model, while ignoring the rich semantic information inherent in image-text pairs. Instead, recently developed open-vocabulary (OV) based methods succeed in exploiting such information of image-text pairs in object detection, and achieve impressive performance. Inspired by the success of OV-based methods, we propose a novel open-vocabulary framework, named multimodal knowledge transfer (MKT), for multi-label classification. Specifically, our method exploits multi-modal knowledge of image-text pairs based on a vision and language pretraining (VLP) model. To facilitate transferring the imagetext matching ability of VLP model, knowledge distillation is used to guarantee the consistency of image and label embeddings, along with prompt tuning to further update the label embeddings. To further recognize multiple objects, a simple but effective two-stream module is developed to capture both local and global features. Extensive experimental results show that our method significantly outperforms state-of-theart methods on public benchmark datasets. Code will be available at https://github.com/seanhe97/MKT.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
