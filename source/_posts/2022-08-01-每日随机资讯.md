---
title: 2022-08-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.LavaTube_ZH-CN5458469336_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-08-01 20:57:23
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.LavaTube_ZH-CN5458469336_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [5个角度思考如何引导用户添加企业微信](https://www.woshipm.com/operate/5545478.html)
> 概要: 编辑导读：不管我们做了多少活动，搞了多少种裂变方式，最后都会引导用户添加我们的企业微信，目的就是要形成自己的私域流量。但是，很多用户对加微有天然的抵触心理，如何引导用户添加企业微信呢？本文作者对此进行......
### [求求品牌们别再“当爹”了](https://www.woshipm.com/marketing/5548648.html)
> 概要: 编辑导语：最近“张小泉事件”霸屏热搜，不仅品牌滑坡，经理人形象也有所下跌，甚至发不到钱视频网友也不买账，那么此品牌究竟是踩了消费者的哪个雷点呢，我们一起来看看。上周朋友圈被「张小泉」事件刷屏。说实话，......
### [小红书投放怒砸60万，“血本无归”泪目总结](https://www.woshipm.com/operate/5548708.html)
> 概要: 编辑导语：在小红书运营方面如何做出爆款笔记以及如何正确投放呢？本文作者做了详细的总结，通过具体实例给品牌方道出真谛——为消费者打造产品，而不是消费者去选产品。一起来看看作者的分享吧。本文内容是3个品牌......
### [用位运算为你的程序加速](https://segmentfault.com/a/1190000042250543)
> 概要: 前言最近在持续优化之前编写的JSON解析库xjson，主要是两个方面的优化。第一个是支持将一个JSONObject对象输出为JSON字符串。这点在上个版本中只是利用自带的Print函数打印数据：fun......
### [提升web输入体验！JS 如何自动配对标点符号？](https://segmentfault.com/a/1190000042251637)
> 概要: 欢迎关注微信公众号前端侦探温馨提示：在阅读本文之前，可以先回顾这篇文章：Web 中的“选区”和“光标”，有很多你可能不太知道的原生 API，以下内容是对该内容的实践运用在写作编辑中，有很多需要成对出现......
### [数据结构与算法-跳表](https://segmentfault.com/a/1190000042251862)
> 概要: 前言跳表可以达到和红黑树一样的时间复杂度O(logN)，且实现简单，Redis中的有序集合对象的底层数据结构就使用了跳表。本篇文章将对跳表的实现进行学习。正文一. 跳表的基础概念跳表，即跳跃表（Ski......
### [关于Request复用的那点破事儿。](https://segmentfault.com/a/1190000042252276)
> 概要: 你好呀， 我是歪歪。之前不是发布了这篇文章嘛:《千万不要把Request传递到异步线程里面！有坑！》说的是由于 Request 在 tomcat 里面是复用的，所以如果在一个 Request 的生命周......
### [ANTA KIDS X 中国天眼联名UFO4.0 产品视频](https://www.zcool.com.cn/work/ZNjExOTg5MDQ=.html)
> 概要: 受邀为ANTA KIDS与中国天眼联名的新鞋UFO4.0制作的产品故事影片。5月6月两个月的周期，由于疫情在家办公，人手严重不足所以这支影片制作的非常辛苦。Client: Anta Kids代理商 A......
### [美媒称美正收紧对华芯片设备出口 专家：目的是阻止中国芯片与其竞争](https://finance.sina.com.cn/tech/it/2022-08-01/doc-imizmscv4319752.shtml)
> 概要: 环球时报报道 记者  倪浩美国彭博社7月30日报道称，美国两家芯片设备公司证实，该国对中国芯片的打压已从10纳米扩展到了14纳米，且新规影响范围可能不限于中芯国际，还包括其他在中国投资运营的芯片制造企......
### [“盲盒式”奥特曼卡牌火了：有公司卖卡收割小学生“韭菜”年入1.7亿](https://www.tuicool.com/articles/QVvEB3U)
> 概要: “盲盒式”奥特曼卡牌火了：有公司卖卡收割小学生“韭菜”年入1.7亿
### [环球度假区漫游卡“杀猪盘”：滋生于团购，诈骗金额数千万](https://finance.sina.com.cn/tech/roll/2022-08-01/doc-imizirav6221382.shtml)
> 概要: 文/邓双琳......
### [元气森林“反攻”可乐，唐彬森闯进最危险的战场？](https://finance.sina.com.cn/tech/internet/2022-08-01/doc-imizmscv4328788.shtml)
> 概要: 文/乔雪......
### [《黑道圣徒：重启版》PC版4K+光追新演示视频](https://www.3dmgame.com/news/202208/3848252.html)
> 概要: 近日Gaming Surface分享了《黑道圣徒：重启版》PC版30分钟演示，展示了4K+光线追踪的画面效果。PC版将有光线追踪的环境光遮蔽技术，至于是否有光追阴影、全局照明或反射，目前未知。全新演示......
### [中国商飞重磅官宣：国产大飞机C919完成取证试飞](https://finance.sina.com.cn/tech/it/2022-08-01/doc-imizmscv4329570.shtml)
> 概要: 编辑/毕陆名......
### [微软：动视暴雪的游戏并“没有什么独特之处”](https://www.3dmgame.com/news/202208/3848257.html)
> 概要: 近日，微软向新西兰监管机构表示，动视暴雪的游戏“没有什么独特之处”，这是微软为了让收购交易获得批准而进行的最新尝试。在提交给商业收购和授权商务委员会的一份文件中，微软表示，该游戏巨头不生产任何“非有不......
### [美国芯片法案，在下什么大棋？](https://www.huxiu.com/article/623446.html)
> 概要: 本文来自微信公众号：芯谋研究（ID：icwise），作者：芯谋评论，原文标题：《比马歇尔计划更庞大的芯片法案在下什么大棋？》，题图来自：视觉中国“因为你的投票，你的孙辈都会做着高薪工作。” 这是美国民......
### [「周刊少年JUMP」2022年第35号封面公开](http://acg.178.com/202208/453319912697.html)
> 概要: 漫画杂志「周刊少年JUMP」公开了2022年第35号的封面图，绘制的是「海贼王」。「海贼王」的最新剧场版动画「ONE PIECE FILM RED」将于8月6日正式上映。本期杂志于今日（8月1日）发售......
### [月薪8万，到底刺痛了我们哪根神经？](https://www.huxiu.com/article/623069.html)
> 概要: 本文来自微信公众号：凤凰WEEKLY财经 （ID：fhzkzk），作者：何劦，编辑：张轶骁，头图来自：《华尔街之狼》剧照“官二代”周劼炫爹之后，下一个刺痛公众的，是金融界月薪8万的交易员。7月28日晚......
### [盲盒式奥特曼卡牌风靡小学生群体 成年人也发烧](https://acg.gamersky.com/news/202208/1504728.shtml)
> 概要: 近年来，奥特曼卡牌风靡小学生群体。孩子们不断地买卡、抽卡、凑卡，为其疯狂氪金。但在卡牌这条大赛道上，收割的大头，其实是成年发烧友。
### [动画「街角魔族 2丁目」公开全新杂志版权绘](http://acg.178.com/202208/453321330289.html)
> 概要: 电视动画「街角魔族 2丁目」公开了在杂志「Megami Magazine」9月号别册附录上的全新版权绘。电视动画「街角魔族」改编自伊藤いづも创作的同名漫画作品，由J.C.STAFF负责动画制作，第一季......
### [锐锢商城半年内累计完成4亿美元融资](http://www.investorscn.com/2022/08/01/102128/)
> 概要: 近日,锐锢商城宣布半年内已经累计完成4亿美元的股权融资,其中2021年10月完成2.5亿美元融资,由春华资本领投,泰康人寿、普洛斯GLP、建发新兴投资、源码资本、钟鼎资本、鼎晖VGC、成为资本跟投;2......
### [【混剪】起于青萍之末，也要成为最骄傲的草莽！——德拉蒙德.格林](https://bbs.hupu.com/54977448.html)
> 概要: 【混剪】起于青萍之末，也要成为最骄傲的草莽！——德拉蒙德.格林
### [P站美图推荐——冰棒特辑（二）](https://news.dmzj.com/article/75108.html)
> 概要: 好热，希望二次元没有雪糕刺客。
### [《大话降龙》太白和二郎神不掐架，就是完美的一天！](https://new.qq.com/omn/20220801/20220801A0320R00.html)
> 概要: 这是天宫的完美一天，二郎神和太白不掐架？伏虎和木吒没用约会？玉帝不打扰嫦娥仙子？不不不，不可能的，精彩和完美并不冲突......
### [China SIF夏季峰会圆桌对话：转型金融要有可信的计划、战略和指标](http://www.investorscn.com/2022/08/01/102129/)
> 概要: 7月12日，由商道融绿主办，亚洲投资者气候变化联盟（AIGCC）联合主办的2022年中国责任投资论坛夏季峰会在北京线上成功举行。本届峰会获得战略合作伙伴穆迪和浦银安盛基金的大力支持。峰会以“构建转型金......
### [ISC 2022 | 360叶健：以“看见”为核心打造数字安全体系](https://www.tuicool.com/articles/zmYzEz6)
> 概要: ISC 2022 | 360叶健：以“看见”为核心打造数字安全体系
### [周渝民夫妻被保险经纪人诈骗 受骗金额超七百万](https://ent.sina.com.cn/s/h/2022-08-01/doc-imizirav6253919.shtml)
> 概要: 新浪娱乐讯 据媒体报道，8月1日，周渝民和太太喻虹渊被曝遭诈骗，起因是他们买了1张6年期的高额储蓄险，承办的冯姓女保险经纪人却取得喻虹渊的信任，陆续以巨额转帐会被查税、可能被误认洗钱、预缴有优惠等理由......
### [微软与历史建筑保护组织合作《我的世界》重现古城](https://www.3dmgame.com/news/202208/3848277.html)
> 概要: 微软日前宣布，将与著名历史建筑保护组织National Trust合作，发挥旗下游戏《我的世界》的特长，在《我的世界》中陆续重现世界古城，用于寓教于乐。•《我的世界》中已经有国内外众多玩家群体自发组织......
### [什么样的亏损才有意义？](https://www.huxiu.com/article/623485.html)
> 概要: 本文来自微信公众号：商业评论 （ID：shangyepinglun），作者：拉蒙·卡扎德絮-马萨内尔（Ramon Casadesus-Masanell）、多劳·霍瓦特（Dóra Horváth）、方睿......
### [历时32年 今年八月蜡笔小新家终于能还清房贷了](https://acg.gamersky.com/news/202208/1504802.shtml)
> 概要: 历时32年，今年八月蜡笔小新家终于还完房贷。
### [「LoveLive!SuperStar!!!」第二季第三话插入曲无字幕MV公开](http://acg.178.com/202208/453326586052.html)
> 概要: 近日，由日升动画、Lantis和「电击G's magazine」联合企划，日升动画制作的电视动画「LoveLive!SuperStar!!」公开了第二季第三话插入曲「GO!! リスタート」的无字幕MV......
### [《下里巴熊申请色色》_下里巴熊第三弹](https://www.zcool.com.cn/work/ZNjEyMDQ4NTY=.html)
> 概要: 正在参与：别管我，表情包是我唯一的交流方式......
### [《小美人鱼》配音演员帕特·卡洛尔去世 享年95岁](https://ent.sina.com.cn/m/f/2022-08-01/doc-imizmscv4360910.shtml)
> 概要: 新浪娱乐讯 北京时间8月1日消息，据外国媒体报道，资深美国演员、喜剧人、配音演员帕特·卡洛尔去世，享年95岁，她去世前罹患肺炎，在马萨诸塞州的家中死去。卡洛尔最知名的角色是为迪士尼经典动画《小美人鱼》......
### [油管主播Hikaru举办“现实开司”企划！第1名奖金1千万日元](https://news.dmzj.com/article/75110.html)
> 概要: 在日本的人气油管主播Hikaru，宣布了将要举办“现实开司”的企划。在这次的企划中，将征集缺钱的人，让他们通过游戏进行比赛。最终获胜的人，会获得1000万日元（约合人民币50万元）的奖励。
### [周渝民夫妻被熟人骗771万！对方以卖保险设计仨骗局，警方已介入](https://new.qq.com/rain/a/20220801A03UCM00)
> 概要: 8月1日，娱乐圈又传大瓜！据知名媒体爆料，周渝民夫妇买保险被骗3447多万台币，折合人民币771万元，着实是一笔不小的费用。而诈骗犯不是别人，正是周渝民妻子的好闺蜜。            据悉，周渝......
### [「灵能百分百」特别见面会主视觉图公开](http://acg.178.com/202208/453331526418.html)
> 概要: 近日，「灵能百分百」公开了特别见面会的主视觉图。该作动画第3季预计将于今年10月播出。「灵能百分百」是ONE创作的漫画作品，讲述了外表不起眼仿佛路人的主人公·影山茂夫，其实是拥有强大力量的超能力者的故......
### [迟到着联盟](https://www.zcool.com.cn/work/ZNjEyMDY3NTI=.html)
> 概要: 新一届阿斯特拉国立艺术学院大一新生开学日上，来自不同文化不同地区的青少年乘坐着自己的飞行器来到这所充满梦想的艺术院校。这个时代，飞行器就和21世纪初的自行车，电动车一样普遍，人们驾驶着形态各异的飞行器......
### [00后送礼清单，大家别想歪了](https://www.huxiu.com/article/619504.html)
> 概要: 出品｜虎嗅内容运营团队头图｜unsplash七夕总会跟浪漫划等号，但浪漫从来都不是“爱情”的专属。七夕属于爱情，更属于每一个勇敢表达爱、接受爱的人。我们看过太多的“七夕送礼清单”，却忽略了收礼人的感受......
### [奇幻漫画《黑色五叶草》步入最终章 纪念长PV公开](https://www.3dmgame.com/news/202208/3848283.html)
> 概要: 8月1日今天最新一期《周刊少年JUMP》发行，步上周《海贼王》的后尘，另一部经典奇幻漫画《黑色五叶草》也步入了最终章，开启新连载，官方公布了5分钟的长PV，回顾了系列漫长的冒险历程，一起来感动下。•《......
### [2022消博会“星”推荐：国控星鲨L-阿拉伯糖粉实力出圈](http://www.investorscn.com/2022/08/01/102131/)
> 概要: 第二届中国国际消费品博览会于2022年7月26日在海南省海口市国际会展中心拉开了帷幕。本届消博会展览总面积达10万平方米，以“共享开放机遇、共创美好生活”为主题，吸引了61个国家和地区的2800多个品......
### [躺着上班就是爽：网易严选人体工学椅 648.9 元探底](https://lapin.ithome.com/html/digi/632662.htm)
> 概要: 【网易严选旗舰店】网易严选“小蛮腰”人体工学椅日常售价 899 元，正逢七夕大促，下单立减 100 元 + 可领 100 元加码券，实付 699 元包邮：京东网易严选 人体工学转椅小蛮腰 双背券后 6......
### [如何正确地配置入口文件？](https://www.tuicool.com/articles/fIF36z7)
> 概要: 如何正确地配置入口文件？
### [知名声优早见沙织确认感染新冠 遵医嘱休养](https://acg.gamersky.com/news/202208/1504933.shtml)
> 概要: 日本知名声优早见沙织在今天（8月1日）宣布确认感染新冠，目前正根据医嘱进行疗养，还请大家不要担心。
### [2022年秋番动画一览（截止到7月29日）](https://news.dmzj.com/article/75112.html)
> 概要: 距离秋番动画开播还有2个月左右的时间，不少作品都已经开始宣传。以下就是截止到6月16日公开的各作品名单（按五十音图顺序排序）。
### [赛力斯汽车 7 月销量达 7807 辆，同比增长 8873.56%](https://www.ithome.com/0/632/689.htm)
> 概要: 感谢IT之家网友很笨的话的线索投递！IT之家8 月 1 日消息，小康股份 7 月的产销数据现已发布。从数据来看，赛力斯集团 7 月生产了 13163 辆新能源汽车，同比增长 247.31%，今年累积生......
### [鸿星尔克帮扶特殊群体，捐赠1个亿！](http://www.investorscn.com/2022/08/01/102135/)
> 概要: 公益在路上，鸿星尔克再捐一个亿......
### [男子躺床玩手机失手砸眼 致视网膜脱离险些失明](https://www.3dmgame.com/news/202208/3848292.html)
> 概要: 如今手机早已经成为了网友们必不可缺的物品之一，相信有不少小伙伴睡前都是由手机陪伴的，不过睡前玩手机除了有长期损害视力的隐患外，还有不可预测的物理直接伤害。本图仅为示意图近日据媒体报道，浙江杭州30岁的......
### [为了让年轻人跑起来，Keep到底能有多么努力](https://www.tuicool.com/articles/r6JBnay)
> 概要: 为了让年轻人跑起来，Keep到底能有多么努力
### [保租房公募REITs加速落地，将解决哪些痛点？](https://www.yicai.com/news/101492003.html)
> 概要: 租赁住房建设普遍面临资金沉淀多、租金收益率不足等痛点。
### [联德股份：商用压缩机零部件行业的龙头 2022年上半年保持高增长](http://www.investorscn.com/2022/08/01/102136/)
> 概要: 8月1日晚间，国内领先的精密零部件专家——杭州联德精密机械股份有限公司（以下简称“联德股份”，股票代码：605060）发布了2022年半年度业绩报告，向市场交出了公司2022年上半年成绩单。根据公告，......
### [声优早见沙织新冠核酸检测呈阳性](https://news.dmzj.com/article/75115.html)
> 概要: 声优早见沙织的事务所I'm Enterprise发布了早见沙织在进行新冠病毒核酸检测时，结果呈阳性的消息。目前早见沙织正在医疗机构的指导下进行疗养。
### [2022年6~7月插画/速涂创作总结](https://www.zcool.com.cn/work/ZNjEyMTI4ODA=.html)
> 概要: 设备：PC+Wacom Pth660软件：PS20196~7月的总结,大部分是速涂创作，对参考照片感兴趣可以看看视频，有发对比总结~......
### [走访28省市160家环保企业后，协会会长道出了行业痛点](https://www.yicai.com/news/101492024.html)
> 概要: 受经济下行影响，环保工程、运营项目回款慢、回款难等现象严重，造成企业经营成本增加。
### [日媒评选最美女声优 声音甜美颜值高谁能不爱](https://acg.gamersky.com/news/202208/1504925.shtml)
> 概要: 近期日本网站ranking.goo投票选出“最美的女声优”，她们不只声音好听，甜美的长相也获得很多人的喜爱，来看看都是哪些熟面孔吧。
### [塑料复合铜箔技术迭代，机构建议关注产业链上这些环节](https://www.yicai.com/vip/news/101492079.html)
> 概要: 塑料复合铜箔技术迭代，机构建议关注产业链上这些环节
### [冷藏冰凉真好吃：林家铺子黄桃椰果罐头 19.9 元 8 罐（京东 49.8 元）](https://lapin.ithome.com/html/digi/632712.htm)
> 概要: 天猫【林家铺子食品旗舰店】林家铺子左右双色罐头 * 3 + 椰果罐头 * 1 日常售价为 19.9 元，下单 2 件打 6.76 折，领取 7 元优惠券，到手价为 19.91 元 8 罐，折合每罐 2......
### [组图：辛芷蕾戴鸭舌帽机场出发 穿无袖印花套装潮酷时髦](http://slide.ent.sina.com.cn/star/slide_4_86448_373338.html)
> 概要: 组图：辛芷蕾戴鸭舌帽机场出发 穿无袖印花套装潮酷时髦
### [惠普战 66 笔记本推出 32GB 大内存版本，售价 4899 元起](https://www.ithome.com/0/632/724.htm)
> 概要: IT之家8 月 1 日消息，今天，惠普官方宣布战 66 五代升级 32GB 大内存，搭配 12 代英特尔酷睿处理器，售价 4899 元起。14 英寸i5-1240P + 32GB 内存 + 512GB......
### [航行警告！南海海域军事训练](https://finance.sina.com.cn/jjxw/2022-08-01/doc-imizmscv4422176.shtml)
> 概要: 来源：央视军事 【航行警告！#南海海域军事训练#】据中国海事局网站消息，清澜海事局发布航行警告，8月2日0时至8月6日24时，南海部分海域进行军事训练，禁止驶入。
### [FPX vs UP精彩回顾：双枪滑步银弹袭敌魂，FPX下路起势先驰得点](https://bbs.hupu.com/54983080.html)
> 概要: 来源：  虎扑
### [技术流中卫！孔德亮相仪式大秀花式颠球，这球感什么水平？](https://bbs.hupu.com/54983123.html)
> 概要: 来源：  虎扑
### [中国能建拟分拆易普力，借壳南岭民爆上市推动民爆行业整合](https://www.yicai.com/news/101492241.html)
> 概要: A股首单“分拆+借壳”交易方案出炉。
### [可穿戴超声成像技术获重大突破 有望创造全新应用场景](https://www.yicai.com/news/101492243.html)
> 概要: 微型化、远程化、智能化和高清化是超声未来的发展趋势。新的生物凝胶超声贴片借助了材料科技的发展，提出了一种新的超声应用的可能性。
### [大连海事局发布航行警告：渤海北部进行实弹射击](https://finance.sina.com.cn/jjxw/2022-08-01/doc-imizirav6324897.shtml)
> 概要: 据中国海事局网站8月1日消息，大连海事局发布航行警告，自8月1日14时至8月4日24时，渤海北部将在部分海域进行实弹射击。禁止驶入。
### [侠客岛：化解村镇银行风险，政治局会议表态了](https://finance.sina.com.cn/wm/2022-08-01/doc-imizmscv4425550.shtml)
> 概要: 【经济Ke】化解村镇银行风险，政治局会议表态了 侠客岛公众号 7月28日，备受关注的村镇银行金融风险问题上了中央政治局会议议程。
### [流言板罗马诺：切尔西推动截胡库库雷利亚，科尔维尔或进入交易](https://bbs.hupu.com/54983270.html)
> 概要: 虎扑08月01日讯 根据意大利天空体育著名记者Fabrizio Romano的跟进报道，他转发了邮报记者Craig Hope的报道，表示切尔西正在推动截胡库库雷利亚的交易！今天与布莱顿的直接对话—而中
### [第19届中国-东盟博览会主题确定](https://finance.sina.com.cn/jjxw/2022-08-01/doc-imizirav6326121.shtml)
> 概要: 总台记者8月1日从中国-东盟博览会秘书处获悉，第19届中国-东盟博览会主题确定为“共享RCEP新机遇，助推中国-东盟自由贸易区3.0版”。
### [防挪用！江西九江公示在售楼盘预售资金监管账户…多地多孩家庭购房有优惠](https://finance.sina.com.cn/wm/2022-08-01/doc-imizmscv4426286.shtml)
> 概要: 近日，为防止资金被挪用，江西九江住建局公示了中心城区85个现有在售楼盘的139个商品房预售资金监管账户。购房人在购房时将购房款直接存入商品房预售资金监管账户。
# 小说
### [开局签到：回到十年前当外科医生](https://m.qidian.com/book/1027581126/catalog)
> 作者：八刃贤狼

> 标签：都市生活

> 简介：我，来自十年后。我，是一个外科医生。我，是超级医师系统的宿主。我，每天都可以签到。我是吴远，将成为医界的传奇。

> 章节总数：共389章

> 状态：完本
# 论文
### [Fair Comparison between Efficient Attentions | Papers With Code](https://paperswithcode.com/paper/fair-comparison-between-efficient-attentions)
> 概要: Transformers have been successfully used in various fields and are becoming the standard tools in computer vision. However, self-attention, a core component of transformers, has a quadratic complexity problem, which limits the use of transformers in various vision tasks that require dense prediction. Many studies aiming at solving this problem have been reported proposed. However, no comparative study of these methods using the same scale has been reported due to different model configurations, training schemes, and new methods. In our paper, we validate these efficient attention models on the ImageNet1K classification task by changing only the attention operation and examining which efficient attention is better.
### [Silver-Bullet-3D at ManiSkill 2021: Learning-from-Demonstrations and Heuristic Rule-based Methods for Object Manipulation | Papers With Code](https://paperswithcode.com/paper/silver-bullet-3d-at-maniskill-2021-learning)
> 概要: This paper presents an overview and comparative analysis of our systems designed for the following two tracks in SAPIEN ManiSkill Challenge 2021: No Interaction Track: The No Interaction track targets for learning policies from pre-collected demonstration trajectories. We investigate both imitation learning-based approach, i.e., imitating the observed behavior using classical supervised learning techniques, and offline reinforcement learning-based approaches, for this track. Moreover, the geometry and texture structures of objects and robotic arms are exploited via Transformer-based networks to facilitate imitation learning. No Restriction Track: In this track, we design a Heuristic Rule-based Method (HRM) to trigger high-quality object manipulation by decomposing the task into a series of sub-tasks. For each sub-task, the simple rule-based controlling strategies are adopted to predict actions that can be applied to robotic arms. To ease the implementations of our systems, all the source codes and pre-trained models are available at \url{https://github.com/caiqi/Silver-Bullet-3D/}.
