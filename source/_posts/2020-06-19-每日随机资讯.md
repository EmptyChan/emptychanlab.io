---
title: 2020-06-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MidsummerEve_EN-CN8806275232_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-06-19 22:09:50
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MidsummerEve_EN-CN8806275232_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [UNINE疑似被GoldenChild抄袭 专辑封面相似度高](https://ent.sina.com.cn/y/yneidi/2020-06-19/doc-iircuyvi9307392.shtml)
> 概要: 新浪娱乐讯 6月19日，有网友指出UNINE专辑封面图疑似被韩团Golden Child抄袭，Golden Child2020年6月的新专视觉图和UNINE2019年10月专辑《UNUSUAL》封面图......
### [视频：秒变成JPG！邓伦看李佳琦涂口红惊呆](https://video.sina.com.cn/p/ent/2020-06-19/detail-iirczymk7828690.d.html)
> 概要: 视频：秒变成JPG！邓伦看李佳琦涂口红惊呆
### [日本媒体关注中国电影 称影院面临艰难困境](https://ent.sina.com.cn/m/f/2020-06-19/doc-iircuyvi9416243.shtml)
> 概要: 新浪娱乐讯 日本《映画》网站近日刊文，称在全世界逐渐重新开放影院的现在，中国电影业依然面临着困境。　　文章称，在中国，电影院从1月开始停止开放，已有多家电影院以及关联公司宣布倒闭。万达电影也宣布可能会......
### [视频：真·端水艺术家！杜海涛为吴昕沈梦辰做横幅应援](https://video.sina.com.cn/p/ent/2020-06-19/detail-iirczymk7896174.d.html)
> 概要: 视频：真·端水艺术家！杜海涛为吴昕沈梦辰做横幅应援
### [《隐秘的角落》配得上9分吗？这些细节太加分](https://ent.sina.com.cn/v/m/2020-06-19/doc-iirczymk7896021.shtml)
> 概要: 秦昊、王景春等人主演的悬疑剧在6月18日晚迎来了超前付费点播大结局（普通会员目前只能看6集），当前豆瓣评分稳居9分之上，说明该剧口碑并未崩塌。有人说，《隐秘的角落》抬高了国产悬疑剧的天花板，这个褒奖不......
### [小米粒很会作，昔日坐扶梯抱秦昊大腿，走累了还让伊能静老公背](https://new.qq.com/omn/20200619/20200619A0JERU00.html)
> 概要: 小米粒很会作，昔日坐扶梯抱秦昊大腿，走累了还让伊能静老公背
### [36岁哈里事业高开低走！离开王室后泯然众人，梅根生活依旧奢侈](https://new.qq.com/omn/20200619/20200619A0JSVT00.html)
> 概要: 36岁哈里事业高开低走！离开王室后泯然众人，梅根生活依旧奢侈
### [刘诗诗携新剧《亲爱的自己》露面！死亡角度一拍，肉肉就藏不住了](https://new.qq.com/omn/20200619/20200619A0KAQ200.html)
> 概要: 刘诗诗携新剧《亲爱的自己》露面！死亡角度一拍，肉肉就藏不住了
### [还没播出就先拿奖！《有翡》成娱乐圈第一人？爆款已预订](https://new.qq.com/omn/20200619/20200619A0H4T100.html)
> 概要: 还没播出就先拿奖！《有翡》成娱乐圈第一人？爆款已预订
### [肖战采访被“带节奏”，官方词条断章取义引不满，网友：小心被封](https://new.qq.com/omn/20200619/20200619A0GWGK00.html)
> 概要: 肖战采访被“带节奏”，官方词条断章取义引不满，网友：小心被封
# 动漫
### [异世界召唤BL动画《巨人的新娘》公开PV](https://news.dmzj.com/article/67723.html)
> 概要: 根据ITKZ原作制作的TV动画《巨人的新娘》公开了一段PV。在这段PV中，可以看到主人公晃一（CV：伊东健人）被召唤到异世界和巨人族的王子（CV：小野友树）相遇的片段。
### [TV动画《池袋西口公园》宣布延期播出](https://news.dmzj.com/article/67720.html)
> 概要: TV动画《池袋西口公园》宣布了延期播出的消息，从原定的7月开播延期到了10月。作品的主宣传图也一并公开。这次的延期是受到了新冠病毒所引发的疫情的影响。
### [《黄金神威》成怪谈！稻川淳二讲怪谈风格的“怪谈神威”](https://news.dmzj.com/article/67718.html)
> 概要: 在本日（19日）发售最新的22卷的漫画《黄金神威》，公开了一个特别网页“稻川淳二的怪谈神威”。
### [bell-fine《魔术学姐》学姐1/7比例手办](https://news.dmzj.com/article/67724.html)
> 概要: bell-fine根据《魔术学姐》中的学姐制作的1/7比例手办目前已经开订了。本作采用了学姐戴着大礼帽，披着斗篷，拿着拐杖时的样子。台座包括了鸽子、纸片、彩带等魔术相关的要素。
### [爆笑漫画：阿衰穿越到各个时空，不是武大郎就是陈世美，最后终于变成大雄](https://new.qq.com/omn/20200619/20200619A0BEXT00.html)
> 概要: ...
### [海贼王983话分析：为什么凯多的儿子大和，跟光月御田装扮一样？](https://new.qq.com/omn/20200619/20200619A0EDWK00.html)
> 概要: 海贼王983话已经，劲爆信息很多。其中凯多的儿子大和更是话题不断。而且从漫画中看，凯多的儿子大和似乎和御田之间扯上了关系，具体是怎么一回事，一起来看看吧。路飞这边和乌尔缇来个个硬碰硬的对决，恐龙果实……
### [真人版《海贼王》8月开拍，好莱坞大制作，选角引发影迷期待](https://new.qq.com/omn/20200619/20200619A0BGB700.html)
> 概要: 6月19日上午，据有关媒体报道，知名日漫《海贼王》的好莱坞真人版剧集将于8月31日在南非开普敦开拍，拍摄持续至明年2月8日，引发了众多影迷们的期待。据悉，上个月制作人Marty Adelstein透……
### [墨香没有填的坑：蓝湛的兔子叫什么名字，花城的通灵口令成谜](https://new.qq.com/omn/20200619/20200619A0HC2700.html)
> 概要: 墨香铜臭是一个很神奇的作者，到目前为止只写过3篇小说，每篇的风格与故事都不一样，却都给大家留下了深刻的印象，同时也收获了很多道友。不过，细心的道友们可能会发现，墨香在小说中也留下了一些没有填完的坑，……
### [本以为中国和菲律宾的山寨动漫公园够奇葩了，没想到日本的也瞎眼](https://new.qq.com/omn/20200619/20200619A0E4YL00.html)
> 概要: 不知道诸位朋友有没有听过关于中国以及菲律宾的一些山寨动漫公园的传闻？之前在北京曾经有人搞过一个《哆啦A梦》的山寨公园，对外宣传是什么机器猫主题科普月的活动，看起来还挺有模有样的，不过游客们实际到访之……
### [一拳超人：英雄等级排名，埼玉第一毋庸置疑，僵尸男排名受争议](https://new.qq.com/omn/20200619/20200619A0CKJV00.html)
> 概要: 《一拳超人》相信大家都看过，这是一部很精彩的动漫，现在还在连载中。里面很多英雄给我们留下了深刻的印象，而在英雄协会中也有英雄排名，不过这个排名在大家的心中是得到了推翻，下面让我来说一下我心中的排名吧……
### [《大春物》第三季动画结衣篇预告 团子的预感](https://acg.gamersky.com/news/202006/1298139.shtml)
> 概要: 《我的青春恋爱物语果然有问题》第三季动画公开了由比滨结衣篇预告，由结衣的内心展示了这场“大战”的可能性的结果。
### [《美少女战士》剧场版宣布延期 明年年初日本公映](https://acg.gamersky.com/news/202006/1298213.shtml)
> 概要: 《美少女战士 Eternal》宣布受新冠疫情影响而推迟档期，前篇从9月11日延期到2021年1月8日在日本公映，后篇延期到2月11日。
### [《海贼王》983话情报：双方大战 雷鸣八卦！](https://acg.gamersky.com/news/202006/1297163.shtml)
> 概要: 《海贼王》983话情报公开，之前一直很神秘的凯多的儿子大和终于登场，他直接来到了路飞的面前，表示自己一直在等待路飞。
### [吉卜力3DCG动画《阿雅与魔女》曝剧照 宫崎吾朗执导](https://acg.gamersky.com/news/202006/1298176.shtml)
> 概要: 吉卜力全新3DCG动画《阿雅与魔女》公开了视觉图和部分剧照，本片改编自《哈尔的移动城堡》原作者黛安娜·温尼·琼斯的同名小说，由宫崎骏企划，其子宫崎吾朗导演。
### [《圣斗士星矢》平角裤 每条247元、穿上战力爆表](https://acg.gamersky.com/news/202006/1298222.shtml)
> 概要: 《圣斗士星矢：黄金魂》推出主题平角裤，共有六款星座可选：白羊座、双子座、狮子座、射手座、处女座、天蝎座。
# 财经
### [庞星火：新发地购买的冰冻海鲜豆制品不建议食用](https://finance.sina.com.cn/china/gncj/2020-06-19/doc-iirczymk7952274.shtml)
> 概要: 原标题：庞星火：新发地购买的冰冻海鲜豆制品不建议食用 6月19日，北京市疾病预防控制中心庞星火称，新发地市场综合交易大厅，特别是水产...
### [城市豪赌：中国经济增长的隐蔽动力和潜在风险](https://finance.sina.com.cn/stock/stockzmt/2020-06-19/doc-iircuyvi9425399.shtml)
> 概要: 城市豪赌：中国经济增长的隐蔽动力和潜在风险 作者：俞铁成 家乡的省会合肥无疑是中国发展最快的省会城市。 1995年合肥GDP为167亿元...
### [新华微评：严格防疫做到位 有序流动行得通](https://finance.sina.com.cn/china/gncj/2020-06-19/doc-iirczymk7952750.shtml)
> 概要: 原标题：#新华微评#：严格防疫做到位，有序流动行得通 【#新华微评#：严格防疫做到位，有序流动行得通】在19日的国务院联防联控机制新闻发布会上，有关部门负责人指出...
### [李兰娟：新冠病毒特别“不怕冷” 在零下20度可存活20年](https://finance.sina.com.cn/china/gncj/2020-06-19/doc-iircuyvi9426953.shtml)
> 概要: 原标题：李兰娟：新冠病毒特别“不怕冷”，在零下20度可存活20年 6月19日，在杭州海关举办的“李兰娟院士抗疫事迹分享会”上，中国工程院院士...
### [今日财经TOP10|重磅：上证指数改了 科创50指数也来了](https://finance.sina.com.cn/china/gncj/2020-06-19/doc-iircuyvi9421491.shtml)
> 概要: 【宏观要闻】 NO.1 重磅：上证指数改了科创50指数也来了 上证指数改了，剔除ST股、延长新股纳入时限、科创板公司入列，科创50指数也来了，7月22日见！
### [房东减租政府减税 上海上半年“房土两税”减免共5.6亿元](https://finance.sina.com.cn/china/gncj/2020-06-19/doc-iircuyvi9422518.shtml)
> 概要: 原标题：房东减租政府减税，上海上半年“房土两税”减免共5.6亿元 6月19日，澎湃新闻（www.thepaper.cn）记者从国家税务总局上海市税务局获悉，上半年...
# 科技
### [新浪微博爬虫，用python爬取新浪微博数据](https://www.ctolib.com/dataabc-weiboSpider.html)
> 概要: 新浪微博爬虫，用python爬取新浪微博数据
### [基于 ThinkPHP6 和 Layui 的后台管理框架](https://www.ctolib.com/lizhenjian-leeAdmin.html)
> 概要: 基于 ThinkPHP6 和 Layui 的后台管理框架
### [Rasa天气查询机器人](https://www.ctolib.com/vba34520-Rasa-Weather.html)
> 概要: Rasa天气查询机器人
### [根据关键词监控github上的更新信息，并通过Server酱推送到微信上](https://www.ctolib.com/gtfly-GitHub-Monitor.html)
> 概要: 根据关键词监控github上的更新信息，并通过Server酱推送到微信上
### [日本大型移动支付软件 PayPay 的 TiDB 迁移实践](https://www.tuicool.com/articles/3mIRbue)
> 概要: 作者介绍Munenori Hirakawa，PayPay Senior Manager at Product Tech Division。PayPay 成立于 2018 年 10 月，由软银集团、日本......
### [对话跟谁学 CEO 陈向东：如果你对中国在线教育的长期有信心，对当下就越有耐心](https://www.tuicool.com/articles/eaUjYbv)
> 概要: 当别人大规模去融资的时候，咱们不融资；当别人都说可以适当竞争，去抄袭对手时，我们关注我们的客户；当竞争对手招人很多，组织规模变大时，我们慢点招，把每个人招对。作者｜钟文2020 年上半年，全国经历了一......
### [“老板学术造假被查了，想退学”](https://www.tuicool.com/articles/Erqa2ii)
> 概要: 导师学术不端对研究生、博士后的学术生涯构成了重大的打击：一些学生会感到对科学的信任从此破灭，想要离开学术界，想要留在学术界的人则面临背着“不良记录”寻找新职位的窘境。在这篇文章里，一些“过来人”分享了......
### [下沉求职市场，「ShoulderU」专注为学生提供长线职前服务](https://www.tuicool.com/articles/Fneeiyr)
> 概要: 2020年我国高校毕业生数量创历史新高，达到874万，高于去年同期40万。由于疫情带来的经济下行，企业招聘需求却大幅下降。数据显示2020年高校应届毕业生新增岗位同比降幅达49%，100人以下的小微企......
### [量产国内最先进 14nm 工艺：中芯国际放弃美股正式回归国内上市](https://www.ithome.com/0/493/768.htm)
> 概要: 6月19日，上海证交所发表公告，中芯国际集成电路制造有限公司科创板首发获通过，意味着这家量产了国产最先进14nm工艺的晶圆代工厂正式回归A股上市。资料显示，中芯国际是目前国内最大的晶圆代工厂，之前是美......
### [华为自动化拆卸平台亮相！6 分钟完成手机拆解](https://www.ithome.com/0/493/723.htm)
> 概要: IT之家6月19日消息 当手机遇到故障需要拆机维修时。很多用户会对拆机过程有所担心。今日华为终端手机产品线总裁何刚晒出了华为维修间里的黑科技——自动化拆卸平台。据何刚介绍，华为自动化拆卸平台拥有安全加......
### [4.5 折 + 送百度文库月卡：爱奇艺会员年卡 89.9 元大促](https://www.ithome.com/0/493/789.htm)
> 概要: 爱奇艺黄金会员年卡原价198元，限时低至5折99.9元，今日领券再减10元+送百度文库会员月卡：天猫爱奇艺 黄金会员年卡送 百度文库会员 1个月券后89元领10元券活动6月20日结束，有需求的小伙伴可......
### [字节跳动设立澳大利亚办公室，前谷歌高管任 TikTok 总经理](https://www.ithome.com/0/493/771.htm)
> 概要: 6月19日消息，据外媒报道，日前字节跳动设立了首个澳大利亚办公室，由前谷歌高管李·亨特（Lee Hunter）任TikTok澳大利亚总经理。与亨特一同加入TikTok的还有前谷歌高管布雷特·阿姆斯特朗......
# 小说
### [剑飞侠舞闯九洲](http://book.zongheng.com/book/916069.html)
> 作者：袁保卫

> 标签：武侠仙侠

> 简介：本小说讲述一批江湖仙剑豪侠显绝技，英雄众杰逞威能，齐心协力诛众寇，上擂台打虎显才能，报冤仇四海远游，众弟兄齐会歃血练兵，大破迷魂阵，运筹帷幄，决胜疆场，御驾班师，风风火火闯九洲的传奇故事。

> 章节末：天下太平大功告成

> 状态：完本
# 游戏
### [《恶魔之魂：重制版》新截图曝光 大战高塔骑士](https://www.3dmgame.com/news/202006/3791250.html)
> 概要: PS5官网曝光了《恶魔之魂：重制版》一张新截图，展示了游戏中的敌人高塔骑士（Tower Knight）。此前在PS5预告片中高塔骑士曾短暂亮相，而今天曝光的新图则让我们看到玩家大战高塔骑士的场景。根据......
### [魂like《致命躯壳》实机演示 双手长剑直面死敌](https://www.3dmgame.com/news/202006/3791267.html)
> 概要: GameSpot于今日（6月19日）公开了ARPG《致命躯壳》的10分钟实机演示视频，该作将于2020年发售，登陆PS4、Xbox One和PC（Epic限时独占）平台。10分钟实机演示：本次的实机演......
### [《骑砍2》登陆Epic商城 开启8折优惠促销活动](https://www.3dmgame.com/news/202006/3791258.html)
> 概要: 今日（6月19日），Epic官方宣布《骑马与砍杀2》已经正式上线Epic商城，目前该作已经在Epic商城和Steam商城开启8折优惠促销活动，以下为官方公开的相关细节。Epic官博截图：Epic商城页......
### [硬件壹周刊62 安培新对手 AMD CDNA架构显卡确认](https://3c.3dmgame.com/show-15-12719-1.html)
> 概要: 6月19日消息，中兴通讯今天召开2019年度股东大会，对于外界关心的5nm芯片相关问题，中兴通讯总裁徐子阳回应称，目前公司7nm芯片已实现规模量产,而5nm芯片将在2021年推出。昨日，中兴通讯港股大......
### [用电子游戏治疗儿童多动症 已获美国监管许可](https://www.3dmgame.com/news/202006/3791296.html)
> 概要: 近日美国食品药物管理署(FDA)通过第一个具有疗效的游戏“处方药”，用于帮助有多动症(ADHD)的儿童提高注意力。这款游戏名为《EndeavorRX》，由游戏开发商Akili Interactive开......
# 论文
### [Can x2vec Save Lives? Integrating Graph and Language Embeddings for Automatic Mental Health Classification](https://paperswithcode.com/paper/can-x2vec-save-lives-integrating-graph-and)
> 日期：4 Jan 2020

> 标签：ACTION CLASSIFICATION

> 代码：https://github.com/AlexMRuch/Can-x2vec-Save-Lives

> 描述：Graph and language embedding models are becoming commonplace in large scale analyses given their ability to represent complex sparse data densely in low-dimensional space. Integrating these models' complementary relational and communicative data may be especially helpful if predicting rare events or classifying members of hidden populations - tasks requiring huge and sparse datasets for generalizable analyses.
