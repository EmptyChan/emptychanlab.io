---
title: 2023-03-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WildGarlic_ZH-CN6787340186_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-03-24 22:47:24
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WildGarlic_ZH-CN6787340186_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [洞察14种通过ChatGPT变现的商业方式，看到就是赚到](https://www.woshipm.com/chuangye/5789461.html)
> 概要: ChatGPT火了几个月了，但有些人还不知道是什么。作者基于自己的尝试，分享自己利用ChatGPT进行商业变现的一些方式，总结14种可以通过ChatGPT普通人也可以赚钱的方法。前言ChatGPT已经......
### [“结算流程管理系统”建设实战分析](https://www.woshipm.com/pd/5789602.html)
> 概要: 在一个企业中，各个部门生产效率通常不是完全一致的，发展速度也是参差不齐，大多公司会采取数字化转型，流程标准化来提升企业内部生产效率。本文讲述了业财一体化中，针对与B端合作方结算佣金服务费等结算流程标准......
### [京东物流“失宠”了？](https://www.woshipm.com/it/5789646.html)
> 概要: 自营商品到货快，是京东一直以来的招牌之一。但这一招牌，也令京东的经营成本大大提升。这就使得在自营生态下，京东从物流、仓储，到流量成本，都逐渐远离“性价比”。如今，京东上线“百亿补贴”，这无疑是开始与自......
### [ChatGPT阴影之下的谷歌Bard实测：珠玉在前](https://finance.sina.com.cn/tech/internet/2023-03-24/doc-imymxhuf8505050.shtml)
> 概要: 新浪科技 郑峻发自美国硅谷......
### [对话李彦宏：不要重复造轮子，AI的十倍机会在别处](https://36kr.com/p/2184414265262467)
> 概要: 对话李彦宏：不要重复造轮子，AI的十倍机会在别处-36氪
### [纽约时报：收购猜词游戏Wordle彻底改变了公司业务](https://www.3dmgame.com/news/202303/3865544.html)
> 概要: 此前，一款名为 Wordle 的英文猜词游戏在网上爆火。时至今日，这款游戏依然有着许多忠实的玩家。而著名刊物《纽约时报》之后于 2022 年 1 月收购了这款游戏。在 GDDC 2023 上，《纽约时......
### [暴雪和NV调查《暗黑破坏神4》让3080 Ti变砖问题](https://www.3dmgame.com/news/202303/3865548.html)
> 概要: 之前有许多玩家反馈称，他们的RTX 3080 Ti显卡运行《暗黑破坏神4》测试版时出现故障。有些人的显卡竟然变砖，无法再使用。现在据外媒Pcgamer报道称，《暗黑破坏神4》团队正在调查关于RTX 3......
### [海外new things | B2B支付技术初创「Parker」融资1.57亿美元，为电商企业提供更高的信贷额度](https://36kr.com/p/2177773363179785)
> 概要: 海外new things | B2B支付技术初创「Parker」融资1.57亿美元，为电商企业提供更高的信贷额度-36氪
### [俄罗斯电视台推出AI美女主播：灵感来自中国](https://www.3dmgame.com/news/202303/3865560.html)
> 概要: 近日据今日俄罗斯报道，俄罗斯南部斯塔夫罗波尔地区的一家电视台推出了一位AI天气预报女播报员，她亮相2次后就圈粉不少，连男主持人都羡慕她有很多“新衣服”。节目负责人介绍称，这位俄罗斯AI女播报员的灵感来......
### [《读者》的读者去哪儿了](https://www.huxiu.com/article/897889.html)
> 概要: 本文来自微信公众号：巨潮WAVE（ID：WAVE-BIZ），作者：老鱼儿，编辑：杨旭然，头图来自：视觉中国在A股资本市场的角落里，躲着一家与众不同的上市公司，身处文化产业中大名鼎鼎，几乎每个人都看到过......
### [海外new things | 机器人技术初创「Nimble Robotics」B轮融资6500万美元，构建全自动第三方物流仓库](https://36kr.com/p/2177773813559557)
> 概要: 海外new things | 机器人技术初创「Nimble Robotics」B轮融资6500万美元，构建全自动第三方物流仓库-36氪
### [提振出行消费，中石化易捷启动第二届出行养车节](http://www.investorscn.com/2023/03/24/106440/)
> 概要: 3月23日，中石化易捷第二届出行养车节在四川成都启动。启动仪式上，中石化易捷推出“出行养车卡”，为消费者提供加油满减、洗车免费、安心出行等优惠服务。同时宣布将积极响应国家扩大内需、提振消费号召，充分发......
### [双创扬帆 “澄”功启航 | 第六届中国江阴（高新区）创新创业大赛启动报名](http://www.investorscn.com/2023/03/24/106441/)
> 概要: 双创扬帆 “澄”功启航 | 第六届中国江阴（高新区）创新创业大赛启动报名
### [《狂想乐园》宣布6月16日发售 预购正式开启](https://www.3dmgame.com/news/202303/3865566.html)
> 概要: 由万代南梦宫发行的游乐园建造模拟游戏《狂想乐园》宣布将于6月16日正式发售，登陆PS5、Xbox Series X|S和Steam，封闭测试将于5月9日开启，目前游戏预购已开启，Steam国区定价24......
### [《菜鸟APP取件主题歌》动画](https://www.zcool.com.cn/work/ZNjQ1OTQ0ODA=.html)
> 概要: 菜鸟APP取件主题曲已上线 3月22日消息,菜鸟APP取件主题曲已上线。在菜鸟驿站打开菜鸟app,让取快递拉满仪式感,洗脑神曲和真香福利组合,让取件更有味儿。CIIENT：菜鸟Agency：WMYPr......
### [视频：新浪娱乐独家对话《声生不息宝岛季》坏特](https://video.sina.com.cn/p/ent/2023-03-24/detail-imymycyc6207838.d.html)
> 概要: 视频：新浪娱乐独家对话《声生不息宝岛季》坏特
### [第六届中国江阴（高新区）创新创业大赛邀你一起为梦想打拼！](http://www.investorscn.com/2023/03/24/106444/)
> 概要: 第六届中国江阴（高新区）创新创业大赛邀你一起为梦想打拼！
### [昨夜，TikTok在美经历生死时刻](https://www.huxiu.com/article/911573.html)
> 概要: 本文来自微信公众号：中国企业家杂志 （ID：iceo-com-cn），采访：赵东山、闫俊文，文字：赵东山，编辑：李薇，原文标题：《昨夜，全球10亿月活TikTok在美经历生死时刻》，头图来自：视觉中国......
### [视频：章泽天现身香港艺术展 二胎后依旧少女感满满](https://video.sina.com.cn/p/ent/2023-03-24/detail-imymycye3016764.d.html)
> 概要: 视频：章泽天现身香港艺术展 二胎后依旧少女感满满
### [宣传册设计](https://www.zcool.com.cn/work/ZNjQ1OTg0NDQ=.html)
> 概要: Collect......
### [美团2022年第四季度营收601.3亿元 调整后净利润8.3亿元](https://finance.sina.com.cn/tech/internet/2023-03-24/doc-imymykha2923854.shtml)
> 概要: 新浪科技讯 3月24日下午消息，美团今日发布2022年第四季度及全年财报。财报显示，该公司第四季度营收601.3亿元，同比增长21.4%。净亏损10.8亿元，预估亏损15.3亿元；调整后净利润为8.3......
### [剧场动画《落第魔女》公开15个新画面](https://news.idmzj.com/article/77565.html)
> 概要: 剧场动画《落第魔女风嘉与暗之魔女》公开了15个新场面。
### [微软回应取消PS5版《红霞岛》：不存在的事](https://www.3dmgame.com/news/202303/3865588.html)
> 概要: 开发商Arkane的创意总监Harvey Smith近日在接受IGN法国采访时透露，《红霞岛》曾拥有过PS5版本，但在被微软收购后，该版本便被取消了，这对于正在收购动视暴雪的微软来说并不是个好消息。而......
### [杰克奥特曼扮演者逝世 86款国产网游过审](https://news.idmzj.com/article/77569.html)
> 概要: 乡秀树扮演者团时朗逝世、国产网游过审名单公布、每周游戏喜加N
### [《咒术回战》第2期《怀玉·玉折篇》主视觉图公开](https://news.idmzj.com/article/77570.html)
> 概要: TV动画《咒术回战》第2期，《怀玉·玉折篇》的主视觉图已经公开了。
### [S-FIRE《Re：从零开始的异世界生活》爱蜜莉雅手办开始预约](https://news.idmzj.com/article/77571.html)
> 概要: 《Re：从零开始的异世界生活》爱蜜莉雅&童年爱蜜莉雅手办开始接受预约了。
### [粉丝透露时代少年团近期有演唱会规划 正在筹备中](https://ent.sina.com.cn/y/yneidi/2023-03-24/doc-imymykha3019638.shtml)
> 概要: 新浪娱乐讯 3月24日，时代少年团粉丝发微博透露：“时代少年团近期有4万人体育场演唱会规划，目前正在筹备报批同时少年们也在进行舞台排练。”(责编：小5)......
### [飞书发布“业务三件套”，从办公延伸至业务核心环节 | 最前线](https://36kr.com/p/2182340344561154)
> 概要: 飞书发布“业务三件套”，从办公延伸至业务核心环节 | 最前线-36氪
### [一汽首款 CTP 构型液冷 HEV 电池包 B 样成功下线，用于红旗高端车型](https://www.ithome.com/0/682/131.htm)
> 概要: IT之家3 月 24 日消息，近日，一汽集团 PH2.1 电池项目首台 OTS 样包暨一汽首款 CTP 构型液冷 HEV 电池包 B 样成功在研发总院下线。一汽集团表示，PH2.1 电池项目组秉承着关......
### [硬件监测软件 Hwinfo 初步支持英特尔新一代桌面处理器 Arrow Lake-S](https://www.ithome.com/0/682/134.htm)
> 概要: IT之家3 月 24 日消息，硬件监测软件 Hwinfo V7.41 build 5020 版本现已添加对英特尔新一代桌面处理器 Arrow Lake-S 的支持。消息称，英特尔今年晚些时候将推出的新......
### [证监会：协调推进投资端和融资端改革，守牢不发生系统性风险的底线](https://finance.sina.com.cn/roll/2023-03-24/doc-imymyqpy2949806.shtml)
> 概要: 来源：证监会 聚焦“健全资本市场功能，提高直接融资比重”，坚持稳中求进工作总基调，协调推进投资端和融资端改革，守牢不发生系统性风险的底线。
### [李强主持召开国务院常务会议 研究优化完善部分阶段性税费优惠政策](https://finance.sina.com.cn/china/2023-03-24/doc-imymyqpz9748179.shtml)
> 概要: 来源：央视网 确定国务院2023年重点工作分工，研究优化完善部分阶段性税费优惠政策，审议“三农”重点任务工作方案，听取巩固拓展脱贫攻坚成果情况汇报。
### [李强主持召开国务院常务会议 研究优化完善部分阶段性税费优惠政策](https://www.yicai.com/news/101711566.html)
> 概要: 确定国务院2023年重点工作分工，研究优化完善部分阶段性税费优惠政策，审议“三农”重点任务工作方案，听取巩固拓展脱贫攻坚成果情况汇报。
### [深圳公积金贷款新规来了，首套房最高额度可上浮40%](https://www.yicai.com/news/101711575.html)
> 概要: 进一步加大对购买首套住房、多子女家庭购房、购买绿色建筑住房的支持力度。
### [长城人寿荣膺“年度金牌创新力金融机构”](http://www.investorscn.com/2023/03/24/106455/)
> 概要: 3月24日，由易趣财经传媒、《金融理财》杂志社主办的第十三届“金貔貅奖”颁奖典礼上，长城人寿凭借在运营模式、投资模式、服务模式、产品营销与文化融合创新方面的丰硕成果，获评“年度金牌创新力金融机构”称号......
### [乐鱼体育宣布与西甲西班牙人俱乐部达成官方合作关系!](http://www.investorscn.com/2023/03/24/106456/)
> 概要: 乐鱼体育宣布与西甲西班牙人俱乐部达成官方合作伙伴关系，双方将充分发挥各自优势，就重要赛事合作、数字体育品牌营销等方面展开深度合作，推进数字体育的市场推广与落地，共同构建互联网时代下的数字体育新篇章......
### [证监会发布20起典型案例，信披违规仍是稽查重点](https://finance.sina.com.cn/wm/2023-03-24/doc-imymyqpz9759977.shtml)
> 概要: 证监会3月24日发布2022年证监稽查20起典型违法案例，信息披露违法违规仍是稽查重点，多家中介机构被点名。 此次发布的20起典型案例包括信息披露违法违规案...
### [李强主持召开国常会：确定国务院2023年重点工作分工](https://finance.sina.com.cn/china/2023-03-24/doc-imymyqpy2988722.shtml)
> 概要: 来源：央视网 国务院总理李强3月24日主持召开国务院常务会议，确定国务院2023年重点工作分工，研究优化完善部分阶段性税费优惠政策，审议“三农”重点任务工作方案...
### [被打断的周受资，和遭“群嘲”的美国议员](https://www.huxiu.com/article/915031.html)
> 概要: 出品｜虎嗅商业消费组作者｜周月明编辑｜苗正卿题图｜视觉中国TikTok与美国长达两年多的斡旋，终于来到了高潮。美国时间3月23日，TikTok CEO周受资出席美国国会听证会，在TikTok的控制权、......
### [【IT之家评测室】OPPO Pad 2 平板上手：黄金比例大屏优势明显，跨屏互联生产力先锋](https://www.ithome.com/0/682/144.htm)
> 概要: 伴随着 Find X6 系列手机的发布，全新的 OPPO Pad 2 平板也如期而至，相较于一年前发布的首款平板 OPPO Pad，OPPO Pad 2 在设计、屏幕、形态以及键盘等方面都迎来巨大变化......
### [年内28城已调整公积金政策，深圳家庭最高可贷额度提升至126万](https://www.yicai.com/news/101711582.html)
> 概要: 深圳公积金贷款最高额度从原本的90万元提升至126万元。
### [打造新一代交易系统、推进智能监管，交易所构建科技服务新生态](https://www.yicai.com/news/101711596.html)
> 概要: 上市公司监管如何智能化？
### [4.36Gbps！爱立信与联发科创下 5G 下行速率新纪录](https://www.ithome.com/0/682/147.htm)
> 概要: 感谢IT之家网友打工轨道人的线索投递！IT之家3 月 24 日消息，近日，爱立信与联发科技成功实现了四个载波的聚合，包括一个频分双工（FDD）载波和三个时分双工（TDD）载波，实现了 4.36Gbps......
### [ChatGPT出笼，这次要革互联网的命了](https://www.huxiu.com/article/915021.html)
> 概要: 出品｜虎嗅科技组作者｜齐健编辑｜陈伊凡头图｜FlagStudio在终结者电影里，人类把人工智能天网接入导弹防御网络后，世界被毁灭了。那么，当ChatGPT接入互联网，是不是也相当于“困兽出笼”？Cha......
### [昨晚，他一人，笑对美国鸿门宴，站在暴风雨中央](https://finance.sina.com.cn/china/2023-03-24/doc-imymyuvv5869198.shtml)
> 概要: 来源：华商韬略 深蓝色西装，只身一人坐在数米长桌的一侧，对面是数十位共和党议员…… 当地时间2023年3月23日，周受资代表TikTok出席了美国听证会。
### [港交所宣布推出特专科技公司上市机制 A股走势分化](https://www.yicai.com/video/101711629.html)
> 概要: 港交所宣布推出特专科技公司上市机制 A股走势分化
# 小说
### [道门狂婿](http://book.zongheng.com/book/1095697.html)
> 作者：雪中红

> 标签：都市娱乐

> 简介：一代崂山掌门人，被妖王趁虚而入，临死之际灵魂出窍，穿越到300年后的现代都市，附身在一名软饭男的身上，于是，在复仇妖王的同时，还要与丈母娘作斗争……难搞，难搞喔……

> 章节末：谁叫你是上门女婿

> 状态：完本
# 论文
### [Assessing Differentially Private Variational Autoencoders under Membership Inference | Papers With Code](https://paperswithcode.com/paper/assessing-differentially-private-variational)
> 概要: We present an approach to quantify and compare the privacy-accuracy trade-off for differentially private Variational Autoencoders. Our work complements previous work in two aspects. First, we evaluate the the strong reconstruction MI attack against Variational Autoencoders under differential privacy. Second, we address the data scientist's challenge of setting privacy parameter epsilon, which steers the differential privacy strength and thus also the privacy-accuracy trade-off. In our experimental study we consider image and time series data, and three local and central differential privacy mechanisms. We find that the privacy-accuracy trade-offs strongly depend on the dataset and model architecture. We do rarely observe favorable privacy-accuracy trade-off for Variational Autoencoders, and identify a case where LDP outperforms CDP.
### [Adaptive Discretization in Online Reinforcement Learning | Papers With Code](https://paperswithcode.com/paper/adaptive-discretization-in-online)
> 概要: Discretization based approaches to solving online reinforcement learning problems have been studied extensively in practice on applications ranging from resource allocation to cache management. Two major questions in designing discretization-based algorithms are how to create the discretization and when to refine it.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
