---
title: 2023-05-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WatSriSawai_ZH-CN7688908090_1920x1080.webp&qlt=50
date: 2023-05-26 19:34:58
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WatSriSawai_ZH-CN7688908090_1920x1080.webp&qlt=50)
# 新闻
### [一次失败的产品交流，带给我九点启示](https://www.woshipm.com/pmd/5834762.html)
> 概要: 产品经理是一个经常需要沟通交流的岗位，尤其是向其他部门讲解产品的信息，非常考验语言能力和逻辑思维带动的应变力。本文作者根据自己的一次失败的产品交流会，总结了一些经验，和大家分享。前段时间，我参加了一场......
### [SaaS产品运营：如何推动从0~1](https://www.woshipm.com/operate/5834242.html)
> 概要: SaaS产品面向的是企业用户，在推行产品的过程中难度系数会比普通的C端产品高。如何运营好一个SaaS产品？本文作者将运用自己的产品运营经验，在冷启动、售前、售中、售后四个环节上，介绍如何从零开始运营S......
### [供应链金融：如何提升上下游企业信用](https://www.woshipm.com/it/5832195.html)
> 概要: 针对中小微企业在供应链金融中面临的融资困境，提升企业信用成为关键之一。本文旨在探讨如何采取有效措施来提升企业信用，进而优化供应链金融效能。在现代供应链中，核心企业、下游采购商和上游供应商都扮演着重要角......
### [《芭比》真人电影正式预告 7月21日上映](https://www.3dmgame.com/news/202305/3869985.html)
> 概要: 《芭比》真人电影正式预告曝光，影片将于7月21日在北美上映。预告片：最耀眼的派对✅最华丽的排舞 ✅生活在芭比乐园，就是身处一个完美地方过着完美的人生，除非你遇到全面性的存在危机，或者你刚好是肯尼。影片......
### [玄天仙猫与金刚狸将](https://www.zcool.com.cn/work/ZNjUzNDYyNzY=.html)
> 概要: 和八仙九猫认识很多年，同是爱猫人合作两幅手绘水彩，收录于这本《猫仙人的世界》八仙九猫原画集，匠心表达，诸神可爱......
### [黄璐发文吐槽国泰航空服务 称工作人员态度冷漠](https://ent.sina.com.cn/2023-05-26/doc-imyvacek1597632.shtml)
> 概要: 新浪娱乐讯 日前，演员黄璐发博讲述乘坐国泰航空的糟心经历， 吐槽工作人员态度冷漠，服务不到位，她还表示回到西安之后，海关态度非常好，“说别担心，证件丢了没关系，只要是中国公民都会让你回家。一瞬间差点想......
### [马斯克的公司称已获美监管机构批准进行人类大脑植入物试验](https://finance.sina.com.cn/jjxw/2023-05-26/doc-imyvacef0698584.shtml)
> 概要: 来源：环球网......
### [50万人齐聚光伏大Party，企业家们吐槽：行业标准？一塌糊涂！](https://finance.sina.com.cn/tech/it/2023-05-26/doc-imyvacek1605686.shtml)
> 概要: 文|新浪财经 刘丽丽......
### [曹骏回应性格不适合娱乐圈：每个人都是很独特的](https://ent.sina.com.cn/v/m/2023-05-26/doc-imyvaceh4831613.shtml)
> 概要: 新浪娱乐讯 在最新采访中，面对网友“性格不适合娱乐圈”的评价，曹骏回应自己并不知道什么样的性格娱乐圈是适用的，所谓有个性又是什么，他表示“每个人都是独特的，把自己认可的东西做好，延续下去，做好自己吧”......
### [傍上雷军、薇娅的德尔玛，撕不掉“代工”的标签](https://finance.sina.com.cn/tech/it/2023-05-26/doc-imyvacek1614240.shtml)
> 概要: 文 丨 新浪科技 周文猛......
### [《牧场物语》系列公布两款新作 平台发售日期待定](https://www.3dmgame.com/news/202305/3869991.html)
> 概要: 在今早举行的发布会直播上，开发商Marvelous公布了《牧场物语》系列两款新作，一款为传统农场生活玩法，另一款偏向多人游戏，具体登陆平台、发售日期没有公布，但官方公布了传统玩法游戏的概念预告视频以及......
### [《星空》将推限定Xbox手柄和无线耳机 6月亮相](https://www.3dmgame.com/news/202305/3869993.html)
> 概要: 近日知名舅舅党billbil-kun爆料称，微软Xbox将在未来推出《星空》主题限定Xbox手柄和无线耳机，代号分别为“Ogden”和“Orren”。据悉，《星空》主题限定Xbox手柄售价79.99美......
### [“参考我们智能选址开店的中小客户，存活率在70%以上”](https://36kr.com/p/2273201024697990)
> 概要: “参考我们智能选址开店的中小客户，存活率在70%以上”-36氪
### [海外new things | 计算机视觉技术公司「Visual Layer」种子轮融资700万美元，帮助企业管理构建AI模型所需的可视化数据集](https://36kr.com/p/2273176506713736)
> 概要: 海外new things | 计算机视觉技术公司「Visual Layer」种子轮融资700万美元，帮助企业管理构建AI模型所需的可视化数据集-36氪
### [《海贼王:时光旅诗》DLC旧日絮语上线 新玩法登场](https://www.3dmgame.com/news/202305/3870004.html)
> 概要: 《海贼王：时光旅诗》追加剧情DLC“旧日絮语”现已正式上线，该DLC需要通过购买冒险扩展包获得，Steam国区售价148元。本次追加剧情DLC将登场两项新玩法，可享受难度更高更刺激并限定胜利条件的战斗......
### [面向北美新兴品牌及国内出海企业，「Passioncy」提供AI短视频生产力工具及服务 | 早期项目](https://36kr.com/p/2267808281731073)
> 概要: 面向北美新兴品牌及国内出海企业，「Passioncy」提供AI短视频生产力工具及服务 | 早期项目-36氪
### [EXO粉丝要求CHEN朴灿烈退队 不满二人私生活丑闻](https://ent.sina.com.cn/y/yrihan/2023-05-26/doc-imyvapua4650154.shtml)
> 概要: 新浪娱乐讯 韩国男团EXO的部分粉丝今天在SM娱乐公司前举行卡车抗议示威，要求CHEN金钟大与朴灿烈两人退出组合。　　今天上午，部分EXO粉丝在SM娱乐公司前停放一辆卡车并拉出“给EXO造成伤害的金钟......
### [必应成中国第一？百度：数据离谱 中文搜索有自信第一](https://www.3dmgame.com/news/202305/3870011.html)
> 概要: 最近美国数据研究机构StatCounter发布了一份统计数据，显示在中国桌面搜索引擎份额中，百度被微软的必应超越。此事很快就在网上引起热议，话题“百度已不是中国第一大桌面搜索引擎”更是登上微博热搜。据......
### [百度飞桨平台产品负责人毕然将离职，将加入土豆数据 | LongChina50独家](https://36kr.com/p/2271711682420743)
> 概要: 百度飞桨平台产品负责人毕然将离职，将加入土豆数据 | LongChina50独家-36氪
### [重磅新品实力亮相！SNEC 2023聚时精彩盘点](http://www.investorscn.com/2023/05/26/107743/)
> 概要: 2023年5月24~26日，第十六届（2023）国际太阳能光伏与智慧能源（上海）大会暨展览会（以下简称SNEC 2023正式向观众开放。时隔两年再次开展，SNEC 2023吸引了近50万人注册参展，展......
### [马斯克公司首获美监管机构批准 可开展人脑芯片植入试验](https://www.yicai.com/news/101767188.html)
> 概要: 但目前，临床试验招募尚未开放。
### [大小朋友儿童节](https://www.zcool.com.cn/work/ZNjUzNTEyMzI=.html)
> 概要: 大小朋友儿童节快乐！哈咪猫和摩丝摩丝儿童节插画、表情、漫画、头像。我们以往儿童节的创作～https://www.zcool.com.cn/work/ZNjAwMzIwNzY=.htmlhttps://......
### [全球股市波动这里还是避风港，美元指数缘何逆风上扬](https://finance.sina.com.cn/roll/2023-05-26/doc-imyvauac6544936.shtml)
> 概要: 5月以来持续上行，剑指年内高位。 随着美国国会债务上限谈判进入关键阶段，美元指数在不确定性忧虑推动下持续拉升，并刷新近十周来高位。
### [​何小鹏：我到底，焦不焦虑？](https://www.huxiu.com/article/1599630.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 小鹏汽车“勿蕉绿”，是小鹏汽车App商城上一款衬衣的颜色名。在4月的上海车展期间，小鹏汽车CEO何小鹏穿着同款“勿蕉绿”衬衣登台，发布了公司......
### [欧洲气价降至两年新低，能源危机结束了吗？](https://finance.sina.com.cn/jjxw/2023-05-26/doc-imyvauak8464810.shtml)
> 概要: 界面新闻记者 | 侯瑞宁界面新闻编辑 | 1欧洲天然气期货价格连续八周下跌。5月25日，洲际交易所荷兰TTF天然气6月期货价格收盘价为25欧元/兆瓦时（约合人民币1...
### [重回巅峰！华体会祝贺国际米兰卫冕意大利杯冠军！](http://www.investorscn.com/2023/05/26/107752/)
> 概要: 在罗马奥林匹克球场进行的22/23赛季意大利杯决赛中，国际米兰凭借着劳塔罗-马丁内斯的两粒进球，2-1逆转战胜佛罗伦萨，成功夺得本赛季第二座冠军奖杯!华体会(HTH)作为国际米兰长期合作伙伴，第一时间......
### [如何让中国白酒飘香世界？中国白酒国际化创新与发展论坛在西安举办](http://www.investorscn.com/2023/05/26/107754/)
> 概要: 如何让中国白酒飘香世界？中国白酒国际化创新与发展论坛在西安举办
### [CFPLS21 蛟龙现 群侠出](https://www.zcool.com.cn/work/ZNjUzNTI1NTY=.html)
> 概要: Play VideoPlayreplay 10 secondsforward 10 secondsCurrent Time0:00/Duration Time0:00Progress: NaN%Pla......
### [网易游戏，混成老三](https://www.huxiu.com/article/1599668.html)
> 概要: 本文来自微信公众号：深燃（ID：shenrancaijing），作者：李秋涵，编辑：魏佳，题图来源：视觉中国5月25日，网易发布2023年第一季度财报。数据表现不错。根据财报，网易净收入为250亿元，......
### [努比亚红魔电竞机械键盘“氘锋银翼版”开启预售：配备屏显交互，到手价 899 元](https://www.ithome.com/0/695/469.htm)
> 概要: IT之家5 月 26 日消息，努比亚红魔电竞机械键盘新款“银翼版”配色上架电商平台，到手价 899 元。红魔电竞机械键盘银翼版采用100 键紧凑键型设计，兼顾游戏和办公全场景；采用银翼 PBT 键帽，......
### [马斯克的Neuralink获批人体临床 脑机接口能使瘫痪者行走](https://www.yicai.com/news/101767284.html)
> 概要: Neuralink公司的脑机接口设备是一种侵入式的大脑植入物，通过神经信号控制外部设备，从而帮助重度瘫痪患者恢复与外界沟通的能力，未来这种技术还能让瘫痪者行走，让失明者重新看见。
### [中小银行入局不良贷款转让业务](https://www.yicai.com/news/101767257.html)
> 概要: 在实际操作过程中目前一些中小银行对上述渠道使用积极性还并不是很高。
### [联想如何克服“库存”魔咒](https://www.huxiu.com/article/1601150.html)
> 概要: 出品｜虎嗅科技组作者｜陈伊凡、齐健头图｜联想40多年前，深圳从香港转移来的“三来一补”加工贸易中，捞取了第一桶金，那是当代“中国制造”的起点。如今，曾经的渔村发展成大都会深圳，这座被制造改变的城市，制......
### [小米空调 1.5 匹 / 立式 3 匹 618 闪降 400-700 元，到手价 1799-4599 元](https://www.ithome.com/0/695/498.htm)
> 概要: IT之家5 月 26 日消息，小米空调 1.5 匹新 3 级能效开启 618 大促，闪降 400 元，到手价 1799 元。小米空调 1.5 匹新 1 级能效开启 618 大促，闪降 600 元，到手......
### [因手机业务终止， LG SmartWorld 服务宣布 7 月 3 日停止运营](https://www.ithome.com/0/695/501.htm)
> 概要: 感谢IT之家网友疯狂星期四的线索投递！IT之家5 月 26 日消息，据IT之家网友反馈，收到 LG 发送的消息显示，由于 LG Electronics 手机业务终止，LG SmartWorld 服务将......
### [天然牛黄一年涨价近50%，安宫牛黄丸又要涨价了？](https://finance.sina.com.cn/jjxw/2023-05-26/doc-imyvaykf1602291.shtml)
> 概要: 界面新闻记者 | 陈杨界面新闻编辑 | 谢欣1前脚片仔癀提价，后脚就有了安宫牛黄丸或将跟上的声音。日前，行业媒体赛柏蓝报道称，业内有声音称天然牛黄中药材价格上涨...
### [打口水战不如比拼硬实力！文心一言VS科大讯飞VS360智脑 谁更胜一筹？](https://finance.sina.com.cn/china/2023-05-26/doc-imyvaykh8386959.shtml)
> 概要: 来源：《科创板日报》 大模型领域的口水战不断。 此前，王小川与百度阵营因在技术差距上的不同意见，引发广泛热议。近日，对于股价一度逼近跌停...
### [时间定了！油价又要调整？](https://finance.sina.com.cn/china/2023-05-26/doc-imyvaykf1608964.shtml)
> 概要: 中国基金报 晨曦 新一轮油价调整，又要来了！ 按照国内现行成品油调整机制，下周二（5月30日）24时国内第十一轮油价调整将正式开始。
### [厂家揭秘！100W+曝光轻松达成，弯道超车火速实现](http://www.investorscn.com/2023/05/26/107762/)
> 概要: “在大多数医药保健企业还在做线下营销时，我们已经开始布局线上，实现弯道超车。”河南汉方药业有限责任公司负责人说道......
### [京东618预售期家居家装大牌爆发 松下、富安娜等多品牌订单额同比增长超5倍](http://www.investorscn.com/2023/05/26/107763/)
> 概要: 随着“更多、更快、更好、更省”的京东618大幕拉开，消费热情逐渐被点燃。2023年5月23日晚8点预售开启，截至5月25日24点，人均购买预售商品数量同比增长超30%，县域及农村市场预售订单金额同比增......
### [马斯克的脑机接口，终于被批准人体试验了](https://www.huxiu.com/article/1601144.html)
> 概要: 本文来自微信公众号：果壳 （ID：Guokr42），作者：作者：Jayden、睿悦、沈知涵、 ChatGPT，编辑：biu，原文标题：《终于！FDA 批准了马斯克脑机接口的真人手术，意义非凡》，题图来......
### [长城举报比亚迪背后：产品技术渠道三重门，昔日“自主三强”光芒为何不再？](https://www.yicai.com/video/101767445.html)
> 概要: 长城举报比亚迪背后：产品技术渠道三重门，昔日“自主三强”光芒为何不再？
### [机构今日买入这16股，抛售广汇能源7.89亿元](https://www.yicai.com/news/101767446.html)
> 概要: 当天机构净买入前三的股票分别是金桥信息、寒武纪、昆药集团，净买入金额分别是1.88亿元、1.32亿元、7170万元。
### [《卧龙：苍天陨落》游戏首个 DLC“逐鹿中原”公布：6 月底发售，定价 69 元](https://www.ithome.com/0/695/539.htm)
> 概要: 感谢IT之家网友雨雪载途的线索投递！IT之家5 月 26 日消息，光荣特库摩开发的魔改三国游戏《卧龙：苍天陨落》即将迎来首个 DLC，该 DLC 定名“逐鹿中原”，官方今日放出了主视觉海报。从海报中可......
# 小说
### [天才纨绔](http://book.zongheng.com/book/316562.html)
> 作者：陌上猪猪

> 标签：都市娱乐

> 简介：超级强者一缕元神穿越地球夺舍重生，融入了因为坠马死亡的纨绔子弟身上，在繁华都市中以新的身份开始了不一样的生活。会修真会泡妞，能治病能杀人，神级天赋，术法通神。且看他如何逆天改命，在红尘美色中，一步步踏上人世巅峰！书友群278615754

> 章节末：完本感言兼新书

> 状态：完本
# 论文
### [Integrate Lattice-Free MMI into End-to-End Speech Recognition | Papers With Code](https://paperswithcode.com/paper/integrate-lattice-free-mmi-into-end-to-end)
> 概要: In automatic speech recognition (ASR) research, discriminative criteria have achieved superior performance in DNN-HMM systems. Given this success, the adoption of discriminative criteria is promising to boost the performance of end-to-end (E2E) ASR systems. With this motivation, previous works have introduced the minimum Bayesian risk (MBR, one of the discriminative criteria) into E2E ASR systems. However, the effectiveness and efficiency of the MBR-based methods are compromised: the MBR criterion is only used in system training, which creates a mismatch between training and decoding; the on-the-fly decoding process in MBR-based methods results in the need for pre-trained models and slow training speeds. To this end, novel algorithms are proposed in this work to integrate another widely used discriminative criterion, lattice-free maximum mutual information (LF-MMI), into E2E ASR systems not only in the training stage but also in the decoding process. The proposed LF-MMI training and decoding methods show their effectiveness on two widely used E2E frameworks: Attention-Based Encoder-Decoders (AEDs) and Neural Transducers (NTs). Compared with MBR-based methods, the proposed LF-MMI method: maintains the consistency between training and decoding; eschews the on-the-fly decoding process; trains from randomly initialized models with superior training efficiency. Experiments suggest that the LF-MMI method outperforms its MBR counterparts and consistently leads to statistically significant performance improvements on various frameworks and datasets from 30 hours to 14.3k hours. The proposed method achieves state-of-the-art (SOTA) results on Aishell-1 (CER 4.10%) and Aishell-2 (CER 5.02%) datasets. Code is released.
### [In Defense of Image Pre-Training for Spatiotemporal Recognition | Papers With Code](https://paperswithcode.com/paper/in-defense-of-image-pre-training-for)
> 概要: Image pre-training, the current de-facto paradigm for a wide range of visual tasks, is generally less favored in the field of video recognition. By contrast, a common strategy is to directly train with spatiotemporal convolutional neural networks (CNNs) from scratch. Nonetheless, interestingly, by taking a closer look at these from-scratch learned CNNs, we note there exist certain 3D kernels that exhibit much stronger appearance modeling ability than others, arguably suggesting appearance information is already well disentangled in learning. Inspired by this observation, we hypothesize that the key to effectively leveraging image pre-training lies in the decomposition of learning spatial and temporal features, and revisiting image pre-training as the appearance prior to initializing 3D kernels. In addition, we propose Spatial-Temporal Separable (STS) convolution, which explicitly splits the feature channels into spatial and temporal groups, to further enable a more thorough decomposition of spatiotemporal features for fine-tuning 3D CNNs. Our experiments show that simply replacing 3D convolution with STS notably improves a wide range of 3D CNNs without increasing parameters and computation on both Kinetics-400 and Something-Something V2. Moreover, this new training pipeline consistently achieves better results on video recognition with significant speedup. For instance, we achieve +0.6% top-1 of Slowfast on Kinetics-400 over the strong 256-epoch 128-GPU baseline while fine-tuning for only 50 epochs with 4 GPUs. The code and models are available at https://github.com/UCSC-VLAA/Image-Pretraining-for-Video.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
