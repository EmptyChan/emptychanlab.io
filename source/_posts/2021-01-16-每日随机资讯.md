---
title: 2021-01-16-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.GlassIgloos_EN-CN8015363995_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-01-16 22:16:56
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.GlassIgloos_EN-CN8015363995_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：邓超官宣担任《创造营2021》发起人 希望自己成团成功](http://slide.ent.sina.com.cn/z/v/slide_4_704_351321.html)
> 概要: 组图：邓超官宣担任《创造营2021》发起人 希望自己成团成功
### [组图：52岁伊能静打扮洋气秀美腿 与小10岁秦昊亲密挽臂好般配](http://slide.ent.sina.com.cn/star/slide_4_704_351304.html)
> 概要: 组图：52岁伊能静打扮洋气秀美腿 与小10岁秦昊亲密挽臂好般配
### [组图：Lucky 6岁生日写真曝光 精灵可爱还越来越漂亮](http://slide.ent.sina.com.cn/star/slide_4_704_351309.html)
> 概要: 组图：Lucky 6岁生日写真曝光 精灵可爱还越来越漂亮
### [金泰妍晒九人少女时代引热议 此前称组合只有8人](https://ent.sina.com.cn/s/j/2021-01-16/doc-ikftpnnx7965054.shtml)
> 概要: 新浪娱乐讯 1月6日，金泰妍在个人社交账号分享了九人少女时代得歌曲引发热议。此前，金泰妍说过我们少女时代八人，后来秀英也怼粉丝说少女时代只有八人，似乎不把退团得郑秀妍纳入在内。据悉，早年郑秀妍因个人事......
### [组图：孟佳化身行走的小白清爽时尚 挎精致小包笑眼弯弯超温柔](http://slide.ent.sina.com.cn/star/slide_4_704_351302.html)
> 概要: 组图：孟佳化身行走的小白清爽时尚 挎精致小包笑眼弯弯超温柔
### [吴奇隆旗下公司上市，老板娘身价暴涨，给刘诗诗当剧终礼物](https://new.qq.com/omn/20210116/20210116A0DA1800.html)
> 概要: 吴奇隆自从和刘诗诗结婚后，影视作品产出大降，每天晒晒美食发发文案，生活好不惬意。然而这是他表现出来的，背地里的努力外人看不到。            据台媒报道，吴奇隆旗下公司江苏稻草熊影业有限公司在......
### [流行“傻白甜”男主了？](https://new.qq.com/omn/20210116/20210116A0D64100.html)
> 概要: 全新解锁更多好玩的版块~~~            远看是霸道男友，敢偷我老婆东西？让你站不起来！▼            敢抢我老婆？先问问我手里的“枪”同不同意！▼            想和我干......
### [美女主持人眼睛像快掉出来，身材魁梧被批丑，旧照太美揭悲惨真相](https://new.qq.com/omn/20210116/20210116A0D59A00.html)
> 概要: 1月16日，有网友发现在某公司举办的活动中，有一位主持人的眼睛过于怪异，而引发了网友热议。在主持过程中这位美女主持人表情有些狰狞，眼睛好像用尽全力瞪大着，导致眼白过多，有些吓人。           ......
### [李明启：幼年丧父，家境贫寒，演容嬷嬷太出色被观众记恨十几年](https://new.qq.com/omn/20210116/20210116A0D8GC00.html)
> 概要: 当我们提到李明启，脑子里第一个想到的是那个让人恨得咬牙切齿的容嬷嬷，而不是绯闻、流量和黑料。这就是一位老艺术家的操守。            一、王婆开启的“黑化”之路1946年，父亲离世。李明启只有......
### [韩网红Hamzy点赞辱华言论后直播道歉，全程呆滞憋笑被指阴阳怪气](https://new.qq.com/omn/20210116/20210116A0CR1C00.html)
> 概要: 1月16日，韩国吃播网红hamzy因在外网点赞多条对中国的侮辱性评论引发了剧烈争议，随后hamzy在多个社交平台公开道歉，并开设直播亲自回应此事。Hamzy在直播中表示很抱歉在接近年关的时候给大家带来......
# 动漫
### [来了！《咒术回战》新片头&片尾MV公布](https://acg.gamersky.com/news/202101/1355362.shtml)
> 概要: 大热动画《咒术回战》公布了新的OP和ED。
### [B站评2020年度动画 《进击的巨人》最终季高票获选](https://acg.gamersky.com/news/202101/1355385.shtml)
> 概要: 哔哩哔哩弹幕网今天通过官方微博公开了“哔哩哔哩年度动画榜单”。根据观众们的票选，《进击的巨人》最终季获评2020年B站年度动画。
# 财经
### [交通运输部：统筹做好石家庄市应急物资中转运输](https://finance.sina.com.cn/china/2021-01-16/doc-ikftssan7115081.shtml)
> 概要: 【交通运输部：统筹做好石家庄市应急物资中转运输】交通运输部发布《关于统筹做好石家庄市应急物资中转运输有关工作的通知》，要求确保进出石家庄市各类应急物资和生产生活...
### [辽宁大连：11个封闭管控社区将于17日0时解封](https://finance.sina.com.cn/china/2021-01-16/doc-ikftpnnx8099558.shtml)
> 概要: 原标题：辽宁大连：11个封闭管控社区将于17日0时解封 来源：央视新闻客户端 原标题： 2021年1月16日晚，大连市统筹推进新冠肺炎疫情防控和经济社会发展工作总指挥部发布...
### [宁德时代董事长曾毓群：未来5年锂电产业市场将进入TWh时代](https://finance.sina.com.cn/stock/relnews/cn/2021-01-16/doc-ikftpnnx8096081.shtml)
> 概要: 原标题：宁德时代董事长曾毓群：未来5年锂电产业市场将进入TWh时代 来源：中国证券报·中证网 中证网讯（记者 崔小粟）1月16日，宁德时代新能源科技股份有限公司创始人...
### [国资委：强化企业创新主体地位 进一步加大科技创新投入](https://finance.sina.com.cn/roll/2021-01-16/doc-ikftssan7112720.shtml)
> 概要: 原标题：国资委：强化企业创新主体地位 进一步加大科技创新投入 来源：证券时报·e公司 e公司讯，1月15日，国资委党委召开扩大会议。
### [发改委党组：科学精准实施宏观政策 确保“十四五”开好局起好步](https://finance.sina.com.cn/china/2021-01-16/doc-ikftpnnx8091572.shtml)
> 概要: 来源：求是网 科学精准实施宏观政策 确保“十四五”开好局起好步 中共国家发展和改革委员会党组 2020年8月，习近平总书记在主持召开经济社会领域专家座谈会时指出：“‘十四五’...
### [想买也买不到？“顶流”基金经理谢治宇、张坤、周蔚文都限购！](https://finance.sina.com.cn/wm/2021-01-16/doc-ikftpnnx8096265.shtml)
> 概要: 原标题：想买也买不到？“顶流”基金经理谢治宇、张坤、周蔚文都限购！ 1月15日，兴证全球基金公告，由谢治宇管理的兴全合润再次宣布“限购”...
# 科技
### [曝华为新款 MateBook 13/14/X Pro 明天发布，配置一览](https://www.ithome.com/0/530/454.htm)
> 概要: IT之家 1 月 16 日消息 根据 @看山的叔叔的消息，华为将在明天正式发布新配置的 MateBook 13/14/X Pro 等产品。新款 MateBook 13/14 2021搭载英特尔 11 ......
### [京东 21.8 元 / 支，冷酸灵极地白泵式牙膏 130g×3 支 39.9 元（猫超次日达）](https://lapin.ithome.com/html/digi/530451.htm)
> 概要: 天猫超市双重护敏，冷酸灵极地白泵式牙膏130g×3支报价64.9元，下单立减15元，限时限量10元券，实付39.9元包邮，领券并购买。▼ 京东自营同款秒杀价21.8元/支：京东冷酸灵极地白泵式牙膏 1......
### [Limiting Private API Availability in Chromium](https://blog.chromium.org/2021/01/limiting-private-api-availability-in.html)
> 概要: Limiting Private API Availability in Chromium
### [Computer System Engineering (MIT OCW)](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-033-computer-system-engineering-spring-2018/)
> 概要: Computer System Engineering (MIT OCW)
### [JS内存泄漏与垃圾回收机制](https://www.tuicool.com/articles/jiyEvmY)
> 概要: 由于字符串、对象和数组没有固定大小，所有当他们的大小已知时，才能对他们进行动态的存储分配。JavaScript程序每次创建字符串、数组或对象时，解释器都必须分配内存来存储那个实体。只要像这样动态地分配......
### [vue响应式原理](https://www.tuicool.com/articles/RbmM73j)
> 概要: Vue 的响应式原理是核心是通过 ES5 的保护对象的Object.defindeProperty中的访问器属性中的get和 set 方法，data 中声明的属性都被添加了访问器属性，当读取 data......
# 小说
### [仙路争锋](http://book.zongheng.com/book/311835.html)
> 作者：缘分0

> 标签：武侠仙侠

> 简介：修仙如渡河，万马千军竟争帆。因此当你选择了这条路时，你就已经踏上了永不停歇的人生竞赛。要想赢得胜利，需要的不仅是勇往直前的勇气，更要面对无数的风刀霜剑与腥风血雨。争夺，在踏上仙途之前，就已开始！搜索“缘分0”，可加公众号，欢迎关注。

> 章节末：新书已发

> 状态：完本
# 游戏
### [2月23日推出《女神异闻录5S》欧美版预购开启](https://www.3dmgame.com/news/202101/3806420.html)
> 概要: ATLUS欧美官推日前发布消息，《女神异闻录5S》欧美版预购现已开启，游戏将于2月23日推出，登陆NS/PS4/PC（Steam）。其中《女神异闻录5S》（全称：《女神异闻录5 乱战：魅影攻手》）St......
### [粉丝自制终结者主题《Tech-Com: 2029》新画面曝光](https://www.3dmgame.com/news/202101/3806419.html)
> 概要: 早在2019年11月DSOGaming曾介绍过了一款新的《终结者》粉丝自制游戏《Tech-Com: 2029》。现在其开发团队分享了一个新的游戏视频。这段视频可以让我们大致了解了它的游戏机制/片段。《......
### [英特尔新CEO：CPU必须比“生活方式公司”苹果更好](https://www.3dmgame.com/news/202101/3806464.html)
> 概要: 英特尔新任首席执行官帕特·基辛格（Pat Gelsinger）在近日一次员工会议上表示，英特尔在芯片方便必须提供比苹果公司更好的产品，并把苹果公司称为一家生活方式公司。基辛格的原话是这么说的：“我们必......
### [《潜行者2》开发进度更新：进展顺利，效果出色](https://www.3dmgame.com/news/202101/3806441.html)
> 概要: 在2020年12月正式公布游戏预告短片之后，GSC Game World的公共关系专家Zakhar Bocharov今日又发布了一则开发进度更新情报，介绍了《潜行者2》的开发进展。全文如下：想象一下......
### [索尼更新了他们的CES预告片 很多游戏发售日被移除](https://www.3dmgame.com/news/202101/3806463.html)
> 概要: 索尼更新了他们的CES 2021视频，移除了多个PS5游戏的发售窗口，包括SE动作新游《Project Athia》。更新后的视频，只提到的游戏是《瑞奇与叮当：分离》和《地平线：禁忌西部》，2021年......
# 论文
### [Deep Reinforcement Learning for Joint Spectrum and Power Allocation in Cellular Networks](https://paperswithcode.com/paper/deep-reinforcement-learning-for-joint-1)
> 日期：19 Dec 2020

> 标签：None

> 代码：https://github.com/sinannasir/Spectrum-Power-Allocation

> 描述：A wireless network operator typically divides the radio spectrum it possesses into a number of subbands. In a cellular network those subbands are then reused in many cells.
### [Ultrasound-Guided Robotic Navigation with Deep Reinforcement Learning](https://paperswithcode.com/paper/ultrasound-guided-robotic-navigation-with)
> 日期：30 Mar 2020

> 标签：None

> 代码：https://github.com/hhase/spinal-navigation-rl

> 描述：In this paper we introduce the first reinforcement learning (RL) based robotic navigation method which utilizes ultrasound (US) images as an input. Our approach combines state-of-the-art RL techniques, specifically deep Q-networks (DQN) with memory buffers and a binary classifier for deciding when to terminate the task.
