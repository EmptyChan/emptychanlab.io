---
title: 2021-05-17-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.Alesund_EN-CN9406197372_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-17 21:56:02
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.Alesund_EN-CN9406197372_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Edge 浏览器扩展了 Android 平台上的翻译功能](https://www.oschina.net/news/141783/edge-improved-transelator)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>继截图功能后，Microsoft 继续为 Edge 浏览器改进其 Android 平台上的翻译功能。此前，该功能只有一个切换键，可以打开......
### [新版本来了! Milvus v1.1 发布！](https://www.oschina.net/news/141855/milvus-1-1-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>在 Milvus 1.0 版本发布后的 2 个月，2021 年 5 月 7 日，Milvus 正式发布了 1.1 版本！Milvus 1......
### [Bodhi Linux 6.0.0 正式发布，基于 Ubuntu 的桌面发行版](https://www.oschina.net/news/141785/bodhi-linux-6-0-0-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>有“菩提 Linux”之称的 Bodhi Linux发布了 6.0 版本。Bodhi Linux 6.0 基于 Ubuntu 20.04......
### [Hunt Framework 3.4.4 发布，D 语言 Web 服务框架！](https://www.oschina.net/news/141896/hunt-framework-3-4-4-released)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Hunt Framework 是一个全栈 D语言 Web 服务框架，框架完全使用 DLang 编写，从底层的 io 设计到协议解析再到框......
### [世界顶级黑客，都有哪些神操作？](https://segmentfault.com/a/1190000040011934?utm_source=sf-homepage)
> 概要: 黑客是大家经常听到的一个词。但你真的知道它的意思吗？黑客源自英文「hacker」，原指热心于计算机技术、水平高超的电脑专家，尤其是程序设计人员。互联网、Unix、Linux 都是黑客智慧的结晶。有人说......
### [阿里奔向“一万亿”](https://www.huxiu.com/article/428270.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年5月13日，阿里巴巴集团（NYSE：BABA；港交所：9988）发布截至2021年3月31日第四季度（2021财年Q4）及财年(2021财年）业绩。202......
### [You Can’t Sacrifice Partition Tolerance (2010)](https://codahale.com/you-cant-sacrifice-partition-tolerance/)
> 概要: You Can’t Sacrifice Partition Tolerance (2010)
### [渡边美优纪花村想太被曝恋情 两人前后承认交往](https://ent.sina.com.cn/s/j/2021-05-17/doc-ikmxzfmm2872548.shtml)
> 概要: 新浪娱乐讯 15日，《文春》发稿爆料渡边美优纪正在与Da-iCE主唱花村想太交往中，并且已经同居。　　知情人士透露，两人已经交往约两年，已经考虑结婚。　　《文春》也采访到花村想太本人，他承认了双方的恋......
### [《转生变成史莱姆》第2季新预告 7.6日开播第2部](https://www.3dmgame.com/news/202105/3814568.html)
> 概要: 人气转生系奇幻TV动画《关于我转生变成史莱姆这档事》 第2季后半部分将于2021年7月6日开播，官方公开了最新预告，一起先睹为快。《关于我转生变成史莱姆这档事》的故事并不复杂，讲述了一个普通的37岁上......
### [《速度与激情9》终极预告发布 太空飞车亮相！](https://www.3dmgame.com/news/202105/3814569.html)
> 概要: 今日(5月17日)动作大片《速度与激情9》终极预告发布，赛弗和雅各布两大反派搞事，速激家族不得不出面迎战，误入雷区， 无数车辆被炸毁，罗曼泰吉这一次真的要飙车上天了？终极预告：《速度与激情9》由范·迪......
### [组图：董又霖深夜与友人聚会 结束后开车与姐姐一同返家](http://slide.ent.sina.com.cn/star/slide_4_86512_356715.html)
> 概要: 组图：董又霖深夜与友人聚会 结束后开车与姐姐一同返家
### [刘丽千澄清妈妈捐款去向 会整理好一切费用清单](https://ent.sina.com.cn/s/m/2021-05-17/doc-ikmyaawc5751613.shtml)
> 概要: 新浪娱乐讯  5月17日，刘丽千更新微博发长文澄清了去年粉丝给妈妈捐款的用途。刘丽千表示非常感谢大家的帮助，救了妈妈一命，如今捐款还没有用完，还有15万左右。本来是要在妈妈的微博和大家更新治疗进度和开......
### [「女神的咖啡厅」第一卷漫画宣传PV公开](http://acg.178.com//202105/415217670628.html)
> 概要: 由漫画家瀬尾公治创作的漫画作品「女神的咖啡厅」于近期公开了第一卷漫画的发售PV，旁白由知名声优鬼头明里担任，漫画正在发售中。「女神的咖啡厅」第一卷漫画宣传PV......
### [「周刊少年JUMP」24号封面及杂志内彩页公开](http://acg.178.com//202105/415218093155.html)
> 概要: 漫画杂志「周刊少年JUMP」24号封面及杂志内彩页正式公开，包含漫画「石纪元」、「No one knowS」、「高中生家族」和「破坏神马克酱」。「周刊少年JUMP」是由集英社发行的漫画杂志，在每周一发......
### [漫画「糖如雨下」作者公开第四话连载宣传绘](http://acg.178.com//202105/415218263217.html)
> 概要: 漫画「糖如雨下」作者みたらし三大公开了本作第四话的连载宣传绘。「糖如雨下」是たけぐし一本原作，みたらし三大作画的漫画作品，连载于集英社旗下漫画杂志「周刊少年JUMP」......
### [原创动画「旅はに」宣布项目启动](http://acg.178.com//202105/415218949212.html)
> 概要: 以旅途为主题的原创动画「旅はに」在社交平台开通了官方账号，宣布项目启动，并发布了一张人设图。不过，更多的详细情报还需等待日后公开。STAFF監督：坂本一也（ライデンフィルム京都スタジオ）角色设计：満田......
### [起底A股坐庄黑产业链](https://www.huxiu.com/article/428348.html)
> 概要: 本文来自微信公众号：界面新闻（ID：wowjiemian），作者：慕泽，编辑：宋烨珺，头图来自：视觉中国随着叶飞爆料上市公司“坐庄内幕”一事持续升级，目前已经有10余家上市公司陆续被卷入其中。5月16......
### [媒体调查希望好莱坞真人化的动画和漫画作品名单](https://news.dmzj1.com/article/70902.html)
> 概要: 运营视频检索服务的viviane社，公开了对1000名10只69岁的人进行的“希望好莱坞真人化的动画和漫画”调查。根据这次调查结果，“无”以456票，接近50%的得票率，获得第一。
### [TV动画《关于我转生成为史莱姆的那件事2》新PV](https://news.dmzj1.com/article/70903.html)
> 概要: 根据川上泰树、伏濑原作制作的TV动画《关于我转生变成史莱姆这档事》第二部公开了最新PV。在这次的PV中，可以看到大战后的休息时间以及准备和魔王克雷曼战斗的场景。
### [漫画《间谍过家家》7卷累计突破1000万部！](https://news.dmzj1.com/article/70904.html)
> 概要: 远藤达哉创作的漫画《间谍过家家》宣布了包括即将于6月4日发售的7卷和电子版在内，系列累计突破了1000万部。另外第八卷预计将于11月4日发售。
### [《特种部队：蛇眼起源》曝预告 忍者们对砍 飙车枪战](https://www.3dmgame.com/news/202105/3814590.html)
> 概要: 今日（5月17日），《特种部队》衍生片《特种部队：蛇眼起源》公布首支预告，忍者们相互对砍，飙车枪战，场面劲爆。本片将于今年7月23日在北美上映。《特种部队：蛇眼起源》首曝预告：《特种部队：蛇眼起源》由......
### [Working over 55 hours a week associated with higher risk of stroke, heart attack](https://www.bbc.co.uk/news/business-57139434)
> 概要: Working over 55 hours a week associated with higher risk of stroke, heart attack
### [刘志轩晒休假图：敢勇追梦，热血无畏。](https://bbs.hupu.com/42948159.html)
> 概要: 刘志轩晒休假图：敢勇追梦，热血无畏。
### [游点意思：颠覆三观的三国漫画 烧脑计策让人上头](https://acg.gamersky.com/news/202105/1388830.shtml)
> 概要: 纵横幻想与史实的三国时代漫画《火凤燎原》，于乱世中牵引各路豪杰，由此展现出群英纷争、燃烧中原的大势。
### [卧底小红书“种草笔记”产业链：0粉也能接广告](https://www.huxiu.com/article/428429.html)
> 概要: 本文来自微信公众号：运营研究社（ID：U_quan），作者：套路编辑部，题图来自：视觉中国上周，我的室友面试了一家广告公司的“文案策划”。结果，面试回来后她告诉我，所谓的“文案”，就是在小红书上，帮品......
### [日媒称庵野秀明将重制国民级动画电影 Khara完全否定](https://news.dmzj1.com/article/70907.html)
> 概要: 近日，有日本媒体称庵野秀明将要重制某国民级动画电影。对此，Khara官方也发表了回应。Khara表示完全没有这件事情。
### [AI Mid 2021: Self Driving Car Meets Reality](https://blog.piekniewski.info/2021/05/12/ai-mid-2021/)
> 概要: AI Mid 2021: Self Driving Car Meets Reality
### [腾讯宣布开源 RoP：Apache Pulsar 支持原生 RocketMQ 协议](https://www.tuicool.com/articles/EfyYziv)
> 概要: 作者 | 冉小龙，刘昱RocketMQ 用户可以无缝迁移到 Apache Pulsar 了。自此，Apache Pulsar 补齐了兼容主流消息队列协议的能力。我们很高兴地宣布腾讯云中间件开源 RoP......
### [刀尖舔血的币圈合约有多疯狂？](https://www.tuicool.com/articles/iANz2qm)
> 概要: 本文来自微信公众号：北京商报（ID：BBT_JLHD），作者：岳品瑜、刘四红，编辑：张兰，题图来自：视觉中国“玩的就是心跳！”这是不少币圈合约玩家的口头禅。5月17日中午12时，比特币再现暴跌，日内跌......
### [送抖音200亿「嫁妆」，京东亏不亏？](https://www.tuicool.com/articles/Jbe6j2U)
> 概要: 又一年618即将开始，今年618打响第一枪的是一则京东与抖音电商合作升级的消息。据媒体报道，此次合作，京东将开设官方抖音蓝V店，未来全量京东商品将接入该抖音小店以及全抖音平台小店，用户通过抖音购买京东......
### [《高达：闪光的哈萨维》再次宣布延期 上映时间未知](https://acg.gamersky.com/news/202105/1388877.shtml)
> 概要: 原定于5月21日上映的《机动战士高达：闪光的哈萨维》再次宣布延期，新的上映时间未知。
### [4 个Vue 路由过渡动效](https://www.tuicool.com/articles/FZFRRnN)
> 概要: // 每日前端夜话 第535 篇// 正文共：2000 字// 预计阅读时间：8 分钟目录在 Vue 程序中添加路由给路由添加过渡将过渡移到每个组件中用 v-bind 进行动态过渡#1 – 渐变过渡#......
### [《风起洛阳》即将来袭，王一博眼神戏十分在线，宋茜男装照好俊俏](https://new.qq.com/omn/20210517/20210517A0BS7W00.html)
> 概要: 哈喽大家好啊，目前有很多新剧都在待播的状态中，非常备受大众的期待，而最近倒是也有一部古装新剧同样还在待播中，引起了不少网友们的注意，这部剧就是《风起洛阳》了，想必大家对该剧也是有一定的了解了，而且这部......
### [掌阅 iReader 新品发布会官宣：5 月 25 日重塑想象](https://www.ithome.com/0/551/916.htm)
> 概要: IT之家5 月 17 日消息 掌阅 iReader 官方宣布，将于5 月 25 日召开 2021 新品发布会，标语为“重塑想象”。IT之家了解到，掌阅的上一款新品还是去年 10 月份的 iReader......
### [组图：闺蜜拍照指南get！孟美岐宋妍霏置身花海拍大片](http://slide.ent.sina.com.cn/star/slide_4_704_356730.html)
> 概要: 组图：闺蜜拍照指南get！孟美岐宋妍霏置身花海拍大片
### [An Erlangen Programme to Establish the Geometric Foundations of Deep Learning](https://syncedreview.com/2021/05/05/deepmind-podracer-tpu-based-rl-frameworks-deliver-exceptional-performance-at-low-cost-12/)
> 概要: An Erlangen Programme to Establish the Geometric Foundations of Deep Learning
### [那一年，张小斐26岁，贾玲30岁……](https://new.qq.com/omn/20210517/20210517A0D67100.html)
> 概要: 作者 | 东野聪明来源 | 最人物ID | iiirenwu无论从什么角度看，在娱乐圈里，张小斐的故事都无疑是一个孤本：有人用“爆红”来定义张小斐的2021，有人用“透明”来形容她的过往，但这些词都无......
### [海盗船展示DDR5内存优势 频率轻松达6400MHz](https://www.3dmgame.com/news/202105/3814611.html)
> 概要: 随着年底Intel推出的Alder Lake平台将支持DDR5内存，各个内存厂家都开始展示自家的新一代内存产品了，其实在年初甚至更早的时候上游DRAM厂家就开始出货DDR5内存颗粒，所以内存厂们都有比......
### [微信 iOS 8.0.6 正式版发布：状态新增时效提醒、朋友圈可转发视频号](https://www.ithome.com/0/551/922.htm)
> 概要: IT之家5 月 17 日消息 微信今天上午在 AppStore 悄悄上线了最新的 8.0.6 正式版，虽然官方并没有透露任何新功能，但IT之家（微信公号：IT之家）还是尽力帮大家找出了新版本所有的变化......
### [国漫《灵笼》将改编为真人影视剧 南派三叔任编剧](https://ent.sina.com.cn/v/m/2021-05-17/doc-ikmxzfmm3006956.shtml)
> 概要: 新浪娱乐讯 17日，国漫《灵笼》宣布将改编为真人影视剧，南派三叔担任编剧。随后，南派三叔在微博发文：“真人版《灵笼》影视剧开拍后，我会和演员们一起举铁！”网友在评论区调侃：“说话算话哦，实在不行每天在......
### [整合华纳资源：AT&T 宣布合并娱乐业务，打造全球顶级娱乐公司](https://www.ithome.com/0/551/924.htm)
> 概要: 北京时间 5 月 17 日晚间消息，据报道，美国电信运营商 AT&T 和探索传播公司（Discovery）今日联合宣布，将把华纳媒体公司（WarnerMedia）的优质娱乐、体育和新闻资产，与探索传播......
### [火币全球站恢复ALGO充提业务](https://www.btc126.com//view/165818.html)
> 概要: 据官方公告，火币全球站现已恢复ALGO（Algorand）的充币和提币业务......
### [3000万男性打光棍？官方回应：我国性别比已在逐步改善](https://finance.sina.com.cn/jjxw/2021-05-17/doc-ikmxzfmm3015203.shtml)
> 概要: 原标题：3000万男性打光棍？官方回应：我国性别比已在逐步改善 备受关注的第七次全国人口普查数据近日公布。数据显示，我国男性比女性人口多出3000余万...
### [他们在长春打成一片](https://finance.sina.com.cn/china/dfjj/2021-05-17/doc-ikmxzfmm3015763.shtml)
> 概要: 他们在长春打成一片 来源：壹地产 章子姨 昨天中午，长春龙湖舜山府项目大约二十位渠道身着便装，走进了旁边不远融创东方宸院项目的售楼处。
### [数据：YFI交易所7日平均流入量创历史新高](https://www.btc126.com//view/165820.html)
> 概要: Glassnode数据显示，YFI交易所流入量（7日移动平均线）创历史新高（ATH），达到2068344.74美元......
### [安徽、辽宁疫情发生至今，存在哪些疑点？带来哪些提示？](https://finance.sina.com.cn/roll/2021-05-17/doc-ikmxzfmm3015625.shtml)
> 概要: （抗击新冠肺炎）安徽、辽宁疫情发生至今，存在哪些疑点？带来哪些提示？ 中新网北京5月17日电 题：安徽、辽宁疫情发生至今，存在哪些疑点？带来哪些提示？
### [解读4月国民经济：仍持续恢复，生产稳中有升，需求继续扩大](https://finance.sina.com.cn/jjxw/2021-05-17/doc-ikmyaawc5878454.shtml)
> 概要: 原标题：解读4月国民经济：仍持续恢复，生产稳中有升，需求继续扩大 根据国家统计局5月17日公布的数据，4月份，生产需求持续增长，就业物价总体稳定，新兴动能培育壮大...
### [亚马逊《新世界》有内购 开发商指出不影响游戏平衡](https://www.3dmgame.com/news/202105/3814613.html)
> 概要: 在多次跳票后，亚马逊游戏工作室的《新世界》将于8月31日发售，登陆PC。该游戏目前正在进行Alpha测试，目前已经有玩家对其游戏中的商店表示担忧，特别是涉及到“花钱就能赢”的元素。在Twitter上，......
### [流言板共和报：意甲面临破产风险，20队申请所有球员放弃2个月薪水](https://bbs.hupu.com/42955240.html)
> 概要: 虎扑05月17日讯 《共和报》的今日消息，意甲目前正面临着严重的破产风险，这就是为什么多支俱乐部向足协申请延后薪水支付截止日，意大利足协主席格拉维纳需要作出决定：是推迟截止日期，还是冒5、6支球队下赛
### [成都前4月线上消费大数据：食品保健、在线餐饮成消费热门](https://finance.sina.com.cn/china/gncj/2021-05-17/doc-ikmxzfmm3017220.shtml)
> 概要: 成都前4月线上消费大数据：食品保健、在线餐饮成消费热门 来源：成都发布 前4月，你是否又忍不住花“重金”为自己购入了不少中意的服装饰品？
### [马自达：2030 年生产的纯电动汽车比例目标由 5% 提高到 25%](https://www.ithome.com/0/551/937.htm)
> 概要: IT之家5 月 17 日消息 据日经中文网报道，马自达近日宣布，在 2030 年生产的车辆中，纯电动汽车（EV）的占比将提高到 25％左右。IT之家了解到，马自达曾在 2018 年宣布到 2030 年......
### [PSG vs RNG精彩回顾：兰博反蹲成功，小明泰坦送出一血](https://bbs.hupu.com/42955302.html)
> 概要: 来源：  虎扑
### [辽宁本土+4 营口中风险地区人员一律不许离营](https://finance.sina.com.cn/wm/2021-05-17/doc-ikmyaawc5879875.shtml)
> 概要: 17日0时至19时，辽宁省新增4例本土新冠肺炎确诊病例，其中沈阳市2例，营口市2例（其中1例为无症状感染者转归）；新增3例境外输入无症状感染者，均为沈阳市报告。
### [大胆猜一下，漫威《奇异博士2》里最大的彩蛋惊喜会不会是他？](https://new.qq.com/omn/20210517/20210517A0EHAI00.html)
> 概要: 如果说在后续漫威电影宇宙可能会上线的诸多“新英雄”中，哪一位是无数漫威粉丝都想要在大荧幕上看到的，那么，排名前几的名单中必然会有一个——恶灵骑士！            其实对于“恶灵骑士”很多影迷应......
### [PSG vs RNG精彩回顾：莉莉娅入侵野区被瞬秒](https://bbs.hupu.com/42955481.html)
> 概要: 来源：  虎扑
### [一线｜麦迪娜怀二胎？姜潮工作人员未否认，曾约定二胎跟女方姓](https://new.qq.com/omn/20210517/20210517A0ENE200.html)
> 概要: 腾讯娱乐《一线》 作者：三禾、沐橙5月17日，有网友拍到姜潮麦迪娜夫妇现身某医院，两人全程手牵手，十分甜蜜。麦迪娜脚踩平底鞋，穿着一件超级肥大的黑色外套，但仍遮不住明显挺起来的肚子，看起来似乎是怀了二......
### [Coinbase下跌5.3%，已跌破发行价250美元](https://www.btc126.com//view/165828.html)
> 概要: 行情显示，加密货币交易所Coinbase跌5.3%，报244.6美元/股，跌破发行价250美元......
### [美股三大指数集体低开，道指跌0.23%](https://www.btc126.com//view/165829.html)
> 概要: 行情显示，美股三大指数集体低开，道指跌0.23%，纳指跌0.46%，标普500指数跌0.24%，区块链板块全线下挫 ......
# 小说
### [赎罪事务所](http://book.zongheng.com/book/909604.html)
> 作者：上官天越

> 标签：悬疑灵异

> 简介：想赚钱吗？想一夜暴富吗？如今在您面前就有这样一个机会。进入地狱替寿命将尽的人遭受劫难，归来之时必有重金酬谢。您心动了吗？

> 章节末：第三十章：完结撒花

> 状态：完本
# 论文
### [FNNP: Fast Neural Network Pruning Using Adaptive Batch Normalization](https://paperswithcode.com/paper/fnnp-fast-neural-network-pruning-using)
> 日期：ICLR 2020

> 标签：NETWORK PRUNING

> 代码：https://github.com/anonymous47823493/FNNP

> 描述：Finding out the computational redundant part of a trained Deep Neural Network (DNN) is the key question that pruning algorithms target on. Many algorithms try to predict model performance of the pruned sub-nets by introducing various evaluation methods.
### [Reinforcement Learning with Prototypical Representations](https://paperswithcode.com/paper/reinforcement-learning-with-prototypical)
> 日期：22 Feb 2021

> 标签：CONTINUOUS CONTROL

> 代码：https://github.com/denisyarats/proto

> 描述：Learning effective representations in image-based environments is crucial for sample efficient Reinforcement Learning (RL). Unfortunately, in RL, representation learning is confounded with the exploratory experience of the agent -- learning a useful representation requires diverse data, while effective exploration is only possible with coherent representations.
