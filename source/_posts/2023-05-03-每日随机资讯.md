---
title: 2023-05-03-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ThreeWildebeest_ZH-CN0175563521_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-05-03 23:18:53
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ThreeWildebeest_ZH-CN0175563521_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [年轻人五一旅游，开始“交换住宿”](https://www.woshipm.com/it/5818050.html)
> 概要: 这个“五一”小长假，许多人都感受到了出游的火热，而在近段时间持续升温的旅游热度里，不一样的旅游方式也在社交平台上走红了起来，比如之前火上热搜的“大学生特种兵式旅游”，又比如现在流行着的“交换旅游”。那......
### [零经验领域，8天涨粉1750，赞藏数超3万 | 我的小红书运营复盘](https://www.woshipm.com/operate/5806458.html)
> 概要: 如果你在小红书运营方面零经验，却想快速起号、实现快速集赞与涨粉，你可以采取怎样的运营策略？在本篇文章里，作者便从账号搭建、账号内容、笔记创作等方面，拆解了自己零经验实现快速涨粉的小红书账号运营经验。一......
### [Siri 们的“愚笨”终于有救了？](https://www.woshipm.com/it/5817929.html)
> 概要: 在ChatGPT出现之后，不仅人类感受到了威胁，部分互联网产品可能也将“备受压力”、经受“不进则退”的困扰，比如在过去这些年里长进似乎不大的语音助手们。那么，语音助手们的发展面临着哪些亟需解决的问题......
### [一季度，山东、江西怎么了？](https://www.huxiu.com/article/1407621.html)
> 概要: 本文来自微信公众号：秦朔朋友圈（ID：qspyq2015），作者：土哥涅夫，原文标题：《一季度经济解读：投资助力逆袭，债务拖累发展》最近，一季度经济数据陆续公布。这是疫情管控放开后的首个季度报，所以数......
### [好莱坞编剧开始罢工 多档脱口秀节目暂时停播](https://ent.sina.com.cn/s/u/2023-05-03/doc-imysncsx7377007.shtml)
> 概要: 新浪娱乐讯 据外媒，随着美国编剧工会（WGA）宣布编剧罢工开始，编剧占据重要地位的脱口秀节目率先显露影响：深夜秀四档台柱节目ABC《鸡毛秀》、CBS《深夜秀》、NBC《今夜秀》《深夜秀》全部暂时停播，......
### [云南一地深夜突发地震，游客从酒店下楼避险！大理震感明显，网友称“有点不敢睡”](https://finance.sina.com.cn/china/gncj/2023-05-03/doc-imysncsx7386216.shtml)
> 概要: 中国地震台网正式测定：5月2日23时27分在云南保山市隆阳区（北纬25.35度，东经99.28度）发生5.2级地震，震源深度10千米。5月2日23时38分，保山市隆阳区发生4.4级地震...
### [VC盯着上市公司搞分拆呢](https://36kr.com/p/2238704948473733)
> 概要: VC盯着上市公司搞分拆呢-36氪
### [特斯拉中国涨价2000元 定价策略转向动态模式](https://www.yicai.com/news/101746081.html)
> 概要: 今年以来，特斯拉的价格调整更为频繁。在美国，特斯拉已经进行了七次调价。其定价策略也已经完全由传统汽车行业的固定定价模式转变为更接近航空酒店业或打车平台的实时定价模式。
### [《超级棒球4》Steam页面上线 6月3日正式发售](https://www.3dmgame.com/news/202305/3868419.html)
> 概要: 今日（5月3日），EA发行Metalhead Software开发的棒球模拟游戏《超级棒球4》Steam页面上线，游戏暂不支持中文，不支持键鼠，预计于6月3日发售，感兴趣的玩家可以点击此处进入商店页面......
### [ASTRO成员JINJIN发文悼念已故队友文彬](https://ent.sina.com.cn/s/j/2023-05-03/doc-imysncsw5254337.shtml)
> 概要: 新浪娱乐讯 3日，ASTRO成员JINJIN朴真佑发文悼念已故文彬，他在个人社交账号上写道“彬啊，过得好吗？ 可能是因为相册里有你，现在还是难以置信”“想念你开玩笑的样子，想起你的笑脸，就忍不住笑了，......
### [红杉高瓴王石，同时看中了无锡](https://36kr.com/p/2241478740242309)
> 概要: 红杉高瓴王石，同时看中了无锡-36氪
### [中国公司全球化周报｜国办发布关于推动外贸稳规模优结构的意见；服务商昕诺服务募资近千万美元](https://36kr.com/p/2240066724704132)
> 概要: 中国公司全球化周报｜国办发布关于推动外贸稳规模优结构的意见；服务商昕诺服务募资近千万美元-36氪
### [被堵在包厢，刘主任：我错了！](https://finance.sina.com.cn/jjxw/2023-05-03/doc-imysnqht7268914.shtml)
> 概要: 据中央纪委国家监委网站，4月30日，临近中午，以广西壮族自治区桂林市象山区纪委监委党风政风监督室主任肖保国为组长的督查组乘车前往某农庄检查。
### [突发！直升机坠落西安白鹿仓景区，机身明显受损！梅西被停赛停训停薪，啥情况？港股、A50走低…](https://finance.sina.com.cn/wm/2023-05-03/doc-imysnqhx3899016.shtml)
> 概要: 继隔夜欧美股市大幅走低后，港股今日大幅低开，恒生指数盘初一度重挫近2%，恒生科技指数一度跌近3%。截至午间收盘，恒生指数跌1.75%，恒生科技指数跌2.38%。
### [当憋坏了的东亚人决定集体发疯](https://www.huxiu.com/article/1370225.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 黄瓜汽水编辑 、题图 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。全世界精神最稳......
### [《街霸6》蛛俐角色介绍视频 6月2日发售](https://www.3dmgame.com/news/202305/3868428.html)
> 概要: 今日（5月3日），Capcom官方公布《街头霸王6》蛛俐角色介绍影片，喜欢摩托车、蜘蛛。讨厌扫兴的家伙、规矩。《街头霸王6》将于6月2日发售，登陆PS4、PS5、XSX|S和PC，支持中文。介绍预告：......
### [蕾哈娜破超级碗中场秀收视纪录 超1.2亿人收看](https://ent.sina.com.cn/y/youmei/2023-05-03/doc-imysnqhx3912370.shtml)
> 概要: 新浪娱乐讯 据媒体报道，尼尔森修正了超级碗中场秀收视人数——1.21017亿，正式成为有史以来收视率最高的中场秀表演。(责编：小5)......
### [美国取消疫苗入境需求，夏季航空需求会井喷吗](https://www.yicai.com/news/101746199.html)
> 概要: 美国四大航空公司都预计，今年夏季航空需求将十分强劲。
### [动荡的楼市与开发商的抉择](https://www.huxiu.com/article/1411488.html)
> 概要: 本文来自微信公众号：经济观察报 （ID：eeo-com-cn），作者：田国宝，头图来自：视觉中国下一步该不该拿地？郭栋所在的公司纠结了很久，如果要拿地，什么时候拿？拿地预算是多少？拿地标准是什么？如果......
### [AITO 问界 4 月交付 4585 辆，累计交付 10 万辆](https://www.ithome.com/0/690/365.htm)
> 概要: 感谢IT之家网友肖战割割、Colorful M的线索投递！IT之家5 月 3 日消息，AITO 问界 4 月共交付新车 4585 辆。自 2022 年 3 月开启交付以来，AITO 问界 M5、M7 ......
### [五粮液“五一”表彰大会：37名员工获评首届五粮液劳动先锋](http://www.investorscn.com/2023/05/03/107243/)
> 概要: 五粮液“五一”表彰大会：37名员工获评首届五粮液劳动先锋
### [孤影首战《蛋仔派对》，疯狂乱斗中惨遭连击，惨呐！](http://www.investorscn.com/2023/05/03/107245/)
> 概要: 4月30日晚19点，虎牙知名主播孤影在结束峡谷纷争之后，开启了他美好的《蛋仔派对》之旅。恰逢《蛋仔派对》新模式—疯狂乱斗的火热进行，他决定和水友们一起会一会这个新模式。但孤影忘了最重要的一件事，他是真......
### [接连地震！云南、西藏、四川…一天4地发生地震！有人员受伤，列车晚点！](https://finance.sina.com.cn/stock/zqgd/2023-05-03/doc-imysnuqv3862743.shtml)
> 概要: 继云南保山、西藏阿里地震后，四川多地接连发生地震！ 中国地震台网正式测定：5月3日13时04分在四川宜宾市兴文县（北纬28.10度，东经105.12度）发生4.5级地震...
### [极狐凝聚五大发展势能](http://www.investorscn.com/2023/05/03/107246/)
> 概要: 日前,在北汽蓝谷二季度核心媒体沟通会上,北汽蓝谷副总经理、北汽新能源常务副总经理表示,北汽蓝谷极狐将致力于极狐的商业成功。极狐继续坚定洞察、智慧、极致的品牌精神,极致守护万千家庭,做场景品牌力的先行者......
### [PC处理器暴跌65% AMD苏姿丰：最坏的日子已过去](https://www.3dmgame.com/news/202305/3868440.html)
> 概要: AMD今天凌晨发布了第一季度财报，过去几年一直高歌猛进的业绩也戛然而止，录得2019年来首次下滑。虽然9%的营收减少比友商暴跌36%的情况要好很多。在AMD的四大营收业务中，企业、嵌入式和半定制事业部......
### [贝特瑞一季度营收创同期新高，业务布局打开广阔空间](http://www.investorscn.com/2023/05/03/107247/)
> 概要: 4月28日晚间，贝特瑞（835185.nq）发布了一季报。财报显示，公司第一季度营业收入为67.65亿元，同比增长65.86%，归母净利润为2.93亿元。另外，截至第一季度末，贝特瑞资产负债率降至61......
### [投资腰斩！这些公司却拿到钱了……](https://36kr.com/p/2241550400860032)
> 概要: 投资腰斩！这些公司却拿到钱了……-36氪
### [FydeOS v16.1 更新发布：新界面、虚拟桌面管理、现代「终端」程序](https://www.ithome.com/0/690/376.htm)
> 概要: IT之家5 月 3 日消息，FydeOS 是一款基于 Chromium OS 开源项目的操作系统，无需 Google 服务，可创建本地账号登录，带有完整的桌面版 Chromium 浏览器，并通过容器技......
### [AI教父谷歌离职后发声 AI正变得比人类更聪明](https://www.3dmgame.com/news/202305/3868447.html)
> 概要: IT之家今日（5月3日）消息，人工智能领域的开创者之一，被誉为“AI 教父”的杰弗里・辛顿（Geoffrey Hinton）近日宣布离开他工作了十年的谷歌公司，原因是他对人工智能技术的发展越来越担忧......
### [2023年“五一”假期国内旅游出游合计2.74亿人次 同比增长70.83%](https://finance.sina.com.cn/jjxw/2023-05-03/doc-imysnywt3827258.shtml)
> 概要: 转自：新华社 文化和旅游部5月3日公布 2023年“五一”假期文化和旅游市场情况 全国国内旅游出游合计2.74亿人次，同比增长70.83% 按可比口径恢复至2019年同期的119...
### [索尼 A6700 旗舰 APS-C 相机爆料：2600 万像素 CMOS、4K120p 录制](https://www.ithome.com/0/690/399.htm)
> 概要: 感谢IT之家网友独立摄影师的线索投递！IT之家5 月 3 日消息，Sonyalpharumors 现已放出索尼 A6700 旗舰 APS-C 相机参数信息，并表示这款相机将在 8 月出货。图源 Son......
### [淄博烧烤不涨价的最大底气，是山东恐怖的供应实力](https://www.huxiu.com/article/1411636.html)
> 概要: 本文来自微信公众号：正解局（ID：zhengjieclub），作者：正解局，头图来自：视觉中国这个五一，旅游市场火爆异常。淄博，无疑是最受欢迎的目的地之一。据大众点评数据显示，3月以来，淄博当地“烧烤......
### [盐田港：2023年一季度实现开门红，归母净利润同比大增27.11%](http://www.investorscn.com/2023/05/03/107250/)
> 概要: 4月28日晚，盐田港（000088.SZ）发布了2022年年报和2023年一季报。公告显示，公司2022全年营业总收入7.98亿元，同比增长17.32%；利润总额5.33亿元，同比增长0.25%；经营......
### [《英雄联盟》13.9 版本更新：妮蔻重制，可变为小兵 / 野怪](https://www.ithome.com/0/690/405.htm)
> 概要: IT之家5 月 3 日消息，《英雄联盟》13.9 版本台服将在明日更新。官方表示，由于正在准备本赛季以来最大的版本更新，也就是 13.10 版本，所以本周进行的更新内容会相对较少。本次更新的亮点是新登......
### [专访清华大学教授杨燕绥：资源下沉，突破医养结合十年困境](https://www.yicai.com/news/101746396.html)
> 概要: 医养结合是个体系问题，要将医防融合的疾控体系、全专融合的社区医疗体系，嵌入社区驿站和家庭病床，实现医养结合。
### [《生化变种》11月30日登陆任天堂Switch](https://www.3dmgame.com/news/202305/3868457.html)
> 概要: 发行商THQ Nordic和开发商Experiment 101宣布，《生化变种》将于11月30日登陆任天堂Switch平台。《生化变种》最初于2021年5月发售，登陆了PC（Steam、Epic、GO......
### [“五一”假期消费火爆，下阶段经济工作重点明晰](https://www.yicai.com/news/101746400.html)
> 概要: 机构分析，二季度政策的亮点主要将集中在科技相关产业政策，数字经济、人工智能、先进装备制造等领域都将是未来政策发力的主线。
### [多地实施公积金租购房新政，专家：提升利用率，避免公积金躺平](https://www.yicai.com/news/101746412.html)
> 概要: 新政策在于支持多子女家庭的租房、购房。
# 小说
### [游戏制作：我把全球玩家吓哭！](https://book.zongheng.com/book/1241700.html)
> 作者：妙笔即生花

> 标签：脑洞星球

> 简介：面对玩家们对于他制作的游戏过于恐怖且难度过高的抱怨，被大家亲切的称为“老贼”的洛寒在接受媒体采访时淡定的表示：“这有什么难度吗？我都是一命通关的！”而洛寒的传奇游戏制作经历，要从他淘到了一台二手笔记本电脑，并在上面发现了一个名为“恐怖游戏制作器”的应用开始说起……

> 章节末：第一百零八章 大结局（下）

> 状态：完本
# 论文
### [Sample Dropout: A Simple yet Effective Variance Reduction Technique in Deep Policy Optimization | Papers With Code](https://paperswithcode.com/paper/sample-dropout-a-simple-yet-effective)
> 日期：5 Feb 2023

> 标签：None

> 代码：https://github.com/linzichuan/sdpo

> 描述：Recent success in Deep Reinforcement Learning (DRL) methods has shown that policy optimization with respect to an off-policy distribution via importance sampling is effective for sample reuse. In this paper, we show that the use of importance sampling could introduce high variance in the objective estimate. Specifically, we show in a principled way that the variance of importance sampling estimate grows quadratically with importance ratios and the large ratios could consequently jeopardize the effectiveness of surrogate objective optimization. We then propose a technique called sample dropout to bound the estimation variance by dropping out samples when their ratio deviation is too high. We instantiate this sample dropout technique on representative policy optimization algorithms, including TRPO, PPO, and ESPO, and demonstrate that it consistently boosts the performance of those DRL algorithms on both continuous and discrete action controls, including MuJoCo, DMControl and Atari video games. Our code is open-sourced at \url{https://github.com/LinZichuan/sdpo.git}.
### [AADG: Automatic Augmentation for Domain Generalization on Retinal Image Segmentation | Papers With Code](https://paperswithcode.com/paper/aadg-automatic-augmentation-for-domain)
> 日期：27 Jul 2022

> 标签：None

> 代码：https://github.com/crazorback/aadg

> 描述：Convolutional neural networks have been widely applied to medical image segmentation and have achieved considerable performance. However, the performance may be significantly affected by the domain gap between training data (source domain) and testing data (target domain). To address this issue, we propose a data manipulation based domain generalization method, called Automated Augmentation for Domain Generalization (AADG). Our AADG framework can effectively sample data augmentation policies that generate novel domains and diversify the training set from an appropriate search space. Specifically, we introduce a novel proxy task maximizing the diversity among multiple augmented novel domains as measured by the Sinkhorn distance in a unit sphere space, making automated augmentation tractable. Adversarial training and deep reinforcement learning are employed to efficiently search the objectives. Quantitative and qualitative experiments on 11 publicly-accessible fundus image datasets (four for retinal vessel segmentation, four for optic disc and cup (OD/OC) segmentation and three for retinal lesion segmentation) are comprehensively performed. Two OCTA datasets for retinal vasculature segmentation are further involved to validate cross-modality generalization. Our proposed AADG exhibits state-of-the-art generalization performance and outperforms existing approaches by considerable margins on retinal vessel, OD/OC and lesion segmentation tasks. The learned policies are empirically validated to be model-agnostic and can transfer well to other models. The source code is available at https://github.com/CRazorback/AADG.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
