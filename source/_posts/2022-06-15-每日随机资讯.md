---
title: 2022-06-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ClingmansDome_ZH-CN0900594339_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-06-15 23:07:46
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ClingmansDome_ZH-CN0900594339_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [如何分析B端产品的市场状况？](http://www.woshipm.com/pmd/4312350.html)
> 概要: 编辑导语：做B端领域的工作前，都有必要去熟悉下市场情况，了解市场需求。那么怎么才能分析好B端产品的市场状况呢？本文作者从行业、客户群体、竞争对手进行了分析，做B端的童鞋一起来看看吧。无论做哪行，是不是......
### [供应链金融系统性风险治理——论资产穿透式行权的重要性](http://www.woshipm.com/it/5486689.html)
> 概要: 编辑导语：资产穿透式行权对于供应链金融系统性风险治理十分重要，而供应链金融自身也存在一些问题。本篇文章作者讲述了供应链中小微企业发展存在的问题以及解决方案，一起来学习一下，希望对你有帮助。一、引言20......
### [这届年轻人竟然开始盘串了？](http://www.woshipm.com/it/5487545.html)
> 概要: 编辑导语：随着互联网的发展，越来越多新奇的玩意儿被年轻人们喜爱和推崇，如今年轻人也迷上文玩盘串等事物。本篇文章作者分享了当下年轻人喜爱的文玩行业的发展状况以及讲述了年轻人的文玩世界等，感兴趣的一起来看......
### [慧示短焦投影仪设计策划分享](https://www.zcool.com.cn/work/ZNjAzODExNDA=.html)
> 概要: 好久没有更新，离职后去了新公司上了一个月班我就主动离职了，干脆自己在家完成一些设计任务后考虑工作，这次也是一次新的突破，主要新客户与预算够，给我实现想法的成本。手头上 4个项目同时进行时真的累。一个人......
### [三国动作游戏《卧龙：苍天陨落》确定支持国语配音](https://www.3dmgame.com/news/202206/3844719.html)
> 概要: 13日早上微软的Xbox展示会上，公开的动作游戏《卧龙：苍天陨落》让喜欢三国的玩家格外兴奋，看过预告片之后大家普遍的看法是，“如果有中文配音就更好了！”在今天（6月15日），Koei Tecmo发布了......
### [马斯克为什么不受白宫干部待见？](https://www.huxiu.com/article/582009.html)
> 概要: 本文来自微信公众号：远川出海研究 （ID：aotekuaitan），作者：吴翠婷，编辑：戴老板，题图来自：视觉中国马斯克常在大洋彼岸成为座上宾，但在老家美国，却不怎么受白宫干部待见。2021年8月，拜......
### [掘金实验室自动化，「赛诺迈德」如何通过开放式流水线做出差异化？](https://36kr.com/p/1778706894113924)
> 概要: 1989年，日本秋田国立医院建立了世界第一条自动流水线实验室。而后，欧美市场在20世纪90年代跟进，各大检验巨头开始布局自动流水线领域。赛默飞、罗氏、雅培等医疗器械公司便着手进行实验室智慧化改革，推出......
### [MasterGo IP形象设计 | Mball·麦宝](https://www.zcool.com.cn/work/ZNjAzODIxOTI=.html)
> 概要: 正在参与：「一起创造」MasterGo IP 创意设计大赛......
### [天际老奶奶的心愿：希望B社快点推出《上古卷轴6》](https://www.3dmgame.com/news/202206/3844740.html)
> 概要: 早先老奶奶Shirley Curry因为其《上古卷轴5》玩家的身份在网上爆红，Bethesda计划把她打造成游戏中的NPC，在《上古卷轴6》世界中永生。最近Shirley Curry主持召开了PAXE......
### [动画《在地下城寻求邂逅是否搞错了什么4》新PV](https://news.dmzj.com/article/74672.html)
> 概要: TV动画《在地下城寻求邂逅是否搞错了什么4》公开了新PV。在这次的PV中，可以看到主人公们准备攻略地下城下层，以及在下层战斗的片段。另外还有新角色登场。
### [Palm OS developer releases source to classic games, 20 years after release](https://www.retrorgb.com/palm-os-developer-releases-source-to-classic-games-20-years-after-release.html)
> 概要: Palm OS developer releases source to classic games, 20 years after release
### [“特斯拉杀手”又鸽了？](https://finance.sina.com.cn/tech/2022-06-15/doc-imizirau8556163.shtml)
> 概要: 新浪科技讯 北京时间6月15日上午消息，据报道，亚马逊投资Rivian公司最新的电动SUV的早期买家面临着又一次交付延迟。一些预购了Rivian R1S SUV的客户在本周收到一封电子邮件，该公司通知......
### [成立军团不是要包揽一切](https://finance.sina.com.cn/tech/2022-06-15/doc-imizmscu6905078.shtml)
> 概要: 新浪科技讯 6月15日上午消息，华为伙伴暨开发者大会2022上，华为企业BG总裁丁耘发表演讲......
### [《Apex英雄》命脉外域故事CG动画：家族事业](https://www.3dmgame.com/news/202206/3844753.html)
> 概要: 《Apex英雄》开发商 Respawn 于今天（6 月 15 日）发布了游戏的CG 动画“外域故事”的最新一章“家族事业”。这部动画宣传片围绕游戏中的经典角色命脉展开，讲述了命脉，以及她的好友动力小子......
### [刘慈欣作品首次日漫化！《赡养上帝》漫画版将开连载](https://acg.gamersky.com/news/202206/1491197.shtml)
> 概要: 刘慈欣短篇小说《赡养上帝》将由日本知名漫画家横山旬执笔，6月19日起在「COMIC Hu」开始连载。
### [动画电影「神之山岭」公开正式PV](http://acg.178.com/202206/449261362482.html)
> 概要: 近日，法国动画电影「神之山岭」公开了日语配音版的正式PV，该电影将于7月8日在日本上映。动画电影「神之山岭」正式PV动画电影「神之山岭」（众神的山岭）改编自夢枕獏笔下的同名漫画，讲述了登山摄影师深町诚......
### [智慧黄埔，科技出行](http://www.investorscn.com/2022/06/15/101298/)
> 概要: None
### [24节气插画合集](https://www.zcool.com.cn/work/ZNjAzODU5OTY=.html)
> 概要: 二十四节气凝聚着中华文明精华，上应天体运行规律，下合劳动人民生产节律，表达了人与自然宇宙之间独特的时间观念，蕴含着中华民族悠久的文化内涵和历史积淀，早已根植在中国大众的心中。我们日常生活中随处可见二十......
### [「身为女主角！～被讨厌的女主角和秘密的工作～」漫画发售纪念PV公开](http://acg.178.com/202206/449262509104.html)
> 概要: 根据HoneyWorks原作改编，由岛阴泪亚绘制的漫画「身为女主角！～被讨厌的女主角和秘密的工作～」于近期公开了单行本第一卷发售的纪念PV，该漫画将于6月16日正式发售。「身为女主角！～被讨厌的女主角......
### [「雾山五行」第二篇章「犀川幻紫林」备案公开](http://acg.178.com/202206/449262552411.html)
> 概要: 国产动画「雾山五行」于近日公开了第二篇章「犀川幻紫林」的备案，其中显示了第二篇章共有4集播出。「犀川幻紫林」剧情将承接第一章，由六道无鱼、好传动画、bilibili联合出品，播出时间待定......
### [《5亿年的按钮》动画化决定！7月14日开始播出](https://news.dmzj.com/article/74674.html)
> 概要: 菅原壮太创作的《大家的托尼奥酱》中的故事《5亿年的按钮》宣布了动画化决定的消息。本作将于7月14日开始播出。
### [《监狱建筑师》帮派DLC发售 国区售价26元](https://www.3dmgame.com/news/202206/3844764.html)
> 概要: 今日（6月15日），模拟经营游戏《监狱建筑师》“帮派”DLC现已正式上线，国区售价26元，感兴趣的玩家可以点击此处进入商店页面。发售预告：DLC介绍：他们做好了充分规划，企图接管您的监狱。让他们知道谁......
### [奈飞向竞对求助：聘请Roku和康卡斯特代表开展广告业务](https://finance.sina.com.cn/tech/2022-06-15/doc-imizirau8574616.shtml)
> 概要: 新浪科技讯 北京时间6月15日上午消息，据报道，为投放广告，奈飞（Netflix）开始向竞争对手寻求帮助......
### [《在地下城寻求邂逅》第四季发布新PV 预计7.21播出](https://acg.gamersky.com/news/202206/1491237.shtml)
> 概要: 《在地下城寻求邂逅是否搞错了什么》第四季新章迷宮篇公布了第三弹PV。
### [《海贼王》1053话简易情报：绿牛露脸 新消息不断](https://acg.gamersky.com/news/202206/1491223.shtml)
> 概要: 在《海贼王》1053话情报公开了一部分，海军大将绿牛终于露出真面目，还挺震撼了。路飞等人的悬赏金也更新了。
### [剧场版动画《IDOL舞SHOW》公开宣传片](https://news.dmzj.com/article/74675.html)
> 概要: 动画电影《IDOL舞SHOW》公开了一段宣传片。在这次的视频中，可以看到动画和真人结合的片段。
### [动画「又酷又有点冒失的男孩子们」先导PV公开](http://acg.178.com/202206/449269388276.html)
> 概要: 近日，动画「又酷又有点冒失的男孩子们」公开了先导PV，本作将于2022年10月开播。「又酷又有点冒失的男孩子们」先导PVSTAFF原作：那多ここね（掲載「ガンガンpixiv」スクウェア・エニックス刊）......
### [《三体》作者刘慈欣短篇小说《赡养上帝》漫画化](https://news.dmzj.com/article/74676.html)
> 概要: 由《三体》作者刘慈欣创作的短篇小说《赡养上帝》宣布了漫画化的消息。本作将于6月19日在KADOKAWA的漫画网站COMIC Hu上开始连载，横山旬担任作画。
### [36氪独家 | 布局生命科学前沿智能自动化，「镁伽」获3亿美元C轮融资](https://36kr.com/p/1764685281343747)
> 概要: 36氪获悉，镁伽科技已完成3亿美元C轮融资，由高盛资产管理、亚投资本、纪源资本联合领投，老股东创新工场持续超额加注，新加坡蘭亭投资（Pavilion Capital）、史带资本（Starr Capit......
### [组图：水果姐奥兰多带娃出行被拍 遮住女儿低调面对镜头](http://slide.ent.sina.com.cn/film/h/slide_4_704_371225.html)
> 概要: 组图：水果姐奥兰多带娃出行被拍 遮住女儿低调面对镜头
### [spaCy: SpanCategorizer a new component that handles overlapping spans](https://explosion.ai/blog/spancat/)
> 概要: spaCy: SpanCategorizer a new component that handles overlapping spans
### [连接新经济，36氪多链条赋能创投领域](http://www.investorscn.com/2022/06/15/101305/)
> 概要: None
### [海外new things | 食品科技初创「Brown Foods」种子轮融资236万美元，利用哺乳动物细胞培养技术制造“全脂牛奶”](https://36kr.com/p/1782167629893250)
> 概要: 据外媒TechCrunch报道，食品科技初创「Brown Foods」近期宣布获得236万美元的种子轮融资，投资者包括Y Combinator、AgFunder、SRI Capital、Amino C......
### [从95%到不足1%，外媒：日本受影响最大](https://finance.sina.com.cn/tech/2022-06-15/doc-imizmscu6950880.shtml)
> 概要: 新浪科技讯 北京时间6月15日下午消息，据报道，明日（16日），微软IE浏览器（Internet Explorer）正式退役。27年，这款曾经的王者浏览器终是走到了尽头。彭博社撰文称，日本或是因此举受......
### [大数据基础软件增速超27%，星环科技自研大数据技术驰骋市场](http://www.investorscn.com/2022/06/15/101308/)
> 概要: None
### [谁在撬动黄金界“华强北”？](https://www.tuicool.com/articles/byqEfeI)
> 概要: 谁在撬动黄金界“华强北”？
### [投资19月后，红杉资本平价“抛售”了一家准IPO公司](https://www.tuicool.com/articles/E7J3ymn)
> 概要: 投资19月后，红杉资本平价“抛售”了一家准IPO公司
### [OGP探索号-射频仪](https://www.zcool.com.cn/work/ZNjAzOTI5ODg=.html)
> 概要: 【参与人员】合作绘画师: 艾莉丝项目总监：文渊艺术指导：雪蔓  CiCi  王小锤视觉效果：子奇 庒森视觉：闪耀统筹：米糕建模：两仪 寒露拍摄：文森  海其产品工艺：老六向未知的“太空绘画美术”进行探......
### [多地继续发布暴雨预警，地下官网建设迫在眉睫](http://www.investorscn.com/2022/06/15/101311/)
> 概要: None
### [电动车的轮子，怎么那么难看](https://www.huxiu.com/article/582576.html)
> 概要: 出品 | 虎嗅汽车组作者 | 李文博编辑 | 周到头图 | Car and Driver《暗信号·发现智能汽车的100个创新》专题由虎嗅汽车出品。聚焦于智能汽车正在发生的关键创新点，结合产业、商业以及......
### [IP+项目助力数字藏品平台打造，中青宝推出“稀元项目招商计划”](http://www.investorscn.com/2022/06/15/101312/)
> 概要: None
### [海外new things | 瑞典金融科技初创「Juni」B轮融资2.06亿美元，针对电商零售企业的跨境、跨平台特征](https://36kr.com/p/1782157705006721)
> 概要: 据外媒TechCrunch报道，总部位于瑞典哥德堡市的金融科技初创「Juni」近期宣布完成2.06亿美元的B轮融资，其中包括1亿美元的股权融资和1.06亿美元的债权融资。本轮股权融资由Mubadala......
### [IE浏览器正式永久关闭  曾经占据95%市场份额](https://www.3dmgame.com/news/202206/3844792.html)
> 概要: 曾经无人不识、无人不用，伴随无数网友互联网生活27年的IE浏览器终于谢幕了，IE浏览器于美国时间6月15日（北京时间16日）被永久关闭，微软公司同时向用户推荐使用该公司在2015年推出的Edge浏览器......
### [八位堂发布 Lite 2 轻薄手柄：支持体感振动，适配 Switch / Android](https://www.ithome.com/0/624/379.htm)
> 概要: IT之家6 月 15 日消息，今天，手柄厂商八位堂发布了新款 Lite 2 轻薄手柄，这是一款轻全功能手柄，有红蓝配色，将于 2022 年 7 月 15 日上市。据官方介绍，手柄采用了轻薄的全新设计，......
### [46岁，卖掉魅族，黄章告别江湖](https://www.huxiu.com/article/582537.html)
> 概要: 本文来自微信公众号：投资界 （ID：pedaily2012），作者：张继文，原文标题：《46岁，黄章告别江湖：正式卖掉公司》，题图来自：视觉中国江湖已经很久没有黄章的消息。直至这次魅族正式卖身——投资......
### [华为问界M5门店起火，北汽计划2050年全面脱碳](https://www.yicai.com/news/101444770.html)
> 概要: 近日，一家华为手机专卖店起火，由于火势较大，店内摆放的车辆基本仅剩下车架，该车为华为与赛力斯联手打造的AITO品牌旗下首款中型 SUV—— 问界M5。
### [如果十二小强改变性别，你最期待的是哪个？宁次的人气稳居第一](https://new.qq.com/omn/20220614/20220614A03TFT00.html)
> 概要: 在动漫火影忍者中，木叶村的十二小强可以说是新生代的接班人，作为新一代忍者的代表人物，十二小强在各自的管辖部门也都扮演了非常重要的角色，那么如果木叶十二小强改变了性别的话，你觉得谁的人气会是最高的呢？ ......
### [OCaml 5.0 Alpha Release](https://discuss.ocaml.org/t/ocaml-5-0-zeroth-alpha-release/10026)
> 概要: OCaml 5.0 Alpha Release
### [何超莲谈创业时遇高门槛 称“家里有钱就是原罪”](https://ent.sina.com.cn/tv/zy/2022-06-15/doc-imizirau8655318.shtml)
> 概要: 新浪娱乐讯 在6月15日播出的《不止于她》第二季节目中，何超莲说自己创业时遇到了比任何人都高的门槛。也许所有人都觉得她“不缺钱”，所以针对她会涨价，对此母亲表示“家里有钱就是原罪。” 这种“有钱是原罪......
### [贵圈｜10万人涌进新东方带货直播间：靠卖大米牛排，俞敏洪绝境逢生](https://new.qq.com/rain/a/20220615A06D6L00)
> 概要: * 版权声明：腾讯新闻出品内容，未经授权，不得复制和转载，否则将追究法律责任董宇辉5天上了4次热搜。这位自称“撞脸兵马俑”的主播，除了每天固定两次出现在新东方旗下的“东方甄选”直播间外，也频繁出现在小......
### [三星 Galaxy A04 Core 渲染图曝光：水滴屏 + 后置单摄](https://www.ithome.com/0/624/417.htm)
> 概要: IT之家6 月 15 日消息，据 MySmartPrice 报道，三星预计将在 A 系列产品线推出一款全新入门级智能手机 ——Galaxy A04 Core，该机为去年推出的 Galaxy A03 C......
### [优惠价购表！飞亚达回馈股东，但有的比电商还贵……](https://www.tuicool.com/articles/nQbIfib)
> 概要: 优惠价购表！飞亚达回馈股东，但有的比电商还贵……
### [Whitby residents vote in favour of ending second home ownership](https://news.sky.com/story/whitby-residents-vote-in-favour-of-ending-second-home-ownership-in-seaside-town-12633627)
> 概要: Whitby residents vote in favour of ending second home ownership
### [被网友调侃“和2有仇” 张若昀发微博这样回应](https://ent.sina.com.cn/s/m/2022-06-15/doc-imizmscu6998599.shtml)
> 概要: 新浪娱乐讯 6月15日，张若昀在微博晒出两张日常随拍，并在评论区写道：“我发了2张照片，所以我和2没仇。”昨日，张若昀主演《警察荣誉》收官，编剧赵冬苓近日受访时透露《警察荣誉》考虑写第二部，视频平台提......
### [A股或重现2020年夏季走势？](https://www.yicai.com/news/101444912.html)
> 概要: None
### [华为 FreeBuds Pro 2 曝光：帝瓦雷调音，更强降噪，30 小时续航](https://www.ithome.com/0/624/423.htm)
> 概要: IT之家6 月 15 日消息，今天，爆料人 Roland Quandt 曝光了 FreeBuds Pro 2 无线耳机的消息。该爆料人称，华为 FreeBuds Pro 2 将由法国音频厂商帝瓦雷（D......
### [75基点加息箭在弦上，全球股市长牛走势恐难现](https://www.yicai.com/news/101444975.html)
> 概要: Fedwatch工具显示，6月加息75BP的概率逼近100%，而此前一周的概率仅为8.2%。
### [《数风流人物》李乃文扮陈独秀 侯京健第13次演中青年时期毛泽东](https://new.qq.com/rain/a/ENT2022061500115000)
> 概要: 由江苏省广播电视总台出品，幸福蓝海影视文化集团股份有限公司、中国人民解放军八一电影制片厂等联合出品的重大革命历史题材电视剧《数风流人物》，将于今晚19：30在江苏卫视、浙江卫视、山东卫视三大卫视同步开......
### [以加拿大蒙特利尔和多伦多为例，探讨如何打造成功的数字城市样板区](https://www.yicai.com/news/101444992.html)
> 概要: 在各种数字化前沿创新概念的外衣之下，离不开与“人”“地”“钱”“技”等传统要素的关系处理，任何一对关系处理得不够恰当，都会影响到规划概念的实施。
### [人事调整持续、MPS风险仍在出清，光大证券缘何强势领涨？](https://www.yicai.com/news/101444993.html)
> 概要: 两年前的“历史”会否重演？
### [水滴公司第一季度营收 6.49 亿元，同比下滑 26.6%](https://www.ithome.com/0/624/428.htm)
> 概要: 北京时间 6 月 15 日下午消息，水滴公司（NYSE: WDH）今日发布了截至 3 月 31 日的 2022 年第一季度财报。财报显示，水滴公司第一季度净营收为 6.487 亿元（约合 1.023 ......
### [流言板纽约自由人库巴吉晒全明星双倍票数日海报：韩旭入围](https://bbs.hupu.com/54249389.html)
> 概要: 虎扑06月15日讯 北京时间今日，WNBA纽约自由人球员库巴吉更新Instagram账号，晒全明星双倍票数日海报，韩旭入围。北京时间6月13日结束的WNBA常规赛中，纽约自由人86-88憾负芝加哥天空
### [KDD2022 | MUVCOG：多模态搜索会话下的用户意图刻画](https://www.tuicool.com/articles/y6ZnYz6)
> 概要: KDD2022 | MUVCOG：多模态搜索会话下的用户意图刻画
### [有码变无码](https://finance.sina.com.cn/china/gncj/2022-06-15/doc-imizirau8675717.shtml)
> 概要: 有码变无码 星球商业评论 前天晚上，乃悟写了篇《犯我者虽远必朱》，被一些账号转载后，很多朋友提出了批评。比如： 朋友，听过《卧春》这首名诗么？
### [夜猫聊天除了虎扑足球，大家平时还逛虎扑哪些板块？](https://bbs.hupu.com/54249497.html)
> 概要: 除了虎扑足球，大家平时还逛虎扑哪些板块？见过哪些有意思的帖子和亮评？独乐乐不如众乐乐，来一起分享逛虎扑的快乐。
### [赛后北京WB 1-1 佛山DRG.GK ，步步为营节奏压制，WB扳回一城](https://bbs.hupu.com/54249528.html)
> 概要: Game2：佛山DRG.GK蓝色方，北京WB红色方蓝色方禁用：沈梦溪、盾山、西施、姜子牙蓝色方选用：百兽关羽、鹏鹏铠、青枫弈星、梦岚蒙犽、阿改孙膑红色方禁用：兰陵王、张飞、大乔、公孙离红色方选用：梓墨
### [翻译团100T.Reapered监督：MSI决赛中单的表现会让选手获得灵感](https://bbs.hupu.com/54249586.html)
> 概要: 虎扑6月15日讯 韩媒INVEN发布了“重新开始的LOL训练营...Reapered监督的近况？”一文，原文翻译如下：（未经允许禁止转载、搬运截图）新冠不知不觉可以看出快要结束的征兆，要感谢这个吗？据
### [农业农村部：我部正加快推动渔港管理条例的制定工作](https://finance.sina.com.cn/jjxw/2022-06-15/doc-imizmscu7011403.shtml)
> 概要: 农业农村部近日答复全国人大代表张立祥提出的“关于加快休闲渔业立法的建议”时提到，“我部正加快推动渔港管理条例的制定工作，以规范渔港规划、建设、维护...
### [河南村镇银行多名储户外地查豫康码又现红码，行程码却是绿色](https://finance.sina.com.cn/jjxw/2022-06-15/doc-imizirau8677416.shtml)
> 概要: 近日，多名在河南几家村镇银行存款的储户反映前往郑州尝试取回存款却被赋红码，后其豫康码在昨日（6月14日）午间变回绿码。不过，目前还有外地储户仍反映豫康码...
### [唐山打人案主犯所开“豆捞坊”曾偷电，物业罚款至今未交](https://finance.sina.com.cn/jjxw/2022-06-15/doc-imizirau8677437.shtml)
> 概要: 唐山烧烤店殴打女子主要犯罪嫌疑人陈继志过往劣迹逐渐被揭露。澎湃新闻记者还了解到，除曾涉嫌参与暴力案件“刑拘在逃”、多次被法院列为失信被执行人限制高消费...
### [购房成本降低，筑底的楼市终见曙光？](https://finance.sina.com.cn/china/gncj/2022-06-15/doc-imizmscu7012675.shtml)
> 概要: 来源：北京商报 拿地同比几近腰斩、开工面积降幅远超竣工、住宅销售跌幅仍超三成…… 6月15日，国家统计局发布的房地产相关数据依然低迷，但黑暗中微见曙光：5月单月...
# 小说
### [超神提取](https://m.qidian.com/book/1026845819/catalog)
> 作者：混沌果

> 标签：东方玄幻

> 简介：“发现普通人尸，拾取获得大力丸，是否提取？”“是！”“发现百年归元遗骸，拾取获得不破金身，是否提取？”“是！”“发现无边风妖遗体，拾取获得神通呼风唤火，是否提取？”“是！”“发现崩灭死寂恒星，拾取获得千亿年太阳真晶，是否提取？”……苏景行，“说出来你可能不信，我这一身无敌力量，全靠卖力气得来。”……（已完本两百万字老书，万订精品《无敌从长生开始》，新书起航，扣求各位老铁订阅收藏！）

> 章节总数：共491章

> 状态：完本
# 论文
### [Explain Me the Painting: Multi-Topic Knowledgeable Art Description Generation | Papers With Code](https://paperswithcode.com/paper/explain-me-the-painting-multi-topic)
> 日期：13 Sep 2021

> 标签：None

> 代码：None

> 描述：Have you ever looked at a painting and wondered what is the story behind it? This work presents a framework to bring art closer to people by generating comprehensive descriptions of fine-art paintings.
### [The Low-Resource Double Bind: An Empirical Study of Pruning for Low-Resource Machine Translation | Papers With Code](https://paperswithcode.com/paper/the-low-resource-double-bind-an-empirical)
> 日期：6 Oct 2021

> 标签：None

> 代码：None

> 描述：A "bigger is better" explosion in the number of parameters in deep neural networks has made it increasingly challenging to make state-of-the-art networks accessible in compute-restricted environments. Compression techniques have taken on renewed importance as a way to bridge the gap.
