---
title: 2021-04-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.KusamaPumpkin_EN-CN7791281365_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-04-27 22:43:02
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.KusamaPumpkin_EN-CN7791281365_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Linux 5.13 添加对 Apple M1 的初步支持](https://www.oschina.net/news/139275/linux-5-13-add-initial-support-for-apple#comments)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Linux 5.13 内核中增加了对苹果 M1 SoC 和 2020 年苹果设备（Mac Mini、MacBook Pro、MacBoo......
### [Microsoft 宣布将停止支持多个 .NET Framework 版本](https://www.oschina.net/news/139281/microsoft-announce-stop-support-netfram)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Microsoft宣布，使用传统的、不安全的安全哈希算法1（SHA-1）签名的多个 .NET 框架版本将在明年停止支持。据 .NET 首......
### [Microsoft 宣布将停止支持多个 .NET Framework 版本](https://www.oschina.net/news/139281/microsoft-announce-stop-support-netfram#comments)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>Microsoft宣布，使用传统的、不安全的安全哈希算法1（SHA-1）签名的多个 .NET 框架版本将在明年停止支持。据 .NET 首......
### [2020 中国独立开发者生存现状调研报告](https://segmentfault.com/a/1190000039910514?utm_source=sf-homepage)
> 概要: 前言谁是独立开发者？其实独立开发者归属于自由职业者，是自由职业者中从事和软件开发相关工作的一支人群，更严格来说一般是“从产品立项、设计、开发、推广、到盈利闭环全部独立完成的人”。独立开发者往往是一个人......
### [组图：姐妹情深！孟佳为王霏霏庆生 写藏头诗表白情真意切](http://slide.ent.sina.com.cn/star/slide_4_704_355891.html)
> 概要: 组图：姐妹情深！孟佳为王霏霏庆生 写藏头诗表白情真意切
### [嘀嗒上市，灵魂拷问](https://www.huxiu.com/article/424053.html)
> 概要: 作者｜Eastland头图｜视觉中国2021年4月13日，嘀嗒出行再次向港交所提交上市申请。十天后，哈啰出行在美提交了上市文件。与此同时，滴滴IPO的“传闻”也越传越真。加上已经上市的永安行（6037......
### [李亚鹏“败走”河南：一个演员如何卷入了地产洪流](https://www.huxiu.com/article/424253.html)
> 概要: 本文来自微信公众号：棱镜（ID：lengjing_qqfinance），作者：郭菲菲，编辑：李超仁，头图来自：视觉中国郑开大道这条主干道，连接着河南省会郑州和古城开封，城市交界处，正处于开发状态的一片......
### [「天宝伏妖录」第二季官方壁纸公开](http://acg.178.com//202104/413486623472.html)
> 概要: 近日，动画「天宝伏妖录」第二季正式上线，官方发布了最新一季的高清壁纸。动画「天宝伏妖录」改编自非天夜翔创作的同名网络小说作品，2017年04月01日连载于晋江文学城，现已完结。该作讲述了唐朝天宝盛世下......
### [网飞《武士弥助》中文正式预告 黑人武士再掀杀戮](https://acg.gamersky.com/news/202104/1383261.shtml)
> 概要: Netflix奇幻时代剧动画《武士弥助》公开了中文正式预告。
### [丝芭传媒推出新人邓歆玥，被誉鞠婧祎“最强平替”，连粉丝都认不出](https://new.qq.com/omn/20210424/20210424A07LSW00.html)
> 概要: 丝芭传媒算是国内很知名的一家公司，之前丝芭传媒就将鞠婧祎这位女爱豆成功捧红，了解鞠婧祎的人都清楚，目前她也称得上是内地娱乐圈中极具存在感的女明星了。也许是因为鞠婧祎目前已经达到一个圈内高度，所以这段时......
### [被放弃的国产主持人](https://www.huxiu.com/article/424433.html)
> 概要: 本文来自微信公众号：Sir电影（ID：dushetv），作者：毒Sir，头图来自：《主持人大赛》截图哪里有吐槽，哪里就有回护。前些日子，挥别《快本》那篇推文里，就有网友留言：难道“只有主持人有问题么......
### [TV动画「奇蛋物语」BD第二卷封面公开](http://acg.178.com//202104/413491805702.html)
> 概要: TV动画「奇蛋物语」公布了第二卷Blu-ray&DVD的封面，该商品将于5月26日发售。「奇蛋物语」又名「WONDER EGG PRIORITY」，是由日本电视台与Aniplex共同推出、Clover......
### [剧场版《FGO》后篇延期上映 冠位时间神殿所罗门上映决定](https://news.dmzj1.com/article/70738.html)
> 概要: 根据手游《Fate/Grand Order》制作的剧场版动画《Fate/Grand Order -神圣圆桌领域卡美洛- 后篇 Paladin; Agateram》宣布了上映延期的消息。
### [PS5《失落之魂》17分钟演示：展示主角战斗细节](https://www.3dmgame.com/news/202104/3813313.html)
> 概要: 今日（4月27日），IGN油管频道分享了一段 PS5版国产ACT《失落之魂》17分钟实机演示影像，此次影像展示了Boss战、主角技能等细节，该作是PlayStation 中国之星计划作品之一，将登录P......
### [甜蜜二人世界！戚薇自曝与李承铉把厕所当约会地](https://ent.sina.com.cn/s/m/2021-04-27/doc-ikmyaawc2065517.shtml)
> 概要: 新浪娱乐讯 近日，戚薇在采访中自曝与李承铉的甜蜜日常。她表示在女儿入睡后，会和李承铉在厕所里聊天、追剧，已经把厕所当做了约会地点，“我们常常喜欢在厕所搞点什么”，“弄一些香气、香氛，让氛围好一些”。　......
### [「半妖的夜叉姬」第二章将于今年秋天开始播出](http://acg.178.com//202104/413494908606.html)
> 概要: 「犬夜叉」续作TV动画「半妖的夜叉姬」官方宣布，第二章将于今年秋天开始播出。「半妖的夜叉姬」由高桥留美子负责原案设计，サンライズ负责动画制作，讲述了一个发生在继承了妖怪与人类血脉的半妖少女日暮永远、刹......
### [郑爽演员身份认证已恢复，跟张恒争完抚养权就准备复出？网友集体抗议](https://new.qq.com/omn/20210423/20210423A07AE200.html)
> 概要: 郑爽从今年年初到现在因为跟张恒的恩怨已经闹到半退圈的状态了，在这4个月里她更多的是在跟张恒打官司，现在两人已经开始争两个孩子的抚养权了。            郑爽为此还专门飞到国外去解决，就是想尽快......
### [俄罗斯驻华大使馆发文恭喜利路修下班成功](https://ent.sina.com.cn/tv/zy/2021-04-27/doc-ikmyaawc2079661.shtml)
> 概要: (责编：Mia)......
### [Netflix动画部门推出专属Vtuber“N子”](https://news.dmzj1.com/article/70739.html)
> 概要: Netflix在本日（27日）宣布了Netflix动画官方Vtuber“N子”活动开始的消息。N子将由最喜欢动画的Netflix社员担任“中之人”，除了介绍作品的魅力和制作内幕之外，还将进行唱歌和游戏等方面的尝试，也会与其他Vtuber联动。
### [Types of Legal Argument](https://philosophicaldisquisitions.blogspot.com/2021/03/understanding-legal-argument-1-five.html)
> 概要: Types of Legal Argument
### [不同年代不用画风！动画电影《CHERRY AND VIRGIN》PV](https://news.dmzj1.com/article/70740.html)
> 概要: 动画电影《CHERRY AND VIRGIN》宣布了2022年上映的消息，本作的PV与宣传图等也一并公开。
### [「驱魔少年」亚连·沃克黏土人开订](http://acg.178.com//202104/413504428294.html)
> 概要: 「驱魔少年」亚连·沃克黏土人开订，作品采用ABS、PVC材质，全高约100mm，日版售价5700日元（含税），约合人民币342元，预计将于2021年12月发售。该手办采用全可动设计，表情零件有「普通脸......
### [《创造营4》张欣尧成功出圈，送甘望星银行卡，引起观众强烈共鸣](https://new.qq.com/omn/20210423/20210423A0DU3W00.html)
> 概要: 《创造营2021》最新一期的节目播出后争议不断，全片最大的看点无疑是广受好评的利老师，在这期节目中利路修金句频出，说出了很多心里话，引起了观众们的强烈共鸣，其实刚开始来参加节目并不是他的本意     ......
### [EVE周年庆福利再加码！携手Alienware开启线下狂欢](https://ol.3dmgame.com/news/202104/30904.html)
> 概要: EVE周年庆福利再加码！携手Alienware开启线下狂欢
### [杨幂“上班照”被疯传，普通雨衣穿成高定款，这身材绝了](https://new.qq.com/omn/20210422/20210422A0AD7100.html)
> 概要: 杨幂身为娱乐圈的当红花旦，她虽然出道至今也很多年了，但是她却依旧深受大众们的喜爱，是一位从出道就一直活到现在的女演员。每当有杨幂名字出现的地方，也必定会引起全网的关注和热议。出道多年杨幂出演了不少的影......
### [原卡普空制作人小野义德5月加入《FGO》开发商](https://shouyou.3dmgame.com/news/55172.html)
> 概要: 根据《FGO》开发商DELiGHTWORKS的新公告，原卡普空制作人小野义德预计将于2021年5月1日加入公司，届时将出任代表取缔役社长COO职位。此次小野义德将参与公司管理工作，以多元化视角来运营公......
### [组图：关晓彤现身央视五四晚会录制 黑大衣配红短裙大长腿吸睛](http://slide.ent.sina.com.cn/z/v/slide_4_704_355912.html)
> 概要: 组图：关晓彤现身央视五四晚会录制 黑大衣配红短裙大长腿吸睛
### [行业呼吁+官方回应 向短视频盗版侵权大声说"不"](https://ent.sina.com.cn/v/m/2021-04-27/doc-ikmxzfmk9264348.shtml)
> 概要: 4月25日，在国务院新闻办公室举行的新闻发布会上，中宣部版权管理局局长于慈珂表示，要继续加大对短视频领域侵权行为的打击力度，坚决整治短视频平台以及自媒体、公众账号生产运营者未经授权复制、表演、传播他人......
### [EdTech初创公司引领拉美教育变革](https://www.tuicool.com/articles/ZniUryQ)
> 概要: 36氪出海网站上线了！获取更多全球商业相关资讯，请点击letschuhai.com编者按：本文选自微信公众号“拉美创投洞察（ID：pvlatam）”，作者：李若星、彭思思，36氪经授权转载。教育是对社......
### [“中等收入陷阱”是文科生的锅吗？](https://www.tuicool.com/articles/f2iAFva)
> 概要: 本文来自微信公众号：看理想（ID：ikanlixiang），作者：梁捷、方可成，编辑：苏小七，题图来自：《死亡诗社》前几周，央行发表了一篇工作论文，论文最后一段的非主旨部分出现了一句话，像是扔进水里的......
### [【直播预告】这周有吃播啦](https://news.dmzj1.com/article/70745.html)
> 概要: 本周直播预告
### [在线教育，安不安全？](https://www.tuicool.com/articles/22aIjqi)
> 概要: 2020 年爆发的新冠疫情促使教育领域发生了翻天覆地的转变。为强化课堂学习功能，许多学校已经购置并部署了新的教室设备和课堂软件程序，以确保过去发生在课堂中的教学操作在远程教学中也能运作。对于常有预算危......
### [【视频】2000-3000 元买啥手机？我们研究了 8 台 2021 年新机给你答案](https://www.ithome.com/0/548/757.htm)
> 概要: 年后的发布会是真的太多了，许多厂商都发布了年度旗舰，不过 2000-3000 元价位真是群魔乱舞，上至骁龙 888，下至天玑 800U，属实有点离谱。刚好IT之家手头新机很全，就做期视频和大家分享一下......
### [一周大师级Cos美图赏  众多美女性感火辣逼真还原](https://www.3dmgame.com/bagua/4523.html)
> 概要: 又到了欣赏每周大师级Cos合集时间，此次包含《英雄联盟》《巫师3》《古墓丽影》《尼尔》等游戏动漫Cos作品。众多Coser完美还原了人物角色，一起来欣赏下美图吧......
### [为什么华米 OV 都跑去做笔记本了](https://www.ithome.com/0/548/761.htm)
> 概要: 显卡价格翻倍沦为“空气”，CPU 屠龙者 AMD 反成“恶龙”。今年本已被频频打上“夕阳产业”标签的 PC（个人电脑）市场真是雪上加霜。不过，笔记本电脑（下称笔记本）这边却风景独好。先不说疫情之下，2......
### [SIMD for C++ Developers pdf](http://const.me/articles/simd/simd.pdf)
> 概要: SIMD for C++ Developers pdf
### [《绿林侠盗：亡命之徒与传奇》发售后内容更新预告](https://www.3dmgame.com/news/202104/3813355.html)
> 概要: Focus Home互动和开发商Sumo Digital发布了中世纪动作RPG游戏《绿林侠盗：亡命之徒与传奇》发售后计划和第一年季票详情。预告片：官方日志：不法之徒们，你们好！我们很高兴向大家公布《绿......
### [一人一句，如何评价北大中文男足？](https://bbs.hupu.com/42553544.html)
> 概要: 北大中文男足这段时间再次带给了无限快乐，体育原本就是应该与快乐密切相关的。大家一人一句，都来说说可爱的北大中文男足吧
### [多因素平滑缴税高峰冲击，5月流动性压力仍不小](https://finance.sina.com.cn/jjxw/2021-04-27/doc-ikmxzfmk9323224.shtml)
> 概要: 原标题：多因素平滑缴税高峰冲击，5月流动性压力仍不小 作为传统缴税大月，此前市场对4月流动性颇为担忧。但自上周进入缴税高峰期以来，流动性和资金利率表现平稳...
### [区块链基础设施公司Figment启动1600万美元的投资基金](https://www.btc126.com//view/161089.html)
> 概要: 据CoinDesk报道，区块链基础设施公司Figment今日宣布已启动1600万美元的投资基金Figment Capital，以支持Cosmos、Terra以及Livepeer等去中心化协议和应用......
### [美股开盘涨跌不一，区块链板块小幅反弹](https://www.btc126.com//view/161090.html)
> 概要: 行情显示，美股开盘涨跌不一，道指跌0.13%，纳指涨0.23%，标普500指数涨0.08%，区块链板块小幅反弹......
### [海口突降暴雨一学生在校门外意外触电 官方回应：全面排查整改](https://finance.sina.com.cn/china/dfjj/2021-04-27/doc-ikmyaawc2184181.shtml)
> 概要: 原标题：海口突降暴雨一学生在校门外意外触电，官方回应：全面排查整改，消除隐患 每经编辑 张杨运 据海口发布27日消息，4月26日下午约6时许...
### [如此野蛮粗糙？上海阳城小区实施分流雨污，仅水管就被挖爆5次](https://finance.sina.com.cn/china/dfjj/2021-04-27/doc-ikmxzfmk9325726.shtml)
> 概要: “前几天，7支弄和8支弄中间处，水管又被挖爆了一次，小区只能停水急修。”而自去年12月底开始实施雨污分流工程以来，闵行区上海阳城小区居民吴女士记忆中...
### [谷歌赢得有史以来最大云计算合同：拿下 Univision 10 亿美元](https://www.ithome.com/0/548/769.htm)
> 概要: 北京时间 4 月 27 日晚间消息，据报道，谷歌和西班牙语内容及传媒公司 Univision 今日达成一项新的多年战略合作伙伴关系，旨在推动 Univision 的媒体和内容资产组合的增长，并助力 U......
### [ZBX交易所辟谣：ZBX交易所被变卖为不实消息](https://www.btc126.com//view/161095.html)
> 概要: ZBX交易所辟谣：近期网传ZBX交易所被变卖为不实信息，ZBX交易所运营正常，没有任何被收购计划。该系列传闻对ZBX交易所造成极大负面影响，ZBX法务部已经介入，以下为律师函内容......
### [“疫苗气泡”下香港放宽防疫措施：推四种营运模式供食肆选择](https://finance.sina.com.cn/china/dfjj/2021-04-27/doc-ikmyaawc2188693.shtml)
> 概要: 原标题：“疫苗气泡”下香港放宽防疫措施：推四种营运模式供食肆选择 香港特区政府食物及卫生局局长陈肇始27日召开记者会公布，将以“疫苗气泡”为基础放宽一系列防疫措施...
### [流言板全运会男足第二轮：福建两连胜，山东逆转广东，辽宁胜云南](https://bbs.hupu.com/42554982.html)
> 概要: 虎扑04月27日讯 据博主“中超青年CSLY”的消息，今天下午，2021陕西全运会足球项目资格赛男足U20组比赛迎来了第二轮的较量。第二轮比赛依旧非常激烈，A组中，福建队零封对手，暂列小组首位，河北队
### [流言板赵继伟出战17分47秒，得到2分1篮板1助攻1抢断6犯规](https://bbs.hupu.com/42554972.html)
> 概要: 虎扑04月27日讯 CBA总决赛第一场辽宁83-88不敌广东的比赛中，赵继伟全场得到2分1篮板1助攻1抢断6犯规。本场比赛，赵继伟出战17分钟，3投0中，其中三分球2投0中，罚球2投2中，全场得到2分
### [Google have declared Droidscript is malware](https://groups.google.com/g/androidscript/c/Mbh5TZ6YYnA/m/GflwflqaDAAJ)
> 概要: Google have declared Droidscript is malware
### [惟愿一生爽一次](https://finance.sina.com.cn/china/gncj/2021-04-27/doc-ikmyaawc2188211.shtml)
> 概要: 来源：星球商业评论 2015年3月，徐州新沂，一个不起眼的六线小城，成为了国家中小城市综合改革试点地区。个人独资企业在新沂开办，会附带一系列的税收优惠...
### [流言板沧州vs河南全场数据：进攻乏力，两队仅1脚射正](https://bbs.hupu.com/42555505.html)
> 概要: 虎扑04月27日讯 今日结束的中超第2轮比赛中，河南嵩山龙门0-0闷平沧州雄狮。赛后数据统计显示，两队全场仅有一脚射正。沧州雄狮在控球、传球、射门上全面领先，但进攻效率始终偏低，最终收获平局。本场过后
### [总书记广西行提到桂林山水、螺蛳粉…严於信：背后有一道“大餐”](https://finance.sina.com.cn/china/gncj/2021-04-27/doc-ikmyaawc2189932.shtml)
> 概要: 责任编辑：尹悦......
# 小说
### [蝗不入境](http://book.zongheng.com/book/953695.html)
> 作者：俞同姥爷

> 标签：武侠仙侠

> 简介：“螟蝗过境，百废待兴。蝗不入境，天下太平。” 修真大陆内，蝗党修士暴虐无道肆行无忌，所到之处民不聊生。劳石仁穿越异世初窥修真之道。崛起？称霸？暂时没想太多，他只求能回到现实，把那即将抢自己女友当老婆的臭小子揍一顿……

> 章节末：第七十九章  奇人异事

> 状态：完本
# 论文
### [Learning Camera Localization via Dense Scene Matching](https://paperswithcode.com/paper/learning-camera-localization-via-dense-scene)
> 日期：31 Mar 2021

> 标签：CAMERA LOCALIZATION

> 代码：https://github.com/Tangshitao/Dense-Scene-Matching

> 描述：Camera localization aims to estimate 6 DoF camera poses from RGB images. Traditional methods detect and match interest points between a query image and a pre-built 3D model.
### [A formalization of Dedekind domains and class groups of global fields](https://paperswithcode.com/paper/a-formalization-of-dedekind-domains-and-class)
> 日期：4 Feb 2021

> 标签：None

> 代码：https://github.com/lean-forward/class-number

> 描述：Dedekind domains and their class groups are notions in commutative algebra that are essential in algebraic number theory. We formalized these structures and several fundamental properties, including number theoretic finiteness results for class groups, in the Lean prover as part of the mathlib mathematical library.
