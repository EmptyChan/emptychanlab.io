---
title: 2023-05-01-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.QuebecCityBridge_ZH-CN9618387961_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-05-01 21:38:58
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.QuebecCityBridge_ZH-CN9618387961_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [ChatGPT背后的打工人，月薪3000](https://www.woshipm.com/it/5817581.html)
> 概要: ChatGPT在社交媒体上引起了巨大的话题度后，国内多家互联网企业相继推出类ChatGPT产品。作为人工智能下游的数据标注行业，也迎来了不小的改变。本文作者对此进行了采访分析，一起来看看吧。2023年......
### [“五一刺客”，贵到离谱](https://www.woshipm.com/it/5817634.html)
> 概要: 五一假期，你是宅家中还是去旅游？看看售罄的高铁票，再看看高昂的酒店价格，最后看看景区里的人山人海，这个五一，不一般。本文作者对五一的旅游业进行了简单的分析，一起来看看吧。距离五一假期越来越近，济南的阿......
### [中文互联网青春流落“天涯”](https://www.woshipm.com/it/5817600.html)
> 概要: 前不久，天涯社区宣布因为技术原因暂时关停，而后关于它即将永久关停的消息不胫而走。作为昔日全球最大的中文互联网社区，它曾陪伴中国最早一代互联网用户的成长，可谓是中文互联网的“青春”。BBS论坛社区的陆续......
### [海外new things | Yahoo收购社交体育博彩应用Wagr，加码美国奇幻体育热潮](https://36kr.com/p/2229841748923777)
> 概要: 海外new things | Yahoo收购社交体育博彩应用Wagr，加码美国奇幻体育热潮-36氪
### [计算机产业长期领导者——百年科技巨头IBM](https://www.yicai.com/video/101744885.html)
> 概要: 作为计算机产业长期的领导者，IBM在大型/小型机和便携机（ThinkPad）方面的成就最为瞩目。其创立的个人计算机（PC）标准，至今仍被不断地沿用和发展。此外，IBM还在大型机、超级计算机、UNIX、服务器方面领先业界。
### [卡牌动作塔防肉鸽新作《Heretic's Fork》发布预告片](https://www.3dmgame.com/news/202305/3868325.html)
> 概要: 多元素融合新作《Heretic's Fork》近日发布最新预告片。这款作品拥有塔防、卡牌、肉鸽和动作元素。玩家需要建造塔楼、用卡牌加固基地、获取新武器来抵御敌人的攻击。《Heretic's Fork》......
### [CMA似乎禁止微软未来10年收购动视暴雪](https://www.3dmgame.com/news/202305/3868327.html)
> 概要: 4月26日，英国反垄断机构CMA（竞争与市场管理局）做出最终决定，禁止微软收购动视暴雪，理由是出于对云游戏的担忧。根据Resetera上网友披露出来的更多条款，CMA似乎还禁止微软在未来十年内收购动视......
### [最近，清华系创始人太火了](https://36kr.com/p/2237295077568129)
> 概要: 最近，清华系创始人太火了-36氪
### [主播Ninja组建新工作室 用UEFN开发大逃杀新作](https://www.3dmgame.com/news/202305/3868330.html)
> 概要: 大逃杀领域受到不少Twitch观众追捧的主播Ninja近日宣布，将使用《堡垒之夜》的虚幻编辑器（UEFN）来打造名为Project V的全新大逃杀游戏。Ninja通过社交媒体宣布：“我很高兴能与一些最......
### [海外new things | 精准农业技术初创「Hydrosat」A轮融资2000万美元，将在明年发射两颗气候监测卫星](https://36kr.com/p/2232898416275081)
> 概要: 海外new things | 精准农业技术初创「Hydrosat」A轮融资2000万美元，将在明年发射两颗气候监测卫星-36氪
### [《生化危机4：重制版》MOD触发尚未使用的对话素材](https://www.3dmgame.com/news/202305/3868331.html)
> 概要: 《生化危机4：重制版》MOD作者aaronlink在游戏中启用了换装菜单，这应该是很多MOD玩家非常熟悉的功能。但作者发现，用换装菜单在游戏中换装的时候，两位主角会互相点评彼此的服装变化，这显然是指向......
### [免费，终极 ChatGPT 提示 + Midjourney 宝藏神图，1200 + 图片和咒语，震撼人心](https://www.ithome.com/0/690/133.htm)
> 概要: 一位网友花费一周，用从 ChatGPT 生成的 prompt，在 Midjourney 中生成了一千多张精彩的作品。Midjourney 虽然功能神奇，但在 prompt 能力平平的人手里，它并不能绽......
### [“港车北上”6月1日起接受申请](https://www.yicai.com/news/101745560.html)
> 概要: 5月1日，《广东省关于香港机动车经港珠澳大桥珠海公路口岸入出内地的管理办法》对外公布，从2023年6月1日9时起开始接受香港机动车车主申请。
### [一家“退出率87%”的基金，突然火了](https://36kr.com/p/2238711634997126)
> 概要: 一家“退出率87%”的基金，突然火了-36氪
### [《豆吉豆子的家里蹲日常》第2弹5月17日开播](https://news.dmzj.com/article/77923.html)
> 概要: 根据豆吉豆子原作改编的TV动画《豆吉豆子的家里蹲日常》第2弹，将于5月17日开始在富士电视网播出。
### [《COMPASS 战斗天赋解析系统》PV第2弹公开](https://news.dmzj.com/article/77924.html)
> 概要: 在4月30日，千叶幕张展览馆举办的“NICONICO超会议2023”中，在线对战型手游《COMPASS 战斗天赋解析系统》公布了动画企划的第2弹PV。
### [《派对浪客诸葛孔明》电视剧化决定](https://news.dmzj.com/article/77925.html)
> 概要: 四叶夕卜原作，小川亮作画的漫画《派对浪客诸葛孔明》的决定电视剧化了。主角诸葛孔明由向井理饰演。
### [AI社交“鬼城”：人类，禁止入内](https://www.huxiu.com/article/1386312.html)
> 概要: 本文来自微信公众号：量子位 （ID：QbitAI），作者：衡宇，原文标题：《AI专属社交平台爆火，全体人类被禁言只能围观》，题图来自：《终结者2：审判日》玩腻了推特和微博？有个新的社交平台火爆外网，成......
### [微软 Edge 浏览器将把 Bing Chat 集成到右键菜单，划词即可询问聊天机器人](https://www.ithome.com/0/690/139.htm)
> 概要: 感谢IT之家网友Coje_He的线索投递！IT之家5 月 1 日消息，微软 Edge 浏览器正在进一步推广其 Bing Chat 聊天机器人，在最新的 Canary 版本中，这一功能已集成到了右键菜单......
### [淄博烧烤，一场普通人的市井生活保卫战](https://www.huxiu.com/article/1387231.html)
> 概要: 本文来自微信公众号：新潮沉思录（ID：xinchaochensi），作者：天书、刘梦龙，头图来自：视觉中国这阵子，淄博烧烤火了，大火。淄博烧烤似乎是这些年来山东少有的一抹亮色。在这几年的网络滤镜里，山......
### [周六福品牌大片《人生一克》今日于CCTV-1重磅首播](http://www.investorscn.com/2023/05/01/107239/)
> 概要: 黄金的意义，不再是那一抹显眼的亮色与外露的贵重，更多的是我们每个人赋予它的独特意义。5月1日起，周六福最新品牌大片《人生一克》重磅登陆央视，透过镜头讲述生命中的美好时刻，捕捉黄金点亮生活里的点滴光芒，......
### [喜临门2022年报：营收78.39亿元，线上线下渠道齐突破](http://www.investorscn.com/2023/05/01/107240/)
> 概要: 4月28日，喜临门(SH603008)发布2022年年度报告。年报显示，报告期内，公司实现营业收入78.39亿元，同比增长0.86%；归属于上市公司股东的净利润为2.38亿元。在2022年多重因素导致......
### [“舒适战略”成效初显，奥康以鞋界黑科技“硬核”出圈](http://www.investorscn.com/2023/05/01/107241/)
> 概要: 后疫情时代,受经济下沉与收入不稳定的影响,民众的消费行为和消费观念都发生了巨大变化。据DATA100数据显示,63%的人消费变得更加理性。受“新消费”影响,不少传统品牌开启“品牌年轻化”的道路......
### [鞋服行业的“疫”后重构，奥康多措并举焕新显效](http://www.investorscn.com/2023/05/01/107242/)
> 概要: 在消费升级的时代背景下,消费者主权意识在不断增强,尤其体现在消费品层面。从鞋服行业角度出发,伴随生活节奏的加快、穿着场景变得更加多样化,消费者在做出购买决策时,不再仅仅考虑产品质量是否信得过、价格是否......
### [靠增程抢的充电桩，凭什么让给纯电？](https://www.huxiu.com/article/1386318.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博头图｜纯电车主正在就充电头使用权进行友好磋商有电就算同林鸟，没电桩前各自飞。每到长假，中国新能源车主们就会揭下平时枪口一致对准燃油车的“面具”，在稀缺的高速充电桩前，不厌其......
### [RED 撤诉，尼康 Z 9 相机可继续使用内录 N-RAW 格式](https://www.ithome.com/0/690/153.htm)
> 概要: 感谢IT之家网友行李箱、斳、Harry12345的线索投递！IT之家5 月 1 日消息，RED.com LLC 于 2022 年 5 月起诉尼康，称尼康 Z 9 旗舰相机 2.0 固件更新中引入的 N......
### [零跑汽车 4 月交付新车 8726 台，环比提升 41%](https://www.ithome.com/0/690/160.htm)
> 概要: 感谢IT之家网友霜风神影的线索投递！IT之家5 月 1 日消息，零跑汽车宣布 4 月累计交付新车 8726 台，环比提升 41%，C 系列车型占比超 83%。根据IT之家此前报道，零跑汽车在 3 月 ......
### [卡普空的商业决策让《洛克人EXE合集》缺席Xbox](https://www.3dmgame.com/news/202305/3868339.html)
> 概要: 《洛克人EXE合集》并没有登陆Xbox平台，卡普空表示，这是他们的“商业决策”。去年，有推特用户就Xbox版《洛克人EXE合集》的问题联系卡普空，而厂商回应表示不能对“商业决策”发表评论。如今，卡普空......
### [刘若英发文纪念《粉红女郎》开播20年](https://ent.sina.com.cn/s/h/2023-05-01/doc-imyshnsr8415605.shtml)
> 概要: 新浪娱乐讯 今年是《粉红女郎》开播20周年，5月1日，刘若英微博发文，回忆了当年接演“结婚狂”的经历：“20年了，真是不敢相信到现在还有人提及此片。真希望看片的人能开心，那就是我们最开心的事情了。”　......
### [中条彩未宣布结婚喜讯 男方是圈外人士](https://ent.sina.com.cn/s/j/2023-05-01/doc-imyshnst6979817.shtml)
> 概要: 新浪娱乐讯 中条彩未更新个人社交平台，宣布结婚喜讯。据悉，对方是IT公司株式会社AViC的社长市原创吾，目前二人已经提出了结婚申请。(责编：小万)......
### [葛均波院士团队最新研究：空气污染激增会增加心律失常风险](https://www.yicai.com/news/101745642.html)
> 概要: 空气污染对心血管的影响再添佐证。中国研究人员近日发表的一项大型研究发现，空气污染激增会增加心律失常的风险。
### [第133届广交会第三期今天开幕](https://finance.sina.com.cn/china/2023-05-01/doc-imyshsyp8376496.shtml)
> 概要: 转自：央视网 央视网消息（新闻联播）：第133届广交会第三期今天（5月1日）开幕，本期展览紧贴国际国内市场需求，聚焦行业发展方向，展览题材更为丰富。
### [史航回应涉嫌性骚扰：情绪我理解但情况不属实](https://ent.sina.com.cn/s/m/2023-05-01/doc-imyshsyp8371096.shtml)
> 概要: 新浪娱乐讯 5月1日晚，编剧史航微博发文回应涉嫌性骚扰：“网上涉及我的文字，我看到了，情绪我理解，但情况不属实。这三天，我一直没有回复网络上的诸多质疑，因为我正试图找到妥当的办法，在保护双方隐私的情况......
### [五一档票房近10亿，《人生路不熟》单日票房反超《长空之王》](https://www.yicai.com/news/101745672.html)
> 概要: 回顾历年的五一档，而2021年五一档全国票房为16.74亿元，为几年内最高。疫情前的2019年为15.27亿元，2018年为10.06亿元，2017年为7.84亿元，2016年为6.5亿元。
### [“进口嗨购节”启动，今年进博会签约面积已超26万平](https://www.yicai.com/news/101745675.html)
> 概要: “进口嗨购节”既是加快建设上海国际消费中心城市的重要抓手，也是持续放大进博会溢出带动效应的重要举措
### [《长空之王》4天3.65亿领跑“五一档”，试飞员故事让观众泪目](https://finance.sina.com.cn/jjxw/2023-05-01/doc-imyshsyp8381681.shtml)
> 概要: 今年“五一档”市场竞争异常激烈，共有18部新片入市。由刘晓世执导，王一博、胡军、周冬雨等主演的《长空之王》表现抢眼，目前以4天3...
### [371.1万人次 长三角铁路再创单日客发量历史新高](https://finance.sina.com.cn/jjxw/2023-05-01/doc-imyshsyt5007559.shtml)
> 概要: 证券时报网讯，据央视新闻，从中国铁路上海局集团有限公司获悉，继4月30日长三角铁路发送旅客365.5万人次、创下单日客发量历史新高后，5月1日，长三角铁路发送旅客371...
### [被多人指控性骚扰，编剧史航回应](https://finance.sina.com.cn/jjxw/2023-05-01/doc-imyshsyt5014135.shtml)
> 概要: 新京报讯 5月1日晚，编剧史航微博发文回应涉嫌性骚扰：“网上涉及我的文字，我看到了，情绪我理解，但情况不属实。这三天，我一直没有回复网络上的诸多质疑...
### [购房者不追涨、市场热度回落 4月北京二手住宅成交量环比下降](https://finance.sina.com.cn/jjxw/2023-05-01/doc-imyshsyp8402995.shtml)
> 概要: 今年2月份，“小阳春”来得早且突然，进入3月下旬，一线经纪人的直观感受是风向有所转变。时至4月份，新增客户量下降明显，二手房市场回归理性。
# 小说
### [我真的是个好人啊](https://book.zongheng.com/book/1047691.html)
> 作者：异端世界

> 标签：二次元

> 简介：“虽然我的班主任黄泉川爱穗总是抓我进局子，隔壁常盘台的御坂美琴称我为恶党，同校的一方通行总想和我打架，前代赤王迦具都玄示想让我做‘炼狱舍’的接班人，但是……我真的是个好人来着！”天道一一脸无奈。“好人？明明是恶党吧……。”不知道是谁的声音，总之不是一个人就是了。“喂，人工智障，咱下次干好事干到底别违法乱纪成么？我真的是个好人啊！”坐在警车上的天道一一脸恳求。“不，你不是，请努力成为一名人见人怕的坏蛋吧！”系统姬一本正经的说道，并向天道一发布了新的任务。

> 章节末：第一百五十七章 往事如烟

> 状态：完本
# 论文
### [Multilingual Fact Linking | Papers With Code](https://paperswithcode.com/paper/multilingual-fact-linking)
> 概要: Knowledge-intensive NLP tasks can benefit from linking natural language text with facts from a Knowledge Graph (KG). Although facts themselves are language-agnostic, the fact labels (i.e., language-specific representation of the fact) in the KG are often present only in a few languages.
### [FS6D: Few-Shot 6D Pose Estimation of Novel Objects | Papers With Code](https://paperswithcode.com/paper/fs6d-few-shot-6d-pose-estimation-of-novel)
> 概要: 6D object pose estimation networks are limited in their capability to scale to large numbers of object instances due to the close-set assumption and their reliance on high-fidelity object CAD models. In this work, we study a new open set problem; the few-shot 6D object poses estimation: estimating the 6D pose of an unknown object by a few support views without extra training. To tackle the problem, we point out the importance of fully exploring the appearance and geometric relationship between the given support views and query scene patches and propose a dense prototypes matching framework by extracting and matching dense RGBD prototypes with transformers. Moreover, we show that the priors from diverse appearances and shapes are crucial to the generalization capability under the problem setting and thus propose a large-scale RGBD photorealistic dataset (ShapeNet6D) for network pre-training. A simple and effective online texture blending approach is also introduced to eliminate the domain gap from the synthesis dataset, which enriches appearance diversity at a low cost. Finally, we discuss possible solutions to this problem and establish benchmarks on popular datasets to facilitate future research. The project page is at \url{https://fs6d.github.io/}.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
