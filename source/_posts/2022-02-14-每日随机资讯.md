---
title: 2022-02-14-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MaldivesHeart_ZH-CN0032539727_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-02-14 21:54:01
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MaldivesHeart_ZH-CN0032539727_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [苹果新操作系统"realityOS"曝光](https://www.oschina.net/news/182298/realityos-hints-in-apple-logs-n-code)
> 概要: 软件开发者在App Store 的上传日志和苹果使用的GitHub 仓库中发现了多次提及"realityOS"的代码。从曝光的信息来看，realityOS 极有可能是苹果打造的新 AR/VR 操作系统......
### [Vite 2.8 发布，全新的前端构建工具](https://www.oschina.net/news/182287/vite-2-8-released)
> 概要: Vite 2.8 已发布。主要更新内容：升级底层：esbuild 0.14 & TypeScript 4.5Workers 使用新的 URL() 模式减少内存占用空间：2.8 的发布包大小约为 2.7......
### [SunnyUI 新版 V3.1.1 发布啦！C# WinForm 开源控件库](https://www.oschina.net/news/182309/sunnyui-3-1-1-released)
> 概要: SunnyUI.Net 是基于.Net Framework 4.0+、.Net 5、.Net 6 框架的 C# WinForm 开源控件库、工具类库、扩展类库、多页面开发框架。此版本更新内容为：+ 增......
### [MatrixOne 0.2.0 发布，最快的 SQL 计算引擎来了](https://www.oschina.net/news/182290/matrixone-0-2-0-released)
> 概要: 在数月的打磨和努力开发之下，MatrixOne 0.2版本正式发布啦！项目文档网站：https://docs.matrixorigin.io/0.2.0/相比于0.1版本，0.2版本在以下几方面有着明......
### [字符作画，我用字符画个冰墩墩](https://segmentfault.com/a/1190000041398208?utm_source=sf-homepage)
> 概要: 文章持续更新，可以关注公众号程序猿阿朗或访问未读代码博客。本文Github.com/niumoo/JavaNotes已经收录;欢迎Star。哈喽，大家好啊，我是阿朗。已经 2022 年了，最近北京冬奥......
### [What does it mean to listen on a port?](https://paulbutler.org/2022/what-does-it-mean-to-listen-on-a-port/)
> 概要: What does it mean to listen on a port?
### [展开操作符：一家人就这么被拆散了](https://segmentfault.com/a/1190000041399412?utm_source=sf-homepage)
> 概要: 大家好，我卡颂。想必大家在业务中应该经常使用展开操作符（Spread syntax），比如展开数组：function sum(x;y;z) {  return x + y + z;}const num......
### [The once-extinct aurochs may soon roam Europe again](https://www.atlasobscura.com/articles/aurochs-rewilding)
> 概要: The once-extinct aurochs may soon roam Europe again
### [情人节被猫拿捏了，95后跟猫当“情侣”](https://www.huxiu.com/article/497775.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜苗正卿题图｜视觉中国“猫比男人靠谱。谈恋爱太麻烦，有的男人一旦成为对象就不可靠，我谈过6段恋爱，现在单身，养了4只猫。”2000年出生的女生莉莉告诉虎嗅，今年情人节她......
### [《鬼灭之刃》刀匠之村篇制作决定 霞柱、恋柱参战](https://acg.gamersky.com/news/202202/1459664.shtml)
> 概要: 《鬼灭之刃》刀匠之村篇决定制作TV动画。与此同时官方还放出了预告视频以及主视觉图。
### [「进击的巨人」三笠・阿克曼粘土人开订](http://acg.178.com/202202/438802525225.html)
> 概要: Good Smile Company作品「进击的巨人」三笠・阿克曼粘土人现已开订，手办高约100mm，主体采用ABS与PVC材料制造，原型师为伊藤灵一。该手办定价为5,500日元（含税），约合人民币3......
### [「关于邻家的天使大人不知不觉把我惯成了废人这档子事」公开全新角色视觉图](http://acg.178.com/202202/438807821903.html)
> 概要: TV动画「关于邻家的天使大人不知不觉把我惯成了废人这档子事」官方公开了女主角椎名真昼的全新视觉图以及一张情人节纪念壁纸。TV动画「关于邻家的天使大人不知不觉把我惯成了废人这档子事」改编自佐伯さん创作、......
### [「DEEMO 樱花之音 -你所弹奏的声音，至今仍在回响-」60秒预告PV公开](http://acg.178.com/202202/438808597512.html)
> 概要: 近日，由SIGNAL.MD和Production I.G联合制作的动画电影「DEEMO 樱花之音 -你所弹奏的声音，至今仍在回响-」公开了60秒的预告PV，该作将于2月25日在日本上映。「DEEMO ......
### [动画「公主连结！Re:Dive」第二季最新彩图公开](http://acg.178.com/202202/438808736011.html)
> 概要: 电视动画「公主连结！Re:Dive」第二季目前正在热播中，官方公开了最新绘制的彩图。电视动画「公主连结Re:Dive」改编自同名游戏作品，由CygamesPictures负责动画制作。本作第一季已于2......
### [韩星金宣虎为白血病患儿捐款 私生活风波后拍新片](https://ent.sina.com.cn/s/j/2022-02-14/doc-ikyamrna0666186.shtml)
> 概要: 新浪娱乐讯 韩国艺人金宣虎近日被爆料去年12月为患有白血病的儿童们捐献了5000万韩元（约合人民币27万元）。　　今天有媒体爆料金宣虎去年12月为某白血病儿童基金捐献5000万韩元，不久后金宣虎的经纪......
### [奇迹梦幻联动！EVA×假面骑士×奥特曼×哥斯拉](https://acg.gamersky.com/news/202202/1459733.shtml)
> 概要: 庵野秀明所在的khara公司今天宣布，将开启一个奇迹的梦幻联动，那就是让代表了日本英雄文化的EVA、假面骑士、奥特曼和哥斯拉同台。
### [《天穗之咲稻姬》线上LIVE预告片 2月19日开演](https://www.3dmgame.com/news/202202/3835702.html)
> 概要: 4GamerSP今日公开了《天穗之咲稻姬》公开线上LIVE活动预告片，本次LIVE时间为北京2月19日19点至2022年2月28日(星期一)22:59结束。LIVE的MC将由游戏本篇主角佐久名的CV大......
### [VC/PE打响农业投资战](https://www.huxiu.com/article/497926.html)
> 概要: 本文来自微信公众号：投中网（ID：China-Venture），作者：曹玮钰，头图来自：视觉中国VC/PE似乎又在酝酿一个新故事。几天前和国内某基金的农业早期投资人朋友聊天，对方说，过去半年，保守算已......
### [中国青年创业发展报告（2021）](https://www.huxiu.com/article/497865.html)
> 概要: 青年是创新创业的生力军。为贯彻落实党中央、国务院“大众创业、万众创新”“乡村振兴”战略部署，进一步推动青年创业发展，中国青年创业就业基金会与泽平宏观联合开展青年创业专题研究。中国创业情况整体蓬勃发展，......
### [The harmful consequences of the robustness principle](https://www.ietf.org/archive/id/draft-iab-protocol-maintenance-05.html)
> 概要: The harmful consequences of the robustness principle
### [TV动画《鬼灭之刃》续篇制作决定！](https://news.dmzj.com/article/73589.html)
> 概要: 根据吾峠呼世晴原作制作的TV动画《鬼灭之刃》宣布了续篇制作决定的消息。
### [《DEEMO Memorial Keys》60秒PV  本月上映](https://www.3dmgame.com/news/202202/3835709.html)
> 概要: 音乐游戏《Deemo（古树旋律）》改编动画电影《DEEMO Memorial Keys》官方今日公开60秒新PV，电影将于2022年2月25日于日本上映，由SIGNAL.MD与PRODUCTION I......
### [米哈游成立新品牌HoYoverse！为玩家提供沉浸式虚拟世界体验](https://news.dmzj.com/article/73590.html)
> 概要: 米哈游宣布了成立新品牌“HoYoverse”的消息。本品牌将以让全世界玩家体验沉浸式虚拟世界为目标展开。
### [Facebook Messenger blocks messages containing “di.wang”](https://twitter.com/atpCountry/status/1493103408462454787)
> 概要: Facebook Messenger blocks messages containing “di.wang”
### [春节档票房同比下滑23% 专家：票价偏高类型局限](https://ent.sina.com.cn/m/c/2022-02-14/doc-ikyamrna0702479.shtml)
> 概要: 春节档是目前国内最大的电影档期。数据显示，刚刚过去的春节长假7天，全国电影票房60.35亿元，相较去年的78.43亿元下滑了23%。截至2月11日，有三部影片的片方相继宣布降价，分别是电影《狙击手》、......
### [日本一游戏《夜廻三》公开宣传视频](https://news.dmzj.com/article/73591.html)
> 概要: 日本一在本日（14日）公开了预计于4月21日发售的游戏《夜廻三》的新宣传视频。在这段视频中，可以看到在夜里探索的主人公和有关故事梗概的介绍。
### [23只猴子植入芯片死了15只 马斯克脑机公司被喷](https://www.3dmgame.com/news/202202/3835727.html)
> 概要: 还记得之前世界首富马斯克创立的脑机接口公司么？不过理想很丰满，现实很骨感，参与实验的23只猴子因为植入芯片已经死了15只，而且马斯克也面临虐待猴子的强烈抗议控诉。•事情要从2016年说起。马斯克和其他......
### [前暴雪开发者：微软收购暴雪后 《魔兽争霸4》更有戏](https://www.3dmgame.com/news/202202/3835728.html)
> 概要: 早先《魔兽争霸3：重制版》推出后，暴雪曾表示没有开发《魔兽争霸4》的计划，这让许多粉丝感到很失望。最近微软宣布斥资近700亿美元收购动视暴雪，将多款知名游戏IP收入囊中，其中包括《暗黑破坏神》《星际争......
### [数据表明 AMD RX 6000 系列欧洲显卡均价已降至 13 个月前水平](https://www.ithome.com/0/602/926.htm)
> 概要: IT之家2 月 14 日消息，根据德国和奥地利统计机构 3DCenter.org 最新的 GPU 报告，在过去几个月中，NVIDIA RTX 30 和 AMD RX 6000 显卡的价格都出现了大幅下......
### [Android 13来了，它真的平庸又鸡肋吗？](https://www.tuicool.com/articles/Nb2U7fi)
> 概要: 本文来自微信公众号：InfoQ（ID：infoqchina），作者：RON AMADEO，译者：核子可乐，编辑：燕珊，头图来自：unsplash虽然不久前 Android 12L beta 版才亮相，......
### [万代新蝙蝠车拼装模型 纪念3月《新蝙蝠侠》上映](https://www.3dmgame.com/news/202202/3835732.html)
> 概要: 系列电影最新作《新蝙蝠侠》即将于3月4日北美上映，4月19日登陆HBO Max，日前万代魂系模型部推出影片中登场的全新蝙蝠车拼装模型，价格实惠，粉丝们可以关注下了。•《新蝙蝠侠》是一部紧张刺激、拥有大......
### [吴倩张雨剑情人节官宣离婚！女方自称单身姑娘 男方称还是好友](https://new.qq.com/rain/a/ENT2022021400327900)
> 概要: 腾讯娱乐讯 2月14日情人节当天，吴倩宣布与张雨剑结束婚姻关系。            女方发文自称单身姑娘，男方则发文称：“感恩曾经美好的相遇，未来漫漫长路，我们还是孩子的父母与彼此最亲密的好友。”......
### [视频：网红孙一宁坐飞机被普信男骚扰 泼对方一脸奶茶并报警](https://video.sina.com.cn/p/ent/2022-02-14/detail-ikyamrna0732748.d.html)
> 概要: 视频：网红孙一宁坐飞机被普信男骚扰 泼对方一脸奶茶并报警
### [视频：许玮甯婚后首次公开露面 用四个字幽默回应阮经天恋情](https://video.sina.com.cn/p/ent/2022-02-14/detail-ikyakumy5908134.d.html)
> 概要: 视频：许玮甯婚后首次公开露面 用四个字幽默回应阮经天恋情
### [小S透露大S近况 并称姐姐一直有演员梦](https://ent.sina.com.cn/s/h/2022-02-14/doc-ikyamrna0734138.shtml)
> 概要: 新浪娱乐讯 据台媒报道，2月14日，小S出席节目开播的记者会，她在记者会上透露了大S的近况，并大赞姐姐大S状态非常好。　　大S离婚后近况备受关注，小S透露她气色好到连许雅钧都吓到：“我老公看到她都很惊......
### [小鹏 P5 新增 2 种续航 4 款车型，补贴后售 16.47 万起](https://www.ithome.com/0/602/943.htm)
> 概要: IT之家2 月 14 日消息，小鹏 P5 新增 460G+、460E+、510G、510E 2 种续航 4 款车型，补贴后售价为 16.47 万元、18.47 万元、17.33 万元、19.33 万元......
### [狂破纪录！2021 年科技巨头们赚翻了](https://www.tuicool.com/articles/Zb6RBfF)
> 概要: 2021 年太不容易了。新冠疫情仍在张牙舞爪，线下市场被割裂成孤岛，经济就业市场不断波动，大公司们也面临着供应链问题、半导体短缺等种种危机……但是，全球最大的科技巨头们，依然赚翻了。▲ 图片来自：Fa......
### [剧场版《擅长捉弄人的高木同学》初夏上映](https://news.dmzj.com/article/73593.html)
> 概要: 根据山本崇一朗原作制作的剧场版《擅长捉弄人的高木同学》宣布了将于今夏上映的消息。本作的一段CM也一并公开。
### [啫喱并未抢到元宇宙门票](https://www.huxiu.com/article/497958.html)
> 概要: 出品｜虎嗅商业、消费与机动组作者｜黄青春题图｜视觉中国2022年伊始，啫喱俨然成了年轻人的新社交阵地——抖音、微博、小红书上突然涌现了大量“打卡帖”推荐这款熟人社交App。这款主打潮玩虚拟形象的社交产......
### [设计师第一次做可用性测试，这篇文章就够了！](https://www.tuicool.com/articles/feMbuaR)
> 概要: 全流程负责与参与设计调研是设计师们的日常工作任务，其流程的繁琐与复杂性时常令人抓耳挠腮。本文作者结合自身学习与实操经验，与大家分享设计调研方法，其中重点讲解可用性测试方法，希望帮助更多的设计师去了解与......
### [《鬼灭之刃》第二季完结，上弦会议没完成，但刀匠村篇制作决定](https://new.qq.com/omn/20220214/20220214A0AJG100.html)
> 概要: 首先恭喜《鬼灭之刃》的第二季游郭篇完结撒花，宇髄天元和三小只能杀掉上弦六是真的不容易。每个人都身负重伤，除了善逸君好一点，其他三人都差点人没了。            大家看起来都很惨，特别是宇髄天元......
### [王炸！四个经典IP联动：新奥、假面、哥斯拉、EVA要统一世界观？](https://new.qq.com/omn/20220214/20220214A0AOPW00.html)
> 概要: 近日，一则近乎“王炸”的消息开始在网络上传开而最终得到确认，那就是由日本的和四家公司将联合宣布一项联动，而该联动已经命名为“”。            然而，根据该联动名的推测，似乎这四家公司要一起合......
### [斗罗大陆：唐三小舞情人节专属Logo出炉，确认过武魂，遇上对的人](https://new.qq.com/omn/20220214/20220214A0AVCO00.html)
> 概要: 其实很多时候一部动漫之所以能够做到深入人心的效果，也并不是单纯的因为动漫本身的质量无人能及，毕竟被埋没的优秀动漫也有很多比斗罗大陆更加优秀的，奈何大家也就只能记住了斗罗大陆，对于这个问题可能很多人都不......
### [外星人推出 AW720M 游戏鼠标：三模连接，磁吸快充，8 个自定义键](https://www.ithome.com/0/602/961.htm)
> 概要: IT之家2 月 14 日消息，外星人现已推出 AW720M 游戏鼠标，支持三模连接，磁吸快充，售价 1399 元起。IT之家了解到，外星人 AW720M 游戏鼠标支持有线、无线、蓝牙三种连接模式；搭载......
### [程序员挖“洞”致富：发现一个漏洞，获赏 1272 万元](https://www.tuicool.com/articles/fyEVju6)
> 概要: 适合程序员的副业有什么？写文章、当技术辅导、开发小程序、接植发代言（bushi）…许多人通过这些副业，长年累月下来，逐渐实现了理想中的“经济自由”。在这之中，还有一种非常受欢迎的业余赚钱方式：“专找别......
### [八部门：加强网约车行业事前事中事后全链条联合监管](https://finance.sina.com.cn/china/2022-02-14/doc-ikyamrna0764090.shtml)
> 概要: 中新经纬2月14日电 据交通运输部官方微信14日消息，近日，交通运输部、工信部、公安部、人社部、中国人民银行、国家税务总局、国家市场监管总局...
### [多所高校新增“双一流”建设学科，部分高校建设学科有调整](https://finance.sina.com.cn/china/gncj/2022-02-14/doc-ikyamrna0764008.shtml)
> 概要: 澎湃新闻记者 程婷 2月14日，教育部、财政部、国家发展改革委公布《第二轮“双一流”建设高校及建设学科名单》。澎湃新闻对比首轮“双一流”建设学科名单发现...
### [国家卫健委答委员提案：将加大临床受试者个人信息数据保护](https://finance.sina.com.cn/china/2022-02-14/doc-ikyamrna0764645.shtml)
> 概要: 21世纪经济报道记者郭美婷 广州报道 近年来，我国医疗健康行业正加速数字化转型。随着《数据安全法》《个人信息保护法》等落地，医疗领域的数据安全...
### [9省密集发布DRG/DIP改革三年行动计划 医保支付将迎“天花板”](https://finance.sina.com.cn/china/2022-02-14/doc-ikyamrna0765267.shtml)
> 概要: 21世纪经济报道记者尤方明北京报道2021年11月，国家医保局发布《DRG/DIP支付方式改革三年行动计划》（下称《国家医保局三年行动计划》）。
### [深圳：供港物资货源充足 鲜活物资与去年同期大体持平](https://finance.sina.com.cn/jjxw/2022-02-14/doc-ikyakumy5940558.shtml)
> 概要: 近日，深港两地均有跨境司机作为病例或者密接者被隔离，深圳市商务局副局长池卫国在深圳今天（14日）召开的疫情防控发布会上介绍...
### [第二轮“双一流”名单公布：新增七所高校，逐步淡化“双一流”身份色彩](https://finance.sina.com.cn/china/2022-02-14/doc-ikyakumy5939592.shtml)
> 概要: 21世纪经济报道王峰报道 教育部官网刚公布第二轮“双一流”名单还不到半小时，湘潭大学的微信公众号就沸腾了。 湘潭大学公号发出的《官宣！官宣！官宣！
### [镜双城结局：珍珠泪牵手戏被删，苏摩去往苍梧渊，真岚才是亲儿子](https://new.qq.com/omn/20220214/20220214A0BVBP00.html)
> 概要: 终于是等到了《镜双城》动漫第一季的大结局，对于这部作品，本来很多观众看得时候是很不看好的，尤其是原著党，觉得动漫魔改的东西太多了。但是架不住李易峰和陈钰琪主演的真人剧魔改的更多，连人设也都改了，里面人......
### [Android 13 立功！谷歌 Pixel 6 成功运行 Win11 虚拟机，安卓反向拥有了 Win11 子系统](https://www.ithome.com/0/602/970.htm)
> 概要: 感谢IT之家网友大药师的线索投递！IT之家2 月 14 日消息，微软在Win11中推出了安卓子系统 WSA，基于 Hyper-V 虚拟化平台，这个大家都不陌生。而谷歌在 Chrome OS 中也采用了......
# 小说
### [诡异世界的进化大神](https://m.qidian.com/book/1024517326/catalog)
> 作者：小胡歌

> 标签：奇妙世界

> 简介：新书《魔教大佬，毒药使我强壮！》已上传！新书《魔教大佬，毒药使我强壮！》已上传！新书《魔教大佬，毒药使我强壮！》已上传！这是一个在诡异世界，凭借着加点系统和智慧成为一方巨佬的故事…

> 章节总数：共546章

> 状态：完本
# 论文
### [LongChecker: Improving scientific claim verification by modeling full-abstract context | Papers With Code](https://paperswithcode.com/paper/longchecker-improving-scientific-claim)
> 日期：2 Dec 2021

> 标签：None

> 代码：None

> 描述：We introduce the LongChecker system for scientific claim verification. Given a scientific claim and an evidence-containing research abstract, LongChecker predicts a veracity label and identifies supporting rationales in a multitask fashion based on a shared encoding of the claim and abstract.
### [Hierarchical Few-Shot Generative Models | Papers With Code](https://paperswithcode.com/paper/hierarchical-few-shot-generative-models)
> 日期：23 Oct 2021

> 标签：None

> 代码：None

> 描述：A few-shot generative model should be able to generate data from a distribution by only observing a limited set of examples. In few-shot learning the model is trained on data from many sets from different distributions sharing some underlying properties such as sets of characters from different alphabets or sets of images of different type objects.
