---
title: 2023-11-03-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SeaNettles_ZH-CN1735729435_1920x1080.webp&qlt=50
date: 2023-11-03 16:46:46
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SeaNettles_ZH-CN1735729435_1920x1080.webp&qlt=50)
# 新闻
### [Nat Cancer：药物伊美司他有望治疗人类急性髓性白血病和其它类型癌症](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx4303389753735&cate_id=-1)
> 概要: 来自澳大利亚QIMR Berghofer医学研究所等机构的科学家们通过研究有望开启一种靶向作用急性髓性白血病的全新疗法，或能为那些对当前疗法不再产生反应的患者带来一定的治疗希望......
### [New gonorrhea antibiotic shows promise in pivotal phase 3 trial](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx3558867239296&cate_id=-1)
> 概要: 一种迫切需要的治疗淋病感染的新抗生素可能很快就会出现......
### [What’s the Best Time To Eat Dinner?](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx4366421754249&cate_id=-1)
> 概要: 一项研究表明，晚餐吃得晚会增加你患肥胖症的几率......
### [国家卫健委：全链条改善患者就医感受](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1841534632312&cate_id=-1)
> 概要: “一些大城市比如北京，通过调整上下午门诊的出诊医生数量，使出诊医生数过于集中的状况有了明显改善。”11月1日，国新办举行“权威部门话开局”系列主题新闻发布会，国家卫生健康委副主任雷海潮在会上介绍，为期......
### [Mindfulness Training Helps People Eat a More Heart-Healthy Diet](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1195326603666&cate_id=-1)
> 概要: 自我意识和自我友善可能有助于支持我们改变生活方式的努力......
### [低价不是京东的终极答案](https://www.woshipm.com/it/5933986.html)
> 概要: 今年的双十一大促活动正开展得如火如荼，相似的低价打法更是让众平台回到同一起跑线，平台需要重新面临突出重围的考验。那么当低价成为了电商标配，京东要如何突围？这篇文章里，作者发表了他的解读和分析，一起来看......
### [电视「沉浮录」：跌出家电“三大件”？](https://www.woshipm.com/share/5934043.html)
> 概要: 是什么让电视机的销量下降了？随着科技的发展，技术的革新，电视机的功能也不例外变得花样。那究竟还需要需要电视机了呢？为什么？看一看下面的文章，或许能够得到解答哦！“这年头谁还看电视，家里电视近一年都没打......
### [如何提升注册及订阅率，降低召回成本：13项步骤要点与10个实用案例分享（上）](https://www.woshipm.com/share/5933582.html)
> 概要: 我们在用邮箱的时候。有时会有弹窗出现。本文笔者整理关于如何提升注册及订阅率从而降低召回成本的13项步骤要点与10个实用案例的分享，大家一起来看看。虽然弹窗在电商邮件营销中并不总是受到欢迎，毕竟它们有时......
### [“减肥神药”新适应症已在路上？高管透露半年内见分晓](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imzthcsy5062517.shtml)
> 概要: 财联社......
### [马斯克旗下星链已实现盈亏平衡 上市之日渐行渐近？](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imzthiyw4935047.shtml)
> 概要: 财联社......
### [动视回应《使命召唤20》文件过大问题：首发内容太多](https://www.3dmgame.com/news/202311/3880802.html)
> 概要: 《使命召唤20：现代战争3》战役抢先体验已上线，有玩家发现PS5版安装总容量达到令人瞠目的234.9GB。安装包似乎包括《现代战争2》、《现代战争3》战役，以及HQ启动器。还不包括《现代战争3》其他未......
### [《宝可梦:朱/紫》DLC后篇“蓝之圆盘”12月14日上线](https://www.3dmgame.com/news/202311/3880806.html)
> 概要: 《宝可梦：朱/紫》DLC“零之秘宝”后篇“蓝之圆盘”确定将于2023年12月14日上线，“零之秘宝”的前篇“碧之假面”已于今年9月上线。在后篇“蓝之圆盘”中，玩家将作为交换生前往蓝莓学院，这是玩家自己......
### [三季报业绩超预期 促A.O.史密斯涨9.95% 家电板块估值修复可期？](https://www.yicai.com/video/101894475.html)
> 概要: 三季报业绩超预期 促A.O.史密斯涨9.95% 家电板块估值修复可期？
### [海外 New Things | Engine Biosciences获2700万美元A轮融资，打造精准医学研发平台生产肿瘤药物](https://36kr.com/p/2499819692583172)
> 概要: 海外 New Things | Engine Biosciences获2700万美元A轮融资，打造精准医学研发平台生产肿瘤药物-36氪
### [《都市朋克2011爱的重拳》Steam页面 发售日待定](https://www.3dmgame.com/news/202311/3880814.html)
> 概要: 今日（11月3日），剧情游戏《S4U: 都市朋克2011与爱的重拳》Steam页面上线，发售日期待定，感兴趣的玩家可以点击此处进入商店页面。游戏介绍：这是一场成年人关于爱和意义的现代冒险。重返2011......
### [苹果概念股走强 机构看好消费电子板块新一轮成长](https://www.yicai.com/news/101894539.html)
> 概要: 截至发稿，朝阳科技五连板，奥普特涨超8%，安达智能、凌云光、田中精机等多股跟涨。
### [EA称《EA FC 24》将成为面向球迷的未来互动平台](https://www.3dmgame.com/news/202311/3880818.html)
> 概要: 最近根据EA官方高层的说法，《EA SPORTS FC™ 24》的推出，成功地将曾经的“FIFA”系列游戏，变成了“面向球迷的未来互动平台”。虽然此前游戏出现过十分滑稽的“真·带球过人”BUG，导致很......
### [安科生物曲妥珠单抗获批上市，背靠大树的安科丽如何扬长避短？](http://www.investorscn.com/2023/11/03/111548/)
> 概要: 2023年 10 月 27 日，由安徽安科生物工程（集团）股份有限公司自主研发的注射用曲妥珠单抗正式获批上市，这是安科生物在抗肿瘤靶向药物领域布局的首款产品，历经10 余年研发，投入研发经费近 10 ......
### [三季度净利润翻2.4倍，和中国对手第一战，亚马逊赢了｜焦点分析](https://36kr.com/p/2500794238150915)
> 概要: 三季度净利润翻2.4倍，和中国对手第一战，亚马逊赢了｜焦点分析-36氪
### [稀物集单品详情&相宜本草线上视觉](https://www.zcool.com.cn/work/ZNjY5NTE3MDg=.html)
> 概要: 机构：nofans® 设计工作室主理人：徐适平面设计：大帅 大兵三维渲染：小鹏 小陶项目监督：书恬项目统筹：得莫利摄影：一芥CollectCollectCollectCollectCollectCol......
### [紫京科技与国贸股份签署战略合作协议，双方达成深度合作](http://www.investorscn.com/2023/11/03/111551/)
> 概要: 11月2日，厦门紫京科技有限责任公司（简称紫京科技）与厦门国贸集团股份有限公司（简称国贸股份）签署战略合作协议，双方达成长期深度合作伙伴关系。莆田市仙游县人民政府党组成员、副县长林金涛，国贸股份总裁助......
### [姐姐证实李玟曾两次轻生 得知丈夫出轨后才立遗嘱](https://ent.sina.com.cn/s/h/2023-11-03/doc-imztifeu3871588.shtml)
> 概要: 新浪娱乐讯 3日，李玟的两位姐姐在接受采访时透露李玟立遗嘱是因为在2017年发现丈夫Bruce出轨，而不是因为自己的身体状况。二姐透露李玟离世前一天晚上还在和好友们欢聚唱歌。出事时那天，Bruce正在......
### [TV动画《华Doll*》公开最新情报](https://news.dmzj.com/article/79499.html)
> 概要: TV动画《华Doll*》公开最新情报。
### [漫画·动画媒体会“IMART2023”即将召开](https://news.dmzj.com/article/79500.html)
> 概要: 漫画·动画媒体会“IMART2023”公开最新情报，11月24日至26日在线上举行。
### [高斯林动作喜剧《特技狂人》预告 明年三月上映](https://www.3dmgame.com/news/202311/3880833.html)
> 概要: 继《芭比》之后，瑞恩·高斯林再次将登上萤幕，出演动作喜剧《特技狂人（TheFall Guy）》。他在本片中将饰演一名动作电影片场中常见的替身特技演员，不仅要进行各种危险的表演，还要寻找一位失踪的电影明......
### [鍋敷《俺は全てを【パリイ】する ～》动画化决定！](https://news.dmzj.com/article/79501.html)
> 概要: 鍋敷《俺は全てを【パリイ】する ～》动画化决定。
### [钉钉AI正式公测，17款产品60+场景，超50万家企业启用 | 最前线](https://36kr.com/p/2502072560592771)
> 概要: 钉钉AI正式公测，17款产品60+场景，超50万家企业启用 | 最前线-36氪
### [通义千问发布半年，大模型已不是阿里云唯一主角 | 焦点分析](https://36kr.com/p/2502143578006916)
> 概要: 通义千问发布半年，大模型已不是阿里云唯一主角 | 焦点分析-36氪
### [指数全线大涨北向流入超70亿 大盘能否看高一线？](https://www.yicai.com/video/101894820.html)
> 概要: 指数全线大涨北向流入超70亿 大盘能否看高一线？
### [TVC视频 | 比音勒芬极肤2.0羽绒服](https://www.zcool.com.cn/work/ZNjY5NTM1NDg=.html)
> 概要: Foodography Visual Lab / Since 2016我们是一家以数字化创意为驱动的创新型品牌视觉服务公司 以电商产品视觉升级服务为核心，致力于为客户提供多样化的数字化视觉和视觉内容解......
### [亚信科技与英特尔达成全球战略合作 联合发布面向海外市场的多款软硬一体产品](http://www.investorscn.com/2023/11/03/111552/)
> 概要: 11月2日，亚信科技（中国）有限公司「以下简称：亚信科技」与英特尔公司「以下简称：英特尔」在京达成全球战略合作。在亚信科技执行董事、首席执行官高念书，英特尔市场营销集团副总裁、中国区总经理王稚聪见证下......
### [大促过后吃点土：鱼泉榨菜 8.9 元 1.2 斤近期好价（京东 26 元）](https://lapin.ithome.com/html/digi/729829.htm)
> 概要: 天猫【鱼泉旗舰店】鱼泉榨菜 15g*40 袋共计 600g，标价 16.8 元，下单领取 5 元优惠券，首次购买用户叠加 1 元首购礼金，到手价为 8.9 元，折合每袋仅需约 0.22 元：天猫鱼泉 ......
### [组图：杨幂最新时尚大片释出 薄纱遮面烈焰红唇优雅迷人](http://slide.ent.sina.com.cn/star/slide_4_704_389546.html)
> 概要: 组图：杨幂最新时尚大片释出 薄纱遮面烈焰红唇优雅迷人
### [一加 11 手机开启 ColorOS 14.0 升级公测版本招募，截至 11 月 6 日](https://www.ithome.com/0/729/838.htm)
> 概要: IT之家11 月 3 日消息，一加 11 开启 ColorOS 14.0 ×Android 14升级公测版本招募，本次升级方式为系统直接推送，报名成功并通过审核后会陆续收到推送。招募时间：11 月 3......
### [英国将投资3亿英镑使AI超算能力增强30倍以上](https://www.yicai.com/news/101894875.html)
> 概要: 英国科学、创新和技术大臣米歇尔·唐兰2日在英国布莱奇利园举办的人工智能安全峰会上宣布，将在英国建造并连接两台新的超级计算机。
### [组图：狗仔曝王鸥还有一两个月生产 透露其与何九华还在一起](http://slide.ent.sina.com.cn/star/slide_4_704_389548.html)
> 概要: 组图：狗仔曝王鸥还有一两个月生产 透露其与何九华还在一起
### [AITO问界新M7销量创历史新高，赛力斯汽车增程技术广受用户追捧](http://www.investorscn.com/2023/11/03/111559/)
> 概要: 11月伊始,各大新能源汽车品牌相继发布了10月的销售成绩,几家欢喜几家愁。增程技术虽然经常被讨论到底是落后还是先进,但从销量来看是取得了消费者的广泛认可。其中,2016年便开始布局增程技术的赛力斯汽车......
### [海外权威媒体聚焦：波场TRON携手ChainGPT Web3版图再度革新](http://www.investorscn.com/2023/11/03/111560/)
> 概要: 近日,波场TRON宣布与Web3 人工智能基础设施服务商 ChainGPT 达成合作,引发全球知名媒体高度关注。国际权威财经媒体金融时报、彭博社、法国第一大报费加罗报、美国第一大通讯社美联社等多家海外......
### [人形机器人产业迎政策利好，到2025年整机产品实现量产](https://www.yicai.com/news/101894864.html)
> 概要: 在政策和技术的双重加持下，人形机器人在工业、医疗、家庭服务等领域具有巨大应用空间。
### [流言板TA：曼联更衣室对滕哈格不让瓦拉内首发+对卡塞米罗评论不满](https://bbs.hupu.com/622809282.html)
> 概要: 虎扑11月03日讯 The Athletic UK曼联跟队记者团队回答关于滕哈格未来的问题。球员们还支持滕哈格吗？俱乐部的高层可能会坚持让滕哈格留任，但球队内部的不安情绪越来越强烈，一些球员开始对他失
### [亚马逊抬高价格算法 Nessie 曝光，已为其赚取 10 亿美元](https://www.ithome.com/0/729/879.htm)
> 概要: IT之家11 月 3 日消息，根据美国联邦贸易委员会（FTC）披露的一份文档，亚马逊通过名为 Project Nessie 的秘密算法，赚取了超过 10 亿美元（IT之家备注：当前约 73.2 亿元人......
### [流言板记者：费莱尼暂时应该不会退役，估计崔康熙口误了](https://bbs.hupu.com/622809361.html)
> 概要: 虎扑11月03日讯 据记者汤幸报道，崔康熙说费莱尼即将退役可能是口误。汤幸写道：费莱尼暂时应该不会退役。 发布会上估计崔康熙口误了，把离开说成了退役。 一切以费莱尼社媒为准吧。相关阅读主帅崔康熙确认：
### [一图流Xun皇子抓下击杀Peyz厄斐琉斯，但Peyz塔下换掉ON烈娜塔](https://bbs.hupu.com/622809369.html)
> 概要: 虎扑11月03日讯 GEN对阵BLG首局，Xun皇子抓下越塔击杀Peyz厄斐琉斯，但ON烈娜塔抗塔过多被换掉。   来源： 虎扑
### [到2025年北京力争实现新能源供热面积占比达到10%](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imztiruh4335252.shtml)
> 概要: 转自：北京商报北京商报讯（记者 方彬楠 实习记者 程靓）11月3日，记者从北京市发展改革委处获悉，近日，北京市发展改革委、北京市规划自然资源委等十部门研究制定了《关于...
### [JetBrains 推出开发工具 Kotlin Multiplatform 首个稳定版，强调“跨平台代码复用”](https://www.ithome.com/0/729/882.htm)
> 概要: IT之家11 月 3 日消息，JetBrains 日前正式推出了跨平台开发工具 Kotlin Multiplatform（KMP），让开发人员可以在不同平台间复用代码，但同时又能保留“原生应用”的优势......
### [工业和信息化部：到2025年，人形机器人创新体系初步建立](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imztirui9657588.shtml)
> 概要: 转自：中国发展网中国发展网讯  近日，工业和信息化部印发《人形机器人创新发展指导意见》（下文简称《指导意见》）。《指导意见》按照谋划三年...
### [流言板马祖拉：塔特姆增加了肌肉，双J的无球能让攻防更上一个台阶](https://bbs.hupu.com/622809471.html)
> 概要: 虎扑11月03日讯 凯尔特人主教练乔-马祖拉接受媒体采访，谈到了凯尔特人球员杰森-塔特姆。“塔特姆增加了12磅的肌肉，他现在在进攻时的节奏也变得更好了。”乔-马祖拉在采访中表示。“他一直在阅读对方的防
### [谢松民任新乡市委副书记、政法委书记](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imztirui9659352.shtml)
> 概要: 转自：中国经济网综合 中国经济网新乡11月3日综合报道 据河南新乡公安网消息，10月30日下午，省委第二十巡视组对新乡市公安党委开展巩固政法队伍教育整顿成果专项巡视工作...
### [新华指数|本期新华·玉林肉桂价格指数运行平稳](https://finance.sina.com.cn/money/bond/2023-11-03/doc-imztirun6926401.shtml)
> 概要: 新华财经北京11月3日电（李伯罧）根据监测数据显示，截至10月30日，新华·玉林八角价格指数报974.56点，较10月23日下跌63.20点，跌幅6.09%；较基期（2022年5月9日）下跌25...
### [青浦区召开重点企业“服务包”工作动员部署会](https://finance.sina.com.cn/jjxw/2023-11-03/doc-imztirun6927298.shtml)
> 概要: 转自：上观新闻为贯彻落实市委、市政府和区委区政府的工作要求，顺利推进重点企业“服务包”工作贯彻执行，昨天（11月2日）下午，青浦区重点企业“服务包”工作动员部署会在区...
### [流言板恩比德：我们每回合都让哈登处理球，他也拿到了助攻王](https://bbs.hupu.com/622809582.html)
> 概要: 虎扑11月03日讯 NBA常规赛，76人主场114-99战胜猛龙，赛后76人中锋乔尔-恩比德接受采访谈起了前队友詹姆斯-哈登。在此前哈登加盟快船的采访中，他曾表示76人改变了他的球队地位，他感觉自己像
# 小说
### [请君入劫](http://book.zongheng.com/book/860022.html)
> 作者：五变

> 标签：武侠仙侠

> 简介：鸿蒙初开,大道孕生天地万物,人为万物之灵.此时大道昌盛,人在道中如同鱼游水里.人与道合,长寿者比比皆是,百岁亡而称殇千岁才是暮年.仙路坦荡,养性修身即可成仙,后世人称鸿蒙仙.然,岁月流转,亿万年来大道流失,人心不古诡变百出,与道渐行渐远，不再为天地眷顾.四十不惑六十已花甲百岁可望不可求.仙路变得坎坷艰难,人欲成仙必要历经千难万险.纵如此也不能为天地认同,每百年千年或万年不等降下一次天劫,修士则炼就各种翻天覆地的法宝来抵御天劫.成仙从修身养性顺应自然变成了养丹炼宝对抗天意.终于,大道震怒.据盘古石所记,宇宙中将有一次大道劫,斩尽天下不合道的人与物.人鬼妖魔神仙六界皆在劫中,而发生时间就在今日今时.

> 章节末：第一百四十章 结尾

> 状态：完本
# 论文
### [BiRdQA: A Bilingual Dataset for Question Answering on Tricky Riddles | Papers With Code](https://paperswithcode.com/paper/birdqa-a-bilingual-dataset-for-question)
> 概要: A riddle is a question or statement with double or veiled meanings, followed by an unexpected answer. Solving riddle is a challenging task for both machine and human, testing the capability of understanding figurative, creative natural language and reasoning with commonsense knowledge.
### [A Bilingual, OpenWorld Video Text Dataset and End-to-end Video Text Spotter with Transformer | Papers With Code](https://paperswithcode.com/paper/a-bilingual-openworld-video-text-dataset-and)
> 概要: Most existing video text spotting benchmarks focus on evaluating a single language and scenario with limited data. In this work, we introduce a large-scale, Bilingual, Open World Video text benchmark dataset(BOVText).

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
