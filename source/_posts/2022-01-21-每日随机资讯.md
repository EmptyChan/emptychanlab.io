---
title: 2022-01-21-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.HuggingDay_ZH-CN2984681593_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-01-21 22:00:37
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.HuggingDay_ZH-CN2984681593_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [1Password 再融资 6.2 亿美元，娱乐圈、商界名人纷纷参投](https://www.oschina.net/news/179481/1password-raises-620-million)
> 概要: 密码管理软件提供商 1Password宣布已完成 6.2 亿美元的 C 轮融资，公司估值提高至 68 亿美元。本轮融资由 ICONIQ Growth 牵头，参投方包括 Tiger Global、Lig......
### [龙芯中科加入 OpenCloudOS 操作系统社区，作为成员单位参与社区共建](https://www.oschina.net/news/179593)
> 概要: 龙芯中科有限公司作为初始成员，宣布加入OpenCloudOS操作系统开源社区。2001年，中国科学院计算技术研究所开始研制龙芯处理器，得到了中科院知识创新工程、863、973、核高基等项目大力支持，完......
### [Google 在 Windows 平台开启 Android 游戏测试，率先登录港台地区](https://www.oschina.net/news/179486/google-play-games-beta)
> 概要: 微软给 Windows 11 新增的 Windows Subsystem for Android（WSA）功能，为 Windows 设备运行 Android 应用创造了条件，但微软选择的合作对象是亚马......
### [开源数据编排平台 Apache Hop 成为 ASF 顶级项目](https://www.oschina.net/news/179487/apache-hop-top-level)
> 概要: Apache 软件基金会 (ASF) 官方宣布Apache Hop 成为 Apache 顶级项目 (TLP)。Apache Hop（Hop 是 Hop Orchestration Platform 的......
### [70% of startups offer remote work options as hiring heats up, YC data shows](https://news.crunchbase.com/news/remote-work-y-combinator/)
> 概要: 70% of startups offer remote work options as hiring heats up, YC data shows
### [中公教育巨额利润消失之谜](https://www.huxiu.com/article/492528.html)
> 概要: 本文来自微信公众号：财经十一人（ID：caijingEleven），作者：柳书琪 郑慧，编辑：谢丽容，头图来自：视觉中国整个2020年，是中国公务员考试培训公司中公教育（002607.SZ）的风光之年......
### [Sir David Cox has died](https://rss.org.uk/news-publication/news-publications/2022/general-news/sir-david-cox-1924-2022/)
> 概要: Sir David Cox has died
### [《哥斯拉》将推最新TV系列 预定Apple TV+发布](https://www.3dmgame.com/news/202201/3833899.html)
> 概要: 苹果旗下流媒体Apple TV+日前宣布，将由美国传奇影业负责制作，打造著名大怪兽IP《哥斯拉》的全新TV系列作品，预定Apple TV+发布，一起来了解下。·据悉，《哥斯拉》最新TV系列目前题名暂未......
### [TV动画+剧场版+手游！谷口悟朗原案《ESTABLIFE》详情](https://news.dmzj.com/article/73407.html)
> 概要: 由谷口悟朗负责原案的原创企划《ESTABLIFE》公开了企划的详细内容。本企划将分为TV动画、动画电影和手机游戏三平台展开。
### [TV动画「白金终局」Blu-ray&DVD第一、二卷封面插图公开](http://acg.178.com/202201/436735391853.html)
> 概要: TV动画「白金终局」公开了Blu-ray&DVD第一卷和第二卷的封面插图，第一卷目前正在发售中，第二卷将于3月16日发售。TV动画「白金终局」改编自大场鸫原作、小畑健作画的同名漫画作品，由SIGNAL......
### [组图：2022央视春晚第一次联排 颖儿头戴白色贝雷帽清新亮眼](http://slide.ent.sina.com.cn/z/v/slide_4_704_365725.html)
> 概要: 组图：2022央视春晚第一次联排 颖儿头戴白色贝雷帽清新亮眼
### [视频：福原爱祝贺刘诗雯回归 “我现在快变成你球迷了”](https://video.sina.com.cn/p/ent/2022-01-21/detail-ikyakumy1735753.d.html)
> 概要: 视频：福原爱祝贺刘诗雯回归 “我现在快变成你球迷了”
### [Spring Boot 3.0.0 M1发布](https://www.tuicool.com/articles/eQvIZvB)
> 概要: 2022年1月20日，Spring官方发布了Spring Boot 3.0.0的第一个里程碑版本M1。下面一起来来看看Spring Boot 3.0.0 M1版本都有哪些重大变化：Java基线从 Ja......
### [漫画「狂赌之渊」本月因故停刊](http://acg.178.com/202201/436737594195.html)
> 概要: 原本今日（1月21日）发售的月刊「ガンガンJOKER」2月号宣布漫画「狂赌之渊」因为器材故障停刊。漫画「狂赌之渊」于2014年03月22日在「月刊GANGAN JOKER」上开始连载，其外传漫画为「狂......
### [多台春节联欢晚会路透：芒果台选择直播，谢娜张杰番茄台合体](https://new.qq.com/rain/a/20220120A042KH00)
> 概要: 多台春节联欢晚会路透：芒果台选择直播，谢娜张杰番茄台合体
### [「Pop Up Parade」名取纱那制服手办开订](http://acg.178.com/202201/436744535455.html)
> 概要: 近日，「Pop Up Parade」模型系列名取纱那制服手办正式开启预订，作品采用ABS、PVC材质，全高约170mm，日版售价3900日元（含税），约合人民币224元，预计将于2022年5月发售......
### [Crystal Installs OpenBSD on the PinePhone](https://www.exoticsilicon.com/crystal/pinephone_openbsd)
> 概要: Crystal Installs OpenBSD on the PinePhone
### [审稿意见基于旧版本论文？ICLR 2022提前放榜，被拒理由遭吐槽](https://www.tuicool.com/articles/bmeENre)
> 概要: 1 月 21 日，深度学习顶级学术会议 ICLR 2022 录用结果放出。目前，投稿论文作者已经收到结果，但网站还没有更新。知乎上也出现了相关话题的讨论。知乎讨论：https://www.zhihu......
### [PS5《消光2》模式间画质及帧率对比 2月5日发售](https://www.3dmgame.com/news/202201/3833922.html)
> 概要: 今日（1月21日），Techland工作室公布《消逝的光芒2人与仁之战》在PS5主机上运行时画质模式对比视频，视频展示了《消逝的光芒2》分别在高性能模式、分辨率模式以及质量模式画质及帧数的表现，其中高......
### [模拟建造游戏《建立自己的王国》 今日在Steam发售](https://www.3dmgame.com/news/202201/3833925.html)
> 概要: 由yo_serjio工作室开发的城市模拟建造游戏《建立自己的王国（Make Your Kingdom）》今日（1月21日）在Steam平台发售，支持中文。游戏原价50元，首周购买享75折优惠，只需42......
### [「魔鬼统治」第4期变体封面公开](http://acg.178.com/202201/436749110244.html)
> 概要: 近日，漫威漫画官方公开了全新故事系列「魔鬼统治」第4期的变体封面，此封面由画师Gerald Parel独家绘制，登场人物是「夜魔侠」、「艾丽卡」以及「蜘蛛侠 迈尔斯·莫拉莱斯」，本期预计将于2022年......
### [互联网为什么让我们越来越不开心？](https://www.huxiu.com/article/492646.html)
> 概要: 本文来自微信公众号：腾讯研究院（ID：cyberlawrc），作者：Kyth，头图来自：pexels2022 年 1 月 11~14 日，腾讯研究院、腾讯可持续社会价值事业部联合主办的“腾讯科技向善创......
### [GSC《我的青春恋爱喜剧果然有问题。完》比企谷八幡黏土人](https://news.dmzj.com/article/73413.html)
> 概要: GSC根据动画《我的青春恋爱喜剧果然有问题。完》中的比企谷八幡制作的黏土人目前已经开订了。本作包括“通常颜”、“焦虑颜”和“害羞颜”三种表情，另外要有“书”和“咖啡罐”配件，可以再现出手插兜的姿势。
### [沈腾艾伦常远王成思央视虎年春晚彩排下班 双手插兜儿很开心](https://new.qq.com/rain/a/20220121V0654Y00)
> 概要: 沈腾艾伦常远王成思央视虎年春晚彩排下班 双手插兜儿很开心
### [三星 Galaxy Tab S8 系列偷跑：3 种尺寸，S8 Ultra 预计价格破万](https://www.ithome.com/0/599/840.htm)
> 概要: 感谢IT之家网友AMD挑战未来、肖战割割的线索投递！IT之家1 月 21 日消息，根据外媒 pocketnow 消息，三星 Galaxy Tab S8 平板电脑尚未发布，亚马逊上便提前上架，公布了全部......
### [大魔王不为人知的内心！《大王饶命》插曲《微光》发行！](https://new.qq.com/omn/ACF20220/ACF2022012100643400.html)
> 概要: 纵有辟天地、碎沉星的通天神力，这个世界上值得我追寻的却唯有善意与爱！怼天怼地怼空气、增进实力靠气人、有仇必须当场报、把人气出个好歹来还坚决不偿命的大魔王，本质竟是个心地善良、渴望温暖的小可爱？！正在腾......
### [发改委等部门：加快推进居住社区充电设施建设安装，加快换电模式推广应用](https://www.ithome.com/0/599/845.htm)
> 概要: IT之家1 月 21 日消息，国家发展改革委等部门发布关于进一步提升电动汽车充电基础设施服务保障能力的实施意见。目标到“十四五”末，我国电动汽车充电保障能力进一步提升，形成适度超前、布局均衡、智能高效......
### [赵丽颖现身央视虎年春晚联排现场 绿色围巾很亮眼](https://new.qq.com/rain/a/20220121V06L9F00)
> 概要: 赵丽颖现身央视虎年春晚联排现场 绿色围巾很亮眼
### [组图：赵丽颖现身2022央视春晚首次联排 绿色围巾搭配亮眼](http://slide.ent.sina.com.cn/z/v/slide_4_704_365733.html)
> 概要: 组图：赵丽颖现身2022央视春晚首次联排 绿色围巾搭配亮眼
### [Filmarks公开2021年秋番动画满意度排行榜](https://news.dmzj.com/article/73414.html)
> 概要: 日本影视剧播出平台Filmarks公开了2021年秋番动画满意度排行榜。这次排行榜是按照用户在观看作品后，给作品的评分计算的。
### [《开端》造型师谈服装 需要准备多套同款造型备用](https://ent.sina.com.cn/2022-01-21/doc-ikyamrmz6620801.shtml)
> 概要: 新浪娱乐讯 近日，有网友调侃电视剧《开端》是“一套服装从头拍到尾”，对此，负责该项目的造型师发博回应表示，为适配剧组拍摄节奏，呈现不同的细节，其实看似同样的服装造型，是通过在背后准备了很多套同款服装达......
### [The Tragic Rape of Charles Bukowski’s Ghost by John Martin’s Black Sparrow Press (2013)](https://mjpbooks.com/blog/the-senseless-tragic-rape-of-charles-bukowskis-ghost-by-john-martins-black-sparrow-press/)
> 概要: The Tragic Rape of Charles Bukowski’s Ghost by John Martin’s Black Sparrow Press (2013)
### [周平均涨粉7.2w B站UP主是怎么做的？](https://www.tuicool.com/articles/QjUveej)
> 概要: 关键词：飞瓜数据b站版、b站数据分析平台、b站up主粉丝排行榜、b站千瓜数据、如何查看b站粉丝排行飞瓜数据b站版统计2022年1月10日-1月16日B站平台UP主涨粉情况，涨粉榜显示周涨粉TOP1是U......
### [《骸骨骑士异世界冒险中》新预告 确定4月开播](https://www.3dmgame.com/news/202201/3833941.html)
> 概要: 根据秤猿鬼原作人气轻小说《骸骨骑士大人异世界冒险中》改编的TV动画官方1月21日今天宣布将于4月开播，同时公布了最新预告以及主艺图，一起来先睹为快。•《骸骨骑士大人异世界冒险中》讲述了男主角亚克玩线上......
### [京东开通数字人民币“硬件钱包”线上消费功能 京东科技提供技术支持](https://www.tuicool.com/articles/aiQn6nj)
> 概要: 【TechWeb】1月21日消息，伴随着2022京东年货节火热进行，近期京东App又增加了新的特色支付方式，用户可以在京东体验使用数字人民币“硬件钱包”贴一贴支付，京东也成为了全国首个支持数字人民币“......
### [组图：阿娇晒图庆生 阿Sa容祖儿Selina郑希怡霍汶希亮相生日聚会](http://slide.ent.sina.com.cn/star/slide_4_86512_365739.html)
> 概要: 组图：阿娇晒图庆生 阿Sa容祖儿Selina郑希怡霍汶希亮相生日聚会
### [央视虎年春晚联排！沈腾艾伦早早下班，赵丽颖陈妍希行色匆匆](https://new.qq.com/rain/a/ENT2022012100541700)
> 概要: 腾讯娱乐讯 1月21日，北京，2022央视春晚首次带妆彩排，继昨天（20日）、前天（19日）两天的彩排后，今天更多艺人来到央视，为春晚做准备。他们不少人都先来到附近的酒店做准备，然后再前往央视大楼做彩......
### [周五榕树下·三连回顾第十期](https://news.dmzj.com/article/73416.html)
> 概要: 近期必看三连的存档回顾！第十！这是一个不能再正经的书单。
### [cos：原神 珊瑚宫心海 “未雨绸缪，才能临危不乱。”](https://new.qq.com/omn/20220121/20220121A09L3500.html)
> 概要: 更多内容，尽在cosplay二次元大全未雨绸缪，才能临危不乱。珊瑚宫心海 CN:@--南宫                                                       ......
### [《巨齿鲨2》下周在英国开拍 杰森斯坦森回归](https://www.3dmgame.com/news/202201/3833947.html)
> 概要: 据KFTV报道，《巨齿鲨2（The Meg 2）》将于下周在英国开拍，由华纳兄弟英国工作室Leavesden制作。2018年电影《巨齿鲨》主要是在新西兰的奥克兰拍摄。除了拍摄场地外，续集也将更换导演......
### [人人想吃垮的自助餐，快凉透了](https://www.huxiu.com/article/492821.html)
> 概要: 本文来自微信公众号：网易数读（ID：datablog163），作者：黄可乐，设计：小羊、白气泡、姜姜，题图来自：视觉中国曾几何时，自助餐火遍大江南北。琳琅满目的美食是欲望的代名词，自助餐龙头“金钱豹”......
### [斗罗大陆：真人版唐晨终于来啦！手持修罗血剑，杀气全开无比霸气](https://new.qq.com/omn/20220121/20220121A0ALKG00.html)
> 概要: 唐晨是《斗罗大陆》剧中的三大绝世斗罗之一，他的实力非常强大。当年就是因为他的存在，武魂殿才不敢对昊天宗出手。不过，唐晨后来消失了，谁也不知道他去了哪里。实际上，唐晨是冲击修罗神的神位失败，最终变成“杀......
### [汤加火山爆发：地球一个响指，万物之灵跌下山巅](https://www.huxiu.com/article/492867.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 木子童制图 | 渣渣郡策划丨黄瓜汽水题图 |nature本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故......
### [北上深“上车价”超300万元！一线城市刚需购房平均年龄33.5岁](https://finance.sina.com.cn/china/gncj/2022-01-21/doc-ikyamrmz6661618.shtml)
> 概要: 原标题：北上深“上车价”超300万元！一线城市刚需购房平均年龄竟然是？ 随着楼市持续调控，房价降温，刚需可以出手了吗？目前主要城市的刚需“上车”门槛在什么价位？
### [福建省南平市政府原党组成员、副市长朱仁秀被“双开”](https://finance.sina.com.cn/jjxw/2022-01-21/doc-ikyakumy1836252.shtml)
> 概要: 原标题：福建省南平市政府原党组成员、副市长朱仁秀被“双开” 新京报快讯 据福建省纪委监委消息，日前，经中共福建省委批准，福建省纪委监委对南平市政府原党组成员...
### [证券市场虚假陈述最新司法解释废除前置程序，如何防滥诉？](https://finance.sina.com.cn/china/gncj/2022-01-21/doc-ikyakumy1836909.shtml)
> 概要: 原标题：证券市场虚假陈述最新司法解释废除前置程序，如何防滥诉？ 新京报快讯（记者沙雪良）1月21日，《最高人民法院关于审理证券市场虚假陈述侵权民事赔偿案件的若干规定...
### [外交部：立陶宛若真有意改善当前局面，就应拿出实际行动](https://finance.sina.com.cn/jjxw/2022-01-21/doc-ikyakumy1837066.shtml)
> 概要: 原标题：外交部：立陶宛若真有意改善当前局面，就应拿出实际行动 新华社北京1月21日电（记者马卓言）外交部发言人赵立坚21日表示，中方同立陶宛沟通的大门始终敞开...
### [这部原创动画汇集了三位人气轻小说作家，但最终的表现却让人遗憾](https://new.qq.com/omn/20220121/20220121A0BF0P00.html)
> 概要: A-1 Pictures是一家很矛盾的动画制作公司。背靠索尼音乐，A-1自身有着充足的制作资源和资金。并且自2005年成立以来，也的确制作了诸如《刀剑神域》、《辉夜大小姐想让我告白》等，不少口碑不错的......
### [0.78 元/瓶闭眼囤：雪碧 24 小瓶 18.9 元新低（日常 43.9 元）](https://lapin.ithome.com/html/digi/599904.htm)
> 概要: 【阿里自营 够实惠超市】雪碧 300ml*24 瓶日常售价 43.9 元，今日可领 1 元券，实付 42.9 元包邮。下单立返 24 元实惠金（结算时可见，没有不返），折合仅需 18.9 元：天猫雪碧......
### [他们对近2500名青年科技人才进行“精准画像”，发现多数“压力山大”](https://finance.sina.com.cn/jjxw/2022-01-21/doc-ikyamrmz6665733.shtml)
> 概要: 原标题：他们对近2500名青年科技人才进行“精准画像”，发现多数“压力山大”| 上海两会 实习记者 赵珺 2022年1月21日，上海市政协十三届第五次会议举行大会发言。
### [各地房贷利率普遍下调5个基点，北京首套降至5.15%](https://finance.sina.com.cn/wm/2022-01-21/doc-ikyamrmz6665201.shtml)
> 概要: 原标题：各地房贷利率普遍下调5个基点，北京首套降至5.15% 来源：华夏时报 1月21日，《华夏时报》记者从中介、银行了解到，北京、上海、深圳、广州...
### [《2022 年春节联欢晚会》举行首次彩排：采用 720 度弧形屏幕，电影化制作](https://www.ithome.com/0/599/905.htm)
> 概要: IT之家1 月 21 日消息，据春晚官方微博消息，1 月 21 日，中央广播电视总台《2022 年春节联欢晚会》进行第一次彩排。据央视新闻消息，《2022 年春节联欢晚会》将用走心的歌舞节目、精致的语......
# 小说
### [我有一个识海世界](http://book.zongheng.com/book/965899.html)
> 作者：仙道引路人

> 标签：武侠仙侠

> 简介：五彩缤纷的修仙世界，神奇莫测的术法，移山填海的神通，流传恒久的传奇故事，强者传承的争夺，有死无生的必杀墓葬，五光十色的秘境。一点点探寻仙之尽头……

> 章节末：第七百二十二章大结局

> 状态：完本
# 论文
### [Tradeoffs of Linear Mixed Models in Genome-wide Association Studies | Papers With Code](https://paperswithcode.com/paper/tradeoffs-of-linear-mixed-models-in-genome)
> 日期：5 Nov 2021

> 标签：None

> 代码：None

> 描述：Motivated by empirical arguments that are well-known from the genome-wide association studies (GWAS) literature, we study the statistical properties of linear mixed models (LMMs) applied to GWAS. First, we study the sensitivity of LMMs to the inclusion of a candidate SNP in the kinship matrix, which is often done in practice to speed up computations.
### [SlovakBERT: Slovak Masked Language Model | Papers With Code](https://paperswithcode.com/paper/slovakbert-slovak-masked-language-model)
> 日期：30 Sep 2021

> 标签：None

> 代码：None

> 描述：We introduce a new Slovak masked language model called SlovakBERT in this paper. It is the first Slovak-only transformers-based model trained on a sizeable corpus.
