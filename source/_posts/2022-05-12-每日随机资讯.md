---
title: 2022-05-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.RiverBrathay_ZH-CN2718424663_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2022-05-12 22:59:03
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.RiverBrathay_ZH-CN2718424663_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [谷歌发布 Flutter 3](https://www.oschina.net/news/195375/flutter-3-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>谷歌宣布推出 Flutter 3。Flutter 3 完成了谷歌从以移动为中心到多平台框架的路线图，提供了 macOS......
### [年增长 1900%，Google Firebase 开源替代品融资 8000 万美元](https://www.oschina.net/news/195380/supabase-b-fund-80m)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>Supabase是开源数据库即服务（DBaaS）公司，也是 Google Firebase 的开源替代品，近日该公司宣......
### [Chrome OS 更新，更名为 chromeOS](https://www.oschina.net/news/195372/chromeos-101-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>Chrome OS 的更新速度通常会比 Chrome 浏览器稍晚一些，Chrome OS 101 原计划于 4 月 2......
### [.NET MAUI 候选版本 3 已发布](https://www.oschina.net/news/195373/dotnet-maui-rc3-released)
> 概要: 如何使用英特尔®oneAPI工具实现PyTorch 优化，直播火热报名中>>>>>NET MAUI （多平台应用程序 UI）候选版本 3已发布！其中包含一批新的改进，比如使用shell 控件实现导航......
### [产品运营那50件事儿（1）：如何制定会员权益的发放周期？](http://www.woshipm.com/operate/5435940.html)
> 概要: 编辑导语：定期发放的会员权益，应该如何制定发放周期呢？本文作者以其自身做过的项目为例，分析不同版本的优缺点，感兴趣的小伙伴们一起来看一下吧。笔者负责的会员项目已有2年，是一个关于车后市场的项目。其中有......
### [去年晒大厂工牌的人，今年去哪儿了？](http://www.woshipm.com/it/5435587.html)
> 概要: 编辑导语：大厂裁员潮还没袭来前，大厂工牌简直是令人羡慕的存在，并开启“晒工牌”团建活动。但是，目前在社交平台“晒工牌”的行为却寥寥无几。那么那些主动离职或者被迫离开大厂的年轻人们又做出了什么样的人生选......
### [一张自拍狂赚百万美元；爆火的NFT背后到底是什么？](http://www.woshipm.com/it/5434820.html)
> 概要: 编辑导语：最近一段时间，NFT在网上被炒的火热，不管是资本家还是打工人纷纷入局了NFT。本篇文章中作者将围绕了解什么是NFT、国内与国外到底有那些区别、以及现在NFT市场的玩法与规则展开一系列详细的讲......
### [搜索是主线 AI布满全篇](https://finance.sina.com.cn/tech/2022-05-12/doc-imcwiwst6907580.shtml)
> 概要: 新酷产品第一时间免费试玩，还有众多优质达人分享独到生活经验，快来新浪众测，体验各领域最前沿、最有趣、最好玩的产品吧~！下载客户端还能获得专享福利哦......
### [中国电动车主，愿用隐私换作妖](https://www.huxiu.com/article/551270.html)
> 概要: 出品｜虎嗅汽车组作者｜李文博编辑｜周到头图｜视觉中国想随时随地欣赏远在几千公里外的中国实时街景，总共分几步？一台售价高达80万的中国品牌智能电动汽车给出的答案是：三步。第一步，拉开车门，坐进车内；第二......
### [持续施压！美国SEC再将11家中概股列入“预摘牌名单”](https://finance.sina.com.cn/tech/2022-05-12/doc-imcwipii9373223.shtml)
> 概要: 编辑/赵昊......
### [FF内部调查暗批贾跃亭：道德缺失导致文件披露不准确](https://finance.sina.com.cn/tech/2022-05-12/doc-imcwiwst6934742.shtml)
> 概要: 新浪科技讯 北京时间5月12日早间消息，据报道，法拉第未来公司（FF）表示，最近完成的一项内部调查发现，该公司领导人没有表现出 “对保持诚信和道德价值观的承诺”......
### [用户下滑、股价暴跌...Netflix颓势尽显 迪士尼流媒体业务增速却超预期](https://finance.sina.com.cn/tech/2022-05-12/doc-imcwipii9381887.shtml)
> 概要: 新浪科技讯 北京时间5月12日早间消息，据报道，当迪士尼旗下的Disney+流媒体服务在最近一个季度实现强劲增长之际，与之形成鲜明对比的却是市场领头羊Netflix的订户下滑......
### [动画电影「神之山岭」将于7月8日在日本上映](http://acg.178.com/202205/446319074102.html)
> 概要: 法国动画电影「神之山岭」确认将于7月8日在日本上映，官方公开了本作的海报、特报PV以及日语配音：堀内贤雄、大塚明夫、逢坂良太、今井麻美。动画电影「神之山岭」特报PV......
### [动画「王者天下」第四季“著雍攻略战”篇章视觉图公开](http://acg.178.com/202205/446319726458.html)
> 概要: 电视动画「王者天下」第四季发布了“著雍攻略战”篇章视觉图，本作目前正在NHK综合频道热播中。MUSIC片头曲：SUIREN「黎-ray-」片尾曲：珀「眩耀」CAST信：森田成一嬴政：福山潤河了貂：釘宮......
### [非网红风字体设计方法](https://www.zcool.com.cn/special/share/)
> 概要: 非网红风字体设计方法
### [组图：Dua Lipa披散长发出街 穿印花吊带裙惬意又随性](http://slide.ent.sina.com.cn/star/h/slide_4_86512_369608.html)
> 概要: 组图：Dua Lipa披散长发出街 穿印花吊带裙惬意又随性
### [丧尸生存动作游戏《创尸纪》概念图 5月20日发售](https://www.3dmgame.com/news/202205/3842244.html)
> 概要: 由Marvelous开发的全新丧尸生存动作游戏《创尸纪（DEADCRAFT）》已上线Steam平台，预计5月20日正式发布，支持中文，5月12日今天官方发布开发近况，公布了多张概念设定图，一起来先睹为......
### [P站美图推荐——画师コユコム特辑](https://news.dmzj.com/article/74386.html)
> 概要: コユコム老师平时在画商业插画，爱好则是玩各种各样的粥原碧蓝（？）能以商业画师的水平看见喜欢的角色是多么幸福的事情
### [剧场版「Free!–the Final Stroke–」角色歌第一弹封面图公开](http://acg.178.com/202205/446324102353.html)
> 概要: 「Free！」最终章剧场版「Free!–the Final Stroke–」公开了角色歌第一弹的封面图，人物为七濑遥，将于2022年5月25日发售。「Free!–the Final Stroke–」是......
### [十年来最佳电视剧竟然被《甄嬛传》占据了，作为剧黑我有意见](https://new.qq.com/rain/a/20220512V041BD00)
> 概要: 十年来最佳电视剧竟然被《甄嬛传》占据了，作为剧黑我有意见
### [楠木灯单曲「もうひとくち」抒情版MV公开](http://acg.178.com/202205/446325457148.html)
> 概要: 近日，声优楠木灯公开了单曲「もうひとくち」的抒情版MV，该曲收录于第四张EP之中，通常版定价为1500日元，约合人民币78元。确认于6月1日发售。「もうひとくち」抒情版MV......
### [为了认祖归宗，我找族长买了5万块的假酒](https://www.huxiu.com/article/552964.html)
> 概要: 出品 | 虎嗅青年文化组作者 | 渣渣郡本文首发于虎嗅年轻内容公众号“那個NG”（ID：huxiu4youth）。在这里，我们呈现当下年轻人的面貌、故事和态度。想必很多人在小时候都有这样的经历：为了满......
### [电影导演纷纷拍剧集 他们也怕观众拖进度条](https://ent.sina.com.cn/v/m/2022-05-12/doc-imcwipii9427929.shtml)
> 概要: 近二三十年间，影视行业的热点在电影和剧集之间来回切换，作为核心创作者的导演们也随着潮流的起伏变换着自己的“弄潮”方式。从拍电影到拍剧集，改变的背后既有个人偏好的考量，更有顺势而为的选择。正如导演管虎所......
### [日本教育媒体调查家长愿意给孩子推荐哪些少年漫画](https://news.dmzj.com/article/74387.html)
> 概要: ARINA株式会社运营的面向幼儿和小学生家长的教育媒体“家中教材之森”，公开对家长进行的“愿意给孩子推荐哪些少年漫画”的调查问卷的结果。
### [《海贼王》1049话情报：战斗终局 应当追求的世界](https://acg.gamersky.com/news/202205/1482202.shtml)
> 概要: 《海贼王》1049话情报公开，路飞和凯多的战斗终于迎来结局，凯多开始了回忆跑马灯，少年凯多、青年白胡子都出现了。
### [一本绘本的诞生《山羊与女孩》](https://www.zcool.com.cn/work/ZNTk3NDQ1ODA=.html)
> 概要: 四月的一些作品，整理汇总分享给大家......
### [法国动画电影《众神的山岭》7月在日本上映](https://news.dmzj.com/article/74388.html)
> 概要: 根据梦枕獏、谷口治郎原作制作的法国动画电影《众神的山岭》即将于7月8日在日本上映。堀内贤雄、大塚明夫、逢坂良太、今井麻美参与配音。一段特报视频也一并公开。
### [网友分享奈良若草山远景 酷似WinXP壁纸引热议](https://www.3dmgame.com/news/202205/3842262.html)
> 概要: 近日一位日本网友分享了奈良若草山美丽远景，静静的草原矮山充满了恬静祥和，不过这不是重点，因为这张图片让不少老网友看着眼熟，没错，就是WinXP的标准官方壁纸。•曾经的WinXP的标准官方壁纸让老用户网......
### [非人哉795-796](https://www.zcool.com.cn/work/ZNTk3NDgzNDA=.html)
> 概要: 四季的风 | THE WIND OF THE FOUR SEASONSEVENT DESIGN for HATS&CAPS BAR SHANGHAI by HOOOLY DESIGN......
### [《蜡笔小新 我与博士的暑假》亚洲版 销量突破10万份](https://www.3dmgame.com/news/202205/3842270.html)
> 概要: Neos Corporation今日宣布旗下NS平台游戏《蜡笔小新我与博士的暑假》亚洲版（中韩文版）在中国台湾、中国香港和韩国等地区，自本月5号发售以来出货量已突破10万份。该作的日文版于去年7月发售......
### [受困上海，燕麦奶江湖告急](https://www.huxiu.com/article/553196.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国“订单少了六成，短期内很难找到替代客户。”燕麦奶创业者车明告诉虎嗅，由于上海和北京的咖啡店、茶饮店因疫情“歇业”，他的燕麦奶生意正遭遇寒冬。在经过了202......
### [小红书困境待解：裁员、虚假种草、商业梦](https://www.tuicool.com/articles/FVZN7j2)
> 概要: 小红书困境待解：裁员、虚假种草、商业梦
### [《戴卡奥特曼》制作团队公布 根元岁三担任总编剧](https://acg.gamersky.com/news/202205/1482325.shtml)
> 概要: 《戴卡奥特曼》公开了本作的制作团队，导演过多部《奥特曼》作品的武居正能担任总导演，剧本由根元岁三、足木淳一郎、中野贵雄、継田淳等人共同完成。
### [组图：高口碑剧集《做工的人》电影版杀青 李铭顺等主创回归](http://slide.ent.sina.com.cn/film/slide_4_86512_369621.html)
> 概要: 组图：高口碑剧集《做工的人》电影版杀青 李铭顺等主创回归
### [首发 499 元，罗技新款 LIFT 垂直人体工程学鼠标今晚开卖，专为亚洲中小手型打造](https://www.ithome.com/0/618/079.htm)
> 概要: IT之家5 月 12 日消息，4 月份，罗技全球发布了新款 LIFT 垂直人体工程学鼠标，官方称适合多数亚洲中小手型使用者。5 月 13 日 0 点，这款鼠标正式开卖，首发 499 元。据介绍，LIF......
### [第一人称恐怖游戏《Impasto》 推出免费抢先体验试玩](https://www.3dmgame.com/news/202205/3842281.html)
> 概要: 第一人称恐怖冒险游戏《Impasto》现已开启免费抢先体验试玩，游戏灵感来自备受尊敬的西班牙浪漫主义派画家弗朗西斯科·戈雅的作品。游戏预计于6月下旬至7月上旬期间登录Steam平台。玩家将在这个被他日......
### [辉大基因完成数亿元 C 轮融资，昆仑资本持续加码](https://www.tuicool.com/articles/rMF7Zny)
> 概要: 辉大基因完成数亿元 C 轮融资，昆仑资本持续加码
### [微星推出海皇戟 S 迷你主机：搭载 AMD 锐龙 5000G 系列处理器，4599 元起](https://www.ithome.com/0/618/083.htm)
> 概要: IT之家5 月 12 日消息，今年早些时候，微星海皇戟 S（Trident S 5M）迷你主机在中国台湾地区推出。现在，微星在中国大陆地区推出了海皇戟 S，R5 5600G 版售价 4599 元，R7......
### [《灵能百分百》第三季动画PV公开 2022年10月开播](https://acg.gamersky.com/news/202205/1482345.shtml)
> 概要: 由《一拳超人》原作者ONE的另一部漫画改编的同名动画《灵能百分百》即将推出第三季，今天，官方公开了本作的先导PV，并且宣布动画将于10月开播。
### [豪威发布显示驱动芯片 TD4160，支持 HD+ 120Hz 触控](https://www.ithome.com/0/618/088.htm)
> 概要: IT之家5 月 12 日消息，据中国半导体设计豪威官方消息，豪威集团打造了一款 LCD 屏触控与显示集成驱动芯片 TD4160，支持 HD+ 分辨率、120Hz 显示帧率，支持 a-Si 面板工艺。官......
### [5月12日这些公告有看头](https://www.yicai.com/news/101410430.html)
> 概要: 5月12日晚间，多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考：
### [动画电影《阿松》公开预告片](https://news.dmzj.com/article/74392.html)
> 概要: 动画电影《阿松〜Hipipo族与光辉的果实〜》公开了一段预告片。在这段视频中，可以看到主人公们在谜之岛屿上冒险的片段。
### [因为年报“不保真”，这些公司可能触发退市风险](https://www.yicai.com/news/101410480.html)
> 概要: 多家年报不保真公司已退市
### [通过Go实现AES加密和解密工具](https://www.tuicool.com/articles/mQnUFjE)
> 概要: 通过Go实现AES加密和解密工具
### [76 款任选：361° 休闲运动鞋 69 元清仓（立减 100 元）](https://lapin.ithome.com/html/digi/618098.htm)
> 概要: 断码清仓：361° 男女 76 款休闲运动鞋报价 169 元，限时限量 100 元券，实付 69 元包邮，领券并购买。使用最会买 App下单，预计还能再返 5.69 元，返后 63.31 元包邮，点击......
### [风投史上最大窟窿：孙正义，亏了900亿](https://www.huxiu.com/article/553304.html)
> 概要: 本文来自微信公众号：投资界 （ID：pedaily2012），作者：刘博，头图来自：视觉中国刚刚，风投史上最惨烈的一幕出现了——今天（5月12日）下午，孙正义在日本东京公布了软银集团最新财报：截至3月......
### [谷歌发布Pixel 6a新机 搭载自研Tensor芯片](https://www.3dmgame.com/news/202205/3842291.html)
> 概要: 12日凌晨，谷歌举行Google I/O 2022开发者大会，不仅推出了重量级的Android 13系统，还顺带推出了多款热门科技数码产品，其中就包含了这款Pixel 6a新机。作为谷歌Pixel 6......
### [人社部：今年预计释放技能提升政策红利约500亿元](https://finance.sina.com.cn/jjxw/2022-05-12/doc-imcwiwst7078187.shtml)
> 概要: 中国青年报客户端北京5月12日电（中青报 中青网记者 李桂杰）国新办今天就失业保险基金稳岗位提技能防失业政策有关情况举行国务院政策例行吹风会...
### [王受文副部长兼国际贸易谈判副代表出席中共中央宣传部举行的经济与生态文明领域建设与改革情况发布会](https://finance.sina.com.cn/china/2022-05-12/doc-imcwipii9521536.shtml)
> 概要: ❖ 中共中央宣传部于2022年5月12日（星期四）上午10时举行“中国这十年”系列主题新闻发布会，请中央财办分管日常工作的副主任韩文秀、国家发展改革委副主任胡祖才...
### [湖南长沙：多套存量房按“一户核减一套”计数，原限购政策保持不变](https://www.yicai.com/news/101410693.html)
> 概要: 5月11日，长沙市发布了《关于推进长沙市租赁住房多主体供给多渠道保障存量房的试点实施方案》引发了不少市场热议。今日，长沙市住建局相关负责人进行了答疑。
### [十年来中国GDP占全球经济比重升至18%以上 单位GDP二氧化碳排放量累计下降约34%](https://finance.sina.com.cn/roll/2022-05-12/doc-imcwipii9521265.shtml)
> 概要: 每经记者 李可愚 中共中央宣传部于5月12日举行“中国这十年”系列主题新闻发布会，介绍经济和生态文明领域建设与改革情况。 《每日经济新闻》记者注意到...
### [A股三大指数走势分化 创指三连阳 成交量重返8000亿](https://www.yicai.com/video/101410713.html)
> 概要: None
### [出海东南亚，谨防“没文化”的雷](https://www.tuicool.com/articles/qqaIJjj)
> 概要: 出海东南亚，谨防“没文化”的雷
### [京报锐评：抓住周末一鼓作气，再宅一宅为抗疫助力！](https://finance.sina.com.cn/china/gncj/2022-05-12/doc-imcwiwst7079916.shtml)
> 概要: 原标题：抓住周末一鼓作气，再宅一宅为抗疫助力！ 当前，北京疫情防控形势严峻复杂。为尽早发现病毒隐匿传播风险，尽快阻断传播链条实现社会面清零，从5月13日开始...
### [北京严格工作场所打卡制度 确保人员“来源可溯、去向可追”](https://finance.sina.com.cn/china/2022-05-12/doc-imcwipii9522698.shtml)
> 概要: 中新网北京5月12日电 （记者 杜燕）北京市委宣传部副部长、市政府新闻办主任、市政府新闻发言人徐和建在12日召开的北京市新冠肺炎疫情防控工作新闻发布会上表示，近期...
### [全国新增14家“双跨”平台 政企合力助推中小企业上“云”](https://finance.sina.com.cn/china/2022-05-12/doc-imcwiwst7080360.shtml)
> 概要: 21世纪经济记者叶碧华 实习生赵婧轩 广州报道 5月11日，由工信部遴选的“2022年新增跨行业跨领域工业互联网平台”（以下简称双跨平台）公示结束...
### [印度热浪破百年纪录：民众接受“烤”验，政府四处找煤](https://www.yicai.com/news/101410750.html)
> 概要: 罕见高温中，印度陷入“煤荒”和“电荒”。
### [海贼王1049话汉化，四皇凯多战败，路飞成为凯多眼中的乔伊波伊](https://new.qq.com/omn/20220512/20220512A0DIOB00.html)
> 概要: 而这个世界，则是路飞之前答应小玉的，让他们吃饱饭的世界。而扉页之中，            凯多回忆开启，他说乔伊波伊就是打败他的人虽然凯多承认路飞确实做得不错，但是他不认为路飞可以改变世界，看来凯多......
### [两部门发文加强智能投递设施建设 磷矿石市场供应趋紧丨明日主题前瞻](https://www.yicai.com/news/101410768.html)
> 概要: 台积电5月11日通知客户，明年1月起将全面调涨晶圆代工价格，涨幅6%，部分台积电客户已证实接获涨价通知。
### [高手坐堂—左佐带你领略字里风景](https://www.zcool.com.cn/work/ZNTk3MzYxNTY=.html)
> 概要: -项目一：蓝牙耳机渲染：嘿先生啊设计：阿良Aaron-项目二：小风扇渲染：随枫oo设计：阿良Aaron-项目三：小夜灯渲染：随枫oo设计：阿良Aaron-项目四：PD快充收纳数据线渲染：嘿先生啊设计：......
# 小说
### [天胡开局](http://book.zongheng.com/book/1080165.html)
> 作者：财小胖

> 标签：竞技同人

> 简介：从火影开始，趟过死神，路过西幻，驾着一叶扁舟，姚发天胡开局！！！

> 章节末：第三百六十四章  开始也是结束

> 状态：完本
# 论文
### [ReViVD: Exploration and Filtering of Trajectories in an Immersive Environment using 3D Shapes | Papers With Code](https://paperswithcode.com/paper/revivd-exploration-and-filtering-of)
> 日期：21 Feb 2022

> 标签：None

> 代码：None

> 描述：We present ReViVD, a tool for exploring and filtering large trajectory-based datasets using virtual reality. ReViVD's novelty lies in using simple 3D shapes -- such as cuboids, spheres and cylinders -- as queries for users to select and filter groups of trajectories. Building on this simple paradigm, more complex queries can be created by combining previously made selection groups through a system of user-created Boolean operations. We demonstrate the use of ReViVD in different application domains, from GPS position tracking to simulated data (e.g., turbulent particle flows and traffic simulation). Our results show the ease of use and expressiveness of the 3D geometric shapes in a broad range of exploratory tasks. ReViVD was found to be particularly useful for progressively refining selections to isolate outlying behaviors. It also acts as a powerful communication tool for conveying the structure of normally abstract datasets to an audience.
### [Retrieving Black-box Optimal Images from External Databases | Papers With Code](https://paperswithcode.com/paper/retrieving-black-box-optimal-images-from)
> 日期：30 Dec 2021

> 标签：None

> 代码：None

> 描述：Suppose we have a black-box function (e.g., deep neural network) that takes an image as input and outputs a value that indicates preference. How can we retrieve optimal images with respect to this function from an external database on the Internet?
