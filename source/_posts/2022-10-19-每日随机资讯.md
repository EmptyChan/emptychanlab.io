---
title: 2022-10-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.WartburgCastle_ZH-CN4201605751_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-19 22:18:20
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.WartburgCastle_ZH-CN4201605751_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [面试过40多人后，写给过去想成为产品经理的自己](https://www.woshipm.com/pmd/5648888.html)
> 概要: 作者成为产品经理后，作为面试官组建了自己的产品团队，这 2 年筛选超过 100 份简历，跟 40+ 位面试者沟通交流。也有一些杂思杂想分享出来，这篇文章，既是送给 2014 年想要成为产品的自己，也希......
### [没内容没技术，元宇宙凭什么吸引人](https://www.woshipm.com/it/5648863.html)
> 概要: 元宇宙这一概念已经逐渐在国内外落地，不过面对已经出现的元宇宙项目或者相关产品，不少用户却发出了自己的吐槽，因为相关的产品或服务并没有达到用户的原有预期。那么在未来，元宇宙的热度还可以持续多久？谁又能走......
### [百度、谷歌抢先布局AI绘画](https://www.woshipm.com/ai/5648219.html)
> 概要: 最近，AI绘画这个话题在网络平台上彻底火了，而在AI绘画版权归属、AI绘画是否会替代人类画手等问题引起大家争相讨论的同时，不少社交平台也出现了AI绘画相关的衍生生意。那么，AI绘画的发展进度如何？不如......
### [苹果十月新品](https://finance.sina.com.cn/tech/it/2022-10-19/doc-imqmmthc1343733.shtml)
> 概要: 财联社10月19日讯（编辑 史正丞）北京时间周二晚间，苹果商店经过短暂更新后上架了三款新的iPad产品，这也意味着苹果“十月新品”的悬念就此揭开。与此前市场爆料大致一致，Pro系列升级了最新的M2芯片......
### [单依纯好声音3首爆款：给电影人的情书永不失联，人歌合一的境界](https://new.qq.com/rain/a/20221019V006SW00)
> 概要: 单依纯好声音3首爆款：给电影人的情书永不失联，人歌合一的境界
### [个推TechDay治数训练营第三期直播预告：10月26日晚19:00分享数据指标体系搭建秘诀！](https://segmentfault.com/a/1190000042645862)
> 概要: 科学完善的数据指标体系是企业开展数字化运营管理、打造数据驱动型组织的重要支撑。透过多维度的数据指标，运营人员能够清晰了解业务现状，产品/研发人员能够高效定位系统问题，管理人员能够更加准确地做出分析决策......
### [新特性解读 | MySQL 8.0 对 limit 的优化](https://segmentfault.com/a/1190000042645871)
> 概要: 作者：杨奇龙网名“北在南方”，资深 DBA，主要负责数据库架构设计和运维平台开发工作，擅长数据库性能调优、故障诊断。本文来源：原创投稿*爱可生开源社区出品，原创内容未经授权不得随意使用，转载请联系小编......
### [宝马中国人士透露：将再增资百亿扩建在华项目](https://finance.sina.com.cn/tech/it/2022-10-19/doc-imqqsmrp3035460.shtml)
> 概要: 环球时报综合报道针对外媒报道德国宝马集团打算停止在英国牛津工厂生产MINI电动汽车、将该生产线转移到中国的新闻，18日凌晨，宝马集团在官方回应中列举了牛津工厂将生产的下一代MINI车型，包括MINI敞......
### [张一鸣卸载抖音了吗？](https://www.huxiu.com/article/689494.html)
> 概要: 来源｜商隐社（ID：shangyinshecj）作者｜里普头图｜视觉中国一个人的上瘾背后，是上千个人努力工作，想方设法让你上瘾。——特里斯坦·哈里斯 前谷歌设计伦理学家01. 膨胀不知不觉就来到了Q4......
### [否极泰来了吗？](https://finance.sina.com.cn/tech/internet/2022-10-19/doc-imqmmthc1365528.shtml)
> 概要: 新浪科技讯 北京时间10月19日早间消息，据报道。Netflix今日公布的第三季度财报重新实现增长，令好莱坞松了口气......
### [《死亡空间：重制版》试玩细节 升级扩充恐怖游戏玩法](https://www.3dmgame.com/news/202210/3854076.html)
> 概要: 近日Playstation官方博客公布了《死亡空间：重制版》试玩细节，艾萨克的回归带来新颖的探索方式、强化的零重力游戏玩法、扩充的升级选项等等。一起来看看详细报道！新游戏玩法特色艾萨克全配音演出：这回......
### [策略战棋游戏《幻灵降世录》发售 Steam多半好评](https://www.3dmgame.com/news/202210/3854082.html)
> 概要: 回合制策略战棋游戏《幻灵降世录》(Lost Eidolons)已在Steam上发售，新预告也同步公开。该作售价179元，支持中文，在Steam上总评为“多半好评”。有玩家表示这是一款好玩的战棋游戏，强......
### [卡牌游戏《漫威 Snap》在Steam免费推出 支持中文](https://www.3dmgame.com/news/202210/3854083.html)
> 概要: 漫威卡牌游戏《漫威Snap》现已在Steam平台免费推出，游戏支持简繁体中文，锁国区。所有你喜欢的英雄和反派都在此集结，随心组建属于你的漫威梦之队，即刻出击！漫威SNAP是一款超快节奏的策略卡牌对战游......
### [泰语版《苍兰诀》笑晕了！娇俏版东方青苍狂飙口音，味道太冲了](https://new.qq.com/rain/a/20221019V01P3M00)
> 概要: 泰语版《苍兰诀》笑晕了！娇俏版东方青苍狂飙口音，味道太冲了
### [高管解读](https://finance.sina.com.cn/tech/internet/2022-10-19/doc-imqmmthc1378826.shtml)
> 概要: 相关新闻：奈飞第三季度营收79亿美元 净利润同比下降3.5%......
### [喜茶发起首届logo全身像征集大赛，开启粉丝联名新玩法](http://www.investorscn.com/2022/10/19/103573/)
> 概要: 近日，喜茶官宣与粉丝“联名”，发起首届“阿喜全身像征集大赛”，并设立最高超过9999元价值的大奖，邀请粉丝以绘制喜茶logo——阿喜的全身像的方式参与这次“联名”。活动开始后，微博、小红书相关话题阅读......
### [医疗新基建风口下，基因检测行业的变与不变](http://www.investorscn.com/2022/10/19/103574/)
> 概要: 万物皆周期......
### [一文告诉你AVM中设置字体的方法](https://segmentfault.com/a/1190000042652135)
> 概要: avm 是一种简便的多端开发框架，可以开发APP、小程序、H5。今天学习了一下使用 avm 开发 APP 怎么设置字体，下面将经验分享给大家。所需步骤：1.  将需要使用的字体文件放到代码包res 目......
### [macOS包管理器 Homebrew 备忘清单](https://segmentfault.com/a/1190000042652192)
> 概要: Homebrew 是 macOS(或Linux)缺少的包管理器，备忘清单包含brew命令的使用与安装在线预览：https://jaywcjlove.github.io/...开源仓库：https://......
### [宣称环保！新款iPad包装不再使用塑料外膜](https://www.3dmgame.com/news/202210/3854103.html)
> 概要: 苹果近几年一直高喊环保口号，为了环保连手机等产品的充电器都取消了，而用户要购买一套原装充电器需要数百元。从iPhone 13开始，苹果就试水了无塑料外膜的包装，采用了两个纸质封条代替，现在这种包装或许......
### [网飞《天线宝宝》新版动画剧中文预告 11月14日开播](https://www.3dmgame.com/news/202210/3854086.html)
> 概要: 近日Netflix打造《天线宝宝》新版动画剧中文预告发布，一起来感受下画风！值得一提的是，在此次预告中，出现了亚裔和黑人太阳宝宝。预告欣赏：《天线宝宝》新版动画剧将于11月14日在Netflix开播，......
### [领峰黄金投资福利再升级，双倍积分活动限时开启！](http://www.investorscn.com/2022/10/19/103577/)
> 概要: 席卷全国的高温虽已退去，但金市行情热浪依旧高涨，初请、非农、议息等重磅事件轮番登场，超多数据引爆投资市场。为助投资者赶上投资快车，领峰特推出限时积分奖励计划，活动期间交易伦敦金/伦敦银，可享双倍积分奖......
### [携手再出发！华体会（HTH）联合沃尔夫斯堡俱乐部再创巅峰](http://www.investorscn.com/2022/10/19/103578/)
> 概要: 德甲劲旅沃尔夫斯堡宣布与华体会（HTH）达成进一步合作协议，双方未来从赛事推广、品牌打造、多元化互动等多方面不断创新。此次合作也标志着华体会（HTH）迈向国际化的步伐不会停止，在未来的日子里持续与沃尔......
### [【众生相】揭幕战便是一场凌乱的败仗，新赛季湖人还有必要继续期待吗？](https://bbs.hupu.com/55981429.html)
> 概要: 刚刚结束的湖人vs勇士的揭幕战中，湖人最终109-123不敌勇士。整场比赛，湖人从技战术到场面都无法匹配勇士，分差甚至一度拉大到30分；用"凌乱“来形容新赛季的湖人太合适不过，就这个状态打下去，真的会
### [日本搞笑综艺里的上海，大不一样](https://www.huxiu.com/article/689740.html)
> 概要: 本文来自微信公众号：外滩TheBund （ID：the-Bund），作者：siri110，原文标题：《日本综艺跑来上海采访，安福路卖花老爷叔亮了》，头图来自：综艺《月曜夜未央》日本综艺《月曜夜未央》，......
### [立足理想城市洞见感质生活，ASKO携手《IDEAT理想家》，共启非凡设计大奖](http://www.investorscn.com/2022/10/19/103585/)
> 概要: 以无界灵感叩响创意之门，于理想空间洞见非凡设计。紧握来自于斯堪的纳维亚的设计灵感，ASKO始终坚持将极简设计、实用功能以及对人文关怀的不懈追求灌注于品牌血液，并致力于在当代生活语境中发掘设计与创新的全......
### [EasyV数据可视化｜2022年度可视化精选作品合集](https://www.zcool.com.cn/work/ZNjI0NDIyMDg=.html)
> 概要: 时光荏苒，转眼2022年也快到尾声，在这个承上启下的时节里，我们为大家推送易知微年度数据可视化优秀作品合集，作品内含智慧水利、智慧旅游、智慧工厂、体育赛事等数据可视化大屏，用作品为设计代言...动图较......
### [奔驰给新中产们造了新玩具](https://www.huxiu.com/article/688709.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 梅赛德斯-奔驰前段时间，一位保时捷Panamera车主朋友问我——60万-70万元预算能买什么纯电SUV？说实话，那一刻我脑中一片空白。目前......
### [超级快充和超大电池，车企该怎么选？](https://www.ithome.com/0/647/536.htm)
> 概要: 步入 2022 年后，1000 公里超长续航电动汽车终于不再是 PPT 产品，一个接着一个上市。临近年底，极氪发布了大概是今年最后一款 1000 公里续航的 MPV 汽车，虽说不是大事，但一年来的发展......
### [视频：60岁周星驰开通首个社交账号 发布人才招募令将亲自挑选](https://video.sina.com.cn/p/ent/2022-10-19/detail-imqqsmrp3097240.d.html)
> 概要: 视频：60岁周星驰开通首个社交账号 发布人才招募令将亲自挑选
### [视频：张馨月晒一家三口出游照 林峯紧搂妻女眼神温柔宠溺](https://video.sina.com.cn/p/ent/2022-10-19/detail-imqmmthc1424691.d.html)
> 概要: 视频：张馨月晒一家三口出游照 林峯紧搂妻女眼神温柔宠溺
### [48岁徐静蕾正式公开孕肚照，穿孕妇裙素颜出镜状态佳](https://new.qq.com/rain/a/20221019A051CE00)
> 概要: 10月19日，徐静蕾分享一组和亲友们的合影并配文：“西米相当于天津来回，带爸妈“郊游”，意外爆胎考验了英文，照例大吃一顿，西米美食不是开玩笑的。回家路上全部呼呼大睡，徐司机硬挺过来了”，字里行间写满惬......
### [视频：吴磊杭州西湖边骑车被偶遇 短发清爽少年感满满](https://video.sina.com.cn/p/ent/2022-10-19/detail-imqmmthc1425266.d.html)
> 概要: 视频：吴磊杭州西湖边骑车被偶遇 短发清爽少年感满满
### [《古惑狼 4：时机已到》结束独占，Steam 国区 243 元现已发售](https://www.ithome.com/0/647/563.htm)
> 概要: IT之家10 月 19 日消息，动视发行的 3D 动作冒险游戏《古惑狼 4：时机已到（Crash Bandicoot 4: It’s About Time）》现已不再属于战网独占，今日开始登陆 Ste......
### [华为智能门锁 / Pro 系列推出全新“鎏光金”配色：鸿蒙 HarmonyOS 分布式猫眼，2799 元起](https://www.ithome.com/0/647/580.htm)
> 概要: IT之家10 月 19 日消息，今年 4 月份推出的华为智能门锁 / Pro 系列上新了“鎏光金”配色，将于明日（10 月 20 日）10:08 开售，售价 2799 元起。华为智能门锁“鎏光金”配色......
### [多少人购物时会把9.9元当成9元？](https://www.huxiu.com/article/689972.html)
> 概要: 本文来自微信公众号：果壳 （ID：Guokr42），作者：吉小迪er，原文标题：《多少人购物时会把9.9元当成9元？算算你被坑了多少钱！》，题图来自：视觉中国大多数人在线上激情购物的时候，会在心里大概......
### [能源危机愈演愈烈，数字化计划能否助力欧盟走出困境？](https://www.yicai.com/news/101566786.html)
> 概要: 专家认为，资金、技术、商业化或是欧盟“能源系统数字化”计划的挑战。
### [胶卷生产复苏柯达扩产 美国制造业薪资大涨为何仍招工难](https://www.yicai.com/news/101566697.html)
> 概要: 新的需求将为柯达创造新的就业机会，柯达表示，目前正在寻找75个空缺职位。但是现在的问题是，他们可能难以招到工人，或者需要为此支付更高的劳动报酬。
### [苹果 iPad 全系放弃 3.5mm 耳机孔](https://www.ithome.com/0/647/590.htm)
> 概要: IT之家10 月 19 日消息，昨日晚间，苹果公司正式发布了 iPad 10 和 iPad Pro 2022，其中 iPad 10 相比上一代有了大幅改动。根据苹果官网列出的信息，iPad 10 有一......
### [《追光者》发糖！罗云熙为爱下厨，直球式表白吴倩：我喜欢你](https://new.qq.com/rain/a/20221019V07BH200)
> 概要: 《追光者》发糖！罗云熙为爱下厨，直球式表白吴倩：我喜欢你
### [湖北省推进长江生态保护攻坚行动，破解“化工围江”](https://finance.sina.com.cn/china/2022-10-19/doc-imqmmthc1444414.shtml)
> 概要: 新京报讯（记者陈琳）10月19日，在党的二十大新闻中心举行的集体采访上，湖北省委常委、宣传部部长许正中介绍，湖北省在长江流域生态保护强化源头治理...
### [这个新一线城市放大招：有70年产权房即可落户](https://www.yicai.com/news/101566849.html)
> 概要: 宁波落户门槛再降低，对有房户取消了原先“缴纳社保”的要求。
### [这个新一线城市放大招：有70年产权房即可落户](https://finance.sina.com.cn/jjxw/2022-10-19/doc-imqmmthc1445203.shtml)
> 概要: 根据宁波市人民政府网站，近期宁波市印发《宁波市区户口迁移实施细则的通知》，其中提到，在宁波居住生活的本人、配偶或未成年子女在市区城镇范围内有合法稳定住所的...
### [财经TOP10|李宁致歉，俞敏洪“双11”将现身淘宝直播？iPhone14Plus被曝减产](https://finance.sina.com.cn/china/caijingtop10/2022-10-19/doc-imqqsmrp3123616.shtml)
> 概要: 【政经要闻】 NO.1央地政策齐发力 稳定制造业投资 进一步延长制造业缓税补缴期限，设立设备更新改造专项再贷款；上海印发《上海市推进高端制造业发展的若干措施》；浙江发...
### [二十大报告传递中国开放政策新信号](https://finance.sina.com.cn/china/2022-10-19/doc-imqqsmrp3120991.shtml)
> 概要: 中国开放政策将走向何方？中共二十大报告中的三个新提法值得关注。 其一，报告提出要推进高水平对外开放，稳步扩大规则、规制、管理、标准等制度型开放。
### [英国9月CPI同比上涨10.1%](https://www.yicai.com/news/101566898.html)
> 概要: 今年4月至7月，英国通胀水平接连刷新40年来最高纪录。7月CPI同比上涨10.1%，8月小幅回落至9.9%。为抑制高通胀，英格兰银行9月22日宣布将基准利率从1.75%上调至2.25%，这是去年12月以来英国央行第七次加息。
### [青岛开启“手拉手”爱心购房团购，有的项目拿出五六套，有的项目拿出二百多套](https://finance.sina.com.cn/china/gncj/2022-10-19/doc-imqqsmrp3124254.shtml)
> 概要: 每经记者 王佳飞每经编辑 陈梦妤 10月19日，青岛市房地产业协会相关工作人员通过电话告诉《每日经济新闻》记者：“团购活动仍在举行...
### [流言板本赛季第一场加时！广东队与山东队常规时间比分战平](https://bbs.hupu.com/55987385.html)
> 概要: 虎扑10月19日讯 2022-23赛季CBA常规赛第4轮，常规时间战罢，广东男篮90-90战平，双方进入加时。值得一提的是，这是本赛季常规赛首次进行加时。本场比赛第四节最后时刻山东队陶汉林上演隔扣，山
### [赛场速递保罗后场推进被吹罚八秒违例](https://bbs.hupu.com/55987433.html)
> 概要: 赛场速递保罗后场推进被吹罚八秒违例
### [估值倒挂、新股破发，锂电板块为何跌落神坛](https://www.yicai.com/news/101566971.html)
> 概要: 如何与巨头竞争是问题
### [流言板詹姆斯-哈登揭幕战单场拿到35+5+5，76人队史第三人](https://bbs.hupu.com/55987650.html)
> 概要: 虎扑10月19日讯 76人客场117-126不敌凯尔特人，全场比赛，76人后卫詹姆斯-哈登首发出战37分钟，14投9中，三分球9投5中，罚球12罚12中得到35分8篮板7助攻。数据统计显示，哈登是76
# 小说
### [异界无限召唤系统](http://book.zongheng.com/book/1160005.html)
> 作者：摩拉

> 标签：奇幻玄幻

> 简介：韩一恒死后无故穿越异世大陆，获得了异界无限召唤系统，从此，韩一恒就开启了他恢宏的新的一生。

> 章节末：大结局

> 状态：完本
# 论文
### [Let's Go to the Alien Zoo: Introducing an Experimental Framework to Study Usability of Counterfactual Explanations for Machine Learning | Papers With Code](https://paperswithcode.com/paper/let-s-go-to-the-alien-zoo-introducing-an)
> 概要: To foster usefulness and accountability of machine learning (ML), it is essential to explain a model's decisions in addition to evaluating its performance. Accordingly, the field of explainable artificial intelligence (XAI) has resurfaced as a topic of active research, offering approaches to address the "how" and "why" of automated decision-making. Within this domain, counterfactual explanations (CFEs) have gained considerable traction as a psychologically grounded approach to generate post-hoc explanations. To do so, CFEs highlight what changes to a model's input would have changed its prediction in a particular way. However, despite the introduction of numerous CFE approaches, their usability has yet to be thoroughly validated at the human level. Thus, to advance the field of XAI, we introduce the Alien Zoo, an engaging, web-based and game-inspired experimental framework. The Alien Zoo provides the means to evaluate usability of CFEs for gaining new knowledge from an automated system, targeting novice users in a domain-general context. As a proof of concept, we demonstrate the practical efficacy and feasibility of this approach in a user study. Our results suggest that users benefit from receiving CFEs compared to no explanation, both in terms of objective performance in the proposed iterative learning task, and subjective usability. With this work, we aim to equip research groups and practitioners with the means to easily run controlled and well-powered user studies to complement their otherwise often more technology-oriented work. Thus, in the interest of reproducible research, we provide the entire code, together with the underlying models and user data.
### [Spatial Concept-based Topometric Semantic Mapping for Hierarchical Path-planning from Speech Instructions | Papers With Code](https://paperswithcode.com/paper/spatial-concept-based-topometric-semantic)
> 概要: Navigating to destinations using human speech instructions is an important task for autonomous mobile robots that operate in the real world. Spatial representations include a semantic level that represents an abstracted location category, a topological level that represents their connectivity, and a metric level that depends on the structure of the environment. The purpose of this study is to realize a hierarchical spatial representation using a topometric semantic map and planning efficient paths through human-robot interactions. We propose a novel probabilistic generative model, SpCoTMHP, that forms a topometric semantic map that adapts to the environment and leads to hierarchical path planning. We also developed approximate inference methods for path planning, where the levels of the hierarchy can influence each other. The proposed path planning method is theoretically supported by deriving a formulation based on control as probabilistic inference. The navigation experiment using human speech instruction shows that the proposed spatial concept-based hierarchical path planning improves the performance and reduces the calculation cost compared with conventional methods. Hierarchical spatial representation provides a mutually understandable form for humans and robots to render language-based navigation tasks feasible.
