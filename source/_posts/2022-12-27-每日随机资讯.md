---
title: 2022-12-27-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BlueLagoon_ZH-CN3874240119_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-12-27 22:25:57
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BlueLagoon_ZH-CN3874240119_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [告别“限速”，个人网盘进入云时代](https://www.woshipm.com/it/5716275.html)
> 概要: 网盘作为云端的储存空间，一直被用户当做资料备份在使用。近些年，不论何种网盘，都面临着空间少、下载速度慢、商业化难等问题。随着5G时代的到来，网盘市场会发生怎样的变化？一起来看看作者的观点吧。在数字经济......
### [营销技能：营销4P之外，还有这些经典理论](https://www.woshipm.com/marketing/5714209.html)
> 概要: 提到市场营销，可能很多人会联想到经典的营销4P理论，然而，4P理论只是“4字营销理论家族”中的一员，还有4R、4C理论构成了市场营销的基础部分。本文作者对“4字家族”进行了详细的介绍，一起来看一下吧......
### [push引流如何闭环？](https://www.woshipm.com/operate/5369273.html)
> 概要: 在日常移动端APP中，我们常常能够收到各类软件发送的消息提醒，这些消息提醒有的令用户反感，有的则让用户感到愉悦。在日常Push引流过程中，我们应该如何做好引流闭环？作者分享了相关经验，一起来看看。引言......
### [《三体》动画口碑直降！豆瓣跌破6分 人脸模型重复利用](https://finance.sina.com.cn/tech/discovery/2022-12-27/doc-imxyaiyh8042598.shtml)
> 概要: 改编自作家刘慈欣的系列同名长篇科幻小说——《三体》动画12月10日在B站开播，开播首日播放量破亿，12月22日播放量破2亿......
### [美互联网健身平台Peloton将提供翻新自行车：优惠最高达500美元](https://finance.sina.com.cn/tech/internet/2022-12-27/doc-imxyaixy9855841.shtml)
> 概要: 新浪科技讯 北京时间12月27日早间消息，据报道，当地时间周一，美国互联网健身平台Peloton表示，该公司将在美国大陆和加拿大提供翻新自行车，比新自行车优惠最高可达500美元......
### [Twitter要求法庭驳回被解雇员工集体诉讼：原告情况各不相同](https://finance.sina.com.cn/tech/internet/2022-12-27/doc-imxyaixy9857127.shtml)
> 概要: 新浪科技讯 北京时间12月27日早间消息，据报道，美国亿万富豪埃隆·马斯克(Elon Musk)收购社交媒体Twitter后，进行了大规模裁员。Twitter员工不服，将公司告上了法庭。而该公司也开始......
### [大疫三年，真的要终结了？](https://www.huxiu.com/article/753238.html)
> 概要: 大疫不过三年。本文来自微信公众号：国民经略 （ID：guominjinglve），作者：凯风，头图来自：视觉中国大疫三年突然之间，一切都来了。12月26日，国家卫健委官方发布通告，将新型冠状病毒肺炎更......
### [专访华兴资本董事秦川：中国企服公司全球化，要抓住新兴市场的移动互联网机遇｜软件全球化](https://36kr.com/p/2060789933919873)
> 概要: 专访华兴资本董事秦川：中国企服公司全球化，要抓住新兴市场的移动互联网机遇｜软件全球化-36氪
### [动画《死神 千年血战篇》第二季PV公开 明年7月播出](https://acg.gamersky.com/news/202212/1551039.shtml)
> 概要: 今日（12月27日），TV动画《死神：千年血战篇》官方公布动画第二季《死神：千年血战篇 诀别谭》的宣传视频，第二季将于2023年7月份播出，敬请期待。
### [CPT Markets分析：投资切忌随波逐流!严格执行才是最佳理财之道!](http://www.investorscn.com/2022/12/27/105060/)
> 概要: 投资市场里存在最多的便是这些八卦小道消息。新手们由于初入市场，对于市场总是感到陌生并深知自己经验不足而对投资感到相当的不自信，因此他们每日都会花上许多时间看遍各大财经报导或是参加坊间讲座只为求稳定获利......
### [P站美图推荐——孤独摇滚特辑](https://news.dmzj.com/article/76663.html)
> 概要: “阴郁的话就去玩摇滚!”
### [漫画杂志《EVENING》2023年2月休刊](https://news.dmzj.com/article/76664.html)
> 概要: 漫画杂志《EVENING》（讲谈社）宣布了将于2023年2月28日发售同杂志6号后休刊的消息。在休刊后，一部分作品将在讲谈社的漫画APP“Comic DAYS”上继续连载。
### [海外new things | 协作式白板「TLDraw」种子轮融资270万美元，为设计团队的协作创造提供强有力的起点](https://36kr.com/p/2056887787736710)
> 概要: 海外new things | 协作式白板「TLDraw」种子轮融资270万美元，为设计团队的协作创造提供强有力的起点-36氪
### [【字幕】阿里纳斯：可能是勇士自己泄露打架片段，以此不续约或压价格林](https://bbs.hupu.com/57136957.html)
> 概要: ﻿【三叶屋出品】JR们如果本作品满意，可以点右下方推荐👍，你的支持就是博主更新的动力，万分感谢！
### [浩瀚卓越参展深圳市专精特新“小巨人”企业融通创新路演暨创新产品展示对接会](http://www.investorscn.com/2022/12/27/105061/)
> 概要: 12月23日,专精特新 成就未来——深圳市专精特新“小巨人”企业融通创新路演暨创新产品展示对接会(以下简称“对接会”)在南山区档案服务大厦举行。工业和信息化部中小企业发展促进中心主任单立坡,深圳市中小......
### [海德股份：储能新能源资管业务布局进一步深化](http://www.investorscn.com/2022/12/27/105063/)
> 概要: 12月26日晚间，持有稀缺AMC牌照，国内困境资产管理领先企业海德股份（000567）发布两则重要公告。公告显示，海德股份持股49％的子公司北京德泰储能科技有限公司（简称“德泰储能”），作为公司储能新......
### [加速经皮介入手术机器人研发，惟德精准完成1亿人民币A轮融资](https://36kr.com/p/2061890412203906)
> 概要: 加速经皮介入手术机器人研发，惟德精准完成1亿人民币A轮融资-36氪
### [TakiToys™ Sugar x Ukiss「末路狂花 Chasing Wild」](https://www.zcool.com.cn/work/ZNjM0NjM2OTI=.html)
> 概要: 2022 # Sugar 半糖少女 x Ukiss🥀𝓢𝓾𝓰𝓪𝓻 𝓧 𝓤𝓴𝓲𝓼𝓼://𝓒𝓱𝓪𝓼𝓲𝓷𝓰 𝓦𝓲𝓵𝓭 🖤与@UKISS悠珂思® 再度联手-推出「SUGAR末路狂花」联名潮流美妆礼盒邀你开启勇敢......
### [新冠更名、“乙类乙管”，我们的生活会有什么不同？](https://www.huxiu.com/article/753494.html)
> 概要: 本文来自微信公众号：果壳 （ID：Guokr42），作者：黎小球，题图来自：视觉中国被当成甲类传染病严格管理的新冠，终于要回归乙类传染病了。12月26日晚，卫健委宣布，自2023年1月8日起，新型冠状......
### [《弈仙牌》新活动模式“论道模式” 12月29日上线](https://www.3dmgame.com/news/202212/3859256.html)
> 概要: 国产修仙在线卡组构筑游戏《弈仙牌》发布新公告宣布将于12月29日上线“论道模式”，这是一个特殊的活动模式（娱乐模式），在特定的规则下玩家之间进行PVP，“论道模式”每隔一段时间就会开启，只要达成目标就......
### [《中国责任投资年度报告2022》在第十届China SIF年会上发布](http://www.investorscn.com/2022/12/27/105066/)
> 概要: 2022年12月14日，第十届中国责任投资论坛（China SIF）年会以线上直播的形式成功举行。中国责任投资论坛自2012年由商道融绿发起，至今已走过整整十年的精彩历程。这十年里，中国责任投资论坛见......
### [张亮儿子张悦轩晒托福成绩 否认看直播关注网红](https://ent.sina.com.cn/s/m/2022-12-27/doc-imxycfee3656741.shtml)
> 概要: 新浪娱乐讯 27日，张亮儿子“天天”张悦轩晒托福考试成绩，并回应此前网络传言。张悦轩称自己初三了一直在忙学习 ，自己没空看直播，“not me！勿尬黑！”张悦轩晒出的成绩表显示他的托福成绩相当优秀。此......
### [再大的订单，也拉不动宁德时代的股价](https://www.huxiu.com/article/753421.html)
> 概要: 本文来自微信公众号：远川汽车评论 （ID：yuanchuanqiche），作者：王磊，题图来自：视觉中国近来，“宁王”喜事不断。12月14日，宁德时代与奇瑞集团、华为终端有限公司达成合作，市场解读为“......
### [国内外包病倒日番工期扑街 你会担心动画延期吗？](https://news.dmzj.com/article/76670.html)
> 概要: 今年7月开播的《异世界舅舅》，可谓是多灾多难的一部动画。先是7月播到一半就因工期问题延到了10月，而就在昨天该作又宣布了最终话将延期播出的消息。
### [网易、腾讯：元旦假期未成年人限玩公告](https://www.3dmgame.com/news/202212/3859263.html)
> 概要: 近日，腾讯游戏以及网易游戏公开元旦假期未成年人限玩公告，公告显示，未成年玩家可于12月30日-1月2日（周五及元旦节假期）每日的20时至21时之间之间登录游戏，其余工作日均为禁玩时段。结合《关于进一步......
### [《赵云传重制版》曹操军BOSS设定图 这就叫硬实力](https://www.3dmgame.com/news/202212/3859267.html)
> 概要: 《赵云传重制版》是由醉江月工作室开发、益时光工作室发行的RPG单机游戏，首次封闭测试时间调整为2023年1月4日。今日(12月27日)官方放出了曹操阵营BOSS立绘设定，分别是曹操、曹仁、夏侯惇、许褚......
### [海外new things | 金融科技初创「Vint」种子轮融资500万美元，为散户投资者提供投资于葡萄酒和烈酒的渠道](https://36kr.com/p/2057404567474056)
> 概要: 海外new things | 金融科技初创「Vint」种子轮融资500万美元，为散户投资者提供投资于葡萄酒和烈酒的渠道-36氪
### [CoolProps初代奥特曼1/1半身雕像开订 售价30万日元](https://www.3dmgame.com/news/202212/3859269.html)
> 概要: 日厂CoolProps推出的初代奥特曼1/1半身雕像现已正式开订，售价300,000日元（约合人民币15684元），预订截止至2023年4月9日，预计2024年5月发货。这款初代奥特曼1/1半身雕像宽......
### [Netflix《猎魔人》前传剧集《猎魔人：血源》上线：杨紫琼主演，共四集](https://www.ithome.com/0/663/891.htm)
> 概要: IT之家12 月 27 日消息，Netflix《猎魔人》前传剧集《猎魔人：血源》已在近日上线，目前豆瓣评分 5.2 分。IT之家了解到，该剧的时间线为《猎魔人》剧情 1200 年之前，讲述第一个猎魔人......
### [【10分钟】詹姆斯追帽名场面，一次让你看个够！](https://bbs.hupu.com/57140741.html)
> 概要: 【10分钟】詹姆斯追帽名场面，一次让你看个够！
### [1天可得4天利息，国债逆回购“薅羊毛”时点又来了](https://www.yicai.com/news/101634846.html)
> 概要: 12月29日将是操作国债逆回购的最佳时点。
### [2023跨年丢人大赛、春节联合汉化预告](https://news.dmzj.com/article/76672.html)
> 概要: 汉化组雀魂丢人大赛和情人节以及联合汉化预告
### [连接现实世界与量子世界的数字，小到令人忽视不见](https://www.ithome.com/0/663/916.htm)
> 概要: 1900 年是 20 世纪的第一年，从伽利略时代算起，近代物理学到这时候已经发展了近 300 年。300 年间，物理学家们格物致理、孜孜不倦地探求自然界的奥秘，开辟出了力学、光学、热学、电磁学等多个研......
### [第二剂加强针接种陆续开展，新冠疫苗股却集体下跌](https://www.yicai.com/news/101634908.html)
> 概要: 今年接种明显放缓，加上资产计提减值等，业绩承压
### [国家移民管理局：有序恢复护照、签注办理](https://www.yicai.com/news/101634915.html)
> 概要: 恢复受理审批外国人申请普通签证延期、换发、补发。
### [499 元，Redmi Watch 3 大屏手表正式发布：支持 NFC、蓝牙通话、AOD 息屏显示](https://www.ithome.com/0/663/931.htm)
> 概要: IT之家12 月 27 日消息，在今日晚间召开的 Redmi 2023 新年发布会上，Redmi Watch 3 大屏手表正式发布，售价 499 元。IT之家了解到，Redmi Watch 3采用 1......
### [交通运输部：明年1月8日起，国际道路客运将逐步有序恢复](https://finance.sina.com.cn/china/2022-12-27/doc-imxycrtw6928623.shtml)
> 概要: 来源：交通运输部 自2023年1月8日起，调整公路口岸国际道路运输“客停货通”政策，在公路口岸恢复客运出入境服务后，逐步有序恢复国际道路客运服务。
### [农业农村部：抓紧研究制定加快建设农业强国规划](https://finance.sina.com.cn/china/2022-12-27/doc-imxycrty3716772.shtml)
> 概要: 【农业农村部：抓紧研究制定加快建设农业强国规划】财联社12月27日电，中央农办主任，农业农村部党组书记、部长唐仁健主持召开部党组会议，传达学习中央经济工作会议...
### [报告：2021年深圳上市企业达484家，境外市场IPO步伐放缓](https://finance.sina.com.cn/jjxw/2022-12-27/doc-imxycrtw6942339.shtml)
> 概要: 上市企业发展状态在一定程度上是城市经济发展的“晴雨表”。截至2021年底，深圳境内外上市企业总数达到484家，广泛分布于国内外主要资本市场。
### [河北国资接盘，华夏幸福“卖子”回笼10亿元](https://www.yicai.com/news/101634985.html)
> 概要: “出险房企”华夏幸福最近动作频频。
### [中国制造加速“出海”！120辆安凯客车出口刚果金](http://www.investorscn.com/2022/12/27/105071/)
> 概要: 近日,由刚果金采购的120辆中国客车抵达首都金沙萨,并将在当地投入运营。据悉,这批车辆全部由安凯客车提供,是中国客车出口刚果金的最大订单,展现了安凯客车在国际市场的稳步开拓,为中国制造“出海”增添了浓......
### [后援会辟谣周也与王星越恋情：二人为合作关系](https://ent.sina.com.cn/s/m/2022-12-27/doc-imxycrty3735758.shtml)
> 概要: 新浪娱乐讯 12月27日，八卦媒体爆料“恶女95花恋情”，随后有网友猜测是周也与王星越，两人刚合作《为有暗香来》，引发热议。随后，周也后援会发文否认，“纯属造谣！经与公司沟通确认，二人为合作关系，切勿......
### [东莞取消楼市限购后，中介称客户咨询量大增，全国已有多个城市宣布放松限购](https://finance.sina.com.cn/china/2022-12-27/doc-imxycrtu9885511.shtml)
> 概要: 红星资本局原创 记者|强亚铣 ↑广东东莞商品住宅楼群楼盘。图据IC photo 12月26日，东莞市住房和城乡建设局宣布，暂停实行莞城街道等5个地区的商品住房限购政策。
### [感染后是否会影响心血管系统？康复期能否进行运动锻炼？专家解答](https://finance.sina.com.cn/wm/2022-12-27/doc-imxycrtw6971499.shtml)
> 概要: 北京安贞医院常务副院长周玉杰介绍，感染新冠病毒5-7天后，大部分人群能够症状好转、逐步康复，但也有少数患者会累及心肌，一般发生在感染新冠病毒两周以后
### [消息称三星 Galaxy S22 系列未达 3000 万台销量预期](https://www.ithome.com/0/663/944.htm)
> 概要: IT之家12 月 27 日消息，据 fnnews 报道，韩国消息人士称，三星今年的设备 ASP（平均销售价格）仅提高了 2%，而苹果提高了 7%，与苹果的 ASP 差距可能还会进一步拉大。IT之家了解......
### [社论：新冠“乙类乙管”需要社会治理能力精进](https://www.yicai.com/news/101635037.html)
> 概要: 战疫模式的结束，是社会公共治理防疫的开始，我们期待所有各方都做好准备，答好各自的答卷，不越位、不缺位、不错位。
### [7900 XTX玩游戏达到110度 玩家要求退款遭拒](https://www.3dmgame.com/news/202212/3859280.html)
> 概要: 据报道AMD拒绝了一名玩家RX 7900 XTX显卡的退款要求，玩家表示他的7900 XTX工作时温度达到了110度。AMD Radeon 7900系列已经发售几个星期了，然而有一些买到的玩家反应显卡......
### [流言板弗格全场比赛投篮13中9，得到24分4篮板2抢断](https://bbs.hupu.com/57143620.html)
> 概要: 虎扑12月27日讯 2﻿022-23赛季CBA常规赛第19轮，北京队81-78战胜辽宁队。此役，辽宁队外援弗格全场比赛出战16分钟，投篮13中9，三分球4中1，得到24分4篮板2抢断。   来源： 虎
### [因田小娟确诊 (G)I-DLE歌谣大祭典事前录制变更](https://ent.sina.com.cn/y/yrihan/2022-12-27/doc-imxycrty3797525.shtml)
> 概要: 新浪娱乐讯 12月27日，（G）I-DLE方发文称，原定于12月28日（周三）进行的《2022MBC歌谣大祭典》事前录制（团体）因成员小娟的新型冠状病毒确诊和隔离，不得已无法进行。　　由于日程突然变更......
### [流言板顾全CBA生涯三分命中达到1000个，位列历史三分榜第10](https://bbs.hupu.com/57143924.html)
> 概要: 虎扑12月27日讯 2022-23赛季CBA常规赛第19轮，深圳队104-94战胜山西队。此役深圳队球员顾全投篮13中6，其中三分11中4，得到19分5篮板2助攻1抢断。此役过后，顾全CBA生涯三分命
# 小说
### [我有一棵世界树](https://book.zongheng.com/book/1035774.html)
> 作者：小小白泽

> 标签：奇幻玄幻

> 简介：别人在为神功神器神丹烦恼时候，叶金却是为了收取世界的气运，本源之力的事情烦恼。从小千世界到完美世界，甚至还有幻想世界。

> 章节末：第七百九十三章 大结局

> 状态：完本
# 论文
### [One-shot Generative Prior Learned from Hankel-k-space for Parallel Imaging Reconstruction | Papers With Code](https://paperswithcode.com/paper/one-shot-generative-prior-learned-from-hankel)
> 日期：15 Aug 2022

> 标签：None

> 代码：https://github.com/yqx7150/hkgm

> 描述：Magnetic resonance imaging serves as an essential tool for clinical diagnosis. However, it suffers from a long acquisition time. The utilization of deep learning, especially the deep generative models, offers aggressive acceleration and better reconstruction in magnetic resonance imaging. Nevertheless, learning the data distribution as prior knowledge and reconstructing the image from limited data remains challenging. In this work, we propose a novel Hankel-k-space generative model (HKGM), which can generate samples from a training set of as little as one k-space data. At the prior learning stage, we first construct a large Hankel matrix from k-space data, then extract multiple structured k-space patches from the large Hankel matrix to capture the internal distribution among different patches. Extracting patches from a Hankel matrix enables the generative model to be learned from redundant and low-rank data space. At the iterative reconstruction stage, it is observed that the desired solution obeys the learned prior knowledge. The intermediate reconstruction solution is updated by taking it as the input of the generative model. The updated result is then alternatively operated by imposing low-rank penalty on its Hankel matrix and data consistency con-strain on the measurement data. Experimental results confirmed that the internal statistics of patches within a single k-space data carry enough information for learning a powerful generative model and provide state-of-the-art reconstruction.
### [SFNet: Faster, Accurate, and Domain Agnostic Semantic Segmentation via Semantic Flow | Papers With Code](https://paperswithcode.com/paper/sfnet-faster-accurate-and-domain-agnostic)
> 日期：10 Jul 2022

> 标签：None

> 代码：https://github.com/lxtGH/SFSegNets

> 描述：In this paper, we focus on exploring effective methods for faster, accurate, and domain agnostic semantic segmentation. Inspired by the Optical Flow for motion alignment between adjacent video frames, we propose a Flow Alignment Module (FAM) to learn \textit{Semantic Flow} between feature maps of adjacent levels, and broadcast high-level features to high resolution features effectively and efficiently. Furthermore, integrating our FAM to a common feature pyramid structure exhibits superior performance over other real-time methods even on light-weight backbone networks, such as ResNet-18 and DFNet. Then to further speed up the inference procedure, we also present a novel Gated Dual Flow Alignment Module to directly align high resolution feature maps and low resolution feature maps where we term improved version network as SFNet-Lite. Extensive experiments are conducted on several challenging datasets, where results show the effectiveness of both SFNet and SFNet-Lite. In particular, the proposed SFNet-Lite series achieve 80.1 mIoU while running at 60 FPS using ResNet-18 backbone and 78.8 mIoU while running at 120 FPS using STDC backbone on RTX-3090. Moreover, we unify four challenging driving datasets (i.e., Cityscapes, Mapillary, IDD and BDD) into one large dataset, which we named Unified Driving Segmentation (UDS) dataset. It contains diverse domain and style information. We benchmark several representative works on UDS. Both SFNet and SFNet-Lite still achieve the best speed and accuracy trade-off on UDS which serves as a strong baseline in such a new challenging setting. All the code and models are publicly available at https://github.com/lxtGH/SFSegNets.
