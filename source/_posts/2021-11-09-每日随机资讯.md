---
title: 2021-11-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DalyanTombs_ZH-CN1519154607_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-09 22:20:19
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DalyanTombs_ZH-CN1519154607_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [再起情怀！Furion v3.0.0 发布，完成 .NET6 兼容适配](https://www.oschina.net/news/168091/furion-3-0-0-released)
> 概要: 举杯对于 .NET 开发者来说，今天是值得举杯的日子，因为 .NET6 LTS 版本发布了。https://www.oschina.net/news/167972/dotnet-6-released无......
### [Linux Lab 发布 v0.9-rc1，新增适配 Kali 和 ezgo 两个发行版](https://www.oschina.net/news/167987/linux-lab-v0-9-rc1-released)
> 概要: 简介自 Linux Lab v0.8 正式版发布以来，社区计划在 v0.9 继续新增嵌入式图形系统 GuiLite 和 RISC-V 真实开发板支持，目前急需社区的同学踊跃报名参与，有一定基础并且乐于......
### [Airbnb 开源无服务器 PKI 框架 Ottr](https://www.oschina.net/news/167979/airbnb-open-sources-ottr)
> 概要: Airbnb 近日宣布开源Ottr，这是一个 Airbnb 内部开发及使用的无服务器公钥基础设施（PKI、Public Key Infrastructure）框架。Ottr 无需使用代理即可处理端到端......
### [华为捐赠欧拉，共建数字基础设施开源操作系统](https://www.oschina.net/news/168023)
> 概要: 中国，北京，2021年11月9日 今天，操作系统产业峰会2021在北京国家会议中心线上线下同步举办。会上，华为携手社区全体伙伴共同将欧拉开源操作系统（openEuler;简称“欧拉”）正式捐赠给开放原......
### [在IntelliJ IDEA中，开发一个摸鱼看书插件](https://segmentfault.com/a/1190000040929376?utm_source=sf-homepage)
> 概要: 作者：小傅哥博客：https://bugstack.cn原文：https://mp.weixin.qq.com/s/R8qvoSNyedVM95Ty8sbhgg沉淀、分享、成长，让自己和他人都能有所收......
### [一些常用的 Git 进阶知识与技巧](https://segmentfault.com/a/1190000040930212?utm_source=sf-homepage)
> 概要: 1. 同一电脑存在多个 Git 账号假设我们在同一电脑上拥有多个 Git 账号，例如公司内部使用的是 Gitlab，个人使用的是 Github 或者 Gitee。那就会遇到一种情况，上班时想给个人开源......
### [动画《攻壳机动队 SAC_2045》系列第二季2022年播出](https://news.dmzj.com/article/72698.html)
> 概要: 根据士郎正宗原作制作的动画《攻壳机动队 SAC_2045》宣布了第二季将于2022年在Netflix上播出。
### [口嫌体正直大姐头的怪XP转校生](https://news.dmzj.com/article/72699.html)
> 概要: 这期给大家推荐一部百合漫《大姐头与转校生》，作者ふじちか，霜月汉化组汉化，讲述转校生×大姐头可爱又羞人的80年代校园喜剧故事。
### [《七大罪》外传动画电影《七大罪怨嗟爱丁堡》制作决定](https://news.dmzj.com/article/72700.html)
> 概要: 根据铃木央原作制作的动画《七大罪》的外传动画电影《七大罪怨嗟爱丁堡》宣布了制作决定的消息。
### [《小太郎一个人生活》动画化决定！钉宫理惠为小太郎配音](https://news.dmzj.com/article/72703.html)
> 概要: 津村麻美创作的漫画《小太郎一个人生活》宣布了动画化决定的消息。本作将于2022年春在Netflix播出。
### [漫画「YUYU式」第12卷封面公开](http://acg.178.com/202111/430420880921.html)
> 概要: 漫画「YUYU式」公开了第12卷的封面图，该卷将于11月26日发售。「YUYU式」是三上小又创作的漫画作品，KINEMA CITRUS负责改编的同名电视动画于2013年4月9日起播出......
### [「魔女之旅」POP UP SHOP最新宣传图公开](http://acg.178.com/202111/430423721576.html)
> 概要: 「魔女之旅」宣布将举办「魔女之旅 POP UP SHOP in 渋谷マルイ」活动，并公开了宣传图。该活动将于2021年12月10日开始举办，持续至2021年12月19日。「魔女之旅」（魔女の旅々）是白......
### [《机动奥特曼》第2季预告！泰罗登场、六兄弟集结](https://acg.gamersky.com/news/202111/1436502.shtml)
> 概要: 泰罗奥特曼登场，带来新的战斗。泰罗将由，《机动奥特曼》第二季动画将在Netflix上独播。
### [《哆啦A梦:大雄的宇宙小战争》重新定档 明年3月上映](https://acg.gamersky.com/news/202111/1436516.shtml)
> 概要: 《哆啦A梦：大雄的宇宙小战争2021》宣布定档2022年3月4日日本上映。
### [Legacy Train Control System Stabilisation (2012) pdf](https://webinfo.uk/webdocssl/irse-kbase/ref-viewer.aspx?refno=1559669757&document=2.10%20strangaric%20-%20legacy%20train%20control%20system%20stabilisation.pdf)
> 概要: Legacy Train Control System Stabilisation (2012) pdf
### [动画「JOJO的奇妙冒险 石之海」公开四个新PV](http://acg.178.com/202111/430428042683.html)
> 概要: 电视动画「JOJO的奇妙冒险 石之海」公开了四个新PV（空条徐伦、替身、父女、水族馆），本作将于2021年12月1日在网飞先行播出，全12话一次放送。「JOJO的奇妙冒险 石之海」新PV空条徐伦「JO......
### [乙一×天野喜孝打造网飞太空恐怖动画 2022年上线](https://acg.gamersky.com/news/202111/1436551.shtml)
> 概要: 著名作家乙一创作，天野喜孝进行角色设计的原创太空恐怖动画《exception》，将于2022年在Netflix独家上线。
### [把“穷人”设想成一个庞大的、无区别的阶级是荒谬的](https://www.huxiu.com/article/470828.html)
> 概要: 本文来自微信公众号：界面文化（ID：BooksAndFun），作者：林子人，编辑：黄月，原文标题：《美国作家凯瑟琳·布：把“穷人”设想成一个庞大的、无区别的阶级是荒谬的 | 专访》，头图来自：视觉中国......
### [西二旗站没有往事](https://www.huxiu.com/article/470812.html)
> 概要: 本文来自微信公众号：刺猬公社（ID：ciweigongshe），作者：陈梅希，编辑：园长，题图来自：视觉中国很少有场所能像地铁站那么抽象。当它出现在盘根错节的线路图中，以一个圆点的方式标记自身存在时，......
### [动画「花园里的吸血鬼」新视觉图公开](http://acg.178.com/202111/430434052691.html)
> 概要: 动画「花园里的吸血鬼」公开了新视觉图，本作由潘めぐみ&小林ゆう担任主要配音，将于2022年在Netflix上线。CAST潘めぐみ、小林ゆうSTAFF导演：牧原亮太郎（「ハル」、「屍者の帝国」）副导演：......
### [高玩《马里奥64》新RTA纪录 1时37分全120星收集](https://www.3dmgame.com/news/202111/3827851.html)
> 概要: 1996年发售的任天堂神作游戏《超级马里奥N64》至今依然有玩家在不断游玩，不过更多的目的是挑战速通新纪录，11月7日一位专门玩《超级马里奥N64》RTA的玩家缔造了全新速通记录，1时37分53秒达成......
### [腾讯下架PC版QQ秀 红钻项目已于去年结束续费](https://www.3dmgame.com/news/202111/3827855.html)
> 概要: 更新：11月9日，腾讯官方回应称，PC端QQ秀只是折叠在聊天窗口，并未正式下线。据财经网科技今日消息，最新PC版QQ 9.5.2版本中，QQ秀已经正式下线，用户将无法再看到自己的 QQ秀装扮形象。QQ......
### [《名侦探柯南》两部外传动画版 将独家登陆Netflix](https://www.3dmgame.com/news/202111/3827856.html)
> 概要: 据今天的Netflix Festival Japan 2021上的消息，《名侦探柯南》外传《名侦探柯南犯人 犯人·犯泽先生》以及《名侦探柯南 零的日常》动画版将于电视广播（地面信号与BS）和Netfl......
### [少女时代金泰妍被恶意攻击 SM娱乐将采取法律手段](https://ent.sina.com.cn/y/yrihan/2021-11-09/doc-iktzscyy4517647.shtml)
> 概要: 新浪娱乐讯 韩国SM娱乐公司今天宣布将对网上针对少女时代成员泰妍的恶意攻击采取更严厉的法律手段。　　SM娱乐公司表示，公司为保护艺人而持续在网上收集资料和证据并对恶意攻击者采取法律手段，已经有一批针对......
### [读 What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?](https://www.tuicool.com/articles/j2ErYnF)
> 概要: What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?论文链接：https://arxiv.org/p......
### [《摇曳露营△》志摩凛手办推出 萌妹子快乐宿营](https://acg.gamersky.com/news/202111/1436641.shtml)
> 概要: 知名玩具制造商Max Factory宣布，将推出《摇曳露营△》志摩凛POP UP PARADE手办。
### [川渝红汤/满口肉香，味美冠香辣肉臊拌面 6 盒 25.8 元（减 30 元）](https://lapin.ithome.com/html/digi/585655.htm)
> 概要: 川渝红汤/满口肉香，味美冠香辣肉臊拌面 6 盒报价 55.8 元，限时限量 30 元券，实付 25.8 元包邮，领券并购买。使用最会买 App下单，预计还能再返 4.25 元，返后 21.55 元包邮......
### [塔防游戏《经验教训》预计明年发售 支持简体中文](https://www.3dmgame.com/news/202111/3827876.html)
> 概要: 今日（11月9日），MadGamesmith工作室开发的塔防游戏《经验教训》宣布在2022年正式发售，该作支持简体中文，感兴趣的玩家可以点击此处进入商店页面。游戏预告：游戏介绍：《经验教训》是塔防类游......
### [金钟国检测391种兴奋剂 回击服药塑造身材质疑](https://ent.sina.com.cn/s/j/2021-11-09/doc-iktzqtyu6309796.shtml)
> 概要: 新浪娱乐讯 据韩国报道，艺人金钟国检测391种兴奋剂，正面回应此前被质疑服药打造身材。　　之前金钟国被国外健身网红Greg Doucette质疑使用药物健身，而Greg Doucette质疑金钟国的依......
### [组图：神宫寺勇太主演舞台剧召开记者会 中山美穗感动落泪](http://slide.ent.sina.com.cn/star/jp/slide_4_704_363533.html)
> 概要: 组图：神宫寺勇太主演舞台剧召开记者会 中山美穗感动落泪
### [融资新闻 | 加密货币初创公司Notabene完成1020万美元A轮融资](https://www.tuicool.com/articles/EN7Bviy)
> 概要: 据The Block 11月9日报道，在全球监管机构金融行动特别工作组（FATF）最终敲定指导方针后，总部位于纽约的加密货币合规初创公司Notabene宣布获得1020万美元的A轮融资。总部位于芝加哥......
### [Ruby 3.1.0 Preview 1](https://www.ruby-lang.org/en/news/2021/11/09/ruby-3-1-0-preview1-released/)
> 概要: Ruby 3.1.0 Preview 1
### [WSL2 can now mount Linux ext4 disks directly](https://www.hanselman.com/blog/wsl2-can-now-mount-linux-ext4-disks-directly)
> 概要: WSL2 can now mount Linux ext4 disks directly
### [AMD下一代GPU或采用3D无限缓存 性能将有大的飞跃](https://www.3dmgame.com/news/202111/3827889.html)
> 概要: AMD似乎在CPU和GPU上都大力投资堆栈缓存和小芯片技术，CPU方面，代号Milan-X、基于Zen 3架构和配备3D垂直缓存（3D V-Cache）技术的EPYC处理器已正式发布，而GPU方面，基......
### [营收翻倍仍未盈利，“元宇宙第一股”Roblox暴涨30%的动力在哪儿？| 财报热评](https://www.tuicool.com/articles/ami6ruu)
> 概要: 图片来源视觉中国11月8日美股盘后，“元宇宙第一股”Roblox发布了2021年Q3财报。数据显示，三季度Roblox营收增长102%，DAU同比增长31%，业绩远高于华尔街分析师预期。受此影响，Ro......
### [为了力证鞠婧祎耳朵后面的发量很真实，有人连医学图都用上了？](https://new.qq.com/rain/a/20211109A0GXID00)
> 概要: 不得不说，鞠婧祎这一次的新剧太难了。从开始网友们就吐槽她，虽然剧中的造型很多，但却是千篇一律的半永久妆。搞得好像造型师工作了，又好像没工作一样。            好不容易这一次的妆容算是说倦了，......
### [BuildZoom (better way to build custom homes) Is hiring a Principal Engineer](https://jobs.lever.co/buildzoom)
> 概要: BuildZoom (better way to build custom homes) Is hiring a Principal Engineer
### [组图：吴宣仪亮片露脐装与程潇同场比美 朱正廷李汶翰西装帅气](http://slide.ent.sina.com.cn/star/slide_4_704_363542.html)
> 概要: 组图：吴宣仪亮片露脐装与程潇同场比美 朱正廷李汶翰西装帅气
### [组图：《谢谢你医生》杀青 杨幂白宇手捧鲜花同框比耶心情好](http://slide.ent.sina.com.cn/tv/slide_4_704_363543.html)
> 概要: 组图：《谢谢你医生》杀青 杨幂白宇手捧鲜花同框比耶心情好
### [英雄联盟的英雄都是如此成长的，没金克斯为什么这么刚？](https://new.qq.com/omn/20211109/20211109A0I9AS00.html)
> 概要: 引言：英雄联盟的英雄都是如此成长的，没有挫折，怎么造就刚毅？在英雄联盟背后的这一个动漫里面，我们看到他们其实有很多相似的地方，这一个大姐也就是大家熟悉的蔚，这个时候他为了维护自己的伙伴，也就是后来的金......
### [分享 | 道法术器响应式 Spring](https://www.tuicool.com/articles/6JjuemI)
> 概要: 曾有读者问我之前的文章《微服务网关演进之路》里提到的编程框架SpringWebflux有没有比较好的学习资料或者文档。我都是看官方文档和网上比较零碎的资料，对新手来说不友好。也想过自己来写一个关于响应......
### [池子为EDG夺冠立flag，退网前愤怒吐槽网络环境现状，字字在理](https://new.qq.com/rain/a/20211109A0INKM00)
> 概要: 在S11全球总决赛中，edg的夺冠振奋了整个娱乐圈。比赛前，很多艺人的相关话题就接二连三登上热搜榜，又是“罗云熙怒吼edg加油”“龚俊为edg打call”，又是各路明星为edg夺冠花样立flag.  ......
### [支付宝蚂蚁森林“能量大户”注意：一次能种一棵树的能量球上线了](https://www.ithome.com/0/585/705.htm)
> 概要: IT之家11 月 9 日消息，蚂蚁森林最大的能量团有多大？18400g！据天猫官方发布，史上最大的蚂蚁森林能量在这个双 11 上线，只要在天猫绿色家电会场下单绿色家电，确认收货后，就能获得一笔相当可观......
### [数字人民币将用于跨境支付？易纲：鉴于其复杂性，当前以满足国内零售需求为主](https://finance.sina.com.cn/china/gncj/2021-11-09/doc-iktzscyy4591568.shtml)
> 概要: 原标题：数字人民币将用于跨境支付？易纲：鉴于其复杂性，当前以满足国内零售需求为主 央行行长易纲：数字人民币累计开立个人钱包1.23亿个...
### [中储棉：10日起开始2021年第二批中央储备棉投放 总量60万吨](https://finance.sina.com.cn/china/gncj/2021-11-09/doc-iktzscyy4595180.shtml)
> 概要: 中国储备棉管理有限公司\r\n关于2021年第二批中央储备棉投放的公告 中储棉信息中心 2021年第一批中央储备棉已结束投放。根据国家相关部门要求...
### [内蒙古额济纳旗新增1例新冠肺炎无症状感染者](https://finance.sina.com.cn/china/gncj/2021-11-09/doc-iktzscyy4590846.shtml)
> 概要: 内蒙古额济纳旗新冠肺炎防控工作指挥部11月9日发布通告称：11月9日，经流行病学调查和核酸复检，并经自治区临床专家组诊断，额济纳旗确认新增1例新冠肺炎无症状感染者。
### [【火影忍者究极风暴Ⅳ】奥义彩蛋：宇智波佐助篇](https://new.qq.com/omn/20211109/20211109A0J5BU00.html)
> 概要: 【火影忍者究极风暴Ⅳ】奥义彩蛋：宇智波佐助篇
### [《跑男》变“走男”？新导演风格引争议，网友吐槽在营销虚假快乐](https://new.qq.com/rain/a/20211109A0J9LY00)
> 概要: 蓝台的综艺《跑男》一直被广大网友观众所喜爱，只是最新一季的《跑男》产出有点困难，甚至一度闹起了人荒。仅仅剩下Angelababy、郑恺、李晨三位元老嘉宾的情况下，还传出Angelababy与节目组闹掰......
### [苹果又一款 Apple-1 拍卖，比大多数电脑更特别](https://www.ithome.com/0/585/710.htm)
> 概要: IT之家11 月 9 日消息，现在又有一台 Apple-1 电脑将开始拍卖，这台电脑声称比大多数电脑更特别。拍卖师约翰・莫兰称，在苹果生产的 200 台初代电脑中，有 50 台是通过 ByteShop......
### [进博会热议：“一带一路”为全球食品安全保驾护航](https://finance.sina.com.cn/china/gncj/2021-11-09/doc-iktzqtyu6356901.shtml)
> 概要: 中国青年报客户端上海11月9日电（中青报·中青网记者 马子倩）保障食品安全是世界各国面临的共同课题。随着经济全球化的深入发展、世界贸易的日益活跃...
### [这个社会还需要英专生吗？](https://www.huxiu.com/article/471049.html)
> 概要: 前段时间，一位长沙摩的司机火了。在流传最广的一则短视频里，他站在湘江边上，用一段流利的英文朗诵《沁园春·雪》。而当“985硕士，英语老师，裸考过专八”与“开摩的”标签，同时出现一个人的身上时，人们不由......
### [S11全球总决赛结束，英雄联盟双城之战上线，人物背景细节塑造非常强](https://new.qq.com/omn/20211109/20211109A0JMAC00.html)
> 概要: S11全球总决赛结束，英雄联盟双城之战：人物和背景细节塑造强，画风很符合双城风格，整体非常细腻，让热爱英雄联盟游戏的玩家感受到了拳头的诚意。雾山五行：中国的水墨特色，独特的艺术风格，尤其是打斗画面风格......
### [元宇宙进入英伟达时间](https://www.huxiu.com/article/471054.html)
> 概要: 出品｜虎嗅科技组作者｜张雪封面｜NVIDIA 官网今年8月，英伟达首席执行官黄仁勋的一段“虚拟人”视频在各大社交平台刷了屏。原因在于，在一次公开演讲中，黄仁勋用了14秒的“虚拟人”替身，但由于“虚拟人......
### [一切从此改变：三星有望于 11 月 19 日推出 Exynos 2200，AMD GPU 技术加持](https://www.ithome.com/0/585/712.htm)
> 概要: IT之家11 月 9 日消息，外媒表示，三星电子可能在 11 月 19 日正式公开新一代“Exynos 2200”，但也有人认为会是 Exynos 1250。IT之家了解到，三星 Exynos 在 I......
### [易纲：数字人民币试点场景已超过350万个，累计开立个人钱包1.23亿个](https://finance.sina.com.cn/china/2021-11-09/doc-iktzscyy4594534.shtml)
> 概要: 易纲：数字人民币试点场景已超过350万个，累计开立个人钱包1.23亿个…… 证券日报网证券日报之声 11月9日，中国人民银行发布中国人民银行行长易纲在芬兰央行新兴经济体研究院...
### [少女时代的徐贤突然变了脸，散发出成熟美的素颜让人认不出来](https://new.qq.com/rain/a/20211109A0JPFH00)
> 概要: 少女时代徐贤，突然变了脸…散发出成熟美的素颜让人认不出来            组合少女时代的徐贤以与众不同的视觉效果夺走了大家的视线。徐贤通过在自己的instagram，上传了几张照片和“快乐星期一......
### [S11全球总决赛结束，英雄联盟双城之战无论从灯光渲染，角色设计，都很强](https://new.qq.com/omn/20211109/20211109A0JV2N00.html)
> 概要: S11全球总决赛结束，双城之战这部动漫无论从灯光渲染，氛围，角色设计，动作流畅度，各方面来讲，都很强，场景细节也非常足，质量堪比动画电影，里面涉及到得设计语言太多。比起动漫，我更喜欢s11开幕式，太叼......
### [成都环球中心已恢复正常经营](https://finance.sina.com.cn/jjxw/2021-11-09/doc-iktzscyy4595867.shtml)
> 概要: 刚刚记者从成都高新区获悉，昨天，成都环球中心完成核酸检测3万余人，检测结果全部呈阴性。在全面完成环境消杀后，今日，成都环球中心已恢复正常经营。
# 小说
### [帝族誓约](http://book.zongheng.com/book/1056007.html)
> 作者：泡沫狙击手

> 标签：奇幻玄幻

> 简介：神语在耳边呢喃。王座之上，少年俯视众神。黄金城，烛天之址，智慧树枝，神与生灵签订誓约。诸神时代的降临。有人说，谁都逃不过命运，而在王座之下，那个凝视少年的人紧握若铭，随时准备划破那个少年的悲哀。这是一个隐藏在和平下的新时代，人类与神的黄昏时代。众生盼我遗失所有，我赐众生失苦积悲。

> 章节末：终章 破晓云开的希望

> 状态：完本
# 论文
### [Dynamic Attentive Graph Learning for Image Restoration | Papers With Code](https://paperswithcode.com/paper/dynamic-attentive-graph-learning-for-image)
> 日期：14 Sep 2021

> 标签：None

> 代码：None

> 描述：Non-local self-similarity in natural images has been verified to be an effective prior for image restoration. However, most existing deep non-local methods assign a fixed number of neighbors for each query item, neglecting the dynamics of non-local correlations.
### [Measuring Fairness under Unawareness via Quantification | Papers With Code](https://paperswithcode.com/paper/measuring-fairness-under-unawareness-via)
> 日期：17 Sep 2021

> 标签：None

> 代码：None

> 描述：Models trained by means of supervised learning are increasingly deployed in high-stakes domains, and, when their predictions inform decisions about people, they inevitably impact (positively or negatively) on their lives. As a consequence, those in charge of developing these models must carefully evaluate their impact on different groups of people and ensure that sensitive demographic attributes, such as race or sex, do not result in unfair treatment for members of specific groups.
