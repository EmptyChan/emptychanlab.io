---
title: 2022-10-10-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ValvestinoDam_ZH-CN8397604653_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-10 22:00:12
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ValvestinoDam_ZH-CN8397604653_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [从“排斥”到“礼待”，圈层化的互联网正在发生变化](https://www.woshipm.com/it/5637488.html)
> 概要: 曾经，一则抖音短视频搬运到了B站，受到了厌恶与不屑；可如今，越来越多的抖音博主入驻B站，受到了“礼遇”，并完成了他们的迁徙之路，找到了自己在B站的舒适圈。这样文化的交融变化，不仅仅代表着当下，更是未来......
### [如果我是B站，怎么插广告](https://www.woshipm.com/marketing/5637653.html)
> 概要: 喜欢用B站的用户，除了是喜欢B站的内容产品和社区氛围，还有很大一部分原因是没有广告。但是，随着商业竞争的逐渐激烈，B站也不可避免的要在产品中加入广告。本文作者从产品和用户的角度出发，分析如果B站要加广......
### [数字化转型的背景下，连锁超市该如何做变革？](https://www.woshipm.com/newretail/5636928.html)
> 概要: 目前超市企业主要遇到了三个问题和挑战：新零售形式越来越多、全渠道零售的快速发展、数字经济带来的转型挑战与机遇。这无疑不让超市企业提高警惕，该如何做出更系统、更完整的转型思考，让企业能够以更低的成本运营......
### [“夸父”逐日，开启中国太阳观测新时代](https://finance.sina.com.cn/tech/roll/2022-10-10/doc-imqmmthc0297773.shtml)
> 概要: 向秋......
### [浅谈移动端单屏解决方案](https://segmentfault.com/a/1190000042587022)
> 概要: 前文《移动端常见适配方案》中，我介绍了移动端的一些常见适配方案移动端适配就是在进行屏幕宽度的等比例缩放文中我强调了移动端适配是对屏幕宽度进行缩放：对于普通的流式布局(长屏幕页面)，页面内容是可以上下滚......
### [Flutter Cocoon 已达到 SLSA 2 级标准的要求](https://segmentfault.com/a/1190000042593375)
> 概要: 文/ Jesse Seales, Dart 和 Flutter 安全工作组工程师今年年初，我们发布了Flutter 2022 产品路线图，其中「基础设施建设」这部分提到：2022 年 Flutter ......
### [开网约车、送外卖……小心！身兼“数职”可能暗藏劳动风险](https://finance.sina.com.cn/tech/internet/2022-10-10/doc-imqmmthc0308672.shtml)
> 概要: 来源：工人日报......
### [设计模式-单例](https://segmentfault.com/a/1190000042593873)
> 概要: 在整个应用中共享一个「单一的」「全局的」实例实现单例是可以全局访问并且仅实例化一次的类。这个单一实例是可以在整个应用中被共享的，这使得单例非常适合管理应用程序中的全局状态首先，让我们看看使用 ES20......
### [蔚来深入BBA腹地，手上还留有底牌｜次世代车研所](https://finance.sina.com.cn/tech/it/2022-10-10/doc-imqqsmrp2017162.shtml)
> 概要: 整个国庆假期，蔚来创始人、董事长兼CEO李斌和蔚来联合创始人、总裁秦力洪一起在欧洲度过......
### [书都出版了，疫情还没过去](https://www.zcool.com.cn/work/ZNjIyNzQ1NDA=.html)
> 概要: 19年11月接的合作，16个月的创作时间，然后是漫长的出版审核……万万没想到都出版了，疫情还在，希望疫情早点过去......
### [阳振坤：分布式技术引领关系数据库发展](https://segmentfault.com/a/1190000042596090)
> 概要: 近日，HI COOL 全球创业者峰会在北京召开，OceanBase 创始人兼首席科学家阳振坤受邀出席了本次大会，并发表了<u>《阳振坤：分布式技术引领关系数据库发展》</u>的主题演讲，分享了全球主流......
### [NV推新版RTX3060 Ti：核心没变 显存升级GDDR6X](https://www.3dmgame.com/news/202210/3853398.html)
> 概要: 早在九月底，就有消息称NVIDIA计划推出改版的RTX 3060 Ti显卡。现在这款新显卡突然出现在英国零售商Scan网页上，标价369英镑(约合人民币2914元)，比目前市面上的非公版RTX 306......
### [魔芯KOKONI 3D打印机：为家庭打印定制无限的创意快乐](http://www.investorscn.com/2022/10/10/103400/)
> 概要: 3D打印机又称三维打印机(3DP),是一种累积制造技术,即快速成形技术的一种机器,它是一种数字模型文件为基础,运用特殊蜡材、粉末状金属或塑料等可粘合材料,通过打印一层层的粘合材料来制造三维的物体。现阶......
### [艾伦摩尔：漫画没变成熟 观众更加幼稚](https://www.3dmgame.com/news/202210/3853403.html)
> 概要: 漫画大师艾伦摩尔近日接受了英国卫报采访，采访过程中摩尔一再表现出对现代漫画行业的厌恶，同时严厉批判当前漫画行业趋势以及观众普遍品味，此外，摩尔还表示虽然对漫画形式并不反感但自己不再打算回归漫画行业。摩......
### [IDEAS BOOK VOL.13](https://www.zcool.com.cn/work/ZNjIyNzk5NzY=.html)
> 概要: 又跟大家见面啦~......
### [嘿·犟牛 酱汁牛排拌饭 品牌策略视觉 餐饮品牌](https://www.zcool.com.cn/work/ZNjIyODA3NzY=.html)
> 概要: BRAND丨品牌名称 ：嘿·犟牛INDUSTRY丨品牌物理定位：酱汁牛排拌饭（商务餐/小吃）ADDRESS丨项目地址：泉州市设计团队 / Project team ：言合品牌设计事务所设计总监 / D......
### [萌娃初长成！诺一妈妈发文庆祝儿子12岁生日快乐](https://ent.sina.com.cn/s/m/2022-10-10/doc-imqqsmrp2066461.shtml)
> 概要: 新浪娱乐讯 10月10日，诺一的妈妈安娜在微博庆祝自己儿子迎来12周岁生日，“生日快乐小伙子！你12岁了，希望你能继续闪光跟跟你的快乐！爱你mon amour ”。　　据悉，刘烨带儿子诺一2015年参......
### [叮当健康：职场人健康生活必备神器](http://www.investorscn.com/2022/10/10/103406/)
> 概要: “叮当快药真得很快,” 大约20分钟之前蔡先生在叮当快药上买了胃药,此刻已经收到了。蔡先生介绍,作为一名上班族,平常有个头疼脑热的不太愿意去医院,被同事推荐了叮当快药后,发现了“网上购药”竟然如此方便......
### [长安汽车 1-9 月销量 168.07 万辆，新能源同比增加 117.85%](https://www.ithome.com/0/645/747.htm)
> 概要: IT之家10 月 10 日消息，长安汽车现发布公告，9 月共生产汽车 221913 辆同比增长 13.02%，今年累计生产汽车 1666711 辆，同比下降 2.09%；本月销售 215411 辆，同......
### [张嘉倪高速冲浪回怼被偷拍 称一没醉酒二没45岁](https://ent.sina.com.cn/s/m/2022-10-10/doc-imqmmthc0382861.shtml)
> 概要: 新浪娱乐讯 10月10日，有八卦媒体拍到张嘉倪与友人聚会，称其”深夜街边醉酒，但粉色头发颇具辣妹氛围感，丝毫看不出45岁 ”，随后张嘉倪高速冲浪回应“一没醉酒二不是45岁，但看在你说辣妹的份上原谅你......
### [TCL 华星与奔驰合作，推出全球首款横贯整个 A 柱曲面的车载显示屏](https://www.ithome.com/0/645/754.htm)
> 概要: IT之家10 月 10 日消息，今年 1 月份，梅赛德斯-奔驰 Vision EQXX 概念车公布，这款概念车搭载品牌首款完全无缝47.5 英寸超薄一体式屏幕，并引入游戏引擎和全面优化用户界面。研发团......
### [孙宇晨方面回应：担任火币全球顾问委员会成员，非买家](http://www.investorscn.com/2022/10/10/103407/)
> 概要: 10月10日，孙宇晨方面对“火币收购”传闻进行了回应。孙宇晨方面表示，孙宇晨和波场TRON均不是火币本次股份出售的收购方。未来，孙宇晨和波场TRON将一如既往地支持火币的发展。孙宇晨已经受邀担任新成立......
### [四季度是否有必要降准？](https://www.yicai.com/news/101557051.html)
> 概要: 针对10月存在的资金缺口，降准是较为理想的解决方式。形式上或为全面降准25bp+置换部分MLF。
### [2000亿缩水到200亿，这届VC不好蒙了](http://www.investorscn.com/2022/10/10/103411/)
> 概要: 来源 | 投资家（ID：touzijias）......
### [中国码农，35 岁后出口海外](https://www.ithome.com/0/645/775.htm)
> 概要: 据免费代码托管平台 GitHub 数据，截至 2021 年，中国有 755 万程序员，排名全球第二仅次于美国，约占全球 7300 万程序员的十分之一。他们菁英、多金、是中国新经济发展的象征。然而在荣耀......
### [经济三季报前瞻：GDP增速回升，基建投资维持高位](https://finance.sina.com.cn/jjxw/2022-10-10/doc-imqqsmrp2109343.shtml)
> 概要: 受超预期因素冲击，今年二季度前期经济明显下滑，我国及时出台33项稳经济一揽子政策和19项接续政策。随着政策落地见效，经济增长态势持续改善，三季度经济总体恢复回稳。
### [诺贝尔经济学奖授予美联储前主席伯南克等3位经济学家](https://www.huxiu.com/article/682104.html)
> 概要: 本文来自微信公众号：华尔街见闻（ID：wallstreetcn），作者：潘凌飞，头图来自：诺贝尔奖官网北京时间10月10日晚，诺贝尔经济学奖得主揭晓。美联储前主席本·伯南克（Ben Bernanke）......
### [海天大跌358亿，烦恼不只是“双标风波”](https://www.huxiu.com/article/682170.html)
> 概要: 出品｜虎嗅商业消费组作者｜苗正卿题图｜视觉中国一场“双标风波”感冒，正引发酱油一哥海天味业更深层的隐疾。10月10日，在国庆节后第一个A股开盘日，海天味业开盘大跌7.99%，截至收盘跌幅约9%，一天之......
### [上海嘉定：暂停开放嘉定区文化娱乐场所](https://finance.sina.com.cn/china/gncj/2022-10-10/doc-imqqsmrp2104698.shtml)
> 概要: 【上海嘉定：暂停开放嘉定区文化娱乐场所】财联社10月10日电，经上海市嘉定区疫情防控领导小组研究决定：2022年10月10日20时起至10月15日24时...
### [模拟游戏《派对策划师》将于10月底发售](https://www.3dmgame.com/news/202210/3853422.html)
> 概要: 专注于模拟游戏的发行商心跳游戏HBG，这一次携手Forestlight Games给玩家带来全新的模拟游戏《派对策划师（Party Maker）》。在游戏中，作为派对策划师的你，需要满足不同客户的需求......
### [光通信的 3 个波段新秀，还不知道吗？](https://www.ithome.com/0/645/782.htm)
> 概要: 5G 网络迅猛发展，网络数据传输需求呈指数增长，光网络作为底层的承载网络，其传输能力对 5G 网络发展至关重要。扩展光网络传输能力的一大法宝就是不停深挖光纤可用的波段资源，也就是不断扩展光网络的传输道......
### [被盗超1亿美金，谁才能做“区块链老三”？](https://www.huxiu.com/article/679891.html)
> 概要: 出品｜虎嗅科技组作者｜周舟头图｜视觉中国除了比特币和以太坊，人们还没能创造出第三条具有全球共识的区块链网络。尤其是在BSC、Solana等区块链“新贵”频繁出错的情况下。10月初，全球最大的加密货币交......
### [沪指失守3000点 市场加速赶底中？](https://www.yicai.com/video/101557303.html)
> 概要: 沪指失守3000点 市场加速赶底中？
### [诺德股份更名扩营，定位新材料战略聚焦国家“十四五”规划](http://www.investorscn.com/2022/10/10/103412/)
> 概要: 10月10日晚间,诺德股份(600110.SH)发布公告称,公司2022年第六次临时股东大会审议并通过将公司全称变更为“诺德新材料股份有限公司”,证券简称及证券代码保持不变......
### [打响货币保卫战！强美元下全球外储大缩水，人民币区间震荡](https://www.yicai.com/news/101557285.html)
> 概要: 美元指数迭创新高，越来越多国家干预汇市的力度骤然升级。
### [商务部回应美商务部升级半导体等领域对华出口管制并调整出口管制“未经验证清单”](https://finance.sina.com.cn/china/2022-10-10/doc-imqqsmrp2107480.shtml)
> 概要: 新华社北京10月10日电 商务部新闻发言人10日就美商务部升级半导体等领域对华出口管制并调整出口管制“未经验证清单”应询答记者问。
### [劳动力市场热度下降缓慢，美联储还有多少耐心？](https://www.yicai.com/news/101557324.html)
> 概要: 劳动力参与率复苏乏力或将成为推动就业供需平衡的重大障碍。
### [网传徐克版《神雕侠侣》2024-2025年开拍](https://www.3dmgame.com/news/202210/3853423.html)
> 概要: 网传徐克版《神雕侠侣》开机时间为2024-2025年，名单中还有《封神传奇2》和《追龙3》。徐克版《神雕侠侣》改编自金庸同名武侠小说，由徐克导演、施南生监制。徐克曾经表示，《神雕侠侣》是他看的第一本武......
### [南京第四批次集中土拍推出4幅地块，起拍总价66亿元](https://finance.sina.com.cn/jjxw/2022-10-10/doc-imqmmthc0410330.shtml)
> 概要: 10月10日，江苏省南京市发布国有建设用地使用权挂牌出让公告（集中供地第四批）（2022年宁出第08号），共推出4幅地块，用地面积约11公顷，起拍总价66亿元。
### [“开放证券”理念提出两年来，落地效果如何，还需如何推动？](https://www.yicai.com/news/101557346.html)
> 概要: 证券公司应如何推动“开放证券”的发展理念在实际业务场景中落地？
### [专家：流动性合理充裕 可对资产价格形成内在支撑](https://finance.sina.com.cn/roll/2022-10-10/doc-imqqsmrp2110200.shtml)
> 概要: 转自：上海证券报 上证报中国证券网讯（记者 梁银妍）10月10日，人民银行再度开展20亿元逆回购操作。“十一”假期后，人民银行已连续两天开展7天期“地量”逆回购操作。
### [越南正妹JiyunChoi福利图赏 性感曲线太惹火了！](https://www.3dmgame.com/bagua/5606.html)
> 概要: 今天为大家带来的是越南正妹Jiyun Choi的福利图。妹子臀围惊人，都快超越肩宽了。Jiyun Choi在推特(@jichoihere)上有近70万粉丝，Cosplay也有十多年了，其性感曲线实在太......
### [组图：鞠婧祎晒秋日拍摄花絮vlog 阳光下卖萌软糯可爱](http://slide.ent.sina.com.cn/star/w/slide_4_704_376299.html)
> 概要: 组图：鞠婧祎晒秋日拍摄花絮vlog 阳光下卖萌软糯可爱
### [流言板巴黎官方：梅西小腿不适，参与对本菲卡的比赛还是操之过急](https://bbs.hupu.com/55853968.html)
> 概要: 虎扑10月10日讯 就在刚刚，巴黎官方公布了欧冠对阵本菲卡的大名单，梅西继续缺阵。随后，巴黎官网也更新了队内最新的伤情通报。梅西感到小腿不适，对于明天参加对阵本菲卡的比赛还是操之过急。金彭贝继续在训练
### [流言板CBA常规赛：郭艾伦16分14篮板，辽宁揭幕战胜广厦](https://bbs.hupu.com/55853991.html)
> 概要: 虎扑10月10日讯 在刚刚结束的CBA揭幕战辽宁对阵广厦的比赛中，经过四节鏖战，辽宁队74-64战胜广厦队取得新赛季开门红。具体数据如下：辽宁队方面，凯尔-弗格24分11篮板，郭艾伦16分14篮板5助
### [流言板郭艾伦21投6中，得到16分14篮板5助攻2抢断1封盖](https://bbs.hupu.com/55854008.html)
> 概要: 虎扑10月10日讯 2022-23赛季CBA常规赛首轮，辽宁男篮74-64战胜广厦男篮。值得一提的是，此役郭艾伦拿到职业生涯篮板新高。本场比赛辽宁队员郭艾伦出场35分钟，投篮21中6，其中三分4中1，
### [流言板2022全球总决赛小组赛第四日首发名单：Yagao交手caPs](https://bbs.hupu.com/55854032.html)
> 概要: 虎扑10月10日讯 英雄联盟赛事官博公布2022全球总决赛小组赛第四日首发名单，原文如下：100：Ssumday、Closer、Abbedagge、FBI、HuhiCFO：Rest、Gemini、Mi
# 小说
### [猫引又见宋梦](http://book.zongheng.com/book/1208314.html)
> 作者：梦慕南

> 标签：奇幻玄幻

> 简介：忽有一天，主人公家中的一只猫被雷鸣之声惊吓得不见踪影。主人公为了找寻那只猫，被穿越至宋朝。身在宋朝，他们举步维艰。之后他们转战宋辽两国的同时，也经历了一次又一次的磨砺与创伤。通过他们的不断地摸索，揭开了种种神秘的面纱。通过自己不懈的努力，也学会用武器来捍卫自己。最终用他们的智慧和双手，如愿以偿穿越回现代。小说以一只猫神秘失踪为题材，以第三人称叙述，并结合北宋历史文化。穿越年间为北宋政和年间，因此也从侧面写照出宋微宗及北宋的人文艺术。全文一路贯穿着北宋诗词，以文人的思绪解读主人公的忧愁与喜悦，诠释着主人公的悲凉与欢快。作者希望文以载道，诗以言情，阐述主人公所经历的遭遇。

> 章节末：第四十章 归途

> 状态：完本
# 论文
### [GhostNets on Heterogeneous Devices via Cheap Operations | Papers With Code](https://paperswithcode.com/paper/ghostnets-on-heterogeneous-devices-via-cheap)
> 概要: Deploying convolutional neural networks (CNNs) on mobile devices is difficult due to the limited memory and computation resources. We aim to design efficient neural networks for heterogeneous devices including CPU and GPU, by exploiting the redundancy in feature maps, which has rarely been investigated in neural architecture design.
### [Data-Driven Modeling and Prediction of Non-Linearizable Dynamics via Spectral Submanifolds | Papers With Code](https://paperswithcode.com/paper/data-driven-modeling-and-prediction-of-non)
> 概要: We develop a methodology to construct low-dimensional predictive models from data sets representing essentially nonlinear (or non-linearizable) dynamical systems with a hyperbolic linear part that are subject to external forcing with finitely many frequencies. Our data-driven, sparse, nonlinear models are obtained as extended normal forms of the reduced dynamics on low-dimensional, attracting spectral submanifolds (SSMs) of the dynamical system.