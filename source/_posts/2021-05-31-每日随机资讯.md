---
title: 2021-05-31-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PoellatWasserfall_EN-CN2494247621_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-05-31 22:56:10
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PoellatWasserfall_EN-CN2494247621_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [WordPress 诞生 18 周年](https://www.oschina.net/news/143786/wordpress-18yr)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>WordPress 联合创始人 Matt Mullenweg发文庆祝WordPress 诞生 18 周年。看着这个占据着博客平台半壁江山......
### [开源运营与治理，是时候重点聊聊了！](https://www.oschina.net/news/143781/gotc-opensource-governance)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>开源近两年以更快的速度在发展，关于开源的运营与治理，也变得越来越受关注，公司如何制定开源策略、个人如何参与开源社区、开源的上下游关系如何......
### [GCC Rust 继续推进其上游计划](https://www.oschina.net/news/143782/gcc-rust-keep-on-upstream)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>根据邮件列表显示，GCC Rust的开发者正在建立一个单独的 GCC Git 分支和邮件列表，以继续推进GCC Rust的上游化。官方的......
### [阿里云携手 VMware 共建云原生 IoT 生态，聚开源社区合力打造领域标准](https://www.oschina.net/news/143894)
> 概要: 极客们，请收下2021 微软 x 英特尔黑客松大赛英雄帖！>>>5 月 29 日，“2021 阿里云开发者大会”于北京国际会议中心举办。在本次大会“云原生技术与最佳实践”论坛现场，阿里云容器服务负责人......
### [官方揭秘！你的颜色是这样算出来的……](https://segmentfault.com/a/1190000040093575?utm_source=sf-homepage)
> 概要: 作者：imyzf上周三，你的朋友圈是不是这样子的？与此同时，有网友开始分析起了本次活动的计算逻辑，甚至反编译出了所有可能的颜色结果。作为本次活动的核心开发人员，接下来将为大家介绍颜色测试活动的技术细节......
### [矢吹健太朗个人推特40万关注纪念绘图公开](http://acg.178.com/202105/416425784209.html)
> 概要: 日本漫画家矢吹健太朗于近日开通了个人推特，目前关注数已突破40万。40万关注纪念绘图随之公开，角色是来自于漫画「邪马台幻想记」的紫苑和壹与。「邪马台幻想记」是矢吹健太朗的处女作漫画，本作以战乱的古代日......
### [「战斗员派遣中！」第9话宣传绘公开](http://acg.178.com/202105/416425875977.html)
> 概要: 「战斗员派遣中！」原作轻小说插画师カカオ・ランタン和改编漫画作者鬼麻正明公开了本作TV动画第9话的宣传绘。カカオ・ランタン：鬼麻正明：TV动画「战斗员派遣中！」改编自晓枣创作的轻小说作品，由J.C.S......
### [「七骑士革命-英雄的继承者-」第二弹主视觉图公开](http://acg.178.com/202105/416426111113.html)
> 概要: TV动画「七骑士革命-英雄的继承者-」公开了第二弹主视觉图。TV动画「七骑士革命-英雄的继承者-」改编自Netmarble Japan发行的手机游戏「七骑士」，由LIDENFILMS和domerica......
### [组图：贝克汉姆夫妇街头约会 46岁小贝紧拉妻子手尽显好丈夫风采](http://slide.ent.sina.com.cn/star/slide_4_86512_357332.html)
> 概要: 组图：贝克汉姆夫妇街头约会 46岁小贝紧拉妻子手尽显好丈夫风采
### [「实物大自由高达立像揭幕式」华丽开幕！](https://acg.gamersky.com/news/202105/1392996.shtml)
> 概要: 在4月26日的安全祈愿式上成功完成机体头部安装后迅速成为上海又一网红“景点”，吸引了众多前往打卡的人群和无数高达粉。
### [《咒术回战》漫画最新话过于潦草 网友担心作者身体](https://acg.gamersky.com/news/202105/1392957.shtml)
> 概要: 《咒术回战》漫画最新一话公开后，其中几页的绘画相当的潦草对此网友们也是很担心漫画作者芥见下下的身体状况，纷纷发帖留言希望他可以好好休息。
### [鸿蒙如何走出青纱帐？](https://www.huxiu.com/article/431584.html)
> 概要: 本文来自微信公众号：曲高和众（ID：m15875），作者：孟庆祥，题图来自：视觉中国6月2日，鸿蒙2.0，也就是手机版就要发布了。大家最关心的问题自然是鸿蒙这个东西到底有没有戏啊？我就掰扯掰扯这个问题......
### [华为官方海报泄露：6月2日会有鸿蒙手机华为P50系列](https://www.3dmgame.com/news/202105/3815560.html)
> 概要: 此前有消息称，备受关注的华为P50系列将推迟到7月份发布。不过华为官方宣布将于6月2日举办鸿蒙操作系统及华为全场景新品发布会，正式发布覆盖手机等移动终端的鸿蒙操作系统。从华为官方近日发布的新品预热海报......
### [《咒术回战》漫画累计发行突破5000万部](https://acg.gamersky.com/news/202105/1393027.shtml)
> 概要: 官方在今天（5月31日）宣布《咒术回战》漫画累计发行突破5000万部，目前《咒术回战》漫画在日本“O榜”2021上半年漫画销量为第二位。
### [任何组织的成长，都是通过耗散来完成的](https://www.huxiu.com/article/431598.html)
> 概要: 本文来自微信公众号：华夏基石e洞察（ID：chnstonewx），作者：胡赛雄（华为公司蓝血十杰，曾任华为公司后备干部系主任、全球技术服务部干部部部长、华为管理名著《以奋斗者为本》核心编委会成员），头......
### [《鬼灭之刃》真人剧贰来了！曝定妆照：祢豆子可爱](https://acg.gamersky.com/news/202105/1393023.shtml)
> 概要: 《鬼灭之刃》真人舞台剧推出续篇，主视觉图和角色海报公布。
### [古川雄大主演七月新剧《女人们的战争》 扮高富帅](https://ent.sina.com.cn/s/j/2021-05-31/doc-ikmyaawc8537193.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 古川雄大首次接拍主演日剧《女人们的战争~单身汉杀人事件~》饰演高富帅，剧集将于七月在东京台开播，与葵若菜、特灵德尔玲奈、寺本莉绪等合作。　　这是一部围绕古川雄大饰演的各方......
### [漫画《咒术回战》累计突破5000万部](https://news.dmzj1.com/article/71056.html)
> 概要: 这次准备的是美少女游戏公司Xuse停业，《本田小狼与我》角色被指违反道交法，国外网友将手办进行二次元画风上色，漫画《进击的巨人》创下吉尼斯世界纪录，最后还是惯例的新作动画相关消息。
### [组图：刘畊宏晒全家福为爱妻庆生 甜蜜表白撒狗粮](http://slide.ent.sina.com.cn/star/w/slide_4_704_357343.html)
> 概要: 组图：刘畊宏晒全家福为爱妻庆生 甜蜜表白撒狗粮
### [USDC开发商Circle募得4.4亿美元](https://www.tuicool.com/articles/IV7BJfi)
> 概要: 暴走时评：上周五，Circle宣布已经从一群主要支持者处筹集了4.4亿美元。翻译：MayaUSDC(-0.06%)稳定币背后的加密货币原生金融服务公司已经完成了一轮大规模的融资。周五，Circle宣布......
### [「咒术回战」漫画第18、19卷特装版特典公开](http://acg.178.com/202105/416439909695.html)
> 概要: 「咒术回战」公开了漫画特装版第18卷和第19卷的特典并开启的预售，预售活动截止至8月6日。漫画第18卷特典为亚克力日历和32个角色吊饰，售价3850日元，预计于2021年12月25日面世；第19卷特典......
### [老派亿万富翁正在进军加密市场](https://www.tuicool.com/articles/2uuyMn3)
> 概要: 暴走时评：随着Carl Icahn和Ray Dalio的加入，有大量“老派”资金正涌入加密市场。曾经的加密批评者们似乎正在改变过去的批判态度，转而采取更为积极的态度。翻译：Maya由于加密货币越来越受......
### [这群人，才是中国最牛“90后”](https://www.huxiu.com/article/431634.html)
> 概要: 本文来自微信公众号：一条（ID：yitiaotv），自述：徐蓓，编辑：倪楚娇，原文标题：《这群中国最牛90后，仍想要每天为社会做一点好事情》，头图来自：《九零后》您目前设备暂不支持播放5月22日这一天......
### [视频：王嘉尔被女生赞“细心体贴” 曾在合照时问要不要开美颜](https://video.sina.com.cn/p/ent/2021-05-31/detail-ikmxzfmm5706042.d.html)
> 概要: 视频：王嘉尔被女生赞“细心体贴” 曾在合照时问要不要开美颜
### [英国零售商RTX 3080Ti价格曝光 超过1.3万人民币](https://www.3dmgame.com/news/202105/3815578.html)
> 概要: 英国零售商LambdaTek在商店页面曝光了非公版RTX 3080Ti定价。该零售商定价从2000美金起，约合人民币1.27万元；但是这和当前的共识不符：目前认为英伟达官方在RTX 3080Ti发售时......
### [GSC《海底两万里》娜汀亚粘土人开订](https://news.dmzj1.com/article/71063.html)
> 概要: 根据吾峠呼世晴原作漫画制作的舞台剧《鬼灭之刃》，公开了续篇《鬼灭之刃 其之贰 绊》的主宣图与角色定妆照。
### [《战狼2》《湄公河行动》等4部电影6月1日起重映](https://www.3dmgame.com/news/202105/3815581.html)
> 概要: 今日（5月31日），据猫眼电影专业版显示的信息，《战狼2》《红海行动》《湄公河行动》《智取威虎山》四部影片将开启重映。据猫眼电影专业版显示，《战狼2》和《湄公河行动》自6月1日起重映，《红海行动》和《......
### [贵圈真乱？！日本八卦杂志整理10大声优丑闻](https://news.dmzj1.com/article/71064.html)
> 概要: 为纪念TV动画播出30周年，GSC根据动画《海底两万里》制作的粘土人目前已经开订了。本作包括“普通颜”、“生气颜”、“困惑颜”三种表情，还有小狮王和罐头等配件。
### [Sri Lanka faces 'worst-ever beach pollution' from burning ship](https://www.dw.com/en/sri-lanka-faces-worst-ever-beach-pollution-from-burning-ship/a-57712086)
> 概要: Sri Lanka faces 'worst-ever beach pollution' from burning ship
### [皮克斯《夏日友情天》确认引进内地！](https://news.dmzj1.com/article/71065.html)
> 概要: 皮克斯年度力作《夏日友晴天》确认引进内地！
### [娱乐圈生三胎的明星家庭有哪些？](https://new.qq.com/omn/20210531/20210531A09UDX00.html)
> 概要: 为了应对国内人口老龄化，改善我国人口结构，关于二胎或三胎的讨论越来越多。近期三胎政策也是被各种讨论，来看看娱乐圈有哪些明星家庭生三胎。            张艺谋妻子陈婷直接转发相关微博，并表示“提......
### [产业链人士：恩智浦半导体也已同联华电子达成 6 年芯片代工协议](https://www.ithome.com/0/554/609.htm)
> 概要: 5 月 31 日消息，据国外媒体报道，芯片代工商联华电子 4 月份在官网宣布，他们将与全球范围内的多家客户携手，扩充 12A 厂产能，参与的客户将以议定价格预先支付订金，确保获得扩充后的长期产能。随后......
### [超540万人在线！《盗墓笔记秦岭神树》牵手斗鱼主播火热追番](https://new.qq.com/omn/ACF20210/ACF2021053100732700.html)
> 概要: 由企鹅影视、腾讯视频、南派泛娱联合出品的动画《盗墓笔记秦岭神树》目前正在腾讯视频独家热播中。最近更新的剧集可以说是一集高能过一集，跌宕起伏的剧情、悬疑诡秘的画面无不牵动着万千稻米的心。        ......
### [Yangtze dam separates Paddlefish, largest, from spawn, now extinct](https://www.nationalgeographic.com/animals/article/chinese-paddlefish-one-of-largest-fish-extinct?loggedin=true)
> 概要: Yangtze dam separates Paddlefish, largest, from spawn, now extinct
### [How the Commodore Amiga Powered Your Cable System in the ’90s](https://www.atlasobscura.com/articles/how-the-commodore-amiga-powered-your-cable-system-in-the-90s)
> 概要: How the Commodore Amiga Powered Your Cable System in the ’90s
### [GraphQL Conf. 2021](https://graphqlconf.org/)
> 概要: GraphQL Conf. 2021
### [京东物流将被纳入恒生综合指数，6 月 11 日起生效](https://www.ithome.com/0/554/648.htm)
> 概要: IT之家5 月 31 日消息 今日，恒生指数公司宣布，京东物流符合相关指数的快速纳入规则要求，将于 2021 年 6 月 10 日（星期四） 收市后被纳入以下指数，并于 2021 年 6 月 11 日......
### [组图：《创4》选手和马近照曝光 白发造型与狗狗互动笑容可爱](http://slide.ent.sina.com.cn/star/slide_4_704_357355.html)
> 概要: 组图：《创4》选手和马近照曝光 白发造型与狗狗互动笑容可爱
### [警惕，大量 ETC 车主遭短信诈骗，银行卡余额 10 秒钟被划走](https://www.ithome.com/0/554/649.htm)
> 概要: IT之家5 月 31 日消息 据央视财经等报道，四川巴中的冯先生手机收到一条短信，提醒他的 ETC 账户会在 4 月 30 日内失效。冯先生按照链接页面要求，输入了自己的姓名、身份证号码、银行卡号、密......
### [三孩政策来了！为何出台？如何提高生育意愿？专家详解背后逻辑](https://finance.sina.com.cn/jjxw/2021-05-31/doc-ikmyaawc8630763.shtml)
> 概要: 原标题：三孩政策来了！为何出台？如何提高生育意愿？专家详解背后逻辑 新京报贝壳财经讯（记者 潘亦纯）5月31日，中共中央政治局召开会议，聚焦人口问题，其中提到...
### [威马 W6 体验：SOA 提升驾驶乐趣，HAVP 解决停车难题](https://www.ithome.com/0/554/655.htm)
> 概要: 5 月 31 日晚间消息，近日，威马汽车组织了旗下最新车型威马 W6 的体验，其中，新浪科技重点体验了 SOA 自定义编程、无人泊车等功能。SOA 自定义编程可以被看成是威马汽车软件生态的入口，是基于......
### [比特币是藏在特斯拉内部的“炸弹”](https://finance.sina.com.cn/china/jrxw/2021-05-31/doc-ikmxzfmm5770494.shtml)
> 概要: 比特币是藏在特斯拉内部的“炸弹” 来源：财富中文网 事情的开始看起来就像是一场鲁莽的赌注，随着加密货币的走势逐渐疯狂，情况看起来也越来越离谱。
### [G7集团财长表示央行数字货币可以作为安全结算资产和支付系统的支柱](https://www.btc126.com//view/168795.html)
> 概要: 据金十消息，草案文件显示，G7集团财长们将就央行数字货币的共同原则展开讨论，并将于2021年晚些时候发布结论。草案文件还显示，G7集团财长表示央行数字货币可以作为流动的安全结算资产和支付系统的支柱......
### [深入浅出负载均衡](https://www.tuicool.com/articles/YvuuyuM)
> 概要: 作者：vivo互联网团队-Zhang Peng一、负载均衡简介1.1. 大型网站面临的挑战大型网站都要面对庞大的用户量，高并发，海量数据等挑战。为了提升系统整体的性能，可以采用垂直扩展和水平扩展两种方......
### [流言板新华社：任何赛事须服从国家防疫大局；善后工作要及时跟上](https://bbs.hupu.com/43281137.html)
> 概要: 虎扑05月31日讯 针对中国足协今日宣布世预赛四十强赛A组剩余比赛易地阿联酋迪拜举行的决定，新华社刊发题为“及时应变，打好剩余四十强赛”的时评文章，对产生的不利影响和后续进行了点评。文章认为，这无疑是
### [从城里人变成村里人！阿圭罗在甘伯城打卡拍照](https://bbs.hupu.com/43281179.html)
> 概要: 未来一切祝好吧！欢迎阿圭罗！欢迎红蓝色的玫瑰组合！
### [从变身个体户到享受工伤保险 地方探索灵活就业保护机制](https://finance.sina.com.cn/china/dfjj/2021-05-31/doc-ikmyaawc8633618.shtml)
> 概要: 原标题：从变身个体户到享受工伤保险，地方探索灵活就业保护机制 除了将劳动者注册为个体户，推动实施相应的社保这一模式之外，未来还应当思考如何创造更多的发展模式...
### [梁建章谈三孩政策：未来中国解决生育成本的力度会是全球最高](https://finance.sina.com.cn/china/2021-05-31/doc-ikmxzfmm5771737.shtml)
> 概要: 据新华社报道，中共中央政治局5月31日召开会议，听取“十四五”时期积极应对人口老龄化重大政策举措汇报，审议《关于优化生育政策促进人口长期均衡发展的决定》。
### [流言板伦纳德连续3场季后赛以至少75%有效命中率拿到25+，历史首人](https://bbs.hupu.com/43281293.html)
> 概要: 虎扑05月31日讯 快船今天在客场以106-81大胜独行侠，将系列赛比分扳平。此役，快船前锋科怀-伦纳德15中11，三分3中2，砍下29分10篮板的两双。据统计，这是伦纳德连续第三场季后赛以至少75%
### [广州护航高考：全市共54900名考生迎高考，统一核检，已新增备用隔离考场14个](https://finance.sina.com.cn/china/dfjj/2021-05-31/doc-ikmxzfmm5772954.shtml)
> 概要: 原标题：广州护航高考：全市共54900名考生迎高考，统一核检，已新增备用隔离考场14个 来源：南方财经全媒体集团 5月31日，据广州市政府新闻办疫情防控新闻发布会通报...
### [流言板埃里克森：感谢孔蒂带我们赢意甲冠军；他的离任令人震惊](https://bbs.hupu.com/43281355.html)
> 概要: 虎扑05月31日讯 接受Tv2 Spo采访时，国际米兰中场埃里克森谈到了自己与孔蒂的关系，以及意甲夺冠等的问题。埃里克森说道：“赢得意甲联赛冠军真的是一个很大的惊喜，因为当你赢得冠军的时候，接下来的时
### [沸腾之夜上百位艺人，主持人团划水，肖战邓紫棋等歌手演出亮眼](https://new.qq.com/omn/20210531/20210531A0F2S900.html)
> 概要: 沸腾之夜如期上演，本次的艺人阵容非常庞大，粗略估计达到了上百位，光署名的主持人团就有11人，其中不乏黄圣依、王鸥、严浩翔、贺峻霖、苏醒、汪东城等跨界艺人。因为是直播，非常考验艺人的现场发挥水平。从播出......
### [业内人士：《战地6》A测可能于7月进行](https://www.3dmgame.com/news/202105/3815602.html)
> 概要: EA已经宣布将于今年6月正式公布《战地6》，虽然网上已经有泄露的视频，但据业内人士Henderson在推文上表示，《战地6》将于今年7月进行A测。Henderson今日发推文表示，《战地6》A测可能在......
### [YFI核心开发者：Yearn v2协议总收入已超5000万美元](https://www.btc126.com//view/168797.html)
> 概要: yearn.finance （YFI）核心开发者banteg发推称，Yearn v2协议的总收入（gross revenues）刚刚超过5000万美元......
### [印度储备银行澄清没有禁止加密货币](https://www.btc126.com//view/168798.html)
> 概要: 据u.today消息，印度储备银行已澄清其对加密货币的立场，声称没有禁止为处理加密货币的账户提供银行服务。 它还补充说，2018年的指引已自最高法院判决之日起不再有效，因此HDFC等银行不能引用该指引......
### [肖战沸腾之夜全开麦唱错词，音响太渣全靠吼，迅速登上海外热搜](https://new.qq.com/omn/20210531/20210531A0F3E800.html)
> 概要: 5月31日晚的沸腾之夜，众星云集，场面火爆，肖战的出场无疑是晚会的一大燃点。就在前一晚上才刚刚结束青岛场《如梦之梦》的话剧表演，31日便匆匆专场来参加这样一个晚会直播，肖战的表现还是可圈可点的。   ......
### [间距不会调？掌握这1个点，让我不再迷茫](https://www.tuicool.com/articles/FB7JR3y)
> 概要: 教你调间距~文章转载自：我们的设计日记ID：sky编辑：zkey之前一直觉得色彩，图形，字体是UI设计的三大构成，任何设计都是从这三大元素开始的，所有的UI设计都是由这些构成的，但是却忽略了界面中空间......
### [《最终幻想7重制过渡版》原声碟宣传片公布](https://www.3dmgame.com/news/202105/3815603.html)
> 概要: 史克威尔日前为即将推出的《最终幻想7重制过渡版》（Final Fantasy VII Remake Intergrade）公布了新预告片，这次聚焦于将于6月份发行的原声碟。原声碟将包括三张CD，将于6......
### [他这新恋情一波三折](https://new.qq.com/omn/20210531/20210531A0F3OY00.html)
> 概要: 今天，王大陆被曝光和参加过青你2的蔡卓宜在外滩拥吻，让网友大吃一惊。两人曾一起参加恋爱真人秀《怦然再心动》，但王大陆是“观察员”，而蔡卓宜是参与相亲的女嘉宾。            王大陆1991年出......
### [经济学家：放开三孩要解决“生得起，养不起”问题](https://finance.sina.com.cn/china/gncj/2021-05-31/doc-ikmyaawc8634974.shtml)
> 概要: 经济学家：放开三孩要解决“生得起，养不起”问题 记者：王世博 5月31日，中共中央政治局召开会议，提出进一步优化生育政策，实施一对夫妻可以生育三个子女政策及配套支持措...
### [NBS理事会成功换届 新一届理事会将恪守去中心化治理理念](https://www.btc126.com//view/168799.html)
> 概要: 据NBS社区消息，NBS新一届理事会经过报名、验仓、竞选等环节，已全部到位，新老理事会成员权责和任务分工已正式交接。目前，新一届理事会已就外联协调、技术开发、宣传推广、市值维护等各项工作都进行了新的部......
# 小说
### [都市最后一个修真者](http://book.zongheng.com/book/1018047.html)
> 作者：天边那道人

> 标签：武侠仙侠

> 简介：低调做人高调做事，开着灵车漂过移，地府酒吧打过碟，阎王殿里蹦过迪，闯魔域灭妖帝收了玉帝当小弟。

> 章节末：第一百一十章 大结局最终章

> 状态：完本
# 论文
### [Improving Pretrained Models for Zero-shot Multi-label Text Classification through Reinforced Label Hierarchy Reasoning](https://paperswithcode.com/paper/improving-pretrained-models-for-zero-shot)
> 日期：4 Apr 2021

> 标签：MULTI-LABEL TEXT CLASSIFICATION

> 代码：https://github.com/layneins/Zero-shot-RLHR

> 描述：Exploiting label hierarchies has become a promising approach to tackling the zero-shot multi-label text classification (ZS-MTC) problem. Conventional methods aim to learn a matching model between text and labels, using a graph encoder to incorporate label hierarchies to obtain effective label representations \cite{rios2018few}.
### [A spin-glass model for the loss surfaces of generative adversarial networks](https://paperswithcode.com/paper/a-spin-glass-model-for-the-loss-surfaces-of)
> 日期：7 Jan 2021

> 标签：None

> 代码：https://github.com/npbaskerville/loss-surfaces-of-gans

> 描述：We present a novel mathematical model that seeks to capture the key design feature of generative adversarial networks (GANs). Our model consists of two interacting spin glasses, and we conduct an extensive theoretical analysis of the complexity of the model's critical points using techniques from Random Matrix Theory.
