---
title: 2020-10-12-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MountCetatea_EN-CN2318138498_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-10-12 20:56:34
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MountCetatea_EN-CN2318138498_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：张若昀自曝腰围只有65厘米 比王俊凯还细、和雷佳音头围差不多](https://video.sina.com.cn/p/ent/2020-10-12/detail-iivhuipp9150206.d.html)
> 概要: 视频：张若昀自曝腰围只有65厘米 比王俊凯还细、和雷佳音头围差不多
### [视频：易烊千玺称“流量标签不用撕掉 那不是我能控制的”](https://video.sina.com.cn/p/ent/2020-10-12/detail-iivhvpwz1511977.d.html)
> 概要: 视频：易烊千玺称“流量标签不用撕掉 那不是我能控制的”
### [视频：魅惑大片首公开！虞书欣穿一字肩“锁骨杀”好惊艳](https://video.sina.com.cn/p/ent/2020-10-12/detail-iivhuipp9239074.d.html)
> 概要: 视频：魅惑大片首公开！虞书欣穿一字肩“锁骨杀”好惊艳
### [迷弟上线！李易峰发文庆祝湖人夺冠](https://ent.sina.com.cn/s/m/2020-10-12/doc-iivhvpwz1539991.shtml)
> 概要: 新浪娱乐讯 10月12日，李易峰在微博发文庆祝湖人夺冠：“我们是冠军了！恭喜湖人，感谢詹姆斯，This is for Kobe！ ”并附上角度清奇的自拍照。(责编：小5)......
### [组图：BY2穿黑色西服外套亮相机场酷劲足 黑长直造型清纯可人](http://slide.ent.sina.com.cn/y/slide_4_704_346527.html)
> 概要: 组图：BY2穿黑色西服外套亮相机场酷劲足 黑长直造型清纯可人
### [林志颖为妈妈庆祝70岁大寿，宴请十几桌，祖孙三代齐聚其乐融融](https://new.qq.com/omn/20201012/20201012A0AH5G00.html)
> 概要: 10月12日，林志颖在社交平台上分享了为妈妈庆祝七十岁大寿的照片。没想到林妈妈的心愿是能完成公益梦想，帮助需要帮助的人，小志自然是全力支持，她还表示希望在妈妈80岁、90岁、100岁的时候，能继续为她......
### [影视圈中的神仙爱情，吴奇隆：为了诗诗，我可以把一切都给她！](https://new.qq.com/omn/20201012/20201012A0C4GZ00.html)
> 概要: 人生在世，一路走来难免磕磕绊绊，如果能遇到一个能和你一起走下去的人，相互扶持相互依偎，那该有多么幸运。很多人都特别羡慕偶尔看到的老年夫妻搀扶着对方，脸上一片深情。这种神仙般的爱情自然是每个人都想拥有的......
### [张丹峰洪欣婚姻状态成谜，毕滢回归与异性自驾游，手戴戒指开启新恋情](https://new.qq.com/omn/20201012/20201012A0CWHW00.html)
> 概要: 10月9日是洪欣49岁的生日，当天洪欣特意晒出庆生动态，不出所料老公张丹峰并未出镜，而且洪欣生日当天张丹峰一直迟迟未曾发布庆生动态，直到次日张丹峰才转发微博向妻子送上迟来的祝福，还特意称呼洪欣为“孩儿......
### [潘粤明携绯闻女友聚餐，与小10岁尹姝贻穿情侣装，形影不离显恩爱](https://new.qq.com/omn/20201012/20201012A0ASIH00.html)
> 概要: 近日，有媒体曝出潘粤明的近况，晒出他携绯闻女友尹姝贻与好友聚餐的视频，其感情状态再次引发关注。            当天，潘粤明与好友到某餐馆聚餐，许久之后走出来，同大家在门口一一告别。他身穿白色T......
### [周杰伦昆凌为刘畊宏补过生日，夫妻俩戴墨镜紧贴一起，比寿星抢镜](https://new.qq.com/omn/20201012/20201012A0BGL200.html)
> 概要: 10月12日下午，刘畊宏在个人社交平台分享了一组庆生照，还直言这次的生日惊喜是由好兄弟周杰伦以及昆凌安排的。虽然这次生日只是补过，但周董和老婆昆凌一点都不含糊，连生日蛋糕都选择现做，对好兄弟简直没话说......
# 动漫
### [网络科幻小说《第一序列》实体书首发，跨次元IP改编力被市场广泛看好](https://new.qq.com/omn/20201012/20201012A0E7VR00.html)
> 概要: 日前，阅文集团白金作家“会说话的肘子”创作完结的网络科幻小说《第一序列》亮相2020广州动漫游戏盛典，限量首发的《第一序列》（第一卷）实体书广受粉丝追捧，在展会上迅速售空，作品号召力可见一斑。《第一……
### [海贼王992话情报：路飞与基德觉醒新形态，凯多与大妈合体变身，强悍](https://new.qq.com/omn/20201012/20201012A07NQM00.html)
> 概要: 海贼王漫画已经停更两周时间了，尾田身体不舒服，突然宣布休刊两周，这也是很正常的，尾田工作时间非常长，每天只睡几个小时，日积月累的把身体累垮了。最新的剧情也有点崩了，海米都希望尾田能够休刊一段时间，把……
### [百年之后，到底谁能接替琪琪当破坏神悟空的老婆呢？](https://new.qq.com/omn/20201012/20201012A0CJBJ00.html)
> 概要: 悟空是龙珠的主角，也是Z战士的领导者。他有着举世无双的实力，不仅率先觉醒了超级赛亚人击败弗利萨，还在龙珠超中开启了神境界与比鲁斯大战。要知道，比鲁斯可是破坏之神，整个宇宙都归他管辖，而悟空居然能够和……
### [《灵笼》追番600万，豆瓣勉强回归8分，当初评分的后悔了吗？](https://new.qq.com/omn/20201012/20201012A0D30Q00.html)
> 概要: 国漫曾经一直被诟病3D画面的崩坏、肢体僵硬、五毛特效等，在经过动画制作软件的升级以及一些制作技术的结合和多元化，慢慢地这些毛病也随之改善和克服，当然这是指那些舍得投资和用心制作的动画，对于那些粗制滥……
### [人性漫画：财不外露，不然你会凉凉](https://new.qq.com/omn/20201012/20201012A06X3F00.html)
> 概要: ...
### [80后的你看看这些图片能勾起多少童年回忆](https://new.qq.com/omn/20201012/20201012A05VXC00.html)
> 概要: 猜猜箱子里是什么？玻璃瓶橘子汁，童年最爱上学，放学和好朋友结伴而行放学后迫不及待去追喜爱的动画片小伙伴之间的游戏80后的你贴过多少这种贴纸呢？80后的你可曾这样写过作业？
### [《星期一的丰满》：爱酱犯困送福利 排球妹子很兴奋](https://acg.gamersky.com/news/202010/1327937.shtml)
> 概要: 本周《星期一的丰满》来到校园里，爱酱因为没睡好的缘故，来到学校也是摇摇晃晃看不清前面。她直接装上了前面的同学，扶住同学的场面也是相当福利。
### [《西游记之再世妖王》首曝预告 黑化孙悟空震撼登场](https://acg.gamersky.com/news/202010/1327912.shtml)
> 概要: 国产动画电影《西游记之再世妖王》在今日（10月12日）公开了本作的首支正式预告片，黑化版大圣震撼登场，烟熏妆和“花臂”造型以及性格设定都透露出孙悟空浓郁的暗黑气质。
### [《宝可梦COCO》新视频及主题曲 别样的亲子故事](https://acg.gamersky.com/news/202010/1328042.shtml)
> 概要: 剧场版动画《宝可梦：COCO》公开了全新主题曲及全新视频，本片讲述了一个被宝可梦抚养长大的孩子“COCO”的故事。
# 财经
### [李克强：坚定不移扩大开放 维护产业链供应链稳定](https://finance.sina.com.cn/china/gncj/2020-10-12/doc-iivhvpwz1642852.shtml)
> 概要: 李克强主持召开经济形势部分地方政府主要负责人视频座谈会时强调 巩固经济稳定恢复增长态势 确保完成全年发展目标任务 央视网消息（新闻联播）：12日...
### [青岛最新发布:楼山后社区升级为中风险地区 其他地区风险等级不变](https://finance.sina.com.cn/china/dfjj/2020-10-12/doc-iivhvpwz1644957.shtml)
> 概要: 原标题：青岛最新发布！楼山后社区升级为中风险地区，其他地区风险等级不变 青岛市于10月12日晚组织召开青岛市疫情防控情况新闻发布会，截至12日17时30分...
### [李克强：扩大普惠金融覆盖面 承诺的都要兑现](https://finance.sina.com.cn/china/gncj/2020-10-12/doc-iivhuipp9263790.shtml)
> 概要: 央视网消息（新闻联播）：12日，中共中央政治局常委、国务院总理李克强主持召开经济形势部分地方政府主要负责人视频座谈会。
### [青岛市疾控中心：青岛确诊病例不是在社区当中发生 风险较小](https://finance.sina.com.cn/china/gncj/2020-10-12/doc-iivhvpwz1643573.shtml)
> 概要: 10月12日，青岛召开疫情防控情况新闻发布会。青岛市疾控中心副主任张华强在发布会上介绍：本次疫情当中，青岛的确诊病例不是在社区当中发生，而是在医院筛查中发生...
### [工信部拟收回426个电信网码号 使用单位有这些公司](https://finance.sina.com.cn/china/gncj/2020-10-12/doc-iivhvpwz1642139.shtml)
> 概要: 据工信部网站12日消息，近期，按照相关规定，工信部拟收回部分电信网码号资源，426个电信网码号资源拟收回。 以下为原文： 关于拟收回部分电信网码号资源情况的公示 近期...
### [习近平赴广东考察调研](https://finance.sina.com.cn/china/gncj/2020-10-12/doc-iiznctkc5161027.shtml)
> 概要: 【#习近平赴广东考察调研#】习近平总书记12日赴广东考察调研。当天下午，他首先考察了潮州市广济桥、广济楼、牌坊街，察看文物修复保护、非遗文化传承...
# 科技
### [Redux异步解决方案之Redux-Thunk原理及源码解析](https://segmentfault.com/a/1190000037437347)
> 概要: 前段时间，我们写了一篇Redux源码分析的文章，也分析了跟React连接的库React-Redux的源码实现。但是在Redux的生态中还有一个很重要的部分没有涉及到，那就是Redux的异步解决方案。本......
### [11-SpringBoot 工程中的异常处理方式](https://segmentfault.com/a/1190000037435700)
> 概要: 背景分析在项目的开发中，不管是对底层的数据逻辑操作过程，还是业务逻辑的处理过程，还是控制逻辑的处理过程，都不可避免会遇到各种可预知的、不可预知的异常。处理好异常对系统有很好的保护作用，同时会大大提高用......
### [从创始人手中偷走了特斯拉公司？马斯克回击](https://www.ithome.com/0/513/187.htm)
> 概要: 美国当地时间周日，特斯拉首席执行官埃隆 · 马斯克 (Elon Musk)驳斥了有关他从创始人马丁 · 埃伯哈德 (Martin Eberhard)手中 “偷走”这家汽车制造商的指控。马斯克声称，如果......
### [国 AA 级照度，华雄 LED 护眼台灯 19 元再发车（减 110 元）](https://lapin.ithome.com/html/digi/513217.htm)
> 概要: 国AA级照度，华雄旗舰店LED护眼台灯报价129元，下单立减110元，限时限量50元券，实付19元包邮，领券并购买。赠运费险。华雄老品牌护眼台灯，3档/5档可调节灯光，RGO豁免级减蓝光，AA级照明护......
### [微信又一波改版，视频号风口真的来了](https://www.huxiu.com/article/386885.html)
> 概要: 本文来自微信公众号：运营研究社（ID：U_quan），原标题《微信又来一波改版，这次也太香了……》，作者：丰之余、蛋蛋，设计：瓜瓜，题图来自：视觉中国昨天，我突然发现我的微信朋友圈可以加“话题”了！在......
### [虚构的万亿咖啡市场](https://www.huxiu.com/article/386840.html)
> 概要: 咖啡豆消耗增长就是咖啡市场增长吗？本文来自微信公众号：赤潮AKASHIO（ID：AKASHIO），作者：评论尸、仿生1，头图来自：视觉中国2017 年 10 月，瑞幸成立，凭借着“消费升级”与“消费降......
### [无人驾驶之后：百度还剩几个冬天？](https://www.tuicool.com/articles/va22Mjv)
> 概要: 作者 | 霍可尼编辑 | 颜宇出品 | 真心编辑部百度将迎来自己最重要的历史转折。两年前，李彦宏与一汽集团董事长徐留平肩并肩的站在百度世界大会上，两人都穿着白衬衣、梳起背头，CP感十足。双方所代表的百......
### [教育界新宠「选课师」，能成为一门长期生意吗？](https://www.tuicool.com/articles/YjEnYvv)
> 概要: 作者|王玮  来源|黑板洞察（ID:heibandongcha）2020年，教育市场突然涌现出一个名为“选课师”的职业，在教育界掀起了一阵不小的热潮。从淘宝平台上搜索有关选课师的店铺，发现有相当一部分......
# 小说
### [日月神主](http://book.zongheng.com/book/841490.html)
> 作者：文殊太一

> 标签：奇幻玄幻

> 简介：流浪可以么？却被逼迫修炼。修炼可以么？却被追杀逃亡。逃亡可以么？却被网来干活。干活可以么？却引来高手围猎。一个人就是这样，被迫一步步往前走，直到笑傲九天……——————————————————日月神主书友群：177784541

> 章节末：三百一十八章 自由之神（全本完）

> 状态：完本
# 游戏
### [肯德基新人造肉汉堡开售：媲美真实肉感 售价32元起](https://www.3dmgame.com/news/202010/3799385.html)
> 概要: 今日（10月12日），肯德基官方微博宣布，植世代系列新品全新上线，包括植世代牛肉芝士汉堡、植世代黄金鸡块以及植世代三件套套餐，率先在北京、上海、广州、深圳、杭州、武汉六大城市推出。据官方介绍，植世代新......
### [登陆次世代！《胡闹厨房全都好吃》中文预告公开](https://www.3dmgame.com/news/202010/3799367.html)
> 概要: 今日（10月12日），香港GSE官方正式公布《胡闹厨房全都好吃》中文预告片，本作为《胡闹厨房1》+《胡闹厨房2》全部内容的高清重制与整合升级，预计将于2020年发售，以下为官方透露的游戏介绍细节。《胡......
### [TV动画《记录的地平线圆桌崩坏》特别寄语PV公开](https://www.3dmgame.com/news/202010/3799378.html)
> 概要: 根据官方公开的新消息，TV动画《记录的地平线》第三季「圆桌崩坏」特别寄语PV已经公开（简体中文版），将于2021年1月13日中日同步放送，以下为官方公开的相关细节。特别寄语PV：故事简介：一天，数万名......
### [国内第四大通信运营商中国广电成立 5G192号段将至](https://www.3dmgame.com/news/202010/3799384.html)
> 概要: 继中国移动、中国联通、中国电信三大通信运营商之后，10月12日今天，中国广电网络股份有限公司正式在京成立，成为国内第四大运营商，注册资本高达1012亿元。早在2019年6月6日，工信部正式发布4张5G......
### [电影《花束般的恋爱》新预告 押井守出演本人角色](https://www.3dmgame.com/news/202010/3799345.html)
> 概要: 由有村架纯、菅田将晖主演的电影新作《花束般的恋爱》即将于2021年1月上映，日前官方公开最新特报预告，同时宣布导演押井守确定出演本人角色，一起来期待下。·《花束般的恋爱》是坂元裕二原创剧本，土井裕泰执......
# 论文
### [Long-term Human Motion Prediction with Scene Context](https://paperswithcode.com/paper/long-term-human-motion-prediction-with-scene)
> 日期：7 Jul 2020

> 标签：HUMAN MOTION PREDICTION

> 代码：https://github.com/ZheC/GTA-IM-Dataset

> 描述：Human movement is goal-directed and influenced by the spatial layout of the objects in the scene. To plan future human motion, it is crucial to perceive the environment -- imagine how hard it is to navigate a new room with lights off.
### [Salvaging Federated Learning by Local Adaptation](https://paperswithcode.com/paper/salvaging-federated-learning-by-local)
> 日期：12 Feb 2020

> 标签：MULTI-TASK LEARNING

> 代码：https://github.com/ebagdasa/federated_adaptation

> 描述：Federated learning (FL) is a heavily promoted approach for training ML models on sensitive data, e.g., text typed by users on their smartphones. FL is expressly designed for training on data that are unbalanced and non-iid across the participants.
