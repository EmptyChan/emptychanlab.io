---
title: 2022-10-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.FrankensteinFriday_ZH-CN5814917673_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2022-10-28 17:52:21
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.FrankensteinFriday_ZH-CN5814917673_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [以途家、木鸟、美团三家国内头部民宿预订平台为例，谈谈民宿预订的产品思维](https://www.woshipm.com/pmd/5658394.html)
> 概要: 相较酒店，民宿的价格更低，环境更多样，对平台和用户都有更强的吸引力。在旺季出行，预订民宿是最重要的步骤之一。本文作者以途家、木鸟、美团三家国内头部民宿预订平台为例，谈谈民宿预订的产品思维，希望对你有帮......
### [在Airbnb工作7年，我学到了创业的7个教训](https://www.woshipm.com/chuangye/5659314.html)
> 概要: 越来越多的人开始创业，然而创业总是充满了艰辛。本文作者结合自己在Airbnb公司的工作经历，向我们介绍了这家公司多年来一直处于良好发展状态的密码，并总结出7条关于创业的教训，帮助新创业公司更快成长与发......
### [华为IPD流程体系：集成产品开发框架](https://www.woshipm.com/operate/5654766.html)
> 概要: IPD，即集成产品开发，来源于美国PRTM公司的PACE理论，如今已经成为一套包含企业产品开发的思想、模式、工具的系统工程。本文作者以华为为例，对IPD集成产品开发体系进行了分析，一起来看一下吧。企业......
### [上个财季净利润6600万美元 东方甄选“盘活”新东方](https://finance.sina.com.cn/tech/it/2022-10-28/doc-imqmmthc2365691.shtml)
> 概要: 作者： 吕倩......
### [麒麟操作系统 (kylinos) 从入门到精通 - 影音娱乐- 第三十九篇 视频剪辑](https://segmentfault.com/a/1190000042704159)
> 概要: 0.基础环境类别：笔记本型号：中国长城 UF712硬件平台：飞腾处理器（ArmV8 指令集）系统：银河麒麟操作系统 V10 SP1(2203)关键词：信创;麒麟系统;linux;PKS;银河麒麟;飞腾......
### [一文了解 NextJS 并对性能优化做出最佳实践](https://segmentfault.com/a/1190000042707818)
> 概要: 引言从本文中，我将从是什么，为什么，怎么做来为大家阐述 NextJS 以及如何优化 NextJS 应用体验。一、NextJS 是什么NextJS是一款基于 React 进行 web 应用开发的框架，它......
### [深入理解函数式编程（下）](https://segmentfault.com/a/1190000042707889)
> 概要: 函数式编程是一种历史悠久的编程范式。作为演算法，它的历史可以追溯到现代计算机诞生之前的λ演算，本文希望带大家快速了解函数式编程的历史、基础技术、重要特性和实践法则。在内容层面，主要使用JavaScri......
### [国产替代是一定要走的路](https://www.huxiu.com/article/694657.html)
> 概要: 作者｜陈伊凡出品｜虎嗅科技组头图｜视觉中国当灰犀牛来临的时候，如何寻找新的增长机会是企业家们必须回答的命题，尤其是在不确定性日益常态的时代。近日，美国商务部工业安全局（BIS）发布了最新管制新规，靶向......
### [高管解读](https://finance.sina.com.cn/tech/internet/2022-10-28/doc-imqmmthc2379487.shtml)
> 概要: 高管解读
### [高管解读](https://finance.sina.com.cn/tech/it/2022-10-28/doc-imqqsmrp4030276.shtml)
> 概要: 新浪科技讯 北京时间10月28日早间消息，苹果公司今天发布了该公司的2022财年第四财季财报。苹果公司第四财季大中华区营收为154.70亿美元，与上年同期的145.63亿美元相比增长6%。 欧洲部门营......
### [36氪首发丨「宅猫找房」获数千万元A轮融资，万科子公司万物云投资](https://36kr.com/p/1975818725188228)
> 概要: 文｜陈彦彤、周有辉编辑｜彭孝秋36氪获悉，房产交易“轻中介”平台「宅猫找房」已完成数千万元A轮融资，本轮融资由万物云投资。万物云为万科集团分拆后的子公司，9月30日于港股上市，在物业股中市值仅次华润万......
### [马斯克已掌控Twitter：CEO和CFO均已离开](https://finance.sina.com.cn/tech/internet/2022-10-28/doc-imqmmthc2386317.shtml)
> 概要: 新浪科技讯 北京时间10月28日早间消息，据报道，知情人士透露，特斯拉CEO埃隆·马斯克（Elon Musk）目前已经掌控Twitter，该公司CEO帕拉格·阿格拉瓦尔（Parag Agrawal）和......
### [PS5日本销量达到200万台 速度比PS4和PS3慢一点](https://www.3dmgame.com/news/202210/3854776.html)
> 概要: PS5日本销量达到200万台，不过这是索尼达到这一重要里程碑最慢的现代主机。据Famitsu统计的销量数据，截止到2022年10月23日，PS5在日本销量为202万7783台，包括174.9万光驱版P......
### [Steam或很快支持局域网下载游戏](https://www.3dmgame.com/news/202210/3854781.html)
> 概要: V社似乎正在努力让Steam游戏文件在多个电脑之间的传输更容易。SteamDB创始人Pavel Djundik在Steam API中发现了几行代码，暗示Steam将支持LAN（局域网）下载。这个功能将......
### [《刺客信条：英灵殿》新里程碑 玩家数突破2000万](https://www.3dmgame.com/news/202210/3854782.html)
> 概要: 育碧官方宣布，自2020年11月推出以来，《刺客信条：英灵殿》玩家人数已超过2000万，这个数字并不是销售数据，它包含了免费周末、Ubisoft+和PS Plus高级会员的玩家，此外，《刺客信条：英灵......
### [海王生物：产业链布局持续完善 Q3实现营业收入108.34亿元](http://www.investorscn.com/2022/10/28/103800/)
> 概要: 医药流通行业作为连接上游医药制造企业和下游医疗机构、零售终端的重要环节，在整个医药产业链中扮演者承上启下的重要角色。随着药品“两票制”政策的全面推行，以及“带量采购”进入提速扩面的新阶段，原有的药品流......
### [沸寂X白无常C4D 虚拟服饰](https://www.zcool.com.cn/work/ZNjI1ODU4MzI=.html)
> 概要: 沸寂X白无常C4D 虚拟服饰虚拟服饰已上线沸寂app可以vr穿衣服了~白无常工作室出品MD服饰设计锦波、高子、港文、伊宁、昊杰......
### [江小白酒厂“江记酒庄”完成10亿元新融资，白酒仍是好生意？｜36氪独家](https://36kr.com/p/1976797927795335)
> 概要: 双十一前，白酒行业热钱涌入。36氪未来消费获悉，江小白旗下酒厂“江记酒庄”已完成总金额10亿元的新一轮融资。此次江记酒庄获得了“国家队”加持，投资方为重庆江津区国资委控股的华信资产。根据工商信息显示，......
### [我国全面放开国际航线？明星基金经理年内亏损超50%？真相来了](https://www.yicai.com/news/101576479.html)
> 概要: 洞悉事件原委，多方一手求证，还原事实真相
### [从三季报，看探路者(300005.SZ)底部确立与转折预期](http://www.investorscn.com/2022/10/28/103804/)
> 概要: 10月26日盘后，探路者发布三季度业绩公告。2022年前三季度，公司共实现营收7.48亿元，同比增长13.69%，归母净利润为1134万元，低于去年同期。透过业绩数据表象，该如何看待探路者后续发展？笔......
### [P站美图推荐——湿发特辑（二）](https://news.dmzj.com/article/76011.html)
> 概要: 随机播放到了《濡れた髪のLonely》，就随便找一期湿发图包看看啦……
### [2023年继续！漫画《街角魔族》宣布暂停连载](https://news.dmzj.com/article/76012.html)
> 概要: 伊藤出云创作的漫画《街角魔族》在本日（28日）发售的《MANGA TIME KIRARA Carat》12月号（芳文社）上宣布了暂停连载的消息。连载预计将于2023年恢复。
### [在家啃老，年轻人不工作的退路](https://www.huxiu.com/article/697892.html)
> 概要: 本文来自微信公众号：真实故事计划 （ID：zhenshigushi1），作者：肖恬，编辑：孙雅兰，头图来自：《突如其来的假期》剧照蹲在家里大学刚毕业，何悦悦就从一名天之骄子，变成一个闲散人士。大四下学......
### [国产操作系统迁移，统信软件有何“解题思路”？](https://36kr.com/p/1976912862505604)
> 概要: 36氪获悉，近期统信软件举办了首届技术开放日。会议中，来自统信软件研发部门负责人、行业专家等角色，一起讨论了国产操作系统迁移落地的进展与未来走向。统信软件的各个负责人也针对当前国产操作系统迁移的解决方......
### [卡普空：《生化危机8》DLC及黄金版现已上线](https://www.3dmgame.com/news/202210/3854804.html)
> 概要: 今日（10月28日），卡普空官方宣布追加内容“温特斯家的传续”以及包含本篇与追加内容的《生化危机8：黄金版》现已登上PlayStation 5、PlayStation 4、Xbox Series X|......
### [高息股—美国疯狂加息之投资出路](http://www.investorscn.com/2022/10/28/103807/)
> 概要: 为应对居高不下的通膨问题，美国联储局今年已经升息5次，累计加息幅度达3厘，市场预期美国通膨仍未明显控制，不排除11月及12月分别升息0.75厘和0.5厘。美国持续加息，导致资金成本大涨，未来股债投资的......
### [今年双11头部主播深度合作国产美妆，四季度业绩有望向好](https://www.yicai.com/news/101576607.html)
> 概要: 根据弗若斯特沙利文数据，2021年中国功效型护肤市场的规模为567亿元，增速达33.6%，且预计将保持30%以上的增长。
### [理性回归与多元化落地：自动驾驶公司无惧「过冬」](http://www.investorscn.com/2022/10/28/103808/)
> 概要: 理性回归与多元化落地：自动驾驶公司无惧「过冬」
### [JRs之声吹杨应激障碍？西蒙斯空切至篮下，面对空篮上出三不沾](https://bbs.hupu.com/56147285.html)
> 概要: 欢迎阅读今日的JRs之声！JRs之声是从外国各大论坛的NBA板块中，选取今日热门讨论话题，并翻译外国网友们的神评论，整理成文分享给国内球迷们。Highlight：Ben Simmons drives
### [《狂怒的暴食 ～只有我突破了等级这概念～》动画化](https://news.dmzj.com/article/76014.html)
> 概要: 由一色一凛创作的小说《狂怒的暴食 ～只有我突破了等级这概念～》宣布了TV动画化决定的消息。有关动画的更多消息，还将于日后公开。
### [新消费的“天”，已经变了？](https://www.huxiu.com/article/698084.html)
> 概要: 本文来自微信公众号：小马宋（ID：xiaomasong999），作者：小马宋，原文标题：《闲话，新消费品牌 | 小马宋》，题图来自：视觉中国两年前，我熟悉的一位新消费领域的投资人，基本上就把自己投过的......
### [AI 绘画逆着玩火了，敢不敢发自拍看 AI 如何用文字形容你？](https://www.ithome.com/0/649/555.htm)
> 概要: 笑不活了家人们，最近突然流行起一个新玩法：给 AI 发自拍，看 AI 如何描述你。比如这位勇敢晒出自拍的纽约大学助理教授，他的笑容在 AI 看来居然是“兽人式微笑”。AI 还吐槽他胡子没有打理，但他表......
### [科技赋能，海尔消费金融构建全流程数字化布局](http://www.investorscn.com/2022/10/28/103810/)
> 概要: 《中国数字经济发展报告(2022年)》显示，2021年我国数字经济规模达到45.5万亿元，占GDP比重达到39.8%，数据成为驱动发展的核心要素。金融行业的全面数字化实现了企业的精准营销、智能风控、智......
### [荣耀畅玩 40 Plus 今日开售：6000mAh 大电池，1199 元起](https://www.ithome.com/0/649/559.htm)
> 概要: IT之家10 月 28 日消息，荣耀畅玩 40 Plus 于 10 月 20 日发布，并于今日正式开售，8GB+128GB 版本售价 1399 元，8GB+256GB 版本售价 1599 元。IT之家......
### [现实题材网剧：寻求“边缘”与“普遍”的平衡](https://ent.sina.com.cn/v/m/2022-10-28/doc-imqmmthc2436798.shtml)
> 概要: 胡祥　　自诞生以来，网剧的一大特性便是追求超脱现实的爽感，近年来的网剧却在现实主义的开掘上越来越深入。比如等作品，从不同侧面实现对现实题材的突破，取得了不错的口碑。近期两部网剧《你安全吗？》在深化现实......
### [《麓山之歌》侯勇从“军旅”转战“国企”](https://ent.sina.com.cn/v/m/2022-10-28/doc-imqqsmrp4085862.shtml)
> 概要: 重工题材电视剧正在湖南卫视播出，侯勇在剧中饰演有谋略、有担当的国企“当家人”方锐舟。这是侯勇第一次饰演国企领导，大量的专业台词给表演带来挑战，但他表示“越是难啃的骨头，越要去啃一啃”。在电视剧拍摄过程......
### [动画《Lycoris Recoil》推出官方外传漫画](https://news.dmzj.com/article/76015.html)
> 概要: TV动画《Lycoris Recoil》的官方外传漫画《Lycoris Recoil Recollect》于本日（28日）开始在ComicWalker上连载。
### [双十一，上海家化静悄悄](https://www.huxiu.com/article/698168.html)
> 概要: 出品｜虎嗅商业消费组作者｜昭晰编辑｜苗正卿题图｜视觉中国双十一，上海家化静悄悄。双十一预售开始一小时后（注：今年双十一开启时间为10月24日20:00），有国货护肤品已经卖出了100万件护肤品，而上海......
### [LG 电子三季度营业利润同比增长 25.1%，销售额超 21 万亿韩元创单季新高](https://www.ithome.com/0/649/587.htm)
> 概要: IT之家10 月 28 日消息，LG 电子 10 月 28 日发布最新的业绩报告，最终核实今年第三季营业利润同比增加 25.1%，为 7466 亿韩元（约合人民币 38 亿元）。销售额同比增加 14......
### [三星 SDI 三季度营收 37.69 亿美元，营收净利润均同比增长超过 50%](https://www.ithome.com/0/649/588.htm)
> 概要: 10 月 28 日消息，据国外媒体报道，在全球经济下滑担忧的影响下，电子消费品需求下滑，也影响到了三星电子、LG 电子等厂商的业绩，导致他们的利润大幅下滑。虽然三星电子和 LG 电子的状况不乐观，但在......
### [海外new things | 美国求职初创平台「Handoff」提出“共享工作”概念，为单亲家长提供全职工作的机会](https://36kr.com/p/1970328304044934)
> 概要: 据外媒TechCrunch报道，美国求职招聘平台「Handoff」在TechCrunch Disrupt Battlefield 200展会上提出了“共享工作”的概念，即两个人分担一项全职岗位的工作内......
### [《火焰纹章 结合》新角色克洛伊 对话战斗演示公布](https://www.3dmgame.com/news/202210/3854823.html)
> 概要: 《火焰之纹章：结合》官方公布了角色“克洛伊”（クロエ）的介绍视频，该角色由早见沙织担任配音，她是菲莱涅王国的王城骑士，性格温文尔雅，非常喜欢公主塞莉奴。克洛伊非常喜欢童话故事和美丽的景色，并一直在寻找......
### [福建省上市公司协会发倡议书：发挥企业优势 践行社会责任](https://finance.sina.com.cn/china/dfjj/2022-10-28/doc-imqqsmrp4099871.shtml)
> 概要: 原标题：福建省上市公司协会发倡议书：发挥企业优势 践行社会责任 e公司讯，10月28日，福建省上市公司协会发倡议书表示，各单位要紧跟疫情防控形势，结合公司自身实际...
### [国家外汇局：2022年以来我国涉外经济活动保持活跃 跨境资金流动总体平稳有序](https://finance.sina.com.cn/china/gncj/2022-10-28/doc-imqqsmrp4100376.shtml)
> 概要: 原标题：国家外汇管理局公布2022年9月银行结售汇和银行代客涉外收付款数据 国家外汇管理局统计数据显示，2022年9月，银行结汇15416亿元人民币，售汇14840亿元人民币。
### [国家外汇管理局：2022年9月中国外汇市场总计成交23.42万亿元人民币](https://finance.sina.com.cn/china/2022-10-28/doc-imqqsmrp4100673.shtml)
> 概要: 原标题：国家外汇管理局公布2022年9月中国外汇市场交易概况数据 国家外汇管理局统计数据显示，2022年9月，中国外汇市场（不含外币对市场，下同）总计成交23...
### [俄方：放弃美元和欧元对俄已不可逆转](https://www.yicai.com/news/101576920.html)
> 概要: 俄罗斯外贸银行行长表示，当前俄罗斯外贸结构迅速调整，不支持西方制裁的国家约占俄罗斯2021年外贸总额的45%，这一比例将继续提高。
### [沪指退守2900点，资金加速撤离机构重仓股丨复盘论](https://www.yicai.com/video/101576902.html)
> 概要: 沪指退守2900点，资金加速撤离机构重仓股丨复盘论
### [【G-战术】独行侠篮网复盘：一场关乎于单防，换防和夹击的顶级博弈](https://bbs.hupu.com/56149495.html)
> 概要: 新赛季到来，又到了小鸡为各位球迷朋友们做比赛复盘的时间啦。本篇文章我们将带来今日的焦点战役篮网vs独行侠的较量。在连续多期文章聚焦于球队个人项的解析和战术普及之后，由于本场比赛打得极为精彩，且在赛场的
### [深圳发布住房项目租赁参考价，南山区154元/平/月，租金最高](https://finance.sina.com.cn/china/gncj/2022-10-28/doc-imqmmthc2454683.shtml)
> 概要: 10月28日，深圳市房地产和城市建设发展研究中心发布关于《深圳市2022年度房屋租赁参考价格》的通知，市民可通过租赁参考价格查询平台查询具体的房源价格。
### [深圳发布住房项目租赁参考价，南山区154元/平/月，租金最高](https://www.yicai.com/news/101576908.html)
> 概要: 租金参考价不涉及银行贷款、加杠杆的问题，所以效果不能和二手房参考价划等号。
### [中国新能源城市商业地理｜天津：老牌汽车工业城等待转机](https://finance.sina.com.cn/china/dfjj/2022-10-28/doc-imqqsmrp4104031.shtml)
> 概要: 原标题：中国新能源城市商业地理⑬|天津：老牌汽车工业城等待转机 21世纪经济报道记者 左茂轩 报道 2021年11月，21世纪新汽车研究院成立之初便重磅推出了《2021中国新能源汽...
### [视频：张天爱黑白色系生日写真曝光 碎发遮面眼神撩人](https://video.sina.com.cn/p/ent/2022-10-28/detail-imqmmthc2454313.d.html)
> 概要: 视频：张天爱黑白色系生日写真曝光 碎发遮面眼神撩人
### [流言板刘维伟：队伍凝聚力很强，利用窗口期继续提高](https://bbs.hupu.com/56149746.html)
> 概要: 虎扑10月28日讯 2022-23赛季CBA常规赛第8轮，青岛男篮80-79险胜广厦男篮。赛后发布会，青岛主帅刘维伟总结比赛。刘维伟说道：“很高兴把这场比赛赢下来。这一阶段比赛我们有三场非常遗憾的输球
### [流言板队报：巴黎将与金彭贝谈续约，谈判可能会在世界杯后进行](https://bbs.hupu.com/56149755.html)
> 概要: 虎扑10月28日讯 据法国媒体《队报》的报道，巴黎圣日耳曼和队内的本土中后卫金彭贝之间的续约谈判现在已经恢复，而续约可能会在今年的卡塔尔世界杯后完成。目前，这位法国国脚与巴黎的合同将于2024年到期，
# 小说
### [道化天劫](http://book.zongheng.com/book/1179054.html)
> 作者：森林小哥

> 标签：武侠仙侠

> 简介：五界离心，天地大乱,消失一千年的妖王重现于世。阴谋迭出，战乱频仍。道祖为了化解这场劫难，派九天玄女打造战神。九天玄女化血成胎，一个男孩历经诸多凶劫，终于横空出世。保天护地，斩妖除魔……演绎出一个充满传奇与魔幻的时代。谁是大自然的缔造者？谁是自然规律的破坏者？谁又是这场劫难的作俑者？一段最为壮丽的历史，将在此书中呈现！

> 章节末：第三百九十八章  转世重生（结局）

> 状态：完本
# 论文
### [EDA: Explicit Text-Decoupling and Dense Alignment for 3D Visual and Language Learning | Papers With Code](https://paperswithcode.com/paper/eda-explicit-text-decoupling-and-dense)
> 日期：29 Sep 2022

> 标签：None

> 代码：https://github.com/yanmin-wu/eda

> 描述：3D visual grounding aims to find the objects within point clouds mentioned by free-form natural language descriptions with rich semantic components. However, existing methods either extract the sentence-level features coupling all words, or focus more on object names, which would lose the word-level information or neglect other attributes. To alleviate this issue, we present EDA that Explicitly Decouples the textual attributes in a sentence and conducts Dense Alignment between such fine-grained language and point cloud objects. Specifically, we first propose a text decoupling module to produce textual features for every semantic component. Then, we design two losses to supervise the dense matching between two modalities: the textual position alignment and object semantic alignment. On top of that, we further introduce two new visual grounding tasks, locating objects without object names and locating auxiliary objects referenced in the descriptions, both of which can thoroughly evaluate the model's dense alignment capacity. Through experiments, we achieve state-of-the-art performance on two widely-adopted visual grounding datasets , ScanRefer and SR3D/NR3D, and obtain absolute leadership on our two newly-proposed tasks. The code will be available at https://github.com/yanmin-wu/EDA.
### [ML-PersRef: A Machine Learning-based Personalized Multimodal Fusion Approach for Referencing Outside Objects From a Moving Vehicle | Papers With Code](https://paperswithcode.com/paper/ml-persref-a-machine-learning-based)
> 概要: Over the past decades, the addition of hundreds of sensors to modern vehicles has led to an exponential increase in their capabilities. This allows for novel approaches to interaction with the vehicle that go beyond traditional touch-based and voice command approaches, such as emotion recognition, head rotation, eye gaze, and pointing gestures.
