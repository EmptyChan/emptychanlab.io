---
title: 2023-06-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SmokyFireflies_ZH-CN3840923626_1920x1080.webp&qlt=50
date: 2023-06-15 22:18:26
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SmokyFireflies_ZH-CN3840923626_1920x1080.webp&qlt=50)
# 新闻
### [谷歌面临分拆风险 欧盟指控其滥用广告技术实力](https://finance.sina.com.cn/stock/usstock/c/2023-06-15/doc-imyxhuqv0069871.shtml)
> 概要: 欧盟对谷歌发起反垄断指控，指称谷歌滥用其在广告技术上的优势地位压制竞争。起诉书直击这家美国公司创收模式的核心，并可能迫使谷歌剥离这块利润丰厚的业务......
### [人工智能对高科技营销的影响](https://www.woshipm.com/ai/5847355.html)
> 概要: 人工智能（AI）是指能够模拟人类智能的计算机系统或软件。AI在各个领域都有广泛的应用，其中之一就是高科技营销。高科技营销是指利用高科技产品或服务来吸引和满足消费者的需求和偏好的营销活动。本文旨在探讨A......
### [美团外卖进军香港，UI竟然改头换面了！](https://www.woshipm.com/pd/5848100.html)
> 概要: 前段时间，美团旗下的外卖平台KeeTa正式在香港上线的消息引发了网友关注，那么，在香港上线的KeeTa，其UI设计有哪些特别之处？本文作者便尝试将KeeTa与其他外卖平台的设计进行了对比分析，一起来看......
### [疯狂小杨哥，下一个辛巴？](https://www.woshipm.com/it/5848346.html)
> 概要: 抖音流量数据极好的“疯狂小杨哥”以其发疯似的直播方式吸引粉丝，与之前的“辛巴”有许多相似之处，本篇文章作者试图在多方面比较“小杨哥”和“辛巴”，分析他们的发展历程和未来。一起来看看吧。“疯狂小杨哥”（......
### [Twitter被控无视版权非法使用音乐：需赔偿2.5亿美元](https://finance.sina.com.cn/stock/usstock/c/2023-06-15/doc-imyximnm2989625.shtml)
> 概要: Twitter被控无视版权非法使用音乐：需赔偿2.5亿美元
### [专注城市服务大模型，众数信科为教育、政务、养老等应用提供AIGC能力 | 早期项目](https://36kr.com/p/2300169943280384)
> 概要: 专注城市服务大模型，众数信科为教育、政务、养老等应用提供AIGC能力 | 早期项目-36氪
### [36氪首发 ｜「世瞳微电子」完成数千万Pre-A轮融资，专注SPAD dToF成像芯片](https://36kr.com/p/2301388660403209)
> 概要: 36氪首发 ｜「世瞳微电子」完成数千万Pre-A轮融资，专注SPAD dToF成像芯片-36氪
### [小S自曝会在具俊晔大S家哭：有时候被气到](https://ent.sina.com.cn/s/h/2023-06-15/doc-imyximnn9777343.shtml)
> 概要: 新浪娱乐讯 近日，小S在节目上自曝经常会在具俊晔大S家哭到不行，因为有时候被气到，又要忍住，就会在孩子不在的时候，到具俊晔大S家哭。　　具俊晔在一旁表示认同，并称小S都是在孩子不在的时候才哭，孩子在家......
### [网易有道近5年亏损45亿，智能硬件能扭转财务困局？](https://finance.sina.com.cn/tech/internet/2023-06-15/doc-imyxiruk9651255.shtml)
> 概要: 网易有道近5年亏损45亿，智能硬件能扭转财务困局？
### [ARO-梦之国度主题系列游乐园 | 原创ip | 品牌ip活动](https://www.zcool.com.cn/work/ZNjU1Njk1Njg=.html)
> 概要: 欢迎来到梦之国度主题游乐园！这里是一个充满魔法和奇幻的世界，让您沉浸在梦幻的氛围中。游客可以在这里遇见各种神奇的角色和生物，如巫师、精灵、独角兽等。园内还有美食街、咖啡馆、商店等设施，供游客购买美食、......
### [宝可梦卡牌强化包“极巨攻防”即将发售](https://acg.gamersky.com/news/202306/1607608.shtml)
> 概要: 宝可梦卡牌简中版【剑&盾】系列的首款强化包——【极巨攻防】来啦，2023年6月28日将会和广大训练家见面！
### [瓜分球王梅西](https://www.huxiu.com/article/1682735.html)
> 概要: 本文来自微信公众号：中国企业家杂志 （ID：iceo-com-cn），作者：胡楠楠，编辑：姚赟‍‍，题图来源：视觉中国他们都在等待梅西。北京亮马河畔的四季酒店西门外，长达百余米的街道两旁人行步道上，挤......
### [《吞食天地归来》二周年庆典福利放送 夏语遥联动合作](https://shouyou.3dmgame.com/news/67160.html)
> 概要: 上海悠游网宣布《吞食天地归来》将在6月15日迎来上线二周年，为了感谢玩家这两年来的鼎力支持，官方推出了一系列精彩活动，包括首次推出联动武将「夏语遥」、夏日沙滩趣网页活动、全新服务器「和声服」以及诸多福......
### [圆桌对话：如何做好产业投资？丨Waves新浪潮大会](https://36kr.com/p/2302379267255300)
> 概要: 圆桌对话：如何做好产业投资？丨Waves新浪潮大会-36氪
### [欧洲议会通过《AI法案》草案 需公布版权数据清单](https://www.3dmgame.com/news/202306/3871398.html)
> 概要: 如今AI爆火，各种问题层出不穷，亟需权力机关出台相关法规约束，根据欧洲议会的公告，在本周三对《人工智能法案》的投票中，议员们以499票赞成、28票反对和93票弃权的结果，确定了议会的协商立场。按照立法......
### [6月23日来北京游戏同人展看看 不去必后悔！](https://acg.gamersky.com/news/202306/1607699.shtml)
> 概要: 端午假期即将来临，天气炎热去外地人多，不如考虑一下不出京旅游。
### [超写实数字人创作—林依](https://www.zcool.com.cn/work/ZNjU1NzIzMTY=.html)
> 概要: 林依是我们去年设计创作的超写实数字人凭借其细腻生动的表情引发热点目前发行了单曲音乐专辑《元梦》，首条视频500W+播放量，获得广泛00后年轻群体的喜爱......
### [贾跃亭未履行2.4亿罚款被限消](http://www.investorscn.com/2023/06/15/108223/)
> 概要: 【#贾跃亭未履行2.4亿罚款被限消# #证监会申请限消贾跃亭#】......
### [组图：李俊昊林允儿出席新剧制作发布会 挽手比心CP感满满](http://slide.ent.sina.com.cn/star/slide_4_704_386404.html)
> 概要: 组图：李俊昊林允儿出席新剧制作发布会 挽手比心CP感满满
### [运通智能融资2.2亿元，曾为北京大兴机场提供“刷掌”服务](https://36kr.com/p/2302417475906305)
> 概要: 运通智能融资2.2亿元，曾为北京大兴机场提供“刷掌”服务-36氪
### [用ChatGPT插件自动写书爆火，还有更多躺着赚钱玩法](https://www.huxiu.com/article/1683438.html)
> 概要: 本文来自微信公众号：量子位 （ID：QbitAI），作者：梦晨，头图来自：unsplashChatGPT插件数量大爆发！总数已达390个，与刚开放时的74个相比，增长超过400%。而且有网友指出，其中......
### [拒绝“套路”提供省心消费体验，京东618全面升级以旧换新服务](http://www.investorscn.com/2023/06/15/108224/)
> 概要: 美国著名社会心理学家马斯诺的需求层次理论告诉我们，消费者低级的需求主要由物质来满足，高级的需求则需由精神方面来满足，当低层次的需求得到满足后，高层次的精神需求也就随之产生。这一理论同样适用于正火热进行......
### [快35岁的制造业打工者，在长三角能干些什么活？](https://www.huxiu.com/article/1680981.html)
> 概要: 本文来自微信公众号：南都观察家（ID：naradainsights），作者：苏怡杰，题图来源：视觉中国2023年，我居住在长三角示范区苏州吴江一个被本地居民称之为“深乡下”的远郊乡镇。吴江是中国制造业......
### [权游衍生剧《龙之家族》第2季将有更“传统”的叙事](https://www.3dmgame.com/news/202306/3871414.html)
> 概要: 《权力的游戏》衍生剧《龙之家族》第二季即将回归，节目主创 Ryan Condal 透露了更多关于第二季的内容。Condal 在接受Deadline 采访时表示，第一季奠定了故事的基础，现在第二季将融入......
### [《星空》游戏尚未发售 却已在M站收到玩家差评](https://www.3dmgame.com/news/202306/3871420.html)
> 概要: 距离《星空》正式发售还有三个月的时间，但现在在评分网站Metacritic上，它已经收到了一个玩家差评，一位名叫“Joppsta360”的用户给《星空》打出了0分评价。据悉，这位名叫“Joppsta3......
### [与君同行品见东方，2023“联盟杯”系列赛传奇开场](http://www.investorscn.com/2023/06/15/108234/)
> 概要: 与君同行品见东方，2023“联盟杯”系列赛传奇开场
### [西门子宣布加大在中国投资：11 亿元扩建成都工厂，成立深圳数字科技公司](https://www.ithome.com/0/700/061.htm)
> 概要: 感谢IT之家网友西窗旧事、航空先生的线索投递！IT之家6 月 15 日消息，在昨日举行的 2023 西门子数字经济论坛上，西门子董事会主席、总裁兼 CEO 博乐仁宣布将加大在华投入，并公布了一系列具体......
### [4个万亿产业集群、新一轮三大产业方案，上海制造业未来怎么干](https://www.yicai.com/news/101784218.html)
> 概要: 行动计划实施6大行动、22项重点任务
### [上市价 2599 元：小米巨省电空调 1.5 匹 1719 元 618 限时购](https://www.ithome.com/0/700/075.htm)
> 概要: 小米米家空调巨省电 1.5 匹 KFR-35GW / N1A1 发布于 2022 年 9 月 30 日，上市定价为 2599 元，首发价为 2199 元。京东 618 大促期间，预售价为 1999 元......
### [苹果科幻美剧《羊毛战记》确认续订第二季](https://www.3dmgame.com/news/202306/3871430.html)
> 概要: 苹果目前续订了科幻剧集《羊毛战记》第二季。该剧由 Graham Yost 主创，改编自作家 Hugh Howey 的《羊毛战记》小说。原作的英文名为《Wool（羊毛）》，而美剧版本英文名则是《Silo......
### [多只基金产品提前结束募集，市场见底信号渐明](https://www.yicai.com/news/101784305.html)
> 概要: “提前结募可以在低位抢到更好价格”
### [【解局】戚继光舰访问菲律宾，释放出哪些信号？](https://finance.sina.com.cn/jjxw/2023-06-15/doc-imyxknym3209284.shtml)
> 概要: 【相关报道】 中国海军戚继光舰抵达菲律宾进行友好访问 【环球时报-环球网报道记者郭媛丹】14日，中国海军戚继光舰按计划抵达菲律宾首都马尼拉，开始为期三天的访问。
### [信息量很大！商务部这场发布会回应了商务领域热点问题(实录全文)](https://finance.sina.com.cn/china/2023-06-15/doc-imyxknyi6444526.shtml)
> 概要: 商务部6月15日召开例行新闻发布会 商务部于2023年6月15日（星期四）15时召开例行新闻发布会。商务部办公厅副主任、新闻发言人束珏婷出席，介绍相关情况并回答媒体提问。
### [189.9 元破冰新低：绿联 100W 智充魔盒 Pro 充电器 618 发车](https://www.ithome.com/0/700/112.htm)
> 概要: 绿联去年 5 月发布了 100W 智充魔盒 Pro，其配备了3 个 AC 强电接口和4 个 USB 接口（3C+1A）。其中 USB 接口方面支持 100W 快充，同时内置了氮化镓芯片，售价 299 ......
### [商务部：中美应推动双边经贸关系健康稳定发展](https://finance.sina.com.cn/china/2023-06-15/doc-imyxknym3237649.shtml)
> 概要: 中新社北京6月15日电 （记者 李晓喻）中国商务部新闻发言人束珏婷15日在北京称，中美应本着相互尊重、和平共处、合作共赢的原则，努力推动双边经贸关系健康稳定发展。
### [万亿光伏产业，卷起来不要命](https://www.huxiu.com/article/1684107.html)
> 概要: 出品｜虎嗅科技组作者｜包校千编辑｜陈伊凡题图｜视觉中国登上世界之巅10年之久的中国光伏，本已难逢敌手，殊不知，一场酝酿多时的危机正在降临。如果给眼下的光伏市场定调，那就是扩产和过剩、火热与内卷并存。6......
### [国企回应招聘应届毕业生有80后；钟薛高21家分支机构均已注销；最挤地铁线拟上市？广州地铁：传闻不实](https://www.yicai.com/news/101784388.html)
> 概要: 国企回应招聘应届毕业生有80后；钟薛高21家分支机构均已注销；最挤地铁线拟上市？广州地铁：传闻不实
### [工信部等五部门：开展2023年新能源汽车下乡活动，参与活动车型共69款](https://finance.sina.com.cn/china/2023-06-15/doc-imyxknym3248488.shtml)
> 概要: 来源：工信部 参与活动车型共有69款。 6月15日，据工信部网站消息，为促进农村地区新能源汽车推广应用，引导农村居民绿色出行，助力美丽乡村建设和乡村振兴战略实施...
### [组图：刘诗诗线下活动生图曝光 着黑白长裙露锁骨肩颈线优越](http://slide.ent.sina.com.cn/star/w/slide_4_704_386418.html)
> 概要: 组图：刘诗诗线下活动生图曝光 着黑白长裙露锁骨肩颈线优越
### [WeLab Bank汇立银行推出创新主题基金服务模式，整合专业分析送上热门投资灵感](http://www.investorscn.com/2023/06/15/108240/)
> 概要: 近日，香港最创新银行WeLabBank汇立银行宣布进一步拓展其财富管理业务，推出全新的自选基金服务。该行去年进军财富管理业务，积极为热衷数字科技的用户提供个性化的智慧投资体验。该行以智能理财顾问GoW......
### [买车想省心又省钱，快去华南首家京东汽车超体中心感受沉浸式体验](http://www.investorscn.com/2023/06/15/108241/)
> 概要: 华南首家京东汽车超级体验中心来啦！6月17日，大湾区首家京东MALL将在东莞正式开业，位于其一层的京东汽车超级体验中心，届时也将与消费者见面。开业当天，京东汽车不但会带来特斯拉1年免费体验权，超级试驾......
### [五部门：开展 2023 年新能源汽车下乡活动，“线下 + 云上”相结合](https://www.ithome.com/0/700/121.htm)
> 概要: 感谢IT之家网友西窗旧事的线索投递！IT之家6 月 15 日消息，工业和信息化部、发展改革委、商务部、农业农村部、国家能源局组织开展 2023 年新能源汽车下乡活动，活动时间为 2023 年 6 月-......
### [低学历无法入编，高学历不会下县：公卫人才就业“尴尬”如何解](https://www.yicai.com/news/101784422.html)
> 概要: 供需结构性矛盾导致今年毕业的公卫大学生面临严峻就业压力。
### [欧洲议会表决支持乌克兰加入北约](https://www.yicai.com/news/101784445.html)
> 概要: 欧洲议会以425票赞成、38票反对、42票弃权通过一项决议，敦促在下个月举行的北约峰会上邀请乌克兰加入北约。
### [知乎讲起“鬼故事”](https://finance.sina.com.cn/china/2023-06-15/doc-imyxkthi3139765.shtml)
> 概要: 来源：光子星球 在中文互联网语境里，内容社区一直是前不着村后不着店的生意。前进道路是商业化，内容社区向商业平台进军的道路往往困难重重，后退则是“小而美”...
# 小说
### [开局修为全靠赌](https://book.zongheng.com/book/1038679.html)
> 作者：常州府

> 标签：武侠仙侠

> 简介：废柴韩长命的法宝只有一个功能：赌！灵根废柴？练级龟速？筑基无望？那就靠赌吧，没有什么是赌一把不能解决的事情。这是一本沙雕小说，如果写得不够沙雕，你可以来打我！

> 章节末：第320章 大结局

> 状态：完本
# 论文
### [XCI-Sketch: Extraction of Color Information from Images for Generation of Colored Outlines and Sketches | Papers With Code](https://paperswithcode.com/paper/xci-sketch-extraction-of-color-information)
> 日期：26 Aug 2021

> 标签：None

> 代码：https://github.com/Sampai28/GeneratedSketches

> 描述：Sketches are a medium to convey a visual scene from an individual's creative perspective. The addition of color substantially enhances the overall expressivity of a sketch.
### [Photo-Realistic Out-of-domain GAN inversion via Invertibility Decomposition | Papers With Code](https://paperswithcode.com/paper/photo-realistic-out-of-domain-gan-inversion)
> 日期：19 Dec 2022

> 标签：None

> 代码：https://github.com/AbnerVictor/OOD-GAN-inversion

> 描述：The fidelity of Generative Adversarial Networks (GAN) inversion is impeded by Out-Of-Domain (OOD) areas (e.g., background, accessories) in the image. Detecting the OOD areas beyond the generation ability of the pretrained model and blending these regions with the input image can enhance fidelity. The ``invertibility mask" figures out these OOD areas, and existing methods predict the mask with the reconstruction error. However, the estimated mask is usually inaccurate due to the influence of the reconstruction error in the In-Domain (ID) area. In this paper, we propose a novel framework that enhances the fidelity of human face inversion by designing a new module to decompose the input images to ID and OOD partitions with invertibility masks. Unlike previous works, our invertibility detector is simultaneously learned with a spatial alignment module. We iteratively align the generated features to the input geometry and reduce the reconstruction error in the ID regions. Thus, the OOD areas are more distinguishable and can be precisely predicted. Then, we improve the fidelity of our results by blending the OOD areas from the input image with the ID GAN inversion results. Our method produces photo-realistic results for real-world human face image inversion and manipulation. Extensive experiments demonstrate our method's superiority over existing methods in the quality of GAN inversion and attribute manipulation.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
