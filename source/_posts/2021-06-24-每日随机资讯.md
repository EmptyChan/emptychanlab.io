---
title: 2021-06-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DenaliCaribou_ZH-CN9804350098_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-06-24 21:27:19
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DenaliCaribou_ZH-CN9804350098_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [Canonical 为 Blender LTS 提供付费支持](https://www.oschina.net/news/147463/blender-support-from-canonical)
> 概要: 开源软件供应链点亮计划，等你来！>>>Ubuntu 开发商 Canonical宣布与 Blender 基金会达成合作，双方将共同为 Blender 的长期支持版本 (LTS) 提供付费的企业级支持。据......
### [杀毒软件 McAfee 创始人 John McAfee 自杀](https://www.oschina.net/news/147467/john-mcafee-dead)
> 概要: 开源软件供应链点亮计划，等你来！>>>路透社和美联社证实，杀毒软件 McAfee 创始人 John McAfee 周三被发现死于西班牙巴塞罗那的监狱。此前不久，西班牙国家法院批准将他引渡到美国，接受逃......
### [IntelliJ IDEA 2021.2 EAP 4 发布](https://www.oschina.net/news/147453/intellij-idea-2021-2-eap4-released)
> 概要: 开源软件供应链点亮计划，等你来！>>>IntelliJ IDEA 2021.2 EAP 4 现已发布。此版本带来了一些功能更新和 bug 修复，包括新的 project-wide analysis、以......
### [Linux 5.14 将支持 OpenPOWER Microwatt Soft CPU 内核](https://www.oschina.net/news/147454/linux-5-14-to-support-microwatt)
> 概要: 开源软件供应链点亮计划，等你来！>>>根据最新的提交显示，即将发布的 Linux 5.14 将增加对Microwatt的支持。Microwatt 是一个用 VHDL 2008 编写的基于 FPGA 的......
### [Show HN: iPod.js – An online iPod that connects to Spotify and Apple Music](https://tannerv.com/ipod)
> 概要: Show HN: iPod.js – An online iPod that connects to Spotify and Apple Music
### [如何平衡兴趣与收入 —— 听尤雨溪访谈有感](https://segmentfault.com/a/1190000040230988?utm_source=sf-homepage)
> 概要: README是一款Github推出的访谈节目，每期会采访一位开源大佬，挖掘开源项目背后的故事。采访尤雨溪的这期标题是如何从一个想法发展成整个JS社区生态。主要讲述了尤雨溪的成长历程，Vue的诞生过程以......
### [网络安全先驱 John McAfee 狱中死亡，享年75岁](https://segmentfault.com/a/1190000040231930?utm_source=sf-homepage)
> 概要: 据西班牙报纸 El Pais 报道，网络安全先驱 John McAfee 于周三在西班牙的监狱中死亡，享年 75 岁。 McAfee 的律师称，John McAfee 在九个月的监禁中因绝望而自杀。目......
### [Lego Island Rebuilder](https://www.legoisland.org/wiki/index.php/LEGO_Island_Rebuilder)
> 概要: Lego Island Rebuilder
### [来华淘金40年，外企发现了一个新的中国](https://www.huxiu.com/article/436745.html)
> 概要: 来源 |远川研究所（ID：caijingyanjiu）作者 | 余佩颖/于可心题图 | 视觉中国诞生了雅戈尔、杉杉的宁波，还出过一家做服装代工做到上市的申洲国际。这家公司最初并非宁波首富马建荣家族的私......
### [人人都踩的海底捞，仍是一家吊打同业的好公司](https://www.huxiu.com/article/436746.html)
> 概要: 本文来自微信公众号：爱思考的柚子（ID：gh_c899544adc0a），作者：彭程柚子投资合伙人，题图来自：视觉中国海底捞又又又被喷上了头条，我们从商业逻辑角度分析为啥海底捞仍然是中式里最好的一家公......
### [「神奇女侠：黑与金」第四期变体封面公开](http://acg.178.com/202106/418502090290.html)
> 概要: 近日，DC漫画公开了神奇女侠的个人单刊「神奇女侠：黑与金」（Wonder Woman Black&Gold）的第四期变体封面图，本期封面采用的是古希腊陶画的美术绘制风格，封面展示的是神奇女侠与达克赛德......
### [「游戏王 怪兽之决斗」拉的翼神龙手办开订](http://acg.178.com/202106/418502428404.html)
> 概要: 寿屋作品「游戏王 怪兽之决斗」拉的翼神龙手办开订，高约500mm，主体采用ABS、PVC材料制造。该手办定价为21980日元（去税），约合人民币1281元，预计于2021年12月发售......
### [「Fate/Grand Order 神圣圆桌领域卡美洛」后篇部分原画摄公开](http://acg.178.com/202106/418503450600.html)
> 概要: 由TYPE-MOON开发的同名手游改编，Production I.G制作的剧场版动画「Fate/Grand Order 神圣圆桌领域卡美洛」公开了后篇的部分原画摄。该剧场版已于2021年5月8日上映......
### [我给“蜜雪冰城”留了个电话，结果引来了半个奶茶界](https://www.huxiu.com/article/436787.html)
> 概要: 本文来自微信公众号：盒饭财经（ID：daxiongfan），作者：谭丽平，头图来自：视觉中国蜜雪冰城需要建立一个打假部门了。在销量和流量的加持下，火爆全网的蜜雪冰城，正在遭受反噬。打开蜜雪冰城的官网，......
### [动画电影「欢乐好声音2」发布角色海报](http://acg.178.com/202106/418505583631.html)
> 概要: 由环球影业出品动画电影「欢乐好声音2」发布了角色海报，影片将于12月22日在北美上映。马修·麦康纳、瑞希·威瑟斯彭、斯嘉丽·约翰逊、塔伦·埃格顿、托里·凯莉、尼克·克罗尔将回归为影片配音，还有多名演员......
### [廉价电话手表短路自燃 四岁女童手背被烧伤](https://www.3dmgame.com/news/202106/3817463.html)
> 概要: 据荔枝新闻报道，近日“电话手表自燃，四岁女童手背被烧伤”一事，引发了社会关注。福建泉州四岁女童依依(化名)佩戴的电话手表突然发生自燃，孩子的手背皮肤被严重烧伤。经医生诊断，依依的手背属于三度烧伤，目前......
### [网曝网飞《变形金刚》王国篇预览视频 猛兽侠登场](https://acg.gamersky.com/news/202106/1400248.shtml)
> 概要: 现在有网友曝出了网飞动画《变形金刚：塞伯坦之战三部曲》“王国”动画的预览，猛兽侠们登场了。
### [体能师和赵睿的训练日常：如何提升侧向移动技巧，请看赵睿的动作示范](https://bbs.hupu.com/43832842.html)
> 概要: 体能师和赵睿的训练日常：如何提升侧向移动技巧，请看赵睿的动作示范
### [杨幂和父亲成立新公司 父女二人共同持股](https://ent.sina.com.cn/s/m/2021-06-24/doc-ikqciyzk1540306.shtml)
> 概要: 新浪娱乐讯 天眼查App显示，6月23日，海南慈爵文化传播合伙企业（有限合伙）成立，执行事务合伙人为杨晓林（杨幂父亲），经营范围含广播电视节目制作经营；演出经纪；文艺创作；文化娱乐经纪人服务等。合伙人......
### [周四福利囧图云飞系列 都是大洋马凭啥你骑我呢？](https://www.3dmgame.com/bagua/4661.html)
> 概要: 转眼又到周四了，一起来看看小编准备的福利囧图。都是大洋马，凭啥你骑我呢？这个臀部技巧也太厉害了，这不叫抛媚眼，这简直是高压电！成熟的果实挂在树上，十分诱人。眼神不错啊羡慕的眼神加油白俄罗斯美女Sash......
### [Show HN: Freeciv-Web](https://www.freecivweb.org/?2021)
> 概要: Show HN: Freeciv-Web
### [组图：Angelababy穿抹胸繁花纱裙秀肩线 氛围静谧美貌明艳](http://slide.ent.sina.com.cn/star/w/slide_4_86512_358462.html)
> 概要: 组图：Angelababy穿抹胸繁花纱裙秀肩线 氛围静谧美貌明艳
### [女生回应被曹云金强行拽上车 解释是恋人日常打闹](https://ent.sina.com.cn/s/m/2021-06-24/doc-ikqciyzk1569887.shtml)
> 概要: (责编：小5)......
### [《海贼王》1017话情报：甚平VS福兹弗 奇妙的因缘](https://acg.gamersky.com/news/202106/1400351.shtml)
> 概要: 《海贼王》漫画1017话情报公开，小玉给所有吃了糯米团子的给赋者们发布命令，让他们帮助路飞这边，给赋者们开始纷纷倒戈。
### [组图：《中国医生》曝女性角色剧照 袁泉周也等亮相](http://slide.ent.sina.com.cn/film/slide_4_704_358464.html)
> 概要: 组图：《中国医生》曝女性角色剧照 袁泉周也等亮相
### [王兴以后怎么管10万人？](https://www.huxiu.com/article/436833.html)
> 概要: 本文来自微信公众号：商隐社（ID：shangyinshecj），作者：齐马、灵竹，头图来自：视觉中国2011年3月，小有名气的青年企业家王兴从“千团大战”的紧张和激烈中抽身，来到了春意渐浓的杭州，前往......
### [感恩陪伴，《一人之下》粉丝集结共赴动画五年之约！](https://new.qq.com/omn/ACF20210/ACF2021062400612400.html)
> 概要: 哪都通公司大型团建开始招募啦！            《一人之下》动画开播五周年，7月9日相约中国国家图书馆总馆-国图艺术中心，共赴异人之约！重磅嘉宾齐现身，聊天互动玩游戏，听歌看戏抽福利，还能抢先获......
### [当产品说要把聊天气泡图标改成“加入聊天”四个字的时候](https://www.tuicool.com/articles/zUb22uj)
> 概要: 编辑导语：设计师在与产品沟通的时候总会遇到一些问题，比如设计师对于功能的设计优化想要更简洁，但是产品想要完整的设计稿，这时候设计师应该如何调节和实施呢？本文作者分享了关于设计师对于此现象的分析，我们一......
### [寿屋官方发布《游戏王》三幻神手办宣传片](https://www.3dmgame.com/news/202106/3817493.html)
> 概要: 今天，日本手办公司寿屋Kotobukiya官方发布了《游戏王》中经典三幻神：“欧西里斯的天空龙”、“太阳神的翼神龙”以及“欧贝利斯克的巨神兵”的手办宣传视频。寿屋三幻神手办宣传视频：其中，巨神兵手办高......
### [都在谈论的用户标签与画像，到底如何应用？](https://www.tuicool.com/articles/Z7Jjeui)
> 概要: 我们访谈了超过50个客户，发现大家在构建用户标签与画像的过程中有一些共性的问题。像业务部门基本都在抱怨：营销部门想用标签却需要等研发排期，标签制作下来，却已经错过最佳营销窗口期；没有标签管理系统，难以......
### [小米电视 6 将搭载双摄像头：4800 万像素，暗示新交互方式](https://www.ithome.com/0/559/159.htm)
> 概要: IT之家6 月 24 日消息 今日 @小米电视 官方再次对即将发布的小米电视 6 进行预热，展现了这款电视搭载的升降式双摄像头模组。这是小米电视首次搭载双摄像头，预计每颗均为 4800 万像素，图中还......
### [《绯红结系》联动TV动画 发现动画暗号获游戏奖励](https://www.3dmgame.com/news/202106/3817496.html)
> 概要: 万代南梦宫旗下新游《绯红结系》今天登陆Xbox Series X/PS5平台发售，Steam版于明日发售，官方宣布将与7月1日开播的《绯红结系》TV动画联动，动画中每话都藏有暗号秘密，只要观看动画找出......
### [《特利迦奥特曼》新情报：前辈泽塔将会客串出场](https://acg.gamersky.com/news/202106/1400480.shtml)
> 概要: 《特利迦奥特曼》公布全新情报，前辈奥泽塔确定客串出场！
### [组图：网友偶遇应采儿郑希怡逛街 闺蜜二人打扮休闲少女感十足](http://slide.ent.sina.com.cn/star/slide_4_86512_358466.html)
> 概要: 组图：网友偶遇应采儿郑希怡逛街 闺蜜二人打扮休闲少女感十足
### [巴萨官方视频：生日这天回顾一下梅西的十大精彩又经典的进球！你对哪球印象最深？](https://bbs.hupu.com/43839557.html)
> 概要: 巴萨官方视频：生日这天回顾一下梅西的十大精彩又经典的进球！你对哪球印象最深？
### [终于下映，总票房近14亿，这部2021年最烂好莱坞大片赢得漂亮](https://new.qq.com/omn/20210624/20210624A0AHH400.html)
> 概要: 6月的电影市场依然是没什么水花，虽然父亲节有15部电影扎堆上映，其中大部分还都是父爱题材，但大家几乎都没什么排片，只有《了不起的爸爸》的排片接近20%，这部电影也凭借微弱的优势夺得了6月20号当天的票......
### [这届年轻人为什么不爱韩妆了？](https://www.tuicool.com/articles/Jnm2YrA)
> 概要: 「核心提示」韩剧、韩流明星的走红，曾经带火了韩妆。过去作为大牌平替，高颜值、上新快的韩国美妆，深受国内年轻消费者喜爱，不过经历销售渠道更替，以及国货崛起，韩妆一年不如一年，甚至在中国大批量关店，韩妆为......
### [Writing ARM64 Code for Apple Platforms](https://developer.apple.com/documentation/xcode/writing-arm64-code-for-apple-platforms)
> 概要: Writing ARM64 Code for Apple Platforms
### [3DM轻松一刻第549期 三上悠亚老师真是羞花闭月](https://www.3dmgame.com/bagua/4662.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！这些羊会一直跑直到累死吗？谁不喜欢拍马屁呢像恶魔人进......
### [5月我国国际货物和服务贸易顺差1701亿元](https://finance.sina.com.cn/roll/2021-06-24/doc-ikqciyzk1623917.shtml)
> 概要: 原标题：5月我国国际货物和服务贸易顺差1701亿元 新华社北京6月24日电（记者刘开雄）国家外汇管理局24日发布数据显示，2021年5月...
### [海南三亚推出旅游消费责任险 发生消费纠纷可获先行赔付](https://finance.sina.com.cn/china/dfjj/2021-06-24/doc-ikqciyzk1625112.shtml)
> 概要: 原标题：海南三亚推出旅游消费责任险 发生消费纠纷可获先行赔付 6月22日，三亚市政府主办的“三亚放心游”公众号上线。该公众号作为三亚市旅游公共服务平台...
### [超级主播的升级焦虑，李佳琦和薇娅真需要上市吗？](https://www.tuicool.com/articles/AnyuYnF)
> 概要: 出品 | 虎嗅大商业组作者 | 苗正卿题图 | IC Photo6月23日晚20点，网红少年丁真出现在薇娅直播间，但这位半年前曾引爆网络的红人并没有给薇娅直播间带来超级人气。1227万的观看数据，是薇......
### [收益聚合器 Eleven Finance 与 Nerve 相关机枪池将于下周公开补偿计划](https://www.btc126.com//view/173512.html)
> 概要: 官方消息，BSC上收益聚合器 Eleven Finance 针对与稳定币交易平台Nerve 相关的机枪池遭到攻击发布完整报告，机枪池因漏洞损失约 450 万美元，Eleven 表示漏洞原因为开发人员代......
### [BTC五分钟内上涨1.14%，现报$34,112.52](https://www.btc126.com//view/173513.html)
> 概要: BTC五分钟内上涨1.14%，上涨金额为383.6美元，其中欧易OKEx上现价为$34,112.52，请密切关注行情走向，注意控制风险。更多实时行情异动提醒，快在APP内添加自选，开启智能盯盘，快人一......
### [风投公司Andreessen Horowitz推出22亿美元的加密基金](https://www.btc126.com//view/173514.html)
> 概要: Cointelegraph消息，顶级风险投资公司Andreessen Horowitz正在推出一个新的22亿美元的加密货币基金，这是有史以来最大的加密风险基金......
### [国家组织药品集采规模最大的一次！百姓用药将如此受益](https://finance.sina.com.cn/china/2021-06-24/doc-ikqciyzk1628798.shtml)
> 概要: 第五批国家组织药品集采6月23日在上海开标，产生拟中选结果：拟中选企业148家，拟中选产品251个，拟中选药品平均降价56%。按约定采购量计算，每年可节省255亿元。
### [这些年轻人，为什么要给自己买养老房？](https://finance.sina.com.cn/china/2021-06-24/doc-ikqciyzk1629320.shtml)
> 概要: 原标题：这些年轻人，为什么要给自己买养老房？ 来源：每日人物 这里是每日人物的专栏“千万间”。 房子背后，是人，是城市，也是我们生活的世界。我们纪录人与房的故事。
### [财经TOP10|售价高达590元的片仔癀一粒难求？谁是幕后推手](https://finance.sina.com.cn/china/caijingtop10/2021-06-24/doc-ikqciyzk1633488.shtml)
> 概要: 【政经要闻】 NO.1 商务部：所谓新疆“强迫劳动”问题完全违背事实 在今天（24日）商务部举行的例行新闻发布会上，针对美国商务部于当地时间6月23日宣布将5家所谓“涉及强迫劳...
### [上线5天票房156万，陈浩民又带来一部烂片，西游题材又遭殃了](https://new.qq.com/omn/20210624/20210624A0C9YR00.html)
> 概要: 作为中国文学史上经典的神魔小说，《西游记》一直是国产影视剧改编的重点题材之一，根据系统统计，西游记的影视化最早可以追溯到1927年的《盘丝洞》，该作品是由上海影视公司出品，改编自《西游记》的第72回......
### [Nexo将Polkadot添加到其生态系统](https://www.btc126.com//view/173518.html)
> 概要: 据Coinpedianews消息，Nexo宣布将Polkadot添加到其生态系统中，并将允许DOT用于借款、抵押和Nexo交易......
### [SN 0-2 EDG集锦：获取优势稳步推进，神钩开团EDG取胜](https://bbs.hupu.com/43842261.html)
> 概要: 来源：  虎扑    标签：EDGSN
### [Anker 65W 2C 口氮化镓充电器开售：239 元，功率盲插功能](https://www.ithome.com/0/559/214.htm)
> 概要: IT之家6 月 24 日消息 充电头厂商 Anker（安克）此前推出了第二代氮化镓充电器产品，包含 30W、45W 至 65W 的多个型号，均采用银灰色的外观。今日 Anker 65W 2C 口氮化镓......
### [应采儿晒38岁庆生照，蛋糕铺满玫瑰超浪漫，获两儿子甜蜜夹吻](https://new.qq.com/omn/20210624/20210624A0C3G500.html)
> 概要: 6月24日应采儿在个人社交账号晒出家人为自己甜蜜庆生的照片，去年5月应采儿为陈小春产下小儿子“Hoho”,如今他也已经1岁多了，时间真的过得非常快。应采儿在配文中感慨表示：已经到了忘记自己年纪的时候了......
### [真的感受到了赵丽颖的尴尬，这一季《拜托了冰箱》让人失望！](https://new.qq.com/omn/20210624/20210624A0CFLY00.html)
> 概要: 在看了《》的6月23号更新的一期之后，谁还能够想起赵丽颖是冰箱的主人呢？，可是全程没有看到赵丽颖的冰箱，也感觉不出来她是C位。            自发布了离婚官宣不久，参加《》第七季的路透照片就被......
### [国资委：着力推动国企改革举措制度化、规范化、长效化](https://finance.sina.com.cn/china/gncj/2021-06-24/doc-ikqcfnca3032489.shtml)
> 概要: 原标题：国资委：着力推动国企改革举措制度化、规范化、长效化 财联社6月24日讯，国务院国有企业改革领导小组办公室以视频会议方式...
### [微软中国官方预热“Win11”：Ta 来了，全新 Windows ！](https://www.ithome.com/0/559/215.htm)
> 概要: IT之家6 月 24 日消息 微软中国官方号“微软科技”今天发布预热，Ta 来了，全新 Windows！微软表示，接下来，Windows 会带来什么样的惊喜呢？北京时间 6 月 24 日 23:00 ......
### [300层赛娜打了70分钟输出不到3W？LPL得有多少AD能打的比teddy强？](https://bbs.hupu.com/43842537.html)
> 概要: 一个赛娜，到后面两百多接近三百层被动的赛娜，打了70分钟比赛，输出仅有2W5。我觉得LPL得有一打AD在这个局面下能打的比teddy有用的多。
### [研究机构：韩国运营商 5G 网络性能领跑全球，LG U+ 表现最优](https://www.ithome.com/0/559/216.htm)
> 概要: 伴随数据使用量持续呈爆炸式增长，尤其是在新冠疫情刺激下，5G 的商业化进展可谓日新月异。作为全球最先大规模部署及商用 5G 的国家之一，韩国的 5G 发展向来吸引着产业界的视线，一定程度上成为了一个标......
# 小说
### [无尽武装](http://book.zongheng.com/book/56579.html)
> 作者：缘分0

> 标签：科幻游戏

> 简介：这里是天堂，因为这里拥有地球上拥有的一切。所有你渴望的而又得不到的，在这里都可以得到；这里是地狱，因为每个人都要在这里艰苦挣扎，然后在分不清真假的世界中醉生梦死。这里，就是无限杀戮的世界……搜索缘分0，关注公众号，掌握更新动态。

> 章节末：新书已发

> 状态：完本
# 论文
### [Linear unit-tests for invariance discovery](https://paperswithcode.com/paper/linear-unit-tests-for-invariance-discovery)
> 日期：22 Feb 2021

> 标签：None

> 代码：https://github.com/facebookresearch/InvarianceUnitTests

> 描述：There is an increasing interest in algorithms to learn invariant correlations across training environments. A big share of the current proposals find theoretical support in the causality literature but, how useful are they in practice?
### [Artificial Neural Variability for Deep Learning: On Overfitting, Noise Memorization, and Catastrophic Forgetting](https://paperswithcode.com/paper/artificial-neural-variability-for-deep)
> 日期：12 Nov 2020

> 标签：None

> 代码：https://github.com/zeke-xie/artificial-neural-variability-for-deep-learning

> 描述：Deep learning is often criticized by two serious issues which rarely exist in natural nervous systems: overfitting and catastrophic forgetting. It can even memorize randomly labelled data, which has little knowledge behind the instance-label pairs.
