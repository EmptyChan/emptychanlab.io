---
title: 2022-08-06-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SFSaltFlats_ZH-CN7261637239_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-08-06 21:36:04
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SFSaltFlats_ZH-CN7261637239_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [元宇宙：银行DAO社区产品国际化探索](https://www.woshipm.com/pd/5555104.html)
> 概要: 编辑导语：产品国际化的实现，一定程度上可以让更大体量的用户体验并使用产品。而社区产品的国际化实现难度相对较大，要想达成目标，在产品语言、产品运营等层面，业务人员都需进行充分考量。本文作者便结合银行DA......
### [电商直播潮水退去，一场关于三方的变革悄然开始](https://www.woshipm.com/it/5555827.html)
> 概要: 编辑导语：电商直播赛道的发展并非一帆风顺，但也时有出圈现象，比如前阵子的东方甄选直播间。而当电商直播的“潮水”逐渐退去，这一赛道后续会走向怎样的变革？本篇文章里，作者便针对电商直播赛道进行了总结分析，......
### [虚拟偶像造梦六十年：爆于AI驱动，困于真人内“芯”](https://www.woshipm.com/it/5555697.html)
> 概要: 编辑导语：虚拟偶像的未来真的到来了吗？随着数字技术的飞速发展，人们的想象空间被无限扩大，元宇宙、虚拟偶像等概念的出现，则恰好印证了想象空间无限延伸的现实。不过，诸如此类概念，在当下，可能依旧面临着无法......
### [What ever happened to the transhumanists?](https://gizmodo.com/what-happened-to-transhumanism-in-2022-life-extension-1849199492)
> 概要: What ever happened to the transhumanists?
### [CudaText: Open-source, cross-platform text editor, written in Lazarus](https://github.com/Alexey-T/CudaText)
> 概要: CudaText: Open-source, cross-platform text editor, written in Lazarus
### [砍单蔓延  部分手机芯片厂商下调今年出货预期](https://finance.sina.com.cn/tech/roll/2022-08-06/doc-imizirav6967023.shtml)
> 概要: 本报记者 陈佳岚 广州报道......
### [新能源乘用车加速“出海”](https://finance.sina.com.cn/tech/roll/2022-08-06/doc-imizmscv5054948.shtml)
> 概要: 本报记者 黄琳 赵毅 深圳报道......
### [新能源汽车购置税继续免征](https://finance.sina.com.cn/tech/roll/2022-08-06/doc-imizirav6968876.shtml)
> 概要: 本报记者 陈燕南 童海华 北京报道......
### [Counterfeits, fraud, and theft: Why Silca changed its return policy](https://cyclingtips.com/2022/07/interview-silca-on-amazon-e-commerce-fraud-theft-returns/)
> 概要: Counterfeits, fraud, and theft: Why Silca changed its return policy
### [外媒：苹果要供应商从台湾地区向中国大陆供货时严格遵守中方规定](https://finance.sina.com.cn/tech/it/2022-08-06/doc-imizirav6971824.shtml)
> 概要: 环球时报综合报道在美国众议院议长佩洛西窜访台湾遭谴责之际，美国科技巨头苹果公司已要求其供应商从台湾地区向中国大陆供货时严格遵守中国海关有关规定。截至记者5日发稿时，苹果方面尚未就有关报道予以置评......
### [男人“力不从心”，别怪电动汽车](https://www.huxiu.com/article/625054.html)
> 概要: 出品｜虎嗅汽车组作者｜周到编辑｜张博文头图｜视觉中国尽管是一项纯粹的科技产品，但电动汽车从不乏都市传说。早在2015年深圳开始推广纯电动出租车时，关于电动汽车的都市传说便甚嚣尘上，首当其冲的便是：电动......
### [三季度全球货币政策展望：进退两难](https://www.huxiu.com/article/628260.html)
> 概要: 本文来自微信公众号：培风客 （ID：peifengke），作者：奥德修斯的凝望，头图来自：视觉中国其实我现在发自内心觉得，货币政策已经丧失了很多作用，但比较无奈的是，现在我们还是只能用这个老方法去刺激......
### [《FIFA 23》正式比赛日体验深度探讨预告发布](https://www.3dmgame.com/news/202208/3848693.html)
> 概要: EA发布了《FIFA 23》“正式比赛日体验深度探讨”预告片，与EA SPORTS FIFA开发团队一起看看《FIFA 23 》比赛日体验，包括AR广播回放、超逼真的球场表面、更逼真的人群等内容。预告......
### [陆金所二季度净利润下跌近四成，赵容奭担任董事长](https://www.yicai.com/news/101497337.html)
> 概要: 陆金所控股预计全年总营业收入将同比减少0%-3%，在603亿元-617亿元之间；净利润同比减少20%-22%，在130亿元-134亿元之间。
### [组图：廖凡李易峰春夏等出席活动 变身超模走T台气质拿捏](http://slide.ent.sina.com.cn/star/slide_4_704_373518.html)
> 概要: 组图：廖凡李易峰春夏等出席活动 变身超模走T台气质拿捏
### [三亚今起全域静态管理！回程机票暴涨，部分航线熔断](https://www.yicai.com/news/101497403.html)
> 概要: 在刚刚过去的暑运上半场，海南是国内旅客飞去的主要目的地之一。
### [动画「电锯人」公开第三弹宣传PV](http://acg.178.com/202208/453748912653.html)
> 概要: 昨晚（8月5日），MAPPA公开了动画「电锯人」的第三弹最新宣传PV与首批声优阵容，同时官方宣布该动画将于2022年10月正式开播。动画「电锯人」第三弹宣传PVCAST电次：户谷菊之介玛奇玛：楠木灯早......
### [苹果吃老本？Apple Watch S8新品与前代完全相同](https://www.3dmgame.com/news/202208/3848697.html)
> 概要: 消息称苹果将于下个月如期召开秋季新品发布会，届时除iPhone 14系列手机外，还有大批新品发布，其中包括智能手表Apple Watch S8。随着发布会的临近，关于Apple Watch S8的爆料......
### [防弹少年团将举办免费演唱会 观众超10万人规模](https://ent.sina.com.cn/2022-08-06/doc-imizmscv5071358.shtml)
> 概要: 新浪娱乐讯 据报道，防弹少年团演唱会观众超10万人规模，所属社HYBE方面已经同意此次演唱会免费，目前所属社正在就场馆选址进行商讨......
### [「总之就是非常可爱」特别篇动画主视觉图公开](http://acg.178.com/202208/453749969095.html)
> 概要: 电视动画「总之就是非常可爱」第2期先行特别篇动画「总之就是非常可爱 ～制服～」发布了主视觉图，本作将于2022年秋季开始播出。CAST由崎司：鬼頭明里由崎星空（ナサ）：榎木淳弥有栖川 要：芹澤優有栖川......
### [GameStop的NFT市场未经允许销售独立游戏遭痛斥](https://www.3dmgame.com/news/202208/3848701.html)
> 概要: GameStop新推出的NFT市场中惊现未经授权的独立游戏，开发者对这一行径表示非常不满。根据媒体报道，NiFTy Arcade合集产品的作者Nathan Ello将《Worm Nom Nom》、《G......
### [通过小程序实现微信扫码登录，个人网站接入微信扫码登录功能](https://segmentfault.com/a/1190000042279451)
> 概要: 需求网站如果想要实现微信扫码登录其实有很多种方案，常见的方案就是微信开放平台和微信公众号服务号。前者是目前大部分网站并且是微信认可的一种方式，后者是开发者发现服务号具备扫码关注后即可获取用户基本信息的......
### [TV动画《电锯人》公开第三弹预告！](https://news.dmzj.com/article/75164.html)
> 概要: 藤本树原作《电锯人》动画公开最新PV！
### [《马里奥赛车8：豪华版》DLC第二弹赛道对比视频](https://www.3dmgame.com/news/202208/3848707.html)
> 概要: 《马里奥赛车8：豪华版》近日发布DCL增值包第二弹，将部分经典地图重新带回到游戏之中。IGN近日对新增地图与对应的经典地图画质和赛道细节进行视频对比，一起来看一下。对比视频：和第一波赛道DLC一样，第......
### [轻小说《转生成自动贩卖机的我今天也在迷宫徘徊》动画化决定！PV公开](https://news.dmzj.com/article/75165.html)
> 概要: 轻小说《转生成自动贩卖机的我今天也在迷宫徘徊》动画化决定！PV公开。
### [台湾路名中藏着中国地图，是老一辈人的乡愁？](https://www.huxiu.com/article/628431.html)
> 概要: 本文来自微信公众号：廖信忠（ID：lxztaiwan），作者：廖信忠，头图来自：图虫创意最近，据说各种手机地图上都完整显示了台湾地区各县市的路名店名，很多人惊讶地发现，许多地方路名都是以大陆城市来命名......
### [A Simple but Nifty Inequality](https://residuetheorem.com/2017/02/10/a-simple-but-nifty-inequality/)
> 概要: A Simple but Nifty Inequality
### [这个院子，开启成都社区融合文创风潮|中国城市社区观察](https://www.yicai.com/news/101497534.html)
> 概要: 在院子文化创意园，史雷用更加宽阔丰富的视野、更多元的形式，激活成都在地文化生活。
### [轻小说「转生成自动贩卖机的我今天也在迷宫徘徊」宣布动画化](http://acg.178.com/202208/453763120311.html)
> 概要: 昼熊原作轻小说「转生成自动贩卖机的我今天也在迷宫徘徊」宣布制作电视动画，并发布了动画化决定PV。轻小说「转生成自动贩卖机的我今天也在迷宫徘徊」动画化决定PV剧情简介：被卷入一场车祸意外的主人公醒来之后......
### [《文豪野犬》第四季PV公开！明年1月开播](https://news.dmzj.com/article/75166.html)
> 概要: 《文豪野犬》第四季PV公开！明年1月开播。
### [低谷期稳住了，阿里距离走入上行通道还有多久？](https://www.tuicool.com/articles/uiYRnyZ)
> 概要: 低谷期稳住了，阿里距离走入上行通道还有多久？
### [组图：王心凌乘风破浪夺冠后首登封面 甜心教主尝试另类风格](http://slide.ent.sina.com.cn/star/slide_4_704_373525.html)
> 概要: 组图：王心凌乘风破浪夺冠后首登封面 甜心教主尝试另类风格
### [航行警告！这些海域禁止驶入](https://www.yicai.com/news/101497601.html)
> 概要: 8月5日至6日，葫芦岛海事局、连云港海事局、大连海事局相继发布航行警告。
### [电视动画「MIX」第二季开始制作](http://acg.178.com/202208/453772055576.html)
> 概要: 近日，电视动画「MIX」官方宣布开始制作第二季，第一季于2019年4月播出。电视动画「MIX」改编自安达充创作的同名运动漫画，并于2018年8月宣布了动画化。故事主要讲述高中生立花投马和他的弟弟立花走......
### [《文豪野犬》动画第四季PV 将于2023年1月开播](https://acg.gamersky.com/news/202208/1506469.shtml)
> 概要: KADOKAWA发布了《文豪野犬》的第四季PV。
### [特斯拉将于 8 月 24 日收盘后 1:3 拆分股票，上一次拆股曾引起市值飙升](https://www.ithome.com/0/633/646.htm)
> 概要: IT之家8 月 6 日消息，特斯拉今天宣布，将推进 1 拆 3 的股票拆分计划，并将于 8 月 24 日收盘后向各位股东派发股息。昨天，特斯拉股东投票表决了 1:3 的股票分割提案，并以多数票通过 (......
### [自嗨锅官旗发车：自热火锅 8.3 元起 / 桶；自热煲仔饭 9.9 元 / 桶](https://lapin.ithome.com/html/digi/633647.htm)
> 概要: 天猫【自嗨锅旗舰店】自嗨锅自热煲仔饭 6 桶日常售价为 79.9 元，下单领取 20 元优惠券，到手价为 59.9 元，折合约 9.9 元 / 桶：天猫自嗨锅自热煲仔饭6 桶券后 59.9 元领 20......
### [英特尔否认 Meteor Lake 处理器延期，消费级芯片仍将于 2023 年上市](https://www.ithome.com/0/633/649.htm)
> 概要: IT之家8 月 6 日消息，英特尔第二季度过的并不好，由于 PC 采购量下滑，营收下降了 22%。昨天集邦咨询（TrendForce）更是放出 Meteor Lake 处理器将推迟一年的坏消息，也就是......
### [三星将于 2023 年开始在越南生产半导体零件](https://www.ithome.com/0/633/656.htm)
> 概要: IT之家8 月 6 日消息，三星正在加强国际供应商网络维系，并试图在更多的国家拓展其制造业务。据越南媒体 Lao Dong 报道，本周早些时候，三星卢泰文会见了越南总理范明钦，为接下来的事情做准备。据......
### [公募REITs添新丁，华夏北京保障房REIT获批](http://www.investorscn.com/2022/08/06/102214/)
> 概要: 8月5日，公募REITs市场再添新丁，华夏北京保障房中心租赁住房REIT（简称华夏北京保障房REIT）获得证监会注册批复，据悉，该REITs将尽快启动发售工作。华夏北京保障房REIT的底层资产是北京市......
### [凯文·费奇等制片人向《蝙蝠女》导演表示支持](https://www.3dmgame.com/news/202208/3848728.html)
> 概要: 在《蝙蝠女》电影被取消之后，导演阿迪尔·埃尔·阿比和比拉勒·法拉赫受到了来自多位制片人的支持，包括凯文·费奇、埃德加·赖特和詹姆斯·古恩。消息曝光后，费奇发送电邮给埃尔·阿比，并将内容公布于Insta......
### [万亿零食行业，开启“壮年”改革](https://www.tuicool.com/articles/mAbIzir)
> 概要: 万亿零食行业，开启“壮年”改革
### [张镇麟美国训练1v1单挑凯尔特人前锋奥杰莱，引用自抖音@德鲁汉伦](https://bbs.hupu.com/55041586.html)
> 概要: 张镇麟美国训练1v1单挑凯尔特人前锋奥杰莱，引用自抖音@德鲁汉伦
### [【CSS】基础选择器，包括标签选择器、类选择器、id 选择器和通配符选择器...](https://www.tuicool.com/articles/iI7JBz3)
> 概要: 【CSS】基础选择器，包括标签选择器、类选择器、id 选择器和通配符选择器...
### [东莞：20年工龄的中年人离场，时薪9块的年轻人入局](https://www.huxiu.com/article/628663.html)
> 概要: 本文来自微信公众号：经济观察网（ID：eeojjgcw），作者：李紫宸，原文标题：《再见东莞：二十年工龄的中年人离场，九块钱时薪的年轻人入场》，头图来源：视觉中国8月3日晚上六点，东莞市塘厦镇下着雨，......
### [三亚市旅文局回应“酒店半价比预订价格高”：变相提价可投诉](https://finance.sina.com.cn/china/gncj/2022-08-06/doc-imizmscv5129832.shtml)
> 概要: 8月6日凌晨6时起，三亚全市实行临时全域静态管理，约8万游客滞留当地。当日，三亚市召开新闻发布会提出“酒店为滞留在住游客提供半价优惠续住服务”...
### [2022互联网广告市场半年大报告：上半年互联网广告市场规模2903.6亿元，总体在筑底，居家场景广告崛起](https://www.tuicool.com/articles/NneYfyU)
> 概要: 2022互联网广告市场半年大报告：上半年互联网广告市场规模2903.6亿元，总体在筑底，居家场景广告崛起
### [腾讯联手罗技 云游戏找到市场定位了吗？](https://finance.sina.com.cn/china/2022-08-06/doc-imizmscv5132667.shtml)
> 概要: 来源：品玩  前几天，罗技宣布要和腾讯联手推出便携游戏机，而据业内人士爆料，这款掌机将会采用安卓系统，主打云游戏，但机器配置比较低。
### [《海贼王》新设定，尾田公布贝克曼技能是霸气＋弹丸，能击伤黄猿](https://new.qq.com/omn/20220806/20220806A07DIV00.html)
> 概要: 《海贼王》新设定，尾田公布贝克曼技能是霸气＋弹丸，能击伤黄猿
### [赛后采访Ming：不用特别在意排名什么的，打好自己的就行](https://bbs.hupu.com/55042911.html)
> 概要: 来源：  虎扑
### [抢到1万多的三亚回上海机票，她被通知下飞机](https://www.yicai.com/news/101497745.html)
> 概要: 铁路部门对三亚地区也全部做了禁售处理，市民不能通过铁路离开三亚。
### [抢到1万多的三亚回上海机票，我却在坐进机舱后被赶下飞机](https://finance.sina.com.cn/china/gncj/2022-08-06/doc-imizirav7047572.shtml)
> 概要: 这个暑期，与不少国人一样，王女士选择带孩子在三亚度过，原本今天下午（6日）就是他们从三亚飞回上海的计划时间，但突如其来的疫情和三亚临时升级的封控措施...
### [宁波震裕通过宁德时代6S标杆线审核](https://finance.sina.com.cn/china/gncj/2022-08-06/doc-imizmscv5132866.shtml)
> 概要: e公司讯，宁波震裕科技消息，宁波震裕通过宁德时代6S标杆线审核。8月2日至8月3日，C客户莅临公司香山工厂进行6S标杆线审核。8月2日，在香山工厂会议室一...
### [连续三年下台阶！清盘基金存续年限下降，发生了什么？](https://finance.sina.com.cn/china/gncj/2022-08-06/doc-imizirav7048447.shtml)
> 概要: 来源：中国基金报 中国基金报记者 李树超 实习生 周倬睿 在公募基金数量超过1万只、经历去年新基金发行大年后，今年A股的剧烈震荡也加大了基金清盘的速度。
### [赛后LSB 1-0 KT：蝎子无解先手见人就拉，LSB轻松逆转击败KT](https://bbs.hupu.com/55043118.html)
> 概要: 虎扑08月06日讯 【GAME1】KT蓝色方、LSB红色方【KT禁用】波比、蔚、卡莉斯塔、奥恩、瑟庄妮【KT选用】Rascal盲僧、Cuzz佛耶戈、Vicla阿狸、Aiming泽丽、Life璐璐【LS
### [赛后采访XYG.秀豆：救射手一般都是打野说的](https://bbs.hupu.com/55043122.html)
> 概要: 来源：  虎扑
# 小说
### [人道守山](http://book.zongheng.com/book/1083022.html)
> 作者：血色悲殇

> 标签：奇幻玄幻

> 简介：七千年前，黄帝以无敌机械星舰联合天庭打败蚩尤，从此机械成为主流，人道主宰星空。五千年前，神农从混沌中归来，人道势力蔓延入苍茫，开启了混沌时代。三千年前，帝喾联合天庭击败神话生物，从此万载山河归人仙，龙凤皆为殿下臣。一千年前，颛顼四面，重现黄帝之威，以一己之力大破天庭，万界一统。但盛极必衰，巅峰之后就是衰退，出生在人族最为巅峰时代的林牧群，遥望山河日落。吾将何从？

> 章节末：一百四十二、终章

> 状态：完本
# 论文
### [Adaptative Perturbation Patterns: Realistic Adversarial Learning for Robust NIDS | Papers With Code](https://paperswithcode.com/paper/adaptative-perturbation-patterns-realistic)
> 概要: Adversarial attacks pose a major threat to machine learning and to the systems that rely on it. Nonetheless, adversarial examples cannot be freely generated for domains with tabular data, such as cybersecurity. This work establishes the fundamental constraint levels required to achieve realism and introduces the Adaptative Perturbation Pattern Method (A2PM) to fulfill these constraints in a gray-box setting. A2PM relies on pattern sequences that are independently adapted to the characteristics of each class to create valid and coherent data perturbations. The developed method was evaluated in a cybersecurity case study with two scenarios: Enterprise and Internet of Things (IoT) networks. Multilayer Perceptron (MLP) and Random Forest (RF) classifiers were created with regular and adversarial training, using the CIC-IDS2017 and IoT-23 datasets. In each scenario, targeted and untargeted attacks were performed against the classifiers, and the generated examples were compared with the original network traffic flows to assess their realism. The obtained results demonstrate that A2PM provides a time efficient generation of realistic adversarial examples, which can be advantageous for both adversarial training and attacks.
### [Why do tree-based models still outperform deep learning on tabular data? | Papers With Code](https://paperswithcode.com/paper/why-do-tree-based-models-still-outperform)
> 日期：18 Jul 2022

> 标签：None

> 代码：https://github.com/leogrin/tabular-benchmark

> 描述：While deep learning has enabled tremendous progress on text and image datasets, its superiority on tabular data is not clear. We contribute extensive benchmarks of standard and novel deep learning methods as well as tree-based models such as XGBoost and Random Forests, across a large number of datasets and hyperparameter combinations. We define a standard set of 45 datasets from varied domains with clear characteristics of tabular data and a benchmarking methodology accounting for both fitting models and finding good hyperparameters. Results show that tree-based models remain state-of-the-art on medium-sized data ($\sim$10K samples) even without accounting for their superior speed. To understand this gap, we conduct an empirical investigation into the differing inductive biases of tree-based models and Neural Networks (NNs). This leads to a series of challenges which should guide researchers aiming to build tabular-specific NNs: 1. be robust to uninformative features, 2. preserve the orientation of the data, and 3. be able to easily learn irregular functions. To stimulate research on tabular architectures, we contribute a standard benchmark and raw data for baselines: every point of a 20 000 compute hours hyperparameter search for each learner.
