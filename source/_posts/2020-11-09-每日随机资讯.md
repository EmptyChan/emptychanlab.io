---
title: 2020-11-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.PiedmontRegion_EN-CN9491375405_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-11-09 21:45:15
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.PiedmontRegion_EN-CN9491375405_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：张韶涵拼色西装外套配阔腿裤 时尚出行大方秀长腿](http://slide.ent.sina.com.cn/y/slide_4_704_347974.html)
> 概要: 组图：张韶涵拼色西装外套配阔腿裤 时尚出行大方秀长腿
### [组图：谭卓穿印花长款风衣女王范足 梳中分短发精神干练](http://slide.ent.sina.com.cn/star/slide_4_704_347999.html)
> 概要: 组图：谭卓穿印花长款风衣女王范足 梳中分短发精神干练
### [喻言工作室投资人为本人 自己给自己当老板](https://ent.sina.com.cn/y/yneidi/2020-11-09/doc-iiznctke0388962.shtml)
> 概要: 新浪娱乐讯 11月9日，有网友据天眼查APP显示发现喻言工作室的投资人就是喻言自己，其公司注册资本50万元，引起不少网友热议：“再也不是打工人了”、“自己做自己的老板太厉害了吧”、“想问喻老板还缺助理......
### [视频：网传小男孩获鲍蕾陪伴出行 陆毅方辟谣有三胎儿子](https://video.sina.com.cn/p/ent/2020-11-09/detail-iiznctke0433887.d.html)
> 概要: 视频：网传小男孩获鲍蕾陪伴出行 陆毅方辟谣有三胎儿子
### [组图：万茜穿裸背装性感迷人 手臂伤疤尚未恢复仍清晰可见](http://slide.ent.sina.com.cn/star/slide_4_704_347972.html)
> 概要: 组图：万茜穿裸背装性感迷人 手臂伤疤尚未恢复仍清晰可见
### [赵雅芝提前庆66岁生日，近照好似吃了防腐剂，奢华豪宅罕见曝光](https://new.qq.com/omn/20201109/20201109A08KTL00.html)
> 概要: 赵雅芝提前庆66岁生日，近照好似吃了防腐剂，奢华豪宅罕见曝光
### [没有CP感就别硬凑了吧！](https://new.qq.com/omn/20201109/20201109A0AFTN00.html)
> 概要: 没有CP感就别硬凑了吧！
### [赵雅芝晒66岁庆生照，穿粉色长裙秀纤细身材，年过半百仍似少女](https://new.qq.com/omn/20201109/20201109A0CEL500.html)
> 概要: 赵雅芝晒66岁庆生照，穿粉色长裙秀纤细身材，年过半百仍似少女
### [你16岁起的所有情感问题，梁静茹的歌里都有答案](https://new.qq.com/omn/20201108/20201108A0AK2000.html)
> 概要: 你16岁起的所有情感问题，梁静茹的歌里都有答案
### [工作人员自我介绍时被叶璇打断，直言房间有脚臭味，其他人尴尬极了！](https://new.qq.com/omn/20201109/20201109V0BX7W00.html)
> 概要: 工作人员自我介绍时被叶璇打断，直言房间有脚臭味，其他人尴尬极了！
# 动漫
### [打入日本票房前五！剧场版《鬼灭之刃》票房突破204亿日元](https://news.dmzj.com/article/69127.html)
> 概要: ​根据剧场版动画《鬼灭之刃 无限列车篇》的官方推特消息，在本作上映24日间，已经有超过1537万3943人次到电影院观看，票房收入达到了204亿8361万1650日元。
### [《刺客信条：英灵殿》PS平台亚洲版内容存差异](https://news.dmzj.com/article/69135.html)
> 概要: 育碧官方今日宣布《刺客信条：英灵殿》在PS平台上的亚洲版会与其他区域版本存在一定的差异：将调整部分血腥画面和部分画面中的女性衣物布料等内容。
### [疫情原因，世嘉要求650名员工自愿离职](https://news.dmzj.com/article/69125.html)
> 概要: 前几日，世嘉飒美公布最新季度财报，其中因新冠疫情对街机业务造成重创，世嘉要求650名员工自愿离职，高管主动降薪。
### [任天堂社长谈PS5发售 认为对公司的经济影响不大](https://news.dmzj.com/article/69126.html)
> 概要: 任天堂的家用游戏机NS在2020年度的销售台数从当初预期的500万台增加到了2400万台，这受到了居家隔离和3月发售的游戏《动物之森》销量火爆的影响。
### [肖战壁纸｜爱你三千遍](https://new.qq.com/omn/20201109/20201109A024CQ00.html)
> 概要: 图源网络，侵删！
### [还等什么？进来学忍术！](https://new.qq.com/omn/20201109/20201109A0F2MW00.html)
> 概要: 火遁豪火球之术:已一未一申一亥一午一寅C级忍术豪火球之术 是以查克拉提炼,从口中发出一团大火球,是一般下忍难以学会的忍术。字智波一族标志性忍术(所以说佐助是天才 )火遁凤仙火之术:子一寅一戌—丑—卯……
### [无声漫画：老爷爷发家致富的秘密](https://new.qq.com/omn/20201109/20201109A053XO00.html)
> 概要: 无声漫画：老爷爷发家致富的秘密
### [《天官赐福》被恶意刷一星低分，墨香的作品为何总是一波三折？](https://new.qq.com/omn/20201109/20201109A0475F00.html)
> 概要: 《天官赐福》是墨香的第3部作品，从小说开始就受到了很多读者的喜爱，今年10月底动画上线，更是引起了追番热潮，作品太火有利有弊，让更多人看到的同时，也引来了一些麻烦，那就是动画被恶意刷一星低分，从渣反……
### [暗黑系童话，当那些动漫中的人物来到现实，等待他们的都是噩梦](https://new.qq.com/omn/20201109/20201109A01V4Y00.html)
> 概要: 大家好，我是优优动漫菌，又来聊动漫啦！前两天优漫菌在网上看到了一组很有意思的图片，迫不及待地来和大家分享一下。在分享之前优漫菌想问问大家：你们看过迪士尼的动画电影吗？我想几乎百分百的观众都看过，比如……
### [《猫眼三姐妹》将翻拍真人网剧？网友：魔爪已经改变方向了](https://new.qq.com/omn/20201109/20201109A02M9R00.html)
> 概要: 近几年国内有不少影视公司专门购买日本那边的漫画作品版权回来翻拍真人网剧，而上段时间才放送改编的真人网剧《棋魂》还未曾完结，这次又有新的日漫改变成真人版网剧了，而这部作品正是《猫眼三姐妹》。《猫眼三姐……
### [剧场版《SAO：无星夜的咏叹调》新PV 2021年上映](https://acg.gamersky.com/news/202011/1335662.shtml)
> 概要: 《刀剑神域》剧场版动画《刀剑神域：进击篇 无星夜的咏叹调》公开了特报预告，宣布本作将于2021年上映。同时还公开了剧场版动画的视觉图。
### [《星期一的丰满》：超养眼 好身材妹子开连载了](https://acg.gamersky.com/news/202011/1335721.shtml)
> 概要: 《星期一的丰满》本周公开了重要决定，《星期一的丰满》将会在下周的《Young Megazine》上正式开始连载，看来本作将成为正式的漫画了，不知道到时候是不是还是只有一张图。
### [剧场版《宝可梦：超梦的逆袭》确认引进 中文海报发布](https://acg.gamersky.com/news/202011/1335752.shtml)
> 概要: Pokemon宝可梦官方微博今日宣布，宝可梦系列剧场版动画《宝可梦：超梦的逆袭 进化》确认引进内地，同时发布中文海报，具体档期尚未公布。
### [《鬼灭》剧场版票房破200亿日元 跻身日本影史前5](https://acg.gamersky.com/news/202011/1335813.shtml)
> 概要: 《鬼灭之刃：无限列车篇》上映24天，票房收入达204亿8361万1650日元，这个票房成绩已经超越了《哈利·波特与魔法石》（203亿），暂列日本影史票房榜第5名。
# 财经
### [五部门联合开展专项执法行动 严厉打击农村食品假冒伪劣违法犯罪](https://finance.sina.com.cn/china/bwdt/2020-11-09/doc-iiznezxs0901098.shtml)
> 概要: 原标题：五部门联合开展专项执法行动严厉打击农村食品假冒伪劣违法犯罪 来源：市场监管总局网站 11月9日，五部门联合部署的农村假冒伪劣食品专项执法行动电视电话会议在京...
### [上海通报一新发新冠肺炎病例 目前26名密接者23名核酸阴性](https://finance.sina.com.cn/china/gncj/2020-11-09/doc-iiznctke0481374.shtml)
> 概要: 原标题：上海通报一新发新冠肺炎病例，目前26名密接者23名核酸阴性 作者：卢杉 1月9日18：00，上海举行新冠肺炎疫情防控新闻发布会，上海市卫生健康委主任邬惊雷...
### [天津7家冷库共364人进行管控 汉沽街全域7.7万人全员核酸检测](https://finance.sina.com.cn/china/dfjj/2020-11-09/doc-iiznezxs0896496.shtml)
> 概要: 原标题：天津7家冷库共364人进行管控 汉沽街全域7.7万人全员核酸检测 天津9日无症状感染者在海联冷库装货期间共与4名员工有过接触。
### [五部门：联合打击农村食品假冒伪劣违法犯罪](https://finance.sina.com.cn/china/gncj/2020-11-09/doc-iiznctke0477894.shtml)
> 概要: 原标题：五部门联合开展专项执法行动 严厉打击农村食品假冒伪劣违法犯罪 11月9日，五部门联合部署的农村假冒伪劣食品专项执法行动电视电话会议在京召开。
### [新华时评：大账目中有小日子](https://finance.sina.com.cn/review/hgds/2020-11-09/doc-iiznezxs0898099.shtml)
> 概要: 原标题：新华时评：大账目中有小日子 新华社北京11月9日电 题：大账目中有小日子 新华社记者王立彬 中央财政不再是一个“高大上”词汇：一百来天...
### [香港特首年内第三次到访深圳 深港携手打造“双城经济”](https://finance.sina.com.cn/china/dfjj/2020-11-09/doc-iiznezxs0903236.shtml)
> 概要: 原标题：香港特首年内第三次到访深圳，深港携手打造“双城经济” 作者：王帆 据《深圳特区报》报道，11月7日下午，深圳市委书记王伟中...
# 科技
### [非常值得一看的 Curl 用法指南](https://segmentfault.com/a/1190000037776222)
> 概要: 导读curl 是常用的命令行工具，用来请求 Web 服务器。它的名字就是客户端（client）的 URL 工具的意思。它的功能非常强大，命令行参数多达几十种。如果熟练的话，完全可以取代 Postman......
### [搭建Springboot项目并集成Mybatis-Plus](https://segmentfault.com/a/1190000037775224)
> 概要: 1.idea创建Springboot项目创建项目：选择spring Initializr，注意要选择jdk，使用默认的spring.io这    样就不用再去写pom文件了输入项目名称：选择Sprin......
### [阿里投资的印度最大杂货电商遭黑客攻击](https://www.ithome.com/0/518/464.htm)
> 概要: 阿里巴巴投资的印度最大杂货电商平台 BigBasket 最近遭到黑客攻击，数百万用户的个人敏感信息被窃。美国网络安全研究公司 Cyble Inc. 最早报告了这一消息，之后得到 BigBasket 联......
### [轮到麒麟 980 ，华为 nova 5 Pro / 荣耀 20 系列开启 EMUI 11/Magic UI 4.0 内测招募](https://www.ithome.com/0/518/521.htm)
> 概要: IT之家11月9日消息 花粉俱乐部今天发布，华为nova 5 Pro 、荣耀20等8款产品开启EMUI 11.0 / Magic UI 4.0版本内测招募。IT之家获悉，本次参与内测机型包括，华为no......
### [成立7年没赚过钱， 这位AI四小龙有点惨](https://www.huxiu.com/article/392396.html)
> 概要: 出品| 虎嗅科技组作者| 张雪封面| CFP我们担心的事情还是发生了。记得几个月前，寒武纪作为“国内AI芯片”第一股冲刺科创板时，曾有业内人士对虎嗅表达过担忧：“若寒武纪上市成功，那么在一定程度上助长......
### [经纬、高瓴、红杉，这些一线机构今年都投了些啥？](https://www.huxiu.com/article/392378.html)
> 概要: 本文来自微信公众号：42章经（ID：myfortytwo），原文标题：《经纬、高瓴、红杉...一线机构今年都投了些啥？｜42章经》，题图来自：视觉中国总有人问我们今年市场感觉怎么样，我们的答案是：上半......
### [Linux系统调用原理](https://www.tuicool.com/articles/qUfYfmY)
> 概要: 一、什么是系统调用系统调用跟用户自定义函数一样也是一个函数，不同的是系统调用运行在内核态，而用户自定义函数运行在用户态。由于某些指令（如设置时钟、关闭/打开中断和I/O操作等）只能运行在内核态，所以操......
### [完美日记踏上IPO：高端品牌依旧是道槛](https://www.tuicool.com/articles/2iQVN3N)
> 概要: （配图来自Canva可画 ）人们总说“女为悦己者容”，而当下社会，女生化妆不只是为了“悦己者”，更是为了“悦己”。在这种心理因素的转变之下，“她经济”成为了时下的热门，不论是服饰包包还是形体健康，都将......
# 小说
### [王牌大高手](http://book.zongheng.com/book/777173.html)
> 作者：梁不凡

> 标签：都市娱乐

> 简介：神秘强者在保护美女的同时深陷阴谋，面对周围重重敌人，我点燃一根烟，除恶在人间。PS：请搜索并且关注微信公众号：梁不凡

> 章节末：第1128章 大结局

> 状态：完本
# 游戏
### [微星官方：深圳宝安工厂发生火灾](https://www.3dmgame.com/news/202011/3801634.html)
> 概要: 微星今天在官网上发布了一则通报，具体内容是2020年11月5日下午，位于中国深圳宝安区的一家微星工厂发生重大火灾事故，该工厂距离中国香港特别行政区约30分钟车程。微星报告称，事故中无人受伤，生产线没有......
### [剧场版动画《刀剑神域进击篇 无星夜的咏叹调》预告](https://www.3dmgame.com/news/202011/3801586.html)
> 概要: 之前《刀剑神域：爱丽丝篇》完结的同时，官方就宣布了《刀剑神域：进击篇》的动画化企划始动。现在官方正式宣布，剧场版动画《刀剑神域：进击篇 无星夜的咏叹调》将于2021年在日本上映。官方还公布了剧场版动画......
### [便携游戏PC《GPD WIN 3》概念图 屏幕滑动隐藏键盘更精致](https://www.3dmgame.com/news/202011/3801601.html)
> 概要: 深圳中软赢科技术有限公司推出的迷你便携游戏电脑GPD系列一直以精致高能深受一批忠实玩家的拥护，近日爆出了下一代《GPD WIN 3》概念图，看上去更加精致工业美学人体工学。·根据概念图所示，《GPD ......
### [国产单人自走棋《众神酒馆》上架Steam 今年上市](https://www.3dmgame.com/news/202011/3801605.html)
> 概要: 目前，一款名为《众神酒馆》的国产rougelike类的单人自走棋游戏已经上架Steam商城页面，该作预计将于2020年12月4日登陆Steam平台，开启抢先体验模式。Steam商城页面截图：游戏简介：......
### [微星3090显卡宙斯盾钛5主机开卖：壕配39999元](https://www.3dmgame.com/news/202011/3801628.html)
> 概要: 今日（11月9日），微星的新一代电竞主机Aegis Ti5（宙斯盾钛5）开始预售了，目前仅在天猫旗舰店首发，起步配置酷睿i7 8核+RTX 3070显卡，最高可选酷睿i9 10核及RTX 3090显卡......
# 论文
### [PCSGAN: Perceptual Cyclic-Synthesized Generative Adversarial Networks for Thermal and NIR to Visible Image Transformation](https://paperswithcode.com/paper/pcsgan-perceptual-cyclic-synthesized)
> 日期：13 Feb 2020

> 标签：None

> 代码：https://github.com/KishanKancharagunta/PCSGAN

> 描述：In many real world scenarios, it is difficult to capture the images in the visible light spectrum (VIS) due to bad lighting conditions. However, the images can be captured in such scenarios using Near-Infrared (NIR) and Thermal (THM) cameras.
### [Uneven Coverage of Natural Disasters in Wikipedia: the Case of Flood](https://paperswithcode.com/paper/uneven-coverage-of-natural-disasters-in)
> 日期：23 Jan 2020

> 标签：None

> 代码：https://github.com/javirandor/disasters-wikipedia-floods

> 描述：The usage of non-authoritative data for disaster management presents the opportunity of accessing timely information that might not be available through other means, as well as the challenge of dealing with several layers of biases. Wikipedia, a collaboratively-produced encyclopedia, includes in-depth information about many natural and human-made disasters, and its editors are particularly good at adding information in real-time as a crisis unfolds.
