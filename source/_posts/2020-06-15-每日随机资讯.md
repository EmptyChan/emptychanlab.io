---
title: 2020-06-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SurfSeason_EN-CN2064123733_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-06-15 21:07:45
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SurfSeason_EN-CN2064123733_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [肖战谈做公益自称新人：想细水长流一直做下去](https://ent.sina.com.cn/s/m/2020-06-15/doc-iircuyvi8581375.shtml)
> 概要: 新浪娱乐讯 6月11日，肖战作为扶贫产品体验官，参加了由蔚县人民政府、人民在线主办的“公益助农融媒行”走进河北蔚县活动。当天，肖战在融媒体行开幕式上作为扶贫产品体验官发言，随后参观了蔚县贡米生产基地、......
### [组图：洗米嫂晒写真 古铜色肌肤配白裙大秀好身材](http://slide.ent.sina.com.cn/star/slide_4_704_340308.html)
> 概要: 组图：洗米嫂晒写真 古铜色肌肤配白裙大秀好身材
### [组图：陶虹穿T恤配阔腿裤青春减龄 对镜挥手微笑暖心](http://slide.ent.sina.com.cn/star/slide_4_704_340319.html)
> 概要: 组图：陶虹穿T恤配阔腿裤青春减龄 对镜挥手微笑暖心
### [视频：周震南呼吁粉丝和平相处反被骂！到底是偶像失声还是被失声？](https://video.sina.com.cn/p/ent/2020-06-15/detail-iirczymk7142451.d.html)
> 概要: 视频：周震南呼吁粉丝和平相处反被骂！到底是偶像失声还是被失声？
### [组图：蒋梦婕穿碎花上衣身材纤细 等车时狂煲电话粥](http://slide.ent.sina.com.cn/star/slide_4_704_340366.html)
> 概要: 组图：蒋梦婕穿碎花上衣身材纤细 等车时狂煲电话粥
### [各大明星玩王者荣耀时最常用的英雄，张杰：霸气，杨幂：女神，李现：暖男](https://new.qq.com/omn/20200518/20200518A00IMK00.html)
> 概要: 各大明星玩王者荣耀时最常用的英雄，张杰：霸气，杨幂：女神，李现：暖男
### [乘风破浪的张雨绮，她本应该成为秦海璐，但她正在成为女版肖战](https://new.qq.com/omn/20200615/20200615A0O4T900.html)
> 概要: 乘风破浪的张雨绮，她本应该成为秦海璐，但她正在成为女版肖战
### [孙俪再晒千字书法作品，水平越来越高，网友：超哥越来越配不上你](https://new.qq.com/omn/20200615/20200615A0LV2F00.html)
> 概要: 孙俪再晒千字书法作品，水平越来越高，网友：超哥越来越配不上你
### [这事真是他做错了吗？](https://new.qq.com/omn/20200615/20200615A0NFRR00.html)
> 概要: 这事真是他做错了吗？
### [《天天向上》放出鞠婧祎旧照，王思聪积极点赞，黑历史躲不过啊！](https://new.qq.com/omn/20200615/20200615A0KP7700.html)
> 概要: 最近《乘风破浪的姐姐》热度高居不下，秉着肥水不流外人田的优良品德，《天天向上》也搞起了追梦的哥哥们，意在打造一支全新唱跳男团。            节目中邀请了筷子兄弟、陈翔、凤凰传奇的曾毅以及王一......
# 动漫
### [从恋人到夫妇，温情述说人生重要节点](https://news.dmzj.com/article/67674.html)
> 概要: 这期给大家推荐一部从恋人到夫妇的温情漫画《爱在结为连理前》，作者にいち，鹅妈妈的童谣汉化，讲述最初认为是特别的事物渐渐都变得理所当然，重复着这样的的每一天，同时向着未来迈开脚步。婚约中的情侣，真纪和哲也所带来的平淡而温馨的恋爱喜剧。
### [TV动画《记录的地平线 圆桌崩坏》延期播出](https://news.dmzj.com/article/67682.html)
> 概要: 原定于10月开始播出的TV动画《记录的地平线 圆桌崩坏》宣布了将延期至2021年1月播出的消息。这次延期是由于新冠病毒所带来的疫情严重影响了当初的制作进度造成的。
### [bell-fine《不要欺负我，长瀞同学》长瀞同学手办](https://news.dmzj.com/article/67681.html)
> 概要: bell-fine根据《不要欺负我，长瀞同学》中的长瀞同学制作的1/7比例手办目前已经开订了。本作再现了漫画2卷的封面图，通过拆卸上衣和裙子，还可以看到长瀞身着泳装的样子。
### [P站美图推荐——熊猫特辑](https://news.dmzj.com/article/67679.html)
> 概要: 圆滚滚，毛茸茸的国宝谁不喜欢呢？
### [宝可梦24：喷火龙出场？小豪收服霸主宝可梦 下集忍蛙或回归](https://new.qq.com/omn/20200615/20200615A0KKIF00.html)
> 概要: 哈喽，大家好，我是你们的小宅。本期小宅要给大家带来的是宝可梦新无印动画24集，上集说到了樱木研究所后花园的大骚动，这集是真主角主场。开篇就是短发坂木老大助理假传“圣旨”，哄骗火箭队一行人去休假，火箭……
### [举报《刺客伍六七》下架？纯属无稽之谈，这样的行为何时能停止？](https://new.qq.com/omn/20200615/20200615A0HF1500.html)
> 概要: 不知道从什么时候开始，恶意的攻击反而成为了一种潮流，国产动画频频遭到抵制，各种令人啼笑皆非的理由，都变成了他们口中伸张正义的利器，早些年就有不少国产或者是引进的动漫，被“热心人士”举报，最终下架，近……
### [别再催沧海横流了，玄机正忙着做《吞噬星空》和《天宝伏妖录》](https://new.qq.com/omn/20200615/20200615A0LG9U00.html)
> 概要: 想必很多人已经期待沧海横流很久了，但是秦时明月官方一直都没有准备要更新的打算。很多人苦苦催更，却得不到任何回应。但尽管如此，秦时明月的粉丝们仍然没有放弃秦时明月，每次一有点消息，都会非常的兴奋。只不……
### [海贼王：尾田总算明确了，赤犬实力不如白胡子一半](https://new.qq.com/omn/20200615/20200615A0ELMK00.html)
> 概要: 关于《海贼王》中各角色的实力，一直是海迷争论的焦点。尤其是大将与四皇谁更强，一直是没有定论。大部分海迷认为，四皇实力比大将强，香克斯、凯多这样的强者，连一个大将都搞不定的话，还怎么当“海上皇帝”。也……
### [狐妖小红娘：cp线混乱，“月红党”无法接受，幸亏涂山红红立场坚定](https://new.qq.com/omn/20200615/20200615A0O9J600.html)
> 概要: 或许很多小伙伴都认为，狐妖小红娘中的cp线并不混乱，月红是一对、苏白是一对、平兰也是一对，他们每个人都各自分开，拥有各自的主体、各自的思想，所以并不能混为一谈，相信这就是支持者的理由了。但是事实真的……
### [纯爱战士牛头人，事实证明，“NTR”才是治愈系动漫的首选](https://new.qq.com/omn/20200615/20200615A0GTFS00.html)
> 概要: 在我们的身边，总是少不了一些刚刚入圈的小萌新。如果让你给他们推荐几部动漫，作为入坑之作，你会推荐哪些动漫给他们呢？如果你是一个像我这般善良的老宅，那你推荐的动漫，一定是那种比较精彩，而且治愈人心的。……
### [《星期一的丰满》：可爱的男孩子有个可爱的姐姐](https://acg.gamersky.com/news/202006/1296789.shtml)
> 概要: 《星期一的丰满》这次带来了那个“男校公主”的新情况，居然是迷糊上班族后辈妹子的弟弟。看来这个男孩子的可爱，和家人分不开啊。
### [《恋与制作人》动画版7月开播 主题曲演唱者公开](https://acg.gamersky.com/news/202006/1296895.shtml)
> 概要: 改编自热门手游的动画《恋与制作人》今天公布了最新情报，该作将于今年7月开播。
# 财经
### [商务部：5月规模以上建材家居卖场销售额同比降超30%](https://finance.sina.com.cn/roll/2020-06-15/doc-iircuyvi8631921.shtml)
> 概要: 原标题：商务部：5月规模以上建材家居卖场销售额同比降超30% 新京报讯（记者 张洁）6月15日，商务部流通业发展司、中国建筑材料流通协会共同发布的数据显示...
### [探访上海水产市场：进口三文鱼正常售卖](https://finance.sina.com.cn/china/gncj/2020-06-15/doc-iirczymk7161891.shtml)
> 概要: 原标题：探访上海水产市场：进口三文鱼正常售卖 近日，因北京新发地市场切割进口三文鱼案板检测出新冠病毒，三文鱼售卖备受关注。
### [［网连中国］注意！出京去这29地需隔离](https://finance.sina.com.cn/china/2020-06-15/doc-iircuyvi8633999.shtml)
> 概要: 近日，北京市出现新冠肺炎聚集性疫情，多区明确进入“战时状态”。截至6月15日，国务院疫情风险等级查询小程序显示，北京市丰台区花乡地区为高风险地区，西城、房山...
### [新发地，究竟是一块什么“地”？](https://finance.sina.com.cn/china/gncj/2020-06-15/doc-iircuyvi8636050.shtml)
> 概要: 原标题：新发地，究竟是一块什么“地”？ ◎ 文 《法人》全媒体记者 姚瑶 北京市在连续50多天无本地报告新增新冠肺炎确诊病例后，6月11日以来，4天累计新增79人确诊。
### [湖北出台“数字经济13条” 助力疫后重振](https://finance.sina.com.cn/china/gncj/2020-06-15/doc-iirczymk7160728.shtml)
> 概要: 原标题：湖北出台“数字经济13条” 助力疫后重振图为新闻发布会现场 武一力 摄 （抗击新冠肺炎）湖北出台“数字经济13条” 助力疫后重振 中新网武汉6月15日电 （武一力）“数字...
### [交通部：加强对北京市及其周边省市交通运输工作指导](https://finance.sina.com.cn/china/gncj/2020-06-15/doc-iircuyvi8635930.shtml)
> 概要: 原标题：交通运输部：加强对北京市及其周边省市交通运输工作的指导 来源：交通运输部 6月15日，交通运输部召开应对新冠肺炎疫情工作领导小组会议，传达学习 中央有关精神...
# 科技
### [基于detectron2的DETR实现](https://www.ctolib.com/poodarchu-DETR-detectron2.html)
> 概要: 基于detectron2的DETR实现
### [./jqview - 用jq检查JSON对象的最简单的原生GUI](https://www.ctolib.com/fiatjaf-jqview.html)
> 概要: ./jqview - 用jq检查JSON对象的最简单的原生GUI
### [Google2Csv是一个简单的google scraper，它将结果保存在csv/xlsx/jsonl文件中](https://www.ctolib.com/psalias2006-Google2Csv.html)
> 概要: Google2Csv是一个简单的google scraper，它将结果保存在csv/xlsx/jsonl文件中
### [OnlineOJ Android 端项目源码](https://www.ctolib.com/SG-XM-OnlineOJ-Android.html)
> 概要: OnlineOJ Android 端项目源码
### [A simple GUI Notepad using C++ natively made in my high school in 2015](https://www.tuicool.com/articles/UZJvmqJ)
> 概要: CNotepadA GUI Notepad completely made from scratch using C++ native library and Graphics library cou......
### [编写 React 组件时常见的 5 个错误](https://www.tuicool.com/articles/FNfiUbj)
> 概要: 本文最初发布于 lorenzweiss.de 网站，经原作者授权由 InfoQ 中文站翻译并分享。React 框架React 在 Web 开发领域已经资格不浅了，近年来它作为敏捷 Web 开发工具的角......
### [NASH：基于丰富网络态射和爬山算法的神经网络架构搜索 | ICLR 2018](https://www.tuicool.com/articles/iI3AfaB)
> 概要: 论文提出NASH方法来进行神经网络结构搜索，核心思想与之前的EAS方法类似，使用网络态射来生成一系列效果一致且继承权重的复杂子网，本文的网络态射更丰富，而且仅需要简单的爬山算法辅助就可以完成搜索，耗时......
### [数据不够，Waymo用GAN来凑：生成逼真相机图像，在仿真环境中训练无人车模型](https://www.tuicool.com/articles/mMviEfJ)
> 概要: 华人实习生一作鱼羊 发自 凹非寺量子位 报道 | 公众号 QbitAI疫情当下，Waymo等自动驾驶厂商暂时不能在现实世界的公共道路上进行训练、测试了。不过，工程师们还可以在GTA（划掉），啊不，在仿......
### [比亚迪：将加快推进比亚迪半导体分拆上市](https://www.ithome.com/0/492/744.htm)
> 概要: IT之家6 月 15 日消息  据中证网报道，6 月 15 日，比亚迪（002594）在投资者关系活动记录表中披露，在公司市场化发展的战略布局下，比亚迪半导体作为中国最大的车规级 IGBT 厂商，仅用......
### [台积电拟 120 亿美元美国建厂，新工厂或被命名为晶圆二十厂](https://www.ithome.com/0/492/726.htm)
> 概要: 据国外媒体报道，为苹果等公司代工芯片的台积电，5 月 15 日就已在官网宣布他们拟在美国亚利桑那州建设芯片生产工厂，采用 5nm 工艺为相关客户代工芯片，从 2019 年到 2021 年，台积电计划在......
### [曝三星入门机 M41 将采用小米 10 同款 TCL 华星 AMOLED 屏幕](https://www.ithome.com/0/492/723.htm)
> 概要: IT之家6 月 15 日消息  外媒 SamMobile 报道，三星 Galaxy M41 手机是一款廉价智能手机。尽管这是一款入门级产品，但现在看来该设备将成为三星的重要里程碑。也就是说 Galax......
### [中国嫦娥四号着陆器、玉兔二号月球车自主唤醒进入第十九月昼工作期](https://www.ithome.com/0/492/733.htm)
> 概要: IT之家6 月 15 日消息  据中国探月工程消息，6 月 15 日，嫦娥四号着陆器和 “玉兔二号”月球车分别于 13 时 49 分和 0 时 54 分，结束了寒冷且漫长的月夜休眠，受光照自主唤醒，进......
# 小说
### [三五成趣](http://book.zongheng.com/book/974195.html)
> 作者：青豆和酒

> 标签：二次元

> 简介：暂无简介

> 章节末：第七十章  346——350

> 状态：完本
# 游戏
### [周一内涵囧图云飞系列 你这样按摩我会把持不住的](https://www.3dmgame.com/bagua/3260.html)
> 概要: 今天是周一，一起来欣赏下云飞系列的新内涵囧图。同样是摆摊，她们的生意总是比我好，为什么？你以为他在刷二维码吗？其实他在拍裙底！妹子你这样按摩，我会把持不住的。妹子把内衣穿出了灵魂，不信？看图就知道了......
### [IGN游戏之夏：《Gori: Cuddly Carnage》预告公开](https://www.3dmgame.com/news/202006/3790927.html)
> 概要: 在IGN游戏之夏直播活动上，开发商兼发行商Angry Demon Studio公开了《Gori: Cuddly Carnage》一段预告片，一起来了解一下。IGN游戏之夏《Gori: Cuddly C......
### [《生化危机8》没有PS4/X1版原因：性能不够加载过慢](https://www.3dmgame.com/news/202006/3790950.html)
> 概要: 《生化危机8》将于2021年登陆PS5、Xbox Series X和PC平台。此前还有消息称卡普空准备让《生化危机8》登陆本世代PS4和Xbox One平台，但是据之前精准爆料《生化危机8》的Dusk......
### [IGN游戏之夏：虐狗游戏《HAVEN》5分钟演示](https://www.3dmgame.com/news/202006/3790908.html)
> 概要: 在IGN游戏之夏直播活动上，合作向虐狗游戏《Haven》公开了5分钟游玩演示，本作将于年内登陆Steam、XboxOne、PS4、Switch平台，同时本作也将首发加入XGP。IGN游戏之夏《HAVE......
### [IGN游戏之夏：恐怖游戏《午夜捉鬼人》预告公开](https://www.3dmgame.com/news/202006/3790918.html)
> 概要: 在IGN游戏之夏直播活动上，《Midnight Ghost Hunt》（暂译为《午夜捉鬼人》）一段预告片已经公开，该作将登陆Steam平台，以下为官方公开的相关介绍内容。IGN游戏之夏《午夜捉鬼人》预......
# 论文
### [Focus-Enhanced Scene Text Recognition with Deformable Convolutions](https://paperswithcode.com/paper/focus-enhanced-scene-text-recognition-with)
> 日期：29 Aug 2019

> 标签：SCENE TEXT RECOGNITION

> 代码：https://github.com/Alpaca07/dtr

> 描述：Recently, scene text recognition methods based on deep learning have sprung up in computer vision area. The existing methods achieved great performances, but the recognition of irregular text is still challenging due to the various shapes and distorted patterns.
