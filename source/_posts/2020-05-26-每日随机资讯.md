---
title: 2020-05-26-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.CheetahCubs_EN-CN7984830771_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2020-05-26 21:23:21
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.CheetahCubs_EN-CN7984830771_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [视频：何鸿燊家人在医院见记者 女儿：家人有共识 集体处理后事](https://video.sina.com.cn/p/ent/2020-05-26/detail-iircuyvi5144445.d.html)
> 概要: 视频：何鸿燊家人在医院见记者 女儿：家人有共识 集体处理后事
### [视频：港媒曝赌王何鸿燊病逝 享年98岁](https://video.sina.com.cn/p/ent/2020-05-26/detail-iircuyvi5102032.d.html)
> 概要: 视频：港媒曝赌王何鸿燊病逝 享年98岁
### [潘玮柏同框蔡依林杨丞琳 罗志祥缺席却悄悄点赞](https://ent.sina.com.cn/y/ygangtai/2020-05-26/doc-iircuyvi5044005.shtml)
> 概要: 新浪娱乐讯 据台媒报道，潘玮柏日前自掏腰包，请蔡依林、杨丞琳吃饭，3人难得同框，让网友超惊喜，不过聚会独缺罗志祥（小猪），引发热议，还意外钓出隔离期满刚出关的罗志祥。　　潘玮柏与蔡依林、杨丞琳合体，引......
### [组图：蔡依林手拿盆栽小摆件玩自拍 认真为娃娃布置新家乐趣足](http://slide.ent.sina.com.cn/y/w/slide_4_704_339342.html)
> 概要: 组图：蔡依林手拿盆栽小摆件玩自拍 认真为娃娃布置新家乐趣足
### [何猷君曾为奚梦瑶怼网友 爆料家产早就分配好](https://ent.sina.com.cn/s/h/2020-05-26/doc-iirczymk3646608.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，赌王何鸿燊26日早上一家人都到医院聚集，最后在下午1点5分病逝，享年98岁。而过去曾在何猷君宣布迎来何家第五代长孙消息，却被酸“又能分到一大笔遗产”，当时他爆气回应中就透露......
### [王思聪又换新女友！与网红闺蜜深夜牵手逛街，女方全程很主动](https://new.qq.com/omn/20200526/20200526A066LZ00.html)
> 概要: 王思聪又换新女友！与网红闺蜜深夜牵手逛街，女方全程很主动
### [女星防晒好拼！赵露思开拍前一刻才把遮阳伞给助理，难怪白到发光](https://new.qq.com/omn/20200526/20200526V071GU00.html)
> 概要: 女星防晒好拼！赵露思开拍前一刻才把遮阳伞给助理，难怪白到发光
### [刘敏涛“儿子”翻唱《红色高跟鞋》，表情模仿到位，小眼神魔性十足](https://new.qq.com/omn/20200526/20200526V06IC900.html)
> 概要: 刘敏涛“儿子”翻唱《红色高跟鞋》，表情模仿到位，小眼神魔性十足
### [《冰雨火》正式开机，王一博寸头造型曝光，和陈晓站位显局促](https://new.qq.com/omn/20200526/20200526A07G9T00.html)
> 概要: 《冰雨火》正式开机，王一博寸头造型曝光，和陈晓站位显局促
### [《月上重火》将播，陈钰琪新剧开机，搭档男主比罗云熙更受欢迎](https://new.qq.com/omn/20200526/20200526A05INI00.html)
> 概要: 《月上重火》将播，陈钰琪新剧开机，搭档男主比罗云熙更受欢迎
# 动漫
### [游戏王历史：从零开始的游戏王环境之旅第四期18](https://news.dmzj.com/article/67488.html)
> 概要: 随着“电子龙”等新战力的加入，当时的环境被一分为二、成为【变异Chaos】和【科学怪人齿轮】两大势力的纷争之地。姑且还有一些人在使用像【弹压王虎】之类的Meta牌组，但由于多种理由让它们没有实力去竞争，落到了一个比较痛苦的田地。环境来到了二强争霸...
### [游戏《魔法师和黑猫妖》的活动“黄昏Mareless”漫画化](https://news.dmzj.com/article/67484.html)
> 概要: COLOPL运营的游戏《猜谜RPG魔法师和黑猫妖》内的活动“黄昏Mareless”开始了漫画版《黄昏Mareless-魔法师和黑猫妖 Chronicle-》的连载。
### [美国世嘉推出索尼克周边因被指类似旭日旗而下架](https://news.dmzj.com/article/67482.html)
> 概要: 美国世嘉的官方网店中，因推出了一款类似旭日旗图案的服装而引发了不少韩国网友的不满。很多网友支持，这样的周边会让人想起旧日本军的残虐行为。
### [抄袭or致敬？充满争议的《时间悖论代笔人》](https://news.dmzj.com/article/67483.html)
> 概要: 近年来越来越多的人吐槽JUMP上的漫画一年不如一年。而最近，JUMP上新连载的漫画《时间悖论代笔人》又引发了激烈的讨论。
### [佩恩的“地爆天星”手办，有九尾价值两万，没九尾只值一千](https://new.qq.com/omn/20200526/20200526A04LHN00.html)
> 概要: 凡是看过火影忍者的漫迷，肯定对佩恩入侵木叶一战印象深刻，这是作品中的巅峰之战，其影响力甚至超过了第四次忍界大战，被称为“佩恩之后无火影”。战斗的主角佩恩天道有三大绝招，神罗天征，万象天引和地爆天星。……
### [fate：是英雄也是普通人，此身为剑所天成](https://new.qq.com/omn/20200526/20200526A07CWQ00.html)
> 概要: 自从FSN之后，我个人对FATE这个系列就有了深深的兴趣，还记得本人是16年看的Fate，也是那个时候开始入坑的Fate，在时间上其实没有很多大佬们入的早。但是这不影响我对红A的见解与看法，哈哈。其……
### [埃罗芒阿老师中的乱入：刀剑神域、魔禁、龙与虎等都有出现](https://new.qq.com/omn/20200526/20200526A0A52K00.html)
> 概要: 在一些动漫作品中，往往会出现其他作品的角色，或者提到某一部作品。而每当这样情节出现的时候，也往往会对已经看过这部作品的漫迷，有着一种特别乐趣的存在。在某种意义上来说，看过的动漫作品越多，遇到动漫作品……
### [当声优cos自己配音的角色，阿梓喵还原100％，最美还是祢豆子](https://new.qq.com/omn/20200525/20200525A0KSK500.html)
> 概要: 声优是塑造二次元角色背后的功臣，他们大多是因为自己的兴趣而进入这个行业的，所以也算是个理想主义者。声优的专业能力大多都是通过专业学习和实践经验进行一点一滴地积累，所以很多声优在其职业生涯中也会塑造很……
### [泽塔奥特曼新武器曝光：名为泽塔枪弓，真不愧是赛罗徒弟！](https://new.qq.com/omn/20200526/20200526A08ATZ00.html)
> 概要: 泽塔奥特曼将会在6月26日正式上映，之前的时候我们就已经看到了泽塔奥特曼的三大强化形态以及小陆的回归。这次的泽塔奥特曼大概率会延续了捷德奥特曼的方式让泽塔和小陆并肩作战，同时在设定上则是采用了艾克斯……
### [灌篮高手：神奈川全明星对阵山王工业，全方位分析，替补才是关键](https://new.qq.com/omn/20200526/20200526A07URF00.html)
> 概要: 神奈川全明星对阵山王工业，全方位分析，替补才是关键。山王工业是全国大赛的霸主，也是灌篮高手动漫最终的大BOSS ，是其他队伍始终无法逾越的大山，但是湘北依靠着自己强大的主角光环，愣是将全国大赛的霸主……
# 财经
### [全国政协委员施大宁：完善产教融合、校企协同育人机制](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iirczymk3691178.shtml)
> 概要: 原标题：全国政协委员施大宁：完善产教融合、校企协同育人机制 “只有把学校、社会、政府、企业四者联合起来，才能培养出适应社会发展需要、面向未来的人才”。
### [李子颖：尽快摸清国内铀资源家底 推进天然铀产能建设](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iircuyvi5167740.shtml)
> 概要: 原标题：李子颖委员：尽快摸清国内铀资源家底，推进天然铀产能建设 安全稳定的铀资源供应，是核工业发展的前提与基础。“铀资源之于核工业，重要性好比粮食之于人类。
### [十三届全国人大三次会议闭幕后 国务院总理李克强将出席记者会](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iirczymk3697267.shtml)
> 概要: 中新网5月26日电 据中国人大网消息，5月28日下午十三届全国人大三次会议闭幕后，国务院总理李克强将在人民大会堂三楼金色大厅出席记者会并回答中外记者提问。
### [全国人大代表刘贵芳：建议农村居家养老服务纳入医保报销](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iircuyvi5166120.shtml)
> 概要: 原标题：全国人大代表刘贵芳：建议农村居家养老服务纳入医保报销 新京报讯（记者 李傲）全国“两会”期间，全国人大代表、河北省广平县南阳堡镇乡村医生刘贵芳建议...
### [两部门：对疫情防控期间执飞不载客国际货运航班给予奖励](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iircuyvi5172909.shtml)
> 概要: 原标题：两部门：对疫情防控期间执飞不载客国际货运航班给予奖励 新京报快讯据财政部网站5月26日消息，财政部、民航局发布关于对民航运输企业在疫情防控期间稳定和提升国际...
### [深圳宝安区住建局：所有住宅新盘采取抽签方式选房销售](https://finance.sina.com.cn/china/gncj/2020-05-26/doc-iirczymk3691021.shtml)
> 概要: 原标题：宝安区住建局：所有住宅新盘采取抽签方式选房销售 连日来，针对网友和群众反映的宝安区个别地产项目存在捂盘惜售、房源内定等情况...
# 科技
### [Tile Editor 一个在浏览器上运行的2D Tile编辑器](https://www.ctolib.com/victorqribeiro-tileEditor.html)
> 概要: Tile Editor 一个在浏览器上运行的2D Tile编辑器
### [Decorator是一个Android库，它可以帮助在RecyclerViews中创建可合成的页边距和分隔线](https://www.ctolib.com/cabriole-Decorator.html)
> 概要: Decorator是一个Android库，它可以帮助在RecyclerViews中创建可合成的页边距和分隔线
### [KongQi Laravel admin 集成了，图片上传，多图上传，批量Excel导入，批量插入，修改，添加，搜索，权限管理RBAC](https://www.ctolib.com/kong-qi-kongqi_laravel_admin2.html)
> 概要: KongQi Laravel admin 集成了，图片上传，多图上传，批量Excel导入，批量插入，修改，添加，搜索，权限管理RBAC
### [这是一款性冷淡风兼着中国风的typora主题](https://www.ctolib.com/luokangyuan-typora-theme-chineseStyle.html)
> 概要: 这是一款性冷淡风兼着中国风的typora主题
### [从0设计App（5）：2套方法绘制业务、页面流程图（下）](https://www.tuicool.com/articles/QNFraun)
> 概要: 至此，我们完成了app的宏观定位、系统架构、V1.0.0的产品演进蓝图以及粗线条的产品结构图。接下来本文将围绕appV1.0.0的具体功能流程和页面流程展开。惯例，先回顾一下。在系统架构/产品结构（中......
### [如何使用git命令行上传项目到github](https://www.tuicool.com/articles/YNFN3q6)
> 概要: 第一步：我们需要先创建一个本地的版本库（其实也就是一个文件夹）。你可以直接右击新建文件夹，也可以右击打开Git bash命令行窗口通过命令来创建。现在我通过命令行在桌面新建一个TEST文件夹（你也可以......
### [利用Podman 和 Buildah 构建容器镜像](https://www.tuicool.com/articles/BVRvaeq)
> 概要: 这是有关构建容器镜像的一系列博客文章中的第二篇。该系列从“未来我们如何构建容器镜像？”开始。该文章探讨了自Docker首次发布以来构建镜像的变化以及如何克服使用Dockerfile的诸多限制。这篇文章......
### [使用Chainlink预言机，十分钟开发一个DeFi项目](https://www.tuicool.com/articles/JF3q22I)
> 概要: Chainlink价格参考数据合约是可以在智能合约网络中值得依赖的真实价格数据的链上参考点。这些合约由多个Chainlink节点定时更新，提供高精度，高频率，可定制化的DeFi价格参考数据，可以方便的......
### [为机器人拨穗、建虚拟校园、Switch 联机...别人家的学校都是这样“云毕业”的](https://www.ithome.com/0/489/336.htm)
> 概要: 一场疫情，让不少高校的毕业典礼或延期或取消。但也有一些高校极具仪式感——给机器人拨穗、在游戏中还原校园、用视频会议 APP 云连线、Switch 动森联机，毕业典礼既硬核又有趣。硬核南邮，在线拨穗20......
### [【IT之家评测室】realme真我X50m上手：双模5G、120Hz高刷性价比手机](https://www.ithome.com/0/489/279.htm)
> 概要: 全球成长最快的智能手机品牌realme，早在4月份便悄然上架了5G新机X50m，这是realme在今年的又一款5G新机，新机X50m定位比今年1月份老早发布的X50稍低，其中6GB+128GB售价20......
### [台电推出 P20HD 安卓平板：8核虎贲+FHD屏，699元](https://www.ithome.com/0/489/311.htm)
> 概要: IT之家5月26日消息 根据台电官方的消息，台电正式推出10.1英寸全高清平板——台电P20HD，预约到手价699元。IT之家了解到，台电P20HD采用紫光虎贲SC9863A八核处理器，具有AI神经网......
### [Redmi 10X 4G版发布：全球首发Helio G85，售价999元起](https://www.ithome.com/0/489/276.htm)
> 概要: IT之家5月26日消息 除了Redmi 10X 5G版之外，Redmi今天还发布了Redmi 10X 4G版，该机全球首发Helio G85游戏处理器，售价999元起。Redmi 10X 4G版采用6......
# 小说
### [这个日式物语不太冷](https://m.qidian.com/book/1015451851/catalog)
> 作者：和风遇月

> 标签：原生幻想

> 简介：白凡面前有一块等身镜。镜中的白凡冲着自己在大大地笑着。很奇怪——因为他根本就没有笑。在白凡的目光下，镜中自己的嘴角咧到脖子根。就算是一直镇静的白凡也露出一抹讶然之色。看着对方对着自己的脑袋张开血盆大口，白凡缓缓地抬起自己手中的手机——咔擦一声。他比了个剪刀手，拍了张合照。————全订书友群：827854776，全订即可进~普通书友群：883892482，暗号北川绘里

> 章节总数：共534章

> 状态：完本
# 游戏
### [除了烧脑 诺兰还在《信条》里真的炸了一架飞机](https://www.3dmgame.com/news/202005/3789386.html)
> 概要: 近日，诺兰的烧脑新片《信条》（Tenet）公布了第二支正式预告，此外主演约翰·大卫·华盛顿也表示，《信条》确实很烧脑，自己也不是理解得很透彻，每天也要请教诺兰，但是除了电影烧脑，和往常一样，诺兰也在电......
### [德国版”怪奇物语“《暗黑》第3季首曝先导预告](https://www.3dmgame.com/news/202005/3789408.html)
> 概要: Netflix高分神剧德国版“怪奇物语”《暗黑》第三季同时也是最终季今日（5月26日）公布了首支预告，本剧将于6月27日正式回归，在Netflix首播。《暗黑》第3季先导预告：《暗黑》讲述了一个超自然......
### [《漫威钢铁侠VR》幕后花絮 打造钢铁侠原创宇宙](https://www.3dmgame.com/news/202005/3789398.html)
> 概要: PlayStation®VR《漫威钢铁侠VR》同捆包以及 PlayStation®Move 同捆包即将于 2020 年 7 月 3 日推出，官方于今日公开VR沉浸效果幕后花絮。 深入访谈开发团队，了解......
### [《赛博朋克2077》限量版Xbox主机6月8日在日本发售](https://www.3dmgame.com/news/202005/3789365.html)
> 概要: 近日日本微软宣布，《赛博朋克2077》限量版Xbox One X主机将于6月8日在日本地区发售，售价39980日元(约合人民币2649元)。之前官方介绍称这是最后一款限定Xbox One X主机量产型......
### [漫画家万乘大智患上癌症 曾创作《高达 Aggressor》](https://www.3dmgame.com/news/202005/3789367.html)
> 概要: 曾创作连载《机动战士高达 Aggressor》的日本漫画家万乘大智近日发推表示自己患上癌症，而且必须要尽快做手术，一起来了解下。·万乘大智是日本众多普通漫画家中的一员，代表作包括《DANDOH》、《机......
# 论文
### [Modeling Extent-of-Texture Information for Ground Terrain Recognition](https://paperswithcode.com/paper/modeling-extent-of-texture-information-for)
> 日期：17 Apr 2020

> 标签：IMAGE CLASSIFICATION

> 代码：https://github.com/ShuvozitGhose/Ground-Terrain-EoT

> 描述：Ground Terrain Recognition is a difficult task as the context information varies significantly over the regions of a ground terrain image. In this paper, we propose a novel approach towards ground-terrain recognition via modeling the Extent-of-Texture information to establish a balance between the order-less texture component and ordered-spatial information locally.
