---
title: 2023-01-06-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BlackFell_ZH-CN9224189688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50
date: 2023-01-06 22:18:38
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BlackFell_ZH-CN9224189688_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=50)
# 新闻
### [长视频平台涨价上瘾？关键在于建立内容护城河](https://www.woshipm.com/it/5718306.html)
> 概要: 近期，爱奇艺宣布会员费涨价一事引发了许多网友们的关注和讨论，而在会员费涨价这件事情的背后，可能隐藏的是长视频平台们在盈利发展、内容商业化上的隐忧。具体如何解读？不如一起来看看作者的拆解与分析。会员涨价......
### [如何设计文化遗产数字化领域的 C 端产品？](https://www.woshipm.com/pd/5724112.html)
> 概要: 在设计一款C端产品时，业务团队需要事先了解你的目标用户与实际使用场景，并搭建好后续的体验设计与运营推广策略。那么具体可以如何操作呢？本篇文章里，作者结合具体产品进行了设计拆解，不妨来看一下吧。我将文化......
### [视频号崛起，商家如何从中获利？](https://www.woshipm.com/it/5724217.html)
> 概要: 近年来，视频号的崛起之路，我们都看在眼里。与抖音相比，视频号的突围，能否带动商家获利？本文梳理了2022年视频号的关键进展，并展望2023年的可能性，一起来看看吧。2022年，低调哑火的电商江湖里，如......
### [XBB.1.5来了，最可怕的是什么？](https://www.huxiu.com/article/761913.html)
> 概要: 出品 | 虎嗅医疗组作者 | 陈广晶编辑 | 陈伊凡头图 | 视觉中国XBB.1.5引起了世界卫生组织（WHO）的关注。根据1月5日中新网消息称，WHO新冠疫情应对技术主管范克尔克霍夫公开表示，XBB......
### [该轮到腾讯收割了](https://www.huxiu.com/article/758715.html)
> 概要: 出品｜虎嗅商业消费组作者｜黄青春题图｜视觉中国“腾讯命门在游戏，年末版号（ 2022 年 12 月 28 核发的版号）总算让腾讯游戏缓过来了。”一位券商分析师向虎嗅表示，版号这一最大困境得到改善之后，......
### [哪吒汽车估值缩水超百亿  上调部分车型售价能否止住销量下滑？](https://finance.sina.com.cn/tech/roll/2023-01-06/doc-imxzewyt5715980.shtml)
> 概要: 《投资者网》葛凡梅......
### [呼吸机和血氧仪单价至少翻番  但大部分上市公司市值近期却在下滑](https://finance.sina.com.cn/tech/roll/2023-01-06/doc-imxzewyt5716009.shtml)
> 概要: 《投资者网》蔡俊......
### [36氪首发 | 「LEGEND ENERGY 乐驾能源」完成由愉悦资本领投的新一轮融资，推进用户侧储能智慧能源生态建设](https://36kr.com/p/2070443861933193)
> 概要: 36氪首发 | 「LEGEND ENERGY 乐驾能源」完成由愉悦资本领投的新一轮融资，推进用户侧储能智慧能源生态建设-36氪
### [市值缩水一个推特，回归地球的特斯拉还有市梦率光环吗？](https://finance.sina.com.cn/tech/it/2023-01-06/doc-imxzfchi5708176.shtml)
> 概要: 经历了黑暗的2022年之后，特斯拉股价在新年伊始就挨了一记闷棒......
### [快速提升战力！《魔域手游2》新手必看攻略](https://shouyou.3dmgame.com/news/65328.html)
> 概要: 《魔域手游2》巅峰测试现已正式开服，第一天服务器也经历了数次爆满，玩家热情相当高涨！开服第一天也涌进了很多新玩家，这些初入雷鸣大陆的神选者也在公屏中纷纷询问到底应该怎么提升战力，那么今天就为大家介绍一......
### [FTC拟新规 禁止美国雇佣合同含有竞业禁止条款](https://www.3dmgame.com/news/202301/3859917.html)
> 概要: 美国联邦贸易委员会（FTC）今天提出了一项新规定，禁止美国雇主在与员工或工人签订的合同中使用竞业禁止条款。竞业禁止，又称竞业避让，是指公司通过劳动合同和保密协议禁止劳动者在任职期间，或离职后一段时间内......
### [全球第三大酒店集团，如何“玩转”数字化｜数智化的秘密](https://36kr.com/p/2075061122153603)
> 概要: 全球第三大酒店集团，如何“玩转”数字化｜数智化的秘密-36氪
### [主机厂价格“卷”至冰点，风电下半场拼的是什么？｜数智前瞻·全球技术图谱](https://36kr.com/p/2075046892878983)
> 概要: 主机厂价格“卷”至冰点，风电下半场拼的是什么？｜数智前瞻·全球技术图谱-36氪
### [《九州缥缈录》动画定档预告：1.24开播、每周二更新](https://acg.gamersky.com/news/202301/1554284.shtml)
> 概要: 《九州缥缈录》动画官宣定档1月24日，每周二上午10:00更新。
### [N号房主犯曾跟踪调查金智秀 称其晚上连灯都不开](https://ent.sina.com.cn/y/yrihan/2023-01-06/doc-imxzfiqf5602023.shtml)
> 概要: 新浪娱乐讯 1月6日，韩国MBC电视台新闻曝光“N号房”主犯赵主彬行为，曾雇佣私人调查所跟踪调查BLACKPINK金智秀私生活。　　“N号房”主犯赵主彬曾经登陆行政系统调查女团个人信息，雇佣四人调查机......
### [科箭荣获“2022 LOG最具创新力供应链&物流科技企业”](http://www.investorscn.com/2023/01/06/105247/)
> 概要: 摘要：科箭凭借“Power Data Viewer供应链数据可视大屏”在产品技术和落地应用上的成功实践，荣获“2022 LOG最具创新力供应链&物流科技企业”称号，并被收录于《2023 LOG中国供应......
### [科箭运输管理TMS云助力博多打造数字化运输管理平台](http://www.investorscn.com/2023/01/06/105248/)
> 概要: “以供应链主导，实现产业多向延展”，是对博多控股集团（以下简称为“”博多）22年来发展脉络最直观的写照。2000年，博多从植脂末工厂起家，围绕茶饮供应链上游深耕；2007年凭借上游优势，博多试水茶饮赛......
### [P站美图推荐——束腰特辑（三）](https://news.dmzj.com/article/76763.html)
> 概要: 华丽的装饰，更能衬托纤细的腰肢和美丽的曲线。
### [上美宝刀未老！动画《中国奇谭》B站播放量破2000万](https://acg.gamersky.com/news/202301/1554358.shtml)
> 概要: 《中国奇谭》官博宣布，该动画B站播放量已突破2000万，感谢大家的支持和鼓励。
### [梦幻联动！王以太晒与赵丽颖比耶合照](https://ent.sina.com.cn/s/m/2023-01-06/doc-imxzfiqi2398598.shtml)
> 概要: 新浪娱乐讯 1月6日，王以太晒出一张与赵丽颖的合照，照片中的赵丽颖身穿红色丝绒上衣，气色极好，两个人对着镜头比耶，引发粉丝期待。(责编：小5)......
### [《我在终点线等你》steam页面  第一季度发售](https://www.3dmgame.com/news/202301/3859937.html)
> 概要: 今日（1月6日），国产AVG《我在终点线等你》Steam页面上线，游戏试玩Demo现已上线，预计于第一季度发售，感兴趣的玩家可以点击此处进入商店页面。游戏介绍：不敢说出自己赛车手梦想的少年夏涵，攒钱两......
### [23万元的特斯拉，治不好马斯克的焦虑](https://www.huxiu.com/article/762539.html)
> 概要: 出品 | 虎嗅汽车组作者 | 王笑渔编辑 | 周到头图 | 视觉中国2023年，大佬们的焦虑并没有缓解迹象。1月的第一周。先是蔚来李斌发布内部信痛批公司八大问题，后是小米雷军发表内部讲话给造车团队打鸡......
### [中国烟花，还有未来吗？](https://www.huxiu.com/article/762591.html)
> 概要: 本文来自微信公众号：巨潮WAVE（ID：WAVE-BIZ），作者：老鱼儿，编辑：杨旭然，原文标题：《烟花也是经济｜巨潮》，题图来自：视觉中国元旦前夕，一则2023跨年的烟花秀预告，让河北省石家庄市的人......
### [《赛博朋克2077》拿爱的付出奖引玩家不满：全靠动画](https://www.3dmgame.com/news/202301/3859950.html)
> 概要: 近日，Steam大奖公布了获奖名单，《赛博朋克2077》在拿下Steam爱的付出奖后引起部分的玩家不满，Steam评测在短时间内收到了大量差评，玩家们认为《赛博朋克2077》2022年什么也没做，能拿......
### [DMM GAMES的创作团队设立新工作室 长谷川雄大任社长](https://news.dmzj.com/article/76770.html)
> 概要: EXNOA宣布了于2022年9月6日，设立DMM集团的100%子公司Studio KUMASAN的消息。该公司由领导制作团队KUMASAN的长谷川雄大任社长。
### [海外new things | 车载技术初创「Holoride」将在奥迪部分型号车辆上搭载VR娱乐设备，加入多款热门新游戏](https://36kr.com/p/2075408095990022)
> 概要: 海外new things | 车载技术初创「Holoride」将在奥迪部分型号车辆上搭载VR娱乐设备，加入多款热门新游戏-36氪
### [阔诺新连载哒！1月新连载漫画不完全指北第一期](https://news.dmzj.com/article/76771.html)
> 概要: 阴角男子竟遇天使恶魔同时求爱？！搬到破烂公寓隔壁的竟是成为国民偶像的青梅竹马？！出现在失忆的她面前的四个男人，究竟哪个才是她选中的未婚夫？！请看本周指北！
### [LG电子去年Q4利润仅655亿韩元 暴跌 91.2%](https://www.3dmgame.com/news/202301/3859958.html)
> 概要: IT之家今日（1月6日）消息，据韩联社，由于全球经济衰退导致需求萎缩，LG电子2022年Q4陷入盈利冲击，其主营业务，如电视和家电，整体不景气。初步计算显示，LG电子2022年Q4营业利润为655亿韩......
### [《三体》动画导演疑似骂人 曾批刘慈欣文字没有连贯性](https://acg.gamersky.com/news/202301/1554580.shtml)
> 概要: 《三体》动画导演骂人。
### [化解地方隐性债务风险，财政部重申：中央不救、谁的孩子谁抱](https://www.yicai.com/news/101643091.html)
> 概要: 为了避免道德风险，真正促成地方理性举债，财政部近年来反复喊话地方，地方债中央坚持不救助，并打破金融机构的政府兜底幻觉。
### [只要胆子大，小丑放产假！你能接受这样的剧情吗？](https://news.dmzj.com/article/76772.html)
> 概要: 今天一条魔幻的消息令DC读者们无不震惊：在DC漫画《小丑：止笑之人》中，著名反派角色小丑居然怀孕了？
### [消息称华为 P60 系列主摄比前代更大，有望首发 IMX8 系列新品](https://www.ithome.com/0/666/105.htm)
> 概要: 感谢IT之家网友肖战割割的线索投递！IT之家1 月 6 日消息，数码博主 @数码闲聊站 今日透露，华为 P60 系列正在研发的工程机采用了索尼新一代 IMX7xx+IMX8xx 大底传感器，而且都是 ......
### [视频：杨超越敦煌飞天神女造型古韵十足 锦衣彩带迎风起舞超抢眼](https://video.sina.com.cn/p/ent/2023-01-06/detail-imxzfync8902377.d.html)
> 概要: 视频：杨超越敦煌飞天神女造型古韵十足 锦衣彩带迎风起舞超抢眼
### [佳贝艾特联合专业机构发布《婴幼儿喂养状况白皮书》，专家呼吁关注喂养效果](http://www.investorscn.com/2023/01/06/105262/)
> 概要: 近日，中国营养学会2022年妇幼营养学术年会在南京召开。会上，中国营养学会妇幼营养分会与佳贝艾特联合发布了《中国婴幼儿喂养状况白皮书》（下称《白皮书》）。妇幼营养分会主委、课题组组长汪之顼教授带头对《......
### [“博迪科技-江南大学联合实验室”成立，品质安全再升级](http://www.investorscn.com/2023/01/06/105264/)
> 概要: 近日，雾化科技龙头企业博迪科技携手江南大学签署协议联合创建“博迪科技-江南大学联合实验室，协议双方将充分利用材料科学、食品化学、生物医药等交叉学科技术，开展电子雾化基础科学、安全技术创新及降害减害等领......
### [徐州市开展卫生应急全员网络培训](http://www.investorscn.com/2023/01/06/105265/)
> 概要: 为进一步提升全市卫生应急管理人员、各级医疗卫生人员卫生应急理论水平，增强突发公共卫生事件、突发事件紧急医疗救援应急处置能力，市卫生健康委研究决定，在全市开展2022年度卫生应急全员网络培训工作。依托华......
### [指数重心不断上移 下周能否向上挑战3200？](https://www.yicai.com/video/101643229.html)
> 概要: 指数重心不断上移 下周能否向上挑战3200？
### [“贪官”冯鹤年被移送司法，证监系统一年之内“五虎”落马](https://www.yicai.com/news/101643209.html)
> 概要: 落马“五虎”，发行腐败是重灾区
### [《赛博朋克 2077》诉讼完结，CDPR 向投资者赔偿 185 万美元](https://www.ithome.com/0/666/129.htm)
> 概要: IT之家1 月 6 日消息，由于《赛博朋克 2077》主机版发售之初存在太多 Bug 导致玩家退款，CDPR 曾被投资者团体提起集体诉讼，投资者认为他们隐瞒了《赛博朋克 2077》发售状态从而导致自己......
### [百度李彦宏谈 2023 年：把技术变成市场需要，就是机会](https://www.ithome.com/0/666/130.htm)
> 概要: 1 月 6 日消息，在 2022 年的末尾，百度创始人、董事长兼 CEO 李彦宏在全员会上用一个小时的时间，回顾 2022、展望 2023。李彦宏告诉全体员工：“面对挑战，要抢机会、讲创新。”李彦宏在......
### [新冠mRNA二价疫苗香港自费市场开打，“阳康”后如何接种](https://www.yicai.com/news/101643278.html)
> 概要: 香港卫生署卫生防护中心官网显示显示，“阳康”后3到6个月可接种复必泰二价疫苗作为加强针使用，特殊人群可于康复后28天接种。
### [齐景发和宋立新为通威集团董事局主席刘汉元开启2022中国经济年度人物奖项](https://finance.sina.com.cn/china/2023-01-06/doc-imxzhetx1966455.shtml)
> 概要: 被誉为中国经济奥斯卡大奖、“中国梦杯•中国经济新闻人物——2022十大经济年度人物”颁奖盛典于2023年1月6日在北京正大中心4层正式举办。
### [通威集团刘汉元：中国光伏产业全面领先全球，欧美日韩使用的光伏70%由中国制造，中国制造占全球光伏产量75%](https://finance.sina.com.cn/china/2023-01-06/doc-imxzheuc5731049.shtml)
> 概要: 被誉为中国经济奥斯卡大奖、“中国梦杯•中国经济新闻人物——2022十大经济年度人物”颁奖盛典于2023年1月6日在北京正大中心4层正式举办。
### [山东能源集团党委书记、董事长李伟当选“2022中国经济年度人物”](https://finance.sina.com.cn/china/2023-01-06/doc-imxzhetz8953660.shtml)
> 概要: 被誉为中国经济奥斯卡大奖、“中国梦杯•中国经济新闻人物——2022十大经济年度人物”颁奖盛典于2023年1月6日在北京正大中心4层正式举办。
### [给你5000元如何创业成功？通威集团刘汉元：看清楚自己的路径，想明白怎么样投资才有可能领先](https://finance.sina.com.cn/china/2023-01-06/doc-imxzhetx1971632.shtml)
> 概要: 被誉为中国经济奥斯卡大奖、“中国梦杯•中国经济新闻人物——2022十大经济年度人物”颁奖盛典于2023年1月6日在北京正大中心4层正式举办。
### [2023年首周5只基金按下“终止键”，近千只基金濒临清盘红线](https://www.yicai.com/news/101643341.html)
> 概要: 基金“换血”速度加快
### [首次发现，科学家找到「专吃病毒」的生物，网友：能消除人体内病毒吗](https://www.ithome.com/0/666/147.htm)
> 概要: 顶级“捕食者”—— 病毒，也遇上对手了！科学家们发现了第一种专以病毒为食的生物，目前该研究已登上 PNAS。研究团队表示，这个惊人的发现不仅可能会改变人们既有认知的生态模型，甚至还会影响人们对全球碳循......
### [JR热议远古龙团挺身而出，目前为止Light是WBG发挥最稳定的点？](https://bbs.hupu.com/57290463.html)
> 概要: 在刚刚结束的微博杯决赛里，在第四局WBG处于危机时刻的时候，Light的泽丽再次挺身而出配合小虎的妖姬帮助WBG打赢了远古龙团从而帮助队伍一举拿下了比赛。远古龙团挺身而出，目前为止Light是WBG发
### [流言板WBG战队官博：捧杯吧！微博人！属于我们的2023第一个冠军](https://bbs.hupu.com/57290470.html)
> 概要: 虎扑01月06日讯 在WBG以3-1击败BLG拿下微博杯冠军后，WBG官博更新微博，原文如下：我们的征途故事，在此刻落笔写下粲然烜赫的第一章。属于我们的2023第一个冠军捧杯吧！微博人！   来源：
### [山东能源董事长李伟：聚焦高质量发展 坚定瞄准建设一流企业目标奋进](https://finance.sina.com.cn/china/2023-01-06/doc-imxzhkzt5071121.shtml)
> 概要: 被誉为中国经济奥斯卡大奖、“中国梦杯•中国经济新闻人物——2022十大经济年度人物”颁奖盛典于2023年1月6日在北京正大中心4层正式举办。
### [流言板数据对比：深圳队篮板比苏州肯帝亚多13个，失误多4个](https://bbs.hupu.com/57290578.html)
> 概要: 虎扑01月06日讯 2﻿022-23赛季CBA常规赛第23轮，苏州肯帝亚113-101战胜深圳队。双方数据对比：篮板：深圳50-苏州肯帝亚37助攻：深圳29-苏州肯帝亚32投篮命中率：深圳45.7%-
### [五城大屏同时点亮，庆祝武汉eStarPro夺得2022王者世冠KIC总冠军](https://bbs.hupu.com/57290632.html)
> 概要: 1月5日-6日，武汉、长沙、南昌、福州、苏州五城大屏同时点亮，庆祝武汉eStarPro夺得2022王者世冠KIC总冠军！老e看到有很多粉丝朋友来现场打卡啦好喜欢这个五彩斑斓的时刻，感谢有你们共同记
# 小说
### [六职](http://book.zongheng.com/book/927669.html)
> 作者：毒尧

> 标签：奇幻玄幻

> 简介：世人不同的天赋衍生出了十二种不同的职业，他们没有激烈的碰撞和争斗，有的是为了各自的生活而努力活着。而余引，一个性格孤僻的人，他就好像一颗不属于这个世界的顽石一般。逐渐成长，随着一个个妻子踏入他的人生，他却越来越好色，然后活成了一个他觉得还算不错的人。无雷无绿帽，低防专有！

> 章节末：第一章：重活

> 状态：完本
# 论文
### [Struct-MDC: Mesh-Refined Unsupervised Depth Completion Leveraging Structural Regularities from Visual SLAM | Papers With Code](https://paperswithcode.com/paper/struct-mdc-mesh-refined-unsupervised-depth)
> 概要: Feature-based visual simultaneous localization and mapping (SLAM) methods only estimate the depth of extracted features, generating a sparse depth map. To solve this sparsity problem, depth completion tasks that estimate a dense depth from a sparse depth have gained significant importance in robotic applications like exploration. Existing methodologies that use sparse depth from visual SLAM mainly employ point features. However, point features have limitations in preserving structural regularities owing to texture-less environments and sparsity problems. To deal with these issues, we perform depth completion with visual SLAM using line features, which can better contain structural regularities than point features. The proposed methodology creates a convex hull region by performing constrained Delaunay triangulation with depth interpolation using line features. However, the generated depth includes low-frequency information and is discontinuous at the convex hull boundary. Therefore, we propose a mesh depth refinement (MDR) module to address this problem. The MDR module effectively transfers the high-frequency details of an input image to the interpolated depth and plays a vital role in bridging the conventional and deep learning-based approaches. The Struct-MDC outperforms other state-of-the-art algorithms on public and our custom datasets, and even outperforms supervised methodologies for some metrics. In addition, the effectiveness of the proposed MDR module is verified by a rigorous ablation study.
### [Randomized Quantization for Data Agnostic Representation Learning | Papers With Code](https://paperswithcode.com/paper/randomized-quantization-for-data-agnostic)
> 日期：19 Dec 2022

> 标签：None

> 代码：https://github.com/microsoft/random_quantize

> 描述：Self-supervised representation learning follows a paradigm of withholding some part of the data and tasking the network to predict it from the remaining part. Towards this end, masking has emerged as a generic and powerful tool where content is withheld along the sequential dimension, e.g., spatial in images, temporal in audio, and syntactic in language. In this paper, we explore the orthogonal channel dimension for generic data augmentation. The data for each channel is quantized through a non-uniform quantizer, with the quantized value sampled randomly within randomly sampled quantization bins. From another perspective, quantization is analogous to channel-wise masking, as it removes the information within each bin, but preserves the information across bins. We apply the randomized quantization in conjunction with sequential augmentations on self-supervised contrastive models. This generic approach achieves results on par with modality-specific augmentation on vision tasks, and state-of-the-art results on 3D point clouds as well as on audio. We also demonstrate this method to be applicable for augmenting intermediate embeddings in a deep neural network on the comprehensive DABS benchmark which is comprised of various data modalities. Code is availabel at http://www.github.com/microsoft/random_quantize.
