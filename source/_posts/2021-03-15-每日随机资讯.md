---
title: 2021-03-15-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.MassapequaOwl_EN-CN6825529741_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2021-03-15 21:17:14
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.MassapequaOwl_EN-CN6825529741_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 新闻
### [16年春晚，两任丈夫都非常爱她，最对不起的是自己女儿](https://new.qq.com/omn/20210309/20210309A05L8400.html)
> 概要: 很多人都知道央视有个董卿，但是却有人不记得了，之前还有个周涛也是个非常厉害的主持人。如果这两个人相比的话还真说不好是谁比较好一点，但是她们也都各自有自己的风格，所以也没什么可比性。不过，不难发现的是董......
### [放过张坤、刘彦春吧](https://www.huxiu.com/article/414881.html)
> 概要: 作者｜Eastland头图｜电影《喜剧之王》3月2日，虎嗅热文《张坤的神话不可持续》发表后，有读者留言称“基金经理都是人中龙凤，哪个不是名校毕业……”对明星基金经理的膜拜之情溢于言表。虎嗅日常臧否的互......
### [舍弃事业相助郭德纲，从粉丝到经纪人，德云社外交官王海是何来历](https://new.qq.com/omn/20210308/20210308A0CBAT00.html)
> 概要: 2003年一天，北京天桥乐茶园，郭德纲与于谦正在这里表演相声。那时德云社刚刚成立，人气惨淡，每次表演台下零零散散坐着几号人，弟子们不得不出去拉客。但今天有点不一样，表演刚开始，一位白白胖胖的陌生男人走......
### [工藤静香不爱修图 50岁素颜照曝光演绎优雅老去](https://ent.sina.com.cn/s/j/2021-03-15/doc-ikkntiam1821988.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，日本一代女神工藤静香，和天王木村拓哉结婚后，生下两个女儿，就淡出演艺圈，专心在家相夫教子，不过仍会透过网络分享生活点滴。近日，她曝光最新近照，大方晒素颜真面目，让网友都大赞......
### [异形粉丝游戏《异形：未来希望》演示 恐怖刺激](https://www.3dmgame.com/news/202103/3810502.html)
> 概要: 近日民间开发者IIapagokc公布演示视频，展示了他的同人作品《异形：未来希望》(Alien: Hope For The Future)的新开发进展。一起来欣赏下吧，注意直接从11分21秒开始看游戏......
### [《全面战争：战锤2》新免费DLC：黑暗精灵新领主](https://www.3dmgame.com/news/202103/3810516.html)
> 概要: CA正在开发新作《全面战争：战锤3》，但也没忘记为《全面战争：战锤2》添加新内容。近日官方宣布了《全面战争：战锤2》新免费DLC，将为黑暗精灵势力追加一位新传奇领主。卡隆卡的黑暗精灵领主Rakarth......
### [带狗末日求生 开放世界新游《FIRST》上架Steam](https://www.3dmgame.com/news/202103/3810517.html)
> 概要: 3月14日，PlayWay工作室新游《FIRST》上架Steam，这是一款带狗末日求生类开放世界新游，整体画面相当出色，极致写实风格，本作暂不支持中文，发售日待定。·FIRST是一个生存模拟器，将玩家......
### [你们眼中宋仲基还能在荧屏多久？](https://new.qq.com/omn/20210312/20210312A0C8PV00.html)
> 概要: 你们眼中宋仲基还能在荧屏多久？
### [动画「MAZICA PARTY」番宣CM公开](http://acg.178.com//202103/409774913195.html)
> 概要: 电视动画「MAZICA PARTY」（魔卡派对）公开了番宣CM，该动画将于2021年4月4日开始播出。「MAZICA PARTY」番宣CM主题曲OP：「マジ歌」遊助ED：「MAZICA PARTY」S......
### [Discovering WW1 tunnel of death hidden in France for a century](https://www.bbc.com/news/world-europe-56370510)
> 概要: Discovering WW1 tunnel of death hidden in France for a century
### [芒格：我如何用五个通用观念，解决复杂问题](https://www.huxiu.com/article/415019.html)
> 概要: 编者按：这篇文章是一篇芒格的演讲，来自最新版本的《查理·芒格传：巴菲特幕后智囊》，本书是国内唯一经过芒格及巴菲特授权的传记，从这篇演讲中我们可以看到芒格独特的逆向思维，以及五个他自称“超级简单”的通用......
### [《星期一的丰满》：白色情人节 努力有了好结果](https://acg.gamersky.com/news/202103/1370263.shtml)
> 概要: 3月14日是白色情人节，之前那个背着大筐送巧克力的迷糊妹子，终于在这天收到了自己期待的回礼。被心仪男生载着去学校，还有巧克力回礼，妹子的心情很雀跃。
### [New Bare Hash Map: 2X-3X Speedup over SOTA](https://github.com/wangyi-fudan/wyhash)
> 概要: New Bare Hash Map: 2X-3X Speedup over SOTA
### [《EVA新剧场版：终》全新CM 绫波丽插秧、真嗣钓鱼](https://acg.gamersky.com/news/202103/1370286.shtml)
> 概要: 《新世纪福音战士新剧场版：终》正在日本热映中，官方发布了“第3村”新预告，这次没有战斗的场面，展现了大家的日常生活。其中绫波丽在插秧，而碇真嗣正在钓鱼。
### [剧场动画「宇宙战舰大和号的时代 西历2202年的选择」正式定档](http://acg.178.com//202103/409779738051.html)
> 概要: 由「宇宙战舰大和号2199」和「宇宙战舰大和号2202 爱的战士们」整合的特别动画电影「宇宙战舰大和号的时代 西历2202年的选择」原定于2021年1月15日上映，后因疫情原因延期。今日（3月15日）......
### [组图：关晓彤春日慵懒大片曝光 浅色衬衣叠穿故事感满满](http://slide.ent.sina.com.cn/star/slide_4_86512_353976.html)
> 概要: 组图：关晓彤春日慵懒大片曝光 浅色衬衣叠穿故事感满满
### [参加春晚一夜成名的金靖被“打回原形”，事业发展出现新方向](https://new.qq.com/omn/20210308/20210308A0FU9O00.html)
> 概要: 艺人想要得到更多观众的关注，登上大舞台是必不可少的环节之一，大型演唱会也好、主流市场举办的晚会也好，当能够出现在这些被聚光灯照耀的舞台上时，艺人的人气才会出现提升。当然春晚作为央视主办的晚会，在影响力......
### [IPFS Local Offline Collaboration Sig](https://blog.fission.codes/ipfs-local-offline-collaboration-sig/)
> 概要: IPFS Local Offline Collaboration Sig
### [TV动画「战斗陀螺Dynamite Battle」预告PV公开](http://acg.178.com//202103/409783748363.html)
> 概要: 电视动画「战斗陀螺Dynamite Battle」公开了预告PV，本作将于4月2日播出。「战斗陀螺Dynamite Battle」预告PVCAST大黒天ベル:田村睦心ランゾー・キヤマ:冈林史泰翠龙バサ......
### [「回转企鹅罐」10周年贺图公开](http://acg.178.com//202103/409786461180.html)
> 概要: 动画「回转企鹅罐」总作画监督公开了最新绘制的10周年贺图。「回转企鹅罐」是由Brain's Base制作，2011年7月在日本播出的原创动画作品。讲述的是高仓一家的妹妹阳毬不幸患绝症去世，却因戴上在水......
### [韩影票房：《水芹》蝉联冠军 《指环王》重映第六](https://ent.sina.com.cn/m/f/2021-03-15/doc-ikknscsi5169141.shtml)
> 概要: 名次 片名 上映时间 占有率 周末人次 累计人次　　11 2021/03/03 30.5% 130，339 496，936　　23《鬼灭之刃剧场版：无限列车篇》 2021/01/27 22.5% 93......
### [国内刚开始萌芽的播客，已经成了美国巨头们的主战场](https://www.huxiu.com/article/415060.html)
> 概要: 本文来自微信公众号：极客公园（ID：geekpark），作者：Jesse在全球市场上，苹果音乐服务还在紧紧追赶 Spotify，但它原本领先的播客服务却被 Spotify 突袭超越了。自 2015 年......
### [动画电影《EVA新剧场版》票房突破33亿日元](https://news.dmzj1.com/article/70344.html)
> 概要: 动画电影《EVA新剧场版》宣布了首周周末（3月13日至14日）票房收入为11亿7744万5400日元，观众数量为76万人。从3月8日上映开始的7天时间，作品票房收入达到了33亿3842万2400日元，观众数量达到了219万4533人。
### [日本香川县高中生向警方举报香川县伪造公民意见](https://news.dmzj1.com/article/70347.html)
> 概要: 在本日（15日），日本高松市的一名18岁男高中，向当地警方提出了嫌疑人不详的涉嫌伪造私文书的告发状。在告发状中，举报香川县在《网络游戏成瘾症对策条例》的征求公民意见时，涉嫌对公民意见进行了伪造。
### [余景天回应"小尼不是尼格买提":和尼格买提同台过](https://ent.sina.com.cn/tv/zy/2021-03-15/doc-ikkntiam2057798.shtml)
> 概要: 新浪娱乐讯 昨日，尼格买提发博称“已经没办法搜小尼了，小尼不是尼格买提了”，搜“小尼”出来的内容全部都是余景天。　　今日，余景天回应尼格买提，表示两人曾在《青春有你3》的第一个舞台《启航》相遇并同台过......
### [《EVA新剧场版：终》首周票房超33亿日元 系列最佳](https://acg.gamersky.com/news/202103/1370396.shtml)
> 概要: 3月8日上映的《新世纪福音战士新剧场版：终》公开了首周的票房，从3月8日起截止至3月14日，7天累计票房33亿3842万2400日元，到场观影人219万4533人。
### [The FBI Should Stop Attacking Encryption](https://www.eff.org/deeplinks/2021/03/fbi-should-stop-attacking-encryption-and-tell-congress-about-all-encrypted-phones)
> 概要: The FBI Should Stop Attacking Encryption
### [AT&T 计划斥资 80 亿美元部署 C 波段 5G 服务，到 2023 年初覆盖 1 亿人](https://www.ithome.com/0/540/117.htm)
> 概要: 据 Mobile World Live 报道，AT&T 公布了斥资 80 亿美元以部署关键的 C 波段（3.7GHz 至 4.2GHz）频谱的计划，并提高了对 HBO Max 流媒体服务的用户目标。在......
### [外媒：AMD RX 6700 XT 显卡首发整个欧洲只有几千张](https://www.ithome.com/0/540/133.htm)
> 概要: IT之家 3 月 15 日消息 根据外媒 Igor's Lab 的消息，AMD 只能为整个欧洲提供几千张 Radeon RX 6700 XT 显卡。这款显卡国内将于 3 月 18 日开卖，售价为 36......
### [50岁父亲殴打玩游戏的十余岁的儿子被逮捕](https://news.dmzj1.com/article/70349.html)
> 概要: 根据日本媒体消息，北海道札幌市警方因涉嫌暴行罪，逮捕了一名殴打十余岁儿子的50岁男性。
### [TV动画《烧的话也烧个马克杯》PV](https://news.dmzj1.com/article/70350.html)
> 概要: TV动画《烧的话也烧个马克杯》宣布了将于4月2日开始播出的消息，一段使用了主题曲片段的正式PV也一并公开。
### [13 省芯片产业规划出炉，谁是未来五年国产顶梁柱](https://www.ithome.com/0/540/167.htm)
> 概要: 3 月 15 日报道，《中华人民共和国国民经济和社会发展第十四个五年规划和 2035 年远景目标纲要》终于公布，集成电路被列为科技攻关的 7 大前沿领域之一。今年是 “十四五”规划开局之年，围绕集成电......
### [仍未走出低迷的苹果产业链板块 又遭天风证券郭明錤唱空一波](https://finance.sina.com.cn/roll/2021-03-15/doc-ikknscsi5406773.shtml)
> 概要: 原标题：歌尔、立讯太难了！知名苹果分析师唱空Air Pods，百亿存货怎么办？ 作者：张赛男 仍未走出低迷的苹果产业链板块，又遭天风证券知名苹果分析师郭明錤唱空一波。
### [统计局数据显示:前两个月主要指标增势平稳 国民经济持续稳定恢复](https://finance.sina.com.cn/jjxw/2021-03-15/doc-ikknscsi5414419.shtml)
> 概要: 原标题：前两个月主要指标增势平稳 国民经济持续稳定恢复 央视网消息（新闻联播）：国务院新闻办今天（3月15日）举行新闻发布会，国家统计局公布了1—2月份我国国民经济运行...
### [已有银行上调经营性贷款利率：整体“涨价”可能性不大](https://finance.sina.com.cn/jjxw/2021-03-15/doc-ikknscsi5412947.shtml)
> 概要: 原标题：已有银行上调经营性贷款利率，整体“涨价”可能性不大 “4月1日后，银行将经营性贷款利率上调5%~10%，并且提高贷款门槛。现正推出对个体企业的多重优惠政策...
### [3DM轻松一刻第480期 变最帅的身撩最可爱的妹](https://www.3dmgame.com/bagua/4400.html)
> 概要: 每天一期的3DM轻松一刻又来了，欢迎各位玩家前来观赏。汇集搞笑瞬间，你怎能错过？好了不多废话，一起来看看今天的搞笑图。希望大家能天天愉快，笑口常开！高玩操作海鸥飞来想夺食麦当劳汉堡，没想到被人一手抓住......
### [给机器人装上蝗虫耳朵，以色列科学家实现生物传感器新突破](https://www.ithome.com/0/540/171.htm)
> 概要: 通俗来讲，要想让机器人 “听见”，就需要麦克风阵列将声音信号转换为电信号，随后经过对电信号进行处理，获得声音包含的信息。当然，打造机器人听觉，说起来容易做起来难。机器人的听觉系统需要传感、机械、控制等......
### [2亿多人的灵活就业 如何在法治轨道健康发展](https://finance.sina.com.cn/jjxw/2021-03-15/doc-ikkntiam2267153.shtml)
> 概要: 原标题：2亿多人的灵活就业，如何在法治轨道健康发展 作者：陈兵 ▪ 程前 保就业就是保民生，稳就业就是稳经济。李克强总理在两会期间强调：中国的灵活就业正在兴起...
### [荣耀或重启Magic对标华为 7月发超级旗舰](https://www.3dmgame.com/news/202103/3810562.html)
> 概要: 据知情人士向腾讯新闻《一线》透露，荣耀最快将于7月发布分离华为后的真正旗舰产品。“不出意外，这次将用上高通最新的旗舰级芯片骁龙888。”荣耀CEO赵明此前接受腾讯新闻《一线》采访时表示，荣耀所有供应商......
### [组图：《少年的你》入围第93届奥斯卡最佳国际影片](http://slide.ent.sina.com.cn/film/slide_4_704_354000.html)
> 概要: 组图：《少年的你》入围第93届奥斯卡最佳国际影片
### [十年罕见强沙尘暴来袭！北京再现“下土天”，取消航班37次](https://finance.sina.com.cn/china/dfjj/2021-03-15/doc-ikknscsi5434997.shtml)
> 概要: 原标题：十年罕见强沙尘暴来袭！北京再现“下土天”，取消航班37次 华夏时报 记者刘诗萌 北京报道 3月15日一大早，在北京石景山区上班的小王刚从地铁口出来...
### [一图跟进|奥马电器反击李东生突袭：再次不予审议临时股东大会](https://finance.sina.com.cn/stock/focus/2021-03-15/doc-ikknscsi5436088.shtml)
> 概要: 今年1月以来，TCL家电通过5次资本运作不断增持奥马电器股份成为第一大股东后，TCL能否进入管理层成为双方博弈的关键。3月15日，奥马电器发布公告，展开第三次回击...
### [张雨剑承认与吴倩结婚生子！评论区翻车，男方频炒cp立单身人设](https://new.qq.com/omn/20210315/20210315A0DPJN00.html)
> 概要: 3月15日，张雨剑首度承认自己和吴倩的关系，两人正常恋爱结婚，有一个可爱的小孩子，法律手续齐全。            除了公开自己和吴倩的夫妻关系，张雨剑还言语十分愤怒，表示：“骂我我无所谓因为我不......
# 小说
### [枭臣](http://book.zongheng.com/book/44202.html)
> 作者：更俗

> 标签：历史军事

> 简介：现代人谭纵含冤而死，英魂不散，意外回到一个陌生的历史时空中去，成为东阳府林家刚考中举人、性格懦弱、有些给人看不起的旁支子弟林缚。还没来得及去实现整日无事生非、溜狗养鸟、调戏年轻妇女的举人老爷梦想，林缚就因迷恋祸国倾城的江宁名妓苏湄给卷入一场由当今名士、地方豪强、朝中权宦、割据枭雄、东海凶盗等诸多势力参与的争夺逐色的旋涡中去。不甘心做太平犬,也不甘沦落为离乱人，且看两世为人的林缚如何从权力金字塔的最底层开始翻云覆雨，在“哪识罗裙里、销魂别有香”的香艳生涯中，完成从“治世之能臣”到“乱世之枭雄”的华丽转变。PS:我在QT语音房间【2133621】 更俗官方QT，大家有空可以一起聊聊。链接地址http://qt.qq.com/go.html?roomid=2133621

> 章节末：新书《踏天无痕》 已发布

> 状态：完本
# 论文
### [What Can We Learn From Almost a Decade of Food Tweets](https://paperswithcode.com/paper/what-can-we-learn-from-almost-a-decade-of)
> 日期：10 Jul 2020

> 标签：QUESTION ANSWERING

> 代码：https://github.com/Usprogis/Latvian-Twitter-Eater-Corpus

> 描述：We present the Latvian Twitter Eater Corpus - a set of tweets in the narrow domain related to food, drinks, eating and drinking. The corpus has been collected over time-span of over 8 years and includes over 2 million tweets entailed with additional useful data.
### [EarthNet2021: A novel large-scale dataset and challenge for forecasting localized climate impacts](https://paperswithcode.com/paper/earthnet2021-a-novel-large-scale-dataset-and)
> 日期：11 Dec 2020

> 标签：CROP YIELD PREDICTION

> 代码：https://github.com/earthnet2021/earthnet-model-intercomparison-suite

> 描述：Climate change is global, yet its concrete impacts can strongly vary between different locations in the same region. Seasonal weather forecasts currently operate at the mesoscale (> 1 km).
