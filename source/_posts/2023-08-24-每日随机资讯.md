---
title: 2023-08-24-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.SharkFinCove_ZH-CN4952934195_1920x1080.webp&qlt=50
date: 2023-08-24 22:27:17
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.SharkFinCove_ZH-CN4952934195_1920x1080.webp&qlt=50)
# 新闻
### [Cell Death and Disease: WFDC3可作为结直肠癌患者雌激素/ERβ通路靶向治疗的指标](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx2358792971920&cate_id=-1)
> 概要: 结直肠癌(CRC)是全球第三大常见癌症，具有很高的转移风险。男性的结直肠癌发病率和死亡率高于女性，可能是由于女性性激素的保护作用。绝经后妇女的激素替代治疗与发生CRC的风险较低相关。更好地了解结直肠癌......
### [7 Reasons Why Cottage Cheese Is Good for You](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx9413729740424&cate_id=-1)
> 概要: 这种乳制品可以帮助你减肥，控制血糖和强壮骨骼......
### [Brain-reading devices allow paralysed people to talk using their thoughts](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx8657245070965&cate_id=-1)
> 概要: 两项研究报告显示，旨在帮助患有颜面神经麻痺的人们进行交流的技术有了相当大的进步......
### [高风险！浙江大学研究证实吸烟与结直肠癌风险高度相关](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx1742616159878&cate_id=-1)
> 概要: <strong>导读	</strong>该研究证实了吸烟和DNA甲基化与结直肠癌风险之间的显著关联，并对吸烟对结直肠癌风险的致病作用产生了新的见解......
### [高脂饮食与肠道微生物：结直肠癌风险的新认识](https://jishu.medpeer.cn/show/news_information/engDetailPage?information_id=zx9558299010690&cate_id=-1)
> 概要: 研究人员在Salk研究所和加利福尼亚大学圣地亚哥分校（UCSD）的团队领导下，通过对小鼠的研究发现，高脂饮食如何改变肠道细菌，并改变被这些细菌修改的消化分子——胆汁酸，使动物易患结肠直肠癌......
### [App Store新变化你发现了吗？App的这些信息变得更重要了！](https://www.woshipm.com/share/5891282.html)
> 概要: 最近有业内人士发现，App Store搜索结果页出现了相对明显的变化，比如APP展示信息增加、展示样式调整，等等。而如果好好利用这些变化，运营人员或许可以提升APP的下载转化。一起来看看作者的介绍和分......
### [长远来看：产品经理哪些职业方向很值钱？](https://www.woshipm.com/zhichang/5891338.html)
> 概要: 互联网的发展速度很快，岗位要求也在随之变化。由于产品经理的特殊性，我们必须有针对性地进行职业方向选择、从而找准定位、长远持续地发展，让自己更值钱。长远来看：产品经理哪些职业方向很值钱？互联网的世界，变......
### [美团直播，意义是什么？](https://www.woshipm.com/it/5891539.html)
> 概要: 美团面对抖音在本地生活的强势进攻，也搞起了直播。本文分析了美团推出外卖直播的背后逻辑，指出其重点并不在直播形式，而在重新构建用户与商品的直接联系，一起来看看吧。大家好，今天接着上篇继续聊一下本地生活的......
### [【NestJS系列】核心概念：Module模块](https://segmentfault.com/a/1190000044144569)
> 概要: 前言模块指的是使用@Module装饰器修饰的类，每个应用程序至少有一个模块，即根模块。根模块是Nest用于构建应用程序的起点，理论上Nest程序可能只有根模块，但在大多数情况下是存在多个模块的，每个模......
### [uniapp中解析markdown支持网页和小程序](https://segmentfault.com/a/1190000044145112)
> 概要: 对于markdown相信大家都不陌生，日常写文档或日常记录都用到的比较多，书写的是markdown的格式，实时预览的是转换后的html样式。本次实现的需求是在uniapp中转换markdown文本展示......
### [满月限定 | 中秋礼盒包装设计 | 团圆之约始终如一](https://www.zcool.com.cn/work/ZNjYzMDEyODQ=.html)
> 概要: 项目委托|畅指网络满月限定·中秋礼盒包装全案设计服务|SERVICE品牌创意策略/品牌logo/品产品包装/品牌视觉系统团队TEAMSTD不眠虎创意工作室®-品牌总监 | 子轩&笑笑Saner品牌策划......
### [微短剧赛道风口下的一站式点播解决方案](https://segmentfault.com/a/1190000044145388)
> 概要: 微短剧行业正风生水起。一种全新的剧集模式正迅速崛起，并引起广泛关注。从线下电影院的“巨幕”到PC端“网络大电影”，从“长视频”再到如今移动端1-3分钟的“微短剧”，影视行业在过去几年经历了一场深刻又显......
### [笑意穿透车窗 马斯克试驾量产赛博卡车](https://finance.sina.com.cn/stock/usstock/c/2023-08-24/doc-imzifxmr6904918.shtml)
> 概要: 当地时间周三，特斯拉首席执行官埃隆·马斯克试驾了该公司Cybertruck的量产版本，此前这款期待已久的卡车生产已被推迟多年......
### [借VinFast东风，越南首个科技公司申请赴美上市](https://finance.sina.com.cn/stock/usstock/c/2023-08-24/doc-imzihcti7893002.shtml)
> 概要: 越南互联网初创公司VNG Ltd．申请在美国进行首次公开募股（IPO），成为首个寻求在纽约上市的越南科技公司。此前越南电动汽车制造商VinFast在纳斯达克的IPO取得了巨大成功，其股票上市一周以来飙......
### [对话震坤行： 工业品B2B的“降本增效”时代 ， MRO平台走向智能制造 | 36氪专访](https://36kr.com/p/2400332836725126)
> 概要: 对话震坤行： 工业品B2B的“降本增效”时代 ， MRO平台走向智能制造 | 36氪专访-36氪
### [刘丹否认杨幂争小糯米抚养权 称上天自有安排](https://ent.sina.com.cn/s/h/2023-08-24/doc-imzihcth2576938.shtml)
> 概要: 新浪娱乐讯 港媒月初爆料指杨幂斥资5.4亿争夺女儿的抚养权，23日，刘恺威父亲刘丹出席活动回应此事。　　记者表示是否有听说杨幂将抢夺小糯米的抚养权、9月将会带女儿回北京读书？“关于家里的事就不说啦，外......
### [36氪专访 | 优车联创始人任昊：商用车后市场是数字蓝海中一座巨大的冰山](https://36kr.com/p/2400401534017673)
> 概要: 36氪专访 | 优车联创始人任昊：商用车后市场是数字蓝海中一座巨大的冰山-36氪
### [组图：朱珠身穿紧身衣裙身材凹凸有致 眼眸如刃气场全开](http://slide.ent.sina.com.cn/star/slide_4_704_388241.html)
> 概要: 组图：朱珠身穿紧身衣裙身材凹凸有致 眼眸如刃气场全开
### [What's new in Pika v3.5.0](https://segmentfault.com/a/1190000044147445)
> 概要: 时隔两年，Pika 社区正式发布经由社区 50 多人参与开发并在 360 生产环境验证可用的v3.5.0版本，新版本在提升性能的同时，也支持了 Codis 集群部署，BlobDB KV 分离，增加 E......
### [TV动画《浪客剑心》公开第六弹PV](https://news.dmzj.com/article/79018.html)
> 概要: TV动画《浪客剑心》公开第六弹PV。
### [全文 | 英伟达业绩会：供应增长会持续、数据中心在转型……](https://finance.sina.com.cn/stock/usstock/c/2023-08-24/doc-imzihize2475799.shtml)
> 概要: 北京时间8月24日上午消息，英伟达今日公布了该公司截至2023年7月30日的2024财年第二财季财报。报告显示，英伟达第二财季营收为135.07亿美元，同比增长101%，环比增长88%，创下历史纪录；......
### [《攻壳机动队SAC_2045》新作剧场版特报PV公开！](https://news.dmzj.com/article/79019.html)
> 概要: 《攻壳机动队SAC_2045》新作剧场版特报PV公开，11月23日上线。
### [类魂《最后的信念》跳票11月 PC测试现已开放](https://www.3dmgame.com/news/202308/3876138.html)
> 概要: 发行商Playstack 和开发商Kumi Souls Games 将类魂黑暗哥特式类银河战士恶魔城横板动作游戏《最后的信念（TheLast Faith）》的发布窗口从之前计划的 10 月推迟至 11......
### [高山忍《稗记舞咏》2024年TV动画化决定！](https://news.dmzj.com/article/79020.html)
> 概要: 高山忍《稗记舞咏》决定TV动画化，高山亲自绘制的先导视觉图也被公开。
### [《星际卡车司机》科隆实机公开 明年正式发售](https://www.3dmgame.com/news/202308/3876140.html)
> 概要: 今日（8月24日），模拟经营游戏《星际卡车司机》科隆游戏展最新实机预告公布，《星际卡车司机》将于明年发售，登陆Steam平台，感兴趣的玩家可以点击此处进入商店页面。游戏实机：这款游戏在卡车模拟类型中融......
### [你的iPhone15来自印度?印度或成苹果手机最大出口国](https://www.3dmgame.com/news/202308/3876143.html)
> 概要: 全球最大的电子产品代工商鸿海集团设在印度的工厂已经开始组装苹果公司即将于9月正式发布的最新款智能手机iPhone 15。这意味着，中印量产苹果最新款手机的时间差不断缩小。(图片来源：网络)这次印度的目......
### [起猛了，看到韩国版X战警了……](https://news.dmzj.com/article/79022.html)
> 概要: 韩国人有自己的X战警
### [【西海岸】科尔：每一场热身赛都很有意义 助我们适应FIBA规则](https://bbs.hupu.com/61807919.html)
> 概要: 【西海岸】科尔：每一场热身赛都很有意义 助我们适应FIBA规则
### [《飞空艇时代：贸易帝国》新预告 年内发售](https://www.3dmgame.com/news/202308/3876151.html)
> 概要: 今日（8月24日），飞艇贸易和战斗模拟游戏《飞空艇时代：贸易帝国》新预告公布，游戏暂不支持中文，感兴趣的玩家可以点击此处进入商店页面。宣传片：欢迎来到赛瑟格！作为新上任的飞艇艇长，你要建立纵横群岛的商......
### [组图：日本核污染水排海开始 郭帆贾乃亮孙坚任豪等发声谴责](http://slide.ent.sina.com.cn/star/slide_4_704_388251.html)
> 概要: 组图：日本核污染水排海开始 郭帆贾乃亮孙坚任豪等发声谴责
### [深圳国际电子展开幕，康盈半导体发动C端存储新攻势](http://www.investorscn.com/2023/08/24/109890/)
> 概要: 8月23日，中国电子、嵌入式及半导体先进封测行业风向标——elexcon2023深圳国际电子展隆重开幕。作为本次深圳电子展的重磅活动之一，国产存储新锐企业康盈半导体再次焕新而来，以“燃青春，随芯存”为......
### [36氪研究院｜2023年中国制造业数字化转型研究报告](https://36kr.com/p/2400392460640385)
> 概要: 36氪研究院｜2023年中国制造业数字化转型研究报告-36氪
### [华硕确认英特尔 14 代酷睿处理器代号 Raptor Lake-S Refresh](https://www.ithome.com/0/714/627.htm)
> 概要: IT之家8 月 24 日消息，华硕 8 月 22 日发布了三款 Z790 主板，包括旗舰型号 ROG Maximus Z790 DARK HERO 以及新款猛禽和 TUF 型号。华硕表示，这三款主板是......
### [【IT之家评测室】微星雷影 17 体验评测，这一次微星带头开卷性价比](https://www.ithome.com/0/714/633.htm)
> 概要: 熟悉微星笔记本产品的小伙伴都知道，他们家的产品主打高端，但是最近微星却推出了一款首发只要 8499 元的性能小钢炮，它就是咱今天的主角 —— 雷影 17，处理器和显卡分别选择旗舰定位的锐龙 9 794......
### [视觉中国回应照片侵权：考虑终止此次争议的所有业务](https://www.3dmgame.com/news/202308/3876172.html)
> 概要: 前段时间，摄影师自己拍摄的照片，被视觉中国致电称是侵权使用，还被要求赔偿8万余元一事，引起网友关注热议。而日前，根据新华财经的最新消息，视觉中国相关负责人接受采访时表示，涉事图片不是摄影师本人使用的，......
### [8月24日这些公告有看头](https://www.yicai.com/news/101843244.html)
> 概要: 8月24日晚间，沪深两市多家上市公司发布公告，以下是第一财经对一些重要公告的汇总，供投资者参考。
### [出海速递 | 传TikTok计划切断电商外链；泡泡玛特：何以解忧，唯有出海？](https://36kr.com/p/2401753551643009)
> 概要: 出海速递 | 传TikTok计划切断电商外链；泡泡玛特：何以解忧，唯有出海？-36氪
### [全国首条高速公路重卡换电干线“宁厦线”通车](https://www.ithome.com/0/714/654.htm)
> 概要: IT之家8 月 24 日消息，据宁德时代消息，8 月 24 日，全国首条高速公路重卡换电绿色物流专线 ——“宁德厦门干线”在福建省高速集团长乐服务区举办了正式通车仪式。据介绍，宁厦线在福建省政府支持下......
### [东方甄选自营南美白虾今天卖爆了；日系化妆品安全性受质疑，多家品牌回应；小米拿到造车资质？最新回应来了](https://www.yicai.com/news/101843321.html)
> 概要: 第一财经每日精选最热门大公司动态，点击「听新闻」，一键收听。
### [紫光展锐官宣“长虹儿童电话手表 V5”，搭载旗下 W377 芯片](https://www.ithome.com/0/714/677.htm)
> 概要: 感谢IT之家网友雨雪载途的线索投递！IT之家8 月 24 日消息，紫光展锐官方微博今日发文官宣“长虹儿童电话手表 V5”，官方表示，“长虹儿童电话手表 V5 内置紫光展锐 W377 穿戴芯片，致力于专......
### [数据资源“入表”新规影响几何？企业利润、财政收入有望增加](https://www.yicai.com/news/101843351.html)
> 概要: 如何激活数据要素潜能，做强做大数字经济，已经成为中国高质量发展一大重要课题，而这离不开相关制度构建。
### [益盛药业：人参全产业链不断深化，经营业绩稳步增长](http://www.investorscn.com/2023/08/24/109895/)
> 概要: 8月24日晚间，益盛药业发布2023年半年度报告，公司实现营业收入46,942.85万元，较上年同期增长9.57%；归属上市公司股东净利润4,742.83万元，较上年同期增长8.25%。作为国内首家人......
### [储能带动业绩增长，阳光电源上半年净利润同比增长383.55%](https://www.yicai.com/news/101843380.html)
> 概要: 8月24日晚间，阳光电源（300274.SZ）披露的2023年半年报显示，该公司实现净利润43.54亿元，意味着今年上半年净利润已超过去年全年的净利润。
### [UESG启动中国ESG人才储备计划](http://www.investorscn.com/2023/08/24/109897/)
> 概要: 据《ESG人才吸引力洞察报告2023》数据显示，2022年5月至2023年4月，全国新发布的ESG（Environmental，Social and Governance，环境、社会和公司治理）职位同......
### [一图流傲寒老夫子闪捆到孙尚香，将滔搏最重要的c位击杀](https://bbs.hupu.com/61813542.html)
> 概要: 虎扑08月24日讯 傲寒老夫子闪捆到孙尚香，将滔搏最重要的c位击杀   来源： 虎扑    标签：傲寒南京Hero久竞
### [沈阳市长与百余家报业传媒老总面对面，唠一唠“心向往新沈阳”](https://finance.sina.com.cn/jjxw/2023-08-24/doc-imziimnt7316173.shtml)
> 概要: 转自：中国劳动保障新闻网作为中国报业新媒体（客户端）发展大会的重要内容，8月23日下午，中国报业新媒体（客户端）发展建设报告会举行。
### [海关总署全面暂停进口日本水产品 A股三大指数集体上涨](https://www.yicai.com/video/101843398.html)
> 概要: 海关总署全面暂停进口日本水产品 A股三大指数集体上涨
### [杭州口岸迎首批使用杭州亚运会身份注册卡人员](https://finance.sina.com.cn/jjxw/2023-08-24/doc-imziifex9612832.shtml)
> 概要: 转自：北京日报客户端“您好，您的入境手续已办理完毕，欢迎来到杭州！”24日，13名持杭州亚运会身份注册卡的人员抵达杭州萧山国际机场。
### [优酷申请多巴胺恋人商标](http://www.investorscn.com/2023/08/24/109902/)
> 概要: 【#优酷申请多巴胺恋人商标#】......
### [国台办：民进党当局用台湾同胞血汗钱换武器，换不来“安全”](https://finance.sina.com.cn/jjxw/2023-08-24/doc-imziimnv9485138.shtml)
> 概要: 转自：央视新闻客户端8月24日，国台办发言人朱凤莲答记者问。问：据媒体报道，美国政府近日决定向中国台湾地区出售价值约5亿元的军售项目。对此有何评论？
### [华熙生物在山东成立医药科技公司](http://www.investorscn.com/2023/08/24/109903/)
> 概要: 【#华熙生物在山东成立医药科技公司# 注册资本5000万】......
### [流言板周鹏：我们有信心能够证明我们自己，也有信心完成目标](https://bbs.hupu.com/61813640.html)
> 概要: 虎扑08月24日讯 中国男篮队长周鹏在世界杯赛前接受采访表示有信心能够证明中国男篮自己，也有信心完成目标。周鹏接受采访：“我们有信心能够证明我们自己，也有信心能够完成我们的目标。我们准备好了，经过了一
### [流言板心率拉满！瓶子发文：傲寒这心率，冷静！冷静！](https://bbs.hupu.com/61813649.html)
> 概要: 虎扑08月24日讯 傲寒今日比赛心率最大值高达206，解说瓶子等人发文：不过傲寒这心率，冷静！冷静！瓶子：这个乔夫龙虎双边真的不错！不过傲寒这心率冷静！冷静！小鹿：傲寒心率203！！！鸡皮疙瘩起来了！
### [流言板FIBA晒戈贝尔年轻时和现在对比照：鲁迪取得了巨大的进步](https://bbs.hupu.com/61813708.html)
> 概要: 虎扑08月24日讯 FIBA晒戈贝尔年轻和现在对比照：鲁迪取得了巨大的进步。法国世界杯赛程：8月25日21:30对阵加拿大8月27日21:30对阵拉脱维亚8月29日17:45对阵黎巴嫩   来源： T
### [中盐集团声明：保障食盐市场供应](https://finance.sina.com.cn/jjxw/2023-08-24/doc-imziimnt7317098.shtml)
> 概要: 转自：北京日报客户端中盐集团发布关于保障食盐市场供应的声明，具体内容如下。受日本核污染水排海影响，我国部分市场发生食盐抢购现象。
### [微博二季度月活用户5.99亿](https://finance.sina.com.cn/jjxw/2023-08-24/doc-imziimnx6261866.shtml)
> 概要: 转自：北京日报客户端北京日报客户端 | 记者 赵语涵8月24日，微博发布2023年第二季度财报。二季度微博总营收4.402亿美元，约合31.4亿元人民币，剔除汇率因素...
# 小说
### [天魔霸体诀](https://book.zongheng.com/book/1242923.html)
> 作者：孤月流光

> 标签：奇幻玄幻

> 简介：少年沦为皇室争斗的牺牲品，家族老幼惨遭屠戮。若能报仇，成魔又如何？！修天魔霸体诀，斩命官，灭王朝，扫八荒六合，吞九天十地！

> 章节末：第367章 终结

> 状态：完本
# 论文
### [3DVNet: Multi-View Depth Prediction and Volumetric Refinement | Papers With Code](https://paperswithcode.com/paper/3dvnet-multi-view-depth-prediction-and)
> 概要: We present 3DVNet, a novel multi-view stereo (MVS) depth-prediction method that combines the advantages of previous depth-based and volumetric MVS approaches. Our key idea is the use of a 3D scene-modeling network that iteratively updates a set of coarse depth predictions, resulting in highly accurate predictions which agree on the underlying scene geometry.

### [Clickbait Spoiling via Question Answering and Passage Retrieval | Papers With Code](https://paperswithcode.com/paper/clickbait-spoiling-via-question-answering-and)
> 概要: We introduce and study the task of clickbait spoiling: generating a short text that satisfies the curiosity induced by a clickbait post. Clickbait links to a web page and advertises its contents by arousing curiosity instead of providing an informative summary. Our contributions are approaches to classify the type of spoiler needed (i.e., a phrase or a passage), and to generate appropriate spoilers. A large-scale evaluation and error analysis on a new corpus of 5,000 manually spoiled clickbait posts -- the Webis Clickbait Spoiling Corpus 2022 -- shows that our spoiler type classifier achieves an accuracy of 80%, while the question answering model DeBERTa-large outperforms all others in generating spoilers for both types.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
