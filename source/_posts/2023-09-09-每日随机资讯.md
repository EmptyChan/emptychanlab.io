---
title: 2023-09-09-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.AyutthayaTemple_ZH-CN5996587937_1920x1080.webp&qlt=50
date: 2023-09-09 22:59:10
---


> 内容均不涉及转发、复制原文等，仅提供外链和标题聚合（可视做聚合引擎且非商业不盈利），查看详情请拷贝并跳转原始链接。如有侵权，还请告知。

![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.AyutthayaTemple_ZH-CN5996587937_1920x1080.webp&qlt=50)
# 新闻
### [产品设计：列表项删除的移动端交互调研](https://www.woshipm.com/share/5900449.html)
> 概要: 在产品设计中，交互设计是很重要的一个环节。这篇文章，作者调研了几个知名度比较高的产品，看看它们的交互做得怎样，是否有我们可以参考的地方。我们日常使用的软件中，很多细节方面的交互，各大厂商之间其实并没有......
### [7天场观腰斩过半，“入淘”东方甄选路走弯了？](https://www.woshipm.com/it/5900729.html)
> 概要: 前段时间东方甄选开始在淘宝直播，第一场直播就拿下了1亿的GMV，本以为赶上了淘宝的历史性时刻，却没有想到后续的发展并不如人意。“赶得早不如赶得巧，历史性时刻往往是赶得巧的人赶上。”8月29日17:30......
### [解锁“文心一言”赋能大型连锁商超密码](https://www.woshipm.com/share/5900302.html)
> 概要: 前几天百度文心一言正式发布，冲上apple store下载第一，这个国内的大模型发布到现在也有一段时间了，成长得咋样？我们来看看2023年，“AIGC”江湖风起云涌，前有百度“文心一言”对标ChatG......
### [《城市：天际线2》加长预告片深入介绍音频设计](https://www.3dmgame.com/news/202309/3877185.html)
> 概要: Colossal Order和ParadoxEntertainment日前发布了即将推出的城市建造游戏《城市:天际线2》新预告片。继本周早些时候分享的有关音乐和音频的预告片之后，在本次的视频中，我们可......
### [自主研发STAR-T技术，「华夏英泰」如何突破细胞治疗的实体瘤之困？](https://36kr.com/p/2423060800643845)
> 概要: 自主研发STAR-T技术，「华夏英泰」如何突破细胞治疗的实体瘤之困？-36氪
### [Epic Games首席创意管Donald Mustard退休](https://www.3dmgame.com/news/202309/3877190.html)
> 概要: Epic Games 首席创意官 Donald Mustard 宣布将于本月从他的职位上退休。Mustard 于 2005 年 5 月联合创立了ChAIR Entertainment Group，该集......
### [《罗布乐思》9月登陆Quest 10月登陆PS主机](https://www.3dmgame.com/news/202309/3877191.html)
> 概要: 罗布乐思公司（RobloxCorporation）宣布，游戏平台《罗布乐思》将于 9 月晚些时候面向 Meta QuestVR设备发布，随后于 10 月通过 PlayStation Store 发布 ......
### [网友罗马偶遇SEVENTEEN录综艺 成员一起排排站](https://ent.sina.com.cn/tv/zy/2023-09-09/doc-imzmamrk1405715.shtml)
> 概要: 新浪娱乐讯 9月9日，有网友在罗马偶遇SEVENTEEN录制综艺《花样青春》，成员们好像学生旅游团排排站，方便清点人数。(责编：小5)......
### [《泡姆泡姆》Steam页面上线 发售日期待定](https://www.3dmgame.com/news/202309/3877192.html)
> 概要: 今日（9月9日），鹰角网络游戏《泡姆泡姆》Steam页面上线，发售日期待定，感兴趣玩家可以点击此处进入商店页面。游戏介绍：和同伴一起，利用颜色变换、三消射击和各式帮手，在充满未知的异世界里不断前进。此......
### [出海周刊62期｜小红书，怎么就成了出海绝缘体；深圳跨境电商，不再是从前模样](https://36kr.com/p/2423410063532808)
> 概要: 出海周刊62期｜小红书，怎么就成了出海绝缘体；深圳跨境电商，不再是从前模样-36氪
### [组图：高晓松晒自拍判若两人 面容瘦到脱相网友直呼认不出](http://slide.ent.sina.com.cn/star/w/slide_4_704_388717.html)
> 概要: 组图：高晓松晒自拍判若两人 面容瘦到脱相网友直呼认不出
### [上海女性研发人员占比位居全国第二！医学科学领域女性占比最高](https://www.yicai.com/news/101855093.html)
> 概要: 上海女性研发人员数量从2016年的7.03万人增长至2021年的9.83万人，年均增长6.94%。
### [摩洛哥地震遇难人数升至632人](https://www.yicai.com/image/101855147.html)
> 概要: 摩洛哥地震遇难人数升至632人
### [肉蛋价格反弹带动CPI转正，下一阶段怎么走？机构这样看](https://www.yicai.com/news/101855150.html)
> 概要: 8月份，消费市场继续恢复，供求关系持续改善，CPI环比涨幅略有扩大，同比由降转涨。
### [亚运会火炬手白象食品姚忠良：一路践行“冠军之志”](http://www.investorscn.com/2023/09/09/110339/)
> 概要: 亚运会火炬手白象食品姚忠良：一路践行“冠军之志”
### [HMD Global 推出粉色版诺基亚 G42 手机](https://www.ithome.com/0/718/026.htm)
> 概要: IT之家9 月 9 日消息，HMD Global 今天发布新闻稿，表示在《芭比》电影全球爆火的情况下，也顺势推出粉色（So Pink）版诺基亚 G42 5G 手机。HMD 于今年 6 月推出 G42 ......
### [古尔曼称苹果会在 10 月直接发布新款 iPad Air](https://www.ithome.com/0/718/027.htm)
> 概要: IT之家9 月 9 日消息，在最新一期的 The MacRumors Show 播客节目中，该媒体邀请了彭博社的马克・古尔曼（Mark Gurman）到场，探讨苹果下周三“好奇心上头”发布会的新品内容......
### [vivo X100 系列、iQOO12 手机亚运后发布，定位“满分旗舰”“性能王者”](https://www.ithome.com/0/718/041.htm)
> 概要: IT之家9 月 9 日消息，vivo 品牌副总裁、品牌与产品战略总经理贾净东今日下午在个人微博发文，透露了部分关于自家最新产品的消息。其称亚运之后，X100 系列和 iQOO12 系列“就越来越近了”......
### [有备无患更安心：JISSBON 0.66 元 / 枚发车（京东 2 元）](https://lapin.ithome.com/html/digi/718045.htm)
> 概要: 天猫【JISSBON 官方旗舰店】Jissbon 润薄精选三合一 16 枚 + 优质超薄 14 枚官方定价 309.9 元，今日下单立减 40 元 + 可领 250 元优惠券，券后实付 19.9 元包......
### [滴滴App上架后连续增长，中国出行6月日均突破3000万单](http://www.investorscn.com/2023/09/09/110340/)
> 概要: 9月9日，滴滴在其官网发布2023年第二季度业绩报告，二季度滴滴实现总收入488亿元，同比增长52.6%；归属于滴滴普通股股东的净亏损为3亿元，经调整EBITA亏损1000万元......
### [组图：易烊千玺给后援会写新TO签 庆祝后援会成立十周年](http://slide.ent.sina.com.cn/star/w/slide_4_704_388725.html)
> 概要: 组图：易烊千玺给后援会写新TO签 庆祝后援会成立十周年
### [广东提出27项举措扩内需，将推动20个战略性产业集群加快发展](https://www.yicai.com/news/101855190.html)
> 概要: 《方案》提出要从发展新产业新产品、促进传统产业改造提升、推动生产性服务业向高端延伸、加强标准质量品牌建设这四个方面发力，提高供给质量，带动需求更好地实现。
### [2023中国（郑州）国际期货论坛圆满结束：更好发挥期市功能作用，助力中国式现代化建设](https://www.yicai.com/news/101855198.html)
> 概要: 2023中国（郑州）国际期货论坛圆满结束：更好发挥期市功能作用，助力中国式现代化建设
### [外媒：要不是承载太多 《星空》的争议可能会更少](https://www.3dmgame.com/news/202309/3877231.html)
> 概要: 《星空》在正式推出后引起了各种争议。除了优化问题外，游戏玩法、画面等也是玩家讨论的热点。外媒Gamesindustry认为，要不是需要承担Xbox巨大的希望和雄心，《星空》也许将是一个争议少得多的游戏......
### [杭州亚运会丨跟着亚运“薪火”看见美丽湖州](http://www.investorscn.com/2023/09/09/110341/)
> 概要: 新华社杭州9月9日电（记者朱涵、郑梦雨）杭州第19届亚运会火炬传递湖州站活动9日启动。170名来自各行各业的火炬手逐绿而行，感受湖州的生态之美、发展之美......
### [流言板黄健翔点评国字号：还有什么可说的呢？随他们去吧](https://bbs.hupu.com/62050709.html)
> 概要: 虎扑09月09日讯 在国足1-1战平马来西亚、国奥2-1艰难战胜印度后，黄健翔在个人社交平台上表示自己已经无话可说。黄健翔写道：本来想着总得说点什么吧，可是想一想，又觉得：还有什么可说的呢？不点名骂人
### [流言板Doinb：其实佐伊我随时都能选，但有点怕玩了佐伊却吞下首败](https://bbs.hupu.com/62050721.html)
> 概要: 虎扑09月09日讯 2023英雄联盟解说主持杯胜者组决赛，DBG以3-0的比分战胜GBS，晋级决赛舞台。赛后，DBG全体成员接受媒体群访，内容如下：Q：提问Doinb。你今天用了FPX的薇恩冠军皮肤。
### [NBL季后赛半决赛：哈德森48+11，辽宁益胜不敌安徽文一](https://bbs.hupu.com/62050740.html)
> 概要: 虎扑09月09日讯 NBL官方更新微博，发布2023NBL半决赛安徽文一与辽宁益胜的首场比赛战报。官方原文：“2023NBL半决赛安徽文一与辽宁益胜的首场比赛今晚战罢，安徽在主场战至最后一刻，以120
### [流言板谢鹏飞：主教练让我多接球，发挥自己优点组织进攻](https://bbs.hupu.com/62050763.html)
> 概要: 虎扑09月09日讯 国际足球友谊赛，中国1-1马来西亚，赛后中国队球员谢鹏飞接受采访。主教练对你的要求让我多接球，发挥自己优点，组织进攻。以主队身份来到凤凰山的感受感谢这么多球迷为我们加油，很可惜没拿
### [小事情大民生 多方发力持续提升适老化交通出行服务](https://finance.sina.com.cn/jjxw/2023-09-09/doc-imzmctkw4142627.shtml)
> 概要: 转自：央视网在交通运输日益发达的今天，尽管地铁出行速度快、打车出门高效率，但每日穿梭在城市路网的地面公交依然是老年人主要的出行工具。
### [积极信号！知名国际PE在华大动作，设立首只人民币基金，30亿！](https://finance.sina.com.cn/stock/zqgd/2023-09-09/doc-imzmctky0919964.shtml)
> 概要: 9月7日，江苏宜兴市与华平投资正式签约，出资华平投资首只人民币基金，专注于大健康产业投资，新基金正式落户宜兴市，总规模预计30亿元。
### [FIBA：扎加尔斯成FIBA单场助攻纪录保持者，被他圈粉了吗？](https://bbs.hupu.com/62050799.html)
> 概要: 虎扑09月09日讯 篮球世界杯5-6名排位赛：拉脱维亚全场98-63战胜立陶宛。最终，拉脱维亚获得第五，立陶宛第六，斯洛文尼亚第七，意大利第八。扎加尔斯全场7中0，得到4分7篮板17助攻，创造世界杯单
### [建三江在科技增产提质上下功夫 为中国饭碗里装上更多优质粮](https://finance.sina.com.cn/jjxw/2023-09-09/doc-imzmctks5424053.shtml)
> 概要: 转自：央视网 央视网消息：提起黑龙江的粮食生产，一定离不开建三江。建三江每年生产粮食140亿斤左右，手握北大荒核心基地的生产能力...
### [消费市场不断恢复 供求关系持续改善](https://finance.sina.com.cn/jjxw/2023-09-09/doc-imzmctky0920486.shtml)
> 概要: 转自：央视网 央视网消息：通过数据看经济。国家统计局今天（9月9日）发布的数据显示，8月份，消费市场继续恢复，供求关系持续改善...
### [科技赋能助力渔业丰收 新疆尼勒克县实现养殖智能化、加工数字化](https://finance.sina.com.cn/jjxw/2023-09-09/doc-imzmctky0920484.shtml)
> 概要: 转自：央视网 央视网消息：在新疆伊犁尼勒克县，来自天山山脉的冰雪融水水质纯净冷凉，富含溶解氧，为三文鱼的生长创造了有利条件...
# 小说
### [人鬼情未了](http://book.zongheng.com/book/1148739.html)
> 作者：咸青虫二

> 标签：奇幻玄幻

> 简介：《人鬼情未了》简介21世纪华夏国的工科男，生活小康而平淡。虽然他是工科男，但对古代历史与文学较为热衷，在他工作之余，不惜花费了三年的时间，写了一篇《江湖风云录》的网文。不曾想，《江湖风云录》的成功发表，彻底地改写了他的人生轨迹。此工科男做了个奇怪的梦，梦中他似乎得到了神谕，只要他历经九九八十一难，便能修成正果，位列仙班。梦中工科男向天神提出了一个充满人性的现实问题，他能与自己的妻子一起得道升仙吗？工科男得到的回答是一个响雷。这算是回答吗？工科男梦醒了，工科男居然已经身处于他所书写的古代南宋时代。MY GOD，工科男居然不得不与自己《江湖风云录》书中描写的人物并肩奋斗，与天斗，与人斗，与鬼斗，换取自己生存的机会。工科男的内心非常拒绝这样的人生安排，他胸无大志地只想与自己的妻子你侬我侬地相守一生。可是工科男遇到一个又一个美女，这难道就是他的劫难吗？他是姓猪，还是天生桃花命。

> 章节末：第一百章 缘生缘灭

> 状态：完本
# 论文
### [Training Cross-Lingual embeddings for Setswana and Sepedi | Papers With Code](https://paperswithcode.com/paper/training-cross-lingual-embeddings-for)
> 概要: African languages still lag in the advances of Natural Language Processing techniques, one reason being the lack of representative data, having a technique that can transfer information between languages can help mitigate against the lack of data problem. This paper trains Setswana and Sepedi monolingual word vectors and uses VecMap to create cross-lingual embeddings for Setswana-Sepedi in order to do a cross-lingual transfer.
### [Instance-Dependent Partial Label Learning | Papers With Code](https://paperswithcode.com/paper/instance-dependent-partial-label-learning)
> 概要: Partial label learning (PLL) is a typical weakly supervised learning problem, where each training example is associated with a set of candidate labels among which only one is true. Most existing PLL approaches assume that the incorrect labels in each training example are randomly picked as the candidate labels.

> 每日夜间，随机给予一天的信息流，防止信息茧房（后续会接入更多信息源），感谢你的阅读！希望你能够从这边获取更多知识！
