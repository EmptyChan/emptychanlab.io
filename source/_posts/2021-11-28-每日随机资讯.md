---
title: 2021-11-28-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.BeechTrees_ZH-CN9605292244_1920x1080.jpg&rf=LaDigue_1920x1080.jpg
date: 2021-11-28 19:12:26
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.BeechTrees_ZH-CN9605292244_1920x1080.jpg&rf=LaDigue_1920x1080.jpg)
# 新闻
### [AMD 开源驱动让《我的世界》性能提升 30%](https://www.oschina.net/news/171073/minecraft-radeonsi-thread)
> 概要: 在 Linux 上使用开源的 AMD Radeon OpenGL 驱动 "RadeonSI" 的用户，现在在游玩《我的世界》（Minecraft）时，性能将提升 30%。Marek Olšák 是知名......
### [GitHub 宕机超 2 个小时](https://www.oschina.net/news/171077/github-down-outage-service-issues)
> 概要: 根据外媒的报道，GitHub 今天经历了长达两个多小时的宕机，影响了数以千计甚至数百万依赖其众多服务的开发者。报道称，GitHub 在美国东部时间下午 3:45 左右开始出现问题，包括 Git 操作、......
### [英特尔准备为 Linux 提供 USI Stylus 支持](https://www.oschina.net/news/171071/intel-may-support-usi-for-linux)
> 概要: 提交记录显示，英特尔工程师提交了一份为 Linux 内核的 HID 子系统添加USI手写笔支持的征求意见补丁。英特尔的开源驱动工程师一直致力于为 Linux 内核提供 USI 手写笔支持。通用手写笔计......
### [IPython 7.30 发布，为 8.0 版本做好准备](https://www.oschina.net/news/171065/ipython-7-30-released)
> 概要: IPython 是一个综合环境，可以帮助程序员或开发人员等高级计算机用户测试或探索各种功能。尽管 Python 附带了一个强大的交互式解释器，使用户无需在目标计算机上创建额外的文件即可运行测试，但它在......
### [孤僻小伙爱上傲娇级草，疯狂心动的暗恋ing](https://news.dmzj.com/article/72883.html)
> 概要: 2021年11月日本新耽美剧《美丽的他》上线，由萩原利久、八木勇征主演的电视剧，改编自凪良悠的同名BL小说，讲述高中生平良一成暗恋清居奏，奏救赎平良，二人互相吸引，拥有共同的秘密的故事。
### [SQLite Release 3.37.0](https://www.sqlite.org/releaselog/3_37_0.html)
> 概要: SQLite Release 3.37.0
### [组图：58岁甄子丹大秀一字马状态佳 与妻子汪诗诗隔空互动显甜蜜](http://slide.ent.sina.com.cn/star/slide_4_704_364082.html)
> 概要: 组图：58岁甄子丹大秀一字马状态佳 与妻子汪诗诗隔空互动显甜蜜
### [画师创作动漫里的动物分类，不被区分开都不知道大家原来是一国的](https://new.qq.com/omn/20211127/20211127A05NC500.html)
> 概要: 画师创作动漫里的动物分类，不被区分开都不知道大家原来是一国的
### [声优橘田泉在生日当天宣布与意大利人结婚](https://news.dmzj.com/article/72884.html)
> 概要: 声优橘田泉于2021年11月27日在自己的博客中公布了结婚的喜讯，另一半是在伦敦留学期间认识的意大利人。
### [《赛博朋克2077》特别好评后 设计师发文感谢粉丝](https://www.3dmgame.com/news/202111/3829399.html)
> 概要: 在《赛博朋克2077》本周获得玩家好评轰炸后，一名设计师感谢了粉丝。作为Steam秋季促销的一部分，《赛博朋克2077》当前半价促销（149元，原价298元），目前是Steam上“全球热销榜”第一名......
### [元宇宙对网络技术的挑战，什么样的网络才能承载元宇宙的野心？](https://www.tuicool.com/articles/6fEV3mZ)
> 概要: 来源：NASP网络实验室最近“元宇宙”这个词非常火热。信息领域向来擅长制造概念和包装概念，而且这些概念往往都由商业公司发起，“云计算”、“大数据”、“工业互联网”、“元宇宙”等概莫例外。新概念的诞生之......
### [从中国统计年鉴2021年，聊聊我国的低收入群体](https://www.huxiu.com/article/476720.html)
> 概要: 本文来自微信公众号：宁南山（ID：ningnanshan2017），作者：深圳宁南山，头图来自：视觉中国中国的低收入人群集中在哪里？《中国统计年鉴2021》在统计局的官网发布了，里面统计的是2020年......
### [漫改剧《继母与女儿的蓝调》将于明年1月推出新作特别篇](https://news.dmzj.com/article/72885.html)
> 概要: ​根据樱泽铃原作四格漫画改编的真人日剧《继母与女儿的蓝调》将推出特别篇《2022年谨贺新年SP》，预计2022年1月2日播出。
### [“PEOPLE效应”带火CityDAO，一文读懂DAO风口下的新宠儿](https://www.tuicool.com/articles/qAFJ3qr)
> 概要: 前段时间斥资0.25个以太Mint了CityDAO发行的NFT，原因很简单，V神在最近关于加密城市的文章中推荐了这个项目。Emmm，紧跟V神步伐，结果一般不会错。最近几天，随着“PEOPLE效应”放大......
### [酷派 COOL 20 Pro 预热：采用 5000 万像素 AI 三摄](https://www.ithome.com/0/589/241.htm)
> 概要: IT之家11 月 28 日消息，酷派将于 12 月 1 日晚 7 点发布 COOL 20 Pro 新机，今天官方再次对新机进行预热。今天酷派官方对 COOL 20 Pro 的摄像头进行了预热，该机将搭......
### [电视动画「JOJO的奇妙冒险 石之海」更新CAST](http://acg.178.com/202111/432065291299.html)
> 概要: 昨日（11月27日），电视动画「JOJO的奇妙冒险 石之海」更新了CAST，角色为“恩里克·普奇”，声优是关智一。「石之海」是「JOJO的奇妙冒险」系列第六部作品，也是JOJO系列中首部由女性担任主角......
### [Z Library](https://z-lib.org/)
> 概要: Z Library
### [一本书掌握Python强大的绘图库Matplotlib](https://www.tuicool.com/articles/qqAJvuf)
> 概要: Python 科学可视化领域是巨大的，由无数工具组成，从最通用和最广泛使用的工具到更专业和机密的工具。其中一些工具是基于社区的，而另一些则是由公司开发的。有些是专门为 Web 制作的，有些仅适用于桌面......
### [外媒评最受期待PS5新游戏TOP17：《战神5》登顶](https://www.3dmgame.com/news/202111/3829407.html)
> 概要: 近日，外媒PSU评选了17款最受期待的PS5游戏新作，其中有独占大作也有第三方大作，其中索尼圣莫妮卡工作室最新大作《战神：诸神黄昏》登顶PS5最受期待新游戏榜榜首的位置。PS5最受期待新游戏榜TOP1......
### [许魏洲私人物品疑似被拿走 工作室发文警告](https://ent.sina.com.cn/s/m/2021-11-28/doc-ikyamrmy5539371.shtml)
> 概要: 新浪娱乐讯 28日，许魏洲工作室发文称“在许魏洲先生住处门口再次发现个别人员无故逗留，并存在疑似拿走私人物品的行为”，并曝光现场照片，并表示“对于此类无底线不理智的行为，我司在此严重警告相关人员，该行......
### [森下suu公开「指尖相触，恋恋不舍」两张新绘图](http://acg.178.com/202111/432068110517.html)
> 概要: 漫画家·森下suu公开了其创作的漫画作品「指尖相触，恋恋不舍」的两张新绘图。「指尖相触，恋恋不舍」（ゆびさきと恋々）是日本漫画家森下suu创作的漫画作品，讲述了自从男女主角相遇之后，他们的世界出现了改......
### [卡通风格4X策略游戏新作《先民》公布实机预告片](https://www.3dmgame.com/news/202111/3829410.html)
> 概要: Gathering Tree是一家专注于制作内容向游戏作品的独立开发商。他们目前正在开发策略模拟类新作《先民》（TFM: The First Men）。开发商现已发布首支实机预告片。本作预计将于202......
### [腾讯财付通回应外汇业务违规被罚278万：已完成整改](https://www.tuicool.com/articles/mm2iiiB)
> 概要: 据科创板日报消息，近日，财付通外汇业务因违规被罚没278万元，对此，财付通回应称，针对2019-2020年例行检查中发现的问题，财付通已第一时间制定了改进计划，并逐项落实，目前已全部完成整改。后续将在......
### [电视动画「群青的幻想曲」追加声优——水瀬いのり](http://acg.178.com/202111/432070258863.html)
> 概要: 电视动画「群青的幻想曲」（群青のファンファーレ）公布了新追加的声优——水瀬いのり（水濑祈），她将为“以成为能赚钱的骑士为目标的有胆量的少女”霜月えり配音。本作将于2022年春季开始播出。STAFF监督......
### [工作7年，30岁考研再出发，如何迈出这一步？](https://www.huxiu.com/article/476726.html)
> 概要: 本文来自微信公众号：奴隶社会（ID：nulishehui），作者：Miranda（85 后诺友，国企 7 年，30 岁再出发，成功申请香港科技大学大数据专业硕士，目前是一名资深数据产品经理），题图来自......
### [年轻人困在周报里，“巻王”藏在其中](https://www.huxiu.com/article/476722.html)
> 概要: 作者：张 琳、曹 杨、冯晓亭、邓双琳、赵晨希、谢中秀、孔月昕、闫俊文，编辑 ：谢中秀，题图来自：视觉中国几位互联网员工在一起吐槽工作，周报必然是最热的话题，因为，其中藏着“巻王”。很多公司都有周报，甚......
### [互联网大厂又开始卷了，这次是NFT](https://www.huxiu.com/article/474154.html)
> 概要: 出品｜虎嗅金融组作者｜周舟头图｜视觉中国NFT这把火，从国外烧到了国内，终于有了燎原之势。11月，又有两家互联网公司推出了NFT项目。虎嗅发现，从今年6月蚂蚁集团吹响第一声号角后，腾讯、字节、小红书、......
### [漫画「打了三百年的史莱姆，不知不觉就练到了满级」第10卷封面公开](http://acg.178.com/202111/432076254404.html)
> 概要: 漫画「打了三百年的史莱姆，不知不觉就练到了满级」公开了第10卷的封面图，该卷将于12月10日发售。漫画「打了三百年的史莱姆，不知不觉就练到了满级」改编自森田季节创作、红绪插画的同名轻小说作品，由シバユ......
### [汪小菲发文向黑人陈建州道歉  否认曾发生过争执](https://ent.sina.com.cn/s/m/2021-11-28/doc-ikyamrmy5563636.shtml)
> 概要: 新浪娱乐讯 11月28日，汪小菲在微博发文向黑人陈建州道歉。他称自己和陈建州是很好的朋友，和范玮琪也是很好的姐妹，并未因任何价值观问题发生过争执，信息不对称导致家人的误解。 此前，汪小菲母亲在直播中透......
### [厂商宣布《盗贼之海》第五赛季将于12月2日正式启动](https://www.3dmgame.com/news/202111/3829423.html)
> 概要: Rare近日正式宣布，《盗贼之海》第五赛季将于12月2日正式开始，距离9月23日开始的第四赛季间隔不到3个月的时间。《盗贼之海》通过官方推特宣布这一消息，还附带了玩家在读取游戏时将会看到的第五赛季标题......
### [蔡昉：三个分配领域都可以获得真金白银的改革红利](https://finance.sina.com.cn/roll/2021-11-28/doc-ikyamrmy5574536.shtml)
> 概要: 原标题：蔡昉：三个分配领域都可以获得真金白银的改革红利 来源：上海证券报 上证报中国证券网讯（记者 李苑）中国社会科学院原副院长、国家高端智库首席专家...
### [国家卫健委：全国累计接种新冠病毒疫苗248289.5万剂次](https://finance.sina.com.cn/china/2021-11-28/doc-ikyakumx0754465.shtml)
> 概要: 新冠病毒疫苗接种情况 截至2021年11月27日，31个省（自治区、直辖市）和新疆生产建设兵团累计报告接种新冠病毒疫苗248289.5万剂次。
### [Terra (YC W21) Is Hiring for Graduate Software Engineers in London](https://www.ycombinator.com/companies/terra/jobs/EWMMj0v-full-stack-developer-graduate)
> 概要: Terra (YC W21) Is Hiring for Graduate Software Engineers in London
### [王腾：小米一直在寻求线下新模式，不依靠门店推销取胜](https://www.ithome.com/0/589/268.htm)
> 概要: IT之家11 月 28 日消息，小米公司 Redmi 产品总监王腾近日一直在小米多地的小米门店进行探访巡查，不断跟小米之家的伙伴们学习和讨教。也正因此，他在这个过程中透露出了一些关于小米线下和运营的信......
### [多个疫苗厂家已着手应对奥密克戎，物理防护仍有效](https://finance.sina.com.cn/jjxw/2021-11-28/doc-ikyamrmy5582218.shtml)
> 概要: 世卫组织26日则强调，依据科学家对新毒株基因序列的研究情况，“需要几个星期了解这一变种对任何在研疫苗的影响”。目前，多个疫苗研发厂家先后表示...
### [各大平台的年番，你认为最好看的是哪一部！](https://new.qq.com/omn/20211127/20211127A05C0400.html)
> 概要: 以前大多数作品都是分季放送（季番)，如今国漫的数量在不断增加，在《斗罗大陆》开了个不错的头之后，有越来越多的作品升级成年番， 年番是指连载一年的动画，不断更的国漫鹅其实有很多，三五分钟一集的那种，今天......
### [钟南山、张文宏对新冠病毒变异毒株Omicron作出最新研判](https://finance.sina.com.cn/jjxw/2021-11-28/doc-ikyakumx0761199.shtml)
> 概要: 原标题：钟南山、张文宏对新冠病毒变异毒株Omicron作出最新研判 钟南山：变异株危害性还需要一段时间研判 11月28日，中国罕见病联盟呼吸病学分会第一届全国会议在广州举行...
### [2022年度国考今日开考 建党百年与新冠疫苗等内容入题](https://finance.sina.com.cn/china/2021-11-28/doc-ikyamrmy5587541.shtml)
> 概要: 原标题：2022年度国考今日开考 建党百年与新冠疫苗等内容入题 11月28日，2022年国家公务员考试笔试在全国30个地区考点同时举行。
### [手游《暗黑破坏神：不朽》内测下载通道开启，29 日精英试炼开服](https://www.ithome.com/0/589/271.htm)
> 概要: 感谢IT之家网友iryke的线索投递！IT之家11 月 28 日消息，由暴雪、网易联合开发的手游《暗黑破坏神：不朽》此前已经开启精英试炼测试申请，内测即将于 11 月 29 日 11:00 正式开服......
### [内蒙古满洲里新增新冠肺炎确诊病例17例](https://finance.sina.com.cn/china/2021-11-28/doc-ikyakumx0765181.shtml)
> 概要: 记者从内蒙古满洲里市新冠肺炎疫情防控新闻发布会了解到，11月27日下午4时至28日中午12时，满洲里新增确诊病例17例，无症状感染者1例。
### [Rita在书店拍写真，粉丝称之为LPL第一美人，小钰的女仆装却能完胜她](https://new.qq.com/rain/a/20211128A05AOQ00)
> 概要: 如果说到LPL当中哪一位工作人员的颜值最高？相信一定会有观众说出Rita、小钰、余霜等人的名字，的确这几位都是当前LPL赛事当中的美女工作人员，她们有的是解说人员，有的时候是赛事主持人员。近期，Rit......
### [《FIFA 22》玩家提前退出比赛 赶到英超攻入闪电进球](https://www.3dmgame.com/news/202111/3829436.html)
> 概要: 在《FIFA 22》国际大赛线上资格赛的过程中，有一位玩家提前退出了比赛。原来这位玩家要赶着去现实生活中的英超联赛进球。利物浦前锋迪奥戈·若塔参加了《FIFA 22》的线上资格赛，但发现如果踢满全场的......
### [CDPR 负责人：《巫师》《赛博朋克 2077》系列 IP 有大计划，明年到来](https://www.ithome.com/0/589/277.htm)
> 概要: IT之家11 月 28 日消息，据外媒 Aroged 消息，游戏公司 CDPR 的负责人 Adam Kichinski 在接受波兰媒体采访时表示，该公司对《巫师》和《赛博朋克 2077》系列有很大的计......
### [爱很美味：比起谁强谁弱，一起成长才是真爱！](https://new.qq.com/rain/a/20211128V0655300)
> 概要: 爱很美味：比起谁强谁弱，一起成长才是真爱！
### [同样是忍者的基本功，为何忍术和体术频繁被使用，幻术却很少见？](https://new.qq.com/omn/20211127/20211127A044AF00.html)
> 概要: 忍者三大基本功中，为何忍术和体术频繁使用，幻术却越来越少见？在动漫火影忍者中，忍者有三项必须要掌握的基本功，分别是忍术，体术还有幻术，在卡卡西对第七班的第一项考试中，卡卡西就分别展示了这三种能力，可是......
### [Measuring Software Complexity: What Metrics to Use?](https://thevaluable.dev/complexity-metrics-software/)
> 概要: Measuring Software Complexity: What Metrics to Use?
### [组图：梦回都敏俊！金秀贤晒怼脸自拍卷毛呆萌](http://slide.ent.sina.com.cn/star/k/slide_4_704_364104.html)
> 概要: 组图：梦回都敏俊！金秀贤晒怼脸自拍卷毛呆萌
### [《追光吧》热度稳坐第一，2大戏骨、2大黑马，撑起舞台，不服不行](https://new.qq.com/rain/a/20211128A06Q2K00)
> 概要: 播出两期，拿了两次综艺热度第一！            这就是《追光吧》的实力，其实《追光吧》拿到这个成绩，也是应该的。毕竟光看嘉宾阵容，这档综艺就赢了。21个哥哥，虽然大多数人一开始都是冲着张卫健、......
### [韩雪崩溃大哭，作品遭评委不留情面怒批，女神跨界惨败意料之中](https://new.qq.com/rain/a/20211128A06QBY00)
> 概要: 近日，久未露面的韩雪因为参加某综艺节目担任导演，处女作短片《超时未送达》播放后饱受争议，不少网友纷纷吐槽韩雪完全不懂真正的穷人生活，大家纷纷将炮火对准韩雪，影评人直接开怼制片人接连批评韩雪，当天已经情......
### [一组画技十分绚丽的国产插画作品！画风多样值得欣赏](https://new.qq.com/omn/20211128/20211128A06RL400.html)
> 概要: 今天小编给大家推荐一组画技出色的国产插画作品，其实看题材不少朋友就能看出这是一组国产插画作品，虽然有一些日风题材但是无论从画技还是角色很多都有国产动漫游戏的影子，这组作品画技没的说，非常绚丽，喜欢动漫......
### [组图：许昕晒儿子玩具车 父子坐地上同款傻笑温馨可爱](http://slide.ent.sina.com.cn/star/w/slide_4_704_364105.html)
> 概要: 组图：许昕晒儿子玩具车 父子坐地上同款傻笑温馨可爱
# 小说
### [不灭亡者](http://book.zongheng.com/book/998875.html)
> 作者：郁之寒

> 标签：武侠仙侠

> 简介：寒孝死后化为厉鬼，获得修鬼之道，重新凝聚肉神，化身为人，隐匿在修真界，逆天而行，踩着那些想杀他之人的尸体，成魔成神，傲啸万界。黑衣黑发的少年，一剑横亘在宇宙之间，眼眸如渊，凝望着遥远的一处冰寒，那是他的心魔，也是他的爱人。不知道经历了多少个万年，依旧无法摆脱宿命的纠葛。“我来了！”生或死，就是现在…

> 章节末：第五百五十一章  天发杀机（终）

> 状态：完本
# 论文
### [Edge-similarity-aware Graph Neural Networks | Papers With Code](https://paperswithcode.com/paper/edge-similarity-aware-graph-neural-networks)
> 日期：20 Sep 2021

> 标签：None

> 代码：None

> 描述：Graph are a ubiquitous data representation, as they represent a flexible and compact representation. For instance, the 3D structure of RNA can be efficiently represented as $\textit{2.5D graphs}$, graphs whose nodes are nucleotides and edges represent chemical interactions.
### [Practical Galaxy Morphology Tools from Deep Supervised Representation Learning | Papers With Code](https://paperswithcode.com/paper/practical-galaxy-morphology-tools-from-deep)
> 日期：25 Oct 2021

> 标签：None

> 代码：None

> 描述：Astronomers have typically set out to solve supervised machine learning problems by creating their own representations from scratch. We show that deep learning models trained to answer every Galaxy Zoo DECaLS question learn meaningful semantic representations of galaxies that are useful for new tasks on which the models were never trained.
