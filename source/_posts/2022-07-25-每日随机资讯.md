---
title: 2022-07-25-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.DolbadarnCastle_ZH-CN5397592090_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30
date: 2022-07-25 22:26:08
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.DolbadarnCastle_ZH-CN5397592090_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&qlt=30)
# 新闻
### [爱、流量与虚拟偶像](https://www.woshipm.com/it/5538291.html)
> 概要: 编辑导语：随着科技和互联网的发展，虚拟技术也在不断地发展，虚拟偶像的风潮受到众多年轻人追捧。本文作者分享了当下虚拟偶像的发展以及流量与虚拟偶像、元宇宙之间的关联，感兴趣的一起来看看吧。“技术宅拯救世界......
### [身为产品经理，这几件事不建议你做！](https://www.woshipm.com/pmd/5538417.html)
> 概要: 编辑导语：在职场生活中，不同的认知占位，驱动不同的问题解决思路，而这些职场经历带给我们的启发，关键在于能否帮助我们建立多元化的认知。本文作者分享了七件不建议产品经理做的事情，希望能给你带来帮助。写作，......
### [从三顿半、泡泡玛特等7个经典案例，拆解内容营销的5大趋势！](https://www.woshipm.com/marketing/5538181.html)
> 概要: 编辑导语：企业如果想抢先“占领”用户心智，可以从那些方面着手进行布局？也许，你需要找到适合自己的内容营销策略，比如建立超级用户思维，打造深入人心的内容IP。本篇文章里，作者就未来内容营销的可能趋势进行......
### [LOGO設計02](https://www.zcool.com.cn/work/ZNjEwNzQzMDQ=.html)
> 概要: ———————年年有風  風吹年年慢慢即漫漫日日無事  事複日日忙忙亦茫茫———————————Thanks for watching // ♡......
### [【docker专栏7】容器自启动与守护进程停止后容器保活](https://segmentfault.com/a/1190000042216589)
> 概要: 本文为大家介绍容器自启动以及docker 守护进程挂掉或者docker升级的情况下，如何保证容器服务的正常运行。主要包含三个部分一、守护进程开机自启在我们安装docker的时候，介绍过启动docker......
### [精读《Flip, Fibonacci, AllCombinations...》](https://segmentfault.com/a/1190000042216640)
> 概要: 解决 TS 问题的最好办法就是多练，这次解读type-challengesMedium 难度 49~56 题。精读Flip实现Flip<T>，将对象T中 Key 与 Value 对调：Flip<{ a......
### [今҈天҈真҈是҈热҈化҈了҈！](https://segmentfault.com/a/1190000042217446)
> 概要: 欢迎关注我的公众号：前端侦探今天真是太热了，不信你看标题今҈天҈真҈是҈热҈化҈了҈！也可以查看下面截图相信大家最近都能看到这样“热化了”的标题。那么，这是如何实现的呢？CSS 可以实现类似的效果吗......
### [黑色四叶草：最终篇章来袭，尤诺会有何表现？阿斯塔成帝已成必然](https://new.qq.com/omn/20220721/20220721A03BOW00.html)
> 概要: 黑色四叶草漫画复刊在即，对于很多黑四迷来说，随着复刊的日子逐渐临近，对于最终篇章的开启，也是充满了好奇，对于不少黑四来说，除了主角阿斯塔的表现值得期待外，以及魔法帝尤里乌斯与终极反派卢修斯的关系之外，......
### [腾讯阿里踩过的坑，字节又跳了进去](https://www.huxiu.com/article/616853.html)
> 概要: 出品｜虎嗅科技组作者｜丸都山头图｜视觉中国精简互联网，投身硬科技，这还是那个我们熟悉的字节吗？7月21日，据界面新闻从字节跳动内部人士获悉，CEO梁汝波将个人OKR中的一项更新为“大幅降低2022—2......
### [说“AI有情感”，谷歌工程师被解雇：违反保护产品信息的公司政策](https://finance.sina.com.cn/tech/it/2022-07-25/doc-imizmscv3384082.shtml)
> 概要: 【环球时报综合报道】此前称“AI有情感”遭停职的谷歌工程师近日已被解雇。据英国广播公司23日报道，谷歌于22日宣布解雇一位名叫布莱克·莱莫因的工程师，理由是该工程师违反保护产品信息、确保数据安全的公司......
### [飞信落幕！运营15年，注册用户达5亿，中国移动宣布和飞信将停止服务](https://finance.sina.com.cn/tech/tele/2022-07-25/doc-imizirav5274820.shtml)
> 概要: 每经编辑 毕陆名......
### [大众为何解雇CEO迪斯？失去员工支持，在中国市场表现不佳](https://finance.sina.com.cn/tech/it/2022-07-25/doc-imizmscv3388399.shtml)
> 概要: 编译 / 维金......
### [「咒术回战」禅院真希手办登场](http://acg.178.com/202207/452712257506.html)
> 概要: 寿屋作品「咒术回战」禅院真希手办现已开订，手办全长约210mm，主体采用PVC材料制造，原型师为Koei Matsumoto。该手办售价为11,760日元（含税），约合人民币582元，预计将于2023......
### [《辐射》真人剧新片场照泄露 动力装甲避难所等](https://www.3dmgame.com/news/202207/3847714.html)
> 概要: 近日，游戏改编真人剧集《辐射》全新片场照流出，其中包括身穿避难所连体衣的神秘角色、避难所内部场景搭设、钢铁兄弟会及原子猫组织使用的T-60型号动力装甲。据悉，亚马逊公司于今年7月开始拍摄本剧，其中该剧......
### [视频：网传李沁试金鹰女神礼服 杨紫谭松韵娜扎都无缘](https://video.sina.com.cn/p/ent/2022-07-25/detail-imizmscv3406323.d.html)
> 概要: 视频：网传李沁试金鹰女神礼服 杨紫谭松韵娜扎都无缘
### [厉害啊，国产剧又开拓了新的高概念](https://new.qq.com/rain/a/20220725A02RWI00)
> 概要: 提到平行时空的概念，就不得不提到著名的波兰电影《机遇之歌》。影片将“赶火车”作为时间节点，让男主角的人生被分出三条大相径庭的轨迹。借由“搭火车”这一行为，电影构建出了三条命运的分轨：男主角赶上火车与否......
### [网友投票喜欢的P.A.WORKS作品 7月第4周新闻汇总](https://news.dmzj.com/article/75038.html)
> 概要: 这次准备的是网友投票喜欢的P.A.WORKS作品，漫画《异世界AV》发售单行本，《来自深渊》娜娜奇等身大手办，网友评选偶像级可爱女声优，最后还是惯例的新作相关消息。
### [艺术馆系列-山鹃仙骥](https://www.zcool.com.cn/work/ZNjEwNzk2MDA=.html)
> 概要: 山鹃 · 仙骥宁波侵尘文化传媒的文化墙插画设计......
### [「幽零幻镜」第四弹协作曲无字幕MV公开](http://acg.178.com/202207/452719531615.html)
> 概要: 由汤浅政明监制，Science SARU负责制作的原创电视动画「幽零幻镜」于近期公开了第四弹协作曲「ココロヤミ」的无字幕MV，动画已于7月3日开始播出。「ココロヤミ」无字幕MVCAST：ベリィ：川勝未......
### [「侦探已经死了。」第二季制作决定PV公开](http://acg.178.com/202207/452719734671.html)
> 概要: 根据二语十创作的同名轻小说改编，ENGI负责制作的电视动画「侦探已经死了。」近日公开了动画第二季的制作决定PV，轻小说第七卷将于8月25日发售。「侦探已经死了。」第二季制作决定PVCast君塚君彦：长......
### [要开空调需加费，网约车司乘“空调之战”如何平息？](https://finance.sina.com.cn/tech/internet/2022-07-25/doc-imizirav5308524.shtml)
> 概要: 近期，关于网约车司机和乘客的“空调之战”频频见诸报端，引发热议......
### [《海贼王》最终章连载开启 尾田对谈青山刚昌](https://www.3dmgame.com/news/202207/3847731.html)
> 概要: 人气少年奇幻漫画《海贼王》在连载了长达25周年之际，于7月25日今天迎来了最终章启动，漫长冒险期间埋下的无数秘密和伏笔都将一一揭晓，敬请期待。•《海贼王》的作者尾田荣一郎之前表示，接下来会短暂的收尾“......
### [动画《侦探已经死了》第二季制作决定](https://news.dmzj.com/article/75040.html)
> 概要: TV动画《侦探已经死了》宣布了第二季制作决定的消息。本作的宣传图和制作决定PV也一并公开。有关这次第二季的更多消息，还将于日后公开。
### [轻小说「Liar·Liar」宣布制作动画](http://acg.178.com/202207/452721566548.html)
> 概要: 轻小说「Liar·Liar」宣布动画化企划正在进行中，新情报将于近日公布。本作最新卷第11卷于今日（7月25日）正式发售。「Liar·Liar」是久追遥希创作、konomi插画的轻小说作品，由MF文库......
### [黑龙江（福州）数字经济投资对接会召开](http://www.investorscn.com/2022/07/25/102003/)
> 概要: 创新驱动新变革，数字引领新格局。7月23日，一场风云际会的中国数字领域盛会——第五届数字中国建设峰会如约而至......
### [富坚义博推特更新角色头像草稿 酷拉皮卡登场](https://acg.gamersky.com/news/202207/1502612.shtml)
> 概要: 日本漫画家富坚义博一直在推特持续更新漫画草稿，在今天（7月25日）的更新中，出现了酷拉皮卡的头像。
### [大厂云业务调整，新一轮战争转向](https://www.tuicool.com/articles/26jAnqz)
> 概要: 大厂云业务调整，新一轮战争转向
### [年轻人没有忘记“校园贷”](https://www.huxiu.com/article/617418.html)
> 概要: 两个月前，罗敏在他的抖音直播首秀上说，自己和新东方的俞敏洪老师一样，在做业务转型。有网友不客气的评论道：“你和俞敏洪没有可比性。学生们去新东方学英语交学费就行，可没有利息。”本文来自微信公众号：市界（......
### [QBot通过DLL侧载方式感染设备](https://www.tuicool.com/articles/3y2ARnQ)
> 概要: QBot通过DLL侧载方式感染设备
### [“海王”扮演者杰森莫玛车祸 与电单车相撞未受伤](https://ent.sina.com.cn/s/u/2022-07-25/doc-imizirav5319067.shtml)
> 概要: 新浪娱乐讯 据外媒，“海王”扮演者杰森·莫玛昨日（24日）在驾驶途中遇上车祸。据悉他当时驾着黄色古董跑车在位于加州的一条峡谷道路，期间在转弯时却与反方向的一辆电单车相撞，有指是电单车司机切线驶入杰森·......
### [富坚义博发布“酷拉皮卡”新草稿：终于有能看懂的了](https://www.3dmgame.com/news/202207/3847737.html)
> 概要: 漫画家富坚义博自从开设推特账号以来，就一直在更新自己创作漫画的草稿进度，今天（7月25日）富坚义博发布了《全职猎人》中角色“酷拉皮卡”的新草稿。对此网友们也是感叹到：终于有能看懂的了，此前富坚义博发布......
### [动画《神渴望着游戏。》公开制作阵容](https://news.dmzj.com/article/75043.html)
> 概要: 目前正在进行动画化企划的《神渴望着游戏。》公开了动画的制作阵容。本作将由LIDEN FILMS负责制作，白石达也担任导演，NTL负责编剧。
### [粉丝向 夏普发售梶裕贵专用空气净化器定制声音](https://www.3dmgame.com/news/202207/3847738.html)
> 概要: 家用电器巨头 夏普于7月25日宣布，邀请人气声优梶裕贵参与设计，发售梶裕贵专用空气净化器定制声音《COCORO VOICE》，对于喜欢梶裕贵众多配音角色声优的粉丝们来说无异于好消息。•如今的家电技术成......
### [创意摄影 | 蜂电 ✖ foodography](https://www.zcool.com.cn/work/ZNjEwODI5MzI=.html)
> 概要: 创意摄影 | 蜂电 ✖ foodography
### [品类观察｜暑期黄金档，开发者可重点布局这四个赛道](http://www.investorscn.com/2022/07/25/102008/)
> 概要: 为帮助开发者更直观判断各品类的发展潜力，穿山甲联合易观分析、巨量算数共同推出《IAA行业品类发展洞察系列季度报告》，通过行业规模、商业变现、品类热度多维数据指标综合评估各细分领域的季度发展状况，并针对......
### [2089:边境概念宣传pv](https://www.zcool.com.cn/work/ZNjEwODQ1MzI=.html)
> 概要: 2089:边境概念宣传pv
### [直击ROBOCON2022赛场，追觅科技联手组委会成立创新基金](http://www.investorscn.com/2022/07/25/102013/)
> 概要: 7月24日，第二十一届全国大学生机器人大赛ROBOCON圆满结束......
### [手游《Heaven Burns Red》将推出Steam版！支持4K画面](https://news.dmzj.com/article/75046.html)
> 概要: 由Wright Flyer Studios和Key制作的手游《Heaven Burns Red》宣布了将要推出面向日本区服务的Steam版的消息。
### [韩国音乐人尹钟信确诊感染新冠 症状轻微自主隔离](https://ent.sina.com.cn/y/yrihan/2022-07-25/doc-imizmscv3450091.shtml)
> 概要: 新浪娱乐讯 韩国著名音乐人尹钟信确诊感染了新冠肺炎。　　据经纪公司透露，尹钟信在本月24日进行自我检测出现阳性反应，随后他第一时间到医院接受检测，确诊感染了新冠肺炎。　　经纪公司表示，尹钟信目前只有一......
### [IDC中国数字金融论坛助力金融机构实现开放、智慧、信任](http://www.investorscn.com/2022/07/25/102014/)
> 概要: 北京，2022年7月25日——随着后疫情时代不确定性的增加，金融行业面临诸多新挑战：如何以数字优先、客户体验为中心，通过业务模式的创新满足客户日趋个性化金融服务的需求；如何以技术驱动场景，重新定义金融......
### [网友票选动漫界最惹不起的美女 娇美人妻都是坑](https://acg.gamersky.com/news/202207/1502527.shtml)
> 概要: 在各式各样题材的动漫作品中，有着各种有趣的人物，美女不一定都是弱不禁风，甚至还有强大的杀伤力，惹到她们绝对不是断两根肋骨就能解决的事情。
### [“阿尔兹海默症”开山之作造假，恐误导全球16年](https://www.huxiu.com/article/616297.html)
> 概要: 本文来自微信公众号：量子位 （ID：QbitAI），作者：杨净、明敏，原文标题：《开山之作造假！Science大曝Nature重磅论文学术不端，恐误导全球16年》，头图来自：视觉中国2006年在Nat......
### [太突然！热血韩漫《我独自升级》作者因脑出血去世 ​​​​](https://acg.gamersky.com/news/202207/1502751.shtml)
> 概要: 据媒体报道，《我独自升级》漫画主笔Jang Sung-rak(Dubu)因脑出血去世，网友们纷纷表示这个消息真是太突然了，祝愿他一路走好。
### [小米 12S Ultra 推送 MIUI 13.0.5 更新：新增相册“那年今日”，优化相机和无线快充](https://www.ithome.com/0/631/446.htm)
> 概要: IT之家7 月 25 日消息，今天，小米王化表示，小米 12S Ultra 开始推送 MIUI 13.0.5.0.SLACNXM 稳定版更新，大小为 740MB，没有收到的朋友别担心，今天是开始小规模......
### [苹果再次被三星挖墙脚，这次是一名半导体芯片专家](https://www.ithome.com/0/631/448.htm)
> 概要: 7 月 25 日消息，据国外媒体报道，苹果和三星之间的关系，略为复杂，两家公司在消费电子产品领域是彼此最大的竞争对手，但同时两家公司也是合作伙伴，苹果从三星采购了包括 OLED 面板在内的诸多关键零部......
### [「全域」到底是什么？](https://www.tuicool.com/articles/a2iyAfi)
> 概要: 「全域」到底是什么？
### [显卡暴跌击穿发售价 华强北矿机店快跑光了](https://www.3dmgame.com/news/202207/3847760.html)
> 概要: 显卡最近这段时间的行情暴跌，也让线下炒卡、从事矿机的经销商们苦不堪言。媒体报道称，赛格大厦前两年卖矿机、显卡的诸多店面如今都撤了，还有的改行卖安防等产品了。华强北电子世界4层只见一家卖显卡的店面，广州......
### [美国基金之星：为全球华人开启财富升级新机遇](http://www.investorscn.com/2022/07/25/102018/)
> 概要: 在财富圈有一个魔咒般的二八法则，也就是俗知的帕累托法则，1906 年由意大利经济学家帕累托最早立论，后经风投家理查德·科赫（Richard Koch）的畅销书《80/20原则》广为流传：20％ 的人口......
### [6月结构性存款规模创年内新低，收益率或仍将下行](https://www.yicai.com/news/101485112.html)
> 概要: 这背后或反映了随着经济逐步复苏，企业投资意愿增强。
### [独家|WTO农业谈判新征途！发展中国家这样强调](https://www.yicai.com/news/101485139.html)
> 概要: 中方表示，目前不平衡的农产品贸易规则是在20世纪80-90年代制定的，不足以应对世界的深刻变化，特别是在粮食安全危机方面的威胁。
### [不会用筷子，直接上手！巴黎小将卡利穆恩多在日本体验日料时的尴尬一幕。                卡利穆恩多上赛季被巴黎租借至朗斯踢球，出战38场比赛，攻入13球](https://bbs.hupu.com/54896865.html)
> 概要: 不会用筷子，直接上手！巴黎小将卡利穆恩多在日本体验日料时的尴尬一幕。                卡利穆恩多上赛季被巴黎租借至朗斯踢球，出战38场比赛，攻入13球
### [8月主线如何预判？](https://www.yicai.com/news/101485109.html)
> 概要: 8月主线如何预判？
### [深入解读国家公路网规划：这些城市将直接受益](https://finance.sina.com.cn/jjxw/2022-07-25/doc-imizmscv3492871.shtml)
> 概要: 我国公路网中最高层次的路网——国家公路网的中长期专项规划已经绘就，这将有力支撑建设全国统一大市场、畅通国内国际双循环，助力实现人享其行、物畅其流。
### [强强联手！北大、剑桥合作开办前海中英研究院，开展国际研究生培养和高端在职培训教育](https://finance.sina.com.cn/china/dfjj/2022-07-25/doc-imizmscv3492806.shtml)
> 概要: 原标题：强强联手！北大、剑桥合作开办前海中英研究院，开展国际研究生培养和高端在职培训教育 21世纪经济报道记者王帆 深圳报道 7月25日，记者从前海管理局获悉...
### [经纬股份：7成收入依赖国企，应收账款急速攀升 | IPO速递](https://www.tuicool.com/articles/fi67Zvz)
> 概要: 经纬股份：7成收入依赖国企，应收账款急速攀升 | IPO速递
### [联想 GeekPro 2022 台式机推出新配置：i5 + RTX 3050 OEM 版，5999 元](https://www.ithome.com/0/631/498.htm)
> 概要: IT之家7 月 25 日消息， 今年 2 月底，联想推出了 2022 款的 GeekPro 2022 台式机，采用了全新的设计，搭载了 12 代酷睿处理器和英伟达 GTX 16 和 RTX 30 系列......
### [数商生态“扩容”，上海数交所与12家机构达成战略合作](https://finance.sina.com.cn/roll/2022-07-25/doc-imizirav5382199.shtml)
> 概要: 本次达成的战略合作主要围绕五个方面展开。 7月25日下午，上海数据交易所在“元宇宙大厅”举行“全能力”战略合作签约仪式，分别与上海联和投资有限公司...
### [数商生态“扩容”，上海数交所与12家机构达成战略合作](https://www.yicai.com/news/101485311.html)
> 概要: 本次达成的战略合作主要围绕五个方面展开。
### [社论：畅通外贸面对面沟通渠道比“见字如晤”更重要](https://www.yicai.com/news/101485315.html)
> 概要: 包机寻找海外市场保订单、抢订单，对企业来说是属于强烈的内在需求，对政府来说是主动责任担当。这既是授之以鱼，也是授之以渔的过程，后者更为重要。
### [科学教育要教什么？钟南山：引导孩子多问“为什么”](https://finance.sina.com.cn/china/2022-07-25/doc-imizmscv3496935.shtml)
> 概要: 原标题：科学教育要教什么？钟南山：引导孩子多问“为什么”“也要培养学生扎实的基本功” 随着科学、技术与知识的迅速迭代，科学教育迎来了新的发展机遇。
### [流言板王哲林为中国男篮加油：倾尽全力，我最热爱的中国男篮](https://bbs.hupu.com/54897459.html)
> 概要: 虎扑07月25日讯 今日，中国男篮球员王哲林更新个人微博，转发中国篮协鼓励视频，为队伍加油。微博原文：“倾尽全力，我最热爱的中国男篮。”根据安排，中国男篮接下来将赴欧洲拉练，以备战世预赛第四窗口期比赛
### [流言板媒体人：中国足球应珍惜戴伟浚，黄紫昌是前车之鉴](https://bbs.hupu.com/54897462.html)
> 概要: 虎扑07月25日讯 戴伟浚堪称近年中国足坛最火热的新星之一，在昨晚东亚杯国足对阵日本队的比赛中，他的带球摆脱动作令球迷们欣喜。对此，《足球》报国内部主任李璇认为中国足球毁人毁人不倦，我们应该珍惜戴伟浚
### [上海第二批集中供地：陆家嘴47.7亿元竞得浦东川沙宅地](https://finance.sina.com.cn/jjxw/2022-07-25/doc-imizmscv3497093.shtml)
> 概要: 7月25日，上海第二批集中供地开拍首日，浦东新区川沙新镇城南社区PDP0-0706单元C06-03地块吸引三位竞买人竞拍，最终陆家嘴（600663.SH）拥有的上海佳川置业有限公司以47...
### [流言板穆尼耶：我是内马尔的超级球迷，但他在巴黎已失去往日魔力](https://bbs.hupu.com/54897630.html)
> 概要: 虎扑07月25日讯 在接受《踢球者》的采访时，多特蒙德后卫穆尼耶谈到了前队友内马尔以及自己离开巴黎的原因。他表示，他是内马尔的超级球迷，但他认为内马尔在巴黎已经失去往日的魔力。关于离开巴黎的原因穆尼耶
### [有菜有肉不重样，自嗨锅自热盖浇饭 6 盒 39.9 元（减 50 元）](https://lapin.ithome.com/html/digi/631504.htm)
> 概要: 6 味任选，自嗨锅自热盖浇饭 6 盒报价 89.9 元，限时限量 50 元券，实付 39.9 元包邮，领券并购买。6 种口味可以选择，包含：菌菇卤肉味、肉末茄子味、酸笋炒肉味、宫保鸡丁味、菌菇牛肉味、......
# 小说
### [澳洲风云1876](https://m.qidian.com/book/1024234124/catalog)
> 作者：葡萄无牙

> 标签：外国历史

> 简介：这是华夏民族百年沉沦的黑暗时刻，一群淘金者踏上了遥远的澳洲大陆，在这里，他们追寻自己的梦想……

> 章节总数：共1158章

> 状态：完本
# 论文
### [UVCGAN: UNet Vision Transformer cycle-consistent GAN for unpaired image-to-image translation | Papers With Code](https://paperswithcode.com/paper/uvcgan-unet-vision-transformer-cycle)
> 概要: Image-to-image translation has broad applications in art, design, and scientific simulations. The original CycleGAN model emphasizes one-to-one mapping via a cycle-consistent loss, while more recent works promote one-to-many mapping to boost the diversity of the translated images. With scientific simulation and one-to-one needs in mind, this work examines if equipping CycleGAN with a vision transformer (ViT) and employing advanced generative adversarial network (GAN) training techniques can achieve better performance. The resulting UNet ViT Cycle-consistent GAN (UVCGAN) model is compared with previous best-performing models on open benchmark image-to-image translation datasets, Selfie2Anime and CelebA. UVCGAN performs better and retains a strong correlation between the original and translated images. An accompanying ablation study shows that the gradient penalty and BERT-like pre-training also contribute to the improvement.~To promote reproducibility and open science, the source code, hyperparameter configurations, and pre-trained model will be made available at: https://github.com/LS4GAN/uvcga.
### [Fully Convolutional Cross-Scale-Flows for Image-based Defect Detection | Papers With Code](https://paperswithcode.com/paper/fully-convolutional-cross-scale-flows-for)
> 概要: In industrial manufacturing processes, errors frequently occur at unpredictable times and in unknown manifestations. We tackle the problem of automatic defect detection without requiring any image samples of defective parts.
